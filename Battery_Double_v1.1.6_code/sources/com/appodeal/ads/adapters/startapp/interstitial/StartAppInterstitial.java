package com.appodeal.ads.adapters.startapp.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.adapters.startapp.StartAppUnifiedFullscreenListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppAd.AdMode;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;

public class StartAppInterstitial extends UnifiedInterstitial<RequestParams> {
    @VisibleForTesting
    StartAppUnifiedFullscreenListener<UnifiedInterstitialCallback> listener;
    @VisibleForTesting
    StartAppAd startAppAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.startAppAd = new StartAppAd(activity);
        this.listener = new StartAppUnifiedFullscreenListener<>(unifiedInterstitialCallback);
        this.startAppAd.setVideoListener(this.listener);
        this.startAppAd.loadAd(AdMode.FULLPAGE, requestParams.prepareAdPreferences(activity), this.listener);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.startAppAd == null || !this.startAppAd.isReady()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.startAppAd.showAd((AdDisplayListener) this.listener);
        }
    }

    public void onDestroy() {
        this.startAppAd = null;
        this.listener = null;
    }
}
