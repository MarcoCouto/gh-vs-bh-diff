package com.appodeal.ads.adapters.startapp;

import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.VideoListener;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;

public final class StartAppUnifiedFullscreenListener<UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> implements AdDisplayListener, AdEventListener, VideoListener {
    private final UnifiedAdCallbackType callback;

    public StartAppUnifiedFullscreenListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public void onReceiveAd(Ad ad) {
        this.callback.onAdLoaded();
    }

    public void onFailedToReceiveAd(Ad ad) {
        if (ad != null) {
            this.callback.printError(ad.getErrorMessage(), null);
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public void adDisplayed(Ad ad) {
        this.callback.onAdShown();
    }

    public void adNotDisplayed(Ad ad) {
        this.callback.onAdShowFailed();
    }

    public void adClicked(Ad ad) {
        this.callback.onAdClicked();
    }

    public void adHidden(Ad ad) {
        this.callback.onAdClosed();
    }

    public void onVideoCompleted() {
        this.callback.onAdFinished();
    }
}
