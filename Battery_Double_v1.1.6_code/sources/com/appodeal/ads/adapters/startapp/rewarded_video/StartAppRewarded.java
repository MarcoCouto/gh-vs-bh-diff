package com.appodeal.ads.adapters.startapp.rewarded_video;

import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.adapters.startapp.StartAppUnifiedFullscreenListener;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppAd.AdMode;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;

public class StartAppRewarded extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    StartAppUnifiedFullscreenListener<UnifiedRewardedCallback> listener;
    @VisibleForTesting
    StartAppAd startAppAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        if (VERSION.SDK_INT < 16) {
            unifiedRewardedCallback.onAdLoadFailed(LoadingError.AdTypeNotSupportedInAdapter);
            return;
        }
        this.startAppAd = new StartAppAd(activity);
        this.listener = new StartAppUnifiedFullscreenListener<>(unifiedRewardedCallback);
        this.startAppAd.setVideoListener(this.listener);
        this.startAppAd.loadAd(AdMode.REWARDED_VIDEO, requestParams.prepareAdPreferences(activity), this.listener);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.startAppAd == null || !this.startAppAd.isReady()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.startAppAd.showAd((AdDisplayListener) this.listener);
        }
    }

    public void onDestroy() {
        this.startAppAd = null;
        this.listener = null;
    }
}
