package com.appodeal.ads.adapters.startapp.banner;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.adapters.startapp.StartAppUnifiedViewListener;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.ads.banner.BannerListener;

public class StartAppBanner extends UnifiedBanner<RequestParams> {
    private Banner banner;
    @VisibleForTesting
    int bannerHeight;
    @VisibleForTesting
    int bannerWidth;

    static final class StartAppBannerListener extends StartAppUnifiedViewListener<UnifiedBannerCallback> {
        private final StartAppBanner startAppBanner;

        StartAppBannerListener(UnifiedBannerCallback unifiedBannerCallback, StartAppBanner startAppBanner2) {
            super(unifiedBannerCallback);
            this.startAppBanner = startAppBanner2;
        }

        public void onReceiveAd(View view) {
            ((UnifiedBannerCallback) this.callback).onAdLoaded(view, this.startAppBanner.bannerWidth, this.startAppBanner.bannerHeight);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        this.banner = new Banner(activity, requestParams.prepareAdPreferences(activity), (BannerListener) new StartAppBannerListener(unifiedBannerCallback, this));
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            this.bannerHeight = 90;
            this.bannerWidth = 728;
        } else {
            this.bannerHeight = 50;
            this.bannerWidth = ModuleDescriptor.MODULE_VERSION;
        }
        this.banner.loadAd(this.bannerWidth, this.bannerHeight);
    }

    public void onDestroy() {
        this.banner = null;
    }
}
