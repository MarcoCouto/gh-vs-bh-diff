package com.appodeal.ads.adapters.startapp.mrec;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import com.appodeal.ads.adapters.startapp.StartAppNetwork.RequestParams;
import com.appodeal.ads.adapters.startapp.StartAppUnifiedViewListener;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.startapp.sdk.ads.banner.BannerListener;
import com.startapp.sdk.ads.banner.Mrec;

public class StartAppMrec extends UnifiedMrec<RequestParams> {
    private Mrec mrecView;

    static final class StartAppMrecListener extends StartAppUnifiedViewListener<UnifiedMrecCallback> {
        StartAppMrecListener(UnifiedMrecCallback unifiedMrecCallback) {
            super(unifiedMrecCallback);
        }

        public void onReceiveAd(View view) {
            ((UnifiedMrecCallback) this.callback).onAdLoaded(view);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.mrecView = new Mrec(activity, requestParams.prepareAdPreferences(activity), (BannerListener) new StartAppMrecListener(unifiedMrecCallback));
        this.mrecView.loadAd();
    }

    public void onDestroy() {
        this.mrecView = null;
    }
}
