package com.appodeal.ads.adapters.tapjoy;

import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJError;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJPlacementVideoListener;

public class TapjoyUnifiedListener<UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> implements TJPlacementListener, TJPlacementVideoListener {
    private final UnifiedAdCallbackType callback;

    public void onPurchaseRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str) {
    }

    public void onRewardRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str, int i) {
    }

    public void onVideoError(TJPlacement tJPlacement, String str) {
    }

    public void onVideoStart(TJPlacement tJPlacement) {
    }

    public TapjoyUnifiedListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public void onRequestSuccess(TJPlacement tJPlacement) {
        if (!tJPlacement.isContentAvailable()) {
            this.callback.printError("Content unavailable (tjPlacement.isContentAvailable() is false).", null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }
    }

    public void onRequestFailure(TJPlacement tJPlacement, TJError tJError) {
        if (tJError != null) {
            this.callback.printError(tJError.message, Integer.valueOf(tJError.code));
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public void onContentReady(TJPlacement tJPlacement) {
        this.callback.onAdLoaded();
    }

    public void onContentShow(TJPlacement tJPlacement) {
        this.callback.onAdShown();
    }

    public void onClick(TJPlacement tJPlacement) {
        this.callback.onAdClicked();
    }

    public void onVideoComplete(TJPlacement tJPlacement) {
        this.callback.onAdFinished();
    }

    public void onContentDismiss(TJPlacement tJPlacement) {
        this.callback.onAdClosed();
    }
}
