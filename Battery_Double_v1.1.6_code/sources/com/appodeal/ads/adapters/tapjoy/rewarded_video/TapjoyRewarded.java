package com.appodeal.ads.adapters.tapjoy.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.tapjoy.TapjoyNetwork.RequestParams;
import com.appodeal.ads.adapters.tapjoy.TapjoyUnifiedListener;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.tapjoy.TJPlacement;
import com.tapjoy.Tapjoy;

public class TapjoyRewarded extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    TJPlacement placement;

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        TapjoyUnifiedListener tapjoyUnifiedListener = new TapjoyUnifiedListener(unifiedRewardedCallback);
        this.placement = Tapjoy.getLimitedPlacement(requestParams.placementName, tapjoyUnifiedListener);
        this.placement.setVideoListener(tapjoyUnifiedListener);
        requestParams.applyParams(this.placement);
        this.placement.requestContent();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.placement == null || !this.placement.isContentReady()) {
            unifiedRewardedCallback.onAdShowFailed();
            return;
        }
        Tapjoy.setActivity(activity);
        this.placement.showContent();
    }

    public void onDestroy() {
        if (this.placement != null) {
            this.placement.setVideoListener(null);
            this.placement = null;
        }
    }
}
