package com.appodeal.ads.adapters.tapjoy.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.tapjoy.TapjoyNetwork.RequestParams;
import com.appodeal.ads.adapters.tapjoy.TapjoyUnifiedListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.tapjoy.TJPlacement;
import com.tapjoy.Tapjoy;

public class TapjoyVideo extends UnifiedVideo<RequestParams> {
    @VisibleForTesting
    TJPlacement placement;

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        TapjoyUnifiedListener tapjoyUnifiedListener = new TapjoyUnifiedListener(unifiedVideoCallback);
        this.placement = Tapjoy.getLimitedPlacement(requestParams.placementName, tapjoyUnifiedListener);
        this.placement.setVideoListener(tapjoyUnifiedListener);
        requestParams.applyParams(this.placement);
        this.placement.requestContent();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.placement == null || !this.placement.isContentReady()) {
            unifiedVideoCallback.onAdShowFailed();
            return;
        }
        Tapjoy.setActivity(activity);
        this.placement.showContent();
    }

    public void onDestroy() {
        if (this.placement != null) {
            this.placement.setVideoListener(null);
            this.placement = null;
        }
    }
}
