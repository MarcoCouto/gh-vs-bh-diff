package com.appodeal.ads.adapters.ironsource.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.ironsource.mediationsdk.IronSource;

public class IronSourceRewarded extends UnifiedRewarded<RequestParams> {
    private String instanceId;

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.instanceId = requestParams.instanceId;
        boolean isISDemandOnlyRewardedVideoAvailable = IronSource.isISDemandOnlyRewardedVideoAvailable(this.instanceId);
        IronSource.setISDemandOnlyRewardedVideoListener(new IronSourceRewardedListener(this.instanceId, unifiedRewardedCallback, isISDemandOnlyRewardedVideoAvailable));
        if (isISDemandOnlyRewardedVideoAvailable) {
            unifiedRewardedCallback.onAdLoaded();
        } else {
            IronSource.loadISDemandOnlyRewardedVideo(this.instanceId);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (IronSource.isISDemandOnlyRewardedVideoAvailable(this.instanceId)) {
            IronSource.showISDemandOnlyRewardedVideo(this.instanceId);
        } else {
            unifiedRewardedCallback.onAdShowFailed();
        }
    }
}
