package com.appodeal.ads.adapters.ironsource.video;

import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.ironsource.IronSourceNetwork;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;

class IronSourceVideoListener implements ISDemandOnlyInterstitialListener {
    @NonNull
    private final IronSourceVideo adObject;
    @NonNull
    private final UnifiedVideoCallback callback;

    IronSourceVideoListener(@NonNull IronSourceVideo ironSourceVideo, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        this.adObject = ironSourceVideo;
        this.callback = unifiedVideoCallback;
    }

    public void onInterstitialAdReady(String str) {
        if (this.adObject.isLoaded || this.adObject.isLoadFailed) {
            IronSourceNetwork.unsubscribeInterstitialListener(str);
            IronSourceNetwork.setInProgressInstance(false);
            if (this.adObject.isLoaded) {
                this.callback.onAdExpired();
            }
            return;
        }
        this.callback.onAdLoaded();
    }

    public void onInterstitialAdLoadFailed(String str, IronSourceError ironSourceError) {
        IronSourceNetwork.unsubscribeInterstitialListener(str);
        IronSourceNetwork.prepareInstance();
        if (ironSourceError != null) {
            this.callback.printError(ironSourceError.getErrorMessage(), Integer.valueOf(ironSourceError.getErrorCode()));
            this.callback.onAdLoadFailed(IronSourceNetwork.mapError(ironSourceError.getErrorCode()));
            return;
        }
        this.callback.onAdLoadFailed(null);
    }

    public void onInterstitialAdOpened(String str) {
        this.callback.onAdShown();
    }

    public void onInterstitialAdClosed(String str) {
        IronSourceNetwork.unsubscribeInterstitialListener(str);
        IronSourceNetwork.setInProgressInstance(false);
        this.callback.onAdClosed();
    }

    public void onInterstitialAdShowFailed(String str, IronSourceError ironSourceError) {
        IronSourceNetwork.unsubscribeInterstitialListener(str);
        IronSourceNetwork.setInProgressInstance(false);
        if (ironSourceError != null) {
            this.callback.printError(ironSourceError.getErrorMessage(), Integer.valueOf(ironSourceError.getErrorCode()));
        }
        this.callback.onAdShowFailed();
    }

    public void onInterstitialAdClicked(String str) {
        this.callback.onAdClicked();
    }
}
