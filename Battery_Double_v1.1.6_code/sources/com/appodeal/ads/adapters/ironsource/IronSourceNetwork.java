package com.appodeal.ads.adapters.ironsource;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.ironsource.interstitial.IronSourceInterstitial;
import com.appodeal.ads.adapters.ironsource.rewarded_video.IronSourceRewarded;
import com.appodeal.ads.adapters.ironsource.video.IronSourceVideo;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedAppStateChangeListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.app.AppState;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceInitializer;
import com.ironsource.mediationsdk.IronSourceInitializer.IronSourceInitializationListener;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.LogListener;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import org.json.JSONArray;
import org.json.JSONObject;

public class IronSourceNetwork extends AdNetwork<RequestParams> {
    private static final String DEFAULT_INSTANCE = "0";
    private static final UnifiedAppStateChangeListener appStateChangeListener = new UnifiedAppStateChangeListener() {
        public void onAppStateChanged(@Nullable Activity activity, @NonNull AppState appState, boolean z) {
            if (!z) {
                switch (AnonymousClass4.$SwitchMap$com$appodeal$ads$utils$app$AppState[appState.ordinal()]) {
                    case 1:
                        IronSource.onResume(activity);
                        return;
                    case 2:
                        IronSource.onPause(activity);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private static final IronSourceInitializer initializer = new IronSourceInitializer();
    private static boolean instanceInProgress = false;
    @VisibleForTesting
    public static Queue<String> instancesList = new LinkedList();
    @VisibleForTesting
    public static final Map<String, ISDemandOnlyInterstitialListener> interstitialListeners = new HashMap();
    @VisibleForTesting
    public static boolean isInitialized = false;

    /* renamed from: com.appodeal.ads.adapters.ironsource.IronSourceNetwork$4 reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] $SwitchMap$com$appodeal$ads$utils$app$AppState = new int[AppState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            $SwitchMap$com$appodeal$ads$utils$app$AppState[AppState.Resumed.ordinal()] = 1;
            $SwitchMap$com$appodeal$ads$utils$app$AppState[AppState.Paused.ordinal()] = 2;
        }
    }

    @VisibleForTesting
    static class GlobalInterstitialListener implements ISDemandOnlyInterstitialListener {
        GlobalInterstitialListener() {
        }

        public void onInterstitialAdReady(String str) {
            ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener = (ISDemandOnlyInterstitialListener) IronSourceNetwork.interstitialListeners.get(str);
            if (iSDemandOnlyInterstitialListener != null) {
                iSDemandOnlyInterstitialListener.onInterstitialAdReady(str);
            }
        }

        public void onInterstitialAdLoadFailed(String str, IronSourceError ironSourceError) {
            ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener = (ISDemandOnlyInterstitialListener) IronSourceNetwork.interstitialListeners.get(str);
            if (iSDemandOnlyInterstitialListener != null) {
                iSDemandOnlyInterstitialListener.onInterstitialAdLoadFailed(str, ironSourceError);
            }
        }

        public void onInterstitialAdOpened(String str) {
            ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener = (ISDemandOnlyInterstitialListener) IronSourceNetwork.interstitialListeners.get(str);
            if (iSDemandOnlyInterstitialListener != null) {
                iSDemandOnlyInterstitialListener.onInterstitialAdOpened(str);
            }
        }

        public void onInterstitialAdClosed(String str) {
            ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener = (ISDemandOnlyInterstitialListener) IronSourceNetwork.interstitialListeners.get(str);
            if (iSDemandOnlyInterstitialListener != null) {
                iSDemandOnlyInterstitialListener.onInterstitialAdClosed(str);
            }
        }

        public void onInterstitialAdShowFailed(String str, IronSourceError ironSourceError) {
            ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener = (ISDemandOnlyInterstitialListener) IronSourceNetwork.interstitialListeners.get(str);
            if (iSDemandOnlyInterstitialListener != null) {
                iSDemandOnlyInterstitialListener.onInterstitialAdShowFailed(str, ironSourceError);
            }
        }

        public void onInterstitialAdClicked(String str) {
            ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener = (ISDemandOnlyInterstitialListener) IronSourceNetwork.interstitialListeners.get(str);
            if (iSDemandOnlyInterstitialListener != null) {
                iSDemandOnlyInterstitialListener.onInterstitialAdClicked(str);
            }
        }
    }

    public static final class RequestParams {
        @NonNull
        public final String instanceId;
        @NonNull
        public final JSONObject jsonData;

        RequestParams(@NonNull String str, @NonNull JSONObject jSONObject) {
            this.instanceId = str;
            this.jsonData = jSONObject;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "3";
        }

        public String getName() {
            return "ironsource";
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.ironsource.sdk.controller.ControllerActivity").build(), new Builder("com.ironsource.sdk.controller.InterstitialActivity").build(), new Builder("com.ironsource.sdk.controller.OpenUrlActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.ironsource.mediationsdk.IronSource"};
        }

        public IronSourceNetwork build() {
            return new IronSourceNetwork(this);
        }
    }

    public boolean canLoadInterstitialWhenDisplaying() {
        return false;
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    private IronSourceNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public UnifiedAppStateChangeListener getAppStateChangeListener() {
        return appStateChangeListener;
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new IronSourceInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new IronSourceVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new IronSourceRewarded();
    }

    public LoadingError verifyLoadAvailability(@NonNull AdType adType) {
        if ((adType == AdType.Interstitial || adType == AdType.Video) && (isInterstitialShowing() || isVideoShowing())) {
            return LoadingError.Canceled;
        }
        return super.verifyLoadAvailability(adType);
    }

    public String getVersion() {
        return IronSourceUtils.getSDKVersion();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull final NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (!isGooglePlayServicesAvailable(activity)) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        String string = adUnit.getJsonData().getString("app_key");
        String optString = adUnit.getJsonData().optString("instance_id", "0");
        updateConsent(adNetworkMediationParams.getRestrictedData());
        setTargeting(adNetworkMediationParams.getRestrictedData());
        setMediatorName(adUnit.getMediatorName());
        final RequestParams requestParams = new RequestParams(optString, adUnit.getJsonData());
        if (!isInitialized) {
            isInitialized = true;
            IronSource.setLogListener(new LogListener() {
                public void onLog(IronSourceTag ironSourceTag, String str, int i) {
                    Log.log(LogConstants.KEY_NETWORK, "Log", String.format("IronSource %s:%s", new Object[]{ironSourceTag, str}));
                }
            });
            IronSource.setISDemandOnlyInterstitialListener(new GlobalInterstitialListener());
            initializer.init(activity, string, new IronSourceInitializationListener() {
                public void onInitialized() {
                    try {
                        networkInitializationListener.onInitializationFinished(requestParams);
                    } catch (Exception unused) {
                        networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
                    }
                }

                public void onInitializationFailed() {
                    networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
                }
            });
        } else {
            networkInitializationListener.onInitializationFinished(requestParams);
        }
    }

    @VisibleForTesting
    public boolean isGooglePlayServicesAvailable(Activity activity) {
        return UnifiedAdUtils.isGooglePlayServicesAvailable(activity);
    }

    @VisibleForTesting
    public void updateConsent(@NonNull RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            IronSource.setConsent(restrictedData.isUserHasConsent());
        }
    }

    private void setTargeting(@NonNull RestrictedData restrictedData) {
        Gender gender = restrictedData.getGender();
        if (gender == Gender.FEMALE) {
            IronSource.setGender("female");
        } else if (gender == Gender.MALE) {
            IronSource.setGender("male");
        }
        Integer age = restrictedData.getAge();
        if (age != null) {
            IronSource.setAge(age.intValue());
        }
    }

    private void setMediatorName(String str) {
        if (!TextUtils.isEmpty(str)) {
            IronSource.setMediationType(str);
        }
    }

    public static void prepareInstance() {
        instanceInProgress = false;
        instancesList.poll();
    }

    public static void registerInterstitialInstances(@Nullable JSONArray jSONArray) {
        if (instancesList.isEmpty()) {
            if (jSONArray == null || jSONArray.length() == 0) {
                jSONArray = new JSONArray();
                jSONArray.put("0");
            }
            for (int i = 0; i < jSONArray.length(); i++) {
                instancesList.add(jSONArray.optString(i));
            }
        }
    }

    public static boolean canLoadInstance(@NonNull String str) {
        return !instanceInProgress && str.equals(instancesList.peek());
    }

    public static void setInProgressInstance(boolean z) {
        instanceInProgress = z;
    }

    public static boolean isInstanceInProgress() {
        return instanceInProgress;
    }

    public static void subscribeInterstitialListener(String str, ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener) {
        interstitialListeners.put(str, iSDemandOnlyInterstitialListener);
    }

    public static void unsubscribeInterstitialListener(String str) {
        interstitialListeners.remove(str);
    }

    public static LoadingError mapError(int i) {
        if (i != 501) {
            if (i == 520) {
                return LoadingError.ConnectionError;
            }
            if (i != 1037) {
                switch (i) {
                    case IronSourceError.ERROR_CODE_KEY_NOT_SET /*505*/:
                    case IronSourceError.ERROR_CODE_INVALID_KEY_VALUE /*506*/:
                        break;
                    default:
                        switch (i) {
                            case IronSourceError.ERROR_CODE_INIT_FAILED /*508*/:
                                break;
                            case IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW /*509*/:
                            case IronSourceError.ERROR_CODE_GENERIC /*510*/:
                                return LoadingError.NoFill;
                            default:
                                return null;
                        }
                }
            }
        }
        return LoadingError.InternalError;
    }
}
