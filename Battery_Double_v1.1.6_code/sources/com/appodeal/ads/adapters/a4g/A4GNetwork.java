package com.appodeal.ads.adapters.a4g;

import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AppodealNetworks;
import com.appodeal.ads.adapters.admob.AdmobNetwork;

public class A4GNetwork extends AdmobNetwork {

    public static class builder extends com.appodeal.ads.adapters.admob.AdmobNetwork.builder {
        public String getAdapterVersion() {
            return "2";
        }

        public String getName() {
            return AppodealNetworks.A4G;
        }

        public AdmobNetwork build() {
            return new A4GNetwork(this);
        }
    }

    public A4GNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }
}
