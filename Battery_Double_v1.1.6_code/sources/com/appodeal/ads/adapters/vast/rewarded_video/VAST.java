package com.appodeal.ads.adapters.vast.rewarded_video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.vast.VASTNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.appodeal.ads.unified.vast.UnifiedVastNetworkParams;
import com.appodeal.ads.unified.vast.UnifiedVastRewarded;

public class VAST extends UnifiedVastRewarded<RequestParams> {
    @Nullable
    public UnifiedVastNetworkParams obtainVastParams(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        return requestParams;
    }

    public void performVastRequest(@NonNull Context context, @NonNull final UnifiedRewardedParams unifiedRewardedParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull final UnifiedRewardedCallback unifiedRewardedCallback, @NonNull String str) {
        S2SAdTask.requestVast(context, str, unifiedVastNetworkParams, unifiedRewardedCallback, new Callback<UnifiedVastNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams) {
                VAST.this.loadVast(context, unifiedRewardedParams, unifiedVastNetworkParams, unifiedRewardedCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedRewardedCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
