package com.appodeal.ads.adapters.vast;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.AppodealNetworks;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.vast.rewarded_video.VAST;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.vast.UnifiedVastNetworkParams;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.explorestack.iab.BuildConfig;
import com.explorestack.iab.IabSettings;
import com.explorestack.iab.utils.Logger.LogLevel;
import com.explorestack.iab.vast.VastLog;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public class VASTNetwork extends AdNetwork<RequestParams> {

    public static class RequestParams extends UnifiedVastNetworkParams {
        public RequestParams(RestrictedData restrictedData, String str, String str2, String str3, int i, long j, boolean z, boolean z2) {
            super(restrictedData, str, str2, str3, i, j, z, z2);
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return AppodealNetworks.VAST;
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.explorestack.iab.vast.activity.VastActivity").build(), new Builder("com.explorestack.iab.mraid.activity.MraidActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.explorestack.iab.vast.VastRequest", "com.explorestack.iab.mraid.MRAIDView", "com.explorestack.iab.mraid.MRAIDInterstitial"};
        }

        public AdNetwork build() {
            return new VASTNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public VASTNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
        IabSettings.mediatorVersion = Appodeal.getVersion();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new VAST();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new com.appodeal.ads.adapters.vast.video.VAST();
    }

    public void setLogging(boolean z) {
        if (z) {
            VastLog.setLoggingLevel(LogLevel.debug);
        } else {
            VastLog.setLoggingLevel(LogLevel.none);
        }
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        JSONObject jsonData = adUnit.getJsonData();
        String optString = jsonData.optString("package");
        int optInt = jsonData.optInt("close_time", 0);
        long optLong = jsonData.optLong("expiry");
        boolean optBoolean = jsonData.optBoolean("use_layout", true);
        String optString2 = jsonData.optString("vast_xml");
        String optString3 = jsonData.optString("vast_url");
        String parseUrlWithTopParams = jsonData.optBoolean(String.TOP, false) ? UnifiedAdUtils.parseUrlWithTopParams(activity, optString3, adNetworkMediationParams) : optString3;
        boolean optBoolean2 = jsonData.optBoolean("video_auto_close", false);
        if ((TextUtils.isEmpty(optString2) || TextUtils.getTrimmedLength(optString2) == 0) && (TextUtils.isEmpty(parseUrlWithTopParams) || TextUtils.getTrimmedLength(parseUrlWithTopParams) == 0)) {
            networkInitializationListener.onInitializationFailed(LoadingError.IncorrectAdunit);
            return;
        }
        RequestParams requestParams = new RequestParams(adNetworkMediationParams.getRestrictedData(), optString2, parseUrlWithTopParams, optString, optInt, optLong, optBoolean2, optBoolean);
        networkInitializationListener.onInitializationFinished(requestParams);
    }
}
