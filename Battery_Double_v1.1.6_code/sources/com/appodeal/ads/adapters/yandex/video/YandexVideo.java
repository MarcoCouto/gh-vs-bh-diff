package com.appodeal.ads.adapters.yandex.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.adapters.yandex.YandexNetwork.RequestParams;
import com.appodeal.ads.adapters.yandex.YandexUnifiedFullscreenListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.yandex.mobile.ads.InterstitialAd;

public class YandexVideo extends UnifiedVideo<RequestParams> {
    @Nullable
    private InterstitialAd videoAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.videoAd = new InterstitialAd(activity);
        this.videoAd.setBlockId(requestParams.yandexKey);
        this.videoAd.setInterstitialEventListener(new YandexUnifiedFullscreenListener(unifiedVideoCallback));
        this.videoAd.loadAd(requestParams.adRequest);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.videoAd == null || !this.videoAd.isLoaded()) {
            unifiedVideoCallback.onAdShowFailed();
        } else {
            this.videoAd.show();
        }
    }

    public void onDestroy() {
        if (this.videoAd != null) {
            this.videoAd.destroy();
            this.videoAd = null;
        }
    }
}
