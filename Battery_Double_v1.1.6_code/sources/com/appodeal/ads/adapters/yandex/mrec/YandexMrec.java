package com.appodeal.ads.adapters.yandex.mrec;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.yandex.YandexNetwork.RequestParams;
import com.appodeal.ads.adapters.yandex.YandexUnifiedViewListener;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.AdView;

public class YandexMrec extends UnifiedMrec<RequestParams> {
    @Nullable
    private AdView mrecView;

    @VisibleForTesting
    static final class YandexMrecListener extends YandexUnifiedViewListener<UnifiedMrecCallback> {
        private final AdView mrecView;

        YandexMrecListener(UnifiedMrecCallback unifiedMrecCallback, AdView adView) {
            super(unifiedMrecCallback);
            this.mrecView = adView;
        }

        public void onAdLoaded() {
            ((UnifiedMrecCallback) this.callback).onAdLoaded(this.mrecView);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.mrecView = new AdView(activity);
        this.mrecView.setBlockId(requestParams.yandexKey);
        this.mrecView.setAdSize(AdSize.BANNER_300x250);
        this.mrecView.setAdEventListener(new YandexMrecListener(unifiedMrecCallback, this.mrecView));
        this.mrecView.loadAd(requestParams.adRequest);
    }

    public void onDestroy() {
        if (this.mrecView != null) {
            this.mrecView.destroy();
            this.mrecView = null;
        }
    }
}
