package com.appodeal.ads.adapters.yandex;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedAdCallback;
import com.yandex.mobile.ads.AdEventListener.SimpleAdEventListener;
import com.yandex.mobile.ads.AdRequestError;

public class YandexUnifiedViewListener<UnifiedAdCallbackType extends UnifiedAdCallback> extends SimpleAdEventListener {
    protected final UnifiedAdCallbackType callback;

    public YandexUnifiedViewListener(UnifiedAdCallbackType unifiedadcallbacktype) {
        this.callback = unifiedadcallbacktype;
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        if (adRequestError != null) {
            this.callback.printError(adRequestError.getDescription(), Integer.valueOf(adRequestError.getCode()));
        }
        this.callback.onAdLoadFailed(YandexNetwork.mapError(adRequestError));
    }

    public final void onAdLeftApplication() {
        this.callback.onAdClicked();
    }
}
