package com.appodeal.ads.adapters.smaato;

public final class R {

    public static final class attr {
        public static final int smaato_sdk_video_cpb_background_progressbar_color = 2130968866;
        public static final int smaato_sdk_video_cpb_background_progressbar_width = 2130968867;
        public static final int smaato_sdk_video_cpb_label_font_size = 2130968868;
        public static final int smaato_sdk_video_cpb_progressbar_color = 2130968869;
        public static final int smaato_sdk_video_cpb_progressbar_width = 2130968870;

        private attr() {
        }
    }

    public static final class color {
        public static final int smaato_sdk_core_ui_ctrl_almost_white = 2131099799;
        public static final int smaato_sdk_core_ui_ctrl_black = 2131099800;
        public static final int smaato_sdk_core_ui_ctrl_grey = 2131099801;
        public static final int smaato_sdk_core_ui_semitransparent = 2131099802;
        public static final int smaato_sdk_richmedia_ui_semitransparent = 2131099803;
        public static final int smaato_sdk_video_blue = 2131099804;
        public static final int smaato_sdk_video_grey = 2131099805;

        private color() {
        }
    }

    public static final class dimen {
        public static final int smaato_sdk_core_activity_margin = 2131165373;
        public static final int smaato_sdk_video_default_background_stroke_width = 2131165374;
        public static final int smaato_sdk_video_default_stroke_width = 2131165375;
        public static final int smaato_sdk_video_progress_label_font_size = 2131165376;
        public static final int smaato_sdk_video_touch_target_minimum = 2131165377;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int smaato_sdk_core_back = 2131231091;
        public static final int smaato_sdk_core_back_disabled = 2131231092;
        public static final int smaato_sdk_core_background = 2131231093;
        public static final int smaato_sdk_core_browser_bottom_button_layout_bg = 2131231094;
        public static final int smaato_sdk_core_browser_progress_bar = 2131231095;
        public static final int smaato_sdk_core_browser_top_button_layout_bg = 2131231096;
        public static final int smaato_sdk_core_circle_close = 2131231097;
        public static final int smaato_sdk_core_close = 2131231098;
        public static final int smaato_sdk_core_forward = 2131231099;
        public static final int smaato_sdk_core_forward_disabled = 2131231100;
        public static final int smaato_sdk_core_ic_browser_background_selector = 2131231101;
        public static final int smaato_sdk_core_ic_browser_backward_selector = 2131231102;
        public static final int smaato_sdk_core_ic_browser_forward_selector = 2131231103;
        public static final int smaato_sdk_core_ic_browser_secure_connection = 2131231104;
        public static final int smaato_sdk_core_lock = 2131231105;
        public static final int smaato_sdk_core_open_in_browser = 2131231106;
        public static final int smaato_sdk_core_refresh = 2131231107;
        public static final int smaato_sdk_core_watermark = 2131231108;
        public static final int smaato_sdk_richmedia_progress_bar = 2131231109;
        public static final int smaato_sdk_video_muted = 2131231110;
        public static final int smaato_sdk_video_skip = 2131231111;
        public static final int smaato_sdk_video_unmuted = 2131231112;

        private drawable() {
        }
    }

    public static final class id {
        public static final int btnBackward = 2131296342;
        public static final int btnClose = 2131296343;
        public static final int btnForward = 2131296344;
        public static final int btnLayoutBottom = 2131296345;
        public static final int btnLayoutTop = 2131296346;
        public static final int btnOpenExternal = 2131296347;
        public static final int btnRefresh = 2131296348;
        public static final int close = 2131296366;
        public static final int container = 2131296379;
        public static final int progressBar = 2131296508;
        public static final int smaato_sdk_interstitial_close = 2131296543;
        public static final int smaato_sdk_interstitial_content = 2131296544;
        public static final int smaato_sdk_rewarded_ads_close = 2131296545;
        public static final int smaato_sdk_rewarded_ads_content = 2131296546;
        public static final int smaato_sdk_video_companion_view_id = 2131296547;
        public static final int smaato_sdk_video_icon_view_id = 2131296548;
        public static final int smaato_sdk_video_mute_button = 2131296549;
        public static final int smaato_sdk_video_player_layout = 2131296550;
        public static final int smaato_sdk_video_player_surface_layout = 2131296551;
        public static final int smaato_sdk_video_skip_button = 2131296552;
        public static final int smaato_sdk_video_surface_holder_view_id = 2131296553;
        public static final int smaato_sdk_video_video_player_view_id = 2131296554;
        public static final int smaato_sdk_video_video_progress = 2131296555;
        public static final int smaato_sdk_video_watermark_button_id = 2131296556;
        public static final int tvHostname = 2131296616;
        public static final int webView = 2131296627;

        private id() {
        }
    }

    public static final class layout {
        public static final int smaato_sdk_core_activity_internal_browser = 2131427430;
        public static final int smaato_sdk_interstitial_activity = 2131427431;
        public static final int smaato_sdk_rewarded_ads_activity = 2131427432;
        public static final int smaato_sdk_richmedia_layout_closable = 2131427433;
        public static final int smaato_sdk_video_player_view = 2131427434;
        public static final int smaato_sdk_video_vast_video_player_view = 2131427435;

        private layout() {
        }
    }

    public static final class string {
        public static final int smaato_sdk_core_browser_hostname_content_description = 2131624215;
        public static final int smaato_sdk_core_btn_browser_backward_content_description = 2131624216;
        public static final int smaato_sdk_core_btn_browser_close_content_description = 2131624217;
        public static final int smaato_sdk_core_btn_browser_forward_content_description = 2131624218;
        public static final int smaato_sdk_core_btn_browser_open_content_description = 2131624219;
        public static final int smaato_sdk_core_btn_browser_refresh_content_description = 2131624220;
        public static final int smaato_sdk_core_fullscreen_dimension = 2131624221;
        public static final int smaato_sdk_core_no_external_browser_found = 2131624222;
        public static final int smaato_sdk_richmedia_collapse_mraid_ad = 2131624223;
        public static final int smaato_sdk_video_close_button_text = 2131624224;
        public static final int smaato_sdk_video_mute_button_text = 2131624225;
        public static final int smaato_sdk_video_skip_button_text = 2131624226;

        private string() {
        }
    }

    public static final class style {
        public static final int smaato_sdk_core_browserProgressBar = 2131689896;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] smaato_sdk_video_circular_progress_bar = {com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_background_progressbar_color, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_background_progressbar_width, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_label_font_size, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_progressbar_color, com.mansoon.BatteryDouble.R.attr.smaato_sdk_video_cpb_progressbar_width};
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_background_progressbar_color = 0;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_background_progressbar_width = 1;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_label_font_size = 2;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_progressbar_color = 3;
        public static final int smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_progressbar_width = 4;

        private styleable() {
        }
    }

    private R() {
    }
}
