package com.appodeal.ads.adapters.smaato.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.smaato.AdContainer;
import com.appodeal.ads.adapters.smaato.SmaatoNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.smaato.sdk.rewarded.EventListener;
import com.smaato.sdk.rewarded.RewardedError;
import com.smaato.sdk.rewarded.RewardedInterstitial;
import com.smaato.sdk.rewarded.RewardedInterstitialAd;
import com.smaato.sdk.rewarded.RewardedRequestError;

public class SmaatoRewarded extends UnifiedRewarded<RequestParams> {
    @VisibleForTesting
    RewardedAdContainer rewardedAdContainer;

    @VisibleForTesting
    static class RewardedAdContainer extends AdContainer<RewardedInterstitialAd> implements EventListener {
        private final UnifiedRewardedCallback callback;

        RewardedAdContainer(@NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
            this.callback = unifiedRewardedCallback;
        }

        public void onAdLoaded(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
            setAd(rewardedInterstitialAd);
            this.callback.onAdLoaded();
        }

        public void onAdFailedToLoad(@NonNull RewardedRequestError rewardedRequestError) {
            onFailedToLoad(rewardedRequestError != null ? rewardedRequestError.getRewardedError() : null);
        }

        public void onAdError(@NonNull RewardedInterstitialAd rewardedInterstitialAd, @NonNull RewardedError rewardedError) {
            if (isShown()) {
                if (rewardedError != null) {
                    this.callback.printError(rewardedError.toString(), null);
                }
                this.callback.onAdShowFailed();
                return;
            }
            onFailedToLoad(rewardedError);
        }

        public void onAdClosed(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
            this.callback.onAdClosed();
        }

        public void onAdClicked(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
            this.callback.onAdClicked();
        }

        public void onAdStarted(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
            this.callback.onAdShown();
        }

        public void onAdReward(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
            this.callback.onAdFinished();
        }

        public void onAdTTLExpired(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
            this.callback.onAdExpired();
        }

        private void onFailedToLoad(@Nullable RewardedError rewardedError) {
            if (rewardedError != null) {
                this.callback.printError(rewardedError.toString(), null);
                switch (rewardedError) {
                    case NETWORK_ERROR:
                        this.callback.onAdLoadFailed(LoadingError.ConnectionError);
                        return;
                    case INVALID_REQUEST:
                        this.callback.onAdLoadFailed(LoadingError.IncorrectAdunit);
                        return;
                    case CREATIVE_RESOURCE_EXPIRED:
                    case INTERNAL_ERROR:
                        this.callback.onAdLoadFailed(LoadingError.InternalError);
                        return;
                    default:
                        this.callback.onAdLoadFailed(LoadingError.NoFill);
                        return;
                }
            } else {
                this.callback.onAdLoadFailed(LoadingError.NoFill);
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.rewardedAdContainer = new RewardedAdContainer(unifiedRewardedCallback);
        if (!TextUtils.isEmpty(requestParams.mediatorName)) {
            RewardedInterstitial.setMediationNetworkName(requestParams.mediatorName);
            RewardedInterstitial.setMediationNetworkSDKVersion(Appodeal.getVersion());
            RewardedInterstitial.setMediationAdapterVersion(requestParams.adapterVersion);
        }
        RewardedInterstitial.loadAd(requestParams.adSpaceId, this.rewardedAdContainer);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        RewardedInterstitialAd rewardedInterstitialAd = this.rewardedAdContainer != null ? (RewardedInterstitialAd) this.rewardedAdContainer.getAd() : null;
        if (rewardedInterstitialAd == null || !rewardedInterstitialAd.isAvailableForPresentation()) {
            unifiedRewardedCallback.onAdShowFailed();
            return;
        }
        this.rewardedAdContainer.setShown(true);
        rewardedInterstitialAd.showAd();
    }

    public void onDestroy() {
        if (this.rewardedAdContainer != null) {
            this.rewardedAdContainer.destroy();
            this.rewardedAdContainer = null;
        }
    }
}
