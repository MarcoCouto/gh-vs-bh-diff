package com.appodeal.ads.adapters.smaato.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.adapters.smaato.InterstitialAdContainer;
import com.appodeal.ads.adapters.smaato.SmaatoNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.smaato.sdk.interstitial.Interstitial;
import com.smaato.sdk.interstitial.InterstitialAd;

public class SmaatoVideo extends UnifiedVideo<RequestParams> {
    @VisibleForTesting
    InterstitialAdContainer interstitialAdContainer;

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.interstitialAdContainer = new InterstitialAdContainer(unifiedVideoCallback);
        if (!TextUtils.isEmpty(requestParams.mediatorName)) {
            Interstitial.setMediationNetworkName(requestParams.mediatorName);
            Interstitial.setMediationNetworkSDKVersion(Appodeal.getVersion());
            Interstitial.setMediationAdapterVersion(requestParams.adapterVersion);
        }
        Interstitial.loadAd(requestParams.adSpaceId, this.interstitialAdContainer);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        InterstitialAd interstitialAd = this.interstitialAdContainer != null ? (InterstitialAd) this.interstitialAdContainer.getAd() : null;
        if (interstitialAd == null || !interstitialAd.isAvailableForPresentation()) {
            unifiedVideoCallback.onAdShowFailed();
            return;
        }
        this.interstitialAdContainer.setShown(true);
        interstitialAd.showAd(activity);
    }

    public void onDestroy() {
        if (this.interstitialAdContainer != null) {
            this.interstitialAdContainer.destroy();
            this.interstitialAdContainer = null;
        }
    }
}
