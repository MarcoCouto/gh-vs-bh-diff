package com.appodeal.ads.adapters.mytarget.mrec;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.mytarget.MyTargetNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.my.target.ads.MyTargetView;

public class MyTargetMrec extends UnifiedMrec<RequestParams> {
    private MyTargetView mrecView;

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.mrecView = new MyTargetView(activity);
        this.mrecView.init(requestParams.myTargetSlot, 1, false);
        requestParams.applyTargeting(this.mrecView.getCustomParams());
        this.mrecView.setListener(new MyTargetMrecListener(unifiedMrecCallback));
        this.mrecView.load();
    }

    public void onDestroy() {
        if (this.mrecView != null) {
            this.mrecView.destroy();
            this.mrecView = null;
        }
    }
}
