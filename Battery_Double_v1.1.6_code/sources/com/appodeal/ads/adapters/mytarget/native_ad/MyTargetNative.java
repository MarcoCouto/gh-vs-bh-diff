package com.appodeal.ads.adapters.mytarget.native_ad;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.adapters.mytarget.MyTargetNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.NativeAd.NativeAdListener;
import com.my.target.nativeads.NativeAdLoader;
import com.my.target.nativeads.NativeAdLoader.OnLoad;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.nativeads.factories.NativeViewsFactory;
import com.my.target.nativeads.views.ContentStreamAdView;
import com.my.target.nativeads.views.MediaAdView;
import java.util.List;

public class MyTargetNative extends UnifiedNative<RequestParams> {

    private static class MyTargetNativeAdWrapper extends UnifiedNativeAd {
        private final NativeAd nativeAd;
        private final UnifiedNativeParams nativeParams;

        MyTargetNativeAdWrapper(UnifiedNativeParams unifiedNativeParams, NativeAd nativeAd2, String str, String str2, String str3, String str4, String str5, Float f) {
            super(str, str2, str3, str4, str5, f);
            this.nativeParams = unifiedNativeParams;
            this.nativeAd = nativeAd2;
            if (nativeAd2.getBanner() != null) {
                setAgeRestriction(nativeAd2.getBanner().getAgeRestrictions());
            }
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            this.nativeAd.registerView(nativeAdView);
        }

        public void onUnregisterForInteraction() {
            super.onUnregisterForInteraction();
            this.nativeAd.unregisterView();
        }

        public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
            if (this.nativeParams.getNativeAdType() == NativeAdType.NoVideo || !hasVideo()) {
                return super.onConfigureMediaView(nativeMediaView);
            }
            nativeMediaView.removeAllViews();
            ContentStreamAdView contentStreamView = NativeViewsFactory.getContentStreamView(this.nativeAd, nativeMediaView.getContext());
            MediaAdView mediaAdView = contentStreamView.getMediaAdView();
            contentStreamView.removeView(mediaAdView);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(13, -1);
            nativeMediaView.addView(mediaAdView, layoutParams);
            return true;
        }

        public boolean hasVideo() {
            return this.nativeAd.getBanner() != null && this.nativeAd.getBanner().hasVideo();
        }

        public boolean containsVideo() {
            return hasVideo();
        }

        public void onDestroy() {
            if (this.nativeAd != null) {
                this.nativeAd.setListener(null);
            }
            super.onDestroy();
        }

        public int getAdId() {
            return this.nativeAd.hashCode();
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull final UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull final UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        if (unifiedNativeParams.getAdCountToLoad() > 1) {
            NativeAdLoader.newLoader(requestParams.myTargetSlot, unifiedNativeParams.getAdCountToLoad(), activity).setOnLoad(new OnLoad() {
                public void onLoad(@NonNull List<NativeAd> list) {
                    try {
                        for (NativeAd nativeAd : list) {
                            nativeAd.setListener(MyTargetNative.this.createListener(unifiedNativeParams, unifiedNativeCallback));
                            if (nativeAd.getBanner() != null) {
                                unifiedNativeCallback.onAdLoaded(MyTargetNative.this.createNativeAdWrapper(unifiedNativeParams, nativeAd));
                            }
                        }
                    } catch (Exception unused) {
                        unifiedNativeCallback.onAdLoadFailed(LoadingError.InternalError);
                    }
                }
            }).load();
            return;
        }
        NativeAd createPromoAd = createPromoAd(activity, requestParams);
        createPromoAd.setListener(createListener(unifiedNativeParams, unifiedNativeCallback));
        createPromoAd.load();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public NativeAd createPromoAd(Activity activity, RequestParams requestParams) {
        NativeAd nativeAd = new NativeAd(requestParams.myTargetSlot, activity);
        requestParams.applyTargeting(nativeAd.getCustomParams());
        return nativeAd;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public NativeAdListener createListener(final UnifiedNativeParams unifiedNativeParams, final UnifiedNativeCallback unifiedNativeCallback) {
        return new NativeAdListener() {
            public void onShow(@NonNull NativeAd nativeAd) {
            }

            public void onVideoComplete(@NonNull NativeAd nativeAd) {
            }

            public void onVideoPause(@NonNull NativeAd nativeAd) {
            }

            public void onVideoPlay(@NonNull NativeAd nativeAd) {
            }

            public void onLoad(@NonNull NativePromoBanner nativePromoBanner, @NonNull NativeAd nativeAd) {
                try {
                    unifiedNativeCallback.onAdLoaded(MyTargetNative.this.createNativeAdWrapper(unifiedNativeParams, nativeAd));
                } catch (Exception unused) {
                    unifiedNativeCallback.onAdLoadFailed(LoadingError.InternalError);
                }
            }

            public void onNoAd(@NonNull String str, @NonNull NativeAd nativeAd) {
                unifiedNativeCallback.printError(str, null);
                unifiedNativeCallback.onAdLoadFailed(null);
            }

            public void onClick(@NonNull NativeAd nativeAd) {
                unifiedNativeCallback.onAdClicked(nativeAd.hashCode(), (UnifiedAdCallbackClickTrackListener) null);
            }
        };
    }

    /* access modifiers changed from: private */
    public MyTargetNativeAdWrapper createNativeAdWrapper(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull NativeAd nativeAd) {
        Float f;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        NativePromoBanner banner = nativeAd.getBanner();
        Float f2 = null;
        if (banner != null) {
            String url = banner.getIcon() != null ? banner.getIcon().getUrl() : null;
            String url2 = banner.getImage() != null ? banner.getImage().getUrl() : null;
            String title = banner.getTitle();
            String description = banner.getDescription();
            String ctaText = banner.getCtaText();
            if (banner.getRating() != 0.0f) {
                f2 = Float.valueOf(banner.getRating());
            }
            f = f2;
            str = url;
            str2 = url2;
            str5 = title;
            str4 = description;
            str3 = ctaText;
        } else {
            str5 = null;
            str4 = null;
            str3 = null;
            str2 = null;
            str = null;
            f = null;
        }
        MyTargetNativeAdWrapper myTargetNativeAdWrapper = new MyTargetNativeAdWrapper(unifiedNativeParams, nativeAd, str5, str4, str3, str2, str, f);
        return myTargetNativeAdWrapper;
    }
}
