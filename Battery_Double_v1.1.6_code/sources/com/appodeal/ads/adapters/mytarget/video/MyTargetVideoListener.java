package com.appodeal.ads.adapters.mytarget.video;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;

class MyTargetVideoListener implements InterstitialAdListener {
    private final UnifiedVideoCallback callback;

    MyTargetVideoListener(UnifiedVideoCallback unifiedVideoCallback) {
        this.callback = unifiedVideoCallback;
    }

    public void onLoad(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdLoaded();
    }

    public void onNoAd(@NonNull String str, @NonNull InterstitialAd interstitialAd) {
        this.callback.printError(str, null);
        this.callback.onAdLoadFailed(null);
    }

    public void onDisplay(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdShown();
    }

    public void onClick(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdClicked();
    }

    public void onDismiss(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdClosed();
    }

    public void onVideoCompleted(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdFinished();
    }
}
