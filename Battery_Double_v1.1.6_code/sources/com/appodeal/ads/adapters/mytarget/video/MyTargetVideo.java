package com.appodeal.ads.adapters.mytarget.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.mytarget.MyTargetNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.my.target.ads.InterstitialAd;

public class MyTargetVideo extends UnifiedVideo<RequestParams> {
    private InterstitialAd videoAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.videoAd = new InterstitialAd(requestParams.myTargetSlot, activity);
        requestParams.applyTargeting(this.videoAd.getCustomParams());
        this.videoAd.setListener(new MyTargetVideoListener(unifiedVideoCallback));
        this.videoAd.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.videoAd != null) {
            this.videoAd.show();
        } else {
            unifiedVideoCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        if (this.videoAd != null) {
            this.videoAd.destroy();
            this.videoAd = null;
        }
    }
}
