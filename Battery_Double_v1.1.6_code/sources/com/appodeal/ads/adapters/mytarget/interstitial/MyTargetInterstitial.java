package com.appodeal.ads.adapters.mytarget.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.mytarget.MyTargetNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.my.target.ads.InterstitialAd;

public class MyTargetInterstitial extends UnifiedInterstitial<RequestParams> {
    private InterstitialAd interstitialAd;

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.interstitialAd = new InterstitialAd(requestParams.myTargetSlot, activity);
        requestParams.applyTargeting(this.interstitialAd.getCustomParams());
        this.interstitialAd.setListener(new MyTargetInterstitialListener(unifiedInterstitialCallback));
        this.interstitialAd.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialAd != null) {
            this.interstitialAd.show();
        } else {
            unifiedInterstitialCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        if (this.interstitialAd != null) {
            this.interstitialAd.destroy();
            this.interstitialAd = null;
        }
    }
}
