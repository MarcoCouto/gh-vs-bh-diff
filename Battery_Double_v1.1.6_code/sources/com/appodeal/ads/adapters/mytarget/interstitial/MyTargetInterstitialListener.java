package com.appodeal.ads.adapters.mytarget.interstitial;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;

class MyTargetInterstitialListener implements InterstitialAdListener {
    private final UnifiedInterstitialCallback callback;

    MyTargetInterstitialListener(UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.callback = unifiedInterstitialCallback;
    }

    public void onLoad(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdLoaded();
    }

    public void onNoAd(@NonNull String str, @NonNull InterstitialAd interstitialAd) {
        this.callback.printError(str, null);
        this.callback.onAdLoadFailed(null);
    }

    public void onDisplay(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdShown();
    }

    public void onClick(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdClicked();
    }

    public void onDismiss(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdClosed();
    }

    public void onVideoCompleted(@NonNull InterstitialAd interstitialAd) {
        this.callback.onAdFinished();
    }
}
