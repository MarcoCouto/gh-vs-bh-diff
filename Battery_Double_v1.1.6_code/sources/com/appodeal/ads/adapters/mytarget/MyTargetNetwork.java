package com.appodeal.ads.adapters.mytarget;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.AppodealNetworks;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.adapters.mytarget.banner.MyTargetBanner;
import com.appodeal.ads.adapters.mytarget.interstitial.MyTargetInterstitial;
import com.appodeal.ads.adapters.mytarget.mrec.MyTargetMrec;
import com.appodeal.ads.adapters.mytarget.native_ad.MyTargetNative;
import com.appodeal.ads.adapters.mytarget.rewarded_video.MyTargetRewarded;
import com.appodeal.ads.adapters.mytarget.video.MyTargetVideo;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.DependencyRule;
import com.my.target.common.CustomParams;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.common.MyTargetVersion;

public class MyTargetNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        private final String mediatorName;
        public final int myTargetSlot;
        private final RestrictedData restrictedData;

        RequestParams(RestrictedData restrictedData2, String str, int i) {
            this.restrictedData = restrictedData2;
            this.mediatorName = str;
            this.myTargetSlot = i;
        }

        public void applyTargeting(CustomParams customParams) {
            if (!this.restrictedData.isUserAgeRestricted()) {
                Integer age = this.restrictedData.getAge();
                if (age != null) {
                    customParams.setAge(age.intValue());
                }
                Gender gender = this.restrictedData.getGender();
                if (gender != null) {
                    customParams.setGender(MyTargetNetwork.transformGender(gender));
                }
            }
            MyTargetNetwork.setMediatorName(customParams, this.mediatorName);
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "3";
        }

        public String getName() {
            return AppodealNetworks.MY_TARGET;
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.my.target.common.MyTargetActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.my.target.ads.MyTargetView", "com.my.target.ads.InterstitialAd", "com.my.target.nativeads.NativeAd", "com.my.target.common.MyTargetPrivacy"};
        }

        public DependencyRule[] getOptionalClasses() {
            return new DependencyRule[]{new DependencyRule("android.support.v7.widget.RecyclerView"), new DependencyRule("com.google.android.exoplayer2.ExoPlayer", "ExoPlayer not found. Check MyTarget integration"), new DependencyRule("com.google.android.exoplayer2.source.hls.HlsMediaSource", "HLS format not supported. Check MyTarget integration")};
        }

        public MyTargetNetwork build() {
            return new MyTargetNetwork(this);
        }
    }

    public String getVersion() {
        return MyTargetVersion.VERSION;
    }

    public boolean isSupportSmartBanners() {
        return true;
    }

    public MyTargetNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new MyTargetBanner();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new MyTargetMrec();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new MyTargetInterstitial();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new MyTargetVideo();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new MyTargetRewarded();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new MyTargetNative();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        int i = adUnit.getJsonData().getInt("mailru_slot_id");
        updateConsent(adNetworkMediationParams.getRestrictedData());
        networkInitializationListener.onInitializationFinished(new RequestParams(adNetworkMediationParams.getRestrictedData(), adUnit.getMediatorName(), i));
    }

    @VisibleForTesting
    public void updateConsent(@NonNull RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            MyTargetPrivacy.setUserConsent(restrictedData.isUserHasConsent());
        }
        MyTargetPrivacy.setUserAgeRestricted(restrictedData.isUserAgeRestricted());
    }

    /* access modifiers changed from: private */
    public static void setMediatorName(CustomParams customParams, @Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            customParams.setCustomParam("mediation", str);
        }
    }

    /* access modifiers changed from: private */
    public static int transformGender(@NonNull Gender gender) {
        switch (gender) {
            case FEMALE:
                return 2;
            case MALE:
                return 1;
            default:
                return 0;
        }
    }
}
