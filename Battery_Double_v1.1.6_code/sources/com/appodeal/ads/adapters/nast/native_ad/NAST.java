package com.appodeal.ads.adapters.nast.native_ad;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.nast.NASTNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.appodeal.ads.utils.Log;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class NAST extends UnifiedNative<RequestParams> {

    @VisibleForTesting
    static final class NASTNativeAd extends UnifiedNativeAd {
        private static final int CTA_ID = 8;
        private static final int DESCRIPTION_ID = 127;
        private static final int ICON_ID = 124;
        private static final int IMAGE_ID = 128;
        private static final int RATING_ID = 7;
        private static final int TITLE_ID = 123;
        private static final int VIDEO_ID = 4;

        @VisibleForTesting
        NASTNativeAd(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4, @NonNull String str5, @NonNull String str6, @NonNull String str7, @Nullable Float f, @NonNull List<String> list, @NonNull List<String> list2, @NonNull List<String> list3) {
            super(str, str2, str3, str4, str5, f);
            String str8 = str6;
            setClickUrl(str6);
            String str9 = str7;
            setVastVideoTag(str7);
            setImpressionNotifyUrls(list);
            setClickNotifyUrls(list2);
            setFinishNotifyUrls(list3);
        }

        @Nullable
        @VisibleForTesting
        static NASTNativeAd createInstance(@NonNull String str) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String str2 = "";
                String str3 = "";
                String str4 = "";
                String str5 = "";
                String str6 = "";
                String str7 = "";
                JSONObject optJSONObject = jSONObject.optJSONObject("native");
                if (optJSONObject != null) {
                    jSONObject = optJSONObject;
                }
                JSONArray jSONArray = jSONObject.getJSONArray("assets");
                Float f = null;
                String str8 = str2;
                String str9 = str3;
                String str10 = str4;
                String str11 = str5;
                String str12 = str6;
                String str13 = str7;
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject optJSONObject2 = jSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        switch (optJSONObject2.getInt("id")) {
                            case 4:
                                JSONObject optJSONObject3 = optJSONObject2.optJSONObject("video");
                                if (optJSONObject3 == null) {
                                    break;
                                } else {
                                    str13 = optJSONObject3.optString("vasttag");
                                    break;
                                }
                            case 7:
                                double optDouble = optJSONObject2.getJSONObject("data").optDouble("value", Utils.DOUBLE_EPSILON);
                                if (optDouble == Utils.DOUBLE_EPSILON) {
                                    break;
                                } else {
                                    f = Float.valueOf((float) optDouble);
                                    break;
                                }
                            case 8:
                                str10 = optJSONObject2.getJSONObject("data").optString("value", "Learn more");
                                break;
                            case TITLE_ID /*123*/:
                                str8 = optJSONObject2.getJSONObject("title").getString(MimeTypes.BASE_TYPE_TEXT);
                                break;
                            case ICON_ID /*124*/:
                                JSONObject jSONObject2 = optJSONObject2.getJSONObject("img");
                                if (jSONObject2 == null) {
                                    break;
                                } else {
                                    str12 = jSONObject2.getString("url");
                                    break;
                                }
                            case DESCRIPTION_ID /*127*/:
                                str9 = optJSONObject2.getJSONObject("data").optString("value");
                                break;
                            case 128:
                                JSONObject optJSONObject4 = optJSONObject2.optJSONObject("img");
                                if (optJSONObject4 == null) {
                                    break;
                                } else {
                                    str11 = optJSONObject4.optString("url");
                                    break;
                                }
                        }
                    }
                }
                ArrayList arrayList = new ArrayList();
                JSONArray optJSONArray = jSONObject.optJSONArray("imptrackers");
                if (optJSONArray != null) {
                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                        arrayList.add(optJSONArray.getString(i2));
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                JSONObject jSONObject3 = jSONObject.getJSONObject("link");
                String string = jSONObject3.getString("url");
                JSONArray optJSONArray2 = jSONObject3.optJSONArray("clicktrackers");
                if (optJSONArray2 != null) {
                    for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                        arrayList2.add(optJSONArray2.getString(i3));
                    }
                }
                ArrayList arrayList3 = new ArrayList();
                if (jSONObject.has(RequestInfoKeys.EXT)) {
                    JSONArray optJSONArray3 = jSONObject.getJSONObject(RequestInfoKeys.EXT).optJSONArray("finishtrackers");
                    if (optJSONArray3 != null) {
                        for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                            arrayList3.add(optJSONArray3.getString(i4));
                        }
                    }
                }
                NASTNativeAd nASTNativeAd = new NASTNativeAd(str8, str9, str10, str11, str12, string, str13, f, arrayList, arrayList2, arrayList3);
                return nASTNativeAd;
            } catch (Exception e) {
                Log.log(e);
                return null;
            }
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull final UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        S2SAdTask.requestNast(activity, requestParams.url, requestParams.restrictedData, unifiedNativeCallback, new Callback<String>() {
            public void onSuccess(@NonNull Context context, @NonNull String str) {
                NASTNativeAd createInstance = NASTNativeAd.createInstance(str);
                if (createInstance == null) {
                    unifiedNativeCallback.onAdLoadFailed(LoadingError.InternalError);
                } else {
                    unifiedNativeCallback.onAdLoaded(createInstance);
                }
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedNativeCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
