package com.appodeal.ads.adapters.unityads.banner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.unityads.UnityAdUnit;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.UnityBannerSize;

public class UnityadsBanner extends UnifiedBanner<RequestParams> implements UnityAdUnit {
    private BannerView bannerView;
    private Context context;
    private boolean isImpressionTracked = false;

    public String getFallbackPlacement() {
        return "";
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        UnityBannerSize unityBannerSize;
        this.context = activity.getApplicationContext();
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            unityBannerSize = new UnityBannerSize(728, 90);
        } else {
            unityBannerSize = new UnityBannerSize(ModuleDescriptor.MODULE_VERSION, 50);
        }
        this.bannerView = new BannerView(activity, requestParams.placementId, unityBannerSize);
        this.bannerView.setListener(new UnityadsBannerListener(unifiedBannerCallback));
        this.bannerView.load();
    }

    public void onImpression() {
        super.onImpression();
        if (!this.isImpressionTracked && this.context != null) {
            this.isImpressionTracked = true;
            UnityadsNetwork.trackImpressionAdCount(this.context, 1);
        }
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (!this.isImpressionTracked && this.context != null) {
            UnityadsNetwork.trackMissedImpressionOrdinal(this.context, 1);
        }
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView = null;
        }
    }
}
