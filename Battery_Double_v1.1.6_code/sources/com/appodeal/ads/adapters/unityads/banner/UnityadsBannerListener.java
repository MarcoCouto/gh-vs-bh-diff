package com.appodeal.ads.adapters.unityads.banner;

import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.unity3d.services.banners.BannerErrorInfo;
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.BannerView.IListener;
import com.unity3d.services.banners.UnityBannerSize;

class UnityadsBannerListener implements IListener {
    private final UnifiedBannerCallback callback;

    public void onBannerLeftApplication(BannerView bannerView) {
    }

    UnityadsBannerListener(UnifiedBannerCallback unifiedBannerCallback) {
        this.callback = unifiedBannerCallback;
    }

    public void onBannerLoaded(BannerView bannerView) {
        UnityBannerSize size = bannerView.getSize();
        this.callback.onAdLoaded(bannerView, size.getWidth(), size.getHeight());
    }

    public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
        if (bannerErrorInfo != null) {
            this.callback.printError(bannerErrorInfo.errorMessage, bannerErrorInfo.errorCode);
            switch (bannerErrorInfo.errorCode) {
                case UNKNOWN:
                case NATIVE_ERROR:
                case WEBVIEW_ERROR:
                    this.callback.onAdLoadFailed(LoadingError.InternalError);
                    return;
                case NO_FILL:
                    this.callback.onAdLoadFailed(LoadingError.NoFill);
                    return;
                default:
                    return;
            }
        } else {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }
    }

    public void onBannerClick(BannerView bannerView) {
        this.callback.onAdClicked();
    }
}
