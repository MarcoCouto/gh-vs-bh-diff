package com.appodeal.ads.adapters.unityads.video;

import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.unityads.ExternalUnityAdsListener;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.unity3d.ads.UnityAds.FinishState;

class UnityadsVideoListener extends ExternalUnityAdsListener {
    private UnifiedVideoCallback callback;
    @VisibleForTesting
    boolean isLoaded;
    @VisibleForTesting
    boolean isShown;

    UnityadsVideoListener(String str, UnifiedVideoCallback unifiedVideoCallback) {
        super(str);
        this.callback = unifiedVideoCallback;
    }

    public void onLoadSuccess() {
        if (!this.isLoaded) {
            this.isLoaded = true;
            this.callback.onAdLoaded();
        }
    }

    public void onLoadFailed(LoadingError loadingError) {
        if (loadingError != null) {
            this.callback.printError(loadingError.toString(), Integer.valueOf(loadingError.getCode()));
        }
        this.callback.onAdLoadFailed(loadingError);
    }

    public void onShowFailed() {
        if (this.isLoaded) {
            this.callback.onAdShowFailed();
        }
    }

    public void onUnityAdsStart(String str) {
        super.onUnityAdsStart(str);
        if (!this.isShown) {
            this.isShown = true;
            this.callback.onAdShown();
        }
    }

    public void onUnityAdsFinish(String str, FinishState finishState) {
        super.onUnityAdsFinish(str, finishState);
        if (this.isShown) {
            if (finishState == FinishState.COMPLETED) {
                this.callback.onAdFinished();
            }
            this.callback.onAdClosed();
        }
    }

    public void onUnityAdsClick(String str) {
        if (this.isShown) {
            this.callback.onAdClicked();
        }
    }
}
