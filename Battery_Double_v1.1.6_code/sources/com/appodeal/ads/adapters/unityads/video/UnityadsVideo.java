package com.appodeal.ads.adapters.unityads.video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.adapters.unityads.UnityAdUnit;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.unity3d.ads.UnityAds;

public class UnityadsVideo extends UnifiedVideo<RequestParams> implements UnityAdUnit {
    @Nullable
    private RequestParams networkParams;

    public String getFallbackPlacement() {
        return "defaultVideoAndPictureZone";
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.networkParams = requestParams;
        UnityadsNetwork.load(this, new UnityadsVideoListener(requestParams.placementId, unifiedVideoCallback));
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.networkParams == null || !UnityAds.isReady(this.networkParams.placementId)) {
            UnityadsNetwork.trackMissedImpressionOrdinal(activity, 1);
            unifiedVideoCallback.onAdShowFailed();
            return;
        }
        UnityadsNetwork.trackImpressionAdCount(activity, 1);
        UnityAds.show(activity, this.networkParams.placementId);
    }

    public void onDestroy() {
        UnityadsNetwork.unsubscribeListener(this);
    }
}
