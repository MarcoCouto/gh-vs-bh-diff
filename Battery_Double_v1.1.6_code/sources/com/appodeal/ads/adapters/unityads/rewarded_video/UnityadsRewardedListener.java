package com.appodeal.ads.adapters.unityads.rewarded_video;

import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.unityads.ExternalUnityAdsListener;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.unity3d.ads.UnityAds.FinishState;

class UnityadsRewardedListener extends ExternalUnityAdsListener {
    private UnifiedRewardedCallback callback;
    @VisibleForTesting
    boolean isLoaded;
    @VisibleForTesting
    boolean isShown;

    UnityadsRewardedListener(String str, UnifiedRewardedCallback unifiedRewardedCallback) {
        super(str);
        this.callback = unifiedRewardedCallback;
    }

    public void onLoadSuccess() {
        if (!this.isLoaded) {
            this.isLoaded = true;
            this.callback.onAdLoaded();
        }
    }

    public void onLoadFailed(LoadingError loadingError) {
        if (loadingError != null) {
            this.callback.printError(loadingError.toString(), Integer.valueOf(loadingError.getCode()));
        }
        this.callback.onAdLoadFailed(loadingError);
    }

    public void onShowFailed() {
        if (this.isLoaded) {
            this.callback.onAdShowFailed();
        }
    }

    public void onUnityAdsStart(String str) {
        super.onUnityAdsStart(str);
        if (!this.isShown) {
            this.isShown = true;
            this.callback.onAdShown();
        }
    }

    public void onUnityAdsFinish(String str, FinishState finishState) {
        super.onUnityAdsFinish(str, finishState);
        if (this.isShown) {
            if (finishState == FinishState.COMPLETED) {
                this.callback.onAdFinished();
            }
            this.callback.onAdClosed();
        }
    }

    public void onUnityAdsClick(String str) {
        if (this.isShown) {
            this.callback.onAdClicked();
        }
    }
}
