package com.appodeal.ads.adapters.unityads.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.adapters.unityads.UnityAdUnit;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork;
import com.appodeal.ads.adapters.unityads.UnityadsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.unity3d.ads.UnityAds;

public class UnityadsRewarded extends UnifiedRewarded<RequestParams> implements UnityAdUnit {
    @Nullable
    private RequestParams networkParams;

    public String getFallbackPlacement() {
        return "rewardedVideoZone";
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.networkParams = requestParams;
        UnityadsNetwork.load(this, new UnityadsRewardedListener(requestParams.placementId, unifiedRewardedCallback));
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.networkParams == null || !UnityAds.isReady(this.networkParams.placementId)) {
            UnityadsNetwork.trackMissedImpressionOrdinal(activity, 1);
            unifiedRewardedCallback.onAdShowFailed();
            return;
        }
        UnityadsNetwork.trackImpressionAdCount(activity, 1);
        UnityAds.show(activity, this.networkParams.placementId);
    }

    public void onDestroy() {
        UnityadsNetwork.unsubscribeListener(this);
    }
}
