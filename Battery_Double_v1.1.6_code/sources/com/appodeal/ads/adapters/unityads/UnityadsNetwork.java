package com.appodeal.ads.adapters.unityads;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdType;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.AppodealNetworks;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.unityads.banner.UnityadsBanner;
import com.appodeal.ads.adapters.unityads.rewarded_video.UnityadsRewarded;
import com.appodeal.ads.adapters.unityads.video.UnityadsVideo;
import com.appodeal.ads.unified.UnifiedAd;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.appodeal.ads.utils.EventsTracker;
import com.appodeal.ads.utils.EventsTracker.EventType;
import com.appodeal.ads.utils.EventsTracker.EventsListener;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.UnityAds.FinishState;
import com.unity3d.ads.UnityAds.PlacementState;
import com.unity3d.ads.UnityAds.UnityAdsError;
import com.unity3d.ads.mediation.IUnityAdsExtendedListener;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.MetaData;
import com.unity3d.services.core.log.DeviceLog;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UnityadsNetwork extends AdNetwork<RequestParams> {
    @VisibleForTesting
    public static final Map<UnifiedAd, ExternalUnityAdsListener> adsEventListeners = new ConcurrentHashMap();
    @VisibleForTesting
    public static boolean isInitialized = false;

    /* renamed from: com.appodeal.ads.adapters.unityads.UnityadsNetwork$2 reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$appodeal$ads$AdType = new int[AdType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            $SwitchMap$com$appodeal$ads$AdType[AdType.Banner.ordinal()] = 1;
            $SwitchMap$com$appodeal$ads$AdType[AdType.Interstitial.ordinal()] = 2;
            $SwitchMap$com$appodeal$ads$AdType[AdType.Video.ordinal()] = 3;
            try {
                $SwitchMap$com$appodeal$ads$AdType[AdType.Rewarded.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    @VisibleForTesting
    public static final class GlobalUnityAdsListener implements IUnityAdsExtendedListener {
        public void onUnityAdsReady(String str) {
            for (ExternalUnityAdsListener externalUnityAdsListener : UnityadsNetwork.adsEventListeners.values()) {
                if (TextUtils.equals(str, externalUnityAdsListener.placementId)) {
                    externalUnityAdsListener.onUnityAdsReady(str);
                }
            }
        }

        public void onUnityAdsStart(String str) {
            for (ExternalUnityAdsListener externalUnityAdsListener : UnityadsNetwork.adsEventListeners.values()) {
                if (TextUtils.equals(str, externalUnityAdsListener.placementId)) {
                    externalUnityAdsListener.onUnityAdsStart(str);
                }
            }
        }

        public void onUnityAdsFinish(String str, FinishState finishState) {
            for (ExternalUnityAdsListener externalUnityAdsListener : UnityadsNetwork.adsEventListeners.values()) {
                if (TextUtils.equals(str, externalUnityAdsListener.placementId)) {
                    externalUnityAdsListener.onUnityAdsFinish(str, finishState);
                }
            }
        }

        public void onUnityAdsError(UnityAdsError unityAdsError, String str) {
            for (ExternalUnityAdsListener externalUnityAdsListener : UnityadsNetwork.adsEventListeners.values()) {
                if (TextUtils.equals(str, externalUnityAdsListener.placementId)) {
                    externalUnityAdsListener.onUnityAdsError(unityAdsError, str);
                }
            }
        }

        public void onUnityAdsClick(String str) {
            for (ExternalUnityAdsListener externalUnityAdsListener : UnityadsNetwork.adsEventListeners.values()) {
                if (TextUtils.equals(str, externalUnityAdsListener.placementId)) {
                    externalUnityAdsListener.onUnityAdsClick(str);
                }
            }
        }

        public void onUnityAdsPlacementStateChanged(String str, PlacementState placementState, PlacementState placementState2) {
            for (ExternalUnityAdsListener externalUnityAdsListener : UnityadsNetwork.adsEventListeners.values()) {
                if (TextUtils.equals(str, externalUnityAdsListener.placementId)) {
                    externalUnityAdsListener.onUnityAdsPlacementStateChanged(str, placementState, placementState2);
                }
            }
        }
    }

    public static final class RequestParams {
        public final String placementId;

        RequestParams(String str) {
            this.placementId = str;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "2";
        }

        public String getName() {
            return AppodealNetworks.UNITY_ADS;
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.unity3d.services.ads.adunit.AdUnitActivity").build(), new Builder("com.unity3d.services.ads.adunit.AdUnitTransparentActivity").build(), new Builder("com.unity3d.services.ads.adunit.AdUnitTransparentSoftwareActivity").build(), new Builder("com.unity3d.services.ads.adunit.AdUnitSoftwareActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.unity3d.ads.UnityAds"};
        }

        public UnityadsNetwork build() {
            return new UnityadsNetwork(this);
        }
    }

    public boolean canLoadRewardedWhenDisplaying() {
        return false;
    }

    public boolean canLoadVideoWhenDisplaying() {
        return false;
    }

    public UnityadsNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    public void setLogging(boolean z) {
        super.setLogging(z);
        DeviceLog.setLogLevel(z ? 8 : 0);
    }

    public String getVersion() {
        return UnityAds.getVersion();
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new UnityadsBanner();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new UnityadsRewarded();
    }

    @Nullable
    public UnifiedVideo<RequestParams> createVideo() {
        return new UnityadsVideo();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (VERSION.SDK_INT < 16) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        String string = adUnit.getJsonData().getString("app_id");
        String optString = adUnit.getJsonData().optString("zone_id", adUnit instanceof UnityAdUnit ? ((UnityAdUnit) adUnit).getFallbackPlacement() : "");
        if (TextUtils.isEmpty(optString) || TextUtils.getTrimmedLength(optString) == 0) {
            networkInitializationListener.onInitializationFailed(LoadingError.IncorrectAdunit);
            return;
        }
        updateConsent(activity, adNetworkMediationParams.getRestrictedData());
        setMediatorName(activity, adUnit.getMediatorName());
        if (!isInitialized) {
            isInitialized = true;
            UnityAds.initialize(activity, string, (IUnityAdsListener) new GlobalUnityAdsListener(), adNetworkMediationParams.isTestMode());
            trackMissedImpressionOrdinal(activity, 0);
            final Context applicationContext = activity.getApplicationContext();
            EventsTracker.get().subscribeEventsListener(getName(), new EventsListener() {
                public void onImpressionStored(int i, String str) {
                    AdType fromCode = AdType.fromCode(i);
                    if (fromCode != null && !UnityadsNetwork.this.getName().equals(str)) {
                        switch (AnonymousClass2.$SwitchMap$com$appodeal$ads$AdType[fromCode.ordinal()]) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                UnityadsNetwork.trackMissedImpressionOrdinal(applicationContext, 0);
                                return;
                            default:
                                return;
                        }
                    }
                }
            });
        }
        networkInitializationListener.onInitializationFinished(new RequestParams(optString));
    }

    public static void trackMissedImpressionOrdinal(Context context, int i) {
        MediationMetaData mediationMetaData = new MediationMetaData(context);
        mediationMetaData.setMissedImpressionOrdinal(getImpressionAdCount() + i);
        mediationMetaData.commit();
    }

    public static void trackImpressionAdCount(Context context, int i) {
        MediationMetaData mediationMetaData = new MediationMetaData(context);
        mediationMetaData.setOrdinal(getImpressionAdCount() + i);
        mediationMetaData.commit();
    }

    private static int getImpressionAdCount() {
        return EventsTracker.get().getEventCount(EventType.Impression, AdType.Banner, AdType.Interstitial, AdType.Video, AdType.Rewarded);
    }

    public static void load(@NonNull UnifiedAd unifiedAd, @NonNull ExternalUnityAdsListener externalUnityAdsListener) {
        subscribeListener(unifiedAd, externalUnityAdsListener);
        if (UnityAds.isReady(externalUnityAdsListener.placementId)) {
            externalUnityAdsListener.onLoadSuccess();
        }
    }

    public static void subscribeListener(@NonNull UnifiedAd unifiedAd, @NonNull ExternalUnityAdsListener externalUnityAdsListener) {
        adsEventListeners.put(unifiedAd, externalUnityAdsListener);
    }

    public static void unsubscribeListener(@NonNull UnifiedAd unifiedAd) {
        adsEventListeners.remove(unifiedAd);
    }

    @VisibleForTesting
    public void updateConsent(Context context, RestrictedData restrictedData) {
        if (restrictedData.isUserInGdprScope()) {
            MetaData metaData = new MetaData(context.getApplicationContext());
            metaData.set("gdpr.consent", Boolean.valueOf(restrictedData.isUserHasConsent()));
            metaData.commit();
        }
    }

    private void setMediatorName(Context context, @Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            MediationMetaData mediationMetaData = new MediationMetaData(context);
            mediationMetaData.setName(str);
            mediationMetaData.setVersion(Appodeal.getVersion());
            mediationMetaData.commit();
        }
    }
}
