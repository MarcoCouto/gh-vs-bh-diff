package com.appodeal.ads.adapters.mintegral;

public final class R {

    public static final class anim {
        public static final int mintegral_reward_activity_open = 2130771984;
        public static final int mintegral_reward_activity_stay = 2130771985;

        private anim() {
        }
    }

    public static final class color {
        public static final int mintegral_interstitial_black = 2131099782;
        public static final int mintegral_interstitial_white = 2131099783;
        public static final int mintegral_nativex_cta_txt_nor = 2131099784;
        public static final int mintegral_nativex_cta_txt_pre = 2131099785;
        public static final int mintegral_nativex_land_cta_bg_nor = 2131099786;
        public static final int mintegral_nativex_por_cta_bg_nor = 2131099787;
        public static final int mintegral_nativex_por_cta_bg_pre = 2131099788;
        public static final int mintegral_nativex_sound_bg = 2131099789;
        public static final int mintegral_reward_black = 2131099790;
        public static final int mintegral_reward_cta_bg = 2131099791;
        public static final int mintegral_reward_desc_textcolor = 2131099792;
        public static final int mintegral_reward_endcard_hor_bg = 2131099793;
        public static final int mintegral_reward_endcard_land_bg = 2131099794;
        public static final int mintegral_reward_endcard_line_bg = 2131099795;
        public static final int mintegral_reward_endcard_vast_bg = 2131099796;
        public static final int mintegral_reward_kiloo_background = 2131099797;
        public static final int mintegral_reward_minicard_bg = 2131099798;
        public static final int mintegral_reward_six_black_transparent = 2131099799;
        public static final int mintegral_reward_title_textcolor = 2131099800;
        public static final int mintegral_reward_white = 2131099801;
        public static final int mintegral_video_common_alertview_bg = 2131099802;
        public static final int mintegral_video_common_alertview_cancel_button_bg_default = 2131099803;
        public static final int mintegral_video_common_alertview_cancel_button_bg_pressed = 2131099804;
        public static final int mintegral_video_common_alertview_cancel_button_textcolor = 2131099805;
        public static final int mintegral_video_common_alertview_confirm_button_bg_default = 2131099806;
        public static final int mintegral_video_common_alertview_confirm_button_bg_pressed = 2131099807;
        public static final int mintegral_video_common_alertview_confirm_button_textcolor = 2131099808;
        public static final int mintegral_video_common_alertview_content_textcolor = 2131099809;
        public static final int mintegral_video_common_alertview_title_textcolor = 2131099810;

        private color() {
        }
    }

    public static final class dimen {
        public static final int mintegral_video_common_alertview_bg_padding = 2131165357;
        public static final int mintegral_video_common_alertview_button_height = 2131165358;
        public static final int mintegral_video_common_alertview_button_margintop = 2131165359;
        public static final int mintegral_video_common_alertview_button_radius = 2131165360;
        public static final int mintegral_video_common_alertview_button_textsize = 2131165361;
        public static final int mintegral_video_common_alertview_button_width = 2131165362;
        public static final int mintegral_video_common_alertview_content_margintop = 2131165363;
        public static final int mintegral_video_common_alertview_content_size = 2131165364;
        public static final int mintegral_video_common_alertview_contentview_maxwidth = 2131165365;
        public static final int mintegral_video_common_alertview_contentview_minwidth = 2131165366;
        public static final int mintegral_video_common_alertview_title_size = 2131165367;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int mintegral_cm_backward = 2131231057;
        public static final int mintegral_cm_backward_disabled = 2131231058;
        public static final int mintegral_cm_backward_nor = 2131231059;
        public static final int mintegral_cm_backward_selected = 2131231060;
        public static final int mintegral_cm_end_animation = 2131231061;
        public static final int mintegral_cm_exits = 2131231062;
        public static final int mintegral_cm_exits_nor = 2131231063;
        public static final int mintegral_cm_exits_selected = 2131231064;
        public static final int mintegral_cm_forward = 2131231065;
        public static final int mintegral_cm_forward_disabled = 2131231066;
        public static final int mintegral_cm_forward_nor = 2131231067;
        public static final int mintegral_cm_forward_selected = 2131231068;
        public static final int mintegral_cm_head = 2131231069;
        public static final int mintegral_cm_highlight = 2131231070;
        public static final int mintegral_cm_progress = 2131231071;
        public static final int mintegral_cm_refresh = 2131231072;
        public static final int mintegral_cm_refresh_nor = 2131231073;
        public static final int mintegral_cm_refresh_selected = 2131231074;
        public static final int mintegral_cm_tail = 2131231075;
        public static final int mintegral_demo_star_nor = 2131231076;
        public static final int mintegral_demo_star_sel = 2131231077;
        public static final int mintegral_interstitial_close = 2131231078;
        public static final int mintegral_interstitial_over = 2131231079;
        public static final int mintegral_native_bg_loading_camera = 2131231080;
        public static final int mintegral_nativex_close = 2131231081;
        public static final int mintegral_nativex_cta_land_nor = 2131231082;
        public static final int mintegral_nativex_cta_land_pre = 2131231083;
        public static final int mintegral_nativex_cta_por_nor = 2131231084;
        public static final int mintegral_nativex_cta_por_pre = 2131231085;
        public static final int mintegral_nativex_full_land_close = 2131231086;
        public static final int mintegral_nativex_full_protial_close = 2131231087;
        public static final int mintegral_nativex_fullview_background = 2131231088;
        public static final int mintegral_nativex_pause = 2131231089;
        public static final int mintegral_nativex_play = 2131231090;
        public static final int mintegral_nativex_play_bg = 2131231091;
        public static final int mintegral_nativex_play_progress = 2131231092;
        public static final int mintegral_nativex_sound1 = 2131231093;
        public static final int mintegral_nativex_sound2 = 2131231094;
        public static final int mintegral_nativex_sound3 = 2131231095;
        public static final int mintegral_nativex_sound4 = 2131231096;
        public static final int mintegral_nativex_sound5 = 2131231097;
        public static final int mintegral_nativex_sound6 = 2131231098;
        public static final int mintegral_nativex_sound7 = 2131231099;
        public static final int mintegral_nativex_sound8 = 2131231100;
        public static final int mintegral_nativex_sound_animation = 2131231101;
        public static final int mintegral_nativex_sound_bg = 2131231102;
        public static final int mintegral_nativex_sound_close = 2131231103;
        public static final int mintegral_nativex_sound_open = 2131231104;
        public static final int mintegral_reward_activity_ad_end_land_des_rl_hot = 2131231105;
        public static final int mintegral_reward_close = 2131231106;
        public static final int mintegral_reward_end_close_shape_oval = 2131231107;
        public static final int mintegral_reward_end_land_shape = 2131231108;
        public static final int mintegral_reward_end_pager_logo = 2131231109;
        public static final int mintegral_reward_end_shape_oval = 2131231110;
        public static final int mintegral_reward_shape_end_pager = 2131231111;
        public static final int mintegral_reward_shape_progress = 2131231112;
        public static final int mintegral_reward_sound_close = 2131231113;
        public static final int mintegral_reward_sound_open = 2131231114;
        public static final int mintegral_reward_vast_end_close = 2131231115;
        public static final int mintegral_reward_vast_end_ok = 2131231116;
        public static final int mintegral_video_common_alertview_bg = 2131231117;
        public static final int mintegral_video_common_alertview_cancel_bg = 2131231118;
        public static final int mintegral_video_common_alertview_cancel_bg_nor = 2131231119;
        public static final int mintegral_video_common_alertview_cancel_bg_pressed = 2131231120;
        public static final int mintegral_video_common_alertview_confirm_bg = 2131231121;
        public static final int mintegral_video_common_alertview_confirm_bg_nor = 2131231122;
        public static final int mintegral_video_common_alertview_confirm_bg_pressed = 2131231123;
        public static final int mintegral_video_common_full_star = 2131231124;
        public static final int mintegral_video_common_full_while_star = 2131231125;
        public static final int mintegral_video_common_half_star = 2131231126;

        private drawable() {
        }
    }

    public static final class id {
        public static final int linescroll = 2131296447;
        public static final int mintegral_fb_mediaview_layout = 2131296472;
        public static final int mintegral_full_animation_content = 2131296473;
        public static final int mintegral_full_animation_player = 2131296474;
        public static final int mintegral_full_iv_close = 2131296475;
        public static final int mintegral_full_pb_loading = 2131296476;
        public static final int mintegral_full_player_parent = 2131296477;
        public static final int mintegral_full_rl_close = 2131296478;
        public static final int mintegral_full_rl_playcontainer = 2131296479;
        public static final int mintegral_full_tv_display_content = 2131296480;
        public static final int mintegral_full_tv_display_description = 2131296481;
        public static final int mintegral_full_tv_display_icon = 2131296482;
        public static final int mintegral_full_tv_display_title = 2131296483;
        public static final int mintegral_full_tv_feeds_star = 2131296484;
        public static final int mintegral_full_tv_install = 2131296485;
        public static final int mintegral_interstitial_iv_close = 2131296486;
        public static final int mintegral_interstitial_pb = 2131296487;
        public static final int mintegral_interstitial_wv = 2131296488;
        public static final int mintegral_iv_adbanner = 2131296489;
        public static final int mintegral_iv_appicon = 2131296490;
        public static final int mintegral_iv_close = 2131296491;
        public static final int mintegral_iv_hottag = 2131296492;
        public static final int mintegral_iv_icon = 2131296493;
        public static final int mintegral_iv_iconbg = 2131296494;
        public static final int mintegral_iv_pause = 2131296495;
        public static final int mintegral_iv_play = 2131296496;
        public static final int mintegral_iv_playend_pic = 2131296497;
        public static final int mintegral_iv_sound = 2131296498;
        public static final int mintegral_iv_sound_animation = 2131296499;
        public static final int mintegral_iv_vastclose = 2131296500;
        public static final int mintegral_iv_vastok = 2131296501;
        public static final int mintegral_jscommon_checkBox = 2131296502;
        public static final int mintegral_jscommon_okbutton = 2131296503;
        public static final int mintegral_jscommon_webcontent = 2131296504;
        public static final int mintegral_ll_bottomlayout = 2131296505;
        public static final int mintegral_ll_loading = 2131296506;
        public static final int mintegral_ll_playerview_container = 2131296507;
        public static final int mintegral_my_big_img = 2131296508;
        public static final int mintegral_native_pb = 2131296509;
        public static final int mintegral_native_rl_root = 2131296510;
        public static final int mintegral_nativex_webview_layout = 2131296511;
        public static final int mintegral_nativex_webview_layout_webview = 2131296512;
        public static final int mintegral_playercommon_ll_loading = 2131296513;
        public static final int mintegral_playercommon_ll_sur_container = 2131296514;
        public static final int mintegral_playercommon_rl_root = 2131296515;
        public static final int mintegral_progress = 2131296516;
        public static final int mintegral_rl_bodycontainer = 2131296517;
        public static final int mintegral_rl_bottomcontainer = 2131296518;
        public static final int mintegral_rl_content = 2131296519;
        public static final int mintegral_rl_mediaview_root = 2131296520;
        public static final int mintegral_rl_playing_close = 2131296521;
        public static final int mintegral_rl_topcontainer = 2131296522;
        public static final int mintegral_sound_switch = 2131296523;
        public static final int mintegral_sv_starlevel = 2131296524;
        public static final int mintegral_textureview = 2131296525;
        public static final int mintegral_tv_adtag = 2131296526;
        public static final int mintegral_tv_appdesc = 2131296527;
        public static final int mintegral_tv_apptitle = 2131296528;
        public static final int mintegral_tv_cta = 2131296529;
        public static final int mintegral_tv_desc = 2131296530;
        public static final int mintegral_tv_install = 2131296531;
        public static final int mintegral_tv_sound = 2131296532;
        public static final int mintegral_tv_vasttag = 2131296533;
        public static final int mintegral_tv_vasttitle = 2131296534;
        public static final int mintegral_vfpv = 2131296535;
        public static final int mintegral_video_common_alertview_cancel_button = 2131296536;
        public static final int mintegral_video_common_alertview_confirm_button = 2131296537;
        public static final int mintegral_video_common_alertview_contentview = 2131296538;
        public static final int mintegral_video_common_alertview_titleview = 2131296539;
        public static final int mintegral_video_templete_container = 2131296540;
        public static final int mintegral_video_templete_progressbar = 2131296541;
        public static final int mintegral_video_templete_videoview = 2131296542;
        public static final int mintegral_video_templete_webview_parent = 2131296543;
        public static final int mintegral_view_bottomline = 2131296544;
        public static final int mintegral_view_shadow = 2131296545;
        public static final int mintegral_viewgroup_ctaroot = 2131296546;
        public static final int mintegral_windwv_close = 2131296547;
        public static final int mintegral_windwv_content = 2131296548;
        public static final int mobivsta_view_cover = 2131296552;
        public static final int progressBar = 2131296587;
        public static final int progressBar1 = 2131296588;
        public static final int textView = 2131296678;

        private id() {
        }
    }

    public static final class layout {
        public static final int loading_alert = 2131427399;
        public static final int mintegral_interstitial_activity = 2131427410;
        public static final int mintegral_jscommon_authoritylayout = 2131427411;
        public static final int mintegral_nativex_fullbasescreen = 2131427412;
        public static final int mintegral_nativex_fullscreen_top = 2131427413;
        public static final int mintegral_nativex_mtgmediaview = 2131427414;
        public static final int mintegral_nativex_playerview = 2131427415;
        public static final int mintegral_playercommon_player_view = 2131427416;
        public static final int mintegral_reward_activity_video_templete = 2131427417;
        public static final int mintegral_reward_activity_video_templete_transparent = 2131427418;
        public static final int mintegral_reward_clickable_cta = 2131427419;
        public static final int mintegral_reward_endcard_h5 = 2131427420;
        public static final int mintegral_reward_endcard_native_hor = 2131427421;
        public static final int mintegral_reward_endcard_native_land = 2131427422;
        public static final int mintegral_reward_endcard_vast = 2131427423;
        public static final int mintegral_reward_videoview_item = 2131427424;
        public static final int mintegral_video_common_alertview = 2131427425;

        private layout() {
        }
    }

    public static final class string {
        public static final int mintegral_reward_appdesc = 2131624116;
        public static final int mintegral_reward_apptitle = 2131624117;
        public static final int mintegral_reward_clickable_cta_btntext = 2131624118;
        public static final int mintegral_reward_endcard_ad = 2131624119;
        public static final int mintegral_reward_endcard_vast_notice = 2131624120;
        public static final int mintegral_reward_install = 2131624121;

        private string() {
        }
    }

    public static final class style {
        public static final int mintegral_reward_theme = 2131689896;
        public static final int mintegral_transparent_theme = 2131689897;

        private style() {
        }
    }

    public static final class xml {
        public static final int mtg_provider_paths = 2131820544;

        private xml() {
        }
    }

    private R() {
    }
}
