package com.appodeal.ads.adapters.appodeal;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.adapters.appodeal.native_ad.AppodealNative;
import com.appodeal.ads.unified.UnifiedNative;
import org.json.JSONObject;

public class AppodealNativeNetwork extends AdNetwork<RequestParams> {

    public static final class RequestParams {
        public final JSONObject ad;
        public final Long expiryTime;
        public final String packageName;

        RequestParams(JSONObject jSONObject, String str, Long l) {
            this.ad = jSONObject;
            this.packageName = str;
            this.expiryTime = l;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "appodeal";
        }

        public AdNetwork build() {
            return new AppodealNativeNetwork(this);
        }
    }

    public String getVersion() {
        return "1.0.0";
    }

    public AppodealNativeNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new AppodealNative();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        networkInitializationListener.onInitializationFinished(new RequestParams(adUnit.getJsonData().getJSONObject("ad"), adUnit.getJsonData().optString("package"), Long.valueOf(adUnit.getJsonData().optLong("expiry"))));
    }
}
