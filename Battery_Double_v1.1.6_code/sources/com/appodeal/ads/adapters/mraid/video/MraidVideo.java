package com.appodeal.ads.adapters.mraid.video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mraid.MraidNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidVideo;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;

public class MraidVideo extends UnifiedMraidVideo<RequestParams> {
    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        return requestParams;
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedVideoParams unifiedVideoParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedVideoCallback unifiedVideoCallback, @NonNull String str) {
        S2SAdTask.requestMraid(context, str, unifiedMraidNetworkParams, unifiedVideoCallback, new Callback<UnifiedMraidNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
                MraidVideo.this.loadMraid(context, unifiedVideoParams, unifiedMraidNetworkParams, unifiedVideoCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedVideoCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
