package com.appodeal.ads.adapters.mraid.mrec;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mraid.MraidNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidMrec;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;

public class MraidMrec extends UnifiedMraidMrec<RequestParams> {
    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) {
        return requestParams;
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedMrecParams unifiedMrecParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedMrecCallback unifiedMrecCallback, @NonNull String str) {
        S2SAdTask.requestMraid(context, str, unifiedMraidNetworkParams, unifiedMrecCallback, new Callback<UnifiedMraidNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
                MraidMrec.this.loadMraid(context, unifiedMrecParams, unifiedMraidNetworkParams, unifiedMrecCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedMrecCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
