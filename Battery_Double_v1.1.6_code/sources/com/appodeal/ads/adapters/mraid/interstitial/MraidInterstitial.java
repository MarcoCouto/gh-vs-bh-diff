package com.appodeal.ads.adapters.mraid.interstitial;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mraid.MraidNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidInterstitial;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;

public class MraidInterstitial extends UnifiedMraidInterstitial<RequestParams> {
    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        return requestParams;
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedInterstitialParams unifiedInterstitialParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedInterstitialCallback unifiedInterstitialCallback, @NonNull String str) {
        S2SAdTask.requestMraid(context, str, unifiedMraidNetworkParams, unifiedInterstitialCallback, new Callback<UnifiedMraidNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
                MraidInterstitial.this.loadMraid(context, unifiedInterstitialParams, unifiedMraidNetworkParams, unifiedInterstitialCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedInterstitialCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
