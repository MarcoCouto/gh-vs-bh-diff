package com.appodeal.ads.adapters.mraid.rewarded_video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mraid.MraidNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.unified.mraid.UnifiedMraidRewarded;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;

public class MraidRewarded extends UnifiedMraidRewarded<RequestParams> {
    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        return new Builder((UnifiedMraidNetworkParams) requestParams).setCloseTime(requestParams.jsonData.optInt("close_time", 30)).build();
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedRewardedParams unifiedRewardedParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedRewardedCallback unifiedRewardedCallback, @NonNull String str) {
        S2SAdTask.requestMraid(context, str, unifiedMraidNetworkParams, unifiedRewardedCallback, new Callback<UnifiedMraidNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
                MraidRewarded.this.loadMraid(context, unifiedRewardedParams, unifiedMraidNetworkParams, unifiedRewardedCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedRewardedCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
