package com.appodeal.ads.adapters.mraid.banner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.mraid.MraidNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidBanner;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;

public class MraidBanner extends UnifiedMraidBanner<RequestParams> {
    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
        return requestParams;
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedBannerParams unifiedBannerParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedBannerCallback unifiedBannerCallback, @NonNull String str) {
        S2SAdTask.requestMraid(context, str, unifiedMraidNetworkParams, unifiedBannerCallback, new Callback<UnifiedMraidNetworkParams>() {
            public void onSuccess(@NonNull Context context, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
                MraidBanner.this.loadMraid(context, unifiedBannerParams, unifiedMraidNetworkParams, unifiedBannerCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedBannerCallback.onAdLoadFailed(loadingError);
            }
        });
    }
}
