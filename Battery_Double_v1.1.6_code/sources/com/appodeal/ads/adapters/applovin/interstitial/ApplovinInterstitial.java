package com.appodeal.ads.adapters.applovin.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.applovin.ApplovinNetwork.RequestParams;
import com.appodeal.ads.adapters.applovin.ApplovinUnifiedListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;

public class ApplovinInterstitial extends UnifiedInterstitial<RequestParams> {
    /* access modifiers changed from: private */
    public AppLovinAd appLovinAd;
    private AppLovinSdk appLovinSdk;
    private ApplovinInterstitialListener listener;

    @VisibleForTesting
    static final class ApplovinInterstitialListener extends ApplovinUnifiedListener<UnifiedInterstitialCallback> implements AppLovinAdDisplayListener {
        private final ApplovinInterstitial adObject;
        private final boolean checkVideo;

        ApplovinInterstitialListener(UnifiedInterstitialCallback unifiedInterstitialCallback, ApplovinInterstitial applovinInterstitial, boolean z) {
            super(unifiedInterstitialCallback);
            this.adObject = applovinInterstitial;
            this.checkVideo = z;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            if (!this.checkVideo || !appLovinAd.isVideoAd()) {
                this.adObject.appLovinAd = appLovinAd;
                ((UnifiedInterstitialCallback) this.callback).onAdLoaded();
                return;
            }
            ((UnifiedInterstitialCallback) this.callback).onAdLoadFailed(LoadingError.IncorrectCreative);
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            ((UnifiedInterstitialCallback) this.callback).onAdShown();
        }

        public void adHidden(AppLovinAd appLovinAd) {
            ((UnifiedInterstitialCallback) this.callback).onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        boolean optBoolean = requestParams.jsonData.optBoolean("check_video");
        this.appLovinSdk = requestParams.sdk;
        this.listener = new ApplovinInterstitialListener(unifiedInterstitialCallback, this, optBoolean);
        AppLovinAdService adService = this.appLovinSdk.getAdService();
        if (TextUtils.isEmpty(requestParams.zoneId)) {
            adService.loadNextAd(AppLovinAdSize.INTERSTITIAL, this.listener);
        } else {
            adService.loadNextAdForZoneId(requestParams.zoneId, this.listener);
        }
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.appLovinAd != null) {
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.appLovinSdk, activity);
            create.setAdDisplayListener(this.listener);
            create.setAdClickListener(this.listener);
            create.showAndRender(this.appLovinAd);
            return;
        }
        unifiedInterstitialCallback.onAdShowFailed();
    }

    public void onDestroy() {
        this.appLovinAd = null;
        this.listener = null;
    }
}
