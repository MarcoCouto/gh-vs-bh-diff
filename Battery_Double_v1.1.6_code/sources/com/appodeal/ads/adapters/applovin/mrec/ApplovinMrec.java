package com.appodeal.ads.adapters.applovin.mrec;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.widget.FrameLayout.LayoutParams;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.appodeal.ads.adapters.applovin.ApplovinNetwork.RequestParams;
import com.appodeal.ads.adapters.applovin.ApplovinUnifiedListener;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;

public class ApplovinMrec extends UnifiedMrec<RequestParams> {
    private AppLovinAdView mrecView;

    @VisibleForTesting
    static final class ApplovinMrecListener extends ApplovinUnifiedListener<UnifiedMrecCallback> {
        private final AppLovinAdView mrecView;

        ApplovinMrecListener(UnifiedMrecCallback unifiedMrecCallback, AppLovinAdView appLovinAdView) {
            super(unifiedMrecCallback);
            this.mrecView = appLovinAdView;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            ((UnifiedMrecCallback) this.callback).onAdLoaded(this.mrecView);
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        LayoutParams layoutParams = new LayoutParams(Math.round(((float) AppLovinAdSize.MREC.getWidth()) * UnifiedAdUtils.getScreenDensity(activity)), Math.round(((float) AppLovinAdSize.MREC.getHeight()) * UnifiedAdUtils.getScreenDensity(activity)), 1);
        this.mrecView = new AppLovinAdView(requestParams.sdk, AppLovinAdSize.MREC, requestParams.zoneId, activity);
        ApplovinMrecListener applovinMrecListener = new ApplovinMrecListener(unifiedMrecCallback, this.mrecView);
        this.mrecView.setLayoutParams(layoutParams);
        this.mrecView.setAdLoadListener(applovinMrecListener);
        this.mrecView.setAdClickListener(applovinMrecListener);
        this.mrecView.setAutoDestroy(false);
        this.mrecView.loadNextAd();
    }

    public void onDestroy() {
        if (this.mrecView != null) {
            this.mrecView.destroy();
            this.mrecView = null;
        }
    }
}
