package com.appodeal.ads.adapters.applovin.banner;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.appodeal.ads.adapters.applovin.ApplovinNetwork.RequestParams;
import com.appodeal.ads.adapters.applovin.ApplovinUnifiedListener;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;

public class ApplovinBanner extends UnifiedBanner<RequestParams> {
    private AppLovinAdView bannerView;

    @VisibleForTesting
    static final class ApplovinBannerListener extends ApplovinUnifiedListener<UnifiedBannerCallback> {
        private final AppLovinAdView bannerView;

        ApplovinBannerListener(UnifiedBannerCallback unifiedBannerCallback, AppLovinAdView appLovinAdView) {
            super(unifiedBannerCallback);
            this.bannerView = appLovinAdView;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            ((UnifiedBannerCallback) this.callback).onAdLoaded(this.bannerView, appLovinAd.getSize().getWidth(), appLovinAd.getSize().getHeight());
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        AppLovinAdSize appLovinAdSize;
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            appLovinAdSize = AppLovinAdSize.LEADER;
        } else {
            appLovinAdSize = AppLovinAdSize.BANNER;
        }
        this.bannerView = new AppLovinAdView(requestParams.sdk, appLovinAdSize, requestParams.zoneId, activity);
        ApplovinBannerListener applovinBannerListener = new ApplovinBannerListener(unifiedBannerCallback, this.bannerView);
        this.bannerView.setAdLoadListener(applovinBannerListener);
        this.bannerView.setAdClickListener(applovinBannerListener);
        this.bannerView.setAutoDestroy(false);
        this.bannerView.loadNextAd();
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView = null;
        }
    }
}
