package com.appodeal.ads.adapters.applovin;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.net.UrlQuerySanitizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.applovin.native_ad.ApplovinNative.ApplovinNativeAd;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.unified.tasks.AdParamsProcessorCallback;
import com.appodeal.ads.unified.tasks.AdParamsResolver;
import com.appodeal.ads.unified.tasks.AdParamsResolverCallback;
import com.appodeal.ads.unified.tasks.AdResponseProcessor;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class ApplovinTask extends S2SAdTask<JSONArray, List<ApplovinNativeAd>> {
    @NonNull
    private UnifiedNativeParams nativeParams;

    private static final class ApplovinAdParamsResolver implements AdParamsResolver<JSONArray, List<ApplovinNativeAd>> {
        private final Context context;

        private ApplovinAdParamsResolver(Context context2) {
            this.context = context2;
        }

        public void processResponse(@NonNull JSONArray jSONArray, @NonNull AdParamsResolverCallback<List<ApplovinNativeAd>> adParamsResolverCallback) throws Exception {
            String str;
            String str2;
            ArrayList arrayList = new ArrayList();
            if (jSONArray.length() > 0) {
                char c = 0;
                int i = 0;
                while (i < jSONArray.length()) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString(TapjoyConstants.TJC_CLICK_URL);
                    if (!TextUtils.isEmpty(string)) {
                        String value = new UrlQuerySanitizer(string).getValue("package_name");
                        if (!TextUtils.isEmpty(value)) {
                            if (UnifiedAdUtils.isPackageInstalled(this.context, "com.android.vending")) {
                                Object[] objArr = new Object[1];
                                objArr[c] = value;
                                str = string;
                                str2 = String.format("market://details?id=%s", objArr);
                                int i2 = i;
                                ApplovinNativeAd applovinNativeAd = new ApplovinNativeAd(jSONObject.getString("title"), jSONObject.optString("description"), jSONObject.getString("cta"), jSONObject.getString(MessengerShareContentUtility.IMAGE_URL), jSONObject.getString("icon_url"), (float) jSONObject.optDouble("star_rating", Utils.DOUBLE_EPSILON), str2, jSONObject.optString(TapjoyConstants.TJC_VIDEO_URL), jSONObject.getString("simp_url"), jSONObject.optString("sclick_url"), jSONObject.optString("completion_url"), str);
                                arrayList.add(applovinNativeAd);
                                i = i2 + 1;
                                c = 0;
                            }
                            str2 = string;
                            str = null;
                            int i22 = i;
                            ApplovinNativeAd applovinNativeAd2 = new ApplovinNativeAd(jSONObject.getString("title"), jSONObject.optString("description"), jSONObject.getString("cta"), jSONObject.getString(MessengerShareContentUtility.IMAGE_URL), jSONObject.getString("icon_url"), (float) jSONObject.optDouble("star_rating", Utils.DOUBLE_EPSILON), str2, jSONObject.optString(TapjoyConstants.TJC_VIDEO_URL), jSONObject.getString("simp_url"), jSONObject.optString("sclick_url"), jSONObject.optString("completion_url"), str);
                            arrayList.add(applovinNativeAd2);
                            i = i22 + 1;
                            c = 0;
                        }
                    }
                    str2 = string;
                    str = null;
                    int i222 = i;
                    ApplovinNativeAd applovinNativeAd22 = new ApplovinNativeAd(jSONObject.getString("title"), jSONObject.optString("description"), jSONObject.getString("cta"), jSONObject.getString(MessengerShareContentUtility.IMAGE_URL), jSONObject.getString("icon_url"), (float) jSONObject.optDouble("star_rating", Utils.DOUBLE_EPSILON), str2, jSONObject.optString(TapjoyConstants.TJC_VIDEO_URL), jSONObject.getString("simp_url"), jSONObject.optString("sclick_url"), jSONObject.optString("completion_url"), str);
                    arrayList.add(applovinNativeAd22);
                    i = i222 + 1;
                    c = 0;
                }
            }
            adParamsResolverCallback.onResolveSuccess(arrayList);
        }
    }

    private static final class ApplovinAdResponseProcessor implements AdResponseProcessor<JSONArray> {
        private ApplovinAdResponseProcessor() {
        }

        public void processResponse(@NonNull URLConnection uRLConnection, @Nullable String str, @NonNull AdParamsProcessorCallback<JSONArray> adParamsProcessorCallback) throws Exception {
            JSONArray jSONArray;
            if (new UrlQuerySanitizer(uRLConnection.getURL().toString()).getValue("format").equals("json")) {
                jSONArray = parseJsonResponse(str);
            } else {
                jSONArray = parseNastResponse(str);
            }
            if (jSONArray == null || jSONArray.length() == 0) {
                adParamsProcessorCallback.onProcessFail(LoadingError.NoFill);
            } else {
                adParamsProcessorCallback.onProcessSuccess(jSONArray);
            }
        }

        private JSONArray parseJsonResponse(String str) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.getInt("status") != 200) {
                    return null;
                }
                JSONArray jSONArray = jSONObject.getJSONArray("native_ads");
                JSONObject jSONObject2 = jSONObject.getJSONObject("native_settings");
                JSONObject jSONObject3 = jSONObject.getJSONObject(DownloadManager.SETTINGS);
                String string = jSONObject2.getString(TapjoyConstants.TJC_CLICK_URL);
                String string2 = jSONObject2.getString("simp_url");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject4 = jSONArray.getJSONObject(i);
                    String string3 = jSONObject4.getString("clcode");
                    String string4 = jSONObject4.getString("event_id");
                    jSONObject4.put(TapjoyConstants.TJC_CLICK_URL, string.replace("{CLCODE}", string3).replace("{EVENT_ID}", string4));
                    jSONObject4.put("simp_url", string2.replace("{CLCODE}", string3).replace("{EVENT_ID}", string4));
                    jSONObject4.put(DownloadManager.SETTINGS, jSONObject3);
                }
                return jSONArray;
            } catch (JSONException e) {
                Log.log(e);
                return null;
            }
        }

        private JSONArray parseNastResponse(String str) {
            try {
                JSONArray jSONArray = new JSONArray();
                DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource inputSource = new InputSource();
                inputSource.setCharacterStream(new StringReader(str));
                NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("/NAST/Ad", newDocumentBuilder.parse(inputSource), XPathConstants.NODESET);
                if (nodeList != null && nodeList.getLength() > 0) {
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        JSONObject jSONObject = new JSONObject();
                        NodeList childNodes = nodeList.item(i).getChildNodes();
                        if (childNodes != null && childNodes.getLength() > 0) {
                            for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                                Node item = childNodes.item(i2);
                                String nodeName = item.getNodeName();
                                if (nodeName.equalsIgnoreCase(LogConstants.EVENT_ASSETS)) {
                                    NodeList childNodes2 = item.getChildNodes();
                                    if (childNodes2 != null && childNodes2.getLength() > 0) {
                                        for (int i3 = 0; i3 < childNodes2.getLength(); i3++) {
                                            Node item2 = childNodes2.item(i3);
                                            if (item2.getNodeName().equalsIgnoreCase("Image")) {
                                                NamedNodeMap attributes = item2.getAttributes();
                                                if (!(attributes == null || attributes.getNamedItem("type") == null)) {
                                                    String nodeValue = attributes.getNamedItem("type").getNodeValue();
                                                    String access$200 = ApplovinTask.getElementValue(item2);
                                                    if (nodeValue.equalsIgnoreCase(ParametersKeys.MAIN)) {
                                                        jSONObject.put(MessengerShareContentUtility.IMAGE_URL, access$200);
                                                    } else if (nodeValue.equalsIgnoreCase(SettingsJsonConstants.APP_ICON_KEY)) {
                                                        jSONObject.put("icon_url", access$200);
                                                    }
                                                }
                                            } else if (item2.getNodeName().equalsIgnoreCase("Text")) {
                                                NamedNodeMap attributes2 = item2.getAttributes();
                                                if (!(attributes2 == null || attributes2.getNamedItem("type") == null)) {
                                                    String nodeValue2 = attributes2.getNamedItem("type").getNodeValue();
                                                    String access$2002 = ApplovinTask.getElementValue(item2);
                                                    if (nodeValue2.equalsIgnoreCase("headline")) {
                                                        jSONObject.put("title", access$2002);
                                                    } else if (nodeValue2.equalsIgnoreCase(ParametersKeys.MAIN)) {
                                                        jSONObject.put("description", access$2002);
                                                    } else if (nodeValue2.equalsIgnoreCase("cta")) {
                                                        jSONObject.put("cta", access$2002);
                                                    } else if (nodeValue2.equalsIgnoreCase("rating")) {
                                                        jSONObject.put("rating", Float.valueOf(access$2002));
                                                    }
                                                }
                                            } else if (item2.getNodeName().equalsIgnoreCase("Video")) {
                                                NamedNodeMap attributes3 = item2.getAttributes();
                                                if (!(attributes3 == null || attributes3.getNamedItem("type") == null)) {
                                                    String nodeValue3 = attributes3.getNamedItem("type").getNodeValue();
                                                    String access$2003 = ApplovinTask.getElementValue(item2);
                                                    if (nodeValue3.equalsIgnoreCase(ParametersKeys.MAIN)) {
                                                        jSONObject.put(TapjoyConstants.TJC_VIDEO_URL, access$2003);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else if (nodeName.equalsIgnoreCase("Actions")) {
                                    NodeList childNodes3 = item.getChildNodes();
                                    if (childNodes3 != null && childNodes3.getLength() > 0) {
                                        for (int i4 = 0; i4 < childNodes3.getLength(); i4++) {
                                            Node item3 = childNodes3.item(i4);
                                            NamedNodeMap attributes4 = item3.getAttributes();
                                            if (!(attributes4 == null || attributes4.getNamedItem("type") == null)) {
                                                String nodeValue4 = attributes4.getNamedItem("type").getNodeValue();
                                                String access$2004 = ApplovinTask.getElementValue(item3);
                                                if (nodeValue4.equalsIgnoreCase(String.CLICK)) {
                                                    jSONObject.put(TapjoyConstants.TJC_CLICK_URL, access$2004);
                                                }
                                            }
                                        }
                                    }
                                } else if (nodeName.equalsIgnoreCase("Trackers")) {
                                    NodeList childNodes4 = item.getChildNodes();
                                    if (childNodes4 != null && childNodes4.getLength() > 0) {
                                        for (int i5 = 0; i5 < childNodes4.getLength(); i5++) {
                                            Node item4 = childNodes4.item(i5);
                                            NamedNodeMap attributes5 = item4.getAttributes();
                                            if (!(attributes5 == null || attributes5.getNamedItem("type") == null)) {
                                                String nodeValue5 = attributes5.getNamedItem("type").getNodeValue();
                                                String access$2005 = ApplovinTask.getElementValue(item4);
                                                if (nodeValue5.equalsIgnoreCase(Events.AD_IMPRESSION)) {
                                                    jSONObject.put("simp_url", access$2005);
                                                } else if (nodeValue5.equalsIgnoreCase(String.CLICK)) {
                                                    jSONObject.put("sclick_url", access$2005);
                                                } else if (nodeValue5.equalsIgnoreCase("completion")) {
                                                    jSONObject.put("completion_url", access$2005);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        jSONArray.put(jSONObject);
                    }
                }
                return jSONArray;
            } catch (Exception e) {
                Log.log(e);
                return null;
            }
        }
    }

    public ApplovinTask(@NonNull Context context, @Nullable String str, @NonNull RestrictedData restrictedData, @NonNull UnifiedNativeParams unifiedNativeParams, @Nullable Callback<List<ApplovinNativeAd>> callback) {
        super(context, str, restrictedData, new ApplovinAdResponseProcessor(), new ApplovinAdParamsResolver(context), callback);
        this.nativeParams = unifiedNativeParams;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public URL obtainRequestUrl(@NonNull String str) throws Exception {
        if (this.nativeParams.getNativeAdType() == NativeAdType.NoVideo) {
            return new URL(str);
        }
        Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter("accept", "video");
        return new URL(buildUpon.build().toString());
    }

    /* access modifiers changed from: private */
    public static String getElementValue(Node node) {
        NodeList childNodes = node.getChildNodes();
        String str = null;
        int i = 0;
        while (i < childNodes.getLength()) {
            str = ((CharacterData) childNodes.item(i)).getData().trim();
            if (str.length() == 0) {
                i++;
            } else {
                Log.log("Utils", "getElementValue", str, LogLevel.verbose);
                return str;
            }
        }
        return str;
    }
}
