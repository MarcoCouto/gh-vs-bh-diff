package com.appodeal.ads.adapters.applovin.native_ad;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.adapters.applovin.ApplovinNetwork.RequestParams;
import com.appodeal.ads.adapters.applovin.ApplovinTask;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.appodeal.ads.utils.Log;
import java.util.ArrayList;
import java.util.List;

public class ApplovinNative extends UnifiedNative<RequestParams> implements Callback<List<ApplovinNativeAd>> {
    @VisibleForTesting
    UnifiedNativeCallback callback;

    public static class ApplovinNativeAd extends UnifiedNativeAd {
        private final String videoCompletionTrackUrl;

        public ApplovinNativeAd(String str, String str2, String str3, String str4, String str5, float f, String str6, String str7, String str8, String str9, @Nullable String str10, @Nullable String str11) {
            super(str, str2, str3, str4, str5, Float.valueOf(f));
            String str12 = str6;
            setClickUrl(str6);
            final String str13 = str8;
            setImpressionNotifyUrls(new ArrayList<String>(1) {
                {
                    add(str13);
                }
            });
            final String str14 = str9;
            final String str15 = str11;
            setClickNotifyUrls(new ArrayList<String>(2) {
                {
                    add(str14);
                    if (!TextUtils.isEmpty(str15)) {
                        add(str15);
                    }
                }
            });
            setVideoUrl(str7);
            this.videoCompletionTrackUrl = str10;
        }

        public void onAdVideoFinish() {
            super.onAdVideoFinish();
            if (!TextUtils.isEmpty(this.videoCompletionTrackUrl)) {
                UnifiedAdUtils.sendGetRequest(this.videoCompletionTrackUrl);
            }
        }
    }

    public void onDestroy() {
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        this.callback = unifiedNativeCallback;
        createTask(activity, requestParams.jsonData.getString("url"), unifiedNativeParams, requestParams.restrictedData).start();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ApplovinTask createTask(Context context, String str, UnifiedNativeParams unifiedNativeParams, RestrictedData restrictedData) {
        ApplovinTask applovinTask = new ApplovinTask(context, str, restrictedData, unifiedNativeParams, this);
        return applovinTask;
    }

    public void onSuccess(@NonNull Context context, List<ApplovinNativeAd> list) {
        if (list != null) {
            try {
                if (!list.isEmpty()) {
                    for (ApplovinNativeAd onAdLoaded : list) {
                        this.callback.onAdLoaded(onAdLoaded);
                    }
                    return;
                }
            } catch (Exception e) {
                this.callback.onAdLoadFailed(LoadingError.InternalError);
                Log.log(e);
                return;
            }
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public void onFail(@Nullable LoadingError loadingError) {
        this.callback.onAdLoadFailed(loadingError);
    }
}
