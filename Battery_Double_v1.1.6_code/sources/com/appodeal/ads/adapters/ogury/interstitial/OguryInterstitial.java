package com.appodeal.ads.adapters.ogury.interstitial;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.ogury.OguryNetwork;
import com.appodeal.ads.adapters.ogury.OguryNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import io.presage.common.AdConfig;
import io.presage.interstitial.PresageInterstitial;
import io.presage.interstitial.PresageInterstitialCallback;

public class OguryInterstitial extends UnifiedInterstitial<RequestParams> {
    private PresageInterstitial interstitial;

    @VisibleForTesting
    static final class OguryInterstitialListener implements PresageInterstitialCallback {
        private final UnifiedInterstitialCallback callback;

        public void onAdAvailable() {
        }

        OguryInterstitialListener(UnifiedInterstitialCallback unifiedInterstitialCallback) {
            this.callback = unifiedInterstitialCallback;
        }

        public void onAdNotAvailable() {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdLoaded() {
            this.callback.onAdLoaded();
        }

        public void onAdNotLoaded() {
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdDisplayed() {
            this.callback.onAdShown();
        }

        public void onAdClosed() {
            this.callback.onAdClosed();
        }

        public void onAdError(int i) {
            this.callback.printError(OguryNetwork.mapMessageError(i), Integer.valueOf(i));
            if (i == 4) {
                this.callback.onAdExpired();
            } else {
                this.callback.onAdLoadFailed(OguryNetwork.mapError(i));
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        if (TextUtils.isEmpty(requestParams.adUnitId)) {
            this.interstitial = new PresageInterstitial((Context) activity);
        } else {
            this.interstitial = new PresageInterstitial(activity, new AdConfig(requestParams.adUnitId));
        }
        this.interstitial.setInterstitialCallback(new OguryInterstitialListener(unifiedInterstitialCallback));
        this.interstitial.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitial == null || !this.interstitial.isLoaded()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.interstitial.show();
        }
    }

    public void onDestroy() {
        this.interstitial = null;
    }
}
