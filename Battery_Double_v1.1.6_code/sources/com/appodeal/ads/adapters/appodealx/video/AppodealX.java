package com.appodeal.ads.adapters.appodealx.video;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.appodealx.sdk.InterstitialAd;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.List;
import org.json.JSONObject;

public class AppodealX extends UnifiedVideo<RequestParams> {
    @VisibleForTesting
    FullScreenAd fullScreenAd;

    @VisibleForTesting
    static final class Listener implements FullScreenAdListener {
        private final UnifiedVideoCallback callback;
        private final List<JSONObject> parallelBiddingAds;
        private final UnifiedVideoParams params;

        public void onFullScreenAdCompleted() {
        }

        Listener(@NonNull UnifiedVideoCallback unifiedVideoCallback, @NonNull List<JSONObject> list, @NonNull UnifiedVideoParams unifiedVideoParams) {
            this.callback = unifiedVideoCallback;
            this.parallelBiddingAds = list;
            this.params = unifiedVideoParams;
        }

        public void onFullScreenAdLoaded(FullScreenAdObject fullScreenAdObject) {
            Bundle bundle = new Bundle();
            bundle.putString("demand_source", fullScreenAdObject.getDemandSource());
            bundle.putDouble(RequestInfoKeys.APPODEAL_ECPM, fullScreenAdObject.getEcpm());
            if (AppodealXNetwork.getWinnerAdUnit(fullScreenAdObject.getAdId(), this.parallelBiddingAds) != null) {
                bundle.putString("id", fullScreenAdObject.getAdId());
            }
            this.callback.onAdInfoRequested(bundle);
            this.callback.onAdLoaded();
        }

        public void onFullScreenAdFailedToLoad(@NonNull AdError adError) {
            this.callback.printError(adError.toString(), null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onFullScreenAdExpired() {
            this.callback.onAdExpired();
        }

        public void onFullScreenAdFailedToShow(@NonNull AdError adError) {
            this.callback.onAdShowFailed();
        }

        public void onFullScreenAdShown() {
            this.callback.onAdShown();
        }

        public void onFullScreenAdClicked() {
            this.callback.onAdClicked();
        }

        public void onFullScreenAdClosed(boolean z) {
            if (z) {
                this.callback.onAdFinished();
            }
            this.callback.onAdClosed();
        }

        public int getPlacementId() {
            return Integer.valueOf(this.params.obtainPlacementId()).intValue();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        List prepareAdUnitListForLoad = AppodealXNetwork.prepareAdUnitListForLoad(requestParams.adUnit, requestParams.parallelBiddingAds, 2);
        AppodealXNetwork.initializeAppodealX(activity, requestParams.restrictedData, prepareAdUnitListForLoad);
        this.fullScreenAd = new InterstitialAd();
        this.fullScreenAd.loadAd(activity, requestParams.url, prepareAdUnitListForLoad, Long.valueOf(unifiedVideoParams.obtainSegmentId()).longValue(), new Listener(unifiedVideoCallback, prepareAdUnitListForLoad, unifiedVideoParams));
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.fullScreenAd != null) {
            this.fullScreenAd.show(activity);
        } else {
            unifiedVideoCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        if (this.fullScreenAd != null) {
            this.fullScreenAd.destroy();
            this.fullScreenAd = null;
        }
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (this.fullScreenAd != null && loadingError == LoadingError.TimeoutError) {
            this.fullScreenAd.trackError(1005);
        }
    }
}
