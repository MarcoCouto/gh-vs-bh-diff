package com.appodeal.ads.adapters.appodealx.interstitial;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork;
import com.appodeal.ads.adapters.appodealx.AppodealXNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.appodealx.sdk.InterstitialAd;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.List;
import org.json.JSONObject;

public class AppodealX extends UnifiedInterstitial<RequestParams> {
    @VisibleForTesting
    FullScreenAd fullScreenAd;

    @VisibleForTesting
    static final class Listener implements FullScreenAdListener {
        private final UnifiedInterstitialCallback callback;
        private final List<JSONObject> parallelBiddingAds;
        private final UnifiedInterstitialParams params;

        public void onFullScreenAdCompleted() {
        }

        Listener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback, @NonNull List<JSONObject> list, @NonNull UnifiedInterstitialParams unifiedInterstitialParams) {
            this.callback = unifiedInterstitialCallback;
            this.parallelBiddingAds = list;
            this.params = unifiedInterstitialParams;
        }

        public void onFullScreenAdLoaded(FullScreenAdObject fullScreenAdObject) {
            Bundle bundle = new Bundle();
            bundle.putString("demand_source", fullScreenAdObject.getDemandSource());
            bundle.putDouble(RequestInfoKeys.APPODEAL_ECPM, fullScreenAdObject.getEcpm());
            if (AppodealXNetwork.getWinnerAdUnit(fullScreenAdObject.getAdId(), this.parallelBiddingAds) != null) {
                bundle.putString("id", fullScreenAdObject.getAdId());
            }
            this.callback.onAdInfoRequested(bundle);
            this.callback.onAdLoaded();
        }

        public void onFullScreenAdFailedToLoad(@NonNull AdError adError) {
            this.callback.printError(adError.toString(), null);
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onFullScreenAdExpired() {
            this.callback.onAdExpired();
        }

        public void onFullScreenAdFailedToShow(@NonNull AdError adError) {
            this.callback.onAdShowFailed();
        }

        public void onFullScreenAdShown() {
            this.callback.onAdShown();
        }

        public void onFullScreenAdClicked() {
            this.callback.onAdClicked();
        }

        public void onFullScreenAdClosed(boolean z) {
            this.callback.onAdClosed();
        }

        public int getPlacementId() {
            return Integer.valueOf(this.params.obtainPlacementId()).intValue();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        List prepareAdUnitListForLoad = AppodealXNetwork.prepareAdUnitListForLoad(requestParams.adUnit, requestParams.parallelBiddingAds, 1);
        AppodealXNetwork.initializeAppodealX(activity, requestParams.restrictedData, prepareAdUnitListForLoad);
        this.fullScreenAd = new InterstitialAd();
        this.fullScreenAd.loadAd(activity, requestParams.url, prepareAdUnitListForLoad, Long.valueOf(unifiedInterstitialParams.obtainSegmentId()).longValue(), new Listener(unifiedInterstitialCallback, prepareAdUnitListForLoad, unifiedInterstitialParams));
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.fullScreenAd != null) {
            this.fullScreenAd.show(activity);
        } else {
            unifiedInterstitialCallback.onAdShowFailed();
        }
    }

    public void onDestroy() {
        if (this.fullScreenAd != null) {
            this.fullScreenAd.destroy();
            this.fullScreenAd = null;
        }
    }

    public void onError(LoadingError loadingError) {
        super.onError(loadingError);
        if (this.fullScreenAd != null && loadingError == LoadingError.TimeoutError) {
            this.fullScreenAd.trackError(1005);
        }
    }
}
