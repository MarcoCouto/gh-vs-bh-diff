package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class App extends GeneratedMessageV3 implements AppOrBuilder {
    public static final int APP_KEY_FIELD_NUMBER = 7;
    public static final int APP_UPTIME_FIELD_NUMBER = 10;
    public static final int BUNDLE_FIELD_NUMBER = 1;
    private static final App DEFAULT_INSTANCE = new App();
    public static final int FRAMEWORK_FIELD_NUMBER = 11;
    public static final int FRAMEWORK_VERSION_FIELD_NUMBER = 12;
    public static final int INSTALLER_FIELD_NUMBER = 5;
    public static final int INSTALL_TIME_FIELD_NUMBER = 3;
    public static final int MULTIDEX_FIELD_NUMBER = 6;
    /* access modifiers changed from: private */
    public static final Parser<App> PARSER = new AbstractParser<App>() {
        public App parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new App(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int PLUGIN_VERSION_FIELD_NUMBER = 4;
    public static final int SDK_FIELD_NUMBER = 8;
    public static final int VERSION_CODE_FIELD_NUMBER = 9;
    public static final int VER_FIELD_NUMBER = 2;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public volatile Object appKey_;
    /* access modifiers changed from: private */
    public long appUptime_;
    /* access modifiers changed from: private */
    public volatile Object bundle_;
    /* access modifiers changed from: private */
    public volatile Object frameworkVersion_;
    /* access modifiers changed from: private */
    public volatile Object framework_;
    /* access modifiers changed from: private */
    public long installTime_;
    /* access modifiers changed from: private */
    public volatile Object installer_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public boolean multidex_;
    /* access modifiers changed from: private */
    public volatile Object pluginVersion_;
    /* access modifiers changed from: private */
    public volatile Object sdk_;
    /* access modifiers changed from: private */
    public volatile Object ver_;
    /* access modifiers changed from: private */
    public int versionCode_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements AppOrBuilder {
        private Object appKey_;
        private long appUptime_;
        private Object bundle_;
        private Object frameworkVersion_;
        private Object framework_;
        private long installTime_;
        private Object installer_;
        private boolean multidex_;
        private Object pluginVersion_;
        private Object sdk_;
        private Object ver_;
        private int versionCode_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_App_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_App_fieldAccessorTable.ensureFieldAccessorsInitialized(App.class, Builder.class);
        }

        private Builder() {
            this.bundle_ = "";
            this.ver_ = "";
            this.pluginVersion_ = "";
            this.installer_ = "";
            this.appKey_ = "";
            this.sdk_ = "";
            this.framework_ = "";
            this.frameworkVersion_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.bundle_ = "";
            this.ver_ = "";
            this.pluginVersion_ = "";
            this.installer_ = "";
            this.appKey_ = "";
            this.sdk_ = "";
            this.framework_ = "";
            this.frameworkVersion_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            App.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.bundle_ = "";
            this.ver_ = "";
            this.installTime_ = 0;
            this.pluginVersion_ = "";
            this.installer_ = "";
            this.multidex_ = false;
            this.appKey_ = "";
            this.sdk_ = "";
            this.versionCode_ = 0;
            this.appUptime_ = 0;
            this.framework_ = "";
            this.frameworkVersion_ = "";
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_App_descriptor;
        }

        public App getDefaultInstanceForType() {
            return App.getDefaultInstance();
        }

        public App build() {
            App buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public App buildPartial() {
            App app = new App((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            app.bundle_ = this.bundle_;
            app.ver_ = this.ver_;
            app.installTime_ = this.installTime_;
            app.pluginVersion_ = this.pluginVersion_;
            app.installer_ = this.installer_;
            app.multidex_ = this.multidex_;
            app.appKey_ = this.appKey_;
            app.sdk_ = this.sdk_;
            app.versionCode_ = this.versionCode_;
            app.appUptime_ = this.appUptime_;
            app.framework_ = this.framework_;
            app.frameworkVersion_ = this.frameworkVersion_;
            onBuilt();
            return app;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof App) {
                return mergeFrom((App) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(App app) {
            if (app == App.getDefaultInstance()) {
                return this;
            }
            if (!app.getBundle().isEmpty()) {
                this.bundle_ = app.bundle_;
                onChanged();
            }
            if (!app.getVer().isEmpty()) {
                this.ver_ = app.ver_;
                onChanged();
            }
            if (app.getInstallTime() != 0) {
                setInstallTime(app.getInstallTime());
            }
            if (!app.getPluginVersion().isEmpty()) {
                this.pluginVersion_ = app.pluginVersion_;
                onChanged();
            }
            if (!app.getInstaller().isEmpty()) {
                this.installer_ = app.installer_;
                onChanged();
            }
            if (app.getMultidex()) {
                setMultidex(app.getMultidex());
            }
            if (!app.getAppKey().isEmpty()) {
                this.appKey_ = app.appKey_;
                onChanged();
            }
            if (!app.getSdk().isEmpty()) {
                this.sdk_ = app.sdk_;
                onChanged();
            }
            if (app.getVersionCode() != 0) {
                setVersionCode(app.getVersionCode());
            }
            if (app.getAppUptime() != 0) {
                setAppUptime(app.getAppUptime());
            }
            if (!app.getFramework().isEmpty()) {
                this.framework_ = app.framework_;
                onChanged();
            }
            if (!app.getFrameworkVersion().isEmpty()) {
                this.frameworkVersion_ = app.frameworkVersion_;
                onChanged();
            }
            mergeUnknownFields(app.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            App app;
            App app2 = null;
            try {
                App app3 = (App) App.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (app3 != null) {
                    mergeFrom(app3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                app = (App) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                app2 = app;
                if (app2 != null) {
                }
                throw th;
            }
        }

        public String getBundle() {
            Object obj = this.bundle_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.bundle_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getBundleBytes() {
            Object obj = this.bundle_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.bundle_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setBundle(String str) {
            if (str != null) {
                this.bundle_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearBundle() {
            this.bundle_ = App.getDefaultInstance().getBundle();
            onChanged();
            return this;
        }

        public Builder setBundleBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.bundle_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getVer() {
            Object obj = this.ver_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ver_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getVerBytes() {
            Object obj = this.ver_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.ver_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setVer(String str) {
            if (str != null) {
                this.ver_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearVer() {
            this.ver_ = App.getDefaultInstance().getVer();
            onChanged();
            return this;
        }

        public Builder setVerBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.ver_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public long getInstallTime() {
            return this.installTime_;
        }

        public Builder setInstallTime(long j) {
            this.installTime_ = j;
            onChanged();
            return this;
        }

        public Builder clearInstallTime() {
            this.installTime_ = 0;
            onChanged();
            return this;
        }

        public String getPluginVersion() {
            Object obj = this.pluginVersion_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.pluginVersion_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getPluginVersionBytes() {
            Object obj = this.pluginVersion_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.pluginVersion_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setPluginVersion(String str) {
            if (str != null) {
                this.pluginVersion_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearPluginVersion() {
            this.pluginVersion_ = App.getDefaultInstance().getPluginVersion();
            onChanged();
            return this;
        }

        public Builder setPluginVersionBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.pluginVersion_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getInstaller() {
            Object obj = this.installer_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.installer_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getInstallerBytes() {
            Object obj = this.installer_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.installer_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setInstaller(String str) {
            if (str != null) {
                this.installer_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearInstaller() {
            this.installer_ = App.getDefaultInstance().getInstaller();
            onChanged();
            return this;
        }

        public Builder setInstallerBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.installer_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        @Deprecated
        public boolean getMultidex() {
            return this.multidex_;
        }

        @Deprecated
        public Builder setMultidex(boolean z) {
            this.multidex_ = z;
            onChanged();
            return this;
        }

        @Deprecated
        public Builder clearMultidex() {
            this.multidex_ = false;
            onChanged();
            return this;
        }

        public String getAppKey() {
            Object obj = this.appKey_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.appKey_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getAppKeyBytes() {
            Object obj = this.appKey_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.appKey_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setAppKey(String str) {
            if (str != null) {
                this.appKey_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearAppKey() {
            this.appKey_ = App.getDefaultInstance().getAppKey();
            onChanged();
            return this;
        }

        public Builder setAppKeyBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.appKey_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getSdk() {
            Object obj = this.sdk_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.sdk_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getSdkBytes() {
            Object obj = this.sdk_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.sdk_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setSdk(String str) {
            if (str != null) {
                this.sdk_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearSdk() {
            this.sdk_ = App.getDefaultInstance().getSdk();
            onChanged();
            return this;
        }

        public Builder setSdkBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.sdk_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getVersionCode() {
            return this.versionCode_;
        }

        public Builder setVersionCode(int i) {
            this.versionCode_ = i;
            onChanged();
            return this;
        }

        public Builder clearVersionCode() {
            this.versionCode_ = 0;
            onChanged();
            return this;
        }

        public long getAppUptime() {
            return this.appUptime_;
        }

        public Builder setAppUptime(long j) {
            this.appUptime_ = j;
            onChanged();
            return this;
        }

        public Builder clearAppUptime() {
            this.appUptime_ = 0;
            onChanged();
            return this;
        }

        public String getFramework() {
            Object obj = this.framework_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.framework_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getFrameworkBytes() {
            Object obj = this.framework_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.framework_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setFramework(String str) {
            if (str != null) {
                this.framework_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearFramework() {
            this.framework_ = App.getDefaultInstance().getFramework();
            onChanged();
            return this;
        }

        public Builder setFrameworkBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.framework_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getFrameworkVersion() {
            Object obj = this.frameworkVersion_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.frameworkVersion_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getFrameworkVersionBytes() {
            Object obj = this.frameworkVersion_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.frameworkVersion_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setFrameworkVersion(String str) {
            if (str != null) {
                this.frameworkVersion_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearFrameworkVersion() {
            this.frameworkVersion_ = App.getDefaultInstance().getFrameworkVersion();
            onChanged();
            return this;
        }

        public Builder setFrameworkVersionBytes(ByteString byteString) {
            if (byteString != null) {
                App.checkByteStringIsUtf8(byteString);
                this.frameworkVersion_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private App(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private App() {
        this.memoizedIsInitialized = -1;
        this.bundle_ = "";
        this.ver_ = "";
        this.pluginVersion_ = "";
        this.installer_ = "";
        this.appKey_ = "";
        this.sdk_ = "";
        this.framework_ = "";
        this.frameworkVersion_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new App();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private App(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    switch (readTag) {
                        case 0:
                            z = true;
                            break;
                        case 10:
                            this.bundle_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 18:
                            this.ver_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 24:
                            this.installTime_ = codedInputStream.readInt64();
                            break;
                        case 34:
                            this.pluginVersion_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 42:
                            this.installer_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 48:
                            this.multidex_ = codedInputStream.readBool();
                            break;
                        case 58:
                            this.appKey_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 66:
                            this.sdk_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 72:
                            this.versionCode_ = codedInputStream.readInt32();
                            break;
                        case 80:
                            this.appUptime_ = codedInputStream.readInt64();
                            break;
                        case 90:
                            this.framework_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 98:
                            this.frameworkVersion_ = codedInputStream.readStringRequireUtf8();
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                                break;
                            }
                            z = true;
                            break;
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_App_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_App_fieldAccessorTable.ensureFieldAccessorsInitialized(App.class, Builder.class);
    }

    public String getBundle() {
        Object obj = this.bundle_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.bundle_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getBundleBytes() {
        Object obj = this.bundle_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.bundle_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getVer() {
        Object obj = this.ver_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.ver_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getVerBytes() {
        Object obj = this.ver_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.ver_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public long getInstallTime() {
        return this.installTime_;
    }

    public String getPluginVersion() {
        Object obj = this.pluginVersion_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.pluginVersion_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getPluginVersionBytes() {
        Object obj = this.pluginVersion_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.pluginVersion_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getInstaller() {
        Object obj = this.installer_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.installer_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getInstallerBytes() {
        Object obj = this.installer_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.installer_ = copyFromUtf8;
        return copyFromUtf8;
    }

    @Deprecated
    public boolean getMultidex() {
        return this.multidex_;
    }

    public String getAppKey() {
        Object obj = this.appKey_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.appKey_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getAppKeyBytes() {
        Object obj = this.appKey_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.appKey_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getSdk() {
        Object obj = this.sdk_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.sdk_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getSdkBytes() {
        Object obj = this.sdk_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.sdk_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getVersionCode() {
        return this.versionCode_;
    }

    public long getAppUptime() {
        return this.appUptime_;
    }

    public String getFramework() {
        Object obj = this.framework_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.framework_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getFrameworkBytes() {
        Object obj = this.framework_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.framework_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getFrameworkVersion() {
        Object obj = this.frameworkVersion_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.frameworkVersion_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getFrameworkVersionBytes() {
        Object obj = this.frameworkVersion_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.frameworkVersion_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (!getBundleBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 1, this.bundle_);
        }
        if (!getVerBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 2, this.ver_);
        }
        if (this.installTime_ != 0) {
            codedOutputStream.writeInt64(3, this.installTime_);
        }
        if (!getPluginVersionBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 4, this.pluginVersion_);
        }
        if (!getInstallerBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 5, this.installer_);
        }
        if (this.multidex_) {
            codedOutputStream.writeBool(6, this.multidex_);
        }
        if (!getAppKeyBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 7, this.appKey_);
        }
        if (!getSdkBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 8, this.sdk_);
        }
        if (this.versionCode_ != 0) {
            codedOutputStream.writeInt32(9, this.versionCode_);
        }
        if (this.appUptime_ != 0) {
            codedOutputStream.writeInt64(10, this.appUptime_);
        }
        if (!getFrameworkBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 11, this.framework_);
        }
        if (!getFrameworkVersionBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 12, this.frameworkVersion_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!getBundleBytes().isEmpty()) {
            i2 = 0 + GeneratedMessageV3.computeStringSize(1, this.bundle_);
        }
        if (!getVerBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(2, this.ver_);
        }
        if (this.installTime_ != 0) {
            i2 += CodedOutputStream.computeInt64Size(3, this.installTime_);
        }
        if (!getPluginVersionBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(4, this.pluginVersion_);
        }
        if (!getInstallerBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(5, this.installer_);
        }
        if (this.multidex_) {
            i2 += CodedOutputStream.computeBoolSize(6, this.multidex_);
        }
        if (!getAppKeyBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(7, this.appKey_);
        }
        if (!getSdkBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(8, this.sdk_);
        }
        if (this.versionCode_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(9, this.versionCode_);
        }
        if (this.appUptime_ != 0) {
            i2 += CodedOutputStream.computeInt64Size(10, this.appUptime_);
        }
        if (!getFrameworkBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(11, this.framework_);
        }
        if (!getFrameworkVersionBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(12, this.frameworkVersion_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof App)) {
            return super.equals(obj);
        }
        App app = (App) obj;
        if (getBundle().equals(app.getBundle()) && getVer().equals(app.getVer()) && getInstallTime() == app.getInstallTime() && getPluginVersion().equals(app.getPluginVersion()) && getInstaller().equals(app.getInstaller()) && getMultidex() == app.getMultidex() && getAppKey().equals(app.getAppKey()) && getSdk().equals(app.getSdk()) && getVersionCode() == app.getVersionCode() && getAppUptime() == app.getAppUptime() && getFramework().equals(app.getFramework()) && getFrameworkVersion().equals(app.getFrameworkVersion()) && this.unknownFields.equals(app.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((((((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + getBundle().hashCode()) * 37) + 2) * 53) + getVer().hashCode()) * 37) + 3) * 53) + Internal.hashLong(getInstallTime())) * 37) + 4) * 53) + getPluginVersion().hashCode()) * 37) + 5) * 53) + getInstaller().hashCode()) * 37) + 6) * 53) + Internal.hashBoolean(getMultidex())) * 37) + 7) * 53) + getAppKey().hashCode()) * 37) + 8) * 53) + getSdk().hashCode()) * 37) + 9) * 53) + getVersionCode()) * 37) + 10) * 53) + Internal.hashLong(getAppUptime())) * 37) + 11) * 53) + getFramework().hashCode()) * 37) + 12) * 53) + getFrameworkVersion().hashCode()) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static App parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (App) PARSER.parseFrom(byteBuffer);
    }

    public static App parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (App) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static App parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (App) PARSER.parseFrom(byteString);
    }

    public static App parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (App) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static App parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (App) PARSER.parseFrom(bArr);
    }

    public static App parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (App) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static App parseFrom(InputStream inputStream) throws IOException {
        return (App) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static App parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (App) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static App parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (App) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static App parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (App) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static App parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (App) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static App parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (App) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(App app) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(app);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static App getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<App> parser() {
        return PARSER;
    }

    public Parser<App> getParserForType() {
        return PARSER;
    }

    public App getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
