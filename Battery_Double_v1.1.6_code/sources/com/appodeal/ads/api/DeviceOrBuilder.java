package com.appodeal.ads.api;

import com.appodeal.ads.api.Device.ConnectionType;
import com.appodeal.ads.api.Device.DeviceType;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;

public interface DeviceOrBuilder extends MessageOrBuilder {
    boolean getAdidg();

    int getBattery();

    ConnectionType getConnectiontype();

    int getConnectiontypeValue();

    DeviceType getDevicetype();

    int getDevicetypeValue();

    int getH();

    String getIfa();

    ByteString getIfaBytes();

    int getLmt();

    String getLocale();

    ByteString getLocaleBytes();

    String getMake();

    ByteString getMakeBytes();

    String getMccmnc();

    ByteString getMccmncBytes();

    String getModel();

    ByteString getModelBytes();

    String getOs();

    ByteString getOsBytes();

    String getOsv();

    ByteString getOsvBytes();

    float getPxratio();

    @Deprecated
    boolean getRooted();

    String getUa();

    ByteString getUaBytes();

    int getW();

    @Deprecated
    String getWebviewVersion();

    @Deprecated
    ByteString getWebviewVersionBytes();
}
