package com.appodeal.ads.api;

import com.explorestack.protobuf.MessageOrBuilder;

public interface AdStatsOrBuilder extends MessageOrBuilder {
    int getBanner320Click();

    int getBanner320Show();

    int getBannerClick();

    int getBannerMrecClick();

    int getBannerMrecShow();

    int getBannerShow();

    int getClick();

    int getFinish();

    int getNativeClick();

    int getNativeShow();

    int getRewardedVideoClick();

    int getRewardedVideoFinish();

    int getRewardedVideoShow();

    int getShow();

    int getVideoClick();

    int getVideoFinish();

    int getVideoShow();
}
