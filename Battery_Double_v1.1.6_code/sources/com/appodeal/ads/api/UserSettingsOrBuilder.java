package com.appodeal.ads.api;

import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;

public interface UserSettingsOrBuilder extends MessageOrBuilder {
    int getAge();

    String getGender();

    ByteString getGenderBytes();

    @Deprecated
    String getUserId();

    @Deprecated
    ByteString getUserIdBytes();
}
