package com.appodeal.ads.api;

import com.appodeal.ads.api.Event.EventType;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;

public interface EventOrBuilder extends MessageOrBuilder {
    float getAmount();

    String getCurrency();

    ByteString getCurrencyBytes();

    EventType getEventType();

    int getEventTypeValue();

    String getId();

    ByteString getIdBytes();

    int getPlacementId();
}
