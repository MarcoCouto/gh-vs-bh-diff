package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.EnumDescriptor;
import com.explorestack.protobuf.Descriptors.EnumValueDescriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal.EnumLiteMap;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.ProtocolMessageEnum;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class Event extends GeneratedMessageV3 implements EventOrBuilder {
    public static final int AMOUNT_FIELD_NUMBER = 5;
    public static final int CURRENCY_FIELD_NUMBER = 4;
    private static final Event DEFAULT_INSTANCE = new Event();
    public static final int EVENTTYPE_FIELD_NUMBER = 1;
    public static final int ID_FIELD_NUMBER = 2;
    /* access modifiers changed from: private */
    public static final Parser<Event> PARSER = new AbstractParser<Event>() {
        public Event parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Event(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int PLACEMENT_ID_FIELD_NUMBER = 3;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public float amount_;
    /* access modifiers changed from: private */
    public volatile Object currency_;
    /* access modifiers changed from: private */
    public int eventType_;
    /* access modifiers changed from: private */
    public volatile Object id_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public int placementId_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements EventOrBuilder {
        private float amount_;
        private Object currency_;
        private int eventType_;
        private Object id_;
        private int placementId_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Event_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Event_fieldAccessorTable.ensureFieldAccessorsInitialized(Event.class, Builder.class);
        }

        private Builder() {
            this.eventType_ = 0;
            this.id_ = "";
            this.currency_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.eventType_ = 0;
            this.id_ = "";
            this.currency_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Event.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.eventType_ = 0;
            this.id_ = "";
            this.placementId_ = 0;
            this.currency_ = "";
            this.amount_ = 0.0f;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Event_descriptor;
        }

        public Event getDefaultInstanceForType() {
            return Event.getDefaultInstance();
        }

        public Event build() {
            Event buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Event buildPartial() {
            Event event = new Event((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            event.eventType_ = this.eventType_;
            event.id_ = this.id_;
            event.placementId_ = this.placementId_;
            event.currency_ = this.currency_;
            event.amount_ = this.amount_;
            onBuilt();
            return event;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Event) {
                return mergeFrom((Event) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Event event) {
            if (event == Event.getDefaultInstance()) {
                return this;
            }
            if (event.eventType_ != 0) {
                setEventTypeValue(event.getEventTypeValue());
            }
            if (!event.getId().isEmpty()) {
                this.id_ = event.id_;
                onChanged();
            }
            if (event.getPlacementId() != 0) {
                setPlacementId(event.getPlacementId());
            }
            if (!event.getCurrency().isEmpty()) {
                this.currency_ = event.currency_;
                onChanged();
            }
            if (event.getAmount() != 0.0f) {
                setAmount(event.getAmount());
            }
            mergeUnknownFields(event.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Event event;
            Event event2 = null;
            try {
                Event event3 = (Event) Event.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (event3 != null) {
                    mergeFrom(event3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                event = (Event) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                event2 = event;
                if (event2 != null) {
                }
                throw th;
            }
        }

        public int getEventTypeValue() {
            return this.eventType_;
        }

        public Builder setEventTypeValue(int i) {
            this.eventType_ = i;
            onChanged();
            return this;
        }

        public EventType getEventType() {
            EventType valueOf = EventType.valueOf(this.eventType_);
            return valueOf == null ? EventType.UNRECOGNIZED : valueOf;
        }

        public Builder setEventType(EventType eventType) {
            if (eventType != null) {
                this.eventType_ = eventType.getNumber();
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearEventType() {
            this.eventType_ = 0;
            onChanged();
            return this;
        }

        public String getId() {
            Object obj = this.id_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.id_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getIdBytes() {
            Object obj = this.id_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.id_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setId(String str) {
            if (str != null) {
                this.id_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearId() {
            this.id_ = Event.getDefaultInstance().getId();
            onChanged();
            return this;
        }

        public Builder setIdBytes(ByteString byteString) {
            if (byteString != null) {
                Event.checkByteStringIsUtf8(byteString);
                this.id_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public int getPlacementId() {
            return this.placementId_;
        }

        public Builder setPlacementId(int i) {
            this.placementId_ = i;
            onChanged();
            return this;
        }

        public Builder clearPlacementId() {
            this.placementId_ = 0;
            onChanged();
            return this;
        }

        public String getCurrency() {
            Object obj = this.currency_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.currency_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getCurrencyBytes() {
            Object obj = this.currency_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.currency_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setCurrency(String str) {
            if (str != null) {
                this.currency_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearCurrency() {
            this.currency_ = Event.getDefaultInstance().getCurrency();
            onChanged();
            return this;
        }

        public Builder setCurrencyBytes(ByteString byteString) {
            if (byteString != null) {
                Event.checkByteStringIsUtf8(byteString);
                this.currency_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public float getAmount() {
            return this.amount_;
        }

        public Builder setAmount(float f) {
            this.amount_ = f;
            onChanged();
            return this;
        }

        public Builder clearAmount() {
            this.amount_ = 0.0f;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    public enum EventType implements ProtocolMessageEnum {
        INSTALL(0),
        IAP(1),
        SHOW(2),
        CLICK(3),
        FINISH(4),
        UNRECOGNIZED(-1);
        
        public static final int CLICK_VALUE = 3;
        public static final int FINISH_VALUE = 4;
        public static final int IAP_VALUE = 1;
        public static final int INSTALL_VALUE = 0;
        public static final int SHOW_VALUE = 2;
        private static final EventType[] VALUES = null;
        private static final EnumLiteMap<EventType> internalValueMap = null;
        private final int value;

        static {
            internalValueMap = new EnumLiteMap<EventType>() {
                public EventType findValueByNumber(int i) {
                    return EventType.forNumber(i);
                }
            };
            VALUES = values();
        }

        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static EventType valueOf(int i) {
            return forNumber(i);
        }

        public static EventType forNumber(int i) {
            switch (i) {
                case 0:
                    return INSTALL;
                case 1:
                    return IAP;
                case 2:
                    return SHOW;
                case 3:
                    return CLICK;
                case 4:
                    return FINISH;
                default:
                    return null;
            }
        }

        public static EnumLiteMap<EventType> internalGetValueMap() {
            return internalValueMap;
        }

        public final EnumValueDescriptor getValueDescriptor() {
            return (EnumValueDescriptor) getDescriptor().getValues().get(ordinal());
        }

        public final EnumDescriptor getDescriptorForType() {
            return getDescriptor();
        }

        public static final EnumDescriptor getDescriptor() {
            return (EnumDescriptor) Event.getDescriptor().getEnumTypes().get(0);
        }

        public static EventType valueOf(EnumValueDescriptor enumValueDescriptor) {
            if (enumValueDescriptor.getType() != getDescriptor()) {
                throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
            } else if (enumValueDescriptor.getIndex() == -1) {
                return UNRECOGNIZED;
            } else {
                return VALUES[enumValueDescriptor.getIndex()];
            }
        }

        private EventType(int i) {
            this.value = i;
        }
    }

    private Event(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Event() {
        this.memoizedIsInitialized = -1;
        this.eventType_ = 0;
        this.id_ = "";
        this.currency_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Event();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Event(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.eventType_ = codedInputStream.readEnum();
                        } else if (readTag == 18) {
                            this.id_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 24) {
                            this.placementId_ = codedInputStream.readInt32();
                        } else if (readTag == 34) {
                            this.currency_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 45) {
                            this.amount_ = codedInputStream.readFloat();
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Event_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Event_fieldAccessorTable.ensureFieldAccessorsInitialized(Event.class, Builder.class);
    }

    public int getEventTypeValue() {
        return this.eventType_;
    }

    public EventType getEventType() {
        EventType valueOf = EventType.valueOf(this.eventType_);
        return valueOf == null ? EventType.UNRECOGNIZED : valueOf;
    }

    public String getId() {
        Object obj = this.id_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.id_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getIdBytes() {
        Object obj = this.id_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.id_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public int getPlacementId() {
        return this.placementId_;
    }

    public String getCurrency() {
        Object obj = this.currency_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.currency_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getCurrencyBytes() {
        Object obj = this.currency_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.currency_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public float getAmount() {
        return this.amount_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.eventType_ != EventType.INSTALL.getNumber()) {
            codedOutputStream.writeEnum(1, this.eventType_);
        }
        if (!getIdBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 2, this.id_);
        }
        if (this.placementId_ != 0) {
            codedOutputStream.writeInt32(3, this.placementId_);
        }
        if (!getCurrencyBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 4, this.currency_);
        }
        if (this.amount_ != 0.0f) {
            codedOutputStream.writeFloat(5, this.amount_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.eventType_ != EventType.INSTALL.getNumber()) {
            i2 = 0 + CodedOutputStream.computeEnumSize(1, this.eventType_);
        }
        if (!getIdBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(2, this.id_);
        }
        if (this.placementId_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(3, this.placementId_);
        }
        if (!getCurrencyBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(4, this.currency_);
        }
        if (this.amount_ != 0.0f) {
            i2 += CodedOutputStream.computeFloatSize(5, this.amount_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Event)) {
            return super.equals(obj);
        }
        Event event = (Event) obj;
        if (this.eventType_ == event.eventType_ && getId().equals(event.getId()) && getPlacementId() == event.getPlacementId() && getCurrency().equals(event.getCurrency()) && Float.floatToIntBits(getAmount()) == Float.floatToIntBits(event.getAmount()) && this.unknownFields.equals(event.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + this.eventType_) * 37) + 2) * 53) + getId().hashCode()) * 37) + 3) * 53) + getPlacementId()) * 37) + 4) * 53) + getCurrency().hashCode()) * 37) + 5) * 53) + Float.floatToIntBits(getAmount())) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode;
        return hashCode;
    }

    public static Event parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Event) PARSER.parseFrom(byteBuffer);
    }

    public static Event parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Event) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Event parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Event) PARSER.parseFrom(byteString);
    }

    public static Event parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Event) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Event parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Event) PARSER.parseFrom(bArr);
    }

    public static Event parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Event) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Event parseFrom(InputStream inputStream) throws IOException {
        return (Event) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Event parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Event) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Event parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Event) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Event parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Event) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Event parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Event) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Event parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Event) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Event event) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(event);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Event getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Event> parser() {
        return PARSER;
    }

    public Parser<Event> getParserForType() {
        return PARSER;
    }

    public Event getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
