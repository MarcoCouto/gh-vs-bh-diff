package com.appodeal.ads.api;

import com.appodeal.ads.api.Stats.AdUnit;
import com.appodeal.ads.api.Stats.AdUnitOrBuilder;
import com.explorestack.protobuf.MessageOrBuilder;
import java.util.List;

public interface StatsOrBuilder extends MessageOrBuilder {
    AdUnit getAdUnit(int i);

    int getAdUnitCount();

    List<AdUnit> getAdUnitList();

    AdUnitOrBuilder getAdUnitOrBuilder(int i);

    List<? extends AdUnitOrBuilder> getAdUnitOrBuilderList();

    int getCapacity();

    boolean getCompleted();

    long getFinish();

    long getStart();

    boolean getSuccessful();
}
