package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.SingleFieldBuilderV3;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class Session extends GeneratedMessageV3 implements SessionOrBuilder {
    public static final int AD_STATS_FIELD_NUMBER = 8;
    private static final Session DEFAULT_INSTANCE = new Session();
    public static final int EXT_FIELD_NUMBER = 2;
    /* access modifiers changed from: private */
    public static final Parser<Session> PARSER = new AbstractParser<Session>() {
        public Session parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Session(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int SEGMENT_ID_FIELD_NUMBER = 7;
    public static final int SESSION_ID_FIELD_NUMBER = 4;
    public static final int SESSION_UPTIME_FIELD_NUMBER = 6;
    public static final int SESSION_UUID_FIELD_NUMBER = 5;
    public static final int TEST_FIELD_NUMBER = 1;
    public static final int TOKEN_FIELD_NUMBER = 3;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public AdStats adStats_;
    /* access modifiers changed from: private */
    public volatile Object ext_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public int segmentId_;
    /* access modifiers changed from: private */
    public long sessionId_;
    /* access modifiers changed from: private */
    public long sessionUptime_;
    /* access modifiers changed from: private */
    public volatile Object sessionUuid_;
    /* access modifiers changed from: private */
    public boolean test_;
    /* access modifiers changed from: private */
    public volatile Object token_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements SessionOrBuilder {
        private SingleFieldBuilderV3<AdStats, com.appodeal.ads.api.AdStats.Builder, AdStatsOrBuilder> adStatsBuilder_;
        private AdStats adStats_;
        private Object ext_;
        private int segmentId_;
        private long sessionId_;
        private long sessionUptime_;
        private Object sessionUuid_;
        private boolean test_;
        private Object token_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Session_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Session_fieldAccessorTable.ensureFieldAccessorsInitialized(Session.class, Builder.class);
        }

        private Builder() {
            this.ext_ = "";
            this.token_ = "";
            this.sessionUuid_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.ext_ = "";
            this.token_ = "";
            this.sessionUuid_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Session.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            this.test_ = false;
            this.ext_ = "";
            this.token_ = "";
            this.sessionId_ = 0;
            this.sessionUuid_ = "";
            this.sessionUptime_ = 0;
            this.segmentId_ = 0;
            if (this.adStatsBuilder_ == null) {
                this.adStats_ = null;
            } else {
                this.adStats_ = null;
                this.adStatsBuilder_ = null;
            }
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Session_descriptor;
        }

        public Session getDefaultInstanceForType() {
            return Session.getDefaultInstance();
        }

        public Session build() {
            Session buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Session buildPartial() {
            Session session = new Session((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            session.test_ = this.test_;
            session.ext_ = this.ext_;
            session.token_ = this.token_;
            session.sessionId_ = this.sessionId_;
            session.sessionUuid_ = this.sessionUuid_;
            session.sessionUptime_ = this.sessionUptime_;
            session.segmentId_ = this.segmentId_;
            if (this.adStatsBuilder_ == null) {
                session.adStats_ = this.adStats_;
            } else {
                session.adStats_ = (AdStats) this.adStatsBuilder_.build();
            }
            onBuilt();
            return session;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Session) {
                return mergeFrom((Session) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Session session) {
            if (session == Session.getDefaultInstance()) {
                return this;
            }
            if (session.getTest()) {
                setTest(session.getTest());
            }
            if (!session.getExt().isEmpty()) {
                this.ext_ = session.ext_;
                onChanged();
            }
            if (!session.getToken().isEmpty()) {
                this.token_ = session.token_;
                onChanged();
            }
            if (session.getSessionId() != 0) {
                setSessionId(session.getSessionId());
            }
            if (!session.getSessionUuid().isEmpty()) {
                this.sessionUuid_ = session.sessionUuid_;
                onChanged();
            }
            if (session.getSessionUptime() != 0) {
                setSessionUptime(session.getSessionUptime());
            }
            if (session.getSegmentId() != 0) {
                setSegmentId(session.getSegmentId());
            }
            if (session.hasAdStats()) {
                mergeAdStats(session.getAdStats());
            }
            mergeUnknownFields(session.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Session session;
            Session session2 = null;
            try {
                Session session3 = (Session) Session.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (session3 != null) {
                    mergeFrom(session3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                session = (Session) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                session2 = session;
                if (session2 != null) {
                }
                throw th;
            }
        }

        public boolean getTest() {
            return this.test_;
        }

        public Builder setTest(boolean z) {
            this.test_ = z;
            onChanged();
            return this;
        }

        public Builder clearTest() {
            this.test_ = false;
            onChanged();
            return this;
        }

        public String getExt() {
            Object obj = this.ext_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.ext_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getExtBytes() {
            Object obj = this.ext_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.ext_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setExt(String str) {
            if (str != null) {
                this.ext_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearExt() {
            this.ext_ = Session.getDefaultInstance().getExt();
            onChanged();
            return this;
        }

        public Builder setExtBytes(ByteString byteString) {
            if (byteString != null) {
                Session.checkByteStringIsUtf8(byteString);
                this.ext_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getToken() {
            Object obj = this.token_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.token_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getTokenBytes() {
            Object obj = this.token_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.token_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setToken(String str) {
            if (str != null) {
                this.token_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearToken() {
            this.token_ = Session.getDefaultInstance().getToken();
            onChanged();
            return this;
        }

        public Builder setTokenBytes(ByteString byteString) {
            if (byteString != null) {
                Session.checkByteStringIsUtf8(byteString);
                this.token_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public long getSessionId() {
            return this.sessionId_;
        }

        public Builder setSessionId(long j) {
            this.sessionId_ = j;
            onChanged();
            return this;
        }

        public Builder clearSessionId() {
            this.sessionId_ = 0;
            onChanged();
            return this;
        }

        public String getSessionUuid() {
            Object obj = this.sessionUuid_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.sessionUuid_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getSessionUuidBytes() {
            Object obj = this.sessionUuid_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.sessionUuid_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setSessionUuid(String str) {
            if (str != null) {
                this.sessionUuid_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearSessionUuid() {
            this.sessionUuid_ = Session.getDefaultInstance().getSessionUuid();
            onChanged();
            return this;
        }

        public Builder setSessionUuidBytes(ByteString byteString) {
            if (byteString != null) {
                Session.checkByteStringIsUtf8(byteString);
                this.sessionUuid_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public long getSessionUptime() {
            return this.sessionUptime_;
        }

        public Builder setSessionUptime(long j) {
            this.sessionUptime_ = j;
            onChanged();
            return this;
        }

        public Builder clearSessionUptime() {
            this.sessionUptime_ = 0;
            onChanged();
            return this;
        }

        public int getSegmentId() {
            return this.segmentId_;
        }

        public Builder setSegmentId(int i) {
            this.segmentId_ = i;
            onChanged();
            return this;
        }

        public Builder clearSegmentId() {
            this.segmentId_ = 0;
            onChanged();
            return this;
        }

        public boolean hasAdStats() {
            return (this.adStatsBuilder_ == null && this.adStats_ == null) ? false : true;
        }

        public AdStats getAdStats() {
            if (this.adStatsBuilder_ != null) {
                return (AdStats) this.adStatsBuilder_.getMessage();
            }
            return this.adStats_ == null ? AdStats.getDefaultInstance() : this.adStats_;
        }

        public Builder setAdStats(AdStats adStats) {
            if (this.adStatsBuilder_ != null) {
                this.adStatsBuilder_.setMessage(adStats);
            } else if (adStats != null) {
                this.adStats_ = adStats;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setAdStats(com.appodeal.ads.api.AdStats.Builder builder) {
            if (this.adStatsBuilder_ == null) {
                this.adStats_ = builder.build();
                onChanged();
            } else {
                this.adStatsBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeAdStats(AdStats adStats) {
            if (this.adStatsBuilder_ == null) {
                if (this.adStats_ != null) {
                    this.adStats_ = AdStats.newBuilder(this.adStats_).mergeFrom(adStats).buildPartial();
                } else {
                    this.adStats_ = adStats;
                }
                onChanged();
            } else {
                this.adStatsBuilder_.mergeFrom(adStats);
            }
            return this;
        }

        public Builder clearAdStats() {
            if (this.adStatsBuilder_ == null) {
                this.adStats_ = null;
                onChanged();
            } else {
                this.adStats_ = null;
                this.adStatsBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.AdStats.Builder getAdStatsBuilder() {
            onChanged();
            return (com.appodeal.ads.api.AdStats.Builder) getAdStatsFieldBuilder().getBuilder();
        }

        public AdStatsOrBuilder getAdStatsOrBuilder() {
            if (this.adStatsBuilder_ != null) {
                return (AdStatsOrBuilder) this.adStatsBuilder_.getMessageOrBuilder();
            }
            return this.adStats_ == null ? AdStats.getDefaultInstance() : this.adStats_;
        }

        private SingleFieldBuilderV3<AdStats, com.appodeal.ads.api.AdStats.Builder, AdStatsOrBuilder> getAdStatsFieldBuilder() {
            if (this.adStatsBuilder_ == null) {
                this.adStatsBuilder_ = new SingleFieldBuilderV3<>(getAdStats(), getParentForChildren(), isClean());
                this.adStats_ = null;
            }
            return this.adStatsBuilder_;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private Session(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Session() {
        this.memoizedIsInitialized = -1;
        this.ext_ = "";
        this.token_ = "";
        this.sessionUuid_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Session();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    private Session(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.test_ = codedInputStream.readBool();
                        } else if (readTag == 18) {
                            this.ext_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 26) {
                            this.token_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 32) {
                            this.sessionId_ = codedInputStream.readInt64();
                        } else if (readTag == 42) {
                            this.sessionUuid_ = codedInputStream.readStringRequireUtf8();
                        } else if (readTag == 48) {
                            this.sessionUptime_ = codedInputStream.readInt64();
                        } else if (readTag == 56) {
                            this.segmentId_ = codedInputStream.readInt32();
                        } else if (readTag == 66) {
                            com.appodeal.ads.api.AdStats.Builder builder = null;
                            if (this.adStats_ != null) {
                                builder = this.adStats_.toBuilder();
                            }
                            this.adStats_ = (AdStats) codedInputStream.readMessage(AdStats.parser(), extensionRegistryLite);
                            if (builder != null) {
                                builder.mergeFrom(this.adStats_);
                                this.adStats_ = builder.buildPartial();
                            }
                        } else if (!parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                        }
                    }
                    z = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Session_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Session_fieldAccessorTable.ensureFieldAccessorsInitialized(Session.class, Builder.class);
    }

    public boolean getTest() {
        return this.test_;
    }

    public String getExt() {
        Object obj = this.ext_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.ext_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getExtBytes() {
        Object obj = this.ext_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.ext_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getToken() {
        Object obj = this.token_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.token_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getTokenBytes() {
        Object obj = this.token_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.token_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public long getSessionId() {
        return this.sessionId_;
    }

    public String getSessionUuid() {
        Object obj = this.sessionUuid_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.sessionUuid_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getSessionUuidBytes() {
        Object obj = this.sessionUuid_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.sessionUuid_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public long getSessionUptime() {
        return this.sessionUptime_;
    }

    public int getSegmentId() {
        return this.segmentId_;
    }

    public boolean hasAdStats() {
        return this.adStats_ != null;
    }

    public AdStats getAdStats() {
        return this.adStats_ == null ? AdStats.getDefaultInstance() : this.adStats_;
    }

    public AdStatsOrBuilder getAdStatsOrBuilder() {
        return getAdStats();
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.test_) {
            codedOutputStream.writeBool(1, this.test_);
        }
        if (!getExtBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 2, this.ext_);
        }
        if (!getTokenBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 3, this.token_);
        }
        if (this.sessionId_ != 0) {
            codedOutputStream.writeInt64(4, this.sessionId_);
        }
        if (!getSessionUuidBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 5, this.sessionUuid_);
        }
        if (this.sessionUptime_ != 0) {
            codedOutputStream.writeInt64(6, this.sessionUptime_);
        }
        if (this.segmentId_ != 0) {
            codedOutputStream.writeInt32(7, this.segmentId_);
        }
        if (this.adStats_ != null) {
            codedOutputStream.writeMessage(8, getAdStats());
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.test_) {
            i2 = 0 + CodedOutputStream.computeBoolSize(1, this.test_);
        }
        if (!getExtBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(2, this.ext_);
        }
        if (!getTokenBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(3, this.token_);
        }
        if (this.sessionId_ != 0) {
            i2 += CodedOutputStream.computeInt64Size(4, this.sessionId_);
        }
        if (!getSessionUuidBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(5, this.sessionUuid_);
        }
        if (this.sessionUptime_ != 0) {
            i2 += CodedOutputStream.computeInt64Size(6, this.sessionUptime_);
        }
        if (this.segmentId_ != 0) {
            i2 += CodedOutputStream.computeInt32Size(7, this.segmentId_);
        }
        if (this.adStats_ != null) {
            i2 += CodedOutputStream.computeMessageSize(8, getAdStats());
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Session)) {
            return super.equals(obj);
        }
        Session session = (Session) obj;
        if (getTest() != session.getTest() || !getExt().equals(session.getExt()) || !getToken().equals(session.getToken()) || getSessionId() != session.getSessionId() || !getSessionUuid().equals(session.getSessionUuid()) || getSessionUptime() != session.getSessionUptime() || getSegmentId() != session.getSegmentId() || hasAdStats() != session.hasAdStats()) {
            return false;
        }
        if ((!hasAdStats() || getAdStats().equals(session.getAdStats())) && this.unknownFields.equals(session.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = ((((((((((((((((((((((((((((779 + getDescriptor().hashCode()) * 37) + 1) * 53) + Internal.hashBoolean(getTest())) * 37) + 2) * 53) + getExt().hashCode()) * 37) + 3) * 53) + getToken().hashCode()) * 37) + 4) * 53) + Internal.hashLong(getSessionId())) * 37) + 5) * 53) + getSessionUuid().hashCode()) * 37) + 6) * 53) + Internal.hashLong(getSessionUptime())) * 37) + 7) * 53) + getSegmentId();
        if (hasAdStats()) {
            hashCode = (((hashCode * 37) + 8) * 53) + getAdStats().hashCode();
        }
        int hashCode2 = (hashCode * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashCode2;
        return hashCode2;
    }

    public static Session parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Session) PARSER.parseFrom(byteBuffer);
    }

    public static Session parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Session) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Session parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Session) PARSER.parseFrom(byteString);
    }

    public static Session parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Session) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Session parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Session) PARSER.parseFrom(bArr);
    }

    public static Session parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Session) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Session parseFrom(InputStream inputStream) throws IOException {
        return (Session) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Session parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Session) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Session parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Session) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Session parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Session) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Session parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Session) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Session parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Session) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Session session) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(session);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Session getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Session> parser() {
        return PARSER;
    }

    public Parser<Session> getParserForType() {
        return PARSER;
    }

    public Session getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
