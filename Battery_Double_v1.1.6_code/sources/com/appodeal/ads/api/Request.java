package com.appodeal.ads.api;

import com.explorestack.protobuf.AbstractParser;
import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.CodedInputStream;
import com.explorestack.protobuf.CodedOutputStream;
import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.ExtensionRegistryLite;
import com.explorestack.protobuf.GeneratedMessageV3;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;
import com.explorestack.protobuf.Internal;
import com.explorestack.protobuf.InvalidProtocolBufferException;
import com.explorestack.protobuf.Message;
import com.explorestack.protobuf.Parser;
import com.explorestack.protobuf.SingleFieldBuilderV3;
import com.explorestack.protobuf.UnknownFieldSet;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class Request extends GeneratedMessageV3 implements RequestOrBuilder {
    public static final int APP_FIELD_NUMBER = 1;
    private static final Request DEFAULT_INSTANCE = new Request();
    public static final int DEVICE_FIELD_NUMBER = 3;
    public static final int EVENT_FIELD_NUMBER = 12;
    public static final int EXT_FIELD_NUMBER = 7;
    public static final int GEO_FIELD_NUMBER = 6;
    public static final int GET_FIELD_NUMBER = 10;
    public static final int IMPID_FIELD_NUMBER = 8;
    public static final int MAIN_ID_FIELD_NUMBER = 9;
    /* access modifiers changed from: private */
    public static final Parser<Request> PARSER = new AbstractParser<Request>() {
        public Request parsePartialFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return new Request(codedInputStream, extensionRegistryLite);
        }
    };
    public static final int REGS_FIELD_NUMBER = 5;
    public static final int SESSION_FIELD_NUMBER = 2;
    public static final int STATS_FIELD_NUMBER = 11;
    public static final int TIMESTAMP_FIELD_NUMBER = 13;
    public static final int USER_FIELD_NUMBER = 4;
    private static final long serialVersionUID = 0;
    /* access modifiers changed from: private */
    public App app_;
    /* access modifiers changed from: private */
    public Device device_;
    /* access modifiers changed from: private */
    public Event event_;
    /* access modifiers changed from: private */
    public Extra ext_;
    /* access modifiers changed from: private */
    public Geo geo_;
    /* access modifiers changed from: private */
    public Get get_;
    /* access modifiers changed from: private */
    public volatile Object impid_;
    /* access modifiers changed from: private */
    public volatile Object mainId_;
    private byte memoizedIsInitialized;
    /* access modifiers changed from: private */
    public Regs regs_;
    /* access modifiers changed from: private */
    public Session session_;
    /* access modifiers changed from: private */
    public Stats stats_;
    /* access modifiers changed from: private */
    public long timestamp_;
    /* access modifiers changed from: private */
    public User user_;

    public static final class Builder extends com.explorestack.protobuf.GeneratedMessageV3.Builder<Builder> implements RequestOrBuilder {
        private SingleFieldBuilderV3<App, com.appodeal.ads.api.App.Builder, AppOrBuilder> appBuilder_;
        private App app_;
        private SingleFieldBuilderV3<Device, com.appodeal.ads.api.Device.Builder, DeviceOrBuilder> deviceBuilder_;
        private Device device_;
        private SingleFieldBuilderV3<Event, com.appodeal.ads.api.Event.Builder, EventOrBuilder> eventBuilder_;
        private Event event_;
        private SingleFieldBuilderV3<Extra, com.appodeal.ads.api.Extra.Builder, ExtraOrBuilder> extBuilder_;
        private Extra ext_;
        private SingleFieldBuilderV3<Geo, com.appodeal.ads.api.Geo.Builder, GeoOrBuilder> geoBuilder_;
        private Geo geo_;
        private SingleFieldBuilderV3<Get, com.appodeal.ads.api.Get.Builder, GetOrBuilder> getBuilder_;
        private Get get_;
        private Object impid_;
        private Object mainId_;
        private SingleFieldBuilderV3<Regs, com.appodeal.ads.api.Regs.Builder, RegsOrBuilder> regsBuilder_;
        private Regs regs_;
        private SingleFieldBuilderV3<Session, com.appodeal.ads.api.Session.Builder, SessionOrBuilder> sessionBuilder_;
        private Session session_;
        private SingleFieldBuilderV3<Stats, com.appodeal.ads.api.Stats.Builder, StatsOrBuilder> statsBuilder_;
        private Stats stats_;
        private long timestamp_;
        private SingleFieldBuilderV3<User, com.appodeal.ads.api.User.Builder, UserOrBuilder> userBuilder_;
        private User user_;

        public final boolean isInitialized() {
            return true;
        }

        public static final Descriptor getDescriptor() {
            return Api.internal_static_com_appodeal_ads_Request_descriptor;
        }

        /* access modifiers changed from: protected */
        public FieldAccessorTable internalGetFieldAccessorTable() {
            return Api.internal_static_com_appodeal_ads_Request_fieldAccessorTable.ensureFieldAccessorsInitialized(Request.class, Builder.class);
        }

        private Builder() {
            this.impid_ = "";
            this.mainId_ = "";
            maybeForceBuilderInitialization();
        }

        private Builder(BuilderParent builderParent) {
            super(builderParent);
            this.impid_ = "";
            this.mainId_ = "";
            maybeForceBuilderInitialization();
        }

        private void maybeForceBuilderInitialization() {
            Request.alwaysUseFieldBuilders;
        }

        public Builder clear() {
            super.clear();
            if (this.appBuilder_ == null) {
                this.app_ = null;
            } else {
                this.app_ = null;
                this.appBuilder_ = null;
            }
            if (this.sessionBuilder_ == null) {
                this.session_ = null;
            } else {
                this.session_ = null;
                this.sessionBuilder_ = null;
            }
            if (this.deviceBuilder_ == null) {
                this.device_ = null;
            } else {
                this.device_ = null;
                this.deviceBuilder_ = null;
            }
            if (this.userBuilder_ == null) {
                this.user_ = null;
            } else {
                this.user_ = null;
                this.userBuilder_ = null;
            }
            if (this.regsBuilder_ == null) {
                this.regs_ = null;
            } else {
                this.regs_ = null;
                this.regsBuilder_ = null;
            }
            if (this.geoBuilder_ == null) {
                this.geo_ = null;
            } else {
                this.geo_ = null;
                this.geoBuilder_ = null;
            }
            if (this.extBuilder_ == null) {
                this.ext_ = null;
            } else {
                this.ext_ = null;
                this.extBuilder_ = null;
            }
            this.impid_ = "";
            this.mainId_ = "";
            if (this.getBuilder_ == null) {
                this.get_ = null;
            } else {
                this.get_ = null;
                this.getBuilder_ = null;
            }
            if (this.statsBuilder_ == null) {
                this.stats_ = null;
            } else {
                this.stats_ = null;
                this.statsBuilder_ = null;
            }
            if (this.eventBuilder_ == null) {
                this.event_ = null;
            } else {
                this.event_ = null;
                this.eventBuilder_ = null;
            }
            this.timestamp_ = 0;
            return this;
        }

        public Descriptor getDescriptorForType() {
            return Api.internal_static_com_appodeal_ads_Request_descriptor;
        }

        public Request getDefaultInstanceForType() {
            return Request.getDefaultInstance();
        }

        public Request build() {
            Request buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw newUninitializedMessageException(buildPartial);
        }

        public Request buildPartial() {
            Request request = new Request((com.explorestack.protobuf.GeneratedMessageV3.Builder) this);
            if (this.appBuilder_ == null) {
                request.app_ = this.app_;
            } else {
                request.app_ = (App) this.appBuilder_.build();
            }
            if (this.sessionBuilder_ == null) {
                request.session_ = this.session_;
            } else {
                request.session_ = (Session) this.sessionBuilder_.build();
            }
            if (this.deviceBuilder_ == null) {
                request.device_ = this.device_;
            } else {
                request.device_ = (Device) this.deviceBuilder_.build();
            }
            if (this.userBuilder_ == null) {
                request.user_ = this.user_;
            } else {
                request.user_ = (User) this.userBuilder_.build();
            }
            if (this.regsBuilder_ == null) {
                request.regs_ = this.regs_;
            } else {
                request.regs_ = (Regs) this.regsBuilder_.build();
            }
            if (this.geoBuilder_ == null) {
                request.geo_ = this.geo_;
            } else {
                request.geo_ = (Geo) this.geoBuilder_.build();
            }
            if (this.extBuilder_ == null) {
                request.ext_ = this.ext_;
            } else {
                request.ext_ = (Extra) this.extBuilder_.build();
            }
            request.impid_ = this.impid_;
            request.mainId_ = this.mainId_;
            if (this.getBuilder_ == null) {
                request.get_ = this.get_;
            } else {
                request.get_ = (Get) this.getBuilder_.build();
            }
            if (this.statsBuilder_ == null) {
                request.stats_ = this.stats_;
            } else {
                request.stats_ = (Stats) this.statsBuilder_.build();
            }
            if (this.eventBuilder_ == null) {
                request.event_ = this.event_;
            } else {
                request.event_ = (Event) this.eventBuilder_.build();
            }
            request.timestamp_ = this.timestamp_;
            onBuilt();
            return request;
        }

        public Builder clone() {
            return (Builder) super.clone();
        }

        public Builder setField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.setField(fieldDescriptor, obj);
        }

        public Builder clearField(FieldDescriptor fieldDescriptor) {
            return (Builder) super.clearField(fieldDescriptor);
        }

        public Builder clearOneof(OneofDescriptor oneofDescriptor) {
            return (Builder) super.clearOneof(oneofDescriptor);
        }

        public Builder setRepeatedField(FieldDescriptor fieldDescriptor, int i, Object obj) {
            return (Builder) super.setRepeatedField(fieldDescriptor, i, obj);
        }

        public Builder addRepeatedField(FieldDescriptor fieldDescriptor, Object obj) {
            return (Builder) super.addRepeatedField(fieldDescriptor, obj);
        }

        public Builder mergeFrom(Message message) {
            if (message instanceof Request) {
                return mergeFrom((Request) message);
            }
            super.mergeFrom(message);
            return this;
        }

        public Builder mergeFrom(Request request) {
            if (request == Request.getDefaultInstance()) {
                return this;
            }
            if (request.hasApp()) {
                mergeApp(request.getApp());
            }
            if (request.hasSession()) {
                mergeSession(request.getSession());
            }
            if (request.hasDevice()) {
                mergeDevice(request.getDevice());
            }
            if (request.hasUser()) {
                mergeUser(request.getUser());
            }
            if (request.hasRegs()) {
                mergeRegs(request.getRegs());
            }
            if (request.hasGeo()) {
                mergeGeo(request.getGeo());
            }
            if (request.hasExt()) {
                mergeExt(request.getExt());
            }
            if (!request.getImpid().isEmpty()) {
                this.impid_ = request.impid_;
                onChanged();
            }
            if (!request.getMainId().isEmpty()) {
                this.mainId_ = request.mainId_;
                onChanged();
            }
            if (request.hasGet()) {
                mergeGet(request.getGet());
            }
            if (request.hasStats()) {
                mergeStats(request.getStats());
            }
            if (request.hasEvent()) {
                mergeEvent(request.getEvent());
            }
            if (request.getTimestamp() != 0) {
                setTimestamp(request.getTimestamp());
            }
            mergeUnknownFields(request.unknownFields);
            onChanged();
            return this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
        public Builder mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            Request request;
            Request request2 = null;
            try {
                Request request3 = (Request) Request.PARSER.parsePartialFrom(codedInputStream, extensionRegistryLite);
                if (request3 != null) {
                    mergeFrom(request3);
                }
                return this;
            } catch (InvalidProtocolBufferException e) {
                request = (Request) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } catch (Throwable th) {
                th = th;
                request2 = request;
                if (request2 != null) {
                }
                throw th;
            }
        }

        public boolean hasApp() {
            return (this.appBuilder_ == null && this.app_ == null) ? false : true;
        }

        public App getApp() {
            if (this.appBuilder_ != null) {
                return (App) this.appBuilder_.getMessage();
            }
            return this.app_ == null ? App.getDefaultInstance() : this.app_;
        }

        public Builder setApp(App app) {
            if (this.appBuilder_ != null) {
                this.appBuilder_.setMessage(app);
            } else if (app != null) {
                this.app_ = app;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setApp(com.appodeal.ads.api.App.Builder builder) {
            if (this.appBuilder_ == null) {
                this.app_ = builder.build();
                onChanged();
            } else {
                this.appBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeApp(App app) {
            if (this.appBuilder_ == null) {
                if (this.app_ != null) {
                    this.app_ = App.newBuilder(this.app_).mergeFrom(app).buildPartial();
                } else {
                    this.app_ = app;
                }
                onChanged();
            } else {
                this.appBuilder_.mergeFrom(app);
            }
            return this;
        }

        public Builder clearApp() {
            if (this.appBuilder_ == null) {
                this.app_ = null;
                onChanged();
            } else {
                this.app_ = null;
                this.appBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.App.Builder getAppBuilder() {
            onChanged();
            return (com.appodeal.ads.api.App.Builder) getAppFieldBuilder().getBuilder();
        }

        public AppOrBuilder getAppOrBuilder() {
            if (this.appBuilder_ != null) {
                return (AppOrBuilder) this.appBuilder_.getMessageOrBuilder();
            }
            return this.app_ == null ? App.getDefaultInstance() : this.app_;
        }

        private SingleFieldBuilderV3<App, com.appodeal.ads.api.App.Builder, AppOrBuilder> getAppFieldBuilder() {
            if (this.appBuilder_ == null) {
                this.appBuilder_ = new SingleFieldBuilderV3<>(getApp(), getParentForChildren(), isClean());
                this.app_ = null;
            }
            return this.appBuilder_;
        }

        public boolean hasSession() {
            return (this.sessionBuilder_ == null && this.session_ == null) ? false : true;
        }

        public Session getSession() {
            if (this.sessionBuilder_ != null) {
                return (Session) this.sessionBuilder_.getMessage();
            }
            return this.session_ == null ? Session.getDefaultInstance() : this.session_;
        }

        public Builder setSession(Session session) {
            if (this.sessionBuilder_ != null) {
                this.sessionBuilder_.setMessage(session);
            } else if (session != null) {
                this.session_ = session;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setSession(com.appodeal.ads.api.Session.Builder builder) {
            if (this.sessionBuilder_ == null) {
                this.session_ = builder.build();
                onChanged();
            } else {
                this.sessionBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeSession(Session session) {
            if (this.sessionBuilder_ == null) {
                if (this.session_ != null) {
                    this.session_ = Session.newBuilder(this.session_).mergeFrom(session).buildPartial();
                } else {
                    this.session_ = session;
                }
                onChanged();
            } else {
                this.sessionBuilder_.mergeFrom(session);
            }
            return this;
        }

        public Builder clearSession() {
            if (this.sessionBuilder_ == null) {
                this.session_ = null;
                onChanged();
            } else {
                this.session_ = null;
                this.sessionBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Session.Builder getSessionBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Session.Builder) getSessionFieldBuilder().getBuilder();
        }

        public SessionOrBuilder getSessionOrBuilder() {
            if (this.sessionBuilder_ != null) {
                return (SessionOrBuilder) this.sessionBuilder_.getMessageOrBuilder();
            }
            return this.session_ == null ? Session.getDefaultInstance() : this.session_;
        }

        private SingleFieldBuilderV3<Session, com.appodeal.ads.api.Session.Builder, SessionOrBuilder> getSessionFieldBuilder() {
            if (this.sessionBuilder_ == null) {
                this.sessionBuilder_ = new SingleFieldBuilderV3<>(getSession(), getParentForChildren(), isClean());
                this.session_ = null;
            }
            return this.sessionBuilder_;
        }

        public boolean hasDevice() {
            return (this.deviceBuilder_ == null && this.device_ == null) ? false : true;
        }

        public Device getDevice() {
            if (this.deviceBuilder_ != null) {
                return (Device) this.deviceBuilder_.getMessage();
            }
            return this.device_ == null ? Device.getDefaultInstance() : this.device_;
        }

        public Builder setDevice(Device device) {
            if (this.deviceBuilder_ != null) {
                this.deviceBuilder_.setMessage(device);
            } else if (device != null) {
                this.device_ = device;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setDevice(com.appodeal.ads.api.Device.Builder builder) {
            if (this.deviceBuilder_ == null) {
                this.device_ = builder.build();
                onChanged();
            } else {
                this.deviceBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeDevice(Device device) {
            if (this.deviceBuilder_ == null) {
                if (this.device_ != null) {
                    this.device_ = Device.newBuilder(this.device_).mergeFrom(device).buildPartial();
                } else {
                    this.device_ = device;
                }
                onChanged();
            } else {
                this.deviceBuilder_.mergeFrom(device);
            }
            return this;
        }

        public Builder clearDevice() {
            if (this.deviceBuilder_ == null) {
                this.device_ = null;
                onChanged();
            } else {
                this.device_ = null;
                this.deviceBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Device.Builder getDeviceBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Device.Builder) getDeviceFieldBuilder().getBuilder();
        }

        public DeviceOrBuilder getDeviceOrBuilder() {
            if (this.deviceBuilder_ != null) {
                return (DeviceOrBuilder) this.deviceBuilder_.getMessageOrBuilder();
            }
            return this.device_ == null ? Device.getDefaultInstance() : this.device_;
        }

        private SingleFieldBuilderV3<Device, com.appodeal.ads.api.Device.Builder, DeviceOrBuilder> getDeviceFieldBuilder() {
            if (this.deviceBuilder_ == null) {
                this.deviceBuilder_ = new SingleFieldBuilderV3<>(getDevice(), getParentForChildren(), isClean());
                this.device_ = null;
            }
            return this.deviceBuilder_;
        }

        public boolean hasUser() {
            return (this.userBuilder_ == null && this.user_ == null) ? false : true;
        }

        public User getUser() {
            if (this.userBuilder_ != null) {
                return (User) this.userBuilder_.getMessage();
            }
            return this.user_ == null ? User.getDefaultInstance() : this.user_;
        }

        public Builder setUser(User user) {
            if (this.userBuilder_ != null) {
                this.userBuilder_.setMessage(user);
            } else if (user != null) {
                this.user_ = user;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setUser(com.appodeal.ads.api.User.Builder builder) {
            if (this.userBuilder_ == null) {
                this.user_ = builder.build();
                onChanged();
            } else {
                this.userBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeUser(User user) {
            if (this.userBuilder_ == null) {
                if (this.user_ != null) {
                    this.user_ = User.newBuilder(this.user_).mergeFrom(user).buildPartial();
                } else {
                    this.user_ = user;
                }
                onChanged();
            } else {
                this.userBuilder_.mergeFrom(user);
            }
            return this;
        }

        public Builder clearUser() {
            if (this.userBuilder_ == null) {
                this.user_ = null;
                onChanged();
            } else {
                this.user_ = null;
                this.userBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.User.Builder getUserBuilder() {
            onChanged();
            return (com.appodeal.ads.api.User.Builder) getUserFieldBuilder().getBuilder();
        }

        public UserOrBuilder getUserOrBuilder() {
            if (this.userBuilder_ != null) {
                return (UserOrBuilder) this.userBuilder_.getMessageOrBuilder();
            }
            return this.user_ == null ? User.getDefaultInstance() : this.user_;
        }

        private SingleFieldBuilderV3<User, com.appodeal.ads.api.User.Builder, UserOrBuilder> getUserFieldBuilder() {
            if (this.userBuilder_ == null) {
                this.userBuilder_ = new SingleFieldBuilderV3<>(getUser(), getParentForChildren(), isClean());
                this.user_ = null;
            }
            return this.userBuilder_;
        }

        public boolean hasRegs() {
            return (this.regsBuilder_ == null && this.regs_ == null) ? false : true;
        }

        public Regs getRegs() {
            if (this.regsBuilder_ != null) {
                return (Regs) this.regsBuilder_.getMessage();
            }
            return this.regs_ == null ? Regs.getDefaultInstance() : this.regs_;
        }

        public Builder setRegs(Regs regs) {
            if (this.regsBuilder_ != null) {
                this.regsBuilder_.setMessage(regs);
            } else if (regs != null) {
                this.regs_ = regs;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setRegs(com.appodeal.ads.api.Regs.Builder builder) {
            if (this.regsBuilder_ == null) {
                this.regs_ = builder.build();
                onChanged();
            } else {
                this.regsBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeRegs(Regs regs) {
            if (this.regsBuilder_ == null) {
                if (this.regs_ != null) {
                    this.regs_ = Regs.newBuilder(this.regs_).mergeFrom(regs).buildPartial();
                } else {
                    this.regs_ = regs;
                }
                onChanged();
            } else {
                this.regsBuilder_.mergeFrom(regs);
            }
            return this;
        }

        public Builder clearRegs() {
            if (this.regsBuilder_ == null) {
                this.regs_ = null;
                onChanged();
            } else {
                this.regs_ = null;
                this.regsBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Regs.Builder getRegsBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Regs.Builder) getRegsFieldBuilder().getBuilder();
        }

        public RegsOrBuilder getRegsOrBuilder() {
            if (this.regsBuilder_ != null) {
                return (RegsOrBuilder) this.regsBuilder_.getMessageOrBuilder();
            }
            return this.regs_ == null ? Regs.getDefaultInstance() : this.regs_;
        }

        private SingleFieldBuilderV3<Regs, com.appodeal.ads.api.Regs.Builder, RegsOrBuilder> getRegsFieldBuilder() {
            if (this.regsBuilder_ == null) {
                this.regsBuilder_ = new SingleFieldBuilderV3<>(getRegs(), getParentForChildren(), isClean());
                this.regs_ = null;
            }
            return this.regsBuilder_;
        }

        public boolean hasGeo() {
            return (this.geoBuilder_ == null && this.geo_ == null) ? false : true;
        }

        public Geo getGeo() {
            if (this.geoBuilder_ != null) {
                return (Geo) this.geoBuilder_.getMessage();
            }
            return this.geo_ == null ? Geo.getDefaultInstance() : this.geo_;
        }

        public Builder setGeo(Geo geo) {
            if (this.geoBuilder_ != null) {
                this.geoBuilder_.setMessage(geo);
            } else if (geo != null) {
                this.geo_ = geo;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setGeo(com.appodeal.ads.api.Geo.Builder builder) {
            if (this.geoBuilder_ == null) {
                this.geo_ = builder.build();
                onChanged();
            } else {
                this.geoBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeGeo(Geo geo) {
            if (this.geoBuilder_ == null) {
                if (this.geo_ != null) {
                    this.geo_ = Geo.newBuilder(this.geo_).mergeFrom(geo).buildPartial();
                } else {
                    this.geo_ = geo;
                }
                onChanged();
            } else {
                this.geoBuilder_.mergeFrom(geo);
            }
            return this;
        }

        public Builder clearGeo() {
            if (this.geoBuilder_ == null) {
                this.geo_ = null;
                onChanged();
            } else {
                this.geo_ = null;
                this.geoBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Geo.Builder getGeoBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Geo.Builder) getGeoFieldBuilder().getBuilder();
        }

        public GeoOrBuilder getGeoOrBuilder() {
            if (this.geoBuilder_ != null) {
                return (GeoOrBuilder) this.geoBuilder_.getMessageOrBuilder();
            }
            return this.geo_ == null ? Geo.getDefaultInstance() : this.geo_;
        }

        private SingleFieldBuilderV3<Geo, com.appodeal.ads.api.Geo.Builder, GeoOrBuilder> getGeoFieldBuilder() {
            if (this.geoBuilder_ == null) {
                this.geoBuilder_ = new SingleFieldBuilderV3<>(getGeo(), getParentForChildren(), isClean());
                this.geo_ = null;
            }
            return this.geoBuilder_;
        }

        public boolean hasExt() {
            return (this.extBuilder_ == null && this.ext_ == null) ? false : true;
        }

        public Extra getExt() {
            if (this.extBuilder_ != null) {
                return (Extra) this.extBuilder_.getMessage();
            }
            return this.ext_ == null ? Extra.getDefaultInstance() : this.ext_;
        }

        public Builder setExt(Extra extra) {
            if (this.extBuilder_ != null) {
                this.extBuilder_.setMessage(extra);
            } else if (extra != null) {
                this.ext_ = extra;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setExt(com.appodeal.ads.api.Extra.Builder builder) {
            if (this.extBuilder_ == null) {
                this.ext_ = builder.build();
                onChanged();
            } else {
                this.extBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeExt(Extra extra) {
            if (this.extBuilder_ == null) {
                if (this.ext_ != null) {
                    this.ext_ = Extra.newBuilder(this.ext_).mergeFrom(extra).buildPartial();
                } else {
                    this.ext_ = extra;
                }
                onChanged();
            } else {
                this.extBuilder_.mergeFrom(extra);
            }
            return this;
        }

        public Builder clearExt() {
            if (this.extBuilder_ == null) {
                this.ext_ = null;
                onChanged();
            } else {
                this.ext_ = null;
                this.extBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Extra.Builder getExtBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Extra.Builder) getExtFieldBuilder().getBuilder();
        }

        public ExtraOrBuilder getExtOrBuilder() {
            if (this.extBuilder_ != null) {
                return (ExtraOrBuilder) this.extBuilder_.getMessageOrBuilder();
            }
            return this.ext_ == null ? Extra.getDefaultInstance() : this.ext_;
        }

        private SingleFieldBuilderV3<Extra, com.appodeal.ads.api.Extra.Builder, ExtraOrBuilder> getExtFieldBuilder() {
            if (this.extBuilder_ == null) {
                this.extBuilder_ = new SingleFieldBuilderV3<>(getExt(), getParentForChildren(), isClean());
                this.ext_ = null;
            }
            return this.extBuilder_;
        }

        public String getImpid() {
            Object obj = this.impid_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.impid_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getImpidBytes() {
            Object obj = this.impid_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.impid_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setImpid(String str) {
            if (str != null) {
                this.impid_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearImpid() {
            this.impid_ = Request.getDefaultInstance().getImpid();
            onChanged();
            return this;
        }

        public Builder setImpidBytes(ByteString byteString) {
            if (byteString != null) {
                Request.checkByteStringIsUtf8(byteString);
                this.impid_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public String getMainId() {
            Object obj = this.mainId_;
            if (obj instanceof String) {
                return (String) obj;
            }
            String stringUtf8 = ((ByteString) obj).toStringUtf8();
            this.mainId_ = stringUtf8;
            return stringUtf8;
        }

        public ByteString getMainIdBytes() {
            Object obj = this.mainId_;
            if (!(obj instanceof String)) {
                return (ByteString) obj;
            }
            ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
            this.mainId_ = copyFromUtf8;
            return copyFromUtf8;
        }

        public Builder setMainId(String str) {
            if (str != null) {
                this.mainId_ = str;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public Builder clearMainId() {
            this.mainId_ = Request.getDefaultInstance().getMainId();
            onChanged();
            return this;
        }

        public Builder setMainIdBytes(ByteString byteString) {
            if (byteString != null) {
                Request.checkByteStringIsUtf8(byteString);
                this.mainId_ = byteString;
                onChanged();
                return this;
            }
            throw new NullPointerException();
        }

        public boolean hasGet() {
            return (this.getBuilder_ == null && this.get_ == null) ? false : true;
        }

        public Get getGet() {
            if (this.getBuilder_ != null) {
                return (Get) this.getBuilder_.getMessage();
            }
            return this.get_ == null ? Get.getDefaultInstance() : this.get_;
        }

        public Builder setGet(Get get) {
            if (this.getBuilder_ != null) {
                this.getBuilder_.setMessage(get);
            } else if (get != null) {
                this.get_ = get;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setGet(com.appodeal.ads.api.Get.Builder builder) {
            if (this.getBuilder_ == null) {
                this.get_ = builder.build();
                onChanged();
            } else {
                this.getBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeGet(Get get) {
            if (this.getBuilder_ == null) {
                if (this.get_ != null) {
                    this.get_ = Get.newBuilder(this.get_).mergeFrom(get).buildPartial();
                } else {
                    this.get_ = get;
                }
                onChanged();
            } else {
                this.getBuilder_.mergeFrom(get);
            }
            return this;
        }

        public Builder clearGet() {
            if (this.getBuilder_ == null) {
                this.get_ = null;
                onChanged();
            } else {
                this.get_ = null;
                this.getBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Get.Builder getGetBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Get.Builder) getGetFieldBuilder().getBuilder();
        }

        public GetOrBuilder getGetOrBuilder() {
            if (this.getBuilder_ != null) {
                return (GetOrBuilder) this.getBuilder_.getMessageOrBuilder();
            }
            return this.get_ == null ? Get.getDefaultInstance() : this.get_;
        }

        private SingleFieldBuilderV3<Get, com.appodeal.ads.api.Get.Builder, GetOrBuilder> getGetFieldBuilder() {
            if (this.getBuilder_ == null) {
                this.getBuilder_ = new SingleFieldBuilderV3<>(getGet(), getParentForChildren(), isClean());
                this.get_ = null;
            }
            return this.getBuilder_;
        }

        public boolean hasStats() {
            return (this.statsBuilder_ == null && this.stats_ == null) ? false : true;
        }

        public Stats getStats() {
            if (this.statsBuilder_ != null) {
                return (Stats) this.statsBuilder_.getMessage();
            }
            return this.stats_ == null ? Stats.getDefaultInstance() : this.stats_;
        }

        public Builder setStats(Stats stats) {
            if (this.statsBuilder_ != null) {
                this.statsBuilder_.setMessage(stats);
            } else if (stats != null) {
                this.stats_ = stats;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setStats(com.appodeal.ads.api.Stats.Builder builder) {
            if (this.statsBuilder_ == null) {
                this.stats_ = builder.build();
                onChanged();
            } else {
                this.statsBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeStats(Stats stats) {
            if (this.statsBuilder_ == null) {
                if (this.stats_ != null) {
                    this.stats_ = Stats.newBuilder(this.stats_).mergeFrom(stats).buildPartial();
                } else {
                    this.stats_ = stats;
                }
                onChanged();
            } else {
                this.statsBuilder_.mergeFrom(stats);
            }
            return this;
        }

        public Builder clearStats() {
            if (this.statsBuilder_ == null) {
                this.stats_ = null;
                onChanged();
            } else {
                this.stats_ = null;
                this.statsBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Stats.Builder getStatsBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Stats.Builder) getStatsFieldBuilder().getBuilder();
        }

        public StatsOrBuilder getStatsOrBuilder() {
            if (this.statsBuilder_ != null) {
                return (StatsOrBuilder) this.statsBuilder_.getMessageOrBuilder();
            }
            return this.stats_ == null ? Stats.getDefaultInstance() : this.stats_;
        }

        private SingleFieldBuilderV3<Stats, com.appodeal.ads.api.Stats.Builder, StatsOrBuilder> getStatsFieldBuilder() {
            if (this.statsBuilder_ == null) {
                this.statsBuilder_ = new SingleFieldBuilderV3<>(getStats(), getParentForChildren(), isClean());
                this.stats_ = null;
            }
            return this.statsBuilder_;
        }

        public boolean hasEvent() {
            return (this.eventBuilder_ == null && this.event_ == null) ? false : true;
        }

        public Event getEvent() {
            if (this.eventBuilder_ != null) {
                return (Event) this.eventBuilder_.getMessage();
            }
            return this.event_ == null ? Event.getDefaultInstance() : this.event_;
        }

        public Builder setEvent(Event event) {
            if (this.eventBuilder_ != null) {
                this.eventBuilder_.setMessage(event);
            } else if (event != null) {
                this.event_ = event;
                onChanged();
            } else {
                throw new NullPointerException();
            }
            return this;
        }

        public Builder setEvent(com.appodeal.ads.api.Event.Builder builder) {
            if (this.eventBuilder_ == null) {
                this.event_ = builder.build();
                onChanged();
            } else {
                this.eventBuilder_.setMessage(builder.build());
            }
            return this;
        }

        public Builder mergeEvent(Event event) {
            if (this.eventBuilder_ == null) {
                if (this.event_ != null) {
                    this.event_ = Event.newBuilder(this.event_).mergeFrom(event).buildPartial();
                } else {
                    this.event_ = event;
                }
                onChanged();
            } else {
                this.eventBuilder_.mergeFrom(event);
            }
            return this;
        }

        public Builder clearEvent() {
            if (this.eventBuilder_ == null) {
                this.event_ = null;
                onChanged();
            } else {
                this.event_ = null;
                this.eventBuilder_ = null;
            }
            return this;
        }

        public com.appodeal.ads.api.Event.Builder getEventBuilder() {
            onChanged();
            return (com.appodeal.ads.api.Event.Builder) getEventFieldBuilder().getBuilder();
        }

        public EventOrBuilder getEventOrBuilder() {
            if (this.eventBuilder_ != null) {
                return (EventOrBuilder) this.eventBuilder_.getMessageOrBuilder();
            }
            return this.event_ == null ? Event.getDefaultInstance() : this.event_;
        }

        private SingleFieldBuilderV3<Event, com.appodeal.ads.api.Event.Builder, EventOrBuilder> getEventFieldBuilder() {
            if (this.eventBuilder_ == null) {
                this.eventBuilder_ = new SingleFieldBuilderV3<>(getEvent(), getParentForChildren(), isClean());
                this.event_ = null;
            }
            return this.eventBuilder_;
        }

        public long getTimestamp() {
            return this.timestamp_;
        }

        public Builder setTimestamp(long j) {
            this.timestamp_ = j;
            onChanged();
            return this;
        }

        public Builder clearTimestamp() {
            this.timestamp_ = 0;
            onChanged();
            return this;
        }

        public final Builder setUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.setUnknownFields(unknownFieldSet);
        }

        public final Builder mergeUnknownFields(UnknownFieldSet unknownFieldSet) {
            return (Builder) super.mergeUnknownFields(unknownFieldSet);
        }
    }

    private Request(com.explorestack.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
        this.memoizedIsInitialized = -1;
    }

    private Request() {
        this.memoizedIsInitialized = -1;
        this.impid_ = "";
        this.mainId_ = "";
    }

    /* access modifiers changed from: protected */
    public Object newInstance(UnusedPrivateParameter unusedPrivateParameter) {
        return new Request();
    }

    public final UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    /* JADX WARNING: type inference failed for: r4v0 */
    /* JADX WARNING: type inference failed for: r4v1, types: [com.appodeal.ads.api.App$Builder] */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.appodeal.ads.api.App$Builder] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.appodeal.ads.api.Session$Builder] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.appodeal.ads.api.Session$Builder] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.appodeal.ads.api.Device$Builder] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.appodeal.ads.api.Device$Builder] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.appodeal.ads.api.User$Builder] */
    /* JADX WARNING: type inference failed for: r4v8, types: [com.appodeal.ads.api.User$Builder] */
    /* JADX WARNING: type inference failed for: r4v9, types: [com.appodeal.ads.api.Regs$Builder] */
    /* JADX WARNING: type inference failed for: r4v10, types: [com.appodeal.ads.api.Regs$Builder] */
    /* JADX WARNING: type inference failed for: r4v11, types: [com.appodeal.ads.api.Geo$Builder] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.appodeal.ads.api.Geo$Builder] */
    /* JADX WARNING: type inference failed for: r4v13, types: [com.appodeal.ads.api.Extra$Builder] */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.appodeal.ads.api.Extra$Builder] */
    /* JADX WARNING: type inference failed for: r4v15, types: [com.appodeal.ads.api.Get$Builder] */
    /* JADX WARNING: type inference failed for: r4v16, types: [com.appodeal.ads.api.Get$Builder] */
    /* JADX WARNING: type inference failed for: r4v17, types: [com.appodeal.ads.api.Stats$Builder] */
    /* JADX WARNING: type inference failed for: r4v18, types: [com.appodeal.ads.api.Stats$Builder] */
    /* JADX WARNING: type inference failed for: r4v19, types: [com.appodeal.ads.api.Event$Builder] */
    /* JADX WARNING: type inference failed for: r4v20, types: [com.appodeal.ads.api.Event$Builder] */
    /* JADX WARNING: type inference failed for: r4v21 */
    /* JADX WARNING: type inference failed for: r4v22 */
    /* JADX WARNING: type inference failed for: r4v23 */
    /* JADX WARNING: type inference failed for: r4v24 */
    /* JADX WARNING: type inference failed for: r4v25 */
    /* JADX WARNING: type inference failed for: r4v26 */
    /* JADX WARNING: type inference failed for: r4v27 */
    /* JADX WARNING: type inference failed for: r4v28 */
    /* JADX WARNING: type inference failed for: r4v29 */
    /* JADX WARNING: type inference failed for: r4v30 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r4v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.appodeal.ads.api.Session$Builder, com.appodeal.ads.api.App$Builder, com.appodeal.ads.api.Device$Builder, com.appodeal.ads.api.User$Builder, com.appodeal.ads.api.Regs$Builder, com.appodeal.ads.api.Geo$Builder, com.appodeal.ads.api.Extra$Builder, com.appodeal.ads.api.Get$Builder, com.appodeal.ads.api.Stats$Builder, com.appodeal.ads.api.Event$Builder]
  uses: [?[int, boolean, OBJECT, ARRAY, byte, short, char], com.appodeal.ads.api.App$Builder, com.appodeal.ads.api.Session$Builder, com.appodeal.ads.api.Device$Builder, com.appodeal.ads.api.User$Builder, com.appodeal.ads.api.Regs$Builder, com.appodeal.ads.api.Geo$Builder, com.appodeal.ads.api.Extra$Builder, com.appodeal.ads.api.Get$Builder, com.appodeal.ads.api.Stats$Builder, com.appodeal.ads.api.Event$Builder]
  mth insns count: 188
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 11 */
    private Request(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        this();
        if (extensionRegistryLite != null) {
            com.explorestack.protobuf.UnknownFieldSet.Builder newBuilder = UnknownFieldSet.newBuilder();
            boolean z = false;
            while (!z) {
                try {
                    int readTag = codedInputStream.readTag();
                    ? r4 = 0;
                    switch (readTag) {
                        case 0:
                            z = true;
                            break;
                        case 10:
                            if (this.app_ != null) {
                                r4 = this.app_.toBuilder();
                            }
                            this.app_ = (App) codedInputStream.readMessage(App.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.app_);
                                this.app_ = r4.buildPartial();
                                break;
                            }
                        case 18:
                            if (this.session_ != null) {
                                r4 = this.session_.toBuilder();
                            }
                            this.session_ = (Session) codedInputStream.readMessage(Session.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.session_);
                                this.session_ = r4.buildPartial();
                                break;
                            }
                        case 26:
                            if (this.device_ != null) {
                                r4 = this.device_.toBuilder();
                            }
                            this.device_ = (Device) codedInputStream.readMessage(Device.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.device_);
                                this.device_ = r4.buildPartial();
                                break;
                            }
                        case 34:
                            if (this.user_ != null) {
                                r4 = this.user_.toBuilder();
                            }
                            this.user_ = (User) codedInputStream.readMessage(User.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.user_);
                                this.user_ = r4.buildPartial();
                                break;
                            }
                        case 42:
                            if (this.regs_ != null) {
                                r4 = this.regs_.toBuilder();
                            }
                            this.regs_ = (Regs) codedInputStream.readMessage(Regs.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.regs_);
                                this.regs_ = r4.buildPartial();
                                break;
                            }
                        case 50:
                            if (this.geo_ != null) {
                                r4 = this.geo_.toBuilder();
                            }
                            this.geo_ = (Geo) codedInputStream.readMessage(Geo.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.geo_);
                                this.geo_ = r4.buildPartial();
                                break;
                            }
                        case 58:
                            if (this.ext_ != null) {
                                r4 = this.ext_.toBuilder();
                            }
                            this.ext_ = (Extra) codedInputStream.readMessage(Extra.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.ext_);
                                this.ext_ = r4.buildPartial();
                                break;
                            }
                        case 66:
                            this.impid_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 74:
                            this.mainId_ = codedInputStream.readStringRequireUtf8();
                            break;
                        case 82:
                            if (this.get_ != null) {
                                r4 = this.get_.toBuilder();
                            }
                            this.get_ = (Get) codedInputStream.readMessage(Get.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.get_);
                                this.get_ = r4.buildPartial();
                                break;
                            }
                        case 90:
                            if (this.stats_ != null) {
                                r4 = this.stats_.toBuilder();
                            }
                            this.stats_ = (Stats) codedInputStream.readMessage(Stats.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.stats_);
                                this.stats_ = r4.buildPartial();
                                break;
                            }
                        case 98:
                            if (this.event_ != null) {
                                r4 = this.event_.toBuilder();
                            }
                            this.event_ = (Event) codedInputStream.readMessage(Event.parser(), extensionRegistryLite);
                            if (r4 == 0) {
                                break;
                            } else {
                                r4.mergeFrom(this.event_);
                                this.event_ = r4.buildPartial();
                                break;
                            }
                        case 104:
                            this.timestamp_ = codedInputStream.readInt64();
                            break;
                        default:
                            if (parseUnknownField(codedInputStream, newBuilder, extensionRegistryLite, readTag)) {
                                break;
                            }
                            z = true;
                            break;
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2).setUnfinishedMessage(this);
                } catch (Throwable th) {
                    this.unknownFields = newBuilder.build();
                    makeExtensionsImmutable();
                    throw th;
                }
            }
            this.unknownFields = newBuilder.build();
            makeExtensionsImmutable();
            return;
        }
        throw new NullPointerException();
    }

    public static final Descriptor getDescriptor() {
        return Api.internal_static_com_appodeal_ads_Request_descriptor;
    }

    /* access modifiers changed from: protected */
    public FieldAccessorTable internalGetFieldAccessorTable() {
        return Api.internal_static_com_appodeal_ads_Request_fieldAccessorTable.ensureFieldAccessorsInitialized(Request.class, Builder.class);
    }

    public boolean hasApp() {
        return this.app_ != null;
    }

    public App getApp() {
        return this.app_ == null ? App.getDefaultInstance() : this.app_;
    }

    public AppOrBuilder getAppOrBuilder() {
        return getApp();
    }

    public boolean hasSession() {
        return this.session_ != null;
    }

    public Session getSession() {
        return this.session_ == null ? Session.getDefaultInstance() : this.session_;
    }

    public SessionOrBuilder getSessionOrBuilder() {
        return getSession();
    }

    public boolean hasDevice() {
        return this.device_ != null;
    }

    public Device getDevice() {
        return this.device_ == null ? Device.getDefaultInstance() : this.device_;
    }

    public DeviceOrBuilder getDeviceOrBuilder() {
        return getDevice();
    }

    public boolean hasUser() {
        return this.user_ != null;
    }

    public User getUser() {
        return this.user_ == null ? User.getDefaultInstance() : this.user_;
    }

    public UserOrBuilder getUserOrBuilder() {
        return getUser();
    }

    public boolean hasRegs() {
        return this.regs_ != null;
    }

    public Regs getRegs() {
        return this.regs_ == null ? Regs.getDefaultInstance() : this.regs_;
    }

    public RegsOrBuilder getRegsOrBuilder() {
        return getRegs();
    }

    public boolean hasGeo() {
        return this.geo_ != null;
    }

    public Geo getGeo() {
        return this.geo_ == null ? Geo.getDefaultInstance() : this.geo_;
    }

    public GeoOrBuilder getGeoOrBuilder() {
        return getGeo();
    }

    public boolean hasExt() {
        return this.ext_ != null;
    }

    public Extra getExt() {
        return this.ext_ == null ? Extra.getDefaultInstance() : this.ext_;
    }

    public ExtraOrBuilder getExtOrBuilder() {
        return getExt();
    }

    public String getImpid() {
        Object obj = this.impid_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.impid_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getImpidBytes() {
        Object obj = this.impid_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.impid_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public String getMainId() {
        Object obj = this.mainId_;
        if (obj instanceof String) {
            return (String) obj;
        }
        String stringUtf8 = ((ByteString) obj).toStringUtf8();
        this.mainId_ = stringUtf8;
        return stringUtf8;
    }

    public ByteString getMainIdBytes() {
        Object obj = this.mainId_;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.mainId_ = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean hasGet() {
        return this.get_ != null;
    }

    public Get getGet() {
        return this.get_ == null ? Get.getDefaultInstance() : this.get_;
    }

    public GetOrBuilder getGetOrBuilder() {
        return getGet();
    }

    public boolean hasStats() {
        return this.stats_ != null;
    }

    public Stats getStats() {
        return this.stats_ == null ? Stats.getDefaultInstance() : this.stats_;
    }

    public StatsOrBuilder getStatsOrBuilder() {
        return getStats();
    }

    public boolean hasEvent() {
        return this.event_ != null;
    }

    public Event getEvent() {
        return this.event_ == null ? Event.getDefaultInstance() : this.event_;
    }

    public EventOrBuilder getEventOrBuilder() {
        return getEvent();
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public final boolean isInitialized() {
        byte b = this.memoizedIsInitialized;
        if (b == 1) {
            return true;
        }
        if (b == 0) {
            return false;
        }
        this.memoizedIsInitialized = 1;
        return true;
    }

    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        if (this.app_ != null) {
            codedOutputStream.writeMessage(1, getApp());
        }
        if (this.session_ != null) {
            codedOutputStream.writeMessage(2, getSession());
        }
        if (this.device_ != null) {
            codedOutputStream.writeMessage(3, getDevice());
        }
        if (this.user_ != null) {
            codedOutputStream.writeMessage(4, getUser());
        }
        if (this.regs_ != null) {
            codedOutputStream.writeMessage(5, getRegs());
        }
        if (this.geo_ != null) {
            codedOutputStream.writeMessage(6, getGeo());
        }
        if (this.ext_ != null) {
            codedOutputStream.writeMessage(7, getExt());
        }
        if (!getImpidBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 8, this.impid_);
        }
        if (!getMainIdBytes().isEmpty()) {
            GeneratedMessageV3.writeString(codedOutputStream, 9, this.mainId_);
        }
        if (this.get_ != null) {
            codedOutputStream.writeMessage(10, getGet());
        }
        if (this.stats_ != null) {
            codedOutputStream.writeMessage(11, getStats());
        }
        if (this.event_ != null) {
            codedOutputStream.writeMessage(12, getEvent());
        }
        if (this.timestamp_ != 0) {
            codedOutputStream.writeInt64(13, this.timestamp_);
        }
        this.unknownFields.writeTo(codedOutputStream);
    }

    public int getSerializedSize() {
        int i = this.memoizedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.app_ != null) {
            i2 = 0 + CodedOutputStream.computeMessageSize(1, getApp());
        }
        if (this.session_ != null) {
            i2 += CodedOutputStream.computeMessageSize(2, getSession());
        }
        if (this.device_ != null) {
            i2 += CodedOutputStream.computeMessageSize(3, getDevice());
        }
        if (this.user_ != null) {
            i2 += CodedOutputStream.computeMessageSize(4, getUser());
        }
        if (this.regs_ != null) {
            i2 += CodedOutputStream.computeMessageSize(5, getRegs());
        }
        if (this.geo_ != null) {
            i2 += CodedOutputStream.computeMessageSize(6, getGeo());
        }
        if (this.ext_ != null) {
            i2 += CodedOutputStream.computeMessageSize(7, getExt());
        }
        if (!getImpidBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(8, this.impid_);
        }
        if (!getMainIdBytes().isEmpty()) {
            i2 += GeneratedMessageV3.computeStringSize(9, this.mainId_);
        }
        if (this.get_ != null) {
            i2 += CodedOutputStream.computeMessageSize(10, getGet());
        }
        if (this.stats_ != null) {
            i2 += CodedOutputStream.computeMessageSize(11, getStats());
        }
        if (this.event_ != null) {
            i2 += CodedOutputStream.computeMessageSize(12, getEvent());
        }
        if (this.timestamp_ != 0) {
            i2 += CodedOutputStream.computeInt64Size(13, this.timestamp_);
        }
        int serializedSize = i2 + this.unknownFields.getSerializedSize();
        this.memoizedSize = serializedSize;
        return serializedSize;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Request)) {
            return super.equals(obj);
        }
        Request request = (Request) obj;
        if (hasApp() != request.hasApp()) {
            return false;
        }
        if ((hasApp() && !getApp().equals(request.getApp())) || hasSession() != request.hasSession()) {
            return false;
        }
        if ((hasSession() && !getSession().equals(request.getSession())) || hasDevice() != request.hasDevice()) {
            return false;
        }
        if ((hasDevice() && !getDevice().equals(request.getDevice())) || hasUser() != request.hasUser()) {
            return false;
        }
        if ((hasUser() && !getUser().equals(request.getUser())) || hasRegs() != request.hasRegs()) {
            return false;
        }
        if ((hasRegs() && !getRegs().equals(request.getRegs())) || hasGeo() != request.hasGeo()) {
            return false;
        }
        if ((hasGeo() && !getGeo().equals(request.getGeo())) || hasExt() != request.hasExt()) {
            return false;
        }
        if ((hasExt() && !getExt().equals(request.getExt())) || !getImpid().equals(request.getImpid()) || !getMainId().equals(request.getMainId()) || hasGet() != request.hasGet()) {
            return false;
        }
        if ((hasGet() && !getGet().equals(request.getGet())) || hasStats() != request.hasStats()) {
            return false;
        }
        if ((hasStats() && !getStats().equals(request.getStats())) || hasEvent() != request.hasEvent()) {
            return false;
        }
        if ((!hasEvent() || getEvent().equals(request.getEvent())) && getTimestamp() == request.getTimestamp() && this.unknownFields.equals(request.unknownFields)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.memoizedHashCode != 0) {
            return this.memoizedHashCode;
        }
        int hashCode = 779 + getDescriptor().hashCode();
        if (hasApp()) {
            hashCode = (((hashCode * 37) + 1) * 53) + getApp().hashCode();
        }
        if (hasSession()) {
            hashCode = (((hashCode * 37) + 2) * 53) + getSession().hashCode();
        }
        if (hasDevice()) {
            hashCode = (((hashCode * 37) + 3) * 53) + getDevice().hashCode();
        }
        if (hasUser()) {
            hashCode = (((hashCode * 37) + 4) * 53) + getUser().hashCode();
        }
        if (hasRegs()) {
            hashCode = (((hashCode * 37) + 5) * 53) + getRegs().hashCode();
        }
        if (hasGeo()) {
            hashCode = (((hashCode * 37) + 6) * 53) + getGeo().hashCode();
        }
        if (hasExt()) {
            hashCode = (((hashCode * 37) + 7) * 53) + getExt().hashCode();
        }
        int hashCode2 = (((((((hashCode * 37) + 8) * 53) + getImpid().hashCode()) * 37) + 9) * 53) + getMainId().hashCode();
        if (hasGet()) {
            hashCode2 = (((hashCode2 * 37) + 10) * 53) + getGet().hashCode();
        }
        if (hasStats()) {
            hashCode2 = (((hashCode2 * 37) + 11) * 53) + getStats().hashCode();
        }
        if (hasEvent()) {
            hashCode2 = (((hashCode2 * 37) + 12) * 53) + getEvent().hashCode();
        }
        int hashLong = (((((hashCode2 * 37) + 13) * 53) + Internal.hashLong(getTimestamp())) * 29) + this.unknownFields.hashCode();
        this.memoizedHashCode = hashLong;
        return hashLong;
    }

    public static Request parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Request) PARSER.parseFrom(byteBuffer);
    }

    public static Request parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Request) PARSER.parseFrom(byteBuffer, extensionRegistryLite);
    }

    public static Request parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Request) PARSER.parseFrom(byteString);
    }

    public static Request parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Request) PARSER.parseFrom(byteString, extensionRegistryLite);
    }

    public static Request parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Request) PARSER.parseFrom(bArr);
    }

    public static Request parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Request) PARSER.parseFrom(bArr, extensionRegistryLite);
    }

    public static Request parseFrom(InputStream inputStream) throws IOException {
        return (Request) GeneratedMessageV3.parseWithIOException(PARSER, inputStream);
    }

    public static Request parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Request) GeneratedMessageV3.parseWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Request parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Request) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream);
    }

    public static Request parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Request) GeneratedMessageV3.parseDelimitedWithIOException(PARSER, inputStream, extensionRegistryLite);
    }

    public static Request parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Request) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream);
    }

    public static Request parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Request) GeneratedMessageV3.parseWithIOException(PARSER, codedInputStream, extensionRegistryLite);
    }

    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(Request request) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(request);
    }

    public Builder toBuilder() {
        if (this == DEFAULT_INSTANCE) {
            return new Builder();
        }
        return new Builder().mergeFrom(this);
    }

    /* access modifiers changed from: protected */
    public Builder newBuilderForType(BuilderParent builderParent) {
        return new Builder(builderParent);
    }

    public static Request getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Request> parser() {
        return PARSER;
    }

    public Parser<Request> getParserForType() {
        return PARSER;
    }

    public Request getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}
