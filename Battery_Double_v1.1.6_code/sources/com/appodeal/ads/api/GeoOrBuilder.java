package com.appodeal.ads.api;

import com.appodeal.ads.api.Geo.LocationType;
import com.explorestack.protobuf.MessageOrBuilder;

public interface GeoOrBuilder extends MessageOrBuilder {
    float getLat();

    long getLocalTime();

    float getLon();

    LocationType getLt();

    int getLtValue();

    int getUtcoffset();
}
