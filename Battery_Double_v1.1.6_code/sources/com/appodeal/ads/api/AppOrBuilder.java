package com.appodeal.ads.api;

import com.explorestack.protobuf.ByteString;
import com.explorestack.protobuf.MessageOrBuilder;

public interface AppOrBuilder extends MessageOrBuilder {
    String getAppKey();

    ByteString getAppKeyBytes();

    long getAppUptime();

    String getBundle();

    ByteString getBundleBytes();

    String getFramework();

    ByteString getFrameworkBytes();

    String getFrameworkVersion();

    ByteString getFrameworkVersionBytes();

    long getInstallTime();

    String getInstaller();

    ByteString getInstallerBytes();

    @Deprecated
    boolean getMultidex();

    String getPluginVersion();

    ByteString getPluginVersionBytes();

    String getSdk();

    ByteString getSdkBytes();

    String getVer();

    ByteString getVerBytes();

    int getVersionCode();
}
