package com.appodeal.ads;

public class AppodealNetworks {
    public static final String A4G = "a4g";
    public static final String ADCOLONY = "adcolony";
    public static final String ADMOB = "admob";
    public static final String AMAZON_ADS = "amazon_ads";
    public static final String APPLOVIN = "applovin";
    public static final String APPODEAL = "appodeal";
    public static final String APPODEALX = "appodealx";
    public static final String CHARTBOOST = "chartboost";
    public static final String FACEBOOK = "facebook";
    public static final String FLURRY = "flurry";
    public static final String FYBER = "fyber";
    public static final String INMOBI = "inmobi";
    public static final String INNER_ACTIVE = "inner-active";
    public static final String IRONSOURCE = "ironsource";
    public static final String MINTEGRAL = "mintegral";
    public static final String MOPUB = "mopub";
    public static final String MRAID = "mraid";
    public static final String MY_TARGET = "my_target";
    public static final String NAST = "nast";
    public static final String OGURY = "ogury";
    public static final String SMAATO = "smaato";
    public static final String STARTAPP = "startapp";
    public static final String TAPJOY = "tapjoy";
    public static final String UNITY_ADS = "unity_ads";
    public static final String VAST = "vast";
    public static final String VUNGLE = "vungle";
    public static final String YANDEX = "yandex";
}
