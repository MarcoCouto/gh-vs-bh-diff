package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.appodeal.ads.m;
import com.appodeal.ads.unified.UnifiedViewAd;
import com.appodeal.ads.unified.UnifiedViewAdCallback;
import com.appodeal.ads.unified.UnifiedViewAdParams;

abstract class bu<AdRequestType extends m, UnifiedAdType extends UnifiedViewAd, UnifiedAdParamsType extends UnifiedViewAdParams, UnifiedAdCallbackType extends UnifiedViewAdCallback> extends i<AdRequestType, UnifiedAdType, UnifiedAdParamsType, UnifiedAdCallbackType> {
    @Nullable
    private View e;

    bu(@NonNull AdRequestType adrequesttype, @NonNull AdNetwork adNetwork, @NonNull bn bnVar, int i) {
        super(adrequesttype, adNetwork, bnVar, i);
    }

    /* access modifiers changed from: protected */
    public void a(@Nullable View view) {
        this.e = view;
    }

    /* access modifiers changed from: protected */
    public abstract int b(Context context);

    /* access modifiers changed from: protected */
    public abstract int c(Context context);

    /* access modifiers changed from: protected */
    public void r() {
        super.r();
        this.e = null;
    }

    @Nullable
    public View v() {
        return this.e;
    }
}
