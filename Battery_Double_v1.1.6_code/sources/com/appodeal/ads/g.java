package com.appodeal.ads;

import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONObject;

class g implements AdNetworkMediationParams {
    @NonNull
    m a;
    @NonNull
    i b;
    @NonNull
    private RestrictedData c;

    g(@NonNull m mVar, @NonNull i iVar, @NonNull RestrictedData restrictedData) {
        this.a = mVar;
        this.b = iVar;
        this.c = restrictedData;
    }

    public String getAppName() {
        return w.a;
    }

    public String getImpressionId() {
        return this.a.E();
    }

    @NonNull
    public List<JSONObject> getParallelBiddingAdUnitList() {
        return this.a.c();
    }

    @NonNull
    public RestrictedData getRestrictedData() {
        return this.c;
    }

    public String getStoreUrl() {
        return w.b;
    }

    public boolean isCoronaApp() {
        return w.a();
    }

    public boolean isTestMode() {
        return z.b;
    }
}
