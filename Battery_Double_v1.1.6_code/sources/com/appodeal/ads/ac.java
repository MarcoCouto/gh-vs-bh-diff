package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;

class ac extends bu<ad, UnifiedBanner, UnifiedBannerParams, UnifiedBannerCallback> {
    @Deprecated
    int e;
    /* access modifiers changed from: private */
    public int f = -1;

    private class a extends UnifiedBannerCallback {
        private a() {
        }

        public void onAdClicked() {
            aa.c().a(ac.this.a(), ac.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            aa.c().a(ac.this.a(), ac.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdExpired() {
            aa.c().i(ac.this.a(), ac.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            ac.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            aa.c().b(ac.this.a(), ac.this, loadingError);
        }

        public void onAdLoaded(View view, int i, int i2) {
            ac.this.a(view);
            ac.this.e = i2;
            ac.this.f = view.getResources().getConfiguration().orientation;
            aa.c().b(ac.this.a(), ac.this);
        }

        public void onAdShowFailed() {
            aa.c().a(ac.this.a(), ac.this, null, LoadingError.ShowFailed);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((ad) ac.this.a()).a((AdUnit) ac.this, str, obj);
        }
    }

    private class b implements UnifiedBannerParams {
        private b() {
        }

        public int getMaxHeight(@NonNull Context context) {
            return aa.d();
        }

        public int getMaxWidth(@NonNull Context context) {
            return aa.e();
        }

        public boolean needLeaderBoard(@NonNull Context context) {
            return aa.f();
        }

        public String obtainPlacementId() {
            return aa.b().u();
        }

        public String obtainSegmentId() {
            return aa.b().s();
        }

        public boolean useSmartBanners(@NonNull Context context) {
            return aa.b;
        }
    }

    public ac(@NonNull ad adVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
        super(adVar, adNetwork, bnVar, 5000);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull Configuration configuration) {
        UnifiedBanner unifiedBanner = (UnifiedBanner) n();
        return (unifiedBanner == null || !unifiedBanner.isRefreshOnRotate() || this.f == -1 || this.f == configuration.orientation) ? false : true;
    }

    /* access modifiers changed from: protected */
    public int b(Context context) {
        if (aa.b && b().isSupportSmartBanners()) {
            return -1;
        }
        return Math.round(bq.h(context) * (aa.f() ? 728.0f : 320.0f));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedBanner a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createBanner();
    }

    /* access modifiers changed from: protected */
    public int c(Context context) {
        return Math.round(((float) this.e) * bq.h(context));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedBannerParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedBannerCallback o() {
        return new a();
    }

    public int u() {
        return this.e;
    }
}
