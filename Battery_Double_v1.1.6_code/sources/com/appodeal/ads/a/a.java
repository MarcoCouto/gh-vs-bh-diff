package com.appodeal.ads.a;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.bq;
import com.appodeal.ads.d;
import com.appodeal.ads.e;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.s;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

public class a implements d {
    private String a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public volatile long c;

    /* renamed from: com.appodeal.ads.a.a$a reason: collision with other inner class name */
    public static class C0013a extends e {
        @NonNull
        public String a() {
            return "criteo";
        }

        @NonNull
        public d c() {
            return new a();
        }
    }

    /* access modifiers changed from: private */
    public URL a(Context context, String str, RestrictedData restrictedData) throws Exception {
        return new URL(String.format(Locale.ENGLISH, "https://gum.criteo.com/appevent/v1/%s?gaid=%s&appId=%s&eventType=%s&limitedAdTracking=%d", new Object[]{this.a, restrictedData.getIfa(), context.getPackageName(), str, Integer.valueOf(restrictedData.isLimitAdTrackingEnabled() ? 1 : 0)}));
    }

    /* access modifiers changed from: private */
    public JSONObject a(InputStream inputStream) {
        BufferedReader bufferedReader;
        try {
            StringBuilder sb = new StringBuilder(inputStream.available());
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append(10);
                } catch (Exception e) {
                    e = e;
                    try {
                        Log.log(e);
                        bq.a((Closeable) bufferedReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        bq.a((Closeable) bufferedReader);
                        throw th;
                    }
                }
            }
            if (sb.length() > 0) {
                sb.setLength(sb.length() - 1);
            }
            JSONObject jSONObject = new JSONObject(sb.toString());
            bq.a((Closeable) bufferedReader);
            return jSONObject;
        } catch (Exception e2) {
            e = e2;
            bufferedReader = null;
            Log.log(e);
            bq.a((Closeable) bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            bq.a((Closeable) bufferedReader);
            throw th;
        }
    }

    private boolean a() {
        return !TextUtils.isEmpty(this.a) && !TextUtils.isEmpty(this.b) && ((this.c > 0 ? 1 : (this.c == 0 ? 0 : -1)) == 0 || (System.currentTimeMillis() > this.c ? 1 : (System.currentTimeMillis() == this.c ? 0 : -1)) > 0);
    }

    private void b(@NonNull final Context context, final String str, final RestrictedData restrictedData) {
        if (a()) {
            s.a.execute(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:49:0x00c8 A[SYNTHETIC, Splitter:B:49:0x00c8] */
                /* JADX WARNING: Removed duplicated region for block: B:56:0x00d7 A[SYNTHETIC, Splitter:B:56:0x00d7] */
                public void run() {
                    InputStream inputStream;
                    HttpsURLConnection httpsURLConnection;
                    Throwable e;
                    InputStream inputStream2 = null;
                    try {
                        httpsURLConnection = (HttpsURLConnection) a.this.a(context, str, restrictedData).openConnection();
                        try {
                            httpsURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                            httpsURLConnection.setRequestProperty("User-Agent", a.this.b);
                            httpsURLConnection.setConnectTimeout(10000);
                            httpsURLConnection.setReadTimeout(10000);
                            int responseCode = httpsURLConnection.getResponseCode();
                            if (responseCode == 200) {
                                long j = 0;
                                InputStream inputStream3 = httpsURLConnection.getInputStream();
                                try {
                                    JSONObject a2 = a.this.a(inputStream3);
                                    if (a2 != null && a2.has("throttleSec")) {
                                        j = System.currentTimeMillis() + ((long) (a2.getInt("throttleSec") * 1000));
                                    }
                                    a.this.c = j;
                                    inputStream2 = inputStream3;
                                } catch (Exception e2) {
                                    e = e2;
                                    inputStream = inputStream3;
                                    try {
                                        Log.log(e);
                                        if (httpsURLConnection != null) {
                                        }
                                        bq.a((Closeable) inputStream);
                                    } catch (Throwable th) {
                                        th = th;
                                        if (httpsURLConnection != null) {
                                            try {
                                                httpsURLConnection.disconnect();
                                            } catch (Exception e3) {
                                                Log.log(e3);
                                            }
                                        }
                                        bq.a((Closeable) inputStream);
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    inputStream = inputStream3;
                                    if (httpsURLConnection != null) {
                                    }
                                    bq.a((Closeable) inputStream);
                                    throw th;
                                }
                            } else if (responseCode == 400) {
                                inputStream = httpsURLConnection.getErrorStream();
                                try {
                                    JSONObject a3 = a.this.a(inputStream);
                                    if (a3 != null && a3.has("error")) {
                                        Log.log(LogConstants.KEY_NETWORK, "Error", String.format(Locale.ENGLISH, "%s - %s", new Object[]{"Criteo", a3.getString("error")}));
                                    }
                                    inputStream2 = inputStream;
                                } catch (Exception e4) {
                                    e = e4;
                                    Log.log(e);
                                    if (httpsURLConnection != null) {
                                        try {
                                            httpsURLConnection.disconnect();
                                        } catch (Exception e5) {
                                            Log.log(e5);
                                        }
                                    }
                                    bq.a((Closeable) inputStream);
                                }
                            }
                            if (httpsURLConnection != null) {
                                try {
                                    httpsURLConnection.disconnect();
                                } catch (Exception e6) {
                                    Log.log(e6);
                                }
                            }
                            bq.a((Closeable) inputStream2);
                        } catch (Exception e7) {
                            Throwable th3 = e7;
                            inputStream = null;
                            e = th3;
                            Log.log(e);
                            if (httpsURLConnection != null) {
                            }
                            bq.a((Closeable) inputStream);
                        } catch (Throwable th4) {
                            Throwable th5 = th4;
                            inputStream = null;
                            th = th5;
                            if (httpsURLConnection != null) {
                            }
                            bq.a((Closeable) inputStream);
                            throw th;
                        }
                    } catch (Exception e8) {
                        inputStream = null;
                        e = e8;
                        httpsURLConnection = null;
                        Log.log(e);
                        if (httpsURLConnection != null) {
                        }
                        bq.a((Closeable) inputStream);
                    } catch (Throwable th6) {
                        inputStream = null;
                        th = th6;
                        httpsURLConnection = null;
                        if (httpsURLConnection != null) {
                        }
                        bq.a((Closeable) inputStream);
                        throw th;
                    }
                }
            });
        }
    }

    public void a(@NonNull Context context, @NonNull RestrictedData restrictedData) {
        b(context, "Active", restrictedData);
    }

    public void a(@NonNull Context context, @NonNull JSONObject jSONObject, @NonNull RestrictedData restrictedData) {
        try {
            this.a = jSONObject.getString("sender_id");
            this.b = restrictedData.getHttpAgent(context);
        } catch (Exception e) {
            Log.log(e);
        }
        b(context, "Launch", restrictedData);
    }

    public void b(@NonNull Context context, @NonNull RestrictedData restrictedData) {
        b(context, "Inactive", restrictedData);
    }
}
