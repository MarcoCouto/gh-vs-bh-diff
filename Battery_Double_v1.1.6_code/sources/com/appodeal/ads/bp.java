package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.b.a;
import com.ironsource.mediationsdk.IronSourceSegment;
import org.json.JSONObject;

class bp implements UserData {
    @VisibleForTesting
    static volatile bp a;
    @VisibleForTesting
    String b;
    @VisibleForTesting
    Gender c;
    @VisibleForTesting
    Integer d;
    @VisibleForTesting
    String e;
    @VisibleForTesting
    String f;
    @VisibleForTesting
    String g;
    @VisibleForTesting
    String h;
    @VisibleForTesting
    Float i;
    @VisibleForTesting
    Float j;
    @VisibleForTesting
    String k;
    @VisibleForTesting
    String l;

    bp() {
    }

    public static bp a() {
        if (a == null) {
            synchronized (bp.class) {
                if (a == null) {
                    a = new bp();
                }
            }
        }
        return a;
    }

    static void a(@Nullable JSONObject jSONObject) {
        if (jSONObject != null) {
            bp a2 = a();
            JSONObject optJSONObject = jSONObject.optJSONObject("user_settings");
            if (optJSONObject != null) {
                if (a2.c == null && optJSONObject.has("gender")) {
                    int optInt = optJSONObject.optInt("gender", -1);
                    if (optInt > -1) {
                        Gender fromInteger = Gender.fromInteger(Integer.valueOf(optInt));
                        if (fromInteger != null) {
                            a2.c = fromInteger;
                        }
                    }
                }
                if (a2.d == null && optJSONObject.has(IronSourceSegment.AGE)) {
                    int optInt2 = optJSONObject.optInt(IronSourceSegment.AGE, -1);
                    if (optInt2 > -1) {
                        a2.d = Integer.valueOf(optInt2);
                    }
                }
                if (optJSONObject.has("lat")) {
                    float optDouble = (float) optJSONObject.optDouble("lat", -1.0d);
                    if (optDouble > -1.0f) {
                        a2.i = Float.valueOf(optDouble);
                    }
                }
                if (optJSONObject.has("lon")) {
                    float optDouble2 = (float) optJSONObject.optDouble("lon", -1.0d);
                    if (optDouble2 > -1.0f) {
                        a2.j = Float.valueOf(optDouble2);
                    }
                }
                a2.k = bq.a(optJSONObject, "city", a2.k);
                a2.l = bq.a(optJSONObject, "zip", a2.l);
            }
            a2.e = bq.a(jSONObject, "ip", a2.e);
            a2.f = bq.a(jSONObject, "ipv6", a2.f);
            a2.g = bq.a(jSONObject, "country_id", a2.g);
            a2.h = bq.a(jSONObject, "address", a2.h);
        }
    }

    public String getAddress() {
        return this.h;
    }

    public Integer getAge() {
        return this.d;
    }

    public String getCity() {
        return this.k;
    }

    public String getCountryId() {
        return this.g;
    }

    public Gender getGender() {
        return this.c;
    }

    public String getIp() {
        return this.e;
    }

    public String getIpv6() {
        return this.f;
    }

    public Float getLat() {
        return this.i;
    }

    public Float getLon() {
        return this.j;
    }

    public String getUserId() {
        return this.b;
    }

    public String getZip() {
        return this.l;
    }

    public UserSettings setAge(int i2) {
        Log.log("UserSettings", LogConstants.EVENT_SET, String.format("age: %s", new Object[]{Integer.valueOf(i2)}), LogLevel.verbose);
        this.d = Integer.valueOf(i2);
        return this;
    }

    public UserSettings setGender(@NonNull Gender gender) {
        if (gender == null) {
            Log.log(new a("Unable to set gender to null"));
            return this;
        }
        Log.log("UserSettings", LogConstants.EVENT_SET, String.format("gender: %s", new Object[]{gender.toString()}), LogLevel.verbose);
        this.c = gender;
        return this;
    }

    public UserSettings setUserId(@NonNull String str) {
        if (str == null) {
            Log.log(new a("Unable to set user id to null"));
            return this;
        }
        Log.log("UserSettings", LogConstants.EVENT_SET, String.format("userId: %s", new Object[]{str}), LogLevel.verbose);
        this.b = str;
        return this;
    }
}
