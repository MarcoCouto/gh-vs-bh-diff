package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.d;
import com.appodeal.ads.b.e;
import com.appodeal.ads.c.b;
import com.appodeal.ads.i;
import com.appodeal.ads.m;
import com.appodeal.ads.n;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.aa;
import com.appodeal.ads.utils.app.AppState;
import com.appodeal.ads.utils.c;
import com.appodeal.ads.utils.o;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

abstract class p<AdObjectType extends i, AdRequestType extends m<AdObjectType>, RequestParamsType extends n> {
    static final /* synthetic */ boolean g = (!p.class.desiredAssertionStatus());
    private boolean A;
    private boolean B;
    private int C;
    private final com.appodeal.ads.utils.app.a D;
    @VisibleForTesting
    long a;
    @VisibleForTesting
    boolean b;
    @Nullable
    @VisibleForTesting
    public AdRequestType c;
    @Nullable
    @VisibleForTesting
    public AdRequestType d;
    float e;
    float f;
    private final BlockingQueue<Runnable> h;
    private ThreadPoolExecutor i;
    /* access modifiers changed from: private */
    public final q<AdObjectType, AdRequestType, ?> j;
    /* access modifiers changed from: private */
    public final h k;
    /* access modifiers changed from: private */
    public final int l;
    /* access modifiers changed from: private */
    public final int m;
    /* access modifiers changed from: private */
    public final int n;
    private final List<AdRequestType> o;
    /* access modifiers changed from: private */
    public boolean p;
    private boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    private boolean s;
    /* access modifiers changed from: private */
    public d t;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    @Nullable
    public com.appodeal.ads.c.a v;
    /* access modifiers changed from: private */
    public Integer w;
    /* access modifiers changed from: private */
    public int x;
    /* access modifiers changed from: private */
    public String y;
    /* access modifiers changed from: private */
    public boolean z;

    private class a implements com.appodeal.ads.aq.a<AdRequestType> {
        private String b;

        a(String str) {
            this.b = str;
        }

        public void a(AdRequestType adrequesttype) {
            p.this.j.a(adrequesttype, null, null, LoadingError.RequestError);
        }

        public void a(JSONObject jSONObject, AdRequestType adrequesttype, String str) {
            try {
                if (!p.this.p && !jSONObject.optBoolean(this.b)) {
                    if (!com.appodeal.ads.b.i.a().b().a(p.this.l)) {
                        if (jSONObject.has("ads")) {
                            if (jSONObject.has("main_id")) {
                                p.this.a = System.currentTimeMillis();
                                p.this.x = jSONObject.optInt("afd", 0);
                                if (jSONObject.has("main_id")) {
                                    p.this.y = jSONObject.getString("main_id");
                                }
                                if (jSONObject.has("rri")) {
                                    p.this.z = jSONObject.optBoolean("rri");
                                }
                                if (jSONObject.has("waterfall_cache_timeout")) {
                                    p.this.w = Integer.valueOf(jSONObject.getInt("waterfall_cache_timeout"));
                                }
                                p.this.a(jSONObject);
                                z.a(jSONObject);
                                p.this.v = new b(jSONObject, p.this.m);
                                p.this.v.a(null);
                                adrequesttype.a(p.this.v);
                                adrequesttype.a(p.this.y);
                                adrequesttype.a(Long.valueOf(com.appodeal.ads.b.i.a().c()));
                                if (!adrequesttype.a()) {
                                    p.this.f(adrequesttype);
                                } else if (!adrequesttype.b() || Appodeal.g == null) {
                                    if (Appodeal.h != null) {
                                        Appodeal.h.a(p.this.n);
                                    }
                                    AdNetwork c = p.this.k.c("debug");
                                    if (c != null) {
                                        c.initialize(Appodeal.e, new s(), new g(adrequesttype, null, bg.a), new NetworkInitializationListener() {
                                            public void onInitializationFailed(LoadingError loadingError) {
                                            }

                                            public void onInitializationFinished(@Nullable Object obj) {
                                            }
                                        });
                                    }
                                } else {
                                    Appodeal.g.b();
                                }
                                return;
                            }
                        }
                        if (jSONObject.has("message")) {
                            p.this.a(LogConstants.EVENT_REQUEST_FAILED, jSONObject.getString("message"));
                        }
                        p.this.j.a(adrequesttype, null, null, LoadingError.RequestError);
                        return;
                    }
                }
                p.this.p = true;
                p.this.a(LogConstants.EVENT_REQUEST_FAILED, "disabled");
                Appodeal.b();
            } catch (Exception e) {
                Log.log(e);
                p.this.j.a(adrequesttype, null, null, LoadingError.InternalError);
            }
        }
    }

    p(q<AdObjectType, AdRequestType, ?> qVar, d dVar, int i2) {
        this(qVar, dVar, i2, i2, i2);
    }

    p(q<AdObjectType, AdRequestType, ?> qVar, d dVar, int i2, int i3, int i4) {
        this.h = new LinkedBlockingQueue();
        H();
        this.o = new ArrayList();
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = true;
        this.a = 0;
        this.w = null;
        this.x = 0;
        this.z = false;
        this.B = false;
        this.b = false;
        this.e = 1.2f;
        this.f = 2.0f;
        this.C = 5000;
        this.D = new aa() {
            public void a(@Nullable Activity activity, @NonNull AppState appState) {
                p.this.a(activity, appState);
            }

            public void a(Configuration configuration) {
                p.this.a(Appodeal.e, AppState.ConfChanged);
            }
        };
        this.j = qVar;
        this.t = dVar;
        this.l = i2;
        this.m = i3;
        this.n = i4;
        this.k = h.a(i3);
        this.j.a(this);
        com.appodeal.ads.b.i.a((com.appodeal.ads.b.i.a) new com.appodeal.ads.b.i.a() {
            public void a() {
                p.this.r = true;
            }
        });
        e.a((com.appodeal.ads.b.e.a) new com.appodeal.ads.b.e.a() {
            public d a() {
                return p.this.t;
            }

            public void a(d dVar) {
                p.this.t = dVar;
                p.this.u = null;
            }

            public String b() {
                return p.this.u;
            }
        });
    }

    private void H() {
        int availableProcessors = Runtime.getRuntime().availableProcessors() * 2;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(availableProcessors, availableProcessors, 0, TimeUnit.MICROSECONDS, this.h);
        this.i = threadPoolExecutor;
    }

    private j<AdRequestType, ? extends AdObjectType> a(AdRequestType adrequesttype, AdObjectType adobjecttype, int i2) {
        final AdObjectType adobjecttype2 = adobjecttype;
        final AdRequestType adrequesttype2 = adrequesttype;
        AnonymousClass5 r0 = new j(adrequesttype, adobjecttype, i2) {
            /* access modifiers changed from: 0000 */
            public void a(@Nullable LoadingError loadingError) {
                p.this.j.b(adrequesttype2, adobjecttype2, loadingError);
            }

            /* access modifiers changed from: 0000 */
            public void b() {
                if (Appodeal.h != null) {
                    Appodeal.h.a(p.this.n, adobjecttype2.d(), adobjecttype2.getId());
                }
                p.this.j.a(adrequesttype2, adobjecttype2);
            }
        };
        return r0;
    }

    /* access modifiers changed from: private */
    public void a(@Nullable Activity activity, @NonNull AppState appState) {
        a(activity, appState, (AdRequestType) z());
        a(activity, appState, (AdRequestType) y());
    }

    private void a(@Nullable Activity activity, @NonNull AppState appState, @Nullable AdObjectType adobjecttype, boolean z2) {
        if (adobjecttype != null) {
            adobjecttype.a(activity, appState, z2);
        }
    }

    private void a(@Nullable Activity activity, @NonNull AppState appState, @Nullable AdRequestType adrequesttype) {
        if (adrequesttype != null) {
            boolean b2 = c.b(activity);
            a(activity, appState, (AdObjectType) adrequesttype.B(), b2);
            for (Entry value : adrequesttype.F().entrySet()) {
                a(activity, appState, (AdObjectType) (i) value.getValue(), b2);
            }
            for (i a2 : adrequesttype.H()) {
                a(activity, appState, (AdObjectType) a2, b2);
            }
        }
    }

    private void a(AdRequestType adrequesttype, Runnable runnable, boolean z2) {
        if (z2) {
            this.i.submit(runnable);
            if (adrequesttype.N() > 0) {
                f(adrequesttype);
                return;
            }
            return;
        }
        bq.a(runnable);
    }

    /* access modifiers changed from: private */
    public void f(AdRequestType adrequesttype) {
        if (a(adrequesttype)) {
            if (Appodeal.h != null) {
                Appodeal.h.a(this.n);
            }
            a(adrequesttype, 0, true, false);
        } else if (adrequesttype.z()) {
            if (Appodeal.h != null) {
                Appodeal.h.a(this.n);
            }
            a(adrequesttype, 0, false, false);
        } else {
            this.j.a(adrequesttype, null, null, LoadingError.NoFill);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public AdRequestType A() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public AdRequestType B() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public int C() {
        return this.C;
    }

    public int D() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public boolean E() {
        return this.z;
    }

    /* access modifiers changed from: 0000 */
    public boolean F() {
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public void G() {
        float f2;
        float f3;
        if (p() > Utils.DOUBLE_EPSILON) {
            f2 = (float) this.C;
            f3 = this.e;
        } else {
            f2 = (float) this.C;
            f3 = this.f;
        }
        this.C = (int) (f2 * f3);
        if (this.C >= 100000) {
            this.C = DefaultOggSeeker.MATCH_BYTE_RANGE;
        }
    }

    /* access modifiers changed from: protected */
    public int a(AdRequestType adrequesttype, AdObjectType adobjecttype, boolean z2) {
        return 1;
    }

    /* access modifiers changed from: protected */
    public abstract AdObjectType a(@NonNull AdRequestType adrequesttype, @NonNull AdNetwork adNetwork, @NonNull bn bnVar);

    /* access modifiers changed from: 0000 */
    @Nullable
    public AdRequestType a(int i2) {
        if (this.o.size() <= i2 || i2 == -1) {
            return null;
        }
        return (m) this.o.get(i2);
    }

    /* access modifiers changed from: protected */
    public abstract AdRequestType a(RequestParamsType requestparamstype);

    public q<AdObjectType, AdRequestType, ?> a() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j2) {
        this.a = j2;
    }

    public abstract void a(Activity activity);

    public synchronized void a(Context context) {
        if (!this.q) {
            try {
                com.appodeal.ads.utils.app.b.All.a(this.D);
                this.k.a(context);
                this.q = true;
                b(context);
                Log.log(bq.b(m()), LogConstants.EVENT_INITIALIZE, "done");
            } catch (Exception e2) {
                Log.log(e2);
            }
        } else {
            return;
        }
        return;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, RequestParamsType requestparamstype) {
        a(LogConstants.EVENT_REQUEST_FAILED, String.format("paused: %s, disabled: %s, disabled by segment: %s", new Object[]{Boolean.valueOf(Appodeal.d), Boolean.valueOf(j()), Boolean.valueOf(com.appodeal.ads.b.i.a().b().a(this.l))}));
        Appodeal.b();
    }

    /* access modifiers changed from: 0000 */
    public void a(d dVar) {
        this.t = dVar;
    }

    public void a(@Nullable final AdRequestType adrequesttype, int i2, boolean z2, boolean z3) {
        q<AdObjectType, AdRequestType, ?> qVar;
        LoadingError loadingError;
        if (adrequesttype != null && !a(adrequesttype, i2)) {
            JSONObject a2 = adrequesttype.a(i2, z2, z3);
            bn a3 = s.a(a2, z2);
            if (TextUtils.isEmpty(a3.getId())) {
                this.j.b(adrequesttype, null, LoadingError.IncorrectAdunit);
                return;
            }
            boolean z4 = true;
            adrequesttype.a(this, true);
            adrequesttype.a(a3);
            try {
                if (i()) {
                    i B2 = adrequesttype.B();
                    if (B2 != null && Double.compare(B2.getEcpm(), a3.getEcpm()) >= 0) {
                        a(LogConstants.EVENT_LOAD_SKIPPED, (AdUnit) a3, (LoadingError) null);
                        adrequesttype.b(B2);
                        adrequesttype.b(a3);
                        B2.a(false);
                        this.j.b(adrequesttype, B2);
                        return;
                    }
                }
                JSONArray optJSONArray = a2.optJSONArray("target_placements");
                if (optJSONArray != null && optJSONArray.length() > 0) {
                    int i3 = 0;
                    for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
                        if (adrequesttype.F().containsKey(optJSONArray.optString(i4))) {
                            i3++;
                        }
                    }
                    if (i3 == optJSONArray.length()) {
                        f(adrequesttype);
                        return;
                    }
                }
                AdNetwork c2 = this.k.c(a3.getStatus());
                if (a(c2, a2, a3.getId(), z3)) {
                    adrequesttype.b(a3);
                    return;
                }
                if (c2 != null) {
                    final i a4 = a(adrequesttype, c2, a3);
                    if (a4 != null) {
                        if (h()) {
                            a4.a(a2);
                        }
                        if (!b(adrequesttype, (AdObjectType) a4) || ((VERSION.SDK_INT > 22 && !a4.f()) || (!c.c(Appodeal.f) && c2.isPermissionRequired("android.permission.WRITE_EXTERNAL_STORAGE", adrequesttype.T())))) {
                            this.j.a(adrequesttype, a4, a3, LoadingError.NoFill);
                        }
                        if (z2) {
                            adrequesttype.d(a4);
                        } else {
                            adrequesttype.b(a4);
                        }
                        if (Appodeal.getLogLevel() != LogLevel.verbose) {
                            z4 = false;
                        }
                        c2.setLogging(z4);
                        adrequesttype.h(a4);
                        a(adrequesttype, (Runnable) a(adrequesttype, (AdObjectType) a4, a(adrequesttype, (AdObjectType) a4, z2)), a4.isAsync());
                        bq.a((Runnable) new Runnable() {
                            public void run() {
                                p.this.j.b(adrequesttype, a4, LoadingError.TimeoutError);
                            }
                        }, (long) a4.getLoadingTimeout());
                    }
                    qVar = this.j;
                    loadingError = LoadingError.AdTypeNotSupportedInAdapter;
                } else {
                    qVar = this.j;
                    loadingError = LoadingError.AdapterNotFound;
                }
                qVar.a(adrequesttype, null, a3, loadingError);
            } catch (Exception e2) {
                Log.log(e2);
                this.j.a(adrequesttype, null, a3, LoadingError.InternalError);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.u = str;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, @Nullable AdUnit adUnit, @Nullable LoadingError loadingError) {
        String format;
        if (Appodeal.getLogLevel() != LogLevel.none) {
            if (adUnit != null) {
                String id = adUnit.getId();
                if (!TextUtils.isEmpty(id) && TextUtils.getTrimmedLength(id) > 5) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(id.substring(0, 5));
                    sb.append("...");
                    id = sb.toString();
                }
                if (loadingError == null) {
                    format = String.format(Locale.ENGLISH, "%s - eCPM: %.2f, precache: %s, expTime: %s, id: %s", new Object[]{bq.c(adUnit.getStatus()), Double.valueOf(adUnit.getEcpm()), Boolean.valueOf(adUnit.isPrecache()), Long.valueOf(adUnit.getExpTime()), id});
                } else {
                    format = String.format(Locale.ENGLISH, "%s - %s (%s) - eCPM: %.2f, precache: %s, expTime: %s, id: %s", new Object[]{bq.c(adUnit.getStatus()), loadingError.toString().toUpperCase(), Integer.valueOf(loadingError.getCode()), Double.valueOf(adUnit.getEcpm()), Boolean.valueOf(adUnit.isPrecache()), Long.valueOf(adUnit.getExpTime()), id});
                }
            } else if (loadingError == null) {
                format = null;
            } else {
                format = String.format("%s (%s)", new Object[]{loadingError.toString().toUpperCase(), Integer.valueOf(loadingError.getCode())});
            }
            a(str, format);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, @Nullable String str2) {
        Log.log(bq.b(m()), str, str2);
    }

    /* access modifiers changed from: protected */
    public abstract void a(JSONObject jSONObject);

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.s = z2;
    }

    /* access modifiers changed from: protected */
    public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z2) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(AdRequestType adrequesttype) {
        return adrequesttype.A();
    }

    /* access modifiers changed from: protected */
    public boolean a(AdRequestType adrequesttype, int i2) {
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        JSONObject j2 = adrequesttype.j(adrequesttype.L());
        return j2 != null && j2.optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON) > adobjecttype.getEcpm();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public AdRequestType b(AdRequestType adrequesttype) {
        int indexOf = this.o.indexOf(adrequesttype) + 1;
        if (indexOf <= 0 || indexOf >= this.o.size()) {
            return null;
        }
        return (m) this.o.get(indexOf);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public AdRequestType b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (g || str != null) {
            for (int size = this.o.size() - 1; size >= 0; size--) {
                AdRequestType adrequesttype = (m) this.o.get(size);
                if (adrequesttype.p() && str.equals(adrequesttype.d())) {
                    return adrequesttype;
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.C = i2;
    }

    /* access modifiers changed from: protected */
    public void b(Context context) {
    }

    public void b(Context context, RequestParamsType requestparamstype) {
        m mVar;
        try {
            if (!this.q) {
                a(LogConstants.EVENT_REQUEST_FAILED, LogConstants.MSG_NOT_INITIALIZED);
            } else if (!bq.a(context)) {
                this.j.a(null, null, null, LoadingError.ConnectionError);
            } else {
                if (!Appodeal.d && !j()) {
                    if (!com.appodeal.ads.b.i.a().b().a(this.l)) {
                        m y2 = y();
                        if (y2 == null) {
                            a(LogConstants.EVENT_CACHE, String.format("isDebug: %s, isLoaded: %s, isLoading: %s", new Object[]{Boolean.valueOf(requestparamstype.a()), Boolean.valueOf(false), Boolean.valueOf(false)}));
                        } else {
                            a(LogConstants.EVENT_CACHE, String.format("isDebug: %s, isLoaded: %s, isLoading: %s", new Object[]{Boolean.valueOf(requestparamstype.a()), Boolean.valueOf(y2.h()), Boolean.valueOf(y2.J())}));
                            if (e()) {
                                o.a(y2.B());
                                o.a(y2.F().values());
                            }
                        }
                        mVar = a(requestparamstype);
                        try {
                            this.o.add(mVar);
                            this.c = mVar;
                            mVar.a(this, true);
                            mVar.a(this.y);
                            com.appodeal.ads.b.i.b(context);
                            mVar.a(Long.valueOf(com.appodeal.ads.b.i.a().c()));
                            this.j.a();
                            if (!mVar.a() && this.a != 0) {
                                if (!z.a(this.a, this.w)) {
                                    if (this.v != null) {
                                        this.v.a(b(mVar.d()));
                                        mVar.a(this.v);
                                    }
                                    this.r = false;
                                    f((AdRequestType) mVar);
                                    f();
                                }
                            }
                            new aq.c(context, mVar, requestparamstype.e()).a((com.appodeal.ads.aq.a<AdRequestType>) new a<AdRequestType>(g())).a(requestparamstype.c()).a().a();
                            f();
                        } catch (Exception e2) {
                            e = e2;
                            e.printStackTrace();
                            Log.log(e);
                            this.j.a(mVar, null, null, LoadingError.InternalError);
                        }
                    }
                }
                a(context, requestparamstype);
            }
        } catch (Exception e3) {
            e = e3;
            mVar = null;
            e.printStackTrace();
            Log.log(e);
            this.j.a(mVar, null, null, LoadingError.InternalError);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.A = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        if (!this.q || (!c() && (this.b || !r()))) {
            return false;
        }
        this.b = true;
        this.B = false;
        d();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        return adrequesttype.a(adobjecttype, this.t, this.m);
    }

    /* access modifiers changed from: 0000 */
    public void c(Context context) {
        m y2 = y();
        if (y2 != null && r()) {
            return;
        }
        if (y2 == null || y2.M() || F()) {
            d(context);
        } else if (y2.h()) {
            this.j.f(y2, y2.B());
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.B;
    }

    /* access modifiers changed from: 0000 */
    public boolean c(AdRequestType adrequesttype) {
        return this.c != null && this.c == adrequesttype;
    }

    /* access modifiers changed from: protected */
    public void d() {
        d(Appodeal.f);
    }

    public void d(Context context) {
        if (Appodeal.b) {
            this.B = true;
        } else {
            e(context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(@Nullable AdRequestType adrequesttype) {
        this.d = adrequesttype;
    }

    /* access modifiers changed from: protected */
    public abstract void e(Context context);

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean e(@Nullable AdRequestType adrequesttype) {
        return this.d != null && this.d == adrequesttype;
    }

    /* access modifiers changed from: protected */
    public void f() {
        for (int i2 = 0; i2 < this.o.size(); i2++) {
            AdRequestType adrequesttype = (m) this.o.get(i2);
            if (!(adrequesttype == null || adrequesttype.s() || adrequesttype == this.c || adrequesttype == this.d)) {
                adrequesttype.P();
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract String g();

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return true;
    }

    public boolean j() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public boolean k() {
        return com.appodeal.ads.b.i.a().b().a(this.l);
    }

    public boolean l() {
        return this.q;
    }

    public int m() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public int n() {
        return this.m;
    }

    public int o() {
        return this.n;
    }

    public double p() {
        return com.appodeal.ads.b.i.a().b().b(m());
    }

    public h q() {
        return this.k;
    }

    public boolean r() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public String s() {
        m y2 = y();
        return y2 != null ? y2.f() : "-1";
    }

    public d t() {
        return this.t == null ? e.c() : this.t;
    }

    public String u() {
        return d.a(this.t);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public String v() {
        return this.t != null ? this.t.n() : "default";
    }

    /* access modifiers changed from: 0000 */
    public boolean w() {
        return this.A;
    }

    public List<AdRequestType> x() {
        return this.o;
    }

    @Nullable
    public AdRequestType y() {
        if (this.o.isEmpty()) {
            return null;
        }
        return (m) this.o.get(this.o.size() - 1);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public AdRequestType z() {
        int indexOf = this.o.indexOf(this.c);
        if (indexOf > 0) {
            return (m) this.o.get(indexOf - 1);
        }
        return null;
    }
}
