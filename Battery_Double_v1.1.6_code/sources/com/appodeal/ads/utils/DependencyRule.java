package com.appodeal.ads.utils;

public class DependencyRule {
    private final String a;
    private final String b;

    public DependencyRule(String str) {
        this(str, null);
    }

    public DependencyRule(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String getDependency() {
        return this.a;
    }

    public String getErrorMessage() {
        return this.b;
    }
}
