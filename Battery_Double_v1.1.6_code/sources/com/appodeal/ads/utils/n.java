package com.appodeal.ads.utils;

import android.content.Context;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import com.appodeal.ads.bq;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.processor.VastAd;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HttpsURLConnection;

public class n implements Runnable {
    @VisibleForTesting
    boolean a;
    private Context b;
    /* access modifiers changed from: private */
    public a c;
    private String d;
    private File e;
    private final Handler f = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (n.this.c != null) {
                switch (message.what) {
                    case 0:
                        n.this.c.a();
                        return;
                    case 1:
                        Pair pair = (Pair) message.obj;
                        n.this.c.a((Uri) pair.first, (VastRequest) pair.second);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private final int g = 0;
    private final int h = 1;

    public interface a {
        void a();

        void a(Uri uri, VastRequest vastRequest);
    }

    public n(Context context, a aVar, String str) {
        if (context == null || str == null || !bq.v(context)) {
            aVar.a();
            return;
        }
        this.b = context;
        this.c = aVar;
        this.d = str;
        File externalFilesDir = context.getExternalFilesDir(null);
        if (externalFilesDir != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(externalFilesDir.getPath());
            sb.append("/native_video/");
            this.e = new File(sb.toString());
            if (!this.e.exists()) {
                this.e.mkdirs();
            }
            this.a = true;
            return;
        }
        aVar.a();
    }

    private InputStream a(String str) throws IOException {
        try {
            URLConnection openConnection = new URL(str).openConnection();
            a(openConnection);
            openConnection.setConnectTimeout(20000);
            openConnection.setReadTimeout(20000);
            openConnection.connect();
            return openConnection.getInputStream();
        } catch (IOException e2) {
            Log.log(e2);
            Builder buildUpon = Uri.parse(str).buildUpon();
            buildUpon.scheme("http");
            URLConnection openConnection2 = new URL(buildUpon.build().toString()).openConnection();
            openConnection2.setConnectTimeout(20000);
            openConnection2.setReadTimeout(20000);
            openConnection2.connect();
            return openConnection2.getInputStream();
        }
    }

    private void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
    }

    private void a(URLConnection uRLConnection) {
        try {
            if (uRLConnection instanceof HttpsURLConnection) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) uRLConnection;
                httpsURLConnection.setSSLSocketFactory(new t(httpsURLConnection.getSSLSocketFactory()));
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    private String b(String str) {
        return new BigInteger(bq.a(str.getBytes())).abs().toString(36);
    }

    public void run() {
        InputStream inputStream;
        Exception e2;
        if (this.a) {
            InputStream inputStream2 = null;
            try {
                VastRequest build = VastRequest.newBuilder().setPreCache(false).build();
                build.loadVideoWithDataSync(this.b, this.d, null);
                VastAd vastAd = build.getVastAd();
                if (vastAd != null && vastAd.getPickedMediaFileTag().getType().matches("video/.*(?i)(mp4|3gpp|mp2t|webm|matroska)")) {
                    String text = vastAd.getPickedMediaFileTag().getText();
                    inputStream = a(text);
                    try {
                        File file = new File(this.e, b(text));
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = inputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        fileOutputStream.close();
                        if (ThumbnailUtils.createVideoThumbnail(file.getPath(), 1) != null) {
                            this.f.sendMessage(this.f.obtainMessage(1, new Pair(Uri.fromFile(file), build)));
                            a(inputStream);
                            return;
                        }
                        inputStream2 = inputStream;
                    } catch (Exception e3) {
                        e2 = e3;
                        try {
                            Log.log(e2);
                            a(inputStream);
                            this.f.sendEmptyMessage(0);
                        } catch (Throwable th) {
                            th = th;
                            a(inputStream);
                            throw th;
                        }
                    }
                }
                a(inputStream2);
            } catch (Exception e4) {
                inputStream = null;
                e2 = e4;
                Log.log(e2);
                a(inputStream);
                this.f.sendEmptyMessage(0);
            } catch (Throwable th2) {
                inputStream = null;
                th = th2;
                a(inputStream);
                throw th;
            }
        }
        this.f.sendEmptyMessage(0);
    }
}
