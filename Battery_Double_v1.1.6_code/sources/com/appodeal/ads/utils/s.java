package com.appodeal.ads.utils;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class s implements Executor {
    public static s a;
    private static final TimeUnit b = TimeUnit.SECONDS;
    private static final int c = Runtime.getRuntime().availableProcessors();
    private static final int d = Math.max(2, Math.min(c - 1, 4));
    private static final int e = ((c * 2) + 1);
    private final ThreadPoolExecutor f;

    private static final class a implements RejectedExecutionHandler {
        private a() {
        }

        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            StringBuilder sb = new StringBuilder();
            sb.append("Task ");
            sb.append(runnable.toString());
            sb.append(" rejected from ");
            sb.append(threadPoolExecutor.toString());
            Log.log(new com.appodeal.ads.utils.b.a(sb.toString()));
        }
    }

    static {
        a = null;
        a = new s();
    }

    @VisibleForTesting
    s() {
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue();
        a aVar = new a();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(d, e, 1, b, linkedBlockingQueue, new u(10), aVar);
        this.f = threadPoolExecutor;
    }

    public void execute(@NonNull Runnable runnable) {
        this.f.execute(runnable);
    }
}
