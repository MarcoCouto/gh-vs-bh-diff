package com.appodeal.ads.utils;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.List;

@TargetApi(23)
public class PermissionFragment extends Fragment {
    private void a() {
        getFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
    }

    public void a(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String str : list) {
            if (getActivity().checkSelfPermission(str) == 0) {
                PermissionsHelper.a().a(new String[]{str}, new int[]{0});
            } else {
                arrayList.add(str);
            }
        }
        if (!arrayList.isEmpty()) {
            requestPermissions((String[]) arrayList.toArray(new String[0]), 1);
        } else {
            a();
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(NativeProtocol.RESULT_ARGS_PERMISSIONS)) {
            ArrayList stringArrayList = arguments.getStringArrayList(NativeProtocol.RESULT_ARGS_PERMISSIONS);
            if (stringArrayList != null) {
                a(stringArrayList);
                return super.onCreateView(layoutInflater, viewGroup, bundle);
            }
        }
        a();
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (1 == i) {
            PermissionsHelper.a().a(strArr, iArr);
        }
        a();
    }
}
