package com.appodeal.ads.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.bq;

public class a extends ArrayAdapter<String> {

    /* renamed from: com.appodeal.ads.utils.a$a reason: collision with other inner class name */
    public enum C0014a {
        Int(1, "Interstitial"),
        Video(2, "Video interstitial"),
        RVideo(128, "Rewarded video"),
        Banner(4, "Banner"),
        MREC(256, "MREC"),
        Native(512, "Native ad");
        
        private final int g;
        private final String h;

        private C0014a(int i2, String str) {
            this.g = i2;
            this.h = str;
        }

        public int a() {
            return this.g;
        }

        public String b() {
            return this.h;
        }
    }

    public a(Context context) {
        super(context, -1);
    }

    public C0014a a(int i) {
        return C0014a.values()[i];
    }

    public int getCount() {
        return C0014a.values().length;
    }

    @SuppressLint({"SetTextI18n"})
    @NonNull
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        relativeLayout.setLayoutParams(new LayoutParams(-1, Math.round(bq.h(getContext()) * 48.0f)));
        StateListDrawable a = g.a(-1, -1692651);
        if (VERSION.SDK_INT >= 16) {
            relativeLayout.setBackground(a);
        } else {
            relativeLayout.setBackgroundDrawable(a);
        }
        TextView textView = new TextView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(Math.round(bq.h(getContext()) * 48.0f), -2);
        layoutParams.addRule(15);
        textView.setLayoutParams(layoutParams);
        textView.setText(String.valueOf(i + 1));
        textView.setTextSize(16.0f);
        textView.setGravity(17);
        textView.setTextColor(g.b(Color.parseColor("#212121"), -1));
        textView.setId(VERSION.SDK_INT >= 17 ? View.generateViewId() : 171);
        TextView textView2 = new TextView(getContext());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams2.addRule(15);
        layoutParams2.addRule(1, textView.getId());
        textView2.setLayoutParams(layoutParams2);
        textView2.setTextSize(16.0f);
        textView2.setTextColor(g.b(Color.parseColor("#212121"), -1));
        textView2.setGravity(16);
        if (Appodeal.isInitialized(C0014a.values()[i].a())) {
            textView2.setText(C0014a.values()[i].b());
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(C0014a.values()[i].b());
            sb.append(" (Isn't Initialized)");
            textView2.setText(sb.toString());
            textView.setTextColor(Color.parseColor("#50000000"));
            textView2.setTextColor(Color.parseColor("#50000000"));
        }
        relativeLayout.addView(textView);
        relativeLayout.addView(textView2);
        return relativeLayout;
    }
}
