package com.appodeal.ads.utils;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.util.SparseArray;
import com.appodeal.ads.bq;
import com.appodeal.ads.utils.b.a;

public class x {
    /* access modifiers changed from: private */
    public static SparseArray<Pair<Handler, Runnable>> a = new SparseArray<>();

    public static void a(int i) {
        if (a.get(i) != null) {
            ((Handler) ((Pair) a.get(i)).first).removeCallbacks((Runnable) ((Pair) a.get(i)).second);
            a.remove(i);
        }
    }

    public static void a(final int i, final String str) {
        Handler handler = new Handler(Looper.getMainLooper());
        AnonymousClass1 r1 = new Runnable() {
            public void run() {
                Log.log(x.b(i, str));
                x.a.remove(i);
            }
        };
        handler.postDelayed(r1, 3000);
        a.put(i, new Pair(handler, r1));
    }

    static Exception b(int i, String str) {
        return new a(String.format("%s %s was not shown", new Object[]{bq.c(str), bq.a(i)}));
    }
}
