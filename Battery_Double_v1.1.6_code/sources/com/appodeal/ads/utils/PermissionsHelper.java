package com.appodeal.ads.utils;

import android.app.Activity;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.List;

public class PermissionsHelper {
    public static boolean a = true;
    public static boolean b = true;
    private static PermissionsHelper c;
    @Nullable
    private AppodealPermissionCallbacks d;

    public interface AppodealPermissionCallbacks {
        void accessCoarseLocationResponse(int i);

        void writeExternalStorageResponse(int i);
    }

    public static PermissionsHelper a() {
        if (c == null) {
            c = new PermissionsHelper();
        }
        return c;
    }

    public void a(@NonNull Activity activity, @Nullable AppodealPermissionCallbacks appodealPermissionCallbacks) {
        if (VERSION.SDK_INT >= 23) {
            this.d = appodealPermissionCallbacks;
            ArrayList arrayList = new ArrayList(2);
            if (a) {
                arrayList.add("android.permission.WRITE_EXTERNAL_STORAGE");
            }
            if (b) {
                List list = null;
                try {
                    list = c.c(activity);
                } catch (Exception e) {
                    Log.log(e);
                }
                arrayList.add((VERSION.SDK_INT < 29 || list == null || list.isEmpty() || !list.contains("android.permission.ACCESS_FINE_LOCATION")) ? "android.permission.ACCESS_COARSE_LOCATION" : "android.permission.ACCESS_FINE_LOCATION");
            }
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(NativeProtocol.RESULT_ARGS_PERMISSIONS, arrayList);
            PermissionFragment permissionFragment = new PermissionFragment();
            permissionFragment.setArguments(bundle);
            activity.getFragmentManager().beginTransaction().add(permissionFragment, "PermissionFragment").commitAllowingStateLoss();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String[] strArr, @NonNull int[] iArr) {
        if (this.d != null) {
            for (int i = 0; i < strArr.length; i++) {
                String str = strArr[i];
                if ("android.permission.WRITE_EXTERNAL_STORAGE".equals(str)) {
                    this.d.writeExternalStorageResponse(iArr[i]);
                } else if ("android.permission.ACCESS_COARSE_LOCATION".equals(str) || "android.permission.ACCESS_FINE_LOCATION".equals(str)) {
                    this.d.accessCoarseLocationResponse(iArr[i]);
                }
            }
        }
    }
}
