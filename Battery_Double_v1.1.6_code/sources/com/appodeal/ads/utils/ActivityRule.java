package com.appodeal.ads.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import com.appodeal.ads.bq;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ActivityRule {
    private final String a;
    private final List<RuleVerification> b;

    public static class Builder {
        private String a;
        private List<RuleVerification> b = new ArrayList();

        public Builder(String str) {
            this.a = str;
        }

        public Builder addVerification(RuleVerification ruleVerification) {
            if (ruleVerification != null) {
                this.b.add(ruleVerification);
            }
            return this;
        }

        public ActivityRule build() {
            return new ActivityRule(this.a, this.b);
        }
    }

    public static class FullscreenVerify implements RuleVerification {
        private static boolean a(Context context, int i) {
            return bq.b(context, i, 16842840) || bq.b(context, i, 16842839) || (VERSION.SDK_INT >= 20 && !bq.a(context, i, 16842840) && bq.b(context, i, 16843763));
        }

        public void verify(Context context, ActivityInfo activityInfo) {
            if (context.getApplicationInfo().targetSdkVersion >= 27 && VERSION.SDK_INT >= 26 && a(context, activityInfo.theme)) {
                Log.log(new IllegalStateException(String.format(Locale.ENGLISH, "Attention! Only fullscreen activities can request orientation: %s", new Object[]{activityInfo.name})));
            }
        }
    }

    public interface RuleVerification {
        void verify(Context context, ActivityInfo activityInfo);
    }

    private ActivityRule(String str, List<RuleVerification> list) {
        this.a = str;
        this.b = list;
    }

    public static String[] a(ActivityRule[] activityRuleArr) {
        String[] strArr = new String[activityRuleArr.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = activityRuleArr[i].a();
        }
        return strArr;
    }

    public String a() {
        return this.a;
    }

    public void a(Context context) {
        ActivityInfo activityInfo;
        try {
            activityInfo = context.getPackageManager().getActivityInfo(new ComponentName(context, this.a), 128);
        } catch (NameNotFoundException e) {
            Log.log(e);
            activityInfo = null;
        }
        if (activityInfo != null) {
            for (RuleVerification verify : this.b) {
                verify.verify(context, activityInfo);
            }
        }
    }
}
