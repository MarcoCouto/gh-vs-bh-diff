package com.appodeal.ads.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appodeal.ads.bq;
import java.util.List;

public class z extends ArrayAdapter<j> {
    private final List<j> a;

    public z(Context context, List<j> list) {
        super(context, -1, list);
        this.a = list;
    }

    @SuppressLint({"SetTextI18n"})
    @NonNull
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        relativeLayout.setLayoutParams(new LayoutParams(-1, Math.round(bq.h(getContext()) * 48.0f)));
        StateListDrawable a2 = g.a(-1, -1692651);
        if (VERSION.SDK_INT >= 16) {
            relativeLayout.setBackground(a2);
        } else {
            relativeLayout.setBackgroundDrawable(a2);
        }
        TextView textView = new TextView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(Math.round(bq.h(getContext()) * 48.0f), -2);
        layoutParams.addRule(15);
        textView.setLayoutParams(layoutParams);
        textView.setText(String.valueOf(i + 1));
        textView.setTextSize(16.0f);
        textView.setGravity(17);
        textView.setTextColor(g.b(Color.parseColor("#212121"), -1));
        textView.setId(VERSION.SDK_INT >= 17 ? View.generateViewId() : 171);
        TextView textView2 = new TextView(getContext());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams2.addRule(15);
        layoutParams2.addRule(1, textView.getId());
        textView2.setLayoutParams(layoutParams2);
        textView2.setText(((j) this.a.get(i)).d);
        textView2.setTextSize(16.0f);
        textView2.setTextColor(g.b(Color.parseColor("#212121"), -1));
        textView2.setGravity(16);
        relativeLayout.addView(textView);
        relativeLayout.addView(textView2);
        return relativeLayout;
    }
}
