package com.appodeal.ads.utils;

import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;

public class g {
    static StateListDrawable a(int i, int i2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.setExitFadeDuration(200);
        stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(i2));
        stateListDrawable.addState(new int[0], new ColorDrawable(i));
        return stateListDrawable;
    }

    static ColorStateList b(int i, int i2) {
        return new ColorStateList(new int[][]{new int[]{-16842919}, new int[]{16842919}}, new int[]{i, i2});
    }
}
