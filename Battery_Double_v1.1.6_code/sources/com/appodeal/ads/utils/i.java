package com.appodeal.ads.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class i extends ArrayAdapter<j> {
    static final /* synthetic */ boolean a = (!i.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public final List<j> b;
    private final int c;
    private final Filter d = new Filter() {
        /* access modifiers changed from: protected */
        public FilterResults performFiltering(CharSequence charSequence) {
            ArrayList arrayList = new ArrayList();
            if (TextUtils.isEmpty(charSequence)) {
                arrayList.addAll(i.this.b);
            } else {
                for (j jVar : i.this.b) {
                    if (jVar.c.toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        arrayList.add(jVar);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.count = arrayList.size();
            filterResults.values = arrayList;
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence charSequence, FilterResults filterResults) {
            i.this.setNotifyOnChange(false);
            i.this.clear();
            i.this.setNotifyOnChange(true);
            i.this.addAll((List) filterResults.values);
        }
    };

    public i(Context context, List<j> list) {
        super(context, -1, list);
        this.b = new ArrayList(list);
        this.c = (int) TypedValue.applyDimension(1, 8.0f, context.getResources().getDisplayMetrics());
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        Comparator comparator;
        switch (i) {
            case 0:
                comparator = new Comparator<j>() {
                    /* renamed from: a */
                    public int compare(j jVar, j jVar2) {
                        return jVar.b - jVar2.b;
                    }
                };
                break;
            case 1:
                comparator = new Comparator<j>() {
                    /* renamed from: a */
                    public int compare(j jVar, j jVar2) {
                        return jVar.c.compareToIgnoreCase(jVar2.c);
                    }
                };
                break;
            default:
                return;
        }
        sort(comparator);
    }

    public void a() {
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), 17367057);
        arrayAdapter.add("Waterfall order");
        arrayAdapter.add("Alphabetical order");
        Builder builder = new Builder(getContext());
        builder.setAdapter(arrayAdapter, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                i.this.a(i);
                dialogInterface.dismiss();
            }
        }).setTitle("Sort items");
        builder.create().show();
    }

    @NonNull
    public Filter getFilter() {
        return this.d;
    }

    @SuppressLint({"SetTextI18n"})
    @NonNull
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        j jVar = (j) getItem(i);
        if (a || jVar != null) {
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setLayoutParams(new LayoutParams(-1, -2));
            linearLayout.setOrientation(1);
            linearLayout.setPadding(this.c, this.c, this.c, this.c);
            TextView textView = new TextView(getContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            textView.setTextSize(22.0f);
            textView.setTextColor(-1);
            textView.setText(jVar.c);
            linearLayout.addView(textView);
            LinearLayout linearLayout2 = new LinearLayout(getContext());
            linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            linearLayout2.setOrientation(0);
            linearLayout2.setGravity(5);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            TextView textView2 = new TextView(getContext());
            textView2.setLayoutParams(layoutParams);
            StringBuilder sb = new StringBuilder();
            sb.append("№ <b>");
            sb.append(jVar.b);
            sb.append("</b>");
            textView2.setText(Html.fromHtml(sb.toString()));
            textView2.setTextSize(16.0f);
            textView2.setGravity(5);
            textView2.setTextColor(Color.parseColor("#B3ffffff"));
            textView2.setPadding(0, 0, this.c, 0);
            linearLayout2.addView(textView2);
            TextView textView3 = new TextView(getContext());
            textView3.setLayoutParams(layoutParams);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("cid: <b>");
            sb2.append(jVar.e);
            sb2.append("</b>");
            textView3.setText(Html.fromHtml(sb2.toString()));
            textView3.setTextSize(16.0f);
            textView3.setTextColor(Color.parseColor("#B3ffffff"));
            textView3.setGravity(5);
            textView3.setPadding(0, 0, this.c, 0);
            linearLayout2.addView(textView3);
            TextView textView4 = new TextView(getContext());
            textView4.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            textView4.setGravity(5);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("ecpm: <b>");
            sb3.append(new DecimalFormat("#.##").format(jVar.f));
            sb3.append("</b>");
            textView4.setText(Html.fromHtml(sb3.toString()));
            textView4.setTextSize(16.0f);
            textView4.setTextColor(Color.parseColor("#B3ffffff"));
            linearLayout2.addView(textView4);
            if (jVar.g) {
                TextView textView5 = new TextView(getContext());
                textView5.setText(Html.fromHtml("<b><i>(precache)</i></b>"));
                textView5.setTextSize(16.0f);
                textView5.setGravity(5);
                textView5.setTextColor(Color.parseColor("#B3ffffff"));
                textView5.setLayoutParams(layoutParams);
                linearLayout2.addView(textView5);
                textView4.setPadding(0, 0, this.c, 0);
                linearLayout.setBackgroundColor(Color.parseColor("#1A000000"));
            }
            linearLayout.addView(linearLayout2);
            return linearLayout;
        }
        throw new AssertionError();
    }
}
