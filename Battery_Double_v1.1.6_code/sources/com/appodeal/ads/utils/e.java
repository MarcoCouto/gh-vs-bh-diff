package com.appodeal.ads.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.processor.url.UrlProcessor;

public class e implements UrlProcessor {
    @Nullable
    public String prepare(@Nullable String str, @Nullable Bundle bundle) {
        if (!TextUtils.isEmpty(str) && bundle != null && bundle.containsKey("placement_id")) {
            String string = bundle.getString("placement_id");
            if (string != null) {
                VastLog.d("AppodealXPlacementUrlProcessor", String.format("Before prepare url: %s", new Object[]{str}));
                if (str.contains("${APPODEALX_PLACEMENT_ID}")) {
                    str = str.replace("${APPODEALX_PLACEMENT_ID}", string);
                }
                if (str.contains("%24%7BAPPODEALX_PLACEMENT_ID%7D")) {
                    str = str.replace("%24%7BAPPODEALX_PLACEMENT_ID%7D", string);
                }
                VastLog.d("AppodealXPlacementUrlProcessor", String.format("After prepare url: %s", new Object[]{str}));
            }
        }
        return str;
    }
}
