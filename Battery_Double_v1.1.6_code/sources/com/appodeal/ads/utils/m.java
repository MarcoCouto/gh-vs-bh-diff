package com.appodeal.ads.utils;

import android.content.Context;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.bq;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;

public class m implements Runnable {
    @VisibleForTesting
    boolean a;
    /* access modifiers changed from: private */
    public a b;
    private String c;
    private File d;
    private final Handler e = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (m.this.b != null) {
                switch (message.what) {
                    case 0:
                        m.this.b.a();
                        return;
                    case 1:
                        m.this.b.a((Uri) message.obj);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private final int f = 0;
    private final int g = 1;

    public interface a {
        void a();

        void a(Uri uri);
    }

    public m(Context context, a aVar, String str) {
        if (context == null || str == null || !bq.v(context)) {
            aVar.a();
            return;
        }
        this.b = aVar;
        this.c = str;
        File externalFilesDir = context.getExternalFilesDir(null);
        if (externalFilesDir != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(externalFilesDir.getPath());
            sb.append("/native_video/");
            this.d = new File(sb.toString());
            if (!this.d.exists()) {
                this.d.mkdirs();
            }
            this.a = true;
            return;
        }
        aVar.a();
    }

    private String a(String str) {
        return new BigInteger(bq.a(str.getBytes())).abs().toString(36);
    }

    private void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
    }

    public void run() {
        InputStream inputStream;
        Exception e2;
        if (this.a) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.c).openConnection();
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.setReadTimeout(20000);
                inputStream = httpURLConnection.getInputStream();
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("temp");
                    sb.append(System.currentTimeMillis());
                    File file = new File(this.d, sb.toString());
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    long contentLength = (long) httpURLConnection.getContentLength();
                    long j = 0;
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                        j += (long) read;
                    }
                    fileOutputStream.close();
                    String a2 = a(this.c);
                    if (contentLength == j) {
                        file.renameTo(new File(this.d, a2));
                    }
                    File file2 = new File(this.d, a2);
                    if (ThumbnailUtils.createVideoThumbnail(file2.getPath(), 1) != null) {
                        this.e.sendMessage(this.e.obtainMessage(1, Uri.fromFile(file2)));
                        a(inputStream);
                        return;
                    }
                    Log.log(LogConstants.EVENT_ASSETS, "Video", "video file not supported");
                    a(inputStream);
                } catch (Exception e3) {
                    e2 = e3;
                }
            } catch (Exception e4) {
                inputStream = null;
                e2 = e4;
                try {
                    Log.log(e2);
                    a(inputStream);
                    this.e.sendEmptyMessage(0);
                } catch (Throwable th) {
                    th = th;
                    a(inputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                inputStream = null;
                th = th2;
                a(inputStream);
                throw th;
            }
        }
        this.e.sendEmptyMessage(0);
    }
}
