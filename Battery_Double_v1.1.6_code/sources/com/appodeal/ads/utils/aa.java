package com.appodeal.ads.utils;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.app.AppState;
import com.appodeal.ads.utils.app.a;

public abstract class aa implements a {
    public abstract void a(@Nullable Activity activity, @NonNull AppState appState);

    public abstract void a(Configuration configuration);

    public void onActivityCreated(Activity activity, Bundle bundle) {
        a(activity, AppState.Created);
    }

    public void onActivityDestroyed(Activity activity) {
        a(activity, AppState.Destroyed);
    }

    public void onActivityPaused(Activity activity) {
        a(activity, AppState.Paused);
    }

    public void onActivityResumed(Activity activity) {
        a(activity, AppState.Resumed);
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
        a(activity, AppState.Started);
    }

    public void onActivityStopped(Activity activity) {
        a(activity, AppState.Stopped);
    }

    public void onConfigurationChanged(Configuration configuration) {
        a(configuration);
    }

    public void onLowMemory() {
    }
}
