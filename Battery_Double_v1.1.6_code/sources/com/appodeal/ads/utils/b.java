package com.appodeal.ads.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import com.appodeal.ads.bq;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;

public class b implements Runnable {
    private Context a;
    private Handler b = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            b.this.c.a((a) message.obj);
        }
    };
    /* access modifiers changed from: private */
    public d c;
    private Runnable d;

    public static abstract class a {
        private String a;
        private boolean b;

        public String a() {
            return this.a;
        }

        /* access modifiers changed from: 0000 */
        public abstract void a(Context context) throws Throwable;

        /* access modifiers changed from: 0000 */
        public void a(String str) {
            this.a = str;
        }

        /* access modifiers changed from: 0000 */
        public void a(boolean z) {
            this.b = z;
        }

        public boolean b() {
            return this.b;
        }
    }

    /* renamed from: com.appodeal.ads.utils.b$b reason: collision with other inner class name */
    private static class C0015b extends a {
        private C0015b() {
        }

        /* access modifiers changed from: 0000 */
        public void a(Context context) throws Throwable {
            ContentResolver contentResolver = context.getContentResolver();
            a(Secure.getString(contentResolver, TapjoyConstants.TJC_ADVERTISING_ID));
            a(Secure.getInt(contentResolver, "limit_ad_tracking") != 0);
        }
    }

    private static class c extends a {
        private c() {
        }

        /* access modifiers changed from: 0000 */
        public void a(Context context) throws Throwable {
            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            Object a = bq.a((Object) cls, cls, "getAdvertisingIdInfo", (Pair<Class, Object>[]) new Pair[]{new Pair(Context.class, context)});
            if (a != null) {
                a((String) bq.a(a, "getId", (Pair<Class, Object>[]) new Pair[0]));
                a(((Boolean) bq.a(a, RequestParameters.isLAT, (Pair<Class, Object>[]) new Pair[0])).booleanValue());
            }
        }
    }

    public interface d {
        void a(@NonNull a aVar);
    }

    private b(Context context, d dVar, @Nullable Runnable runnable) {
        this.a = context;
        this.c = dVar;
        this.d = runnable;
    }

    @NonNull
    private static a a(Context context) {
        a bVar = "Amazon".equals(Build.MANUFACTURER) ? new C0015b() : new c();
        try {
            bVar.a(context);
        } catch (Throwable th) {
            Log.log(th);
        }
        return bVar;
    }

    public static void a(Context context, d dVar, @Nullable Runnable runnable) {
        if (context != null && dVar != null) {
            s.a.execute(new b(context, dVar, runnable));
        }
    }

    public void run() {
        if (this.d != null) {
            this.d.run();
        }
        this.b.sendMessage(this.b.obtainMessage(0, a(this.a)));
    }
}
