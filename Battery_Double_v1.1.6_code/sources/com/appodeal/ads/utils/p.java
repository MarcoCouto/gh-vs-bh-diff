package com.appodeal.ads.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.ImageView;
import com.appodeal.ads.bq;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.lang.ref.WeakReference;

public class p {

    @VisibleForTesting
    static class a implements Runnable {
        private final Context a;
        private final String b;
        /* access modifiers changed from: private */
        public final WeakReference<ImageView> c;
        /* access modifiers changed from: private */
        public final b d;
        /* access modifiers changed from: private */
        public Bitmap e;

        a(Context context, String str, ImageView imageView, b bVar) {
            this.a = context;
            this.b = str;
            this.c = new WeakReference<>(imageView);
            this.d = bVar;
        }

        public void run() {
            try {
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(this.b, options);
                if (options.outWidth != 0) {
                    if (options.outHeight != 0) {
                        int a2 = p.a(this.a);
                        options.inSampleSize = p.a(options, a2, p.a(a2, false));
                        options.inJustDecodeBounds = false;
                        this.e = BitmapFactory.decodeFile(this.b, options);
                        bq.a((Runnable) new Runnable() {
                            public void run() {
                                ImageView imageView = (ImageView) a.this.c.get();
                                if (imageView == null || a.this.e == null) {
                                    a.this.d.a("Target ImageView or Bitmap is invalid");
                                } else {
                                    a.this.d.a(imageView, a.this.e);
                                }
                            }
                        });
                        return;
                    }
                }
                this.d.a("Image size is (0;0)");
            } catch (Exception e2) {
                if (e2.getMessage() == null) {
                    this.d.a("ImagePreparation error");
                } else {
                    this.d.a(e2.getMessage());
                }
            }
        }
    }

    public interface b {
        void a(@NonNull ImageView imageView, @NonNull Bitmap bitmap);

        void a(String str);
    }

    static int a(int i, boolean z) {
        if (z) {
            i = (int) (((float) i) / 1.5f);
        }
        if (i > 700) {
            return 700;
        }
        return i;
    }

    static int a(@NonNull Context context) {
        Pair e = bq.e(context);
        return Math.min(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, Math.min(((Integer) e.first).intValue(), ((Integer) e.second).intValue()));
    }

    static int a(Options options, int i, int i2) {
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        int i5 = 1;
        while (true) {
            if (i3 / i5 <= i && i4 / i5 <= i2) {
                return i5;
            }
            i5 *= 2;
        }
    }

    public static void a(String str, ImageView imageView, b bVar) {
        if (bVar != null) {
            if (TextUtils.isEmpty(str) || imageView == null) {
                bVar.a("Target ImageView or ImagePath is invalid");
            } else {
                s.a.execute(new a(imageView.getContext(), str, imageView, bVar));
            }
        }
    }
}
