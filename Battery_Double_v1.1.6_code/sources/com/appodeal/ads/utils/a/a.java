package com.appodeal.ads.utils.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.appodeal.ads.bl;
import com.appodeal.ads.utils.Log;
import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONObject;

public class a implements c {
    private final String a;

    public a(String str) {
        this.a = str;
    }

    public static void b(Context context, JSONObject jSONObject) {
        Editor edit = bl.a(context, "freq").b().edit();
        edit.clear();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            try {
                String str = (String) keys.next();
                edit.putString(str, jSONObject.getString(str));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        edit.apply();
    }

    public static JSONObject d(Context context) {
        SharedPreferences b = bl.a(context, "freq").b();
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : b.getAll().entrySet()) {
            try {
                jSONObject.put((String) entry.getKey(), new JSONObject((String) entry.getValue()));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return jSONObject;
    }

    public static void e(Context context) {
        SharedPreferences b = bl.a(context, "freq_clicks").b();
        Editor edit = b.edit();
        long currentTimeMillis = System.currentTimeMillis() - 259200000;
        for (Entry entry : b.getAll().entrySet()) {
            try {
                if (((Long) entry.getValue()).longValue() < currentTimeMillis) {
                    edit.remove((String) entry.getKey());
                }
            } catch (Exception unused) {
                edit.remove((String) entry.getKey());
            }
        }
        edit.apply();
    }

    public JSONObject a(Context context) {
        SharedPreferences b = bl.a(context, "freq").b();
        if (b.contains(this.a)) {
            try {
                return new JSONObject(b.getString(this.a, null));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return null;
    }

    public void a(Context context, JSONObject jSONObject) {
        try {
            bl.a(context, "freq").b().edit().putString(this.a, jSONObject.toString()).apply();
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public boolean b(Context context) {
        return bl.a(context, "freq_clicks").b().contains(this.a);
    }

    public void c(Context context) {
        try {
            bl.a(context, "freq_clicks").a().putLong(this.a, System.currentTimeMillis()).apply();
        } catch (Exception e) {
            Log.log(e);
        }
    }
}
