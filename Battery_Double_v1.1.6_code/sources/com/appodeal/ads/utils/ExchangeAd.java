package com.appodeal.ads.utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.bq;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class ExchangeAd implements Parcelable {
    public static final int CLICK_REQUEST_ERROR = 1002;
    @VisibleForTesting
    public static final String CREATIVE_HEIGHT = "X-Appodeal-Creative-Height";
    @VisibleForTesting
    public static final String CREATIVE_LAYOUT = "X-Appodeal-Creative-Layout";
    @VisibleForTesting
    public static final String CREATIVE_WIDTH = "X-Appodeal-Creative-Width";
    public static final Creator<ExchangeAd> CREATOR = new Creator<ExchangeAd>() {
        /* renamed from: a */
        public ExchangeAd createFromParcel(Parcel parcel) {
            return new ExchangeAd(parcel);
        }

        /* renamed from: a */
        public ExchangeAd[] newArray(int i) {
            return new ExchangeAd[i];
        }
    };
    public static final int FILL_REQUEST_ERROR = 1004;
    public static final int FINISH_REQUEST_ERROR = 1003;
    public static final int IMPRESSION_REQUEST_ERROR = 1001;
    public static final int LOADING_TIMEOUT_ERROR = 1005;
    @VisibleForTesting
    int a = -1;
    private final String b;
    private final Map<String, List<String>> c;
    private long d;

    protected ExchangeAd(Parcel parcel) {
        this.b = parcel.readString();
        this.c = parcel.readHashMap(List.class.getClassLoader());
        this.a = parcel.readInt();
        this.d = parcel.readLong();
    }

    public ExchangeAd(String str, Map<String, List<String>> map, long j) {
        this.b = str;
        this.c = map;
        this.d = j;
    }

    private int a(String str) {
        try {
            if (this.c != null && this.c.containsKey(str)) {
                List list = (List) this.c.get(str);
                if (list.size() > 0) {
                    return Integer.valueOf((String) list.get(0)).intValue();
                }
            }
        } catch (Exception e) {
            Log.log(e);
        }
        return 0;
    }

    @Nullable
    private String a(@Nullable String str, long j, int i, @NonNull String str2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str.replace("%%SEGMENT%%", String.valueOf(j)).replace("%25%25SEGMENT%25%25", String.valueOf(j)).replace("%%PLACEMENT%%", String.valueOf(i)).replace("%25%25PLACEMENT%25%25", String.valueOf(i)).replace("%%ERRORCODE%%", str2).replace("%25%25ERRORCODE%25%25", str2);
    }

    private void a(String str, Runnable runnable) {
        a(str, "", runnable);
    }

    private void a(String str, @NonNull String str2, Runnable runnable) {
        if (this.c != null && this.c.containsKey(str)) {
            for (String a2 : (List) this.c.get(str)) {
                bq.a(a(a2, this.d, this.a, str2), (Executor) s.a, runnable);
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getAdm() {
        return this.b;
    }

    public int getCloseTime() {
        return a("X-Appodeal-Close-Time");
    }

    public int getHeight() {
        return a(CREATIVE_HEIGHT);
    }

    public String getType() {
        try {
            if (this.c != null && this.c.containsKey("X-Appodeal-Creative-Type")) {
                List list = (List) this.c.get("X-Appodeal-Creative-Type");
                if (list.size() > 0) {
                    return (String) list.get(0);
                }
            }
        } catch (Exception e) {
            Log.log(e);
        }
        return "";
    }

    public int getWidth() {
        return a(CREATIVE_WIDTH);
    }

    public void trackClick() {
        a("X-Appodeal-Url-Click", new Runnable() {
            public void run() {
                ExchangeAd.this.trackError(1002);
            }
        });
    }

    public void trackError(int i) {
        a("X-Appodeal-Url-Error", String.valueOf(i), null);
    }

    public void trackFill() {
        a("X-Appodeal-Url-Fill", new Runnable() {
            public void run() {
                ExchangeAd.this.trackError(1004);
            }
        });
    }

    public void trackFinish() {
        a("X-Appodeal-Url-Finish", new Runnable() {
            public void run() {
                ExchangeAd.this.trackError(1003);
            }
        });
    }

    public void trackImpression(int i) {
        this.a = i;
        a("X-Appodeal-Url-Impression", new Runnable() {
            public void run() {
                ExchangeAd.this.trackError(1001);
            }
        });
    }

    public boolean useLayout() {
        try {
            if (this.c != null && this.c.containsKey(CREATIVE_LAYOUT)) {
                List list = (List) this.c.get(CREATIVE_LAYOUT);
                if (list != null && list.size() > 0) {
                    return Boolean.valueOf((String) list.get(0)).booleanValue();
                }
            }
        } catch (Exception e) {
            Log.log(e);
        }
        return true;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeMap(this.c);
        parcel.writeInt(this.a);
        parcel.writeLong(this.d);
    }
}
