package com.appodeal.ads.utils;

public class LogConstants {
    public static final String EVENT_AD_DESTROY = "Destroy";
    public static final String EVENT_AD_HIDE = "Hide";
    public static final String EVENT_ASSETS = "Assets";
    public static final String EVENT_ASSETS_ERROR = "Assets Error";
    public static final String EVENT_CACHE = "Cache";
    public static final String EVENT_CANCEL = "Cancel";
    public static final String EVENT_CAN_LOAD_CAMPAIGN = "Can Load Campaign";
    public static final String EVENT_CAN_SHOW = "Can show";
    public static final String EVENT_CLICKED = "Clicked";
    public static final String EVENT_CLOSED = "Closed";
    public static final String EVENT_DISABLE_NETWORK = "Disable Network";
    public static final String EVENT_DUMP = "Dump";
    public static final String EVENT_ERROR = "Error";
    public static final String EVENT_EXPIRED = "Expired";
    public static final String EVENT_FINISHED = "Finished";
    public static final String EVENT_GET = "Get";
    public static final String EVENT_GET_ADS = "Get Ads";
    public static final String EVENT_INFO = "Info";
    public static final String EVENT_INITIALIZE = "Initialize";
    public static final String EVENT_LAUNCH_ACTIVITY = "Launch";
    public static final String EVENT_LAUNCH_ERROR = "Launch Error";
    public static final String EVENT_LAUNCH_URL = "Launch";
    public static final String EVENT_LOADED = "Loaded";
    public static final String EVENT_LOAD_FAILED = "Load Failed";
    public static final String EVENT_LOAD_FAILED_SOFT = "Load Failed (soft)";
    public static final String EVENT_LOAD_SKIPPED = "Load Skipped";
    public static final String EVENT_LOAD_START = "Load Start";
    public static final String EVENT_LOCATION = "Location";
    public static final String EVENT_MV_PLAYER = "Player";
    public static final String EVENT_MV_PLAYER_ERROR = "Player Error";
    public static final String EVENT_MV_STATE = "State";
    public static final String EVENT_MV_VIDEO = "Video";
    public static final String EVENT_NETWORK_CONNECTION = "Network Connection";
    public static final String EVENT_NETWORK_ERROR = "Network Error";
    public static final String EVENT_NOTIFY_CLICKED = String.format("%s %s", new Object[]{"Notify", EVENT_CLICKED});
    public static final String EVENT_NOTIFY_CLOSED = String.format("%s %s", new Object[]{"Notify", EVENT_CLOSED});
    public static final String EVENT_NOTIFY_EXPIRED = String.format("%s %s", new Object[]{"Notify", EVENT_EXPIRED});
    public static final String EVENT_NOTIFY_FINISHED = String.format("%s %s", new Object[]{"Notify", EVENT_FINISHED});
    public static final String EVENT_NOTIFY_LOADED = String.format("%s %s", new Object[]{"Notify", EVENT_LOADED});
    public static final String EVENT_NOTIFY_LOAD_FAILED = String.format("%s %s", new Object[]{"Notify", EVENT_LOAD_FAILED});
    public static final String EVENT_NOTIFY_SHOWN = String.format("%s %s", new Object[]{"Notify", EVENT_SHOWN});
    public static final String EVENT_NOTIFY_SHOW_FAILED = String.format("%s %s", new Object[]{"Notify", EVENT_SHOW_FAILED});
    public static final String EVENT_PAUSE = "Pause";
    public static final String EVENT_PERMISSIONS = "Permissions";
    public static final String EVENT_REQUEST_FAILED = "Request Failed";
    public static final String EVENT_RESPONSE = "Response";
    public static final String EVENT_RESUME = "Resume";
    public static final String EVENT_SCREEN_ORIENTATION = "Screen Orientation";
    public static final String EVENT_SET = "Set";
    public static final String EVENT_SHOW = "Show";
    public static final String EVENT_SHOWN = "Shown";
    public static final String EVENT_SHOW_DIALOG = "Show Dialog";
    public static final String EVENT_SHOW_ERROR = "Show Error";
    public static final String EVENT_SHOW_FAILED = "Show Failed";
    public static final String EVENT_SHOW_PROGRESS = "Show Progress";
    public static final String EVENT_SHOW_TOAST = "Show Toast";
    public static final String EVENT_TRACK = "Track";
    public static final String EVENT_VIEWABILITY = "Viewability";
    public static final String EVENT_WARNING = "Warning";
    public static final String KEY_BANNER = "Banner";
    public static final String KEY_INTERSTITIAL = "Interstitial";
    public static final String KEY_MREC = "Mrec";
    public static final String KEY_NATIVE = "Native";
    public static final String KEY_NETWORK = "Network";
    public static final String KEY_NON_SKIPPABLE_VIDEO = "NonSkippableVideo";
    public static final String KEY_REWARDED_VIDEO = "RewardedVideo";
    public static final String KEY_SDK = "SDK";
    public static final String KEY_SDK_PUBLIC = "SDK-Public";
    public static final String KEY_UNKNOWN = "Unknown";
    public static final String KEY_VIDEO = "Video";
    public static final String MSG_AD_TYPE_DISABLED = "disabled";
    public static final String MSG_AD_TYPE_DISABLED_BY_SEGMENT = "disabled by segment";
    public static final String MSG_NOT_INITIALIZED = "isn't initialized";
    public static final String MSG_VIEW_NOT_FOUND = "view not found";
}
