package com.appodeal.ads.utils;

import android.support.annotation.NonNull;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ThreadFactory;

final class u implements ThreadFactory {
    private final int a;

    u(int i) {
        this.a = i;
    }

    public Thread newThread(@NonNull Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setPriority(this.a);
        thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                Log.log(th);
            }
        });
        return thread;
    }
}
