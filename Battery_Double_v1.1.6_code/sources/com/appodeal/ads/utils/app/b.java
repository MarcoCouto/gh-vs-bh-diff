package com.appodeal.ads.utils.app;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.utils.c;
import java.lang.ref.WeakReference;
import java.util.Queue;

@Deprecated
public enum b implements a {
    All,
    Ad,
    NotAd;
    
    @VisibleForTesting
    public final Queue<WeakReference<a>> d;

    private boolean a(Activity activity) {
        return this == All || (this == Ad && c.b(activity)) || (this == NotAd && !c.b(activity));
    }

    public void a(@Nullable a aVar) {
        if (aVar != null) {
            this.d.add(new WeakReference(aVar));
        }
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivityCreated(activity, bundle);
                }
            }
        }
    }

    public void onActivityDestroyed(Activity activity) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivityDestroyed(activity);
                }
            }
        }
    }

    public void onActivityPaused(Activity activity) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivityPaused(activity);
                }
            }
        }
    }

    public void onActivityResumed(Activity activity) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivityResumed(activity);
                }
            }
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivitySaveInstanceState(activity, bundle);
                }
            }
        }
    }

    public void onActivityStarted(Activity activity) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivityStarted(activity);
                }
            }
        }
    }

    public void onActivityStopped(Activity activity) {
        if (a(activity)) {
            for (WeakReference weakReference : this.d) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.onActivityStopped(activity);
                }
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        for (WeakReference weakReference : this.d) {
            a aVar = (a) weakReference.get();
            if (aVar != null) {
                aVar.onConfigurationChanged(configuration);
            }
        }
    }

    public void onLowMemory() {
        for (WeakReference weakReference : this.d) {
            a aVar = (a) weakReference.get();
            if (aVar != null) {
                aVar.onLowMemory();
            }
        }
    }
}
