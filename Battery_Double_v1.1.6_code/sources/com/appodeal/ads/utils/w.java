package com.appodeal.ads.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import com.appodeal.ads.bl;
import com.tapjoy.TapjoyConstants;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class w {
    private static final long a = TimeUnit.SECONDS.toMillis(30);
    private static volatile w b;
    private final String c = UUID.randomUUID().toString();
    private long d;
    private long e;
    private long f;
    private long g;
    private long h;
    private long i = a;

    private w() {
    }

    public static w a() {
        if (b == null) {
            synchronized (w.class) {
                if (b == null) {
                    b = new w();
                }
            }
        }
        return b;
    }

    private static void g(@NonNull Context context) {
        synchronized (w.class) {
            if (b != null) {
                w wVar = b;
                b = new w();
                b.i = wVar.i;
                b.a(context);
            }
        }
    }

    public void a(long j) {
        this.i = j;
    }

    public void a(Context context) {
        SharedPreferences b2 = bl.a(context).b();
        this.h = b2.getLong("last_session_start", 0);
        (b2.contains(TapjoyConstants.TJC_SESSION_ID) ? b2.edit().putLong(TapjoyConstants.TJC_SESSION_ID, b2.getLong(TapjoyConstants.TJC_SESSION_ID, 0) + 1) : b2.edit().putLong(TapjoyConstants.TJC_SESSION_ID, 1)).apply();
        b2.edit().putLong("app_uptime", b2.getLong("app_uptime", 0) + b2.getLong("session_uptime", 0)).putLong("session_uptime", 0).putLong("last_session_start", System.currentTimeMillis()).apply();
        this.d = System.currentTimeMillis();
    }

    public String b() {
        return this.c;
    }

    public void b(@NonNull Context context) {
        this.d = System.currentTimeMillis();
        if (this.d - this.e >= this.i) {
            g(context);
        }
    }

    public long c() {
        if (this.d == 0) {
            return 0;
        }
        return ((this.f + System.currentTimeMillis()) - this.d) / 1000;
    }

    public void c(@NonNull Context context) {
        this.e = System.currentTimeMillis();
        this.f += System.currentTimeMillis() - this.d;
        f(context);
    }

    public long d(Context context) {
        return bl.a(context).b().getLong(TapjoyConstants.TJC_SESSION_ID, 0);
    }

    public long e(Context context) {
        return (bl.a(context).b().getLong("app_uptime", 0) / 1000) + c();
    }

    public void f(Context context) {
        if (System.currentTimeMillis() - this.g >= TapjoyConstants.TIMER_INCREMENT) {
            bl.a(context).a().putLong("session_uptime", this.f).apply();
            this.g = System.currentTimeMillis();
        }
    }
}
