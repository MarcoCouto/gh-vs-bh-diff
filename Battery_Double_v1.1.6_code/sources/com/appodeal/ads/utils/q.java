package com.appodeal.ads.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.bq;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class q {
    static final /* synthetic */ boolean b = (!q.class.desiredAssertionStatus());
    @Nullable
    @VisibleForTesting
    public String a;

    public interface a {
        void onHandleError();

        void onHandled();

        void processClick(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener);
    }

    public void a(@NonNull final Context context, @Nullable String str, @Nullable String str2, long j, @NonNull final a aVar) {
        Runnable runnable;
        if (TextUtils.isEmpty(str)) {
            aVar.onHandleError();
        } else if (b || str != null) {
            r.a(context, str2, j);
            if (!str.equals("appodeal://")) {
                this.a = str;
                aVar.processClick(null);
                runnable = new Runnable() {
                    public void run() {
                        aVar.onHandled();
                    }
                };
            } else if (!TextUtils.isEmpty(this.a)) {
                str = this.a;
                runnable = new Runnable() {
                    public void run() {
                        aVar.onHandled();
                    }
                };
            } else {
                aVar.processClick(new UnifiedAdCallbackClickTrackListener() {
                    public void onTrackError() {
                        aVar.onHandleError();
                    }

                    public void onTrackSuccess(JSONObject jSONObject) {
                        a aVar;
                        try {
                            if (jSONObject.getString("status").equals("ok")) {
                                JSONArray jSONArray = null;
                                if (jSONObject.has("urls")) {
                                    jSONArray = jSONObject.getJSONArray("urls");
                                }
                                if (jSONArray == null) {
                                    jSONArray = new JSONArray();
                                }
                                if (jSONObject.has("url")) {
                                    jSONArray.put(jSONObject.getString("url"));
                                }
                                if (jSONArray.length() > 0) {
                                    q.this.a = bq.a(context, jSONArray, (Runnable) new Runnable() {
                                        public void run() {
                                            aVar.onHandled();
                                        }
                                    });
                                    return;
                                }
                                aVar = aVar;
                            } else {
                                aVar = aVar;
                            }
                            aVar.onHandleError();
                        } catch (JSONException e) {
                            Log.log(e);
                            aVar.onHandleError();
                        }
                    }
                });
            }
            bq.a(context, str, runnable);
        } else {
            throw new AssertionError();
        }
    }
}
