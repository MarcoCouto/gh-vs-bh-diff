package com.appodeal.ads.utils.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.aj;
import com.appodeal.ads.bl;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.tapjoy.TapjoyConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class c {
    @VisibleForTesting
    final int a = 2;
    @VisibleForTesting
    final int b = 10;
    private SharedPreferences c;

    static String a(Collection<String> collection, String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String append : collection) {
            sb.append(append);
            i++;
            if (i < collection.size()) {
                sb.append(str);
            }
        }
        return sb.toString();
    }

    private synchronized void a(List<String> list, String str) {
        try {
            Iterator it = list.iterator();
            boolean z = false;
            while (it.hasNext() && !z) {
                if (!new JSONObject((String) it.next()).optBoolean(Param.FATAL)) {
                    it.remove();
                    z = true;
                }
            }
            if (!z && new JSONObject(str).optBoolean(Param.FATAL)) {
                list.remove(0);
            }
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public SharedPreferences a(Context context) {
        if (this.c == null && context != null) {
            this.c = bl.a(context, "exceptions").b();
        }
        return this.c;
    }

    public List<String> a(@NonNull Context context, boolean z) {
        List<String> list;
        SharedPreferences a2 = a(context);
        if (a2 == null) {
            return new ArrayList();
        }
        String string = a2.getString("exceptions", "");
        if (TextUtils.isEmpty(string)) {
            list = new ArrayList<>();
        } else {
            list = a(string, z ? 0 : 2, ":::");
        }
        return list;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public List<String> a(String str, int i, String str2) {
        Matcher matcher = Pattern.compile(str2, 2).matcher(str);
        ArrayList arrayList = new ArrayList(i);
        int i2 = 0;
        while (matcher.find() && (arrayList.size() < i || i == 0)) {
            arrayList.add(str.subSequence(i2, matcher.start()).toString());
            i2 = matcher.end();
        }
        if (i == 0 && i2 > 0) {
            arrayList.add(str.subSequence(i2, str.length()).toString());
        }
        if (str.length() > 0 && arrayList.size() == 0) {
            arrayList.add(str);
        }
        return arrayList;
    }

    public synchronized void a(@NonNull Context context, String str) {
        SharedPreferences a2 = a(context);
        if (!(a2 == null || str == null || str.length() <= 0)) {
            List a3 = a(context, true);
            if (a3.size() >= 10) {
                a(a3, str);
            }
            a3.add(str);
            a2.edit().putString("exceptions", a((Collection<String>) a3, ":::")).apply();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting(otherwise = 3)
    public synchronized void a(@NonNull Context context, List<String> list) {
        SharedPreferences a2 = a(context);
        if (!(a2 == null || list == null || list.size() <= 0)) {
            List a3 = a(context, true);
            if (a3.removeAll(list)) {
                a2.edit().putString("exceptions", a((Collection<String>) a3, ":::")).apply();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @VisibleForTesting(otherwise = 3)
    public void b(@NonNull Context context, String str) {
        long j;
        Date date;
        long j2;
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            try {
                date = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z", Locale.ENGLISH).parse(str);
                try {
                    j = date.getTime();
                } catch (Exception unused) {
                    j = 0;
                    if (date == null) {
                    }
                    if (j2 != 0) {
                    }
                }
            } catch (Exception unused2) {
                date = null;
                j = 0;
                if (date == null) {
                }
                if (j2 != 0) {
                }
            }
            if (date == null) {
                j2 = (Long.parseLong(str) * 1000) + System.currentTimeMillis();
            } else {
                j2 = j;
            }
            if (j2 != 0) {
                a2.edit().putLong(TapjoyConstants.TJC_RETRY, j2).apply();
            }
        }
    }

    public boolean b(@NonNull Context context) {
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            return TextUtils.isEmpty(a2.getString("exceptions", ""));
        }
        return true;
    }

    public void c(@NonNull Context context, String str) {
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            a2.edit().putString("active", str).apply();
        }
    }

    public boolean c(@NonNull Context context) {
        return !g(context).equals(aj.b);
    }

    public boolean d(@NonNull Context context) {
        return g(context).equals(aj.d);
    }

    public void e(@NonNull Context context) {
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            a2.edit().remove("exceptions").remove("active").apply();
        }
    }

    public boolean f(@NonNull Context context) {
        SharedPreferences a2 = a(context);
        return a2 == null || a2.getLong(TapjoyConstants.TJC_RETRY, System.currentTimeMillis()) <= System.currentTimeMillis();
    }

    public String g(@NonNull Context context) {
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            try {
                return a2.getString("active", aj.b);
            } catch (Exception unused) {
                e(context);
            }
        }
        return aj.b;
    }
}
