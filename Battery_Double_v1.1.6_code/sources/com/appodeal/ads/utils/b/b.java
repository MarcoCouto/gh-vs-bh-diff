package com.appodeal.ads.utils.b;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class b implements Runnable {
    @NonNull
    private final Context a;
    private final c b;

    public b(@NonNull Context context, c cVar) {
        this.a = context;
        this.b = cVar;
    }

    /* access modifiers changed from: 0000 */
    public URL a() throws MalformedURLException {
        return new URL("https://ach.appodeal.com/api/v0/android/crashes");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00af A[SYNTHETIC, Splitter:B:32:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bc A[Catch:{ Exception -> 0x00c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    public void run() {
        String str = "ExceptionTask";
        try {
            List<String> a2 = this.b.a(this.a, false);
            JSONArray jSONArray = new JSONArray();
            for (String jSONObject : a2) {
                jSONArray.put(new JSONObject(jSONObject));
            }
            HttpURLConnection httpURLConnection = null;
            try {
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) a().openConnection();
                try {
                    httpURLConnection2.setConnectTimeout(10000);
                    httpURLConnection2.setReadTimeout(10000);
                    httpURLConnection2.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    httpURLConnection2.setDoOutput(true);
                    httpURLConnection2.setRequestMethod(HttpRequest.METHOD_POST);
                    DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection2.getOutputStream());
                    dataOutputStream.write(jSONArray.toString().getBytes(Charset.forName("UTF-8")));
                    dataOutputStream.flush();
                    dataOutputStream.close();
                    int responseCode = httpURLConnection2.getResponseCode();
                    if (responseCode == 200) {
                        this.b.a(this.a, a2);
                    } else if (responseCode == 503) {
                        String headerField = httpURLConnection2.getHeaderField("Retry-After");
                        if (headerField != null) {
                            this.b.b(this.a, headerField);
                        }
                    }
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.getInputStream().close();
                        httpURLConnection2.disconnect();
                    }
                } catch (Exception e) {
                    e = e;
                    httpURLConnection = httpURLConnection2;
                    try {
                        Log.e(str, e.toString());
                        if (httpURLConnection == null) {
                            httpURLConnection.getInputStream().close();
                            httpURLConnection.disconnect();
                        }
                    } catch (Throwable th) {
                        th = th;
                        httpURLConnection2 = httpURLConnection;
                        if (httpURLConnection2 != null) {
                            httpURLConnection2.getInputStream().close();
                            httpURLConnection2.disconnect();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (httpURLConnection2 != null) {
                    }
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                Log.e(str, e.toString());
                if (httpURLConnection == null) {
                }
            }
        } catch (Exception e3) {
            Log.e(str, e3.toString());
        }
    }
}
