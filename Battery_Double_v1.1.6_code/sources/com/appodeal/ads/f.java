package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.a.a.C0013a;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

class f {
    @VisibleForTesting
    Set<d> a = new HashSet();

    f() {
    }

    private void a(Set<e> set, e eVar) {
        if (bq.a(eVar.b())) {
            set.add(eVar);
            return;
        }
        Log.log(LogConstants.KEY_NETWORK, "Error", String.format("Required classes not found for %s", new Object[]{bq.c(eVar.a())}));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public e a(@NonNull Set<e> set, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        for (e eVar : set) {
            if (eVar.a().equals(str)) {
                return eVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Set<e> a() {
        HashSet hashSet = new HashSet();
        a((Set<e>) hashSet, (e) new C0013a());
        return hashSet;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Context context) {
        if (context != null) {
            for (d a2 : this.a) {
                a2.a(context, bg.a);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Context context, @Nullable JSONArray jSONArray) {
        if (context != null && jSONArray != null) {
            try {
                if (jSONArray.length() != 0) {
                    a(context, jSONArray, a());
                }
            } catch (Exception e) {
                Log.log(e);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull Context context, @NonNull JSONArray jSONArray, @NonNull Set<e> set) {
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                e a2 = a(set, optJSONObject.optString("status"));
                if (a2 != null) {
                    d c = a2.c();
                    c.a(context, optJSONObject, bg.a);
                    this.a.add(c);
                    Log.log(LogConstants.KEY_NETWORK, LogConstants.EVENT_INITIALIZE, String.format("%s", new Object[]{bq.c(a2.a())}));
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable Context context) {
        if (context != null) {
            for (d b : this.a) {
                b.b(context, bg.a);
            }
        }
    }
}
