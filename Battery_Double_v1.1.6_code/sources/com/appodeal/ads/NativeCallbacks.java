package com.appodeal.ads;

public interface NativeCallbacks {
    void onNativeClicked(NativeAd nativeAd);

    void onNativeExpired();

    void onNativeFailedToLoad();

    void onNativeLoaded();

    void onNativeShowFailed(NativeAd nativeAd);

    void onNativeShown(NativeAd nativeAd);
}
