package com.appodeal.ads;

import android.app.Activity;
import android.support.annotation.NonNull;

class ah extends AdNetwork<a> {

    static final class a {
        a() {
        }
    }

    public static class b extends AdNetworkBuilder {
        public AdNetwork build() {
            return new ah(this);
        }

        public String getAdapterVersion() {
            return "1";
        }

        public String getName() {
            return "debug";
        }
    }

    private ah(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    public String getVersion() {
        return "1";
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull NetworkInitializationListener<a> networkInitializationListener) throws Exception {
        final p pVar = null;
        final m mVar = adNetworkMediationParams instanceof g ? ((g) adNetworkMediationParams).a : null;
        if (mVar == null) {
            networkInitializationListener.onInitializationFailed(LoadingError.AdTypeNotSupportedInAdapter);
            return;
        }
        if (mVar instanceof ap) {
            pVar = am.a();
        } else if (mVar instanceof ad) {
            pVar = aa.b();
        } else if (mVar instanceof ba) {
            pVar = Native.a();
        } else if (mVar instanceof aw) {
            pVar = at.a();
        } else if (mVar instanceof bt) {
            pVar = bd.a();
        } else if (mVar instanceof bi) {
            pVar = bj.a();
        }
        if (pVar == null) {
            networkInitializationListener.onInitializationFailed(LoadingError.AdTypeNotSupportedInAdapter);
        } else {
            ag.a(activity, mVar, (com.appodeal.ads.ag.b) new com.appodeal.ads.ag.b() {
                public void a(int i, boolean z) {
                    pVar.a(mVar, i, z, true);
                }
            });
            networkInitializationListener.onInitializationFinished(new a());
        }
    }

    public void setLogging(boolean z) {
    }
}
