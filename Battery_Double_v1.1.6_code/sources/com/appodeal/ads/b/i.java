package com.appodeal.ads.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.AdType;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.UserData;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.bl;
import com.appodeal.ads.bq;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.Version;
import com.appodeal.ads.z;
import com.facebook.appevents.UserDataStore;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;

public class i {
    @VisibleForTesting
    static g a = null;
    @VisibleForTesting
    static Map<String, Object> b = null;
    @VisibleForTesting
    static JSONArray c = null;
    @VisibleForTesting
    static int d = -1;
    private static float i;
    private static boolean j;
    private static ArrayList<a> k = new ArrayList<>(AdType.values().length);
    private final Context e;
    private final UserData f;
    private final Float g;
    private final boolean h;

    public interface a {
        void a();
    }

    i(Context context, float f2, boolean z) {
        this.f = bq.r(context);
        this.e = context;
        this.g = Float.valueOf(f2);
        this.h = z;
    }

    public i(Context context, @Nullable JSONObject jSONObject) {
        this.f = bq.r(context);
        this.e = context;
        boolean z = false;
        if (jSONObject != null) {
            this.g = Float.valueOf((float) jSONObject.optDouble("inapp_amount", Utils.DOUBLE_EPSILON));
            if (jSONObject.has("inapp_amount") && this.g.floatValue() > 0.0f) {
                z = true;
            }
        } else {
            this.g = Float.valueOf(0.0f);
        }
        this.h = z;
        i = this.g.floatValue();
        j = this.h;
    }

    @VisibleForTesting
    static int a(Calendar calendar) {
        return ((calendar.get(7) - 1) * 24) + calendar.get(11);
    }

    @NonNull
    public static g a() {
        if (a == null) {
            a = new g(new JSONObject());
        }
        return a;
    }

    private Object a(String str) {
        if (str == null) {
            return null;
        }
        try {
            if (str.equals(UserDataStore.COUNTRY)) {
                return this.f.getCountryId();
            }
            if (str.equals(TapjoyConstants.TJC_APP_VERSION_NAME)) {
                return new Version(this.e.getPackageManager().getPackageInfo(this.e.getPackageName(), 0).versionName);
            }
            if (str.equals("app")) {
                return bl.a(this.e).b().getString(ServerResponseWrapper.APP_KEY_FIELD, null);
            }
            if (str.equals("sdk_version")) {
                return new Version("2.6.4");
            }
            if (str.equals(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME)) {
                return new Version(VERSION.RELEASE);
            }
            if (str.equals("session_count")) {
                return Integer.valueOf((int) Appodeal.getSession().d(this.e));
            }
            if (str.equals("average_session_length")) {
                return Integer.valueOf(k());
            }
            if (str.equals(TapjoyConstants.TJC_CONNECTION_TYPE)) {
                return i();
            }
            if (str.equals("gender")) {
                return j();
            }
            if (str.equals(IronSourceSegment.AGE)) {
                return h();
            }
            if (str.equals("bought_inapps")) {
                return e();
            }
            if (str.equals("inapp_amount")) {
                return d();
            }
            if (str.equals(TapjoyConstants.TJC_DEVICE_TYPE_NAME)) {
                return bq.k(this.e) ? "tablet" : PlaceFields.PHONE;
            } else if (b != null && b.containsKey(str)) {
                return b.get(str);
            } else {
                if (str.equals("session_time")) {
                    d = a(Calendar.getInstance());
                    return Integer.valueOf(d);
                }
                if (str.equals("part_of_audience")) {
                    return Integer.valueOf(f());
                }
                return null;
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    static void a(float f2) {
        i = f2;
        j = f2 > 0.0f;
    }

    static void a(@Nullable Context context) {
        if (context != null) {
            i iVar = new i(context, i, j);
            g gVar = null;
            try {
                if (c != null) {
                    gVar = iVar.a(c);
                }
                if (gVar == null) {
                    boolean z = (a == null || a.c() == -1) ? false : true;
                    c();
                    if (!z) {
                        return;
                    }
                } else if (a == null || gVar.c() != a.c()) {
                    gVar.a();
                    a(gVar);
                } else {
                    return;
                }
                g();
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
    }

    public static void a(@Nullable Context context, String str, Object obj) {
        if (b == null) {
            b = new HashMap();
        }
        b.put(str, obj);
        a(context);
    }

    public static void a(@NonNull g gVar) {
        String str;
        String str2;
        String str3;
        a = gVar;
        if (gVar.d == null || gVar.d.a == null) {
            str3 = "Segment";
            str2 = LogConstants.EVENT_SET;
            str = String.format("matched segment #%s", new Object[]{Long.valueOf(gVar.c())});
        } else {
            str3 = "Segment";
            str2 = LogConstants.EVENT_SET;
            str = String.format("matched segment #%s: %s", new Object[]{Long.valueOf(gVar.c()), gVar.d.a});
        }
        Log.log(str3, str2, str);
    }

    public static void a(@NonNull a aVar) {
        k.add(aVar);
    }

    private boolean a(Integer[] numArr, Integer num) {
        for (Integer equals : numArr) {
            if (equals.equals(num)) {
                return true;
            }
        }
        return false;
    }

    private boolean a(String[] strArr, String str) {
        for (String equals : strArr) {
            if (str.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    public static void b(@Nullable Context context) {
        if (c != null && a(Calendar.getInstance()) != d) {
            a(context);
        }
    }

    static boolean b() {
        return a().c() == -1;
    }

    public static void c() {
        Log.log("Segment", LogConstants.EVENT_SET, "matched default segment");
        a = null;
        e.a.clear();
    }

    private static void g() {
        z.a();
        Iterator it = k.iterator();
        while (it.hasNext()) {
            ((a) it.next()).a();
        }
    }

    private boolean g(f fVar, Object obj) {
        if (obj == null) {
            return false;
        }
        if (fVar.d == a.Unknown) {
            fVar.d = f.a(obj);
            fVar.a();
        }
        if (fVar.d == a.Unknown) {
            return false;
        }
        switch (fVar.b) {
            case IN:
                return f(fVar, obj);
            case EQUALS:
                return e(fVar, obj);
            case NOT_EQUALS:
                return !e(fVar, obj);
            case LESS:
                return c(fVar, obj);
            case MORE:
                return d(fVar, obj);
            case LESS_EQUALS:
                return b(fVar, obj);
            case MORE_EQUALS:
                return a(fVar, obj);
            default:
                return false;
        }
    }

    private Integer h() {
        return this.f.getAge();
    }

    private String i() {
        String str = bq.b(this.e).type;
        if (str != null) {
            if (str.equals(TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE)) {
                return TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE;
            }
            if (str.equals("wifi")) {
                return "wifi";
            }
        }
        return FacebookRequestErrorClassification.KEY_OTHER;
    }

    private String j() {
        Gender gender = this.f.getGender();
        return gender == null ? FacebookRequestErrorClassification.KEY_OTHER : gender == Gender.FEMALE ? "female" : gender == Gender.MALE ? "male" : FacebookRequestErrorClassification.KEY_OTHER;
    }

    private int k() {
        return (int) (Appodeal.getSession().e(this.e) / Appodeal.getSession().d(this.e));
    }

    public g a(JSONArray jSONArray) {
        int i2 = 0;
        while (i2 < jSONArray.length()) {
            try {
                g gVar = new g(jSONArray.getJSONObject(i2));
                if (b(gVar)) {
                    return gVar;
                }
                i2++;
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(f fVar, Object obj) {
        return e(fVar, obj) || d(fVar, obj);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(String str, int i2) {
        return i2 >= 0 && str.length() > i2 && str.charAt(i2) == '1';
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(f[] fVarArr) {
        for (f fVar : fVarArr) {
            if (!g(fVar, a(fVar.a))) {
                return false;
            }
        }
        return true;
    }

    public void b(@NonNull JSONArray jSONArray) {
        c = jSONArray;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(f fVar, Object obj) {
        return e(fVar, obj) || c(fVar, obj);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(g gVar) {
        switch (gVar.a) {
            case AND:
                return a(gVar.b);
            case OR:
                return b(gVar.b);
            default:
                return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(f[] fVarArr) {
        if (fVarArr.length == 0) {
            return true;
        }
        for (f fVar : fVarArr) {
            if (g(fVar, a(fVar.a))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean c(f fVar, Object obj) {
        boolean z = true;
        if (fVar.d == a.Float) {
            if (((Float) fVar.c).floatValue() <= ((Float) obj).floatValue()) {
                z = false;
            }
            return z;
        } else if (fVar.d == a.Integer) {
            if (((Integer) fVar.c).intValue() <= ((Integer) obj).intValue()) {
                z = false;
            }
            return z;
        } else if (fVar.d != a.Version) {
            return false;
        } else {
            if (((Version) fVar.c).compareTo((Version) obj) <= 0) {
                z = false;
            }
            return z;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Float d() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean d(f fVar, Object obj) {
        boolean z = true;
        if (fVar.d == a.Float) {
            if (((Float) fVar.c).floatValue() >= ((Float) obj).floatValue()) {
                z = false;
            }
            return z;
        } else if (fVar.d == a.Integer) {
            if (((Integer) fVar.c).intValue() >= ((Integer) obj).intValue()) {
                z = false;
            }
            return z;
        } else if (fVar.d != a.Version) {
            return false;
        } else {
            if (((Version) fVar.c).compareTo((Version) obj) >= 0) {
                z = false;
            }
            return z;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Boolean e() {
        return Boolean.valueOf(this.h);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean e(f fVar, Object obj) {
        boolean z = true;
        switch (fVar.d) {
            case Version:
                if (((Version) fVar.c).compareTo((Version) obj) != 0) {
                    z = false;
                }
                return z;
            case String:
                if (obj == null || !obj.equals(fVar.c)) {
                    z = false;
                }
                return z;
            case Integer:
            case Float:
            case Boolean:
                if (obj == null || !obj.equals(fVar.c)) {
                    z = false;
                }
                return z;
            default:
                return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int f() {
        SharedPreferences b2 = bl.a(this.e).b();
        int i2 = b2.getInt("part_of_audience", -1);
        if (i2 != -1) {
            return i2;
        }
        int nextInt = new Random().nextInt(100) + 1;
        b2.edit().putInt("part_of_audience", nextInt).apply();
        return nextInt;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean f(f fVar, Object obj) {
        int i2 = AnonymousClass1.c[fVar.d.ordinal()];
        if (i2 == 2) {
            return ((String) obj).toLowerCase().contains(((String) fVar.c).toLowerCase());
        }
        switch (i2) {
            case 6:
                return a((String[]) fVar.c, (String) obj);
            case 7:
                return a((Integer[]) fVar.c, (Integer) obj);
            case 8:
                return a((String) fVar.c, ((Integer) obj).intValue());
            default:
                return false;
        }
    }
}
