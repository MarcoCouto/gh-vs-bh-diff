package com.appodeal.ads.b;

import android.support.annotation.Nullable;
import com.smaato.sdk.core.api.VideoType;

public class h {

    public static class a {
        @Nullable
        public static String a(int i) {
            if (i == 128) {
                return "rewarded_video";
            }
            if (i == 256) {
                return "mrec";
            }
            if (i == 512) {
                return "native";
            }
            switch (i) {
                case 2:
                    return "video";
                case 3:
                    return VideoType.INTERSTITIAL;
                case 4:
                    return "banner";
                default:
                    return null;
            }
        }
    }
}
