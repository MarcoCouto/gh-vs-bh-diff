package com.appodeal.ads.b;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.m;
import com.github.mikephil.charting.utils.Utils;
import org.json.JSONObject;

public class a implements com.appodeal.ads.aq.a {
    @NonNull
    private final Context a;

    public a(@NonNull Context context) {
        this.a = context;
    }

    public void a(m mVar) {
    }

    public void a(JSONObject jSONObject, @Nullable m mVar, String str) {
        if (jSONObject.has("inapp_amount")) {
            i.a((float) jSONObject.optDouble("inapp_amount", Utils.DOUBLE_EPSILON));
            i.b(this.a);
        }
    }
}
