package com.appodeal.ads.b;

public enum c {
    LESS("<"),
    LESS_EQUALS("<="),
    EQUALS("=="),
    NOT_EQUALS("!="),
    MORE_EQUALS(">="),
    MORE(">"),
    IN("IN");
    
    private final String h;

    private c(String str) {
        this.h = str;
    }

    static c a(String str) {
        c[] values;
        for (c cVar : values()) {
            if (cVar.h.equals(str)) {
                return cVar;
            }
        }
        return null;
    }
}
