package com.appodeal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;

public class bl {
    private static Map<String, bl> a = new HashMap(5);
    private SharedPreferences b;

    private bl(Context context, String str) {
        this.b = context.getSharedPreferences(str, 0);
    }

    public static bl a(@NonNull Context context) {
        return a(context, "appodeal");
    }

    public static bl a(@NonNull Context context, String str) {
        bl blVar = (bl) a.get(str);
        if (blVar == null) {
            synchronized (bl.class) {
                blVar = (bl) a.get(str);
                if (blVar == null) {
                    blVar = new bl(context, str);
                    a.put(str, blVar);
                }
            }
        }
        return blVar;
    }

    public Editor a() {
        return this.b.edit();
    }

    public SharedPreferences b() {
        return this.b;
    }
}
