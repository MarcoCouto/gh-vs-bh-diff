package com.appodeal.ads;

import android.support.annotation.NonNull;
import com.appodeal.ads.b.d;

class bw extends l {
    @NonNull
    final b c;
    final boolean d;

    bw(@NonNull d dVar, @NonNull b bVar) {
        this(dVar, bVar, false, false);
    }

    bw(@NonNull d dVar, @NonNull b bVar, boolean z, boolean z2) {
        super(dVar, z2);
        this.c = bVar;
        this.d = z;
    }
}
