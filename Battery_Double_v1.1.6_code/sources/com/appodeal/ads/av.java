package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;

class av extends bu<aw, UnifiedMrec, UnifiedMrecParams, UnifiedMrecCallback> {

    private final class a extends UnifiedMrecCallback {
        private a() {
        }

        public void onAdClicked() {
            at.b().a(av.this.a(), av.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            at.b().a(av.this.a(), av.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdExpired() {
            at.b().i(av.this.a(), av.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            av.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            at.b().b(av.this.a(), av.this, loadingError);
        }

        public void onAdLoaded(View view) {
            av.this.a(view);
            at.b().b(av.this.a(), av.this);
        }

        public void onAdShowFailed() {
            at.b().a(av.this.a(), av.this, null, LoadingError.ShowFailed);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((aw) av.this.a()).a((AdUnit) av.this, str, obj);
        }
    }

    private final class b implements UnifiedMrecParams {
        private b() {
        }

        public String obtainPlacementId() {
            return at.a().u();
        }

        public String obtainSegmentId() {
            return at.a().s();
        }
    }

    av(@NonNull aw awVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
        super(awVar, adNetwork, bnVar, 5000);
    }

    /* access modifiers changed from: protected */
    public int b(Context context) {
        return -1;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedMrec a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createMrec();
    }

    /* access modifiers changed from: protected */
    public int c(Context context) {
        return -2;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedMrecParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedMrecCallback o() {
        return new a();
    }
}
