package com.appodeal.ads;

public class ConnectionData {
    public final boolean isFast;
    public final String subType;
    public final String type;

    ConnectionData(String str, String str2, boolean z) {
        this.type = str;
        this.subType = str2;
        this.isFast = z;
    }
}
