package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseArray;
import com.appodeal.ads.unified.UnifiedAppStateChangeListener;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.DependencyRule;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.aa;
import com.appodeal.ads.utils.app.AppState;
import com.appodeal.ads.utils.app.b;
import com.appodeal.ads.utils.c;
import com.appodeal.ads.utils.k;
import com.smaato.sdk.core.api.VideoType;
import com.tapjoy.TapjoyConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {
    @VisibleForTesting
    static SparseArray<h> a = new SparseArray<>();
    static final /* synthetic */ boolean d = (!h.class.desiredAssertionStatus());
    private static final aa e = new aa() {
        public void a(@Nullable Activity activity, @NonNull AppState appState) {
            for (UnifiedAppStateChangeListener onAppStateChanged : h.k.values()) {
                onAppStateChanged.onAppStateChanged(activity, appState, c.b(activity));
            }
        }

        public void a(Configuration configuration) {
            for (UnifiedAppStateChangeListener onAppStateChanged : h.k.values()) {
                onAppStateChanged.onAppStateChanged(Appodeal.e, AppState.ConfChanged, c.b(Appodeal.e));
            }
        }
    };
    private static final Set<String> f = new CopyOnWriteArraySet();
    private static final List<String> j = new ArrayList();
    /* access modifiers changed from: private */
    public static final Map<String, UnifiedAppStateChangeListener> k = new ConcurrentHashMap();
    private static AtomicBoolean n = new AtomicBoolean(false);
    @VisibleForTesting
    final Map<String, AdNetwork> b = new ConcurrentHashMap();
    @VisibleForTesting
    final Map<String, AdNetworkBuilder> c = new ConcurrentHashMap();
    private final Map<String, Pair<String, String>> g = new ConcurrentHashMap();
    private final Map<String, AdNetworkBuilder> h = new ConcurrentHashMap();
    private final Set<String> i = new CopyOnWriteArraySet();
    private final AtomicBoolean l = new AtomicBoolean(false);
    private final AtomicBoolean m = new AtomicBoolean(false);

    interface a {
        void a(@NonNull AdNetworkBuilder adNetworkBuilder);

        void a(@NonNull String str);
    }

    static {
        b.All.a((com.appodeal.ads.utils.app.a) e);
    }

    public static h a(int i2) {
        h hVar = (h) a.get(i2);
        if (hVar == null) {
            synchronized (h.class) {
                hVar = (h) a.get(i2);
                if (hVar == null) {
                    hVar = new h();
                    a.put(i2, hVar);
                }
            }
        }
        return hVar;
    }

    private static String a(@NonNull InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                    sb.append(10);
                } else {
                    try {
                        break;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (IOException e3) {
                e3.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    private void a(@NonNull Context context, @NonNull String str, @NonNull AdNetworkBuilder adNetworkBuilder, @Nullable a aVar) {
        if (b(str)) {
            d(context, str, adNetworkBuilder, aVar);
        } else if (bq.b(context, str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".dx");
            String sb2 = sb.toString();
            String adapterVersion = adNetworkBuilder.getAdapterVersion();
            String[] requiredClasses = adNetworkBuilder.getRequiredClasses();
            final Context context2 = context;
            final String str2 = str;
            final AdNetworkBuilder adNetworkBuilder2 = adNetworkBuilder;
            final a aVar2 = aVar;
            AnonymousClass5 r3 = new com.appodeal.ads.utils.k.a() {
                public void a(boolean z) {
                    if (z) {
                        h.this.b(context2, str2, adNetworkBuilder2, aVar2);
                    } else {
                        h.this.d(context2, str2, adNetworkBuilder2, aVar2);
                    }
                }
            };
            k.a(context, sb2, adapterVersion, requiredClasses, r3);
        } else {
            b(context, str, adNetworkBuilder, aVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Context context, @NonNull String str, @NonNull String str2, @Nullable a aVar) {
        try {
            if (!bq.a(str2)) {
                d(context, str, null, aVar);
                Log.log(new com.appodeal.ads.utils.b.a(String.format("Failed to load classes for network: %s", new Object[]{bq.c(str.split("\\.")[0])})));
                return;
            }
            b(context, str, (AdNetworkBuilder) Class.forName(str2).newInstance(), aVar);
        } catch (Exception e2) {
            Log.log(e2);
            d(context, str, null, aVar);
        }
    }

    private void a(@NonNull Context context, @NonNull String str, @NonNull String str2, @NonNull String str3, @Nullable a aVar) {
        if (b(str)) {
            d(context, str, null, aVar);
        } else if (bq.b(context, str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".dx");
            String sb2 = sb.toString();
            String[] strArr = {str2};
            final Context context2 = context;
            final String str4 = str;
            final String str5 = str2;
            final a aVar2 = aVar;
            AnonymousClass4 r3 = new com.appodeal.ads.utils.k.a() {
                public void a(boolean z) {
                    if (z) {
                        h.this.a(context2, str4, str5, aVar2);
                    } else {
                        h.this.d(context2, str4, null, aVar2);
                    }
                }
            };
            k.a(context, sb2, str3, strArr, r3);
        } else {
            a(context, str, str2, aVar);
        }
    }

    private static void a(@NonNull String str, @NonNull AdNetworkBuilder adNetworkBuilder) {
        DependencyRule[] optionalClasses;
        if (!f.contains(str)) {
            f.add(str);
            for (DependencyRule dependencyRule : adNetworkBuilder.getOptionalClasses()) {
                if (!bq.a(dependencyRule.getDependency())) {
                    StringBuilder sb = new StringBuilder("WARNING: ");
                    sb.append(bq.c(adNetworkBuilder.getName()));
                    sb.append(" - ");
                    sb.append(dependencyRule.getDependency());
                    sb.append(" did not found");
                    if (!TextUtils.isEmpty(dependencyRule.getErrorMessage())) {
                        sb.append(". ");
                        sb.append(dependencyRule.getErrorMessage());
                    }
                    android.util.Log.e(Appodeal.a, sb.toString());
                }
            }
        }
    }

    public static boolean a(int i2, String str) {
        h hVar = (h) a.get(i2);
        return hVar == null || hVar.d(str);
    }

    /* access modifiers changed from: private */
    public void b(@NonNull Context context, @NonNull String str, @NonNull AdNetworkBuilder adNetworkBuilder, @Nullable a aVar) {
        if (b(str)) {
            d(context, str, adNetworkBuilder, aVar);
        } else if (!bq.a(adNetworkBuilder.getRequiredClasses())) {
            d(context, str, adNetworkBuilder, aVar);
            Log.log(new com.appodeal.ads.utils.b.a(String.format("Failed to load classes for network: %s", new Object[]{bq.c(str.split("\\.")[0])})));
        } else {
            a(str, adNetworkBuilder);
            if (c.a(context, adNetworkBuilder)) {
                c(context, str, adNetworkBuilder, aVar);
            } else {
                if (!adNetworkBuilder.isOptional()) {
                    String format = String.format("%s not found", new Object[]{bq.c(str)});
                    Log.log(LogConstants.KEY_NETWORK, "Error", format);
                    bq.c(context, String.format("ERROR: %s", new Object[]{format}));
                }
                d(context, str, adNetworkBuilder, aVar);
            }
        }
    }

    static synchronized void c(@NonNull Context context) {
        synchronized (h.class) {
            if (!n.get()) {
                n.set(true);
                try {
                    String[] list = context.getAssets().list("apd_adapters");
                    if (list != null) {
                        if (list.length != 0) {
                            for (String str : list) {
                                if (str.endsWith(".apdnetwork")) {
                                    try {
                                        AssetManager assets = context.getAssets();
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("apd_adapters/");
                                        sb.append(str);
                                        JSONObject jSONObject = new JSONObject(a(assets.open(sb.toString())));
                                        String optString = jSONObject.optString("name");
                                        String optString2 = jSONObject.optString("builder");
                                        String optString3 = jSONObject.optString(TapjoyConstants.TJC_ADAPTER_VERSION);
                                        JSONArray optJSONArray = jSONObject.optJSONArray("types");
                                        if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && optJSONArray != null && optJSONArray.length() > 0) {
                                            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                                                String optString4 = optJSONArray.optString(i2);
                                                int i3 = "banner".equals(optString4) ? 4 : "mrec".equals(optString4) ? 256 : "rewarded_video".equals(optString4) ? 128 : VideoType.INTERSTITIAL.equals(optString4) ? 1 : "video".equals(optString4) ? 2 : "native".equals(optString4) ? 512 : -1;
                                                if (i3 != -1) {
                                                    a(i3).a(optString, optString2, optString3);
                                                }
                                            }
                                        }
                                    } catch (JSONException e2) {
                                        Log.log(e2);
                                    }
                                }
                            }
                        }
                    }
                    Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_INITIALIZE, "No adapters found in app assets");
                    return;
                } catch (IOException e3) {
                    Log.log(e3);
                }
            } else {
                return;
            }
        }
        return;
    }

    private void c(@NonNull Context context, @NonNull String str, @NonNull AdNetworkBuilder adNetworkBuilder, @Nullable a aVar) {
        AdNetwork build = adNetworkBuilder.build();
        if (build != null) {
            this.b.put(str, build);
            synchronized (j) {
                if (!j.contains(str)) {
                    Log.log(LogConstants.KEY_NETWORK, LogConstants.EVENT_INFO, String.format("%s - ver. %s", new Object[]{bq.c(build.getName()), build.getVersion()}), LogLevel.verbose);
                    c.a.addAll(Arrays.asList(ActivityRule.a(build.getAdActivityRules())));
                    j.add(str);
                    UnifiedAppStateChangeListener appStateChangeListener = build.getAppStateChangeListener();
                    if (appStateChangeListener != null) {
                        k.put(str, appStateChangeListener);
                    }
                }
            }
            if (aVar != null) {
                aVar.a(adNetworkBuilder);
            }
        } else {
            d(context, str, adNetworkBuilder, aVar);
        }
        d(context);
    }

    private void d(@NonNull Context context) {
        if (this.i.size() + this.b.size() == this.g.size()) {
            c.a(context, this.b.values(), this.c.values());
        }
    }

    /* access modifiers changed from: private */
    public void d(@NonNull Context context, @NonNull String str, @Nullable AdNetworkBuilder adNetworkBuilder, @Nullable a aVar) {
        if (adNetworkBuilder != null) {
            this.c.put(str, adNetworkBuilder);
        }
        a(str);
        if (aVar != null) {
            aVar.a(str);
        }
        d(context);
    }

    @WorkerThread
    public final synchronized h a(@NonNull final Context context) {
        synchronized (this.l) {
            if (!this.l.get()) {
                this.l.set(true);
                new Thread() {
                    public void run() {
                        super.run();
                        h.this.b(context);
                    }
                }.start();
            }
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Collection<AdNetwork> a() {
        return this.b.values();
    }

    public void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            if (d || str != null) {
                this.i.add(str);
                this.b.remove(str);
                return;
            }
            throw new AssertionError();
        }
    }

    public final void a(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        this.g.put(str, Pair.create(str2, str3));
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final synchronized h b(@NonNull Context context) {
        synchronized (this.m) {
            if (this.m.get()) {
                return this;
            }
            c(context);
            final CountDownLatch countDownLatch = new CountDownLatch(this.g.size() + this.h.size());
            AnonymousClass3 r2 = new a() {
                public void a(@NonNull AdNetworkBuilder adNetworkBuilder) {
                    countDownLatch.countDown();
                }

                public void a(@NonNull String str) {
                    countDownLatch.countDown();
                }
            };
            for (Entry entry : this.g.entrySet()) {
                a(context, (String) entry.getKey(), (String) ((Pair) entry.getValue()).first, (String) ((Pair) entry.getValue()).second, (a) r2);
            }
            for (Entry entry2 : this.h.entrySet()) {
                a(context, (String) entry2.getKey(), (AdNetworkBuilder) entry2.getValue(), (a) r2);
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            ah.b bVar = new ah.b();
            b(context, bVar.getName(), bVar, r2);
            this.m.set(true);
            return this;
        }
    }

    @NonNull
    public Set<String> b() {
        Set<String> keySet = this.g.keySet();
        keySet.removeAll(this.i);
        return keySet;
    }

    public final boolean b(@Nullable String str) {
        return this.i.contains(str);
    }

    @Nullable
    public AdNetwork c(@NonNull String str) {
        return (AdNetwork) this.b.get(str);
    }

    public boolean d(String str) {
        return b(str) || !this.g.containsKey(str) || !this.h.containsKey(str);
    }
}
