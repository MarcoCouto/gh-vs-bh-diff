package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import com.appodeal.ads.i;
import com.appodeal.ads.l;
import com.appodeal.ads.m;
import com.appodeal.ads.utils.LogConstants;

abstract class k<AdRequestType extends m<AdObjectType>, AdObjectType extends i, RendererParams extends l> {
    @NonNull
    final String a;

    static class a {
        static final a a = new a(LogConstants.EVENT_SHOW_FAILED, LogConstants.MSG_NOT_INITIALIZED);
        static final a b = new a(LogConstants.EVENT_SHOW_FAILED, LogConstants.EVENT_NETWORK_CONNECTION);
        static final a c = new a(LogConstants.EVENT_SHOW_FAILED, "Pause");
        static final a d = new a(LogConstants.EVENT_SHOW_FAILED, "disabled");
        static final a e = new a(LogConstants.EVENT_SHOW_FAILED, LogConstants.MSG_AD_TYPE_DISABLED_BY_SEGMENT);
        @NonNull
        final String f;
        @NonNull
        final String g;

        a(@NonNull String str, @NonNull String str2) {
            this.f = str;
            this.g = str2;
        }
    }

    k(@NonNull String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void a(@NonNull Activity activity, @NonNull RendererParams rendererparams, @NonNull p<AdObjectType, AdRequestType, ?> pVar, @NonNull a aVar) {
        pVar.a(aVar.f, aVar.g);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull Activity activity, @NonNull RendererParams rendererparams, @NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        a aVar;
        if (!pVar.l()) {
            aVar = a.a;
        } else {
            pVar.a(rendererparams.a);
            if (pVar.j()) {
                aVar = a.d;
            } else if (pVar.k()) {
                aVar = a.e;
            } else if (Appodeal.d) {
                aVar = a.c;
            } else if (bq.a((Context) activity)) {
                return b(activity, rendererparams, pVar);
            } else {
                aVar = a.b;
            }
        }
        a(activity, rendererparams, pVar, aVar);
        return false;
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean a(@NonNull Activity activity, @NonNull p<AdObjectType, AdRequestType, ?> pVar);

    /* access modifiers changed from: 0000 */
    public abstract boolean b(@NonNull Activity activity, @NonNull RendererParams rendererparams, @NonNull p<AdObjectType, AdRequestType, ?> pVar);
}
