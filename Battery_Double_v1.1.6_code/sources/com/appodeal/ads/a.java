package com.appodeal.ads;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.b;
import com.appodeal.ads.utils.b.d;
import com.appodeal.ads.utils.o;
import java.util.Collection;

class a implements com.appodeal.ads.utils.app.a {
    /* access modifiers changed from: private */
    public long a;
    /* access modifiers changed from: private */
    public long b;

    a() {
    }

    private void a() {
        a(aa.b());
        a(at.a());
        a(am.a());
        a(bd.a());
        a(bj.a());
        o.b((Collection<i>) Native.c().d());
    }

    private void a(p pVar) {
        m y = pVar.y();
        if (y != null && !y.t()) {
            o.b(y.B());
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        b(aa.b());
        b(at.a());
        b(am.a());
        b(bd.a());
        b(bj.a());
        o.c((Collection<i>) Native.c().d());
    }

    private void b(p pVar) {
        m y = pVar.y();
        if (y != null) {
            o.c(y.B());
        }
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        Appodeal.e = activity;
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        Appodeal.getSession().c(activity);
        try {
            aa.a().a(activity);
            this.a = System.currentTimeMillis();
            final long j = this.a;
            bq.a((Runnable) new Runnable() {
                public void run() {
                    if (j == a.this.a && a.this.b < a.this.a) {
                        Log.log(LogConstants.KEY_SDK, "Pause");
                        Appodeal.d = true;
                        a.this.b();
                        Appodeal.c().b(Appodeal.f);
                    }
                }
            }, 1000);
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public void onActivityResumed(Activity activity) {
        Appodeal.e = activity;
        Appodeal.getSession().b(activity);
        try {
            this.b = System.currentTimeMillis();
            if (Appodeal.d) {
                Appodeal.d = false;
                Appodeal.c().a(activity);
                a();
                b.a(activity, new d() {
                    public void a(@NonNull com.appodeal.ads.utils.b.a aVar) {
                        if (be.a(aVar)) {
                            z.a();
                        }
                    }
                }, null);
                am.a().a(activity);
                bd.a().a(activity);
                bj.a().a(activity);
                aa.b().a(activity);
                at.a().a(activity);
                Native.a().a(activity);
                Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_RESUME);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onConfigurationChanged(Configuration configuration) {
        ad adVar = (ad) aa.b().y();
        if (adVar == null) {
            return;
        }
        if ((adVar.a(configuration) || !adVar.h()) && aa.a().f() == u.VISIBLE) {
            adVar.i(false);
            aa.a(Appodeal.f, new d(aa.a().d()));
        }
    }

    public void onLowMemory() {
    }
}
