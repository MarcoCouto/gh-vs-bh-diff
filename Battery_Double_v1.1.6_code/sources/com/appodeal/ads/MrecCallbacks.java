package com.appodeal.ads;

public interface MrecCallbacks {
    void onMrecClicked();

    void onMrecExpired();

    void onMrecFailedToLoad();

    void onMrecLoaded(boolean z);

    void onMrecShowFailed();

    void onMrecShown();
}
