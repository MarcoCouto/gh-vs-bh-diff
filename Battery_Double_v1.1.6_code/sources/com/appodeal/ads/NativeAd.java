package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

public interface NativeAd {
    boolean canShow(@NonNull Context context, @NonNull String str);

    boolean containsVideo();

    void destroy();

    String getAdProvider();

    String getAgeRestrictions();

    String getCallToAction();

    String getDescription();

    double getPredictedEcpm();

    View getProviderView(Context context);

    float getRating();

    String getTitle();

    boolean isPrecache();
}
