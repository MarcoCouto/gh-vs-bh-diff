package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.appodeal.ads.b.e;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.o;
import java.util.Map.Entry;
import org.json.JSONObject;

final class at {
    static final au a = new au();
    @VisibleForTesting
    static c b;
    @VisibleForTesting
    static b c;
    /* access modifiers changed from: private */
    public static Integer d = null;
    @SuppressLint({"StaticFieldLeak"})
    private static a e;

    static class a extends bv<aw, av> {
        a() {
            super("debug_mrec", b.VIEW);
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull Activity activity, @NonNull b bVar) {
            at.a((Context) activity, new d(true));
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view) {
            return view instanceof MrecView;
        }
    }

    @VisibleForTesting(otherwise = 3)
    static class b extends p<av, aw, d> {
        b(q<av, aw, ?> qVar) {
            super(qVar, e.c(), 256);
        }

        /* access modifiers changed from: protected */
        public av a(@NonNull aw awVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
            return new av(awVar, adNetwork, bnVar);
        }

        /* access modifiers changed from: protected */
        public aw a(d dVar) {
            return new aw(dVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                aw awVar = (aw) y();
                if (awVar == null || (awVar.M() && !awVar.t())) {
                    u f = at.d().f();
                    if (f == u.HIDDEN || f == u.NEVER_SHOWN) {
                        d((Context) activity);
                    } else {
                        at.a((Context) activity, new d(true));
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("refresh_period")) {
                at.d = Integer.valueOf(jSONObject.optInt("refresh_period") * 1000);
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return super.c() && y() == null;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            at.a(context, at.b(at.d().c()));
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "mrec_disabled";
        }
    }

    @VisibleForTesting
    static class c extends bc<av, aw> {
        c() {
            super(at.a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(aw awVar) {
            at.a(awVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void q(final aw awVar, av avVar) {
            if (!awVar.a() && this.a.r()) {
                aw awVar2 = (aw) this.a.y();
                if (awVar2 == null || awVar2.M()) {
                    this.a.d(Appodeal.f);
                }
            }
            if (this.a.r()) {
                bq.a((Runnable) new Runnable() {
                    public void run() {
                        try {
                            u f = at.d().f();
                            if (c.this.a.c(c.this.a.b(awVar)) && f != u.HIDDEN && f != u.NEVER_SHOWN) {
                                View e = at.d().e();
                                if (e != null && e.isShown()) {
                                    if (!com.appodeal.ads.utils.c.b(Appodeal.e)) {
                                        at.a(Appodeal.e, new bw(c.this.a.t(), b.VIEW));
                                    } else {
                                        bq.a((Runnable) this, 1000);
                                    }
                                }
                            }
                        } catch (Exception e2) {
                            Log.log(e2);
                        }
                    }
                }, (long) at.f().intValue());
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(aw awVar, av avVar, boolean z) {
            super.b(awVar, avVar, z);
            if (d(awVar, avVar) && !com.appodeal.ads.utils.c.b(Appodeal.e)) {
                at.a(Appodeal.e, new bw(this.a.t(), b.VIEW, true, false));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(aw awVar, av avVar, Object obj) {
            return super.j(awVar, avVar, obj) && this.a.D() > 0;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(aw awVar) {
            at.a(awVar, 0, false, true);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void j(aw awVar, av avVar) {
            if (this.a.r()) {
                u f = at.d().f();
                if (f == u.HIDDEN || f == u.NEVER_SHOWN) {
                    this.a.d(Appodeal.f);
                } else {
                    at.a(Appodeal.f, new d(true));
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void d(@Nullable aw awVar) {
            if (awVar != null) {
                if (awVar.M()) {
                    u f = at.d().f();
                    if (!(f == u.HIDDEN || f == u.NEVER_SHOWN)) {
                        at.a(Appodeal.f, new d(true));
                        return;
                    }
                } else {
                    return;
                }
            }
            this.a.d(Appodeal.f);
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public boolean e(aw awVar, av avVar) {
            return super.e(awVar, avVar) && !d(awVar, avVar);
        }

        /* access modifiers changed from: protected */
        public boolean d(aw awVar, av avVar) {
            return awVar.u() && !avVar.h();
        }
    }

    static class d extends n<d> {
        d() {
            super("banner_mrec", "debug_mrec");
        }

        d(boolean z) {
            this();
            b(z);
        }
    }

    static p<av, aw, d> a() {
        if (c == null) {
            c = new b(b());
        }
        return c;
    }

    static void a(Activity activity) {
        d().a(activity, a());
    }

    static void a(Context context, d dVar) {
        a().b(context, dVar);
    }

    static void a(aw awVar, int i, boolean z, boolean z2) {
        a().a(awVar, i, z2, z);
    }

    static boolean a(Activity activity, bw bwVar) {
        return d().a(activity, bwVar, a());
    }

    /* access modifiers changed from: private */
    public static d b(@Nullable b bVar) {
        return bVar != null ? new d(true) : new d();
    }

    static q<av, aw, ?> b() {
        if (b == null) {
            b = new c();
        }
        return b;
    }

    static void c() {
        if (d().f() != u.VISIBLE) {
            Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_AD_DESTROY, LogLevel.debug);
            aw awVar = (aw) a().y();
            if (awVar != null) {
                if (awVar.B() != null) {
                    o.a(awVar.B());
                    ((av) awVar.B()).q();
                }
                for (Entry value : awVar.F().entrySet()) {
                    i iVar = (i) value.getValue();
                    if (iVar != null) {
                        o.a(iVar);
                        iVar.q();
                    }
                }
                b().g(awVar);
                awVar.Q();
                awVar.R();
            }
        }
    }

    static a d() {
        if (e == null) {
            e = new a();
        }
        return e;
    }

    /* access modifiers changed from: private */
    public static Integer f() {
        int i;
        com.appodeal.ads.b.d t = a().t();
        if (t == null || t.c() <= 0) {
            if (d == null) {
                i = 15000;
            }
            return d;
        }
        i = t.c();
        d = Integer.valueOf(i);
        return d;
    }
}
