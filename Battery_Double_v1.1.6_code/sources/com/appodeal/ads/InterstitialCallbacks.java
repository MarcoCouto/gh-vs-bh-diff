package com.appodeal.ads;

public interface InterstitialCallbacks {
    void onInterstitialClicked();

    void onInterstitialClosed();

    void onInterstitialExpired();

    void onInterstitialFailedToLoad();

    void onInterstitialLoaded(boolean z);

    void onInterstitialShowFailed();

    void onInterstitialShown();
}
