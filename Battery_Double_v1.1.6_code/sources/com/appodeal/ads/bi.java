package com.appodeal.ads;

import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import org.json.JSONException;
import org.json.JSONObject;

class bi extends m<bh> {
    bi(@Nullable n nVar) {
        super(nVar);
    }

    public AdType T() {
        return AdType.Rewarded;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void l(bh bhVar) {
        super.l(bhVar);
        try {
            a(new JSONObject().put("type", "rewarded_video"));
        } catch (JSONException e) {
            Log.log(e);
        }
    }
}
