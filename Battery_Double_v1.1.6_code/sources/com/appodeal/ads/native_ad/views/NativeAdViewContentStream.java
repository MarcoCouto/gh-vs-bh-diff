package com.appodeal.ads.native_ad.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeIconView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.bq;

public class NativeAdViewContentStream extends a {
    final int B = 5;
    final int C = 20;
    final int D = 50;
    final int E = 10;
    final int F = 5;
    final int G = 5;
    final int H = 16;
    final int I = 12;
    final int J = 10;
    final int K = 3;
    final int L = 5;

    public NativeAdViewContentStream(Context context) {
        super(context);
    }

    public NativeAdViewContentStream(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeAdViewContentStream(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public NativeAdViewContentStream(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    public NativeAdViewContentStream(Context context, NativeAd nativeAd) {
        super(context, nativeAd, "default");
    }

    public NativeAdViewContentStream(Context context, NativeAd nativeAd, String str) {
        super(context, nativeAd, str);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        LinearLayout linearLayout;
        int i;
        NativeIconView nativeIconView;
        int i2;
        View view;
        int i3;
        View view2;
        int i4;
        NativeMediaView nativeMediaView;
        int i5;
        View view3;
        int i6;
        View view4;
        int i7;
        if (!this.n) {
            TypedArray obtainStyledAttributes = this.m.obtainStyledAttributes(new int[]{16843534});
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            if (VERSION.SDK_INT >= 16) {
                setBackground(drawable);
            } else {
                setBackgroundDrawable(drawable);
            }
            this.h = new RelativeLayout(this.m);
            this.h.setLayoutParams(new LayoutParams(-1, -1));
            int round = Math.round(bq.h(this.m) * 5.0f);
            this.h.setPadding(round, round, round, round);
            this.h.setVisibility(8);
            addView(this.h);
            RelativeLayout relativeLayout = new RelativeLayout(this.m);
            relativeLayout.setLayoutParams(new LayoutParams(-1, -2));
            relativeLayout.setId(VERSION.SDK_INT >= 17 ? View.generateViewId() : 78);
            this.k = new LinearLayout(this.m);
            this.k.setOrientation(0);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(11);
            layoutParams.addRule(10);
            this.k.setLayoutParams(layoutParams);
            if (VERSION.SDK_INT >= 17) {
                linearLayout = this.k;
                i = View.generateViewId();
            } else {
                linearLayout = this.k;
                i = 77;
            }
            linearLayout.setId(i);
            relativeLayout.addView(this.k);
            this.j = new TextView(this.m);
            this.j.setTextSize(2, 10.0f);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.gravity = 16;
            this.j.setLayoutParams(layoutParams2);
            c();
            this.k.addView(this.j);
            this.i = new RelativeLayout(this.m);
            this.i.setLayoutParams(new LayoutParams(-2, Math.round(bq.h(this.m) * 20.0f)));
            this.k.addView(this.i);
            this.f = new NativeIconView(this.m);
            this.o = Math.round(bq.h(this.m) * 50.0f);
            LayoutParams layoutParams3 = new LayoutParams(this.o, this.o);
            layoutParams3.setMargins(0, 0, Math.round(bq.h(this.m) * 10.0f), 0);
            layoutParams3.addRule(10);
            layoutParams3.addRule(9);
            this.f.setLayoutParams(layoutParams3);
            if (VERSION.SDK_INT >= 17) {
                nativeIconView = this.f;
                i2 = View.generateViewId();
            } else {
                nativeIconView = this.f;
                i2 = 71;
            }
            nativeIconView.setId(i2);
            relativeLayout.addView(this.f);
            this.a = new TextView(this.m);
            ((TextView) this.a).setTextSize(2, 16.0f);
            LayoutParams layoutParams4 = new LayoutParams(-2, -2);
            layoutParams4.setMargins(0, 0, 0, Math.round(bq.h(this.m) * 5.0f));
            layoutParams4.addRule(1, this.f.getId());
            layoutParams4.addRule(0, this.k.getId());
            this.a.setLayoutParams(layoutParams4);
            if (VERSION.SDK_INT >= 17) {
                view = this.a;
                i3 = View.generateViewId();
            } else {
                view = this.a;
                i3 = 72;
            }
            view.setId(i3);
            relativeLayout.addView(this.a);
            this.c = new RatingBar(this.m, null, 16842877);
            this.c.setVisibility(8);
            LayoutParams layoutParams5 = new LayoutParams(-2, -2);
            layoutParams5.addRule(1, this.f.getId());
            layoutParams5.addRule(3, this.a.getId());
            this.c.setLayoutParams(layoutParams5);
            if (VERSION.SDK_INT >= 17) {
                view2 = this.c;
                i4 = View.generateViewId();
            } else {
                view2 = this.c;
                i4 = 74;
            }
            view2.setId(i4);
            relativeLayout.addView(this.c);
            this.h.addView(relativeLayout);
            this.g = new NativeMediaView(this.m);
            LayoutParams layoutParams6 = new LayoutParams(-1, -1);
            int round2 = Math.round(bq.h(this.m) * 10.0f);
            layoutParams6.setMargins(0, round2, 0, round2);
            layoutParams6.addRule(3, relativeLayout.getId());
            this.g.setLayoutParams(layoutParams6);
            if (VERSION.SDK_INT >= 17) {
                nativeMediaView = this.g;
                i5 = View.generateViewId();
            } else {
                nativeMediaView = this.g;
                i5 = 76;
            }
            nativeMediaView.setId(i5);
            this.h.addView(this.g);
            this.b = new TextView(this.m);
            LayoutParams layoutParams7 = new LayoutParams(-2, -2);
            int round3 = Math.round(bq.h(this.m) * 3.0f);
            int round4 = Math.round(bq.h(this.m) * 5.0f);
            layoutParams7.setMargins(round3, 0, 3, 3);
            layoutParams7.addRule(11);
            layoutParams7.addRule(3, this.g.getId());
            this.b.setLayoutParams(layoutParams7);
            this.b.setPadding(round4, round4, round4, round4);
            if (VERSION.SDK_INT >= 17) {
                view3 = this.b;
                i6 = View.generateViewId();
            } else {
                view3 = this.b;
                i6 = 75;
            }
            view3.setId(i6);
            d();
            this.h.addView(this.b);
            this.d = new TextView(this.m);
            ((TextView) this.d).setTextSize(2, 12.0f);
            LayoutParams layoutParams8 = new LayoutParams(-1, -2);
            layoutParams8.setMargins(Math.round(bq.h(this.m) * 5.0f), 0, 0, 0);
            layoutParams8.addRule(0, this.b.getId());
            layoutParams8.addRule(3, this.g.getId());
            this.d.setLayoutParams(layoutParams8);
            ((TextView) this.d).setMaxLines(2);
            ((TextView) this.d).setMinLines(2);
            if (VERSION.SDK_INT >= 17) {
                view4 = this.d;
                i7 = View.generateViewId();
            } else {
                view4 = this.d;
                i7 = 73;
            }
            view4.setId(i7);
            this.h.addView(this.d);
            this.n = true;
        }
        b();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.l != null) {
            this.f.measure(getWidth(), getHeight());
            ((TextView) this.a).setText(this.l.getTitle());
            this.a.measure(getWidth(), getHeight());
            ((TextView) this.d).setText(this.l.getDescription());
            this.d.measure(getWidth(), getHeight());
            if (this.l.getRating() > 0.0f) {
                ((RatingBar) this.c).setRating(this.l.getRating());
                this.c.setVisibility(0);
            } else {
                this.c.setVisibility(8);
            }
            this.c.measure(getWidth(), getHeight());
            if (this.l.getCallToAction() == null || this.l.getCallToAction().isEmpty() || this.l.getCallToAction().equals("")) {
                this.b.setVisibility(8);
            } else {
                ((TextView) this.b).setText(this.l.getCallToAction());
                this.b.setVisibility(0);
            }
            this.b.measure(getWidth(), getHeight());
            if (!(Math.max(this.f.getMeasuredHeight(), this.a.getMeasuredHeight() + this.c.getMeasuredHeight()) + this.g.getMeasuredHeight() + Math.max(this.d.getMeasuredHeight(), this.b.getMeasuredHeight()) <= getHeight() || getLayoutParams() == null || getLayoutParams().height == -2 || getLayoutParams().height == -1)) {
                this.g.setVisibility(8);
            }
            this.e = this.l.getProviderView(this.m);
            if (this.e != null) {
                if (this.e.getParent() != null && (this.e.getParent() instanceof ViewGroup)) {
                    ((ViewGroup) this.e.getParent()).removeView(this.e);
                }
                this.i.removeAllViews();
                this.i.addView(this.e, new ViewGroup.LayoutParams(-2, -2));
            } else if (this.i != null) {
                this.i.setVisibility(8);
            }
            registerView(this.l, this.s);
            this.h.setVisibility(0);
        }
    }
}
