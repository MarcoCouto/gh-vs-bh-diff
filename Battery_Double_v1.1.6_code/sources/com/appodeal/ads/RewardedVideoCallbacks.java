package com.appodeal.ads;

public interface RewardedVideoCallbacks {
    void onRewardedVideoClicked();

    void onRewardedVideoClosed(boolean z);

    void onRewardedVideoExpired();

    void onRewardedVideoFailedToLoad();

    void onRewardedVideoFinished(double d, String str);

    void onRewardedVideoLoaded(boolean z);

    void onRewardedVideoShowFailed();

    void onRewardedVideoShown();
}
