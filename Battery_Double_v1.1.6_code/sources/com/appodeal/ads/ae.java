package com.appodeal.ads;

import com.smaato.sdk.core.api.VideoType;

public class ae {
    public static String a(int i) {
        if (i == 128) {
            return "rewarded_video";
        }
        if (i == 256) {
            return "banner_mrec";
        }
        if (i == 512) {
            return "native";
        }
        switch (i) {
            case 2:
                return "video";
            case 3:
                return "banner";
            case 4:
                return "banner_320";
            default:
                return null;
        }
    }

    public static String b(int i) {
        if (i == 128) {
            return "rewarded_video";
        }
        if (i == 256) {
            return "mrec";
        }
        if (i == 512) {
            return "native";
        }
        switch (i) {
            case 2:
                return "video";
            case 3:
                return VideoType.INTERSTITIAL;
            case 4:
                return "banner";
            default:
                return "unknown";
        }
    }
}
