package com.appodeal.ads;

import android.support.annotation.Nullable;
import org.json.JSONObject;

public interface AdUnit {
    double getEcpm();

    long getExpTime();

    String getId();

    JSONObject getJsonData();

    int getLoadingTimeout();

    @Nullable
    String getMediatorName();

    @Nullable
    t getRequestResult();

    String getStatus();

    boolean isAsync();

    boolean isPrecache();
}
