package com.appodeal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;

class an extends ak<ap, UnifiedInterstitial, UnifiedInterstitialParams, UnifiedInterstitialCallback> {

    private final class a extends UnifiedInterstitialCallback {
        private a() {
        }

        public void onAdClicked() {
            am.b().a(an.this.a(), an.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            am.b().a(an.this.a(), an.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdClosed() {
            am.b().m(an.this.a(), an.this);
        }

        public void onAdExpired() {
            am.b().i(an.this.a(), an.this);
        }

        public void onAdFinished() {
            am.b().o(an.this.a(), an.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            an.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            am.b().b(an.this.a(), an.this, loadingError);
        }

        public void onAdLoaded() {
            am.b().b(an.this.a(), an.this);
        }

        public void onAdShowFailed() {
            am.b().a(an.this.a(), an.this, null, LoadingError.ShowFailed);
        }

        public void onAdShown() {
            am.b().p(an.this.a(), an.this);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((ap) an.this.a()).a((AdUnit) an.this, str, obj);
        }
    }

    private final class b implements UnifiedInterstitialParams {
        private b() {
        }

        public int getAfd() {
            return am.a().D();
        }

        public String obtainPlacementId() {
            return am.a().u();
        }

        public String obtainSegmentId() {
            return am.a().s();
        }
    }

    an(@NonNull ap apVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
        super(apVar, adNetwork, bnVar, 10000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedInterstitial a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createInterstitial();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedInterstitialParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return b().isInterstitialShowing() ? LoadingError.Canceled : super.s();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedInterstitialCallback o() {
        return new a();
    }
}
