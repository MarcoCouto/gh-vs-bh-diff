package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.appodeal.ads.b.e;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.o;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import java.util.Map.Entry;
import org.json.JSONObject;

final class aa {
    static final ab a = new ab();
    static boolean b = true;
    static boolean c = true;
    @VisibleForTesting
    static c d;
    @VisibleForTesting
    static b e;
    /* access modifiers changed from: private */
    public static Integer f = null;
    @SuppressLint({"StaticFieldLeak"})
    private static a g;

    static class a extends bv<ad, ac> {
        a() {
            super("debug_banner_320", b.BOTTOM);
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull Activity activity, @NonNull b bVar) {
            aa.a((Context) activity, new d(bVar));
        }

        /* access modifiers changed from: 0000 */
        public boolean a(View view) {
            return view instanceof BannerView;
        }
    }

    @VisibleForTesting(otherwise = 3)
    static class b extends p<ac, ad, d> {
        b(q<ac, ad, ?> qVar) {
            super(qVar, e.c(), 4);
        }

        /* access modifiers changed from: protected */
        public ac a(@NonNull ad adVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
            return new ac(adVar, adNetwork, bnVar);
        }

        /* access modifiers changed from: protected */
        public ad a(d dVar) {
            return new ad(dVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                ad adVar = (ad) y();
                if (adVar == null || (adVar.M() && !adVar.t())) {
                    u f = aa.a().f();
                    if (f == u.HIDDEN || f == u.NEVER_SHOWN) {
                        d((Context) activity);
                    } else {
                        aa.a((Context) activity, new d(aa.a().d()));
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("refresh_period")) {
                aa.f = Integer.valueOf(jSONObject.optInt("refresh_period") * 1000);
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return super.c() && y() == null;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            aa.a(context, aa.b(aa.a().c()));
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "banners_disabled";
        }
    }

    @VisibleForTesting
    static class c extends bc<ac, ad> {
        c() {
            super(aa.a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void c(ad adVar) {
            aa.a(adVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(ad adVar, ac acVar, boolean z) {
            super.b(adVar, acVar, z);
            if (b(adVar, acVar) && !com.appodeal.ads.utils.c.b(Appodeal.e)) {
                aa.a(Appodeal.e, new bw(this.a.t(), adVar.U(), true, false));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean e(ad adVar, ac acVar) {
            return super.e(adVar, acVar) && !b(adVar, acVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(ad adVar, ac acVar, Object obj) {
            return super.j(adVar, acVar, obj) && this.a.D() > 0;
        }

        /* access modifiers changed from: 0000 */
        public void b(ad adVar) {
            aa.a(adVar, 0, false, false);
        }

        /* access modifiers changed from: protected */
        public boolean b(ad adVar, ac acVar) {
            return adVar.u() && !acVar.h();
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void d(@Nullable ad adVar) {
            if (adVar != null) {
                if (adVar.M()) {
                    u f = aa.a().f();
                    if (!(f == u.HIDDEN || f == u.NEVER_SHOWN)) {
                        aa.a(Appodeal.f, new d(aa.a().d()));
                        return;
                    }
                } else {
                    return;
                }
            }
            this.a.d(Appodeal.f);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void j(ad adVar, ac acVar) {
            if (this.a.r()) {
                u f = aa.a().f();
                if (f == u.HIDDEN || f == u.NEVER_SHOWN) {
                    this.a.d(Appodeal.f);
                } else {
                    aa.a(Appodeal.f, new d(aa.a().d()));
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public void q(final ad adVar, ac acVar) {
            if (!adVar.a() && this.a.r()) {
                ad adVar2 = (ad) this.a.y();
                if (adVar2 == null || adVar2.M()) {
                    this.a.d(Appodeal.f);
                }
            }
            if (this.a.r()) {
                bq.a((Runnable) new Runnable() {
                    public void run() {
                        try {
                            u f = aa.a().f();
                            if (c.this.a.c(c.this.a.b(adVar)) && f != u.HIDDEN && f != u.NEVER_SHOWN) {
                                View e = aa.a().e();
                                if (e != null && e.isShown()) {
                                    if (!com.appodeal.ads.utils.c.b(Appodeal.e)) {
                                        aa.a(Appodeal.e, new bw(c.this.a.t(), aa.a().d(), false, false));
                                    } else {
                                        bq.a((Runnable) this, 1000);
                                    }
                                }
                            }
                        } catch (Exception e2) {
                            Log.log(e2);
                        }
                    }
                }, (long) aa.i().intValue());
            }
        }
    }

    static class d extends n<d> {
        private b a;

        d() {
            super("banner_320", "debug_banner_320");
        }

        d(b bVar) {
            this();
            this.a = bVar;
            b(true);
        }

        /* access modifiers changed from: 0000 */
        public b f() {
            return this.a;
        }
    }

    static a a() {
        if (g == null) {
            g = new a();
        }
        return g;
    }

    static void a(Context context, d dVar) {
        b().b(context, dVar);
    }

    static void a(ad adVar, int i, boolean z, boolean z2) {
        b().a(adVar, i, z2, z);
    }

    static boolean a(Activity activity) {
        return a().a(activity, b());
    }

    static boolean a(Activity activity, bw bwVar) {
        return a().a(activity, bwVar, b());
    }

    /* access modifiers changed from: private */
    public static d b(@Nullable b bVar) {
        return bVar != null ? new d(bVar) : new d();
    }

    static p<ac, ad, d> b() {
        if (e == null) {
            e = new b(c());
        }
        return e;
    }

    static q<ac, ad, Object> c() {
        if (d == null) {
            d = new c();
        }
        return d;
    }

    static int d() {
        return ((b || c) && bq.g(Appodeal.f) > 720.0f) ? 90 : 50;
    }

    static int e() {
        return b ? Math.round(bq.f(Appodeal.f)) : (!c || bq.f(Appodeal.f) < 728.0f) ? ModuleDescriptor.MODULE_VERSION : Math.min(Math.round(bq.f(Appodeal.f)), 728);
    }

    static boolean f() {
        return c && bq.f(Appodeal.f) >= 728.0f && bq.g(Appodeal.f) > 720.0f;
    }

    static void g() {
        if (a().f() != u.VISIBLE) {
            Log.log("Banner", LogConstants.EVENT_AD_DESTROY, LogLevel.debug);
            ad adVar = (ad) b().y();
            if (adVar != null) {
                if (adVar.B() != null) {
                    o.a(adVar.B());
                    ((ac) adVar.B()).q();
                }
                for (Entry value : adVar.F().entrySet()) {
                    i iVar = (i) value.getValue();
                    if (iVar != null) {
                        o.a(iVar);
                        iVar.q();
                    }
                }
                c().g(adVar);
                adVar.R();
                adVar.Q();
            }
        }
    }

    /* access modifiers changed from: private */
    public static Integer i() {
        int i;
        com.appodeal.ads.b.d t = b().t();
        if (t == null || t.c() <= 0) {
            if (f == null) {
                i = 15000;
            }
            return f;
        }
        i = t.c();
        f = Integer.valueOf(i);
        return f;
    }
}
