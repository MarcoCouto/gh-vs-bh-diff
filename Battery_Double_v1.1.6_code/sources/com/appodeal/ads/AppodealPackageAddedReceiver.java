package com.appodeal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.appodeal.ads.aq.c;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.r;

public class AppodealPackageAddedReceiver extends BroadcastReceiver {
    private String getPackageName(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split(":");
        if (split.length != 2) {
            return null;
        }
        return split[1];
    }

    public void onReceive(Context context, Intent intent) {
        if (!Appodeal.a()) {
            try {
                if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                    String packageName = getPackageName(intent.getData().toString());
                    if (packageName != null && r.a(context, packageName)) {
                        new c(context, "install").a(packageName).a().a();
                    }
                }
            } catch (Exception e) {
                Log.log(e);
            }
        }
    }
}
