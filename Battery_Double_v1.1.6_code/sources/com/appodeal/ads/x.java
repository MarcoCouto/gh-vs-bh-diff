package com.appodeal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.aq.a;
import com.appodeal.ads.utils.Log;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import org.json.JSONObject;

class x implements a {
    x() {
    }

    @Nullable
    static JSONObject a() {
        SharedPreferences b = bl.a(Appodeal.f).b();
        long j = b.getLong("init_saved_time", -1);
        if (j > 0 && System.currentTimeMillis() - j < 86400000) {
            try {
                return new JSONObject(b.getString("init_response", null));
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return null;
    }

    public void a(@Nullable m mVar) {
        JSONObject jSONObject;
        try {
            jSONObject = a();
        } catch (Exception e) {
            Log.log(e);
            jSONObject = null;
        }
        a(jSONObject);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(JSONObject jSONObject) {
        Appodeal.b = false;
        Appodeal.c = true;
        if (jSONObject != null) {
            b(jSONObject);
            c(jSONObject);
        }
        Appodeal.b();
    }

    public void a(JSONObject jSONObject, @Nullable m mVar, String str) {
        try {
            bl.a(Appodeal.f).a().putLong("init_saved_time", System.currentTimeMillis()).putString("init_response", jSONObject.toString()).apply();
        } catch (Exception e) {
            Log.log(e);
        }
        a(jSONObject);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void b(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY);
        if (optJSONObject == null) {
            optJSONObject = jSONObject.optJSONObject("fingerprint");
        }
        if (optJSONObject != null) {
            be.a(optJSONObject);
        }
        z.b(jSONObject);
        z.c(jSONObject);
        z.d(jSONObject);
        w.b(jSONObject);
        be.a(Appodeal.f, jSONObject);
        if (jSONObject.has("initialize_with_queue")) {
            z.a(jSONObject.optBoolean("initialize_with_queue"));
        }
        if (jSONObject.has("session_timeout_duration")) {
            Appodeal.getSession().a(jSONObject.optLong("session_timeout_duration"));
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(@NonNull JSONObject jSONObject) {
        Appodeal.c().a((Context) Appodeal.e, jSONObject.optJSONArray("init"));
    }
}
