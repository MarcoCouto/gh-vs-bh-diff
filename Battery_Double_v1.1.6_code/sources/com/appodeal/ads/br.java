package com.appodeal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;

class br extends ak<bt, UnifiedVideo, UnifiedVideoParams, UnifiedVideoCallback> {

    private final class a extends UnifiedVideoCallback {
        private a() {
        }

        public void onAdClicked() {
            bd.b().a(br.this.a(), br.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            bd.b().a(br.this.a(), br.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdClosed() {
            bd.b().m(br.this.a(), br.this);
        }

        public void onAdExpired() {
            bd.b().i(br.this.a(), br.this);
        }

        public void onAdFinished() {
            bd.b().o(br.this.a(), br.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            br.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            bd.b().b(br.this.a(), br.this, loadingError);
        }

        public void onAdLoaded() {
            bd.b().b(br.this.a(), br.this);
        }

        public void onAdShowFailed() {
            bd.b().a(br.this.a(), br.this, null, LoadingError.ShowFailed);
        }

        public void onAdShown() {
            bd.b().p(br.this.a(), br.this);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((bt) br.this.a()).a((AdUnit) br.this, str, obj);
        }
    }

    private final class b implements UnifiedVideoParams {
        private b() {
        }

        public int getAfd() {
            return bd.a().D();
        }

        public String obtainPlacementId() {
            return bd.a().u();
        }

        public String obtainSegmentId() {
            return bd.a().s();
        }
    }

    br(@NonNull bt btVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
        super(btVar, adNetwork, bnVar, 10000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedVideo a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createVideo();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedVideoParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return b().isVideoShowing() ? LoadingError.Canceled : super.s();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedVideoCallback o() {
        return new a();
    }
}
