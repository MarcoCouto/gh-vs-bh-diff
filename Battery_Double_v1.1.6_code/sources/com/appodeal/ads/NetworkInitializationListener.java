package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface NetworkInitializationListener<NetworkRequestParams> {
    void onInitializationFailed(@Nullable LoadingError loadingError);

    void onInitializationFinished(@NonNull NetworkRequestParams networkrequestparams) throws Exception;
}
