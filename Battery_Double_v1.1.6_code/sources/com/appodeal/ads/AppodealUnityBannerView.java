package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.PopupWindow;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.b.a;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import java.lang.reflect.Method;

public final class AppodealUnityBannerView {
    private static AppodealUnityBannerView a;
    /* access modifiers changed from: private */
    public PopupWindow b;
    private boolean c = true;

    private AppodealUnityBannerView() {
    }

    /* access modifiers changed from: private */
    public int a(int i) {
        switch (i) {
            case -4:
            case -3:
            case -2:
            case -1:
                return 0;
            default:
                return i;
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"RtlHardcoded"})
    public int a(int i, int i2) {
        int i3;
        switch (i) {
            case -3:
                i3 = 5;
                break;
            case -2:
            case -1:
                i3 = 1;
                break;
            default:
                i3 = 3;
                break;
        }
        return i3 | (i2 != 8 ? 48 : 80);
    }

    private void a(final Activity activity, final int i) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (AppodealUnityBannerView.this.b != null) {
                    Appodeal.hide(activity, i);
                    AppodealUnityBannerView.this.b.dismiss();
                    AppodealUnityBannerView.this.b = null;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull PopupWindow popupWindow, int i) {
        try {
            Method declaredMethod = PopupWindow.class.getDeclaredMethod("setWindowLayoutType", new Class[]{Integer.TYPE});
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(popupWindow, new Object[]{Integer.valueOf(i)});
        } catch (Exception e) {
            Log.log(LogConstants.KEY_SDK, String.format("Unable to set popUpWindow window layout type: %s", new Object[]{e.getLocalizedMessage()}));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b  */
    private boolean a(@NonNull Activity activity, int i, int i2, int i3, @NonNull String str) {
        final View view;
        int i4;
        int i5 = i;
        int i6 = i2;
        String str2 = str;
        if (activity == null) {
            Log.log(new a("Unable to show an ad: activity = null"));
            return false;
        } else if (str2 == null) {
            Log.log(new a("Unable to show an ad: placement = null"));
            return false;
        } else {
            if (i6 == -1) {
                if (this.c) {
                    Appodeal.setSmartBanners(true);
                    MrecView mrecView = null;
                    if (i5 != 64) {
                        view = Appodeal.getBannerView(activity);
                        i4 = ModuleDescriptor.MODULE_VERSION;
                    } else {
                        if (i5 == 256) {
                            mrecView = Appodeal.getMrecView(activity);
                        }
                        view = mrecView;
                        i4 = 300;
                    }
                    if (view != null) {
                        Log.log(new a("Unable to show an ad: adView = null"));
                        return false;
                    }
                    final int round = i6 == -1 ? -1 : Math.round(((float) i4) * bq.h(activity));
                    view.setBackgroundColor(0);
                    final Activity activity2 = activity;
                    final int i7 = i;
                    final int i8 = i2;
                    final int i9 = i3;
                    AnonymousClass1 r0 = new Runnable() {
                        public void run() {
                            if (AppodealUnityBannerView.this.b != null) {
                                Appodeal.hide(activity2, i7);
                                AppodealUnityBannerView.this.b.dismiss();
                                AppodealUnityBannerView.this.b = null;
                            }
                            AppodealUnityBannerView.this.b = new PopupWindow(view, round, -2);
                            if (VERSION.SDK_INT >= 23) {
                                AppodealUnityBannerView.this.b.setWindowLayoutType(1002);
                            } else {
                                AppodealUnityBannerView.this.a(AppodealUnityBannerView.this.b, 1002);
                            }
                            AppodealUnityBannerView.this.b.getContentView().setSystemUiVisibility(activity2.getWindow().getAttributes().flags);
                            AppodealUnityBannerView.this.b.showAtLocation(activity2.getWindow().getDecorView().getRootView(), AppodealUnityBannerView.this.a(i8, i9), AppodealUnityBannerView.this.a(i8), AppodealUnityBannerView.this.b(i9));
                        }
                    };
                    activity.runOnUiThread(r0);
                    return Appodeal.show(activity, i, str2);
                }
            }
            Appodeal.setSmartBanners(false);
            MrecView mrecView2 = null;
            if (i5 != 64) {
            }
            if (view != null) {
            }
        }
    }

    /* access modifiers changed from: private */
    public int b(int i) {
        if (i == 8 || i == 16) {
            return 0;
        }
        return i;
    }

    public static AppodealUnityBannerView getInstance() {
        if (a == null) {
            a = new AppodealUnityBannerView();
        }
        return a;
    }

    public void hideBannerView(Activity activity) {
        a(activity, 4);
    }

    public void hideMrecView(Activity activity) {
        a(activity, 256);
    }

    public void setSmartBanners(boolean z) {
        this.c = z;
    }

    public boolean showBannerView(@NonNull Activity activity, int i, int i2, @NonNull String str) {
        return a(activity, 64, i, i2, str);
    }

    public boolean showMrecView(@NonNull Activity activity, int i, int i2, @NonNull String str) {
        return a(activity, 256, i, i2, str);
    }
}
