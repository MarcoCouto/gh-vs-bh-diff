package com.appodeal.ads;

import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONObject;

public interface AdNetworkMediationParams {
    @Deprecated
    String getAppName();

    String getImpressionId();

    @NonNull
    List<JSONObject> getParallelBiddingAdUnitList();

    @NonNull
    RestrictedData getRestrictedData();

    @Deprecated
    String getStoreUrl();

    @Deprecated
    boolean isCoronaApp();

    boolean isTestMode();
}
