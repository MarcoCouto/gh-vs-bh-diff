package com.appodeal.ads.unified.vast;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.explorestack.iab.vast.VideoType;

public abstract class UnifiedVastRewarded<NetworkRequestParams> extends UnifiedRewarded<NetworkRequestParams> implements UnifiedFullscreenVast<UnifiedRewardedParams, UnifiedRewardedCallback, NetworkRequestParams> {
    @VisibleForTesting
    UnifiedVastFullscreenAd<UnifiedRewardedParams, UnifiedRewardedCallback, NetworkRequestParams> unifiedVast = new UnifiedVastFullscreenAd<>(this);

    public UnifiedVastFullscreenListener<UnifiedRewardedCallback> createListener(@NonNull Context context, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        return new UnifiedVastRewardedListener(unifiedRewardedCallback, unifiedVastNetworkParams);
    }

    @NonNull
    public VideoType getVideoType() {
        return VideoType.Rewarded;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.unifiedVast.load(activity, unifiedRewardedParams, networkrequestparams, unifiedRewardedCallback);
    }

    public void loadVast(@NonNull Context context, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        this.unifiedVast.loadVast(context, unifiedRewardedParams, unifiedVastNetworkParams, unifiedRewardedCallback);
    }

    public void onDestroy() {
        this.unifiedVast.onDestroy();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedRewardedParams, networkrequestparams);
        this.unifiedVast.onPrepareToShow(activity, unifiedRewardedParams, networkrequestparams);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        this.unifiedVast.show(activity, unifiedRewardedCallback);
    }
}
