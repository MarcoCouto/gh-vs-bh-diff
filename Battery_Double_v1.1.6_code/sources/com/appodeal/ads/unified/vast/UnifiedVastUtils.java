package com.appodeal.ads.unified.vast;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedFullscreenAd;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.unified.UnifiedFullscreenAdParams;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.VastRequest.Builder;
import com.explorestack.iab.vast.VideoType;

class UnifiedVastUtils {

    interface UnifiedFullscreenVast<UnifiedAdParamsType extends UnifiedFullscreenAdParams, UnifiedAdCallbackType extends UnifiedFullscreenAdCallback, NetworkRequestParams> {
        UnifiedVastFullscreenListener<UnifiedAdCallbackType> createListener(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        @NonNull
        VideoType getVideoType();

        void loadVast(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        @Nullable
        UnifiedVastNetworkParams obtainVastParams(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype);

        void performVastRequest(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, @NonNull String str);
    }

    static class UnifiedVastFullscreenAd<UnifiedAdParamsType extends UnifiedFullscreenAdParams, UnifiedAdCallbackType extends UnifiedFullscreenAdCallback, NetworkRequestParams> extends UnifiedFullscreenAd<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> {
        @NonNull
        private UnifiedFullscreenVast<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> unifiedVastAd;
        @Nullable
        private UnifiedVastFullscreenListener<UnifiedAdCallbackType> vastListener;
        @Nullable
        @VisibleForTesting
        VastRequest vastRequest;

        UnifiedVastFullscreenAd(@NonNull UnifiedFullscreenVast<UnifiedAdParamsType, UnifiedAdCallbackType, NetworkRequestParams> unifiedFullscreenVast) {
            this.unifiedVastAd = unifiedFullscreenVast;
        }

        public void load(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) throws Exception {
            UnifiedVastNetworkParams obtainVastParams = this.unifiedVastAd.obtainVastParams(activity, unifiedadparamstype, networkrequestparams, unifiedadcallbacktype);
            if (obtainVastParams == null) {
                unifiedadcallbacktype.onAdLoadFailed(LoadingError.IncorrectAdunit);
            } else if (UnifiedVastUtils.isValidAdm(obtainVastParams.adm)) {
                loadVast(activity, unifiedadparamstype, obtainVastParams, unifiedadcallbacktype);
            } else {
                this.unifiedVastAd.performVastRequest(activity, unifiedadparamstype, obtainVastParams, unifiedadcallbacktype, obtainVastParams.vastUrl);
            }
        }

        /* access modifiers changed from: 0000 */
        public void loadVast(@NonNull Context context, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) {
            this.vastListener = this.unifiedVastAd.createListener(context, unifiedadparamstype, unifiedVastNetworkParams, unifiedadcallbacktype);
            this.vastRequest = UnifiedVastUtils.createVastRequest(unifiedadparamstype, unifiedVastNetworkParams, unifiedVastNetworkParams.vastUrl);
            this.vastRequest.loadVideoWithData(context, unifiedVastNetworkParams.adm, this.vastListener);
        }

        public void onDestroy() {
            if (this.vastRequest != null) {
                this.vastRequest = null;
            }
        }

        public void show(@NonNull Activity activity, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) {
            if (this.vastRequest == null || !this.vastRequest.checkFile()) {
                unifiedadcallbacktype.onAdShowFailed();
            } else {
                this.vastRequest.display(activity, this.unifiedVastAd.getVideoType(), this.vastListener);
            }
        }
    }

    UnifiedVastUtils() {
    }

    /* access modifiers changed from: private */
    public static VastRequest createVastRequest(@NonNull UnifiedFullscreenAdParams unifiedFullscreenAdParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull String str) {
        Builder addExtra = VastRequest.newBuilder().setPreCache(true).setXmlUrl(str).setCloseTime(unifiedVastNetworkParams.closeTime).setAutoClose(unifiedVastNetworkParams.autoClose).setUseLayoutInCompanion(unifiedVastNetworkParams.useLayoutInCompanion).addExtra("segment_id", unifiedFullscreenAdParams.obtainSegmentId()).addExtra("placement_id", unifiedFullscreenAdParams.obtainPlacementId());
        if (unifiedFullscreenAdParams instanceof UnifiedRewardedParams) {
            addExtra.setMaxDuration(((UnifiedRewardedParams) unifiedFullscreenAdParams).getMaxDuration());
        }
        return addExtra.build();
    }

    /* access modifiers changed from: private */
    public static boolean isValidAdm(String str) {
        return !TextUtils.isEmpty(str) && TextUtils.getTrimmedLength(str) > 0;
    }
}
