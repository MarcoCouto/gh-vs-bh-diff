package com.appodeal.ads.unified.vast;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.explorestack.iab.vast.VideoType;

public abstract class UnifiedVastVideo<NetworkRequestParams> extends UnifiedVideo<NetworkRequestParams> implements UnifiedFullscreenVast<UnifiedVideoParams, UnifiedVideoCallback, NetworkRequestParams> {
    @VisibleForTesting
    UnifiedVastFullscreenAd<UnifiedVideoParams, UnifiedVideoCallback, NetworkRequestParams> unifiedVast = new UnifiedVastFullscreenAd<>(this);

    public UnifiedVastFullscreenListener<UnifiedVideoCallback> createListener(@NonNull Context context, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        return new UnifiedVastVideoListener(unifiedVideoCallback, unifiedVastNetworkParams);
    }

    @NonNull
    public VideoType getVideoType() {
        return VideoType.NonRewarded;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.unifiedVast.load(activity, unifiedVideoParams, networkrequestparams, unifiedVideoCallback);
    }

    public void loadVast(@NonNull Context context, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        this.unifiedVast.loadVast(context, unifiedVideoParams, unifiedVastNetworkParams, unifiedVideoCallback);
    }

    public void onDestroy() {
        this.unifiedVast.onDestroy();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedVideoParams, networkrequestparams);
        this.unifiedVast.onPrepareToShow(activity, unifiedVideoParams, networkrequestparams);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        this.unifiedVast.show(activity, unifiedVideoCallback);
    }
}
