package com.appodeal.ads.unified.vast;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.explorestack.iab.vast.VideoType;

public abstract class UnifiedVastInterstitial<NetworkRequestParams> extends UnifiedInterstitial<NetworkRequestParams> implements UnifiedFullscreenVast<UnifiedInterstitialParams, UnifiedInterstitialCallback, NetworkRequestParams> {
    private UnifiedVastFullscreenAd<UnifiedInterstitialParams, UnifiedInterstitialCallback, NetworkRequestParams> unfiedVast = new UnifiedVastFullscreenAd<>(this);

    public UnifiedVastFullscreenListener<UnifiedInterstitialCallback> createListener(@NonNull Context context, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        return new UnifiedVastInterstitialListener(unifiedInterstitialCallback, unifiedVastNetworkParams);
    }

    @NonNull
    public VideoType getVideoType() {
        return VideoType.NonRewarded;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.unfiedVast.load(activity, unifiedInterstitialParams, networkrequestparams, unifiedInterstitialCallback);
    }

    public void loadVast(@NonNull Context context, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.unfiedVast.loadVast(context, unifiedInterstitialParams, unifiedVastNetworkParams, unifiedInterstitialCallback);
    }

    public void onDestroy() {
        this.unfiedVast.onDestroy();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedInterstitialParams, networkrequestparams);
        this.unfiedVast.onPrepareToShow(activity, unifiedInterstitialParams, networkrequestparams);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        this.unfiedVast.show(activity, unifiedInterstitialCallback);
    }
}
