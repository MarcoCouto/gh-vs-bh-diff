package com.appodeal.ads.unified;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class UnifiedNativeCallback extends UnifiedAdCallback {
    public abstract void onAdClicked(int i, @Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener);

    public void onAdClicked(@NonNull UnifiedNativeAd unifiedNativeAd, @Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
        onAdClicked(unifiedNativeAd.getAdId(), unifiedAdCallbackClickTrackListener);
    }

    public abstract void onAdFinished(int i);

    public void onAdFinished(@NonNull UnifiedNativeAd unifiedNativeAd) {
        onAdFinished(unifiedNativeAd.getAdId());
    }

    public abstract void onAdLoaded(@NonNull UnifiedNativeAd unifiedNativeAd);

    public abstract void onAdShown(int i);

    public void onAdShown(@NonNull UnifiedNativeAd unifiedNativeAd) {
        onAdShown(unifiedNativeAd.getAdId());
    }
}
