package com.appodeal.ads.unified;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedAdCallback;
import com.appodeal.ads.unified.UnifiedAdParams;
import com.appodeal.ads.utils.app.AppState;

public abstract class UnifiedAd<UnifiedAdParamsType extends UnifiedAdParams, UnifiedAdCallbackType extends UnifiedAdCallback, NetworkRequestParamsType> {
    public abstract void load(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParamsType networkrequestparamstype, @NonNull UnifiedAdCallbackType unifiedadcallbacktype) throws Exception;

    @CallSuper
    public void onAppStateChanged(@Nullable Activity activity, @NonNull AppState appState, @NonNull UnifiedAdCallbackType unifiedadcallbacktype, boolean z) {
    }

    @CallSuper
    public void onClicked() {
    }

    public abstract void onDestroy();

    @CallSuper
    public void onError(LoadingError loadingError) {
    }

    @CallSuper
    public void onFinished() {
    }

    @CallSuper
    public void onHide() {
    }

    @CallSuper
    public void onImpression() {
    }

    @CallSuper
    public void onLoaded() {
    }

    @CallSuper
    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedAdParamsType unifiedadparamstype, @NonNull NetworkRequestParamsType networkrequestparamstype) {
    }

    @CallSuper
    public void onShow() {
    }
}
