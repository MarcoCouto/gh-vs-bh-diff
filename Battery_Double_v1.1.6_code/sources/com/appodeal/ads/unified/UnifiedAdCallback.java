package com.appodeal.ads.unified;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;

public abstract class UnifiedAdCallback {
    public abstract void onAdClicked();

    public abstract void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener);

    public abstract void onAdExpired();

    public abstract void onAdInfoRequested(@Nullable Bundle bundle);

    public abstract void onAdLoadFailed(@Nullable LoadingError loadingError);

    public abstract void onAdShowFailed();

    public abstract void printError(@Nullable String str, @Nullable Object obj);
}
