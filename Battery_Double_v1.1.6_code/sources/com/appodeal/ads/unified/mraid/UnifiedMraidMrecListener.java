package com.appodeal.ads.unified.mraid;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.explorestack.iab.mraid.MRAIDView;

public class UnifiedMraidMrecListener extends UnifiedMraidViewListener<UnifiedMrecCallback> {
    UnifiedMraidMrecListener(@NonNull UnifiedMrecCallback unifiedMrecCallback, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull MRAIDView mRAIDView) {
        super(unifiedMrecCallback, unifiedMraidNetworkParams, mRAIDView);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureCallTel(String str) {
        super.mraidNativeFeatureCallTel(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureCreateCalendarEvent(String str) {
        super.mraidNativeFeatureCreateCalendarEvent(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureOpenBrowser(String str, WebView webView) {
        super.mraidNativeFeatureOpenBrowser(str, webView);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeaturePlayVideo(String str) {
        super.mraidNativeFeaturePlayVideo(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureSendSms(String str) {
        super.mraidNativeFeatureSendSms(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureStorePicture(String str) {
        super.mraidNativeFeatureStorePicture(str);
    }

    public /* bridge */ /* synthetic */ void mraidViewClose(MRAIDView mRAIDView) {
        super.mraidViewClose(mRAIDView);
    }

    public /* bridge */ /* synthetic */ void mraidViewExpand(MRAIDView mRAIDView) {
        super.mraidViewExpand(mRAIDView);
    }

    public void mraidViewLoaded(MRAIDView mRAIDView) {
        ((UnifiedMrecCallback) this.callback).onAdLoaded(mRAIDView);
    }

    public /* bridge */ /* synthetic */ void mraidViewNoFill(MRAIDView mRAIDView) {
        super.mraidViewNoFill(mRAIDView);
    }

    public /* bridge */ /* synthetic */ boolean mraidViewResize(MRAIDView mRAIDView, int i, int i2, int i3, int i4) {
        return super.mraidViewResize(mRAIDView, i, i2, i3, i4);
    }
}
