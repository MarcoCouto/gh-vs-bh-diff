package com.appodeal.ads.unified.mraid;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;
import com.explorestack.iab.mraid.activity.MraidActivity.MraidType;

public abstract class UnifiedMraidRewarded<NetworkRequestParams> extends UnifiedRewarded<NetworkRequestParams> implements UnifiedFullscreenMraid<UnifiedRewardedParams, UnifiedRewardedCallback, NetworkRequestParams> {
    private final UnifiedMraidFullscreenAd<UnifiedRewardedParams, UnifiedRewardedCallback, NetworkRequestParams> unifiedAd = new UnifiedMraidFullscreenAd<>(this);

    public UnifiedMraidFullscreenListener<UnifiedRewardedCallback> createListener(@NonNull Context context, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        return new UnifiedMraidRewardedListener(unifiedRewardedCallback, unifiedMraidNetworkParams);
    }

    public MraidType getMraidType() {
        return MraidType.Rewarded;
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull NetworkRequestParams networkrequestparams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.unifiedAd.load(activity, unifiedRewardedParams, networkrequestparams, unifiedRewardedCallback);
    }

    public void loadMraid(@NonNull Context context, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        this.unifiedAd.loadMraid(context, unifiedRewardedParams, unifiedMraidNetworkParams, unifiedRewardedCallback);
    }

    public void onClicked() {
        super.onClicked();
        this.unifiedAd.onClicked();
    }

    public void onDestroy() {
        this.unifiedAd.onDestroy();
    }

    public void onFinished() {
        super.onFinished();
        this.unifiedAd.onFinished();
    }

    public void onPrepareToShow(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull NetworkRequestParams networkrequestparams) {
        super.onPrepareToShow(activity, unifiedRewardedParams, networkrequestparams);
        this.unifiedAd.onPrepareToShow(activity, unifiedRewardedParams, networkrequestparams);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        this.unifiedAd.show(activity, unifiedRewardedCallback);
    }
}
