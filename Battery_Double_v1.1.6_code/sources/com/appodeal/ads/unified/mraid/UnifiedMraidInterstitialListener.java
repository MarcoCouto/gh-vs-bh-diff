package com.appodeal.ads.unified.mraid;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.explorestack.iab.mraid.MRAIDInterstitial;

public class UnifiedMraidInterstitialListener extends UnifiedMraidFullscreenListener<UnifiedInterstitialCallback> {
    UnifiedMraidInterstitialListener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
        super(unifiedInterstitialCallback, unifiedMraidNetworkParams);
    }

    public /* bridge */ /* synthetic */ void mraidInterstitialHide(MRAIDInterstitial mRAIDInterstitial) {
        super.mraidInterstitialHide(mRAIDInterstitial);
    }

    public /* bridge */ /* synthetic */ void mraidInterstitialLoaded(MRAIDInterstitial mRAIDInterstitial) {
        super.mraidInterstitialLoaded(mRAIDInterstitial);
    }

    public /* bridge */ /* synthetic */ void mraidInterstitialNoFill(MRAIDInterstitial mRAIDInterstitial) {
        super.mraidInterstitialNoFill(mRAIDInterstitial);
    }

    public /* bridge */ /* synthetic */ void mraidInterstitialShow(MRAIDInterstitial mRAIDInterstitial) {
        super.mraidInterstitialShow(mRAIDInterstitial);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureCallTel(String str) {
        super.mraidNativeFeatureCallTel(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureCreateCalendarEvent(String str) {
        super.mraidNativeFeatureCreateCalendarEvent(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureOpenBrowser(String str, WebView webView) {
        super.mraidNativeFeatureOpenBrowser(str, webView);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeaturePlayVideo(String str) {
        super.mraidNativeFeaturePlayVideo(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureSendSms(String str) {
        super.mraidNativeFeatureSendSms(str);
    }

    public /* bridge */ /* synthetic */ void mraidNativeFeatureStorePicture(String str) {
        super.mraidNativeFeatureStorePicture(str);
    }

    public /* bridge */ /* synthetic */ void onMraidActivityClose() {
        super.onMraidActivityClose();
    }

    public /* bridge */ /* synthetic */ void onMraidActivityShowFailed() {
        super.onMraidActivityShowFailed();
    }
}
