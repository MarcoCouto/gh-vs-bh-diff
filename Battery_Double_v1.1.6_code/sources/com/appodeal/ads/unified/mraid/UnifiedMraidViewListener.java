package com.appodeal.ads.unified.mraid;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedViewAdCallback;
import com.explorestack.iab.mraid.MRAIDView;
import com.explorestack.iab.mraid.MRAIDViewListener;

abstract class UnifiedMraidViewListener<UnifiedCallbackType extends UnifiedViewAdCallback> extends UnifiedMraidListener<UnifiedCallbackType> implements MRAIDViewListener {
    @NonNull
    private MRAIDView mraidView;

    UnifiedMraidViewListener(@NonNull UnifiedCallbackType unifiedcallbacktype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull MRAIDView mRAIDView) {
        super(unifiedcallbacktype, unifiedMraidNetworkParams);
        this.mraidView = mRAIDView;
    }

    /* access modifiers changed from: 0000 */
    public void displayProgress(WebView webView) {
        UnifiedMraidUtils.addBannerSpinnerView(this.mraidView);
    }

    /* access modifiers changed from: 0000 */
    public void hideProgress(WebView webView) {
        UnifiedMraidUtils.hideBannerSpinnerView(this.mraidView);
    }

    public void mraidViewClose(MRAIDView mRAIDView) {
    }

    public void mraidViewExpand(MRAIDView mRAIDView) {
    }

    public void mraidViewNoFill(MRAIDView mRAIDView) {
        ((UnifiedViewAdCallback) this.callback).onAdLoadFailed(LoadingError.NoFill);
    }

    public boolean mraidViewResize(MRAIDView mRAIDView, int i, int i2, int i3, int i4) {
        return false;
    }
}
