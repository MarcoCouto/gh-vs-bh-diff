package com.appodeal.ads.unified.mraid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.webkit.WebView;
import com.appodeal.ads.unified.UnifiedAdCallback;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.utils.q;
import com.appodeal.ads.utils.q.a;
import com.explorestack.iab.mraid.MRAIDNativeFeatureListener;

abstract class UnifiedMraidListener<UnifiedCallbackType extends UnifiedAdCallback> implements MRAIDNativeFeatureListener {
    @NonNull
    protected final UnifiedCallbackType callback;
    @VisibleForTesting
    @NonNull
    q clickHandler = new q();
    @NonNull
    protected final UnifiedMraidNetworkParams mraidParams;

    UnifiedMraidListener(@NonNull UnifiedCallbackType unifiedcallbacktype, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
        this.callback = unifiedcallbacktype;
        this.mraidParams = unifiedMraidNetworkParams;
    }

    /* access modifiers changed from: 0000 */
    public abstract void displayProgress(WebView webView);

    /* access modifiers changed from: 0000 */
    public abstract void hideProgress(WebView webView);

    public void mraidNativeFeatureCallTel(String str) {
    }

    public void mraidNativeFeatureCreateCalendarEvent(String str) {
    }

    public void mraidNativeFeatureOpenBrowser(String str, final WebView webView) {
        Context context = webView.getContext();
        displayProgress(webView);
        this.clickHandler.a(context, str, this.mraidParams.packageName, this.mraidParams.expiryTime, new a() {
            public void onHandleError() {
                UnifiedMraidListener.this.hideProgress(webView);
            }

            public void onHandled() {
                UnifiedMraidListener.this.hideProgress(webView);
            }

            public void processClick(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
                UnifiedMraidListener.this.callback.onAdClicked(unifiedAdCallbackClickTrackListener);
            }
        });
    }

    public void mraidNativeFeaturePlayVideo(String str) {
    }

    public void mraidNativeFeatureSendSms(String str) {
    }

    public void mraidNativeFeatureStorePicture(String str) {
    }
}
