package com.appodeal.ads.unified;

import com.appodeal.ads.Native.MediaAssetType;
import com.appodeal.ads.Native.NativeAdType;

public interface UnifiedNativeParams extends UnifiedAdParams {
    int getAdCountToLoad();

    MediaAssetType getMediaAssetType();

    NativeAdType getNativeAdType();
}
