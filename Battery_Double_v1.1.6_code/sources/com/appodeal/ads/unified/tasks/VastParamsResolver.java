package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.vast.UnifiedVastNetworkParams;
import com.appodeal.ads.unified.vast.UnifiedVastNetworkParams.Builder;
import com.appodeal.ads.utils.ExchangeAd;

final class VastParamsResolver implements AdParamsResolver<ExchangeAd, UnifiedVastNetworkParams> {
    private UnifiedVastNetworkParams inputParams;

    VastParamsResolver(@NonNull UnifiedVastNetworkParams unifiedVastNetworkParams) {
        this.inputParams = unifiedVastNetworkParams;
    }

    public void processResponse(@NonNull ExchangeAd exchangeAd, @NonNull AdParamsResolverCallback<UnifiedVastNetworkParams> adParamsResolverCallback) throws Exception {
        Builder useLayoutInCompanion = new Builder(this.inputParams).setAdm(exchangeAd.getAdm()).setUseLayoutInCompanion(exchangeAd.useLayout());
        if (exchangeAd.getCloseTime() > 0) {
            useLayoutInCompanion.setCloseTime(exchangeAd.getCloseTime());
        }
        adParamsResolverCallback.onResolveSuccess(useLayoutInCompanion.build());
    }
}
