package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;

public interface AdParamsProcessorCallback<ResponseType> {
    void onProcessFail(@Nullable LoadingError loadingError);

    void onProcessSuccess(@NonNull ResponseType responsetype);
}
