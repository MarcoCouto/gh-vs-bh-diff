package com.appodeal.ads.unified.tasks;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.bq;
import com.appodeal.ads.unified.UnifiedAdCallback;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.vast.UnifiedVastNetworkParams;
import com.appodeal.ads.utils.ExchangeAd;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.s;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class S2SAdTask<ResponseType, OutputType> implements Runnable {
    private static final int RESULT_FAIL = 0;
    private static final int RESULT_SUCCESS = 1;
    @Nullable
    protected final Callback<OutputType> callback;
    @NonNull
    protected final Context context;
    private final Handler handler;
    @NonNull
    private final AdResponseProcessor<ResponseType> processor;
    private final AdParamsProcessorCallback<ResponseType> processorCallback = new AdParamsProcessorCallback<ResponseType>() {
        public void onProcessFail(@Nullable LoadingError loadingError) {
            S2SAdTask.this.notifyFail(loadingError);
        }

        public void onProcessSuccess(@NonNull ResponseType responsetype) {
            try {
                S2SAdTask.this.onParamsProcessSuccess(responsetype);
                S2SAdTask.this.resolver.processResponse(responsetype, S2SAdTask.this.resolverCallback);
            } catch (Throwable th) {
                Log.log(th);
                S2SAdTask.this.notifyFail(LoadingError.InternalError);
            }
        }
    };
    /* access modifiers changed from: private */
    @NonNull
    public final AdParamsResolver<ResponseType, OutputType> resolver;
    /* access modifiers changed from: private */
    public final AdParamsResolverCallback<OutputType> resolverCallback = new AdParamsResolverCallback<OutputType>() {
        public void onResolveFail(@Nullable LoadingError loadingError) {
            S2SAdTask.this.notifyFail(loadingError);
        }

        public void onResolveSuccess(@NonNull OutputType outputtype) {
            S2SAdTask.this.onParamsResolveSuccess(outputtype);
            S2SAdTask.this.notifySuccess(outputtype);
        }
    };
    @NonNull
    public final RestrictedData restrictedData;
    @Nullable
    public final String url;

    public interface Callback<OutputType> {
        void onFail(@Nullable LoadingError loadingError);

        void onSuccess(@NonNull Context context, OutputType outputtype);
    }

    public S2SAdTask(@NonNull final Context context2, @Nullable String str, @NonNull RestrictedData restrictedData2, @NonNull AdResponseProcessor<ResponseType> adResponseProcessor, @NonNull AdParamsResolver<ResponseType, OutputType> adParamsResolver, @Nullable final Callback<OutputType> callback2) {
        this.context = context2;
        this.url = str;
        this.restrictedData = restrictedData2;
        this.processor = adResponseProcessor;
        this.resolver = adParamsResolver;
        this.callback = callback2;
        this.handler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message message) {
                if (callback2 != null) {
                    switch (message.what) {
                        case 0:
                            callback2.onFail((LoadingError) message.obj);
                            return;
                        case 1:
                            callback2.onSuccess(context2, message.obj);
                            return;
                        default:
                            return;
                    }
                }
            }
        };
    }

    public static void requestMraid(@NonNull Context context2, @NonNull String str, @NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull UnifiedAdCallback unifiedAdCallback, @NonNull Callback<UnifiedMraidNetworkParams> callback2) {
        final UnifiedAdCallback unifiedAdCallback2 = unifiedAdCallback;
        AnonymousClass4 r0 = new S2SAdTask<ExchangeAd, UnifiedMraidNetworkParams>(context2, str, unifiedMraidNetworkParams.restrictedData, new ExchangeAdResponseProcessor(), new MraidParamsResolver(unifiedMraidNetworkParams), callback2) {
            /* access modifiers changed from: protected */
            public void onParamsProcessSuccess(@NonNull ExchangeAd exchangeAd) {
                S2SAdTask.super.onParamsProcessSuccess(exchangeAd);
                Bundle bundle = new Bundle();
                bundle.putParcelable("exchange_ad", exchangeAd);
                unifiedAdCallback2.onAdInfoRequested(bundle);
            }
        };
        r0.start();
    }

    public static void requestNast(@NonNull Context context2, @NonNull String str, @NonNull RestrictedData restrictedData2, @NonNull UnifiedAdCallback unifiedAdCallback, @NonNull Callback<String> callback2) {
        final UnifiedAdCallback unifiedAdCallback2 = unifiedAdCallback;
        AnonymousClass6 r0 = new S2SAdTask<ExchangeAd, String>(context2, str, restrictedData2, new ExchangeAdResponseProcessor(), new NastParamsResolver(), callback2) {
            /* access modifiers changed from: protected */
            public void onParamsProcessSuccess(@NonNull ExchangeAd exchangeAd) {
                S2SAdTask.super.onParamsProcessSuccess(exchangeAd);
                Bundle bundle = new Bundle();
                bundle.putParcelable("exchange_ad", exchangeAd);
                unifiedAdCallback2.onAdInfoRequested(bundle);
            }
        };
        r0.start();
    }

    public static void requestVast(@NonNull Context context2, @NonNull String str, @NonNull UnifiedVastNetworkParams unifiedVastNetworkParams, @NonNull UnifiedAdCallback unifiedAdCallback, @NonNull Callback<UnifiedVastNetworkParams> callback2) {
        final UnifiedAdCallback unifiedAdCallback2 = unifiedAdCallback;
        AnonymousClass5 r0 = new S2SAdTask<ExchangeAd, UnifiedVastNetworkParams>(context2, str, unifiedVastNetworkParams.restrictedData, new ExchangeAdResponseProcessor(), new VastParamsResolver(unifiedVastNetworkParams), callback2) {
            /* access modifiers changed from: protected */
            public void onParamsProcessSuccess(@NonNull ExchangeAd exchangeAd) {
                S2SAdTask.super.onParamsProcessSuccess(exchangeAd);
                Bundle bundle = new Bundle();
                bundle.putParcelable("exchange_ad", exchangeAd);
                unifiedAdCallback2.onAdInfoRequested(bundle);
            }
        };
        r0.start();
    }

    /* access modifiers changed from: protected */
    public final void notifyFail(@Nullable LoadingError loadingError) {
        this.handler.obtainMessage(0, loadingError).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public final void notifySuccess(@NonNull OutputType outputtype) {
        this.handler.sendMessage(this.handler.obtainMessage(1, outputtype));
    }

    /* access modifiers changed from: protected */
    @Nullable
    public URL obtainRequestUrl(@NonNull String str) throws Exception {
        return new URL(str);
    }

    /* access modifiers changed from: protected */
    public void onParamsProcessSuccess(@NonNull ResponseType responsetype) {
    }

    /* access modifiers changed from: protected */
    public void onParamsResolveSuccess(@NonNull OutputType outputtype) {
    }

    /* access modifiers changed from: protected */
    public void prepareUrlConnection(Context context2, HttpURLConnection httpURLConnection) throws Exception {
        String httpAgent = this.restrictedData.getHttpAgent(context2);
        if (!TextUtils.isEmpty(httpAgent)) {
            httpURLConnection.setRequestProperty("User-Agent", httpAgent);
        }
    }

    /* access modifiers changed from: protected */
    public void processServerResult(@NonNull URLConnection uRLConnection) throws Exception {
        String a = bq.a(uRLConnection.getInputStream());
        if (TextUtils.isEmpty(a) || TextUtils.getTrimmedLength(a) == 0) {
            notifyFail(LoadingError.NoFill);
        } else {
            processServerResult(uRLConnection, a);
        }
    }

    /* access modifiers changed from: protected */
    public void processServerResult(@NonNull URLConnection uRLConnection, @NonNull String str) throws Exception {
        this.processor.processResponse(uRLConnection, str, this.processorCallback);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0059  */
    public final void run() {
        if (TextUtils.isEmpty(this.url) || TextUtils.getTrimmedLength(this.url) == 0) {
            notifyFail(LoadingError.IncorrectAdunit);
            return;
        }
        HttpURLConnection httpURLConnection = null;
        try {
            URL obtainRequestUrl = obtainRequestUrl(this.url);
            if (obtainRequestUrl == null) {
                notifyFail(LoadingError.IncorrectAdunit);
                return;
            }
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) obtainRequestUrl.openConnection();
            try {
                httpURLConnection2.setConnectTimeout(20000);
                httpURLConnection2.setReadTimeout(20000);
                prepareUrlConnection(this.context, httpURLConnection2);
                processServerResult(httpURLConnection2);
                if (httpURLConnection2 != null) {
                    httpURLConnection2.disconnect();
                }
            } catch (Exception e) {
                HttpURLConnection httpURLConnection3 = httpURLConnection2;
                e = e;
                httpURLConnection = httpURLConnection3;
                try {
                    Log.log(e);
                    notifyFail(LoadingError.InternalError);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                } catch (Throwable th) {
                    th = th;
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                HttpURLConnection httpURLConnection4 = httpURLConnection2;
                th = th2;
                httpURLConnection = httpURLConnection4;
                if (httpURLConnection != null) {
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            Log.log(e);
            notifyFail(LoadingError.InternalError);
            if (httpURLConnection != null) {
            }
        }
    }

    public void start() {
        s.a.execute(this);
    }
}
