package com.appodeal.ads.unified.tasks;

import android.support.annotation.NonNull;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.utils.ExchangeAd;

final class MraidParamsResolver implements AdParamsResolver<ExchangeAd, UnifiedMraidNetworkParams> {
    @NonNull
    private UnifiedMraidNetworkParams inputParams;

    MraidParamsResolver(@NonNull UnifiedMraidNetworkParams unifiedMraidNetworkParams) {
        this.inputParams = unifiedMraidNetworkParams;
    }

    public void processResponse(@NonNull ExchangeAd exchangeAd, @NonNull AdParamsResolverCallback<UnifiedMraidNetworkParams> adParamsResolverCallback) throws Exception {
        Builder useLayout = new Builder(this.inputParams).setAdm(exchangeAd.getAdm()).setUseLayout(exchangeAd.useLayout());
        if (exchangeAd.getWidth() > 0) {
            useLayout.setWidth(exchangeAd.getWidth());
        }
        if (exchangeAd.getHeight() > 0) {
            useLayout.setHeight(exchangeAd.getHeight());
        }
        if (exchangeAd.getCloseTime() > 0) {
            useLayout.setCloseTime(exchangeAd.getCloseTime());
        }
        adParamsResolverCallback.onResolveSuccess(useLayout.build());
    }
}
