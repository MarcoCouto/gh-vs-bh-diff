package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.e;
import org.json.JSONObject;

final class bj {
    static final bk a = new bk();
    static int b = 90000;
    @VisibleForTesting
    static c c;
    @VisibleForTesting
    static b d;
    private static bs<bi, bh> e;

    static class a extends n<a> {
        a() {
            super("rewarded_video", "debug_rewarded_video");
        }
    }

    @VisibleForTesting
    static class b extends p<bh, bi, a> {
        b(q<bh, bi, ?> qVar) {
            super(qVar, e.c(), 128);
        }

        /* access modifiers changed from: protected */
        public bh a(@NonNull bi biVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
            return new bh(biVar, adNetwork, bnVar);
        }

        /* access modifiers changed from: protected */
        public bi a(a aVar) {
            return new bi(aVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                bi biVar = (bi) y();
                if (biVar == null || biVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("max_duration")) {
                bj.b = jSONObject.optInt("max_duration", 0);
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z) {
            if (!z && adNetwork != null && adNetwork.isRewardedShowing() && x().size() > 1) {
                bi biVar = (bi) A();
                bi biVar2 = (bi) z();
                if (!(biVar == null || biVar2 == null || biVar2.B() == null)) {
                    if (str.equals(((bh) biVar2.B()).getId())) {
                        biVar.b(jSONObject);
                    }
                    bj.a(biVar, 0, false, false);
                    return true;
                }
            }
            return super.a(adNetwork, jSONObject, str, z);
        }

        /* access modifiers changed from: protected */
        public boolean a(bi biVar, int i) {
            if (biVar.y() != 1 || biVar.v() == null || biVar.v() != biVar.a(i)) {
                return super.a(biVar, i);
            }
            String optString = biVar.v().optString("status");
            boolean z = false;
            if (TextUtils.isEmpty(optString)) {
                return false;
            }
            AdNetwork c = q().c(optString);
            if (c != null && c.isRewardedShowing()) {
                z = true;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            bj.a(context, new a());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return null;
        }
    }

    @VisibleForTesting
    static class c extends bc<bh, bi> {
        c() {
            super(bj.a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(bi biVar) {
            bj.a(biVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void q(bi biVar, bh bhVar) {
            bhVar.b().setRewardedShowing(true);
            if (!biVar.a() && this.a.r()) {
                bi biVar2 = (bi) this.a.y();
                if (biVar2 == null || biVar2.M()) {
                    this.a.d(Appodeal.f);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(@Nullable bi biVar, @Nullable bh bhVar, @Nullable LoadingError loadingError) {
            super.a(biVar, bhVar, loadingError);
            al.a();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(bi biVar, bh bhVar, boolean z) {
            super.b(biVar, bhVar, z);
            if (!bhVar.h() && e(biVar, bhVar)) {
                bj.a(Appodeal.e, new l(this.a.t()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(bi biVar, bh bhVar, Object obj) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(bi biVar) {
            bj.a(biVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void n(bi biVar, bh bhVar) {
            bj.f().b();
            al.a();
            this.a.d(null);
            bhVar.b().setRewardedShowing(false);
            f(biVar);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void j(bi biVar, bh bhVar) {
            if (this.a.r()) {
                this.a.d(Appodeal.f);
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public boolean a(bi biVar) {
            return biVar.v() == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean e(bi biVar, bh bhVar) {
            return (!e(biVar, bhVar) && (biVar.a(0) == biVar.v() || bhVar.isPrecache() || bhVar.h() || this.a.a(biVar, bhVar))) && super.e(biVar, bhVar);
        }

        /* access modifiers changed from: protected */
        public boolean e(bi biVar, bh bhVar) {
            return biVar.u();
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean l(bi biVar, bh bhVar) {
            return biVar.v() == null || (bhVar != null && biVar.v().optString("id").equals(bhVar.getId()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: g */
        public void c(bi biVar, bh bhVar) {
            super.c(biVar, bhVar);
            if (biVar.v() == bhVar.getJsonData()) {
                biVar.b((JSONObject) null);
            }
        }
    }

    static p<bh, bi, a> a() {
        if (d == null) {
            d = new b(b());
        }
        return d;
    }

    static void a(Context context, a aVar) {
        a().b(context, aVar);
    }

    static void a(bi biVar, int i, boolean z, boolean z2) {
        a().a(biVar, i, z2, z);
    }

    static boolean a(Activity activity, l lVar) {
        return f().a(activity, lVar, a());
    }

    static q<bh, bi, Object> b() {
        if (c == null) {
            c = new c();
        }
        return c;
    }

    static double c() {
        return a().t().h();
    }

    static String d() {
        return a().t().g();
    }

    /* access modifiers changed from: private */
    public static bs<bi, bh> f() {
        if (e == null) {
            e = new bs<>("debug_rewarded_video");
        }
        return e;
    }
}
