package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.PathInterpolatorCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.i;
import com.appodeal.ads.utils.j;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class ag {
    static String a = null;
    static final String[] b = {"a.appbaqend.com", "dev.appodeal.com", "appodeal.local", "staging.appodeal.com", "staging2.appodeal.com", "staging3.appodeal.com", "staging4.appodeal.com", "staging5.appodeal.com", "staging6.appodeal.com"};
    static final Integer[] c = {Integer.valueOf(443), Integer.valueOf(80), Integer.valueOf(PathInterpolatorCompat.MAX_NUM_POINTS), Integer.valueOf(8080), Integer.valueOf(8081)};
    private static SparseIntArray d = null;
    private static boolean e = false;
    private static boolean f = false;

    enum a {
        DEBUG(0),
        LOG(1),
        SERVER(2),
        PORT(3),
        LOG_LEVEL(4),
        TEST_ACTIVITY(5);
        
        private final int g;

        private a(int i) {
            this.g = i;
        }

        public int a() {
            return this.g;
        }
    }

    public interface b {
        void a(int i, boolean z);
    }

    static int a() {
        int b2 = b(a.SERVER.a());
        if (b2 >= b.length) {
            return 0;
        }
        return b2;
    }

    private static int a(Context context, int i) {
        String readLine;
        a[] values;
        if (f().indexOfKey(i) > 0) {
            return f().get(i);
        }
        try {
            if (bq.k()) {
                File file = new File(Environment.getExternalStorageDirectory(), "appodeal.txt");
                if (file.exists()) {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                    try {
                        readLine = bufferedReader.readLine();
                        String readLine2 = bufferedReader.readLine();
                        if (context != null && !e) {
                            e = true;
                            if (readLine2 != null && !readLine2.isEmpty()) {
                                String[] split = readLine2.split(",");
                                List<String> h = h();
                                h.removeAll(Arrays.asList(split));
                                for (String disableNetwork : h) {
                                    Appodeal.disableNetwork(context, disableNetwork);
                                }
                            }
                        }
                    } catch (Exception e2) {
                        Log.log(e2);
                    } catch (Throwable th) {
                        try {
                            bufferedReader.close();
                        } catch (Exception e3) {
                            Log.log(e3);
                        }
                        throw th;
                    }
                    if (readLine != null) {
                        for (a aVar : a.values()) {
                            if (aVar.a() <= readLine.length() - 1) {
                                f().put(aVar.a(), Character.getNumericValue(readLine.charAt(aVar.a())));
                            }
                        }
                        if (f().indexOfKey(i) > 0) {
                            int i2 = f().get(i);
                            try {
                                bufferedReader.close();
                            } catch (Exception e4) {
                                Log.log(e4);
                            }
                            return i2;
                        }
                    }
                    try {
                        bufferedReader.close();
                    } catch (Exception e5) {
                        Log.log(e5);
                    }
                }
            }
        } catch (Exception e6) {
            if (VERSION.SDK_INT < 23 || !(e6 instanceof FileNotFoundException)) {
                android.util.Log.d(Appodeal.a, "Invalid debug file");
                android.util.Log.d(Appodeal.a, "Exception", e6);
            } else if (!f) {
                f = true;
                android.util.Log.d(Appodeal.a, "WRITE_EXTERNAL_STORAGE permission is missing");
            }
        }
        f().put(i, 0);
        return 0;
    }

    private static String a(int i) {
        return i == 443 ? "https://" : "http://";
    }

    static List<j> a(int i, List<JSONObject> list, List<JSONObject> list2) {
        String string;
        String str;
        String string2;
        String str2;
        List<JSONObject> list3 = list;
        List<JSONObject> list4 = list2;
        ArrayList arrayList = new ArrayList();
        if (list4 != null && list2.size() > 0) {
            for (JSONObject jSONObject : list2) {
                try {
                    if (jSONObject.has("package_name")) {
                        string2 = jSONObject.getString("package_name");
                        str2 = ":";
                    } else {
                        string2 = jSONObject.getString("status");
                        str2 = ":";
                    }
                    String[] split = string2.split(str2);
                    j jVar = new j(i, arrayList.size(), list4.indexOf(jSONObject), split[0], bq.c(jSONObject.getString("status")), split.length > 1 ? split[1] : "-1", jSONObject.getString(RequestInfoKeys.APPODEAL_ECPM), true);
                    arrayList.add(jVar);
                } catch (JSONException e2) {
                    Log.log(e2);
                }
            }
        }
        if (list3 != null && list.size() > 0) {
            for (JSONObject jSONObject2 : list) {
                try {
                    if (jSONObject2.has("package_name")) {
                        string = jSONObject2.getString("package_name");
                        str = ":";
                    } else {
                        string = jSONObject2.getString("status");
                        str = ":";
                    }
                    String[] split2 = string.split(str);
                    j jVar2 = new j(i, arrayList.size(), list3.indexOf(jSONObject2), split2[0], bq.c(jSONObject2.getString("status")), split2.length > 1 ? split2[1] : "-1", jSONObject2.getString(RequestInfoKeys.APPODEAL_ECPM), false);
                    arrayList.add(jVar2);
                } catch (JSONException e3) {
                    Log.log(e3);
                }
            }
        }
        return arrayList;
    }

    static void a(@NonNull Activity activity, @NonNull m mVar, @Nullable final b bVar) {
        final i iVar = new i(activity, a(4, mVar.w(), mVar.x()));
        final LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(Color.parseColor("#404040"));
        linearLayout.setTag("appodeal");
        linearLayout.setClickable(true);
        EditText editText = new EditText(activity);
        editText.setLayoutParams(new LayoutParams(-1, (int) TypedValue.applyDimension(1, 56.0f, activity.getResources().getDisplayMetrics())));
        editText.setTextSize(20.0f);
        editText.setTextColor(-1);
        editText.setHint("What adunit you search for?");
        editText.setHintTextColor(Color.parseColor("#80ffffff"));
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                iVar.getFilter().filter(editable.toString());
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        ListView listView = new ListView(activity);
        listView.setAdapter(iVar);
        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                ((InputMethodManager) view.getContext().getSystemService("input_method")).hideSoftInputFromWindow(linearLayout.getWindowToken(), 0);
                ((ViewGroup) linearLayout.getParent()).removeView(linearLayout);
                j jVar = (j) adapterView.getAdapter().getItem(i);
                if (bVar != null) {
                    bVar.a(jVar.h, jVar.g);
                }
            }
        });
        listView.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                ((i) adapterView.getAdapter()).a();
                return true;
            }
        });
        listView.setLayoutParams(new LayoutParams(-1, -1, 1.0f));
        listView.setCacheColorHint(Color.parseColor("#404040"));
        listView.setDivider(new ColorDrawable(Color.parseColor("#b3b3b3")));
        listView.setDividerHeight(1);
        linearLayout.addView(editText);
        linearLayout.addView(listView);
        activity.addContentView(linearLayout, new ViewGroup.LayoutParams(-1, -1));
    }

    static void a(Context context) {
        for (a a2 : a.values()) {
            a(context, a2.a());
        }
    }

    static int b() {
        int b2 = b(a.PORT.a());
        if (b2 >= c.length) {
            return 0;
        }
        return b2;
    }

    private static int b(int i) {
        return a(null, i);
    }

    static boolean c() {
        return b(a.TEST_ACTIVITY.a()) == 1;
    }

    static String d() {
        return b[a()];
    }

    public static String e() {
        if (a != null) {
            return a;
        }
        return String.format("%s%s:%s", new Object[]{a(g()), d(), Integer.valueOf(g())});
    }

    private static SparseIntArray f() {
        if (d == null) {
            d = new SparseIntArray();
        }
        return d;
    }

    private static int g() {
        return c[b()].intValue();
    }

    private static List<String> h() {
        Field[] fields = AppodealNetworks.class.getFields();
        ArrayList arrayList = new ArrayList();
        for (Field field : fields) {
            try {
                if (field.getType().toString().contains("String")) {
                    arrayList.add((String) field.get(AppodealNetworks.class));
                }
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        return arrayList;
    }
}
