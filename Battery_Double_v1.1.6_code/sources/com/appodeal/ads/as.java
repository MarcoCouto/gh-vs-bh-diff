package com.appodeal.ads;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.m;
import com.appodeal.ads.utils.n;
import com.appodeal.ads.utils.s;
import com.explorestack.iab.utils.Assets;
import com.explorestack.iab.vast.TrackingEvent;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.view.CircleCountdownView;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;

public class as extends RelativeLayout implements OnCompletionListener, OnErrorListener, OnPreparedListener, OnVideoSizeChangedListener, SurfaceTextureListener, a {
    public static final String a = "as";
    public static a d;
    ax b;
    boolean c = false;
    private ImageView e;
    private ProgressBar f;
    private ImageView g;
    private CircleCountdownView h;
    /* access modifiers changed from: private */
    public MediaPlayer i;
    private TextureView j;
    private Timer k;
    private boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n = true;
    private boolean o;
    private boolean p;
    private boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public volatile boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public VastRequest v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;
    private Surface y;
    /* access modifiers changed from: private */
    public a z = a.IMAGE;

    private enum a {
        IMAGE,
        PLAYING,
        LOADING,
        PAUSED
    }

    public as(Context context) {
        super(context);
    }

    /* access modifiers changed from: private */
    public void a(TrackingEvent trackingEvent) {
        if (!(this.v == null || this.v.getVastAd() == null)) {
            a((List) this.v.getVastAd().getTrackingEventListMap().get(trackingEvent));
        }
        if (trackingEvent == TrackingEvent.complete && this.b != null) {
            this.b.m();
        }
    }

    private void a(Runnable runnable) {
        s.a.execute(runnable);
    }

    private void a(List<String> list) {
        if (list != null) {
            for (String a2 : list) {
                bq.a(a2, (Executor) s.a);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (m() && this.i.isPlaying()) {
            this.i.pause();
        }
        if (this.z != a.LOADING) {
            this.z = a.PAUSED;
            j();
        }
    }

    private void f() {
        this.i = new MediaPlayer();
        this.i.setOnCompletionListener(this);
        this.i.setOnErrorListener(this);
        this.i.setOnPreparedListener(this);
        this.i.setOnVideoSizeChangedListener(this);
        this.i.setAudioStreamType(3);
        q();
    }

    /* access modifiers changed from: private */
    public void g() {
        try {
            if (!this.o && this.b.j() != null && !this.p && !this.t) {
                this.i.setDataSource(getContext(), this.b.j());
                this.i.prepareAsync();
                this.p = true;
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    private void h() {
        if (this.i != null) {
            try {
                if (!this.t) {
                    if (this.i.isPlaying()) {
                        this.i.stop();
                    }
                    this.i.reset();
                }
                this.i.setOnCompletionListener(null);
                this.i.setOnErrorListener(null);
                this.i.setOnPreparedListener(null);
                this.i.setOnVideoSizeChangedListener(null);
                this.i.release();
            } catch (Exception e2) {
                Log.log(e2);
            }
            this.i = null;
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.i == null) {
            f();
        }
        if (!this.o) {
            g();
        }
        if (m() && !this.i.isPlaying() && this.o && this.q && s()) {
            this.z = a.PLAYING;
            j();
            this.i.start();
            n();
            if (this.k == null) {
                c();
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        switch (this.z) {
            case IMAGE:
                if (this.e != null) {
                    this.e.setVisibility(0);
                    this.e.bringToFront();
                }
                if (this.s) {
                    this.j.setVisibility(4);
                    this.f.setVisibility(4);
                    break;
                } else {
                    return;
                }
            case LOADING:
                if (this.e != null) {
                    this.e.setVisibility(0);
                    this.e.bringToFront();
                }
                if (this.s) {
                    this.f.setVisibility(0);
                    this.f.bringToFront();
                    this.j.setVisibility(4);
                    break;
                } else {
                    return;
                }
            case PLAYING:
                if (this.e != null) {
                    this.e.setVisibility(4);
                }
                if (this.s) {
                    this.j.setVisibility(0);
                    this.j.bringToFront();
                    this.h.setVisibility(0);
                    this.h.bringToFront();
                    r();
                    this.f.setVisibility(4);
                    this.g.setVisibility(4);
                    return;
                }
                return;
            case PAUSED:
                if (this.e != null) {
                    this.e.setVisibility(0);
                    this.e.bringToFront();
                }
                if (this.s) {
                    this.g.setVisibility(0);
                    this.g.bringToFront();
                    this.j.setVisibility(4);
                    this.f.setVisibility(4);
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        this.g.setVisibility(4);
        this.h.setVisibility(4);
    }

    private void k() {
        o();
        d();
        e();
        if (m()) {
            this.i.seekTo(0);
        }
        this.u = true;
    }

    /* access modifiers changed from: private */
    public void l() {
        this.r = false;
        h();
        this.z = a.IMAGE;
        j();
        d();
        this.t = true;
        this.s = false;
        u();
    }

    /* access modifiers changed from: private */
    public boolean m() {
        return !this.t && this.i != null;
    }

    private void n() {
        if (!this.l) {
            t();
            this.l = true;
            Log.log(a, "Video", ParametersKeys.VIDEO_STATUS_STARTED);
        }
    }

    private void o() {
        if (!this.m) {
            a(TrackingEvent.complete);
            this.m = true;
            Log.log(a, "Video", "finished");
        }
    }

    private void p() {
        this.h = new CircleCountdownView(getContext(), Assets.mainAssetsColor, Assets.backgroundColor);
        int round = Math.round(bq.h(getContext()) * 50.0f);
        LayoutParams layoutParams = new LayoutParams(round, round);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        this.h.setLayoutParams(layoutParams);
        r();
        this.h.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                as asVar;
                boolean z;
                if (as.this.m()) {
                    if (as.this.n) {
                        as.this.i.setVolume(1.0f, 1.0f);
                        asVar = as.this;
                        z = false;
                    } else {
                        as.this.i.setVolume(0.0f, 0.0f);
                        asVar = as.this;
                        z = true;
                    }
                    asVar.n = z;
                    as.this.r();
                }
            }
        });
        addView(this.h);
    }

    private void q() {
        MediaPlayer mediaPlayer;
        float f2;
        if (m()) {
            if (this.n) {
                mediaPlayer = this.i;
                f2 = 0.0f;
            } else {
                mediaPlayer = this.i;
                f2 = 1.0f;
            }
            mediaPlayer.setVolume(f2, f2);
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        CircleCountdownView circleCountdownView;
        String str;
        if (this.h != null) {
            if (this.n) {
                circleCountdownView = this.h;
                str = Assets.unmute;
            } else {
                circleCountdownView = this.h;
                str = Assets.mute;
            }
            circleCountdownView.setImage(Assets.getBitmapFromBase64(str));
        }
    }

    /* access modifiers changed from: private */
    public boolean s() {
        return getGlobalVisibleRect(new Rect()) && isShown() && hasWindowFocus();
    }

    private void t() {
        if (this.v != null && this.v.getVastAd() != null) {
            a(this.v.getVastAd().getImpressionUrlList());
        }
    }

    private void u() {
        if (this.v != null) {
            this.v.sendError(405);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        Runnable nVar;
        if (!this.c) {
            this.c = true;
            this.e = new ImageView(getContext());
            this.e.setLayoutParams(new LayoutParams(-1, -1));
            this.e.setScaleType(ScaleType.FIT_CENTER);
            this.e.setAdjustViewBounds(true);
            addView(this.e);
            if (this.s) {
                int round = Math.round(bq.h(getContext()) * 50.0f);
                this.f = new ProgressBar(getContext(), null, 16842874);
                LayoutParams layoutParams = new LayoutParams(round, round);
                layoutParams.addRule(13, -1);
                this.f.setLayoutParams(layoutParams);
                this.f.setBackgroundColor(Color.parseColor("#6b000000"));
                addView(this.f);
                this.g = new ImageView(getContext());
                this.g.setImageResource(17301540);
                LayoutParams layoutParams2 = new LayoutParams(round, round);
                layoutParams2.addRule(13, -1);
                this.g.setLayoutParams(layoutParams2);
                this.g.setBackgroundColor(Color.parseColor("#6b000000"));
                this.g.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        as.this.r = true;
                        as.this.i();
                    }
                });
                addView(this.g);
                this.j = new TextureView(getContext());
                this.j.setSurfaceTextureListener(this);
                LayoutParams layoutParams3 = new LayoutParams(-1, -1);
                layoutParams3.addRule(13);
                this.j.setLayoutParams(layoutParams3);
                this.j.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Uri j = as.this.b.j();
                        if (j == null) {
                            Log.log(as.a, "Video", "click url is absent");
                            return;
                        }
                        Log.log(as.a, "Video", "clicked");
                        as.d = as.this;
                        as.this.u = true;
                        int i = 0;
                        if (as.this.m() && as.this.i.isPlaying()) {
                            i = as.this.i.getCurrentPosition();
                        }
                        as.this.e();
                        as.this.getContext().startActivity(VideoPlayerActivity.a(as.this.getContext(), j.getPath(), i));
                    }
                });
                addView(this.j);
                p();
                f();
                if (Native.b != NativeAdType.Video || this.b == null || this.b.j() == null || !new File(this.b.j().getPath()).exists()) {
                    this.z = a.LOADING;
                    j();
                    if (this.b.g() != null && !this.b.g().isEmpty()) {
                        nVar = new m(getContext(), new com.appodeal.ads.utils.m.a() {
                            public void a() {
                                Log.log(as.a, "Video", "hasn't been loaded");
                                as.this.z = a.IMAGE;
                                as.this.j();
                                as.this.s = false;
                            }

                            public void a(Uri uri) {
                                Log.log(as.a, "Video", "has been loaded");
                                as.this.b.a(uri);
                                as.this.g();
                            }
                        }, this.b.g());
                    } else if (this.b.h() != null && !this.b.h().isEmpty()) {
                        nVar = new n(getContext(), new com.appodeal.ads.utils.n.a() {
                            public void a() {
                                as.this.z = a.IMAGE;
                                as.this.j();
                                as.this.s = false;
                            }

                            public void a(Uri uri, VastRequest vastRequest) {
                                as.this.v = vastRequest;
                                as.this.b.a(vastRequest);
                                as.this.b.a(uri);
                                as.this.g();
                            }
                        }, this.b.h());
                    }
                    a(nVar);
                } else {
                    this.r = Native.e;
                }
            } else {
                this.z = a.IMAGE;
                j();
                this.e.bringToFront();
            }
        }
        if (this.b != null) {
            this.b.a(this.e, this.b.e(), this.b.f());
        }
    }

    public void a(int i2, boolean z2) {
        Log.log(a, LogConstants.EVENT_MV_STATE, String.format("videoPlayerActivityClosed, position: %s, finished: %s", new Object[]{Integer.valueOf(i2), Boolean.valueOf(z2)}));
        if (z2) {
            try {
                k();
            } catch (Exception e2) {
                Log.log(e2);
            }
        } else if (m()) {
            this.i.seekTo(i2);
        }
        d = null;
    }

    public void b() {
        Log.log(a, LogConstants.EVENT_MV_STATE, "onViewAppearOnScreen");
        this.q = true;
        if (Native.b != NativeAdType.Video) {
            return;
        }
        if (this.r) {
            i();
        } else if (this.z != a.LOADING) {
            this.z = a.PAUSED;
            j();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        if (this.s) {
            this.k = new Timer();
            this.k.schedule(new TimerTask() {
                public void run() {
                    Runnable r0;
                    as asVar;
                    TrackingEvent trackingEvent;
                    try {
                        if (as.this.t) {
                            bq.a((Runnable) new Runnable() {
                                public void run() {
                                    as.this.l();
                                }
                            });
                            return;
                        }
                        if (!as.this.s()) {
                            r0 = new Runnable() {
                                public void run() {
                                    as.this.e();
                                    if (!Native.e || as.this.u) {
                                        as.this.d();
                                    }
                                }
                            };
                        } else {
                            if (as.this.m() && as.this.i.isPlaying()) {
                                if (as.this.w == 0) {
                                    as.this.w = as.this.i.getDuration();
                                }
                                if (as.this.w != 0) {
                                    int currentPosition = (as.this.i.getCurrentPosition() * 100) / as.this.w;
                                    if (currentPosition >= as.this.x * 25) {
                                        if (as.this.x == 0) {
                                            Log.log(as.a, "Video", String.format("started: %s%%", new Object[]{Integer.valueOf(currentPosition)}));
                                            asVar = as.this;
                                            trackingEvent = TrackingEvent.start;
                                        } else if (as.this.x == 1) {
                                            Log.log(as.a, "Video", String.format("at first quartile: %s%%", new Object[]{Integer.valueOf(currentPosition)}));
                                            asVar = as.this;
                                            trackingEvent = TrackingEvent.firstQuartile;
                                        } else if (as.this.x == 2) {
                                            Log.log(as.a, "Video", String.format("at midpoint: %s%%", new Object[]{Integer.valueOf(currentPosition)}));
                                            asVar = as.this;
                                            trackingEvent = TrackingEvent.midpoint;
                                        } else {
                                            if (as.this.x == 3) {
                                                Log.log(as.a, "Video", String.format("at third quartile: %s%%", new Object[]{Integer.valueOf(currentPosition)}));
                                                asVar = as.this;
                                                trackingEvent = TrackingEvent.thirdQuartile;
                                            }
                                            as.this.x = as.this.x + 1;
                                        }
                                        asVar.a(trackingEvent);
                                        as.this.x = as.this.x + 1;
                                    }
                                }
                            }
                            r0 = new Runnable() {
                                public void run() {
                                    as.this.i();
                                }
                            };
                        }
                        bq.a(r0);
                    } catch (Throwable th) {
                        Log.log(th);
                        bq.a((Runnable) new Runnable() {
                            public void run() {
                                as.this.l();
                            }
                        });
                    }
                }
            }, 0, (long) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        if (this.k != null) {
            this.k.cancel();
            this.k = null;
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        k();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        Log.log(a, LogConstants.EVENT_MV_PLAYER_ERROR, String.format("what: %s, extra: %s", new Object[]{Integer.valueOf(i2), Integer.valueOf(i3)}));
        l();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = MeasureSpec.getMode(i2);
        int mode2 = MeasureSpec.getMode(i3);
        int size = MeasureSpec.getSize(i2);
        int size2 = MeasureSpec.getSize(i3);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (mode != 1073741824) {
            size = mode == Integer.MIN_VALUE ? Math.min(size, measuredWidth) : measuredWidth;
        }
        int i4 = (int) (((float) size) * 0.5625f);
        if (mode2 == 1073741824 && size2 < i4) {
            size = (int) (((float) size2) * 1.7777778f);
            i4 = size2;
        }
        if (Math.abs(i4 - measuredHeight) >= 2 || Math.abs(size - measuredWidth) >= 2) {
            getLayoutParams().width = size;
            getLayoutParams().height = i4;
        }
        super.onMeasure(i2, i3);
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.log(a, LogConstants.EVENT_MV_PLAYER, "prepared");
        this.o = true;
        if (Native.b == NativeAdType.NoVideo) {
            return;
        }
        if (this.r) {
            i();
            return;
        }
        this.z = a.PAUSED;
        j();
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        try {
            if (Native.b != NativeAdType.NoVideo) {
                if (this.i == null) {
                    f();
                }
                this.y = new Surface(surfaceTexture);
                this.i.setSurface(this.y);
                g();
            }
        } catch (Exception e2) {
            Log.log(e2);
            this.z = a.IMAGE;
            j();
            this.s = false;
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        if (i2 == 0 || i3 == 0) {
            Log.log(a, "Video", "onVideoSizeChanged - skip: width or height is 0");
            return;
        }
        ViewGroup.LayoutParams layoutParams = this.j.getLayoutParams();
        int width = getWidth();
        int height = getHeight();
        if (i2 > i3) {
            layoutParams.width = width;
            layoutParams.height = (width * i3) / i2;
        } else {
            layoutParams.width = (i2 * height) / i3;
            layoutParams.height = height;
        }
        this.j.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        if (Native.b != NativeAdType.NoVideo) {
            if (i2 != 0) {
                e();
            } else if (this.r) {
                i();
            }
        }
        super.onWindowVisibilityChanged(i2);
    }

    public void setNativeAd(ax axVar) {
        this.b = axVar;
        if (Native.b != NativeAdType.NoVideo && ((axVar.g() != null && !axVar.g().isEmpty()) || (axVar.h() != null && !axVar.h().isEmpty()))) {
            this.s = true;
            if (axVar.i() != null) {
                this.v = axVar.i();
            }
        }
        a();
    }
}
