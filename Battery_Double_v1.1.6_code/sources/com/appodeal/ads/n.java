package com.appodeal.ads;

import android.support.annotation.NonNull;
import com.appodeal.ads.n;

abstract class n<SelfType extends n> {
    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d;
    @Deprecated
    @NonNull
    private String e;
    @Deprecated
    @NonNull
    private String f;

    n(@Deprecated @NonNull String str, @Deprecated @NonNull String str2) {
        this.e = str;
        this.f = str2;
    }

    public SelfType a(boolean z) {
        this.a = z;
        return this;
    }

    public boolean a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public SelfType b(boolean z) {
        this.b = z;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.b;
    }

    public SelfType c(boolean z) {
        this.c = z;
        return this;
    }

    public boolean c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public SelfType d(boolean z) {
        this.d = z;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    @NonNull
    public String e() {
        return a() ? this.f : this.e;
    }
}
