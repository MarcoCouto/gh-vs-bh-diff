package com.appodeal.ads;

import android.support.annotation.NonNull;
import com.appodeal.ads.b.d;

class l {
    @NonNull
    final d a;
    final boolean b;

    l(@NonNull d dVar) {
        this(dVar, false);
    }

    l(@NonNull d dVar, boolean z) {
        this.a = dVar;
        this.b = z;
    }
}
