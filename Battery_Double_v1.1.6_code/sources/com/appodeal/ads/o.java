package com.appodeal.ads;

import android.support.annotation.NonNull;
import com.appodeal.ads.i;

abstract class o<AdObjectType extends i> {
    private AdObjectType a;

    o() {
    }

    public void a() {
        this.a = null;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull m<AdObjectType> mVar, @NonNull AdObjectType adobjecttype) {
        if (adobjecttype.h()) {
            return true;
        }
        if (this.a == null || this.a.getEcpm() < adobjecttype.getEcpm()) {
            this.a = adobjecttype;
        }
        return !mVar.k(adobjecttype);
    }

    /* access modifiers changed from: 0000 */
    public AdObjectType b() {
        return this.a;
    }
}
