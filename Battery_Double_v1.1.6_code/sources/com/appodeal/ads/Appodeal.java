package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Pair;
import android.view.View;
import com.appodeal.ads.Native.MediaAssetType;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.b.d;
import com.appodeal.ads.b.i;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.PermissionsHelper;
import com.appodeal.ads.utils.PermissionsHelper.AppodealPermissionCallbacks;
import com.appodeal.ads.utils.ab;
import com.appodeal.ads.utils.b;
import com.appodeal.ads.utils.b.a;
import com.appodeal.ads.utils.c;
import com.appodeal.ads.utils.e;
import com.appodeal.ads.utils.f;
import com.appodeal.ads.utils.h;
import com.appodeal.ads.utils.k;
import com.appodeal.ads.utils.w;
import com.explorestack.iab.vast.VastUrlProcessorRegistry;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.logger.IronSourceError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.json.JSONObject;

public class Appodeal {
    public static final int BANNER = 4;
    public static final int BANNER_BOTTOM = 8;
    public static final int BANNER_TOP = 16;
    public static final int BANNER_VIEW = 64;
    public static final int INTERSTITIAL = 3;
    @Deprecated
    public static final int MREC = 256;
    public static final int NATIVE = 512;
    public static final int NONE = 0;
    public static final int NON_SKIPPABLE_VIDEO = 128;
    public static final int REWARDED_VIDEO = 128;
    public static final String a = "Appodeal";
    static boolean b = false;
    static boolean c = false;
    static boolean d = false;
    @SuppressLint({"StaticFieldLeak"})
    static Activity e;
    static Context f;
    public static String frameworkName = "android";
    @SuppressLint({"StaticFieldLeak"})
    static TestActivity g;
    static y h = new y();
    public static String i = null;
    private static f j = new f();
    public static String pluginVersion = null;

    static {
        VastUrlProcessorRegistry.register(new e());
        VastUrlProcessorRegistry.register(new f());
    }

    private Appodeal() {
    }

    private static double a(@Nullable m mVar) {
        return (mVar == null || !mVar.K()) ? Utils.DOUBLE_EPSILON : mVar.C();
    }

    private static void a(@Nullable Context context) {
        if (context != null) {
            if (context instanceof Activity) {
                e = (Activity) context;
            }
            if (f == null) {
                f = context.getApplicationContext();
            }
        }
    }

    private static void a(@NonNull Context context, @NonNull p pVar, @NonNull Set<String> set, int i2, int i3) {
        if ((i2 & i3) > 0) {
            set.addAll(pVar.q().b(context).b());
        }
    }

    private static void a(@NonNull BannerView bannerView) {
        if (bannerView == null) {
            Log.log(new a("Unable to set BannerView to null"));
            return;
        }
        aa.a().a(-1);
        aa.a().b((View) bannerView);
    }

    private static void a(@NonNull MrecView mrecView) {
        if (mrecView == null) {
            Log.log(new a("Unable to set MrecView to null"));
            return;
        }
        at.d().a(-1);
        at.d().b((View) mrecView);
    }

    private static void a(@NonNull p pVar, int i2, int i3) {
        if ((i2 & i3) > 0) {
            pVar.a((Context) e);
        }
    }

    private static void a(@NonNull p pVar, @NonNull String str, int i2, int i3) {
        if ((i2 & i3) > 0 && !pVar.l()) {
            pVar.q().a(str);
        }
    }

    @SuppressLint({"ObsoleteSdkInt"})
    static boolean a() {
        return VERSION.SDK_INT < 14;
    }

    private static boolean a(@NonNull Activity activity, int i2, @NonNull String str) {
        a aVar;
        Activity activity2 = activity;
        int i3 = i2;
        String str2 = str;
        if (activity2 == null) {
            aVar = new a("Unable to show an ad: activity = null");
        } else if (str2 == null) {
            aVar = new a("Unable to show an ad: placement = null");
        } else if (a()) {
            return false;
        } else {
            a((Context) activity);
            aj.a().b();
            try {
                d a2 = com.appodeal.ads.b.e.a(str);
                if (i3 == 4) {
                    if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                        aa.b().a(str2);
                    }
                    return aa.a(activity2, new bw(a2, aa.a().d()));
                } else if (i3 == 8) {
                    if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                        aa.b().a(str2);
                    }
                    return aa.a(activity2, new bw(a2, b.BOTTOM));
                } else if (i3 == 16) {
                    if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                        aa.b().a(str2);
                    }
                    return aa.a(activity2, new bw(a2, b.TOP));
                } else if (i3 == 64) {
                    if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                        aa.b().a(str2);
                    }
                    return aa.a(activity2, new bw(a2, b.VIEW));
                } else if (i3 == 128) {
                    if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                        bj.a().a(str2);
                    }
                    return bj.a(activity2, new l(a2));
                } else if (i3 != 256) {
                    switch (i3) {
                        case 1:
                            if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                                am.a().a(str2);
                            }
                            return am.a(activity2, new l(a2));
                        case 2:
                            if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                                bd.a().a(str2);
                            }
                            return bd.a(activity2, new l(a2));
                        default:
                            TreeMap treeMap = new TreeMap();
                            ad adVar = (ad) aa.b().y();
                            if ((i3 & 92) > 0 && a(adVar, a2)) {
                                if ((i3 & 4) > 0) {
                                    treeMap.put(Double.valueOf(((ac) adVar.b(str2)).getEcpm()), Integer.valueOf(4));
                                }
                                if ((i3 & 8) > 0) {
                                    treeMap.put(Double.valueOf(((ac) adVar.b(str2)).getEcpm()), Integer.valueOf(8));
                                }
                                if ((i3 & 16) > 0) {
                                    treeMap.put(Double.valueOf(((ac) adVar.b(str2)).getEcpm()), Integer.valueOf(16));
                                }
                                if ((i3 & 64) > 0) {
                                    treeMap.put(Double.valueOf(((ac) adVar.b(str2)).getEcpm()), Integer.valueOf(64));
                                }
                            }
                            aw awVar = (aw) at.a().y();
                            if ((i3 & 256) > 0 && a(awVar, a2)) {
                                treeMap.put(Double.valueOf(((av) awVar.b(str2)).getEcpm()), Integer.valueOf(256));
                            }
                            ap apVar = (ap) am.a().y();
                            int i4 = i3 & 1;
                            if (i4 > 0 && a(apVar, a2)) {
                                treeMap.put(Double.valueOf(((an) apVar.b(str2)).getEcpm()), Integer.valueOf(1));
                            }
                            bi biVar = (bi) bj.a().y();
                            if ((i3 & 128) > 0 && a(biVar, a2)) {
                                treeMap.put(Double.valueOf(((bh) biVar.b(str2)).getEcpm()), Integer.valueOf(128));
                            }
                            bt btVar = (bt) bd.a().y();
                            if ((i3 & 2) > 0 && a(btVar, a2)) {
                                treeMap.put(Double.valueOf(((br) btVar.b(str2)).getEcpm()), Integer.valueOf(2));
                            }
                            if (!treeMap.isEmpty()) {
                                int intValue = ((Integer) treeMap.lastEntry().getValue()).intValue();
                                if (intValue == 4) {
                                    return aa.a(activity2, new bw(a2, aa.a().d()));
                                }
                                if (intValue == 8) {
                                    return aa.a(activity2, new bw(a2, b.BOTTOM));
                                }
                                if (intValue == 16) {
                                    return aa.a(activity2, new bw(a2, b.TOP));
                                }
                                if (intValue == 64) {
                                    return aa.a(activity2, new bw(a2, b.VIEW));
                                }
                                if (intValue == 128) {
                                    return bj.a(activity2, new l(a2));
                                }
                                if (intValue == 256) {
                                    return at.a(activity2, new bw(a2, b.VIEW));
                                }
                                switch (intValue) {
                                    case 1:
                                        return am.a(activity2, new l(a2));
                                    case 2:
                                        return bd.a(activity2, new l(a2));
                                }
                            } else if (i4 <= 0) {
                                return false;
                            } else {
                                if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                                    am.a().a(str2);
                                }
                                return am.a(activity2, new l(a2));
                            }
                    }
                    return false;
                } else {
                    if (com.appodeal.ads.b.e.a(a2) && !com.appodeal.ads.b.e.a()) {
                        at.a().a(str2);
                    }
                    return at.a(activity2, new bw(a2, b.VIEW));
                }
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        Log.log(aVar);
        return false;
    }

    private static boolean a(@Nullable m mVar, @NonNull d dVar) {
        return mVar != null && mVar.K() && dVar.a(f, mVar.T().getCode(), mVar);
    }

    private static boolean a(@NonNull p pVar) {
        m y = pVar.y();
        return y != null && y.K();
    }

    private static boolean a(@NonNull p pVar, @NonNull d dVar, int i2, int i3) {
        if ((i2 & i3) > 0) {
            return a(pVar.y(), dVar);
        }
        return false;
    }

    static void b() {
        if (aa.b().b() && z.b()) {
            return;
        }
        if (Native.a().b() && z.b()) {
            return;
        }
        if ((!am.a().b() && !bd.a().b()) || !z.b()) {
            if ((!bj.a().b() || !z.b()) && at.a().b() && z.b()) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(int i2) {
        if (ag.c()) {
            setAutoCache(IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE, false);
            aa.b().a((Context) e);
            at.a().a((Context) e);
            am.a().a((Context) e);
            bd.a().a((Context) e);
            bj.a().a((Context) e);
            Native.a().a((Context) e);
            startTestActivity(e);
            return;
        }
        a(am.a(), i2, 1);
        a(bd.a(), i2, 2);
        a(bj.a(), i2, 128);
        a(aa.b(), i2, 92);
        a(at.a(), i2, 256);
        a(Native.a(), i2, 512);
        if (c) {
            b();
        }
    }

    static f c() {
        return j;
    }

    public static void cache(@NonNull Activity activity, int i2) {
        cache(activity, i2, 1);
    }

    public static void cache(@NonNull Activity activity, int i2, int i3) {
        if (activity == null) {
            a.k.b("activity is null");
        } else if (!a()) {
            a.k.a();
            a((Context) activity);
            if ((i2 & 3) > 0) {
                ap apVar = (ap) am.a().y();
                if ((((bt) bd.a().y()) == null && apVar == null) || !ao.a().b()) {
                    ao.a().c();
                    am.a().d((Context) activity);
                    bd.a().d((Context) activity);
                }
            }
            if ((i2 & 128) > 0) {
                bi biVar = (bi) bj.a().y();
                if (biVar == null || !bj.a().r()) {
                    if (biVar == null || biVar.M() || bj.a().F()) {
                        bj.a().d((Context) activity);
                    } else if (biVar.h()) {
                        bj.a.a(biVar, (bh) biVar.B());
                    }
                }
            }
            if ((i2 & 92) > 0) {
                aa.b().c((Context) activity);
            }
            if ((i2 & 256) > 0) {
                at.a().c((Context) activity);
            }
            if ((i2 & 512) > 0) {
                Native.c().a(i3);
                Native.c().a();
            }
        }
    }

    public static boolean canShow(int i2) {
        return canShow(i2, "default");
    }

    public static boolean canShow(int i2, @NonNull String str) {
        c cVar;
        String str2;
        boolean z = false;
        if (!c) {
            cVar = a.K;
            str2 = "Appodeal is not initialized";
        } else if (!bq.a(f)) {
            cVar = a.K;
            str2 = "no Internet";
        } else if (str == null) {
            cVar = a.K;
            str2 = "placement is null";
        } else {
            a.K.a();
            try {
                d a2 = com.appodeal.ads.b.e.a(str);
                if (a(aa.b(), a2, i2, 92) || a(at.a(), a2, i2, 256) || a(am.a(), a2, i2, 1) || a(bj.a(), a2, i2, 128) || a(bd.a(), a2, i2, 2)) {
                    z = true;
                }
                return z;
            } catch (Exception e2) {
                Log.log(e2);
                return false;
            }
        }
        cVar.b(str2);
        return false;
    }

    public static void destroy(int i2) {
        a.Q.a();
        if ((i2 & 92) > 0) {
            try {
                aa.g();
            } catch (Exception e2) {
                Log.log(e2);
                return;
            }
        }
        if ((i2 & 256) > 0) {
            at.c();
        }
    }

    public static void disableLocationPermissionCheck() {
        a.y.a();
        PermissionsHelper.b = false;
        c.a();
    }

    public static void disableNetwork(@NonNull Context context, @NonNull String str) {
        disableNetwork(context, str, IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE);
    }

    public static void disableNetwork(@NonNull Context context, @NonNull String str, int i2) {
        if (context == null) {
            a.x.b("context is null");
        } else if (TextUtils.isEmpty(str)) {
            a.x.b("network is null or empty");
        } else if (!a()) {
            a.x.a(String.format("%s - %s", new Object[]{str, bq.a(i2)}));
            a(context);
            a(am.a(), str, i2, 1);
            a(bd.a(), str, i2, 2);
            a(bj.a(), str, i2, 128);
            a(aa.b(), str, i2, 92);
            a(at.a(), str, i2, 256);
            a(Native.a(), str, i2, 512);
        }
    }

    public static void disableWebViewCacheClear() {
        a.N.a();
        z.f = false;
    }

    public static void disableWriteExternalStoragePermissionCheck() {
        a.z.a();
        PermissionsHelper.a = false;
        c.b();
    }

    public static int getAvailableNativeAdsCount() {
        Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_GET, "available Native Ads count");
        return Native.c().c();
    }

    public static BannerView getBannerView(@NonNull Activity activity) {
        Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_GET, "BannerView", LogLevel.verbose);
        if (activity == null) {
            Log.log(new a("Unable to getAdView: activity = null"));
            return null;
        }
        BannerView bannerView = new BannerView(activity, null);
        a(bannerView);
        return bannerView;
    }

    public static Date getBuildDate() {
        return com.appodeal.sdk.a.a;
    }

    public static LogLevel getLogLevel() {
        return z.c;
    }

    @Deprecated
    public static MrecView getMrecView(@NonNull Activity activity) {
        Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_GET, "MrecView", LogLevel.verbose);
        if (activity == null) {
            Log.log(new a("Unable to get MrecView: activity = null"));
            return null;
        }
        MrecView mrecView = new MrecView(activity, null);
        a(mrecView);
        return mrecView;
    }

    public static NativeAdType getNativeAdType() {
        return Native.b;
    }

    public static List<NativeAd> getNativeAds(int i2) {
        Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_GET, String.format("NativeAds: %s", new Object[]{Integer.valueOf(i2)}), LogLevel.verbose);
        return Native.c().b(i2);
    }

    public static List<String> getNetworks(@NonNull Context context, int i2) {
        if (context == null) {
            Log.log(new a("Context not provided"));
            return Collections.emptyList();
        }
        a(context);
        HashSet hashSet = new HashSet();
        a(context, am.a(), hashSet, i2, 1);
        a(context, bd.a(), hashSet, i2, 2);
        a(context, bj.a(), hashSet, i2, 128);
        a(context, aa.b(), hashSet, i2, 92);
        a(context, at.a(), hashSet, i2, 256);
        a(context, Native.a(), hashSet, i2, 512);
        ArrayList arrayList = new ArrayList(hashSet);
        Collections.sort(arrayList);
        return arrayList;
    }

    public static double getPredictedEcpm(int i2) {
        if (!(i2 == 8 || i2 == 16 || i2 == 64)) {
            if (i2 == 128) {
                return a(bj.a().y());
            }
            if (i2 == 256) {
                return a(at.a().y());
            }
            switch (i2) {
                case 1:
                    return a(am.a().y());
                case 2:
                    return a(bd.a().y());
                case 3:
                    return Math.max(getPredictedEcpm(1), getPredictedEcpm(2));
                case 4:
                    break;
                default:
                    return Utils.DOUBLE_EPSILON;
            }
        }
        return a(aa.b().y());
    }

    public static Pair<Double, String> getRewardParameters() {
        return getRewardParameters("default");
    }

    public static Pair<Double, String> getRewardParameters(@NonNull String str) {
        if (str == null) {
            Log.log(new a("Unable to get reward parameters: placement = null"));
            return new Pair<>(Double.valueOf(Utils.DOUBLE_EPSILON), null);
        }
        d a2 = com.appodeal.ads.b.e.a(str);
        return new Pair<>(Double.valueOf(a2.h()), a2.g());
    }

    public static w getSession() {
        return w.a();
    }

    @Nullable
    public static Integer getUserAge() {
        return bp.a().getAge();
    }

    @Nullable
    public static Gender getUserGender() {
        return bp.a().getGender();
    }

    @Nullable
    public static String getUserId() {
        return bp.a().getUserId();
    }

    @Deprecated
    public static UserSettings getUserSettings(@NonNull Context context) {
        if (context == null) {
            Log.log(new a("Unable to get user settings: context = null"));
            return null;
        }
        Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_GET, "user settings", LogLevel.verbose);
        return bp.a();
    }

    public static String getVersion() {
        return "2.6.4";
    }

    public static void hide(@NonNull Activity activity, int i2) {
        if (activity == null) {
            a.m.b("activity is null");
        } else if (!a()) {
            a.m.a(bq.a(i2));
            a((Context) activity);
            if ((i2 & 92) > 0) {
                aa.a(activity);
            }
            if ((i2 & 256) > 0) {
                at.a(activity);
            }
        }
    }

    @SuppressLint({"NewApi"})
    public static void initialize(@NonNull Activity activity, @NonNull String str, int i2) {
        initialize(activity, str, i2, be.e());
    }

    public static void initialize(@NonNull Activity activity, @NonNull final String str, final int i2, final boolean z) {
        c cVar;
        String str2;
        if (activity == null) {
            cVar = a.a;
            str2 = "activity is null";
        } else if (str == null) {
            cVar = a.a;
            str2 = "appKey is null";
        } else if (!a()) {
            a.a.a();
            a((Context) activity);
            if (c || b) {
                b(i2);
            } else {
                b = true;
                bm.a(activity);
                b.a(activity, new b.d() {
                    public void a(@NonNull b.a aVar) {
                        com.appodeal.ads.utils.app.b[] values;
                        String str;
                        String str2;
                        String format;
                        be.b(aVar.b());
                        be.a(aVar.a());
                        try {
                            if (!bq.a(Appodeal.e)) {
                                new aq.c(Appodeal.f, "install").a(Appodeal.f.getPackageName()).a().a();
                            }
                            bq.a(Appodeal.e, str);
                            be.a(z);
                            ag.a((Context) Appodeal.e);
                            bq.b(Appodeal.e);
                            Appodeal.getSession().a(Appodeal.f);
                            c.a(Appodeal.e);
                            c.d(Appodeal.e);
                            c.a((Context) Appodeal.e);
                            h.a(Appodeal.e);
                            com.appodeal.ads.utils.a.b.d(Appodeal.e);
                            com.appodeal.ads.b.e.a((Context) Appodeal.e);
                            Application application = Appodeal.e.getApplication();
                            a aVar2 = new a();
                            application.registerActivityLifecycleCallbacks(aVar2);
                            application.registerComponentCallbacks(aVar2);
                            for (com.appodeal.ads.utils.app.b bVar : com.appodeal.ads.utils.app.b.values()) {
                                application.registerActivityLifecycleCallbacks(bVar);
                                application.registerComponentCallbacks(bVar);
                            }
                            aj.a();
                            if (Appodeal.h == null) {
                                Appodeal.h = new y();
                            }
                            Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_INITIALIZE, String.format("v%s/%s initialized, appKey: %s, package name: %s, consent: %b", new Object[]{"2.6.4", DateFormat.format("ddMMyy", com.appodeal.sdk.a.a).toString(), str, Appodeal.f.getPackageName(), Boolean.valueOf(z)}));
                            String str3 = LogConstants.KEY_SDK_PUBLIC;
                            String str4 = LogConstants.EVENT_INITIALIZE;
                            String str5 = "Current device is: %s";
                            Object[] objArr = new Object[1];
                            objArr[0] = bq.i() ? "Emulator" : "Real Device";
                            Log.log(str3, str4, String.format(str5, objArr));
                            if (!(Appodeal.frameworkName == null || Appodeal.pluginVersion == null)) {
                                if (Appodeal.i != null) {
                                    str = LogConstants.KEY_SDK_PUBLIC;
                                    str2 = LogConstants.EVENT_INITIALIZE;
                                    format = String.format("For %s v%s ev%s", new Object[]{Appodeal.frameworkName, Appodeal.pluginVersion, Appodeal.i});
                                } else {
                                    str = LogConstants.KEY_SDK_PUBLIC;
                                    str2 = LogConstants.EVENT_INITIALIZE;
                                    format = String.format("For %s v%s", new Object[]{Appodeal.frameworkName, Appodeal.pluginVersion});
                                }
                                Log.log(str, str2, format);
                            }
                            Log.log(LogConstants.KEY_SDK_PUBLIC, LogConstants.EVENT_INITIALIZE, String.format("Google play services version: %s", new Object[]{bq.p(Appodeal.e)}));
                            if (VERSION.SDK_INT >= 26 && c.d((Context) Appodeal.e) >= 26) {
                                try {
                                    AppodealPackageAddedReceiver appodealPackageAddedReceiver = new AppodealPackageAddedReceiver();
                                    IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
                                    intentFilter.addDataScheme("package");
                                    Appodeal.f.registerReceiver(appodealPackageAddedReceiver, intentFilter);
                                } catch (Exception e) {
                                    Log.log(e);
                                }
                            }
                            new aq.c(Appodeal.e, "init").a((aq.a<AdRequestType>) new x<AdRequestType>()).a().a();
                        } catch (Exception e2) {
                            Log.log(e2);
                            Appodeal.b = false;
                        }
                        Appodeal.b(i2);
                    }
                }, new Runnable() {
                    public void run() {
                        if (!k.a(Appodeal.f)) {
                            Log.log(new a("Failed to load classes for required libraries"));
                        }
                        h.c(Appodeal.f);
                    }
                });
            }
            return;
        } else {
            return;
        }
        cVar.b(str2);
    }

    public static boolean isAutoCacheEnabled(int i2) {
        if (!(i2 == 8 || i2 == 16 || i2 == 64)) {
            if (i2 == 128) {
                return bj.a().r();
            }
            if (i2 == 256) {
                return at.a().r();
            }
            if (i2 == 512) {
                return Native.a().r();
            }
            switch (i2) {
                case 3:
                    return ao.a().b();
                case 4:
                    break;
                default:
                    return false;
            }
        }
        return aa.b().r();
    }

    public static boolean isInitialized(int i2) {
        p b2;
        if (!(i2 == 8 || i2 == 16 || i2 == 64)) {
            if (i2 == 128) {
                b2 = bj.a();
            } else if (i2 == 256) {
                b2 = at.a();
            } else if (i2 != 512) {
                boolean z = false;
                switch (i2) {
                    case 1:
                        b2 = am.a();
                        break;
                    case 2:
                        b2 = bd.a();
                        break;
                    case 3:
                        if (am.a().l() && bd.a().l()) {
                            z = true;
                        }
                        return z;
                    case 4:
                        break;
                    default:
                        return false;
                }
            } else {
                b2 = Native.a();
            }
            return b2.l();
        }
        b2 = aa.b();
        return b2.l();
    }

    public static boolean isLoaded(int i2) {
        if (a()) {
            return false;
        }
        if ((i2 & 3) > 0) {
            try {
                if (a(am.a()) || a(bd.a())) {
                    return true;
                }
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
        if ((i2 & 128) > 0 && a(bj.a())) {
            return true;
        }
        if ((i2 & 92) > 0 && a(aa.b())) {
            return true;
        }
        if ((i2 & 256) <= 0 || !a(at.a())) {
            return (i2 & 512) > 0 && Native.c().b();
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0091 A[RETURN] */
    public static boolean isPrecache(int i2) {
        if (a()) {
            return false;
        }
        if (!(i2 == 8 || i2 == 16 || i2 == 64)) {
            if (i2 == 128) {
                return false;
            }
            if (i2 != 256) {
                switch (i2) {
                    case 3:
                        try {
                            ap apVar = (ap) am.a().y();
                            bt btVar = (bt) bd.a().y();
                            return (apVar == null || btVar == null) ? (btVar == null && apVar != null && apVar.L()) || (apVar == null && btVar != null && btVar.L()) : apVar.C() > btVar.C() ? apVar.L() : btVar.L();
                        } catch (Exception e2) {
                            Log.log(e2);
                        }
                    case 4:
                        break;
                }
            } else {
                aw awVar = (aw) at.a().y();
                if (awVar != null && awVar.L()) {
                    return true;
                }
            }
        }
        ad adVar = (ad) aa.b().y();
        if (adVar != null && adVar.L()) {
            return true;
        }
    }

    public static boolean isSmartBannersEnabled() {
        return aa.b;
    }

    public static void muteVideosIfCallsMuted(boolean z) {
        a.M.a(String.format("muteVideosIfCallsMuted: %s", new Object[]{Boolean.valueOf(z)}));
        z.d = z;
    }

    public static void onResume(@NonNull Activity activity, int i2) {
        c cVar;
        String str;
        if (activity == null) {
            cVar = a.v;
            str = "activity is null";
        } else if (!a()) {
            if (!c) {
                cVar = a.v;
                str = "Appodeal is not initialized";
            } else {
                a.v.a(String.format("called for %s", new Object[]{bq.a(i2)}));
                a((Context) activity);
                if ((i2 & 92) > 0 && aa.a().b(activity, aa.b())) {
                    show(activity, i2, aa.b().v());
                }
                if ((i2 & 256) > 0 && at.d().b(activity, at.a())) {
                    show(activity, i2, at.a().v());
                }
                return;
            }
        } else {
            return;
        }
        cVar.b(str);
    }

    public static void requestAndroidMPermissions(@NonNull Activity activity, @Nullable AppodealPermissionCallbacks appodealPermissionCallbacks) {
        if (activity == null) {
            a.J.b("activity is null");
            return;
        }
        a.J.a();
        PermissionsHelper.a().a(activity, appodealPermissionCallbacks);
    }

    public static void set728x90Banners(boolean z) {
        a.r.a(String.format("728x90 Banners: %s", new Object[]{Boolean.valueOf(z)}));
        aa.c = z;
    }

    public static void setAutoCache(int i2, boolean z) {
        a.n.a(String.format("auto cache for %s: %s", new Object[]{bq.a(i2), Boolean.valueOf(z)}));
        if ((i2 & 3) > 0) {
            ao.a().a(z);
        }
        if ((i2 & 128) > 0) {
            bj.a().a(z);
        }
        if ((i2 & 92) > 0) {
            aa.b().a(z);
        }
        if ((i2 & 256) > 0) {
            at.a().a(z);
        }
        if ((i2 & 512) > 0) {
            Native.a().a(z);
        }
    }

    public static void setBannerAnimation(boolean z) {
        a.s.a(String.format("Banner animation: %s", new Object[]{Boolean.valueOf(z)}));
        aa.a().a(z);
    }

    public static void setBannerCallbacks(BannerCallbacks bannerCallbacks) {
        a.g.a();
        aa.a.a(bannerCallbacks);
    }

    public static void setBannerViewId(int i2) {
        a.p.a(String.format("Banner ViewId: %s", new Object[]{Integer.valueOf(i2)}));
        aa.a().a(i2);
        aa.a().b((View) null);
    }

    public static void setChildDirectedTreatment(@Nullable Boolean bool) {
        a.P.a(String.valueOf(bool));
        w.a(bool);
    }

    public static void setExtraData(@NonNull String str, double d2) {
        a.T.a();
        ExtraData.a(str, Double.valueOf(d2));
    }

    public static void setExtraData(@NonNull String str, int i2) {
        a.S.a();
        ExtraData.a(str, Integer.valueOf(i2));
    }

    public static void setExtraData(@NonNull String str, @NonNull String str2) {
        a.R.a();
        ExtraData.a(str, str2);
    }

    public static void setExtraData(@NonNull String str, @NonNull JSONObject jSONObject) {
        a.V.a();
        ExtraData.a(str, jSONObject);
    }

    public static void setExtraData(@NonNull String str, boolean z) {
        a.U.a();
        ExtraData.a(str, Boolean.valueOf(z));
    }

    public static void setFramework(String str, String str2) {
        setFramework(str, str2, null, false, true);
    }

    public static void setFramework(String str, String str2, String str3) {
        setFramework(str, str2, str3, false, true);
    }

    public static void setFramework(String str, String str2, String str3, boolean z, boolean z2) {
        frameworkName = str;
        pluginVersion = str2;
        i = str3;
        z.h = z;
        ab.a(z2);
        if (str3 != null) {
            a.L.a(String.format("framework: %s, pluginVersion: %s, engineVersion: %s, bannerShowsInPopUp: %s, bannerCheckWindowFocus: %s", new Object[]{str, str2, str3, Boolean.valueOf(z), Boolean.valueOf(z2)}));
            return;
        }
        a.L.a(String.format("framework: %s, pluginVersion: %s, bannerShowsInPopUp: %s, bannerCheckWindowFocus: %s", new Object[]{str, str2, Boolean.valueOf(z), Boolean.valueOf(z2)}));
    }

    public static void setInterstitialCallbacks(InterstitialCallbacks interstitialCallbacks) {
        a.d.a();
        ao.a().a(interstitialCallbacks);
    }

    public static void setLogLevel(LogLevel logLevel) {
        z.c = logLevel;
        a.E.a(String.format("log level: %s", new Object[]{logLevel}));
    }

    @Deprecated
    public static void setMrecCallbacks(MrecCallbacks mrecCallbacks) {
        a.h.a();
        at.a.a(mrecCallbacks);
    }

    @Deprecated
    public static void setMrecViewId(int i2) {
        a.t.a(String.format("Mrec ViewId: %s", new Object[]{Integer.valueOf(i2)}));
        at.d().a(i2);
        at.d().b((View) null);
    }

    public static void setNativeAdType(@NonNull NativeAdType nativeAdType) {
        if (nativeAdType == null) {
            a.j.b("adType is null");
            return;
        }
        a.j.a(String.format("NativeAd type: %s", new Object[]{nativeAdType.toString()}));
        Native.b = nativeAdType;
    }

    public static void setNativeCallbacks(NativeCallbacks nativeCallbacks) {
        a.i.a();
        ay.a(nativeCallbacks);
    }

    public static void setNonSkippableVideoCallbacks(NonSkippableVideoCallbacks nonSkippableVideoCallbacks) {
        a.f.a();
        bj.a.a(nonSkippableVideoCallbacks);
    }

    public static void setRequestCallbacks(AppodealRequestCallbacks appodealRequestCallbacks) {
        a.c.a();
        h = new y(appodealRequestCallbacks);
    }

    public static void setRequiredNativeMediaAssetType(MediaAssetType mediaAssetType) {
        a.u.a(String.format("required native media assets type: %s", new Object[]{mediaAssetType}));
        Native.c = mediaAssetType;
    }

    public static void setRewardedVideoCallbacks(RewardedVideoCallbacks rewardedVideoCallbacks) {
        a.e.a();
        bj.a.a(rewardedVideoCallbacks);
    }

    public static void setSegmentFilter(@NonNull String str, double d2) {
        if (str == null) {
            a.H.b("name is null");
            return;
        }
        a.H.a(String.format("custom segment filter name: %s, value: %s", new Object[]{str, Double.valueOf(d2)}));
        i.a(f, str, Float.valueOf((float) d2));
    }

    public static void setSegmentFilter(@NonNull String str, int i2) {
        if (str == null) {
            a.G.b("name is null");
            return;
        }
        a.G.a(String.format("custom segment filter name: %s, value: %s", new Object[]{str, Integer.valueOf(i2)}));
        i.a(f, str, Float.valueOf((float) i2));
    }

    public static void setSegmentFilter(@NonNull String str, @NonNull String str2) {
        if (str == null) {
            a.I.b("name is null");
        } else if (str2 == null) {
            a.I.b("value is null");
        } else {
            a.I.a(String.format("custom segment filter name: %s, value: %s", new Object[]{str, str2}));
            i.a(f, str, str2);
        }
    }

    public static void setSegmentFilter(@NonNull String str, boolean z) {
        if (str == null) {
            a.F.b("name is null");
            return;
        }
        a.F.a(String.format("custom segment filter name: %s, value: %s", new Object[]{str, Boolean.valueOf(z)}));
        i.a(f, str, Boolean.valueOf(z));
    }

    public static void setSmartBanners(boolean z) {
        a.q.a(String.format("smart Banners: %s", new Object[]{Boolean.valueOf(z)}));
        aa.b = z;
    }

    public static void setTesting(boolean z) {
        a.D.a(String.format("testing: %s", new Object[]{Boolean.valueOf(z)}));
        z.b = z;
    }

    public static void setTriggerOnLoadedOnPrecache(int i2, boolean z) {
        a.o.a(String.format("triggerOnLoadedOnPrecache for %s: %s", new Object[]{bq.a(i2), Boolean.valueOf(z)}));
        if ((i2 & 3) > 0) {
            am.a().b(z);
            bd.a().b(z);
        }
        if ((i2 & 128) > 0) {
            bj.a().b(z);
        }
        if ((i2 & 92) > 0) {
            aa.b().b(z);
        }
        if ((i2 & 256) > 0) {
            at.a().b(z);
        }
    }

    public static void setUserAge(int i2) {
        a.C.a();
        bp.a().setAge(i2);
    }

    public static void setUserGender(@NonNull Gender gender) {
        a.B.a();
        bp.a().setGender(gender);
    }

    public static void setUserId(@NonNull String str) {
        a.A.a();
        bp.a().setUserId(str);
    }

    public static boolean show(@NonNull Activity activity, int i2) {
        return show(activity, i2, "default");
    }

    public static boolean show(@NonNull Activity activity, int i2, @NonNull String str) {
        boolean a2 = a(activity, i2, str);
        a.l.a(String.format("%s, result: %s", new Object[]{bq.a(i2), Boolean.valueOf(a2)}));
        return a2;
    }

    public static void startTestActivity(@NonNull Activity activity) {
        if (activity == null) {
            a.O.b("activity is null");
            return;
        }
        a.O.a();
        a((Context) activity);
        bq.c(activity);
    }

    public static void trackInAppPurchase(@NonNull Context context, double d2, @NonNull String str) {
        if (!c) {
            a.w.b("Appodeal is not initialized");
        } else if (context == null) {
            a.w.b("context is null");
        } else if (str == null) {
            a.w.b("currency is null");
        } else if (bg.a.isUserGdprProtected()) {
            a.w.b("The user did not accept the agreement");
        } else {
            a.w.a(String.format("inapp purchase, amount: %s, currency: %s", new Object[]{Double.valueOf(d2), str}));
            new aq.c(context, "iap").a((aq.a<AdRequestType>) new com.appodeal.ads.b.a<AdRequestType>(context)).a(d2, str).a().a();
        }
    }

    public static void updateConsent(boolean z) {
        a.b.a(String.format("consent is %b", new Object[]{Boolean.valueOf(z)}));
        be.a(z);
    }
}
