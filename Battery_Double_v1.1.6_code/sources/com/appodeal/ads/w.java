package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.utils.Log;
import com.appodealx.sdk.utils.RequestInfoKeys;
import org.json.JSONObject;

class w {
    static String a = null;
    static String b = null;
    static Integer c = null;
    static String d = null;
    static String e = null;
    @VisibleForTesting
    static boolean f = false;
    static JSONObject g = null;
    static int h = 0;
    static boolean i = true;
    @Nullable
    @VisibleForTesting
    static Boolean j;
    @Nullable
    @VisibleForTesting
    static Boolean k;

    static void a(@Nullable Context context, @Nullable JSONObject jSONObject) {
        String sb;
        if (!(context == null || jSONObject == null)) {
            JSONObject optJSONObject = jSONObject.optJSONObject("app_data");
            if (optJSONObject != null) {
                if (optJSONObject.has("store_url")) {
                    sb = optJSONObject.optString("store_url", b);
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("https://play.google.com/store/apps/details?id=");
                    sb2.append(context.getPackageName());
                    sb = sb2.toString();
                }
                b = sb;
                a = optJSONObject.optString("name");
                if (TextUtils.isEmpty(a)) {
                    try {
                        a = (String) context.getPackageManager().getApplicationLabel(context.getApplicationInfo());
                    } catch (Exception e2) {
                        Log.log(e2);
                    }
                }
                if (optJSONObject.has("paid")) {
                    c = Integer.valueOf(optJSONObject.optInt("paid"));
                }
                d = optJSONObject.optString("publisher", null);
                if (optJSONObject.has("id")) {
                    e = String.valueOf(optJSONObject.optInt("id"));
                }
                g = optJSONObject.optJSONObject(RequestInfoKeys.EXT);
                h = optJSONObject.optInt("ad_box_size");
                i = optJSONObject.optBoolean("hr", true);
            }
        }
    }

    static void a(@Nullable Boolean bool) {
        boolean b2 = b();
        j = bool;
        if (b2 != b()) {
            z.a();
        }
    }

    static void a(@Nullable JSONObject jSONObject) {
        if (jSONObject != null) {
            f = jSONObject.optBoolean("corona");
        }
    }

    static boolean a() {
        return f;
    }

    static void b(@Nullable JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.has("for_kids")) {
            boolean b2 = b();
            k = Boolean.valueOf(jSONObject.optBoolean("for_kids", false));
            if (b2 != b()) {
                z.a();
            }
        }
    }

    static boolean b() {
        Boolean bool;
        if (j != null) {
            bool = j;
        } else if (k == null) {
            return false;
        } else {
            bool = k;
        }
        return bool.booleanValue();
    }
}
