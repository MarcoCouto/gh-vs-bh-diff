package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.facebook.share.internal.MessengerShareContentUtility;

class ai {

    static final class a {
        static c A = a("setUserId");
        static c B = a("setUserGender");
        static c C = a("setUserAge");
        static c D = a("setTesting");
        static c E = a("setLogLevel");
        static c F = a("setSegmentFilter");
        static c G = a("setSegmentFilter");
        static c H = a("setSegmentFilter");
        static c I = a("setSegmentFilter");
        static c J = a("requestAndroidMPermissions");
        static c K = a("canShow");
        static c L = a("setFramework");
        static c M = a("muteVideosIfCallsMuted");
        static c N = a("disableWebViewCacheClear");
        static c O = a("startTestActivity");
        static c P = a("setChildDirectedTreatment");
        static c Q = a("destroy");
        static c R = a("setExtraData");
        static c S = a("setExtraData");
        static c T = a("setExtraData");
        static c U = a("setExtraData");
        static c V = a("setExtraData");
        static c a = a("initialize");
        static c b = a("updateConsent");
        static c c = a("setRequestCallbacks");
        static c d = a("setInterstitialCallbacks");
        static c e = a("setRewardedVideoCallbacks");
        static c f = a("setNonSkippableVideoCallbacks");
        static c g = a("setBannerCallbacks");
        static c h = a("setMrecCallbacks");
        static c i = a("setNativeCallbacks");
        static c j = a("setNativeAdType");
        static c k = a("cache");
        static c l = a("show");
        static c m = a(MessengerShareContentUtility.SHARE_BUTTON_HIDE);
        static c n = a("setAutoCache");
        static c o = a("setTriggerOnLoadedOnPreCache");
        static c p = a("setBannerViewId");
        static c q = a("setSmartBanners");
        static c r = a("set728x90Banners");
        static c s = a("setBannerAnimation");
        static c t = a("setMrecViewId");
        static c u = a("setRequiredNativeMediaAssetType");
        static c v = a("onResume");
        static c w = a("trackInAppPurchase");
        static c x = a("disableNetwork");
        static c y = a("disableLocationPermissionCheck");
        static c z = a("disableWriteExternalStoragePermissionCheck");

        private static c a(String str) {
            return new c("Appodeal", str);
        }
    }

    static final class b {
        static c a = a("setTitleView");
        static c b = a("setCallToActionView");
        static c c = a("setRatingView");
        static c d = a("setDescriptionView");
        static c e = a("setProviderView");
        static c f = a("setNativeIconView");
        static c g = a("setNativeMediaView");
        static c h = a("registerView");
        static c i = a("unregisterViewForInteraction");
        static c j = a("destroy");

        private static c a(String str) {
            return new c("NativeAdView", str);
        }
    }

    static final class c {
        private String a;
        private String b;

        private c(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            a(null);
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable String str) {
            String str2;
            String str3 = LogConstants.KEY_SDK_PUBLIC;
            String str4 = this.a;
            if (TextUtils.isEmpty(str)) {
                str2 = this.b;
            } else {
                str2 = String.format("%s. %s", new Object[]{this.b, bq.c(str)});
            }
            Log.log(str3, str4, str2, LogLevel.verbose);
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull String str) {
            Log.log(LogConstants.KEY_SDK_PUBLIC, this.a, String.format("%s. Error during executing method - %s", new Object[]{this.b, str}), LogLevel.verbose);
            aj.a().a((Throwable) new com.appodeal.ads.utils.b.a(str));
        }
    }
}
