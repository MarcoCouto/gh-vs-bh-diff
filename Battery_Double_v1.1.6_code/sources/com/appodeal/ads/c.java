package com.appodeal.ads;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.i;
import com.appodeal.ads.m;

abstract class c<AdRequestType extends m<AdObjectType>, AdObjectType extends i, ReferenceObjectType> {
    c() {
    }

    /* access modifiers changed from: 0000 */
    @MainThread
    public abstract void a(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype);

    /* access modifiers changed from: 0000 */
    @MainThread
    public abstract void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable LoadingError loadingError);

    /* access modifiers changed from: 0000 */
    @MainThread
    public abstract void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype);

    /* access modifiers changed from: 0000 */
    @MainThread
    public abstract void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype, @Nullable LoadingError loadingError);

    /* access modifiers changed from: 0000 */
    @MainThread
    public abstract void b(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype);

    /* access modifiers changed from: 0000 */
    @MainThread
    public abstract void b(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype);

    /* access modifiers changed from: 0000 */
    @MainThread
    public void c(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype) {
    }

    /* access modifiers changed from: 0000 */
    @MainThread
    public void c(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype) {
    }
}
