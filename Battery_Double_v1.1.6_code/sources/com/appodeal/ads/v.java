package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.api.Get;
import com.appodeal.ads.api.Request.Builder;
import com.appodeal.ads.api.Stats;
import com.appodeal.ads.bb.h;
import com.explorestack.protobuf.AbstractMessageLite;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class v<RequestDataType, RequestResultType> extends bb<RequestDataType, RequestResultType, LoadingError> {
    /* access modifiers changed from: private */
    public m c;
    /* access modifiers changed from: private */
    public i d;
    private String e;
    /* access modifiers changed from: private */
    public double f;
    private boolean g;

    public interface a<RequestResultType> extends com.appodeal.ads.bb.b<RequestResultType, LoadingError> {
    }

    private static abstract class b<RequestDataType extends AbstractMessageLite, RequestResultType> extends f<RequestDataType, RequestResultType, LoadingError> {
        private b() {
        }

        /* access modifiers changed from: protected */
        public abstract void a(Builder builder, RequestDataType requestdatatype);

        /* access modifiers changed from: protected */
        @Nullable
        public byte[] a(bb<RequestDataType, RequestResultType, LoadingError> bbVar, URLConnection uRLConnection, RequestDataType requestdatatype) {
            if (bbVar instanceof v) {
                try {
                    v vVar = (v) bbVar;
                    Builder a = bf.a(Appodeal.f, bg.a, vVar.c, vVar.d, vVar.f);
                    a(a, requestdatatype);
                    return a.build().toByteArray();
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unknown exception: ");
                    sb.append(e.getMessage());
                    throw new IllegalArgumentException(sb.toString());
                }
            } else {
                throw new IllegalArgumentException("Unknown exception");
            }
        }
    }

    public static class c<RequestDataType, RequestResultType> {
        private d a;
        private a<RequestResultType> b;
        private RequestDataType c;
        private m d;
        private i e;
        @Deprecated
        private String f;
        @Deprecated
        private double g;
        @Deprecated
        private boolean h;

        public c(d dVar) {
            this.a = dVar;
        }

        public c<RequestDataType, RequestResultType> a(i iVar) {
            this.e = iVar;
            return this;
        }

        public c<RequestDataType, RequestResultType> a(m mVar) {
            this.d = mVar;
            return this;
        }

        public c<RequestDataType, RequestResultType> a(RequestDataType requestdatatype) {
            this.c = requestdatatype;
            return this;
        }

        public v<RequestDataType, RequestResultType> a() {
            v<RequestDataType, RequestResultType> vVar = new v<>(this.a.c, this.a.d, this.c);
            vVar.a((com.appodeal.ads.bb.b<RequestResultType, ErrorResultType>) this.b);
            vVar.a(this.a.e);
            vVar.a(this.d);
            vVar.a(this.e);
            vVar.a(this.f);
            vVar.a(this.g);
            vVar.a(this.h);
            return vVar;
        }

        public v<RequestDataType, RequestResultType> b() {
            v<RequestDataType, RequestResultType> a2 = a();
            a2.c();
            return a2;
        }
    }

    enum d {
        Stats("stats", d.Post, new b<Stats, Object>() {
            /* access modifiers changed from: protected */
            public Object a(bb bbVar, URLConnection uRLConnection, byte[] bArr) throws Exception {
                return null;
            }

            /* access modifiers changed from: protected */
            public void a(Builder builder, Stats stats) {
                builder.setStats(stats);
            }
        }),
        Get("get", d.Get, new b<Get, Object>() {
            /* access modifiers changed from: protected */
            public Object a(bb<Get, Object, LoadingError> bbVar, URLConnection uRLConnection, byte[] bArr) throws Exception {
                return null;
            }

            /* access modifiers changed from: protected */
            public void a(Builder builder, Get get) {
                builder.setGet(get);
            }
        });
        
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public d d;
        /* access modifiers changed from: private */
        public g e;

        private d(String str, d dVar, g gVar) {
            this.c = str;
            this.d = dVar;
            this.e = gVar;
        }
    }

    private v(@NonNull String str, @NonNull d dVar, @Nullable RequestDataType requestdatatype) {
        super(str, dVar, requestdatatype);
        a((h<RequestDataType, RequestResultType, ErrorResultType>) a);
    }

    @Deprecated
    private URL b(@NonNull String str) throws MalformedURLException {
        if (b() != d.Get) {
            return new URL(str);
        }
        if (this.g) {
            return bq.f(this.e);
        }
        return new URL(String.format("%s/%s", new Object[]{str, "get"}));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public LoadingError b(URLConnection uRLConnection, @Nullable OutputStream outputStream) {
        return LoadingError.InternalError;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public LoadingError b(URLConnection uRLConnection, @Nullable Exception exc) {
        return LoadingError.InternalError;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public LoadingError b(URLConnection uRLConnection, @Nullable Object obj) {
        return LoadingError.InternalError;
    }

    /* access modifiers changed from: protected */
    public String a() throws Exception {
        return b(z.a == null ? ag.e() : z.a).toString();
    }

    @Deprecated
    public void a(double d2) {
        this.f = d2;
    }

    public void a(i iVar) {
        this.d = iVar;
    }

    public void a(m mVar) {
        this.c = mVar;
    }

    @Deprecated
    public void a(String str) {
        this.e = str;
    }

    /* access modifiers changed from: protected */
    public void a(URLConnection uRLConnection) {
        super.a(uRLConnection);
        uRLConnection.setConnectTimeout(20000);
        uRLConnection.setReadTimeout(20000);
    }

    @Deprecated
    public void a(boolean z) {
        this.g = z;
    }
}
