package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class ab extends c<ad, ac, Object> {
    private BannerCallbacks a;

    ab() {
    }

    /* access modifiers changed from: 0000 */
    public void a(BannerCallbacks bannerCallbacks) {
        this.a = bannerCallbacks;
    }

    public void a(@NonNull ad adVar, @NonNull ac acVar) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_LOADED, String.format("height: %sdp, isPrecache: %s", new Object[]{Integer.valueOf(acVar.u()), Boolean.valueOf(acVar.isPrecache())}), LogLevel.verbose);
        Appodeal.b();
        if (this.a != null) {
            this.a.onBannerLoaded(acVar.u(), acVar.isPrecache());
        }
    }

    public void a(@Nullable ad adVar, @Nullable ac acVar, @Nullable LoadingError loadingError) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onBannerFailedToLoad();
        }
    }

    public void a(@Nullable ad adVar, @Nullable ac acVar, @Nullable Object obj) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (this.a != null) {
            this.a.onBannerShown();
        }
    }

    public void a(@Nullable ad adVar, @Nullable ac acVar, @Nullable Object obj, @Nullable LoadingError loadingError) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onBannerShowFailed();
        }
    }

    public void b(@Nullable ad adVar, @Nullable ac acVar) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onBannerExpired();
        }
    }

    public void b(@Nullable ad adVar, @Nullable ac acVar, @Nullable Object obj) {
        Log.log("Banner", LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onBannerClicked();
        }
    }
}
