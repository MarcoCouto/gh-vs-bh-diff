package com.integralads.avid.library.inmobi;

import android.os.AsyncTask;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.integralads.avid.library.inmobi.utils.AvidLogs;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class DownloadAvidTask extends AsyncTask<String, Void, String> {
    private static final int BSIZE = 1024;
    private DownloadAvidTaskListener listener;

    public interface DownloadAvidTaskListener {
        void failedToLoadAvid();

        void onLoadAvid(String str);
    }

    public DownloadAvidTaskListener getListener() {
        return this.listener;
    }

    public void setListener(DownloadAvidTaskListener downloadAvidTaskListener) {
        this.listener = downloadAvidTaskListener;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0070 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006b A[SYNTHETIC, Splitter:B:26:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008f A[Catch:{ IOException -> 0x0046 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0070=Splitter:B:31:0x0070, B:23:0x0054=Splitter:B:23:0x0054} */
    public String doInBackground(String... strArr) {
        String str;
        InputStream inputStream;
        str = strArr[0];
        if (TextUtils.isEmpty(str)) {
            AvidLogs.e("AvidLoader: URL is empty");
            return null;
        }
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.connect();
            inputStream = new BufferedInputStream(openConnection.getInputStream());
            try {
                StringWriter stringWriter = new StringWriter();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read != -1) {
                        stringWriter.write(new String(bArr, 0, read));
                    } else {
                        String obj = stringWriter.toString();
                        try {
                            inputStream.close();
                            return obj;
                        } catch (IOException e) {
                            AvidLogs.e("AvidLoader: can not close Stream", e);
                            return null;
                        }
                    }
                }
            } catch (MalformedURLException ) {
                StringBuilder sb = new StringBuilder("AvidLoader: something is wrong with the URL '");
                sb.append(str);
                sb.append("'");
                AvidLogs.e(sb.toString());
                if (inputStream != null) {
                    inputStream.close();
                }
                return null;
            } catch (IOException e2) {
                e = e2;
                try {
                    StringBuilder sb2 = new StringBuilder("AvidLoader: IO error ");
                    sb2.append(e.getLocalizedMessage());
                    AvidLogs.e(sb2.toString());
                    if (inputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (inputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (MalformedURLException unused) {
            inputStream = null;
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            StringBuilder sb22 = new StringBuilder("AvidLoader: IO error ");
            sb22.append(e.getLocalizedMessage());
            AvidLogs.e(sb22.toString());
            if (inputStream != null) {
                inputStream.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            if (inputStream != null) {
                inputStream.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        if (this.listener != null) {
            if (!TextUtils.isEmpty(str)) {
                this.listener.onLoadAvid(str);
                return;
            }
            this.listener.failedToLoadAvid();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.listener != null) {
            this.listener.failedToLoadAvid();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void invokeOnPostExecute(String str) {
        onPostExecute(str);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void invokeOnCancelled() {
        onCancelled();
    }
}
