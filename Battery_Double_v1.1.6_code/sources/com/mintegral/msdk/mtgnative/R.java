package com.mintegral.msdk.mtgnative;

public final class R {

    public static final class drawable {
        public static final int mintegral_native_bg_loading_camera = 2131231080;

        private drawable() {
        }
    }

    private R() {
    }
}
