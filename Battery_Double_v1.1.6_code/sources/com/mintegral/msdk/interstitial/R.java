package com.mintegral.msdk.interstitial;

public final class R {

    public static final class color {
        public static final int mintegral_interstitial_black = 2131099782;
        public static final int mintegral_interstitial_white = 2131099783;

        private color() {
        }
    }

    public static final class drawable {
        public static final int mintegral_interstitial_close = 2131231078;
        public static final int mintegral_interstitial_over = 2131231079;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_interstitial_iv_close = 2131296486;
        public static final int mintegral_interstitial_pb = 2131296487;
        public static final int mintegral_interstitial_wv = 2131296488;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_interstitial_activity = 2131427410;

        private layout() {
        }
    }

    private R() {
    }
}
