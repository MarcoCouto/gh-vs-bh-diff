package com.mintegral.msdk.playercommon;

public final class R {

    public static final class id {
        public static final int mintegral_playercommon_ll_loading = 2131296513;
        public static final int mintegral_playercommon_ll_sur_container = 2131296514;
        public static final int mintegral_playercommon_rl_root = 2131296515;
        public static final int progressBar = 2131296587;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_playercommon_player_view = 2131427416;

        private layout() {
        }
    }

    private R() {
    }
}
