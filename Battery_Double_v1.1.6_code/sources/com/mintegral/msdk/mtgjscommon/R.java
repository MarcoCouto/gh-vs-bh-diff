package com.mintegral.msdk.mtgjscommon;

public final class R {

    public static final class id {
        public static final int linescroll = 2131296447;
        public static final int mintegral_jscommon_checkBox = 2131296502;
        public static final int mintegral_jscommon_okbutton = 2131296503;
        public static final int mintegral_jscommon_webcontent = 2131296504;
        public static final int progressBar1 = 2131296588;
        public static final int textView = 2131296678;

        private id() {
        }
    }

    public static final class layout {
        public static final int loading_alert = 2131427399;
        public static final int mintegral_jscommon_authoritylayout = 2131427411;

        private layout() {
        }
    }

    private R() {
    }
}
