package com.mintegral.msdk.reward;

public final class R {

    public static final class anim {
        public static final int mintegral_reward_activity_open = 2130771984;
        public static final int mintegral_reward_activity_stay = 2130771985;

        private anim() {
        }
    }

    public static final class color {
        public static final int mintegral_reward_kiloo_background = 2131099797;

        private color() {
        }
    }

    public static final class id {
        public static final int mintegral_video_templete_container = 2131296540;
        public static final int mintegral_video_templete_progressbar = 2131296541;
        public static final int mintegral_video_templete_videoview = 2131296542;
        public static final int mintegral_video_templete_webview_parent = 2131296543;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_reward_activity_video_templete = 2131427417;
        public static final int mintegral_reward_activity_video_templete_transparent = 2131427418;

        private layout() {
        }
    }

    public static final class style {
        public static final int mintegral_reward_theme = 2131689896;
        public static final int mintegral_transparent_theme = 2131689897;

        private style() {
        }
    }

    private R() {
    }
}
