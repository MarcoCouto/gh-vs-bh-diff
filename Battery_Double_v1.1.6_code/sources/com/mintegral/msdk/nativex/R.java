package com.mintegral.msdk.nativex;

public final class R {

    public static final class color {
        public static final int mintegral_nativex_cta_txt_nor = 2131099784;
        public static final int mintegral_nativex_cta_txt_pre = 2131099785;
        public static final int mintegral_nativex_land_cta_bg_nor = 2131099786;
        public static final int mintegral_nativex_por_cta_bg_nor = 2131099787;
        public static final int mintegral_nativex_por_cta_bg_pre = 2131099788;
        public static final int mintegral_nativex_sound_bg = 2131099789;

        private color() {
        }
    }

    public static final class drawable {
        public static final int mintegral_demo_star_nor = 2131231076;
        public static final int mintegral_demo_star_sel = 2131231077;
        public static final int mintegral_nativex_close = 2131231081;
        public static final int mintegral_nativex_cta_land_nor = 2131231082;
        public static final int mintegral_nativex_cta_land_pre = 2131231083;
        public static final int mintegral_nativex_cta_por_nor = 2131231084;
        public static final int mintegral_nativex_cta_por_pre = 2131231085;
        public static final int mintegral_nativex_full_land_close = 2131231086;
        public static final int mintegral_nativex_full_protial_close = 2131231087;
        public static final int mintegral_nativex_fullview_background = 2131231088;
        public static final int mintegral_nativex_pause = 2131231089;
        public static final int mintegral_nativex_play = 2131231090;
        public static final int mintegral_nativex_play_bg = 2131231091;
        public static final int mintegral_nativex_play_progress = 2131231092;
        public static final int mintegral_nativex_sound1 = 2131231093;
        public static final int mintegral_nativex_sound2 = 2131231094;
        public static final int mintegral_nativex_sound3 = 2131231095;
        public static final int mintegral_nativex_sound4 = 2131231096;
        public static final int mintegral_nativex_sound5 = 2131231097;
        public static final int mintegral_nativex_sound6 = 2131231098;
        public static final int mintegral_nativex_sound7 = 2131231099;
        public static final int mintegral_nativex_sound8 = 2131231100;
        public static final int mintegral_nativex_sound_animation = 2131231101;
        public static final int mintegral_nativex_sound_bg = 2131231102;
        public static final int mintegral_nativex_sound_close = 2131231103;
        public static final int mintegral_nativex_sound_open = 2131231104;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_fb_mediaview_layout = 2131296472;
        public static final int mintegral_full_animation_content = 2131296473;
        public static final int mintegral_full_animation_player = 2131296474;
        public static final int mintegral_full_iv_close = 2131296475;
        public static final int mintegral_full_pb_loading = 2131296476;
        public static final int mintegral_full_player_parent = 2131296477;
        public static final int mintegral_full_rl_close = 2131296478;
        public static final int mintegral_full_rl_playcontainer = 2131296479;
        public static final int mintegral_full_tv_display_content = 2131296480;
        public static final int mintegral_full_tv_display_description = 2131296481;
        public static final int mintegral_full_tv_display_icon = 2131296482;
        public static final int mintegral_full_tv_display_title = 2131296483;
        public static final int mintegral_full_tv_feeds_star = 2131296484;
        public static final int mintegral_full_tv_install = 2131296485;
        public static final int mintegral_iv_pause = 2131296495;
        public static final int mintegral_iv_play = 2131296496;
        public static final int mintegral_iv_playend_pic = 2131296497;
        public static final int mintegral_iv_sound = 2131296498;
        public static final int mintegral_iv_sound_animation = 2131296499;
        public static final int mintegral_ll_loading = 2131296506;
        public static final int mintegral_ll_playerview_container = 2131296507;
        public static final int mintegral_my_big_img = 2131296508;
        public static final int mintegral_native_pb = 2131296509;
        public static final int mintegral_native_rl_root = 2131296510;
        public static final int mintegral_nativex_webview_layout = 2131296511;
        public static final int mintegral_nativex_webview_layout_webview = 2131296512;
        public static final int mintegral_progress = 2131296516;
        public static final int mintegral_rl_mediaview_root = 2131296520;
        public static final int mintegral_textureview = 2131296525;
        public static final int mobivsta_view_cover = 2131296552;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_nativex_fullbasescreen = 2131427412;
        public static final int mintegral_nativex_fullscreen_top = 2131427413;
        public static final int mintegral_nativex_mtgmediaview = 2131427414;
        public static final int mintegral_nativex_playerview = 2131427415;

        private layout() {
        }
    }

    private R() {
    }
}
