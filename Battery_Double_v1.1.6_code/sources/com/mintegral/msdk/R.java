package com.mintegral.msdk;

public final class R {

    public static final class drawable {
        public static final int mintegral_cm_backward = 2131231057;
        public static final int mintegral_cm_backward_disabled = 2131231058;
        public static final int mintegral_cm_backward_nor = 2131231059;
        public static final int mintegral_cm_backward_selected = 2131231060;
        public static final int mintegral_cm_end_animation = 2131231061;
        public static final int mintegral_cm_exits = 2131231062;
        public static final int mintegral_cm_exits_nor = 2131231063;
        public static final int mintegral_cm_exits_selected = 2131231064;
        public static final int mintegral_cm_forward = 2131231065;
        public static final int mintegral_cm_forward_disabled = 2131231066;
        public static final int mintegral_cm_forward_nor = 2131231067;
        public static final int mintegral_cm_forward_selected = 2131231068;
        public static final int mintegral_cm_head = 2131231069;
        public static final int mintegral_cm_highlight = 2131231070;
        public static final int mintegral_cm_progress = 2131231071;
        public static final int mintegral_cm_refresh = 2131231072;
        public static final int mintegral_cm_refresh_nor = 2131231073;
        public static final int mintegral_cm_refresh_selected = 2131231074;
        public static final int mintegral_cm_tail = 2131231075;

        private drawable() {
        }
    }

    private R() {
    }
}
