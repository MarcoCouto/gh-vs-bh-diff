package com.jaredrummler.android.processes.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.io.IOException;

public class AndroidProcess implements Parcelable {
    public static final Creator<AndroidProcess> CREATOR = new Creator<AndroidProcess>() {
        public AndroidProcess createFromParcel(Parcel parcel) {
            return new AndroidProcess(parcel);
        }

        public AndroidProcess[] newArray(int i) {
            return new AndroidProcess[i];
        }
    };
    public final String name;
    public final int pid;

    public int describeContents() {
        return 0;
    }

    static String getProcessName(int i) throws IOException {
        String str;
        try {
            str = ProcFile.readFile(String.format("/proc/%d/cmdline", new Object[]{Integer.valueOf(i)})).trim();
        } catch (IOException unused) {
            str = null;
        }
        return TextUtils.isEmpty(str) ? Stat.get(i).getComm() : str;
    }

    public AndroidProcess(int i) throws IOException {
        this.pid = i;
        this.name = getProcessName(i);
    }

    public String read(String str) throws IOException {
        return ProcFile.readFile(String.format("/proc/%d/%s", new Object[]{Integer.valueOf(this.pid), str}));
    }

    public String attr_current() throws IOException {
        return read("attr/current");
    }

    public String cmdline() throws IOException {
        return read("cmdline");
    }

    public Cgroup cgroup() throws IOException {
        return Cgroup.get(this.pid);
    }

    public int oom_score() throws IOException {
        return Integer.parseInt(read("oom_score"));
    }

    public int oom_adj() throws IOException {
        return Integer.parseInt(read("oom_adj"));
    }

    public int oom_score_adj() throws IOException {
        return Integer.parseInt(read("oom_score_adj"));
    }

    public Stat stat() throws IOException {
        return Stat.get(this.pid);
    }

    public Statm statm() throws IOException {
        return Statm.get(this.pid);
    }

    public Status status() throws IOException {
        return Status.get(this.pid);
    }

    public String wchan() throws IOException {
        return read("wchan");
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeInt(this.pid);
    }

    protected AndroidProcess(Parcel parcel) {
        this.name = parcel.readString();
        this.pid = parcel.readInt();
    }
}
