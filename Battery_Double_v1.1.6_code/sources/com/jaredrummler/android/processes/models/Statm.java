package com.jaredrummler.android.processes.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;

public final class Statm extends ProcFile {
    public static final Creator<Statm> CREATOR = new Creator<Statm>() {
        public Statm createFromParcel(Parcel parcel) {
            return new Statm(parcel);
        }

        public Statm[] newArray(int i) {
            return new Statm[i];
        }
    };
    public final String[] fields;

    public static Statm get(int i) throws IOException {
        return new Statm(String.format("/proc/%d/statm", new Object[]{Integer.valueOf(i)}));
    }

    private Statm(String str) throws IOException {
        super(str);
        this.fields = this.content.split("\\s+");
    }

    private Statm(Parcel parcel) {
        super(parcel);
        this.fields = parcel.createStringArray();
    }

    public long getSize() {
        return Long.parseLong(this.fields[0]) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
    }

    public long getResidentSetSize() {
        return Long.parseLong(this.fields[1]) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeStringArray(this.fields);
    }
}
