package com.tapjoy;

import java.util.List;
import java.util.Map;

public class TapjoyHttpURLResponse {
    public int contentLength;
    public long date;
    public long expires;
    public Map headerFields;
    public String redirectURL;
    public String response;
    public int statusCode;

    public String getHeaderFieldAsString(String str) {
        String str2 = "";
        if (this.headerFields == null) {
            return str2;
        }
        List list = (List) this.headerFields.get(str);
        return (list == null || list.get(0) == null) ? str2 : (String) list.get(0);
    }
}
