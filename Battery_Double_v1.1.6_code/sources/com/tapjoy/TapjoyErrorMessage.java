package com.tapjoy;

public class TapjoyErrorMessage {
    private ErrorType a;
    private String b;

    public enum ErrorType {
        INTERNAL_ERROR,
        SDK_ERROR,
        SERVER_ERROR,
        INTEGRATION_ERROR,
        NETWORK_ERROR
    }

    public TapjoyErrorMessage(ErrorType errorType, String str) {
        this.a = errorType;
        this.b = str;
    }

    public ErrorType getType() {
        return this.a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("Type=");
        sb2.append(this.a.toString());
        sb.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder(";Message=");
        sb3.append(this.b);
        sb.append(sb3.toString());
        return sb.toString();
    }
}
