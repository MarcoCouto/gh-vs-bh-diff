package com.tapjoy.android;

public final class R {

    public static final class attr {
        public static final int adSize = 2130968610;
        public static final int adSizes = 2130968611;
        public static final int adUnitId = 2130968612;
        public static final int buttonSize = 2130968651;
        public static final int circleCrop = 2130968665;
        public static final int colorScheme = 2130968682;
        public static final int imageAspectRatio = 2130968777;
        public static final int imageAspectRatioAdjust = 2130968778;
        public static final int scopeUris = 2130968852;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131099735;
        public static final int common_google_signin_btn_text_dark_default = 2131099736;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099737;
        public static final int common_google_signin_btn_text_dark_focused = 2131099738;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099739;
        public static final int common_google_signin_btn_text_light = 2131099740;
        public static final int common_google_signin_btn_text_light_default = 2131099741;
        public static final int common_google_signin_btn_text_light_disabled = 2131099742;
        public static final int common_google_signin_btn_text_light_focused = 2131099743;
        public static final int common_google_signin_btn_text_light_pressed = 2131099744;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131230872;
        public static final int common_google_signin_btn_icon_dark = 2131230873;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230874;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230875;
        public static final int common_google_signin_btn_icon_light = 2131230878;
        public static final int common_google_signin_btn_icon_light_focused = 2131230879;
        public static final int common_google_signin_btn_icon_light_normal = 2131230880;
        public static final int common_google_signin_btn_text_dark = 2131230882;
        public static final int common_google_signin_btn_text_dark_focused = 2131230883;
        public static final int common_google_signin_btn_text_dark_normal = 2131230884;
        public static final int common_google_signin_btn_text_light = 2131230887;
        public static final int common_google_signin_btn_text_light_focused = 2131230888;
        public static final int common_google_signin_btn_text_light_normal = 2131230889;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131296292;
        public static final int adjust_width = 2131296293;
        public static final int auto = 2131296312;
        public static final int dark = 2131296386;
        public static final int icon_only = 2131296421;
        public static final int light = 2131296444;
        public static final int none = 2131296493;
        public static final int normal = 2131296494;
        public static final int standard = 2131296569;
        public static final int wide = 2131296629;
        public static final int wrap_content = 2131296634;

        private id() {
        }
    }

    public static final class integer {
        public static final int google_play_services_version = 2131361799;

        private integer() {
        }
    }

    public static final class string {
        public static final int accept = 2131623967;
        public static final int cancel = 2131624000;
        public static final int common_google_play_services_enable_button = 2131624034;
        public static final int common_google_play_services_enable_text = 2131624035;
        public static final int common_google_play_services_enable_title = 2131624036;
        public static final int common_google_play_services_install_button = 2131624037;
        public static final int common_google_play_services_install_title = 2131624039;
        public static final int common_google_play_services_notification_ticker = 2131624041;
        public static final int common_google_play_services_unknown_issue = 2131624042;
        public static final int common_google_play_services_unsupported_text = 2131624043;
        public static final int common_google_play_services_update_button = 2131624044;
        public static final int common_google_play_services_update_text = 2131624045;
        public static final int common_google_play_services_update_title = 2131624046;
        public static final int common_google_play_services_updating_text = 2131624047;
        public static final int common_open_on_phone = 2131624049;
        public static final int common_signin_button_text = 2131624050;
        public static final int common_signin_button_text_long = 2131624051;
        public static final int date_format_month_day = 2131624052;
        public static final int date_format_year_month_day = 2131624053;
        public static final int decline = 2131624054;
        public static final int empty = 2131624070;
        public static final int failed_to_get_more = 2131624093;
        public static final int failed_to_load = 2131624094;
        public static final int failed_to_refresh = 2131624095;
        public static final int getting_more = 2131624101;
        public static final int just_before = 2131624108;
        public static final int loading = 2131624111;
        public static final int no = 2131624125;
        public static final int ok = 2131624139;
        public static final int please_wait = 2131624148;
        public static final int pull_down_to_load = 2131624195;
        public static final int pull_down_to_refresh = 2131624196;
        public static final int pull_up_to_get_more = 2131624197;
        public static final int refresh = 2131624198;
        public static final int release_to_get_more = 2131624199;
        public static final int release_to_load = 2131624200;
        public static final int release_to_refresh = 2131624201;
        public static final int search_hint = 2131624209;
        public static final int settings = 2131624211;
        public static final int sign_in = 2131624212;
        public static final int sign_out = 2131624213;
        public static final int sign_up = 2131624214;
        public static final int today = 2131624245;
        public static final int updating = 2131624246;
        public static final int yes = 2131624249;
        public static final int yesterday = 2131624250;

        private string() {
        }
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131689790;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.mansoon.BatteryDouble.R.attr.adSize, com.mansoon.BatteryDouble.R.attr.adSizes, com.mansoon.BatteryDouble.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.mansoon.BatteryDouble.R.attr.circleCrop, com.mansoon.BatteryDouble.R.attr.imageAspectRatio, com.mansoon.BatteryDouble.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.mansoon.BatteryDouble.R.attr.buttonSize, com.mansoon.BatteryDouble.R.attr.colorScheme, com.mansoon.BatteryDouble.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
