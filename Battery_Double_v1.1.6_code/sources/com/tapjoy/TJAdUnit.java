package com.tapjoy;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.VideoView;
import com.google.android.exoplayer2.util.MimeTypes;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitJSBridge.AdUnitAsyncTaskListner;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.internal.fm;
import com.tapjoy.internal.gf;
import com.tapjoy.internal.hn;
import com.tapjoy.internal.jq;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import org.json.JSONObject;

public class TJAdUnit implements OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener {
    public static TJVideoListener a;
    /* access modifiers changed from: private */
    public boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    private int C = -1;
    private int D;
    private int E;
    /* access modifiers changed from: private */
    public boolean F;
    /* access modifiers changed from: private */
    public boolean G;
    private fm H;
    private final Runnable I = new Runnable() {
        public final void run() {
            int streamVolume = TJAdUnit.this.s.getStreamVolume(3);
            if (TJAdUnit.this.t != streamVolume) {
                TJAdUnit.this.t = streamVolume;
                TJAdUnit.this.b.onVolumeChanged();
            }
        }
    };
    /* access modifiers changed from: private */
    public final Runnable J = new Runnable() {
        public final void run() {
            if (TJAdUnit.this.e.getCurrentPosition() != 0) {
                if (!TJAdUnit.this.p) {
                    TJAdUnit.this.p = true;
                }
                TJAdUnit.this.b.onVideoStarted(TJAdUnit.this.n);
                TJAdUnit.this.K.run();
            } else if (!TJAdUnit.this.F) {
                TJAdUnit.this.i.postDelayed(TJAdUnit.this.J, 200);
            } else {
                TJAdUnit.this.G = true;
            }
        }
    };
    /* access modifiers changed from: private */
    public final Runnable K = new Runnable() {
        public final void run() {
            TJAdUnit.this.b.onVideoProgress(TJAdUnit.this.e.getCurrentPosition());
            TJAdUnit.this.i.postDelayed(TJAdUnit.this.K, 500);
        }
    };
    /* access modifiers changed from: 0000 */
    public TJAdUnitJSBridge b;
    /* access modifiers changed from: 0000 */
    public TJWebView c;
    /* access modifiers changed from: 0000 */
    public TJWebView d;
    VideoView e;
    volatile boolean f;
    WebViewClient g = new WebViewClient() {
        public final void onLoadResource(WebView webView, String str) {
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return a(str);
        }

        @TargetApi(24)
        public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return a(webResourceRequest.getUrl().toString());
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            StringBuilder sb = new StringBuilder("onPageStarted: ");
            sb.append(str);
            TapjoyLog.d("TJAdUnit", sb.toString());
            if (TJAdUnit.this.b != null) {
                TJAdUnit.this.b.allowRedirect = true;
                TJAdUnit.this.b.customClose = false;
                TJAdUnit.this.b.closeRequested = false;
                TJAdUnit.this.a();
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            StringBuilder sb = new StringBuilder("onPageFinished: ");
            sb.append(str);
            TapjoyLog.d("TJAdUnit", sb.toString());
            if (TJAdUnit.this.l != null) {
                TJAdUnit.this.l.setProgressSpinnerVisibility(false);
            }
            TJAdUnit.this.B = true;
            if (TJAdUnit.this.y) {
                TJAdUnit.this.b.display();
            }
            TJAdUnit.this.b.flushMessageQueue();
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder("error:");
            sb.append(str);
            TapjoyLog.d("TJAdUnit", sb.toString());
            if (TJAdUnit.this.l != null) {
                TJAdUnit.this.l.showErrorDialog();
            }
            super.onReceivedError(webView, i, str, str2);
        }

        public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (TapjoyCache.getInstance() != null) {
                TapjoyCachedAssetData cachedDataForURL = TapjoyCache.getInstance().getCachedDataForURL(str);
                if (cachedDataForURL != null) {
                    WebResourceResponse a2 = a(cachedDataForURL);
                    if (a2 != null) {
                        StringBuilder sb = new StringBuilder("Reading request for ");
                        sb.append(str);
                        sb.append(" from cache -- localPath: ");
                        sb.append(cachedDataForURL.getLocalFilePath());
                        TapjoyLog.d("TJAdUnit", sb.toString());
                        return a2;
                    }
                }
            }
            return super.shouldInterceptRequest(webView, str);
        }

        private boolean a(String str) {
            if (!TJAdUnit.this.e() || !URLUtil.isValidUrl(str)) {
                if (TJAdUnit.this.l != null) {
                    TJAdUnit.this.l.showErrorDialog();
                }
                return true;
            } else if (TJAdUnit.b(str)) {
                return false;
            } else {
                if (TJAdUnit.this.b.allowRedirect) {
                    Uri parse = Uri.parse(str);
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(parse);
                    intent.addFlags(268435456);
                    if (TJAdUnit.this.d.getContext() != null) {
                        try {
                            TJAdUnit.this.d.getContext().startActivity(intent);
                            return true;
                        } catch (Exception e) {
                            StringBuilder sb = new StringBuilder("Exception in loading URL. ");
                            sb.append(e.getMessage());
                            TapjoyLog.e("TJAdUnit", sb.toString());
                        }
                    }
                } else if (str.startsWith("javascript:") && VERSION.SDK_INT >= 19) {
                    try {
                        TJAdUnit.this.d.evaluateJavascript(str.replaceFirst("javascript:", ""), null);
                        return true;
                    } catch (Exception e2) {
                        StringBuilder sb2 = new StringBuilder("Exception in evaluateJavascript. Device not supported. ");
                        sb2.append(e2.toString());
                        TapjoyLog.e("TJAdUnit", sb2.toString());
                    }
                }
                return false;
            }
        }

        private static WebResourceResponse a(TapjoyCachedAssetData tapjoyCachedAssetData) {
            if (tapjoyCachedAssetData == null) {
                return null;
            }
            try {
                return new WebResourceResponse(tapjoyCachedAssetData.getMimeType(), "UTF-8", new FileInputStream(tapjoyCachedAssetData.getLocalFilePath()));
            } catch (Exception unused) {
                return null;
            }
        }
    };
    WebChromeClient h = new WebChromeClient() {
        public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            TapjoyLog.d("TJAdUnit", str2);
            return false;
        }

        public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            if (TJAdUnit.this.b.closeRequested) {
                int i = 0;
                String[] strArr = {"Uncaught", "uncaught", "Error", "error", "not defined"};
                if (TJAdUnit.this.l != null) {
                    while (true) {
                        if (i >= 5) {
                            break;
                        }
                        if (consoleMessage.message().contains(strArr[i])) {
                            TJAdUnit.this.l.handleClose();
                            break;
                        }
                        i++;
                    }
                }
            }
            return true;
        }
    };
    /* access modifiers changed from: private */
    public final Handler i = new Handler(Looper.getMainLooper());
    private TJAdUnitWebViewListener j;
    private TJAdUnitVideoListener k;
    /* access modifiers changed from: private */
    public TJAdUnitActivity l;
    private MediaPlayer m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    private boolean q;
    @Nullable
    private ScheduledFuture r;
    /* access modifiers changed from: private */
    public AudioManager s;
    /* access modifiers changed from: private */
    public int t = 0;
    private int u;
    private boolean v;
    private boolean w;
    private boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    public interface TJAdUnitVideoListener {
        void onVideoCompleted();

        void onVideoError(String str);

        void onVideoStart();
    }

    public interface TJAdUnitWebViewListener {
        void onClick();

        void onClosed();

        void onContentReady();
    }

    private static boolean a(int i2) {
        return i2 == 0 || i2 == 8 || i2 == 6 || i2 == 11;
    }

    private static boolean b(int i2) {
        return i2 == 1 || i2 == 9 || i2 == 7 || i2 == 12;
    }

    public boolean preload(TJPlacementData tJPlacementData, Context context) {
        if (this.z || !tJPlacementData.isPrerenderingRequested() || !TJPlacementManager.canPreRenderPlacement() || TapjoyConnectCore.isViewOpen()) {
            fireContentReady();
            return false;
        }
        StringBuilder sb = new StringBuilder("Pre-rendering ad unit for placement: ");
        sb.append(tJPlacementData.getPlacementName());
        TapjoyLog.i("TJAdUnit", sb.toString());
        TJPlacementManager.incrementPlacementPreRenderCount();
        load(tJPlacementData, true, context);
        return true;
    }

    public void load(final TJPlacementData tJPlacementData, final boolean z2, final Context context) {
        this.z = false;
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                TJAdUnit tJAdUnit = TJAdUnit.this;
                Context context = context;
                if (Looper.myLooper() == Looper.getMainLooper() && !tJAdUnit.f && context != null) {
                    TapjoyLog.d("TJAdUnit", "Constructing ad unit");
                    tJAdUnit.f = true;
                    tJAdUnit.c = new TJWebView(context);
                    tJAdUnit.c.loadDataWithBaseURL(null, "<!DOCTYPE html><html><head><title>Tapjoy Background Webview</title></head></html>", WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
                    tJAdUnit.d = new TJWebView(context);
                    tJAdUnit.d.setWebViewClient(tJAdUnit.g);
                    tJAdUnit.d.setWebChromeClient(tJAdUnit.h);
                    tJAdUnit.e = new VideoView(context);
                    tJAdUnit.e.setOnCompletionListener(tJAdUnit);
                    tJAdUnit.e.setOnErrorListener(tJAdUnit);
                    tJAdUnit.e.setOnPreparedListener(tJAdUnit);
                    tJAdUnit.e.setVisibility(4);
                    tJAdUnit.b = new TJAdUnitJSBridge(context, tJAdUnit);
                    if (context instanceof TJAdUnitActivity) {
                        tJAdUnit.setAdUnitActivity((TJAdUnitActivity) context);
                    }
                }
                if (tJAdUnit.f) {
                    TapjoyLog.i("TJAdUnit", "Loading ad unit content");
                    TJAdUnit.this.z = true;
                    boolean z = false;
                    if (!jq.c(tJPlacementData.getRedirectURL())) {
                        if (tJPlacementData.isPreloadDisabled()) {
                            TJAdUnit.this.d.postUrl(tJPlacementData.getRedirectURL(), null);
                        } else {
                            TJAdUnit.this.d.loadUrl(tJPlacementData.getRedirectURL());
                        }
                    } else if (tJPlacementData.getBaseURL() == null || tJPlacementData.getHttpResponse() == null) {
                        TapjoyLog.e("TJAdUnit", new TapjoyErrorMessage(ErrorType.SDK_ERROR, "Error loading ad unit content"));
                        TJAdUnit.this.z = false;
                    } else {
                        TJAdUnit.this.d.loadDataWithBaseURL(tJPlacementData.getBaseURL(), tJPlacementData.getHttpResponse(), WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
                    }
                    TJAdUnit tJAdUnit2 = TJAdUnit.this;
                    if (TJAdUnit.this.z && z2) {
                        z = true;
                    }
                    tJAdUnit2.A = z;
                }
            }
        });
    }

    public void resume(TJAdUnitSaveStateData tJAdUnitSaveStateData) {
        if (this.b.didLaunchOtherActivity) {
            StringBuilder sb = new StringBuilder("onResume bridge.didLaunchOtherActivity callbackID: ");
            sb.append(this.b.otherActivityCallbackID);
            TapjoyLog.d("TJAdUnit", sb.toString());
            this.b.invokeJSCallback(this.b.otherActivityCallbackID, Boolean.TRUE);
            this.b.didLaunchOtherActivity = false;
        }
        this.F = false;
        this.b.setEnabled(true);
        if (tJAdUnitSaveStateData != null) {
            this.n = tJAdUnitSaveStateData.seekTime;
            this.e.seekTo(this.n);
            if (this.m != null) {
                this.v = tJAdUnitSaveStateData.isVideoMuted;
            }
        }
        if (this.G) {
            this.G = false;
            this.i.postDelayed(this.J, 200);
        }
    }

    public void pause() {
        this.F = true;
        this.b.setEnabled(false);
        pauseVideo();
    }

    public void invokeBridgeCallback(String str, Object... objArr) {
        if (this.b != null && str != null) {
            this.b.invokeJSCallback(str, objArr);
        }
    }

    public void destroy() {
        this.b.destroy();
        c();
        if (this.c != null) {
            this.c.removeAllViews();
            this.c = null;
        }
        if (this.d != null) {
            this.d.removeAllViews();
            this.d = null;
        }
        this.f = false;
        this.y = false;
        setAdUnitActivity(null);
        a();
        this.m = null;
        if (this.j != null) {
            this.j.onClosed();
        }
        resetContentLoadState();
    }

    public void resetContentLoadState() {
        this.z = false;
        this.B = false;
        this.A = false;
        this.C = -1;
        this.x = false;
        this.v = false;
    }

    public void setVisible(boolean z2) {
        this.b.notifyOrientationChanged(getScreenOrientationString(), this.D, this.E);
        this.y = z2;
        if (this.y && this.B) {
            this.b.display();
        }
    }

    public void fireContentReady() {
        if (this.j != null) {
            this.j.onContentReady();
        }
    }

    public void fireOnClick() {
        if (this.j != null) {
            this.j.onClick();
        }
    }

    public void closeRequested(boolean z2) {
        this.b.closeRequested(Boolean.valueOf(z2));
    }

    public void setOrientation(int i2) {
        TJAdUnitActivity tJAdUnitActivity = this.l;
        if (tJAdUnitActivity != null) {
            int b2 = b();
            if (this.C != -1) {
                b2 = this.C;
            }
            if ((a(b2) && a(i2)) || (b(b2) && b(i2))) {
                i2 = b2;
            }
            tJAdUnitActivity.setRequestedOrientation(i2);
            this.C = i2;
            this.x = true;
        }
    }

    public void unsetOrientation() {
        TJAdUnitActivity tJAdUnitActivity = this.l;
        if (tJAdUnitActivity != null) {
            tJAdUnitActivity.setRequestedOrientation(-1);
        }
        this.C = -1;
        this.x = false;
    }

    /* access modifiers changed from: private */
    public void a() {
        TapjoyLog.d("TJAdUnit", "detachVolumeListener");
        if (this.r != null) {
            this.r.cancel(false);
            this.r = null;
        }
        this.s = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return 0;
     */
    private int b() {
        TJAdUnitActivity tJAdUnitActivity = this.l;
        if (tJAdUnitActivity == null) {
            return -1;
        }
        int rotation = tJAdUnitActivity.getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        tJAdUnitActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.D = displayMetrics.widthPixels;
        this.E = displayMetrics.heightPixels;
        if (((rotation != 0 && rotation != 2) || this.E <= this.D) && ((rotation != 1 && rotation != 3) || this.D <= this.E)) {
            switch (rotation) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    return 8;
                case 3:
                    break;
                default:
                    TapjoyLog.w("TJAdUnit", "Unknown screen orientation. Defaulting to landscape.");
                    break;
            }
        } else {
            switch (rotation) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    return 8;
            }
        }
        return 1;
    }

    public void setAdUnitActivity(TJAdUnitActivity tJAdUnitActivity) {
        this.l = tJAdUnitActivity;
        if (this.b != null) {
            this.b.setAdUnitActivity(this.l);
        }
    }

    public void setAdContentTracker(fm fmVar) {
        this.H = fmVar;
    }

    public void setBackgroundColor(final String str, final AdUnitAsyncTaskListner adUnitAsyncTaskListner) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                String str = "TJAdUnit";
                try {
                    StringBuilder sb = new StringBuilder("setBackgroundColor: ");
                    sb.append(str);
                    TapjoyLog.d(str, sb.toString());
                    TJAdUnit.this.c.setBackgroundColor(Color.parseColor(str));
                    adUnitAsyncTaskListner.onComplete(true);
                } catch (Exception unused) {
                    StringBuilder sb2 = new StringBuilder("Error setting background color. backgroundWebView: ");
                    sb2.append(TJAdUnit.this.c);
                    sb2.append(", hexColor: ");
                    sb2.append(str);
                    TapjoyLog.d("TJAdUnit", sb2.toString());
                    adUnitAsyncTaskListner.onComplete(false);
                }
            }
        });
    }

    public void setBackgroundContent(final String str, final AdUnitAsyncTaskListner adUnitAsyncTaskListner) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                String str = "TJAdUnit";
                try {
                    StringBuilder sb = new StringBuilder("setBackgroundContent: ");
                    sb.append(str);
                    TapjoyLog.d(str, sb.toString());
                    TJAdUnit.this.c.loadDataWithBaseURL(null, str, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
                    adUnitAsyncTaskListner.onComplete(true);
                } catch (Exception unused) {
                    StringBuilder sb2 = new StringBuilder("Error setting background content. backgroundWebView: ");
                    sb2.append(TJAdUnit.this.c);
                    sb2.append(", content: ");
                    sb2.append(str);
                    TapjoyLog.d("TJAdUnit", sb2.toString());
                    adUnitAsyncTaskListner.onComplete(false);
                }
            }
        });
    }

    public void setWebViewListener(TJAdUnitWebViewListener tJAdUnitWebViewListener) {
        this.j = tJAdUnitWebViewListener;
    }

    public void setVideoListener(TJAdUnitVideoListener tJAdUnitVideoListener) {
        this.k = tJAdUnitVideoListener;
    }

    public int getLockedOrientation() {
        return this.C;
    }

    public int getScreenWidth() {
        return this.D;
    }

    public int getScreenHeight() {
        return this.E;
    }

    public String getScreenOrientationString() {
        return a(b()) ? "landscape" : "portrait";
    }

    public boolean hasCalledLoad() {
        return this.z;
    }

    public boolean isPrerendered() {
        return this.A;
    }

    public boolean isLockedOrientation() {
        return this.x;
    }

    public TJWebView getBackgroundWebView() {
        return this.c;
    }

    public TJWebView getWebView() {
        return this.d;
    }

    public boolean getCloseRequested() {
        return this.b.closeRequested;
    }

    public void loadVideoUrl(final String str, final AdUnitAsyncTaskListner adUnitAsyncTaskListner) {
        TapjoyUtil.runOnMainThread(new Runnable() {
            public final void run() {
                if (TJAdUnit.this.e != null) {
                    StringBuilder sb = new StringBuilder("loadVideoUrl: ");
                    sb.append(str);
                    TapjoyLog.i("TJAdUnit", sb.toString());
                    TJAdUnit.this.e.setVideoPath(str);
                    TJAdUnit.this.e.setVisibility(0);
                    TJAdUnit.this.e.seekTo(0);
                    adUnitAsyncTaskListner.onComplete(true);
                    return;
                }
                adUnitAsyncTaskListner.onComplete(false);
            }
        });
    }

    public boolean playVideo() {
        TapjoyLog.i("TJAdUnit", MraidJsMethods.PLAY_VIDEO);
        if (this.e == null) {
            return false;
        }
        this.e.start();
        this.q = false;
        this.i.postDelayed(this.J, 200);
        return true;
    }

    public boolean pauseVideo() {
        c();
        if (this.e == null || !this.e.isPlaying()) {
            return false;
        }
        this.e.pause();
        this.n = this.e.getCurrentPosition();
        StringBuilder sb = new StringBuilder("Video paused at: ");
        sb.append(this.n);
        TapjoyLog.i("TJAdUnit", sb.toString());
        this.b.onVideoPaused(this.n);
        return true;
    }

    public void clearVideo(final AdUnitAsyncTaskListner adUnitAsyncTaskListner, final boolean z2) {
        if (this.e != null) {
            c();
            TapjoyUtil.runOnMainThread(new Runnable() {
                public final void run() {
                    TJAdUnit.this.e.setVisibility(z2 ? 0 : 4);
                    TJAdUnit.this.e.stopPlayback();
                    TJAdUnit.this.p = false;
                    TJAdUnit.this.o = false;
                    TJAdUnit.this.n = 0;
                    adUnitAsyncTaskListner.onComplete(true);
                }
            });
            return;
        }
        adUnitAsyncTaskListner.onComplete(false);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z2) {
        if (this.m != null) {
            if (z2) {
                this.m.setVolume(0.0f, 0.0f);
            } else {
                this.m.setVolume(1.0f, 1.0f);
            }
            if (this.w != z2) {
                this.w = z2;
                this.b.onVolumeChanged();
            }
        } else {
            this.v = z2;
        }
    }

    public void attachVolumeListener(boolean z2, int i2) {
        StringBuilder sb = new StringBuilder("attachVolumeListener: isAttached=");
        sb.append(z2);
        sb.append("; interval=");
        sb.append(i2);
        TapjoyLog.d("TJAdUnit", sb.toString());
        a();
        if (z2) {
            TJAdUnitActivity tJAdUnitActivity = this.l;
            if (tJAdUnitActivity != null) {
                this.s = (AudioManager) tJAdUnitActivity.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
                this.t = this.s.getStreamVolume(3);
                this.u = this.s.getStreamMaxVolume(3);
                long j2 = (long) i2;
                this.r = hn.a.scheduleWithFixedDelay(this.I, j2, j2, TimeUnit.MILLISECONDS);
            }
        }
    }

    public VideoView getVideoView() {
        return this.e;
    }

    public int getVideoSeekTime() {
        return this.n;
    }

    public boolean isVideoComplete() {
        return this.q;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        TapjoyLog.i("TJAdUnit", "video -- onPrepared");
        final int duration = this.e.getDuration();
        final int measuredWidth = this.e.getMeasuredWidth();
        final int measuredHeight = this.e.getMeasuredHeight();
        this.m = mediaPlayer;
        if (this.v) {
            a(this.v);
        }
        if (this.n <= 0 || this.e.getCurrentPosition() == this.n) {
            this.b.onVideoReady(duration, measuredWidth, measuredHeight);
        } else {
            this.m.setOnSeekCompleteListener(new OnSeekCompleteListener() {
                public final void onSeekComplete(MediaPlayer mediaPlayer) {
                    TJAdUnit.this.b.onVideoReady(duration, measuredWidth, measuredHeight);
                }
            });
        }
        this.m.setOnInfoListener(this);
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        String str;
        ErrorType errorType = ErrorType.SDK_ERROR;
        StringBuilder sb = new StringBuilder("Error encountered when instantiating the VideoView: ");
        sb.append(i2);
        sb.append(" - ");
        sb.append(i3);
        TapjoyLog.e("TJAdUnit", new TapjoyErrorMessage(errorType, sb.toString()));
        this.o = true;
        c();
        String str2 = i2 != 100 ? "MEDIA_ERROR_UNKNOWN" : "MEDIA_ERROR_SERVER_DIED";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str2);
        sb2.append(" -- ");
        String sb3 = sb2.toString();
        if (i3 == -1010) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(sb3);
            sb4.append("MEDIA_ERROR_UNSUPPORTED");
            str = sb4.toString();
        } else if (i3 == -1007) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append(sb3);
            sb5.append("MEDIA_ERROR_MALFORMED");
            str = sb5.toString();
        } else if (i3 == -1004) {
            StringBuilder sb6 = new StringBuilder();
            sb6.append(sb3);
            sb6.append("MEDIA_ERROR_IO");
            str = sb6.toString();
        } else if (i3 != -110) {
            StringBuilder sb7 = new StringBuilder();
            sb7.append(sb3);
            sb7.append("MEDIA_ERROR_EXTRA_UNKNOWN");
            str = sb7.toString();
        } else {
            StringBuilder sb8 = new StringBuilder();
            sb8.append(sb3);
            sb8.append("MEDIA_ERROR_TIMED_OUT");
            str = sb8.toString();
        }
        this.b.onVideoError(str);
        if (i2 == 1 || i3 == -1004) {
            return true;
        }
        return false;
    }

    private void c() {
        this.i.removeCallbacks(this.J);
        this.i.removeCallbacks(this.K);
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        TapjoyLog.i("TJAdUnit", "video -- onCompletion");
        c();
        this.q = true;
        if (!this.o) {
            this.b.onVideoCompletion();
        }
        this.o = false;
    }

    public void fireOnVideoStart() {
        TapjoyLog.v("TJAdUnit", "Firing onVideoStart");
        if (getPublisherVideoListener() != null) {
            getPublisherVideoListener().onVideoStart();
        }
        if (this.k != null) {
            this.k.onVideoStart();
        }
    }

    public void fireOnVideoError(String str) {
        StringBuilder sb = new StringBuilder("Firing onVideoError with error: ");
        sb.append(str);
        TapjoyLog.e("TJAdUnit", sb.toString());
        if (getPublisherVideoListener() != null) {
            getPublisherVideoListener().onVideoError(3);
        }
        if (this.k != null) {
            this.k.onVideoError(str);
        }
    }

    public void fireOnVideoComplete() {
        TapjoyLog.v("TJAdUnit", "Firing onVideoComplete");
        if (getPublisherVideoListener() != null) {
            getPublisherVideoListener().onVideoComplete();
        }
        if (this.k != null) {
            this.k.onVideoCompleted();
        }
    }

    public float getVolume() {
        return ((float) this.t) / ((float) this.u);
    }

    public boolean isMuted() {
        return this.w;
    }

    public void startAdContentTracking(String str, JSONObject jSONObject) {
        if (this.H != null) {
            this.H.a(str, jSONObject);
        }
    }

    public void endAdContentTracking(String str, JSONObject jSONObject) {
        if (this.H != null) {
            d();
            this.H.b(str, jSONObject);
        }
    }

    public void sendAdContentTracking(String str, JSONObject jSONObject) {
        if (this.H != null) {
            d();
            fm fmVar = this.H;
            Map a2 = fm.a(jSONObject);
            gf.e(str).a(fmVar.a).a(a2).b(fm.b(jSONObject)).c();
        }
    }

    private void d() {
        if (this.H != null) {
            this.H.a(SettingsJsonConstants.ICON_PRERENDERED_KEY, Boolean.valueOf(this.A));
        }
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        String str = "";
        if (i2 == 3) {
            str = "MEDIA_INFO_VIDEO_RENDERING_START";
        } else if (i2 != 801) {
            switch (i2) {
                case 700:
                    str = "MEDIA_INFO_VIDEO_TRACK_LAGGING";
                    break;
                case 701:
                    str = "MEDIA_INFO_BUFFERING_START";
                    break;
                case 702:
                    str = "MEDIA_INFO_BUFFERING_END";
                    break;
            }
        } else {
            str = "MEDIA_INFO_NOT_SEEKABLE";
        }
        this.b.onVideoInfo(str);
        return false;
    }

    public TJVideoListener getPublisherVideoListener() {
        return a;
    }

    /* access modifiers changed from: private */
    public static boolean b(String str) {
        try {
            String host = new URL(TapjoyConfig.TJC_SERVICE_URL).getHost();
            if ((host == null || !str.contains(host)) && !str.contains(TapjoyConnectCore.getRedirectDomain()) && !str.contains(TapjoyUtil.getRedirectDomain(TapjoyConnectCore.getPlacementURL()))) {
                return false;
            }
            return true;
        } catch (MalformedURLException unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        boolean z2 = false;
        try {
            if (this.d.getContext() != null) {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.d.getContext().getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
                    z2 = true;
                }
                return z2;
            }
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder("Exception getting NetworkInfo: ");
            sb.append(e2.getLocalizedMessage());
            TapjoyLog.d("TJAdUnit", sb.toString());
        }
        return false;
    }

    public void notifyOrientationChanged() {
        this.b.notifyOrientationChanged(getScreenOrientationString(), this.D, this.E);
    }
}
