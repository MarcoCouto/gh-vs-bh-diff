package com.tapjoy.internal;

import android.os.SystemClock;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public final class gk {
    public long a;
    public long b;
    public long c;

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0100  */
    public final boolean a(String str, int i) {
        DatagramSocket datagramSocket;
        byte[] bArr;
        DatagramPacket datagramPacket;
        long currentTimeMillis;
        long elapsedRealtime;
        try {
            DatagramSocket datagramSocket2 = new DatagramSocket();
            try {
                datagramSocket2.setSoTimeout(i);
                bArr = new byte[48];
                datagramPacket = new DatagramPacket(bArr, 48, InetAddress.getByName(str), 123);
                bArr[0] = 27;
                currentTimeMillis = System.currentTimeMillis();
                elapsedRealtime = SystemClock.elapsedRealtime();
                long j = currentTimeMillis / 1000;
                long j2 = currentTimeMillis - (j * 1000);
                long j3 = j + 2208988800L;
                bArr[40] = (byte) ((int) (j3 >> 24));
                DatagramSocket datagramSocket3 = datagramSocket2;
                try {
                    bArr[41] = (byte) ((int) (j3 >> 16));
                    try {
                        bArr[42] = (byte) ((int) (j3 >> 8));
                        bArr[43] = (byte) ((int) (j3 >> 0));
                        long j4 = (j2 * 4294967296L) / 1000;
                        bArr[44] = (byte) ((int) (j4 >> 24));
                        bArr[45] = (byte) ((int) (j4 >> 16));
                        bArr[46] = (byte) ((int) (j4 >> 8));
                        bArr[47] = (byte) ((int) (Math.random() * 255.0d));
                        datagramSocket = datagramSocket3;
                    } catch (Exception unused) {
                        datagramSocket = datagramSocket3;
                        if (datagramSocket != null) {
                        }
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        datagramSocket = datagramSocket3;
                        if (datagramSocket != null) {
                        }
                        throw th;
                    }
                } catch (Exception unused2) {
                    datagramSocket = datagramSocket3;
                    if (datagramSocket != null) {
                    }
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    datagramSocket = datagramSocket3;
                    if (datagramSocket != null) {
                    }
                    throw th;
                }
            } catch (Exception unused3) {
                datagramSocket = datagramSocket2;
                if (datagramSocket != null) {
                }
                return false;
            } catch (Throwable th3) {
                th = th3;
                datagramSocket = datagramSocket2;
                if (datagramSocket != null) {
                }
                throw th;
            }
            try {
                datagramSocket.send(datagramPacket);
                datagramSocket.receive(new DatagramPacket(bArr, 48));
                long elapsedRealtime2 = SystemClock.elapsedRealtime();
                long j5 = elapsedRealtime2 - elapsedRealtime;
                long j6 = currentTimeMillis + j5;
                long b2 = b(bArr, 24);
                long b3 = b(bArr, 32);
                long b4 = b(bArr, 40);
                long j7 = j5 - (b4 - b3);
                long j8 = j6 + (((b3 - b2) + (b4 - j6)) / 2);
                try {
                    this.a = j8;
                    this.b = elapsedRealtime2;
                    this.c = j7;
                    datagramSocket.close();
                    return true;
                } catch (Exception unused4) {
                    if (datagramSocket != null) {
                    }
                    return false;
                } catch (Throwable th4) {
                    th = th4;
                    if (datagramSocket != null) {
                    }
                    throw th;
                }
            } catch (Exception unused5) {
                if (datagramSocket != null) {
                }
                return false;
            } catch (Throwable th5) {
                th = th5;
                if (datagramSocket != null) {
                }
                throw th;
            }
        } catch (Exception unused6) {
            datagramSocket = null;
            if (datagramSocket != null) {
                datagramSocket.close();
            }
            return false;
        } catch (Throwable th6) {
            th = th6;
            datagramSocket = null;
            if (datagramSocket != null) {
                datagramSocket.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r0v0, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r1v1, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r2v1, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r5v1, types: [byte] */
    private static long a(byte[] bArr, int i) {
        int i2 = bArr[i];
        int i3 = bArr[i + 1];
        int i4 = bArr[i + 2];
        int i5 = bArr[i + 3];
        if (i2 & true) {
            i2 = (i2 & 127) + 128;
        }
        if (i3 & true) {
            i3 = (i3 & 127) + 128;
        }
        if (i4 & true) {
            i4 = (i4 & 127) + 128;
        }
        if (i5 & true) {
            i5 = (i5 & 127) + 128;
        }
        return (((long) i2) << 24) + (((long) i3) << 16) + (((long) i4) << 8) + ((long) i5);
    }

    private static long b(byte[] bArr, int i) {
        return ((a(bArr, i) - 2208988800L) * 1000) + ((a(bArr, i + 4) * 1000) / 4294967296L);
    }
}
