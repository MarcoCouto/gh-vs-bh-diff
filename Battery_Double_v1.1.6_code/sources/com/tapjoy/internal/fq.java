package com.tapjoy.internal;

import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.gf.a;
import java.util.HashMap;
import java.util.Map;

public abstract class fq {
    private static final String c = "fq";
    public final Map a = new HashMap();
    public final Map b = new HashMap();

    protected fq(String str, String str2, String str3) {
        this.a.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, str);
        this.a.put("placement_type", str2);
        this.a.put(Param.CONTENT_TYPE, str3);
    }

    /* access modifiers changed from: protected */
    public final a a(String str, Map map, Map map2) {
        a b2 = gf.e(str).a().a(this.a).a(map).b(map2);
        this.b.put(str, b2);
        return b2;
    }

    public final void a(String str, Object obj) {
        this.a.put(str, obj);
    }

    /* access modifiers changed from: protected */
    public final a b(String str, Map map, Map map2) {
        a aVar = !al.a(str) ? (a) this.b.remove(str) : null;
        if (aVar == null) {
            String str2 = c;
            StringBuilder sb = new StringBuilder("Error when calling endTrackingEvent -- ");
            sb.append(str);
            sb.append(" tracking has not been started.");
            TapjoyLog.e(str2, sb.toString());
        } else {
            aVar.a(this.a).a(map).b(map2).b().c();
        }
        return aVar;
    }

    public final a a() {
        return a("Content.rendered", null, null);
    }

    public final a b() {
        return b("Content.rendered", null, null);
    }
}
