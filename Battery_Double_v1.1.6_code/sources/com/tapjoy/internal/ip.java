package com.tapjoy.internal;

import android.content.Context;
import android.view.ViewGroup.LayoutParams;

public final class ip extends ai {
    private final hu a;
    private final iq b;
    private aa c = null;

    public ip(Context context, hu huVar, iq iqVar) {
        super(context);
        this.a = huVar;
        this.b = iqVar;
        addView(iqVar, new LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        aa aaVar;
        aa aaVar2;
        aa a2 = aa.a(getContext());
        if (!this.a.a()) {
            aaVar = aa.LANDSCAPE;
            if (!a2.a()) {
                setRotationCount(0);
            } else if (a2.c() == 3) {
                setRotationCount(1);
            } else {
                setRotationCount(1);
            }
        } else if (this.a.b()) {
            if (a2.a()) {
                aaVar2 = aa.PORTRAIT;
            } else if (a2.b() || !aa.b(getContext()).a()) {
                aaVar2 = aa.LANDSCAPE;
            } else {
                aaVar2 = aa.PORTRAIT;
            }
            setRotationCount(0);
            aaVar = aaVar2;
        } else {
            aaVar = aa.PORTRAIT;
            if (!a2.b()) {
                setRotationCount(0);
            } else if (a2.c() == 3) {
                setRotationCount(1);
            } else {
                setRotationCount(3);
            }
        }
        if (this.c != aaVar) {
            this.c = aaVar;
            this.b.setLandscape(this.c.b());
        }
        super.onMeasure(i, i2);
    }
}
