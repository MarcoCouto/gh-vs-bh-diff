package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

public abstract class kc implements kf {
    private final ReentrantLock a = new ReentrantLock();
    private final a b = new a(this, 0);
    private final a c = new a(this, 0);
    private com.tapjoy.internal.kf.a d = com.tapjoy.internal.kf.a.NEW;
    private boolean e = false;

    class a extends kb {
        private a() {
        }

        /* synthetic */ a(kc kcVar, byte b) {
            this();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public com.tapjoy.internal.kf.a get(long j, TimeUnit timeUnit) {
            try {
                return (com.tapjoy.internal.kf.a) super.get(j, timeUnit);
            } catch (TimeoutException unused) {
                throw new TimeoutException(kc.this.toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public final ke e() {
        this.a.lock();
        try {
            if (this.d == com.tapjoy.internal.kf.a.NEW) {
                this.d = com.tapjoy.internal.kf.a.STARTING;
                a();
            }
        } catch (Throwable th) {
            this.a.unlock();
            throw th;
        }
        this.a.unlock();
        return this.b;
    }

    private ke g() {
        this.a.lock();
        try {
            if (this.d == com.tapjoy.internal.kf.a.NEW) {
                this.d = com.tapjoy.internal.kf.a.TERMINATED;
                this.b.a((Object) com.tapjoy.internal.kf.a.TERMINATED);
                this.c.a((Object) com.tapjoy.internal.kf.a.TERMINATED);
            } else if (this.d == com.tapjoy.internal.kf.a.STARTING) {
                this.e = true;
                this.b.a((Object) com.tapjoy.internal.kf.a.STOPPING);
            } else if (this.d == com.tapjoy.internal.kf.a.RUNNING) {
                this.d = com.tapjoy.internal.kf.a.STOPPING;
                b();
            }
        } catch (Throwable th) {
            this.a.unlock();
            throw th;
        }
        this.a.unlock();
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.a.lock();
        try {
            if (this.d == com.tapjoy.internal.kf.a.STARTING) {
                this.d = com.tapjoy.internal.kf.a.RUNNING;
                if (this.e) {
                    g();
                } else {
                    this.b.a((Object) com.tapjoy.internal.kf.a.RUNNING);
                }
                return;
            }
            StringBuilder sb = new StringBuilder("Cannot notifyStarted() when the service is ");
            sb.append(this.d);
            IllegalStateException illegalStateException = new IllegalStateException(sb.toString());
            a(illegalStateException);
            throw illegalStateException;
        } finally {
            this.a.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.a.lock();
        try {
            if (this.d != com.tapjoy.internal.kf.a.STOPPING) {
                if (this.d != com.tapjoy.internal.kf.a.RUNNING) {
                    StringBuilder sb = new StringBuilder("Cannot notifyStopped() when the service is ");
                    sb.append(this.d);
                    IllegalStateException illegalStateException = new IllegalStateException(sb.toString());
                    a(illegalStateException);
                    throw illegalStateException;
                }
            }
            this.d = com.tapjoy.internal.kf.a.TERMINATED;
            this.c.a((Object) com.tapjoy.internal.kf.a.TERMINATED);
        } finally {
            this.a.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Throwable th) {
        jp.a(th);
        this.a.lock();
        try {
            if (this.d == com.tapjoy.internal.kf.a.STARTING) {
                this.b.a(th);
                this.c.a((Throwable) new Exception("Service failed to start.", th));
            } else if (this.d == com.tapjoy.internal.kf.a.STOPPING) {
                this.c.a(th);
            } else if (this.d == com.tapjoy.internal.kf.a.RUNNING) {
                this.c.a((Throwable) new Exception("Service failed while running", th));
            } else if (this.d == com.tapjoy.internal.kf.a.NEW || this.d == com.tapjoy.internal.kf.a.TERMINATED) {
                StringBuilder sb = new StringBuilder("Failed while in state:");
                sb.append(this.d);
                throw new IllegalStateException(sb.toString(), th);
            }
            this.d = com.tapjoy.internal.kf.a.FAILED;
        } finally {
            this.a.unlock();
        }
    }

    public final com.tapjoy.internal.kf.a f() {
        com.tapjoy.internal.kf.a aVar;
        this.a.lock();
        try {
            if (!this.e || this.d != com.tapjoy.internal.kf.a.STARTING) {
                aVar = this.d;
            } else {
                aVar = com.tapjoy.internal.kf.a.STOPPING;
            }
            return aVar;
        } finally {
            this.a.unlock();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(f());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
