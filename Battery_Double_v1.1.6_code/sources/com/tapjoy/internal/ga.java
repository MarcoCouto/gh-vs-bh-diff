package com.tapjoy.internal;

import android.content.Context;
import android.content.SharedPreferences;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.internal.gj.a;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public final class ga {
    private static final ga b;
    private static ga c;
    public final fy a = new fy();
    private Context d;

    static {
        ga gaVar = new ga();
        b = gaVar;
        c = gaVar;
    }

    public static ga a() {
        return c;
    }

    public static fy b() {
        return c.a;
    }

    ga() {
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0030 */
    public final synchronized void a(Context context) {
        SharedPreferences c2;
        if (context != null) {
            if (this.d == null) {
                this.d = context;
                c2 = c();
                String string = c().getString(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS, null);
                if (string != null) {
                    bn b2 = bn.b(string);
                    try {
                        Map d2 = b2.d();
                        b2.close();
                        this.a.a(d2);
                    } catch (Exception ) {
                        c2.edit().remove(TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS).apply();
                    } catch (Throwable th) {
                        b2.close();
                        throw th;
                    }
                }
                AnonymousClass1 r4 = new Observer() {
                    public final void update(Observable observable, Object obj) {
                        Object obj2;
                        gf.a(ga.this.a.a("usage_tracking_enabled", false));
                        String str = "usage_tracking_exclude";
                        Class<List> cls = List.class;
                        Iterator it = ga.this.a.b.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj2 = null;
                                break;
                            }
                            Object a2 = ((a) it.next()).a(str);
                            if (a2 != null && cls.isInstance(a2)) {
                                obj2 = cls.cast(a2);
                                break;
                            }
                        }
                        gf.a((Collection) obj2);
                    }
                };
                this.a.addObserver(r4);
                r4.update(this.a, null);
            }
        }
    }

    public final SharedPreferences c() {
        return this.d.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0);
    }
}
