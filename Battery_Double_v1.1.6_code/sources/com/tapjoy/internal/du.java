package com.tapjoy.internal;

import android.os.Handler;
import android.webkit.WebView;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import java.util.List;

public final class du extends ds {
    WebView f;
    private List g;
    private final String h;

    public du(List list, String str) {
        this.g = list;
        this.h = str;
    }

    public final void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {
            private WebView b = du.this.f;

            public final void run() {
                this.b.destroy();
            }
        }, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
        this.f = null;
    }

    public final void a() {
        super.a();
        this.f = new WebView(df.a().a);
        this.f.getSettings().setJavaScriptEnabled(true);
        a(this.f);
        dg.a();
        dg.a(this.f, this.h);
        for (cy cyVar : this.g) {
            String externalForm = cyVar.b.toExternalForm();
            dg.a();
            WebView webView = this.f;
            if (externalForm != null) {
                dg.a(webView, "var script=document.createElement('script');script.setAttribute(\"type\",\"text/javascript\");script.setAttribute(\"src\",\"%SCRIPT_SRC%\");document.body.appendChild(script);".replace("%SCRIPT_SRC%", externalForm));
            }
        }
    }
}
