package com.tapjoy.internal;

import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Map;
import javax.annotation.Nullable;

public abstract class ii extends bz {
    public final String b() {
        return HttpRequest.METHOD_POST;
    }

    public final String d() {
        return "application/json";
    }

    public Map e() {
        Map e = super.e();
        gz a = gz.a();
        StringBuilder sb = new StringBuilder();
        sb.append(a.m);
        sb.append("/Android");
        e.put("sdk_ver", sb.toString());
        e.put(TapjoyConstants.TJC_API_KEY, a.l);
        if (gw.a) {
            e.put("debug", Boolean.valueOf(true));
        }
        return e;
    }

    /* access modifiers changed from: protected */
    public Object f() {
        try {
            return super.f();
        } catch (Exception e) {
            new Object[1][0] = this;
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    @Nullable
    public Object a(bn bnVar) {
        bnVar.s();
        return null;
    }
}
