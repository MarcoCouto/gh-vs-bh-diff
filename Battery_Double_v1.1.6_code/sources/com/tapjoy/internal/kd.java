package com.tapjoy.internal;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public final class kd {
    private static final Logger a = Logger.getLogger(kd.class.getName());
    private final Queue b = new LinkedList();
    private boolean c = false;

    static class a {
        final Runnable a;
        final Executor b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        if (r6.b.isEmpty() != false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        r0 = (com.tapjoy.internal.kd.a) r6.b.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0.b.execute(r0.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        r2 = a;
        r3 = java.util.logging.Level.SEVERE;
        r4 = new java.lang.StringBuilder("RuntimeException while executing runnable ");
        r4.append(r0.a);
        r4.append(" with executor ");
        r4.append(r0.b);
        r2.log(r3, r4.toString(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        return;
     */
    public final void a() {
        synchronized (this.b) {
            if (!this.c) {
                this.c = true;
            }
        }
    }
}
