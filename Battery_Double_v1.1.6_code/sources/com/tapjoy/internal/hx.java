package com.tapjoy.internal;

import android.graphics.Bitmap;
import com.tapjoy.internal.ap.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutorService;

public final class hx {
    public static final bi e = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            return new hx(bnVar);
        }
    };
    private static final an f;
    public URL a;
    public Bitmap b;
    public byte[] c;
    public ie d;

    static {
        aq arVar = new ar();
        if (!(arVar instanceof as)) {
            arVar = new a(arVar);
        }
        f = arVar;
    }

    public hx(URL url) {
        this.a = url;
    }

    public final boolean a() {
        return (this.b == null && this.c == null) ? false : true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0099, code lost:
        r12 = java.lang.Long.parseLong(r8.substring(8));
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004a  */
    public final void b() {
        boolean a2 = ga.b().a("mm_external_cache_enabled", true);
        boolean z = !a2;
        if (z) {
            this.b = (Bitmap) f.a(this.a);
            if (this.b != null) {
                return;
            }
        }
        if (a2) {
            File a3 = ht.a.a(this.a);
            if (a3 != null) {
                FileInputStream fileInputStream = null;
                try {
                    FileInputStream fileInputStream2 = new FileInputStream(a3);
                    try {
                        a(fileInputStream2);
                        jz.a(fileInputStream2);
                    } catch (IOException unused) {
                        fileInputStream = fileInputStream2;
                        jz.a(fileInputStream);
                        if (this.b == null) {
                        }
                        f.a(this.a, this.b);
                        return;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream = fileInputStream2;
                        jz.a(fileInputStream);
                        throw th;
                    }
                } catch (IOException unused2) {
                    jz.a(fileInputStream);
                    if (this.b == null) {
                    }
                    f.a(this.a, this.b);
                    return;
                } catch (Throwable th2) {
                    th = th2;
                    jz.a(fileInputStream);
                    throw th;
                }
                if (this.b == null || this.c != null) {
                    if (z && this.b != null) {
                        f.a(this.a, this.b);
                    }
                    return;
                }
                a3.delete();
            }
        }
        URLConnection a4 = fj.a(this.a);
        String headerField = a4.getHeaderField(HttpRequest.HEADER_CACHE_CONTROL);
        if (!jq.c(headerField)) {
            String[] split = headerField.split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                String trim = split[i].trim();
                if (trim.startsWith("max-age=")) {
                    break;
                }
                i++;
            }
        }
        long j = 0;
        InputStream inputStream = a4.getInputStream();
        ByteArrayInputStream a5 = a(inputStream);
        jz.a(inputStream);
        ht htVar = ht.a;
        if (ht.a(j) && a2 && !(this.b == null && this.c == null)) {
            ht htVar2 = ht.a;
            URL url = this.a;
            if (htVar2.b != null) {
                ExecutorService executorService = htVar2.e;
                AnonymousClass2 r8 = new Runnable(url, a5, j) {
                    final /* synthetic */ URL a;
                    final /* synthetic */ InputStream b;
                    final /* synthetic */ long c;

                    {
                        this.a = r2;
                        this.b = r3;
                        this.c = r4;
                    }

                    public final void run() {
                        try {
                            File createTempFile = File.createTempFile("tj_", null, ht.this.b());
                            if (createTempFile == null) {
                                new Object[1][0] = this.a;
                                return;
                            }
                            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
                            try {
                                jx.a(this.b, fileOutputStream);
                                fileOutputStream.close();
                                long j = this.c;
                                if (j > 604800) {
                                    j = 604800;
                                }
                                long b2 = v.b() + (j * 1000);
                                synchronized (ht.this) {
                                    String b3 = ht.this.b(this.a);
                                    if (createTempFile.renameTo(ht.this.a(b3))) {
                                        ht.this.c.edit().putLong(b3, b2).commit();
                                        Object[] objArr = {createTempFile, b3, this.a};
                                    }
                                }
                            } catch (IOException unused) {
                                new Object[1][0] = this.a;
                            }
                        } catch (FileNotFoundException unused2) {
                            new Object[1][0] = this.a;
                        } catch (IOException unused3) {
                            new Object[1][0] = this.a;
                        }
                    }
                };
                executorService.submit(r8);
            }
        }
        if (z && this.b != null) {
            f.a(this.a, this.b);
        }
    }

    private ByteArrayInputStream a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        jx.a(inputStream, byteArrayOutputStream);
        byteArrayOutputStream.close();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        Cif ifVar = new Cif();
        ifVar.a(byteArray);
        ie a2 = ifVar.a();
        if (a2.b == 0) {
            this.c = byteArray;
            this.d = a2;
        } else {
            s sVar = s.a;
            this.b = s.a((InputStream) byteArrayInputStream);
            byteArrayInputStream.reset();
        }
        return byteArrayInputStream;
    }

    hx(bn bnVar) {
        if (bnVar.k() == bs.STRING) {
            this.a = bnVar.e();
            return;
        }
        bnVar.h();
        String l = bnVar.l();
        while (bnVar.j()) {
            if ("url".equals(l)) {
                this.a = bnVar.e();
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
    }
}
