package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public final class io extends RelativeLayout {
    private hr a;
    /* access modifiers changed from: private */
    public a b;
    private aa c = aa.UNSPECIFIED;
    private int d = 0;
    private int e = 0;
    private ia f = null;
    private ArrayList g = null;
    private ArrayList h = null;

    public interface a {
        void a();

        void a(hz hzVar);
    }

    public io(Context context, hr hrVar, a aVar) {
        super(context);
        this.a = hrVar;
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.b.a();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0091  */
    public final void onMeasure(int i, int i2) {
        float f2;
        float f3;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        if (size >= size2) {
            if (this.c != aa.LANDSCAPE) {
                this.c = aa.LANDSCAPE;
                a();
            }
        } else if (this.c != aa.PORTRAIT) {
            this.c = aa.PORTRAIT;
            a();
        }
        if (!(this.d == size && this.e == size2)) {
            this.d = size;
            this.e = size2;
            float f4 = (float) size;
            float f5 = (float) size2;
            float f6 = 0.0f;
            if (!(this.f == null || this.f.b == null)) {
                float f7 = ((this.f.b.y * f4) / this.f.b.x) / f5;
                if (f7 < 1.0f) {
                    f2 = (this.f.b.y * f4) / this.f.b.x;
                    f3 = (f5 - f2) / 2.0f;
                    for (View view : ac.a(this)) {
                        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                        hz hzVar = (hz) view.getTag();
                        float a2 = hzVar.a.a(f4, f2);
                        float a3 = hzVar.b.a(f4, f2);
                        float a4 = hzVar.c.a(f4, f2);
                        float a5 = hzVar.d.a(f4, f2);
                        int i3 = hzVar.e;
                        int i4 = hzVar.f;
                        if (i3 == 14) {
                            a2 += (f4 - a4) / 2.0f;
                            i3 = 9;
                        }
                        if (i4 == 15) {
                            a3 += (f2 - a5) / 2.0f;
                            i4 = 10;
                        }
                        layoutParams.addRule(i3, -1);
                        layoutParams.addRule(i4, -1);
                        layoutParams.width = Math.round(a4);
                        layoutParams.height = Math.round(a5);
                        if (i3 == 9) {
                            layoutParams.leftMargin = Math.round(a2 + f6);
                        } else if (i3 == 11) {
                            layoutParams.rightMargin = Math.round(a2 + f6);
                        }
                        if (i4 == 10) {
                            layoutParams.topMargin = Math.round(a3 + f3);
                        } else if (i4 == 12) {
                            layoutParams.bottomMargin = Math.round(a3 + f3);
                        }
                    }
                } else if (f7 > 1.0f) {
                    float f8 = (this.f.b.x * f5) / this.f.b.y;
                    f6 = (f4 - f8) / 2.0f;
                    f4 = f8;
                }
            }
            f2 = f5;
            f3 = 0.0f;
            for (View view2 : ac.a(this)) {
            }
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public final void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i == 0) {
            if (this.h != null) {
                Iterator it = this.h.iterator();
                while (it.hasNext()) {
                    ig igVar = (ig) ((WeakReference) it.next()).get();
                    if (igVar != null) {
                        igVar.setVisibility(4);
                        igVar.b();
                    }
                }
            }
            if (this.g != null) {
                Iterator it2 = this.g.iterator();
                while (it2.hasNext()) {
                    ig igVar2 = (ig) ((WeakReference) it2.next()).get();
                    if (igVar2 != null) {
                        igVar2.setVisibility(0);
                        igVar2.a();
                    }
                }
            }
        } else {
            if (this.g != null) {
                Iterator it3 = this.g.iterator();
                while (it3.hasNext()) {
                    ig igVar3 = (ig) ((WeakReference) it3.next()).get();
                    if (igVar3 != null) {
                        igVar3.b();
                    }
                }
            }
            if (this.h != null) {
                Iterator it4 = this.h.iterator();
                while (it4.hasNext()) {
                    ig igVar4 = (ig) ((WeakReference) it4.next()).get();
                    if (igVar4 != null) {
                        igVar4.b();
                    }
                }
            }
        }
    }

    private void a() {
        ig igVar;
        ig igVar2;
        Bitmap bitmap;
        Iterator it = this.a.a.iterator();
        ia iaVar = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ia iaVar2 = (ia) it.next();
            if (iaVar2.a == this.c) {
                iaVar = iaVar2;
                break;
            } else if (iaVar2.a == aa.UNSPECIFIED) {
                iaVar = iaVar2;
            }
        }
        removeAllViews();
        if (this.g != null) {
            Iterator it2 = this.g.iterator();
            while (it2.hasNext()) {
                ig igVar3 = (ig) ((WeakReference) it2.next()).get();
                if (igVar3 != null) {
                    igVar3.c();
                }
            }
            this.g.clear();
        }
        if (this.h != null) {
            Iterator it3 = this.h.iterator();
            while (it3.hasNext()) {
                ig igVar4 = (ig) ((WeakReference) it3.next()).get();
                if (igVar4 != null) {
                    igVar4.c();
                }
            }
            this.h.clear();
        }
        if (iaVar != null) {
            this.f = iaVar;
            Context context = getContext();
            Iterator it4 = iaVar.c.iterator();
            while (it4.hasNext()) {
                hz hzVar = (hz) it4.next();
                RelativeLayout relativeLayout = new RelativeLayout(context);
                if (hzVar.l.c != null) {
                    ig igVar5 = new ig(context);
                    igVar5.setScaleType(ScaleType.FIT_XY);
                    igVar5.a(hzVar.l.d, hzVar.l.c);
                    if (this.g == null) {
                        this.g = new ArrayList();
                    }
                    this.g.add(new WeakReference(igVar5));
                    igVar = igVar5;
                } else {
                    igVar = null;
                }
                if (hzVar.m == null || hzVar.m.c == null) {
                    igVar2 = null;
                } else {
                    ig igVar6 = new ig(context);
                    igVar6.setScaleType(ScaleType.FIT_XY);
                    igVar6.a(hzVar.m.d, hzVar.m.c);
                    if (this.h == null) {
                        this.h = new ArrayList();
                    }
                    this.h.add(new WeakReference(igVar6));
                    igVar2 = igVar6;
                }
                LayoutParams layoutParams = new LayoutParams(0, 0);
                LayoutParams layoutParams2 = new LayoutParams(-1, -1);
                Bitmap bitmap2 = hzVar.l.b;
                if (hzVar.m != null) {
                    bitmap = hzVar.m.b;
                } else {
                    bitmap = null;
                }
                final BitmapDrawable bitmapDrawable = bitmap2 != null ? new BitmapDrawable(null, bitmap2) : null;
                final BitmapDrawable bitmapDrawable2 = bitmap != null ? new BitmapDrawable(null, bitmap) : null;
                if (bitmapDrawable != null) {
                    ab.a(relativeLayout, bitmapDrawable);
                }
                if (igVar != null) {
                    relativeLayout.addView(igVar, layoutParams2);
                    igVar.a();
                }
                if (igVar2 != null) {
                    relativeLayout.addView(igVar2, layoutParams2);
                    igVar2.setVisibility(4);
                }
                final ig igVar7 = igVar2;
                final ig igVar8 = igVar;
                AnonymousClass1 r0 = new OnTouchListener() {
                    public final boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == 0) {
                            if (!(igVar7 == null && bitmapDrawable2 == null)) {
                                if (igVar8 != null) {
                                    igVar8.b();
                                    igVar8.setVisibility(4);
                                }
                                ab.a(view, null);
                            }
                            if (bitmapDrawable2 != null) {
                                ab.a(view, bitmapDrawable2);
                            } else if (igVar7 != null) {
                                igVar7.setVisibility(0);
                                igVar7.a();
                            }
                        } else {
                            boolean z = true;
                            if (motionEvent.getAction() == 1) {
                                float x = motionEvent.getX();
                                float y = motionEvent.getY();
                                if (x >= 0.0f && x < ((float) view.getWidth()) && y >= 0.0f && y < ((float) view.getHeight())) {
                                    z = false;
                                }
                                if (z) {
                                    if (bitmapDrawable != null) {
                                        ab.a(view, bitmapDrawable);
                                    } else if (bitmapDrawable2 != null) {
                                        ab.a(view, null);
                                    }
                                }
                                if (igVar7 != null) {
                                    igVar7.b();
                                    igVar7.setVisibility(4);
                                }
                                if (!((igVar7 == null && bitmapDrawable2 == null) || igVar8 == null || !z)) {
                                    igVar8.setVisibility(0);
                                    igVar8.a();
                                }
                            }
                        }
                        return false;
                    }
                };
                relativeLayout.setOnTouchListener(r0);
                final RelativeLayout relativeLayout2 = relativeLayout;
                final hz hzVar2 = hzVar;
                AnonymousClass2 r02 = new OnClickListener() {
                    public final void onClick(View view) {
                        if (igVar7 != null) {
                            igVar7.b();
                            relativeLayout2.removeView(igVar7);
                        }
                        if (igVar8 != null) {
                            igVar8.b();
                            relativeLayout2.removeView(igVar8);
                        }
                        io.this.b.a(hzVar2);
                    }
                };
                relativeLayout.setOnClickListener(r02);
                relativeLayout.setTag(hzVar);
                addView(relativeLayout, layoutParams);
            }
        }
    }
}
