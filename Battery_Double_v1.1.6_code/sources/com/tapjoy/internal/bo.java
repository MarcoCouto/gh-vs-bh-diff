package com.tapjoy.internal;

import com.tapjoy.internal.bn.a;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public final class bo extends bn {
    public static final a a = new a() {
        public final bn a(Reader reader) {
            return new bo(reader);
        }

        public final bn a(String str) {
            return new bo(new StringReader(str));
        }
    };
    private final cj b = new cj();
    private final Reader c;
    private boolean d = false;
    private final char[] e = new char[1024];
    private int f = 0;
    private int g = 0;
    private int h = 1;
    private int i = 1;
    private final List j = new ArrayList();
    private bs k;
    private String l;
    private String m;
    private int n;
    private int o;
    private boolean p;

    public bo(Reader reader) {
        a(bq.EMPTY_DOCUMENT);
        this.p = false;
        if (reader != null) {
            this.c = reader;
            return;
        }
        throw new NullPointerException("in == null");
    }

    public final void f() {
        a(bs.BEGIN_ARRAY);
    }

    public final void g() {
        a(bs.END_ARRAY);
    }

    public final void h() {
        a(bs.BEGIN_OBJECT);
    }

    public final void i() {
        a(bs.END_OBJECT);
    }

    private void a(bs bsVar) {
        k();
        if (this.k == bsVar) {
            t();
            return;
        }
        StringBuilder sb = new StringBuilder("Expected ");
        sb.append(bsVar);
        sb.append(" but was ");
        sb.append(k());
        throw new IllegalStateException(sb.toString());
    }

    public final boolean j() {
        k();
        return (this.k == bs.END_OBJECT || this.k == bs.END_ARRAY) ? false : true;
    }

    public final bs k() {
        if (this.k != null) {
            return this.k;
        }
        switch ((bq) this.j.get(this.j.size() - 1)) {
            case EMPTY_DOCUMENT:
                b(bq.NONEMPTY_DOCUMENT);
                bs v = v();
                if (this.d || this.k == bs.BEGIN_ARRAY || this.k == bs.BEGIN_OBJECT) {
                    return v;
                }
                StringBuilder sb = new StringBuilder("Expected JSON document to start with '[' or '{' but was ");
                sb.append(this.k);
                throw new IOException(sb.toString());
            case EMPTY_ARRAY:
                return a(true);
            case NONEMPTY_ARRAY:
                return a(false);
            case EMPTY_OBJECT:
                return b(true);
            case DANGLING_NAME:
                int y = y();
                if (y != 58) {
                    if (y == 61) {
                        z();
                        if ((this.f < this.g || a(1)) && this.e[this.f] == '>') {
                            this.f++;
                        }
                    } else {
                        throw d("Expected ':'");
                    }
                }
                b(bq.NONEMPTY_OBJECT);
                return v();
            case NONEMPTY_OBJECT:
                return b(false);
            case NONEMPTY_DOCUMENT:
                try {
                    bs v2 = v();
                    if (this.d) {
                        return v2;
                    }
                    throw d("Expected EOF");
                } catch (EOFException unused) {
                    bs bsVar = bs.END_DOCUMENT;
                    this.k = bsVar;
                    return bsVar;
                }
            case CLOSED:
                throw new IllegalStateException("JsonReader is closed");
            default:
                throw new AssertionError();
        }
    }

    private bs t() {
        k();
        bs bsVar = this.k;
        this.k = null;
        this.m = null;
        this.l = null;
        return bsVar;
    }

    public final String l() {
        k();
        if (this.k == bs.NAME) {
            String str = this.l;
            t();
            return str;
        }
        StringBuilder sb = new StringBuilder("Expected a name but was ");
        sb.append(k());
        throw new IllegalStateException(sb.toString());
    }

    public final String m() {
        k();
        if (this.k == bs.STRING || this.k == bs.NUMBER) {
            String str = this.m;
            t();
            return str;
        }
        StringBuilder sb = new StringBuilder("Expected a string but was ");
        sb.append(k());
        throw new IllegalStateException(sb.toString());
    }

    public final boolean n() {
        k();
        if (this.k == bs.BOOLEAN) {
            boolean z = this.m == "true";
            t();
            return z;
        }
        StringBuilder sb = new StringBuilder("Expected a boolean but was ");
        sb.append(this.k);
        throw new IllegalStateException(sb.toString());
    }

    public final void o() {
        k();
        if (this.k == bs.NULL) {
            t();
            return;
        }
        StringBuilder sb = new StringBuilder("Expected null but was ");
        sb.append(this.k);
        throw new IllegalStateException(sb.toString());
    }

    public final double p() {
        k();
        if (this.k == bs.STRING || this.k == bs.NUMBER) {
            double parseDouble = Double.parseDouble(this.m);
            t();
            return parseDouble;
        }
        StringBuilder sb = new StringBuilder("Expected a double but was ");
        sb.append(this.k);
        throw new IllegalStateException(sb.toString());
    }

    public final long q() {
        long j2;
        k();
        if (this.k == bs.STRING || this.k == bs.NUMBER) {
            try {
                j2 = Long.parseLong(this.m);
            } catch (NumberFormatException unused) {
                double parseDouble = Double.parseDouble(this.m);
                long j3 = (long) parseDouble;
                if (((double) j3) == parseDouble) {
                    j2 = j3;
                } else {
                    throw new NumberFormatException(this.m);
                }
            }
            t();
            return j2;
        }
        StringBuilder sb = new StringBuilder("Expected a long but was ");
        sb.append(this.k);
        throw new IllegalStateException(sb.toString());
    }

    public final int r() {
        int i2;
        k();
        if (this.k == bs.STRING || this.k == bs.NUMBER) {
            try {
                i2 = Integer.parseInt(this.m);
            } catch (NumberFormatException unused) {
                double parseDouble = Double.parseDouble(this.m);
                int i3 = (int) parseDouble;
                if (((double) i3) == parseDouble) {
                    i2 = i3;
                } else {
                    throw new NumberFormatException(this.m);
                }
            }
            t();
            return i2;
        }
        StringBuilder sb = new StringBuilder("Expected an int but was ");
        sb.append(this.k);
        throw new IllegalStateException(sb.toString());
    }

    public final void close() {
        this.m = null;
        this.k = null;
        this.j.clear();
        this.j.add(bq.CLOSED);
        this.c.close();
    }

    public final void s() {
        k();
        if (this.k == bs.END_ARRAY || this.k == bs.END_OBJECT) {
            StringBuilder sb = new StringBuilder("Expected a value but was ");
            sb.append(this.k);
            throw new IllegalStateException(sb.toString());
        }
        this.p = true;
        int i2 = 0;
        do {
            try {
                bs t = t();
                if (t != bs.BEGIN_ARRAY) {
                    if (t != bs.BEGIN_OBJECT) {
                        if (t == bs.END_ARRAY || t == bs.END_OBJECT) {
                            i2--;
                            continue;
                        }
                    }
                }
                i2++;
                continue;
            } finally {
                this.p = false;
            }
        } while (i2 != 0);
    }

    private bq u() {
        return (bq) this.j.remove(this.j.size() - 1);
    }

    private void a(bq bqVar) {
        this.j.add(bqVar);
    }

    private void b(bq bqVar) {
        this.j.set(this.j.size() - 1, bqVar);
    }

    private bs a(boolean z) {
        if (z) {
            b(bq.NONEMPTY_ARRAY);
        } else {
            int y = y();
            if (y != 44) {
                if (y == 59) {
                    z();
                } else if (y == 93) {
                    u();
                    bs bsVar = bs.END_ARRAY;
                    this.k = bsVar;
                    return bsVar;
                } else {
                    throw d("Unterminated array");
                }
            }
        }
        int y2 = y();
        if (!(y2 == 44 || y2 == 59)) {
            if (y2 != 93) {
                this.f--;
                return v();
            } else if (z) {
                u();
                bs bsVar2 = bs.END_ARRAY;
                this.k = bsVar2;
                return bsVar2;
            }
        }
        z();
        this.f--;
        this.m = "null";
        bs bsVar3 = bs.NULL;
        this.k = bsVar3;
        return bsVar3;
    }

    private bs b(boolean z) {
        if (!z) {
            int y = y();
            if (!(y == 44 || y == 59)) {
                if (y == 125) {
                    u();
                    bs bsVar = bs.END_OBJECT;
                    this.k = bsVar;
                    return bsVar;
                }
                throw d("Unterminated object");
            }
        } else if (y() != 125) {
            this.f--;
        } else {
            u();
            bs bsVar2 = bs.END_OBJECT;
            this.k = bsVar2;
            return bsVar2;
        }
        int y2 = y();
        if (y2 != 34) {
            if (y2 != 39) {
                z();
                this.f--;
                this.l = c(false);
                if (this.l.length() == 0) {
                    throw d("Expected name");
                }
                b(bq.DANGLING_NAME);
                bs bsVar3 = bs.NAME;
                this.k = bsVar3;
                return bsVar3;
            }
            z();
        }
        this.l = a((char) y2);
        b(bq.DANGLING_NAME);
        bs bsVar32 = bs.NAME;
        this.k = bsVar32;
        return bsVar32;
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x01eb  */
    private bs v() {
        bs bsVar;
        int i2;
        int i3;
        char c2;
        int y = y();
        if (y != 34) {
            if (y == 39) {
                z();
            } else if (y == 91) {
                a(bq.EMPTY_ARRAY);
                bs bsVar2 = bs.BEGIN_ARRAY;
                this.k = bsVar2;
                return bsVar2;
            } else if (y != 123) {
                this.f--;
                this.m = c(true);
                if (this.o != 0) {
                    if (this.n != -1) {
                        if (this.o == 4 && (('n' == this.e[this.n] || 'N' == this.e[this.n]) && (('u' == this.e[this.n + 1] || 'U' == this.e[this.n + 1]) && (('l' == this.e[this.n + 2] || 'L' == this.e[this.n + 2]) && ('l' == this.e[this.n + 3] || 'L' == this.e[this.n + 3]))))) {
                            this.m = "null";
                            bsVar = bs.NULL;
                            this.k = bsVar;
                            if (this.k == bs.STRING) {
                            }
                            return this.k;
                        } else if (this.o == 4 && (('t' == this.e[this.n] || 'T' == this.e[this.n]) && (('r' == this.e[this.n + 1] || 'R' == this.e[this.n + 1]) && (('u' == this.e[this.n + 2] || 'U' == this.e[this.n + 2]) && ('e' == this.e[this.n + 3] || 'E' == this.e[this.n + 3]))))) {
                            this.m = "true";
                            bsVar = bs.BOOLEAN;
                            this.k = bsVar;
                            if (this.k == bs.STRING) {
                            }
                            return this.k;
                        } else if (this.o == 5 && (('f' == this.e[this.n] || 'F' == this.e[this.n]) && (('a' == this.e[this.n + 1] || 'A' == this.e[this.n + 1]) && (('l' == this.e[this.n + 2] || 'L' == this.e[this.n + 2]) && (('s' == this.e[this.n + 3] || 'S' == this.e[this.n + 3]) && ('e' == this.e[this.n + 4] || 'E' == this.e[this.n + 4])))))) {
                            this.m = "false";
                            bsVar = bs.BOOLEAN;
                            this.k = bsVar;
                            if (this.k == bs.STRING) {
                            }
                            return this.k;
                        } else {
                            this.m = this.b.a(this.e, this.n, this.o);
                            char[] cArr = this.e;
                            int i4 = this.n;
                            int i5 = this.o;
                            char c3 = cArr[i4];
                            if (c3 == '-') {
                                int i6 = i4 + 1;
                                i2 = i6;
                                c3 = cArr[i6];
                            } else {
                                i2 = i4;
                            }
                            if (c3 == '0') {
                                i3 = i2 + 1;
                                c2 = cArr[i3];
                            } else if (c3 < '1' || c3 > '9') {
                                bsVar = bs.STRING;
                                this.k = bsVar;
                                if (this.k == bs.STRING) {
                                    z();
                                }
                                return this.k;
                            } else {
                                i3 = i2 + 1;
                                c2 = cArr[i3];
                                while (c2 >= '0' && c2 <= '9') {
                                    i3++;
                                    c2 = cArr[i3];
                                }
                            }
                            if (c2 == '.') {
                                int i7 = i3 + 1;
                                char c4 = cArr[i7];
                                while (c2 >= '0' && c2 <= '9') {
                                    i7 = i3 + 1;
                                    c4 = cArr[i7];
                                }
                            }
                            if (c2 == 'e' || c2 == 'E') {
                                int i8 = i3 + 1;
                                char c5 = cArr[i8];
                                if (c5 == '+' || c5 == '-') {
                                    i8++;
                                    c5 = cArr[i8];
                                }
                                if (c5 < '0' || c5 > '9') {
                                    bsVar = bs.STRING;
                                    this.k = bsVar;
                                    if (this.k == bs.STRING) {
                                    }
                                    return this.k;
                                }
                                int i9 = i8 + 1;
                                char c6 = cArr[i9];
                                while (c6 >= '0' && c6 <= '9') {
                                    i9 = i3 + 1;
                                    c6 = cArr[i9];
                                }
                            }
                            if (i3 == i4 + i5) {
                                bsVar = bs.NUMBER;
                                this.k = bsVar;
                                if (this.k == bs.STRING) {
                                }
                                return this.k;
                            }
                        }
                    }
                    bsVar = bs.STRING;
                    this.k = bsVar;
                    if (this.k == bs.STRING) {
                    }
                    return this.k;
                }
                throw d("Expected literal value");
            } else {
                a(bq.EMPTY_OBJECT);
                bs bsVar3 = bs.BEGIN_OBJECT;
                this.k = bsVar3;
                return bsVar3;
            }
        }
        this.m = a((char) y);
        bs bsVar4 = bs.STRING;
        this.k = bsVar4;
        return bsVar4;
    }

    private boolean a(int i2) {
        for (int i3 = 0; i3 < this.f; i3++) {
            if (this.e[i3] == 10) {
                this.h++;
                this.i = 1;
            } else {
                this.i++;
            }
        }
        if (this.g != this.f) {
            this.g -= this.f;
            System.arraycopy(this.e, this.f, this.e, 0, this.g);
        } else {
            this.g = 0;
        }
        this.f = 0;
        do {
            int read = this.c.read(this.e, this.g, this.e.length - this.g);
            if (read == -1) {
                return false;
            }
            this.g += read;
            if (this.h == 1 && this.i == 1 && this.g > 0 && this.e[0] == 65279) {
                this.f++;
                this.i--;
            }
        } while (this.g < i2);
        return true;
    }

    private int w() {
        int i2 = this.h;
        for (int i3 = 0; i3 < this.f; i3++) {
            if (this.e[i3] == 10) {
                i2++;
            }
        }
        return i2;
    }

    private int x() {
        int i2 = this.i;
        for (int i3 = 0; i3 < this.f; i3++) {
            i2 = this.e[i3] == 10 ? 1 : i2 + 1;
        }
        return i2;
    }

    private int y() {
        while (true) {
            boolean z = true;
            if (this.f < this.g || a(1)) {
                char[] cArr = this.e;
                int i2 = this.f;
                this.f = i2 + 1;
                char c2 = cArr[i2];
                if (!(c2 == 13 || c2 == ' ')) {
                    if (c2 == '#') {
                        z();
                        A();
                    } else if (c2 != '/') {
                        switch (c2) {
                            case 9:
                            case 10:
                                break;
                            default:
                                return c2;
                        }
                    } else if (this.f == this.g && !a(1)) {
                        return c2;
                    } else {
                        z();
                        char c3 = this.e[this.f];
                        if (c3 == '*') {
                            this.f++;
                            String str = "*/";
                            while (true) {
                                int i3 = 0;
                                if (this.f + str.length() > this.g && !a(str.length())) {
                                    z = false;
                                    break;
                                }
                                while (i3 < str.length()) {
                                    if (this.e[this.f + i3] == str.charAt(i3)) {
                                        i3++;
                                    } else {
                                        this.f++;
                                    }
                                }
                                break;
                            }
                            if (z) {
                                this.f += 2;
                            } else {
                                throw d("Unterminated comment");
                            }
                        } else if (c3 != '/') {
                            return c2;
                        } else {
                            this.f++;
                            A();
                        }
                    }
                }
            } else {
                throw new EOFException("End of input");
            }
        }
    }

    private void z() {
        if (!this.d) {
            throw d("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    private void A() {
        char c2;
        do {
            if (this.f < this.g || a(1)) {
                char[] cArr = this.e;
                int i2 = this.f;
                this.f = i2 + 1;
                c2 = cArr[i2];
                if (c2 == 13) {
                    return;
                }
            } else {
                return;
            }
        } while (c2 != 10);
    }

    private String a(char c2) {
        StringBuilder sb = null;
        do {
            int i2 = this.f;
            while (this.f < this.g) {
                char[] cArr = this.e;
                int i3 = this.f;
                this.f = i3 + 1;
                char c3 = cArr[i3];
                if (c3 == c2) {
                    if (this.p) {
                        return "skipped!";
                    }
                    if (sb == null) {
                        return this.b.a(this.e, i2, (this.f - i2) - 1);
                    }
                    sb.append(this.e, i2, (this.f - i2) - 1);
                    return sb.toString();
                } else if (c3 == '\\') {
                    if (sb == null) {
                        sb = new StringBuilder();
                    }
                    sb.append(this.e, i2, (this.f - i2) - 1);
                    if (this.f != this.g || a(1)) {
                        char[] cArr2 = this.e;
                        int i4 = this.f;
                        this.f = i4 + 1;
                        char c4 = cArr2[i4];
                        if (c4 == 'b') {
                            c4 = 8;
                        } else if (c4 == 'f') {
                            c4 = 12;
                        } else if (c4 == 'n') {
                            c4 = 10;
                        } else if (c4 != 'r') {
                            switch (c4) {
                                case 't':
                                    c4 = 9;
                                    break;
                                case 'u':
                                    if (this.f + 4 <= this.g || a(4)) {
                                        String a2 = this.b.a(this.e, this.f, 4);
                                        this.f += 4;
                                        c4 = (char) Integer.parseInt(a2, 16);
                                        break;
                                    } else {
                                        throw d("Unterminated escape sequence");
                                    }
                                    break;
                            }
                        } else {
                            c4 = 13;
                        }
                        sb.append(c4);
                        i2 = this.f;
                    } else {
                        throw d("Unterminated escape sequence");
                    }
                }
            }
            if (sb == null) {
                sb = new StringBuilder();
            }
            sb.append(this.e, i2, this.f - i2);
        } while (a(1));
        throw d("Unterminated string");
    }

    private String c(boolean z) {
        int i2;
        this.n = -1;
        int i3 = 0;
        this.o = 0;
        String str = null;
        StringBuilder sb = null;
        while (true) {
            i2 = 0;
            while (true) {
                if (this.f + i2 < this.g) {
                    switch (this.e[this.f + i2]) {
                        case 9:
                        case 10:
                        case 12:
                        case 13:
                        case ' ':
                        case ',':
                        case ':':
                        case '[':
                        case ']':
                        case '{':
                        case '}':
                            break;
                        case '#':
                        case '/':
                        case ';':
                        case '=':
                        case '\\':
                            z();
                            break;
                        default:
                            i2++;
                            break;
                    }
                } else if (i2 >= this.e.length) {
                    if (sb == null) {
                        sb = new StringBuilder();
                    }
                    sb.append(this.e, this.f, i2);
                    this.o += i2;
                    this.f += i2;
                    if (!a(1)) {
                    }
                } else if (!a(i2 + 1)) {
                    this.e[this.g] = 0;
                }
            }
        }
        i3 = i2;
        if (z && sb == null) {
            this.n = this.f;
        } else if (this.p) {
            str = "skipped!";
        } else if (sb == null) {
            str = this.b.a(this.e, this.f, i3);
        } else {
            sb.append(this.e, this.f, i3);
            str = sb.toString();
        }
        this.o += i3;
        this.f += i3;
        return str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" near ");
        StringBuilder sb2 = new StringBuilder();
        int min = Math.min(this.f, 20);
        sb2.append(this.e, this.f - min, min);
        sb2.append(this.e, this.f, Math.min(this.g - this.f, 20));
        sb.append(sb2);
        return sb.toString();
    }

    private IOException d(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" at line ");
        sb.append(w());
        sb.append(" column ");
        sb.append(x());
        throw new bu(sb.toString());
    }
}
