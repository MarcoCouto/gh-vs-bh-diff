package com.tapjoy.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Build.VERSION;
import android.support.v4.app.FragmentTransaction;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ic {
    private static final String d = "ic";
    int a;
    int b;
    ie c;
    private int[] e;
    private final int[] f;
    private ByteBuffer g;
    private byte[] h;
    @Nullable
    private byte[] i;
    private int j;
    private int k;
    private Cif l;
    private short[] m;
    private byte[] n;
    private byte[] o;
    private byte[] p;
    private int[] q;
    private a r;
    private Bitmap s;
    private boolean t;
    private int u;
    private int v;
    private int w;
    private int x;
    private boolean y;

    interface a {
        @Nonnull
        Bitmap a(int i, int i2, Config config);

        byte[] a(int i);

        int[] b(int i);
    }

    ic(a aVar, ie ieVar, ByteBuffer byteBuffer) {
        this(aVar, ieVar, byteBuffer, 0);
    }

    private ic(a aVar, ie ieVar, ByteBuffer byteBuffer, byte b2) {
        this(aVar);
        b(ieVar, byteBuffer);
    }

    private ic(a aVar) {
        this.f = new int[256];
        this.j = 0;
        this.k = 0;
        this.r = aVar;
        this.c = new ie();
    }

    ic() {
        this(new ih());
    }

    /* JADX WARNING: type inference failed for: r23v0 */
    /* JADX WARNING: type inference failed for: r24v0 */
    /* JADX WARNING: type inference failed for: r24v1 */
    /* JADX WARNING: type inference failed for: r23v1 */
    /* JADX WARNING: type inference failed for: r9v19 */
    /* JADX WARNING: type inference failed for: r27v0 */
    /* JADX WARNING: type inference failed for: r27v1 */
    /* JADX WARNING: type inference failed for: r9v20, types: [int] */
    /* JADX WARNING: type inference failed for: r2v28 */
    /* JADX WARNING: type inference failed for: r24v2 */
    /* JADX WARNING: type inference failed for: r23v2 */
    /* JADX WARNING: type inference failed for: r24v3 */
    /* JADX WARNING: type inference failed for: r23v3 */
    /* JADX WARNING: type inference failed for: r18v14 */
    /* JADX WARNING: type inference failed for: r27v2 */
    /* JADX WARNING: type inference failed for: r9v22 */
    /* JADX WARNING: type inference failed for: r2v32 */
    /* JADX WARNING: type inference failed for: r24v4 */
    /* JADX WARNING: type inference failed for: r23v4 */
    /* JADX WARNING: type inference failed for: r28v0 */
    /* JADX WARNING: type inference failed for: r2v34 */
    /* JADX WARNING: type inference failed for: r2v35 */
    /* JADX WARNING: type inference failed for: r9v24 */
    /* JADX WARNING: type inference failed for: r4v49, types: [short[]] */
    /* JADX WARNING: type inference failed for: r2v38, types: [short] */
    /* JADX WARNING: type inference failed for: r28v1 */
    /* JADX WARNING: type inference failed for: r28v2 */
    /* JADX WARNING: type inference failed for: r2v39, types: [int] */
    /* JADX WARNING: type inference failed for: r2v41 */
    /* JADX WARNING: type inference failed for: r27v4 */
    /* JADX WARNING: type inference failed for: r9v27 */
    /* JADX WARNING: type inference failed for: r9v29 */
    /* JADX WARNING: type inference failed for: r27v5 */
    /* JADX WARNING: type inference failed for: r9v30 */
    /* JADX WARNING: type inference failed for: r23v5 */
    /* JADX WARNING: type inference failed for: r24v5 */
    /* JADX WARNING: type inference failed for: r27v6 */
    /* JADX WARNING: type inference failed for: r24v6 */
    /* JADX WARNING: type inference failed for: r23v6 */
    /* JADX WARNING: type inference failed for: r18v20 */
    /* JADX WARNING: type inference failed for: r27v7 */
    /* JADX WARNING: type inference failed for: r9v38 */
    /* JADX WARNING: type inference failed for: r2v48 */
    /* JADX WARNING: type inference failed for: r2v49 */
    /* JADX WARNING: type inference failed for: r27v8 */
    /* JADX WARNING: type inference failed for: r9v39 */
    /* JADX WARNING: type inference failed for: r9v40 */
    /* JADX WARNING: type inference failed for: r9v41 */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=null, for r2v38, types: [short] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short[], code=null, for r4v49, types: [short[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r24v3
  assigns: []
  uses: []
  mth insns count: 535
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0222 A[LOOP:4: B:114:0x0220->B:115:0x0222, LOOP_END] */
    /* JADX WARNING: Unknown variable types count: 22 */
    public final synchronized Bitmap a() {
        int i2;
        int i3;
        ? r23;
        ? r24;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        ? r242;
        ? r232;
        ? r27;
        ? r9;
        ? r28;
        int i10;
        byte b2;
        ? r92;
        ? r272;
        int i11;
        synchronized (this) {
            if (this.c.c <= 0 || this.a < 0) {
                Object[] objArr = {Integer.valueOf(this.c.c), Integer.valueOf(this.a)};
                this.u = 1;
            }
            if (this.u != 1) {
                if (this.u != 2) {
                    this.u = 0;
                    id idVar = (id) this.c.e.get(this.a);
                    int i12 = this.a - 1;
                    id idVar2 = i12 >= 0 ? (id) this.c.e.get(i12) : null;
                    this.e = idVar.k != null ? idVar.k : this.c.a;
                    if (this.e == null) {
                        new Object[1][0] = Integer.valueOf(this.a);
                        this.u = 1;
                        return null;
                    }
                    if (idVar.f) {
                        System.arraycopy(this.e, 0, this.f, 0, this.e.length);
                        this.e = this.f;
                        this.e[idVar.h] = 0;
                    }
                    int[] iArr = this.q;
                    if (idVar2 == null) {
                        Arrays.fill(iArr, 0);
                    }
                    int i13 = 3;
                    if (idVar2 != null && idVar2.g > 0) {
                        if (idVar2.g == 2) {
                            if (!idVar.f) {
                                i11 = this.c.l;
                                if (idVar.k != null && this.c.j == idVar.h) {
                                }
                                a(iArr, idVar2, i11);
                            } else if (this.a == 0) {
                                this.y = true;
                            }
                            i11 = 0;
                            a(iArr, idVar2, i11);
                        } else if (idVar2.g == 3) {
                            if (this.s == null) {
                                a(iArr, idVar2, 0);
                            } else {
                                int i14 = idVar2.b / this.v;
                                int i15 = idVar2.a / this.v;
                                this.s.getPixels(iArr, (this.x * i14) + i15, this.x, i15, i14, idVar2.c / this.v, idVar2.d / this.v);
                            }
                        }
                    }
                    this.j = 0;
                    this.k = 0;
                    if (idVar != null) {
                        this.g.position(idVar.j);
                    }
                    if (idVar == null) {
                        i3 = this.c.f;
                        i2 = this.c.g;
                    } else {
                        i3 = idVar.c;
                        i2 = idVar.d;
                    }
                    int i16 = i3 * i2;
                    if (this.p == null || this.p.length < i16) {
                        this.p = this.r.a(i16);
                    }
                    if (this.m == null) {
                        this.m = new short[4096];
                    }
                    if (this.n == null) {
                        this.n = new byte[4096];
                    }
                    if (this.o == null) {
                        this.o = new byte[FragmentTransaction.TRANSIT_FRAGMENT_OPEN];
                    }
                    int c2 = c();
                    int i17 = 1 << c2;
                    int i18 = i17 + 1;
                    int i19 = i17 + 2;
                    int i20 = c2 + 1;
                    int i21 = (1 << i20) - 1;
                    for (int i22 = 0; i22 < i17; i22++) {
                        this.m[i22] = 0;
                        this.n[i22] = (byte) i22;
                    }
                    int i23 = -1;
                    int i24 = i20;
                    int i25 = i19;
                    int i26 = i21;
                    int i27 = 0;
                    int i28 = 0;
                    int i29 = 0;
                    int i30 = 0;
                    ? r18 = 0;
                    int i31 = 0;
                    int i32 = 0;
                    ? r233 = -1;
                    ? r243 = 0;
                    while (true) {
                        if (i27 >= i16) {
                            break;
                        }
                        if (i28 == 0) {
                            i28 = d();
                            if (i28 <= 0) {
                                this.u = i13;
                                break;
                            }
                            i30 = 0;
                        }
                        i29 += (this.h[i30] & 255) << r18;
                        i30++;
                        i28 += i23;
                        int i33 = r18 + 8;
                        int i34 = i25;
                        ? r93 = r233;
                        ? r273 = r243;
                        int i35 = i27;
                        int i36 = i24;
                        while (true) {
                            if (i33 < i36) {
                                r24 = r273;
                                i25 = i34;
                                r23 = r93;
                                i24 = i36;
                                i27 = i35;
                                i23 = -1;
                                i9 = i33;
                                break;
                            }
                            byte b3 = i29 & i26;
                            i29 >>= i36;
                            i33 -= i36;
                            if (b3 != i17) {
                                if (b3 <= i34) {
                                    if (b3 == i18) {
                                        break;
                                    }
                                    if (r93 == -1) {
                                        int i37 = i32 + 1;
                                        this.o[i32] = this.n[b3];
                                        ? r94 = b3;
                                        b2 = r94;
                                        i32 = i37;
                                        r92 = r94;
                                    } else {
                                        if (b3 >= i34) {
                                            i10 = i32 + 1;
                                            r28 = b3;
                                            this.o[i32] = (byte) r273;
                                            b3 = r93;
                                        } else {
                                            r28 = b3;
                                            i10 = i32;
                                        }
                                        ? r2 = b3;
                                        while (r2 >= i17) {
                                            int i38 = i10 + 1;
                                            int i39 = i33;
                                            this.o[i10] = this.n[r2];
                                            i10 = i38;
                                            i33 = i39;
                                            r2 = this.m[r2];
                                        }
                                        int i40 = i33;
                                        byte b4 = this.n[r2] & 255;
                                        int i41 = i10 + 1;
                                        byte b5 = (byte) b4;
                                        this.o[i10] = b5;
                                        if (i34 < 4096) {
                                            this.m[i34] = (short) r93;
                                            this.n[i34] = b5;
                                            i34++;
                                            if ((i34 & i26) == 0) {
                                                if (i34 < 4096) {
                                                    i36++;
                                                    i26 += i34;
                                                }
                                                i32 = i41;
                                                while (i32 > 0) {
                                                    int i42 = i31 + 1;
                                                    i32--;
                                                    this.p[i31] = this.o[i32];
                                                    i35++;
                                                    i31 = i42;
                                                }
                                                b2 = b4;
                                                r92 = r28;
                                                i33 = i40;
                                            }
                                        }
                                        i32 = i41;
                                        while (i32 > 0) {
                                        }
                                        b2 = b4;
                                        r92 = r28;
                                        i33 = i40;
                                    }
                                    i13 = 3;
                                    r27 = r272;
                                    r9 = r92;
                                } else {
                                    this.u = i13;
                                    break;
                                }
                            } else {
                                i36 = i20;
                                i34 = i19;
                                i26 = i21;
                                r27 = r273;
                                r9 = -1;
                            }
                            r273 = r27;
                            r93 = r9;
                        }
                        r24 = r273;
                        i25 = i34;
                        r23 = r93;
                        i24 = i36;
                        i27 = i35;
                        i9 = i33;
                        i23 = -1;
                        i13 = 3;
                        r243 = r242;
                        r233 = r232;
                        r18 = i9;
                    }
                    for (int i43 = i31; i43 < i16; i43++) {
                        this.p[i43] = 0;
                    }
                    int i44 = idVar.d / this.v;
                    int i45 = idVar.b / this.v;
                    int i46 = idVar.c / this.v;
                    int i47 = idVar.a / this.v;
                    boolean z = this.a == 0;
                    int i48 = 0;
                    int i49 = 0;
                    int i50 = 1;
                    int i51 = 8;
                    while (i48 < i44) {
                        if (idVar.e) {
                            if (i49 >= i44) {
                                i50++;
                                switch (i50) {
                                    case 2:
                                        i49 = 4;
                                        break;
                                    case 3:
                                        i49 = 2;
                                        i51 = 4;
                                        break;
                                    case 4:
                                        i49 = 1;
                                        i51 = 2;
                                        break;
                                }
                            }
                            i4 = i49 + i51;
                        } else {
                            i4 = i49;
                            i49 = i48;
                        }
                        int i52 = i49 + i45;
                        if (i52 < this.w) {
                            int i53 = i52 * this.x;
                            int i54 = i53 + i47;
                            int i55 = i54 + i46;
                            if (this.x + i53 < i55) {
                                i55 = this.x + i53;
                            }
                            int i56 = this.v * i48 * idVar.c;
                            i5 = i44;
                            int i57 = ((i55 - i54) * this.v) + i56;
                            while (i54 < i55) {
                                int i58 = i45;
                                if (this.v == 1) {
                                    i8 = this.e[this.p[i56] & 255];
                                    i7 = i46;
                                    i6 = i47;
                                } else {
                                    int i59 = idVar.c;
                                    i7 = i46;
                                    int i60 = i56;
                                    int i61 = 0;
                                    int i62 = 0;
                                    int i63 = 0;
                                    int i64 = 0;
                                    int i65 = 0;
                                    while (i60 < this.v + i56 && i60 < this.p.length && i60 < i57) {
                                        int i66 = i47;
                                        int i67 = this.e[this.p[i60] & 255];
                                        if (i67 != 0) {
                                            i61 += (i67 >> 24) & 255;
                                            i62 += (i67 >> 16) & 255;
                                            i63 += (i67 >> 8) & 255;
                                            i64 += i67 & 255;
                                            i65++;
                                        }
                                        i60++;
                                        i47 = i66;
                                    }
                                    i6 = i47;
                                    int i68 = i59 + i56;
                                    int i69 = i68;
                                    while (i69 < this.v + i68 && i69 < this.p.length && i69 < i57) {
                                        int i70 = this.e[this.p[i69] & 255];
                                        if (i70 != 0) {
                                            i61 += (i70 >> 24) & 255;
                                            i62 += (i70 >> 16) & 255;
                                            i63 += (i70 >> 8) & 255;
                                            i64 += i70 & 255;
                                            i65++;
                                        }
                                        i69++;
                                    }
                                    if (i65 == 0) {
                                        i8 = 0;
                                    } else {
                                        i8 = ((i61 / i65) << 24) | ((i62 / i65) << 16) | ((i63 / i65) << 8) | (i64 / i65);
                                    }
                                }
                                if (i8 != 0) {
                                    iArr[i54] = i8;
                                } else if (!this.y && z) {
                                    this.y = true;
                                }
                                i56 += this.v;
                                i54++;
                                i45 = i58;
                                i46 = i7;
                                i47 = i6;
                            }
                        } else {
                            i5 = i44;
                        }
                        i48++;
                        i49 = i4;
                        i44 = i5;
                        i45 = i45;
                        i46 = i46;
                        i47 = i47;
                    }
                    if (this.t && (idVar.g == 0 || idVar.g == 1)) {
                        if (this.s == null) {
                            this.s = e();
                        }
                        this.s.setPixels(iArr, 0, this.x, 0, 0, this.x, this.w);
                    }
                    Bitmap e2 = e();
                    e2.setPixels(iArr, 0, this.x, 0, 0, this.x, this.w);
                    return e2;
                }
            }
            new Object[1][0] = Integer.valueOf(this.u);
            return null;
        }
    }

    private synchronized void a(ie ieVar, byte[] bArr) {
        a(ieVar, ByteBuffer.wrap(bArr));
    }

    private synchronized void a(ie ieVar, ByteBuffer byteBuffer) {
        b(ieVar, byteBuffer);
    }

    private synchronized void b(ie ieVar, ByteBuffer byteBuffer) {
        int highestOneBit = Integer.highestOneBit(1);
        this.u = 0;
        this.c = ieVar;
        this.y = false;
        this.a = -1;
        this.b = 0;
        this.g = byteBuffer.asReadOnlyBuffer();
        this.g.position(0);
        this.g.order(ByteOrder.LITTLE_ENDIAN);
        this.t = false;
        Iterator it = ieVar.e.iterator();
        while (true) {
            if (it.hasNext()) {
                if (((id) it.next()).g == 3) {
                    this.t = true;
                    break;
                }
            } else {
                break;
            }
        }
        this.v = highestOneBit;
        this.x = ieVar.f / highestOneBit;
        this.w = ieVar.g / highestOneBit;
        this.p = this.r.a(ieVar.f * ieVar.g);
        this.q = this.r.b(this.x * this.w);
    }

    private void a(int[] iArr, id idVar, int i2) {
        int i3 = idVar.d / this.v;
        int i4 = idVar.b / this.v;
        int i5 = idVar.c / this.v;
        int i6 = (i4 * this.x) + (idVar.a / this.v);
        int i7 = (i3 * this.x) + i6;
        while (i6 < i7) {
            int i8 = i6 + i5;
            for (int i9 = i6; i9 < i8; i9++) {
                iArr[i9] = i2;
            }
            i6 += this.x;
        }
    }

    private void b() {
        if (this.j <= this.k) {
            if (this.i == null) {
                this.i = this.r.a(16384);
            }
            this.k = 0;
            this.j = Math.min(this.g.remaining(), 16384);
            this.g.get(this.i, 0, this.j);
        }
    }

    private int c() {
        try {
            b();
            byte[] bArr = this.i;
            int i2 = this.k;
            this.k = i2 + 1;
            return bArr[i2] & 255;
        } catch (Exception unused) {
            this.u = 1;
            return 0;
        }
    }

    private int d() {
        int c2 = c();
        if (c2 > 0) {
            try {
                if (this.h == null) {
                    this.h = this.r.a(255);
                }
                int i2 = this.j - this.k;
                if (i2 >= c2) {
                    System.arraycopy(this.i, this.k, this.h, 0, c2);
                    this.k += c2;
                } else if (this.g.remaining() + i2 >= c2) {
                    System.arraycopy(this.i, this.k, this.h, 0, i2);
                    this.k = this.j;
                    b();
                    int i3 = c2 - i2;
                    System.arraycopy(this.i, 0, this.h, i2, i3);
                    this.k += i3;
                } else {
                    this.u = 1;
                }
            } catch (Exception e2) {
                new Object[1][0] = e2;
                this.u = 1;
            }
        }
        return c2;
    }

    private Bitmap e() {
        Bitmap a2 = this.r.a(this.x, this.w, this.y ? Config.ARGB_4444 : Config.RGB_565);
        if (VERSION.SDK_INT >= 12) {
            a2.setHasAlpha(true);
        }
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized int a(byte[] bArr) {
        if (this.l == null) {
            this.l = new Cif();
        }
        this.c = this.l.a(bArr).a();
        if (bArr != null) {
            a(this.c, bArr);
        }
        return this.u;
    }
}
