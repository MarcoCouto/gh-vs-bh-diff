package com.tapjoy.internal;

import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import java.io.File;
import java.util.concurrent.TimeUnit;

public final class gx implements Runnable {
    final hl a;
    cd b;
    private final Object c = this.a;
    private final Thread d;
    private boolean e;

    public gx(File file) {
        this.a = new hl(file);
        new Object[1][0] = Integer.valueOf(this.a.b());
        this.d = new Thread(this, "5Rocks");
        this.d.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00f6, code lost:
        r7 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x014a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x014b, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0093 A[Catch:{ InterruptedException -> 0x014b, Exception -> 0x014a }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x010a A[Catch:{ InterruptedException -> 0x014b, Exception -> 0x014a }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[ExcHandler: InterruptedException (unused java.lang.InterruptedException), SYNTHETIC, Splitter:B:2:0x0006] */
    public final void run() {
        long j;
        ik ikVar;
        int i = 1;
        while (true) {
            long j2 = 0;
            while (this.b != null && this.a.b() > 0 && j2 <= 0) {
                try {
                    if (this.a.b() > 10000) {
                        this.a.a(this.a.b() - 10000);
                    }
                    ev b2 = this.a.b(0);
                    if (b2 == null) {
                        break;
                    }
                    fh fhVar = b2.w;
                    if (fhVar != null && fhVar.G == null) {
                        hn.c.await(3, TimeUnit.SECONDS);
                    }
                    if (!v.c()) {
                        hn.b.await(3, TimeUnit.SECONDS);
                    }
                    if (!this.e && b2.n != ey.APP && this.a.b() < 100) {
                        if (b2.p.longValue() <= System.currentTimeMillis()) {
                            j = Math.min(Math.max((b2.p.longValue() + ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) - System.currentTimeMillis(), 0), ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            if (j > 0) {
                                ikVar = new ik();
                                ikVar.a(b2);
                                new Object[1][0] = b2;
                                int i2 = 1;
                                while (i2 < 100 && i2 < this.a.b()) {
                                    ev b3 = this.a.b(i2);
                                    if (b3 == null || !ikVar.a(b3)) {
                                        break;
                                    }
                                    new Object[1][0] = b3;
                                    i2++;
                                }
                                i++;
                                Object[] objArr = {Integer.valueOf(ikVar.g()), Integer.valueOf(i)};
                                this.b.a(ikVar);
                                this.a.a(ikVar.g());
                                new Object[1][0] = Integer.valueOf(ikVar.g());
                                j2 = j;
                                i = 0;
                            } else {
                                j2 = j;
                            }
                        }
                    }
                    j = 0;
                    if (j > 0) {
                    }
                } catch (Exception e2) {
                    e = e2;
                    i = 0;
                    Object[] objArr2 = {Integer.valueOf(ikVar.g()), e};
                    j2 = 300000;
                } catch (InterruptedException unused) {
                }
            }
            this.a.flush();
            if (j2 > 0) {
                synchronized (this.c) {
                    this.e = false;
                    new Object[1][0] = Long.valueOf(j2);
                    this.c.wait(j2);
                }
            } else {
                synchronized (this.c) {
                    this.e = false;
                    if (this.b == null || this.a.c()) {
                        this.c.wait();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        synchronized (this.c) {
            this.e = z;
            this.c.notify();
        }
    }

    public final void a() {
        if (this.b != null && !this.a.c()) {
            a(true);
        }
    }
}
