package com.tapjoy.internal;

import android.support.annotation.VisibleForTesting;
import com.tapjoy.internal.dy.b;
import org.json.JSONObject;

public final class ed implements b {
    final dz a;
    private JSONObject b;

    public ed(dz dzVar) {
        this.a = dzVar;
    }

    @VisibleForTesting
    public final JSONObject a() {
        return this.b;
    }

    @VisibleForTesting
    public final void a(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    public final void b() {
        this.a.a(new ea(this));
    }
}
