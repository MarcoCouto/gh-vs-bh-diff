package com.tapjoy.internal;

import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;

public final class ev extends eh {
    public static final ej c = new b();
    public static final ey d = ey.APP;
    public static final Long e = Long.valueOf(0);
    public static final Long f = Long.valueOf(0);
    public static final Long g = Long.valueOf(0);
    public static final Long h = Long.valueOf(0);
    public static final Integer i = Integer.valueOf(0);
    public static final Integer j = Integer.valueOf(0);
    public static final Integer k = Integer.valueOf(0);
    public static final Long l = Long.valueOf(0);
    public static final Long m = Long.valueOf(0);
    public final fd A;
    public final String B;
    public final String C;
    public final fc D;
    public final String E;
    public final String F;
    public final String G;
    public final List H;
    public final String I;
    public final Integer J;
    public final Long K;
    public final Long L;
    public final ey n;
    public final String o;
    public final Long p;
    public final Long q;
    public final String r;
    public final Long s;
    public final Long t;
    public final fa u;
    public final eu v;
    public final fh w;
    public final Integer x;
    public final Integer y;
    public final ex z;

    public static final class a extends com.tapjoy.internal.eh.a {
        public Long A;
        public ey c;
        public String d;
        public Long e;
        public Long f;
        public String g;
        public Long h;
        public Long i;
        public fa j;
        public eu k;
        public fh l;
        public Integer m;
        public Integer n;
        public ex o;
        public fd p;
        public String q;
        public String r;
        public fc s;
        public String t;
        public String u;
        public String v;
        public List w = eo.a();
        public String x;
        public Integer y;
        public Long z;

        public final ev b() {
            if (this.c == null || this.d == null || this.e == null) {
                throw eo.a(this.c, "type", this.d, "name", this.e, LocationConst.TIME);
            }
            ev evVar = r2;
            ev evVar2 = new ev(this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, super.a());
            return evVar;
        }
    }

    static final class b extends ej {
        public final /* synthetic */ int a(Object obj) {
            ev evVar = (ev) obj;
            int i = 0;
            int a = ey.ADAPTER.a(1, (Object) evVar.n) + ej.p.a(2, (Object) evVar.o) + ej.i.a(3, (Object) evVar.p) + (evVar.q != null ? ej.i.a(19, (Object) evVar.q) : 0) + (evVar.r != null ? ej.p.a(20, (Object) evVar.r) : 0) + (evVar.s != null ? ej.i.a(21, (Object) evVar.s) : 0) + (evVar.t != null ? ej.i.a(4, (Object) evVar.t) : 0) + (evVar.u != null ? fa.c.a(5, (Object) evVar.u) : 0) + (evVar.v != null ? eu.c.a(6, (Object) evVar.v) : 0) + (evVar.w != null ? fh.c.a(7, (Object) evVar.w) : 0) + (evVar.x != null ? ej.d.a(8, (Object) evVar.x) : 0) + (evVar.y != null ? ej.d.a(9, (Object) evVar.y) : 0) + (evVar.z != null ? ex.c.a(10, (Object) evVar.z) : 0) + (evVar.A != null ? fd.c.a(11, (Object) evVar.A) : 0) + (evVar.B != null ? ej.p.a(12, (Object) evVar.B) : 0) + (evVar.C != null ? ej.p.a(13, (Object) evVar.C) : 0) + (evVar.D != null ? fc.c.a(18, (Object) evVar.D) : 0) + (evVar.E != null ? ej.p.a(14, (Object) evVar.E) : 0) + (evVar.F != null ? ej.p.a(15, (Object) evVar.F) : 0) + (evVar.G != null ? ej.p.a(16, (Object) evVar.G) : 0) + ez.c.a().a(17, (Object) evVar.H) + (evVar.I != null ? ej.p.a(22, (Object) evVar.I) : 0) + (evVar.J != null ? ej.d.a(23, (Object) evVar.J) : 0) + (evVar.K != null ? ej.i.a(24, (Object) evVar.K) : 0);
            if (evVar.L != null) {
                i = ej.i.a(25, (Object) evVar.L);
            }
            return a + i + evVar.a().c();
        }

        public final /* synthetic */ Object a(ek ekVar) {
            return b(ekVar);
        }

        public final /* bridge */ /* synthetic */ void a(el elVar, Object obj) {
            ev evVar = (ev) obj;
            ey.ADAPTER.a(elVar, 1, evVar.n);
            ej.p.a(elVar, 2, evVar.o);
            ej.i.a(elVar, 3, evVar.p);
            if (evVar.q != null) {
                ej.i.a(elVar, 19, evVar.q);
            }
            if (evVar.r != null) {
                ej.p.a(elVar, 20, evVar.r);
            }
            if (evVar.s != null) {
                ej.i.a(elVar, 21, evVar.s);
            }
            if (evVar.t != null) {
                ej.i.a(elVar, 4, evVar.t);
            }
            if (evVar.u != null) {
                fa.c.a(elVar, 5, evVar.u);
            }
            if (evVar.v != null) {
                eu.c.a(elVar, 6, evVar.v);
            }
            if (evVar.w != null) {
                fh.c.a(elVar, 7, evVar.w);
            }
            if (evVar.x != null) {
                ej.d.a(elVar, 8, evVar.x);
            }
            if (evVar.y != null) {
                ej.d.a(elVar, 9, evVar.y);
            }
            if (evVar.z != null) {
                ex.c.a(elVar, 10, evVar.z);
            }
            if (evVar.A != null) {
                fd.c.a(elVar, 11, evVar.A);
            }
            if (evVar.B != null) {
                ej.p.a(elVar, 12, evVar.B);
            }
            if (evVar.C != null) {
                ej.p.a(elVar, 13, evVar.C);
            }
            if (evVar.D != null) {
                fc.c.a(elVar, 18, evVar.D);
            }
            if (evVar.E != null) {
                ej.p.a(elVar, 14, evVar.E);
            }
            if (evVar.F != null) {
                ej.p.a(elVar, 15, evVar.F);
            }
            if (evVar.G != null) {
                ej.p.a(elVar, 16, evVar.G);
            }
            ez.c.a().a(elVar, 17, evVar.H);
            if (evVar.I != null) {
                ej.p.a(elVar, 22, evVar.I);
            }
            if (evVar.J != null) {
                ej.d.a(elVar, 23, evVar.J);
            }
            if (evVar.K != null) {
                ej.i.a(elVar, 24, evVar.K);
            }
            if (evVar.L != null) {
                ej.i.a(elVar, 25, evVar.L);
            }
            elVar.a(evVar.a());
        }

        b() {
            super(eg.LENGTH_DELIMITED, ev.class);
        }

        private static ev b(ek ekVar) {
            a aVar = new a();
            long a = ekVar.a();
            while (true) {
                int b = ekVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            try {
                                aVar.c = (ey) ey.ADAPTER.a(ekVar);
                                break;
                            } catch (com.tapjoy.internal.ej.a e) {
                                aVar.a(b, eg.VARINT, Long.valueOf((long) e.a));
                                break;
                            }
                        case 2:
                            aVar.d = (String) ej.p.a(ekVar);
                            break;
                        case 3:
                            aVar.e = (Long) ej.i.a(ekVar);
                            break;
                        case 4:
                            aVar.i = (Long) ej.i.a(ekVar);
                            break;
                        case 5:
                            aVar.j = (fa) fa.c.a(ekVar);
                            break;
                        case 6:
                            aVar.k = (eu) eu.c.a(ekVar);
                            break;
                        case 7:
                            aVar.l = (fh) fh.c.a(ekVar);
                            break;
                        case 8:
                            aVar.m = (Integer) ej.d.a(ekVar);
                            break;
                        case 9:
                            aVar.n = (Integer) ej.d.a(ekVar);
                            break;
                        case 10:
                            aVar.o = (ex) ex.c.a(ekVar);
                            break;
                        case 11:
                            aVar.p = (fd) fd.c.a(ekVar);
                            break;
                        case 12:
                            aVar.q = (String) ej.p.a(ekVar);
                            break;
                        case 13:
                            aVar.r = (String) ej.p.a(ekVar);
                            break;
                        case 14:
                            aVar.t = (String) ej.p.a(ekVar);
                            break;
                        case 15:
                            aVar.u = (String) ej.p.a(ekVar);
                            break;
                        case 16:
                            aVar.v = (String) ej.p.a(ekVar);
                            break;
                        case 17:
                            aVar.w.add(ez.c.a(ekVar));
                            break;
                        case 18:
                            aVar.s = (fc) fc.c.a(ekVar);
                            break;
                        case 19:
                            aVar.f = (Long) ej.i.a(ekVar);
                            break;
                        case 20:
                            aVar.g = (String) ej.p.a(ekVar);
                            break;
                        case 21:
                            aVar.h = (Long) ej.i.a(ekVar);
                            break;
                        case 22:
                            aVar.x = (String) ej.p.a(ekVar);
                            break;
                        case 23:
                            aVar.y = (Integer) ej.d.a(ekVar);
                            break;
                        case 24:
                            aVar.z = (Long) ej.i.a(ekVar);
                            break;
                        case 25:
                            aVar.A = (Long) ej.i.a(ekVar);
                            break;
                        default:
                            eg c = ekVar.c();
                            aVar.a(b, c, c.a().a(ekVar));
                            break;
                    }
                } else {
                    ekVar.a(a);
                    return aVar.b();
                }
            }
        }
    }

    public ev(ey eyVar, String str, Long l2, Long l3, String str2, Long l4, Long l5, fa faVar, eu euVar, fh fhVar, Integer num, Integer num2, ex exVar, fd fdVar, String str3, String str4, fc fcVar, String str5, String str6, String str7, List list, String str8, Integer num3, Long l6, Long l7, iu iuVar) {
        super(c, iuVar);
        this.n = eyVar;
        this.o = str;
        this.p = l2;
        this.q = l3;
        this.r = str2;
        this.s = l4;
        this.t = l5;
        this.u = faVar;
        this.v = euVar;
        this.w = fhVar;
        this.x = num;
        this.y = num2;
        this.z = exVar;
        this.A = fdVar;
        this.B = str3;
        this.C = str4;
        this.D = fcVar;
        this.E = str5;
        this.F = str6;
        this.G = str7;
        this.H = eo.a(String.USAGE_TRACKER_VALUES, list);
        this.I = str8;
        this.J = num3;
        this.K = l6;
        this.L = l7;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ev)) {
            return false;
        }
        ev evVar = (ev) obj;
        return a().equals(evVar.a()) && this.n.equals(evVar.n) && this.o.equals(evVar.o) && this.p.equals(evVar.p) && eo.a((Object) this.q, (Object) evVar.q) && eo.a((Object) this.r, (Object) evVar.r) && eo.a((Object) this.s, (Object) evVar.s) && eo.a((Object) this.t, (Object) evVar.t) && eo.a((Object) this.u, (Object) evVar.u) && eo.a((Object) this.v, (Object) evVar.v) && eo.a((Object) this.w, (Object) evVar.w) && eo.a((Object) this.x, (Object) evVar.x) && eo.a((Object) this.y, (Object) evVar.y) && eo.a((Object) this.z, (Object) evVar.z) && eo.a((Object) this.A, (Object) evVar.A) && eo.a((Object) this.B, (Object) evVar.B) && eo.a((Object) this.C, (Object) evVar.C) && eo.a((Object) this.D, (Object) evVar.D) && eo.a((Object) this.E, (Object) evVar.E) && eo.a((Object) this.F, (Object) evVar.F) && eo.a((Object) this.G, (Object) evVar.G) && this.H.equals(evVar.H) && eo.a((Object) this.I, (Object) evVar.I) && eo.a((Object) this.J, (Object) evVar.J) && eo.a((Object) this.K, (Object) evVar.K) && eo.a((Object) this.L, (Object) evVar.L);
    }

    public final int hashCode() {
        int i2 = this.b;
        if (i2 != 0) {
            return i2;
        }
        int i3 = 0;
        int hashCode = ((((((((((((((((((((((((((((((((((((((((((((((((a().hashCode() * 37) + this.n.hashCode()) * 37) + this.o.hashCode()) * 37) + this.p.hashCode()) * 37) + (this.q != null ? this.q.hashCode() : 0)) * 37) + (this.r != null ? this.r.hashCode() : 0)) * 37) + (this.s != null ? this.s.hashCode() : 0)) * 37) + (this.t != null ? this.t.hashCode() : 0)) * 37) + (this.u != null ? this.u.hashCode() : 0)) * 37) + (this.v != null ? this.v.hashCode() : 0)) * 37) + (this.w != null ? this.w.hashCode() : 0)) * 37) + (this.x != null ? this.x.hashCode() : 0)) * 37) + (this.y != null ? this.y.hashCode() : 0)) * 37) + (this.z != null ? this.z.hashCode() : 0)) * 37) + (this.A != null ? this.A.hashCode() : 0)) * 37) + (this.B != null ? this.B.hashCode() : 0)) * 37) + (this.C != null ? this.C.hashCode() : 0)) * 37) + (this.D != null ? this.D.hashCode() : 0)) * 37) + (this.E != null ? this.E.hashCode() : 0)) * 37) + (this.F != null ? this.F.hashCode() : 0)) * 37) + (this.G != null ? this.G.hashCode() : 0)) * 37) + this.H.hashCode()) * 37) + (this.I != null ? this.I.hashCode() : 0)) * 37) + (this.J != null ? this.J.hashCode() : 0)) * 37) + (this.K != null ? this.K.hashCode() : 0)) * 37;
        if (this.L != null) {
            i3 = this.L.hashCode();
        }
        int i4 = hashCode + i3;
        this.b = i4;
        return i4;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(", type=");
        sb.append(this.n);
        sb.append(", name=");
        sb.append(this.o);
        sb.append(", time=");
        sb.append(this.p);
        if (this.q != null) {
            sb.append(", systemTime=");
            sb.append(this.q);
        }
        if (this.r != null) {
            sb.append(", instanceId=");
            sb.append(this.r);
        }
        if (this.s != null) {
            sb.append(", elapsedRealtime=");
            sb.append(this.s);
        }
        if (this.t != null) {
            sb.append(", duration=");
            sb.append(this.t);
        }
        if (this.u != null) {
            sb.append(", info=");
            sb.append(this.u);
        }
        if (this.v != null) {
            sb.append(", app=");
            sb.append(this.v);
        }
        if (this.w != null) {
            sb.append(", user=");
            sb.append(this.w);
        }
        if (this.x != null) {
            sb.append(", xxx_session_seq=");
            sb.append(this.x);
        }
        if (this.y != null) {
            sb.append(", eventSeq=");
            sb.append(this.y);
        }
        if (this.z != null) {
            sb.append(", eventPrev=");
            sb.append(this.z);
        }
        if (this.A != null) {
            sb.append(", purchase=");
            sb.append(this.A);
        }
        if (this.B != null) {
            sb.append(", exception=");
            sb.append(this.B);
        }
        if (this.C != null) {
            sb.append(", metaBase=");
            sb.append(this.C);
        }
        if (this.D != null) {
            sb.append(", meta=");
            sb.append(this.D);
        }
        if (this.E != null) {
            sb.append(", category=");
            sb.append(this.E);
        }
        if (this.F != null) {
            sb.append(", p1=");
            sb.append(this.F);
        }
        if (this.G != null) {
            sb.append(", p2=");
            sb.append(this.G);
        }
        if (!this.H.isEmpty()) {
            sb.append(", values=");
            sb.append(this.H);
        }
        if (this.I != null) {
            sb.append(", dimensions=");
            sb.append(this.I);
        }
        if (this.J != null) {
            sb.append(", count=");
            sb.append(this.J);
        }
        if (this.K != null) {
            sb.append(", firstTime=");
            sb.append(this.K);
        }
        if (this.L != null) {
            sb.append(", lastTime=");
            sb.append(this.L);
        }
        StringBuilder replace = sb.replace(0, 2, "Event{");
        replace.append('}');
        return replace.toString();
    }
}
