package com.tapjoy.internal;

import android.graphics.Point;
import android.graphics.Rect;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;

public final class bj {
    public static final bi a = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            Point point = new Point();
            bnVar.h();
            while (bnVar.j()) {
                String l = bnVar.l();
                if (AvidJSONUtil.KEY_X.equals(l)) {
                    point.x = bnVar.r();
                } else if (AvidJSONUtil.KEY_Y.equals(l)) {
                    point.y = bnVar.r();
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            return point;
        }
    };
    public static final bi b = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            Rect rect = new Rect();
            switch (AnonymousClass3.a[bnVar.k().ordinal()]) {
                case 1:
                    bnVar.f();
                    rect.left = bnVar.r();
                    rect.top = bnVar.r();
                    rect.right = bnVar.r();
                    rect.bottom = bnVar.r();
                    while (bnVar.j()) {
                        bnVar.s();
                    }
                    bnVar.g();
                    break;
                case 2:
                    bnVar.h();
                    while (bnVar.j()) {
                        String l = bnVar.l();
                        if ("left".equals(l)) {
                            rect.left = bnVar.r();
                        } else if (String.TOP.equals(l)) {
                            rect.top = bnVar.r();
                        } else if ("right".equals(l)) {
                            rect.right = bnVar.r();
                        } else if (String.BOTTOM.equals(l)) {
                            rect.bottom = bnVar.r();
                        } else {
                            bnVar.s();
                        }
                    }
                    bnVar.i();
                    break;
                default:
                    StringBuilder sb = new StringBuilder("Unexpected token: ");
                    sb.append(bnVar.k());
                    throw new IllegalStateException(sb.toString());
            }
            return rect;
        }
    };

    /* renamed from: com.tapjoy.internal.bj$3 reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] a = new int[bs.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            a[bs.BEGIN_ARRAY.ordinal()] = 1;
            a[bs.BEGIN_OBJECT.ordinal()] = 2;
        }
    }
}
