package com.tapjoy.internal;

import com.tapjoy.internal.gf.a;

public final class fw {
    public fq a;
    public volatile a b;
    public int c;
    public volatile a d;
    public volatile a e;

    public final void a() {
        a(16);
        a aVar = this.d;
        if (aVar != null) {
            this.d = null;
            aVar.b().c();
        }
    }

    public final synchronized void a(int i) {
        a aVar = this.b;
        if (aVar != null && this.c < i) {
            this.c = i | this.c;
            aVar.a("state", (Object) Integer.valueOf(this.c)).b().c();
        }
    }
}
