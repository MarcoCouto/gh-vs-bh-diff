package com.tapjoy.internal;

import android.content.Context;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJPlacementManager;
import com.tapjoy.TapjoyConnectCore;
import com.tapjoy.internal.hk.a;
import java.util.Observer;

public final class fx extends hk {
    private final fz b = new fz() {
        /* access modifiers changed from: protected */
        public final boolean a() {
            return true;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ TJPlacement a(Context context, TJPlacementListener tJPlacementListener, Object obj) {
            a aVar = (a) obj;
            TJPlacement createPlacement = TJPlacementManager.createPlacement(TapjoyConnectCore.getContext(), aVar.b, false, tJPlacementListener);
            createPlacement.pushId = aVar.a;
            return createPlacement;
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ String a(Object obj) {
            a aVar = (a) obj;
            if (aVar != null) {
                return aVar.b;
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ a b(Object obj) {
            a aVar = (a) obj;
            return new a(aVar, aVar.d);
        }

        /* access modifiers changed from: protected */
        public final boolean a(Observer observer) {
            if (TapjoyConnectCore.isViewOpen()) {
                TJPlacementManager.dismissContentShowing(true);
            }
            return super.a(observer);
        }
    };

    public static void a() {
    }

    static {
        hk.a((hk) new fx());
    }

    private fx() {
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return this.b.b != null;
    }

    /* access modifiers changed from: protected */
    public final void a(a aVar) {
        this.b.c(aVar);
    }
}
