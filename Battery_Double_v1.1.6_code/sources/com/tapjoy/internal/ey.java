package com.tapjoy.internal;

public enum ey implements em {
    APP(0),
    CAMPAIGN(1),
    CUSTOM(2),
    USAGES(3);
    
    public static final ej ADAPTER = null;
    private final int a;

    static final class a extends ef {
        a() {
            super(ey.class);
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ em a(int i) {
            return ey.a(i);
        }
    }

    static {
        ADAPTER = new a();
    }

    private ey(int i) {
        this.a = i;
    }

    public static ey a(int i) {
        switch (i) {
            case 0:
                return APP;
            case 1:
                return CAMPAIGN;
            case 2:
                return CUSTOM;
            case 3:
                return USAGES;
            default:
                return null;
        }
    }

    public final int a() {
        return this.a;
    }
}
