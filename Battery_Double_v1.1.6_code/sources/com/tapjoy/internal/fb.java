package com.tapjoy.internal;

public final class fb extends eh {
    public static final ej c = new b();
    public final fa d;
    public final eu e;
    public final fh f;

    public static final class a extends com.tapjoy.internal.eh.a {
        public fa c;
        public eu d;
        public fh e;

        public final fb b() {
            return new fb(this.c, this.d, this.e, super.a());
        }
    }

    static final class b extends ej {
        public final /* synthetic */ int a(Object obj) {
            fb fbVar = (fb) obj;
            int i = 0;
            int a = (fbVar.d != null ? fa.c.a(1, (Object) fbVar.d) : 0) + (fbVar.e != null ? eu.c.a(2, (Object) fbVar.e) : 0);
            if (fbVar.f != null) {
                i = fh.c.a(3, (Object) fbVar.f);
            }
            return a + i + fbVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(el elVar, Object obj) {
            fb fbVar = (fb) obj;
            if (fbVar.d != null) {
                fa.c.a(elVar, 1, fbVar.d);
            }
            if (fbVar.e != null) {
                eu.c.a(elVar, 2, fbVar.e);
            }
            if (fbVar.f != null) {
                fh.c.a(elVar, 3, fbVar.f);
            }
            elVar.a(fbVar.a());
        }

        b() {
            super(eg.LENGTH_DELIMITED, fb.class);
        }

        public final /* synthetic */ Object a(ek ekVar) {
            a aVar = new a();
            long a = ekVar.a();
            while (true) {
                int b = ekVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (fa) fa.c.a(ekVar);
                            break;
                        case 2:
                            aVar.d = (eu) eu.c.a(ekVar);
                            break;
                        case 3:
                            aVar.e = (fh) fh.c.a(ekVar);
                            break;
                        default:
                            eg c = ekVar.c();
                            aVar.a(b, c, c.a().a(ekVar));
                            break;
                    }
                } else {
                    ekVar.a(a);
                    return aVar.b();
                }
            }
        }
    }

    public fb(fa faVar, eu euVar, fh fhVar) {
        this(faVar, euVar, fhVar, iu.b);
    }

    public fb(fa faVar, eu euVar, fh fhVar, iu iuVar) {
        super(c, iuVar);
        this.d = faVar;
        this.e = euVar;
        this.f = fhVar;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fb)) {
            return false;
        }
        fb fbVar = (fb) obj;
        return a().equals(fbVar.a()) && eo.a((Object) this.d, (Object) fbVar.d) && eo.a((Object) this.e, (Object) fbVar.e) && eo.a((Object) this.f, (Object) fbVar.f);
    }

    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        int hashCode = ((((a().hashCode() * 37) + (this.d != null ? this.d.hashCode() : 0)) * 37) + (this.e != null ? this.e.hashCode() : 0)) * 37;
        if (this.f != null) {
            i2 = this.f.hashCode();
        }
        int i3 = hashCode + i2;
        this.b = i3;
        return i3;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.d != null) {
            sb.append(", info=");
            sb.append(this.d);
        }
        if (this.e != null) {
            sb.append(", app=");
            sb.append(this.e);
        }
        if (this.f != null) {
            sb.append(", user=");
            sb.append(this.f);
        }
        StringBuilder replace = sb.replace(0, 2, "InfoSet{");
        replace.append('}');
        return replace.toString();
    }
}
