package com.tapjoy.internal;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.EOFException;
import java.nio.charset.Charset;

public final class ir implements is, it, Cloneable {
    private static final byte[] c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    iy a;
    long b;

    public final is a() {
        return this;
    }

    public final void close() {
    }

    public final void flush() {
    }

    public final boolean b() {
        return this.b == 0;
    }

    public final void a(long j) {
        if (this.b < j) {
            throw new EOFException();
        }
    }

    public final byte c() {
        if (this.b != 0) {
            iy iyVar = this.a;
            int i = iyVar.b;
            int i2 = iyVar.c;
            int i3 = i + 1;
            byte b2 = iyVar.a[i];
            this.b--;
            if (i3 == i2) {
                this.a = iyVar.a();
                iz.a(iyVar);
            } else {
                iyVar.b = i3;
            }
            return b2;
        }
        throw new IllegalStateException("size == 0");
    }

    public final int d() {
        if (this.b >= 4) {
            iy iyVar = this.a;
            int i = iyVar.b;
            int i2 = iyVar.c;
            if (i2 - i < 4) {
                return ((c() & 255) << 24) | ((c() & 255) << 16) | ((c() & 255) << 8) | (c() & 255);
            }
            byte[] bArr = iyVar.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
            int i5 = i4 + 1;
            byte b3 = b2 | ((bArr[i4] & 255) << 8);
            int i6 = i5 + 1;
            byte b4 = b3 | (bArr[i5] & 255);
            this.b -= 4;
            if (i6 == i2) {
                this.a = iyVar.a();
                iz.a(iyVar);
            } else {
                iyVar.b = i6;
            }
            return b4;
        }
        StringBuilder sb = new StringBuilder("size < 4: ");
        sb.append(this.b);
        throw new IllegalStateException(sb.toString());
    }

    public final int e() {
        return je.a(d());
    }

    public final iu b(long j) {
        return new iu(g(j));
    }

    public final String c(long j) {
        Charset charset = je.a;
        je.a(this.b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            StringBuilder sb = new StringBuilder("byteCount > Integer.MAX_VALUE: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (j == 0) {
            return "";
        } else {
            iy iyVar = this.a;
            if (((long) iyVar.b) + j > ((long) iyVar.c)) {
                return new String(g(j), charset);
            }
            String str = new String(iyVar.a, iyVar.b, (int) j, charset);
            iyVar.b = (int) (((long) iyVar.b) + j);
            this.b -= j;
            if (iyVar.b == iyVar.c) {
                this.a = iyVar.a();
                iz.a(iyVar);
            }
            return str;
        }
    }

    public final byte[] g() {
        try {
            return g(this.b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    private byte[] g(long j) {
        je.a(this.b, 0, j);
        if (j <= 2147483647L) {
            byte[] bArr = new byte[((int) j)];
            a(bArr);
            return bArr;
        }
        StringBuilder sb = new StringBuilder("byteCount > Integer.MAX_VALUE: ");
        sb.append(j);
        throw new IllegalArgumentException(sb.toString());
    }

    private void a(byte[] bArr) {
        int i;
        int i2 = 0;
        while (i2 < bArr.length) {
            int length = bArr.length - i2;
            je.a((long) bArr.length, (long) i2, (long) length);
            iy iyVar = this.a;
            if (iyVar == null) {
                i = -1;
            } else {
                i = Math.min(length, iyVar.c - iyVar.b);
                System.arraycopy(iyVar.a, iyVar.b, bArr, i2, i);
                iyVar.b += i;
                this.b -= (long) i;
                if (iyVar.b == iyVar.c) {
                    this.a = iyVar.a();
                    iz.a(iyVar);
                }
            }
            if (i != -1) {
                i2 += i;
            } else {
                throw new EOFException();
            }
        }
    }

    public final void d(long j) {
        while (j > 0) {
            if (this.a != null) {
                int min = (int) Math.min(j, (long) (this.a.c - this.a.b));
                long j2 = (long) min;
                this.b -= j2;
                j -= j2;
                this.a.b += min;
                if (this.a.b == this.a.c) {
                    iy iyVar = this.a;
                    this.a = iyVar.a();
                    iz.a(iyVar);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    /* renamed from: a */
    public final ir b(iu iuVar) {
        if (iuVar != null) {
            iuVar.a(this);
            return this;
        }
        throw new IllegalArgumentException("byteString == null");
    }

    /* renamed from: a */
    public final ir b(String str) {
        char c2;
        int length = str.length();
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (length < 0) {
            StringBuilder sb = new StringBuilder("endIndex < beginIndex: ");
            sb.append(length);
            sb.append(" < 0");
            throw new IllegalArgumentException(sb.toString());
        } else if (length <= str.length()) {
            int i = 0;
            while (i < length) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    iy c3 = c(1);
                    byte[] bArr = c3.a;
                    int i2 = c3.c - i;
                    int min = Math.min(length, 8192 - i2);
                    int i3 = i + 1;
                    bArr[i + i2] = (byte) charAt;
                    while (true) {
                        i = i3;
                        if (i >= min) {
                            break;
                        }
                        char charAt2 = str.charAt(i);
                        if (charAt2 >= 128) {
                            break;
                        }
                        i3 = i + 1;
                        bArr[i + i2] = (byte) charAt2;
                    }
                    int i4 = (i2 + i) - c3.c;
                    c3.c += i4;
                    this.b += (long) i4;
                } else if (charAt < 2048) {
                    e((charAt >> 6) | PsExtractor.AUDIO_STREAM);
                    e((int) (charAt & '?') | 128);
                    i++;
                } else if (charAt < 55296 || charAt > 57343) {
                    e((charAt >> 12) | 224);
                    e(((charAt >> 6) & 63) | 128);
                    e((int) (charAt & '?') | 128);
                    i++;
                } else {
                    int i5 = i + 1;
                    if (i5 < length) {
                        c2 = str.charAt(i5);
                    } else {
                        c2 = 0;
                    }
                    if (charAt > 56319 || c2 < 56320 || c2 > 57343) {
                        e(63);
                        i = i5;
                    } else {
                        int i6 = (((charAt & 10239) << 10) | (9215 & c2)) + 0;
                        e((i6 >> 18) | PsExtractor.VIDEO_STREAM_MASK);
                        e(((i6 >> 12) & 63) | 128);
                        e(((i6 >> 6) & 63) | 128);
                        e((i6 & 63) | 128);
                        i += 2;
                    }
                }
            }
            return this;
        } else {
            StringBuilder sb2 = new StringBuilder("endIndex > string.length: ");
            sb2.append(length);
            sb2.append(" > ");
            sb2.append(str.length());
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public final ir a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            long j = (long) i2;
            je.a((long) bArr.length, 0, j);
            int i3 = i2 + 0;
            while (i < i3) {
                iy c2 = c(1);
                int min = Math.min(i3 - i, 8192 - c2.c);
                System.arraycopy(bArr, i, c2.a, c2.c, min);
                i += min;
                c2.c += min;
            }
            this.b += j;
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: a */
    public final ir e(int i) {
        iy c2 = c(1);
        byte[] bArr = c2.a;
        int i2 = c2.c;
        c2.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.b++;
        return this;
    }

    /* renamed from: b */
    public final ir d(int i) {
        int a2 = je.a(i);
        iy c2 = c(4);
        byte[] bArr = c2.a;
        int i2 = c2.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((a2 >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((a2 >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((a2 >>> 8) & 255);
        int i6 = i5 + 1;
        bArr[i5] = (byte) (a2 & 255);
        c2.c = i6;
        this.b += 4;
        return this;
    }

    /* renamed from: e */
    public final ir f(long j) {
        long a2 = je.a(j);
        iy c2 = c(8);
        byte[] bArr = c2.a;
        int i = c2.c;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) ((a2 >>> 56) & 255));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) ((a2 >>> 48) & 255));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) ((a2 >>> 40) & 255));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) ((a2 >>> 32) & 255));
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) ((a2 >>> 24) & 255));
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) ((a2 >>> 16) & 255));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) ((a2 >>> 8) & 255));
        int i9 = i8 + 1;
        bArr[i8] = (byte) ((int) (a2 & 255));
        c2.c = i9;
        this.b += 8;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public final iy c(int i) {
        if (i <= 0 || i > 8192) {
            throw new IllegalArgumentException();
        } else if (this.a == null) {
            this.a = iz.a();
            iy iyVar = this.a;
            iy iyVar2 = this.a;
            iy iyVar3 = this.a;
            iyVar2.g = iyVar3;
            iyVar.f = iyVar3;
            return iyVar3;
        } else {
            iy iyVar4 = this.a.g;
            if (iyVar4.c + i > 8192 || !iyVar4.e) {
                iyVar4 = iyVar4.a(iz.a());
            }
            return iyVar4;
        }
    }

    public final void a(ir irVar, long j) {
        iy iyVar;
        if (irVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (irVar != this) {
            je.a(irVar.b, 0, j);
            while (j > 0) {
                int i = 0;
                if (j < ((long) (irVar.a.c - irVar.a.b))) {
                    iy iyVar2 = this.a != null ? this.a.g : null;
                    if (iyVar2 != null && iyVar2.e) {
                        if ((((long) iyVar2.c) + j) - ((long) (iyVar2.d ? 0 : iyVar2.b)) <= PlaybackStateCompat.ACTION_PLAY_FROM_URI) {
                            irVar.a.a(iyVar2, (int) j);
                            irVar.b -= j;
                            this.b += j;
                            return;
                        }
                    }
                    iy iyVar3 = irVar.a;
                    int i2 = (int) j;
                    if (i2 <= 0 || i2 > iyVar3.c - iyVar3.b) {
                        throw new IllegalArgumentException();
                    }
                    if (i2 >= 1024) {
                        iyVar = new iy(iyVar3);
                    } else {
                        iyVar = iz.a();
                        System.arraycopy(iyVar3.a, iyVar3.b, iyVar.a, 0, i2);
                    }
                    iyVar.c = iyVar.b + i2;
                    iyVar3.b += i2;
                    iyVar3.g.a(iyVar);
                    irVar.a = iyVar;
                }
                iy iyVar4 = irVar.a;
                long j2 = (long) (iyVar4.c - iyVar4.b);
                irVar.a = iyVar4.a();
                if (this.a == null) {
                    this.a = iyVar4;
                    iy iyVar5 = this.a;
                    iy iyVar6 = this.a;
                    iy iyVar7 = this.a;
                    iyVar6.g = iyVar7;
                    iyVar5.f = iyVar7;
                } else {
                    iy a2 = this.a.g.a(iyVar4);
                    if (a2.g == a2) {
                        throw new IllegalStateException();
                    } else if (a2.g.e) {
                        int i3 = a2.c - a2.b;
                        int i4 = 8192 - a2.g.c;
                        if (!a2.g.d) {
                            i = a2.g.b;
                        }
                        if (i3 <= i4 + i) {
                            a2.a(a2.g, i3);
                            a2.a();
                            iz.a(a2);
                        }
                    }
                }
                irVar.b -= j2;
                this.b += j2;
                j -= j2;
            }
        } else {
            throw new IllegalArgumentException("source == this");
        }
    }

    public final long b(ir irVar, long j) {
        if (irVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            StringBuilder sb = new StringBuilder("byteCount < 0: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (this.b == 0) {
            return -1;
        } else {
            if (j > this.b) {
                j = this.b;
            }
            irVar.a(this, j);
            return j;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ir)) {
            return false;
        }
        ir irVar = (ir) obj;
        if (this.b != irVar.b) {
            return false;
        }
        long j = 0;
        if (this.b == 0) {
            return true;
        }
        iy iyVar = this.a;
        iy iyVar2 = irVar.a;
        int i = iyVar.b;
        int i2 = iyVar2.b;
        while (j < this.b) {
            long min = (long) Math.min(iyVar.c - i, iyVar2.c - i2);
            int i3 = i2;
            int i4 = i;
            int i5 = 0;
            while (((long) i5) < min) {
                int i6 = i4 + 1;
                int i7 = i3 + 1;
                if (iyVar.a[i4] != iyVar2.a[i3]) {
                    return false;
                }
                i5++;
                i4 = i6;
                i3 = i7;
            }
            if (i4 == iyVar.c) {
                iyVar = iyVar.f;
                i = iyVar.b;
            } else {
                i = i4;
            }
            if (i3 == iyVar2.c) {
                iyVar2 = iyVar2.f;
                i2 = iyVar2.b;
            } else {
                i2 = i3;
            }
            j += min;
        }
        return true;
    }

    public final int hashCode() {
        iy iyVar = this.a;
        if (iyVar == null) {
            return 0;
        }
        int i = 1;
        do {
            for (int i2 = iyVar.b; i2 < iyVar.c; i2++) {
                i = (i * 31) + iyVar.a[i2];
            }
            iyVar = iyVar.f;
        } while (iyVar != this.a);
        return i;
    }

    /* renamed from: h */
    public final ir clone() {
        ir irVar = new ir();
        if (this.b == 0) {
            return irVar;
        }
        irVar.a = new iy(this.a);
        iy iyVar = irVar.a;
        iy iyVar2 = irVar.a;
        iy iyVar3 = irVar.a;
        iyVar2.g = iyVar3;
        iyVar.f = iyVar3;
        iy iyVar4 = this.a;
        while (true) {
            iyVar4 = iyVar4.f;
            if (iyVar4 != this.a) {
                irVar.a.g.a(new iy(iyVar4));
            } else {
                irVar.b = this.b;
                return irVar;
            }
        }
    }

    public final long f() {
        long j;
        if (this.b >= 8) {
            iy iyVar = this.a;
            int i = iyVar.b;
            int i2 = iyVar.c;
            if (i2 - i < 8) {
                j = ((((long) d()) & 4294967295L) << 32) | (4294967295L & ((long) d()));
            } else {
                byte[] bArr = iyVar.a;
                int i3 = i + 1;
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                int i6 = i5 + 1;
                int i7 = i6 + 1;
                int i8 = i7 + 1;
                long j2 = ((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i3]) & 255) << 48) | ((((long) bArr[i4]) & 255) << 40) | ((((long) bArr[i5]) & 255) << 32) | ((((long) bArr[i6]) & 255) << 24) | ((((long) bArr[i7]) & 255) << 16);
                int i9 = i8 + 1;
                long j3 = ((((long) bArr[i8]) & 255) << 8) | j2;
                int i10 = i9 + 1;
                long j4 = (((long) bArr[i9]) & 255) | j3;
                this.b -= 8;
                if (i10 == i2) {
                    this.a = iyVar.a();
                    iz.a(iyVar);
                } else {
                    iyVar.b = i10;
                }
                j = j4;
            }
            return je.a(j);
        }
        StringBuilder sb = new StringBuilder("size < 8: ");
        sb.append(this.b);
        throw new IllegalStateException(sb.toString());
    }

    public final String toString() {
        iu iuVar;
        if (this.b <= 2147483647L) {
            int i = (int) this.b;
            if (i == 0) {
                iuVar = iu.b;
            } else {
                iuVar = new ja(this, i);
            }
            return iuVar.toString();
        }
        StringBuilder sb = new StringBuilder("size > Integer.MAX_VALUE: ");
        sb.append(this.b);
        throw new IllegalArgumentException(sb.toString());
    }
}
