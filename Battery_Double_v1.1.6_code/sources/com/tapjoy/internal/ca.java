package com.tapjoy.internal;

import java.io.InputStream;
import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import javax.annotation.Nullable;

public abstract class ca {
    public static ExecutorService a;
    public static cd b;
    private Future c;

    public abstract Object a(URI uri, InputStream inputStream);

    public abstract String b();

    public abstract String c();

    public String d() {
        return null;
    }

    public Map a() {
        return Collections.emptyMap();
    }

    public Map e() {
        return new LinkedHashMap();
    }

    public Object f() {
        return b.a(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0015  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0022 A[SYNTHETIC, Splitter:B:14:0x0022] */
    public final synchronized void a(@Nullable cf cfVar, ExecutorService executorService) {
        boolean z;
        if (this.c != null) {
            if (!this.c.isDone()) {
                z = false;
                String str = "Call has not completed";
                if (!z) {
                    this.c = executorService.submit(new cc(this, cfVar));
                } else {
                    throw new IllegalStateException(String.valueOf(str));
                }
            }
        }
        z = true;
        String str2 = "Call has not completed";
        if (!z) {
        }
    }
}
