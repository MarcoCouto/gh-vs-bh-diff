package com.tapjoy.internal;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;

public class j extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        a(context, intent);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|(3:11|12|(5:14|15|16|17|28)(1:26))(4:18|19|20|29)|24|7|6) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0021 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0021 A[LOOP:0: B:6:0x0021->B:24:0x0021, LOOP_START, PHI: r3 
  PHI: (r3v2 int) = (r3v1 int), (r3v3 int) binds: [B:5:0x0020, B:24:0x0021] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:6:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0027 A[Catch:{ NameNotFoundException -> 0x005a }] */
    public final int a(Context context, Intent intent) {
        try {
            Bundle bundle = context.getPackageManager().getReceiverInfo(new ComponentName(context, getClass()), 128).metaData;
            if (bundle == null) {
                return 0;
            }
            int i = 0;
            for (String str : bundle.keySet()) {
                String string = bundle.getString(str);
                if (string != null) {
                    Object newInstance = Class.forName(string).newInstance();
                    if (newInstance instanceof BroadcastReceiver) {
                        BroadcastReceiver broadcastReceiver = (BroadcastReceiver) newInstance;
                        Intent intent2 = new Intent(intent);
                        intent2.setComponent(new ComponentName(context, string));
                        broadcastReceiver.onReceive(context, intent2);
                        i++;
                    }
                } else {
                    new Object[1][0] = str;
                }
            }
            return i;
        } catch (NameNotFoundException unused) {
            return 0;
        }
    }
}
