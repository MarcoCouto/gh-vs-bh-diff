package com.tapjoy.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@ft
public final class PluginSupport {
    private PluginSupport() {
    }

    @ft
    public static void trackUsage(String str, String str2, String str3) {
        TreeMap treeMap;
        bn b;
        bn b2;
        try {
            HashMap hashMap = null;
            if (!al.a(str2)) {
                treeMap = new TreeMap();
                b2 = bn.b(str2);
                b2.a((Map) treeMap);
                b2.close();
            } else {
                treeMap = null;
            }
            if (!al.a(str3)) {
                hashMap = new HashMap();
                b = bn.b(str3);
                b.h();
                while (b.j()) {
                    hashMap.put(b.l(), Long.valueOf(b.q()));
                }
                b.i();
                b.close();
            }
            gf.a(str, treeMap, (Map) hashMap);
        } catch (Exception unused) {
        } catch (Throwable th) {
            b2.close();
            throw th;
        }
    }
}
