package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class bt implements Closeable {
    final Writer a;
    private final List b = new ArrayList();
    private String c;
    private String d;
    private boolean e;

    public bt(Writer writer) {
        this.b.add(bq.EMPTY_DOCUMENT);
        this.d = ":";
        if (writer != null) {
            this.a = writer;
            return;
        }
        throw new NullPointerException("out == null");
    }

    public final bt a() {
        return a(bq.EMPTY_ARRAY, RequestParameters.LEFT_BRACKETS);
    }

    public final bt b() {
        return a(bq.EMPTY_ARRAY, bq.NONEMPTY_ARRAY, RequestParameters.RIGHT_BRACKETS);
    }

    public final bt c() {
        return a(bq.EMPTY_OBJECT, "{");
    }

    public final bt d() {
        return a(bq.EMPTY_OBJECT, bq.NONEMPTY_OBJECT, "}");
    }

    private bt a(bq bqVar, String str) {
        a(true);
        this.b.add(bqVar);
        this.a.write(str);
        return this;
    }

    private bt a(bq bqVar, bq bqVar2, String str) {
        bq e2 = e();
        if (e2 == bqVar2 || e2 == bqVar) {
            this.b.remove(this.b.size() - 1);
            if (e2 == bqVar2) {
                g();
            }
            this.a.write(str);
            return this;
        }
        StringBuilder sb = new StringBuilder("Nesting problem: ");
        sb.append(this.b);
        throw new IllegalStateException(sb.toString());
    }

    private bq e() {
        return (bq) this.b.get(this.b.size() - 1);
    }

    private void a(bq bqVar) {
        this.b.set(this.b.size() - 1, bqVar);
    }

    public final bt b(String str) {
        if (str == null) {
            return f();
        }
        a(false);
        c(str);
        return this;
    }

    private bt f() {
        a(false);
        this.a.write("null");
        return this;
    }

    public final bt a(long j) {
        a(false);
        this.a.write(Long.toString(j));
        return this;
    }

    public final bt a(Number number) {
        if (number == null) {
            return f();
        }
        String obj = number.toString();
        if (this.e || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            a(false);
            this.a.append(obj);
            return this;
        }
        StringBuilder sb = new StringBuilder("Numeric values must be finite, but was ");
        sb.append(number);
        throw new IllegalArgumentException(sb.toString());
    }

    public final void close() {
        this.a.close();
        if (e() != bq.NONEMPTY_DOCUMENT) {
            throw new IOException("Incomplete document");
        }
    }

    private void c(String str) {
        this.a.write("\"");
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    this.a.write("\\b");
                    continue;
                case 9:
                    this.a.write("\\t");
                    continue;
                case 10:
                    this.a.write("\\n");
                    continue;
                case 12:
                    this.a.write("\\f");
                    continue;
                case 13:
                    this.a.write("\\r");
                    continue;
                case '\"':
                case '\\':
                    this.a.write(92);
                    break;
                case 8232:
                case 8233:
                    this.a.write(String.format("\\u%04x", new Object[]{Integer.valueOf(charAt)}));
                    continue;
                default:
                    if (charAt <= 31) {
                        this.a.write(String.format("\\u%04x", new Object[]{Integer.valueOf(charAt)}));
                        continue;
                    }
                    break;
            }
            this.a.write(charAt);
        }
        this.a.write("\"");
    }

    private void g() {
        if (this.c != null) {
            this.a.write("\n");
            for (int i = 1; i < this.b.size(); i++) {
                this.a.write(this.c);
            }
        }
    }

    private void a(boolean z) {
        switch (e()) {
            case EMPTY_DOCUMENT:
                if (this.e || z) {
                    a(bq.NONEMPTY_DOCUMENT);
                    return;
                }
                throw new IllegalStateException("JSON must start with an array or an object.");
            case EMPTY_ARRAY:
                a(bq.NONEMPTY_ARRAY);
                g();
                return;
            case NONEMPTY_ARRAY:
                this.a.append(',');
                g();
                return;
            case DANGLING_NAME:
                this.a.append(this.d);
                a(bq.NONEMPTY_OBJECT);
                return;
            case NONEMPTY_DOCUMENT:
                throw new IllegalStateException("JSON must have only one top-level value.");
            default:
                StringBuilder sb = new StringBuilder("Nesting problem: ");
                sb.append(this.b);
                throw new IllegalStateException(sb.toString());
        }
    }

    public final bt a(Object obj) {
        if (obj == null) {
            return f();
        }
        if (obj instanceof br) {
            if (this.b.size() == this.b.size()) {
                return this;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(obj.getClass().getName());
            sb.append(".writeToJson(JsonWriter) wrote incomplete value");
            throw new IllegalStateException(sb.toString());
        } else if (obj instanceof Boolean) {
            boolean booleanValue = ((Boolean) obj).booleanValue();
            a(false);
            this.a.write(booleanValue ? "true" : "false");
            return this;
        } else if (obj instanceof Number) {
            if (obj instanceof Long) {
                return a(((Number) obj).longValue());
            }
            if (!(obj instanceof Double)) {
                return a((Number) obj);
            }
            double doubleValue = ((Number) obj).doubleValue();
            if (this.e || (!Double.isNaN(doubleValue) && !Double.isInfinite(doubleValue))) {
                a(false);
                this.a.append(Double.toString(doubleValue));
                return this;
            }
            StringBuilder sb2 = new StringBuilder("Numeric values must be finite, but was ");
            sb2.append(doubleValue);
            throw new IllegalArgumentException(sb2.toString());
        } else if (obj instanceof String) {
            return b((String) obj);
        } else {
            if (obj instanceof bl) {
                return a((bl) obj);
            }
            if (obj instanceof Collection) {
                return a((Collection) obj);
            }
            if (obj instanceof Map) {
                return a((Map) obj);
            }
            if (obj instanceof Date) {
                Date date = (Date) obj;
                if (date == null) {
                    return f();
                }
                return b(w.a(date));
            } else if (obj instanceof Object[]) {
                return a((Object[]) obj);
            } else {
                StringBuilder sb3 = new StringBuilder("Unknown type: ");
                sb3.append(obj.getClass().getName());
                throw new IllegalArgumentException(sb3.toString());
            }
        }
    }

    private bt a(Object[] objArr) {
        if (objArr == null) {
            return f();
        }
        a();
        for (Object a2 : objArr) {
            a(a2);
        }
        b();
        return this;
    }

    public final bt a(bl blVar) {
        a(false);
        blVar.a(this.a);
        return this;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Collection, code=java.util.Collection<java.lang.Object>, for r2v0, types: [java.util.Collection<java.lang.Object>, java.util.Collection] */
    public final bt a(Collection<Object> collection) {
        if (collection == null) {
            return f();
        }
        a();
        for (Object a2 : collection) {
            a(a2);
        }
        b();
        return this;
    }

    public final bt a(String str) {
        if (str != null) {
            bq e2 = e();
            if (e2 == bq.NONEMPTY_OBJECT) {
                this.a.write(44);
            } else if (e2 != bq.EMPTY_OBJECT) {
                StringBuilder sb = new StringBuilder("Nesting problem: ");
                sb.append(this.b);
                throw new IllegalStateException(sb.toString());
            }
            g();
            a(bq.DANGLING_NAME);
            c(str);
            return this;
        }
        throw new NullPointerException("name == null");
    }

    public final bt a(Map map) {
        if (map == null) {
            return f();
        }
        c();
        for (Entry entry : map.entrySet()) {
            a(String.valueOf(entry.getKey()));
            a(entry.getValue());
        }
        d();
        return this;
    }
}
