package com.tapjoy.internal;

public final class eu extends eh {
    public static final ej c = new b();
    public static final Integer d = Integer.valueOf(0);
    public final String e;
    public final Integer f;
    public final String g;
    public final String h;
    public final String i;

    public static final class a extends com.tapjoy.internal.eh.a {
        public String c;
        public Integer d;
        public String e;
        public String f;
        public String g;

        public final eu b() {
            eu euVar = new eu(this.c, this.d, this.e, this.f, this.g, super.a());
            return euVar;
        }
    }

    static final class b extends ej {
        public final /* synthetic */ int a(Object obj) {
            eu euVar = (eu) obj;
            int i = 0;
            int a = (euVar.e != null ? ej.p.a(1, (Object) euVar.e) : 0) + (euVar.f != null ? ej.d.a(2, (Object) euVar.f) : 0) + (euVar.g != null ? ej.p.a(3, (Object) euVar.g) : 0) + (euVar.h != null ? ej.p.a(4, (Object) euVar.h) : 0);
            if (euVar.i != null) {
                i = ej.p.a(5, (Object) euVar.i);
            }
            return a + i + euVar.a().c();
        }

        public final /* bridge */ /* synthetic */ void a(el elVar, Object obj) {
            eu euVar = (eu) obj;
            if (euVar.e != null) {
                ej.p.a(elVar, 1, euVar.e);
            }
            if (euVar.f != null) {
                ej.d.a(elVar, 2, euVar.f);
            }
            if (euVar.g != null) {
                ej.p.a(elVar, 3, euVar.g);
            }
            if (euVar.h != null) {
                ej.p.a(elVar, 4, euVar.h);
            }
            if (euVar.i != null) {
                ej.p.a(elVar, 5, euVar.i);
            }
            elVar.a(euVar.a());
        }

        b() {
            super(eg.LENGTH_DELIMITED, eu.class);
        }

        public final /* synthetic */ Object a(ek ekVar) {
            a aVar = new a();
            long a = ekVar.a();
            while (true) {
                int b = ekVar.b();
                if (b != -1) {
                    switch (b) {
                        case 1:
                            aVar.c = (String) ej.p.a(ekVar);
                            break;
                        case 2:
                            aVar.d = (Integer) ej.d.a(ekVar);
                            break;
                        case 3:
                            aVar.e = (String) ej.p.a(ekVar);
                            break;
                        case 4:
                            aVar.f = (String) ej.p.a(ekVar);
                            break;
                        case 5:
                            aVar.g = (String) ej.p.a(ekVar);
                            break;
                        default:
                            eg c = ekVar.c();
                            aVar.a(b, c, c.a().a(ekVar));
                            break;
                    }
                } else {
                    ekVar.a(a);
                    return aVar.b();
                }
            }
        }
    }

    public eu(String str, Integer num, String str2, String str3, String str4, iu iuVar) {
        super(c, iuVar);
        this.e = str;
        this.f = num;
        this.g = str2;
        this.h = str3;
        this.i = str4;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof eu)) {
            return false;
        }
        eu euVar = (eu) obj;
        return a().equals(euVar.a()) && eo.a((Object) this.e, (Object) euVar.e) && eo.a((Object) this.f, (Object) euVar.f) && eo.a((Object) this.g, (Object) euVar.g) && eo.a((Object) this.h, (Object) euVar.h) && eo.a((Object) this.i, (Object) euVar.i);
    }

    public final int hashCode() {
        int i2 = this.b;
        if (i2 != 0) {
            return i2;
        }
        int i3 = 0;
        int hashCode = ((((((((a().hashCode() * 37) + (this.e != null ? this.e.hashCode() : 0)) * 37) + (this.f != null ? this.f.hashCode() : 0)) * 37) + (this.g != null ? this.g.hashCode() : 0)) * 37) + (this.h != null ? this.h.hashCode() : 0)) * 37;
        if (this.i != null) {
            i3 = this.i.hashCode();
        }
        int i4 = hashCode + i3;
        this.b = i4;
        return i4;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.e != null) {
            sb.append(", pkgVer=");
            sb.append(this.e);
        }
        if (this.f != null) {
            sb.append(", pkgRev=");
            sb.append(this.f);
        }
        if (this.g != null) {
            sb.append(", dataVer=");
            sb.append(this.g);
        }
        if (this.h != null) {
            sb.append(", installer=");
            sb.append(this.h);
        }
        if (this.i != null) {
            sb.append(", store=");
            sb.append(this.i);
        }
        StringBuilder replace = sb.replace(0, 2, "App{");
        replace.append('}');
        return replace.toString();
    }
}
