package com.tapjoy.internal;

import com.tapjoy.internal.dy.a;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class dz implements a {
    private final BlockingQueue a = new LinkedBlockingQueue();
    private final ThreadPoolExecutor b;
    private final ArrayDeque c = new ArrayDeque();
    private dy d = null;

    public dz() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, this.a);
        this.b = threadPoolExecutor;
    }

    public final void a() {
        this.d = null;
        b();
    }

    public final void a(dy dyVar) {
        dyVar.d = this;
        this.c.add(dyVar);
        if (this.d == null) {
            b();
        }
    }

    private void b() {
        this.d = (dy) this.c.poll();
        if (this.d != null) {
            this.d.executeOnExecutor(this.b, new Object[0]);
        }
    }
}
