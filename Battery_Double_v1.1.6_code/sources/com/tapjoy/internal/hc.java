package com.tapjoy.internal;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Rect;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.tapjoy.TapjoyConnectFlag;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.internal.fh.a;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import javax.annotation.Nullable;

public final class hc {
    public static final String a = UUID.randomUUID().toString();
    private static hc d;
    public final a b = new a();
    public final hj c;
    private final fa.a e = new fa.a();
    private final eu.a f = new eu.a();
    private final Context g;

    public static synchronized hc a(Context context) {
        hc hcVar;
        synchronized (hc.class) {
            if (d == null) {
                d = new hc(context, hj.a(context));
            }
            hcVar = d;
        }
        return hcVar;
    }

    private hc(Context context, hj hjVar) {
        hn.a();
        Context applicationContext = context.getApplicationContext();
        this.g = applicationContext;
        fa.a aVar = this.e;
        SharedPreferences sharedPreferences = applicationContext.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0);
        File file = new File(gz.c(applicationContext), "deviceid");
        String string = sharedPreferences.getString(TapjoyConstants.PREF_ANALYTICS_ID, null);
        if (jq.c(string)) {
            String b2 = file.exists() ? jq.b(bg.a(file)) : null;
            if (b2 == null) {
                b2 = UUID.randomUUID().toString();
            }
            string = b2;
            Editor edit = sharedPreferences.edit();
            edit.putString(TapjoyConstants.PREF_ANALYTICS_ID, string);
            edit.apply();
        }
        aVar.d = string;
        if (!ga.b().a(TapjoyConnectFlag.DISABLE_ANDROID_ID_AS_ANALYTICS_ID, true)) {
            fa.a aVar2 = this.e;
            String string2 = Secure.getString(applicationContext.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            aVar2.t = !"9774d56d682e549c".equals(string2) ? jq.b(string2) : null;
        }
        PackageManager packageManager = applicationContext.getPackageManager();
        TelephonyManager telephonyManager = (TelephonyManager) applicationContext.getSystemService(PlaceFields.PHONE);
        if (telephonyManager != null) {
            String simCountryIso = telephonyManager.getSimCountryIso();
            if (!jq.c(simCountryIso)) {
                this.e.q = simCountryIso.toUpperCase(Locale.US);
            }
            String networkCountryIso = telephonyManager.getNetworkCountryIso();
            if (!jq.c(networkCountryIso)) {
                this.e.r = networkCountryIso.toUpperCase(Locale.US);
            }
        }
        String packageName = applicationContext.getPackageName();
        this.e.n = packageName;
        fa.a aVar3 = this.e;
        Signature[] e2 = z.e(packageManager, packageName);
        aVar3.o = jq.a((e2 == null || e2.length <= 0) ? null : Base64.encodeToString(ch.a(e2[0].toByteArray()), 2));
        this.f.c = z.a(packageManager, packageName);
        this.f.d = Integer.valueOf(z.b(packageManager, packageName));
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        if (!jq.c(installerPackageName)) {
            this.f.f = installerPackageName;
        }
        String a2 = a(packageManager, packageName);
        if (!jq.c(a2)) {
            this.f.g = a2;
        }
        a();
        this.c = hjVar;
        String a3 = this.c.c.a();
        if (a3 != null && a3.length() > 0) {
            fa.a aVar4 = this.e;
            StringBuilder sb = new StringBuilder();
            sb.append(a3);
            sb.append(" 12.4.2/Android");
            aVar4.p = sb.toString();
        }
        String b3 = this.c.b();
        if (b3 != null) {
            this.b.d = b3;
        }
        a aVar5 = this.b;
        hj hjVar2 = this.c;
        long j = hjVar2.b.getLong("it", 0);
        if (j == 0) {
            Context context2 = hjVar2.a;
            j = z.c(context2.getPackageManager(), context2.getPackageName());
            if (j == 0) {
                j = gz.d(hjVar2.a).lastModified();
                if (j == 0) {
                    Context context3 = hjVar2.a;
                    j = new File(z.d(context3.getPackageManager(), context3.getPackageName())).lastModified();
                    if (j == 0) {
                        j = System.currentTimeMillis();
                    }
                }
            }
            hjVar2.b.edit().putLong("it", j).apply();
        }
        aVar5.c = Long.valueOf(j);
        int b4 = this.c.f.b();
        this.b.e = Integer.valueOf(a(7, b4));
        this.b.f = Integer.valueOf(a(30, b4));
        int b5 = this.c.h.b();
        if (b5 > 0) {
            this.b.h = Integer.valueOf(b5);
        }
        long a4 = this.c.i.a();
        if (a4 > 0) {
            this.b.i = Long.valueOf(a4);
        }
        long a5 = this.c.j.a();
        if (a5 > 0) {
            this.b.j = Long.valueOf(a5);
        }
        long a6 = this.c.k.a();
        if (a6 > 0) {
            this.b.k = Long.valueOf(a6);
        }
        String a7 = this.c.l.a();
        if (a7 != null) {
            this.b.l = a7;
        }
        int b6 = this.c.m.b();
        if (b6 > 0) {
            this.b.m = Integer.valueOf(b6);
        }
        double a8 = this.c.n.a();
        if (a8 != Utils.DOUBLE_EPSILON) {
            this.b.n = Double.valueOf(a8);
        }
        long a9 = this.c.o.a();
        if (a9 > 0) {
            this.b.o = Long.valueOf(a9);
        }
        double a10 = this.c.p.a();
        if (a10 != Utils.DOUBLE_EPSILON) {
            this.b.p = Double.valueOf(a10);
        }
        String a11 = this.c.g.a();
        if (a11 != null) {
            try {
                ff ffVar = (ff) ff.c.a(Base64.decode(a11, 2));
                this.b.g.clear();
                this.b.g.addAll(ffVar.d);
            } catch (IllegalArgumentException unused) {
                this.c.g.c();
            } catch (IOException unused2) {
                this.c.g.c();
            }
        }
        this.f.e = this.c.q.a();
        this.b.s = this.c.r.a();
        int intValue = this.c.s.a().intValue();
        this.b.t = intValue != -1 ? Integer.valueOf(intValue) : null;
        int intValue2 = this.c.t.a().intValue();
        this.b.u = intValue2 != -1 ? Integer.valueOf(intValue2) : null;
        this.b.v = this.c.u.a();
        this.b.w = this.c.v.a();
        this.b.x = this.c.w.a();
        this.b.y = this.c.x.a();
        this.b.z = this.c.y.a();
        String a12 = this.c.z.a();
        if (a12 != null) {
            try {
                fg fgVar = (fg) fg.c.a(Base64.decode(a12, 2));
                this.b.A.clear();
                this.b.A.addAll(fgVar.d);
            } catch (IllegalArgumentException unused3) {
                this.c.z.c();
            } catch (IOException unused4) {
                this.c.z.c();
            }
        }
        String a13 = this.c.A.a();
        boolean booleanValue = this.c.B.a().booleanValue();
        if (a13 != null) {
            this.b.q = a13;
            this.b.r = Boolean.valueOf(booleanValue);
        } else {
            this.b.q = null;
            this.b.r = null;
        }
        this.b.B = this.c.C.a();
    }

    private static String a(PackageManager packageManager, String str) {
        try {
            Bundle bundle = packageManager.getApplicationInfo(str, 128).metaData;
            if (bundle != null) {
                Object obj = bundle.get("com.tapjoy.appstore");
                if (obj != null) {
                    return obj.toString().trim();
                }
            }
        } catch (NameNotFoundException unused) {
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0057 */
    public final void a() {
        synchronized (this) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) this.g.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            Activity a2 = gr.a();
            if (a2 != null) {
                Window window = a2.getWindow();
                if (window != null) {
                    int i = displayMetrics.heightPixels;
                    Rect rect = new Rect();
                    window.getDecorView().getWindowVisibleDisplayFrame(rect);
                    displayMetrics.heightPixels = i - rect.top;
                }
            }
            this.e.i = Integer.valueOf(displayMetrics.densityDpi);
            this.e.j = Integer.valueOf(displayMetrics.widthPixels);
            this.e.k = Integer.valueOf(displayMetrics.heightPixels);
        }
    }

    public final fb b() {
        fb fbVar;
        synchronized (this) {
            this.e.l = Locale.getDefault().toString();
            this.e.m = TimeZone.getDefault().getID();
            boolean z = false;
            long currentTimeMillis = System.currentTimeMillis() - 259200000;
            Iterator it = this.b.g.iterator();
            while (it.hasNext()) {
                if (((fe) it.next()).g.longValue() <= currentTimeMillis) {
                    it.remove();
                    z = true;
                }
            }
            if (z) {
                g();
            }
            fbVar = new fb(this.e.b(), this.f.b(), this.b.b());
        }
        return fbVar;
    }

    /* access modifiers changed from: 0000 */
    public final String c() {
        String a2;
        synchronized (this) {
            a2 = this.c.d.a();
        }
        return a2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00dc  */
    @Nullable
    public final fc d() {
        fc fcVar;
        int i;
        long j;
        Calendar calendar;
        synchronized (this) {
            Calendar instance = Calendar.getInstance();
            int i2 = 1;
            int i3 = (instance.get(1) * 10000) + (instance.get(2) * 100) + 100 + instance.get(5);
            int intValue = this.c.e.a().intValue();
            if (intValue == i3) {
                return null;
            }
            if (intValue == 0) {
                this.b.e = Integer.valueOf(1);
                this.b.f = Integer.valueOf(1);
                fcVar = new fc("fq7_0_1", "fq30_0_1", null);
            } else {
                int intValue2 = this.c.f.a().intValue();
                int a2 = a(7, intValue2);
                int a3 = a(30, intValue2);
                Calendar instance2 = Calendar.getInstance();
                instance2.set(intValue / 10000, ((intValue / 100) % 100) - 1, intValue % 100);
                int signum = Integer.signum(instance.get(1) - instance2.get(1));
                if (signum == -1) {
                    calendar = (Calendar) instance2.clone();
                    calendar.set(instance.get(1), instance.get(2), instance.get(5));
                    j = instance2.getTimeInMillis();
                } else if (signum != 1) {
                    i = instance.get(6) - instance2.get(6);
                    int i4 = Math.abs(i) < 30 ? 0 : i >= 0 ? intValue2 << i : intValue2 >> (-i);
                    i2 = 1 | i4;
                    int a4 = a(7, i2);
                    int a5 = a(30, i2);
                    this.b.e = Integer.valueOf(a4);
                    this.b.f = Integer.valueOf(a5);
                    StringBuilder sb = new StringBuilder("fq7_");
                    sb.append(a2);
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(a4);
                    String sb2 = sb.toString();
                    StringBuilder sb3 = new StringBuilder("fq30_");
                    sb3.append(a3);
                    sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb3.append(a5);
                    fcVar = new fc(sb2, sb3.toString(), null);
                } else {
                    calendar = (Calendar) instance.clone();
                    calendar.set(instance2.get(1), instance2.get(2), instance2.get(5));
                    j = instance.getTimeInMillis();
                }
                i = 0;
                while (calendar.getTimeInMillis() < j) {
                    calendar.add(5, 1);
                    i++;
                }
                if (signum <= 0) {
                    i = -i;
                }
                if (Math.abs(i) < 30) {
                }
                i2 = 1 | i4;
                int a42 = a(7, i2);
                int a52 = a(30, i2);
                this.b.e = Integer.valueOf(a42);
                this.b.f = Integer.valueOf(a52);
                StringBuilder sb4 = new StringBuilder("fq7_");
                sb4.append(a2);
                sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                sb4.append(a42);
                String sb22 = sb4.toString();
                StringBuilder sb32 = new StringBuilder("fq30_");
                sb32.append(a3);
                sb32.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                sb32.append(a52);
                fcVar = new fc(sb22, sb32.toString(), null);
            }
            this.c.e.a(i3);
            this.c.f.a(i2);
            return fcVar;
        }
    }

    private static int a(int i, int i2) {
        return Integer.bitCount(((1 << i) - 1) & i2);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(String str, long j, boolean z) {
        synchronized (this) {
            int size = this.b.g.size();
            int i = 0;
            while (i < size) {
                fe feVar = (fe) this.b.g.get(i);
                if (!feVar.f.equals(str)) {
                    i++;
                } else if (!z) {
                    return false;
                } else {
                    fe.a b2 = feVar.b();
                    b2.d = Long.valueOf(j);
                    this.b.g.set(i, b2.b());
                    return true;
                }
            }
            this.b.g.add(new fe(str, Long.valueOf(j)));
            g();
            return true;
        }
    }

    private void g() {
        this.c.g.a(Base64.encodeToString(ff.c.b(new ff(this.b.g)), 2));
    }

    public final boolean a(String str) {
        boolean z;
        synchronized (this) {
            this.c.q.a(str);
            z = true;
            if (str != null) {
                z = true ^ jo.a(this.f.e, str);
                this.f.e = str;
            } else {
                if (this.f.e == null) {
                    z = false;
                }
                this.f.e = null;
            }
        }
        return z;
    }

    public final boolean b(String str) {
        boolean z;
        synchronized (this) {
            this.c.r.a(str);
            z = !jo.a(this.b.s, str);
            if (z) {
                this.b.s = str;
            }
        }
        return z;
    }

    public final boolean a(Integer num) {
        boolean z;
        synchronized (this) {
            this.c.s.a(num);
            z = !jo.a(this.b.t, num);
            if (z) {
                this.b.t = num;
            }
        }
        return z;
    }

    public final boolean b(Integer num) {
        boolean z;
        synchronized (this) {
            this.c.t.a(num);
            z = !jo.a(this.b.u, num);
            if (z) {
                this.b.u = num;
            }
        }
        return z;
    }

    public final boolean a(int i, String str) {
        boolean z;
        synchronized (this) {
            z = false;
            switch (i) {
                case 1:
                    this.c.u.a(str);
                    z = !jo.a(this.b.v, str);
                    if (z) {
                        this.b.v = str;
                        break;
                    }
                    break;
                case 2:
                    this.c.v.a(str);
                    z = !jo.a(this.b.w, str);
                    if (z) {
                        this.b.w = str;
                        break;
                    }
                    break;
                case 3:
                    this.c.w.a(str);
                    z = !jo.a(this.b.x, str);
                    if (z) {
                        this.b.x = str;
                        break;
                    }
                    break;
                case 4:
                    this.c.x.a(str);
                    z = !jo.a(this.b.y, str);
                    if (z) {
                        this.b.y = str;
                        break;
                    }
                    break;
                case 5:
                    this.c.y.a(str);
                    z = !jo.a(this.b.z, str);
                    if (z) {
                        this.b.z = str;
                        break;
                    }
                    break;
            }
        }
        return z;
    }

    public final Set e() {
        HashSet hashSet;
        synchronized (this) {
            hashSet = new HashSet(this.b.A);
        }
        return hashSet;
    }

    public final boolean c(String str) {
        synchronized (this) {
            for (int size = this.b.g.size() - 1; size >= 0; size--) {
                fe feVar = (fe) this.b.g.get(size);
                if (feVar.f.equals(str)) {
                    fe.a b2 = feVar.b();
                    b2.e = Long.valueOf(System.currentTimeMillis());
                    this.b.g.set(size, b2.b());
                    g();
                    return true;
                }
            }
            return false;
        }
    }

    public final boolean f() {
        return ((Boolean) jo.b(this.b.B, fh.r)).booleanValue();
    }

    public final boolean a(boolean z) {
        boolean z2;
        synchronized (this) {
            this.c.C.a(z);
            z2 = z != ((Boolean) jo.b(this.b.B, fh.r)).booleanValue();
            this.b.B = Boolean.valueOf(z);
        }
        return z2;
    }
}
