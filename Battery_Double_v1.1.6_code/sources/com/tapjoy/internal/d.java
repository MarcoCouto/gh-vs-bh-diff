package com.tapjoy.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tapjoy.TapjoyConstants;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.annotation.Nullable;

public final class d extends BroadcastReceiver {
    public final void onReceive(Context context, Intent intent) {
        String a = a(intent);
        if (a != null) {
            a(context, "install_referrer", a);
        }
    }

    @Nullable
    public static String a(Intent intent) {
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
            return intent.getStringExtra(TapjoyConstants.TJC_REFERRER);
        }
        return null;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[ExcHandler: FileNotFoundException (unused java.io.FileNotFoundException), SYNTHETIC, Splitter:B:1:0x0001] */
    private static boolean a(Context context, String str, String str2) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = context.openFileOutput(str, 0);
            bg.a((OutputStream) fileOutputStream, str2);
            fileOutputStream.close();
            return true;
        } catch (FileNotFoundException unused) {
        } catch (IOException unused2) {
            fileOutputStream = null;
            jz.a(fileOutputStream);
            context.deleteFile("install_referrer");
            return false;
        }
    }
}
