package com.tapjoy.internal;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJPlacementManager;
import com.tapjoy.internal.fs.a;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public final class fp extends gt implements Observer {
    private final Map b = new HashMap();
    private final fi c = new fi();
    private boolean d;
    private final fz e = new fz() {
        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ String a(Object obj) {
            return "AppLaunch";
        }

        /* access modifiers changed from: protected */
        public final boolean a() {
            return super.a() && !hk.c();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ TJPlacement a(Context context, TJPlacementListener tJPlacementListener, Object obj) {
            return TJPlacementManager.createPlacement(context, "AppLaunch", true, tJPlacementListener);
        }
    };

    public static void a() {
    }

    static {
        gt.a = new fp();
    }

    private fp() {
    }

    public final void update(Observable observable, Object obj) {
        a aVar = fs.d;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
        if (r3 == false) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005f, code lost:
        if (r5.c.a() != false) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0061, code lost:
        r5.e.c(null);
     */
    public final void a(Activity activity) {
        if (activity != null) {
            int taskId = activity.getTaskId();
            boolean z = false;
            if (taskId != -1) {
                Intent intent = activity.getIntent();
                if (intent != null) {
                    Set categories = intent.getCategories();
                    if (categories != null && categories.contains("android.intent.category.LAUNCHER") && "android.intent.action.MAIN".equals(intent.getAction())) {
                        ComponentName component = intent.getComponent();
                        if (component != null) {
                            Integer num = (Integer) this.b.put(component.getClassName(), Integer.valueOf(taskId));
                            if (num == null || num.intValue() != taskId) {
                                z = true;
                            }
                        }
                    }
                }
            }
        }
        if (!this.d) {
        }
        this.d = true;
    }
}
