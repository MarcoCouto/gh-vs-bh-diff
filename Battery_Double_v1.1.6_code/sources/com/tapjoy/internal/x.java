package com.tapjoy.internal;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;

public final class x {
    public static void a(String str, String str2, Object... objArr) {
        a(6, str, str2, objArr);
    }

    public static void a(int i, String str, @Nullable String str2, @Nullable Throwable th) {
        if (str2 != null) {
            Log.println(i, str, str2);
        }
        if (th != null) {
            Log.println(i, str, Log.getStackTraceString(th));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0016  */
    public static void a(int i, String str, String str2, Object... objArr) {
        Throwable th;
        jf jfVar;
        int i2;
        if (!(objArr == null || objArr.length == 0)) {
            Throwable th2 = objArr[objArr.length - 1];
            if (th2 instanceof Throwable) {
                th = th2;
                if (str2 != null) {
                    jfVar = new jf(null, objArr, th);
                } else if (objArr == null) {
                    jfVar = new jf(str2);
                } else {
                    StringBuffer stringBuffer = new StringBuffer(str2.length() + 50);
                    int i3 = 0;
                    int i4 = 0;
                    while (true) {
                        if (i3 < objArr.length) {
                            int indexOf = str2.indexOf("{}", i4);
                            if (indexOf != -1) {
                                if (indexOf != 0 && str2.charAt(indexOf + -1) == '\\') {
                                    if (!(indexOf >= 2 && str2.charAt(indexOf + -2) == '\\')) {
                                        i3--;
                                        stringBuffer.append(str2.substring(i4, indexOf - 1));
                                        stringBuffer.append('{');
                                        i2 = indexOf + 1;
                                    } else {
                                        stringBuffer.append(str2.substring(i4, indexOf - 1));
                                        jg.a(stringBuffer, objArr[i3], (Map) new HashMap());
                                        i2 = indexOf + 2;
                                    }
                                } else {
                                    stringBuffer.append(str2.substring(i4, indexOf));
                                    jg.a(stringBuffer, objArr[i3], (Map) new HashMap());
                                    i2 = indexOf + 2;
                                }
                                i4 = i2;
                                i3++;
                            } else if (i4 == 0) {
                                jfVar = new jf(str2, objArr, th);
                            } else {
                                stringBuffer.append(str2.substring(i4, str2.length()));
                                jfVar = new jf(stringBuffer.toString(), objArr, th);
                            }
                        } else {
                            stringBuffer.append(str2.substring(i4, str2.length()));
                            if (i3 < objArr.length - 1) {
                                jfVar = new jf(stringBuffer.toString(), objArr, th);
                            } else {
                                jfVar = new jf(stringBuffer.toString(), objArr, null);
                            }
                        }
                    }
                }
                a(i, str, jfVar.b, jfVar.c);
            }
        }
        th = null;
        if (str2 != null) {
        }
        a(i, str, jfVar.b, jfVar.c);
    }
}
