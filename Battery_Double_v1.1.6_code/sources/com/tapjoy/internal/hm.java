package com.tapjoy.internal;

import android.content.SharedPreferences.Editor;
import android.os.SystemClock;
import com.tapjoy.internal.ev.a;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nullable;

public final class hm {
    final gy a;
    final AtomicBoolean b = new AtomicBoolean();
    @Nullable
    ScheduledFuture c;
    private final Runnable d = new Runnable() {
        public final void run() {
            if (hm.this.b.compareAndSet(true, false)) {
                gw.a("The session ended");
                gy gyVar = hm.this.a;
                long elapsedRealtime = SystemClock.elapsedRealtime() - gyVar.c;
                hc hcVar = gyVar.a;
                synchronized (hcVar) {
                    long a2 = hcVar.c.i.a() + elapsedRealtime;
                    hcVar.c.i.a(a2);
                    hcVar.b.i = Long.valueOf(a2);
                }
                a a3 = gyVar.a(ey.APP, SettingsJsonConstants.SESSION_KEY);
                a3.i = Long.valueOf(elapsedRealtime);
                gyVar.a(a3);
                gyVar.c = 0;
                hc hcVar2 = gyVar.a;
                long longValue = a3.e.longValue();
                synchronized (hcVar2) {
                    Editor a4 = hcVar2.c.a();
                    hcVar2.c.j.a(a4, longValue);
                    hcVar2.c.k.a(a4, elapsedRealtime);
                    a4.apply();
                    hcVar2.b.j = Long.valueOf(longValue);
                    hcVar2.b.k = Long.valueOf(elapsedRealtime);
                }
                gx gxVar = gyVar.b;
                if (gxVar.b != null) {
                    gxVar.a();
                    new im() {
                        /* access modifiers changed from: protected */
                        public final boolean a() {
                            return !gx.this.a.c();
                        }
                    }.run();
                }
                gxVar.a.flush();
                fs.d.notifyObservers();
            }
        }
    };
    private final Runnable e = new Runnable() {
        public final void run() {
        }
    };

    hm(gy gyVar) {
        this.a = gyVar;
    }

    public final void a() {
        if (this.b.get()) {
            if (Boolean.FALSE.booleanValue()) {
                if (this.c == null || this.c.cancel(false)) {
                    this.c = hn.a.schedule(this.d, 3000, TimeUnit.MILLISECONDS);
                }
                return;
            }
            this.d.run();
        }
    }
}
