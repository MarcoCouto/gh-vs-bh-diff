package com.tapjoy.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import javax.annotation.Nullable;

public final class hd {
    private final File a;

    public hd(File file) {
        this.a = file;
    }

    public final synchronized boolean a() {
        FileOutputStream fileOutputStream;
        if (b() != null) {
            return false;
        }
        try {
            File file = this.a;
            String uuid = UUID.randomUUID().toString();
            fileOutputStream = new FileOutputStream(file);
            bg.a((OutputStream) fileOutputStream, uuid);
            jz.a(fileOutputStream);
            if (b() != null) {
                return true;
            }
            return false;
        } catch (IOException e) {
            try {
                this.a.delete();
                throw e;
            } catch (IOException unused) {
                return false;
            }
        } catch (Throwable th) {
            jz.a(fileOutputStream);
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String b() {
        if (this.a.exists()) {
            try {
                String a2 = bg.a(this.a, ak.c);
                if (a2.length() > 0) {
                    return a2;
                }
            } catch (IOException unused) {
            }
        }
        return null;
    }
}
