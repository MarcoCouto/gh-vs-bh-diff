package com.tapjoy.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Window;
import android.view.WindowManager.BadTokenException;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.tapjoy.TJContentActivity;
import com.tapjoy.TJContentActivity.AbstractContentProducer;
import com.tapjoy.TapjoyErrorMessage;
import com.tapjoy.TapjoyErrorMessage.ErrorType;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.iq.a;

public class he extends hg {
    private static final String h = "he";
    /* access modifiers changed from: private */
    public static he i;
    final String a;
    final hu b;
    /* access modifiers changed from: private */
    public final gz j;
    /* access modifiers changed from: private */
    public c k;
    private boolean l;
    /* access modifiers changed from: private */
    public long m;
    private Context n;
    /* access modifiers changed from: private */
    public boolean o = false;

    public static void a() {
        he heVar = i;
        if (heVar != null) {
            heVar.e();
        }
    }

    public he(gz gzVar, String str, hu huVar, Context context) {
        this.j = gzVar;
        this.a = str;
        this.b = huVar;
        this.n = context;
    }

    public final void b() {
        hu huVar = this.b;
        if (huVar.a != null) {
            huVar.a.b();
        }
        if (huVar.b != null) {
            huVar.b.b();
        }
        huVar.c.b();
        if (huVar.e != null) {
            huVar.e.b();
        }
        if (huVar.f != null) {
            huVar.f.b();
        }
        if (huVar.m != null && huVar.m.a != null) {
            huVar.m.a.b();
        }
    }

    public final boolean c() {
        hu huVar = this.b;
        return (huVar.c == null || huVar.c.b == null || (huVar.m != null && huVar.m.a != null && huVar.m.a.b == null) || ((huVar.b == null || huVar.f == null || huVar.b.b == null || huVar.f.b == null) && (huVar.a == null || huVar.e == null || huVar.a.b == null || huVar.e.b == null))) ? false : true;
    }

    public final void a(final ha haVar, final fw fwVar) {
        Activity a2 = a.a(this.n);
        if (a2 != null && !a2.isFinishing()) {
            try {
                a(a2, haVar, fwVar);
                new Object[1][0] = this.a;
                return;
            } catch (BadTokenException unused) {
            }
        }
        Activity a3 = gr.a();
        try {
            TJContentActivity.start(gz.a().e, new AbstractContentProducer() {
                public final void show(Activity activity) {
                    try {
                        he.this.a(activity, haVar, fwVar);
                    } catch (BadTokenException unused) {
                        gw.b("Failed to show the content for \"{}\" caused by invalid activity", he.this.a);
                        haVar.a(he.this.a, he.this.f, null);
                    }
                }

                public final void dismiss(Activity activity) {
                    he.this.e();
                }
            }, (a3 == null || (a3.getWindow().getAttributes().flags & 1024) == 0) ? false : true);
            new Object[1][0] = this.a;
        } catch (ActivityNotFoundException unused2) {
            if (a3 != null && !a3.isFinishing()) {
                try {
                    a(a3, haVar, fwVar);
                    new Object[1][0] = this.a;
                    return;
                } catch (BadTokenException unused3) {
                    gw.b("Failed to show the content for \"{}\" caused by no registration of TJContentActivity", this.a);
                    haVar.a(this.a, this.f, null);
                }
            }
            gw.b("Failed to show the content for \"{}\" caused by no registration of TJContentActivity", this.a);
            haVar.a(this.a, this.f, null);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009d  */
    public void a(final Activity activity, final ha haVar, fw fwVar) {
        boolean z;
        if (this.l) {
            TapjoyLog.e(h, new TapjoyErrorMessage(ErrorType.INTEGRATION_ERROR, "Content is already displayed"));
            return;
        }
        this.l = true;
        i = this;
        this.g = fwVar.a;
        this.k = new c(activity);
        this.k.setOnCancelListener(new OnCancelListener() {
            public final void onCancel(DialogInterface dialogInterface) {
                haVar.d(he.this.a);
            }
        });
        this.k.setOnDismissListener(new OnDismissListener() {
            public final void onDismiss(DialogInterface dialogInterface) {
                String str;
                he.i = null;
                hg.a((Context) activity, he.this.b.g);
                he.this.j.a(he.this.b.k, SystemClock.elapsedRealtime() - he.this.m);
                if (!he.this.d) {
                    haVar.a(he.this.a, he.this.f, he.this.b.h);
                }
                if (he.this.o && he.this.b.k != null && he.this.b.k.containsKey("action_id")) {
                    String obj = he.this.b.k.get("action_id").toString();
                    if (obj != null && obj.length() > 0) {
                        gz c2 = he.this.j;
                        if (c2.b != null) {
                            hi hiVar = c2.b;
                            String a2 = hi.a();
                            String a3 = hiVar.b.a();
                            String a4 = hiVar.a.a();
                            if (a4 == null || !a2.equals(a4)) {
                                hiVar.a.a(a2);
                                str = "";
                            } else {
                                str = a3;
                            }
                            if (!(str.length() == 0)) {
                                if (!str.contains(obj)) {
                                    StringBuilder sb = new StringBuilder(",");
                                    sb.append(obj);
                                    obj = str.concat(sb.toString());
                                } else {
                                    obj = str;
                                }
                            }
                            hiVar.b.a(obj);
                        }
                    }
                }
                if (activity instanceof TJContentActivity) {
                    activity.finish();
                }
            }
        });
        this.k.setCanceledOnTouchOutside(false);
        ip ipVar = new ip(activity, this.b, new iq(activity, this.b, new a() {
            public final void a() {
                he.this.k.cancel();
            }

            public final void a(hs hsVar) {
                if (he.this.g instanceof fu) {
                    fu fuVar = (fu) he.this.g;
                    if (!(fuVar == null || fuVar.c == null)) {
                        fuVar.c.a();
                    }
                }
                he.this.j.a(he.this.b.k, hsVar.b);
                hg.a((Context) activity, hsVar.d);
                if (!jq.c(hsVar.e)) {
                    he.this.e.a(activity, hsVar.e, jq.b(hsVar.f));
                    he.this.d = true;
                }
                haVar.a(he.this.a, hsVar.g);
                if (hsVar.c) {
                    he.this.k.dismiss();
                }
            }

            public final void b() {
                he.this.o = !he.this.o;
            }
        }));
        FrameLayout frameLayout = new FrameLayout(activity);
        frameLayout.addView(ipVar, new LayoutParams(-2, -2, 17));
        this.k.setContentView(frameLayout);
        if (Boolean.FALSE.booleanValue()) {
            Window window = this.k.getWindow();
            if (VERSION.SDK_INT == 16 && "4.1.2".equals(VERSION.RELEASE)) {
                if (Boolean.FALSE.equals(a(window.getContext()))) {
                    z = false;
                    if (z) {
                        int i2 = ad.a.b;
                        ae aeVar = new ae();
                        switch (com.tapjoy.internal.ad.AnonymousClass1.a[i2 - 1]) {
                            case 1:
                                ag agVar = new ag();
                                agVar.a = false;
                                agVar.b = 60.0f;
                                aeVar.a(agVar.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(1.0f).b(0.3f).a());
                                break;
                            case 2:
                                ag agVar2 = new ag();
                                agVar2.a = false;
                                agVar2.b = -60.0f;
                                aeVar.a(agVar2.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(-0.4f).b(0.3f).a());
                                break;
                            case 3:
                                ag agVar3 = new ag();
                                agVar3.a = true;
                                agVar3.b = -60.0f;
                                aeVar.a(agVar3.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(0.3f).b(1.0f).a());
                                break;
                            case 4:
                                ag agVar4 = new ag();
                                agVar4.a = true;
                                agVar4.b = 60.0f;
                                aeVar.a(agVar4.a()).a(new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f)).a(new ah().a(0.3f).b(-0.4f).a());
                                break;
                        }
                        ipVar.startAnimation(aeVar.b().a());
                    }
                } else {
                    window.setFlags(16777216, 16777216);
                }
            }
            z = true;
            if (z) {
            }
        }
        try {
            this.k.show();
            this.k.getWindow().setLayout(-1, -1);
            if ((activity.getWindow().getAttributes().flags & 1024) != 0) {
                this.k.getWindow().setFlags(1024, 1024);
            }
            this.m = SystemClock.elapsedRealtime();
            this.j.a(this.b.k);
            fwVar.a();
            fq fqVar = this.g;
            if (fqVar != null) {
                fqVar.b();
            }
            haVar.c(this.a);
        } catch (BadTokenException e) {
            throw e;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.k != null) {
            this.k.dismiss();
        }
    }

    private static Boolean a(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle != null) {
                Object obj = bundle.get("tapjoy:hardwareAccelerated");
                if (obj instanceof Boolean) {
                    return (Boolean) obj;
                }
            }
        } catch (NameNotFoundException unused) {
        }
        return null;
    }
}
