package com.tapjoy.internal;

import com.github.mikephil.charting.utils.Utils;

public final class gi {
    public static final gi a;
    public final long b;
    public final long c;
    public final double d;
    public long e;
    private final long f;

    static {
        gi giVar = new gi(0, 0, 0, Utils.DOUBLE_EPSILON);
        a = giVar;
    }

    public gi(long j, long j2, long j3, double d2) {
        this.f = j;
        this.b = j2;
        this.c = j3;
        this.d = d2;
        this.e = j;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        gi giVar = (gi) obj;
        return this.f == giVar.f && this.b == giVar.b && this.c == giVar.c && this.d == giVar.d && this.e == giVar.e;
    }
}
