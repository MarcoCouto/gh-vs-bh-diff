package com.tapjoy.internal;

import android.view.animation.Animation;

public class ad {
    protected final Animation a;

    /* renamed from: com.tapjoy.internal.ad$1 reason: invalid class name */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] a = new int[a.a().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
        static {
            a[a.a - 1] = 1;
            a[a.b - 1] = 2;
            a[a.c - 1] = 3;
            try {
                a[a.d - 1] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public enum a {
        ;
        
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;

        static {
            e = new int[]{a, b, c, d};
        }

        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    public ad(Animation animation) {
        this.a = animation;
        animation.setDuration(400);
    }

    public Animation a() {
        return this.a;
    }

    public final ad b() {
        this.a.setDuration(600);
        return this;
    }
}
