package com.tapjoy.internal;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.tapjoy.TJConnectListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class gb {
    private final ReentrantLock a = new ReentrantLock();
    volatile int b = c.a;
    b c;
    long d = 1000;
    a e;
    private final Condition f = this.a.newCondition();
    private final LinkedList g = new LinkedList();
    private a h;

    /* renamed from: com.tapjoy.internal.gb$3 reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] a = new int[c.a().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0029 */
        static {
            a[c.e - 1] = 1;
            a[c.a - 1] = 2;
            a[c.b - 1] = 3;
            a[c.c - 1] = 4;
            a[c.d - 1] = 5;
        }
    }

    class a {
        public final Context a;
        public final String b;
        public final Hashtable c;

        public a(Context context, String str, Hashtable hashtable) {
            Context context2;
            if (context == null) {
                context2 = null;
            } else if (context instanceof Application) {
                context2 = context;
            } else {
                context2 = context.getApplicationContext();
            }
            if (context2 == null) {
                context2 = context;
            }
            this.a = context2;
            this.b = str;
            this.c = hashtable;
        }
    }

    class b extends ka {
        private boolean b;
        /* access modifiers changed from: private */
        public boolean c;
        private Context d;
        private BroadcastReceiver e;

        private b() {
            this.e = new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    gb.this.b();
                }
            };
        }

        /* synthetic */ b(gb gbVar, byte b2) {
            this();
        }

        /* access modifiers changed from: protected */
        public final void a() {
            this.b = true;
            gb.this.b();
        }

        /* access modifiers changed from: protected */
        public final void b() {
            gb gbVar = gb.this;
            int i = c.c;
            int i2 = c.b;
            gbVar.a(i);
        }

        /* access modifiers changed from: protected */
        public final void c() {
            if (gb.this.c == this) {
                gb.this.c = null;
            }
            if (gb.this.b == c.c) {
                gb gbVar = gb.this;
                int i = c.a;
                int i2 = c.c;
                gbVar.a(i);
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x004e */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0064 A[SYNTHETIC, Splitter:B:17:0x0064] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0052 A[SYNTHETIC] */
        public final void d() {
            this.d = gb.this.a().a;
            this.d.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            while (!this.b) {
                try {
                    final CountDownLatch countDownLatch = new CountDownLatch(1);
                    fs.b.addObserver(new Observer() {
                        public final void update(Observable observable, Object obj) {
                            fs.b.deleteObserver(this);
                            b.this.c = Boolean.TRUE.equals(obj);
                            countDownLatch.countDown();
                        }
                    });
                    a a2 = gb.this.a();
                    if (!gb.this.a(a2.a, a2.b, a2.c, null)) {
                        gb.this.a(false);
                        h();
                        return;
                    }
                    countDownLatch.await();
                    if (!this.c) {
                        gb gbVar = gb.this;
                        int i = c.e;
                        int i2 = c.c;
                        gbVar.a(i);
                        gb.this.a(true);
                        return;
                    }
                    gb.this.a(false);
                    long max = Math.max(gb.this.d, 1000);
                    gb.this.d = Math.min(max << 2, 3600000);
                    gb.this.a(max);
                } finally {
                    h();
                }
            }
            h();
        }

        private void h() {
            this.d.unregisterReceiver(this.e);
        }
    }

    enum c {
        ;
        
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;

        static {
            f = new int[]{a, b, c, d, e};
        }

        public static int[] a() {
            return (int[]) f.clone();
        }
    }

    public abstract boolean a(Context context, String str, Hashtable hashtable, TJConnectListener tJConnectListener);

    public final boolean b(Context context, String str, Hashtable hashtable, TJConnectListener tJConnectListener) {
        this.a.lock();
        if (tJConnectListener != null) {
            try {
                this.g.addLast(fn.a(tJConnectListener, TJConnectListener.class));
            } catch (Throwable th) {
                this.a.unlock();
                throw th;
            }
        }
        a aVar = new a(context, str, hashtable);
        switch (AnonymousClass3.a[this.b - 1]) {
            case 1:
                a(true);
                this.a.unlock();
                return true;
            case 2:
                this.e = aVar;
                fs.b.addObserver(new Observer() {
                    public final void update(Observable observable, Object obj) {
                        fs.b.deleteObserver(this);
                        if (!Boolean.valueOf(Boolean.TRUE.equals(obj)).booleanValue() && gb.this.e != null && gb.this.e.a != null) {
                            gb.this.c = new b(gb.this, 0);
                            gb.this.c.e();
                        }
                    }
                });
                if (a(aVar.a, aVar.b, aVar.c, new TJConnectListener() {
                    public final void onConnectSuccess() {
                        gb gbVar = gb.this;
                        int i = c.e;
                        int i2 = c.b;
                        gbVar.a(i);
                        gb.this.a(true);
                    }

                    public final void onConnectFailure() {
                        gb.this.a(false);
                    }
                })) {
                    int i = c.b;
                    int i2 = c.a;
                    a(i);
                    this.a.unlock();
                    return true;
                }
                this.g.clear();
                this.a.unlock();
                return false;
            case 3:
            case 4:
                this.h = aVar;
                this.a.unlock();
                return true;
            case 5:
                this.h = aVar;
                b();
                this.a.unlock();
                return true;
            default:
                a(c.a);
                this.a.unlock();
                return false;
        }
        this.a.unlock();
        throw th;
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i) {
        this.a.lock();
        try {
            int i2 = this.b;
            this.b = i;
        } finally {
            this.a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final a a() {
        this.a.lock();
        try {
            if (this.h != null) {
                this.e = this.h;
                this.h = null;
            }
            return this.e;
        } finally {
            this.a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        this.a.lock();
        try {
            if (this.g.size() != 0) {
                ArrayList arrayList = new ArrayList(this.g);
                this.g.clear();
                this.a.unlock();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    TJConnectListener tJConnectListener = (TJConnectListener) it.next();
                    if (z) {
                        tJConnectListener.onConnectSuccess();
                    } else {
                        tJConnectListener.onConnectFailure();
                    }
                }
            }
        } finally {
            this.a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.a.lock();
        try {
            this.d = 1000;
            this.f.signal();
        } finally {
            this.a.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(long j) {
        this.a.lock();
        try {
            int i = c.d;
            int i2 = c.c;
            a(i);
            if (this.f.await(j, TimeUnit.MILLISECONDS)) {
                this.d = 1000;
            }
            return false;
        } catch (InterruptedException unused) {
            return false;
        } finally {
            int i3 = c.c;
            int i4 = c.d;
            a(i3);
            this.a.unlock();
        }
    }
}
