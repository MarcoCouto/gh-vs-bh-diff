package com.tapjoy.internal;

import java.util.Arrays;

final class ja extends iu {
    final transient byte[][] f;
    final transient int[] g;

    ja(ir irVar, int i) {
        super(null);
        je.a(irVar.b, 0, (long) i);
        int i2 = 0;
        iy iyVar = irVar.a;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            if (iyVar.c != iyVar.b) {
                i3 += iyVar.c - iyVar.b;
                i4++;
                iyVar = iyVar.f;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        this.f = new byte[i4][];
        this.g = new int[(i4 * 2)];
        iy iyVar2 = irVar.a;
        int i5 = 0;
        while (i2 < i) {
            this.f[i5] = iyVar2.a;
            i2 += iyVar2.c - iyVar2.b;
            if (i2 > i) {
                i2 = i;
            }
            this.g[i5] = i2;
            this.g[this.f.length + i5] = iyVar2.b;
            iyVar2.d = true;
            i5++;
            iyVar2 = iyVar2.f;
        }
    }

    public final String a() {
        return e().a();
    }

    public final String b() {
        return e().b();
    }

    public final iu a(int i, int i2) {
        return e().a(i, i2);
    }

    public final byte a(int i) {
        int i2;
        je.a((long) this.g[this.f.length - 1], (long) i, 1);
        int b = b(i);
        if (b == 0) {
            i2 = 0;
        } else {
            i2 = this.g[b - 1];
        }
        return this.f[b][(i - i2) + this.g[this.f.length + b]];
    }

    private int b(int i) {
        int binarySearch = Arrays.binarySearch(this.g, 0, this.f.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    public final int c() {
        return this.g[this.f.length - 1];
    }

    public final byte[] d() {
        byte[] bArr = new byte[this.g[this.f.length - 1]];
        int length = this.f.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = this.g[length + i];
            int i4 = this.g[i];
            System.arraycopy(this.f[i], i3, bArr, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr;
    }

    /* access modifiers changed from: 0000 */
    public final void a(ir irVar) {
        int length = this.f.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = this.g[length + i];
            int i4 = this.g[i];
            iy iyVar = new iy(this.f[i], i3, (i3 + i4) - i2);
            if (irVar.a == null) {
                iyVar.g = iyVar;
                iyVar.f = iyVar;
                irVar.a = iyVar;
            } else {
                irVar.a.g.a(iyVar);
            }
            i++;
            i2 = i4;
        }
        irVar.b += (long) i2;
    }

    public final boolean a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        if (i < 0 || i > c() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int b = b(i);
        while (i3 > 0) {
            if (b == 0) {
                i4 = 0;
            } else {
                i4 = this.g[b - 1];
            }
            int min = Math.min(i3, ((this.g[b] - i4) + i4) - i);
            if (!je.a(this.f[b], (i - i4) + this.g[this.f.length + b], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b++;
        }
        return true;
    }

    private iu e() {
        return new iu(d());
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x005e A[RETURN] */
    public final boolean equals(Object obj) {
        boolean z;
        int i;
        if (obj == this) {
            return true;
        }
        if (obj instanceof iu) {
            iu iuVar = (iu) obj;
            if (iuVar.c() == c()) {
                int c = c();
                if (c() - c >= 0) {
                    int b = b(0);
                    int i2 = 0;
                    int i3 = 0;
                    while (true) {
                        if (c <= 0) {
                            z = true;
                            break;
                        }
                        if (b == 0) {
                            i = 0;
                        } else {
                            i = this.g[b - 1];
                        }
                        int min = Math.min(c, ((this.g[b] - i) + i) - i2);
                        if (!iuVar.a(i3, this.f[b], (i2 - i) + this.g[this.f.length + b], min)) {
                            break;
                        }
                        i2 += min;
                        i3 += min;
                        c -= min;
                        b++;
                    }
                    if (!z) {
                        return true;
                    }
                }
                z = false;
                if (!z) {
                    return false;
                }
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int length = this.f.length;
        int i2 = 0;
        int i3 = 0;
        int i4 = 1;
        while (i2 < length) {
            byte[] bArr = this.f[i2];
            int i5 = this.g[length + i2];
            int i6 = this.g[i2];
            int i7 = (i6 - i3) + i5;
            while (i5 < i7) {
                i4 = (i4 * 31) + bArr[i5];
                i5++;
            }
            i2++;
            i3 = i6;
        }
        this.d = i4;
        return i4;
    }

    public final String toString() {
        return e().toString();
    }
}
