package com.tapjoy.internal;

import java.util.Collections;
import java.util.List;

public final class eo {
    public static List a() {
        return new ep(Collections.emptyList());
    }

    public static List a(String str, List list) {
        if (list != null) {
            if (list instanceof ep) {
                list = ((ep) list).a;
            }
            if (list == Collections.emptyList() || (list instanceof en)) {
                return list;
            }
            en enVar = new en(list);
            if (!enVar.contains(null)) {
                return enVar;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".contains(null)");
            throw new IllegalArgumentException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" == null");
        throw new NullPointerException(sb2.toString());
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static IllegalStateException a(Object... objArr) {
        StringBuilder sb = new StringBuilder();
        String str = "";
        int length = objArr.length;
        for (int i = 0; i < length; i += 2) {
            if (objArr[i] == null) {
                if (sb.length() > 0) {
                    str = "s";
                }
                sb.append("\n  ");
                sb.append(objArr[i + 1]);
            }
        }
        StringBuilder sb2 = new StringBuilder("Required field");
        sb2.append(str);
        sb2.append(" not set:");
        sb2.append(sb);
        throw new IllegalStateException(sb2.toString());
    }
}
