package com.tapjoy.internal;

import android.content.SharedPreferences.Editor;
import android.os.SystemClock;
import com.tapjoy.internal.ex.a;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;

public final class gy {
    final hc a;
    final gx b;
    long c;
    private int d = 1;
    private final a e = new a();

    gy(hc hcVar, gx gxVar) {
        this.a = hcVar;
        this.b = gxVar;
    }

    public final void a(String str, String str2, double d2, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
        double d3;
        hc hcVar = this.a;
        synchronized (hcVar) {
            Editor a2 = hcVar.c.a();
            int i = 1;
            if (str2.equals(hcVar.c.l.a())) {
                i = 1 + hcVar.c.m.b();
                hcVar.c.m.a(a2, i);
                d3 = hcVar.c.n.a() + d2;
                hcVar.c.n.a(a2, d3);
                a2.apply();
            } else {
                hcVar.c.l.a(a2, str2);
                hcVar.c.m.a(a2, 1);
                hcVar.c.n.a(a2, d2);
                hcVar.c.o.a(a2);
                hcVar.c.p.a(a2);
                a2.apply();
                hcVar.b.l = str2;
                hcVar.b.o = null;
                hcVar.b.p = null;
                d3 = d2;
            }
            hcVar.b.m = Integer.valueOf(i);
            hcVar.b.n = Double.valueOf(d3);
        }
        ev.a a3 = a(ey.APP, "purchase");
        fd.a aVar = new fd.a();
        aVar.c = str;
        if (str2 != null) {
            aVar.f = str2;
        }
        aVar.e = Double.valueOf(d2);
        if (str5 != null) {
            aVar.m = str5;
        }
        if (str3 != null) {
            aVar.o = str3;
        }
        if (str4 != null) {
            aVar.p = str4;
        }
        a3.p = aVar.b();
        a(a3);
        hc hcVar2 = this.a;
        long longValue = a3.e.longValue();
        synchronized (hcVar2) {
            Editor a4 = hcVar2.c.a();
            hcVar2.c.o.a(a4, longValue);
            hcVar2.c.p.a(a4, d2);
            a4.apply();
            hcVar2.b.o = Long.valueOf(longValue);
            hcVar2.b.p = Double.valueOf(d2);
        }
    }

    public final void a(String str, String str2, String str3, String str4, Map map) {
        ev.a a2 = a(ey.CUSTOM, str2);
        a2.t = str;
        a2.u = str3;
        a2.v = str4;
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                a2.w.add(new ez((String) entry.getKey(), (Long) entry.getValue()));
            }
        }
        a(a2);
    }

    public final void a(String str, String str2, int i, long j, long j2, Map map) {
        ev.a a2 = a(ey.USAGES, str);
        a2.x = str2;
        a2.y = Integer.valueOf(i);
        a2.z = Long.valueOf(j);
        a2.A = Long.valueOf(j2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                a2.w.add(new ez((String) entry.getKey(), (Long) entry.getValue()));
            }
        }
        a(a2);
    }

    public final ev.a a(ey eyVar, String str) {
        fb b2 = this.a.b();
        ev.a aVar = new ev.a();
        aVar.g = hc.a;
        aVar.c = eyVar;
        aVar.d = str;
        if (v.c()) {
            aVar.e = Long.valueOf(v.b());
            aVar.f = Long.valueOf(System.currentTimeMillis());
        } else {
            aVar.e = Long.valueOf(System.currentTimeMillis());
            aVar.h = Long.valueOf(SystemClock.elapsedRealtime());
        }
        aVar.j = b2.d;
        aVar.k = b2.e;
        aVar.l = b2.f;
        return aVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:12|13|15|16|17|18|19|20) */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0046 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004e */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006a A[SYNTHETIC, Splitter:B:37:0x006a] */
    public final synchronized void a(ev.a aVar) {
        if (aVar.c != ey.USAGES) {
            int i = this.d;
            this.d = i + 1;
            aVar.n = Integer.valueOf(i);
            if (this.e.c != null) {
                aVar.o = this.e.b();
            }
            this.e.c = aVar.c;
            this.e.d = aVar.d;
            this.e.e = aVar.t;
        }
        gx gxVar = this.b;
        ev b2 = aVar.b();
        try {
            hl hlVar = gxVar.a;
            synchronized (hlVar.a) {
                hlVar.b.add(b2);
                hlVar.a();
                hlVar.b.add(b2);
            }
            if (gxVar.b == null) {
                if (!gw.a) {
                    if (b2.n == ey.CUSTOM) {
                        gxVar.a(false);
                        return;
                    }
                }
                gxVar.a(true);
                return;
            }
            gxVar.a.flush();
        } catch (Exception unused) {
        }
    }
}
