package com.tapjoy.internal;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

public abstract class bn implements bk, bp {
    private HashMap a;

    public static abstract class a {
        private static a a;

        public static a a() {
            a aVar = a;
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = bo.a;
            a = aVar2;
            return aVar2;
        }

        public final bn a(InputStream inputStream) {
            return a((Reader) new InputStreamReader(inputStream, jm.c));
        }

        public bn a(Reader reader) {
            return a(jy.a(reader));
        }

        public bn a(String str) {
            return a((InputStream) new ByteArrayInputStream(str.getBytes(jm.c.name())));
        }
    }

    public static bn a(InputStream inputStream) {
        return a.a().a(inputStream);
    }

    public static bn b(String str) {
        return a.a().a(str);
    }

    public final Object a(String str) {
        if (this.a != null) {
            return this.a.get(str);
        }
        return null;
    }

    public final void a(String str, Object obj) {
        if (this.a == null) {
            this.a = new HashMap();
        }
        this.a.put(str, obj);
    }

    public final boolean a() {
        return k() == bs.BEGIN_OBJECT;
    }

    private boolean t() {
        if (k() != bs.NULL) {
            return false;
        }
        o();
        return true;
    }

    public final String b() {
        if (t()) {
            return null;
        }
        return m();
    }

    public final String c(String str) {
        if (t()) {
            return str;
        }
        return m();
    }

    private Object u() {
        bs k = k();
        switch (k) {
            case BEGIN_ARRAY:
                return c();
            case BEGIN_OBJECT:
                return d();
            case NULL:
                o();
                return null;
            case BOOLEAN:
                return Boolean.valueOf(n());
            case NUMBER:
                return new ci(m());
            case STRING:
                return m();
            default:
                StringBuilder sb = new StringBuilder("Expected a value but was ");
                sb.append(k);
                throw new IllegalStateException(sb.toString());
        }
    }

    public final List c() {
        LinkedList linkedList = new LinkedList();
        a((List) linkedList);
        return linkedList;
    }

    private void a(List list) {
        f();
        while (j()) {
            list.add(u());
        }
        g();
    }

    public final Map d() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        a((Map) linkedHashMap);
        return linkedHashMap;
    }

    public final void a(Map map) {
        h();
        while (j()) {
            map.put(l(), u());
        }
        i();
    }

    @Nullable
    public final Object a(bi biVar) {
        if (t()) {
            return null;
        }
        return biVar.a(this);
    }

    public final void a(List list, bi biVar) {
        f();
        while (j()) {
            list.add(biVar.a(this));
        }
        g();
    }

    private static URI d(String str) {
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            throw new bv(e);
        }
    }

    public final URL e() {
        URI uri = (URI) a("BASE_URI");
        if (uri != null) {
            return uri.resolve(d(m())).toURL();
        }
        return new URL(m());
    }
}
