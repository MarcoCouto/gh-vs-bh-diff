package com.tapjoy.internal;

import android.graphics.Rect;
import com.tapjoy.TapjoyConstants;

public final class hs {
    public static final bi h = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            bnVar.h();
            String str = "";
            Rect rect = null;
            String str2 = null;
            String str3 = null;
            String str4 = null;
            gl glVar = null;
            boolean z = false;
            while (bnVar.j()) {
                String l = bnVar.l();
                if (TtmlNode.TAG_REGION.equals(l)) {
                    rect = (Rect) bj.b.a(bnVar);
                } else if ("value".equals(l)) {
                    str2 = bnVar.m();
                } else if (TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL.equals(l)) {
                    z = bnVar.n();
                } else if ("url".equals(l)) {
                    str = bnVar.m();
                } else if (TapjoyConstants.TJC_REDIRECT_URL.equals(l)) {
                    str3 = bnVar.b();
                } else if ("ad_content".equals(l)) {
                    str4 = bnVar.b();
                } else if (ho.a(l)) {
                    glVar = ho.a(l, bnVar);
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            hs hsVar = new hs(rect, str2, z, str, str3, str4, glVar);
            return hsVar;
        }
    };
    public final Rect a;
    public final String b;
    public final boolean c;
    public final String d;
    public String e;
    public String f;
    public final gl g;

    hs(Rect rect, String str, boolean z, String str2, String str3, String str4, gl glVar) {
        this.a = rect;
        this.b = str;
        this.c = z;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = glVar;
    }
}
