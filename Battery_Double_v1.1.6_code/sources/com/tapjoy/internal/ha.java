package com.tapjoy.internal;

import android.os.Handler;
import android.os.Looper;

public class ha implements go {
    private static final ha a = new ha() {
        public final void a(String str) {
        }

        public final void a(String str, gl glVar) {
        }

        public final void a(String str, String str2, gl glVar) {
        }

        public final void b(String str) {
        }

        public final void c(String str) {
        }

        public final void d(String str) {
        }
    };
    /* access modifiers changed from: private */
    public final go b;
    private final ba c;

    /* synthetic */ ha(byte b2) {
        this();
    }

    public static ha a(go goVar) {
        if (!(!(goVar instanceof ha))) {
            throw new IllegalArgumentException();
        } else if (goVar != null) {
            return new ha(goVar);
        } else {
            return a;
        }
    }

    private ha() {
        this.b = null;
        this.c = null;
    }

    private ha(go goVar) {
        Handler handler;
        this.b = goVar;
        Looper myLooper = Looper.myLooper();
        if (myLooper != null) {
            jp.a(myLooper);
            handler = myLooper == Looper.getMainLooper() ? u.a() : new Handler(myLooper);
        } else {
            handler = null;
        }
        if (handler != null) {
            this.c = u.a(handler);
            new Object[1][0] = handler.getLooper();
        } else if (Thread.currentThread() == gr.b()) {
            this.c = gr.a;
        } else {
            this.c = u.a(u.a());
        }
    }

    public void a(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                ha.this.b.a(str);
            }
        });
    }

    public void b(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                ha.this.b.b(str);
            }
        });
    }

    public void c(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                ha.this.b.c(str);
            }
        });
    }

    public void d(final String str) {
        this.c.a(new Runnable() {
            public final void run() {
                ha.this.b.d(str);
            }
        });
    }

    public void a(final String str, final gl glVar) {
        this.c.a(new Runnable() {
            public final void run() {
                ha.this.b.a(str, glVar);
            }
        });
    }

    public void a(final String str, final String str2, final gl glVar) {
        this.c.a(new Runnable() {
            public final void run() {
                ha.this.b.a(str, str2, glVar);
            }
        });
    }
}
