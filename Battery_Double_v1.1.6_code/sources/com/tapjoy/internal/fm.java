package com.tapjoy.internal;

import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyLog;
import com.tapjoy.internal.gf.a;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class fm extends fq {
    private static final String c = "fm";

    public fm(String str, String str2) {
        super(str, str2, "ad");
    }

    public final a a(String str, JSONObject jSONObject) {
        return a(str, a(jSONObject), b(jSONObject));
    }

    public final a b(String str, JSONObject jSONObject) {
        return b(str, a(jSONObject), b(jSONObject));
    }

    public static Map a(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        if (jSONObject != null) {
            try {
                JSONObject jSONObject2 = jSONObject.getJSONObject(String.USAGE_TRACKER_DIMENSIONS);
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    hashMap.put(str, jSONObject2.get(str));
                }
            } catch (JSONException e) {
                String str2 = c;
                StringBuilder sb = new StringBuilder("Unable to getAdUnitDimensions. Invalid params: ");
                sb.append(e);
                TapjoyLog.d(str2, sb.toString());
            }
        }
        return hashMap;
    }

    public static Map b(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        if (jSONObject != null) {
            try {
                JSONObject jSONObject2 = jSONObject.getJSONObject(String.USAGE_TRACKER_VALUES);
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    hashMap.put(str, Long.valueOf(jSONObject2.getLong(str)));
                }
            } catch (JSONException e) {
                String str2 = c;
                StringBuilder sb = new StringBuilder("Unable to getAdUnitValues. Invalid params: ");
                sb.append(e);
                TapjoyLog.d(str2, sb.toString());
            }
        }
        return hashMap;
    }
}
