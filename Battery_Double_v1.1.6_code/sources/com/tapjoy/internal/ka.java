package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.internal.kf.a;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public abstract class ka implements kf {
    /* access modifiers changed from: private */
    public static final Logger a = Logger.getLogger(ka.class.getName());
    private final kf b = new kc() {
        /* access modifiers changed from: protected */
        public final void a() {
            new Executor() {
                public final void execute(Runnable runnable) {
                    new Thread(runnable, ka.this.getClass().getSimpleName()).start();
                }
            }.execute(new Runnable() {
                public final void run() {
                    try {
                        ka.this.b();
                        AnonymousClass1.this.c();
                        if (AnonymousClass1.this.f() == a.RUNNING) {
                            ka.this.d();
                        }
                        ka.this.c();
                        AnonymousClass1.this.d();
                    } catch (Throwable th) {
                        AnonymousClass1.this.a(th);
                        throw jr.a(th);
                    }
                }
            });
        }

        /* access modifiers changed from: protected */
        public final void b() {
            ka.this.a();
        }
    };

    public void a() {
    }

    public void b() {
    }

    public void c() {
    }

    public abstract void d();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(f());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public final ke e() {
        return this.b.e();
    }

    public final a f() {
        return this.b.f();
    }
}
