package com.tapjoy.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import javax.annotation.Nonnull;

final class ih implements a {
    ih() {
    }

    @Nonnull
    public final Bitmap a(int i, int i2, Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }

    public final byte[] a(int i) {
        return new byte[i];
    }

    public final int[] b(int i) {
        return new int[i];
    }
}
