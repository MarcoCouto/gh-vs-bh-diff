package com.yandex.mobile.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.eg;

public final class VideoController {
    @NonNull
    private final eg a;

    public VideoController(@NonNull eg egVar) {
        this.a = egVar;
    }

    public final void setVideoEventListener(@Nullable VideoEventListener videoEventListener) {
        this.a.a(videoEventListener);
    }
}
