package com.yandex.mobile.ads;

public final class Gender {
    public static final String FEMALE = "female";
    public static final String MALE = "male";

    private Gender() {
    }
}
