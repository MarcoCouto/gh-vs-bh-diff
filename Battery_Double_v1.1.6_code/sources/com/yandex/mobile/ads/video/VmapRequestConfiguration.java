package com.yandex.mobile.ads.video;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.tf;

public final class VmapRequestConfiguration {
    @NonNull
    private final String a;
    @NonNull
    private final String b;

    public static final class Builder {
        /* access modifiers changed from: private */
        @NonNull
        public final String a;
        /* access modifiers changed from: private */
        @NonNull
        public String b = "0";

        public Builder(@NonNull String str) {
            this.a = str;
            tf.a(this.a, "PageId");
        }

        public final VmapRequestConfiguration build() {
            return new VmapRequestConfiguration(this, 0);
        }

        public final Builder setCategory(@NonNull String str) {
            if (!TextUtils.isEmpty(str)) {
                this.b = str;
                return this;
            }
            throw new IllegalArgumentException("Passed categoryId is empty");
        }
    }

    /* synthetic */ VmapRequestConfiguration(Builder builder, byte b2) {
        this(builder);
    }

    private VmapRequestConfiguration(@NonNull Builder builder) {
        this.a = builder.b;
        this.b = builder.a;
    }

    @NonNull
    public final String getCategoryId() {
        return this.a;
    }

    @NonNull
    public final String getPageId() {
        return this.b;
    }
}
