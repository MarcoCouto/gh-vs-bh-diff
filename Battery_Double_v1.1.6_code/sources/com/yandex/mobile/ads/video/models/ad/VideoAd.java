package com.yandex.mobile.ads.video.models.ad;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.ia;
import com.yandex.mobile.ads.impl.tm;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class VideoAd implements Parcelable {
    public static final Creator<VideoAd> CREATOR = new Creator<VideoAd>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new VideoAd[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new VideoAd(parcel);
        }
    };
    private static final String ERROR = "error";
    private String mAdSystem;
    private String mAdTitle;
    private List<Creative> mCreatives;
    private String mDescription;
    private String mSurvey;
    private Map<String, List<String>> mTrackingEvents;
    private String mVastAdTagUri;
    private final boolean mWrapper;
    @Nullable
    private tm mWrapperConfiguration;

    public final int describeContents() {
        return 0;
    }

    public final boolean isWrapper() {
        return this.mWrapper;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final tm getWrapperConfiguration() {
        return this.mWrapperConfiguration;
    }

    public final String getVastAdTagUri() {
        return this.mVastAdTagUri;
    }

    private void setVastAdTagUri(String str) {
        this.mVastAdTagUri = str;
    }

    public final String getAdSystem() {
        return this.mAdSystem;
    }

    private void setAdSystem(String str) {
        this.mAdSystem = str;
    }

    public final String getAdTitle() {
        return this.mAdTitle;
    }

    private void setAdTitle(String str) {
        this.mAdTitle = str;
    }

    public final String getDescription() {
        return this.mDescription;
    }

    private void setDescription(String str) {
        this.mDescription = str;
    }

    public final String getSurvey() {
        return this.mSurvey;
    }

    private void setSurvey(String str) {
        this.mSurvey = str;
    }

    public final List<Creative> getCreatives() {
        return this.mCreatives;
    }

    private void addCreative(Creative creative) {
        this.mCreatives.add(creative);
    }

    private void addCreatives(Collection<Creative> collection) {
        for (Creative addCreative : ia.a(collection)) {
            addCreative(addCreative);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void addTrackingEvent(String str, String str2) {
        List list = (List) this.mTrackingEvents.get(str);
        if (list == null) {
            list = new ArrayList();
            this.mTrackingEvents.put(str, list);
        }
        if (!TextUtils.isEmpty(str2)) {
            list.add(str2);
        }
    }

    private void addError(String str) {
        addTrackingEvent("error", str);
    }

    private void addImpression(String str) {
        addTrackingEvent(Events.AD_IMPRESSION, str);
    }

    private void addImpressions(Collection<String> collection) {
        for (String addImpression : ia.a(collection)) {
            addImpression(addImpression);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void setWrapperConfiguration(@Nullable tm tmVar) {
        this.mWrapperConfiguration = tmVar;
    }

    public final Map<String, List<String>> getTrackingEvents() {
        return Collections.unmodifiableMap(this.mTrackingEvents);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VideoAd videoAd = (VideoAd) obj;
        if (this.mWrapper != videoAd.mWrapper) {
            return false;
        }
        if (this.mAdSystem == null ? videoAd.mAdSystem != null : !this.mAdSystem.equals(videoAd.mAdSystem)) {
            return false;
        }
        if (this.mAdTitle == null ? videoAd.mAdTitle != null : !this.mAdTitle.equals(videoAd.mAdTitle)) {
            return false;
        }
        if (!this.mCreatives.equals(videoAd.mCreatives)) {
            return false;
        }
        if (this.mDescription == null ? videoAd.mDescription != null : !this.mDescription.equals(videoAd.mDescription)) {
            return false;
        }
        if (this.mSurvey == null ? videoAd.mSurvey != null : !this.mSurvey.equals(videoAd.mSurvey)) {
            return false;
        }
        if (!this.mTrackingEvents.equals(videoAd.mTrackingEvents)) {
            return false;
        }
        return this.mVastAdTagUri == null ? videoAd.mVastAdTagUri == null : this.mVastAdTagUri.equals(videoAd.mVastAdTagUri);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((this.mAdSystem != null ? this.mAdSystem.hashCode() : 0) * 31) + (this.mWrapper ? 1 : 0)) * 31) + (this.mAdTitle != null ? this.mAdTitle.hashCode() : 0)) * 31) + (this.mDescription != null ? this.mDescription.hashCode() : 0)) * 31) + (this.mSurvey != null ? this.mSurvey.hashCode() : 0)) * 31;
        if (this.mVastAdTagUri != null) {
            i = this.mVastAdTagUri.hashCode();
        }
        return ((((hashCode + i) * 31) + this.mCreatives.hashCode()) * 31) + this.mTrackingEvents.hashCode();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mWrapper ? 1 : 0);
        parcel.writeString(this.mAdSystem);
        parcel.writeString(this.mAdTitle);
        parcel.writeString(this.mDescription);
        parcel.writeString(this.mSurvey);
        parcel.writeString(this.mVastAdTagUri);
        parcel.writeTypedList(this.mCreatives);
        parcel.writeInt(this.mTrackingEvents.size());
        for (Entry entry : this.mTrackingEvents.entrySet()) {
            parcel.writeString((String) entry.getKey());
            parcel.writeList((List) entry.getValue());
        }
    }

    private VideoAd(Boolean bool) {
        this.mCreatives = new ArrayList();
        this.mTrackingEvents = new HashMap();
        this.mWrapper = bool.booleanValue();
    }

    private VideoAd(Parcel parcel) {
        this.mCreatives = new ArrayList();
        this.mTrackingEvents = new HashMap();
        boolean z = true;
        if (parcel.readInt() != 1) {
            z = false;
        }
        this.mWrapper = z;
        this.mAdSystem = parcel.readString();
        this.mAdTitle = parcel.readString();
        this.mDescription = parcel.readString();
        this.mSurvey = parcel.readString();
        this.mVastAdTagUri = parcel.readString();
        parcel.readTypedList(this.mCreatives, Creative.CREATOR);
        this.mTrackingEvents = new HashMap();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.mTrackingEvents.put(parcel.readString(), parcel.readArrayList(getClass().getClassLoader()));
        }
    }
}
