package com.yandex.mobile.ads.video.models.ad;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.tm;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class VideoAdConfigurator {
    @NonNull
    private final VideoAd mVideoAd;

    public VideoAdConfigurator(@NonNull VideoAd videoAd) {
        this.mVideoAd = videoAd;
    }

    public void addTrackingEvents(@NonNull Map<String, List<String>> map) {
        for (Entry entry : map.entrySet()) {
            for (String addTrackingEvent : (List) entry.getValue()) {
                this.mVideoAd.addTrackingEvent((String) entry.getKey(), addTrackingEvent);
            }
        }
    }

    public void setWrapperConfiguration(@Nullable tm tmVar) {
        this.mVideoAd.setWrapperConfiguration(tmVar);
    }
}
