package com.yandex.mobile.ads.video.models.ad;

import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CreativeConfigurator {
    @NonNull
    private final Creative mCreative;

    public CreativeConfigurator(@NonNull Creative creative) {
        this.mCreative = creative;
    }

    public void addTrackingEvents(@NonNull Map<String, List<String>> map) {
        for (Entry entry : map.entrySet()) {
            for (String addTrackingEvent : (List) entry.getValue()) {
                this.mCreative.addTrackingEvent((String) entry.getKey(), addTrackingEvent);
            }
        }
    }

    public void addIcons(@NonNull Collection<Icon> collection) {
        this.mCreative.addIcons(collection);
    }
}
