package com.yandex.mobile.ads.video.models.ad;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.impl.tf;
import java.util.Arrays;

public class Icon implements Parcelable {
    public static final Creator<Icon> CREATOR = new Creator<Icon>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new Icon[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new Icon(parcel);
        }
    };
    private String mApiFramework;
    private Long mDuration;
    private Integer mHeight;
    private IconHorizontalPosition mHorizontalPosition;
    private Long mOffset;
    private String mProgram;
    private IconResourceType mResourceType;
    private String mResourceUrl;
    private IconVerticalPosition mVerticalPosition;
    private Integer mWidth;
    private Integer mX;
    private Integer mY;

    public enum IconHorizontalPosition {
        ICON_HORIZONTAL_POSITION_LEFT("left"),
        ICON_HORIZONTAL_POSITION_RIGHT("right"),
        ICON_HORIZONTAL_POSITION_LEFT_OFFSET("leftOffset");
        
        public final String horizontalPosition;

        private IconHorizontalPosition(String str) {
            this.horizontalPosition = str;
        }

        /* access modifiers changed from: private */
        public static IconHorizontalPosition getPosition(String str) {
            if ("left".equals(str)) {
                return ICON_HORIZONTAL_POSITION_LEFT;
            }
            if ("right".equals(str)) {
                return ICON_HORIZONTAL_POSITION_RIGHT;
            }
            return ICON_HORIZONTAL_POSITION_LEFT_OFFSET;
        }
    }

    public enum IconResourceType {
        STATIC_RESOURCE("StaticResource"),
        IFRAME_RESOURCE("IFrameResource"),
        HTML_RESOURCE("HTMLResource");
        
        public final String resourceType;

        private IconResourceType(String str) {
            this.resourceType = str;
        }

        /* access modifiers changed from: private */
        public static IconResourceType getConvertType(String str) {
            for (IconResourceType iconResourceType : Arrays.asList(values())) {
                if (iconResourceType.resourceType.equals(str)) {
                    return iconResourceType;
                }
            }
            return null;
        }

        public static boolean contains(String str) {
            for (IconResourceType iconResourceType : Arrays.asList(values())) {
                if (iconResourceType.resourceType.equals(str)) {
                    return true;
                }
            }
            return false;
        }
    }

    public enum IconVerticalPosition {
        ICON_VERTICAL_POSITION_TOP(String.TOP),
        ICON_VERTICAL_POSITION_BOTTOM(String.BOTTOM),
        ICON_VERTICAL_POSITION_TOP_OFFSET("topOffset");
        
        public final String verticalPosition;

        private IconVerticalPosition(String str) {
            this.verticalPosition = str;
        }

        /* access modifiers changed from: private */
        public static IconVerticalPosition getPosition(String str) {
            if (String.TOP.equals(str)) {
                return ICON_VERTICAL_POSITION_TOP;
            }
            if (String.BOTTOM.equals(str)) {
                return ICON_VERTICAL_POSITION_BOTTOM;
            }
            return ICON_VERTICAL_POSITION_TOP_OFFSET;
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getResourceUrl() {
        return this.mResourceUrl;
    }

    public void setResourceUrl(String str) {
        this.mResourceUrl = str;
    }

    public IconResourceType getResourceType() {
        return this.mResourceType;
    }

    public void setResourceType(String str) {
        this.mResourceType = IconResourceType.getConvertType(str);
    }

    public String getProgram() {
        return this.mProgram;
    }

    public void setProgram(String str) {
        this.mProgram = str;
    }

    public Integer getWidth() {
        return this.mWidth;
    }

    public void setWidth(String str) {
        this.mWidth = tf.b(str);
    }

    public Integer getHeight() {
        return this.mHeight;
    }

    public void setHeight(String str) {
        this.mHeight = tf.b(str);
    }

    public IconHorizontalPosition getHorizontalPosition() {
        return this.mHorizontalPosition;
    }

    public void setHorizontalPosition(String str) {
        this.mHorizontalPosition = IconHorizontalPosition.getPosition(str);
        if (this.mHorizontalPosition == IconHorizontalPosition.ICON_HORIZONTAL_POSITION_LEFT_OFFSET) {
            this.mX = tf.b(str);
        }
    }

    public IconVerticalPosition getVerticalPosition() {
        return this.mVerticalPosition;
    }

    public void setVerticalPosition(String str) {
        this.mVerticalPosition = IconVerticalPosition.getPosition(str);
        if (this.mVerticalPosition == IconVerticalPosition.ICON_VERTICAL_POSITION_TOP_OFFSET) {
            this.mY = tf.b(str);
        }
    }

    public String getApiFramework() {
        return this.mApiFramework;
    }

    public void setApiFramework(String str) {
        this.mApiFramework = str;
    }

    public Integer getXPosition() {
        return this.mX;
    }

    public Integer getYPosition() {
        return this.mY;
    }

    public Long getOffset() {
        return this.mOffset;
    }

    public void setOffset(String str) {
        this.mOffset = tf.a(str);
    }

    public Long getDuration() {
        return this.mDuration;
    }

    public void setDuration(String str) {
        this.mDuration = tf.a(str);
    }

    public Icon() {
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mResourceUrl);
        int i2 = -1;
        parcel.writeInt(this.mResourceType == null ? -1 : this.mResourceType.ordinal());
        parcel.writeString(this.mProgram);
        parcel.writeValue(this.mWidth);
        parcel.writeValue(this.mHeight);
        parcel.writeInt(this.mHorizontalPosition == null ? -1 : this.mHorizontalPosition.ordinal());
        if (this.mVerticalPosition != null) {
            i2 = this.mVerticalPosition.ordinal();
        }
        parcel.writeInt(i2);
        parcel.writeString(this.mApiFramework);
        parcel.writeValue(this.mOffset);
        parcel.writeValue(this.mDuration);
        parcel.writeValue(this.mX);
        parcel.writeValue(this.mY);
    }

    private Icon(Parcel parcel) {
        IconResourceType iconResourceType;
        IconHorizontalPosition iconHorizontalPosition;
        this.mResourceUrl = parcel.readString();
        int readInt = parcel.readInt();
        IconVerticalPosition iconVerticalPosition = null;
        if (readInt == -1) {
            iconResourceType = null;
        } else {
            iconResourceType = IconResourceType.values()[readInt];
        }
        this.mResourceType = iconResourceType;
        this.mProgram = parcel.readString();
        this.mWidth = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.mHeight = (Integer) parcel.readValue(Integer.class.getClassLoader());
        int readInt2 = parcel.readInt();
        if (readInt2 == -1) {
            iconHorizontalPosition = null;
        } else {
            iconHorizontalPosition = IconHorizontalPosition.values()[readInt2];
        }
        this.mHorizontalPosition = iconHorizontalPosition;
        int readInt3 = parcel.readInt();
        if (readInt3 != -1) {
            iconVerticalPosition = IconVerticalPosition.values()[readInt3];
        }
        this.mVerticalPosition = iconVerticalPosition;
        this.mApiFramework = parcel.readString();
        this.mOffset = (Long) parcel.readValue(Long.class.getClassLoader());
        this.mDuration = (Long) parcel.readValue(Long.class.getClassLoader());
        this.mX = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.mY = (Integer) parcel.readValue(Integer.class.getClassLoader());
    }
}
