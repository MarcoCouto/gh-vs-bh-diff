package com.yandex.mobile.ads.video.models.ad;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class MediaFile implements Parcelable {
    public static final Creator<MediaFile> CREATOR = new Creator<MediaFile>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new MediaFile[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new MediaFile(parcel);
        }
    };
    public int mBitrate;
    private DeliveryMethod mDeliveryMethod;
    private int mHeight;
    private String mId;
    private String mMimeType;
    private String mUri;
    private int mWidth;

    public enum DeliveryMethod {
        STREAMING("streaming"),
        PROGRESSIVE("progressive");
        
        private String mDeliveryMethod;

        private DeliveryMethod(String str) {
            this.mDeliveryMethod = str;
        }

        public static DeliveryMethod getByMethod(String str) {
            DeliveryMethod[] values;
            for (DeliveryMethod deliveryMethod : values()) {
                if (deliveryMethod.mDeliveryMethod.equals(str)) {
                    return deliveryMethod;
                }
            }
            return null;
        }
    }

    public final int describeContents() {
        return 0;
    }

    public final String getId() {
        return this.mId;
    }

    private void setId(String str) {
        this.mId = str;
    }

    public final String getUri() {
        return this.mUri;
    }

    private void setUri(String str) {
        this.mUri = str;
    }

    public final DeliveryMethod getDeliveryMethod() {
        return this.mDeliveryMethod;
    }

    private void setDeliveryMethod(String str) {
        this.mDeliveryMethod = DeliveryMethod.getByMethod(str);
    }

    public final int getWidth() {
        return this.mWidth;
    }

    private void setWidth(String str) {
        try {
            this.mWidth = Integer.valueOf(str).intValue();
        } catch (Exception unused) {
        }
    }

    public final int getHeight() {
        return this.mHeight;
    }

    private void setHeight(String str) {
        try {
            this.mHeight = Integer.valueOf(str).intValue();
        } catch (Exception unused) {
        }
    }

    public final int getBitrate() {
        return this.mBitrate;
    }

    private void setBitrate(String str) {
        try {
            this.mBitrate = Integer.valueOf(str).intValue();
        } catch (Exception unused) {
        }
    }

    public final String getMimeType() {
        return this.mMimeType;
    }

    public final void setMimeType(String str) {
        this.mMimeType = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MediaFile mediaFile = (MediaFile) obj;
        if (this.mHeight != mediaFile.mHeight || this.mWidth != mediaFile.mWidth || this.mDeliveryMethod != mediaFile.mDeliveryMethod) {
            return false;
        }
        if (this.mId == null ? mediaFile.mId != null : !this.mId.equals(mediaFile.mId)) {
            return false;
        }
        if (this.mMimeType == null ? mediaFile.mMimeType == null : this.mMimeType.equals(mediaFile.mMimeType)) {
            return this.mUri == null ? mediaFile.mUri == null : this.mUri.equals(mediaFile.mUri);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((this.mId != null ? this.mId.hashCode() : 0) * 31) + (this.mUri != null ? this.mUri.hashCode() : 0)) * 31) + (this.mDeliveryMethod != null ? this.mDeliveryMethod.hashCode() : 0)) * 31) + this.mHeight) * 31) + this.mWidth) * 31;
        if (this.mMimeType != null) {
            i = this.mMimeType.hashCode();
        }
        return hashCode + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mId);
        parcel.writeString(this.mUri);
        parcel.writeInt(this.mDeliveryMethod == null ? -1 : this.mDeliveryMethod.ordinal());
        parcel.writeInt(this.mHeight);
        parcel.writeInt(this.mWidth);
        parcel.writeString(this.mMimeType);
    }

    private MediaFile() {
    }

    private MediaFile(Parcel parcel) {
        DeliveryMethod deliveryMethod;
        this.mId = parcel.readString();
        this.mUri = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt == -1) {
            deliveryMethod = null;
        } else {
            deliveryMethod = DeliveryMethod.values()[readInt];
        }
        this.mDeliveryMethod = deliveryMethod;
        this.mHeight = parcel.readInt();
        this.mWidth = parcel.readInt();
        this.mMimeType = parcel.readString();
    }
}
