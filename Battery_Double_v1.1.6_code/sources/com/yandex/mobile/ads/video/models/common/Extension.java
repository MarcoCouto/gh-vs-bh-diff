package com.yandex.mobile.ads.video.models.common;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public final class Extension implements Parcelable {
    public static final Creator<Extension> CREATOR = new Creator<Extension>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new Extension[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new Extension(parcel);
        }
    };
    @NonNull
    private final String a;
    @NonNull
    private final String b;

    public final int describeContents() {
        return 0;
    }

    Extension(@NonNull String str, @NonNull String str2) {
        this.a = str;
        this.b = str2;
    }

    @NonNull
    public final String getType() {
        return this.a;
    }

    @NonNull
    public final String getValue() {
        return this.b;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
    }

    protected Extension(@NonNull Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
    }
}
