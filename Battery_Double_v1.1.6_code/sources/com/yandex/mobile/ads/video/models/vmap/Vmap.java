package com.yandex.mobile.ads.video.models.vmap;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.common.Extension;
import java.util.List;

public final class Vmap implements Parcelable {
    public static final Creator<Vmap> CREATOR = new Creator<Vmap>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new Vmap[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new Vmap(parcel);
        }
    };
    @NonNull
    private final String a;
    @NonNull
    private final List<AdBreak> b;
    @NonNull
    private final List<Extension> c;

    public final int describeContents() {
        return 0;
    }

    Vmap(@NonNull String str, @NonNull List<AdBreak> list, @NonNull List<Extension> list2) {
        this.a = str;
        this.b = list;
        this.c = list2;
    }

    @NonNull
    public final String getVersion() {
        return this.a;
    }

    @NonNull
    public final List<AdBreak> getAdBreaks() {
        return this.b;
    }

    @NonNull
    public final List<Extension> getExtensions() {
        return this.c;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeTypedList(this.b);
        parcel.writeTypedList(this.c);
    }

    protected Vmap(@NonNull Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.createTypedArrayList(AdBreak.CREATOR);
        this.c = parcel.createTypedArrayList(Extension.CREATOR);
    }
}
