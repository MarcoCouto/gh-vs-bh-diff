package com.yandex.mobile.ads.video.models.vmap;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public final class TimeOffset implements Parcelable {
    public static final Creator<TimeOffset> CREATOR = new Creator<TimeOffset>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new TimeOffset[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new TimeOffset(parcel);
        }
    };
    @NonNull
    private final String a;

    public final int describeContents() {
        return 0;
    }

    TimeOffset(@NonNull String str) {
        this.a = str;
    }

    @NonNull
    public final String getRawValue() {
        return this.a;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.a);
    }

    protected TimeOffset(@NonNull Parcel parcel) {
        this.a = parcel.readString();
    }
}
