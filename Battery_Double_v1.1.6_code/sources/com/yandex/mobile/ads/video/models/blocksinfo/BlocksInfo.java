package com.yandex.mobile.ads.video.models.blocksinfo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.yandex.mobile.ads.impl.ia;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class BlocksInfo implements Parcelable {
    public static final Creator<BlocksInfo> CREATOR = new Creator<BlocksInfo>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new BlocksInfo[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new BlocksInfo(parcel);
        }
    };
    private static final int INVALID_POSITIVE_VALUE = -1;
    private List<Block> mBlocks;
    private int mBufferEmptyLimit;
    private int mBufferFullTimeout;
    private String mCategoryId;
    private String mCategoryName;
    private int mFirstBuffTimeout;
    private String mPartnerId;
    private String mSessionId;
    private boolean mShowSkipTimeLeft;
    private boolean mShowTimeLeft;
    private String mSkin;
    private int mSkinTimeout;
    private int mSkipDelay;
    private String mTitle;
    private boolean mVPaidEnabled;
    private int mVastTimeout;
    private int mVersion;
    private int mVideoTimeout;
    private int mVpaidTimeout;
    private int mWrapperMaxCount;
    private int mWrapperTimeout;

    public final int describeContents() {
        return 0;
    }

    public final int getVersion() {
        return this.mVersion;
    }

    private void setVersion(String str) {
        this.mVersion = parsePositiveIntOrInvalidValue(str);
    }

    public final String getPartnerId() {
        return this.mPartnerId;
    }

    private void setPartnerId(String str) {
        this.mPartnerId = str;
    }

    public final String getSessionId() {
        return this.mSessionId;
    }

    private void setSessionId(String str) {
        this.mSessionId = str;
    }

    public final String getCategoryId() {
        return this.mCategoryId;
    }

    private void setCategoryId(String str) {
        this.mCategoryId = str;
    }

    public final String getCategoryName() {
        return this.mCategoryName;
    }

    private void setCategoryName(String str) {
        this.mCategoryName = str;
    }

    public final String getSkin() {
        return this.mSkin;
    }

    private void setSkin(String str) {
        this.mSkin = str;
    }

    public final boolean isVPaidEnabled() {
        return this.mVPaidEnabled;
    }

    private void setVPaidEnabled(String str) {
        this.mVPaidEnabled = parseBooleanWithDefault(str, this.mVPaidEnabled);
    }

    public final int getBufferEmptyLimit() {
        return this.mBufferEmptyLimit;
    }

    private void setBufferEmptyLimit(String str) {
        this.mBufferEmptyLimit = parsePositiveIntOrInvalidValue(str);
    }

    public final String getTitle() {
        return this.mTitle;
    }

    private void setTitle(String str) {
        this.mTitle = str;
    }

    public final int getSkipDelay() {
        return this.mSkipDelay;
    }

    private void setSkipDelay(String str) {
        this.mSkipDelay = parsePositiveIntOrInvalidValue(str);
    }

    public final boolean showSkipTimeLeft() {
        return this.mShowSkipTimeLeft;
    }

    private void setShowSkipTimeLeft(String str) {
        this.mShowSkipTimeLeft = parseBooleanWithDefault(str, this.mShowSkipTimeLeft);
    }

    public final boolean showTimeLeft() {
        return this.mShowTimeLeft;
    }

    private void setShowTimeLeft(String str) {
        this.mShowTimeLeft = parseBooleanWithDefault(str, this.mShowTimeLeft);
    }

    public final int getVastTimeout() {
        return this.mVastTimeout;
    }

    private void setVastTimeout(String str) {
        this.mVastTimeout = parsePositiveIntOrInvalidValue(str);
    }

    public final int getVideoTimeout() {
        return this.mVideoTimeout;
    }

    private void setVideoTimeout(String str) {
        this.mVideoTimeout = parsePositiveIntOrInvalidValue(str);
    }

    public final int getWrapperTimeout() {
        return this.mWrapperTimeout;
    }

    private void setWrapperTimeout(String str) {
        this.mWrapperTimeout = parsePositiveIntOrInvalidValue(str);
    }

    public final int getVpaidTimeout() {
        return this.mVpaidTimeout;
    }

    private void setVpaidTimeout(String str) {
        this.mVpaidTimeout = parsePositiveIntOrInvalidValue(str);
    }

    public final int getSkinTimeout() {
        return this.mSkinTimeout;
    }

    private void setSkinTimeout(String str) {
        this.mSkinTimeout = parsePositiveIntOrInvalidValue(str);
    }

    public final int getBufferFullTimeout() {
        return this.mBufferFullTimeout;
    }

    private void setBufferFullTimeout(String str) {
        this.mBufferFullTimeout = parsePositiveIntOrInvalidValue(str);
    }

    public final int getWrapperMaxCount() {
        return this.mWrapperMaxCount;
    }

    private void setWrapperMaxCount(String str) {
        this.mWrapperMaxCount = parsePositiveIntOrInvalidValue(str);
    }

    public final int getFirstBuffTimeout() {
        return this.mFirstBuffTimeout;
    }

    private void setFirstBuffTimeout(String str) {
        this.mFirstBuffTimeout = parsePositiveIntOrInvalidValue(str);
    }

    private static int parsePositiveIntOrInvalidValue(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception unused) {
            return -1;
        }
    }

    private static boolean parseBooleanWithDefault(String str, boolean z) {
        try {
            return Boolean.parseBoolean(str);
        } catch (Exception unused) {
            return z;
        }
    }

    public final List<Block> getBlocks() {
        return this.mBlocks;
    }

    private void addBlock(Block block) {
        this.mBlocks.add(block);
    }

    private void addBlocks(Collection<Block> collection) {
        for (Block addBlock : ia.a(collection)) {
            addBlock(addBlock);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("mVersion=");
        sb.append(this.mVersion);
        sb.append('\'');
        sb.append("\nmPartnerId='");
        sb.append(this.mPartnerId);
        sb.append('\'');
        sb.append("\nmSessionId='");
        sb.append(this.mSessionId);
        sb.append('\'');
        sb.append("\nmCategoryId='");
        sb.append(this.mCategoryId);
        sb.append('\'');
        sb.append("\nmCategoryName='");
        sb.append(this.mCategoryName);
        sb.append('\'');
        sb.append("\nmSkin='");
        sb.append(this.mSkin);
        sb.append('\'');
        sb.append("\nmVPaidEnabled=");
        sb.append(this.mVPaidEnabled);
        sb.append("\nmBufferEmptyLimit=");
        sb.append(this.mBufferEmptyLimit);
        sb.append("\nmTitle='");
        sb.append(this.mTitle);
        sb.append('\'');
        sb.append("\nmSkipDelay=");
        sb.append(this.mSkipDelay);
        sb.append("\nmSkinTimeout=");
        sb.append(this.mSkinTimeout);
        sb.append("\nmVpaidTimeout=");
        sb.append(this.mVpaidTimeout);
        sb.append("\nmWrapperTimeout=");
        sb.append(this.mWrapperTimeout);
        sb.append("\nmVideoTimeout=");
        sb.append(this.mVideoTimeout);
        sb.append("\nmVastTimeout=");
        sb.append(this.mVastTimeout);
        sb.append("\nmShowTimeLeft=");
        sb.append(this.mShowTimeLeft);
        sb.append("\nmShowSkipTimeLeft=");
        sb.append(this.mShowSkipTimeLeft);
        sb.append("\nmBufferFullTimeout=");
        sb.append(this.mBufferFullTimeout);
        sb.append("\nmWrapperMaxCount=");
        sb.append(this.mWrapperMaxCount);
        sb.append("\nmFirstBuffTimeout=");
        sb.append(this.mFirstBuffTimeout);
        sb.append("\nmBlocks.size()=");
        sb.append(this.mBlocks.size());
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BlocksInfo blocksInfo = (BlocksInfo) obj;
        if (this.mBufferEmptyLimit != blocksInfo.mBufferEmptyLimit || this.mBufferFullTimeout != blocksInfo.mBufferFullTimeout || this.mFirstBuffTimeout != blocksInfo.mFirstBuffTimeout || this.mVPaidEnabled != blocksInfo.mVPaidEnabled || this.mShowSkipTimeLeft != blocksInfo.mShowSkipTimeLeft || this.mShowTimeLeft != blocksInfo.mShowTimeLeft || this.mSkinTimeout != blocksInfo.mSkinTimeout || this.mSkipDelay != blocksInfo.mSkipDelay || this.mVastTimeout != blocksInfo.mVastTimeout || this.mVersion != blocksInfo.mVersion || this.mVideoTimeout != blocksInfo.mVideoTimeout || this.mVpaidTimeout != blocksInfo.mVpaidTimeout || this.mWrapperMaxCount != blocksInfo.mWrapperMaxCount || this.mWrapperTimeout != blocksInfo.mWrapperTimeout) {
            return false;
        }
        if (this.mBlocks == null ? blocksInfo.mBlocks != null : !this.mBlocks.equals(blocksInfo.mBlocks)) {
            return false;
        }
        if (this.mCategoryId == null ? blocksInfo.mCategoryId != null : !this.mCategoryId.equals(blocksInfo.mCategoryId)) {
            return false;
        }
        if (this.mCategoryName == null ? blocksInfo.mCategoryName != null : !this.mCategoryName.equals(blocksInfo.mCategoryName)) {
            return false;
        }
        if (this.mPartnerId == null ? blocksInfo.mPartnerId != null : !this.mPartnerId.equals(blocksInfo.mPartnerId)) {
            return false;
        }
        if (this.mSessionId == null ? blocksInfo.mSessionId != null : !this.mSessionId.equals(blocksInfo.mSessionId)) {
            return false;
        }
        if (this.mSkin == null ? blocksInfo.mSkin == null : this.mSkin.equals(blocksInfo.mSkin)) {
            return this.mTitle == null ? blocksInfo.mTitle == null : this.mTitle.equals(blocksInfo.mTitle);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((((((((((((((((((((this.mVersion * 31) + (this.mPartnerId != null ? this.mPartnerId.hashCode() : 0)) * 31) + (this.mSessionId != null ? this.mSessionId.hashCode() : 0)) * 31) + (this.mCategoryId != null ? this.mCategoryId.hashCode() : 0)) * 31) + (this.mCategoryName != null ? this.mCategoryName.hashCode() : 0)) * 31) + (this.mSkin != null ? this.mSkin.hashCode() : 0)) * 31) + (this.mVPaidEnabled ? 1 : 0)) * 31) + this.mBufferEmptyLimit) * 31) + (this.mTitle != null ? this.mTitle.hashCode() : 0)) * 31) + this.mSkipDelay) * 31) + this.mSkinTimeout) * 31) + this.mVpaidTimeout) * 31) + this.mWrapperTimeout) * 31) + this.mVideoTimeout) * 31) + this.mVastTimeout) * 31) + (this.mShowTimeLeft ? 1 : 0)) * 31) + (this.mShowSkipTimeLeft ? 1 : 0)) * 31) + this.mBufferFullTimeout) * 31) + this.mWrapperMaxCount) * 31) + this.mFirstBuffTimeout) * 31;
        if (this.mBlocks != null) {
            i = this.mBlocks.hashCode();
        }
        return hashCode + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mVersion);
        parcel.writeString(this.mPartnerId);
        parcel.writeString(this.mSessionId);
        parcel.writeString(this.mCategoryId);
        parcel.writeString(this.mCategoryName);
        parcel.writeString(this.mSkin);
        parcel.writeByte(this.mVPaidEnabled ? (byte) 1 : 0);
        parcel.writeInt(this.mBufferEmptyLimit);
        parcel.writeString(this.mTitle);
        parcel.writeInt(this.mSkipDelay);
        parcel.writeInt(this.mSkinTimeout);
        parcel.writeInt(this.mVpaidTimeout);
        parcel.writeInt(this.mWrapperTimeout);
        parcel.writeInt(this.mVideoTimeout);
        parcel.writeInt(this.mVastTimeout);
        parcel.writeByte(this.mShowTimeLeft ? (byte) 1 : 0);
        parcel.writeByte(this.mShowSkipTimeLeft ? (byte) 1 : 0);
        parcel.writeInt(this.mBufferFullTimeout);
        parcel.writeInt(this.mWrapperMaxCount);
        parcel.writeInt(this.mFirstBuffTimeout);
        parcel.writeTypedList(this.mBlocks);
    }

    private BlocksInfo() {
        this.mVPaidEnabled = false;
        this.mSkipDelay = -1;
        this.mSkinTimeout = -1;
        this.mVpaidTimeout = -1;
        this.mWrapperTimeout = -1;
        this.mVideoTimeout = -1;
        this.mVastTimeout = -1;
        this.mShowTimeLeft = false;
        this.mShowSkipTimeLeft = false;
        this.mBufferFullTimeout = -1;
        this.mWrapperMaxCount = -1;
        this.mFirstBuffTimeout = -1;
        this.mBlocks = new ArrayList();
    }

    private BlocksInfo(Parcel parcel) {
        boolean z = false;
        this.mVPaidEnabled = false;
        this.mSkipDelay = -1;
        this.mSkinTimeout = -1;
        this.mVpaidTimeout = -1;
        this.mWrapperTimeout = -1;
        this.mVideoTimeout = -1;
        this.mVastTimeout = -1;
        this.mShowTimeLeft = false;
        this.mShowSkipTimeLeft = false;
        this.mBufferFullTimeout = -1;
        this.mWrapperMaxCount = -1;
        this.mFirstBuffTimeout = -1;
        this.mBlocks = new ArrayList();
        this.mVersion = parcel.readInt();
        this.mPartnerId = parcel.readString();
        this.mSessionId = parcel.readString();
        this.mCategoryId = parcel.readString();
        this.mCategoryName = parcel.readString();
        this.mSkin = parcel.readString();
        this.mVPaidEnabled = parcel.readByte() != 0;
        this.mBufferEmptyLimit = parcel.readInt();
        this.mTitle = parcel.readString();
        this.mSkipDelay = parcel.readInt();
        this.mSkinTimeout = parcel.readInt();
        this.mVpaidTimeout = parcel.readInt();
        this.mWrapperTimeout = parcel.readInt();
        this.mVideoTimeout = parcel.readInt();
        this.mVastTimeout = parcel.readInt();
        this.mShowTimeLeft = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z = true;
        }
        this.mShowSkipTimeLeft = z;
        this.mBufferFullTimeout = parcel.readInt();
        this.mWrapperMaxCount = parcel.readInt();
        this.mFirstBuffTimeout = parcel.readInt();
        parcel.readTypedList(this.mBlocks, Block.CREATOR);
    }
}
