package com.yandex.mobile.ads.video;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.tl;
import com.yandex.mobile.ads.video.VideoAdLoader.OnVideoAdLoadedListener;

public final class a implements RequestListener<tl> {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public OnVideoAdLoadedListener c;

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        final tl tlVar = (tl) obj;
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onRawVideoAdLoaded(tlVar.b());
                        a.this.c.onVideoAdLoaded(tlVar.a().b());
                    }
                }
            }
        });
    }

    a() {
    }

    public final void onFailure(@NonNull final VideoAdError videoAdError) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onVideoAdFailedToLoad(videoAdError);
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable OnVideoAdLoadedListener onVideoAdLoadedListener) {
        synchronized (a) {
            this.c = onVideoAdLoadedListener;
        }
    }
}
