package com.yandex.mobile.ads.video;

import com.yandex.mobile.ads.impl.tn;
import com.yandex.mobile.ads.impl.to;

public final class VideoAdError {
    private final int a;
    private final String b;
    private final String c;

    public interface Code {
    }

    private VideoAdError(int i, String str) {
        this(i, str, 0);
    }

    private VideoAdError(int i, String str, byte b2) {
        this.a = i;
        this.b = str;
        this.c = null;
    }

    public final int getCode() {
        return this.a;
    }

    public final String getDescription() {
        return this.b;
    }

    public final String getRawResponse() {
        return this.c;
    }

    public static VideoAdError createInternalError(to toVar) {
        return new VideoAdError(1, "Internal error. Failed to parse response");
    }

    public static VideoAdError createRetriableError(String str) {
        return new VideoAdError(4, str);
    }

    public static VideoAdError createInternalError(String str) {
        return new VideoAdError(1, str);
    }

    public static final VideoAdError createNoAdError(tn tnVar) {
        return new VideoAdError(3, tnVar.getMessage());
    }

    public static VideoAdError createConnectionError(String str) {
        return new VideoAdError(2, str);
    }
}
