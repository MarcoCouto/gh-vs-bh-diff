package com.yandex.mobile.ads.video;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gh;
import com.yandex.mobile.ads.impl.gj;
import com.yandex.mobile.ads.impl.th;

public final class b {
    private static final Object a = new Object();
    private static volatile b b;
    @NonNull
    private final th c;
    @NonNull
    private final gj d;
    @Nullable
    private gj e;

    @NonNull
    public static b a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new b(context);
                }
            }
        }
        return b;
    }

    private b(@NonNull Context context) {
        this.c = th.a(context);
        this.d = gh.a(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull BlocksInfoRequest blocksInfoRequest) {
        this.c.a(context, blocksInfoRequest, a());
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull VideoAdRequest videoAdRequest) {
        this.c.a(context, videoAdRequest, a());
    }

    @NonNull
    private gj a() {
        return this.e != null ? this.e : this.d;
    }
}
