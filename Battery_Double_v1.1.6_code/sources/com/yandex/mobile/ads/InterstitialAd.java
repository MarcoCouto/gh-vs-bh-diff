package com.yandex.mobile.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ac;
import com.yandex.mobile.ads.impl.dr;
import com.yandex.mobile.ads.impl.ig;
import com.yandex.mobile.ads.impl.jr;
import com.yandex.mobile.ads.impl.js;

public final class InterstitialAd extends ig {
    @NonNull
    private final js a;

    public InterstitialAd(@NonNull Context context) {
        super(context);
        jr jrVar = new jr(context);
        this.a = new js(context, jrVar);
        jrVar.a(this.a.r());
    }

    public final void setBlockId(String str) {
        this.a.a_(str);
    }

    public final String getBlockId() {
        return this.a.q();
    }

    public final void setInterstitialEventListener(InterstitialEventListener interstitialEventListener) {
        this.a.a(interstitialEventListener);
    }

    public final InterstitialEventListener getInterstitialEventListener() {
        return this.a.E();
    }

    public final void loadAd(AdRequest adRequest) {
        this.a.a(adRequest);
    }

    public final void shouldOpenLinksInApp(boolean z) {
        this.a.a_(z);
    }

    public final void show() {
        if (this.a.z()) {
            this.a.a();
        }
    }

    public final boolean isLoaded() {
        return this.a.z();
    }

    public final void destroy() {
        if (!dr.a((ac) this.a)) {
            this.a.e();
        }
    }
}
