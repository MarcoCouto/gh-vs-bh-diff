package com.yandex.mobile.ads.rewarded;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.cr;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.ib;
import com.yandex.mobile.ads.impl.ii;

public final class a implements ii {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public RewardedAdEventListener c;
    @NonNull
    private final cr d;

    public a(@NonNull Context context) {
        this.d = new cr(context);
    }

    public final void a(@NonNull fl flVar) {
        this.d.a(flVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable RewardedAdEventListener rewardedAdEventListener) {
        synchronized (a) {
            this.c = rewardedAdEventListener;
        }
    }

    public final void b() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdDismissed();
                    }
                }
            }
        });
    }

    public final void a(@NonNull final AdRequestError adRequestError) {
        this.d.a(adRequestError);
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdFailedToLoad(adRequestError);
                    }
                }
            }
        });
    }

    public final void e() {
        this.d.a();
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdLoaded();
                    }
                }
            }
        });
    }

    public final void g() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdShown();
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdClosed();
                    }
                }
            }
        });
    }

    public final void c() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        ib.a((Object) a.this.c, "onAdImpressionTracked", new Object[0]);
                    }
                }
            }
        });
    }

    public final void d() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdLeftApplication();
                    }
                }
            }
        });
    }

    public final void f() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onAdOpened();
                    }
                }
            }
        });
    }

    public final void a(@NonNull final Reward reward) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (a.a) {
                    if (a.this.c != null) {
                        a.this.c.onRewarded(reward);
                    }
                }
            }
        });
    }
}
