package com.yandex.mobile.ads.rewarded;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;

public abstract class RewardedAdEventListener {

    public static class SimpleRewardedAdEventListener extends RewardedAdEventListener {
        public void onAdClosed() {
        }

        public void onAdDismissed() {
        }

        public void onAdFailedToLoad(AdRequestError adRequestError) {
        }

        public void onAdLeftApplication() {
        }

        public void onAdLoaded() {
        }

        public void onAdOpened() {
        }

        public void onAdShown() {
        }

        public void onRewarded(@NonNull Reward reward) {
        }
    }

    public abstract void onAdClosed();

    public abstract void onAdDismissed();

    public abstract void onAdFailedToLoad(AdRequestError adRequestError);

    public abstract void onAdLeftApplication();

    public abstract void onAdLoaded();

    public abstract void onAdOpened();

    public abstract void onAdShown();

    public abstract void onRewarded(@NonNull Reward reward);
}
