package com.yandex.mobile.ads.rewarded;

import android.support.annotation.NonNull;

public final class c implements Reward {
    private final int a;
    @NonNull
    private final String b;

    public c(int i, @NonNull String str) {
        this.a = i;
        this.b = str;
    }

    public final int getAmount() {
        return this.a;
    }

    @NonNull
    public final String getType() {
        return this.b;
    }
}
