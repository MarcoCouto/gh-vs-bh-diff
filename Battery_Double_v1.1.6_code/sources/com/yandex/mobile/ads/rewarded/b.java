package com.yandex.mobile.ads.rewarded;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bp;
import com.yandex.mobile.ads.impl.ef;
import com.yandex.mobile.ads.impl.ij;
import com.yandex.mobile.ads.impl.kj;
import com.yandex.mobile.ads.impl.kk;
import com.yandex.mobile.ads.impl.ta;
import com.yandex.mobile.ads.impl.tb;
import com.yandex.mobile.ads.impl.u;
import com.yandex.mobile.ads.impl.w;

public final class b extends ij {
    @NonNull
    private final a h;
    @NonNull
    private final ef i = new ef();
    @NonNull
    private final tb j;
    @Nullable
    private ta k;

    public b(@NonNull Context context, @NonNull a aVar) {
        super(context, com.yandex.mobile.ads.b.REWARDED, aVar);
        this.h = aVar;
        this.j = new tb(aVar);
    }

    public final void a(int i2, @Nullable Bundle bundle) {
        if (i2 != 13) {
            super.a(i2, bundle);
        } else {
            E();
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final kj a(@NonNull kk kkVar) {
        return kkVar.a(this);
    }

    public final void a(@NonNull w<String> wVar) {
        bp o = wVar.o();
        boolean z = false;
        if (o != null && (!o.c() ? o.a() != null : o.b() != null)) {
            z = true;
        }
        if (z) {
            super.a(wVar);
        } else {
            onAdFailedToLoad(u.e);
        }
    }

    public final void a() {
        this.k = this.j.a(this.b, this.f, this.g);
        super.a();
    }

    public final void E() {
        if (this.k != null) {
            this.k.a();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable RewardedAdEventListener rewardedAdEventListener) {
        this.h.a(rewardedAdEventListener);
    }

    /* access modifiers changed from: 0000 */
    public final void c(@Nullable String str) {
        this.f.e(str);
    }
}
