package com.yandex.mobile.ads.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.impl.ac;
import com.yandex.mobile.ads.impl.dr;
import com.yandex.mobile.ads.impl.ig;

public final class RewardedAd extends ig {
    @NonNull
    private final b a;

    public RewardedAd(@NonNull Context context) {
        super(context);
        a aVar = new a(context);
        this.a = new b(context, aVar);
        aVar.a(this.a.r());
    }

    public final String getBlockId() {
        return this.a.q();
    }

    public final void loadAd(AdRequest adRequest) {
        this.a.a(adRequest);
    }

    public final boolean isLoaded() {
        return this.a.z();
    }

    public final void setBlockId(String str) {
        this.a.a_(str);
    }

    public final void setUserId(String str) {
        this.a.c(str);
    }

    public final void setRewardedAdEventListener(@Nullable RewardedAdEventListener rewardedAdEventListener) {
        this.a.a(rewardedAdEventListener);
    }

    public final void shouldOpenLinksInApp(boolean z) {
        this.a.a_(z);
    }

    public final void show() {
        if (this.a.z()) {
            this.a.a();
        }
    }

    public final void destroy() {
        if (!dr.a((ac) this.a)) {
            this.a.e();
        }
    }
}
