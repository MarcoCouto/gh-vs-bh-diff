package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.yandex.mobile.ads.impl.ag;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.as;
import com.yandex.mobile.ads.impl.at;
import com.yandex.mobile.ads.impl.ax;
import com.yandex.mobile.ads.impl.ay;
import com.yandex.mobile.ads.impl.az;
import com.yandex.mobile.ads.impl.cl;
import com.yandex.mobile.ads.impl.cq;
import com.yandex.mobile.ads.impl.dr;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.lg;
import com.yandex.mobile.ads.impl.nn;
import com.yandex.mobile.ads.impl.ny;
import com.yandex.mobile.ads.impl.ri;
import com.yandex.mobile.ads.impl.rj;
import com.yandex.mobile.ads.impl.w;
import java.util.List;

public abstract class ba {
    /* access modifiers changed from: private */
    @NonNull
    public final Context a;
    @NonNull
    private final bf b;
    /* access modifiers changed from: private */
    @NonNull
    public final be c;
    @NonNull
    private final bh d;
    /* access modifiers changed from: private */
    @NonNull
    public final ax e;
    @NonNull
    private final ag f;
    @NonNull
    private final bm g;
    @NonNull
    private final r h;
    @NonNull
    private final t i;
    @NonNull
    private final e j;
    @NonNull
    private final ny k;
    @NonNull
    private final fl l;
    @NonNull
    private final w m;
    @NonNull
    private final bl n;
    @NonNull
    private final rj o;
    @NonNull
    private final ri p;
    @NonNull
    private final as q;
    @NonNull
    private final cq r;
    @NonNull
    private final lg s;
    @Nullable
    private ag t;
    private final b u = new b() {
        public final void a(@NonNull Intent intent) {
            boolean z = !ba.this.c.a();
            StringBuilder sb = new StringBuilder("onPhoneStateChanged(), intent.getAction = ");
            sb.append(intent.getAction());
            sb.append(", isNativeAdViewShown = ");
            sb.append(z);
            sb.append(", clazz = ");
            sb.append(getClass());
            ba.this.e.a(intent, z);
        }
    };
    @NonNull
    private final az v = new az() {
        @NonNull
        public final am a(int i) {
            return ba.this.c.a(ba.this.a, i);
        }
    };

    enum a {
        CUSTOM("custom"),
        TEMPLATE(MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
        
        final String c;

        private a(String str) {
            this.c = str;
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public abstract List<String> a();

    public ba(@NonNull Context context, @NonNull c cVar) {
        this.a = context;
        this.b = cVar.d();
        this.c = cVar.b();
        this.d = cVar.c();
        p a2 = cVar.a();
        this.l = a2.a();
        this.m = a2.b();
        String e2 = this.l.e();
        com.yandex.mobile.ads.b a3 = this.l.a();
        this.i = cVar.e();
        this.h = this.i.a().a(context, this.l);
        this.r = new cq(context, this.l);
        this.q = new as(this.h, this.r);
        this.q.a(this.m);
        List b2 = this.d.b();
        this.q.a(b2);
        this.n = new bl();
        ny nyVar = new ny(context, this.m, this.l, this.h, this.n);
        this.k = nyVar;
        new ay();
        this.e = ay.a(this.a, this.l, this.r, this.v, dr.a(this));
        this.j = new e(this.k, this.e);
        this.f = ag.a();
        cl clVar = new cl(this.a, new an(this.c), this.m, this.l, this.d.c());
        this.g = this.i.d().a(this.e, clVar, new nn(this.c, b2), this.f);
        this.g.a((at) this.q);
        this.g.a(this.m, b2);
        List a4 = this.d.a();
        this.s = new lg(a4);
        this.o = new rj(this.a, a3, e2, a4);
        this.p = new ri(this.a, this.l, a4);
        this.p.a(a());
    }

    /* access modifiers changed from: 0000 */
    public final <T extends View> void a(@NonNull T t2, @NonNull j jVar, @NonNull ak<T> akVar, @NonNull f fVar) throws NativeAdException {
        aj a2 = aj.a();
        ba a3 = a2.a((View) t2);
        if (!equals(a3)) {
            Context context = t2.getContext();
            if (a3 != null) {
                a3.a(context);
            }
            if (a2.a(this)) {
                a(context);
            }
            a2.a(t2, this);
            ag agVar = new ag(t2, akVar, jVar, this.q, fVar, this.i, this.s);
            agVar.a();
            this.o.a(agVar);
            this.t = agVar;
            this.c.a(agVar);
            bk b2 = this.c.b();
            if (b2.b()) {
                a(agVar);
                this.b.a(agVar, this.j);
                new StringBuilder("renderAdView(), BIND, clazz = ").append(dr.a(this));
                b();
                return;
            }
            throw new NativeAdException(String.format("Resource for required view %s is not present", new Object[]{b2.a()}));
        }
    }

    private void a(@NonNull ag agVar) {
        this.b.a(agVar);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        boolean z = !this.c.a();
        StringBuilder sb = new StringBuilder("registerTrackers(), isNativeAdViewShown = ");
        sb.append(z);
        sb.append(", clazz = ");
        sb.append(dr.a(this));
        this.g.a(this.a, this.u, this.t);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        new StringBuilder("unregisterTrackers(), clazz = ").append(dr.a(this));
        this.g.a(this.a, this.u);
    }

    public final void a(int i2) {
        StringBuilder sb = new StringBuilder("onVisibilityChanged(), visibility = ");
        sb.append(i2);
        sb.append(", clazz = ");
        sb.append(dr.a(this));
        if (i2 == 0) {
            b();
        } else {
            c();
        }
    }

    public void setAdEventListener(NativeAdEventListener nativeAdEventListener) {
        this.h.a(nativeAdEventListener);
    }

    public void shouldOpenLinksInApp(boolean z) {
        this.l.a(z);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Context context) {
        c();
        this.h.h();
        if (this.t != null) {
            a(this.t);
            this.g.a(this.t);
        }
    }

    public void setAdTapHandler(@Nullable AdTapHandler adTapHandler) {
        this.n.a(adTapHandler);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull com.yandex.mobile.ads.impl.hr.a aVar) {
        this.k.a(aVar);
        this.r.a(aVar);
        this.o.a(aVar);
        this.h.a(aVar);
        this.g.a(aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final be d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bh e() {
        return this.d;
    }
}
