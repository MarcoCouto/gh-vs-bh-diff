package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.at;
import com.yandex.mobile.ads.impl.bn;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.w;
import java.util.List;

public interface bm {
    void a(@NonNull Context context, @NonNull b bVar);

    void a(@NonNull Context context, @NonNull b bVar, @Nullable ag agVar);

    void a(@NonNull at atVar);

    void a(@NonNull a aVar);

    void a(@NonNull w wVar, @NonNull List<bn> list);

    void a(@NonNull ag agVar);
}
