package com.yandex.mobile.ads.nativeads;

public final class ao {
    private static final Object a = new Object();
    private static volatile ao b;
    private boolean c = true;

    public static ao a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new ao();
                }
            }
        }
        return b;
    }

    private ao() {
    }

    public final boolean b() {
        return this.c;
    }
}
