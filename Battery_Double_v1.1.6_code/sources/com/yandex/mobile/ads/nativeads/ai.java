package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.al.a;

final class ai implements ak<View> {
    @NonNull
    private final NativeAdViewBinder a;

    @NonNull
    public final /* synthetic */ al a(@NonNull Object obj) {
        return new a((View) obj).a(this.a.getAgeView()).b(this.a.getBodyView()).c((TextView) this.a.getCallToActionView()).d(this.a.getDomainView()).a(this.a.getFaviconView()).a(this.a.getFeedbackView()).b(this.a.getIconView()).c(this.a.getImageView()).a(this.a.getMediaView()).e(this.a.getPriceView()).a(this.a.getRatingView()).f(this.a.getReviewCountView()).g(this.a.getSponsoredView()).h(this.a.getTitleView()).i(this.a.getWarningView()).a();
    }

    ai(@NonNull NativeAdViewBinder nativeAdViewBinder) {
        this.a = nativeAdViewBinder;
    }
}
