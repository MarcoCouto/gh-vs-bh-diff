package com.yandex.mobile.ads.nativeads;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.nativeads.au.a;
import com.yandex.mobile.ads.nativeads.au.b;
import java.util.List;

public final class aa extends m {
    public final /* bridge */ /* synthetic */ a a() {
        return super.a();
    }

    public final /* bridge */ /* synthetic */ void a(ag agVar) {
        super.a(agVar);
    }

    @VisibleForTesting
    public final /* bridge */ /* synthetic */ boolean b() {
        return super.b();
    }

    @VisibleForTesting
    public final /* bridge */ /* synthetic */ boolean c() {
        return super.c();
    }

    public final /* bridge */ /* synthetic */ boolean d() {
        return super.d();
    }

    @NonNull
    public final /* bridge */ /* synthetic */ bk e() {
        return super.e();
    }

    aa(@Nullable List<oj> list, @NonNull ao aoVar) {
        super(list, aoVar);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"VisibleForTests"})
    public final boolean a(@NonNull b bVar, @Nullable List<oj> list) {
        if (!this.a.b() || list == null || bVar.a(list)) {
            return true;
        }
        return false;
    }
}
