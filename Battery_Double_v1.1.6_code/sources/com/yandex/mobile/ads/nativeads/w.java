package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.impl.ok;
import com.yandex.mobile.ads.impl.om;
import com.yandex.mobile.ads.impl.oo;
import com.yandex.mobile.ads.impl.op;
import com.yandex.mobile.ads.impl.oq;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public final class w {
    @NonNull
    private final oq a;
    @NonNull
    private final j b;
    @NonNull
    private final i c;
    /* access modifiers changed from: private */
    @NonNull
    public final bf d;
    /* access modifiers changed from: private */
    @NonNull
    public final Set<NativeAdImageLoadingListener> e = new CopyOnWriteArraySet();

    public w(@NonNull Context context, @NonNull oq oqVar, @NonNull j jVar, @NonNull bf bfVar) {
        this.a = oqVar;
        this.b = jVar;
        this.d = bfVar;
        this.c = new i(context);
    }

    public final NativeAdType a() {
        return this.a.b();
    }

    public final String b() {
        return this.a.f();
    }

    public final void c() {
        this.c.a(this.c.a(Collections.singletonList(this.a)), new k() {
            public final void a(@NonNull Map<String, Bitmap> map) {
                w.this.d.a();
                for (NativeAdImageLoadingListener nativeAdImageLoadingListener : w.this.e) {
                    if (nativeAdImageLoadingListener != null) {
                        nativeAdImageLoadingListener.onFinishLoadingImages();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.e.add(nativeAdImageLoadingListener);
    }

    /* access modifiers changed from: 0000 */
    public final void b(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.e.remove(nativeAdImageLoadingListener);
    }

    @Nullable
    @VisibleForTesting
    private static <T> T a(@Nullable oj<T> ojVar) {
        if (ojVar != null) {
            return ojVar.c();
        }
        return null;
    }

    public final NativeAdAssets d() {
        l lVar = new l();
        List<oj> c2 = this.a.c();
        HashMap hashMap = new HashMap();
        for (oj ojVar : c2) {
            hashMap.put(ojVar.a(), ojVar);
        }
        op opVar = (op) a((oj) hashMap.get("media"));
        lVar.a((String) a((oj) hashMap.get(IronSourceSegment.AGE)));
        lVar.b((String) a((oj) hashMap.get(TtmlNode.TAG_BODY)));
        lVar.c((String) a((oj) hashMap.get("call_to_action")));
        lVar.a((ok) a((oj) hashMap.get("close_button")));
        lVar.d((String) a((oj) hashMap.get(RequestParameters.DOMAIN)));
        lVar.a((om) a((oj) hashMap.get("favicon")), this.b);
        lVar.b((om) a((oj) hashMap.get(SettingsJsonConstants.APP_ICON_KEY)), this.b);
        oo ooVar = null;
        lVar.c(opVar != null ? opVar.b() : null, this.b);
        if (opVar != null) {
            ooVar = opVar.a();
        }
        lVar.a(ooVar);
        lVar.e((String) a((oj) hashMap.get("price")));
        lVar.f((String) a((oj) hashMap.get("rating")));
        lVar.g((String) a((oj) hashMap.get("review_count")));
        lVar.h((String) a((oj) hashMap.get("sponsored")));
        lVar.i((String) a((oj) hashMap.get("title")));
        lVar.j((String) a((oj) hashMap.get("warning")));
        return lVar;
    }
}
