package com.yandex.mobile.ads.nativeads;

import com.facebook.share.internal.MessengerShareContentUtility;

public enum NativeAdType {
    CONTENT("content"),
    APP_INSTALL("app"),
    IMAGE(MessengerShareContentUtility.MEDIA_IMAGE);
    
    private final String a;

    private NativeAdType(String str) {
        this.a = str;
    }

    public final String getValue() {
        return this.a;
    }
}
