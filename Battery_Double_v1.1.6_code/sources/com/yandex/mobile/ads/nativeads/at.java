package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.nativeads.au.a;

final class at implements a {
    private final String a;
    private final am.a b;

    at(@NonNull am.a aVar, @Nullable String str) {
        this.a = str;
        this.b = aVar;
    }

    public final String a() {
        return this.a;
    }

    public final am.a b() {
        return this.b;
    }
}
