package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.oj;
import java.util.List;

interface au {

    public interface a {
        String a();

        com.yandex.mobile.ads.impl.am.a b();
    }

    @VisibleForTesting
    public interface b {
        boolean a(@NonNull List<oj> list);
    }

    a a();

    void a(ag agVar);

    @NonNull
    bk e();
}
