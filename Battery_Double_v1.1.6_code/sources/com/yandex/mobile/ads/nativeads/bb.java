package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.oq;
import java.util.ArrayList;
import java.util.List;

public final class bb extends n implements NativeImageAd {
    public bb(@NonNull Context context, @NonNull oq oqVar, @NonNull w wVar, @NonNull j jVar, @NonNull c cVar) {
        super(context, oqVar, wVar, jVar, cVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("media");
        return arrayList;
    }

    public final void bindImageAd(@Nullable NativeImageAdView nativeImageAdView) throws NativeAdException {
        if (nativeImageAdView != null) {
            nativeImageAdView.a(this);
            a(nativeImageAdView, new bc());
        }
    }
}
