package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bn;
import com.yandex.mobile.ads.impl.oj;
import java.util.Collections;
import java.util.List;

public final class bh {
    private final List<oj> a;
    private final List<bn> b;
    @NonNull
    private final List<String> c;

    public bh(@Nullable List<oj> list, @NonNull List<bn> list2, @NonNull List<String> list3) {
        this.a = list;
        this.b = list2;
        this.c = list3;
    }

    @NonNull
    public final List<oj> a() {
        return this.a != null ? this.a : Collections.emptyList();
    }

    @NonNull
    public final List<bn> b() {
        return this.b;
    }

    @NonNull
    public final List<String> c() {
        return this.c;
    }
}
