package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.ae;
import com.yandex.mobile.ads.impl.af;
import com.yandex.mobile.ads.impl.qz;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration.Builder;

public class NativeAdLoader {
    @NonNull
    protected final Context a;
    @NonNull
    private final v b;

    public interface OnImageAdLoadListener extends OnLoadListener {
        void onImageAdLoaded(@NonNull NativeImageAd nativeImageAd);
    }

    public interface OnLoadListener {
        void onAdFailedToLoad(@NonNull AdRequestError adRequestError);

        void onAppInstallAdLoaded(@NonNull NativeAppInstallAd nativeAppInstallAd);

        void onContentAdLoaded(@NonNull NativeContentAd nativeContentAd);
    }

    public NativeAdLoader(Context context, String str) {
        this(context, new Builder(str, true).setImageSizes(NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_SMALL).build());
    }

    public NativeAdLoader(@NonNull Context context, @NonNull NativeAdLoaderConfiguration nativeAdLoaderConfiguration) {
        this.a = context.getApplicationContext();
        x xVar = new x(context);
        this.b = new v(this.a, nativeAdLoaderConfiguration, xVar);
        xVar.a(this.b.r());
    }

    public void setOnLoadListener(@Nullable OnLoadListener onLoadListener) {
        this.b.a(onLoadListener);
    }

    public void setNativeAdLoadListener(@Nullable OnImageAdLoadListener onImageAdLoadListener) {
        this.b.a((OnLoadListener) onImageAdLoadListener);
    }

    public void loadAd(AdRequest adRequest) {
        this.b.a(adRequest, new qz(this.a), ae.AD, af.AD);
    }

    public void cancelLoading() {
        this.b.a();
    }
}
