package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View.OnClickListener;
import com.yandex.mobile.ads.impl.ax;
import com.yandex.mobile.ads.impl.mw;
import com.yandex.mobile.ads.impl.my;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.impl.on;
import com.yandex.mobile.ads.impl.os;

public abstract class f {
    @VisibleForTesting
    static final f a = new f() {
        @NonNull
        public final OnClickListener a(@NonNull oj ojVar, @Nullable on onVar, @NonNull a aVar, @NonNull ag agVar, @Nullable ax axVar) {
            mw mwVar = new mw(ojVar, aVar, agVar, onVar, axVar);
            return mwVar;
        }
    };
    @VisibleForTesting
    static final f b = new f() {
        @NonNull
        public final OnClickListener a(@NonNull oj ojVar, @Nullable on onVar, @NonNull a aVar, @NonNull ag agVar, @Nullable ax axVar) {
            if (!"call_to_action".equals(ojVar.a())) {
                return new my(agVar.d().d());
            }
            mw mwVar = new mw(ojVar, aVar, agVar, onVar, axVar);
            return mwVar;
        }
    };

    @NonNull
    public abstract OnClickListener a(@NonNull oj ojVar, @Nullable on onVar, @NonNull a aVar, @NonNull ag agVar, @Nullable ax axVar);

    @NonNull
    static f a() {
        return a;
    }

    @NonNull
    static f a(@Nullable os osVar) {
        if (osVar == null || !"button_click_only".equals(osVar.a())) {
            return a;
        }
        return b;
    }
}
