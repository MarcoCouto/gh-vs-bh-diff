package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class NativeAdUnitView extends u<ab> {
    @Nullable
    private OnTouchListener a;
    private TextView b;

    public NativeAdUnitView(Context context) {
        super(context);
    }

    public NativeAdUnitView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeAdUnitView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setSponsoredView(TextView textView) {
        this.b = textView;
    }

    /* access modifiers changed from: 0000 */
    public final TextView a() {
        return this.b;
    }

    public ab getNativeAd() {
        return (ab) super.getNativeAd();
    }

    public boolean onInterceptTouchEvent(@NonNull MotionEvent motionEvent) {
        if (this.a != null) {
            this.a.onTouch(this, motionEvent);
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull OnTouchListener onTouchListener) {
        this.a = onTouchListener;
    }
}
