package com.yandex.mobile.ads.nativeads;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.om;
import java.util.Map;

public final class bj implements j {
    private Map<String, Bitmap> a;

    public final void a(@NonNull Map<String, Bitmap> map) {
        this.a = map;
    }

    @Nullable
    public final Bitmap a(@NonNull om omVar) {
        String c = omVar.c();
        if (this.a != null) {
            return (Bitmap) this.a.get(c);
        }
        return null;
    }
}
