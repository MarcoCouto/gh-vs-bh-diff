package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.yandex.mobile.ads.impl.lf;
import com.yandex.mobile.ads.impl.mh;
import com.yandex.mobile.ads.impl.oj;
import java.util.List;

final class ae implements bf {
    @Nullable
    private final List<oj> a;

    public final void a() {
    }

    public final void a(@NonNull ag agVar) {
    }

    ae(@Nullable List<oj> list) {
        this.a = list;
    }

    public final void a(@NonNull ag agVar, @NonNull e eVar) {
        if (this.a != null) {
            mh mhVar = new mh(agVar, eVar);
            for (oj ojVar : this.a) {
                lf a2 = agVar.a(ojVar);
                if (a2 != null) {
                    a2.a(ojVar.c());
                    a2.a(ojVar, mhVar);
                }
            }
        }
        View a3 = agVar.d().a();
        if (a3 instanceof NativeAdUnitView) {
            eVar.a((NativeAdUnitView) a3);
        }
    }
}
