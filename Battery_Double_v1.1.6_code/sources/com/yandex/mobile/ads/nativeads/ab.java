package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ae;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.oj;
import java.util.ArrayList;
import java.util.List;

final class ab extends ba implements NativeAdUnit {
    @NonNull
    private final j a;
    @NonNull
    private final List<? extends NativeGenericAd> b;
    @NonNull
    private final bh c;

    ab(@NonNull Context context, @NonNull List<? extends NativeGenericAd> list, @NonNull j jVar, @NonNull c cVar) {
        super(context, cVar);
        this.b = list;
        this.a = jVar;
        this.c = cVar.c();
        p a2 = cVar.a();
        List c2 = a2.c().c();
        fl a3 = a2.a();
        String a4 = ae.AD_UNIT.a();
        ap apVar = new ap(c2, a3);
        apVar.a(a.CUSTOM);
        apVar.a(a4);
        a((a) apVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("sponsored");
        return arrayList;
    }

    public final void bindAdUnit(@Nullable NativeAdUnitView nativeAdUnitView) throws NativeAdException {
        if (nativeAdUnitView != null) {
            nativeAdUnitView.a(this);
            a(nativeAdUnitView, this.a, new ad(), f.a());
        }
    }

    @NonNull
    public final List<? extends NativeGenericAd> getNativeAds() {
        return this.b;
    }

    public final String getSponsored() {
        List<oj> a2 = this.c.a();
        if (a2 != null) {
            for (oj ojVar : a2) {
                if ("sponsored".equals(ojVar.a())) {
                    return (String) ojVar.c();
                }
            }
        }
        return null;
    }
}
