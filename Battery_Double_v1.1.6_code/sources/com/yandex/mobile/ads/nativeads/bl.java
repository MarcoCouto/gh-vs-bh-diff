package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.dk;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.fv;
import com.yandex.mobile.ads.impl.fw;
import com.yandex.mobile.ads.impl.nz;

public final class bl {
    @Nullable
    private AdTapHandler a;

    @NonNull
    public final dk a(@NonNull Context context, @NonNull fl flVar, @NonNull ResultReceiver resultReceiver) {
        if (this.a != null) {
            fw a2 = fv.a().a(context);
            if (a2 != null && a2.c()) {
                return new g(this.a);
            }
        }
        return new nz(context, flVar, resultReceiver);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable AdTapHandler adTapHandler) {
        this.a = adTapHandler;
    }
}
