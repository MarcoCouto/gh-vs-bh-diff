package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.yandex.mobile.ads.impl.as;
import com.yandex.mobile.ads.impl.lf;
import com.yandex.mobile.ads.impl.lg;
import com.yandex.mobile.ads.impl.lm;
import com.yandex.mobile.ads.impl.ln;
import com.yandex.mobile.ads.impl.oj;
import java.util.Map;

public final class ag<T extends View> {
    @NonNull
    private final f a;
    @NonNull
    private final j b;
    @NonNull
    private final al c;
    @NonNull
    private final Map<String, lf> d;
    @NonNull
    private final ah e = new ah();

    public ag(@NonNull T t, @NonNull ak<T> akVar, @NonNull j jVar, @NonNull as asVar, @NonNull f fVar, @NonNull t tVar, @NonNull lg lgVar) {
        this.a = fVar;
        this.b = jVar;
        ln lnVar = new ln(lgVar, asVar, tVar.c());
        this.c = akVar.a(t);
        this.d = new lm(this.c, this.b, lnVar).a();
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        for (lf lfVar : this.d.values()) {
            if (lfVar != null) {
                lfVar.a();
            }
        }
    }

    @Nullable
    public final View c() {
        return this.c.a();
    }

    @NonNull
    public final al d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final lf a(@Nullable oj ojVar) {
        if (ojVar != null) {
            return (lf) this.d.get(ojVar.a());
        }
        return null;
    }

    @NonNull
    public final f e() {
        return this.a;
    }

    @NonNull
    public final j f() {
        return this.b;
    }

    @Nullable
    public final NativeAdViewBinder b() {
        View a2 = this.c.a();
        if (a2 != null) {
            return ah.a(a2, this.c);
        }
        return null;
    }
}
