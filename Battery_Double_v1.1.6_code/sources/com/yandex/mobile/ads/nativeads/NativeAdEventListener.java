package com.yandex.mobile.ads.nativeads;

public interface NativeAdEventListener {

    public static class SimpleNativeAdEventListener implements NativeAdEventListener {
        public void onAdClosed() {
        }

        public void onAdLeftApplication() {
        }

        public void onAdOpened() {
        }
    }

    void onAdClosed();

    void onAdLeftApplication();

    void onAdOpened();
}
