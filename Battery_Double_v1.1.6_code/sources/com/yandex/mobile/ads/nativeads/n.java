package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.oq;
import com.yandex.mobile.ads.nativeads.template.NativeBannerView;
import com.yandex.mobile.ads.nativeads.template.c;
import java.util.Collections;

public abstract class n extends ba implements az, y {
    @NonNull
    protected j a;
    @NonNull
    private final w b;
    @NonNull
    private final f c;
    @NonNull
    private final bo d;
    @NonNull
    private final ap e;

    n(@NonNull Context context, @NonNull oq oqVar, @NonNull w wVar, @NonNull j jVar, @NonNull c cVar) {
        super(context, cVar);
        this.b = wVar;
        this.a = jVar;
        p a2 = cVar.a();
        this.c = f.a(a2.c().d());
        ap apVar = new ap(Collections.singletonList(oqVar), a2.a());
        NativeAdType b2 = oqVar.b();
        if (b2 != null) {
            apVar.a(b2.getValue());
        }
        this.e = apVar;
        a((a) this.e);
        this.d = new bo();
    }

    public final void a(@NonNull NativeBannerView nativeBannerView) throws NativeAdException {
        c cVar = new c();
        nativeBannerView.a(this);
        a(nativeBannerView, cVar, this.c, a.TEMPLATE);
    }

    public void bindNativeAd(@NonNull NativeAdViewBinder nativeAdViewBinder) throws NativeAdException {
        View nativeAdView = nativeAdViewBinder.getNativeAdView();
        this.d.a(nativeAdView, new b() {
            public final void a() {
                n.this.b();
            }

            public final void b() {
                n.this.c();
            }
        });
        a(nativeAdView, new ai(nativeAdViewBinder), f.a(), a.CUSTOM);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull u uVar, @NonNull ak akVar) throws NativeAdException {
        a(uVar, akVar, f.a(), a.CUSTOM);
    }

    private <T extends View> void a(@NonNull T t, @NonNull ak<T> akVar, @NonNull f fVar, @NonNull a aVar) throws NativeAdException {
        this.e.a(aVar);
        a(t, this.a, akVar, fVar);
    }

    public NativeAdType getAdType() {
        return this.b.a();
    }

    public String getInfo() {
        return this.b.b();
    }

    public NativeAdAssets getAdAssets() {
        return this.b.d();
    }

    public void loadImages() {
        this.b.c();
    }

    public void addImageLoadingListener(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.b.a(nativeAdImageLoadingListener);
    }

    public void removeImageLoadingListener(NativeAdImageLoadingListener nativeAdImageLoadingListener) {
        this.b.b(nativeAdImageLoadingListener);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context) {
        this.d.a(context);
        super.a(context);
    }
}
