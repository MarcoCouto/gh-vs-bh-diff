package com.yandex.mobile.ads.nativeads;

public final class NativeAdMedia {
    private final float a;

    NativeAdMedia(float f) {
        this.a = f;
    }

    public final float getAspectRatio() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && getClass() == obj.getClass() && Float.compare(((NativeAdMedia) obj).a, this.a) == 0;
    }

    public final int hashCode() {
        if (this.a != 0.0f) {
            return Float.floatToIntBits(this.a);
        }
        return 0;
    }
}
