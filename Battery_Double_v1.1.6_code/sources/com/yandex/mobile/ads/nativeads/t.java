package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.lo;

public final class t {
    @NonNull
    private final s a;
    @NonNull
    private final bg b;
    @NonNull
    private final lo c;
    @NonNull
    private final bn d;

    public t(@NonNull s sVar, @NonNull bg bgVar, @NonNull lo loVar, @NonNull bn bnVar) {
        this.a = sVar;
        this.b = bgVar;
        this.c = loVar;
        this.d = bnVar;
    }

    @NonNull
    public final s a() {
        return this.a;
    }

    @NonNull
    public final bg b() {
        return this.b;
    }

    @NonNull
    public final lo c() {
        return this.c;
    }

    @NonNull
    public final bn d() {
        return this.d;
    }
}
