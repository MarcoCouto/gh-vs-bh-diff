package com.yandex.mobile.ads.nativeads;

import java.util.Arrays;

public final class NativeAdLoaderConfiguration {
    public static final String NATIVE_IMAGE_SIZE_LARGE = "large";
    public static final String NATIVE_IMAGE_SIZE_MEDIUM = "medium";
    public static final String NATIVE_IMAGE_SIZE_SMALL = "small";
    private final String a;
    private final String[] b;
    private final boolean c;

    public static final class Builder {
        /* access modifiers changed from: private */
        public final String a;
        /* access modifiers changed from: private */
        public final boolean b;
        /* access modifiers changed from: private */
        public String[] c = {NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_SMALL, "medium", NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_LARGE};

        public Builder(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        public final NativeAdLoaderConfiguration build() {
            return new NativeAdLoaderConfiguration(this, 0);
        }

        public final Builder setImageSizes(String... strArr) {
            if (!(strArr == null || strArr.length == 0 || Arrays.asList(strArr).contains(null))) {
                this.c = strArr;
            }
            return this;
        }
    }

    /* synthetic */ NativeAdLoaderConfiguration(Builder builder, byte b2) {
        this(builder);
    }

    private NativeAdLoaderConfiguration(Builder builder) {
        this.a = builder.a;
        this.b = builder.c;
        this.c = builder.b;
    }

    public final String getBlockId() {
        return this.a;
    }

    public final String[] getImageSizes() {
        return this.b;
    }

    public final boolean shouldLoadImagesAutomatically() {
        return this.c;
    }
}
