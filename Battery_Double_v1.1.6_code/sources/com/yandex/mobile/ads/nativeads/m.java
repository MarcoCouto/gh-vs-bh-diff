package com.yandex.mobile.ads.nativeads;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.lf;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.nativeads.au.a;
import com.yandex.mobile.ads.nativeads.au.b;
import java.util.List;

class m implements au {
    final ao a;
    private final List<oj> b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public ag d;

    m(@Nullable List<oj> list, @NonNull ao aoVar) {
        this.b = list;
        this.a = aoVar;
    }

    public void a(ag agVar) {
        this.d = agVar;
    }

    @VisibleForTesting
    public boolean b() {
        return !a((b) new b() {
            public final boolean a(@NonNull List<oj> list) {
                for (oj ojVar : list) {
                    if (ojVar.f()) {
                        lf a2 = m.this.d.a(ojVar);
                        if (a2 != null && a2.d()) {
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    @VisibleForTesting
    public boolean c() {
        return !a((b) new b() {
            public final boolean a(@NonNull List<oj> list) {
                for (oj ojVar : list) {
                    if (ojVar.f()) {
                        lf a2 = m.this.d.a(ojVar);
                        if (a2 == null || !a2.c()) {
                            m.this.c = ojVar.a();
                            return false;
                        }
                    }
                }
                return true;
            }
        });
    }

    public boolean d() {
        return !a((b) new b() {
            public final boolean a(@NonNull List<oj> list) {
                for (oj ojVar : list) {
                    if (ojVar.f()) {
                        lf a2 = m.this.d.a(ojVar);
                        Object c = ojVar.c();
                        if (a2 == null || !a2.b(c)) {
                            m.this.c = ojVar.a();
                            return false;
                        }
                    }
                }
                return true;
            }
        });
    }

    private boolean a(@NonNull b bVar) {
        return this.d != null && a(bVar, this.b);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"VisibleForTests"})
    public boolean a(@NonNull b bVar, @Nullable List<oj> list) {
        if (!this.a.b()) {
            return true;
        }
        if (list == null || !bVar.a(list)) {
            return false;
        }
        return true;
    }

    @NonNull
    public bk e() {
        return new bk(a((b) new b() {
            public final boolean a(@NonNull List<oj> list) {
                for (oj ojVar : list) {
                    if (ojVar.f()) {
                        lf a2 = m.this.d.a(ojVar);
                        if (a2 == null || !a2.b()) {
                            m.this.c = ojVar.a();
                            return false;
                        }
                    }
                }
                return true;
            }
        }), this.c);
    }

    public a a() {
        int i;
        am.a aVar;
        boolean z = false;
        if (this.b != null) {
            i = 0;
            for (oj f : this.b) {
                if (f.f()) {
                    i++;
                }
            }
        } else {
            i = 0;
        }
        if ((i >= 2) && b()) {
            z = true;
        }
        if (z) {
            aVar = am.a.NO_VISIBLE_REQUIRED_ASSETS;
        } else if (c()) {
            aVar = am.a.REQUIRED_ASSET_CAN_NOT_BE_VISIBLE;
        } else if (d()) {
            aVar = am.a.INCONSISTENT_ASSET_VALUE;
        } else {
            aVar = am.a.SUCCESS;
        }
        return new at(aVar, this.c);
    }
}
