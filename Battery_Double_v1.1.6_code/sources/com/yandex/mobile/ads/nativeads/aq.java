package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.yandex.mobile.ads.impl.oq;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.List;

public final class aq extends n implements ar {
    public aq(@NonNull Context context, @NonNull oq oqVar, @NonNull w wVar, @NonNull j jVar, @NonNull c cVar) {
        super(context, oqVar, wVar, jVar, cVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(IronSourceSegment.AGE);
        arrayList.add(TtmlNode.TAG_BODY);
        arrayList.add("call_to_action");
        arrayList.add(SettingsJsonConstants.APP_ICON_KEY);
        arrayList.add("rating");
        arrayList.add("sponsored");
        arrayList.add("title");
        return arrayList;
    }

    public final void bindAppInstallAd(@Nullable NativeAppInstallAdView nativeAppInstallAdView) throws NativeAdException {
        if (nativeAppInstallAdView != null) {
            nativeAppInstallAdView.a(this);
            a(nativeAppInstallAdView, new as());
        }
    }
}
