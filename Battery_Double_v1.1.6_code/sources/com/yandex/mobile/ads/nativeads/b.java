package com.yandex.mobile.ads.nativeads;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.x;
import com.yandex.mobile.ads.impl.x.a;
import java.lang.ref.WeakReference;

public final class b implements a {
    @NonNull
    private final x a = new x(new Handler(Looper.getMainLooper()));
    @NonNull
    private final WeakReference<r> b;

    public b(@NonNull r rVar) {
        this.b = new WeakReference<>(rVar);
        this.a.a(this);
    }

    public final void a(int i, @Nullable Bundle bundle) {
        r rVar = (r) this.b.get();
        if (rVar != null) {
            switch (i) {
                case 6:
                    rVar.e();
                    break;
                case 7:
                    rVar.d();
                    return;
                case 8:
                    rVar.b();
                    return;
                case 9:
                    rVar.a();
                    return;
            }
        }
    }

    @NonNull
    public final x a() {
        return this.a;
    }
}
