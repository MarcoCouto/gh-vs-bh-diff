package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.w;

public final class p {
    private final w a;
    private final fl b;
    private final or c;

    public p(@NonNull or orVar, @NonNull w wVar, @NonNull fl flVar) {
        this.a = wVar;
        this.b = flVar;
        this.c = orVar;
    }

    @NonNull
    public final fl a() {
        return this.b;
    }

    @NonNull
    public final w b() {
        return this.a;
    }

    @NonNull
    public final or c() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        p pVar = (p) obj;
        if (this.a == null ? pVar.a != null : !this.a.equals(pVar.a)) {
            return false;
        }
        if (this.b == null ? pVar.b != null : !this.b.equals(pVar.b)) {
            return false;
        }
        if (this.c != null) {
            return this.c.equals(pVar.c);
        }
        return pVar.c == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }
}
