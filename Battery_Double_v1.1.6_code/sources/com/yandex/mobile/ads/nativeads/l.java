package com.yandex.mobile.ads.nativeads;

import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ok;

public final class l extends NativeAdAssets {
    private av a;

    l() {
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        l lVar = (l) obj;
        if (this.a != null) {
            return this.a.equals(lVar.a);
        }
        return lVar.a == null;
    }

    public final int hashCode() {
        return (super.hashCode() * 31) + (this.a != null ? this.a.hashCode() : 0);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable ok okVar) {
        this.a = okVar != null ? new av(okVar) : null;
    }
}
