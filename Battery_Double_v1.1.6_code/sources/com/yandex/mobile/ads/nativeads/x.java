package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.cr;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.u;
import com.yandex.mobile.ads.nativeads.NativeAdLoader.OnImageAdLoadListener;
import com.yandex.mobile.ads.nativeads.NativeAdLoader.OnLoadListener;

public final class x {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @NonNull
    public final cr c;
    /* access modifiers changed from: private */
    @Nullable
    public OnLoadListener d;
    /* access modifiers changed from: private */
    @Nullable
    public NativeAdUnitLoadListener e;

    x(@NonNull Context context) {
        this.c = new cr(context);
    }

    public final void a(@NonNull fl flVar) {
        this.c.a(flVar);
    }

    public final void a(@NonNull a aVar) {
        this.c.a(aVar);
    }

    public final void a(@Nullable OnLoadListener onLoadListener) {
        synchronized (a) {
            this.d = onLoadListener;
        }
    }

    public final void a(@Nullable NativeAdUnitLoadListener nativeAdUnitLoadListener) {
        synchronized (a) {
            this.e = nativeAdUnitLoadListener;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.b.removeCallbacksAndMessages(null);
    }

    public final void a(@NonNull AdRequestError adRequestError) {
        b(adRequestError);
    }

    public final void a(@NonNull final NativeGenericAd nativeGenericAd) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (x.a) {
                    if (x.this.d != null) {
                        if (nativeGenericAd instanceof NativeContentAd) {
                            x.this.c.a();
                            x.this.d.onContentAdLoaded((NativeContentAd) nativeGenericAd);
                        } else if (nativeGenericAd instanceof NativeAppInstallAd) {
                            x.this.c.a();
                            x.this.d.onAppInstallAdLoaded((NativeAppInstallAd) nativeGenericAd);
                        } else if ((nativeGenericAd instanceof NativeImageAd) && (x.this.d instanceof OnImageAdLoadListener)) {
                            x.this.c.a();
                            ((OnImageAdLoadListener) x.this.d).onImageAdLoaded((NativeImageAd) nativeGenericAd);
                        } else if (!(nativeGenericAd instanceof bd) || !(x.this.d instanceof bi)) {
                            x.this.b(u.a);
                        } else {
                            x.this.c.a();
                            x.this.d;
                        }
                    }
                }
            }
        });
    }

    public final void a(@NonNull final NativeAdUnit nativeAdUnit) {
        this.c.a();
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (x.a) {
                    if (x.this.e != null) {
                        x.this.e.onNativeAdUnitLoaded(nativeAdUnit);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull final AdRequestError adRequestError) {
        this.c.a(adRequestError);
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (x.a) {
                    if (x.this.d != null) {
                        x.this.d.onAdFailedToLoad(adRequestError);
                    }
                    if (x.this.e != null) {
                        x.this.e.onNativeAdUnitFailedToLoad(adRequestError);
                    }
                }
            }
        });
    }
}
