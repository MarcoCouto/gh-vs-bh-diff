package com.yandex.mobile.ads.nativeads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.yandex.mobile.ads.impl.ax;

public final class h implements OnTouchListener {
    @NonNull
    private final ax a;
    @NonNull
    private final GestureDetector b;

    static class a extends SimpleOnGestureListener {
        public final boolean onSingleTapUp(MotionEvent motionEvent) {
            return true;
        }

        a() {
        }
    }

    h(@NonNull Context context, @NonNull ax axVar) {
        this.a = axVar;
        this.b = new GestureDetector(context.getApplicationContext(), new a());
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final boolean onTouch(@NonNull View view, @NonNull MotionEvent motionEvent) {
        if (this.b.onTouchEvent(motionEvent)) {
            this.a.c();
        }
        return false;
    }
}
