package com.yandex.mobile.ads.nativeads;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.yandex.mobile.ads.impl.ml;
import com.yandex.mobile.ads.impl.mm;
import com.yandex.mobile.ads.impl.mo;
import com.yandex.mobile.ads.impl.rb;
import java.lang.ref.WeakReference;

final class bo {
    @NonNull
    private final rb a = new rb();
    @NonNull
    private final ml b = mm.a();
    /* access modifiers changed from: private */
    @Nullable
    public b c;
    @Nullable
    private mo d;
    @Nullable
    private a e;

    private class a implements OnGlobalLayoutListener {
        @NonNull
        private final WeakReference<View> b;

        a(View view) {
            this.b = new WeakReference<>(view);
        }

        public final void onGlobalLayout() {
            View view = (View) this.b.get();
            if (view != null) {
                Integer valueOf = Integer.valueOf(view.getVisibility());
                if (view.getTag() != valueOf) {
                    view.setTag(valueOf);
                    if (bo.this.c != null) {
                        if (valueOf.intValue() == 0) {
                            bo.this.c.a();
                            return;
                        }
                        bo.this.c.b();
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            if (VERSION.SDK_INT >= 16) {
                View view = (View) this.b.get();
                if (view != null) {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        }
    }

    interface b {
        void a();

        void b();
    }

    private class c implements mo {
        private final WeakReference<Context> b;

        c(Context context) {
            this.b = new WeakReference<>(context);
        }

        public final void a(@NonNull Activity activity) {
            Context context = (Context) this.b.get();
            if (context != null && context.equals(activity) && bo.this.c != null) {
                bo.this.c.a();
            }
        }

        public final void b(@NonNull Activity activity) {
            Context context = (Context) this.b.get();
            if (context != null && context.equals(activity) && bo.this.c != null) {
                bo.this.c.b();
            }
        }
    }

    bo() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view, @NonNull b bVar) {
        this.c = bVar;
        b(view.getContext());
        Context a2 = rb.a(view.getContext());
        if (a2 != null) {
            this.d = new c(a2);
            this.e = new a(view);
            this.b.a(a2, this.d);
            view.getViewTreeObserver().addOnGlobalLayoutListener(this.e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context) {
        this.c = null;
        b(context);
    }

    private void b(@NonNull Context context) {
        if (this.d != null) {
            this.b.b(context, this.d);
        }
        if (this.e != null) {
            this.e.a();
        }
    }
}
