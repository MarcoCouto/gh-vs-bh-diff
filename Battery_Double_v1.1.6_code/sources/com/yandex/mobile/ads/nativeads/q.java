package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.om;
import com.yandex.mobile.ads.impl.oq;
import com.yandex.mobile.ads.impl.ql;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class q {
    @NonNull
    private final ql a = new ql();

    @NonNull
    static List<String> a(@NonNull List<oq> list) {
        HashSet hashSet = new HashSet();
        for (oq a2 : list) {
            hashSet.addAll(a(a2));
        }
        return new ArrayList(hashSet);
    }

    @NonNull
    public static List<String> a(@NonNull oq oqVar) {
        HashSet hashSet = new HashSet();
        for (om omVar : ql.a(oqVar)) {
            if (!TextUtils.isEmpty(omVar.d())) {
                hashSet.add(omVar.d());
            }
        }
        return new ArrayList(hashSet);
    }
}
