package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ax;
import com.yandex.mobile.ads.impl.mq;
import com.yandex.mobile.ads.impl.mr;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.impl.on;

public final class e {
    @NonNull
    private final a a;
    @NonNull
    private final ax b;

    e(@NonNull a aVar, @NonNull ax axVar) {
        this.a = aVar;
        this.b = axVar;
    }

    public final void a(@NonNull oj ojVar, @Nullable on onVar, @NonNull ag agVar, @NonNull mq mqVar) {
        if (ojVar.e() && onVar != null) {
            mqVar.a(onVar, new mr(ojVar, this.a, agVar, this.b));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull NativeAdUnitView nativeAdUnitView) {
        nativeAdUnitView.a(new h(nativeAdUnitView.getContext(), this.b));
    }
}
