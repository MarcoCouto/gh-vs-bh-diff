package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.yandex.mobile.ads.nativeads.ba;

public abstract class u<T extends ba> extends FrameLayout {
    @Nullable
    private T a;

    public u(Context context) {
        super(context);
    }

    public u(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public u(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull T t) {
        this.a = t;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.a != null) {
            this.a.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.a != null) {
            this.a.c();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        int i2 = 0;
        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(getVisibility())};
        if (!(i == 0 && getVisibility() == 0)) {
            i2 = 8;
        }
        a(i2);
        super.onWindowVisibilityChanged(i);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        int i2 = 0;
        Object[] objArr = {view, Integer.valueOf(i)};
        if (i != 0 || !isShown()) {
            i2 = 8;
        }
        a(i2);
    }

    private void a(int i) {
        if (this.a != null) {
            this.a.a(i);
        }
    }

    @Nullable
    public T getNativeAd() {
        return this.a;
    }
}
