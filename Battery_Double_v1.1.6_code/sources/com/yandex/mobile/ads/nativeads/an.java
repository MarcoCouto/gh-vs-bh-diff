package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ci;

public final class an implements ci {
    @NonNull
    private final be a;

    public an(@NonNull be beVar) {
        this.a = beVar;
    }

    public final boolean a() {
        return !this.a.a();
    }
}
