package com.yandex.mobile.ads.nativeads;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.om;
import java.util.Map;

public interface j {
    @Nullable
    Bitmap a(@NonNull om omVar);

    void a(@NonNull Map<String, Bitmap> map);
}
