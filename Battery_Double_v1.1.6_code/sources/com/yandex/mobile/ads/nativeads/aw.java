package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.mobile.ads.impl.oq;
import java.util.ArrayList;
import java.util.List;

public final class aw extends n implements ax {
    public aw(@NonNull Context context, @NonNull oq oqVar, @NonNull w wVar, @NonNull j jVar, @NonNull c cVar) {
        super(context, oqVar, wVar, jVar, cVar);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final List<String> a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(TtmlNode.TAG_BODY);
        arrayList.add(RequestParameters.DOMAIN);
        arrayList.add("sponsored");
        arrayList.add("title");
        return arrayList;
    }

    public final void bindContentAd(@Nullable NativeContentAdView nativeContentAdView) throws NativeAdException {
        if (nativeContentAdView != null) {
            nativeContentAdView.a(this);
            a(nativeContentAdView, new ay());
        }
    }
}
