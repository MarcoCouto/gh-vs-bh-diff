package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import android.view.View;
import com.yandex.mobile.ads.impl.ag;
import com.yandex.mobile.ads.impl.am;
import com.yandex.mobile.ads.impl.am.a;
import com.yandex.mobile.ads.impl.cm;
import com.yandex.mobile.ads.impl.ee;

class af implements be {
    @VisibleForTesting
    @NonNull
    final au a;
    @NonNull
    private final ag b = ag.a();
    @Nullable
    private ag c;

    af(@NonNull au auVar) {
        this.a = auVar;
    }

    public final void a(@NonNull ag agVar) {
        this.c = agVar;
        this.a.a(agVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    @NonNull
    public final am a(@NonNull Context context, int i) {
        a aVar;
        boolean z;
        boolean z2 = true;
        String str = null;
        if (!this.b.a(context)) {
            aVar = a.APPLICATION_INACTIVE;
        } else if (a()) {
            aVar = a.SUPERVIEW_HIDDEN;
        } else {
            if (this.c != null) {
                View c2 = this.c.c();
                if (c2 != null) {
                    z = ee.a(c2, 10);
                    if (!z) {
                        aVar = a.TOO_SMALL;
                    } else {
                        if (this.c != null && ee.b(this.c.c(), i)) {
                            z2 = false;
                        }
                        if (z2) {
                            aVar = a.NOT_VISIBLE_FOR_PERCENT;
                        } else {
                            au.a a2 = this.a.a();
                            a b2 = a2.b();
                            str = a2.a();
                            aVar = b2;
                        }
                    }
                }
            }
            z = true;
            if (!z) {
            }
        }
        Pair pair = new Pair(aVar, str);
        am a3 = a(context, (a) pair.first);
        a3.a((String) pair.second);
        return a3;
    }

    /* access modifiers changed from: protected */
    public am a(@NonNull Context context, a aVar) {
        return new am(aVar, new cm());
    }

    @VisibleForTesting
    public final boolean a() {
        if (this.c != null) {
            View c2 = this.c.c();
            if (c2 != null) {
                return ee.d(c2);
            }
        }
        return true;
    }

    @NonNull
    public final bk b() {
        return this.a.e();
    }
}
