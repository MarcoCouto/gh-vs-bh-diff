package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.dw;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.impl.ol;
import com.yandex.mobile.ads.impl.om;
import com.yandex.mobile.ads.impl.oq;
import com.yandex.mobile.ads.impl.pe;
import com.yandex.mobile.ads.impl.pe.b;
import com.yandex.mobile.ads.impl.ql;
import com.yandex.mobile.ads.impl.qp;
import com.yandex.mobile.ads.impl.qt;
import com.yandex.mobile.ads.impl.sf;
import com.yandex.mobile.ads.impl.sm;
import com.yandex.mobile.ads.impl.sm.c;
import com.yandex.mobile.ads.impl.sm.d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class i {
    @NonNull
    private final qp a = new qp();
    @NonNull
    private final qt b;
    @NonNull
    private final ql c;
    @NonNull
    private final sm d;
    @NonNull
    private final b e;

    static class a {
        @NonNull
        private final Handler a = new Handler(Looper.getMainLooper());
        /* access modifiers changed from: private */
        @NonNull
        public final sm b;
        @NonNull
        private final AtomicInteger c;
        @NonNull
        private final Set<om> d;
        @NonNull
        private final k e;
        @NonNull
        private final dw f;

        a(@NonNull sm smVar, @NonNull Set<om> set, @NonNull k kVar) {
            this.b = smVar;
            this.d = set;
            this.e = kVar;
            this.c = new AtomicInteger(set.size());
            this.f = new dw();
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            HashMap hashMap = new HashMap();
            for (final om omVar : this.d) {
                final String c2 = omVar.c();
                final int b2 = omVar.b();
                final int a2 = omVar.a();
                boolean z = true;
                new Object[1][0] = c2;
                int b3 = omVar.b();
                int a3 = omVar.a();
                Runtime runtime = Runtime.getRuntime();
                float maxMemory = ((float) Runtime.getRuntime().maxMemory()) - (((float) runtime.totalMemory()) - ((float) runtime.freeMemory()));
                float f2 = ((float) (b3 * a3 * 4)) + 1048576.0f;
                if (maxMemory < f2) {
                    StringBuilder sb = new StringBuilder("Not enough free memory to create bitmap. FreeMemory = ");
                    sb.append(maxMemory);
                    sb.append(", RequiredMemory = ");
                    sb.append(f2);
                    z = false;
                }
                if (z) {
                    Handler handler = this.a;
                    final HashMap hashMap2 = hashMap;
                    AnonymousClass1 r0 = new Runnable() {
                        public final void run() {
                            a.this.b.a(c2, (d) new d() {
                                public final void a(@NonNull sf sfVar) {
                                    new Object[1][0] = sfVar;
                                    a.this.a(hashMap2);
                                }

                                public final void a(c cVar) {
                                    String c = omVar.c();
                                    Bitmap a2 = cVar.a();
                                    if (a2 != null) {
                                        if (c != null) {
                                            hashMap2.put(c, a2);
                                        }
                                        a.this.a(hashMap2);
                                    }
                                }
                            }, a2, b2);
                        }
                    };
                    handler.post(r0);
                } else {
                    a((Map<String, Bitmap>) hashMap);
                }
            }
        }

        /* access modifiers changed from: private */
        public void a(@NonNull Map<String, Bitmap> map) {
            if (this.c.decrementAndGet() == 0) {
                this.e.a(map);
            }
        }
    }

    public i(Context context) {
        this.b = new qt(context);
        this.c = new ql();
        pe a2 = pe.a(context);
        this.d = a2.b();
        this.e = a2.a();
    }

    public final void a(@NonNull Set<om> set, @NonNull k kVar) {
        if (set.size() == 0) {
            kVar.a(Collections.emptyMap());
        } else {
            new a(this.d, set, kVar).a();
        }
    }

    public final void a(@NonNull Map<String, Bitmap> map) {
        for (Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            Bitmap bitmap = (Bitmap) entry.getValue();
            if (bitmap != null) {
                this.e.a(str, bitmap);
            }
        }
    }

    @NonNull
    public final Set<om> a(@NonNull List<oq> list) {
        HashSet hashSet = new HashSet();
        for (oq oqVar : list) {
            hashSet.addAll(ql.a(oqVar));
            ArrayList arrayList = new ArrayList();
            oj b2 = oqVar.b("feedback");
            if (b2 != null && (b2.c() instanceof ol)) {
                om a2 = ((ol) b2.c()).a();
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
            hashSet.addAll(arrayList);
            hashSet.addAll(this.b.a(oqVar));
        }
        return hashSet;
    }
}
