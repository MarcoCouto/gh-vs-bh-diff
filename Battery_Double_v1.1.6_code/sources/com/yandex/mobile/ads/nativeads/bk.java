package com.yandex.mobile.ads.nativeads;

import android.support.annotation.Nullable;

public final class bk {
    @Nullable
    private final String a;
    private final boolean b;

    bk(boolean z, @Nullable String str) {
        this.b = z;
        this.a = str;
    }

    @Nullable
    public final String a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.b;
    }
}
