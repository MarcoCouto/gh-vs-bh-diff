package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

final class aj {
    private static final Object a = new Object();
    private static volatile aj b;
    @NonNull
    private final Map<View, ba> c = new WeakHashMap();

    aj() {
    }

    @NonNull
    static aj a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new aj();
                }
            }
        }
        return b;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ba a(@NonNull View view) {
        ba baVar;
        synchronized (a) {
            baVar = (ba) this.c.get(view);
        }
        return baVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view, @NonNull ba baVar) {
        synchronized (a) {
            this.c.put(view, baVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull ba baVar) {
        Iterator it = this.c.entrySet().iterator();
        boolean z = false;
        while (it.hasNext()) {
            if (((ba) ((Entry) it.next()).getValue()) == baVar) {
                it.remove();
                z = true;
            }
        }
        return z;
    }
}
