package com.yandex.mobile.ads.nativeads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.aa;
import com.yandex.mobile.ads.impl.ae;
import com.yandex.mobile.ads.impl.af;
import com.yandex.mobile.ads.impl.bw;
import com.yandex.mobile.ads.impl.cz;
import com.yandex.mobile.ads.impl.lc;
import com.yandex.mobile.ads.impl.nc;
import com.yandex.mobile.ads.impl.nd;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.pg;
import com.yandex.mobile.ads.impl.qk;
import com.yandex.mobile.ads.impl.qv;
import com.yandex.mobile.ads.impl.t;
import com.yandex.mobile.ads.impl.u;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.nativeads.NativeAdLoader.OnLoadListener;

public final class v extends aa<or> {
    @NonNull
    private final nc h = new a();
    @NonNull
    private final nd i;
    @NonNull
    private final lc j;
    @NonNull
    private final qv k;
    /* access modifiers changed from: private */
    @NonNull
    public final x l;
    @Nullable
    private cz<or> m;

    @VisibleForTesting
    class a implements nc {
        a() {
        }

        public final void a(@NonNull AdRequestError adRequestError) {
            v.this.onAdFailedToLoad(adRequestError);
        }

        public final void a(@NonNull NativeGenericAd nativeGenericAd) {
            v.this.s();
            v.this.l.a(nativeGenericAd);
        }

        public final void a(@NonNull NativeAdUnit nativeAdUnit) {
            v.this.s();
            v.this.l.a(nativeAdUnit);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean d(@Nullable AdRequest adRequest) {
        return true;
    }

    public v(@NonNull Context context, @NonNull NativeAdLoaderConfiguration nativeAdLoaderConfiguration, @NonNull x xVar) {
        super(context, b.NATIVE);
        this.l = xVar;
        a_(nativeAdLoaderConfiguration.getBlockId());
        this.f.a(nativeAdLoaderConfiguration.getImageSizes());
        this.f.b(nativeAdLoaderConfiguration.shouldLoadImagesAutomatically());
        this.f.b(qk.a(context).a());
        this.i = new nd(context, r(), nativeAdLoaderConfiguration);
        this.j = new lc();
        this.k = new qv();
        this.l.a((com.yandex.mobile.ads.impl.hr.a) this.k);
    }

    public final void a(@NonNull w<or> wVar) {
        this.k.a(wVar);
        if (!l()) {
            lc.a(wVar).a(this).a(this.b, wVar);
        }
    }

    public final void a(@NonNull w<or> wVar, @NonNull t tVar) {
        if (!l()) {
            this.i.a(this.b, wVar, tVar, this.h);
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final bw<or> a(String str, String str2) {
        pg pgVar = new pg(this.b, this.m, this.f, str, str2, this);
        return pgVar;
    }

    public final void a(@Nullable AdRequest adRequest, @NonNull cz<or> czVar, @NonNull ae aeVar, @NonNull af afVar) {
        this.m = czVar;
        if (czVar.a()) {
            this.f.a(aeVar);
            this.f.a(afVar);
            a(adRequest);
            return;
        }
        onAdFailedToLoad(u.j);
    }

    public final synchronized void a(@Nullable AdRequest adRequest) {
        b(adRequest);
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull AdRequestError adRequestError) {
        this.l.a(adRequestError);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"VisibleForTests"})
    public final boolean n() {
        return o();
    }

    public final void a(@Nullable OnLoadListener onLoadListener) {
        this.l.a(onLoadListener);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable NativeAdUnitLoadListener nativeAdUnitLoadListener) {
        this.l.a(nativeAdUnitLoadListener);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        super.v();
        super.w();
        this.c.a();
        this.e.a();
        this.l.a();
        a(t.CANCELLED);
    }
}
