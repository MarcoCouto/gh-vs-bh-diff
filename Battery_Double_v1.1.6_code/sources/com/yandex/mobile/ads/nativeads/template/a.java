package com.yandex.mobile.ads.nativeads.template;

import android.content.Context;
import com.yandex.mobile.ads.impl.ee;

abstract class a {
    protected final float a;

    /* renamed from: com.yandex.mobile.ads.nativeads.template.a$a reason: collision with other inner class name */
    static class C0117a extends a {
        /* access modifiers changed from: protected */
        public final float a(float f) {
            if (f < 10.0f) {
                return 10.0f;
            }
            return f;
        }

        public C0117a(float f) {
            super(f);
        }

        public final d a(Context context, int i, int i2, int i3) {
            int min = Math.min(ee.a(context, this.a), i);
            return new d(min, Math.round(((float) i3) * (((float) min) / ((float) i2))));
        }
    }

    static class b extends a {
        public b(float f) {
            super(f);
        }

        public final d a(Context context, int i, int i2, int i3) {
            int round = Math.round(((float) i) * this.a);
            return new d(round, Math.round(((float) i3) * (((float) round) / ((float) i2))));
        }

        /* access modifiers changed from: protected */
        public final float a(float f) {
            return b(f);
        }
    }

    static class c extends a {
        public c(float f) {
            super(f);
        }

        public final d a(Context context, int i, int i2, int i3) {
            int a = ee.a(context, 140.0f);
            int round = Math.round(((float) i) * this.a);
            if (i2 > round) {
                i3 = Math.round(((float) i3) / (((float) i2) / ((float) round)));
            } else {
                round = i2;
            }
            if (i3 > a) {
                round = Math.round(((float) round) / (((float) i3) / ((float) a)));
            } else {
                a = i3;
            }
            return new d(round, a);
        }

        /* access modifiers changed from: protected */
        public final float a(float f) {
            return b(f);
        }
    }

    static class d {
        private final int a;
        private final int b;

        public d(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public final int a() {
            return this.a;
        }

        public final int b() {
            return this.b;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            return this.a == dVar.a && this.b == dVar.b;
        }

        public final int hashCode() {
            return (this.a * 31) + this.b;
        }
    }

    protected static float b(float f) {
        if (f < 0.01f) {
            return 0.01f;
        }
        if (f > 1.0f) {
            return 1.0f;
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public abstract float a(float f);

    public abstract d a(Context context, int i, int i2, int i3);

    a(float f) {
        this.a = a(f);
    }
}
