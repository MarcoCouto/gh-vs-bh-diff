package com.yandex.mobile.ads.nativeads.template;

import android.support.annotation.NonNull;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.ak;
import com.yandex.mobile.ads.nativeads.al;
import com.yandex.mobile.ads.nativeads.al.a;

public final class c implements ak<NativeBannerView> {
    @NonNull
    public final /* synthetic */ al a(@NonNull Object obj) {
        NativeBannerView nativeBannerView = (NativeBannerView) obj;
        return new a(nativeBannerView).a(nativeBannerView.a()).b(nativeBannerView.b()).c((TextView) nativeBannerView.c()).d(nativeBannerView.d()).a(nativeBannerView.m()).b(nativeBannerView.j()).c(nativeBannerView.e()).a(nativeBannerView.f()).a(nativeBannerView.k()).f(nativeBannerView.l()).g(nativeBannerView.g()).h(nativeBannerView.h()).i(nativeBannerView.i()).a();
    }
}
