package com.yandex.mobile.ads.nativeads.template;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeAdAssets;
import com.yandex.mobile.ads.nativeads.NativeAdImage;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import com.yandex.mobile.ads.nativeads.NativeAdMedia;
import com.yandex.mobile.ads.nativeads.NativeAdType;

public final class d {
    private final NativeAdImage a;
    private final NativeAdImage b;
    private final NativeAdImage c;
    private final NativeAdMedia d;
    private final NativeAdType e;

    public d(@NonNull NativeAdAssets nativeAdAssets, @NonNull NativeAdType nativeAdType) {
        this.a = nativeAdAssets.getFavicon();
        this.b = nativeAdAssets.getIcon();
        this.c = nativeAdAssets.getImage();
        this.d = nativeAdAssets.getMedia();
        this.e = nativeAdType;
    }

    static boolean a(@NonNull NativeAdImage nativeAdImage) {
        return NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_LARGE.equals(nativeAdImage.a()) || "wide".equals(nativeAdImage.a());
    }

    static boolean b(@NonNull NativeAdImage nativeAdImage) {
        return "fill".equals(nativeAdImage.a());
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return !b() && this.a != null && (f() || this.c == null || a(this.c));
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.b != null && (g() || !c());
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        return !f() && this.c != null && !a(this.c) && !g();
    }

    /* access modifiers changed from: 0000 */
    public final boolean d() {
        return !f() && this.c != null && a(this.c);
    }

    /* access modifiers changed from: 0000 */
    public final boolean e() {
        return !f() && this.c != null && b(this.c);
    }

    /* access modifiers changed from: 0000 */
    public final boolean f() {
        return this.d != null;
    }

    private boolean g() {
        return NativeAdType.APP_INSTALL == this.e;
    }
}
