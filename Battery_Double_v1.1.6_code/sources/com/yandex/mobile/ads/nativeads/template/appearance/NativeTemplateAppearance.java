package com.yandex.mobile.ads.nativeads.template.appearance;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewCompat;
import com.yandex.mobile.ads.impl.ee;
import com.yandex.mobile.ads.nativeads.template.HorizontalOffset;
import com.yandex.mobile.ads.nativeads.template.SizeConstraint;
import com.yandex.mobile.ads.nativeads.template.SizeConstraint.SizeConstraintType;

public final class NativeTemplateAppearance implements Parcelable {
    public static final Creator<NativeTemplateAppearance> CREATOR = new Creator<NativeTemplateAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new NativeTemplateAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new NativeTemplateAppearance(parcel);
        }
    };
    @VisibleForTesting
    static final int a = Color.parseColor("#7f7f7f");
    static final int b = Color.parseColor("#ffd200");
    static final int c = Color.parseColor("#ffd200");
    static final int d = Color.parseColor("#f4c900");
    private final BannerAppearance e;
    private final TextAppearance f;
    private final TextAppearance g;
    private final TextAppearance h;
    private final TextAppearance i;
    private final TextAppearance j;
    private final TextAppearance k;
    private final TextAppearance l;
    private final ImageAppearance m;
    private final ImageAppearance n;
    private final ButtonAppearance o;
    private final RatingAppearance p;

    public static final class Builder {
        /* access modifiers changed from: private */
        public BannerAppearance a = new com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder().setBackgroundColor(-1).setBorderColor(ee.a((int) ViewCompat.MEASURED_STATE_MASK, 90.0f)).setBorderWidth(1.0f).setContentPadding(new HorizontalOffset(10.0f, 10.0f)).setImageMargins(new HorizontalOffset(0.0f, 10.0f)).build();
        /* access modifiers changed from: private */
        public TextAppearance b = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(NativeTemplateAppearance.a).setTextSize(15.0f).setFontFamilyName(null).setFontStyle(0).build();
        /* access modifiers changed from: private */
        public TextAppearance c = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(ViewCompat.MEASURED_STATE_MASK).setTextSize(13.0f).setFontFamilyName(null).setFontStyle(0).build();
        /* access modifiers changed from: private */
        public TextAppearance d = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(NativeTemplateAppearance.a).setTextSize(13.0f).setFontFamilyName(null).setFontStyle(0).build();
        /* access modifiers changed from: private */
        public TextAppearance e = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(NativeTemplateAppearance.a).setTextSize(13.0f).setFontFamilyName(null).setFontStyle(0).build();
        /* access modifiers changed from: private */
        public TextAppearance f = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(NativeTemplateAppearance.a).setTextSize(11.0f).setFontFamilyName(null).setFontStyle(0).build();
        /* access modifiers changed from: private */
        public TextAppearance g = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(ViewCompat.MEASURED_STATE_MASK).setTextSize(15.0f).setFontFamilyName(null).setFontStyle(1).build();
        /* access modifiers changed from: private */
        public TextAppearance h = new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(NativeTemplateAppearance.a).setTextSize(13.0f).setFontFamilyName(null).setFontStyle(0).build();
        /* access modifiers changed from: private */
        public ImageAppearance i = new com.yandex.mobile.ads.nativeads.template.appearance.ImageAppearance.Builder().setWidthConstraint(new SizeConstraint(SizeConstraintType.PREFERRED_RATIO, 0.3f)).build();
        /* access modifiers changed from: private */
        public ImageAppearance j = new com.yandex.mobile.ads.nativeads.template.appearance.ImageAppearance.Builder().setWidthConstraint(new SizeConstraint(SizeConstraintType.FIXED, 16.0f)).build();
        /* access modifiers changed from: private */
        public ButtonAppearance k = new com.yandex.mobile.ads.nativeads.template.appearance.ButtonAppearance.Builder().setBorderColor(NativeTemplateAppearance.b).setBorderWidth(1.0f).setNormalColor(-1).setPressedColor(NativeTemplateAppearance.c).setTextAppearance(new com.yandex.mobile.ads.nativeads.template.appearance.TextAppearance.Builder().setTextColor(ViewCompat.MEASURED_STATE_MASK).setTextSize(13.0f).setFontFamilyName(null).setFontStyle(0).build()).build();
        /* access modifiers changed from: private */
        public RatingAppearance l = new com.yandex.mobile.ads.nativeads.template.appearance.RatingAppearance.Builder().setBackgroundStarColor(-3355444).setProgressStarColor(NativeTemplateAppearance.d).build();

        public final NativeTemplateAppearance build() {
            return new NativeTemplateAppearance(this, 0);
        }

        public final Builder withAgeAppearance(TextAppearance textAppearance) {
            this.b = a.a(this.b, textAppearance);
            return this;
        }

        public final Builder withBannerAppearance(BannerAppearance bannerAppearance) {
            HorizontalOffset horizontalOffset;
            BannerAppearance bannerAppearance2 = this.a;
            if (bannerAppearance != null && !bannerAppearance2.equals(bannerAppearance)) {
                int backgroundColor = bannerAppearance.getBackgroundColor();
                int borderColor = bannerAppearance.getBorderColor();
                float borderWidth = bannerAppearance.getBorderWidth();
                HorizontalOffset imageMargins = bannerAppearance.getImageMargins();
                com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder builder = new com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder();
                if (backgroundColor == 0) {
                    backgroundColor = bannerAppearance2.getBackgroundColor();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder backgroundColor2 = builder.setBackgroundColor(backgroundColor);
                if (borderColor == 0) {
                    borderColor = bannerAppearance2.getBorderColor();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder borderColor2 = backgroundColor2.setBorderColor(borderColor);
                float f2 = 0.0f;
                if (borderWidth < 0.0f) {
                    borderWidth = bannerAppearance2.getBorderWidth();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder borderWidth2 = borderColor2.setBorderWidth(borderWidth);
                HorizontalOffset contentPadding = bannerAppearance2.getContentPadding();
                HorizontalOffset contentPadding2 = bannerAppearance.getContentPadding();
                if (contentPadding2 == null || contentPadding.equals(contentPadding2)) {
                    horizontalOffset = contentPadding;
                } else {
                    float left = contentPadding2.getLeft() >= 0.0f ? contentPadding2.getLeft() : 0.0f;
                    if (contentPadding2.getRight() >= 0.0f) {
                        f2 = contentPadding2.getRight();
                    }
                    horizontalOffset = new HorizontalOffset(left, f2);
                }
                com.yandex.mobile.ads.nativeads.template.appearance.BannerAppearance.Builder contentPadding3 = borderWidth2.setContentPadding(horizontalOffset);
                if (imageMargins == null) {
                    imageMargins = bannerAppearance2.getImageMargins();
                }
                bannerAppearance2 = contentPadding3.setImageMargins(imageMargins).build();
            }
            this.a = bannerAppearance2;
            return this;
        }

        public final Builder withBodyAppearance(TextAppearance textAppearance) {
            this.c = a.a(this.c, textAppearance);
            return this;
        }

        public final Builder withCallToActionAppearance(ButtonAppearance buttonAppearance) {
            ButtonAppearance buttonAppearance2 = this.k;
            if (buttonAppearance != null && !buttonAppearance2.equals(buttonAppearance)) {
                TextAppearance a2 = a.a(buttonAppearance2.getTextAppearance(), buttonAppearance.getTextAppearance());
                int borderColor = buttonAppearance.getBorderColor();
                float borderWidth = buttonAppearance.getBorderWidth();
                int normalColor = buttonAppearance.getNormalColor();
                int pressedColor = buttonAppearance.getPressedColor();
                com.yandex.mobile.ads.nativeads.template.appearance.ButtonAppearance.Builder textAppearance = new com.yandex.mobile.ads.nativeads.template.appearance.ButtonAppearance.Builder().setTextAppearance(a2);
                if (borderColor == 0) {
                    borderColor = buttonAppearance2.getBorderColor();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.ButtonAppearance.Builder borderColor2 = textAppearance.setBorderColor(borderColor);
                if (borderWidth < 0.0f) {
                    borderWidth = buttonAppearance2.getBorderWidth();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.ButtonAppearance.Builder borderWidth2 = borderColor2.setBorderWidth(borderWidth);
                if (normalColor == 0) {
                    normalColor = buttonAppearance2.getNormalColor();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.ButtonAppearance.Builder normalColor2 = borderWidth2.setNormalColor(normalColor);
                if (pressedColor == 0) {
                    pressedColor = buttonAppearance2.getPressedColor();
                }
                buttonAppearance2 = normalColor2.setPressedColor(pressedColor).build();
            }
            this.k = buttonAppearance2;
            return this;
        }

        public final Builder withDomainAppearance(TextAppearance textAppearance) {
            this.d = a.a(this.d, textAppearance);
            return this;
        }

        public final Builder withFaviconAppearance(ImageAppearance imageAppearance) {
            this.j = a.a(this.j, imageAppearance);
            return this;
        }

        public final Builder withImageAppearance(ImageAppearance imageAppearance) {
            this.i = a.a(this.i, imageAppearance);
            return this;
        }

        public final Builder withRatingAppearance(RatingAppearance ratingAppearance) {
            RatingAppearance ratingAppearance2 = this.l;
            if (ratingAppearance != null && !ratingAppearance2.equals(ratingAppearance)) {
                int backgroundStarColor = ratingAppearance.getBackgroundStarColor();
                int progressStarColor = ratingAppearance.getProgressStarColor();
                com.yandex.mobile.ads.nativeads.template.appearance.RatingAppearance.Builder builder = new com.yandex.mobile.ads.nativeads.template.appearance.RatingAppearance.Builder();
                if (backgroundStarColor == 0) {
                    backgroundStarColor = ratingAppearance2.getBackgroundStarColor();
                }
                com.yandex.mobile.ads.nativeads.template.appearance.RatingAppearance.Builder backgroundStarColor2 = builder.setBackgroundStarColor(backgroundStarColor);
                if (progressStarColor == 0) {
                    progressStarColor = ratingAppearance2.getProgressStarColor();
                }
                ratingAppearance2 = backgroundStarColor2.setProgressStarColor(progressStarColor).build();
            }
            this.l = ratingAppearance2;
            return this;
        }

        public final Builder withReviewCountAppearance(TextAppearance textAppearance) {
            this.e = a.a(this.e, textAppearance);
            return this;
        }

        public final Builder withSponsoredAppearance(TextAppearance textAppearance) {
            this.f = a.a(this.f, textAppearance);
            return this;
        }

        public final Builder withTitleAppearance(TextAppearance textAppearance) {
            this.g = a.a(this.g, textAppearance);
            return this;
        }

        public final Builder withWarningAppearance(TextAppearance textAppearance) {
            this.h = a.a(this.h, textAppearance);
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ NativeTemplateAppearance(Builder builder, byte b2) {
        this(builder);
    }

    private NativeTemplateAppearance(Builder builder) {
        this.e = builder.a;
        this.f = builder.b;
        this.g = builder.c;
        this.h = builder.d;
        this.i = builder.e;
        this.j = builder.f;
        this.k = builder.g;
        this.l = builder.h;
        this.n = builder.i;
        this.m = builder.j;
        this.o = builder.k;
        this.p = builder.l;
    }

    public final TextAppearance getAgeAppearance() {
        return this.f;
    }

    public final BannerAppearance getBannerAppearance() {
        return this.e;
    }

    public final TextAppearance getBodyAppearance() {
        return this.g;
    }

    public final ButtonAppearance getCallToActionAppearance() {
        return this.o;
    }

    public final TextAppearance getDomainAppearance() {
        return this.h;
    }

    public final ImageAppearance getFaviconAppearance() {
        return this.m;
    }

    public final ImageAppearance getImageAppearance() {
        return this.n;
    }

    public final RatingAppearance getRatingAppearance() {
        return this.p;
    }

    public final TextAppearance getReviewCountAppearance() {
        return this.i;
    }

    public final TextAppearance getSponsoredAppearance() {
        return this.j;
    }

    public final TextAppearance getTitleAppearance() {
        return this.k;
    }

    public final TextAppearance getWarningAppearance() {
        return this.l;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NativeTemplateAppearance nativeTemplateAppearance = (NativeTemplateAppearance) obj;
        if (this.e == null ? nativeTemplateAppearance.e != null : !this.e.equals(nativeTemplateAppearance.e)) {
            return false;
        }
        if (this.f == null ? nativeTemplateAppearance.f != null : !this.f.equals(nativeTemplateAppearance.f)) {
            return false;
        }
        if (this.g == null ? nativeTemplateAppearance.g != null : !this.g.equals(nativeTemplateAppearance.g)) {
            return false;
        }
        if (this.h == null ? nativeTemplateAppearance.h != null : !this.h.equals(nativeTemplateAppearance.h)) {
            return false;
        }
        if (this.i == null ? nativeTemplateAppearance.i != null : !this.i.equals(nativeTemplateAppearance.i)) {
            return false;
        }
        if (this.j == null ? nativeTemplateAppearance.j != null : !this.j.equals(nativeTemplateAppearance.j)) {
            return false;
        }
        if (this.k == null ? nativeTemplateAppearance.k != null : !this.k.equals(nativeTemplateAppearance.k)) {
            return false;
        }
        if (this.l == null ? nativeTemplateAppearance.l != null : !this.l.equals(nativeTemplateAppearance.l)) {
            return false;
        }
        if (this.n == null ? nativeTemplateAppearance.n != null : !this.n.equals(nativeTemplateAppearance.n)) {
            return false;
        }
        if (this.m == null ? nativeTemplateAppearance.m != null : !this.m.equals(nativeTemplateAppearance.m)) {
            return false;
        }
        if (this.o == null ? nativeTemplateAppearance.o == null : this.o.equals(nativeTemplateAppearance.o)) {
            return this.p == null ? nativeTemplateAppearance.p == null : this.p.equals(nativeTemplateAppearance.p);
        }
        return false;
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = (((((((((((((((((((((this.e != null ? this.e.hashCode() : 0) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31) + (this.h != null ? this.h.hashCode() : 0)) * 31) + (this.i != null ? this.i.hashCode() : 0)) * 31) + (this.j != null ? this.j.hashCode() : 0)) * 31) + (this.k != null ? this.k.hashCode() : 0)) * 31) + (this.l != null ? this.l.hashCode() : 0)) * 31) + (this.n != null ? this.n.hashCode() : 0)) * 31) + (this.m != null ? this.m.hashCode() : 0)) * 31) + (this.o != null ? this.o.hashCode() : 0)) * 31;
        if (this.p != null) {
            i2 = this.p.hashCode();
        }
        return hashCode + i2;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelable(this.e, i2);
        parcel.writeParcelable(this.f, i2);
        parcel.writeParcelable(this.g, i2);
        parcel.writeParcelable(this.h, i2);
        parcel.writeParcelable(this.i, i2);
        parcel.writeParcelable(this.j, i2);
        parcel.writeParcelable(this.k, i2);
        parcel.writeParcelable(this.l, i2);
        parcel.writeParcelable(this.m, i2);
        parcel.writeParcelable(this.n, i2);
        parcel.writeParcelable(this.o, i2);
        parcel.writeParcelable(this.p, i2);
    }

    protected NativeTemplateAppearance(Parcel parcel) {
        this.e = (BannerAppearance) parcel.readParcelable(BannerAppearance.class.getClassLoader());
        this.f = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.g = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.h = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.i = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.j = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.k = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.l = (TextAppearance) parcel.readParcelable(TextAppearance.class.getClassLoader());
        this.m = (ImageAppearance) parcel.readParcelable(ImageAppearance.class.getClassLoader());
        this.n = (ImageAppearance) parcel.readParcelable(ImageAppearance.class.getClassLoader());
        this.o = (ButtonAppearance) parcel.readParcelable(ButtonAppearance.class.getClassLoader());
        this.p = (RatingAppearance) parcel.readParcelable(RatingAppearance.class.getClassLoader());
    }
}
