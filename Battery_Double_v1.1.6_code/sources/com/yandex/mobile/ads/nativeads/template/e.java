package com.yandex.mobile.ads.nativeads.template;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeAdAssets;
import com.yandex.mobile.ads.nativeads.NativeAdImage;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import com.yandex.mobile.ads.nativeads.NativeAdType;

public final class e {
    private final NativeAdType a;
    private final String b;
    private final NativeAdImage c;
    private final Float d;
    private final String e;
    private final String f;

    public e(@NonNull NativeAdAssets nativeAdAssets, @NonNull NativeAdType nativeAdType) {
        this.b = nativeAdAssets.getCallToAction();
        this.c = nativeAdAssets.getImage();
        this.d = nativeAdAssets.getRating();
        this.e = nativeAdAssets.getReviewCount();
        this.f = nativeAdAssets.getWarning();
        this.a = nativeAdType;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.c != null && (NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_LARGE.equals(this.c.a()) || "wide".equals(this.c.a()));
    }

    private boolean g() {
        return j() && (NativeAdType.CONTENT == this.a || i());
    }

    private boolean h() {
        return (this.d == null && this.e == null) ? false : true;
    }

    private boolean i() {
        return !h();
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return j() && (g() || a());
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        return j() || h();
    }

    private boolean j() {
        return this.b != null;
    }

    /* access modifiers changed from: 0000 */
    public final boolean d() {
        return g();
    }

    /* access modifiers changed from: 0000 */
    public final boolean e() {
        return g() || (a() && i());
    }

    public final boolean f() {
        return this.f != null;
    }
}
