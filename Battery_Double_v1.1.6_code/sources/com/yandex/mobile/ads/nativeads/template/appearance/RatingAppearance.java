package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class RatingAppearance implements Parcelable {
    public static final Creator<RatingAppearance> CREATOR = new Creator<RatingAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new RatingAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new RatingAppearance(parcel);
        }
    };
    private final int a;
    private final int b;

    public static final class Builder {
        /* access modifiers changed from: private */
        public int a;
        /* access modifiers changed from: private */
        public int b;

        public final RatingAppearance build() {
            return new RatingAppearance(this, 0);
        }

        public final Builder setBackgroundStarColor(int i) {
            this.a = i;
            return this;
        }

        public final Builder setProgressStarColor(int i) {
            this.b = i;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ RatingAppearance(Builder builder, byte b2) {
        this(builder);
    }

    private RatingAppearance(Builder builder) {
        this.a = builder.a;
        this.b = builder.b;
    }

    public final int getBackgroundStarColor() {
        return this.a;
    }

    public final int getProgressStarColor() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RatingAppearance ratingAppearance = (RatingAppearance) obj;
        return this.a == ratingAppearance.a && this.b == ratingAppearance.b;
    }

    public final int hashCode() {
        return (this.a * 31) + this.b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.b);
    }

    protected RatingAppearance(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readInt();
    }
}
