package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.yandex.mobile.ads.nativeads.template.SizeConstraint;

public final class ImageAppearance implements Parcelable {
    public static final Creator<ImageAppearance> CREATOR = new Creator<ImageAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new ImageAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new ImageAppearance(parcel);
        }
    };
    private final SizeConstraint a;

    public static final class Builder {
        /* access modifiers changed from: private */
        public SizeConstraint a;

        public final ImageAppearance build() {
            return new ImageAppearance(this, 0);
        }

        public final Builder setWidthConstraint(SizeConstraint sizeConstraint) {
            this.a = sizeConstraint;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ ImageAppearance(Builder builder, byte b) {
        this(builder);
    }

    private ImageAppearance(Builder builder) {
        this.a = builder.a;
    }

    public final SizeConstraint getWidthConstraint() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ImageAppearance imageAppearance = (ImageAppearance) obj;
        return this.a == null ? imageAppearance.a == null : this.a.equals(imageAppearance.a);
    }

    public final int hashCode() {
        if (this.a != null) {
            return this.a.hashCode();
        }
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
    }

    protected ImageAppearance(Parcel parcel) {
        this.a = (SizeConstraint) parcel.readParcelable(SizeConstraint.class.getClassLoader());
    }
}
