package com.yandex.mobile.ads.nativeads.template.appearance;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.yandex.mobile.ads.nativeads.template.HorizontalOffset;

public final class BannerAppearance implements Parcelable {
    public static final Creator<BannerAppearance> CREATOR = new Creator<BannerAppearance>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new BannerAppearance[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new BannerAppearance(parcel);
        }
    };
    private final int a;
    private final int b;
    private final float c;
    private final HorizontalOffset d;
    private final HorizontalOffset e;

    public static final class Builder {
        /* access modifiers changed from: private */
        public int a;
        /* access modifiers changed from: private */
        public int b;
        /* access modifiers changed from: private */
        public float c;
        /* access modifiers changed from: private */
        public HorizontalOffset d;
        /* access modifiers changed from: private */
        public HorizontalOffset e;

        public final BannerAppearance build() {
            return new BannerAppearance(this, 0);
        }

        public final Builder setBackgroundColor(int i) {
            this.a = i;
            return this;
        }

        public final Builder setBorderColor(int i) {
            this.b = i;
            return this;
        }

        public final Builder setBorderWidth(float f) {
            this.c = f;
            return this;
        }

        public final Builder setContentPadding(HorizontalOffset horizontalOffset) {
            this.d = horizontalOffset;
            return this;
        }

        public final Builder setImageMargins(HorizontalOffset horizontalOffset) {
            this.e = horizontalOffset;
            return this;
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ BannerAppearance(Builder builder, byte b2) {
        this(builder);
    }

    private BannerAppearance(Builder builder) {
        this.a = builder.a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
        this.e = builder.e;
    }

    public final int getBackgroundColor() {
        return this.a;
    }

    public final int getBorderColor() {
        return this.b;
    }

    public final float getBorderWidth() {
        return this.c;
    }

    public final HorizontalOffset getContentPadding() {
        return this.d;
    }

    public final HorizontalOffset getImageMargins() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BannerAppearance bannerAppearance = (BannerAppearance) obj;
        if (this.a != bannerAppearance.a || this.b != bannerAppearance.b || Float.compare(bannerAppearance.c, this.c) != 0) {
            return false;
        }
        if (this.d == null ? bannerAppearance.d == null : this.d.equals(bannerAppearance.d)) {
            return this.e == null ? bannerAppearance.e == null : this.e.equals(bannerAppearance.e);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int floatToIntBits = ((((((this.a * 31) + this.b) * 31) + (this.c != 0.0f ? Float.floatToIntBits(this.c) : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31;
        if (this.e != null) {
            i = this.e.hashCode();
        }
        return floatToIntBits + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.b);
        parcel.writeFloat(this.c);
        parcel.writeParcelable(this.d, 0);
        parcel.writeParcelable(this.e, 0);
    }

    protected BannerAppearance(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readFloat();
        this.d = (HorizontalOffset) parcel.readParcelable(HorizontalOffset.class.getClassLoader());
        this.e = (HorizontalOffset) parcel.readParcelable(HorizontalOffset.class.getClassLoader());
    }
}
