package com.yandex.mobile.ads.nativeads.template;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class SizeConstraint implements Parcelable {
    public static final Creator<SizeConstraint> CREATOR = new Creator<SizeConstraint>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new SizeConstraint[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new SizeConstraint(parcel);
        }
    };
    private final float a;
    private final SizeConstraintType b;

    public enum SizeConstraintType {
        FIXED,
        FIXED_RATIO,
        PREFERRED_RATIO
    }

    public final int describeContents() {
        return 0;
    }

    public SizeConstraint(SizeConstraintType sizeConstraintType, float f) {
        this.b = sizeConstraintType;
        this.a = f;
    }

    public final float getValue() {
        return this.a;
    }

    public final SizeConstraintType getSizeConstraintType() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SizeConstraint sizeConstraint = (SizeConstraint) obj;
        return Float.compare(sizeConstraint.a, this.a) == 0 && this.b == sizeConstraint.b;
    }

    public final int hashCode() {
        int i = 0;
        int floatToIntBits = (this.a != 0.0f ? Float.floatToIntBits(this.a) : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return floatToIntBits + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.a);
        parcel.writeInt(this.b == null ? -1 : this.b.ordinal());
    }

    protected SizeConstraint(Parcel parcel) {
        SizeConstraintType sizeConstraintType;
        this.a = parcel.readFloat();
        int readInt = parcel.readInt();
        if (readInt == -1) {
            sizeConstraintType = null;
        } else {
            sizeConstraintType = SizeConstraintType.values()[readInt];
        }
        this.b = sizeConstraintType;
    }
}
