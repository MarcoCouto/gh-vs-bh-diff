package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.om;
import com.yandex.mobile.ads.impl.pe;
import com.yandex.mobile.ads.impl.pe.b;
import java.util.Map;

public final class d implements j {
    @NonNull
    private final Context a;

    public final void a(@NonNull Map<String, Bitmap> map) {
    }

    public d(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @Nullable
    public final Bitmap a(@NonNull om omVar) {
        b a2 = pe.a(this.a).a();
        String c = omVar.c();
        if (c == null) {
            return null;
        }
        Bitmap a3 = a2.a(c);
        if (a3 == null || a3.getWidth() != 1 || a3.getHeight() != 1) {
            return a3;
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(a3, omVar.a(), omVar.b(), false);
        a2.a(c, createScaledBitmap);
        return createScaledBitmap;
    }
}
