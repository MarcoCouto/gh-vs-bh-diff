package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ok;

public final class av {
    @Nullable
    private final String a;
    @NonNull
    private final a b;

    public enum a {
        TEXT,
        IMAGE
    }

    av(@NonNull ok okVar) {
        this.a = okVar.a();
        this.b = okVar.b();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        av avVar = (av) obj;
        if (this.a == null ? avVar.a == null : this.a.equals(avVar.a)) {
            return this.b == avVar.b;
        }
        return false;
    }

    public final int hashCode() {
        return ((this.a != null ? this.a.hashCode() : 0) * 31) + this.b.hashCode();
    }
}
