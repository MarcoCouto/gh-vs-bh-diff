package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.oq;
import com.yandex.mobile.ads.impl.or;
import java.util.ArrayList;
import java.util.List;

public final class o {
    @NonNull
    public static bh a(@NonNull p pVar, @NonNull oq oqVar) {
        return new bh(oqVar.c(), a((T) oqVar.d(), pVar.c().e()), a((T) oqVar.e(), pVar.c().f()));
    }

    @NonNull
    public static c a(@NonNull p pVar, @NonNull bh bhVar, @NonNull bf bfVar, @NonNull t tVar) {
        c cVar = new c(pVar, new af(new m(bhVar.a(), ao.a())), bhVar, bfVar, tVar);
        return cVar;
    }

    @NonNull
    private static List<ba> a(@NonNull List<? extends NativeGenericAd> list) {
        ArrayList arrayList = new ArrayList();
        for (NativeGenericAd nativeGenericAd : list) {
            arrayList.add((ba) nativeGenericAd);
        }
        return arrayList;
    }

    @NonNull
    private static <T> List<T> a(@Nullable T t, @Nullable List<T> list) {
        ArrayList arrayList = new ArrayList();
        if (t != null) {
            arrayList.add(t);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @NonNull
    public static NativeAdUnit a(@NonNull Context context, @NonNull p pVar, @NonNull j jVar, @NonNull t tVar) {
        ArrayList arrayList = new ArrayList();
        List<oq> c = pVar.c().c();
        bg b = tVar.b();
        for (oq oqVar : c) {
            bf a = b.a(oqVar);
            w wVar = new w(context, oqVar, jVar, a);
            c a2 = a(pVar, new bh(oqVar.c(), a((T) oqVar.d(), null), a((T) oqVar.e(), null)), a, tVar);
            if (NativeAdType.CONTENT == oqVar.b()) {
                aw awVar = new aw(context, oqVar, wVar, jVar, a2);
                arrayList.add(awVar);
            } else if (NativeAdType.APP_INSTALL == oqVar.b()) {
                aq aqVar = new aq(context, oqVar, wVar, jVar, a2);
                arrayList.add(aqVar);
            } else if (NativeAdType.IMAGE == oqVar.b()) {
                bb bbVar = new bb(context, oqVar, wVar, jVar, a2);
                arrayList.add(bbVar);
            }
        }
        List a3 = a(arrayList);
        or c2 = pVar.c();
        bh bhVar = new bh(c2.b(), a((T) null, c2.e()), a((T) null, c2.f()));
        c cVar = new c(pVar, new ac(a3, new aa(bhVar.a(), ao.a())), bhVar, new ae(bhVar.a()), tVar);
        return new ab(context, arrayList, jVar, cVar);
    }
}
