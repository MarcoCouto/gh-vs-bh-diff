package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.al.a;

public final class as implements ak<NativeAppInstallAdView> {
    @NonNull
    public final /* synthetic */ al a(@NonNull Object obj) {
        NativeAppInstallAdView nativeAppInstallAdView = (NativeAppInstallAdView) obj;
        return new a(nativeAppInstallAdView).a(nativeAppInstallAdView.a()).b(nativeAppInstallAdView.b()).c((TextView) nativeAppInstallAdView.c()).d(nativeAppInstallAdView.d()).a(nativeAppInstallAdView.e()).b(nativeAppInstallAdView.f()).c(nativeAppInstallAdView.g()).a(nativeAppInstallAdView.h()).e(nativeAppInstallAdView.i()).a(nativeAppInstallAdView.j()).f(nativeAppInstallAdView.k()).g(nativeAppInstallAdView.l()).h(nativeAppInstallAdView.m()).i(nativeAppInstallAdView.n()).a();
    }
}
