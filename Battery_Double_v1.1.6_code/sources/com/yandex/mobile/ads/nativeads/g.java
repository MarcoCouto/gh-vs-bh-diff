package com.yandex.mobile.ads.nativeads;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.co;
import com.yandex.mobile.ads.impl.dk;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;

public final class g implements dk {
    @NonNull
    private final Handler a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @NonNull
    public final AdTapHandler b;

    public g(@NonNull AdTapHandler adTapHandler) {
        this.b = adTapHandler;
    }

    public final void a(@NonNull co coVar, @Nullable final String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("click_type", "custom");
        coVar.a(b.CLICK, hashMap);
        this.a.post(new Runnable() {
            public final void run() {
                g.this.b.handleAdTapWithURL(str);
            }
        });
    }
}
