package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ag;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.at;
import com.yandex.mobile.ads.impl.ax;
import com.yandex.mobile.ads.impl.bn;
import com.yandex.mobile.ads.impl.cl;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.nn;
import com.yandex.mobile.ads.impl.w;
import java.util.List;

public final class bq implements bm {
    @NonNull
    private final ax a;
    @NonNull
    private final ag b;
    @NonNull
    private final nn c;
    @NonNull
    private final cl d;

    bq(@NonNull ax axVar, @NonNull cl clVar, @NonNull nn nnVar, @NonNull ag agVar) {
        this.a = axVar;
        this.d = clVar;
        this.c = nnVar;
        this.b = agVar;
    }

    public final void a(@NonNull Context context, @NonNull b bVar, @Nullable ag agVar) {
        this.d.b();
        this.a.a();
        this.b.a(bVar, context);
        if (agVar != null) {
            this.c.a(context, agVar);
        }
    }

    public final void a(@NonNull Context context, @NonNull b bVar) {
        this.d.c();
        this.a.b();
        this.b.b(bVar, context);
        this.c.a();
    }

    public final void a(@NonNull w wVar, @NonNull List<bn> list) {
        this.a.a(wVar, list);
    }

    public final void a(@NonNull ag agVar) {
        this.c.a(agVar);
    }

    public final void a(@NonNull at atVar) {
        this.a.a(atVar);
    }

    public final void a(@NonNull a aVar) {
        this.d.a(aVar);
    }
}
