package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ct;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.oq;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class ap implements a {
    @NonNull
    private final fl a;
    @NonNull
    private final List<oq> b;
    @NonNull
    private final q c = new q();
    @NonNull
    private final ct d = new ct();
    @Nullable
    private String e;
    @Nullable
    private a f;

    ap(@NonNull List<oq> list, @NonNull fl flVar) {
        this.b = list;
        this.a = flVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull a aVar) {
        this.f = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str) {
        this.e = str;
    }

    @NonNull
    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        if (this.f != null) {
            hashMap.put("bind_type", this.f.c);
        }
        if (this.e != null) {
            hashMap.put("native_ad_type", this.e);
        }
        hashMap.putAll(ct.a(this.a.c()));
        List a2 = q.a(this.b);
        if (a2.size() > 0) {
            hashMap.put("image_sizes", a2.toArray(new String[a2.size()]));
        }
        return hashMap;
    }
}
