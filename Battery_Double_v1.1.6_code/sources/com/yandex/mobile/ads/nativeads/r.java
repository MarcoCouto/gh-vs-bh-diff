package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.as.a;
import com.yandex.mobile.ads.impl.dh;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.hr;
import com.yandex.mobile.ads.impl.ib;
import com.yandex.mobile.ads.impl.on;

public final class r implements a {
    @NonNull
    private final dh a;
    @Nullable
    private NativeAdEventListener b;

    public r(@NonNull Context context, @NonNull fl flVar) {
        this.a = new dh(context, flVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable NativeAdEventListener nativeAdEventListener) {
        this.b = nativeAdEventListener;
    }

    public final void a(@NonNull hr.a aVar) {
        this.a.a(aVar);
    }

    public final void a() {
        this.a.d();
    }

    public final void b() {
        if (this.b != null) {
            this.b.onAdClosed();
        }
        this.a.b();
    }

    public final void c() {
        if (this.b != null) {
            ib.a((Object) this.b, "onAdImpressionTracked", new Object[0]);
        }
    }

    public final void d() {
        if (this.b != null) {
            this.b.onAdLeftApplication();
        }
        this.a.c();
    }

    public final void e() {
        if (this.b != null) {
            this.b.onAdOpened();
        }
        this.a.a();
    }

    public final void g() {
        if (this.b instanceof ClosableNativeAdEventListener) {
            ((ClosableNativeAdEventListener) this.b).closeNativeAd();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        this.a.e();
    }

    public final void a(@NonNull on onVar) {
        this.a.a(onVar.b());
    }

    public final void f() {
        c();
    }
}
