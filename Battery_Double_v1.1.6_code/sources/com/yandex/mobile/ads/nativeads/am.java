package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.lf;
import com.yandex.mobile.ads.impl.li;
import com.yandex.mobile.ads.impl.mi;
import com.yandex.mobile.ads.impl.oj;
import com.yandex.mobile.ads.impl.oq;

public final class am implements bf {
    @NonNull
    private final oq a;
    @Nullable
    private ag b;

    am(@NonNull oq oqVar) {
        this.a = oqVar;
    }

    public final void a(@NonNull ag agVar) {
        agVar.a();
    }

    public final void a(@NonNull ag agVar, @NonNull e eVar) {
        this.b = agVar;
        mi miVar = new mi(agVar, eVar, this.a.a());
        for (oj ojVar : this.a.c()) {
            lf a2 = agVar.a(ojVar);
            if (a2 != null) {
                a2.a(ojVar.c());
                a2.a(ojVar, miVar);
            }
        }
    }

    public final void a() {
        if (this.b != null) {
            for (oj ojVar : this.a.c()) {
                lf a2 = this.b.a(ojVar);
                if (a2 instanceof li) {
                    ((li) a2).c(ojVar.c());
                }
            }
        }
    }
}
