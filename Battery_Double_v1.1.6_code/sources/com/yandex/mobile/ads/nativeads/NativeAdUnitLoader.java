package com.yandex.mobile.ads.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.impl.ae;
import com.yandex.mobile.ads.impl.af;
import com.yandex.mobile.ads.impl.qz;

public class NativeAdUnitLoader {
    @NonNull
    protected final Context a;
    @NonNull
    private final v b;

    public NativeAdUnitLoader(@NonNull Context context, @NonNull NativeAdLoaderConfiguration nativeAdLoaderConfiguration) {
        this.a = context.getApplicationContext();
        x xVar = new x(context);
        this.b = new v(this.a, nativeAdLoaderConfiguration, xVar);
        xVar.a(this.b.r());
    }

    public void setNativeAdUnitLoadListener(@Nullable NativeAdUnitLoadListener nativeAdUnitLoadListener) {
        this.b.a(nativeAdUnitLoadListener);
    }

    public void loadAdUnit(@NonNull AdRequest adRequest) {
        this.b.a(adRequest, new qz(this.a), ae.AD_UNIT, af.AD);
    }

    public void cancelLoading() {
        this.b.a();
    }
}
