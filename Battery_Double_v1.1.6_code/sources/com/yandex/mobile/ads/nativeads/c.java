package com.yandex.mobile.ads.nativeads;

import android.support.annotation.NonNull;

public final class c {
    @NonNull
    private final p a;
    @NonNull
    private final be b;
    @NonNull
    private final bh c;
    @NonNull
    private final bf d;
    @NonNull
    private final t e;

    c(@NonNull p pVar, @NonNull be beVar, @NonNull bh bhVar, @NonNull bf bfVar, @NonNull t tVar) {
        this.a = pVar;
        this.b = beVar;
        this.c = bhVar;
        this.d = bfVar;
        this.e = tVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final p a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final be b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bh c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bf d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final t e() {
        return this.e;
    }
}
