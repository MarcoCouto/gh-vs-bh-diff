package com.yandex.mobile.ads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.RelativeLayout;
import com.yandex.mobile.ads.impl.k;
import com.yandex.mobile.ads.impl.m;
import com.yandex.mobile.ads.impl.p;

public final class AdActivity extends Activity {
    public static final String a = AdActivity.class.getCanonicalName();
    @Nullable
    private RelativeLayout b;
    @Nullable
    private k c;

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        k kVar;
        super.onCreate(bundle);
        this.b = new RelativeLayout(this);
        RelativeLayout relativeLayout = this.b;
        Intent intent = getIntent();
        if (intent != null) {
            Window window = getWindow();
            ResultReceiver a2 = a(intent);
            kVar = m.a().a(this, relativeLayout, a2, new p(this, a2), intent, window);
        } else {
            kVar = null;
        }
        this.c = kVar;
        if (this.c != null) {
            this.c.a();
            this.c.c_();
            setContentView(this.b);
            return;
        }
        finish();
    }

    @Nullable
    private static ResultReceiver a(@NonNull Intent intent) {
        try {
            return (ResultReceiver) intent.getParcelableExtra("extra_receiver");
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        if (this.c != null) {
            this.c.d();
            this.c.g();
        }
        if (this.b != null) {
            this.b.removeAllViews();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onPause() {
        if (this.c != null) {
            this.c.f();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        if (this.c != null) {
            this.c.e();
        }
    }

    public final void onBackPressed() {
        if (this.c == null || this.c.c()) {
            super.onBackPressed();
        }
    }
}
