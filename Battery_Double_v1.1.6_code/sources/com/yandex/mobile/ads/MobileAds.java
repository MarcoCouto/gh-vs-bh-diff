package com.yandex.mobile.ads;

import com.yandex.mobile.ads.impl.Cif;
import com.yandex.mobile.ads.impl.fv;

public final class MobileAds {
    public static String getLibraryVersion() {
        return "2.142";
    }

    public static void enableLogging(boolean z) {
        Cif.a(z);
    }

    public static void enableDebugErrorIndicator(boolean z) {
        fv.a().a(z);
    }

    public static void setUserConsent(boolean z) {
        fv.a().b(z);
    }
}
