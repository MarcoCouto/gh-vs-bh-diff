package com.yandex.mobile.ads.core.identifiers.ad.huawei;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gm;
import com.yandex.mobile.ads.impl.gn;

final class c {
    @NonNull
    private final gn a = new gn();

    c() {
    }

    @Nullable
    static gm a(@NonNull b bVar) {
        try {
            OpenDeviceIdentifierService a2 = bVar.a();
            if (a2 != null) {
                return gn.a(a2.getOaid(), Boolean.valueOf(a2.isOaidTrackLimited()));
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
