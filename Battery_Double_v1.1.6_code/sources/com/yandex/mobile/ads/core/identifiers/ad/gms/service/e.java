package com.yandex.mobile.ads.core.identifiers.ad.gms.service;

import android.os.IBinder;
import android.os.IInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gm;
import com.yandex.mobile.ads.impl.gn;

final class e {
    @NonNull
    private final gn a = new gn();
    @NonNull
    private final b b = new b();

    e() {
    }

    @Nullable
    static gm a(@NonNull c cVar) {
        a aVar;
        try {
            IBinder a2 = cVar.a();
            if (a2 == null) {
                return null;
            }
            IInterface queryLocalInterface = a2.queryLocalInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
            if (queryLocalInterface instanceof a) {
                aVar = (a) queryLocalInterface;
            } else {
                aVar = new GmsServiceAdvertisingInfoReader(a2);
            }
            return gn.a(aVar.readAdvertisingId(), aVar.readAdTrackingLimited());
        } catch (InterruptedException unused) {
            return null;
        }
    }
}
