package com.yandex.mobile.ads.core.identifiers.ad.huawei;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gm;
import com.yandex.mobile.ads.impl.gr;
import com.yandex.mobile.ads.impl.ic;

public final class a implements gr {
    @NonNull
    private final Context a;
    @NonNull
    private final ic b = new ic();
    @NonNull
    private final c c = new c();
    @NonNull
    private final d d = new d();

    public a(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @Nullable
    private gm a(@NonNull Intent intent) {
        try {
            b bVar = new b();
            if (this.a.bindService(intent, bVar, 1)) {
                gm a2 = c.a(bVar);
                try {
                    this.a.unbindService(bVar);
                    return a2;
                } catch (Throwable unused) {
                    return a2;
                }
            }
        } catch (Throwable unused2) {
        }
        return null;
    }

    @Nullable
    public final gm a() {
        Intent intent = new Intent("com.uodis.opendevice.OPENIDS_SERVICE");
        intent.setPackage("com.huawei.hwid");
        if (ic.a(this.a, intent) != null) {
            return a(intent);
        }
        return null;
    }
}
