package com.yandex.mobile.ads.core.identifiers.ad.gms.service;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.gm;
import com.yandex.mobile.ads.impl.ic;

public final class d {
    @NonNull
    private final Context a;
    @NonNull
    private final ic b = new ic();
    @NonNull
    private final e c = new e();
    @NonNull
    private final f d = new f();

    public d(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @Nullable
    private gm a(@NonNull Intent intent) {
        try {
            c cVar = new c();
            if (this.a.bindService(intent, cVar, 1)) {
                gm a2 = e.a(cVar);
                try {
                    this.a.unbindService(cVar);
                    return a2;
                } catch (Throwable unused) {
                    return a2;
                }
            }
        } catch (Throwable unused2) {
        }
        return null;
    }

    @Nullable
    public final gm a() {
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        if (ic.a(this.a, intent) != null) {
            return a(intent);
        }
        return null;
    }
}
