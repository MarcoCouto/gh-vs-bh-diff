package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.a;
import com.yandex.mobile.ads.impl.bk;
import com.yandex.mobile.ads.impl.w;
import java.lang.ref.WeakReference;

public final class h implements b {
    @NonNull
    private final WeakReference<a> a;
    @NonNull
    private final bk b;

    public final void a(@NonNull Context context) {
    }

    public h(@NonNull a aVar) {
        this.a = new WeakReference<>(aVar);
        this.b = new bk(aVar.r());
    }

    public final void a(@NonNull Context context, @NonNull w<String> wVar) {
        a aVar = (a) this.a.get();
        if (aVar != null) {
            this.b.a(context, wVar);
            this.b.b(context, wVar);
            aVar.b(wVar);
        }
    }
}
