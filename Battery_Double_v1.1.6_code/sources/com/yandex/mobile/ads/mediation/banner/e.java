package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.a;
import com.yandex.mobile.ads.impl.bc;
import com.yandex.mobile.ads.impl.bf;
import com.yandex.mobile.ads.impl.bh;
import com.yandex.mobile.ads.impl.bi;
import com.yandex.mobile.ads.impl.bl;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.w;

public final class e implements b {
    @NonNull
    private final MediatedBannerAdapterListener a;
    @NonNull
    private final bc<MediatedBannerAdapter, MediatedBannerAdapterListener> b;

    public e(@NonNull a aVar, @NonNull w<String> wVar, @NonNull bl blVar) {
        fl r = aVar.r();
        f fVar = new f(r);
        bi biVar = new bi(r, wVar);
        bc bcVar = new bc(r, new d(), biVar, new c(new bf(blVar, fVar, biVar)), new bh(aVar, blVar));
        this.b = bcVar;
        this.a = new a(aVar, this.b, new g(aVar.a(), this.b));
    }

    public final void a(@NonNull Context context) {
        this.b.a(context);
    }

    public final void a(@NonNull Context context, @NonNull w<String> wVar) {
        this.b.a(context, this.a);
    }
}
