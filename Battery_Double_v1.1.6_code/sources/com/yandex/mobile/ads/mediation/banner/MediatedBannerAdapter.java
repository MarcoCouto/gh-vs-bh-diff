package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.Map;

abstract class MediatedBannerAdapter extends a {

    interface MediatedBannerAdapterListener {
        void onAdClicked();

        void onAdFailedToLoad(@NonNull AdRequestError adRequestError);

        void onAdLeftApplication();

        void onAdLoaded(@NonNull View view);
    }

    /* access modifiers changed from: 0000 */
    public abstract void loadBanner(@NonNull Context context, @NonNull MediatedBannerAdapterListener mediatedBannerAdapterListener, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2);

    /* access modifiers changed from: 0000 */
    public abstract void onInvalidate();

    MediatedBannerAdapter() {
    }
}
