package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.al;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.fl;
import java.util.Map;

final class f extends bg {
    f(@NonNull fl flVar) {
        super(flVar);
    }

    @NonNull
    public final Map<String, Object> a(@NonNull Context context) {
        Map<String, Object> a = super.a(context);
        al b = this.a.b();
        if (b != null) {
            a.put("width", Integer.valueOf(b.a()));
            a.put("height", Integer.valueOf(b.b()));
        }
        return a;
    }
}
