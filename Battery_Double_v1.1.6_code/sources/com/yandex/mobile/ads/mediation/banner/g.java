package com.yandex.mobile.ads.mediation.banner;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.impl.bc;
import java.lang.ref.WeakReference;

final class g {
    /* access modifiers changed from: private */
    @NonNull
    public final Handler a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @NonNull
    public final WeakReference<ViewGroup> b;
    /* access modifiers changed from: private */
    @NonNull
    public final bc<MediatedBannerAdapter, MediatedBannerAdapterListener> c;

    g(@Nullable ViewGroup viewGroup, @NonNull bc<MediatedBannerAdapter, MediatedBannerAdapterListener> bcVar) {
        this.b = new WeakReference<>(viewGroup);
        this.c = bcVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final View view) {
        this.a.post(new Runnable() {
            public final void run() {
                ViewGroup viewGroup = (ViewGroup) g.this.b.get();
                if (viewGroup != null && viewGroup.indexOfChild(view) == -1) {
                    LayoutParams layoutParams = new LayoutParams(-2, -2);
                    layoutParams.addRule(13);
                    viewGroup.addView(view, layoutParams);
                    viewGroup.setVisibility(0);
                    view.setVisibility(0);
                    g gVar = g.this;
                    View view = view;
                    view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener(view) {
                        final /* synthetic */ View a;

                        {
                            this.a = r2;
                        }

                        public final boolean onPreDraw() {
                            new StringBuilder("onPreDraw(), clazz = ").append(this);
                            if (this.a.isShown()) {
                                this.a.getViewTreeObserver().removeOnPreDrawListener(this);
                                g.this.c.c(this.a.getContext());
                                g.this.a.postDelayed(new Runnable() {
                                    public final void run() {
                                        g.c(g.this);
                                    }
                                }, 50);
                            }
                            return true;
                        }
                    });
                }
            }
        });
    }

    static /* synthetic */ void c(g gVar) {
        ViewGroup viewGroup = (ViewGroup) gVar.b.get();
        if (viewGroup != null && viewGroup.getChildCount() > 0) {
            int childCount = viewGroup.getChildCount() - 1;
            if (childCount > 0) {
                viewGroup.removeViews(0, childCount);
            }
        }
    }
}
