package com.yandex.mobile.ads.mediation.banner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.bf;

final class c implements bd<MediatedBannerAdapter> {
    @NonNull
    private final bf<MediatedBannerAdapter> a;

    c(@NonNull bf<MediatedBannerAdapter> bfVar) {
        this.a = bfVar;
    }

    @Nullable
    public final bb<MediatedBannerAdapter> a(@NonNull Context context) {
        return this.a.a(context, MediatedBannerAdapter.class);
    }
}
