package com.yandex.mobile.ads.mediation.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class MediatedAdapterInfo {
    @Nullable
    private final String a;
    @Nullable
    private final String b;
    @Nullable
    private final String c;

    static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public String a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;

        Builder() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final MediatedAdapterInfo build() {
            return new MediatedAdapterInfo(this);
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setAdapterVersion(@NonNull String str) {
            this.a = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setNetworkName(@NonNull String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder setNetworkSdkVersion(@NonNull String str) {
            this.c = str;
            return this;
        }
    }

    private MediatedAdapterInfo(@NonNull Builder builder) {
        this.a = builder.a;
        this.b = builder.b;
        this.c = builder.c;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String getAdapterVersion() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String getNetworkName() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String getNetworkSdkVersion() {
        return this.c;
    }
}
