package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.bf;

final class b implements bd<MediatedInterstitialAdapter> {
    @NonNull
    private final bf<MediatedInterstitialAdapter> a;

    b(@NonNull bf<MediatedInterstitialAdapter> bfVar) {
        this.a = bfVar;
    }

    @Nullable
    public final bb<MediatedInterstitialAdapter> a(@NonNull Context context) {
        return this.a.a(context, MediatedInterstitialAdapter.class);
    }
}
