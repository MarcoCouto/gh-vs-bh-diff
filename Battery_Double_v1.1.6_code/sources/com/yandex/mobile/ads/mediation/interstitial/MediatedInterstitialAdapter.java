package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.Map;

abstract class MediatedInterstitialAdapter extends a {

    interface MediatedInterstitialAdapterListener {
        void onInterstitialClicked();

        void onInterstitialDismissed();

        void onInterstitialFailedToLoad(@NonNull AdRequestError adRequestError);

        void onInterstitialLeftApplication();

        void onInterstitialLoaded();

        void onInterstitialShown();
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean isLoaded();

    /* access modifiers changed from: 0000 */
    public abstract void loadInterstitial(@NonNull Context context, @NonNull MediatedInterstitialAdapterListener mediatedInterstitialAdapterListener, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2);

    /* access modifiers changed from: 0000 */
    public abstract void onInvalidate();

    /* access modifiers changed from: 0000 */
    public abstract void showInterstitial();

    MediatedInterstitialAdapter() {
    }
}
