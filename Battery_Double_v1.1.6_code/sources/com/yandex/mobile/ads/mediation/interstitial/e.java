package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.bk;
import com.yandex.mobile.ads.impl.ij;
import com.yandex.mobile.ads.impl.kj;
import com.yandex.mobile.ads.impl.w;
import java.lang.ref.WeakReference;

public final class e implements kj {
    @NonNull
    private final WeakReference<ij> a;
    @NonNull
    private final bk b;

    public final void a(@NonNull Context context) {
    }

    public e(@NonNull ij ijVar) {
        this.a = new WeakReference<>(ijVar);
        this.b = new bk(ijVar.r());
    }

    public final boolean a() {
        ij ijVar = (ij) this.a.get();
        return ijVar != null && ijVar.k();
    }

    public final void a(@NonNull Context context, @NonNull w<String> wVar) {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            this.b.a(context, wVar);
            this.b.b(context, wVar);
            ijVar.b(wVar);
        }
    }

    public final void b() {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            ijVar.g();
        }
    }
}
