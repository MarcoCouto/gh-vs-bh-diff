package com.yandex.mobile.ads.mediation.interstitial;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.bc;
import com.yandex.mobile.ads.impl.ij;
import java.lang.ref.WeakReference;

final class a implements MediatedInterstitialAdapterListener {
    @NonNull
    private final WeakReference<ij> a;
    @NonNull
    private final bc<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> b;

    a(@NonNull ij ijVar, @NonNull bc<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> bcVar) {
        this.a = new WeakReference<>(ijVar);
        this.b = bcVar;
    }

    public final void onInterstitialDismissed() {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            ijVar.B();
        }
    }

    public final void onInterstitialFailedToLoad(@NonNull AdRequestError adRequestError) {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            this.b.a(ijVar.m(), adRequestError, this);
        }
    }

    public final void onInterstitialLoaded() {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            this.b.e(ijVar.m());
            ijVar.onAdLoaded();
        }
    }

    public final void onInterstitialShown() {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            this.b.c(ijVar.m());
            ijVar.A();
        }
    }

    public final void onInterstitialClicked() {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            this.b.b(ijVar.m());
        }
    }

    public final void onInterstitialLeftApplication() {
        ij ijVar = (ij) this.a.get();
        if (ijVar != null) {
            ijVar.onAdLeftApplication();
        }
    }
}
