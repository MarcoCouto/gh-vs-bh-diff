package com.yandex.mobile.ads.mediation.interstitial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.be;
import java.util.Map;

final class c implements be<MediatedInterstitialAdapter, MediatedInterstitialAdapterListener> {
    @Nullable
    private MediatedInterstitialAdapter a;

    c() {
    }

    public final /* synthetic */ void a(@NonNull Context context, @NonNull Object obj, @NonNull Object obj2, @NonNull Map map, @NonNull Map map2) {
        MediatedInterstitialAdapter mediatedInterstitialAdapter = (MediatedInterstitialAdapter) obj;
        MediatedInterstitialAdapterListener mediatedInterstitialAdapterListener = (MediatedInterstitialAdapterListener) obj2;
        this.a = mediatedInterstitialAdapter;
        mediatedInterstitialAdapter.loadInterstitial(context, mediatedInterstitialAdapterListener, map, map2);
    }

    public final /* synthetic */ void a(@NonNull Object obj) {
        ((MediatedInterstitialAdapter) obj).onInvalidate();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final MediatedInterstitialAdapter a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.a != null && this.a.isLoaded();
    }
}
