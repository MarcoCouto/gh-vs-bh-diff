package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.bk;
import com.yandex.mobile.ads.impl.lq;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.nativeads.bp;
import com.yandex.mobile.ads.nativeads.br;
import com.yandex.mobile.ads.nativeads.bs;
import com.yandex.mobile.ads.nativeads.t;
import com.yandex.mobile.ads.nativeads.v;
import java.lang.ref.WeakReference;

public final class u implements t {
    @NonNull
    private final WeakReference<v> a;
    @NonNull
    private final bk b;

    public u(@NonNull v vVar) {
        this.a = new WeakReference<>(vVar);
        this.b = new bk(vVar.r());
    }

    public final void a(@NonNull Context context, @NonNull w<or> wVar) {
        v vVar = (v) this.a.get();
        if (vVar != null) {
            this.b.a(context, wVar);
            this.b.b(context, wVar);
            vVar.a(wVar, new t(new bp(), new bs(), new lq(), new br()));
        }
    }
}
