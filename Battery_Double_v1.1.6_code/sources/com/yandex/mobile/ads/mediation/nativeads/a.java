package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.om;
import java.util.Map;

final class a {
    @NonNull
    private final c a;
    @NonNull
    private final b b;

    a(@NonNull Context context, @NonNull c cVar) {
        this.a = cVar;
        this.b = new b(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final om a(@NonNull Map<String, Bitmap> map, @Nullable MediatedNativeAdImage mediatedNativeAdImage) {
        if (mediatedNativeAdImage == null) {
            return null;
        }
        String url = mediatedNativeAdImage.getUrl();
        int width = mediatedNativeAdImage.getWidth();
        int height = mediatedNativeAdImage.getHeight();
        if (c.a(width, height)) {
            return a(width, height, url, this.b.a(width, height));
        }
        Bitmap bitmap = (Bitmap) map.get(url);
        if (bitmap == null) {
            return null;
        }
        int width2 = bitmap.getWidth();
        int height2 = bitmap.getHeight();
        return a(width2, height2, url, this.b.a(width2, height2));
    }

    @NonNull
    private static om a(int i, int i2, @NonNull String str, @NonNull String str2) {
        om omVar = new om();
        omVar.a(str);
        omVar.a(i);
        omVar.b(i2);
        omVar.b(str2);
        return omVar;
    }
}
