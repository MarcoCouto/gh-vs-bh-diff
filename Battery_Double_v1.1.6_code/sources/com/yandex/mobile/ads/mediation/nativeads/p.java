package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.oq;
import com.yandex.mobile.ads.nativeads.bf;
import com.yandex.mobile.ads.nativeads.bg;
import com.yandex.mobile.ads.nativeads.bs;

public final class p implements bg {
    @NonNull
    private final MediatedNativeAd a;
    @NonNull
    private final bg b = new bs();

    p(@NonNull MediatedNativeAd mediatedNativeAd) {
        this.a = mediatedNativeAd;
    }

    @NonNull
    public final bf a(@NonNull oq oqVar) {
        return new m(this.b.a(oqVar), this.a);
    }
}
