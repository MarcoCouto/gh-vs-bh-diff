package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.nativeads.r;
import com.yandex.mobile.ads.nativeads.s;

public final class e implements s {
    @NonNull
    private final a a;

    interface a {
        void a(@NonNull r rVar);
    }

    e(@NonNull a aVar) {
        this.a = aVar;
    }

    @NonNull
    public final r a(@NonNull Context context, @NonNull fl flVar) {
        r rVar = new r(context, flVar);
        this.a.a(rVar);
        return rVar;
    }
}
