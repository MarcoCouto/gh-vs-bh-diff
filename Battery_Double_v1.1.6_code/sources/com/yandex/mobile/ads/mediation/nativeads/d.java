package com.yandex.mobile.ads.mediation.nativeads;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.om;
import com.yandex.mobile.ads.impl.oo;
import com.yandex.mobile.ads.impl.op;
import java.util.Map;

final class d {
    @NonNull
    private final a a;

    d(@NonNull a aVar) {
        this.a = aVar;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final op a(@NonNull Map<String, Bitmap> map, @Nullable MediatedNativeAdImage mediatedNativeAdImage, @Nullable MediatedNativeAdMedia mediatedNativeAdMedia) {
        om a2 = this.a.a(map, mediatedNativeAdImage);
        oo ooVar = mediatedNativeAdMedia != null ? new oo(null, mediatedNativeAdMedia.getAspectRatio()) : null;
        if (a2 == null && ooVar == null) {
            return null;
        }
        return new op(ooVar, a2);
    }
}
