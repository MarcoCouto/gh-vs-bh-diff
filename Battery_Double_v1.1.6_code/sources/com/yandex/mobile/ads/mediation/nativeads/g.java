package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.k;
import java.util.List;
import java.util.Map;

final class g {
    @NonNull
    private final i a;
    @NonNull
    private final h b;
    @NonNull
    private final r c;

    interface a {
        void a(@NonNull w<or> wVar);
    }

    g(@NonNull Context context, @NonNull i iVar, @NonNull h hVar) {
        this.a = iVar;
        this.b = hVar;
        this.c = new r(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final MediatedNativeAd mediatedNativeAd, @NonNull final NativeAdType nativeAdType, @NonNull List<MediatedNativeAdImage> list, @NonNull final a aVar) {
        this.a.a(this.b.b(list), new k() {
            public final void a(@NonNull Map<String, Bitmap> map) {
                aVar.a(g.this.c.a(mediatedNativeAd, map, nativeAdType));
            }
        });
    }
}
