package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;

final class MediatedNativeAdMedia {
    private final float a;

    static final class Builder {
        /* access modifiers changed from: private */
        public final float a;

        Builder(float f) {
            this.a = f;
        }

        @NonNull
        public final MediatedNativeAdMedia build() {
            return new MediatedNativeAdMedia(this);
        }
    }

    private MediatedNativeAdMedia(@NonNull Builder builder) {
        this.a = builder.a;
    }

    /* access modifiers changed from: 0000 */
    public final float getAspectRatio() {
        return this.a;
    }
}
