package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.oq;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.impl.w.a;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.Collections;
import java.util.List;
import java.util.Map;

final class r {
    @NonNull
    private final q a;

    r(@NonNull Context context) {
        this.a = new q(context);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final w<or> a(@NonNull MediatedNativeAd mediatedNativeAd, @NonNull Map<String, Bitmap> map, @NonNull NativeAdType nativeAdType) {
        List a2 = this.a.a(mediatedNativeAd.getMediatedNativeAdAssets(), map);
        oq oqVar = new oq();
        oqVar.a(nativeAdType.getValue());
        oqVar.a(a2);
        or orVar = new or();
        orVar.b(Collections.singletonList(oqVar));
        return new a().a(orVar).a();
    }
}
