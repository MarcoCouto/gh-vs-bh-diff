package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.bf;

final class j implements bd<MediatedNativeAdapter> {
    @NonNull
    private final bf<MediatedNativeAdapter> a;

    j(@NonNull bf<MediatedNativeAdapter> bfVar) {
        this.a = bfVar;
    }

    @Nullable
    public final bb<MediatedNativeAdapter> a(@NonNull Context context) {
        return this.a.a(context, MediatedNativeAdapter.class);
    }
}
