package com.yandex.mobile.ads.mediation.nativeads;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.r;
import java.util.WeakHashMap;

final class f {
    private final WeakHashMap<r, Object> a = new WeakHashMap<>();

    f() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull r rVar) {
        this.a.put(rVar, null);
    }

    public final void a() {
        for (r a2 : this.a.keySet()) {
            a2.a();
        }
    }

    public final void b() {
        for (r b : this.a.keySet()) {
            b.b();
        }
    }

    public final void c() {
        for (r c : this.a.keySet()) {
            c.c();
        }
    }

    public final void d() {
        for (r d : this.a.keySet()) {
            d.d();
        }
    }

    public final void e() {
        for (r e : this.a.keySet()) {
            e.e();
        }
    }
}
