package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.dy;
import com.yandex.mobile.ads.impl.om;
import com.yandex.mobile.ads.impl.rh;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class h {
    @NonNull
    private final rh a = new rh();
    @NonNull
    private final c b = new c();
    @NonNull
    private final Point c;

    h(@NonNull Context context) {
        this.c = dy.j(context);
    }

    @NonNull
    static Map<String, Bitmap> a(@NonNull List<MediatedNativeAdImage> list) {
        HashMap hashMap = new HashMap();
        for (MediatedNativeAdImage mediatedNativeAdImage : list) {
            Drawable drawable = mediatedNativeAdImage.getDrawable();
            String url = mediatedNativeAdImage.getUrl();
            if (drawable != null && !TextUtils.isEmpty(url)) {
                Bitmap a2 = rh.a(drawable);
                if (a2 != null) {
                    hashMap.put(url, a2);
                }
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<om> b(@NonNull List<MediatedNativeAdImage> list) {
        HashSet hashSet = new HashSet();
        for (MediatedNativeAdImage mediatedNativeAdImage : list) {
            String url = mediatedNativeAdImage.getUrl();
            int width = mediatedNativeAdImage.getWidth();
            int height = mediatedNativeAdImage.getHeight();
            if (!TextUtils.isEmpty(url) && !c.a(width, height)) {
                om omVar = new om();
                omVar.a(url);
                omVar.a(this.c.x);
                omVar.b(this.c.y);
                hashSet.add(omVar);
            }
        }
        return hashSet;
    }
}
