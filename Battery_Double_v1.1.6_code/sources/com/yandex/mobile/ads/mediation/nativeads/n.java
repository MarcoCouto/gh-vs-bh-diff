package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.at;
import com.yandex.mobile.ads.impl.bn;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.bm;
import java.util.List;

public final class n implements bm {
    public final void a(@NonNull Context context, @NonNull b bVar) {
    }

    public final void a(@NonNull Context context, @NonNull b bVar, @Nullable ag agVar) {
    }

    public final void a(@NonNull at atVar) {
    }

    public final void a(@NonNull a aVar) {
    }

    public final void a(@NonNull w wVar, @NonNull List<bn> list) {
    }

    public final void a(@NonNull ag agVar) {
    }
}
