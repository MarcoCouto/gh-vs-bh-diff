package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.mobile.ads.impl.kp;
import com.yandex.mobile.ads.impl.kv;
import com.yandex.mobile.ads.impl.kw;
import com.yandex.mobile.ads.impl.oj;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class q {
    @NonNull
    private final a a;
    @NonNull
    private final d b = new d(this.a);
    @NonNull
    private final kp c = new kp();
    @NonNull
    private final kw d = new kw();

    q(@NonNull Context context) {
        this.a = new a(context, new c());
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<oj> a(@NonNull MediatedNativeAdAssets mediatedNativeAdAssets, @NonNull Map<String, Bitmap> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(a(IronSourceSegment.AGE, (T) mediatedNativeAdAssets.getAge()));
        arrayList.add(a(TtmlNode.TAG_BODY, (T) mediatedNativeAdAssets.getBody()));
        arrayList.add(a("call_to_action", (T) mediatedNativeAdAssets.getCallToAction()));
        arrayList.add(a(RequestParameters.DOMAIN, (T) mediatedNativeAdAssets.getDomain()));
        arrayList.add(a("favicon", (T) this.a.a(map, mediatedNativeAdAssets.getFavicon())));
        arrayList.add(a(SettingsJsonConstants.APP_ICON_KEY, (T) this.a.a(map, mediatedNativeAdAssets.getIcon())));
        arrayList.add(a("media", (T) this.b.a(map, mediatedNativeAdAssets.getImage(), mediatedNativeAdAssets.getMedia())));
        arrayList.add(a("price", (T) mediatedNativeAdAssets.getPrice()));
        arrayList.add(a("rating", (T) String.valueOf(mediatedNativeAdAssets.getRating())));
        arrayList.add(a("review_count", (T) mediatedNativeAdAssets.getReviewCount()));
        arrayList.add(a("sponsored", (T) mediatedNativeAdAssets.getSponsored()));
        arrayList.add(a("title", (T) mediatedNativeAdAssets.getTitle()));
        arrayList.add(a("warning", (T) mediatedNativeAdAssets.getWarning()));
        return a(arrayList);
    }

    @NonNull
    private static List<oj> a(List<oj> list) {
        ArrayList arrayList = new ArrayList();
        for (oj ojVar : list) {
            if (ojVar != null) {
                arrayList.add(ojVar);
            }
        }
        return arrayList;
    }

    @Nullable
    private static <T> oj a(@NonNull String str, @Nullable T t) {
        kv a2 = kw.a(str);
        if (t == null || !a2.a(t)) {
            return null;
        }
        return kp.a(str).a(str, t);
    }
}
