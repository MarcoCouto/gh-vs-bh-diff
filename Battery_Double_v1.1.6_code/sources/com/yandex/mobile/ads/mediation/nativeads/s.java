package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.bc;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.impl.lp;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.r;
import com.yandex.mobile.ads.nativeads.t;
import com.yandex.mobile.ads.nativeads.v;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class s implements MediatedNativeAdapterListener {
    @NonNull
    private final bc<MediatedNativeAdapter, MediatedNativeAdapterListener> a;
    @NonNull
    private final WeakReference<v> b;
    /* access modifiers changed from: private */
    @NonNull
    public final f c;
    @NonNull
    private final i d;
    @NonNull
    private final g e;
    @NonNull
    private final Map<String, Object> f = new HashMap();
    @NonNull
    private final Map<String, Object> g = new HashMap();
    @NonNull
    private final i h;
    @NonNull
    private final h i;
    private boolean j;

    s(@NonNull v vVar, @NonNull bc<MediatedNativeAdapter, MediatedNativeAdapterListener> bcVar) {
        Context m = vVar.m();
        this.a = bcVar;
        this.b = new WeakReference<>(vVar);
        this.c = new f();
        this.d = new i(m);
        this.h = new i();
        this.i = new h(m);
        this.e = new g(m, this.d, this.i);
    }

    public final void onAdClicked() {
        v vVar = (v) this.b.get();
        if (vVar != null) {
            Context m = vVar.m();
            this.a.a(m, this.f);
            a(m, b.CLICK);
        }
        this.c.a();
    }

    public final void onAdClosed() {
        this.c.b();
    }

    public final void onAdImpression() {
        if (!this.j) {
            this.j = true;
            v vVar = (v) this.b.get();
            if (vVar != null) {
                Context m = vVar.m();
                this.a.c(m, this.f);
                a(m, b.IMPRESSION_TRACKING_SUCCESS);
            }
            this.c.c();
        }
    }

    private void a(@NonNull Context context, @NonNull b bVar) {
        HashMap hashMap = new HashMap(this.f);
        hashMap.put("event_type", bVar.a());
        hashMap.put("ad_info", this.g);
        this.a.b(context, (Map<String, Object>) hashMap);
    }

    public final void onAdLeftApplication() {
        this.c.d();
    }

    public final void onAdOpened() {
        this.c.e();
    }

    public final void onAppInstallAdLoaded(@NonNull MediatedNativeAd mediatedNativeAd) {
        a(mediatedNativeAd, NativeAdType.APP_INSTALL);
    }

    public final void onContentAdLoaded(@NonNull MediatedNativeAd mediatedNativeAd) {
        a(mediatedNativeAd, NativeAdType.CONTENT);
    }

    private void a(@NonNull final MediatedNativeAd mediatedNativeAd, @NonNull NativeAdType nativeAdType) {
        final v vVar = (v) this.b.get();
        if (vVar != null) {
            Context m = vVar.m();
            this.f.put("native_ad_type", nativeAdType.getValue());
            this.a.d(m, this.f);
            this.g.putAll(a(mediatedNativeAd));
            List a2 = i.a(mediatedNativeAd);
            a(a2);
            this.e.a(mediatedNativeAd, nativeAdType, a2, new a() {
                public final void a(@NonNull w<or> wVar) {
                    vVar.a(wVar, new t(new e(s.a(s.this)), new p(mediatedNativeAd), new lp(), new o()));
                }
            });
        }
    }

    private void a(@NonNull List<MediatedNativeAdImage> list) {
        this.d.a(h.a(list));
    }

    @NonNull
    private static Map<String, Object> a(@NonNull MediatedNativeAd mediatedNativeAd) {
        HashMap hashMap = new HashMap();
        hashMap.put("title", mediatedNativeAd.getMediatedNativeAdAssets().getTitle());
        return hashMap;
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        v vVar = (v) this.b.get();
        if (vVar != null) {
            this.a.a(vVar.m(), adRequestError, this);
        }
    }

    static /* synthetic */ a a(s sVar) {
        return new a() {
            public final void a(@NonNull r rVar) {
                s.this.c.a(rVar);
            }
        };
    }
}
