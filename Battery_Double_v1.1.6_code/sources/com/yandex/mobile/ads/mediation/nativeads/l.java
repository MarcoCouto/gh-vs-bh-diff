package com.yandex.mobile.ads.mediation.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.bc;
import com.yandex.mobile.ads.impl.bf;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.bh;
import com.yandex.mobile.ads.impl.bi;
import com.yandex.mobile.ads.impl.bl;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.or;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.nativeads.v;

public final class l implements t {
    @NonNull
    private final MediatedNativeAdapterListener a;
    @NonNull
    private final bc<MediatedNativeAdapter, MediatedNativeAdapterListener> b;

    public l(@NonNull v vVar, @NonNull w<or> wVar, @NonNull bl blVar) {
        fl r = vVar.r();
        bg bgVar = new bg(r);
        bi biVar = new bi(r, wVar);
        bc bcVar = new bc(r, new k(), biVar, new j(new bf(blVar, bgVar, biVar)), new bh(vVar, blVar));
        this.b = bcVar;
        this.a = new s(vVar, this.b);
    }

    public final void a(@NonNull Context context, @NonNull w<or> wVar) {
        this.b.a(context, this.a);
    }
}
