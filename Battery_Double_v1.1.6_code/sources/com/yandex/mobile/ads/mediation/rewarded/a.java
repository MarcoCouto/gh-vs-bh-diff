package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.bc;
import com.yandex.mobile.ads.impl.bf;
import com.yandex.mobile.ads.impl.bg;
import com.yandex.mobile.ads.impl.bh;
import com.yandex.mobile.ads.impl.bi;
import com.yandex.mobile.ads.impl.bl;
import com.yandex.mobile.ads.impl.fl;
import com.yandex.mobile.ads.impl.kj;
import com.yandex.mobile.ads.impl.w;
import com.yandex.mobile.ads.rewarded.b;

public final class a implements kj {
    @NonNull
    private final bc<MediatedRewardedAdapter, MediatedRewardedAdapterListener> a;
    @NonNull
    private final c b = new c();
    @NonNull
    private final MediatedRewardedAdapterListener c;

    public a(@NonNull b bVar, @NonNull w<String> wVar, @NonNull bl blVar) {
        fl r = bVar.r();
        bg bgVar = new bg(r);
        bi biVar = new bi(r, wVar);
        b bVar2 = new b(new bf(blVar, bgVar, biVar));
        bh bhVar = new bh(bVar, blVar);
        bc bcVar = new bc(r, this.b, biVar, bVar2, bhVar);
        this.a = bcVar;
        this.c = new d(bVar, this.a);
    }

    public final void a(@NonNull Context context) {
        this.a.a(context);
    }

    public final boolean a() {
        return this.b.b();
    }

    public final void a(@NonNull Context context, @NonNull w<String> wVar) {
        this.a.a(context, this.c);
    }

    public final void b() {
        MediatedRewardedAdapter a2 = this.b.a();
        if (a2 != null) {
            a2.showRewardedAd();
        }
    }
}
