package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.be;
import java.util.Map;

final class c implements be<MediatedRewardedAdapter, MediatedRewardedAdapterListener> {
    @Nullable
    private MediatedRewardedAdapter a;

    c() {
    }

    public final /* synthetic */ void a(@NonNull Context context, @NonNull Object obj, @NonNull Object obj2, @NonNull Map map, @NonNull Map map2) {
        MediatedRewardedAdapter mediatedRewardedAdapter = (MediatedRewardedAdapter) obj;
        MediatedRewardedAdapterListener mediatedRewardedAdapterListener = (MediatedRewardedAdapterListener) obj2;
        this.a = mediatedRewardedAdapter;
        mediatedRewardedAdapter.loadRewardedAd(context, mediatedRewardedAdapterListener, map, map2);
    }

    public final /* synthetic */ void a(@NonNull Object obj) {
        ((MediatedRewardedAdapter) obj).onInvalidate();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final MediatedRewardedAdapter a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.a != null && this.a.isLoaded();
    }
}
