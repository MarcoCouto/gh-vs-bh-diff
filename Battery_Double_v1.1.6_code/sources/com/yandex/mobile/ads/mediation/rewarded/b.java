package com.yandex.mobile.ads.mediation.rewarded;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.bb;
import com.yandex.mobile.ads.impl.bd;
import com.yandex.mobile.ads.impl.bf;

final class b implements bd<MediatedRewardedAdapter> {
    @NonNull
    private final bf<MediatedRewardedAdapter> a;

    b(@NonNull bf<MediatedRewardedAdapter> bfVar) {
        this.a = bfVar;
    }

    @Nullable
    public final bb<MediatedRewardedAdapter> a(@NonNull Context context) {
        return this.a.a(context, MediatedRewardedAdapter.class);
    }
}
