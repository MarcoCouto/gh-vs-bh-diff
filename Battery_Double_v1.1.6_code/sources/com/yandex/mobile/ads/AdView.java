package com.yandex.mobile.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.yandex.mobile.ads.impl.a;
import com.yandex.mobile.ads.impl.c;
import com.yandex.mobile.ads.impl.e;

public final class AdView extends e {
    public AdView(@NonNull Context context) {
        super(context);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void loadAd(AdRequest adRequest) {
        super.loadAd(adRequest);
    }

    public final void setAdEventListener(AdEventListener adEventListener) {
        super.setAdEventListener(adEventListener);
    }

    public final AdEventListener getAdEventListener() {
        return super.getAdEventListener();
    }

    @NonNull
    public final VideoController getVideoController() {
        return super.getVideoController();
    }

    public final void setAutoRefreshEnabled(boolean z) {
        super.setAutoRefreshEnabled(z);
    }

    public final void setAdSize(@Nullable AdSize adSize) {
        super.setAdSize(adSize);
    }

    @Nullable
    public final AdSize getAdSize() {
        return super.getAdSize();
    }

    public final void setBlockId(String str) {
        super.setBlockId(str);
    }

    public final String getBlockId() {
        return super.getBlockId();
    }

    public final void destroy() {
        super.destroy();
    }

    public final void pause() {
        super.pause();
    }

    public final void resume() {
        super.resume();
    }

    public final void shouldOpenLinksInApp(boolean z) {
        super.shouldOpenLinksInApp(z);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final a a(@NonNull Context context, @NonNull c cVar) {
        return new a(context, this, cVar);
    }
}
