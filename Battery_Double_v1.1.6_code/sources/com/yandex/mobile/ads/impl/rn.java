package com.yandex.mobile.ads.impl;

public final class rn implements rw {
    private int a;
    private int b;
    private final int c;
    private final float d;

    public rn() {
        this(2500, 1, 1.0f);
    }

    public rn(int i, int i2, float f) {
        this.a = i;
        this.c = i2;
        this.d = f;
    }

    public final int a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }

    public final void a(sf sfVar) throws sf {
        boolean z = true;
        this.b++;
        this.a = (int) (((float) this.a) + (((float) this.a) * this.d));
        if (this.b > this.c) {
            z = false;
        }
        if (!z) {
            throw sfVar;
        }
    }
}
