package com.yandex.mobile.ads.impl;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;
import java.util.Map.Entry;

final class qn {
    qn() {
    }

    @NonNull
    static Intent a(@NonNull oy oyVar) {
        String b = oyVar.b();
        String a = oyVar.a();
        Map c = oyVar.c();
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(b));
        intent.addFlags(1342177280);
        intent.setPackage(a);
        a(intent, c);
        return intent;
    }

    private static void a(@NonNull Intent intent, @Nullable Map<String, Object> map) {
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value instanceof Boolean) {
                    intent.putExtra(str, (Boolean) value);
                } else if (value instanceof Integer) {
                    intent.putExtra(str, (Integer) value);
                } else if (value instanceof String) {
                    intent.putExtra(str, (String) value);
                }
            }
        }
    }
}
