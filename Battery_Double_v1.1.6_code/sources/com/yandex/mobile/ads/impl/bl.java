package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class bl implements Parcelable {
    public static final Creator<bl> CREATOR = new Creator<bl>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bl[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new bl(parcel);
        }
    };
    @NonNull
    private final List<bm> a;
    @NonNull
    private final Map<String, String> b;

    public int describeContents() {
        return 0;
    }

    public bl(@NonNull List<bm> list, @NonNull Map<String, String> map) {
        this.a = list;
        this.b = map;
    }

    @NonNull
    public final List<bm> a() {
        return this.a;
    }

    @NonNull
    public final Map<String, String> b() {
        return this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.a);
        parcel.writeInt(this.b.size());
        for (Entry entry : this.b.entrySet()) {
            parcel.writeString((String) entry.getKey());
            parcel.writeString((String) entry.getValue());
        }
    }

    protected bl(Parcel parcel) {
        this.a = new ArrayList();
        parcel.readList(this.a, bm.class.getClassLoader());
        int readInt = parcel.readInt();
        this.b = new HashMap(readInt);
        for (int i = 0; i < readInt; i++) {
            this.b.put(parcel.readString(), parcel.readString());
        }
    }
}
