package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.Callable;

final class gd {
    gd() {
    }

    @Nullable
    static <T, S> S a(@NonNull Callable<S> callable, @Nullable T t, @NonNull String str) {
        if (t != null) {
            try {
                return callable.call();
            } catch (Throwable unused) {
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" is null.");
            return null;
        }
    }
}
