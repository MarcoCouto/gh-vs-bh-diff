package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class lg {
    @NonNull
    private final Map<String, Object> a;

    public lg(@NonNull List<oj> list) {
        this.a = a(list);
    }

    @NonNull
    private static Map<String, Object> a(@NonNull List<oj> list) {
        HashMap hashMap = new HashMap();
        for (oj ojVar : list) {
            hashMap.put(ojVar.a(), ojVar.c());
        }
        return hashMap;
    }

    @Nullable
    public final op a() {
        Object obj = this.a.get("media");
        if (obj instanceof op) {
            return (op) obj;
        }
        return null;
    }
}
