package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.j;

public final class qu {
    @NonNull
    private final co a;
    @NonNull
    private final j b;
    @NonNull
    private final qt c;
    @NonNull
    private final ra d;

    public qu(@NonNull Context context, @NonNull co coVar, @NonNull ag agVar) {
        this.a = coVar;
        this.b = agVar.f();
        this.c = new qt(context);
        this.d = new ra(context);
    }

    public final void a(@NonNull Context context, @NonNull ow owVar) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(owVar.d()));
        if (this.d.a(intent)) {
            Bitmap a2 = this.b.a(this.c.a(owVar.b()));
            if (a2 != null) {
                this.a.a(b.SHORTCUT);
                String c2 = owVar.c();
                Intent intent2 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                intent2.putExtra("android.intent.extra.shortcut.NAME", c2);
                intent2.putExtra("android.intent.extra.shortcut.ICON", a2);
                intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
                intent2.putExtra("duplicate", false);
                try {
                    context.sendBroadcast(intent2);
                } catch (Exception unused) {
                }
            }
        }
    }
}
