package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import org.xmlpull.v1.XmlPullParser;

final class um {
    um() {
    }

    @NonNull
    static tm a(@NonNull XmlPullParser xmlPullParser) {
        return new tm(a(xmlPullParser, "allowMultipleAds"), a(xmlPullParser, "followAdditionalWrappers"));
    }

    private static boolean a(@NonNull XmlPullParser xmlPullParser, @NonNull String str) {
        return Boolean.parseBoolean(xmlPullParser.getAttributeValue(null, str));
    }
}
