package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.File;

public final class bv {
    public static rt a(@NonNull Context context, int i) {
        rx.a("Yandex Mobile Ads");
        rx.b = false;
        sg sgVar = new sg(new sr(context, new sy()).a());
        new br();
        File a = br.a(context, "mobileads-volley-cache");
        return new rt(new si(a, (int) dw.a(a)), sgVar, i);
    }
}
