package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public final class ul {
    @NonNull
    private final bt a = new bt();
    @NonNull
    private final uk b = new uk();

    @Nullable
    public final tl a(@NonNull rr rrVar) {
        String a2 = bt.a(rrVar);
        if (!TextUtils.isEmpty(a2)) {
            try {
                tk a3 = this.b.a(a2);
                if (a3 != null) {
                    boolean z = false;
                    if (rrVar.c != null) {
                        z = bs.b(rrVar.c, rk.YMAD_RAW_VAST_ENABLED);
                    }
                    if (!z) {
                        a2 = null;
                    }
                    return new tl(a3, a2);
                }
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
