package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class dl implements dn {
    @NonNull
    private final Context a;
    @NonNull
    private final ah b = ah.a();

    private static class a implements com.yandex.mobile.ads.impl.sv.a<rr> {
        private final String a;

        public final /* synthetic */ void a(Object obj) {
            Object[] objArr = {this.a, Integer.valueOf(((rr) obj).a)};
        }

        a(String str) {
            this.a = str;
        }

        public final void a(@NonNull sf sfVar) {
            Object[] objArr = {this.a, sfVar.toString()};
        }
    }

    public dl(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    public final void a(@NonNull String str) {
        this.b.a(this.a, (rs) new bx(str, new a(str)));
    }
}
