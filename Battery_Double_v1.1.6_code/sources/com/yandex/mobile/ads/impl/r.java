package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class r extends sf {
    private static final long serialVersionUID = 9076708591501334094L;
    private final int b;

    private static boolean a(int i) {
        return i >= 500 && i <= 599;
    }

    public r(rr rrVar, int i) {
        super(rrVar);
        this.b = i;
    }

    r() {
        this.b = 11;
    }

    public final int a() {
        return this.b;
    }

    public static r a(rr rrVar) {
        r rVar;
        int i = rrVar != null ? rrVar.a : -1;
        if (204 == i) {
            rVar = new r(rrVar, 6);
        } else if (403 == i) {
            rVar = new r(rrVar, 10);
        } else if (404 == i) {
            rVar = new r(rrVar, 4);
        } else if (a(i)) {
            rVar = new r(rrVar, 9);
        } else if (-1 == i) {
            rVar = new r(rrVar, 7);
        } else {
            rVar = new r(rrVar, 8);
        }
        new Object[1][0] = Integer.valueOf(i);
        return rVar;
    }

    @NonNull
    public static r b(@Nullable rr rrVar) {
        int i = rrVar != null ? rrVar.a : -1;
        int i2 = a(i) ? 9 : -1 == i ? 7 : 8;
        new Object[1][0] = Integer.valueOf(i);
        return new r(rrVar, i2);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.b == ((r) obj).b;
    }

    public final int hashCode() {
        return this.b;
    }
}
