package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.a;

public final class dh {
    @NonNull
    private final Context a;
    @NonNull
    private final fl b;
    @Nullable
    private dg c;

    public dh(@NonNull Context context, @NonNull fl flVar) {
        this.a = context.getApplicationContext();
        this.b = flVar;
    }

    public final void a(@Nullable di diVar) {
        if (diVar != null) {
            this.c = new dg(this.a, this.b, diVar);
        }
    }

    public final void a() {
        if (this.c != null) {
            this.c.a();
        }
    }

    public final void b() {
        if (this.c != null) {
            this.c.b();
        }
    }

    public final void c() {
        if (this.c != null) {
            this.c.c();
        }
    }

    public final void d() {
        if (this.c != null) {
            this.c.e();
        }
    }

    public final void e() {
        if (this.c != null) {
            this.c.f();
        }
    }

    public final void a(@NonNull a aVar) {
        if (this.c != null) {
            this.c.a(aVar);
        }
    }
}
