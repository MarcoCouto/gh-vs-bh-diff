package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@SuppressLint({"StaticFieldLeak"})
public final class gz {
    private static final Object a = new Object();
    private static volatile gz b;
    @NonNull
    private final Context c;
    @NonNull
    private final hc d = new hc();
    @NonNull
    private final hb e = new hb();

    private gz(@NonNull Context context) {
        this.c = context.getApplicationContext();
    }

    @NonNull
    public static gz a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new gz(context);
                }
            }
        }
        return b;
    }

    @Nullable
    public final Location a() {
        Location a2;
        synchronized (a) {
            Context context = this.c;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new hl(context));
            fw a3 = fv.a().a(context);
            if (a3 != null && !a3.j()) {
                arrayList.add(hg.a(context));
                arrayList.add(hh.a(context));
            }
            a2 = hc.a(a((List<ha>) arrayList));
        }
        return a2;
    }

    @NonNull
    private static List<Location> a(@NonNull List<ha> list) {
        ArrayList arrayList = new ArrayList();
        for (ha a2 : list) {
            Location a3 = a2.a();
            if (a3 != null) {
                arrayList.add(a3);
            }
        }
        return arrayList;
    }
}
