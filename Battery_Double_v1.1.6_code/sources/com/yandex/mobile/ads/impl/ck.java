package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.a;
import java.util.List;

public final class ck {
    @NonNull
    private final de a;
    @NonNull
    private final cs b;
    @Nullable
    private final List<String> c;
    @NonNull
    private final w d;
    @NonNull
    private final ap e = ap.a();

    public ck(@NonNull Context context, @NonNull w wVar, @NonNull fl flVar, @Nullable List<String> list) {
        this.c = list;
        this.d = wVar;
        this.a = new de(context, flVar);
        this.b = new cs(context, wVar);
    }

    public final void a(@NonNull a aVar) {
        this.b.a(aVar);
    }

    public final void a() {
        if (this.c != null) {
            for (String a2 : this.c) {
                this.a.a(a2);
            }
        }
        if (this.d.m() != null) {
            this.e.a(this.d.m());
        }
        this.b.a();
    }
}
