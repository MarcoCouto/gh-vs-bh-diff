package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.z;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class py {
    @NonNull
    private final px a;

    py(@NonNull pk pkVar) {
        this.a = new px(pkVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<oy> a(@Nullable JSONArray jSONArray) throws JSONException, z {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    arrayList.add(this.a.a(optJSONObject));
                }
            }
        }
        return arrayList;
    }
}
