package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

public final class mc extends md<TextView, String> {
    public final /* synthetic */ void a(@NonNull View view) {
        TextView textView = (TextView) view;
        textView.setText("");
        super.a(textView);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        return ((TextView) view).getText().toString().equalsIgnoreCase((String) obj);
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        ((TextView) view).setText((String) obj);
    }

    public mc(@NonNull TextView textView) {
        super(textView);
    }
}
