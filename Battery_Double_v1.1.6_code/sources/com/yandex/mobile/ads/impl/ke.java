package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

public final class ke {
    @NonNull
    private final jx a;

    public ke(@NonNull jx jxVar) {
        this.a = jxVar;
    }

    @NonNull
    public static kd a(@NonNull w wVar, @NonNull View view, boolean z, boolean z2) {
        Context context = view.getContext();
        jw a2 = jx.a(wVar, view, z);
        if (z2) {
            return new kc(a2);
        }
        return new kb(context, a2);
    }
}
