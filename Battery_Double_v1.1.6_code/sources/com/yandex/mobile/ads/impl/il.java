package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdRequestError;
import java.util.Map;

public final class il implements el {
    @NonNull
    private final el a;

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
    }

    public final void a(@NonNull String str) {
    }

    public final void b(boolean z) {
    }

    public il(@NonNull el elVar) {
        this.a = elVar;
    }

    public final void onAdLoaded() {
        this.a.onAdLoaded();
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        this.a.onAdFailedToLoad(adRequestError);
    }
}
