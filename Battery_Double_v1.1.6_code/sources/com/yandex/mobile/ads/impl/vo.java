package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class vo implements RequestListener<List<VideoAd>> {
    @NonNull
    private final vm a;
    @NonNull
    private final RequestListener<List<VideoAd>> b;

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        this.b.onSuccess(this.a.a((List) obj));
    }

    vo(@NonNull VideoAd videoAd, @NonNull RequestListener<List<VideoAd>> requestListener) {
        this.b = requestListener;
        this.a = new vm(videoAd);
    }

    public final void onFailure(@NonNull VideoAdError videoAdError) {
        this.b.onFailure(videoAdError);
    }
}
