package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.b;
import java.util.HashMap;

public final class ve implements hs<VastRequestConfiguration, tl> {
    @NonNull
    private final b a;

    public ve(@NonNull b bVar) {
        this.a = bVar;
    }

    public final /* synthetic */ hr a(@Nullable ru ruVar, int i, @NonNull Object obj) {
        HashMap hashMap = new HashMap();
        String b = this.a.b();
        String c = this.a.c();
        String str = "page_id";
        if (TextUtils.isEmpty(c)) {
            c = "null";
        }
        hashMap.put(str, c);
        String str2 = "imp_id";
        if (TextUtils.isEmpty(b)) {
            b = "null";
        }
        hashMap.put(str2, b);
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new hr(hr.b.VAST_RESPONSE, hashMap);
    }

    public final /* synthetic */ hr a(Object obj) {
        HashMap hashMap = new HashMap();
        String b = this.a.b();
        String c = this.a.c();
        String str = "page_id";
        if (TextUtils.isEmpty(c)) {
            c = "null";
        }
        hashMap.put(str, c);
        String str2 = "imp_id";
        if (TextUtils.isEmpty(b)) {
            b = "null";
        }
        hashMap.put(str2, b);
        return new hr(hr.b.VAST_REQUEST, hashMap);
    }
}
