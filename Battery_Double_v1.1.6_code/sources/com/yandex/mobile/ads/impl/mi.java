package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.e;

public final class mi implements mj {
    @NonNull
    private final ag a;
    @NonNull
    private final e b;
    @NonNull
    private final on c;
    @NonNull
    private final ed d = new ed();

    public mi(@NonNull ag agVar, @NonNull e eVar, @NonNull on onVar) {
        this.a = agVar;
        this.b = eVar;
        this.c = onVar;
    }

    public final void a(@NonNull oj ojVar, @NonNull View view) {
        if (view.getTag() == null) {
            view.setTag(ed.a(ojVar.a()));
        }
    }

    public final void a(@NonNull oj ojVar, @NonNull mq mqVar) {
        on d2 = ojVar.d();
        if (d2 == null) {
            d2 = this.c;
        }
        this.b.a(ojVar, d2, this.a, mqVar);
    }
}
