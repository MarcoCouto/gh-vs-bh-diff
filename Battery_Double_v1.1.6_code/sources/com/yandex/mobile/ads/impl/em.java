package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.es.a;

@SuppressLint({"ViewConstructor"})
public final class em extends ep {
    @NonNull
    private final en a;
    @NonNull
    private es b = new eu();

    public em(@NonNull Context context, @NonNull as asVar) {
        super(context);
        this.a = new en(this, asVar);
    }

    public final void c(@NonNull String str) {
        this.a.b(str);
    }

    public final void setAspectRatio(float f) {
        this.b = new et(f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.a.a();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        a a2 = this.b.a(i, i2);
        super.onMeasure(a2.a, a2.b);
    }

    public final void setClickListener(@NonNull mu muVar) {
        this.a.a(muVar);
    }
}
