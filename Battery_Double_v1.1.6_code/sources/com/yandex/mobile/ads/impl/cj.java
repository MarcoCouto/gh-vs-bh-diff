package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

final class cj {
    /* access modifiers changed from: private */
    @NonNull
    public final ci a;
    /* access modifiers changed from: private */
    @NonNull
    public final b b;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler c = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean d;

    private final class a implements Runnable {
        private a() {
        }

        /* synthetic */ a(cj cjVar, byte b) {
            this();
        }

        public final void run() {
            if (!cj.this.d) {
                if (cj.this.a.a()) {
                    cj.this.d = true;
                    cj.this.b.a();
                    return;
                }
                cj.this.c.postDelayed(new a(), 300);
            }
        }
    }

    public interface b {
        void a();
    }

    cj(@NonNull ci ciVar, @NonNull b bVar) {
        this.a = ciVar;
        this.b = bVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.c.post(new a(this, 0));
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.c.removeCallbacksAndMessages(null);
    }
}
