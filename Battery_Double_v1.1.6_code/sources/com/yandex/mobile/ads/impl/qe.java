package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qe implements pz<String> {
    @NonNull
    private final qf a = new qf();

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, z {
        String a2 = pi.a(jSONObject, "name");
        String a3 = pi.a(jSONObject, "value");
        return "review_count".equals(a2) ? this.a.a(a3) : a3;
    }
}
