package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.sv.a;
import java.io.UnsupportedEncodingException;

abstract class ug<R, T> extends sw<R, T> {
    /* access modifiers changed from: protected */
    public abstract T a(String str) throws Exception;

    private ug(String str, a<T> aVar, Context context, R r, hs<R, T> hsVar) {
        super(context, 0, str, aVar, r, hsVar);
    }

    ug(String str, a<T> aVar, Context context, R r, hs<R, T> hsVar, byte b) {
        this(str, aVar, context, r, hsVar);
    }

    /* access modifiers changed from: protected */
    public final ru<T> a(@NonNull rr rrVar, int i) {
        String str;
        try {
            str = new String(rrVar.b, sj.a(rrVar.c));
        } catch (UnsupportedEncodingException unused) {
            str = new String(rrVar.b);
        }
        try {
            return c(a(str));
        } catch (Exception e) {
            return a(e);
        }
    }

    /* access modifiers changed from: protected */
    public ru<T> a(Exception exc) {
        return ru.a(new to((Throwable) exc));
    }

    /* access modifiers changed from: protected */
    public ru<T> c(T t) {
        return ru.a(t, null);
    }
}
