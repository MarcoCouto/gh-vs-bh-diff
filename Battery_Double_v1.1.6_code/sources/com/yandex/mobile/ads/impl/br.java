package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import java.io.File;

final class br {
    br() {
    }

    @NonNull
    static File a(@NonNull Context context, @NonNull String str) {
        File file;
        String externalStorageState = Environment.getExternalStorageState();
        if ("mounted".equals(externalStorageState) || (!Environment.isExternalStorageRemovable() && !"mounted_ro".equals(externalStorageState))) {
            file = context.getExternalCacheDir();
        } else {
            file = null;
        }
        if (file == null) {
            file = context.getCacheDir();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(file.getPath());
        sb.append(File.separator);
        sb.append(str);
        return new File(sb.toString());
    }
}
