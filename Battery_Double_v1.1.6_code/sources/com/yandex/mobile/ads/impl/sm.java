package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.ImageView.ScaleType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class sm {
    private final rt a;
    private int b = 100;
    private final b c;
    private final HashMap<String, a> d = new HashMap<>();
    /* access modifiers changed from: private */
    public final HashMap<String, a> e = new HashMap<>();
    private final Handler f = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public Runnable g;

    private class a {
        private final rs<?> b;
        /* access modifiers changed from: private */
        public Bitmap c;
        private sf d;
        /* access modifiers changed from: private */
        public final LinkedList<c> e = new LinkedList<>();

        public a(rs<?> rsVar, c cVar) {
            this.b = rsVar;
            this.e.add(cVar);
        }

        public final void a(sf sfVar) {
            this.d = sfVar;
        }

        public final sf a() {
            return this.d;
        }

        public final void a(c cVar) {
            this.e.add(cVar);
        }
    }

    public interface b {
        Bitmap a(String str);

        void a(String str, Bitmap bitmap);
    }

    public class c {
        /* access modifiers changed from: private */
        public Bitmap b;
        /* access modifiers changed from: private */
        public final d c;
        private final String d;
        private final String e;

        public c(Bitmap bitmap, String str, String str2, d dVar) {
            this.b = bitmap;
            this.e = str;
            this.d = str2;
            this.c = dVar;
        }

        public final Bitmap a() {
            return this.b;
        }
    }

    public interface d extends com.yandex.mobile.ads.impl.ru.a {
        void a(c cVar);
    }

    public sm(rt rtVar, b bVar) {
        this.a = rtVar;
        this.c = bVar;
    }

    public c a(String str, d dVar, int i, int i2) {
        d dVar2 = dVar;
        ScaleType scaleType = ScaleType.CENTER_INSIDE;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            final String a2 = a(str, i, i2, scaleType);
            Bitmap a3 = this.c.a(a2);
            if (a3 != null) {
                c cVar = new c(a3, str, null, null);
                dVar2.a(cVar);
                return cVar;
            }
            c cVar2 = new c(null, str, a2, dVar);
            dVar2.a(cVar2);
            a aVar = (a) this.d.get(a2);
            if (aVar != null) {
                aVar.a(cVar2);
                return cVar2;
            }
            c cVar3 = cVar2;
            sn snVar = new sn(str, new com.yandex.mobile.ads.impl.ru.b<Bitmap>() {
                public final /* bridge */ /* synthetic */ void a(Object obj) {
                    sm.this.a(a2, (Bitmap) obj);
                }
            }, i, i2, scaleType, Config.RGB_565, new com.yandex.mobile.ads.impl.ru.a() {
                public final void a(@NonNull sf sfVar) {
                    sm.this.a(a2, sfVar);
                }
            });
            this.a.a((rs<T>) snVar);
            this.d.put(a2, new a(snVar, cVar3));
            return cVar3;
        }
        throw new IllegalStateException("ImageLoader must be invoked from the main thread.");
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Bitmap bitmap) {
        this.c.a(str, bitmap);
        a aVar = (a) this.d.remove(str);
        if (aVar != null) {
            aVar.c = bitmap;
            a(str, aVar);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, sf sfVar) {
        a aVar = (a) this.d.remove(str);
        if (aVar != null) {
            aVar.a(sfVar);
            a(str, aVar);
        }
    }

    private void a(String str, a aVar) {
        this.e.put(str, aVar);
        if (this.g == null) {
            this.g = new Runnable() {
                public final void run() {
                    for (a aVar : sm.this.e.values()) {
                        Iterator it = aVar.e.iterator();
                        while (it.hasNext()) {
                            c cVar = (c) it.next();
                            if (cVar.c != null) {
                                if (aVar.a() == null) {
                                    cVar.b = aVar.c;
                                    cVar.c.a(cVar);
                                } else {
                                    cVar.c.a(aVar.a());
                                }
                            }
                        }
                    }
                    sm.this.e.clear();
                    sm.this.g = null;
                }
            };
            this.f.postDelayed(this.g, (long) this.b);
        }
    }

    public String a(@NonNull String str, int i, int i2, @NonNull ScaleType scaleType) {
        StringBuilder sb = new StringBuilder("#W");
        sb.append(i);
        sb.append("#H");
        sb.append(i2);
        sb.append("#S");
        sb.append(scaleType.ordinal());
        sb.append(str);
        return sb.toString();
    }
}
