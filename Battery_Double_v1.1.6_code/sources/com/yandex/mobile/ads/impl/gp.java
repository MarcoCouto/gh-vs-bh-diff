package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.TimeUnit;

final class gp implements gs {
    private static final long a = TimeUnit.SECONDS.toMillis(1);
    private static final Object b = new Object();
    private static volatile gp c;
    @NonNull
    private final Handler d = new Handler(Looper.getMainLooper());
    @NonNull
    private final gu e;
    @NonNull
    private final gt f;
    @Nullable
    private go g;
    private boolean h;

    @NonNull
    static gp a(@NonNull Context context) {
        if (c == null) {
            synchronized (b) {
                if (c == null) {
                    c = new gp(context);
                }
            }
        }
        return c;
    }

    private gp(@NonNull Context context) {
        this.e = new gu(context);
        this.f = new gt();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull gv gvVar) {
        synchronized (b) {
            go goVar = this.g;
            if (goVar != null) {
                gvVar.a(goVar);
            } else {
                this.f.a(gvVar);
                if (!this.h) {
                    this.h = true;
                    this.d.postDelayed(new Runnable() {
                        public final void run() {
                            gp.this.a();
                        }
                    }, a);
                    this.e.a((gs) this);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull gv gvVar) {
        synchronized (b) {
            this.f.b(gvVar);
        }
    }

    public final void a(@NonNull go goVar) {
        synchronized (b) {
            this.g = goVar;
            b();
            this.f.a(goVar);
        }
    }

    public final void a() {
        synchronized (b) {
            b();
            this.f.a();
        }
    }

    private void b() {
        this.d.removeCallbacksAndMessages(null);
        this.h = false;
    }
}
