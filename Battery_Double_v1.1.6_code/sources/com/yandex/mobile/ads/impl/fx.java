package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Locale;

public final class fx {
    @NonNull
    private final gb a = new gb();
    @NonNull
    private final fz b = new fz();

    @Nullable
    public final String a(@NonNull Context context) {
        return this.a.a(context).name().toLowerCase(Locale.US);
    }
}
