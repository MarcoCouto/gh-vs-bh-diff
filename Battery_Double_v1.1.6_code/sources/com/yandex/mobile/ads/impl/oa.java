package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class oa {
    @NonNull
    private final de a;
    @NonNull
    private final w b;
    @NonNull
    private final dk c;
    @NonNull
    private final co d;

    public oa(@NonNull de deVar, @NonNull w wVar, @NonNull dk dkVar, @NonNull co coVar) {
        this.a = deVar;
        this.b = wVar;
        this.c = dkVar;
        this.d = coVar;
    }

    public final void a(@Nullable String str) {
        this.a.a(str, this.b, this.c, this.d);
    }
}
