package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.video.VideoAdError;
import java.util.HashMap;
import java.util.Map;

public final class va {
    @NonNull
    private final vb a;
    @NonNull
    private final hp b;

    public va(@NonNull Context context, @NonNull vb vbVar) {
        this.a = vbVar;
        this.b = hp.a(context);
    }

    @NonNull
    private Map<String, Object> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("block_id", c());
        return hashMap;
    }

    @NonNull
    private String c() {
        return String.format("R-V-%s-%s", new Object[]{this.a.a(), this.a.b()});
    }

    public final void a() {
        Map b2 = b();
        b2.put("status", "success");
        this.b.a(new hr(b.AD_LOADING_RESULT, b2));
    }

    public final void a(@NonNull VideoAdError videoAdError) {
        Map b2 = b();
        b2.put("failure_reason", videoAdError.getCode() == 3 ? "no_ads" : "null");
        b2.put("status", "error");
        this.b.a(new hr(b.AD_LOADING_RESULT, b2));
    }
}
