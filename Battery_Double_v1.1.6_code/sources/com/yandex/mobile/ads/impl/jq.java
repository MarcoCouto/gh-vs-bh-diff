package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Window;
import android.widget.RelativeLayout;

public final class jq implements l {
    @Nullable
    public final k a(@NonNull Context context, @NonNull RelativeLayout relativeLayout, @Nullable ResultReceiver resultReceiver, @NonNull n nVar, @NonNull Intent intent, @NonNull Window window) {
        jo a = jo.a();
        w b = a.b();
        fl c = a.c();
        String str = b != null ? (String) b.p() : null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        boolean a2 = a(intent);
        jt jtVar = new jt(b, str, c);
        Context context2 = context;
        ResultReceiver resultReceiver2 = resultReceiver;
        jp jpVar = new jp(context, relativeLayout, new dj(context, a2, resultReceiver), nVar, window, jtVar);
        return jpVar;
    }

    private static boolean a(@NonNull Intent intent) {
        try {
            return intent.getBooleanExtra("extra_interstitial_isShouldOpenLinksInApp", false);
        } catch (Exception unused) {
            return false;
        }
    }
}
