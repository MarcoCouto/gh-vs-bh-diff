package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Map;

public interface ba {
    @NonNull
    Map<String, Object> a(@NonNull Context context);

    @NonNull
    Map<String, String> a(@NonNull bm bmVar);
}
