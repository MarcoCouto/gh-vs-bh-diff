package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback.Reason;
import java.util.EnumMap;
import java.util.Map;

final class gg {
    private static final Map<Reason, String> a;

    gg() {
    }

    static {
        EnumMap enumMap = new EnumMap(Reason.class);
        a = enumMap;
        enumMap.put(Reason.NETWORK, "Network error");
        a.put(Reason.INVALID_RESPONSE, "Invalid response");
        a.put(Reason.UNKNOWN, "Unknown");
    }

    @NonNull
    static String a(@Nullable Reason reason) {
        String str = (String) a.get(reason);
        return str != null ? str : "Unknown";
    }
}
