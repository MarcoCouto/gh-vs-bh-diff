package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.be;
import java.util.List;

public final class nn {
    @NonNull
    private final nt a = new nt();
    /* access modifiers changed from: private */
    @NonNull
    public final nq b;
    /* access modifiers changed from: private */
    @NonNull
    public final no c;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler d;

    private class a implements Runnable {
        @NonNull
        private final ag b;

        a(ag agVar) {
            this.b = agVar;
        }

        public final void run() {
            View c = this.b.c();
            if (c instanceof FrameLayout) {
                nn.this.c.a(nn.this.b.a(c.getContext()), (FrameLayout) c);
                nn.this.d.postDelayed(new a(this.b), 300);
            }
        }
    }

    public nn(@NonNull be beVar, @NonNull List<bn> list) {
        new nr();
        this.b = nr.a(beVar, list);
        this.c = new no();
        this.d = new Handler(Looper.getMainLooper());
    }

    public final void a() {
        this.d.removeCallbacksAndMessages(null);
    }

    public final void a(@NonNull ag agVar) {
        a();
        View c2 = agVar.c();
        if (c2 instanceof FrameLayout) {
            np.a((FrameLayout) c2);
        }
    }

    public final void a(@NonNull Context context, @NonNull ag agVar) {
        fv a2 = fv.a();
        fw a3 = a2.a(context);
        Boolean n = a3 != null ? a3.n() : null;
        boolean z = n != null ? n.booleanValue() : a2.d() && hz.a(context);
        if (z) {
            this.d.post(new a(agVar));
        }
    }
}
