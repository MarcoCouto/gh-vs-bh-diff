package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.dq.a;

/* renamed from: com.yandex.mobile.ads.impl.do reason: invalid class name */
public final class Cdo implements dq {
    @Nullable
    public final String a(@NonNull fl flVar) {
        return ad.a(flVar);
    }

    @NonNull
    public final String a(@NonNull Context context, @NonNull fl flVar) {
        return ad.a(context, flVar).d();
    }

    @NonNull
    public final int a() {
        return a.a;
    }
}
