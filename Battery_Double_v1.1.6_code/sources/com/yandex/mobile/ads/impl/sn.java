package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.widget.ImageView.ScaleType;
import com.yandex.mobile.ads.impl.ru.a;
import com.yandex.mobile.ads.impl.ru.b;

public final class sn extends rs<Bitmap> {
    private static final Object f = new Object();
    private final b<Bitmap> a;
    private final Config b;
    private final int c;
    private final int d;
    private ScaleType e;

    /* access modifiers changed from: protected */
    public final /* synthetic */ void b(Object obj) {
        this.a.a((Bitmap) obj);
    }

    public sn(String str, b<Bitmap> bVar, int i, int i2, ScaleType scaleType, Config config, a aVar) {
        super(0, str, aVar);
        a((rw) new rn(1000, 2, 2.0f));
        this.a = bVar;
        this.b = config;
        this.c = i;
        this.d = i2;
        this.e = scaleType;
    }

    public final rs.a o() {
        return rs.a.LOW;
    }

    private static int a(int i, int i2, int i3, int i4, ScaleType scaleType) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (scaleType == ScaleType.FIT_XY) {
            return i == 0 ? i3 : i;
        }
        if (i == 0) {
            double d2 = (double) i2;
            double d3 = (double) i4;
            Double.isNaN(d2);
            Double.isNaN(d3);
            double d4 = d2 / d3;
            double d5 = (double) i3;
            Double.isNaN(d5);
            return (int) (d5 * d4);
        } else if (i2 == 0) {
            return i;
        } else {
            double d6 = (double) i4;
            double d7 = (double) i3;
            Double.isNaN(d6);
            Double.isNaN(d7);
            double d8 = d6 / d7;
            if (scaleType == ScaleType.CENTER_CROP) {
                double d9 = (double) i;
                Double.isNaN(d9);
                double d10 = (double) i2;
                if (d9 * d8 < d10) {
                    Double.isNaN(d10);
                    i = (int) (d10 / d8);
                }
                return i;
            }
            double d11 = (double) i;
            Double.isNaN(d11);
            double d12 = (double) i2;
            if (d11 * d8 > d12) {
                Double.isNaN(d12);
                i = (int) (d12 / d8);
            }
            return i;
        }
    }

    /* access modifiers changed from: protected */
    public final ru<Bitmap> a(rr rrVar) {
        Bitmap bitmap;
        ru<Bitmap> ruVar;
        synchronized (f) {
            try {
                byte[] bArr = rrVar.b;
                Options options = new Options();
                if (this.c == 0 && this.d == 0) {
                    options.inPreferredConfig = this.b;
                    bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
                } else {
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
                    int i = options.outWidth;
                    int i2 = options.outHeight;
                    int a2 = a(this.c, this.d, i, i2, this.e);
                    int a3 = a(this.d, this.c, i2, i, this.e);
                    options.inJustDecodeBounds = false;
                    options.inSampleSize = a(i, i2, a2, a3);
                    bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
                    if (bitmap != null && (bitmap.getWidth() > a2 || bitmap.getHeight() > a3)) {
                        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, a2, a3, true);
                        bitmap.recycle();
                        bitmap = createScaledBitmap;
                    }
                }
                if (bitmap == null) {
                    ruVar = ru.a(new sc(rrVar));
                } else {
                    ruVar = ru.a(bitmap, sj.a(rrVar));
                }
            } catch (OutOfMemoryError e2) {
                rx.c("Caught OOM for %d byte image, url=%s", Integer.valueOf(rrVar.b.length), b());
                return ru.a(new sc((Throwable) e2));
            } catch (Throwable th) {
                throw th;
            }
        }
        return ruVar;
    }

    private static int a(int i, int i2, int i3, int i4) {
        double d2 = (double) i;
        double d3 = (double) i3;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        double d5 = (double) i2;
        double d6 = (double) i4;
        Double.isNaN(d5);
        Double.isNaN(d6);
        double min = Math.min(d4, d5 / d6);
        float f2 = 1.0f;
        while (true) {
            float f3 = 2.0f * f2;
            if (((double) f3) > min) {
                return (int) f2;
            }
            f2 = f3;
        }
    }
}
