package com.yandex.mobile.ads.impl;

import android.util.Log;
import java.util.Locale;

/* renamed from: com.yandex.mobile.ads.impl.if reason: invalid class name */
public final class Cif extends ie {
    private static boolean a;

    public static void a(boolean z) {
        a = z;
    }

    public static void a(String str, Object... objArr) {
        if (a) {
            Log.i("Yandex Mobile Ads", String.format(Locale.US, str, objArr));
        }
    }

    public static void b(String str, Object... objArr) {
        if (a) {
            Log.w("Yandex Mobile Ads", String.format(Locale.US, str, objArr));
        }
    }

    public static void c(String str, Object... objArr) {
        if (a) {
            Log.e("Yandex Mobile Ads", String.format(Locale.US, str, objArr));
        }
    }
}
