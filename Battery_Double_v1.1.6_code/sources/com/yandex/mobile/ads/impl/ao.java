package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class ao {
    static int a(@NonNull Context context) {
        fw a = fv.a().a(context);
        if (a == null || a.a() == 0) {
            return 5;
        }
        return a.a();
    }
}
