package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class hh {
    private static final Object a = new Object();
    private static volatile he b;

    @NonNull
    public static ha a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new he(context, "com.huawei.hms.location.LocationServices");
                }
            }
        }
        return b;
    }
}
