package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qb implements pz<ol> {
    @NonNull
    private final qc a;

    public qb(@NonNull qc qcVar) {
        this.a = qcVar;
    }

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, z {
        if (jSONObject.has("value")) {
            om omVar = null;
            if (!jSONObject.isNull("value")) {
                omVar = this.a.a(jSONObject);
            }
            return new ol(omVar);
        }
        throw new z("Native Ad json has not required attributes");
    }
}
