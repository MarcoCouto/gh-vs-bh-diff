package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.mobile.ads.nativeads.al;
import com.yandex.mobile.ads.nativeads.j;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Map;

public final class lm {
    @NonNull
    private final ll a;
    @NonNull
    private final al b;

    public lm(@NonNull al alVar, @NonNull j jVar, @NonNull ln lnVar) {
        this.b = alVar;
        this.a = new ll(jVar, lnVar);
    }

    @NonNull
    public final Map<String, lf> a() {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceSegment.AGE, ll.a(this.b.b()));
        hashMap.put(TtmlNode.TAG_BODY, ll.a(this.b.c()));
        hashMap.put("call_to_action", ll.a(this.b.d()));
        String str = "close_button";
        TextView e = this.b.e();
        lh lhVar = null;
        md lvVar = e != null ? new lv(e) : null;
        hashMap.put(str, lvVar != null ? new lh(lvVar) : null);
        hashMap.put(RequestParameters.DOMAIN, ll.a(this.b.f()));
        hashMap.put("favicon", this.a.a(this.b.g()));
        hashMap.put("feedback", this.a.b(this.b.h()));
        hashMap.put(SettingsJsonConstants.APP_ICON_KEY, this.a.a(this.b.i()));
        hashMap.put("media", this.a.a(this.b.j(), this.b.k()));
        String str2 = "rating";
        View m = this.b.m();
        md mbVar = m != null ? new mb(m) : null;
        if (mbVar != null) {
            lhVar = new lh(mbVar);
        }
        hashMap.put(str2, lhVar);
        hashMap.put("review_count", ll.a(this.b.n()));
        hashMap.put("price", ll.a(this.b.l()));
        hashMap.put("sponsored", ll.a(this.b.o()));
        hashMap.put("title", ll.a(this.b.p()));
        hashMap.put("warning", ll.a(this.b.q()));
        return hashMap;
    }
}
