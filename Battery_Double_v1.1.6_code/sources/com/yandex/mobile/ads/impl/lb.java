package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.nativeads.l;
import com.yandex.mobile.ads.mediation.nativeads.t;
import com.yandex.mobile.ads.nativeads.v;

final class lb implements ld {
    @NonNull
    private final w<or> a;
    @NonNull
    private final bl b;

    lb(@NonNull w<or> wVar, @NonNull bl blVar) {
        this.a = wVar;
        this.b = blVar;
    }

    @NonNull
    public final t a(@NonNull v vVar) {
        return new l(vVar, this.a, this.b);
    }
}
