package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.j;
import com.yandex.mobile.ads.nativeads.k;
import com.yandex.mobile.ads.nativeads.p;
import com.yandex.mobile.ads.nativeads.t;
import java.util.Map;
import java.util.Set;

final class nh {
    @NonNull
    private final mz a = new mz();
    /* access modifiers changed from: private */
    @NonNull
    public final ne b;
    @NonNull
    private final i c;

    nh(@NonNull ne neVar, @NonNull i iVar) {
        this.b = neVar;
        this.c = iVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull p pVar, @NonNull j jVar, @NonNull t tVar, @NonNull nc ncVar) {
        Set a2 = this.c.a(pVar.c().c());
        i iVar = this.c;
        final p pVar2 = pVar;
        final j jVar2 = jVar;
        final Context context2 = context;
        final t tVar2 = tVar;
        final nc ncVar2 = ncVar;
        AnonymousClass1 r2 = new k() {
            public final void a(@NonNull Map<String, Bitmap> map) {
                mz.a(pVar2, map);
                jVar2.a(map);
                nh.this.b.a(context2, pVar2, jVar2, tVar2, ncVar2);
            }
        };
        iVar.a(a2, r2);
    }
}
