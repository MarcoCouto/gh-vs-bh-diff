package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.impl.ru.a;
import com.yandex.mobile.ads.impl.ru.b;
import java.io.UnsupportedEncodingException;

public final class sq extends rs<String> {
    private final b<String> a;

    /* access modifiers changed from: protected */
    public final /* synthetic */ void b(Object obj) {
        this.a.a((String) obj);
    }

    public sq(String str, b<String> bVar, a aVar) {
        super(0, str, aVar);
        this.a = bVar;
    }

    /* access modifiers changed from: protected */
    public final ru<String> a(rr rrVar) {
        String str;
        try {
            str = new String(rrVar.b, sj.a(rrVar.c));
        } catch (UnsupportedEncodingException unused) {
            str = new String(rrVar.b);
        }
        return ru.a(str, sj.a(rrVar));
    }
}
