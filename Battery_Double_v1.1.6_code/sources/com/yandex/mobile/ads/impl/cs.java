package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.hr.a;
import java.util.HashMap;

public final class cs {
    @NonNull
    private final w a;
    @NonNull
    private final hp b;
    @Nullable
    private a c;

    public cs(@NonNull Context context, @NonNull w wVar) {
        this.a = wVar;
        this.b = hp.a(context);
    }

    public final void a(@Nullable a aVar) {
        this.c = aVar;
    }

    public final void a() {
        ea eaVar = new ea(new HashMap());
        eaVar.a("adapter", "Yandex");
        eaVar.a("block_id", this.a.d());
        eaVar.a("ad_type_format", this.a.b());
        eaVar.a("product_type", this.a.c());
        eaVar.a("ad_source", this.a.k());
        b a2 = this.a.a();
        eaVar.a(AppEventsConstants.EVENT_PARAM_AD_TYPE, a2 != null ? a2.a() : null);
        if (this.c != null) {
            eaVar.a(this.c.a());
        }
        this.b.a(new hr(hr.b.RENDERING_START, eaVar.a()));
    }
}
