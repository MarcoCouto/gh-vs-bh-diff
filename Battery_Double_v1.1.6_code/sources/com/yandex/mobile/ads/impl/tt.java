package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class tt implements RequestListener<List<VideoAd>> {
    /* access modifiers changed from: private */
    @NonNull
    public final va a;
    @NonNull
    private final tw b;
    @NonNull
    private final VideoAdRequest c;

    class a implements RequestListener<List<VideoAd>> {
        @NonNull
        private final RequestListener<List<VideoAd>> b;

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            List list = (List) obj;
            tt.this.a.a();
            this.b.onSuccess(list);
        }

        a(RequestListener<List<VideoAd>> requestListener) {
            this.b = requestListener;
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            tt.this.a.a(videoAdError);
            this.b.onFailure(videoAdError);
        }
    }

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        List list = (List) obj;
        RequestListener requestListener = this.c.getRequestListener();
        if (requestListener != null) {
            this.b.a(list, new a(requestListener));
        }
    }

    tt(@NonNull VideoAdRequest videoAdRequest, @NonNull vb vbVar) {
        this.c = videoAdRequest;
        Context context = videoAdRequest.getContext();
        this.a = new va(context, vbVar);
        this.b = new tw(context, vbVar);
    }

    public final void onFailure(@NonNull VideoAdError videoAdError) {
        RequestListener requestListener = this.c.getRequestListener();
        if (requestListener != null) {
            requestListener.onFailure(videoAdError);
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VideoAdRequest a() {
        return this.c;
    }
}
