package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class jo {
    private static final Object a = new Object();
    private static volatile jo b;
    @Nullable
    private w<String> c;
    @Nullable
    private fl d;

    @NonNull
    public static jo a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new jo();
                }
            }
        }
        return b;
    }

    private jo() {
    }

    public final void a(@NonNull w<String> wVar) {
        synchronized (a) {
            this.c = wVar;
        }
    }

    public final void a(@NonNull fl flVar) {
        synchronized (a) {
            this.d = flVar;
        }
    }

    @Nullable
    public final w<String> b() {
        w<String> wVar;
        synchronized (a) {
            wVar = this.c;
        }
        return wVar;
    }

    @Nullable
    public final fl c() {
        fl flVar;
        synchronized (a) {
            flVar = this.d;
        }
        return flVar;
    }
}
