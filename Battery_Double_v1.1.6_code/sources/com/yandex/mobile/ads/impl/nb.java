package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

final class nb {
    private static final Map<ae, na> a = new HashMap<ae, na>() {
        {
            put(ae.AD, new ng());
            put(ae.AD_UNIT, new nf());
        }
    };

    nb() {
    }

    @Nullable
    static na a(@Nullable ae aeVar) {
        return (na) a.get(aeVar);
    }
}
