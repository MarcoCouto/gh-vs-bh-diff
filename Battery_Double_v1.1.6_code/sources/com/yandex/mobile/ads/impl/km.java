package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.interstitial.d;
import com.yandex.mobile.ads.mediation.rewarded.a;
import com.yandex.mobile.ads.rewarded.b;

final class km implements kk {
    @NonNull
    private final w<String> a;
    @NonNull
    private final bl b;

    km(@NonNull w<String> wVar, @NonNull bl blVar) {
        this.a = wVar;
        this.b = blVar;
    }

    @NonNull
    public final kj a(@NonNull ij ijVar) {
        return new d(ijVar, this.a, this.b);
    }

    @NonNull
    public final kj a(@NonNull b bVar) {
        return new a(bVar, this.a, this.b);
    }
}
