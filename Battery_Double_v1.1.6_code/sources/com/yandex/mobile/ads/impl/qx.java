package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.c;
import java.util.List;
import java.util.Map;

public final class qx extends cw<or> {
    @NonNull
    private final qy a = new qy();

    /* access modifiers changed from: protected */
    @NonNull
    public final Map<String, Object> a(@NonNull fl flVar) {
        Map<String, Object> a2 = super.a(flVar);
        a2.put("image_loading_automatically", Boolean.valueOf(flVar.u()));
        String[] o = flVar.o();
        if (o != null && o.length > 0) {
            a2.put("image_sizes", flVar.o());
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Map<String, Object> a(@NonNull fl flVar, @Nullable ru<w<or>> ruVar, int i) {
        c cVar;
        Map<String, Object> a2 = super.a(flVar, ruVar, i);
        if (204 == i) {
            cVar = c.NO_ADS;
        } else if (ruVar == null || ruVar.a == null || i != 200) {
            cVar = c.ERROR;
        } else {
            w wVar = (w) ruVar.a;
            or orVar = (or) wVar.p();
            cVar = orVar != null ? (c) orVar.a().get("status") : wVar.n() == null ? c.ERROR : null;
        }
        if (cVar != null) {
            a2.put("status", cVar.a());
        }
        if (!(ruVar == null || ruVar.a == null)) {
            List a3 = qy.a((w) ruVar.a);
            if (!a3.isEmpty()) {
                a2.put("image_sizes", a3.toArray(new String[a3.size()]));
            }
            List b = qy.b((w) ruVar.a);
            if (!b.isEmpty()) {
                a2.put("native_ad_types", b.toArray(new String[b.size()]));
            }
        }
        return a2;
    }
}
