package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.Set;

public final class ql {
    @NonNull
    public static Set<om> a(@NonNull oq oqVar) {
        HashSet hashSet = new HashSet();
        for (oj c : oqVar.c()) {
            Object c2 = c.c();
            if (c2 instanceof om) {
                hashSet.add((om) c2);
            }
            if (c2 instanceof op) {
                om b = ((op) c2).b();
                if (b != null) {
                    hashSet.add(b);
                }
            }
        }
        return hashSet;
    }
}
