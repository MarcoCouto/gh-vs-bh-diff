package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.interstitial.e;
import com.yandex.mobile.ads.rewarded.b;

final class kn implements kk {
    kn() {
    }

    @NonNull
    public final kj a(@NonNull ij ijVar) {
        return new e(ijVar);
    }

    @NonNull
    public final kj a(@NonNull b bVar) {
        return new com.yandex.mobile.ads.mediation.rewarded.e(bVar);
    }
}
