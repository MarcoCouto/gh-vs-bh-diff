package com.yandex.mobile.ads.impl;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.TimeUnit;

final class gy {
    private static final long a = TimeUnit.MINUTES.toMillis(2);

    gy() {
    }

    static boolean a(@NonNull Location location, @Nullable Location location2) {
        if (location2 != null) {
            long time = location.getTime() - location2.getTime();
            boolean z = time > a;
            boolean z2 = time < (-a);
            boolean z3 = time > 0;
            int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
            boolean z4 = accuracy > 0;
            boolean z5 = accuracy < 0;
            boolean z6 = ((long) accuracy) > 200;
            String provider = location.getProvider();
            String provider2 = location2.getProvider();
            boolean z7 = provider == null ? provider2 == null : provider.equals(provider2);
            if (z || (!z2 && (z5 || ((z3 && !z4) || (z3 && !z6 && z7))))) {
                return true;
            }
            return false;
        }
        return true;
    }
}
