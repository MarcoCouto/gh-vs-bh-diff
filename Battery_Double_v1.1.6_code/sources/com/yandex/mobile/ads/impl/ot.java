package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public abstract class ot {
    @NonNull
    private final String a;

    protected ot(@NonNull String str) {
        this.a = str;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.a.equals(((ot) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
