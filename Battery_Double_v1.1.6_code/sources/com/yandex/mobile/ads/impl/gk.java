package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public final class gk {
    @NonNull
    private final fl a;
    /* access modifiers changed from: private */
    @NonNull
    public final List<gi> b = new CopyOnWriteArrayList();

    public interface a {
        void a();

        void a(@NonNull String str);
    }

    public gk(@NonNull fl flVar) {
        this.a = flVar;
    }

    public final void a(@NonNull gj gjVar, @NonNull final a aVar) {
        AnonymousClass1 r0 = new gi() {
            public final void a(@NonNull Map<String, String> map) {
                gk.this.b.remove(this);
                gk.a(gk.this, (Map) map);
                aVar.a();
            }

            public final void a(@NonNull String str) {
                gk.this.b.remove(this);
                aVar.a(str);
            }
        };
        this.b.add(r0);
        gjVar.a(r0);
    }

    public final void a(@NonNull gj gjVar) {
        for (gi b2 : this.b) {
            gjVar.b(b2);
        }
    }

    static /* synthetic */ void a(gk gkVar, Map map) {
        new Object[1][0] = map;
        gkVar.a.a((String) map.get("yandex_mobile_metrica_uuid"));
        gkVar.a.c((String) map.get("yandex_mobile_metrica_get_ad_url"));
        gkVar.a.d((String) map.get("yandex_mobile_metrica_device_id"));
    }
}
