package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sv.a;

public final class pg extends bw<or> {
    private final cz<or> a;

    public pg(@NonNull Context context, @NonNull cz<or> czVar, @NonNull fl flVar, @NonNull String str, @NonNull String str2, @NonNull a<w<or>> aVar) {
        super(context, flVar, str, str2, aVar, new qx());
        this.a = czVar;
    }

    /* access modifiers changed from: protected */
    public final boolean b(@NonNull rr rrVar, int i) {
        return (200 == i || a(i)) && b(rrVar);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final /* synthetic */ Object a_(@NonNull rr rrVar) {
        return (or) this.a.a(rrVar);
    }
}
