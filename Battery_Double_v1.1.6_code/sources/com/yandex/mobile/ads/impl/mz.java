package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.yandex.mobile.ads.nativeads.p;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class mz {
    mz() {
    }

    static void a(@NonNull p pVar, @NonNull Map<String, Bitmap> map) {
        for (oq oqVar : pVar.c().c()) {
            List<oj> c = oqVar.c();
            if (c != null && !c.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (oj ojVar : c) {
                    Object c2 = ojVar.c();
                    String b = ojVar.b();
                    if (!MessengerShareContentUtility.MEDIA_IMAGE.equals(b) || !(c2 instanceof om)) {
                        if ("media".equals(b) && (c2 instanceof op) && ((op) c2).b() != null) {
                            om b2 = ((op) c2).b();
                            if (b2 != null && a(b2, map)) {
                                arrayList.add(ojVar);
                            }
                        } else {
                            arrayList.add(ojVar);
                        }
                    } else if (a((om) c2, map)) {
                        arrayList.add(ojVar);
                    }
                }
                oqVar.a((List<oj>) arrayList);
            }
        }
    }

    private static boolean a(@NonNull om omVar, @NonNull Map<String, Bitmap> map) {
        return a((Bitmap) map.get(omVar.c()));
    }

    private static boolean a(@Nullable Bitmap bitmap) {
        return bitmap != null && bitmap.getWidth() > 1 && bitmap.getHeight() > 1;
    }
}
