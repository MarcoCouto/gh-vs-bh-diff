package com.yandex.mobile.ads.impl;

public class sf extends Exception {
    public final rr a;
    private long b;

    public sf() {
        this.a = null;
    }

    public sf(rr rrVar) {
        this.a = rrVar;
    }

    public sf(String str) {
        super(str);
        this.a = null;
    }

    public sf(String str, Throwable th) {
        super(str, th);
        this.a = null;
    }

    public sf(Throwable th) {
        super(th);
        this.a = null;
    }

    public void a(long j) {
        this.b = j;
    }
}
