package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.bm.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class cc implements cd<bl> {
    @NonNull
    private final bt a = new bt();

    @Nullable
    public final /* synthetic */ Object b(@NonNull rr rrVar) {
        return a(rrVar);
    }

    @Nullable
    public static bl a(@NonNull rr rrVar) {
        String a2 = bt.a(rrVar);
        if (!TextUtils.isEmpty(a2)) {
            return a(a2);
        }
        return null;
    }

    @Nullable
    private static bl a(@NonNull String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Map a2 = a(jSONObject);
            if (a2.isEmpty()) {
                return null;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("networks");
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                bm b = b(jSONArray.getJSONObject(i));
                if (b != null) {
                    arrayList.add(b);
                }
            }
            if (!arrayList.isEmpty()) {
                return new bl(arrayList, a2);
            }
            return null;
        } catch (JSONException unused) {
            return null;
        }
    }

    @NonNull
    private static Map<String, String> a(@NonNull JSONObject jSONObject) throws JSONException {
        try {
            return dv.a(jSONObject, "passback_parameters");
        } catch (JSONException e) {
            throw new JSONException(e.getMessage());
        }
    }

    @Nullable
    private static bm b(@NonNull JSONObject jSONObject) {
        try {
            String c = dv.c(jSONObject, "adapter");
            Map a2 = dv.a(jSONObject, "network_data");
            if (a2.isEmpty()) {
                return null;
            }
            List b = dv.b(jSONObject, "click_tracking_urls");
            List b2 = dv.b(jSONObject, "impression_tracking_urls");
            return new a(c, a2).a(b2).b(b).c(dv.b(jSONObject, "ad_response_tracking_urls")).a();
        } catch (JSONException unused) {
            return null;
        }
    }
}
