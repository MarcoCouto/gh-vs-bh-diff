package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.List;

public class oq {
    private on a;
    private NativeAdType b;
    private List<oj> c;
    private bn d;
    @Nullable
    private String e;
    private String f;
    private oz g;
    private oz h;

    public final on a() {
        return this.a;
    }

    public final NativeAdType b() {
        return this.b;
    }

    public final List<oj> c() {
        return this.c;
    }

    public final void a(List<oj> list) {
        this.c = list;
    }

    public final void a(@Nullable on onVar) {
        if (onVar != null) {
            this.a = onVar;
        }
    }

    @Nullable
    public final oj b(@NonNull String str) {
        if (this.c != null) {
            for (oj ojVar : this.c) {
                if (ojVar.a().equals(str)) {
                    return ojVar;
                }
            }
        }
        return null;
    }

    public final void a(bn bnVar) {
        this.d = bnVar;
    }

    public final bn d() {
        return this.d;
    }

    public final void c(@Nullable String str) {
        this.e = str;
    }

    @Nullable
    public final String e() {
        return this.e;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final void a(oz ozVar) {
        this.g = ozVar;
    }

    public final void b(oz ozVar) {
        this.h = ozVar;
    }

    public final String f() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        oq oqVar = (oq) obj;
        if (this.a == null ? oqVar.a != null : !this.a.equals(oqVar.a)) {
            return false;
        }
        if (this.b != oqVar.b) {
            return false;
        }
        if (this.c == null ? oqVar.c != null : !this.c.equals(oqVar.c)) {
            return false;
        }
        if (this.d == null ? oqVar.d != null : !this.d.equals(oqVar.d)) {
            return false;
        }
        if (this.e == null ? oqVar.e != null : !this.e.equals(oqVar.e)) {
            return false;
        }
        if (this.f == null ? oqVar.f != null : !this.f.equals(oqVar.f)) {
            return false;
        }
        if (this.g == null ? oqVar.g != null : !this.g.equals(oqVar.g)) {
            return false;
        }
        if (this.h != null) {
            return this.h.equals(oqVar.h);
        }
        return oqVar.h == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31;
        if (this.h != null) {
            i = this.h.hashCode();
        }
        return hashCode + i;
    }

    public final void a(String str) {
        NativeAdType nativeAdType;
        NativeAdType[] values = NativeAdType.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                nativeAdType = null;
                break;
            }
            nativeAdType = values[i];
            if (nativeAdType.getValue().equals(str)) {
                break;
            }
            i++;
        }
        this.b = nativeAdType;
    }
}
