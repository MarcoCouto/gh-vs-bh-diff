package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class kf {
    @NonNull
    public static kg a(@NonNull w<String> wVar) {
        bl n = wVar.n();
        if (n != null) {
            return new kh(wVar, n);
        }
        return new ki();
    }
}
