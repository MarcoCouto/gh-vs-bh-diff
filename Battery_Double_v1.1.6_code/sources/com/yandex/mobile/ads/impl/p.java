package com.yandex.mobile.ads.impl;

import android.app.Activity;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class p implements n {
    @NonNull
    private final Activity a;
    @Nullable
    private final ResultReceiver b;

    public p(@NonNull Activity activity, @Nullable ResultReceiver resultReceiver) {
        this.a = activity;
        this.b = resultReceiver;
    }

    public final void a(int i) {
        try {
            if (VERSION.SDK_INT != 26) {
                this.a.setRequestedOrientation(i);
            }
        } catch (Exception unused) {
        }
    }

    public final void a(int i, @Nullable Bundle bundle) {
        if (this.b != null) {
            this.b.send(i, bundle);
        }
    }

    public final void a() {
        this.a.finish();
    }
}
