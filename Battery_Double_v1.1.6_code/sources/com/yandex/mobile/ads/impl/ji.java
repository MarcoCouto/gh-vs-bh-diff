package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class ji implements Runnable {
    @Nullable
    private el a;

    public final void run() {
        if (this.a != null) {
            this.a.onAdFailedToLoad(u.i);
        }
    }

    public final void a(@Nullable el elVar) {
        this.a = elVar;
    }
}
