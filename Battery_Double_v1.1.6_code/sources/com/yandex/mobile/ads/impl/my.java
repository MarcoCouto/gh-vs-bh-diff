package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;

public final class my implements OnClickListener {
    private final View a;

    public my(@Nullable View view) {
        this.a = view;
    }

    public final void onClick(View view) {
        if (this.a != null) {
            this.a.setSelected(!this.a.isSelected());
        }
    }
}
