package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.InterstitialEventListener;

public final class jr implements ii {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final cr c;
    /* access modifiers changed from: private */
    @Nullable
    public InterstitialEventListener d;

    public jr(@NonNull Context context) {
        this.c = new cr(context);
    }

    public final void a(@NonNull fl flVar) {
        this.c.a(flVar);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final InterstitialEventListener h() {
        InterstitialEventListener interstitialEventListener;
        synchronized (a) {
            interstitialEventListener = this.d;
        }
        return interstitialEventListener;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable InterstitialEventListener interstitialEventListener) {
        synchronized (a) {
            this.d = interstitialEventListener;
        }
    }

    public final void b() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onInterstitialDismissed();
                    }
                }
            }
        });
    }

    public final void c() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        ib.a((Object) jr.this.d, "onAdImpressionTracked", new Object[0]);
                    }
                }
            }
        });
    }

    public final void a(@NonNull final AdRequestError adRequestError) {
        this.c.a(adRequestError);
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onInterstitialFailedToLoad(adRequestError);
                    }
                }
            }
        });
    }

    public final void e() {
        this.c.a();
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onInterstitialLoaded();
                    }
                }
            }
        });
    }

    public final void g() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onInterstitialShown();
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onAdClosed();
                    }
                }
            }
        });
    }

    public final void d() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onAdLeftApplication();
                    }
                }
            }
        });
    }

    public final void f() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (jr.a) {
                    if (jr.this.d != null) {
                        jr.this.d.onAdOpened();
                    }
                }
            }
        });
    }
}
