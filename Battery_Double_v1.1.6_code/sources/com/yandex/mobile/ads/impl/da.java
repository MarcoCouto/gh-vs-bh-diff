package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.MobileAds;

public final class da implements cz<fw> {
    @NonNull
    private final Context a;
    @NonNull
    private final cy b = new cy();
    @NonNull
    private final db c = new db();
    @NonNull
    private final dd d = new dd();
    @NonNull
    private final cd<fw> e = new cf();

    public da(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    public final boolean a() {
        fw a2 = fv.a().a(this.a);
        if (a2 != null) {
            if (!(System.currentTimeMillis() >= a2.b()) && !(!MobileAds.getLibraryVersion().equals(a2.m()))) {
                if (fv.a().e() != a2.e()) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    @Nullable
    public final /* synthetic */ Object a(@NonNull rr rrVar) {
        return (fw) this.e.b(rrVar);
    }
}
