package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.p;
import com.yandex.mobile.ads.impl.aj.a;

final class hq implements a {
    @NonNull
    private final ge a = new ge();
    @NonNull
    private final fv b = fv.a();
    @Nullable
    private IReporter c;

    public hq(@NonNull aj ajVar) {
        ajVar.a(this);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull Context context) {
        fw a2 = this.b.a(context);
        return a2 != null && a2.d();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable IReporter iReporter) {
        this.c = iReporter;
    }

    public final void a(@NonNull Context context, @NonNull fw fwVar) {
        if (this.c != null) {
            this.c.setStatisticsSending(fwVar.d());
        }
        boolean z = a(context) && this.b.b();
        try {
            if (ib.b(p.class, "slte", context)) {
                p.slte(context, z);
            }
        } catch (Throwable unused) {
        }
    }
}
