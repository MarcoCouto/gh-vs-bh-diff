package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.j;
import com.yandex.mobile.ads.nativeads.o;
import com.yandex.mobile.ads.nativeads.p;
import com.yandex.mobile.ads.nativeads.t;

final class ne {
    @NonNull
    private final fl a;
    @NonNull
    private final o b = new o();
    @NonNull
    private final nb c = new nb();

    ne(@NonNull fl flVar) {
        this.a = flVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull p pVar, @NonNull j jVar, @NonNull t tVar, @NonNull nc ncVar) {
        na a2 = nb.a(this.a.p());
        if (a2 != null) {
            a2.a(context, pVar, jVar, tVar, ncVar);
        } else {
            ncVar.a(u.a);
        }
    }
}
