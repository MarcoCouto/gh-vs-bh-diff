package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public interface iy {
    @NonNull
    iu a(@NonNull ep epVar, @NonNull el elVar, @NonNull jg jgVar, @NonNull eo eoVar, @NonNull jf jfVar);

    @NonNull
    iv a(@NonNull ep epVar, @NonNull el elVar);

    @NonNull
    iv a(@NonNull ep epVar, @NonNull el elVar, @NonNull eh ehVar, @NonNull jf jfVar);
}
