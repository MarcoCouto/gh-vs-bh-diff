package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdActivity;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.a;
import com.yandex.mobile.ads.b;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

public abstract class ij extends iq {
    @NonNull
    private final ii h;
    @NonNull
    private final kl i = new kl();
    @Nullable
    private kj j;
    @Nullable
    private kj k;
    @NonNull
    private final l l = new jq();

    /* access modifiers changed from: protected */
    @NonNull
    public abstract kj a(@NonNull kk kkVar);

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return true;
    }

    public ij(@NonNull Context context, @NonNull b bVar, @NonNull ii iiVar) {
        super(context, new ih(), bVar);
        this.h = iiVar;
        b(a.a(AdSize.FULL_SCREEN));
        m.a().a("window_type_interstitial", this.l);
    }

    public void a() {
        if (this.j != null && !a_()) {
            this.j.b();
        }
    }

    public final void g() {
        Context context = this.b;
        w y = y();
        fl r = r();
        x xVar = this.d;
        if (context != null) {
            Intent intent = new Intent(context, AdActivity.class);
            intent.putExtra("window_type", "window_type_interstitial");
            intent.putExtra("extra_receiver", dr.a((ResultReceiver) xVar));
            intent.putExtra("extra_interstitial_isShouldOpenLinksInApp", r.k());
            intent.addFlags(268435456);
            jo a = jo.a();
            a.a(y);
            a.a(r);
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                Cif.c("Failed to show Interstitial Ad. Exception: ".concat(String.valueOf(e)), new Object[0]);
            }
        }
    }

    public final void a(AdRequest adRequest) {
        t();
        super.a(adRequest);
    }

    public final boolean z() {
        return this.j != null && this.j.a();
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull al alVar) {
        return alVar.b(this.b) > 0 && alVar.a(this.b) > 0;
    }

    public void a(int i2, @Nullable Bundle bundle) {
        StringBuilder sb = new StringBuilder("onReceiveResult(), resultCode = ");
        sb.append(i2);
        sb.append(", clazz = ");
        sb.append(getClass());
        if (i2 == 0) {
            a((WebView) null, bundle != null ? (Map) bundle.getSerializable("extra_tracking_parameters") : null);
        } else if (i2 != 8) {
            switch (i2) {
                case 2:
                    b(0);
                    return;
                case 3:
                    b(8);
                    return;
                case 4:
                    B();
                    return;
                case 5:
                    return;
                case 6:
                    onAdOpened();
                    return;
                default:
                    super.a(i2, bundle);
                    return;
            }
        } else {
            onAdClosed();
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final iv a(@NonNull String str, @NonNull w<String> wVar, @NonNull al alVar, @NonNull as asVar) {
        il ilVar = new il(this);
        ik ikVar = new ik(this.b, wVar);
        new iw();
        boolean a = iw.a(str);
        iz.a();
        return iz.a(a).a(ikVar, ilVar);
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        A();
        super.a(webView, map);
    }

    public final void e() {
        a(this.b, this.k, this.j);
        super.e();
    }

    public final synchronized void d() {
        super.d();
    }

    public final void A() {
        this.h.g();
        if (this.k != this.j) {
            a(this.b, this.k);
            this.k = this.j;
        }
    }

    private static void a(@NonNull Context context, @NonNull kj... kjVarArr) {
        for (kj kjVar : new HashSet(Arrays.asList(kjVarArr))) {
            if (kjVar != null) {
                kjVar.a(context);
            }
        }
    }

    public void onAdClosed() {
        super.onAdClosed();
        this.h.a();
    }

    public void onAdOpened() {
        super.onAdOpened();
        this.h.f();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.h.e();
    }

    /* access modifiers changed from: protected */
    public final void a(@NonNull AdRequestError adRequestError) {
        this.h.a(adRequestError);
    }

    public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        super.onAdFailedToLoad(adRequestError);
        u();
    }

    public void onAdLeftApplication() {
        super.onAdLeftApplication();
        this.h.d();
    }

    public final void B() {
        u();
        this.h.b();
    }

    public final void f() {
        this.h.c();
    }

    public void a(@NonNull w<String> wVar) {
        super.a(wVar);
        this.j = a(kl.a(wVar));
        this.j.a(this.b, wVar);
    }
}
