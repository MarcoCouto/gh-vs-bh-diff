package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdRequestError;
import java.util.Collections;
import java.util.Map;

public final class en implements el {
    @NonNull
    private final ep a;
    @NonNull
    private final as b;
    @NonNull
    private final eg c = new eg();
    @NonNull
    private final er d;
    @NonNull
    private final iw e = new iw();
    @Nullable
    private mu f;

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
    }

    public final void b(boolean z) {
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
    }

    public final void onAdLoaded() {
    }

    en(@NonNull em emVar, @NonNull as asVar) {
        this.a = emVar;
        this.b = asVar;
        this.d = new er(emVar);
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull String str) {
        boolean a2 = iw.a(str);
        iz.a();
        iz.a(a2).a(this.a, this, this.c, this.b).a(str);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull mu muVar) {
        this.f = muVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.d.a(Collections.emptyMap());
    }

    public final void a(@NonNull String str) {
        if (this.f != null) {
            this.f.a(this.a, str);
        }
    }
}
