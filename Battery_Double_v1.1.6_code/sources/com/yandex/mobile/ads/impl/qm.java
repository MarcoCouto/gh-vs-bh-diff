package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.hr.b;

public final class qm {
    @NonNull
    private final co a;
    @NonNull
    private final oa b;
    @NonNull
    private final rb c = new rb();
    @NonNull
    private final qo d;

    public qm(@NonNull Context context, @NonNull co coVar, @NonNull oa oaVar) {
        this.a = coVar;
        this.b = oaVar;
        this.d = new qo(context);
    }

    public final void a(@NonNull Context context, @NonNull ox oxVar) {
        Intent a2 = this.d.a(oxVar.c());
        if (a2 != null) {
            Context a3 = rb.a(context);
            if (a3 != null) {
                this.a.a(b.DEEPLINK);
                a3.startActivity(a2);
            }
            return;
        }
        this.b.a(oxVar.b());
    }
}
