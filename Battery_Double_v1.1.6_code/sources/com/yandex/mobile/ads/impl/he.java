package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class he implements ha {
    private static final Object a = new Object();
    @NonNull
    private final hk b;

    he(@NonNull Context context, @NonNull String str) {
        this.b = new hk(context, str);
    }

    @Nullable
    public final Location a() {
        Location location;
        synchronized (a) {
            location = null;
            hj a2 = this.b.a();
            if (a2 != null && a2.a()) {
                location = a2.b();
                this.b.b();
            }
        }
        return location;
    }
}
