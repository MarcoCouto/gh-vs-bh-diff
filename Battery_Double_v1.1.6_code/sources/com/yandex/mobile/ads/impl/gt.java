package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.WeakHashMap;

public final class gt {
    @NonNull
    private final Object a = new Object();
    @NonNull
    private final WeakHashMap<gv, Object> b = new WeakHashMap<>();

    gt() {
    }

    public final void a(@NonNull gv gvVar) {
        synchronized (this.a) {
            this.b.put(gvVar, null);
        }
    }

    public final void b(@NonNull gv gvVar) {
        synchronized (this.a) {
            this.b.remove(gvVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull go goVar) {
        synchronized (this.a) {
            b(goVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        synchronized (this.a) {
            b((go) null);
        }
    }

    private void b(@Nullable go goVar) {
        for (gv a2 : this.b.keySet()) {
            a2.a(goVar);
        }
        this.b.clear();
    }
}
