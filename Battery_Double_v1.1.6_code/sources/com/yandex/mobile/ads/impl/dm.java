package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public final class dm implements dn {
    @NonNull
    private final co a;
    @NonNull
    private final dk b;
    @NonNull
    private final Context c;
    private final int d;

    public dm(@NonNull Context context, @NonNull w wVar, @NonNull co coVar, @NonNull dk dkVar) {
        this.a = coVar;
        this.b = dkVar;
        this.c = context.getApplicationContext();
        this.d = wVar.t();
    }

    public final void a(@NonNull String str) {
        this.b.a(this.a, b(str));
    }

    @VisibleForTesting
    private String b(@NonNull String str) {
        HttpURLConnection httpURLConnection = null;
        String str2 = str;
        int i = 0;
        while (true) {
            if (i >= this.d) {
                break;
            }
            boolean z = true;
            try {
                if (ec.a(str2)) {
                    break;
                } else if (!ec.c(str2)) {
                    break;
                } else {
                    Context context = this.c;
                    int i2 = hn.a;
                    HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str2).openConnection();
                    new hy();
                    httpURLConnection2.setRequestProperty("User-Agent", hy.a(context));
                    if (i2 > 0) {
                        httpURLConnection2.setConnectTimeout(i2);
                        httpURLConnection2.setReadTimeout(i2);
                    }
                    if (hz.a(21) && (httpURLConnection2 instanceof HttpsURLConnection)) {
                        SSLSocketFactory a2 = sx.a();
                        if (a2 != null) {
                            ((HttpsURLConnection) httpURLConnection2).setSSLSocketFactory(a2);
                        }
                    }
                    httpURLConnection2.setInstanceFollowRedirects(false);
                    try {
                        int responseCode = httpURLConnection2.getResponseCode();
                        Object[] objArr = {str2, Integer.valueOf(responseCode)};
                        String headerField = httpURLConnection2.getHeaderField(rk.LOCATION.a());
                        new Object[1][0] = headerField;
                        if (responseCode < 300 || responseCode >= 400 || TextUtils.isEmpty(headerField)) {
                            z = false;
                        }
                        if (!z) {
                            dr.a(httpURLConnection2);
                            break;
                        }
                        dr.a(httpURLConnection2);
                        str2 = headerField;
                        httpURLConnection = httpURLConnection2;
                        i++;
                    } catch (Exception e) {
                        e = e;
                        httpURLConnection = httpURLConnection2;
                        try {
                            Object[] objArr2 = {str2, e.toString()};
                            dr.a(httpURLConnection);
                            i++;
                        } catch (Throwable th) {
                            th = th;
                            dr.a(httpURLConnection);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        httpURLConnection = httpURLConnection2;
                        dr.a(httpURLConnection);
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                Object[] objArr22 = {str2, e.toString()};
                dr.a(httpURLConnection);
                i++;
            }
        }
        dr.a(httpURLConnection);
        return str2;
    }
}
