package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.StatFs;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.DisplayMetrics;
import java.io.File;

public final class dw {
    public static long a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j = (((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 50;
        } catch (IllegalArgumentException unused) {
            j = 10485760;
        }
        return Math.max(Math.min(j, 104857600), 10485760);
    }

    public static int a(Context context) {
        int i;
        try {
            int maxMemory = (int) (Runtime.getRuntime().maxMemory() / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            i = Math.min(maxMemory / 8, ((int) ((((float) (displayMetrics.widthPixels * displayMetrics.heightPixels)) * displayMetrics.density) / 1024.0f)) * 3);
        } catch (IllegalArgumentException unused) {
            i = 5120;
        }
        return Math.max(i, 5120);
    }
}
