package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class cg implements cd<String> {
    @Nullable
    public final /* synthetic */ Object b(@NonNull rr rrVar) {
        return a(rrVar);
    }

    @Nullable
    private static String a(@NonNull rr rrVar) {
        if (rrVar.b == null) {
            return null;
        }
        try {
            return new String(rrVar.b, sj.a(rrVar.c));
        } catch (Exception unused) {
            return new String(rrVar.b);
        }
    }
}
