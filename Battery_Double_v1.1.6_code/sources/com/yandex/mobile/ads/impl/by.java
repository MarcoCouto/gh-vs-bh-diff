package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.sv.a;

public final class by extends sv<fw> {
    @NonNull
    private final cz<fw> a;

    public by(@NonNull String str, @NonNull cz<fw> czVar, @NonNull a<fw> aVar) {
        super(0, str, aVar);
        this.a = czVar;
    }

    /* access modifiers changed from: protected */
    public final ru<fw> a(@NonNull rr rrVar) {
        int i;
        if (200 == rrVar.a) {
            fw fwVar = (fw) this.a.a(rrVar);
            if (fwVar != null) {
                return ru.a(fwVar, sj.a(rrVar));
            }
            i = 5;
        } else {
            i = 8;
        }
        return ru.a(new r(rrVar, i));
    }

    /* access modifiers changed from: protected */
    public final sf a(sf sfVar) {
        return super.a((sf) r.b(sfVar.a));
    }
}
