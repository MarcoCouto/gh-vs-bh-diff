package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;
import java.util.Map;

public final class cp implements co {
    @NonNull
    private final ct a = new ct();
    @NonNull
    private final hp b;
    @NonNull
    private final w c;
    @Nullable
    private final fl d;
    @Nullable
    private final a e;

    public cp(@NonNull Context context, @NonNull w wVar, @Nullable fl flVar, @Nullable a aVar) {
        this.c = wVar;
        this.d = flVar;
        this.e = aVar;
        this.b = hp.a(context);
    }

    public final void a(@NonNull b bVar) {
        this.b.a(b(bVar, new HashMap()));
    }

    public final void a(@NonNull b bVar, @NonNull Map<String, Object> map) {
        this.b.a(b(bVar, map));
    }

    private hr b(@NonNull b bVar, @NonNull Map<String, Object> map) {
        ea eaVar = new ea(map);
        com.yandex.mobile.ads.b a2 = this.c.a();
        if (a2 != null) {
            eaVar.a(AppEventsConstants.EVENT_PARAM_AD_TYPE, a2.a());
        } else {
            eaVar.a(AppEventsConstants.EVENT_PARAM_AD_TYPE);
        }
        eaVar.a("block_id", this.c.d());
        eaVar.a("adapter", "Yandex");
        eaVar.a("ad_type_format", this.c.b());
        eaVar.a("product_type", this.c.c());
        eaVar.a("ad_source", this.c.k());
        if (this.d != null) {
            map.putAll(ct.a(this.d.c()));
        }
        if (this.e != null) {
            map.putAll(this.e.a());
        }
        return new hr(bVar, eaVar.a());
    }
}
