package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class go {
    @Nullable
    private final gm a;
    @Nullable
    private final gm b;

    go(@Nullable gm gmVar, @Nullable gm gmVar2) {
        this.a = gmVar;
        this.b = gmVar2;
    }

    @Nullable
    public final gm a() {
        return this.a;
    }

    @Nullable
    public final gm b() {
        return this.b;
    }
}
