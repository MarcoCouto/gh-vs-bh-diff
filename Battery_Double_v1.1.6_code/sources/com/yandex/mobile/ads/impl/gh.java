package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.startapp.networkTest.c.a;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.p;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class gh implements IIdentifierCallback, gj {
    private static final long a = ((long) hn.b);
    private static final Object b = new Object();
    private static volatile gj c;
    @NonNull
    private final Context d;
    @NonNull
    private final ge e = new ge();
    @NonNull
    private final WeakHashMap<gi, Object> f = new WeakHashMap<>();
    @NonNull
    private final Handler g = new Handler(Looper.getMainLooper());
    @NonNull
    private final gl h = new gl();
    /* access modifiers changed from: private */
    @NonNull
    public final gf i = new gf();
    @Nullable
    private Map<String, String> j;
    private boolean k;

    private gh(@NonNull Context context) {
        this.d = context.getApplicationContext();
        hp.a(context);
    }

    @NonNull
    public static gj a(@NonNull Context context) {
        if (c == null) {
            synchronized (b) {
                if (c == null) {
                    c = new gh(context.getApplicationContext());
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:19|20) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:14|15|16|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        a(com.yandex.mobile.ads.impl.gf.b());
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0046 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004e */
    public final void a(@NonNull gi giVar) {
        synchronized (b) {
            if (this.j == null || !gl.a(this.j)) {
                this.f.put(giVar, null);
                if (!this.k) {
                    this.k = true;
                    this.g.postDelayed(new Runnable() {
                        public final void run() {
                            gh.this.i;
                            gh.this.a(gf.a());
                        }
                    }, a);
                    Context context = this.d;
                    if (ib.b(p.class, a.a, context, this)) {
                        p.a(context, (IIdentifierCallback) this);
                        p.a(this);
                    } else {
                        p.a(this);
                    }
                }
            } else {
                giVar.a(this.j);
            }
        }
    }

    public final void b(@NonNull gi giVar) {
        synchronized (b) {
            this.f.remove(giVar);
        }
    }

    public final void onReceive(@Nullable Map<String, String> map) {
        synchronized (b) {
            if (map != null) {
                try {
                    if (gl.a(map)) {
                        this.j = new HashMap(map);
                        a(this.j);
                    }
                } finally {
                }
            }
            a(gf.c());
        }
    }

    private void a(@NonNull Map<String, String> map) {
        synchronized (b) {
            a();
            for (gi a2 : this.f.keySet()) {
                a2.a(map);
            }
            this.f.clear();
        }
    }

    public final void onRequestError(@NonNull Reason reason) {
        synchronized (b) {
            a(gf.a(reason));
        }
    }

    /* access modifiers changed from: private */
    public void a(@NonNull String str) {
        synchronized (b) {
            a();
            for (gi a2 : this.f.keySet()) {
                a2.a(str);
            }
            this.f.clear();
        }
    }

    private void a() {
        this.g.removeCallbacksAndMessages(null);
        this.k = false;
    }
}
