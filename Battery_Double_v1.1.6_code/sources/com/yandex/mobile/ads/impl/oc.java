package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ot;

public interface oc<T extends ot> {
    void a(@NonNull Context context, @NonNull T t);
}
