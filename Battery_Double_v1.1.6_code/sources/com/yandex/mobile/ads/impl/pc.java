package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.widget.ImageView.ScaleType;
import com.yandex.mobile.ads.impl.sm.b;
import com.yandex.mobile.ads.impl.sm.c;
import com.yandex.mobile.ads.impl.sm.d;

final class pc extends sm {
    @NonNull
    private final pb a;
    @NonNull
    private final pd b;

    @TargetApi(13)
    pc(@NonNull rt rtVar, @NonNull b bVar, @NonNull pb pbVar, @NonNull pd pdVar) {
        super(rtVar, bVar);
        this.a = pbVar;
        this.b = pdVar;
    }

    public final c a(String str, d dVar, int i, int i2) {
        return super.a(str, dVar, this.b.a(i), i2);
    }

    public final String a(@NonNull String str, int i, int i2, @NonNull ScaleType scaleType) {
        return pb.a(str, scaleType);
    }
}
