package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class lj<V extends View, T> extends lh<V, T> implements li<T> {
    public lj(@NonNull md<V, T> mdVar) {
        super(mdVar);
    }

    public final void c(@NonNull T t) {
        a(t);
    }
}
