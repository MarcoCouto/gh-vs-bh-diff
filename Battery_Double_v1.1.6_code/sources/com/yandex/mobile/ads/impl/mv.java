package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class mv implements mu {
    @NonNull
    private final on a;
    @NonNull
    private final mr b;

    public mv(@NonNull on onVar, @NonNull mr mrVar) {
        this.a = onVar;
        this.b = mrVar;
    }

    public final void a(@NonNull View view, @NonNull String str) {
        this.b.a(new on(this.a.a(), this.a.b(), this.a.c(), str)).onClick(view);
    }
}
