package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class om {
    private int a;
    private int b;
    @Nullable
    private String c;
    @Nullable
    private String d;

    public final int a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }

    @Nullable
    public final String c() {
        return this.c;
    }

    @Nullable
    public final String d() {
        return this.d;
    }

    public final void a(int i) {
        this.a = i;
    }

    public final void b(int i) {
        this.b = i;
    }

    public final void a(@Nullable String str) {
        this.c = str;
    }

    public final void b(@Nullable String str) {
        this.d = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        om omVar = (om) obj;
        if (this.a != omVar.a || this.b != omVar.b) {
            return false;
        }
        if (this.c == null ? omVar.c != null : !this.c.equals(omVar.c)) {
            return false;
        }
        if (this.d != null) {
            return this.d.equals(omVar.d);
        }
        return omVar.d == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.a * 31) + this.b) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return hashCode + i;
    }
}
