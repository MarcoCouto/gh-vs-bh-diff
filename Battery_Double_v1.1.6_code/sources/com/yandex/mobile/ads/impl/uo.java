package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.models.vmap.AdBreak;
import com.yandex.mobile.ads.video.models.vmap.AdSource;
import com.yandex.mobile.ads.video.models.vmap.TimeOffset;
import com.yandex.mobile.ads.video.models.vmap.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class uo {
    @NonNull
    private final un a;
    @NonNull
    private final up b;
    @NonNull
    private final ur c = new ur();
    @NonNull
    private final ut d;
    @NonNull
    private final uu e = new uu();
    @NonNull
    private final uv f = new uv();

    public uo(@NonNull un unVar, @NonNull ut utVar) {
        this.a = unVar;
        this.d = utVar;
        this.b = new up(unVar);
    }

    @Nullable
    static AdBreak a(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        TimeOffset timeOffset;
        un.a(xmlPullParser, "AdBreak");
        String attributeValue = xmlPullParser.getAttributeValue(null, "breakId");
        Long a2 = uu.a(xmlPullParser);
        String attributeValue2 = xmlPullParser.getAttributeValue(null, "timeOffset");
        if (!TextUtils.isEmpty(attributeValue2)) {
            timeOffset = d.a(attributeValue2);
        } else {
            timeOffset = null;
        }
        List a3 = ur.a(xmlPullParser);
        ArrayList arrayList = new ArrayList();
        AdSource adSource = null;
        while (un.b(xmlPullParser)) {
            if (un.a(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if ("AdSource".equals(name)) {
                    adSource = up.a(xmlPullParser);
                } else if ("Extensions".equals(name)) {
                    arrayList.addAll(ut.a(xmlPullParser));
                } else {
                    un.d(xmlPullParser);
                }
            }
        }
        if (adSource == null || timeOffset == null || a3.isEmpty()) {
            return null;
        }
        return d.a(adSource, attributeValue, a2, timeOffset, a3, arrayList);
    }
}
