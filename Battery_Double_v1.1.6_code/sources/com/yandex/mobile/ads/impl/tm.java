package com.yandex.mobile.ads.impl;

public final class tm {
    private final boolean a;
    private final boolean b;

    public tm(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }

    public final boolean a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }
}
