package com.yandex.mobile.ads.impl;

import android.graphics.RectF;
import android.support.annotation.Nullable;

public final class ff {
    @Nullable
    private final RectF a;
    private final int b;

    public ff(int i, @Nullable RectF rectF) {
        this.b = i;
        this.a = rectF;
    }

    public final int a() {
        return this.b;
    }

    @Nullable
    public final RectF b() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ff ffVar = (ff) obj;
        if (this.b != ffVar.b) {
            return false;
        }
        if (this.a != null) {
            return this.a.equals(ffVar.a);
        }
        return ffVar.a == null;
    }

    public final int hashCode() {
        return ((this.a != null ? this.a.hashCode() : 0) * 31) + this.b;
    }
}
