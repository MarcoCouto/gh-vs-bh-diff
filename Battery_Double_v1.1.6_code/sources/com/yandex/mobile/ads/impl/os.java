package com.yandex.mobile.ads.impl;

public class os {
    private String a;
    private boolean b;

    public final void a(String str) {
        this.a = str;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final String a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        os osVar = (os) obj;
        if (this.b != osVar.b) {
            return false;
        }
        if (this.a != null) {
            return this.a.equals(osVar.a);
        }
        return osVar.a == null;
    }

    public int hashCode() {
        return ((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b ? 1 : 0);
    }
}
