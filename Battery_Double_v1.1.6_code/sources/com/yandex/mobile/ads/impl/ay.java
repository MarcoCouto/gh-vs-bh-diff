package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class ay {
    @NonNull
    public static ax a(@NonNull Context context, @NonNull fl flVar, @NonNull cq cqVar, @NonNull az azVar, @NonNull String str) {
        fw a = fv.a().a(context);
        if (a == null || !a.h()) {
            av avVar = new av(context, flVar, cqVar, azVar, str);
            return avVar;
        }
        aw awVar = new aw(context, flVar, cqVar, azVar, str);
        return awVar;
    }
}
