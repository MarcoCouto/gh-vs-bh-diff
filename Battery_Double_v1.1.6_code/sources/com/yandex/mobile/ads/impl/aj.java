package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Map;
import java.util.WeakHashMap;

public final class aj {
    private static final Object a = new Object();
    private static volatile aj b;
    @NonNull
    private final Map<a, Object> c = new WeakHashMap();

    public interface a {
        void a(@NonNull Context context, @NonNull fw fwVar);
    }

    private aj() {
    }

    public static aj a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new aj();
                }
            }
        }
        return b;
    }

    public final void a(@NonNull a aVar) {
        synchronized (a) {
            if (!this.c.containsKey(aVar)) {
                this.c.put(aVar, null);
            }
        }
    }

    public final void a(@NonNull Context context, @NonNull fw fwVar) {
        synchronized (a) {
            fv.a().a(context, fwVar);
            for (a a2 : this.c.keySet()) {
                a2.a(context, fwVar);
            }
        }
    }
}
