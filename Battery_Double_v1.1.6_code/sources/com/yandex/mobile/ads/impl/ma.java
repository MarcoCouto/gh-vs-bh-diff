package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;

public final class ma extends md<em, oo> {
    public final /* bridge */ /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        return true;
    }

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        super.a((em) view);
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        em emVar = (em) view;
        oo ooVar = (oo) obj;
        String a = ooVar.a();
        if (!TextUtils.isEmpty(a)) {
            emVar.setAspectRatio(ooVar.b());
            emVar.c(a);
        }
    }

    public ma(@NonNull em emVar) {
        super(emVar);
    }

    public final void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        em emVar = (em) a();
        if (emVar != null) {
            mjVar.a(ojVar, (View) emVar);
            mjVar.a(ojVar, (mq) new ms(emVar));
        }
    }
}
