package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class pv implements pq<ox> {
    @NonNull
    private final py a = new py(this.b);
    @NonNull
    private final pk b;

    public pv(@NonNull pk pkVar) {
        this.b = pkVar;
    }

    @NonNull
    public final /* synthetic */ ot a(@NonNull JSONObject jSONObject) throws JSONException, z {
        return new ox(pi.a(jSONObject, "type"), this.b.a(jSONObject, "fallbackUrl"), this.a.a(jSONObject.optJSONArray("preferredPackages")));
    }
}
