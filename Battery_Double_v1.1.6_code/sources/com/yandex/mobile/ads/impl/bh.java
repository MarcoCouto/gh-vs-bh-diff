package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.lang.ref.WeakReference;

public final class bh {
    @NonNull
    private final dq a;
    @NonNull
    private final WeakReference<aa> b;

    public bh(@NonNull aa aaVar, @NonNull bl blVar) {
        this.b = new WeakReference<>(aaVar);
        this.a = new dp(blVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        aa aaVar = (aa) this.b.get();
        if (aaVar != null && !aaVar.l()) {
            aaVar.c(this.a);
        }
    }
}
