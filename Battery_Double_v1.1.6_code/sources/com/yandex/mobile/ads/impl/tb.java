package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.rewarded.a;

public final class tb {
    @NonNull
    private final a a;

    public tb(@NonNull a aVar) {
        this.a = aVar;
    }

    @Nullable
    public final ta a(@NonNull Context context, @NonNull fl flVar, @Nullable w<String> wVar) {
        bp o = wVar != null ? wVar.o() : null;
        if (o == null) {
            return null;
        }
        if (o.c()) {
            bq b = o.b();
            if (b != null) {
                return new tc(context, flVar, b);
            }
            return null;
        }
        bo a2 = o.a();
        if (a2 != null) {
            return new sz(a2, this.a);
        }
        return null;
    }
}
