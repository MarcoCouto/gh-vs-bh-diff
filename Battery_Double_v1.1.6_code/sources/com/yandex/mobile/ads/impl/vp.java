package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class vp {
    @NonNull
    private final Context a;
    @NonNull
    private final vb b;
    private int c;

    public vp(@NonNull Context context, @NonNull vb vbVar) {
        this.a = context.getApplicationContext();
        this.b = vbVar;
    }

    public final void a(@NonNull Context context, @NonNull List<VideoAd> list, @NonNull RequestListener<List<VideoAd>> requestListener) {
        this.c++;
        if (this.c <= 5) {
            new vq(this.a, this.b).a(context, list, requestListener);
        } else {
            requestListener.onFailure(VideoAdError.createInternalError("Maximum count of VAST wrapper requests exceeded."));
        }
    }
}
