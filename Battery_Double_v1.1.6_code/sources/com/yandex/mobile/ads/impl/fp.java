package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.p;
import java.util.WeakHashMap;

public final class fp implements ft {
    private static final long a = ((long) hn.b);
    /* access modifiers changed from: private */
    public static final Object b = new Object();
    private static volatile fp c;
    @NonNull
    private final fo d = new fo();
    /* access modifiers changed from: private */
    @NonNull
    public final gf e = new gf();
    @NonNull
    private final Handler f = new Handler(Looper.getMainLooper());
    @NonNull
    private final WeakHashMap<fu, Object> g = new WeakHashMap<>();
    private boolean h;

    private class a implements com.yandex.mobile.ads.impl.fo.a {
        private a() {
        }

        /* synthetic */ a(fp fpVar, byte b) {
            this();
        }

        public final void a(@Nullable String str) {
            synchronized (fp.b) {
                fp.this.a(str);
            }
        }

        public final void a() {
            synchronized (fp.b) {
                fp.this.c();
            }
        }
    }

    private fp() {
    }

    @NonNull
    public static fp a() {
        if (c == null) {
            synchronized (b) {
                if (c == null) {
                    c = new fp();
                }
            }
        }
        return c;
    }

    public final void a(@NonNull fu fuVar) {
        synchronized (b) {
            this.g.remove(fuVar);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:7|8|9|10|11|12|13) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x002f */
    public final void b(@NonNull fu fuVar) {
        synchronized (b) {
            this.g.put(fuVar, null);
            if (!this.h) {
                this.h = true;
                this.f.postDelayed(new Runnable() {
                    public final void run() {
                        fp.this.e;
                        gf.a();
                        fp.this.c();
                    }
                }, a);
                a aVar = new a(this, 0);
                p.guc(new b(aVar), true);
                aVar.a();
                gf.b();
                c();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        synchronized (b) {
            a((String) null);
        }
    }

    /* access modifiers changed from: private */
    public void a(@Nullable String str) {
        synchronized (b) {
            d();
            for (fu a2 : this.g.keySet()) {
                a2.a(str);
            }
            this.g.clear();
        }
    }

    private void d() {
        this.f.removeCallbacksAndMessages(null);
        this.h = false;
    }
}
