package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.bb;
import com.yandex.mobile.ads.nativeads.c;
import com.yandex.mobile.ads.nativeads.j;
import com.yandex.mobile.ads.nativeads.w;

final class nk implements nl {
    nk() {
    }

    public final NativeGenericAd a(@NonNull Context context, @NonNull oq oqVar, @NonNull w wVar, @NonNull j jVar, @NonNull c cVar) {
        bb bbVar = new bb(context, oqVar, wVar, jVar, cVar);
        return bbVar;
    }
}
