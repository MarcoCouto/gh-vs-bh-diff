package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

final class ix implements iy {
    ix() {
    }

    @NonNull
    public final iv a(@NonNull ep epVar, @NonNull el elVar, @NonNull eh ehVar, @NonNull jf jfVar) {
        it itVar = new it(epVar);
        itVar.a(elVar);
        return itVar;
    }

    @NonNull
    public final iu a(@NonNull ep epVar, @NonNull el elVar, @NonNull jg jgVar, @NonNull eo eoVar, @NonNull jf jfVar) {
        is isVar = new is(epVar);
        isVar.a(elVar);
        return isVar;
    }

    @NonNull
    public final iv a(@NonNull ep epVar, @NonNull el elVar) {
        it itVar = new it(epVar);
        itVar.a(elVar);
        return itVar;
    }
}
