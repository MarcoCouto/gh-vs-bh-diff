package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.concurrent.ThreadFactory;

public final class dx implements ThreadFactory {
    private final String a;

    public dx(String str) {
        this.a = str;
    }

    public final Thread newThread(@NonNull Runnable runnable) {
        return new Thread(runnable, this.a);
    }
}
