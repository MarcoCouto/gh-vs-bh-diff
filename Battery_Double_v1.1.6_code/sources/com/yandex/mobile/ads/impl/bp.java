package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class bp implements Parcelable {
    public static final Creator<bp> CREATOR = new Creator<bp>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bp[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new bp(parcel);
        }
    };
    private final boolean a;
    @Nullable
    private final bo b;
    @Nullable
    private final bq c;

    public static class a {
        /* access modifiers changed from: private */
        public boolean a;
        /* access modifiers changed from: private */
        @Nullable
        public bo b;
        /* access modifiers changed from: private */
        @Nullable
        public bq c;

        @NonNull
        public final bp a() {
            return new bp(this, 0);
        }

        @NonNull
        public final a a(boolean z) {
            this.a = z;
            return this;
        }

        @NonNull
        public final a a(@Nullable bo boVar) {
            this.b = boVar;
            return this;
        }

        @NonNull
        public final a a(@Nullable bq bqVar) {
            this.c = bqVar;
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    /* synthetic */ bp(a aVar, byte b2) {
        this(aVar);
    }

    private bp(@NonNull a aVar) {
        this.b = aVar.b;
        this.c = aVar.c;
        this.a = aVar.a;
    }

    @Nullable
    public final bo a() {
        return this.b;
    }

    @Nullable
    public final bq b() {
        return this.c;
    }

    public final boolean c() {
        return this.a;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeByte(this.a ? (byte) 1 : 0);
        parcel.writeParcelable(this.b, i);
        parcel.writeParcelable(this.c, i);
    }

    protected bp(@NonNull Parcel parcel) {
        this.a = parcel.readByte() != 0;
        this.b = (bo) parcel.readParcelable(bo.class.getClassLoader());
        this.c = (bq) parcel.readParcelable(bq.class.getClassLoader());
    }
}
