package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;

public final class st implements sk {
    @NonNull
    private final sk a;
    @NonNull
    private final hy b = new hy();
    @Nullable
    private final Context c;

    public st(@Nullable Context context, @NonNull sk skVar) {
        this.a = skVar;
        this.c = context != null ? context.getApplicationContext() : null;
    }

    public final HttpResponse a(rs<?> rsVar, Map<String, String> map) throws IOException, sf {
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.put(rk.USER_AGENT.a(), hy.a(this.c));
        return this.a.a(rsVar, hashMap);
    }
}
