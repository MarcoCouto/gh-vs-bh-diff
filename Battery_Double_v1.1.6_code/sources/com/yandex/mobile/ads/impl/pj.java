package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.List;
import org.json.JSONObject;

public final class pj {
    static boolean a(@NonNull List<oj> list) {
        HashSet hashSet = new HashSet();
        for (oj a : list) {
            hashSet.add(a.a());
        }
        return list.size() > hashSet.size();
    }

    public static boolean a(JSONObject jSONObject, String... strArr) {
        for (String has : strArr) {
            if (!jSONObject.has(has)) {
                return false;
            }
        }
        return true;
    }
}
