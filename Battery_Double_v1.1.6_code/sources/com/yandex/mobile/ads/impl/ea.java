package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public final class ea {
    @NonNull
    private final Map<String, Object> a;

    public ea(@NonNull Map<String, Object> map) {
        this.a = map;
    }

    public final void a(@NonNull String str, @Nullable Object obj) {
        if (obj == null) {
            a(str);
        } else {
            this.a.put(str, obj);
        }
    }

    public final void b(@NonNull String str, @Nullable Object obj) {
        if (obj != null) {
            this.a.put(str, obj);
        }
    }

    public final void a(@NonNull String str) {
        this.a.put(str, "undefined");
    }

    public final void a(@NonNull Map<String, Object> map) {
        this.a.putAll(map);
    }

    @NonNull
    public final Map<String, Object> a() {
        return this.a;
    }
}
