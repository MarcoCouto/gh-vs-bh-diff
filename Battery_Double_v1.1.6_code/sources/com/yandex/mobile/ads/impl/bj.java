package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public final class bj {
    @NonNull
    static Map<String, String> a(@Nullable w wVar) {
        HashMap hashMap = new HashMap();
        if (wVar != null) {
            bp o = wVar.o();
            if (o != null) {
                hashMap.put("rewarding_side", o.c() ? "server_side" : "client_side");
            }
        }
        return hashMap;
    }
}
