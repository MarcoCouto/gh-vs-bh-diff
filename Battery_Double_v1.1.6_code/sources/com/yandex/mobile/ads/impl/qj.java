package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qj implements qh<oo> {
    @NonNull
    private final bz a = new bz();

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, z {
        String a2 = bz.a(pi.a(jSONObject, String.HTML));
        if (!TextUtils.isEmpty(a2)) {
            float f = (float) jSONObject.getDouble("aspectRatio");
            if (f == 0.0f) {
                f = 1.7777778f;
            }
            return new oo(a2, f);
        }
        throw new z("Native Ad json has attribute with broken base64 encoding");
    }
}
