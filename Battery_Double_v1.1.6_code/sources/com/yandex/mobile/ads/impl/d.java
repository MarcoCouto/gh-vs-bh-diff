package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.impl.ip.a;

@SuppressLint({"ViewConstructor"})
public final class d extends ip {
    @VisibleForTesting
    final int a;
    @VisibleForTesting
    int b;
    @NonNull
    private final al g;
    @Nullable
    private al h;
    private boolean i = true;

    d(@NonNull Context context, @NonNull w wVar, @NonNull al alVar) {
        super(context, wVar);
        this.g = alVar;
        if (k()) {
            this.a = alVar.b(context);
            this.b = alVar.a(context);
        } else {
            this.a = wVar.q() == 0 ? alVar.b(context) : wVar.q();
            this.b = wVar.r();
        }
        a(this.a, this.b);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.i) {
            a(this.a, this.b);
            boolean a2 = jn.a(getContext(), this.h, this.g);
            if (this.e != null && a2) {
                this.e.a(this, j());
            }
            if (this.e != null) {
                if (a2) {
                    this.e.onAdLoaded();
                } else {
                    this.e.onAdFailedToLoad(u.c);
                }
            }
            this.i = false;
        }
    }

    private void a(int i2, int i3) {
        this.h = new al(i2, i3, this.g.c());
    }

    /* access modifiers changed from: protected */
    public final String b() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        if (this.f.w()) {
            str = eq.a(this.a);
        } else {
            str = "";
        }
        sb.append(str);
        Context context = getContext();
        int b2 = this.g.b(context);
        int a2 = this.g.a(context);
        if (k()) {
            str2 = eq.a(b2, a2);
        } else {
            str2 = "";
        }
        sb.append(str2);
        sb.append(super.b());
        return sb.toString();
    }

    @Nullable
    public final al c() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"AddJavascriptInterface"})
    public final void a(@NonNull Context context) {
        addJavascriptInterface(new a(context), "AdPerformActionsJSI");
    }

    @VisibleForTesting
    private boolean k() {
        Context context = getContext();
        return i() && this.f.q() == 0 && this.f.r() == 0 && this.g.b(context) > 0 && this.g.a(context) > 0;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, String str) {
        if (this.f.r() != 0) {
            i2 = this.f.r();
        }
        this.b = i2;
        super.a(this.b, str);
    }
}
