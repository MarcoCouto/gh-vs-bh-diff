package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.c;
import com.yandex.mobile.ads.nativeads.j;
import com.yandex.mobile.ads.nativeads.w;

public interface nl {
    NativeGenericAd a(@NonNull Context context, @NonNull oq oqVar, @NonNull w wVar, @NonNull j jVar, @NonNull c cVar);
}
