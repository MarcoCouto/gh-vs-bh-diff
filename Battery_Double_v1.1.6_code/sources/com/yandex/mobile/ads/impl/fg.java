package com.yandex.mobile.ads.impl;

import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class fg implements fh {
    @Nullable
    private final RectF a;
    private final int b;

    public fg(int i, @Nullable RectF rectF) {
        this.b = i;
        this.a = rectF;
    }

    @NonNull
    public final String a() {
        String str;
        String str2 = "exposure:{exposedPercentage:%s,visibleRectangle:%s,occlusionRectangles:[]}";
        Object[] objArr = new Object[2];
        objArr[0] = Integer.valueOf(this.b);
        RectF rectF = this.a;
        if (rectF != null) {
            str = String.format("{x:%s,y:%s,width:%s,height:%s}", new Object[]{Float.valueOf(rectF.left), Float.valueOf(rectF.top), Float.valueOf(rectF.width()), Float.valueOf(rectF.height())});
        } else {
            str = null;
        }
        objArr[1] = str;
        return String.format(str2, objArr);
    }
}
