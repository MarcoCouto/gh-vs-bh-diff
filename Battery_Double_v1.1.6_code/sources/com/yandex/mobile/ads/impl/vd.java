package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.video.BlocksInfoRequest;
import com.yandex.mobile.ads.video.models.blocksinfo.BlocksInfo;
import java.util.HashMap;

public final class vd implements hs<BlocksInfoRequest, BlocksInfo> {
    public final /* synthetic */ hr a(@Nullable ru ruVar, int i, @NonNull Object obj) {
        BlocksInfoRequest blocksInfoRequest = (BlocksInfoRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", blocksInfoRequest.getPartnerId());
        hashMap.put("category_id", blocksInfoRequest.getCategoryId());
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new hr(b.BLOCKS_INFO_RESPONSE, hashMap);
    }

    public final /* synthetic */ hr a(Object obj) {
        BlocksInfoRequest blocksInfoRequest = (BlocksInfoRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", blocksInfoRequest.getPartnerId());
        hashMap.put("category_id", blocksInfoRequest.getCategoryId());
        return new hr(b.BLOCKS_INFO_REQUEST, hashMap);
    }
}
