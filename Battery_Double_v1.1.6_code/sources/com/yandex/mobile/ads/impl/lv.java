package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.av.a;

public final class lv extends md<TextView, ok> {
    public final /* synthetic */ void a(@NonNull View view) {
        TextView textView = (TextView) view;
        textView.setText("");
        super.a(textView);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        ok okVar = (ok) obj;
        if (a.TEXT == okVar.b()) {
            return textView.getText().toString().equals(okVar.a());
        }
        return true;
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        ok okVar = (ok) obj;
        if (a.TEXT == okVar.b()) {
            textView.setText(okVar.a());
        }
    }

    public lv(@NonNull TextView textView) {
        super(textView);
    }
}
