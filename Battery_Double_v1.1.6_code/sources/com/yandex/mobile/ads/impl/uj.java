package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.sv.a;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class uj extends sw<VmapRequestConfiguration, Vmap> {
    @NonNull
    private final uw a = new uw();
    @NonNull
    private final bt b = new bt();

    public uj(@NonNull Context context, @NonNull String str, @NonNull a<Vmap> aVar, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull hs<VmapRequestConfiguration, Vmap> hsVar) {
        super(context, 0, str, aVar, vmapRequestConfiguration, hsVar);
        new Object[1][0] = str;
    }

    /* access modifiers changed from: protected */
    public final ru<Vmap> a(@NonNull rr rrVar, int i) {
        String a2 = bt.a(rrVar);
        if (!TextUtils.isEmpty(a2)) {
            try {
                Vmap a3 = this.a.a(a2);
                if (a3 != null) {
                    return ru.a(a3, null);
                }
            } catch (Exception e) {
                return ru.a(new to((Throwable) e));
            }
        }
        return ru.a(new to("Can't parse VMAP response"));
    }
}
