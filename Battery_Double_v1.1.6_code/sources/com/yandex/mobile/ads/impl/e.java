package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.VideoController;
import com.yandex.mobile.ads.a;

public abstract class e extends RelativeLayout {
    @NonNull
    private final a a;
    @NonNull
    private final f b;
    @Nullable
    private AdSize c;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract a a(@NonNull Context context, @NonNull c cVar);

    public void pause() {
    }

    public void resume() {
    }

    public e(@NonNull Context context) {
        this(context, null);
    }

    public e(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public e(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        f fVar;
        super(context, attributeSet, i);
        c cVar = new c(context);
        this.a = a(context, cVar);
        cVar.a(this.a.r());
        a aVar = this.a;
        if (isInEditMode()) {
            fVar = new b();
        } else {
            fVar = new a(aVar);
        }
        this.b = fVar;
        this.b.a(context, this);
    }

    public void loadAd(AdRequest adRequest) {
        this.a.a(adRequest);
    }

    public void setAdEventListener(AdEventListener adEventListener) {
        this.a.a(adEventListener);
    }

    @NonNull
    public VideoController getVideoController() {
        return this.a.g();
    }

    public AdEventListener getAdEventListener() {
        return this.a.h();
    }

    public void setAutoRefreshEnabled(boolean z) {
        this.a.a(z);
    }

    public void setAdSize(@Nullable AdSize adSize) {
        this.c = adSize;
        this.a.b(a.a(adSize));
    }

    @Nullable
    public AdSize getAdSize() {
        return this.c;
    }

    public void setBlockId(String str) {
        this.a.a_(str);
    }

    public String getBlockId() {
        return this.a.q();
    }

    public void destroy() {
        if (!dr.a((ac) this.a)) {
            this.a.e();
        }
    }

    public void shouldOpenLinksInApp(boolean z) {
        this.a.a_(z);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        new StringBuilder("onAttachedToWindow(), clazz = ").append(getClass());
        f fVar = this.b;
        getContext();
        fVar.a();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        new StringBuilder("onDetachedFromWindow(), clazz = ").append(getClass());
        f fVar = this.b;
        getContext();
        fVar.b();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        int i2 = 0;
        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(getVisibility())};
        if (!(i == 0 && getVisibility() == 0)) {
            i2 = 8;
        }
        a(i2);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        boolean z = false;
        Object[] objArr = {view, Integer.valueOf(i)};
        fw a2 = fv.a().a(getContext());
        if (a2 != null && a2.p()) {
            z = true;
        }
        if (!z || this == view) {
            a(i);
        }
    }

    private void a(int i) {
        if (!dr.a((ac) this.a)) {
            this.b.a(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (!dr.a((ac) this.a)) {
            setVisibility(this.a.c() ? 0 : 8);
        }
        new Object[1][0] = configuration.toString();
    }
}
