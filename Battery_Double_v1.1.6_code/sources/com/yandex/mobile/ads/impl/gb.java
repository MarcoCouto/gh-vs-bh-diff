package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.util.concurrent.Callable;

public final class gb {
    @NonNull
    private final ic a = new ic();
    @NonNull
    private final gc b = new gc();

    @NonNull
    public final ga a(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        gc gcVar = this.b;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        Display display = (Display) gd.a(new Callable<Display>(windowManager) {
            final /* synthetic */ WindowManager a;

            {
                this.a = r2;
            }

            @Nullable
            public final /* synthetic */ Object call() throws Exception {
                return this.a.getDefaultDisplay();
            }
        }, windowManager, "WindowManager");
        boolean z = false;
        Object point = new Point(0, 0);
        Object a2 = gd.a(new Callable<Point>(display) {
            final /* synthetic */ Display a;

            {
                this.a = r2;
            }

            @NonNull
            public final /* synthetic */ Object call() throws Exception {
                return gc.a(this.a);
            }
        }, display, "Display");
        if (a2 == null) {
            a2 = point;
        }
        Point point2 = (Point) a2;
        int i = point2.x;
        int i2 = point2.y;
        float f = displayMetrics.density;
        float f2 = (float) i;
        float f3 = (float) i2;
        float min = Math.min(f2 / f, f3 / f);
        float f4 = f * 160.0f;
        float f5 = f2 / f4;
        float f6 = f3 / f4;
        double sqrt = Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
        if (sqrt >= 15.0d && !ic.a(context, "android.hardware.touchscreen")) {
            z = true;
        }
        if (z) {
            return ga.TV;
        }
        if (sqrt >= 7.0d || min >= 600.0f) {
            return ga.TABLET;
        }
        return ga.PHONE;
    }
}
