package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.rewarded.Reward;
import com.yandex.mobile.ads.rewarded.a;
import com.yandex.mobile.ads.rewarded.c;

public final class sz implements ta {
    @NonNull
    private final c a;
    @NonNull
    private final a b;

    public sz(@NonNull bo boVar, @NonNull a aVar) {
        this.b = aVar;
        this.a = new c(boVar.a(), boVar.b());
    }

    public final void a() {
        this.b.a((Reward) this.a);
    }
}
