package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.r;

final class od {
    @NonNull
    private final fl a;
    @NonNull
    private final co b;
    @NonNull
    private final oa c;
    @NonNull
    private final r d;
    @NonNull
    private final ag e;

    od(@NonNull fl flVar, @NonNull co coVar, @NonNull oa oaVar, @NonNull ag agVar, @NonNull r rVar) {
        this.a = flVar;
        this.b = coVar;
        this.c = oaVar;
        this.e = agVar;
        this.d = rVar;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d  */
    @Nullable
    public final oc a(@NonNull Context context, @NonNull ot otVar) {
        char c2;
        String a2 = otVar.a();
        int hashCode = a2.hashCode();
        if (hashCode == -342500282) {
            if (a2.equals("shortcut")) {
                c2 = 3;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == -191501435) {
            if (a2.equals("feedback")) {
                c2 = 2;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 94756344) {
            if (a2.equals("close")) {
                c2 = 0;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 629233382 && a2.equals("deeplink")) {
            c2 = 1;
            switch (c2) {
                case 0:
                    return new of(this.b, this.d);
                case 1:
                    return new og(new qm(context, this.b, this.c));
                case 2:
                    return new oh(new qr(this.a, this.b, this.e, this.d));
                case 3:
                    return new oi(new qu(context, this.b, this.e));
                default:
                    return null;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }
}
