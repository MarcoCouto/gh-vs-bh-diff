package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sv.a;

public abstract class sw<R, T> extends sv<T> {
    @NonNull
    private final R a;
    @NonNull
    private final hs<R, T> b;
    @NonNull
    private final hp c;

    /* access modifiers changed from: protected */
    public abstract ru<T> a(@NonNull rr rrVar, int i);

    public sw(@NonNull Context context, int i, @NonNull String str, @NonNull a<T> aVar, @NonNull R r, @NonNull hs<R, T> hsVar) {
        super(i, str, aVar);
        this.a = r;
        this.b = hsVar;
        this.c = hp.a(context);
        this.c.a(this.b.a(this.a));
    }

    /* access modifiers changed from: protected */
    public final ru<T> a(@NonNull rr rrVar) {
        int i = rrVar.a;
        ru<T> a2 = a(rrVar, i);
        a(a2, i);
        return a2;
    }

    /* access modifiers changed from: protected */
    public sf a(sf sfVar) {
        a(null, sfVar.a != null ? sfVar.a.a : -1);
        return super.a(sfVar);
    }

    private void a(@Nullable ru<T> ruVar, int i) {
        this.c.a(this.b.a(ruVar, i, this.a));
    }
}
