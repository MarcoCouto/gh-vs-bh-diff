package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;
import java.util.Map;

public final class cq {
    @NonNull
    private final hp a;
    @NonNull
    private final fl b;
    @NonNull
    private final ct c = new ct();
    @Nullable
    private w d;
    @Nullable
    private a e;

    public cq(@NonNull Context context, @NonNull fl flVar) {
        this.b = flVar;
        this.a = hp.a(context);
    }

    public final void a(@NonNull b bVar) {
        b(bVar, a());
    }

    public final void a(@NonNull b bVar, @NonNull Map<String, Object> map) {
        Map a2 = a();
        a2.putAll(map);
        b(bVar, a2);
    }

    public final void a(@NonNull am amVar) {
        b(amVar.c(), c(amVar));
    }

    public final void b(@NonNull am amVar) {
        b(amVar.e(), c(amVar));
    }

    private void b(@NonNull b bVar, @NonNull Map<String, Object> map) {
        this.a.a(new hr(bVar, map));
    }

    @NonNull
    private Map<String, Object> c(@NonNull am amVar) {
        Map<String, Object> a2 = a();
        a2.put(IronSourceConstants.EVENTS_ERROR_REASON, amVar.b().a());
        String a3 = amVar.a();
        if (!TextUtils.isEmpty(a3)) {
            a2.put("asset_name", a3);
        }
        return a2;
    }

    @NonNull
    private Map<String, Object> a() {
        ea eaVar = new ea(new HashMap());
        eaVar.a("adapter", "Yandex");
        if (this.d != null) {
            eaVar.a("block_id", this.d.d());
            eaVar.a("ad_type_format", this.d.b());
            eaVar.a("product_type", this.d.c());
            eaVar.a("ad_source", this.d.k());
            com.yandex.mobile.ads.b a2 = this.d.a();
            if (a2 != null) {
                eaVar.a(AppEventsConstants.EVENT_PARAM_AD_TYPE, a2.a());
            } else {
                eaVar.a(AppEventsConstants.EVENT_PARAM_AD_TYPE);
            }
        } else {
            eaVar.a("block_id");
            eaVar.a("ad_type_format");
            eaVar.a("product_type");
            eaVar.a("ad_source");
        }
        eaVar.a(ct.a(this.b.c()));
        if (this.e != null) {
            eaVar.a(this.e.a());
        }
        return eaVar.a();
    }

    public final void a(@NonNull w wVar) {
        this.d = wVar;
    }

    public final void a(@NonNull a aVar) {
        this.e = aVar;
    }
}
