package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;

enum ey {
    AD_VIDEO_COMPLETE("advideocomplete"),
    IMPRESSION_TRACKING_START("impressionTrackingStart"),
    IMPRESSION_TRACKING_SUCCESS("impressionTrackingSuccess"),
    CLOSE("close"),
    OPEN(MraidJsMethods.OPEN),
    REWARDED_AD_COMPLETE("rewardedAdComplete"),
    USE_CUSTOM_CLOSE("usecustomclose"),
    UNSPECIFIED("");
    
    @NonNull
    private final String i;

    private ey(String str) {
        this.i = str;
    }

    static ey a(@NonNull String str) {
        ey[] values;
        for (ey eyVar : values()) {
            if (eyVar.i.equals(str)) {
                return eyVar;
            }
        }
        return UNSPECIFIED;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String a() {
        return this.i;
    }
}
