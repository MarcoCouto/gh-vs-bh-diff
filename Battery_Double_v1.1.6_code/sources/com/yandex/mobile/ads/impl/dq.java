package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface dq {

    public enum a {
        ;
        
        public static final int a = 1;
        public static final int b = 2;

        static {
            c = new int[]{a, b};
        }
    }

    @NonNull
    int a();

    @NonNull
    String a(@NonNull Context context, @NonNull fl flVar);

    @Nullable
    String a(@NonNull fl flVar);
}
