package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;

public interface kd {
    @NonNull
    View a(@NonNull View view, @NonNull w<String> wVar);

    void a();

    void a(@NonNull Context context, @NonNull n nVar, @NonNull al alVar);

    void a(@NonNull RelativeLayout relativeLayout);

    void a(boolean z);

    void b();

    boolean c();
}
