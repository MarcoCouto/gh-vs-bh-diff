package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class jl implements Runnable {
    private final int a;
    @Nullable
    private final String b;
    @NonNull
    private final jj c;

    jl(int i, @Nullable String str, @NonNull jj jjVar) {
        this.a = i;
        this.b = str;
        this.c = jjVar;
    }

    public final void run() {
        this.c.b(this.a, this.b);
    }
}
