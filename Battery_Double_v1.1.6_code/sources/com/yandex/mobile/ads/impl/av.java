package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

final class av implements ax {
    @NonNull
    private final Context a;
    /* access modifiers changed from: private */
    @NonNull
    public final b b = new b(Looper.getMainLooper());
    @NonNull
    private final az c;
    /* access modifiers changed from: private */
    @NonNull
    public final String d;
    /* access modifiers changed from: private */
    @NonNull
    public final List<a> e;
    /* access modifiers changed from: private */
    @NonNull
    public final List<a> f;
    @NonNull
    private final cq g;
    @NonNull
    private final de h;
    @Nullable
    private at i;
    private boolean j;
    private int k;
    private boolean l;
    private boolean m;

    @VisibleForTesting
    static class a {
        String a;
        long b;
        int c;

        a(String str, long j, int i) {
            this.a = str;
            this.b = j;
            this.c = i;
        }
    }

    private static class b extends Handler {
        b(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Pair pair = (Pair) message.obj;
                    av avVar = (av) ((WeakReference) pair.first).get();
                    if (avVar != null) {
                        a aVar = (a) pair.second;
                        new Object[1][0] = avVar.d;
                        avVar.e.remove(aVar);
                        am a = avVar.a(aVar);
                        avVar.b(aVar, a);
                        if (av.c(a)) {
                            avVar.f.remove(aVar);
                            if (avVar.f.isEmpty()) {
                                avVar.a(a.c());
                                return;
                            }
                        } else {
                            avVar.a();
                        }
                    }
                    return;
                case 2:
                    av avVar2 = (av) ((WeakReference) message.obj).get();
                    if (avVar2 != null) {
                        Object[] objArr = {Integer.valueOf(avVar2.f.size()), avVar2.d};
                        int size = avVar2.f.size();
                        for (int i = 0; i < size; i++) {
                            a aVar2 = (a) avVar2.f.get(i);
                            if (!avVar2.e.contains(aVar2)) {
                                am a2 = avVar2.a(aVar2);
                                if (av.c(a2)) {
                                    avVar2.b.sendMessageDelayed(Message.obtain(avVar2.b, 1, new Pair(new WeakReference(avVar2), aVar2)), aVar2.b);
                                    avVar2.e.add(aVar2);
                                    avVar2.b(a2.d());
                                } else {
                                    avVar2.a(a2);
                                }
                            }
                        }
                        if (avVar2.d()) {
                            avVar2.b.sendMessageDelayed(Message.obtain(avVar2.b, 2, new WeakReference(avVar2)), 300);
                            break;
                        }
                    }
                    break;
            }
        }
    }

    av(@NonNull Context context, @NonNull fl flVar, @NonNull cq cqVar, @NonNull az azVar, @NonNull String str) {
        this.a = context;
        this.c = azVar;
        this.g = cqVar;
        this.d = str;
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.h = new de(context, flVar);
    }

    public final synchronized void a(@NonNull w wVar, @NonNull List<bn> list) {
        new StringBuilder("updateNotices(), clazz = ").append(this.d);
        this.g.a(wVar);
        this.f.clear();
        this.k = 0;
        this.j = false;
        this.l = false;
        this.m = false;
        b();
        a(list);
    }

    @VisibleForTesting
    private synchronized void a(List<bn> list) {
        for (bn bnVar : list) {
            this.f.add(new a(bnVar.b(), bnVar.a(), bnVar.c()));
        }
    }

    public final synchronized void a() {
        new StringBuilder("startTrackingIfNeeded(), clazz = ").append(this.d);
        if (ag.a().a(this.a)) {
            if (!hz.a(this.f) && d()) {
                this.b.sendMessage(Message.obtain(this.b, 2, new WeakReference(this)));
            }
        }
    }

    public final synchronized void b() {
        new StringBuilder("stopTracking(), clazz = ").append(this.d);
        this.b.removeMessages(2);
        this.b.removeMessages(1);
        this.e.clear();
    }

    /* access modifiers changed from: private */
    public synchronized boolean d() {
        return this.f.size() > this.e.size();
    }

    public final synchronized void c() {
        Object[] objArr = {Integer.valueOf(this.f.size()), this.d};
        b();
        com.yandex.mobile.ads.impl.hr.b bVar = com.yandex.mobile.ads.impl.hr.b.IMPRESSION_TRACKING_SUCCESS;
        ArrayList arrayList = new ArrayList();
        this.m = false;
        for (a aVar : this.f) {
            am a2 = a(aVar);
            a(aVar, a2);
            if (c(a2)) {
                arrayList.add(aVar);
                bVar = a2.c();
                b(a2.d());
            }
        }
        if (!arrayList.isEmpty()) {
            this.f.removeAll(arrayList);
            if (this.f.isEmpty()) {
                a(bVar);
            }
        }
        a();
    }

    private synchronized void a(@NonNull a aVar, @NonNull am amVar) {
        b(aVar, amVar);
        if (!this.m && !c(amVar)) {
            this.g.b(amVar);
            this.m = true;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(@NonNull a aVar, @NonNull am amVar) {
        if (c(amVar)) {
            this.h.a(aVar.a);
        } else {
            a(amVar);
        }
    }

    /* access modifiers changed from: private */
    public static boolean c(@NonNull am amVar) {
        return amVar.b() == com.yandex.mobile.ads.impl.am.a.SUCCESS;
    }

    /* access modifiers changed from: private */
    @NonNull
    public am a(@NonNull a aVar) {
        am a2 = this.c.a(aVar.c);
        new Object[1][0] = a2.b().a();
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        return;
     */
    public final synchronized void a(@NonNull Intent intent, boolean z) {
        Object[] objArr = {intent, Boolean.valueOf(z), this.d};
        String action = intent.getAction();
        char c2 = 65535;
        int hashCode = action.hashCode();
        if (hashCode != -2128145023) {
            if (hashCode != -1454123155) {
                if (hashCode == 823795052) {
                    if (action.equals("android.intent.action.USER_PRESENT")) {
                        c2 = 2;
                    }
                }
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
                c2 = 1;
            }
        } else if (action.equals("android.intent.action.SCREEN_OFF")) {
            c2 = 0;
        }
        switch (c2) {
            case 0:
                b();
                return;
            case 1:
            case 2:
                if (z) {
                    a();
                    break;
                }
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(com.yandex.mobile.ads.impl.hr.b bVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("failure_tracked", Boolean.valueOf(this.j));
        this.g.a(bVar, hashMap);
        if (this.i != null) {
            this.i.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(am amVar) {
        this.k++;
        if (this.k == 20) {
            this.g.a(amVar);
            this.j = true;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void b(@NonNull com.yandex.mobile.ads.impl.hr.b bVar) {
        if (!this.l) {
            this.g.a(bVar);
            Cif.a("Ad binding successful", new Object[0]);
            this.l = true;
        }
    }

    public final void a(@NonNull at atVar) {
        this.i = atVar;
    }
}
