package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class rt {
    private AtomicInteger a;
    private final Map<String, Queue<rs<?>>> b;
    private final Set<rs<?>> c;
    private final PriorityBlockingQueue<rs<?>> d;
    private final PriorityBlockingQueue<rs<?>> e;
    private final rl f;
    private final rp g;
    private final rv h;
    private rq[] i;
    private rm j;
    private List<Object> k;

    public interface a {
        boolean a(rs<?> rsVar);
    }

    private rt(rl rlVar, rp rpVar, int i2, rv rvVar) {
        this.a = new AtomicInteger();
        this.b = new HashMap();
        this.c = new HashSet();
        this.d = new PriorityBlockingQueue<>();
        this.e = new PriorityBlockingQueue<>();
        this.k = new ArrayList();
        this.f = rlVar;
        this.g = rpVar;
        this.i = new rq[i2];
        this.h = rvVar;
    }

    public rt(rl rlVar, rp rpVar, int i2) {
        this(rlVar, rpVar, i2, new ro(new Handler(Looper.getMainLooper())));
    }

    public final void a(a aVar) {
        synchronized (this.c) {
            for (rs rsVar : this.c) {
                if (aVar.a(rsVar)) {
                    rsVar.i();
                }
            }
        }
    }

    public final <T> rs<T> a(rs<T> rsVar) {
        rsVar.a(this);
        synchronized (this.c) {
            this.c.add(rsVar);
        }
        rsVar.b(this.a.incrementAndGet());
        if (!rsVar.m()) {
            this.e.add(rsVar);
            return rsVar;
        }
        synchronized (this.b) {
            String b2 = rsVar.b();
            if (this.b.containsKey(b2)) {
                Queue queue = (Queue) this.b.get(b2);
                if (queue == null) {
                    queue = new LinkedList();
                }
                queue.add(rsVar);
                this.b.put(b2, queue);
                if (rx.b) {
                    rx.a("Request for cacheKey=%s is in flight, putting on hold.", b2);
                }
            } else {
                this.b.put(b2, null);
                this.d.add(rsVar);
            }
        }
        return rsVar;
    }

    /* access modifiers changed from: 0000 */
    public final <T> void b(rs<T> rsVar) {
        synchronized (this.c) {
            this.c.remove(rsVar);
        }
        synchronized (this.k) {
            Iterator it = this.k.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        if (rsVar.m()) {
            synchronized (this.b) {
                String b2 = rsVar.b();
                Queue queue = (Queue) this.b.remove(b2);
                if (queue != null) {
                    if (rx.b) {
                        rx.a("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(queue.size()), b2);
                    }
                    this.d.addAll(queue);
                }
            }
        }
    }

    public final void a() {
        if (this.j != null) {
            this.j.a();
        }
        for (int i2 = 0; i2 < this.i.length; i2++) {
            if (this.i[i2] != null) {
                this.i[i2].a();
            }
        }
        this.j = new rm(this.d, this.e, this.f, this.h);
        this.j.start();
        for (int i3 = 0; i3 < this.i.length; i3++) {
            rq rqVar = new rq(this.e, this.g, this.f, this.h);
            this.i[i3] = rqVar;
            rqVar.start();
        }
    }
}
