package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class jt {
    @NonNull
    private final w<String> a;
    @NonNull
    private final String b;
    @NonNull
    private final fl c;

    jt(@NonNull w<String> wVar, @NonNull String str, @NonNull fl flVar) {
        this.a = wVar;
        this.b = str;
        this.c = flVar;
    }

    @NonNull
    public final w<String> a() {
        return this.a;
    }

    @NonNull
    public final fl b() {
        return this.c;
    }

    @NonNull
    public final String c() {
        return this.b;
    }
}
