package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public interface pz<T> {
    @NonNull
    T a(@NonNull JSONObject jSONObject) throws JSONException, z;
}
