package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import com.yandex.mobile.ads.video.VmapError;

public final class ti implements VmapError {
    private final int a;
    @Nullable
    private final String b;

    public ti(int i, @Nullable String str) {
        this.a = i;
        this.b = str;
    }

    public final int getCode() {
        return this.a;
    }

    @Nullable
    public final String getDescription() {
        return this.b;
    }
}
