package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.Map;

public final class bb<T extends a> {
    @NonNull
    private final T a;
    @NonNull
    private final bm b;
    @NonNull
    private final ba c;

    bb(@NonNull T t, @NonNull bm bmVar, @NonNull ba baVar) {
        this.a = t;
        this.b = bmVar;
        this.c = baVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final T a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final bm b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, Object> a(@NonNull Context context) {
        return this.c.a(context);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> c() {
        return this.c.a(this.b);
    }
}
