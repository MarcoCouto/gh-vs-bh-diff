package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public final class oy {
    @NonNull
    private final String a;
    @NonNull
    private final String b;
    @Nullable
    private final Map<String, Object> c;

    public oy(@NonNull String str, @NonNull String str2, @Nullable Map<String, Object> map) {
        this.a = str;
        this.b = str2;
        this.c = map;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    @NonNull
    public final String b() {
        return this.b;
    }

    @Nullable
    public final Map<String, Object> c() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        oy oyVar = (oy) obj;
        if (!this.a.equals(oyVar.a) || !this.b.equals(oyVar.b)) {
            return false;
        }
        if (this.c != null) {
            return this.c.equals(oyVar.c);
        }
        return oyVar.c == null;
    }

    public final int hashCode() {
        return (((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0);
    }
}
