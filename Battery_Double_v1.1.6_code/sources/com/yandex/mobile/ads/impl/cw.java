package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.impl.dq.a;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;
import java.util.Map;

public abstract class cw<T> implements hs<fl, w<T>> {
    @NonNull
    private final ct a = new ct();
    @NonNull
    private final cu b = new cu();

    public final /* synthetic */ hr a(@Nullable ru ruVar, int i, @NonNull Object obj) {
        return new hr(b.RESPONSE, a((fl) obj, ruVar, i));
    }

    public final /* synthetic */ hr a(@NonNull Object obj) {
        return new hr(b.REQUEST, a((fl) obj));
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, Object> a(@NonNull fl flVar) {
        HashMap hashMap = new HashMap();
        a(hashMap, flVar);
        hashMap.put("block_id", flVar.e());
        hashMap.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, flVar.a().a());
        hashMap.put("is_passback", Boolean.valueOf(flVar.r() == a.b));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, Object> a(@NonNull fl flVar, @Nullable ru<w<T>> ruVar, int i) {
        HashMap hashMap = new HashMap();
        ea eaVar = new ea(new HashMap());
        eaVar.a("block_id", flVar.e());
        eaVar.a(AppEventsConstants.EVENT_PARAM_AD_TYPE, flVar.a().a());
        if (!(ruVar == null || ruVar.a == null || ((w) ruVar.a).n() != null)) {
            eaVar.a("ad_type_format", ((w) ruVar.a).b());
            eaVar.a("product_type", ((w) ruVar.a).c());
        }
        eaVar.a(i == -1 ? NativeProtocol.BRIDGE_ARG_ERROR_CODE : "code", Integer.valueOf(i));
        String str = "empty";
        if (!(ruVar == null || ruVar.a == null)) {
            if (((w) ruVar.a).n() != null) {
                str = "mediation";
            } else if (((w) ruVar.a).p() != null) {
                str = "ad";
            }
        }
        eaVar.a(ServerProtocol.DIALOG_PARAM_RESPONSE_TYPE, str);
        hashMap.putAll(eaVar.a());
        a(hashMap, flVar);
        return hashMap;
    }

    private static void a(@NonNull Map<String, Object> map, @NonNull fl flVar) {
        AdRequest c = flVar.c();
        if (c != null) {
            map.putAll(ct.a(c));
        }
    }
}
