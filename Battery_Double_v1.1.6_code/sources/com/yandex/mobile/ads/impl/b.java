package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import java.lang.ref.WeakReference;

public final class b implements ci {
    @NonNull
    private WeakReference<e> a;

    b(@NonNull e eVar) {
        this.a = new WeakReference<>(eVar);
    }

    public final boolean a() {
        e eVar = (e) this.a.get();
        return eVar != null && !ee.d((View) eVar);
    }
}
