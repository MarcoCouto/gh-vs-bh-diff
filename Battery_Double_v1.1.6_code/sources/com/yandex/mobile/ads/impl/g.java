package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

final class g {

    static final class a implements f {
        @NonNull
        private final a a;

        public final void a(@NonNull Context context, @NonNull View view) {
        }

        a(@NonNull a aVar) {
            this.a = aVar;
        }

        public final void a() {
            if (!dr.a((ac) this.a)) {
                this.a.t();
            }
        }

        public final void b() {
            if (!dr.a((ac) this.a)) {
                this.a.u();
            }
        }

        public final void a(int i) {
            this.a.b(i);
        }
    }

    static final class b implements f {
        public final void a() {
        }

        public final void a(int i) {
        }

        public final void b() {
        }

        b() {
        }

        public final void a(@NonNull Context context, @NonNull View view) {
            view.setVisibility(0);
            view.setMinimumHeight(ee.a(context, 50.0f));
        }
    }
}
