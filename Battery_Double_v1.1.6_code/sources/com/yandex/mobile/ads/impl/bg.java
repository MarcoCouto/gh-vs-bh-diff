package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.yandex.mobile.ads.AdRequest;
import java.util.HashMap;
import java.util.Map;

public class bg implements ba {
    @NonNull
    protected final fl a;

    public bg(@NonNull fl flVar) {
        this.a = flVar;
    }

    @NonNull
    public Map<String, Object> a(@NonNull Context context) {
        HashMap hashMap = new HashMap();
        AdRequest c = this.a.c();
        if (c != null) {
            hashMap.put(IronSourceSegment.AGE, c.getAge());
            hashMap.put("context_tags", c.getContextTags());
            hashMap.put("gender", c.getGender());
            hashMap.put("location", c.getLocation());
            fw a2 = fv.a().a(context);
            Boolean g = a2 != null ? a2.g() : null;
            if (g != null) {
                hashMap.put("user_consent", g);
            }
        }
        return hashMap;
    }

    @NonNull
    public final Map<String, String> a(@NonNull bm bmVar) {
        return bmVar.b();
    }
}
