package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class kl {
    @NonNull
    public static kk a(@NonNull w<String> wVar) {
        bl n = wVar.n();
        if (n != null) {
            return new km(wVar, n);
        }
        return new kn();
    }
}
