package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class bu {
    @NonNull
    private static final Object a = new Object();
    private static volatile rt b;

    @NonNull
    public static rt a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    rt a2 = bv.a(context, 1);
                    b = a2;
                    a2.a();
                }
            }
        }
        return b;
    }
}
