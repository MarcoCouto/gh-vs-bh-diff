package com.yandex.mobile.ads.impl;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import java.util.WeakHashMap;

public final class ag extends BroadcastReceiver {
    private static volatile ag d;
    private static final Object e = new Object();
    private boolean a = false;
    private int b = a.c;
    private WeakHashMap<b, Object> c = new WeakHashMap<>();

    enum a {
        ;
        
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;

        static {
            d = new int[]{a, b, c};
        }
    }

    public interface b {
        void a(@NonNull Intent intent);
    }

    private ag() {
    }

    public static ag a() {
        if (d == null) {
            synchronized (e) {
                if (d == null) {
                    d = new ag();
                }
            }
        }
        return d;
    }

    public final synchronized void onReceive(Context context, Intent intent) {
        if (intent != null) {
            new Object[1][0] = intent.getAction();
            String action = intent.getAction();
            if ("android.intent.action.SCREEN_OFF".equals(action)) {
                this.b = a.b;
            } else if ("android.intent.action.USER_PRESENT".equals(action)) {
                this.b = a.c;
            } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                this.b = a.a;
            }
            a(intent);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        b(r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002c, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x002f */
    public final synchronized void a(b bVar, Context context) {
        if (context != null) {
            this.c.put(bVar, null);
            if (!this.a) {
                Context applicationContext = context.getApplicationContext();
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.SCREEN_ON");
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                intentFilter.addAction("android.intent.action.USER_PRESENT");
                applicationContext.registerReceiver(this, intentFilter);
                this.a = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        return;
     */
    public final synchronized void b(b bVar, Context context) {
        if (context != null) {
            this.c.remove(bVar);
            try {
                if (this.a && this.c.isEmpty()) {
                    context.getApplicationContext().unregisterReceiver(this);
                    this.a = false;
                }
            } catch (Exception unused) {
            }
        }
    }

    private synchronized void a(@NonNull Intent intent) {
        for (b a2 : this.c.keySet()) {
            a2.a(intent);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        return r3;
     */
    public final synchronized boolean a(Context context) {
        boolean z;
        boolean c2 = c(context);
        fw a2 = fv.a().a(context);
        if (a2 != null && a2.k()) {
            return c2;
        }
        if (c2) {
            if (!b(context)) {
                z = true;
            }
        }
        z = false;
    }

    private synchronized boolean b(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
        if (VERSION.SDK_INT < 16) {
            return this.b == a.b;
        }
        return keyguardManager.isKeyguardLocked();
    }

    private synchronized boolean c(Context context) {
        boolean z;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (VERSION.SDK_INT >= 20) {
            z = powerManager.isInteractive();
        } else {
            z = powerManager.isScreenOn();
        }
        return z;
    }
}
