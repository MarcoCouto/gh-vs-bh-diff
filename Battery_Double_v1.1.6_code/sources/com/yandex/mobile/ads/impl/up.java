package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.models.vmap.AdSource;
import com.yandex.mobile.ads.video.models.vmap.c;
import com.yandex.mobile.ads.video.models.vmap.d;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

final class up {
    @NonNull
    private final un a;
    @NonNull
    private final uq b;

    public up(@NonNull un unVar) {
        this.a = unVar;
        this.b = new uq(unVar);
    }

    @Nullable
    static AdSource a(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        c cVar;
        un.a(xmlPullParser, "AdSource");
        Boolean b2 = un.b(xmlPullParser, "allowMultipleAds");
        Boolean b3 = un.b(xmlPullParser, "followRedirects");
        String attributeValue = xmlPullParser.getAttributeValue(null, "id");
        AdSource adSource = null;
        while (un.b(xmlPullParser)) {
            if (un.a(xmlPullParser)) {
                if ("AdTagURI".equals(xmlPullParser.getName())) {
                    un.a(xmlPullParser, "AdTagURI");
                    String attributeValue2 = xmlPullParser.getAttributeValue(null, "templateType");
                    String c = un.c(xmlPullParser);
                    if (!TextUtils.isEmpty(c)) {
                        cVar = d.a(c, attributeValue2);
                    } else {
                        cVar = null;
                    }
                    if (cVar != null) {
                        adSource = d.a(cVar, b2, b3, attributeValue);
                    }
                } else {
                    un.d(xmlPullParser);
                }
            }
        }
        return adSource;
    }
}
