package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ux implements Parcelable {
    public static final Creator<ux> CREATOR = new Creator<ux>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new ux[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new ux(parcel);
        }
    };
    @Nullable
    private final String a;
    @Nullable
    private final String b;

    public static class a {
        /* access modifiers changed from: private */
        @Nullable
        public String a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;

        @NonNull
        public final a a(@NonNull String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public final a b(@NonNull String str) {
            this.b = str;
            return this;
        }
    }

    public int describeContents() {
        return 0;
    }

    /* synthetic */ ux(a aVar, byte b2) {
        this(aVar);
    }

    private ux(@NonNull a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
    }

    @Nullable
    public final String a() {
        return this.a;
    }

    @Nullable
    public final String b() {
        return this.b;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
    }

    protected ux(@NonNull Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
    }
}
