package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.b;

public final class uz implements vb {
    @NonNull
    private final b a;

    public uz(@NonNull VastRequestConfiguration vastRequestConfiguration) {
        this.a = new b(vastRequestConfiguration);
    }

    @NonNull
    public final String a() {
        String c = this.a.c();
        return TextUtils.isEmpty(c) ? "null" : c;
    }

    @NonNull
    public final String b() {
        String b = this.a.b();
        return TextUtils.isEmpty(b) ? "null" : b;
    }
}
