package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

final class re implements rc<BitmapDrawable> {
    re() {
    }

    public final /* synthetic */ boolean a(@NonNull Drawable drawable, @NonNull Bitmap bitmap) {
        return bitmap.equals(((BitmapDrawable) drawable).getBitmap());
    }
}
