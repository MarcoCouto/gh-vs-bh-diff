package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ov.a;
import com.yandex.mobile.ads.nativeads.z;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class pt implements pq<ov> {
    @NonNull
    private final pk a;

    public pt(@NonNull pk pkVar) {
        this.a = pkVar;
    }

    @NonNull
    public final /* synthetic */ ot a(@NonNull JSONObject jSONObject) throws JSONException, z {
        String a2 = pi.a(jSONObject, "type");
        JSONArray jSONArray = jSONObject.getJSONArray("items");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            arrayList.add(new a(pi.a(jSONObject2, "title"), this.a.a(jSONObject2, "url")));
        }
        if (!arrayList.isEmpty()) {
            return new ov(a2, arrayList);
        }
        throw new z("Native Ad json has not required attributes");
    }
}
