package com.yandex.mobile.ads.impl;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public final class ch {
    @Nullable
    public static String a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                int length = Uri.encode(str).length();
                if (length <= 1024) {
                    return str;
                }
                Cif.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. Current size is %s bytes", Integer.valueOf(1024), Integer.valueOf(length));
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
