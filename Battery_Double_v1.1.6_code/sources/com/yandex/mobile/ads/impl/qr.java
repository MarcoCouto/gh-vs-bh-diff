package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.widget.Button;
import android.widget.PopupMenu;
import com.yandex.mobile.ads.impl.ov.a;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.r;
import java.util.List;

public final class qr {
    @NonNull
    private final fl a;
    @NonNull
    private final co b;
    @NonNull
    private final ag c;
    @NonNull
    private final qq d = new qq();
    @NonNull
    private final r e;

    public qr(@NonNull fl flVar, @NonNull co coVar, @NonNull ag agVar, @NonNull r rVar) {
        this.a = flVar;
        this.b = coVar;
        this.c = agVar;
        this.e = rVar;
    }

    public final void a(@NonNull Context context, @NonNull ov ovVar) {
        PopupMenu popupMenu;
        Button h = this.c.d().h();
        if (h != null && VERSION.SDK_INT >= 11) {
            List b2 = ovVar.b();
            if (!b2.isEmpty()) {
                try {
                    de deVar = new de(context, this.a);
                    if (VERSION.SDK_INT >= 19) {
                        popupMenu = new PopupMenu(context, h, 5);
                    } else {
                        popupMenu = new PopupMenu(context, h);
                    }
                    Menu menu = popupMenu.getMenu();
                    for (int i = 0; i < b2.size(); i++) {
                        menu.add(0, i, 0, ((a) b2.get(i)).a());
                    }
                    popupMenu.setOnMenuItemClickListener(new qs(deVar, b2, this.b, this.e));
                    popupMenu.show();
                } catch (Exception unused) {
                }
            }
        }
    }
}
