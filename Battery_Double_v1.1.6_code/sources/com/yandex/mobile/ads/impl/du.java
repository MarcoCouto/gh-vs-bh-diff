package com.yandex.mobile.ads.impl;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.yandex.metrica.YandexMetrica;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class du {
    private static final Object a = new Object();
    private static volatile du b;
    private static final List<String> d = new ArrayList<String>() {
        private static final long serialVersionUID = 5712356855156500689L;

        {
            add("com.yandex.mobile.ads.AdActivity");
        }
    };
    private static final List<String> e = new ArrayList<String>() {
        private static final long serialVersionUID = 7066618132468587294L;

        {
            add("android.permission.ACCESS_NETWORK_STATE");
            add("android.permission.INTERNET");
        }
    };
    private boolean c = true;

    public static class a extends Exception {
        private static final long serialVersionUID = 3046464751153928670L;

        public a(String str) {
            super(str);
        }
    }

    public static du a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new du();
                }
            }
        }
        return b;
    }

    public final boolean a(Context context) throws a {
        if (this.c) {
            synchronized (a) {
                if (this.c) {
                    if (hz.a(context)) {
                        PackageManager packageManager = context.getPackageManager();
                        String packageName = context.getPackageName();
                        for (String str : new ArrayList(d)) {
                            try {
                                packageManager.getActivityInfo(new ComponentName(packageName, str), 32);
                            } catch (NameNotFoundException unused) {
                                throw new a(String.format("Please, check %s permission in AndroidManifest file.", new Object[]{e}));
                            } catch (NameNotFoundException unused2) {
                                throw new a(String.format("Please, check %s activity in AndroidManifest file.", new Object[]{str}));
                            }
                        }
                        ArrayList arrayList = new ArrayList(e);
                        PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 4096);
                        if (packageInfo.requestedPermissions != null) {
                            arrayList.removeAll(Arrays.asList(packageInfo.requestedPermissions));
                            if (arrayList.size() > 0) {
                                throw new a(String.format("Please, check %s permission in AndroidManifest file.", new Object[]{arrayList}));
                            }
                        }
                    }
                    this.c = false;
                }
            }
        }
        return true;
    }

    public static boolean b() {
        try {
            YandexMetrica.getLibraryVersion();
            return true;
        } catch (NoClassDefFoundError unused) {
            return false;
        }
    }
}
