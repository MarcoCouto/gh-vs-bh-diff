package com.yandex.mobile.ads.impl;

public final class z {
    private final float a;

    public z(float f) {
        if (f == 0.0f) {
            f = 1.7777778f;
        }
        this.a = f;
    }

    public final int a(int i) {
        return Math.round(((float) i) * this.a);
    }

    public final int b(int i) {
        return Math.round(((float) i) / this.a);
    }
}
