package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;

public final class ox extends ot {
    @NonNull
    private final String a;
    @NonNull
    private final List<oy> b;

    public ox(@NonNull String str, @NonNull String str2, @NonNull List<oy> list) {
        super(str);
        this.a = str2;
        this.b = list;
    }

    @NonNull
    public final String b() {
        return this.a;
    }

    @NonNull
    public final List<oy> c() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        ox oxVar = (ox) obj;
        if (!this.a.equals(oxVar.a)) {
            return false;
        }
        return this.b.equals(oxVar.b);
    }

    public final int hashCode() {
        return (((super.hashCode() * 31) + this.a.hashCode()) * 31) + this.b.hashCode();
    }
}
