package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class hf {
    @NonNull
    private final Object a;

    hf(@NonNull Object obj) {
        this.a = obj;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final hj a() {
        Object a2 = ib.a(this.a, "getLastLocation", new Object[0]);
        if (a2 != null) {
            return new hj(a2);
        }
        return null;
    }
}
