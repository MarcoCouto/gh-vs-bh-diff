package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.a;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class tg {
    @NonNull
    private gj a;

    public tg(@NonNull Context context) {
        this.a = gh.a(context);
    }

    public final void a(@NonNull Context context, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull tj tjVar) {
        th.a(context).a(context, vmapRequestConfiguration, this.a, (RequestListener<Vmap>) tjVar);
    }

    public final void a(@NonNull Context context, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull a aVar) {
        th.a(context).a(context, vastRequestConfiguration, this.a, (RequestListener<tl>) aVar);
    }
}
