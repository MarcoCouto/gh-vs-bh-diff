package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.fw.a;
import java.util.Collection;

public final class hz {
    public static <T> T[] a(T[] tArr) {
        return tArr == null ? (Object[]) new Object[0] : tArr;
    }

    public static boolean a(Collection... collectionArr) {
        Collection[] collectionArr2;
        for (Collection collection : (Collection[]) a((T[]) collectionArr)) {
            if (collection == null || collection.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(int i) {
        return VERSION.SDK_INT >= i;
    }

    public static boolean a(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static synchronized void a(@NonNull Context context, @NonNull fw fwVar) {
        synchronized (hz.class) {
            Editor putInt = context.getSharedPreferences("YadPreferenceFile", 0).edit().putString("SdkConfigurationLibraryVersion", fwVar.m()).putBoolean("SdkConfigurationSensitiveModeDisabled", fwVar.d()).putLong("SdkConfigurationExpiredDate", fwVar.b()).putString("SdkConfigurationMraidUrl", fwVar.f()).putBoolean("CustomClickHandlingEnabled", fwVar.c()).putBoolean("UrlCorrectionEnabled", fwVar.o()).putInt("AdIdsStorageSize", fwVar.a());
            Boolean n = fwVar.n();
            Boolean g = fwVar.g();
            Boolean valueOf = Boolean.valueOf(fwVar.h());
            Boolean valueOf2 = Boolean.valueOf(fwVar.i());
            Boolean valueOf3 = Boolean.valueOf(fwVar.j());
            Boolean valueOf4 = Boolean.valueOf(fwVar.k());
            Boolean valueOf5 = Boolean.valueOf(fwVar.l());
            Boolean e = fwVar.e();
            Boolean valueOf6 = Boolean.valueOf(fwVar.p());
            a(putInt, "SdkConfigurationVisibilityErrorIndicatorEnabled", n);
            a(putInt, "SdkConfigurationMediationSensitiveModeDisabled", g);
            a(putInt, "SdkConfigurationMrcVisibilityTrackingEnabled", valueOf);
            a(putInt, "SdkConfigurationCustomUserAgentEnabled", valueOf2);
            a(putInt, "SdkConfigurationFusedLocationProviderDisabled", valueOf3);
            a(putInt, "SdkConfigurationLockScreenEnabled", valueOf4);
            a(putInt, "SdkConfigurationAutograbEnabled", valueOf5);
            a(putInt, "SdkConfigurationUserConsent", e);
            a(putInt, "SdkConfigurationLegacyVisibilityLogicEnabled", valueOf6);
            putInt.apply();
        }
    }

    @Nullable
    public static synchronized fw b(@NonNull Context context) {
        fw fwVar;
        synchronized (hz.class) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("YadPreferenceFile", 0);
            long j = sharedPreferences.getLong("SdkConfigurationExpiredDate", 0);
            Boolean a = a(sharedPreferences, "SdkConfigurationVisibilityErrorIndicatorEnabled");
            fwVar = null;
            if (j != 0) {
                int i = sharedPreferences.getInt("AdIdsStorageSize", 0);
                Boolean a2 = a(sharedPreferences, "SdkConfigurationMediationSensitiveModeDisabled");
                boolean z = sharedPreferences.getBoolean("SdkConfigurationMrcVisibilityTrackingEnabled", false);
                boolean z2 = sharedPreferences.getBoolean("SdkConfigurationCustomUserAgentEnabled", false);
                boolean z3 = sharedPreferences.getBoolean("SdkConfigurationFusedLocationProviderDisabled", false);
                boolean z4 = sharedPreferences.getBoolean("SdkConfigurationLockScreenEnabled", false);
                boolean z5 = sharedPreferences.getBoolean("SdkConfigurationAutograbEnabled", false);
                Boolean a3 = a(sharedPreferences, "SdkConfigurationUserConsent");
                String string = sharedPreferences.getString("SdkConfigurationLibraryVersion", null);
                String string2 = sharedPreferences.getString("SdkConfigurationMraidUrl", null);
                Boolean bool = a;
                boolean z6 = sharedPreferences.getBoolean("CustomClickHandlingEnabled", false);
                boolean z7 = sharedPreferences.getBoolean("SdkConfigurationSensitiveModeDisabled", false);
                String str = string2;
                boolean z8 = sharedPreferences.getBoolean("UrlCorrectionEnabled", false);
                fwVar = new a().b(string).c(a3).a(j).a(i).b(a2).d(z).e(z2).f(z3).g(z4).h(z5).c(z7).a(str).a(z6).a(bool).b(z8).i(sharedPreferences.getBoolean("SdkConfigurationLegacyVisibilityLogicEnabled", false)).a();
            }
        }
        return fwVar;
    }

    private static void a(@NonNull Editor editor, @NonNull String str, @Nullable Boolean bool) {
        if (bool != null) {
            editor.putBoolean(str, bool.booleanValue());
        } else {
            editor.remove(str);
        }
    }

    @Nullable
    private static Boolean a(@NonNull SharedPreferences sharedPreferences, @NonNull String str) {
        if (sharedPreferences.contains(str)) {
            return Boolean.valueOf(sharedPreferences.getBoolean(str, false));
        }
        return null;
    }
}
