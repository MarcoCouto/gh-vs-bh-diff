package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ai.a;
import com.yandex.mobile.ads.video.VideoAdError;

final class te {
    @NonNull
    private final fl a;
    @NonNull
    private final fn b = new fn();

    te(@NonNull fl flVar) {
        this.a = flVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull final td tdVar) {
        new ai(context, this.a).a(new a() {
            public final void a() {
                tdVar.a();
            }

            public final void a(@NonNull sf sfVar) {
                tdVar.a(VideoAdError.createInternalError("Internal state wasn't completely configured"));
            }
        });
    }
}
