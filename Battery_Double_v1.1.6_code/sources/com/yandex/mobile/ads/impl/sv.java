package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.impl.ru.b;
import java.util.concurrent.TimeUnit;

public abstract class sv<T> extends rs<T> {
    private static final int a = ((int) TimeUnit.SECONDS.toMillis(10));
    private final a<T> b;

    public interface a<T> extends com.yandex.mobile.ads.impl.ru.a, b<T> {
    }

    public sv(int i, String str, a<T> aVar) {
        super(i, str, aVar);
        l();
        a((rw) new rn(a, 0, 1.0f));
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public final void b(T t) {
        if (this.b != null) {
            this.b.a(t);
        }
    }
}
