package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.mediation.base.a;

public interface bd<T extends a> {
    @Nullable
    bb<T> a(@NonNull Context context);
}
