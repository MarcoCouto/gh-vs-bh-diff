package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.mediation.base.a;
import java.util.HashMap;
import java.util.List;

public final class bf<T extends a> {
    @NonNull
    private final bl a;
    @NonNull
    private final ba b;
    @NonNull
    private final bi c;
    private int d;

    public bf(@NonNull bl blVar, @NonNull ba baVar, @NonNull bi biVar) {
        this.a = blVar;
        this.b = baVar;
        this.c = biVar;
    }

    @Nullable
    public final bb<T> a(@NonNull Context context, @NonNull Class<T> cls) {
        List a2 = this.a.a();
        bb<T> bbVar = null;
        while (bbVar == null && this.d < a2.size()) {
            int i = this.d;
            this.d = i + 1;
            bm bmVar = (bm) a2.get(i);
            try {
                a aVar = (a) cls.cast(ib.a(Class.forName(bmVar.a()), new Object[0]));
                if (aVar == null) {
                    a(context, bmVar, "could_not_create_adapter");
                } else {
                    bbVar = new bb<>(aVar, bmVar, this.b);
                }
            } catch (ClassCastException unused) {
                a(context, bmVar, "does_not_conform_to_protocol");
            } catch (Exception unused2) {
                a(context, bmVar, "could_not_create_adapter");
            }
        }
        return bbVar;
    }

    private void a(@NonNull Context context, @NonNull bm bmVar, @NonNull String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, str);
        this.c.f(context, bmVar, hashMap);
    }
}
