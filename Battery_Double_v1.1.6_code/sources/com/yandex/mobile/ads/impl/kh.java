package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.mediation.banner.b;
import com.yandex.mobile.ads.mediation.banner.e;

final class kh implements kg {
    @NonNull
    private final w<String> a;
    @NonNull
    private final bl b;

    kh(@NonNull w<String> wVar, @NonNull bl blVar) {
        this.a = wVar;
        this.b = blVar;
    }

    @NonNull
    public final b a(@NonNull a aVar) {
        return new e(aVar, this.a, this.b);
    }
}
