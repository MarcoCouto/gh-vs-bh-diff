package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.rt.a;

public final class ah {
    private static volatile ah a;
    private static final Object b = new Object();

    public static ah a() {
        if (a == null) {
            synchronized (b) {
                if (a == null) {
                    a = new ah();
                }
            }
        }
        return a;
    }

    public final synchronized void a(Context context, rs rsVar) {
        bu.a(context).a(rsVar);
    }

    public final void a(@NonNull Context context, @NonNull final Object obj) {
        bu.a(context).a((a) new a() {
            public final boolean a(rs<?> rsVar) {
                return obj.equals(rsVar.e());
            }
        });
    }
}
