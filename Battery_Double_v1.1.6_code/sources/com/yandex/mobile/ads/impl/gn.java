package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public final class gn {
    @Nullable
    public static gm a(@Nullable String str, @Nullable Boolean bool) {
        if (bool == null || TextUtils.isEmpty(str)) {
            return null;
        }
        return new gm(str, bool.booleanValue());
    }
}
