package com.yandex.mobile.ads.impl;

public class bn {
    private long a;
    private String b;
    private int c;

    public final void a(long j) {
        this.a = j;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final long a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bn bnVar = (bn) obj;
        if (this.a != bnVar.a || this.c != bnVar.c) {
            return false;
        }
        if (this.b != null) {
            return this.b.equals(bnVar.b);
        }
        return bnVar.b == null;
    }

    public int hashCode() {
        return (((((int) (this.a ^ (this.a >>> 32))) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + this.c;
    }
}
