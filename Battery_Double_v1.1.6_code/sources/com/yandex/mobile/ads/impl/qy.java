package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.q;
import java.util.ArrayList;
import java.util.List;

public final class qy {
    @NonNull
    private final q a = new q();

    @NonNull
    public static List<String> a(@NonNull w<or> wVar) {
        List<oq> c = c(wVar);
        ArrayList arrayList = new ArrayList();
        for (oq a2 : c) {
            arrayList.addAll(q.a(a2));
        }
        return arrayList;
    }

    @NonNull
    public static List<String> b(@NonNull w<or> wVar) {
        List<oq> c = c(wVar);
        ArrayList arrayList = new ArrayList();
        for (oq b : c) {
            NativeAdType b2 = b.b();
            if (b2 != null) {
                arrayList.add(b2.getValue());
            }
        }
        return arrayList;
    }

    @NonNull
    private static List<oq> c(@NonNull w<or> wVar) {
        or orVar = (or) wVar.p();
        List<oq> c = orVar != null ? orVar.c() : null;
        if (c != null) {
            return c;
        }
        return new ArrayList();
    }
}
