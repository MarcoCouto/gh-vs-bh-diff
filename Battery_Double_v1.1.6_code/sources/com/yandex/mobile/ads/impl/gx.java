package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public final class gx {
    @NonNull
    private final Context a;
    @NonNull
    private final ib b = new ib();
    @NonNull
    private final gn c = new gn();

    public gx(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @Nullable
    public final gm a() {
        try {
            Class a2 = ib.a("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            if (a2 == null) {
                return null;
            }
            Object a3 = ib.a(a2, "getAdvertisingIdInfo", this.a);
            return gn.a((String) ib.a(a3, "getId", new Object[0]), (Boolean) ib.a(a3, RequestParameters.isLAT, new Object[0]));
        } catch (Throwable unused) {
            return null;
        }
    }
}
