package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class fw {
    private final int a;
    private final long b;
    private final boolean c;
    private final boolean d;
    private final boolean e;
    private final boolean f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;
    @Nullable
    private final Boolean l;
    @Nullable
    private final Boolean m;
    @Nullable
    private final String n;
    @Nullable
    private final String o;
    @Nullable
    private final Boolean p;

    public static class a {
        /* access modifiers changed from: private */
        public int a;
        /* access modifiers changed from: private */
        public long b;
        /* access modifiers changed from: private */
        public boolean c;
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public boolean e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public boolean g;
        /* access modifiers changed from: private */
        public boolean h;
        /* access modifiers changed from: private */
        public boolean i;
        /* access modifiers changed from: private */
        public boolean j;
        /* access modifiers changed from: private */
        public boolean k;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean l;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean m;
        /* access modifiers changed from: private */
        @Nullable
        public String n;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean o;
        /* access modifiers changed from: private */
        @Nullable
        public String p;

        @NonNull
        public final fw a() {
            return new fw(this, 0);
        }

        @NonNull
        public final a a(int i2) {
            this.a = i2;
            return this;
        }

        @NonNull
        public final a a(long j2) {
            this.b = j2;
            return this;
        }

        @NonNull
        public final a a(boolean z) {
            this.c = z;
            return this;
        }

        @NonNull
        public final a b(boolean z) {
            this.j = z;
            return this;
        }

        @NonNull
        public final a a(@Nullable String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public final a a(@Nullable Boolean bool) {
            this.m = bool;
            return this;
        }

        @NonNull
        public final a b(@Nullable String str) {
            this.p = str;
            return this;
        }

        @NonNull
        public final a c(boolean z) {
            this.i = z;
            return this;
        }

        @NonNull
        public final a b(@Nullable Boolean bool) {
            this.o = bool;
            return this;
        }

        @NonNull
        public final a d(boolean z) {
            this.h = z;
            return this;
        }

        @NonNull
        public final a e(boolean z) {
            this.d = z;
            return this;
        }

        @NonNull
        public final a f(boolean z) {
            this.e = z;
            return this;
        }

        @NonNull
        public final a g(boolean z) {
            this.f = z;
            return this;
        }

        @NonNull
        public final a h(boolean z) {
            this.g = z;
            return this;
        }

        @NonNull
        public final a c(@Nullable Boolean bool) {
            this.l = bool;
            return this;
        }

        @NonNull
        public final a i(boolean z) {
            this.k = z;
            return this;
        }
    }

    /* synthetic */ fw(a aVar, byte b2) {
        this(aVar);
    }

    private fw(@NonNull a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.n = aVar.n;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.m = aVar.m;
        this.o = aVar.p;
        this.p = aVar.o;
        this.h = aVar.h;
        this.i = aVar.i;
        this.l = aVar.l;
        this.j = aVar.j;
        this.k = aVar.k;
    }

    public final int a() {
        return this.a;
    }

    public final long b() {
        return this.b;
    }

    public final boolean c() {
        return this.c;
    }

    public final boolean d() {
        return this.i;
    }

    @Nullable
    public final Boolean e() {
        return this.l;
    }

    @Nullable
    public final String f() {
        return this.n;
    }

    @Nullable
    public final Boolean g() {
        return this.p;
    }

    public final boolean h() {
        return this.h;
    }

    public final boolean i() {
        return this.d;
    }

    public final boolean j() {
        return this.e;
    }

    public final boolean k() {
        return this.f;
    }

    public final boolean l() {
        return this.g;
    }

    @Nullable
    public final String m() {
        return this.o;
    }

    @Nullable
    public final Boolean n() {
        return this.m;
    }

    public final boolean o() {
        return this.j;
    }

    public final boolean p() {
        return this.k;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        fw fwVar = (fw) obj;
        if (this.b != fwVar.b || this.a != fwVar.a || this.c != fwVar.c || this.d != fwVar.d || this.e != fwVar.e || this.f != fwVar.f || this.g != fwVar.g || this.h != fwVar.h || this.i != fwVar.i || this.j != fwVar.j || this.k != fwVar.k) {
            return false;
        }
        if (this.l == null ? fwVar.l != null : !this.l.equals(fwVar.l)) {
            return false;
        }
        if (this.m == null ? fwVar.m != null : !this.m.equals(fwVar.m)) {
            return false;
        }
        if (this.n == null ? fwVar.n != null : !this.n.equals(fwVar.n)) {
            return false;
        }
        if (this.o == null ? fwVar.o != null : !this.o.equals(fwVar.o)) {
            return false;
        }
        if (this.p != null) {
            return this.p.equals(fwVar.p);
        }
        return fwVar.p == null;
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = ((((((((((((((((((((((((((((((int) (this.b ^ (this.b >>> 32))) * 31) + this.a) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0)) * 31) + (this.j ? 1 : 0)) * 31) + (this.k ? 1 : 0)) * 31) + (this.l != null ? this.l.hashCode() : 0)) * 31) + (this.m != null ? this.m.hashCode() : 0)) * 31) + (this.n != null ? this.n.hashCode() : 0)) * 31) + (this.o != null ? this.o.hashCode() : 0)) * 31;
        if (this.p != null) {
            i2 = this.p.hashCode();
        }
        return hashCode + i2;
    }
}
