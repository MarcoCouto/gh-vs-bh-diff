package com.yandex.mobile.ads.impl;

import android.support.v4.app.NotificationCompat;

public enum af {
    AD("ad"),
    PROMO(NotificationCompat.CATEGORY_PROMO);
    
    private final String c;

    private af(String str) {
        this.c = str;
    }

    public final String a() {
        return this.c;
    }
}
