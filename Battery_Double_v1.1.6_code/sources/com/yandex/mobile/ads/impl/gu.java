package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.core.identifiers.ad.huawei.a;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

final class gu {
    @NonNull
    private final Executor a = Executors.newSingleThreadExecutor(new dx("YandexMobileAds.AdvertisingId"));
    @NonNull
    private final gr b;
    @NonNull
    private final gr c;

    gu(@NonNull Context context) {
        this.b = new gw(context);
        this.c = new a(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final gs gsVar) {
        this.a.execute(new Runnable() {
            public final void run() {
                go a2 = gu.a(gu.this);
                if (a2.a() == null && a2.b() == null) {
                    gsVar.a();
                } else {
                    gsVar.a(a2);
                }
            }
        });
    }

    static /* synthetic */ go a(gu guVar) {
        return new go(guVar.b.a(), guVar.c.a());
    }
}
