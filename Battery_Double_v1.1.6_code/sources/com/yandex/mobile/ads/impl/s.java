package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.yandex.mobile.ads.AdRequestError;
import java.lang.ref.WeakReference;

public final class s implements ac {
    private static final SparseArray<AdRequestError> c;
    private final ah a = ah.a();
    private final WeakReference<aa> b;

    public final boolean a_() {
        return false;
    }

    public s(aa aaVar) {
        this.b = new WeakReference<>(aaVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull bw bwVar) {
        aa aaVar = (aa) this.b.get();
        if (aaVar != null) {
            this.a.a(aaVar.m(), (rs) bwVar);
        }
    }

    public final void a() {
        aa aaVar = (aa) this.b.get();
        if (aaVar != null) {
            this.a.a(aaVar.m(), (Object) dr.a(aaVar));
        }
    }

    public final void b() {
        a();
        this.b.clear();
    }

    public static AdRequestError a(int i) {
        return (AdRequestError) c.get(i, u.t);
    }

    static {
        SparseArray<AdRequestError> sparseArray = new SparseArray<>();
        c = sparseArray;
        sparseArray.put(6, u.j);
        c.put(2, u.g);
        c.put(5, u.e);
        c.put(8, u.f);
        c.put(10, u.m);
        c.put(4, u.m);
        c.put(9, u.h);
        c.put(7, u.l);
        c.put(11, u.o);
    }
}
