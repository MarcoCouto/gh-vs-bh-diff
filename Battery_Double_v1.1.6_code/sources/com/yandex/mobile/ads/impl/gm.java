package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class gm {
    @NonNull
    private final String a;
    private final boolean b;

    public gm(@NonNull String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }
}
