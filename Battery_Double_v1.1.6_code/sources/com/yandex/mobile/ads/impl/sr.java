package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import javax.net.ssl.SSLSocketFactory;

public final class sr {
    @NonNull
    private final ss a;
    @NonNull
    private final sy b;
    @Nullable
    private final Context c;

    public sr(@Nullable Context context, @NonNull sy syVar) {
        this.b = syVar;
        this.c = context != null ? context.getApplicationContext() : null;
        this.a = fv.a().c();
    }

    public final sk a() {
        SSLSocketFactory sSLSocketFactory;
        if (hz.a(21)) {
            sSLSocketFactory = sx.a();
        } else {
            sSLSocketFactory = null;
        }
        return new st(this.c, this.a.a(sSLSocketFactory));
    }
}
