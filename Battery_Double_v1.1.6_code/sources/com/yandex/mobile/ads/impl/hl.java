package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class hl implements ha {
    @NonNull
    private final hc a = new hc();
    @NonNull
    private final hm b;
    @Nullable
    private final LocationManager c;

    public hl(@NonNull Context context) {
        Context applicationContext = context.getApplicationContext();
        this.c = (LocationManager) applicationContext.getSystemService("location");
        this.b = new hm(applicationContext, this.c);
    }

    @Nullable
    public final Location a() {
        List<String> b2 = b();
        if (b2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String a2 : b2) {
            Location a3 = this.b.a(a2);
            if (a3 != null) {
                arrayList.add(a3);
            }
        }
        return hc.a(arrayList);
    }

    @Nullable
    private List<String> b() {
        try {
            if (this.c != null) {
                return this.c.getAllProviders();
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }
}
