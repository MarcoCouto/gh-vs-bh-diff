package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import org.json.JSONObject;

public enum fj implements fh {
    DEFAULT("default"),
    LOADING("loading"),
    HIDDEN("hidden");
    
    private final String d;

    private fj(String str) {
        this.d = str;
    }

    @NonNull
    public final String a() {
        return String.format("state: %s", new Object[]{JSONObject.quote(this.d)});
    }
}
