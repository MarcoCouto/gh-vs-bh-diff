package com.yandex.mobile.ads.impl;

import java.util.Locale;

public final class eq {
    public static final String a = a("onCollapse", "config", "AdPerformActionsJSI");
    public static final String b = a("onExpand", "config", "AdPerformActionsJSI");
    public static final String c = String.format(Locale.US, "<script type='text/javascript'> \nfunction wrapJsFunction_%1$s() { \n  window['%1$s'] = function(%3$s) { \n      return %4$s.%1$s(%2$s, %3$s); \n  } \n} \n \nwrapJsFunction_%1$s('%1$s'); \n</script> \n", new Object[]{"onAdRender", "document.querySelector('#rtb').offsetHeight", "testTag", "AdPerformActionsJSI"});
    static final String d = String.format(Locale.US, "<script type='text/javascript'> \nfunction wrapJsFunction_%1$s() { \n  window['%1$s'] = function() { \n      return %2$s.%1$s(); \n  } \n} \n \nwrapJsFunction_%1$s('%1$s'); \n</script> \n", new Object[]{"getBannerInfo", "AdPerformActionsJSI"});

    private static String a(String str, String str2, String str3) {
        return String.format(Locale.US, "<script type='text/javascript'> \nfunction wrapJsFunction_%1$s() { \n  window['%1$s'] = function(%2$s) { \n      %3$s.%1$s(JSON.stringify(%2$s)); \n  } \n} \n \nwrapJsFunction_%1$s('%1$s'); \n</script> \n", new Object[]{str, str2, str3}).replace("JSON.stringify()", "");
    }

    public static String a(int i) {
        StringBuilder sb = new StringBuilder("<body style='width:");
        sb.append(i);
        sb.append("px;'>");
        return sb.toString();
    }

    public static String a(int i, int i2) {
        StringBuilder sb = new StringBuilder("\n<style>ytag.container { width:");
        sb.append(i);
        sb.append("px; height:");
        sb.append(i2);
        sb.append("px; }</style>\n");
        return sb.toString();
    }
}
