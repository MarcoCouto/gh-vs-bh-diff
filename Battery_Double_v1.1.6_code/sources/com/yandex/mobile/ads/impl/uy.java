package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.ux.a;
import com.yandex.mobile.ads.video.models.common.Extension;
import java.util.List;

public final class uy {
    public static ux a(@NonNull List<Extension> list) {
        a aVar = new a();
        for (Extension extension : list) {
            String type = extension.getType();
            String value = extension.getValue();
            if ("PageID".equals(type)) {
                aVar.a(value);
            } else if ("SessionID".equals(type)) {
                aVar.b(value);
            }
        }
        return new ux(aVar, 0);
    }
}
