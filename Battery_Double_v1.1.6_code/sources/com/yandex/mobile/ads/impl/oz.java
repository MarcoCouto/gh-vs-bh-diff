package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.util.List;

public final class oz {
    @NonNull
    private final List<pa> a;

    public oz(@NonNull List<pa> list) {
        this.a = list;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.a.equals(((oz) obj).a);
    }

    public final int hashCode() {
        return this.a.hashCode();
    }
}
