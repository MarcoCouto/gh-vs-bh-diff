package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import com.yandex.mobile.ads.impl.pe.b;

final class pf implements b {
    @NonNull
    private final LruCache<String, Bitmap> a;
    @NonNull
    private final pb b;

    pf(@NonNull LruCache<String, Bitmap> lruCache, @NonNull pb pbVar) {
        this.a = lruCache;
        this.b = pbVar;
    }

    @Nullable
    public final Bitmap a(@NonNull String str) {
        if (VERSION.SDK_INT < 12) {
            return null;
        }
        return (Bitmap) this.a.get(pb.a(str));
    }

    public final void a(@NonNull String str, @NonNull Bitmap bitmap) {
        if (VERSION.SDK_INT >= 12) {
            this.a.put(pb.a(str), bitmap);
        }
    }
}
