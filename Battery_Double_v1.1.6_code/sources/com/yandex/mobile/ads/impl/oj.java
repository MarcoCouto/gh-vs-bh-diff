package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class oj<T> {
    @NonNull
    private final T a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;
    @Nullable
    private final on d;
    private final boolean e;
    private final boolean f;

    public oj(@NonNull String str, @NonNull String str2, @NonNull T t, @Nullable on onVar, boolean z, boolean z2) {
        this.b = str;
        this.c = str2;
        this.a = t;
        this.d = onVar;
        this.f = z;
        this.e = z2;
    }

    @NonNull
    public final String a() {
        return this.b;
    }

    @NonNull
    public final String b() {
        return this.c;
    }

    @NonNull
    public final T c() {
        return this.a;
    }

    @Nullable
    public final on d() {
        return this.d;
    }

    public final boolean e() {
        return this.f;
    }

    public final boolean f() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        oj ojVar = (oj) obj;
        if (this.e != ojVar.e || this.f != ojVar.f || !this.a.equals(ojVar.a) || !this.b.equals(ojVar.b) || !this.c.equals(ojVar.c)) {
            return false;
        }
        if (this.d != null) {
            return this.d.equals(ojVar.d);
        }
        return ojVar.d == null;
    }

    public final int hashCode() {
        return (((((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0);
    }
}
