package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.ag;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public final class rj {
    @NonNull
    private final hp a;
    @NonNull
    private final List<oj> b;
    @NonNull
    private final b c;
    @Nullable
    private final String d;
    @Nullable
    private a e;

    public rj(@NonNull Context context, @NonNull b bVar, @Nullable String str, @Nullable List<oj> list) {
        this.c = bVar;
        this.d = str;
        this.a = hp.a(context);
        if (list == null) {
            list = Collections.emptyList();
        }
        this.b = list;
    }

    public final void a(@NonNull a aVar) {
        this.e = aVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    public final void a(@NonNull ag agVar) {
        boolean z;
        MediaView k = agVar.d().k();
        String str = "media";
        oj ojVar = null;
        for (oj ojVar2 : this.b) {
            if (str.equals(ojVar2.a())) {
                ojVar = ojVar2;
            }
        }
        if (k == null) {
            if ((ojVar == null || !(ojVar.c() instanceof op) || ((op) ojVar.c()).a() == null) ? false : true) {
                z = true;
                if (!z) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, this.c.a());
                    hashMap.put("block_id", this.d);
                    hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, "expected_view_missing");
                    hashMap.put(ParametersKeys.VIEW, "mediaview");
                    hashMap.put("asset_name", "media");
                    if (this.e != null) {
                        hashMap.putAll(this.e.a());
                    }
                    this.a.a(new hr(hr.b.BINDING_FAILURE, hashMap));
                    new Object[1][0] = "mediaview";
                    return;
                }
                return;
            }
        }
        z = false;
        if (!z) {
        }
    }
}
