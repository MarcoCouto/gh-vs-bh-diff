package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import java.lang.ref.WeakReference;

public abstract class md<V extends View, T> {
    @NonNull
    private final WeakReference<V> a;

    public abstract boolean a(@NonNull V v, @NonNull T t);

    public abstract void b(@NonNull V v, @NonNull T t);

    public md(@NonNull V v) {
        this.a = new WeakReference<>(v);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@NonNull V v) {
        v.setVisibility(8);
        v.setOnClickListener(null);
        v.setOnTouchListener(null);
        v.setSelected(false);
    }

    @Nullable
    public final V a() {
        return (View) this.a.get();
    }

    public void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        View a2 = a();
        if (a2 != null) {
            mjVar.a(ojVar, a2);
            mjVar.a(ojVar, (mq) new mt(a2));
        }
    }

    public final boolean b() {
        return a() != null;
    }

    public final boolean c() {
        View a2 = a();
        if (a2 == null || ee.d(a2) || ee.a(a2, 1)) {
            return false;
        }
        return true;
    }

    public final boolean d() {
        return ee.b(a(), 100);
    }
}
