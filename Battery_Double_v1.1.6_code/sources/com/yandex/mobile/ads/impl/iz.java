package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class iz {
    private static volatile iz a;
    private static final Object b = new Object();

    @NonNull
    public static iz a() {
        if (a == null) {
            synchronized (b) {
                if (a == null) {
                    a = new iz();
                }
            }
        }
        return a;
    }

    private iz() {
    }

    @NonNull
    public static iy a(boolean z) {
        if (z) {
            return new ja();
        }
        return new ix();
    }
}
