package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;

public final class nz implements dk {
    @NonNull
    private final WeakReference<Context> a;
    @NonNull
    private final fl b;
    @NonNull
    private final ResultReceiver c;

    public nz(@NonNull Context context, @NonNull fl flVar, @NonNull ResultReceiver resultReceiver) {
        this.a = new WeakReference<>(context);
        this.b = flVar;
        this.c = resultReceiver;
    }

    public final void a(@NonNull co coVar, @Nullable String str) {
        o.a((Context) this.a.get(), coVar, str, this.c, this.b.k());
    }
}
