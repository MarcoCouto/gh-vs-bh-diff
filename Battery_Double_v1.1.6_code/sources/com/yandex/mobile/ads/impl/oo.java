package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class oo {
    @Nullable
    private final String a;
    private final float b;

    public oo(@Nullable String str, float f) {
        this.a = str;
        this.b = f;
    }

    @Nullable
    public final String a() {
        return this.a;
    }

    public final float b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        oo ooVar = (oo) obj;
        if (Float.compare(ooVar.b, this.b) != 0) {
            return false;
        }
        return this.a.equals(ooVar.a);
    }

    public final int hashCode() {
        return (this.a.hashCode() * 31) + (this.b != 0.0f ? Float.floatToIntBits(this.b) : 0);
    }
}
