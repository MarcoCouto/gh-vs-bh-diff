package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.cj.b;
import com.yandex.mobile.ads.impl.hr.a;
import java.util.List;

public final class cl implements b {
    @NonNull
    private final ck a;
    @NonNull
    private final cj b;

    public cl(@NonNull Context context, @NonNull ci ciVar, @NonNull w wVar, @NonNull fl flVar, @Nullable List<String> list) {
        this.a = new ck(context, wVar, flVar, list);
        this.b = new cj(ciVar, this);
    }

    public final void b() {
        this.b.a();
    }

    public final void c() {
        this.b.b();
    }

    public final void a(@NonNull a aVar) {
        this.a.a(aVar);
    }

    public final void a() {
        this.a.a();
    }
}
