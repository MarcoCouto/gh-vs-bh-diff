package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.core.identifiers.ad.gms.service.d;

public final class gw implements gr {
    @NonNull
    private final gx a;
    @NonNull
    private final d b;

    public gw(@NonNull Context context) {
        this.a = new gx(context);
        this.b = new d(context);
    }

    @Nullable
    public final gm a() {
        gm a2 = this.a.a();
        return a2 == null ? this.b.a() : a2;
    }
}
