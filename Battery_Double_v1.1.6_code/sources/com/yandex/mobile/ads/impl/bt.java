package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class bt {
    @NonNull
    private final bz a = new bz();

    @Nullable
    public static String a(@NonNull rr rrVar) {
        if (rrVar.b != null) {
            return bz.a(rrVar.b);
        }
        return null;
    }
}
