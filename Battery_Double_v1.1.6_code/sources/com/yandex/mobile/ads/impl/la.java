package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;

final class la implements kv<String> {
    la() {
    }

    public final /* bridge */ /* synthetic */ boolean a(@NonNull Object obj) {
        return a((String) obj);
    }

    public static boolean a(@NonNull String str) {
        return !(TextUtils.isEmpty(str) || "null".equals(str));
    }
}
