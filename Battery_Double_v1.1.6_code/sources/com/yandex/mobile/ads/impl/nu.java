package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.annotation.NonNull;
import android.support.v4.internal.view.SupportMenu;
import android.widget.FrameLayout;

@SuppressLint({"ViewConstructor"})
public final class nu extends FrameLayout {
    @NonNull
    private final dt a;
    @NonNull
    private Paint b = new Paint();
    private int c;

    public nu(@NonNull Context context, @NonNull dt dtVar) {
        super(context);
        this.a = dtVar;
        int a2 = dt.a(context, 1.0f);
        this.c = dt.a(context, 0.5f);
        this.b.setStyle(Style.STROKE);
        this.b.setStrokeWidth((float) a2);
        this.b.setColor(SupportMenu.CATEGORY_MASK);
        setClickable(false);
        setFocusable(false);
        setWillNotDraw(false);
    }

    public final void setColor(int i) {
        if (this.b.getColor() != i) {
            this.b.setColor(i);
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect((float) this.c, (float) this.c, (float) (getWidth() - this.c), (float) (getHeight() - this.c), this.b);
    }
}
