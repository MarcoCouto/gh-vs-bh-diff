package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.internal.NativeProtocol;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.mediation.base.a;
import com.yandex.mobile.ads.mediation.base.b;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class bc<T extends a, L> {
    @NonNull
    private final fl a;
    @NonNull
    private final be<T, L> b;
    @NonNull
    private final bi c;
    @NonNull
    private final bd<T> d;
    @NonNull
    private final b e = new b();
    @NonNull
    private final bh f;
    @Nullable
    private bb<T> g;

    public bc(@NonNull fl flVar, @NonNull be<T, L> beVar, @NonNull bi biVar, @NonNull bd<T> bdVar, @NonNull bh bhVar) {
        this.a = flVar;
        this.b = beVar;
        this.f = bhVar;
        this.d = bdVar;
        this.c = biVar;
    }

    public final void a(@NonNull Context context) {
        if (this.g != null) {
            try {
                this.b.a(this.g.a());
            } catch (Throwable th) {
                a(context, th, this.g.b());
            }
        }
    }

    public final void a(@NonNull Context context, @NonNull L l) {
        while (true) {
            this.g = this.d.a(context);
            if (this.g != null) {
                bm b2 = this.g.b();
                this.c.a(context, b2);
                try {
                    this.b.a(context, this.g.a(), l, this.g.a(context), this.g.c());
                    return;
                } catch (Throwable th) {
                    a(context, th, b2);
                }
            } else {
                this.f.a();
                return;
            }
        }
    }

    public final void b(@NonNull Context context) {
        a(context, (Map<String, Object>) new HashMap<String,Object>());
    }

    public final void a(@NonNull Context context, @NonNull Map<String, Object> map) {
        if (this.g != null) {
            bm b2 = this.g.b();
            List<String> d2 = b2.d();
            if (d2 != null) {
                for (String a2 : d2) {
                    new de(context, this.a).a(a2);
                }
            }
            HashMap hashMap = new HashMap(map);
            hashMap.put("click_type", "default");
            this.c.a(context, b2, (Map<String, Object>) hashMap);
        }
    }

    public final void b(@NonNull Context context, @NonNull Map<String, Object> map) {
        if (this.g != null) {
            this.c.e(context, this.g.b(), map);
        }
    }

    public final void c(@NonNull Context context) {
        c(context, new HashMap());
    }

    public final void c(@NonNull Context context, @NonNull Map<String, Object> map) {
        if (this.g != null) {
            bm b2 = this.g.b();
            List<String> c2 = b2.c();
            if (c2 != null) {
                for (String a2 : c2) {
                    new de(context, this.a).a(a2);
                }
            }
            this.c.b(context, b2, map);
        }
    }

    private void b(@NonNull Context context, @NonNull L l) {
        a(context);
        a(context, l);
    }

    public final void d(@NonNull Context context) {
        if (this.g != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("status", "success");
            this.c.c(context, this.g.b(), hashMap);
        }
    }

    public final void e(@NonNull Context context) {
        d(context, new HashMap());
    }

    public final void d(@NonNull Context context, @NonNull Map<String, Object> map) {
        if (this.g != null) {
            List<String> e2 = this.g.b().e();
            de deVar = new de(context, this.a);
            if (e2 != null) {
                for (String a2 : e2) {
                    deVar.a(a2);
                }
            }
        }
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.put("status", "success");
        e(context, hashMap);
    }

    private void e(@NonNull Context context, @NonNull Map<String, Object> map) {
        if (this.g != null) {
            map.putAll(b.a(this.g.a()));
            this.c.d(context, this.g.b(), map);
        }
    }

    private void a(@NonNull Context context, @NonNull Throwable th, @NonNull bm bmVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("exception_in_adapter", th.toString());
        HashMap hashMap2 = new HashMap();
        hashMap2.put(IronSourceConstants.EVENTS_ERROR_REASON, hashMap);
        this.c.f(context, bmVar, hashMap2);
    }

    public final void a(@NonNull Context context, @Nullable w<String> wVar) {
        if (this.g != null) {
            this.c.a(context, this.g.b(), (w) wVar);
        }
    }

    public final void a(@NonNull Context context, @NonNull AdRequestError adRequestError, @NonNull L l) {
        HashMap hashMap = new HashMap();
        hashMap.put("status", "error");
        hashMap.put(NativeProtocol.BRIDGE_ARG_ERROR_CODE, Integer.valueOf(adRequestError.getCode()));
        hashMap.put(NativeProtocol.BRIDGE_ARG_ERROR_DESCRIPTION, adRequestError.getDescription());
        e(context, hashMap);
        b(context, l);
    }

    public final void b(@NonNull Context context, @NonNull AdRequestError adRequestError, @NonNull L l) {
        if (this.g != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("status", "error");
            hashMap.put(NativeProtocol.BRIDGE_ARG_ERROR_CODE, Integer.valueOf(adRequestError.getCode()));
            this.c.c(context, this.g.b(), hashMap);
        }
        b(context, l);
    }
}
