package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

final class vn {
    @NonNull
    private final tv a;
    @NonNull
    private final vb b;

    vn(@NonNull Context context, @NonNull vb vbVar) {
        this.b = vbVar;
        this.a = tv.a(context);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull VideoAd videoAd, @NonNull RequestListener<List<VideoAd>> requestListener) {
        this.a.a(context, videoAd, this.b, (RequestListener<List<VideoAd>>) new vo<List<VideoAd>>(videoAd, requestListener));
    }
}
