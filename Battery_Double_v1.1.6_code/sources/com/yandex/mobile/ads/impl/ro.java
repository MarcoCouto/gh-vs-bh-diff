package com.yandex.mobile.ads.impl;

import android.os.Handler;
import java.util.concurrent.Executor;

public final class ro implements rv {
    private final Executor a;

    private class a implements Runnable {
        private final rs b;
        private final ru c;
        private final Runnable d;

        public a(rs rsVar, ru ruVar, Runnable runnable) {
            this.b = rsVar;
            this.c = ruVar;
            this.d = runnable;
        }

        public final void run() {
            if (this.b.j()) {
                this.b.g();
                return;
            }
            if (this.c.c == null) {
                this.b.b(this.c.a);
            } else {
                this.b.b(this.c.c);
            }
            if (!this.c.d) {
                this.b.g();
            }
            if (this.d != null) {
                this.d.run();
            }
        }
    }

    public ro(final Handler handler) {
        this.a = new Executor() {
            public final void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    public final void a(rs<?> rsVar, ru<?> ruVar) {
        a(rsVar, ruVar, null);
    }

    public final void a(rs<?> rsVar, ru<?> ruVar, Runnable runnable) {
        rsVar.r();
        this.a.execute(new a(rsVar, ruVar, runnable));
    }

    public final void a(rs<?> rsVar, sf sfVar) {
        this.a.execute(new a(rsVar, ru.a(sfVar), null));
    }
}
