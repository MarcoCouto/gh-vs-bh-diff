package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Xml;
import com.yandex.mobile.ads.video.models.vmap.AdBreak;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import com.yandex.mobile.ads.video.models.vmap.a;
import com.yandex.mobile.ads.video.models.vmap.d;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class uw {
    @NonNull
    private final uo a = new uo(this.c, this.b);
    @NonNull
    private final ut b = new ut(this.c);
    @NonNull
    private final un c = new un();
    @NonNull
    private final a d = new a();
    @NonNull
    private final uy e = new uy();

    @Nullable
    public final Vmap a(@NonNull String str) throws IOException, XmlPullParserException {
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        newPullParser.setInput(new StringReader(str));
        newPullParser.nextTag();
        return a(newPullParser);
    }

    @Nullable
    private static Vmap a(@NonNull XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        un.a(xmlPullParser, "VMAP");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        String attributeValue = xmlPullParser.getAttributeValue(null, "version");
        while (un.b(xmlPullParser)) {
            if (un.a(xmlPullParser)) {
                String name = xmlPullParser.getName();
                if ("AdBreak".equals(name)) {
                    AdBreak a2 = uo.a(xmlPullParser);
                    if (a2 != null) {
                        arrayList.add(a2);
                    }
                } else if ("Extensions".equals(name)) {
                    arrayList2.addAll(ut.a(xmlPullParser));
                } else {
                    un.d(xmlPullParser);
                }
            }
        }
        if (TextUtils.isEmpty(attributeValue)) {
            return null;
        }
        a.a(arrayList, uy.a(arrayList2));
        return d.a(attributeValue, arrayList, arrayList2);
    }
}
