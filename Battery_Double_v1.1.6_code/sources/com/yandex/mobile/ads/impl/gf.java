package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.metrica.IIdentifierCallback.Reason;

public final class gf {
    @NonNull
    private final gg a = new gg();

    @NonNull
    public static String a() {
        return a("Connection timeout");
    }

    @NonNull
    public static String b() {
        return a("Incorrect integration");
    }

    @NonNull
    static String c() {
        return a("Invalid response");
    }

    @NonNull
    static String a(@NonNull Reason reason) {
        return a(gg.a(reason));
    }

    @NonNull
    private static String a(@NonNull String str) {
        StringBuilder sb = new StringBuilder("Internal state wasn't completely configured. ");
        sb.append(str);
        sb.append(".");
        return sb.toString();
    }
}
