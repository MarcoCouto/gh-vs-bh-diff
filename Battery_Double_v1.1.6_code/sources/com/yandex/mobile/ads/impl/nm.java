package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.HashMap;
import java.util.Map;

public final class nm {
    private static final Map<NativeAdType, nl> a = new HashMap<NativeAdType, nl>() {
        {
            put(NativeAdType.APP_INSTALL, new ni());
            put(NativeAdType.CONTENT, new nj());
            put(NativeAdType.IMAGE, new nk());
        }
    };

    @Nullable
    public static nl a(@NonNull NativeAdType nativeAdType) {
        return (nl) a.get(nativeAdType);
    }
}
