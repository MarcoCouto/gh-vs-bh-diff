package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.nativeads.r;

final class of implements oc<ot> {
    @NonNull
    private final co a;
    @NonNull
    private final r b;

    of(@NonNull co coVar, @NonNull r rVar) {
        this.a = coVar;
        this.b = rVar;
    }

    public final void a(@NonNull Context context, @NonNull ot otVar) {
        this.b.g();
        this.a.a(b.CLOSE);
    }
}
