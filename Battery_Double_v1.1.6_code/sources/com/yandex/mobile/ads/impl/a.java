package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.webkit.WebView;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.VideoController;
import com.yandex.mobile.ads.impl.in.d;
import com.yandex.mobile.ads.mediation.banner.b;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

public final class a extends h {
    @NonNull
    private final c h;
    @NonNull
    private final VideoController i;
    @NonNull
    private final eg j;
    @NonNull
    private final kf k;
    @Nullable
    private e l;
    @Nullable
    private b m;
    @Nullable
    private b n;
    /* access modifiers changed from: private */
    public final OnPreDrawListener o = new OnPreDrawListener() {
        public final boolean onPreDraw() {
            new StringBuilder("onPreDraw(), clazz = ").append(this);
            a.this.C();
            a.this.a.postDelayed(new Runnable() {
                public final void run() {
                    a.this.c(false);
                }
            }, 50);
            return true;
        }
    };

    public final /* bridge */ /* synthetic */ void a(@NonNull Intent intent) {
        super.a(intent);
    }

    public final /* bridge */ /* synthetic */ void b(int i2) {
        super.b(i2);
    }

    public final /* bridge */ /* synthetic */ void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        super.onAdFailedToLoad(adRequestError);
    }

    public a(@NonNull Context context, @NonNull e eVar, @NonNull c cVar) {
        super(context, new b(eVar), com.yandex.mobile.ads.b.BANNER);
        this.h = cVar;
        eVar.setHorizontalScrollBarEnabled(false);
        eVar.setVerticalScrollBarEnabled(false);
        eVar.setVisibility(8);
        eVar.setBackgroundColor(0);
        this.l = eVar;
        this.j = new eg();
        this.i = new VideoController(this.j);
        this.k = new kf();
    }

    @Nullable
    public final e a() {
        return this.l;
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        if (webView != null) {
            final d dVar = (d) webView;
            if (this.l != null && c(dVar.c())) {
                this.l.setVisibility(0);
                this.a.post(new Runnable() {
                    public final void run() {
                        e a2 = a.this.a();
                        if (a2 != null && a2.indexOfChild(dVar) == -1) {
                            LayoutParams a3 = d.a(a.this.b, dVar.c());
                            a.a(a.this, a2, dVar);
                            a2.addView(dVar, a3);
                            ee.a((View) dVar, a.this.o);
                        }
                    }
                });
                super.a(webView, (Map) map);
            }
        }
    }

    private boolean c(@Nullable al alVar) {
        if (alVar != null) {
            al b = this.f.b();
            if (b != null) {
                return a(alVar, b);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        if (this.l == null) {
            return false;
        }
        View findViewById = this.l.findViewById(2);
        if (findViewById == null) {
            return false;
        }
        Rect b = ee.b(findViewById);
        Rect rect = new Rect();
        findViewById.getWindowVisibleDisplayFrame(rect);
        if (!((b.bottom <= rect.top || b.top >= rect.bottom) || (b.right <= rect.left || b.left >= rect.right))) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        if (this.l != null) {
            return ee.b(this.l.findViewById(2), i2);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        w y = y();
        al e = y != null ? y.e() : null;
        return e != null && c(e);
    }

    public final void d() {
        super.d();
        this.h.a((AdEventListener) null);
        if (this.l != null) {
            c(true);
            this.l.setVisibility(8);
            ee.a((View) this.l);
            this.l = null;
        }
    }

    public final void e() {
        a(this.b, this.n, this.m);
        super.e();
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull al alVar) {
        return alVar.b(this.b) >= 0 && alVar.a(this.b) >= 0;
    }

    public final void onAdLoaded() {
        super.onAdLoaded();
        if (this.n != this.m) {
            a(this.b, this.n);
            this.n = this.m;
        }
    }

    private static void a(@NonNull Context context, @NonNull b... bVarArr) {
        for (b bVar : new HashSet(Arrays.asList(bVarArr))) {
            if (bVar != null) {
                bVar.a(context);
            }
        }
    }

    public final void a(@NonNull w<String> wVar) {
        super.a((w) wVar);
        this.m = kf.a(wVar).a(this);
        this.m.a(this.b, wVar);
    }

    public final void f() {
        this.h.a();
    }

    @NonNull
    public final VideoController g() {
        return this.i;
    }

    public final void a(@Nullable AdEventListener adEventListener) {
        super.a((AdEventListener) this.h);
        this.h.a(adEventListener);
    }

    @Nullable
    public final AdEventListener h() {
        return this.h.b();
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        e eVar = this.l;
        if (eVar != null && eVar.getChildCount() > 0) {
            int childCount = eVar.getChildCount() - (z ^ true ? 1 : 0);
            if (childCount > 0) {
                ArrayList arrayList = new ArrayList(childCount);
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = eVar.getChildAt(i2);
                    if (childAt instanceof ab) {
                        arrayList.add((ab) childAt);
                    }
                }
                eVar.removeViews(0, childCount);
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    ((ab) arrayList.get(i3)).g();
                }
                arrayList.clear();
            }
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final iv a(@NonNull String str, @NonNull w<String> wVar, @NonNull al alVar, @NonNull as asVar) {
        d dVar = new d(this.b, wVar, alVar);
        new iw();
        boolean a = iw.a(str);
        iz.a();
        return iz.a(a).a(dVar, this, this.j, asVar);
    }

    static /* synthetic */ void a(a aVar, final e eVar, final d dVar) {
        al c = dVar.c();
        if (c == null || c.c() == com.yandex.mobile.ads.impl.al.a.FIXED) {
            dVar.setVisibility(0);
        } else if (dVar.d != null) {
            eVar.setBackgroundColor(dVar.d.intValue());
        } else {
            aVar.a.postDelayed(new Runnable() {
                public final void run() {
                    Integer num;
                    e eVar = eVar;
                    Bitmap c2 = ee.c((View) dVar);
                    if (c2 == null) {
                        num = Integer.valueOf(0);
                    } else {
                        int width = c2.getWidth();
                        int height = c2.getHeight();
                        int i = width * height;
                        int[] iArr = new int[i];
                        c2.getPixels(iArr, 0, width, 0, 0, width, height);
                        int i2 = 0;
                        int i3 = 0;
                        while (i2 < height) {
                            int i4 = i3;
                            for (int i5 = 0; i5 < width; i5++) {
                                if (Color.alpha(iArr[(i2 * width) + i5]) != 255) {
                                    i4++;
                                }
                            }
                            i2++;
                            i3 = i4;
                        }
                        if (((float) i3) >= ((float) i) * 0.1f) {
                            num = Integer.valueOf(0);
                        } else {
                            int[][][] iArr2 = (int[][][]) Array.newInstance(int.class, new int[]{16, 16, 16});
                            for (int i6 = 0; i6 < height; i6++) {
                                for (int i7 = 0; i7 < width; i7++) {
                                    int i8 = iArr[(i6 * width) + i7];
                                    int red = Color.red(i8) / 16;
                                    int green = Color.green(i8) / 16;
                                    int blue = Color.blue(i8) / 16;
                                    iArr2[red][green][blue] = iArr2[red][green][blue] + 1;
                                }
                            }
                            int i9 = 0;
                            int i10 = 0;
                            int i11 = 0;
                            while (i9 < 16) {
                                int i12 = i11;
                                int i13 = i10;
                                int i14 = 0;
                                while (i14 < 16) {
                                    int i15 = i13;
                                    for (int i16 = 0; i16 < 16; i16++) {
                                        int i17 = iArr2[i16][i14][i9];
                                        if (i17 > i12) {
                                            i15 = Color.rgb(i16 * 16, i14 * 16, i9 * 16);
                                            i12 = i17;
                                        }
                                    }
                                    i14++;
                                    i13 = i15;
                                }
                                i9++;
                                i10 = i13;
                                i11 = i12;
                            }
                            num = Integer.valueOf(i10);
                        }
                    }
                    eVar.setBackgroundColor(num.intValue());
                    dVar.setVisibility(0);
                }
            }, 200);
        }
    }
}
