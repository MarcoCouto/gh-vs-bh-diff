package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.tx.b;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class ub {
    @NonNull
    private final vg a = new vg();

    @NonNull
    public final rs<Vmap> a(@NonNull Context context, @NonNull fl flVar, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull RequestListener<Vmap> requestListener) {
        String pageId = vmapRequestConfiguration.getPageId();
        String categoryId = vmapRequestConfiguration.getCategoryId();
        String d = flVar.d();
        String f = flVar.f();
        if (TextUtils.isEmpty(f)) {
            f = "https://mobile.yandexadexchange.net";
        }
        Builder buildUpon = Uri.parse(f).buildUpon();
        buildUpon.appendPath("v1").appendPath("vmap").appendPath(pageId).appendQueryParameter("video-category-id", categoryId).appendQueryParameter("uuid", d).build();
        new ty(context, flVar).a(context, buildUpon);
        uj ujVar = new uj(context, buildUpon.build().toString(), new b(requestListener), vmapRequestConfiguration, this.a);
        return ujVar;
    }
}
