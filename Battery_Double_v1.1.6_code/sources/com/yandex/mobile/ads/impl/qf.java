package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.z;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public final class qf {
    @NonNull
    private final DecimalFormat a;

    public qf() {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.US);
        decimalFormatSymbols.setGroupingSeparator(' ');
        this.a = new DecimalFormat("#,###,###", decimalFormatSymbols);
    }

    @NonNull
    public final String a(@NonNull String str) throws z {
        String str2;
        try {
            if (!TextUtils.isEmpty(str)) {
                str2 = str.replaceAll(" ", "");
            } else {
                str2 = str;
            }
            return this.a.format(Integer.valueOf(str2));
        } catch (NumberFormatException e) {
            String.format("Could not parse review count value. Review Count value is %s", new Object[]{str});
            new Object[1][0] = e;
            throw new z("Native Ad json has not required attributes");
        }
    }
}
