package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.PopupMenu.OnMenuItemClickListener;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.impl.ov.a;
import com.yandex.mobile.ads.nativeads.r;
import java.util.List;

@TargetApi(11)
final class qs implements OnMenuItemClickListener {
    @NonNull
    private final co a;
    @NonNull
    private final de b;
    @NonNull
    private final List<a> c;
    @NonNull
    private final r d;

    qs(@NonNull de deVar, @NonNull List<a> list, @NonNull co coVar, @NonNull r rVar) {
        this.c = list;
        this.b = deVar;
        this.a = coVar;
        this.d = rVar;
    }

    public final boolean onMenuItemClick(@NonNull MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId < this.c.size()) {
            this.b.a(((a) this.c.get(itemId)).b());
            this.a.a(b.FEEDBACK);
            this.d.g();
        }
        return true;
    }
}
