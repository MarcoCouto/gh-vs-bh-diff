package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import org.json.JSONException;
import org.json.JSONObject;

final class pu implements pq<ow> {
    @NonNull
    private final pk a;

    public pu(@NonNull pk pkVar) {
        this.a = pkVar;
    }

    @NonNull
    public final /* synthetic */ ot a(@NonNull JSONObject jSONObject) throws JSONException, z {
        return new ow(pi.a(jSONObject, "type"), pi.a(jSONObject, SettingsJsonConstants.APP_ICON_KEY), pi.a(jSONObject, "title"), this.a.a(jSONObject, "url"));
    }
}
