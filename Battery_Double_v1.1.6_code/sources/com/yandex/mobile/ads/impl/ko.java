package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public abstract class ko<T, V> {
    private static final on a = null;

    @NonNull
    public abstract oj<T> a(@NonNull String str, @NonNull V v);

    @NonNull
    protected static oj<T> a(@NonNull String str, @NonNull String str2, @NonNull T t) {
        oj ojVar = new oj(str, str2, t, a, false, false);
        return ojVar;
    }
}
