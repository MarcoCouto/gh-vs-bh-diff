package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.gk.a;
import com.yandex.mobile.ads.video.BlocksInfoRequest;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import com.yandex.mobile.ads.video.tracking.Tracker.ErrorListener;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class th {
    private static final Object a = new Object();
    private static volatile th b;
    /* access modifiers changed from: private */
    @NonNull
    public final fl c = new fl(b.VASTVIDEO);
    /* access modifiers changed from: private */
    @NonNull
    public final gk d = new gk(this.c);
    /* access modifiers changed from: private */
    @NonNull
    public tv e;
    /* access modifiers changed from: private */
    @NonNull
    public final te f = new te(this.c);
    @NonNull
    private final Executor g = Executors.newSingleThreadExecutor(new dx("YandexMobileAds.VideoAdsImpl"));
    /* access modifiers changed from: private */
    @NonNull
    public final tu h = new tu();
    /* access modifiers changed from: private */
    @NonNull
    public final ts i = new ts();
    /* access modifiers changed from: private */
    @NonNull
    public final gq j = new gq();

    private th(@Nullable Context context) {
        this.e = tv.a(context);
    }

    @NonNull
    public static th a(@Nullable Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new th(context);
                }
            }
        }
        return b;
    }

    public final void a(@NonNull final Context context, @NonNull final VmapRequestConfiguration vmapRequestConfiguration, @NonNull gj gjVar, @NonNull final RequestListener<Vmap> requestListener) {
        a(context, gjVar, (td) new td() {
            public final void a() {
                th.this.e.a(context, th.this.c, vmapRequestConfiguration, requestListener);
            }

            public final void a(@NonNull VideoAdError videoAdError) {
                requestListener.onFailure(videoAdError);
            }
        });
    }

    public final void a(@NonNull final Context context, @NonNull final VastRequestConfiguration vastRequestConfiguration, @NonNull gj gjVar, @NonNull final RequestListener<tl> requestListener) {
        a(context, gjVar, (td) new td() {
            public final void a() {
                th.this.i;
                th.this.e.a(context, th.this.c, vastRequestConfiguration, (RequestListener<tl>) ts.a(context, vastRequestConfiguration, requestListener));
            }

            public final void a(@NonNull VideoAdError videoAdError) {
                requestListener.onFailure(videoAdError);
            }
        });
    }

    public final void a(@NonNull Context context, @NonNull final BlocksInfoRequest blocksInfoRequest, @NonNull gj gjVar) {
        a(context, gjVar, (td) new td() {
            public final void a() {
                th.this.e.a(blocksInfoRequest, th.this.c);
            }

            public final void a(@NonNull VideoAdError videoAdError) {
                RequestListener requestListener = blocksInfoRequest.getRequestListener();
                if (requestListener != null) {
                    requestListener.onFailure(videoAdError);
                }
            }
        });
    }

    public final void a(@NonNull Context context, @NonNull final VideoAdRequest videoAdRequest, @NonNull gj gjVar) {
        a(context, gjVar, (td) new td() {
            public final void a() {
                th.this.h;
                th.this.e.a(tu.a(videoAdRequest), th.this.c);
            }

            public final void a(@NonNull VideoAdError videoAdError) {
                RequestListener requestListener = videoAdRequest.getRequestListener();
                if (requestListener != null) {
                    requestListener.onFailure(videoAdError);
                }
            }
        });
    }

    private void a(@NonNull final Context context, @NonNull final gj gjVar, @NonNull final td tdVar) {
        this.g.execute(new Runnable() {
            public final void run() {
                th.this.d.a(gjVar, (a) new a() {
                    public final void a() {
                        th.this.g.execute(new Runnable(context, tdVar) {
                            public final void run() {
                                th.this.j.a(r3, new gv() {
                                    public final void a(@Nullable go goVar) {
                                        if (goVar != null) {
                                            th.this.c.a(goVar.a());
                                            th.this.c.b(goVar.b());
                                        }
                                        th.this.g.execute(new Runnable(r3, r4) {
                                            public final void run() {
                                                th.this.f.a(r3, r4);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    public final void a(@NonNull String str) {
                        tdVar.a(VideoAdError.createInternalError(str));
                    }
                });
            }
        });
    }

    public final void a(@NonNull String str, @NonNull ErrorListener errorListener) {
        this.e.a(str, errorListener);
    }
}
