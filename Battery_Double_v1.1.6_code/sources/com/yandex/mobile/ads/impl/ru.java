package com.yandex.mobile.ads.impl;

public final class ru<T> {
    public final T a;
    public final com.yandex.mobile.ads.impl.rl.a b;
    public final sf c;
    public boolean d;

    public interface a {
        void a(sf sfVar);
    }

    public interface b<T> {
        void a(T t);
    }

    public static <T> ru<T> a(T t, com.yandex.mobile.ads.impl.rl.a aVar) {
        return new ru<>(t, aVar);
    }

    public static <T> ru<T> a(sf sfVar) {
        return new ru<>(sfVar);
    }

    private ru(T t, com.yandex.mobile.ads.impl.rl.a aVar) {
        this.d = false;
        this.a = t;
        this.b = aVar;
        this.c = null;
    }

    private ru(sf sfVar) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = sfVar;
    }
}
