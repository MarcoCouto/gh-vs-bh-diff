package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import java.util.HashMap;

public final class vg implements hs<VmapRequestConfiguration, Vmap> {
    public final /* synthetic */ hr a(@Nullable ru ruVar, int i, @NonNull Object obj) {
        VmapRequestConfiguration vmapRequestConfiguration = (VmapRequestConfiguration) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("page_id", vmapRequestConfiguration.getPageId());
        hashMap.put("category_id", vmapRequestConfiguration.getCategoryId());
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new hr(b.VMAP_RESPONSE, hashMap);
    }

    public final /* synthetic */ hr a(Object obj) {
        VmapRequestConfiguration vmapRequestConfiguration = (VmapRequestConfiguration) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("page_id", vmapRequestConfiguration.getPageId());
        hashMap.put("category_id", vmapRequestConfiguration.getCategoryId());
        return new hr(b.VMAP_REQUEST, hashMap);
    }
}
