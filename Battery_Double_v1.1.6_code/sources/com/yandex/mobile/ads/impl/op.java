package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;

public final class op {
    @Nullable
    private final oo a;
    @Nullable
    private final om b;

    public op(@Nullable oo ooVar, @Nullable om omVar) {
        this.a = ooVar;
        this.b = omVar;
    }

    @Nullable
    public final oo a() {
        return this.a;
    }

    @Nullable
    public final om b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof op)) {
            return false;
        }
        op opVar = (op) obj;
        if (this.a == null ? opVar.a != null : !this.a.equals(opVar.a)) {
            return false;
        }
        if (this.b != null) {
            return this.b.equals(opVar.b);
        }
        return opVar.b == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
