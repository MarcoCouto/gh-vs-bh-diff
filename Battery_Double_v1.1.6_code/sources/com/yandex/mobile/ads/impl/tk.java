package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class tk implements Parcelable {
    public static final Creator<tk> CREATOR = new Creator<tk>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new tk[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new tk(parcel);
        }
    };
    @NonNull
    private final String a;
    @NonNull
    private final List<VideoAd> b;

    public final int describeContents() {
        return 0;
    }

    public tk(@NonNull String str, @NonNull List<VideoAd> list) {
        this.a = str;
        this.b = list;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    @NonNull
    public final List<VideoAd> b() {
        return this.b;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeTypedList(this.b);
    }

    protected tk(@NonNull Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.createTypedArrayList(VideoAd.CREATOR);
    }
}
