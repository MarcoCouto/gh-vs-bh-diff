package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import com.yandex.mobile.ads.nativeads.bj;
import com.yandex.mobile.ads.nativeads.d;
import com.yandex.mobile.ads.nativeads.i;
import com.yandex.mobile.ads.nativeads.p;
import com.yandex.mobile.ads.nativeads.t;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class nd {
    @NonNull
    private final Executor a = Executors.newSingleThreadExecutor(new dx("YandexMobileAds.BaseController"));
    /* access modifiers changed from: private */
    @NonNull
    public final fl b;
    /* access modifiers changed from: private */
    @NonNull
    public final ne c;
    /* access modifiers changed from: private */
    @NonNull
    public final nh d;
    /* access modifiers changed from: private */
    @NonNull
    public final NativeAdLoaderConfiguration e;

    @VisibleForTesting
    class a implements Runnable {
        @NonNull
        private final t b;
        @NonNull
        private final WeakReference<Context> c;
        @NonNull
        private final w<or> d;
        @NonNull
        private final nc e;

        a(Context context, @NonNull w<or> wVar, @NonNull t tVar, @NonNull nc ncVar) {
            this.d = wVar;
            this.b = tVar;
            this.c = new WeakReference<>(context);
            this.e = ncVar;
        }

        public final void run() {
            Context context = (Context) this.c.get();
            if (context != null) {
                try {
                    or orVar = (or) this.d.p();
                    if (orVar == null) {
                        this.e.a(u.e);
                        return;
                    }
                    if (hz.a(orVar.c())) {
                        this.e.a(u.j);
                        return;
                    }
                    p pVar = new p(orVar, this.d, nd.this.b);
                    nc ncVar = this.e;
                    if (nd.this.e.shouldLoadImagesAutomatically()) {
                        nd.this.d.a(context, pVar, new bj(), this.b, ncVar);
                        return;
                    }
                    nd.this.c.a(context, pVar, new d(context), this.b, ncVar);
                } catch (Exception unused) {
                    this.e.a(u.e);
                }
            }
        }
    }

    public nd(@NonNull Context context, @NonNull fl flVar, @NonNull NativeAdLoaderConfiguration nativeAdLoaderConfiguration) {
        this.b = flVar;
        this.e = nativeAdLoaderConfiguration;
        this.c = new ne(flVar);
        this.d = new nh(this.c, new i(context));
    }

    public final void a(@NonNull Context context, @NonNull w<or> wVar, @NonNull t tVar, @NonNull nc ncVar) {
        Executor executor = this.a;
        a aVar = new a(context, wVar, tVar, ncVar);
        executor.execute(aVar);
    }
}
