package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.sv.a;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class uh extends sw<VideoAd, List<VideoAd>> {
    @NonNull
    private final ul a = new ul();

    public uh(@NonNull Context context, @NonNull String str, @NonNull a<List<VideoAd>> aVar, @NonNull VideoAd videoAd, @NonNull hs<VideoAd, List<VideoAd>> hsVar) {
        super(context, 0, str, aVar, videoAd, hsVar);
    }

    /* access modifiers changed from: protected */
    public final ru<List<VideoAd>> a(@NonNull rr rrVar, int i) {
        tl a2 = this.a.a(rrVar);
        if (a2 == null) {
            return ru.a(new to("Can't parse VAST response."));
        }
        List b = a2.a().b();
        if (b.isEmpty()) {
            return ru.a(new tn());
        }
        return ru.a(b, null);
    }
}
