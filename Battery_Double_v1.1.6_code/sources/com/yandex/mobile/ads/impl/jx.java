package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class jx {
    @NonNull
    public static jw a(@NonNull w wVar, @NonNull View view, boolean z) {
        if (z) {
            return new jz(wVar, view, new ka());
        }
        return new jy(view);
    }
}
