package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;

public final class df {
    @NonNull
    private final de a;
    @NonNull
    private final ct b = new ct();
    @NonNull
    private final hp c;
    @NonNull
    private final fl d;
    @NonNull
    private final di e;
    @Nullable
    private a f;
    @Nullable
    private com.yandex.mobile.ads.impl.hr.a g;
    private long h;

    public enum a {
        BROWSER("browser"),
        WEBVIEW(ParametersKeys.WEB_VIEW);
        
        final String c;

        private a(String str) {
            this.c = str;
        }
    }

    df(@NonNull Context context, @NonNull fl flVar, @NonNull di diVar) {
        this.d = flVar;
        this.e = diVar;
        this.a = new de(context, flVar);
        this.c = hp.a(context);
    }

    public final void a(@Nullable com.yandex.mobile.ads.impl.hr.a aVar) {
        this.g = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull a aVar) {
        new StringBuilder("startActivityInteraction, type = ").append(aVar);
        this.h = System.currentTimeMillis();
        this.f = aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable a aVar) {
        new StringBuilder("finishActivityInteraction, type = ").append(aVar);
        if (this.h != 0 && this.f == aVar) {
            long currentTimeMillis = System.currentTimeMillis() - this.h;
            String str = currentTimeMillis < 1000 ? "<1" : (currentTimeMillis <= 1000 || currentTimeMillis > AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS) ? (currentTimeMillis <= AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS || currentTimeMillis > 3000) ? (currentTimeMillis <= 3000 || currentTimeMillis > DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) ? (currentTimeMillis <= DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS || currentTimeMillis > TapjoyConstants.TIMER_INCREMENT) ? (currentTimeMillis <= TapjoyConstants.TIMER_INCREMENT || currentTimeMillis > 15000) ? (currentTimeMillis <= 15000 || currentTimeMillis > 20000) ? ">20" : "15-20" : "10-15" : "5-10" : "3-5" : "2-3" : "1-2";
            HashMap hashMap = new HashMap();
            hashMap.put("type", aVar.c);
            hashMap.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, this.d.a().a());
            hashMap.put("block_id", this.d.e());
            hashMap.put(String.INTERVAL, str);
            hashMap.putAll(ct.a(this.d.c()));
            if (this.g != null) {
                hashMap.putAll(this.g.a());
            }
            this.c.a(new hr(b.RETURNED_TO_APP, hashMap));
            Object[] objArr = {aVar.c, str};
            if (currentTimeMillis <= this.e.b()) {
                this.a.a(this.e.a());
            }
            this.h = 0;
            this.f = null;
        }
    }
}
