package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

final class qo {
    @NonNull
    private final ra a;
    @NonNull
    private final qn b = new qn();

    qo(@NonNull Context context) {
        this.a = new ra(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final Intent a(@NonNull List<oy> list) {
        for (oy a2 : list) {
            Intent a3 = qn.a(a2);
            if (this.a.a(a3)) {
                return a3;
            }
        }
        return null;
    }
}
