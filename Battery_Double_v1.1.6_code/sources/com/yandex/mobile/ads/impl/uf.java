package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.sv.a;
import com.yandex.mobile.ads.video.VastRequestConfiguration;

public final class uf extends sw<VastRequestConfiguration, tl> {
    @NonNull
    private final ul a = new ul();

    public uf(@NonNull Context context, @NonNull String str, @NonNull a<tl> aVar, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull hs<VastRequestConfiguration, tl> hsVar) {
        super(context, 0, str, aVar, vastRequestConfiguration, hsVar);
        new Object[1][0] = str;
    }

    /* access modifiers changed from: protected */
    public final ru<tl> a(@NonNull rr rrVar, int i) {
        tl a2 = this.a.a(rrVar);
        if (a2 == null) {
            return ru.a(new to("Can't parse VAST response."));
        }
        if (!a2.a().b().isEmpty()) {
            return ru.a(a2, null);
        }
        return ru.a(new tn());
    }
}
