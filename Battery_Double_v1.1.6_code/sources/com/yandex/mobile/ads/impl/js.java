package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.InterstitialEventListener;
import com.yandex.mobile.ads.b;

public final class js extends ij {
    @NonNull
    private final jr h;

    public js(@NonNull Context context, @NonNull jr jrVar) {
        super(context, b.INTERSTITIAL, jrVar);
        this.h = jrVar;
    }

    @Nullable
    public final InterstitialEventListener E() {
        return this.h.h();
    }

    public final void a(@Nullable InterstitialEventListener interstitialEventListener) {
        this.h.a(interstitialEventListener);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final kj a(@NonNull kk kkVar) {
        return kkVar.a((ij) this);
    }
}
