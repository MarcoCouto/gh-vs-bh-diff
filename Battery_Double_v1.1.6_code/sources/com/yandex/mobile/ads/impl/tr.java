package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class tr implements RequestListener<tl> {
    /* access modifiers changed from: private */
    @NonNull
    public final va a;
    @NonNull
    private final tw b;
    @NonNull
    private final RequestListener<tl> c;

    class a implements RequestListener<List<VideoAd>> {
        @NonNull
        private final tl b;
        @NonNull
        private final RequestListener<tl> c;

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            List list = (List) obj;
            tr.this.a.a();
            this.c.onSuccess(new tl(new tk(this.b.a().a(), list), this.b.b()));
        }

        a(tl tlVar, @NonNull RequestListener<tl> requestListener) {
            this.b = tlVar;
            this.c = requestListener;
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            tr.this.a.a(videoAdError);
            this.c.onFailure(videoAdError);
        }
    }

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        tl tlVar = (tl) obj;
        this.b.a(tlVar.a().b(), new a(tlVar, this.c));
    }

    tr(@NonNull Context context, @NonNull vb vbVar, @NonNull RequestListener<tl> requestListener) {
        this.c = requestListener;
        this.a = new va(context, vbVar);
        this.b = new tw(context, vbVar);
    }

    public final void onFailure(@NonNull VideoAdError videoAdError) {
        this.c.onFailure(videoAdError);
    }
}
