package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.nativeads.av.a;

public final class ok {
    private final String a;
    private final a b;

    public ok(a aVar, String str) {
        this.b = aVar;
        this.a = str;
    }

    public final String a() {
        return this.a;
    }

    public final a b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ok okVar = (ok) obj;
        if (this.a == null ? okVar.a == null : this.a.equals(okVar.a)) {
            return this.b == okVar.b;
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
