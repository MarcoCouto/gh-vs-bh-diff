package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;
import android.widget.FrameLayout;

final class np {
    @NonNull
    private final ns a = new ns();
    @NonNull
    private final nv b = new nv();
    @NonNull
    private final dt c = new dt();

    np() {
    }

    private static void b(@NonNull FrameLayout frameLayout) {
        View findViewById = frameLayout.findViewById(7845);
        if (findViewById != null) {
            frameLayout.removeView(findViewById);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull am amVar, @NonNull FrameLayout frameLayout, boolean z) {
        nu nuVar = (nu) frameLayout.findViewById(7846);
        if (nuVar == null) {
            nuVar = new nu(frameLayout.getContext(), this.c);
            nuVar.setId(7846);
            frameLayout.addView(nuVar);
        }
        nuVar.setColor(z ? SupportMenu.CATEGORY_MASK : -16711936);
        if (z) {
            nx nxVar = (nx) frameLayout.findViewById(7845);
            if (nxVar == null) {
                nxVar = new nx(frameLayout.getContext());
                nxVar.setId(7845);
                frameLayout.addView(nxVar);
            }
            nxVar.setDescription(this.a.a(amVar));
            return;
        }
        b(frameLayout);
    }

    static void a(@NonNull FrameLayout frameLayout) {
        View findViewById = frameLayout.findViewById(7846);
        if (findViewById != null) {
            frameLayout.removeView(findViewById);
        }
        b(frameLayout);
    }
}
