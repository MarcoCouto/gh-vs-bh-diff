package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.video.models.common.Extension;
import com.yandex.mobile.ads.video.models.common.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class ut {
    @NonNull
    private final un a;
    @NonNull
    private final us b;

    public ut(@NonNull un unVar) {
        this.a = unVar;
        this.b = new us(unVar);
    }

    @NonNull
    static List<Extension> a(@NonNull XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        un.a(xmlPullParser, "Extensions");
        ArrayList arrayList = new ArrayList();
        while (un.b(xmlPullParser)) {
            if (un.a(xmlPullParser)) {
                if ("Extension".equals(xmlPullParser.getName())) {
                    un.a(xmlPullParser, "Extension");
                    Extension extension = null;
                    String attributeValue = xmlPullParser.getAttributeValue(null, "type");
                    String c = un.c(xmlPullParser);
                    if (!TextUtils.isEmpty(attributeValue) && !TextUtils.isEmpty(c)) {
                        extension = a.a(attributeValue, c);
                    }
                    if (extension != null) {
                        arrayList.add(extension);
                    }
                } else {
                    un.d(xmlPullParser);
                }
            }
        }
        return arrayList;
    }
}
