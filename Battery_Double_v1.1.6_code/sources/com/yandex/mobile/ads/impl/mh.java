package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.e;

public final class mh implements mj {
    @NonNull
    private final ag a;
    @NonNull
    private final e b;

    public final void a(@NonNull oj ojVar, @NonNull View view) {
    }

    public mh(@NonNull ag agVar, @NonNull e eVar) {
        this.a = agVar;
        this.b = eVar;
    }

    public final void a(@NonNull oj ojVar, @NonNull mq mqVar) {
        this.b.a(ojVar, ojVar.d(), this.a, mqVar);
    }
}
