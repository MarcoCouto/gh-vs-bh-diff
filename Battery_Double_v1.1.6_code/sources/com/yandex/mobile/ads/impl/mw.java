package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import com.yandex.mobile.ads.nativeads.a;
import com.yandex.mobile.ads.nativeads.ag;

public final class mw implements OnClickListener {
    @NonNull
    private final oj a;
    @NonNull
    private final a b;
    @NonNull
    private final ag c;
    @Nullable
    private final on d;
    @Nullable
    private final ax e;

    public mw(@NonNull oj ojVar, @NonNull a aVar, @NonNull ag agVar, @Nullable on onVar, @Nullable ax axVar) {
        this.a = ojVar;
        this.b = aVar;
        this.c = agVar;
        this.d = onVar;
        this.e = axVar;
    }

    public final void onClick(View view) {
        if (this.d != null && this.a.e()) {
            if (this.e != null) {
                this.e.c();
            }
            this.b.a(view.getContext(), this.d, this.c);
        }
    }
}
