package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

final class jy implements jw {
    @NonNull
    private final View a;

    public final void a(boolean z) {
    }

    public final void b() {
    }

    public final void c() {
    }

    public final boolean d() {
        return false;
    }

    jy(@NonNull View view) {
        this.a = view;
        this.a.setVisibility(0);
    }

    @NonNull
    public final View a() {
        return this.a;
    }
}
