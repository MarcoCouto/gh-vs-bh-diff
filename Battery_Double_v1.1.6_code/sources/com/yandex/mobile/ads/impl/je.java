package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import java.lang.ref.WeakReference;

public class je extends it {
    final ex a;

    static class a implements fa {
        @NonNull
        private final WeakReference<el> a;

        a(@NonNull el elVar) {
            this.a = new WeakReference<>(elVar);
        }

        public final void a() {
            el elVar = (el) this.a.get();
            if (elVar != null) {
                elVar.onAdLoaded();
            }
        }

        public final void a(@NonNull String str) {
            el elVar = (el) this.a.get();
            if (elVar != null) {
                elVar.a(str);
            }
        }
    }

    public je(@NonNull ep epVar, @NonNull el elVar) {
        super(epVar);
        this.a = new ex(epVar);
        this.a.a((fa) new a(elVar));
    }

    public final void a(@NonNull String str) {
        this.a.a(str);
    }

    public final void a(@NonNull el elVar) {
        super.a((el) new jd(this.a, elVar));
    }

    public final void b() {
        super.b();
        this.a.c();
    }
}
