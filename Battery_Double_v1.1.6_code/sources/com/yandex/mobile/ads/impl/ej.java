package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ej extends WebViewClient {
    private final ek a;

    public ej(@NonNull ek ekVar) {
        this.a = ekVar;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.a.d();
    }

    public boolean shouldOverrideUrlLoading(@NonNull WebView webView, @NonNull String str) {
        this.a.a(webView.getContext(), str);
        return true;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        Object[] objArr = {Integer.valueOf(i), str};
    }
}
