package com.yandex.mobile.ads.impl;

import android.os.SystemClock;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.yandex.mobile.ads.impl.rl.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.cookie.DateUtils;

public final class sg implements rp {
    protected static final boolean a = rx.b;
    protected final sk b;
    protected final sh c;

    public sg(sk skVar) {
        this(skVar, new sh());
    }

    private sg(sk skVar, sh shVar) {
        this.b = skVar;
        this.c = shVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01d3, code lost:
        a("socket", r2, new com.yandex.mobile.ads.impl.se());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0090, code lost:
        r14 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ca, code lost:
        r13 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0111, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0113, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0114, code lost:
        r18 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0116, code lost:
        r13 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0118, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0119, code lost:
        r18 = r11;
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x011c, code lost:
        r14 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0123, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0124, code lost:
        r14 = r5;
        r10 = null;
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0129, code lost:
        r0 = r10.getStatusLine().getStatusCode();
        com.yandex.mobile.ads.impl.rx.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r21.b());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0144, code lost:
        if (r13 != null) goto L_0x0146;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0146, code lost:
        r11 = new com.yandex.mobile.ads.impl.rr(r0, r13, r14, false, android.os.SystemClock.elapsedRealtime() - r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0156, code lost:
        if (r0 == 401) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0161, code lost:
        if (r0 < 400) goto L_0x016c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x016b, code lost:
        throw new com.yandex.mobile.ads.impl.rz(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x016c, code lost:
        if (r0 < 500) goto L_0x018a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0176, code lost:
        if (r21.n() != false) goto L_0x0178;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0178, code lost:
        a("server", r2, new com.yandex.mobile.ads.impl.sd(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0189, code lost:
        throw new com.yandex.mobile.ads.impl.sd(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x018f, code lost:
        throw new com.yandex.mobile.ads.impl.sd(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0190, code lost:
        a("auth", r2, new com.yandex.mobile.ads.impl.ry(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x019c, code lost:
        a("network", r2, new com.yandex.mobile.ads.impl.sa());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01ad, code lost:
        throw new com.yandex.mobile.ads.impl.sb(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ae, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01af, code lost:
        r4 = new java.lang.StringBuilder("Bad URL ");
        r4.append(r21.b());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01c6, code lost:
        throw new java.lang.RuntimeException(r4.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01c7, code lost:
        a("connection", r2, new com.yandex.mobile.ads.impl.se());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:101:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x0010] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01a8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01ae A[ExcHandler: MalformedURLException (r0v2 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x0010] */
    /* JADX WARNING: Removed duplicated region for block: B:99:? A[ExcHandler: ConnectTimeoutException (unused org.apache.http.conn.ConnectTimeoutException), SYNTHETIC, Splitter:B:2:0x0010] */
    public final rr a(rs<?> rsVar) throws sf {
        HttpResponse httpResponse;
        Map a2;
        byte[] bArr;
        rs<?> rsVar2 = rsVar;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            Map emptyMap = Collections.emptyMap();
            try {
                HashMap hashMap = new HashMap();
                a h = rsVar.h();
                if (h != null) {
                    if (h.b != null) {
                        hashMap.put(HttpRequest.HEADER_IF_NONE_MATCH, h.b);
                    }
                    if (h.d > 0) {
                        hashMap.put("If-Modified-Since", DateUtils.formatDate(new Date(h.d)));
                    }
                }
                httpResponse = this.b.a(rsVar2, hashMap);
                StatusLine statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                a2 = a(httpResponse.getAllHeaders());
                if (statusCode == 304) {
                    a h2 = rsVar.h();
                    if (h2 == null) {
                        rr rrVar = new rr(ErrorCode.INLINE_AD_DISPLAY_TIMEOUT_ERROR, null, a2, true, SystemClock.elapsedRealtime() - elapsedRealtime);
                        return rrVar;
                    }
                    h2.g.putAll(a2);
                    rr rrVar2 = new rr(ErrorCode.INLINE_AD_DISPLAY_TIMEOUT_ERROR, h2.a, h2.g, true, SystemClock.elapsedRealtime() - elapsedRealtime);
                    return rrVar2;
                }
                if (httpResponse.getEntity() != null) {
                    bArr = a(httpResponse.getEntity());
                } else {
                    bArr = new byte[0];
                }
                long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                if (a || elapsedRealtime2 > 3000) {
                    String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
                    Object[] objArr = new Object[5];
                    objArr[0] = rsVar2;
                    objArr[1] = Long.valueOf(elapsedRealtime2);
                    objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
                    objArr[3] = Integer.valueOf(statusLine.getStatusCode());
                    objArr[4] = Integer.valueOf(rsVar.q().b());
                    rx.b(str, objArr);
                }
                if (statusCode < 200 || statusCode >= 300) {
                    Map map = a2;
                } else {
                    Map map2 = a2;
                    r11 = r11;
                    rr rrVar3 = new rr(statusCode, bArr, map2, false, SystemClock.elapsedRealtime() - elapsedRealtime);
                    return rrVar3;
                }
            } catch (SocketTimeoutException unused) {
            } catch (ConnectTimeoutException unused2) {
            } catch (MalformedURLException e) {
            } catch (IOException e2) {
                e = e2;
                Map map3 = emptyMap;
                byte[] bArr2 = null;
                if (httpResponse != null) {
                }
            }
        }
        Map map4 = a2;
        throw new IOException();
    }

    private static void a(String str, rs<?> rsVar, sf sfVar) throws sf {
        rw q = rsVar.q();
        int p = rsVar.p();
        try {
            q.a(sfVar);
            String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(p)});
        } catch (sf e) {
            String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(p)});
            throw e;
        }
    }

    private byte[] a(HttpEntity httpEntity) throws IOException, sd {
        sp spVar = new sp(this.c, (int) httpEntity.getContentLength());
        byte[] bArr = null;
        try {
            InputStream content = httpEntity.getContent();
            if (content != null) {
                byte[] a2 = this.c.a(1024);
                while (true) {
                    try {
                        int read = content.read(a2);
                        if (read == -1) {
                            break;
                        }
                        spVar.write(a2, 0, read);
                    } catch (Throwable th) {
                        th = th;
                        bArr = a2;
                        try {
                            httpEntity.consumeContent();
                        } catch (IOException unused) {
                            rx.a("Error occured when calling consumingContent", new Object[0]);
                        }
                        this.c.a(bArr);
                        spVar.close();
                        throw th;
                    }
                }
                byte[] byteArray = spVar.toByteArray();
                try {
                    httpEntity.consumeContent();
                } catch (IOException unused2) {
                    rx.a("Error occured when calling consumingContent", new Object[0]);
                }
                this.c.a(a2);
                spVar.close();
                return byteArray;
            }
            throw new sd();
        } catch (Throwable th2) {
            th = th2;
            httpEntity.consumeContent();
            this.c.a(bArr);
            spVar.close();
            throw th;
        }
    }

    private static Map<String, String> a(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headerArr.length; i++) {
            treeMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return treeMap;
    }
}
