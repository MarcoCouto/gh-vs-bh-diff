package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

final class ky implements kv<op> {
    @NonNull
    private final kx a = new kx();
    @NonNull
    private final kz b = new kz();

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038  */
    public final /* synthetic */ boolean a(@NonNull Object obj) {
        boolean z;
        op opVar = (op) obj;
        om b2 = opVar.b();
        oo a2 = opVar.a();
        boolean z2 = b2 != null && kx.a(b2);
        if (a2 != null) {
            if (a2.b() > 0.0f) {
                z = true;
                if (b2 != null || a2 == null) {
                    if (b2 == null) {
                        return z2;
                    }
                    if (a2 != null) {
                        return z;
                    }
                    return false;
                } else if (!z || !z2) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        z = false;
        if (b2 != null) {
        }
        if (b2 == null) {
        }
    }

    ky() {
    }
}
