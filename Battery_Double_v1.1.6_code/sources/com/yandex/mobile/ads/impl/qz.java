package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class qz implements cz<or> {
    private final cd<or> a;

    public final boolean a() {
        return true;
    }

    public qz(@NonNull Context context) {
        this.a = new ph(context);
    }

    @Nullable
    public final /* synthetic */ Object a(@NonNull rr rrVar) {
        return (or) this.a.b(rrVar);
    }
}
