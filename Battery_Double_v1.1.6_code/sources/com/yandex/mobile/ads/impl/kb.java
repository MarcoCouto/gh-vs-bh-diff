package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.impl.in.b;
import com.yandex.mobile.ads.impl.in.c;
import com.yandex.mobile.ads.impl.in.d;
import com.yandex.mobile.ads.impl.in.f;

final class kb implements kd {
    @NonNull
    private final jw a;
    @NonNull
    private final RelativeLayout b;
    @NonNull
    private final f c;

    kb(@NonNull Context context, @NonNull jw jwVar) {
        this.a = jwVar;
        this.b = c.a(context);
        this.c = new f(context);
    }

    public final void a(@NonNull RelativeLayout relativeLayout) {
        if (VERSION.SDK_INT >= 16) {
            relativeLayout.setBackground(b.a);
        } else {
            relativeLayout.setBackgroundDrawable(b.a);
        }
    }

    public final void a(@NonNull Context context, @NonNull n nVar, @NonNull al alVar) {
        int i = context.getResources().getConfiguration().orientation;
        boolean a2 = jn.a(context, alVar);
        boolean b2 = jn.b(context, alVar);
        int i2 = 1;
        if (a2 == b2) {
            i2 = -1;
        } else if (!b2 ? 1 != i : 1 == i) {
            i2 = 0;
        }
        if (-1 != i2) {
            nVar.a(i2);
        }
    }

    @NonNull
    public final View a(@NonNull View view, @NonNull w wVar) {
        Context context = view.getContext();
        LayoutParams a2 = d.a(context, wVar);
        this.b.addView(view, a2);
        LayoutParams a3 = d.a(context, view);
        this.b.addView(this.a.a(), a3);
        LayoutParams b2 = d.b(context, wVar);
        RelativeLayout b3 = c.b(context);
        this.c.setBackFace(this.b, b2);
        this.c.setFrontFace(b3, a2);
        this.c.setLayoutParams(d.a(context, (w) null));
        return this.c;
    }

    public final void a() {
        this.a.b();
        im.a(this.c, ee.b((View) this.b));
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final void b() {
        this.a.c();
    }

    public final boolean c() {
        return this.a.d();
    }
}
