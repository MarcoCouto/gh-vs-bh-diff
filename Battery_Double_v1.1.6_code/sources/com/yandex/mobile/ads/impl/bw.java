package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.sv.a;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public abstract class bw<T> extends sw<fl, w<T>> {
    private final String a;
    private final Context b;
    private final fl c;
    @NonNull
    private final cc d = new cc();
    @NonNull
    private final ce e = new ce();
    @NonNull
    private final cb f = new cb();
    @NonNull
    private final eb g;
    @NonNull
    private final an h;

    protected static boolean a(int i) {
        return 204 == i;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T a_(@NonNull rr rrVar);

    public bw(@NonNull Context context, @NonNull fl flVar, @NonNull String str, @NonNull String str2, @NonNull a<w<T>> aVar, @NonNull hs<fl, w<T>> hsVar) {
        super(context, flVar.l(), str, aVar, flVar, hsVar);
        Object[] objArr = {str, str2};
        a((rw) new rn(flVar.m(), 0, 1.0f));
        this.a = str2;
        this.c = flVar;
        this.b = context.getApplicationContext();
        this.g = new eb(context);
        this.h = new an();
    }

    public final Map<String, String> a() throws ry {
        HashMap hashMap = new HashMap();
        String a2 = dr.a(this.b);
        if (a2 != null) {
            new Object[1][0] = a2;
            hashMap.put(rk.YMAD_SESSION_DATA.a(), a2);
        }
        hashMap.put(rk.YMAD_RENDER_AD_IDS.a(), this.h.a(this.b));
        hashMap.put(rk.YMAD_IMPRESSION_AD_IDS.a(), this.h.b(this.b));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final ru<w<T>> a(@NonNull rr rrVar, int i) {
        if (b(rrVar, i)) {
            Map<String, String> map = rrVar.c;
            b a2 = b.a(bs.a(map, rk.YMAD_TYPE));
            if (a2 == this.c.a()) {
                w.a aVar = new w.a();
                aVar.c(this.c.e());
                aVar.a(a2);
                int c2 = bs.c(map, rk.YMAD_HEADER_WIDTH);
                int c3 = bs.c(map, rk.YMAD_HEADER_HEIGHT);
                aVar.a(c2);
                aVar.b(c3);
                String a3 = bs.a(map, rk.YMAD_TYPE_FORMAT);
                String a4 = bs.a(map, rk.YMAD_PRODUCT_TYPE);
                aVar.a(a3);
                aVar.b(a4);
                al b2 = this.c.b();
                di diVar = null;
                aVar.a(b2 != null ? b2.c() : null);
                aVar.a(bs.e(map, rk.YMAD_SHOW_NOTICE));
                aVar.c(bs.a(map, rk.YMAD_NOTICE_DELAY, new bs.a<Long>() {
                    @Nullable
                    public final /* synthetic */ Object a(String str) {
                        return dr.a(str, Long.valueOf(0));
                    }
                }));
                aVar.d(bs.a(map, rk.YMAD_VISIBILITY_PERCENT, new bs.a<Integer>() {
                    @NonNull
                    public final /* synthetic */ Object a(String str) {
                        return Integer.valueOf(Math.min(dr.b(str), w.a.intValue()));
                    }
                }));
                aVar.b(bs.e(map, rk.YMAD_RENDER_TRACKING_URLS));
                aVar.f(bs.c(map, rk.YMAD_PREFETCH_COUNT));
                aVar.c(bs.c(map, rk.YMAD_REFRESH_PERIOD));
                aVar.d(bs.c(map, rk.YMAD_RELOAD_TIMEOUT));
                aVar.e(bs.c(map, rk.YMAD_EMPTY_INTERVAL));
                aVar.d(bs.a(map, rk.YMAD_RENDERER));
                Map<String, String> map2 = rrVar.c;
                Integer c4 = dr.c(bs.a(map2, rk.YMAD_REWARD_AMOUNT));
                String a5 = bs.a(map2, rk.YMAD_REWARD_TYPE);
                String a6 = a5 != null ? bz.a(a5.getBytes()) : null;
                bo boVar = (c4 == null || TextUtils.isEmpty(a6)) ? null : new bo(c4.intValue(), a6);
                String d2 = bs.d(map2, rk.YMAD_REWARD_URL);
                aVar.a(new bp.a().a(boVar).a(!TextUtils.isEmpty(d2) ? new bq(d2) : null).a(bs.b(map2, rk.YMAD_SERVER_SIDE_REWARD)).a());
                Map<String, String> map3 = rrVar.c;
                String d3 = bs.d(map3, rk.YMAD_FALSE_CLICK_URL);
                Long a7 = dr.a(bs.a(map3, rk.YMAD_FALSE_CLICK_INTERVAL), (Long) null);
                if (!(d3 == null || a7 == null)) {
                    diVar = new di(d3, a7.longValue());
                }
                aVar.a(diVar);
                String a8 = bs.a(map, rk.YMAD_SESSION_DATA);
                Object[] objArr = {rk.YMAD_SESSION_DATA.a(), a8};
                dr.a(this.b, a8);
                aVar.a(bs.b(map, rk.YMAD_ROTATION_ENABLED));
                aVar.b(bs.b(map, rk.YMAD_NON_SKIPPABLE_AD_ENABLED));
                if (bs.b(map, rk.YMAD_MEDIATION)) {
                    aVar.a(cc.a(rrVar));
                } else {
                    aVar.a(a_(rrVar));
                }
                aVar.e(bs.a(map, rk.YMAD_SOURCE));
                aVar.f(bs.a(map, rk.YMAD_ID));
                w a9 = aVar.a();
                if (!a(i)) {
                    return ru.a(a9, sj.a(rrVar));
                }
            }
        }
        return ru.a(r.a(rrVar));
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public boolean b(@NonNull rr rrVar, int i) {
        return 200 == i && b(rrVar);
    }

    protected static boolean b(@NonNull rr rrVar) {
        return rrVar.b != null && rrVar.b.length > 0;
    }

    /* access modifiers changed from: protected */
    public final sf a(sf sfVar) {
        return super.a((sf) r.a(sfVar.a));
    }

    public final String b() {
        String b2 = super.b();
        if (d() == 0) {
            b2 = Uri.parse(b2).buildUpon().encodedQuery(this.a).build().toString();
        }
        return this.g.a(b2);
    }

    public final byte[] c() throws ry {
        byte[] c2 = super.c();
        if (1 != d()) {
            return c2;
        }
        try {
            if (this.a != null) {
                return this.a.getBytes("UTF-8");
            }
            return c2;
        } catch (UnsupportedEncodingException unused) {
            Object[] objArr = {this.a, "UTF-8"};
            return c2;
        }
    }
}
