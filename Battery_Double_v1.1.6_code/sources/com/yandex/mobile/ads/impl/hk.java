package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class hk {
    @NonNull
    private final hi a;
    @NonNull
    private final hd b;
    @Nullable
    private hj c;

    hk(@NonNull Context context, @NonNull String str) {
        this.a = new hi(context, str);
        this.b = new hd(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final hj a() {
        return this.c != null ? this.c : c();
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.c = c();
        this.c = c();
    }

    @Nullable
    private hj c() {
        hf a2 = this.a.a();
        if (a2 != null) {
            boolean a3 = this.b.a();
            boolean b2 = this.b.b();
            if (a3 || b2) {
                return a2.a();
            }
        }
        return null;
    }
}
