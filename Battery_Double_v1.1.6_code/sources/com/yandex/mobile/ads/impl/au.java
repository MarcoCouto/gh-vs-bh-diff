package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;

final class au {
    @NonNull
    private final cq a;
    private boolean b;
    private boolean c;

    au(@NonNull cq cqVar) {
        this.a = cqVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.b = false;
        this.c = false;
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        if (!this.b) {
            this.b = true;
            this.a.a(b.IMPRESSION_TRACKING_START);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        if (!this.c) {
            this.c = true;
            HashMap hashMap = new HashMap();
            hashMap.put("failure_tracked", Boolean.FALSE);
            this.a.a(b.IMPRESSION_TRACKING_SUCCESS, hashMap);
        }
    }
}
