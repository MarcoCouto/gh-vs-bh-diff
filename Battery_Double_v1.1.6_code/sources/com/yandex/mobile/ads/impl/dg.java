package com.yandex.mobile.ads.impl;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.lang.ref.WeakReference;

final class dg {
    @NonNull
    private final Context a;
    @NonNull
    private final df b;
    @NonNull
    private final mo c = new a(this);
    @NonNull
    private final ml d = mm.a();

    @VisibleForTesting
    static class a implements mo {
        @Nullable
        private WeakReference<Activity> a;
        @NonNull
        private final dg b;

        a(@NonNull dg dgVar) {
            this.b = dgVar;
        }

        public final void a(@NonNull Activity activity) {
            new StringBuilder("onResume, activity = ").append(activity);
            if (this.a != null && activity.equals(this.a.get())) {
                this.b.d();
            }
        }

        public final void b(@NonNull Activity activity) {
            new StringBuilder("onPause, activity = ").append(activity);
            if (this.a == null) {
                this.a = new WeakReference<>(activity);
            }
        }
    }

    dg(@NonNull Context context, @NonNull fl flVar, @NonNull di diVar) {
        this.a = context.getApplicationContext();
        this.b = new df(context, flVar, diVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.b.a(com.yandex.mobile.ads.impl.df.a.WEBVIEW);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.b.b(com.yandex.mobile.ads.impl.df.a.WEBVIEW);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        this.d.a(this.a, this.c);
        this.b.a(com.yandex.mobile.ads.impl.df.a.BROWSER);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.b.b(com.yandex.mobile.ads.impl.df.a.BROWSER);
        this.d.b(this.a, this.c);
    }

    public final void e() {
        this.d.a(this.a, this.c);
    }

    public final void f() {
        this.d.b(this.a, this.c);
    }

    public final void a(@NonNull com.yandex.mobile.ads.impl.hr.a aVar) {
        this.b.a(aVar);
    }
}
