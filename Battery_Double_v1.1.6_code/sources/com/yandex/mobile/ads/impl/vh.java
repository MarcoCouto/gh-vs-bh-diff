package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.impl.hr.c;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class vh implements hs<VideoAd, List<VideoAd>> {
    @NonNull
    private final vb a;

    public vh(@NonNull vb vbVar) {
        this.a = vbVar;
    }

    @NonNull
    private Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("page_id", this.a.a());
        hashMap.put("imp_id", this.a.b());
        return hashMap;
    }

    public final /* synthetic */ hr a(@Nullable ru ruVar, int i, @NonNull Object obj) {
        Map a2 = a();
        c cVar = 204 == i ? c.NO_ADS : (ruVar == null || ruVar.a == null || i != 200) ? c.ERROR : ((List) ruVar.a).isEmpty() ? c.NO_ADS : c.SUCCESS;
        a2.put("status", cVar.a());
        return new hr(b.VAST_WRAPPER_RESPONSE, a2);
    }

    public final /* synthetic */ hr a(Object obj) {
        return new hr(b.VAST_WRAPPER_REQUEST, a());
    }
}
