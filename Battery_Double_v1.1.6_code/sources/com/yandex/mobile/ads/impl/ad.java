package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.MobileAds;
import com.yandex.mobile.ads.b;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class ad {
    private final String A;
    private final String B;
    private final String C;
    private final String D;
    private final Map<String, String> E;
    private final Long F;
    private final String G;
    private final String H;
    private final String I;
    @Nullable
    private final String J;
    private final String K;
    private final String L;
    private final String M;
    private final String N;
    private final String O;
    private final String P;
    private final String Q;
    private final String R;
    private final String S;
    private final String T;
    private final Integer U;
    private final String V;
    private final String W;
    private final Boolean X;
    private final String Y;
    private final String Z;
    private final String a;
    private StringBuilder aa;
    private final String b;
    private final String c;
    private final Integer d;
    private final Integer e;
    private final String f;
    private final String g;
    private final Integer h;
    private final Integer i;
    private final Float j;
    private final Location k;
    private final Integer l;
    private final Integer m;
    private final String n;
    private final String o;
    private final AdRequest p;
    private final Integer q;
    private final Integer r;
    private final String s;
    private final Boolean t;
    private final String u;
    private final Boolean v;
    private final String w;
    private final Integer x;
    private final String y;
    private final String z;

    public static final class a<T> {
        /* access modifiers changed from: private */
        public Boolean A;
        /* access modifiers changed from: private */
        public String B;
        /* access modifiers changed from: private */
        public Integer C;
        /* access modifiers changed from: private */
        public String D;
        /* access modifiers changed from: private */
        public String E;
        /* access modifiers changed from: private */
        public String F;
        /* access modifiers changed from: private */
        public String G;
        /* access modifiers changed from: private */
        public String H;
        /* access modifiers changed from: private */
        public String I;
        /* access modifiers changed from: private */
        public Long J;
        /* access modifiers changed from: private */
        public String K;
        /* access modifiers changed from: private */
        public String L;
        /* access modifiers changed from: private */
        public String M;
        /* access modifiers changed from: private */
        @Nullable
        public String N;
        /* access modifiers changed from: private */
        public String O;
        /* access modifiers changed from: private */
        public String P;
        /* access modifiers changed from: private */
        public String Q;
        /* access modifiers changed from: private */
        public String R;
        /* access modifiers changed from: private */
        public String S;
        /* access modifiers changed from: private */
        public String T;
        /* access modifiers changed from: private */
        public String U;
        /* access modifiers changed from: private */
        public String V;
        /* access modifiers changed from: private */
        public String W;
        /* access modifiers changed from: private */
        public String X;
        /* access modifiers changed from: private */
        public int Y;
        /* access modifiers changed from: private */
        public String Z;
        @NonNull
        private final fx a;
        /* access modifiers changed from: private */
        public String aa;
        /* access modifiers changed from: private */
        public String ab;
        /* access modifiers changed from: private */
        public String ac;
        @NonNull
        private final y b;
        private final boolean c;
        /* access modifiers changed from: private */
        @NonNull
        public final Map<String, String> d;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean e;
        /* access modifiers changed from: private */
        public String f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public String h;
        /* access modifiers changed from: private */
        public Integer i;
        /* access modifiers changed from: private */
        public Integer j;
        /* access modifiers changed from: private */
        public String k;
        /* access modifiers changed from: private */
        public String l;
        /* access modifiers changed from: private */
        public Location m;
        /* access modifiers changed from: private */
        public Integer n;
        /* access modifiers changed from: private */
        public Integer o;
        /* access modifiers changed from: private */
        public Float p;
        /* access modifiers changed from: private */
        public Integer q;
        /* access modifiers changed from: private */
        public Integer r;
        /* access modifiers changed from: private */
        public String s;
        /* access modifiers changed from: private */
        public String t;
        /* access modifiers changed from: private */
        public AdRequest u;
        /* access modifiers changed from: private */
        public Integer v;
        /* access modifiers changed from: private */
        public Integer w;
        /* access modifiers changed from: private */
        public String x;
        /* access modifiers changed from: private */
        public Boolean y;
        /* access modifiers changed from: private */
        public String z;

        /* synthetic */ a(boolean z2, byte b2) {
            this(z2);
        }

        private a(boolean z2) {
            this.d = new HashMap();
            this.c = z2;
            this.a = new fx();
            this.b = new y();
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(String str) {
            this.f = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(@Nullable String str) {
            this.g = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> c(String str) {
            this.L = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(b bVar) {
            if (bVar != null) {
                this.h = bVar.a();
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@NonNull Context context, @Nullable al alVar) {
            if (alVar != null) {
                this.k = alVar.c().a();
                this.i = Integer.valueOf(alVar.b(context));
                this.j = Integer.valueOf(alVar.a(context));
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@NonNull Context context) {
            this.m = this.c ? null : gz.a(context).a();
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(Context context) {
            this.n = Integer.valueOf(ee.c(context));
            this.o = Integer.valueOf(ee.d(context));
            this.p = Float.valueOf(ee.e(context));
            this.Y = context.getResources().getDisplayMetrics().densityDpi;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> c(Context context) {
            if (1 == context.getResources().getConfiguration().orientation) {
                this.l = "portrait";
            } else {
                this.l = "landscape";
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(Integer num) {
            this.C = num;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable AdRequest adRequest) {
            String str;
            String str2;
            if (adRequest != null) {
                HashMap hashMap = null;
                this.u = this.c ? null : adRequest;
                new ch();
                String contextQuery = adRequest.getContextQuery();
                if (!TextUtils.isEmpty(contextQuery)) {
                    String encode = Uri.encode(contextQuery);
                    if (encode != null && encode.length() > 1024) {
                        Cif.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. First %s bytes of the parameter will be used", Integer.valueOf(1024), Integer.valueOf(1024));
                        String encode2 = Uri.encode(" ");
                        String substring = encode.substring(0, 1024);
                        encode = encode.startsWith(encode2, 1024) ? substring : substring.substring(0, substring.lastIndexOf(encode2));
                    }
                    str = Uri.decode(encode);
                } else {
                    str = null;
                }
                this.D = str;
                List<String> contextTags = adRequest.getContextTags();
                StringBuilder sb = new StringBuilder();
                if (contextTags != null) {
                    String str3 = "";
                    for (String str4 : contextTags) {
                        if (!TextUtils.isEmpty(str4)) {
                            sb.append(str3);
                            sb.append(str4);
                            sb.append("\n");
                            str3 = "3";
                        }
                    }
                }
                String sb2 = sb.toString();
                if (!TextUtils.isEmpty(sb2)) {
                    String encode3 = Uri.encode(sb2);
                    if (encode3 != null && encode3.length() > 2048) {
                        Cif.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. First %s bytes of the parameter will be used", Integer.valueOf(2048), Integer.valueOf(2048));
                        String encode4 = Uri.encode("\n");
                        encode3 = encode3.substring(0, 2048);
                        if (!encode3.endsWith(encode4)) {
                            encode3 = encode3.substring(0, encode3.lastIndexOf(encode4));
                        }
                    }
                    str2 = Uri.decode(encode3);
                } else {
                    str2 = null;
                }
                this.E = str2;
                this.Z = ch.a(adRequest.getAge());
                this.aa = ch.a(adRequest.getGender());
                Map parameters = adRequest.getParameters();
                if (parameters != null && !parameters.isEmpty()) {
                    hashMap = new HashMap();
                    StringBuilder sb3 = new StringBuilder();
                    Iterator it = parameters.entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Entry entry = (Entry) it.next();
                        sb3.append(RequestParameters.AMPERSAND);
                        sb3.append(Uri.encode((String) entry.getKey()));
                        sb3.append(RequestParameters.EQUAL);
                        sb3.append(Uri.encode((String) entry.getValue()));
                        if (sb3.length() > 61440) {
                            Cif.b("Exceeded the length of the parameter! The maximum size of the parameter is %s bytes. First %s bytes of the parameter will be used", Integer.valueOf(61440), Integer.valueOf(61440));
                            break;
                        }
                        hashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                a((Map<String, String>) hashMap);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable String[] strArr) {
            this.F = c(strArr);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(@Nullable String[] strArr) {
            this.G = c(strArr);
            return this;
        }

        @Nullable
        private static String c(@Nullable String[] strArr) {
            if (strArr == null || strArr.length <= 0) {
                return null;
            }
            String str = "";
            StringBuilder sb = new StringBuilder();
            for (String str2 : strArr) {
                if (!TextUtils.isEmpty(str2)) {
                    sb.append(str);
                    sb.append(str2);
                    str = ",";
                }
            }
            return sb.toString();
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable ae aeVar) {
            if (aeVar != null) {
                this.H = aeVar.a();
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable af afVar) {
            if (afVar != null && af.PROMO == afVar) {
                this.I = afVar.a();
            }
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable Map<String, String> map) {
            if (map != null) {
                this.d.putAll(map);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> d(Context context) {
            this.q = dy.a(context);
            this.r = dy.b(context);
            this.s = dy.d(context);
            this.t = dy.c(context);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> e(Context context) {
            this.v = dy.e(context);
            this.w = dy.f(context);
            this.x = dy.g(context);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@NonNull Context context, @Nullable String str) {
            this.O = this.a.a(context);
            this.P = "android";
            this.Q = VERSION.RELEASE;
            this.R = Build.MANUFACTURER;
            this.S = Build.MODEL;
            this.T = fz.a(context);
            if (this.c) {
                str = null;
            }
            this.X = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a() {
            this.ac = y.a();
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b() {
            this.ab = MobileAds.getLibraryVersion();
            return this;
        }

        /* access modifiers changed from: 0000 */
        public final a<T> d(@Nullable String str) {
            this.M = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> e(@Nullable String str) {
            this.N = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> c() {
            this.e = fv.a().e();
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(@Nullable gm gmVar) {
            if (gmVar != null) {
                this.y = Boolean.valueOf(gmVar.b());
                if (!this.c && !this.y.booleanValue()) {
                    this.z = gmVar.a();
                }
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> b(@Nullable gm gmVar) {
            if (gmVar != null) {
                this.A = Boolean.valueOf(gmVar.b());
                if (!this.c && !this.A.booleanValue()) {
                    this.B = gmVar.a();
                }
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> a(long j2) {
            this.J = Long.valueOf(j2);
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> f(String str) {
            this.K = str;
            return this;
        }

        @NonNull
        public final String d() {
            return new ad(this, 0).toString();
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final a<T> f(Context context) {
            this.U = context.getPackageName();
            this.V = ds.a(context);
            this.W = ds.b(context);
            return this;
        }
    }

    /* synthetic */ ad(a aVar, byte b2) {
        this(aVar);
    }

    private ad(a<?> aVar) {
        this.a = aVar.f;
        this.b = aVar.g;
        this.c = aVar.h;
        this.d = aVar.i;
        this.e = aVar.j;
        this.f = aVar.k;
        this.g = aVar.l;
        this.h = aVar.n;
        this.i = aVar.o;
        this.j = aVar.p;
        this.k = aVar.m;
        this.l = aVar.q;
        this.m = aVar.r;
        this.n = aVar.s;
        this.o = aVar.t;
        this.p = aVar.u;
        this.q = aVar.v;
        this.r = aVar.w;
        this.s = aVar.x;
        this.t = aVar.y;
        this.u = aVar.z;
        this.v = aVar.A;
        this.w = aVar.B;
        this.x = aVar.C;
        this.y = aVar.D;
        this.z = aVar.E;
        this.A = aVar.F;
        this.B = aVar.G;
        this.C = aVar.H;
        this.D = aVar.I;
        this.E = aVar.d;
        this.F = aVar.J;
        this.G = aVar.K;
        this.H = aVar.L;
        this.I = aVar.M;
        this.J = aVar.N;
        this.K = aVar.O;
        this.L = aVar.P;
        this.M = aVar.Q;
        this.N = aVar.R;
        this.O = aVar.S;
        this.P = aVar.T;
        this.Q = aVar.U;
        this.R = aVar.V;
        this.S = aVar.W;
        this.T = aVar.X;
        this.U = Integer.valueOf(aVar.Y);
        this.V = aVar.Z;
        this.W = aVar.aa;
        this.X = aVar.e;
        this.Z = aVar.ac;
        this.Y = aVar.ab;
    }

    public static a<?> a(boolean z2) {
        return new a<>(z2, 0);
    }

    public final String toString() {
        if (!TextUtils.isEmpty(this.aa)) {
            return this.aa.toString();
        }
        this.aa = new StringBuilder();
        a("ad_unit_id", (Object) this.a);
        a("uuid", (Object) this.b);
        a("width", (Object) this.d);
        a("height", (Object) this.e);
        a("ad_size_type", (Object) this.f);
        a("orientation", (Object) this.g);
        a("screen_width", (Object) this.h);
        a("screen_height", (Object) this.i);
        a("scalefactor", (Object) this.j);
        a(RequestParameters.NETWORK_MCC, (Object) this.l);
        a(RequestParameters.NETWORK_MNC, (Object) this.m);
        a(AppEventsConstants.EVENT_PARAM_AD_TYPE, (Object) this.c);
        a("network_type", (Object) this.n);
        a("carrier", (Object) this.o);
        a("cellid", (Object) this.q);
        a("lac", (Object) this.r);
        a("wifi", (Object) this.s);
        a("dnt", this.t);
        a("google_aid", (Object) this.u);
        a("huawei_dnt", this.v);
        a("huawei_oaid", (Object) this.w);
        a("battery_charge", (Object) this.x);
        a("context_query", (Object) this.y);
        a("context_taglist", (Object) this.z);
        a("image_sizes", (Object) this.A);
        a("app_supported_features", (Object) this.B);
        a("response_ad_format", (Object) this.C);
        a("ad_source", (Object) this.D);
        a("debug_yandexuid", (Object) this.H);
        a(AccessToken.USER_ID_KEY, (Object) this.I);
        a("session_random", (Object) this.F);
        a(HttpRequest.PARAM_CHARSET, (Object) this.G);
        a(TapjoyConstants.TJC_DEVICE_TYPE_NAME, (Object) this.K);
        a("os_name", (Object) this.L);
        a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, (Object) this.M);
        a("manufacturer", (Object) this.N);
        a("model", (Object) this.O);
        a("locale", (Object) this.P);
        a("app_id", (Object) this.Q);
        a("app_version_code", (Object) this.R);
        a("app_version_name", (Object) this.S);
        a("appmetrica_version", (Object) this.Z);
        a("device-id", (Object) this.T);
        a("screen_dpi", (Object) this.U);
        a(IronSourceSegment.AGE, (Object) this.V);
        a("gender", (Object) this.W);
        a("user_consent", this.X);
        a("autograb", (Object) this.J);
        a("sdk_version", (Object) this.Y);
        a(this.E);
        AdRequest adRequest = this.p;
        if (adRequest != null) {
            a(adRequest.getLocation());
        }
        if (this.p == null || this.p.getLocation() == null) {
            a(this.k);
        }
        return this.aa.toString();
    }

    private void a(Map<String, String> map) {
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                a((String) entry.getKey(), entry.getValue());
            }
        }
    }

    private void a(String str, Object obj) {
        if (obj != null) {
            a();
            StringBuilder sb = this.aa;
            sb.append(Uri.encode(str));
            sb.append(RequestParameters.EQUAL);
            sb.append(Uri.encode(obj.toString()));
        }
    }

    private void a(String str, Boolean bool) {
        if (bool != null) {
            a(str, (Object) Integer.valueOf(bool.booleanValue() ? 1 : 0));
        }
    }

    private void a(Location location) {
        if (location != null) {
            a("lat", (Object) String.valueOf(location.getLatitude()));
            a("lon", (Object) String.valueOf(location.getLongitude()));
            a("location_timestamp", (Object) String.valueOf(location.getTime()));
            a("precision", (Object) String.valueOf((int) location.getAccuracy()));
        }
    }

    private void a() {
        this.aa.append(TextUtils.isEmpty(this.aa) ? "" : RequestParameters.AMPERSAND);
    }

    @Nullable
    public static String a(@NonNull fl flVar) {
        String f2 = flVar.f();
        if (TextUtils.isEmpty(f2)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(f2);
        sb.append(f2.endsWith("/") ? "" : "/");
        sb.append("v4/ad");
        return sb.toString();
    }

    @NonNull
    public static a a(@NonNull Context context, @NonNull fl flVar) {
        return a(fn.a(context)).a(flVar.c()).a(flVar.a()).a(flVar.i()).b(flVar.j()).f(context).a().a(dy.h(context)).a(flVar.e()).f("UTF-8").c(flVar.h()).a(context, flVar.g()).a(flVar.o()).e(context).a(context).c(context).d(context).a(flVar.p()).b(context).b().a(dr.a).a(context, flVar.b()).a(flVar.q()).b(flVar.s()).d(flVar.t()).e(flVar.v()).b(flVar.d());
    }
}
