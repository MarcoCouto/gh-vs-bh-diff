package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qi implements qh<om> {
    @NonNull
    private final pk a;

    public qi(@NonNull Context context) {
        this.a = new pk(new eb(context));
    }

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, z {
        om omVar = new om();
        omVar.a(this.a.a(jSONObject, "url"));
        omVar.a(jSONObject.getInt("w"));
        omVar.b(jSONObject.getInt("h"));
        String optString = jSONObject.optString("sizeType");
        if (!TextUtils.isEmpty(optString)) {
            omVar.b(optString);
        }
        return omVar;
    }
}
