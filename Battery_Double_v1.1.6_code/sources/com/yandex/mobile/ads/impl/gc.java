package com.yandex.mobile.ads.impl;

import android.graphics.Point;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Display;

final class gc {
    @NonNull
    private final gd a = new gd();

    gc() {
    }

    @Nullable
    private static Point b(@NonNull Display display) {
        try {
            return new Point(((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(display, new Object[0])).intValue(), ((Integer) Display.class.getMethod("getRawHeight", new Class[0]).invoke(display, new Object[0])).intValue());
        } catch (Throwable unused) {
            return null;
        }
    }

    @NonNull
    private static Point c(@NonNull Display display) {
        return new Point(display.getWidth(), display.getHeight());
    }

    static /* synthetic */ Point a(Display display) {
        if (VERSION.SDK_INT >= 17) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            display.getRealMetrics(displayMetrics);
            return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
        } else if (VERSION.SDK_INT < 14) {
            return c(display);
        } else {
            Point b = b(display);
            return b == null ? c(display) : b;
        }
    }
}
