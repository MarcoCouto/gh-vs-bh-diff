package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;

public final class qk {
    private static final Object a = new Object();
    @Nullable
    private static volatile qk b;
    @NonNull
    private final ra c;
    @Nullable
    private String[] d;

    private qk(@NonNull Context context) {
        this.c = new ra(context);
    }

    public static qk a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new qk(context.getApplicationContext());
                }
            }
        }
        return b;
    }

    @NonNull
    public final String[] a() {
        if (this.d == null) {
            ArrayList arrayList = new ArrayList();
            if (this.c.a("com.android.launcher.permission.INSTALL_SHORTCUT") && this.c.a("com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
                arrayList.add("shortcut");
            }
            this.d = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        return this.d;
    }
}
