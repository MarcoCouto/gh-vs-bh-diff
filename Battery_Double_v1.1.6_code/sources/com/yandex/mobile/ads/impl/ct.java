package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.AdRequest;
import java.util.HashMap;
import java.util.Map;

public final class ct {
    @NonNull
    private final cv a = new cv();
    @NonNull
    private final cx b = new cx();

    @NonNull
    public static Map<String, Object> a(@Nullable AdRequest adRequest) {
        ea eaVar = new ea(new HashMap());
        if (adRequest != null) {
            Map parameters = adRequest.getParameters();
            ea eaVar2 = new ea(new HashMap());
            if (parameters != null) {
                String str = (String) parameters.get(TapjoyConstants.TJC_ADAPTER_VERSION);
                String str2 = (String) parameters.get("adapter_network_sdk_version");
                eaVar2.b("adapter_network_name", (String) parameters.get("adapter_network_name"));
                eaVar2.b(TapjoyConstants.TJC_ADAPTER_VERSION, str);
                eaVar2.b("adapter_network_sdk_version", str2);
            }
            Map a2 = eaVar2.a();
            ea eaVar3 = new ea(new HashMap());
            if (parameters != null) {
                String str3 = (String) parameters.get("plugin_type");
                String str4 = (String) parameters.get("plugin_version");
                eaVar3.b("plugin_type", str3);
                eaVar3.b("plugin_version", str4);
            }
            Map a3 = eaVar3.a();
            eaVar.a(a2);
            eaVar.a(a3);
        }
        return eaVar.a();
    }
}
