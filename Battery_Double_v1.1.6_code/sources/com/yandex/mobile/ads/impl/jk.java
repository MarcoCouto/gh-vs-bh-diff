package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.tapjoy.TapjoyConstants;

public final class jk {
    @NonNull
    private final jj a;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final jv c = new jv();
    /* access modifiers changed from: private */
    @NonNull
    public final ji d = new ji();
    private boolean e;

    public jk(@NonNull jj jjVar) {
        this.a = jjVar;
    }

    public final void a(int i, String str) {
        this.e = true;
        this.b.removeCallbacks(this.d);
        this.b.post(new jl(i, str, this.a));
    }

    public final void a() {
        if (!this.e) {
            this.c.a(new Runnable() {
                public final void run() {
                    jk.this.b.postDelayed(jk.this.d, TapjoyConstants.TIMER_INCREMENT);
                }
            });
        }
    }

    public final void a(@Nullable el elVar) {
        this.d.a(elVar);
    }

    public final void b() {
        this.b.removeCallbacksAndMessages(null);
        this.d.a(null);
    }
}
