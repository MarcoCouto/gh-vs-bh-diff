package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.Locale;

public final class id {
    @NonNull
    public static String a(@NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Character.toUpperCase(str.charAt(0)));
        sb.append(str.substring(1).toLowerCase(Locale.US));
        return sb.toString();
    }
}
