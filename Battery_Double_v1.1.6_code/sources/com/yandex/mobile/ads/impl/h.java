package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.b;

abstract class h extends iq {
    private boolean h;
    private Runnable i = new Runnable() {
        public final void run() {
            h.this.a(h.this.j());
        }
    };

    @SuppressLint({"VisibleForTests"})
    h(@NonNull Context context, @NonNull ci ciVar, @NonNull b bVar) {
        super(context, ciVar, bVar);
        if (p() && du.b()) {
            this.h = true;
        }
    }

    public void d() {
        super.d();
        a(false);
    }

    private void a() {
        this.a.removeCallbacks(this.i);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        this.h = z;
        if (this.h) {
            g();
        } else {
            a();
        }
    }

    private void g() {
        a();
        w y = y();
        if (y != null && y.x() && this.h && !D()) {
            this.a.postDelayed(this.i, (long) y.u());
            new Object[1][0] = Integer.valueOf(y.s());
        }
    }

    public void b(int i2) {
        super.b(i2);
        g();
    }

    public void a(@NonNull Intent intent) {
        super.a(intent);
        g();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        super.i();
        g();
    }

    public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        super.onAdFailedToLoad(adRequestError);
        if (5 != adRequestError.getCode() && 2 != adRequestError.getCode()) {
            g();
        }
    }
}
