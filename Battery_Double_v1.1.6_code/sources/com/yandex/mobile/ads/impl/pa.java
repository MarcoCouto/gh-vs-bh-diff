package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class pa {
    @NonNull
    private final String a;
    private final int b;
    private final int c;

    public pa(@NonNull String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        pa paVar = (pa) obj;
        if (this.b == paVar.b && this.c == paVar.c) {
            return this.a.equals(paVar.a);
        }
        return false;
    }

    public final int hashCode() {
        return (((this.a.hashCode() * 31) + this.b) * 31) + this.c;
    }
}
