package com.yandex.mobile.ads.impl;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;

public final class mm {
    @NonNull
    public static ml a() {
        if (VERSION.SDK_INT >= 14) {
            return mn.a();
        }
        return mp.a();
    }
}
