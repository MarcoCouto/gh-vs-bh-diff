package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.YandexMetrica;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class hp {
    private static final Object a = new Object();
    private static volatile hp b;
    @Nullable
    private final IReporter c;
    @NonNull
    private final hq d;
    @NonNull
    private final ho e;

    private hp(@Nullable IReporter iReporter, @NonNull hq hqVar, @NonNull ho hoVar) {
        this.c = iReporter;
        this.d = hqVar;
        this.e = hoVar;
        this.d.a(iReporter);
    }

    @NonNull
    public static hp a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    Context applicationContext = context.getApplicationContext();
                    String str = hz.a(applicationContext) ? "322a737a-a0ca-44e0-bc85-649b1c7c1db6" : "478cb909-6ad1-4e12-84cc-b3629a789f93";
                    hq hqVar = new hq(aj.a());
                    b = new hp(a(applicationContext, str, hqVar), hqVar, new ho());
                }
            }
        }
        return b;
    }

    @Nullable
    private static IReporter a(@NonNull Context context, @NonNull String str, @NonNull hq hqVar) {
        try {
            YandexMetrica.activateReporter(context, ReporterConfig.newConfigBuilder(str).withStatisticsSending(hqVar.a(context)).build());
            return YandexMetrica.getReporter(context, str);
        } catch (Throwable unused) {
            return null;
        }
    }

    public final void a(@NonNull hr hrVar) {
        if (fv.a().b() && this.c != null) {
            String b2 = hrVar.b();
            Map a2 = hrVar.a();
            try {
                HashMap hashMap = new HashMap();
                for (Entry entry : a2.entrySet()) {
                    hashMap.put((String) entry.getKey(), Arrays.deepToString(new Object[]{entry.getValue()}));
                }
                StringBuilder sb = new StringBuilder("reportEvent(), eventName = ");
                sb.append(b2);
                sb.append(", reportData = ");
                sb.append(hashMap);
                this.c.reportEvent(b2, a2);
            } catch (Throwable unused) {
            }
        }
    }
}
