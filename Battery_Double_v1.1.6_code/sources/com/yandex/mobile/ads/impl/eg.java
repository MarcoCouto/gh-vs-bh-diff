package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.VideoEventListener;

public final class eg implements eh {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public VideoEventListener c;

    public final void a(@Nullable VideoEventListener videoEventListener) {
        synchronized (a) {
            this.c = videoEventListener;
        }
    }

    public final void a() {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (eg.a) {
                    if (eg.this.c != null) {
                        eg.this.c.onVideoComplete();
                    }
                }
            }
        });
    }
}
