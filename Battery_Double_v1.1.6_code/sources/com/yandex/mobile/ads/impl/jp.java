package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.in.g;
import java.io.Serializable;
import java.util.Map;

public final class jp implements el, eo, jf, jg, k {
    @NonNull
    private final RelativeLayout a;
    @NonNull
    private final dk b;
    @NonNull
    private final n c;
    @NonNull
    private final Context d;
    @NonNull
    private final Window e;
    @NonNull
    private final w<String> f;
    @NonNull
    private final String g;
    @NonNull
    private final de h;
    @NonNull
    private final bk i;
    @NonNull
    private final ip j = new ju(this.d, this.f);
    @NonNull
    private final kd k;
    @NonNull
    private final iu l;

    public final void b(boolean z) {
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
    }

    public final void onAdLoaded() {
    }

    jp(@NonNull Context context, @NonNull RelativeLayout relativeLayout, @NonNull dk dkVar, @NonNull n nVar, @NonNull Window window, @NonNull jt jtVar) {
        this.d = context;
        this.a = relativeLayout;
        this.b = dkVar;
        this.c = nVar;
        this.e = window;
        this.f = jtVar.a();
        this.g = jtVar.c();
        fl b2 = jtVar.b();
        this.h = new de(context, b2);
        this.i = new bk(b2);
        new iw();
        boolean a2 = iw.a(this.g);
        iz.a();
        this.l = iz.a(a2).a(this.j, this, this, this, this);
        boolean y = this.f.y();
        final iu iuVar = this.l;
        View a3 = g.a(this.d);
        a3.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                iuVar.a();
                jp.this.h();
            }
        });
        new ke(new jx());
        this.k = ke.a(this.f, a3, a2, y);
    }

    public final void a() {
        this.e.requestFeature(1);
        this.e.addFlags(1024);
        if (hz.a(11)) {
            this.e.addFlags(16777216);
        }
        this.k.a(this.d, this.c, this.f.e());
    }

    public final void c_() {
        this.l.a(this.g);
        this.k.a(this.a);
        this.j.setId(2);
        this.a.addView(this.k.a(this.j, this.f));
        this.c.a(5, null);
    }

    public final boolean c() {
        return !this.k.c();
    }

    public final void d() {
        this.c.a(4, null);
    }

    public final void e() {
        this.j.f();
        this.c.a(2, null);
    }

    public final void f() {
        this.j.e();
        this.c.a(3, null);
    }

    public final void g() {
        this.j.g();
        this.l.b();
        this.k.b();
    }

    public final void h() {
        this.c.a();
    }

    public final void a(boolean z) {
        this.k.a(z);
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        this.k.a();
        Bundle bundle = new Bundle();
        bundle.putSerializable("extra_tracking_parameters", (Serializable) map);
        this.c.a(0, bundle);
    }

    public final void a(@NonNull String str) {
        this.h.a(str, this.f, this.b);
    }

    public final void i() {
        this.i.c(this.d, this.f);
        this.c.a(13, null);
    }

    public final void b() {
        this.c.a(14, null);
    }

    public final void d_() {
        this.c.a(15, null);
    }
}
