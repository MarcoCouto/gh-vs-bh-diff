package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.AdRequestError;
import java.util.Map;

public final class jd implements el {
    private final ex a;
    private final el b;

    jd(@NonNull ex exVar, @NonNull el elVar) {
        this.a = exVar;
        this.b = elVar;
    }

    public final void onAdLoaded() {
        this.a.a();
    }

    public final void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        this.b.onAdFailedToLoad(adRequestError);
    }

    public final void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        this.b.a(webView, map);
    }

    public final void a(@NonNull String str) {
        this.a.b(str);
    }

    public final void b(boolean z) {
        this.a.a(z);
    }
}
