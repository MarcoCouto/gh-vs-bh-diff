package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;

public final class dj implements dk {
    @NonNull
    private final WeakReference<Context> a;
    @Nullable
    private final ResultReceiver b;
    private final boolean c;

    public dj(@Nullable Context context, boolean z, @Nullable ResultReceiver resultReceiver) {
        this.a = new WeakReference<>(context);
        this.c = z;
        this.b = resultReceiver;
    }

    public final void a(@NonNull co coVar, @Nullable String str) {
        o.a((Context) this.a.get(), coVar, str, this.b, this.c);
    }
}
