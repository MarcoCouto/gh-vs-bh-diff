package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.b;

public abstract class io extends aa<String> implements el {
    @Nullable
    private iv h;

    /* access modifiers changed from: protected */
    public abstract iv a(@NonNull String str, @NonNull w<String> wVar, @NonNull al alVar);

    /* access modifiers changed from: protected */
    public abstract boolean a(@NonNull al alVar);

    public final void b(boolean z) {
    }

    io(@NonNull Context context, @NonNull b bVar) {
        super(context, bVar);
    }

    public void b(@NonNull w<String> wVar) {
        al b = this.f.b();
        if (b == null) {
            onAdFailedToLoad(u.d);
        } else if (!a(wVar.e(), b)) {
            onAdFailedToLoad(u.c);
        } else {
            String str = (String) wVar.p();
            if (!TextUtils.isEmpty(str)) {
                this.h = a(str, wVar, b);
                this.h.a(str);
                return;
            }
            onAdFailedToLoad(u.e);
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final bw<String> a(String str, String str2) {
        jh jhVar = new jh(this.b, this.f, str, str2, this);
        return jhVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a(@NonNull al alVar, @NonNull al alVar2) {
        return a(alVar) && jn.a(this.b, alVar, alVar2);
    }

    public void d() {
        super.d();
        if (this.h != null) {
            this.h.b();
        }
        this.h = null;
    }

    public final void c(@NonNull dq dqVar) {
        a(this.f.c(), dqVar);
    }
}
