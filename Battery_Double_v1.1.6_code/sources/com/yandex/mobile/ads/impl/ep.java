package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.ir.a;

public abstract class ep extends ab implements b, ek, a {
    private static boolean a = false;
    @NonNull
    private final ir b = new ir();
    @Nullable
    protected el e;
    @NonNull
    private final jv f = new jv();
    @NonNull
    private final ag g = ag.a();
    @NonNull
    private final hy h = new hy();
    private boolean i;
    private boolean j;

    /* access modifiers changed from: protected */
    public abstract void a();

    public ep(@NonNull Context context) {
        super(context.getApplicationContext());
        a_(context);
        if (!a) {
            Context context2 = getContext();
            if (VERSION.SDK_INT == 19) {
                WebView webView = new WebView(context2.getApplicationContext());
                webView.setBackgroundColor(0);
                webView.loadDataWithBaseURL(null, "", WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
                LayoutParams layoutParams = new LayoutParams();
                layoutParams.width = 1;
                layoutParams.height = 1;
                layoutParams.type = IronSourceConstants.IS_INSTANCE_OPENED;
                layoutParams.flags = 16777240;
                layoutParams.format = -2;
                layoutParams.gravity = 8388659;
                try {
                    ((WindowManager) context2.getSystemService("window")).addView(webView, layoutParams);
                } catch (Exception unused) {
                }
            }
            a = true;
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void a_(Context context) {
        super.a_(context);
        setBackgroundColor(0);
        setVisibility(4);
        setHorizontalScrollBarEnabled(false);
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(false);
        setScrollBarStyle(0);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setMinimumFontSize(1);
        settings.setMinimumLogicalFontSize(1);
        if (hz.a(21)) {
            settings.setMixedContentMode(0);
        }
        if (VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        WebSettings settings2 = getSettings();
        fw a2 = fv.a().a(context);
        if (a2 != null && a2.i()) {
            settings2.setUserAgentString(hy.a(context));
        }
        setWebViewClient(new ej(this));
        setWebChromeClient(new ei());
    }

    public void d() {
        this.f.a(new Runnable() {
            public final void run() {
                ep.this.a();
            }
        });
    }

    public final void a(@NonNull Context context, @NonNull String str) {
        if (this.e != null) {
            this.e.a(str);
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i2) {
        super.onVisibilityChanged(view, i2);
        a(ir.a(this));
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        a(ir.a(this));
    }

    /* access modifiers changed from: protected */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.b());
        sb.append("<style type='text/css'> \n  * { \n      -webkit-tap-highlight-color: rgba(0, 0, 0, 0) !important; \n      -webkit-focus-ring-color: rgba(0, 0, 0, 0) !important; \n      outline: none !important; \n    } \n</style> \n");
        sb.append(eq.d);
        return sb.toString();
    }

    public void setHtmlWebViewListener(@NonNull el elVar) {
        this.e = elVar;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.j = true;
        this.g.a(this, getContext());
        a(ir.a(this));
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.j = false;
        a(ir.a(this));
        this.g.b(this, getContext());
        super.onDetachedFromWindow();
    }

    public final boolean h() {
        return this.j;
    }

    public final void a(@NonNull Intent intent) {
        a(!"android.intent.action.SCREEN_OFF".equals(intent.getAction()) && ir.a(this) && this.g.a(getContext()));
    }

    private void a(boolean z) {
        if (this.i != z) {
            this.i = z;
            if (this.e != null) {
                this.e.b(this.i);
            }
        }
    }

    public void g() {
        this.e = null;
        super.g();
    }
}
