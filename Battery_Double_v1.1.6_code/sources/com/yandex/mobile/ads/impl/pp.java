package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.z;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class pp {
    @NonNull
    private final eb a;
    @NonNull
    private final pr b = new pr(this.a);

    public pp(@NonNull eb ebVar) {
        this.a = ebVar;
    }

    @Nullable
    private String a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, z {
        return this.a.a(b(jSONObject, str));
    }

    @Nullable
    private static String b(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, z {
        if (jSONObject.has(str)) {
            return pi.a(jSONObject, str);
        }
        return null;
    }

    @NonNull
    public final on a(@NonNull JSONObject jSONObject) throws JSONException, z {
        List list;
        JSONArray optJSONArray = jSONObject.optJSONArray("actions");
        di diVar = null;
        if (optJSONArray != null) {
            list = new ArrayList();
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                pq a2 = this.b.a(jSONObject2);
                if (a2 != null) {
                    list.add(a2.a(jSONObject2));
                }
            }
        } else {
            list = null;
        }
        String a3 = a(jSONObject, "falseClickUrl");
        Long valueOf = Long.valueOf(jSONObject.optLong("falseClickInterval", 0));
        if (!(a3 == null || valueOf == null)) {
            diVar = new di(a3, valueOf.longValue());
        }
        return new on(list, diVar, a(jSONObject, "trackingUrl"), a(jSONObject, "url"));
    }
}
