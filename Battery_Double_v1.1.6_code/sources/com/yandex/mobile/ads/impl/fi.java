package com.yandex.mobile.ads.impl;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;

public final class fi implements fh {
    private final boolean a;

    public fi(@NonNull View view) {
        boolean z;
        if (VERSION.SDK_INT >= 11) {
            z = view.isHardwareAccelerated();
        } else {
            z = false;
        }
        this.a = z;
    }

    @NonNull
    public final String a() {
        return String.format("supports: {inlineVideo: %s}", new Object[]{Boolean.valueOf(this.a)});
    }
}
