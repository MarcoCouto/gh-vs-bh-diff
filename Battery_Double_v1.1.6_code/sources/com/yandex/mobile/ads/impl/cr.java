package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.hr.a;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;
import java.util.Map;

public final class cr {
    @NonNull
    private final hp a;
    @Nullable
    private a b;
    @NonNull
    private final ct c = new ct();
    @Nullable
    private fl d;

    public cr(@NonNull Context context) {
        this.a = hp.a(context);
    }

    public final void a(@NonNull fl flVar) {
        this.d = flVar;
    }

    public final void a(@NonNull a aVar) {
        this.b = aVar;
    }

    private void a(@NonNull Map<String, Object> map) {
        this.a.a(b(map));
    }

    private hr b(@NonNull Map<String, Object> map) {
        if (this.d != null) {
            map.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, this.d.a().a());
            String e = this.d.e();
            if (e != null) {
                map.put("block_id", e);
            }
            map.putAll(ct.a(this.d.c()));
        }
        if (this.b != null) {
            map.putAll(this.b.a());
        }
        return new hr(b.AD_LOADING_RESULT, map);
    }

    public final void a() {
        HashMap hashMap = new HashMap();
        hashMap.put("status", "success");
        a((Map<String, Object>) hashMap);
    }

    public final void a(@NonNull AdRequestError adRequestError) {
        HashMap hashMap = new HashMap();
        hashMap.put("status", "error");
        hashMap.put("failure_reason", adRequestError.getDescription());
        a((Map<String, Object>) hashMap);
    }
}
