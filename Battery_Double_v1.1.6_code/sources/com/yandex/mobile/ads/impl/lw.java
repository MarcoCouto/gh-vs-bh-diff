package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.j;

public final class lw extends md<TextView, ol> {
    @NonNull
    private final j a;
    @NonNull
    private final mk b;

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        super.a((TextView) view);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        om a2 = ((ol) obj).a();
        if (a2 == null) {
            return true;
        }
        return this.b.a(textView.getBackground(), a2);
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        TextView textView = (TextView) view;
        om a2 = ((ol) obj).a();
        if (a2 != null) {
            Bitmap a3 = this.a.a(a2);
            if (a3 != null) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(textView.getResources(), a3);
                if (VERSION.SDK_INT >= 16) {
                    textView.setBackground(bitmapDrawable);
                    return;
                }
                textView.setBackgroundDrawable(bitmapDrawable);
            }
        }
    }

    public lw(@NonNull TextView textView, @NonNull j jVar) {
        super(textView);
        this.a = jVar;
        this.b = new mk(jVar);
    }
}
