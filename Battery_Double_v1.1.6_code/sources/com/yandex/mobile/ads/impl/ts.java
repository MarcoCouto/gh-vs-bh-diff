package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;

public final class ts {
    @NonNull
    public static tr a(@NonNull Context context, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull RequestListener<tl> requestListener) {
        return new tr(context, new uz(vastRequestConfiguration), requestListener);
    }
}
