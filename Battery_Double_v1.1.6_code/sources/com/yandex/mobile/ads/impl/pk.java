package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class pk {
    @NonNull
    private final eb a;

    public pk(@NonNull eb ebVar) {
        this.a = ebVar;
    }

    @NonNull
    public final String a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, z {
        String a2 = this.a.a(pi.a(jSONObject, str));
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        throw new z("Native Ad json has not required attributes");
    }
}
