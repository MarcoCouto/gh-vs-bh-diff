package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.nativeads.z;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import org.json.JSONException;
import org.json.JSONObject;

public final class pl {
    @NonNull
    private final Context a;
    @NonNull
    private final pp b;

    public pl(@NonNull Context context, @NonNull pp ppVar) {
        this.a = context.getApplicationContext();
        this.b = ppVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        if (r11.equals("close_button") != false) goto L_0x00fa;
     */
    @NonNull
    public final oj a(@NonNull JSONObject jSONObject) throws JSONException, z {
        char c;
        boolean z;
        pz pzVar;
        pz pzVar2;
        JSONObject jSONObject2 = jSONObject;
        char c2 = 5;
        if (pj.a(jSONObject2, "name", "type", String.CLICKABLE, "required", "value")) {
            String a2 = pi.a(jSONObject2, "type");
            String a3 = pi.a(jSONObject2, "name");
            switch (a3.hashCode()) {
                case -1074675180:
                    if (a3.equals("favicon")) {
                        c = 0;
                        break;
                    }
                case -938102371:
                    if (a3.equals("rating")) {
                        c = 4;
                        break;
                    }
                case -807286424:
                    if (a3.equals("review_count")) {
                        c = 5;
                        break;
                    }
                case -191501435:
                    if (a3.equals("feedback")) {
                        c = 1;
                        break;
                    }
                case 3226745:
                    if (a3.equals(SettingsJsonConstants.APP_ICON_KEY)) {
                        c = 2;
                        break;
                    }
                case 103772132:
                    if (a3.equals("media")) {
                        c = 3;
                        break;
                    }
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                case 1:
                case 2:
                    z = MessengerShareContentUtility.MEDIA_IMAGE.equals(a2);
                    break;
                case 3:
                    z = "media".equals(a2);
                    break;
                case 4:
                case 5:
                    z = "number".equals(a2);
                    break;
                default:
                    z = "string".equals(a2);
                    break;
            }
            if (z) {
                JSONObject optJSONObject = jSONObject2.optJSONObject("link");
                on a4 = optJSONObject != null ? this.b.a(optJSONObject) : null;
                Context context = this.a;
                switch (a3.hashCode()) {
                    case -1678958759:
                        break;
                    case -1074675180:
                        if (a3.equals("favicon")) {
                            c2 = 0;
                            break;
                        }
                    case -938102371:
                        if (a3.equals("rating")) {
                            c2 = 3;
                            break;
                        }
                    case -807286424:
                        if (a3.equals("review_count")) {
                            c2 = 4;
                            break;
                        }
                    case -191501435:
                        if (a3.equals("feedback")) {
                            c2 = 6;
                            break;
                        }
                    case 3226745:
                        if (a3.equals(SettingsJsonConstants.APP_ICON_KEY)) {
                            c2 = 1;
                            break;
                        }
                    case 103772132:
                        if (a3.equals("media")) {
                            c2 = 2;
                            break;
                        }
                    default:
                        c2 = 65535;
                        break;
                }
                switch (c2) {
                    case 0:
                    case 1:
                        pzVar2 = new qc(context);
                        break;
                    case 2:
                        pzVar2 = new qd(context);
                        break;
                    case 3:
                    case 4:
                        pzVar = new qe();
                        break;
                    case 5:
                        pzVar = new qa();
                        break;
                    case 6:
                        pzVar = new qb(new qc(context));
                        break;
                    default:
                        pzVar = new qg();
                        break;
                }
                pzVar = pzVar2;
                oj ojVar = new oj(a3, a2, pzVar.a(jSONObject2), a4, jSONObject2.getBoolean(String.CLICKABLE), jSONObject2.getBoolean("required"));
                return ojVar;
            }
            throw new z("Native Ad json has not required attributes");
        }
        throw new z("Native Ad json has not required attributes");
    }
}
