package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.yandex.mobile.ads.AdActivity;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public final class o {

    static final class a {
        static final AtomicLong a = new AtomicLong(SystemClock.elapsedRealtime() - AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);

        static boolean a() {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long andSet = elapsedRealtime - a.getAndSet(elapsedRealtime);
            return andSet < 0 || andSet > 1000;
        }
    }

    private static void a(Context context, String str, ResultReceiver resultReceiver) {
        if (context != null) {
            try {
                context.startActivity(b(context, str, resultReceiver));
            } catch (Exception e) {
                new StringBuilder("Failed to show Browser. Exception: ").append(e);
            }
        }
    }

    private static Intent b(Context context, String str, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, AdActivity.class);
        intent.putExtra("window_type", "window_type_browser");
        intent.putExtra("extra_receiver", dr.a(resultReceiver));
        intent.putExtra("extra_browser_url", str);
        intent.addFlags(268435456);
        return intent;
    }

    public static void a(@Nullable Context context, @NonNull co coVar, @Nullable String str, @Nullable ResultReceiver resultReceiver, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("click_type", "default");
        coVar.a(b.CLICK, hashMap);
        if (resultReceiver != null) {
            resultReceiver.send(9, null);
        }
        if (context != null && a.a()) {
            if (z && ec.d(str)) {
                a(context, str, resultReceiver);
            } else if (!ec.a(context, str, true)) {
                a(context, str, resultReceiver);
            } else if (resultReceiver != null) {
                resultReceiver.send(7, null);
            }
        }
    }
}
