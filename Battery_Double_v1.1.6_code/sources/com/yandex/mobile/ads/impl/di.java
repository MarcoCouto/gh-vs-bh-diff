package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class di {
    @NonNull
    private final String a;
    private final long b;

    public di(@NonNull String str, long j) {
        this.a = str;
        this.b = j;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    public final long b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        di diVar = (di) obj;
        if (this.b != diVar.b) {
            return false;
        }
        return this.a.equals(diVar.a);
    }

    public final int hashCode() {
        return (this.a.hashCode() * 31) + ((int) (this.b ^ (this.b >>> 32)));
    }
}
