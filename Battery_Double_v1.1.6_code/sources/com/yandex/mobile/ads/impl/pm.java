package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class pm {
    @NonNull
    private final po a = new po();

    @NonNull
    public static oz a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, z {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject2.optJSONArray("installedPackages");
        if (optJSONArray != null) {
            int i = 0;
            while (i < optJSONArray.length()) {
                JSONObject jSONObject3 = optJSONArray.getJSONObject(i);
                if (pj.a(jSONObject3, "name")) {
                    arrayList.add(new pa(pi.a(jSONObject3, "name"), dr.a(jSONObject3, "minVersion", 0), dr.a(jSONObject3, "maxVersion", Integer.MAX_VALUE)));
                    i++;
                } else {
                    throw new z("Native Ad json has not required attributes");
                }
            }
        }
        return new oz(arrayList);
    }
}
