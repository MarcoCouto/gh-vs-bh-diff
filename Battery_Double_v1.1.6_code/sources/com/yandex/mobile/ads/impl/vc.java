package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.VideoAdRequest;

public final class vc implements vb {
    @NonNull
    private final VideoAdRequest a;

    public vc(@NonNull VideoAdRequest videoAdRequest) {
        this.a = videoAdRequest;
    }

    @NonNull
    public final String a() {
        return this.a.getBlocksInfo().getPartnerId();
    }

    @NonNull
    public final String b() {
        return this.a.getBlockId();
    }
}
