package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.ad.a;

public final class dp implements dq {
    @NonNull
    private final bl a;

    public dp(@NonNull bl blVar) {
        this.a = blVar;
    }

    @Nullable
    public final String a(@NonNull fl flVar) {
        return ad.a(flVar);
    }

    @NonNull
    public final String a(@NonNull Context context, @NonNull fl flVar) {
        a a2 = ad.a(context, flVar);
        a2.a(this.a.b());
        return a2.d();
    }

    @NonNull
    public final int a() {
        return dq.a.b;
    }
}
