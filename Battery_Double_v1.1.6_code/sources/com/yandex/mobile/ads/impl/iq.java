package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.yandex.mobile.ads.b;
import com.yandex.mobile.ads.impl.as.a;
import java.util.List;
import java.util.Map;

public abstract class iq extends io implements a {
    @NonNull
    private final ax h;
    @NonNull
    private final ci i;
    @NonNull
    private final as j;
    @Nullable
    private cl k;
    @NonNull
    private final dh l;
    private final az m = new az() {
        @NonNull
        public final am a(int i) {
            am.a aVar;
            if (iq.this.x()) {
                aVar = am.a.APPLICATION_INACTIVE;
            } else if (!iq.this.k()) {
                aVar = am.a.AD_NOT_LOADED;
            } else if (iq.this.g()) {
                aVar = am.a.SUPERVIEW_HIDDEN;
            } else if (!iq.this.a(i) || !iq.this.b()) {
                aVar = am.a.NOT_VISIBLE_FOR_PERCENT;
            } else {
                aVar = am.a.SUCCESS;
            }
            return new am(aVar, new cm());
        }
    };

    /* access modifiers changed from: protected */
    @NonNull
    public abstract iv a(@NonNull String str, @NonNull w<String> wVar, @NonNull al alVar, @NonNull as asVar);

    /* access modifiers changed from: protected */
    public abstract boolean a(int i2);

    /* access modifiers changed from: protected */
    public abstract boolean b();

    protected iq(@NonNull Context context, @NonNull ci ciVar, @NonNull b bVar) {
        super(context, bVar);
        this.i = ciVar;
        cq cqVar = new cq(context, r());
        this.j = new as(this, cqVar);
        new ay();
        this.h = ay.a(this.b, r(), cqVar, this.m, dr.a(this));
        this.h.a(this.j);
        this.l = new dh(this.b, r());
    }

    public final void b(@NonNull w<String> wVar) {
        if (a(wVar.e())) {
            super.b(wVar);
            this.j.a((w) wVar);
            return;
        }
        onAdFailedToLoad(u.e);
    }

    public void a(@NonNull Intent intent) {
        StringBuilder sb = new StringBuilder("onPhoneStateChanged(), intent.getAction = ");
        sb.append(intent.getAction());
        sb.append(", isAdVisible = ");
        sb.append(a());
        this.h.a(intent, a());
    }

    public void b(int i2) {
        fw a = fv.a().a(this.b);
        if (a != null && a.p()) {
            if (i2 == 0) {
                this.h.a();
            } else {
                this.h.b();
            }
        } else if (a()) {
            this.h.a();
        } else {
            this.h.b();
        }
        Object[] objArr = {getClass().toString(), Integer.valueOf(i2)};
    }

    public void d() {
        new StringBuilder("cleanOut(), clazz = ").append(this);
        super.d();
        this.h.b();
    }

    /* access modifiers changed from: protected */
    public final iv a(@NonNull String str, @NonNull w<String> wVar, @NonNull al alVar) {
        return a(str, wVar, alVar, this.j);
    }

    public synchronized void a(@NonNull w<String> wVar) {
        super.a(wVar);
        w<String> wVar2 = wVar;
        cl clVar = new cl(this.b, this.i, wVar2, this.f, wVar.g());
        this.k = clVar;
    }

    public void a(int i2, @Nullable Bundle bundle) {
        if (i2 != 9) {
            switch (i2) {
                case 14:
                    this.j.b();
                    return;
                case 15:
                    this.j.d_();
                    return;
                default:
                    super.a(i2, bundle);
                    return;
            }
        } else {
            this.l.d();
            this.h.c();
        }
    }

    public void a(@Nullable WebView webView, @Nullable Map<String, String> map) {
        new StringBuilder("onAdDisplayed(), clazz = ").append(this);
        if (this.g != null) {
            List a = dr.a(this.g, map);
            this.j.a(a);
            this.h.a(this.g, a);
        }
        this.l.a(this.g != null ? this.g.h() : null);
        C();
    }

    public final void a(@NonNull String str) {
        b(str);
    }

    public void onAdOpened() {
        new StringBuilder("onAdOpened(), clazz = ").append(this);
        super.onAdOpened();
        this.l.a();
    }

    public void onAdClosed() {
        super.onAdClosed();
        new StringBuilder("onAdClosed(), clazz = ").append(getClass());
        this.l.b();
    }

    public synchronized void onAdLeftApplication() {
        super.onAdLeftApplication();
        this.l.c();
    }

    public void e() {
        super.e();
        this.l.e();
    }

    /* access modifiers changed from: protected */
    public final synchronized void C() {
        if (a()) {
            new StringBuilder("trackAdOnDisplayed(), clazz = ").append(this);
            this.h.a();
            if (this.k != null) {
                this.k.b();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean D() {
        return g() || x();
    }

    private boolean a() {
        return this.i.a();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        return !a();
    }
}
