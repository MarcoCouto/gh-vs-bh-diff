package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.ArrayList;
import java.util.List;

final class tw {
    /* access modifiers changed from: private */
    @NonNull
    public final Context a;
    /* access modifiers changed from: private */
    @NonNull
    public final vp b;
    @NonNull
    private final vj c = new vj();
    /* access modifiers changed from: private */
    @NonNull
    public final List<VideoAd> d = new ArrayList();

    private class a implements RequestListener<List<VideoAd>> {
        @NonNull
        private final RequestListener<List<VideoAd>> b;

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            vi a2 = vj.a((List) obj);
            tw.this.d.addAll(a2.a());
            List b2 = a2.b();
            if (b2.isEmpty()) {
                a();
            } else {
                tw.this.b.a(tw.this.a, b2, this);
            }
        }

        a(RequestListener<List<VideoAd>> requestListener) {
            this.b = requestListener;
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            a();
        }

        private void a() {
            if (tw.this.d.isEmpty()) {
                this.b.onFailure(VideoAdError.createNoAdError(new tn()));
                return;
            }
            this.b.onSuccess(tw.this.d);
        }
    }

    tw(@NonNull Context context, @NonNull vb vbVar) {
        this.a = context.getApplicationContext();
        this.b = new vp(context, vbVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull List<VideoAd> list, @NonNull RequestListener<List<VideoAd>> requestListener) {
        vi a2 = vj.a(list);
        this.d.addAll(a2.a());
        this.b.a(this.a, a2.b(), new a(requestListener));
    }
}
