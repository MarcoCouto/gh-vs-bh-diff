package com.yandex.mobile.ads.impl;

import java.util.List;

public class ie {

    public static class a {
        private final List<C0116a> a;
        private boolean b;

        /* renamed from: com.yandex.mobile.ads.impl.ie$a$a reason: collision with other inner class name */
        private static class C0116a {
            public final String a;
            public final long b;
            public final long c;
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            long j;
            if (!this.b) {
                String str = "Request on the loose";
                this.b = true;
                if (this.a.size() == 0) {
                    j = 0;
                } else {
                    j = ((C0116a) this.a.get(this.a.size() - 1)).c - ((C0116a) this.a.get(0)).c;
                }
                if (j > 0) {
                    long j2 = ((C0116a) this.a.get(0)).c;
                    Object[] objArr = {Long.valueOf(j), str};
                    for (C0116a aVar : this.a) {
                        long j3 = aVar.c;
                        Object[] objArr2 = {Long.valueOf(j3 - j2), Long.valueOf(aVar.b), aVar.a};
                        j2 = j3;
                    }
                }
            }
            super.finalize();
        }
    }
}
