package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

final class aw implements ax {
    @NonNull
    private final Context a;
    /* access modifiers changed from: private */
    @NonNull
    public final b b = new b(Looper.getMainLooper());
    @NonNull
    private final az c;
    /* access modifiers changed from: private */
    @NonNull
    public final String d;
    /* access modifiers changed from: private */
    @NonNull
    public final List<a> e;
    @NonNull
    private final cq f;
    @NonNull
    private final de g;
    @Nullable
    private at h;
    private boolean i;
    private int j;
    private boolean k;
    private boolean l;

    @VisibleForTesting
    static class a {
        String a;
        long b;
        int c;
        Long d;

        a(String str, long j, int i) {
            this.a = str;
            this.b = j;
            this.c = i;
        }
    }

    @VisibleForTesting
    static class b extends Handler {
        b(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Pair pair = (Pair) message.obj;
                    aw awVar = (aw) ((WeakReference) pair.first).get();
                    if (awVar != null) {
                        a aVar = (a) pair.second;
                        new Object[1][0] = awVar.d;
                        am a = awVar.a(aVar);
                        awVar.b(aVar, a);
                        if (aw.c(a)) {
                            awVar.e.remove(aVar);
                            if (awVar.e.isEmpty()) {
                                awVar.a(a.c());
                                return;
                            }
                        } else {
                            aVar.d = null;
                            awVar.a();
                        }
                    }
                    return;
                case 2:
                    aw awVar2 = (aw) ((WeakReference) message.obj).get();
                    if (awVar2 != null) {
                        Object[] objArr = {Integer.valueOf(awVar2.e.size()), awVar2.d};
                        int size = awVar2.e.size();
                        for (int i = 0; i < size; i++) {
                            a aVar2 = (a) awVar2.e.get(i);
                            am a2 = awVar2.a(aVar2);
                            if (aw.c(a2)) {
                                if (aVar2.d == null) {
                                    aVar2.d = Long.valueOf(SystemClock.elapsedRealtime());
                                }
                                if (SystemClock.elapsedRealtime() - aVar2.d.longValue() >= aVar2.b) {
                                    awVar2.b.sendMessage(Message.obtain(awVar2.b, 1, new Pair(new WeakReference(awVar2), aVar2)));
                                }
                                awVar2.b(a2.d());
                            } else {
                                aVar2.d = null;
                                awVar2.a(a2);
                            }
                        }
                        if (awVar2.e()) {
                            awVar2.b.sendMessageDelayed(Message.obtain(awVar2.b, 2, new WeakReference(awVar2)), 200);
                            break;
                        }
                    }
                    break;
            }
        }
    }

    aw(@NonNull Context context, @NonNull fl flVar, @NonNull cq cqVar, @NonNull az azVar, @NonNull String str) {
        this.a = context;
        this.c = azVar;
        this.f = cqVar;
        this.d = str;
        this.e = new ArrayList();
        this.g = new de(context, flVar);
    }

    public final synchronized void a(@NonNull w wVar, @NonNull List<bn> list) {
        new StringBuilder("updateNotices(), clazz = ").append(this.d);
        this.f.a(wVar);
        this.e.clear();
        this.j = 0;
        this.i = false;
        this.k = false;
        this.l = false;
        b();
        a(list);
    }

    @VisibleForTesting
    private synchronized void a(List<bn> list) {
        for (bn bnVar : list) {
            this.e.add(new a(bnVar.b(), bnVar.a(), bnVar.c()));
        }
    }

    public final synchronized void a() {
        new StringBuilder("startTrackingIfNeeded(), clazz = ").append(this.d);
        if (ag.a().a(this.a)) {
            if (!hz.a(this.e) && e() && !this.b.hasMessages(2)) {
                this.b.sendMessage(Message.obtain(this.b, 2, new WeakReference(this)));
            }
        }
    }

    public final synchronized void b() {
        new StringBuilder("stopTracking(), clazz = ").append(this.d);
        this.b.removeMessages(2);
        this.b.removeMessages(1);
        d();
    }

    private void d() {
        for (a aVar : this.e) {
            aVar.d = null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized boolean e() {
        return this.e.size() > 0;
    }

    public final synchronized void c() {
        Object[] objArr = {Integer.valueOf(this.e.size()), this.d};
        b();
        com.yandex.mobile.ads.impl.hr.b bVar = com.yandex.mobile.ads.impl.hr.b.IMPRESSION_TRACKING_SUCCESS;
        ArrayList arrayList = new ArrayList();
        this.l = false;
        for (a aVar : this.e) {
            am a2 = a(aVar);
            a(aVar, a2);
            if (c(a2)) {
                arrayList.add(aVar);
                bVar = a2.c();
                b(a2.d());
            }
        }
        if (!arrayList.isEmpty()) {
            this.e.removeAll(arrayList);
            if (this.e.isEmpty()) {
                a(bVar);
            }
        }
        a();
    }

    private synchronized void a(@NonNull a aVar, @NonNull am amVar) {
        b(aVar, amVar);
        if (!this.l && !c(amVar)) {
            this.f.b(amVar);
            this.l = true;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(@NonNull a aVar, @NonNull am amVar) {
        if (c(amVar)) {
            this.g.a(aVar.a);
        } else {
            a(amVar);
        }
    }

    /* access modifiers changed from: private */
    public static boolean c(@NonNull am amVar) {
        return amVar.b() == com.yandex.mobile.ads.impl.am.a.SUCCESS;
    }

    /* access modifiers changed from: private */
    @NonNull
    public am a(@NonNull a aVar) {
        am a2 = this.c.a(aVar.c);
        new Object[1][0] = a2.b().a();
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        return;
     */
    public final synchronized void a(@NonNull Intent intent, boolean z) {
        Object[] objArr = {intent, Boolean.valueOf(z), this.d};
        String action = intent.getAction();
        char c2 = 65535;
        int hashCode = action.hashCode();
        if (hashCode != -2128145023) {
            if (hashCode != -1454123155) {
                if (hashCode == 823795052) {
                    if (action.equals("android.intent.action.USER_PRESENT")) {
                        c2 = 2;
                    }
                }
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
                c2 = 1;
            }
        } else if (action.equals("android.intent.action.SCREEN_OFF")) {
            c2 = 0;
        }
        switch (c2) {
            case 0:
                b();
                return;
            case 1:
            case 2:
                if (z) {
                    a();
                    break;
                }
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(com.yandex.mobile.ads.impl.hr.b bVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("failure_tracked", Boolean.valueOf(this.i));
        this.f.a(bVar, hashMap);
        if (this.h != null) {
            this.h.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void a(am amVar) {
        this.j++;
        if (this.j == 20) {
            this.f.a(amVar);
            this.i = true;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void b(@NonNull com.yandex.mobile.ads.impl.hr.b bVar) {
        if (!this.k) {
            this.f.a(bVar);
            Cif.a("Ad binding successful", new Object[0]);
            this.k = true;
        }
    }

    public final void a(@NonNull at atVar) {
        this.h = atVar;
    }
}
