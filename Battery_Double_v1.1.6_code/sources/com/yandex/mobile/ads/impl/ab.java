package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.HashMap;
import java.util.Map;

public abstract class ab extends WebView {
    @Nullable
    Map<String, Object> c;
    public Integer d = null;

    /* access modifiers changed from: protected */
    public void a_(Context context) {
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "";
    }

    protected ab(Context context) {
        super(context);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        if (hz.a(11)) {
            settings.setAllowContentAccess(false);
            if (hz.a(16)) {
                settings.setAllowFileAccessFromFileURLs(false);
                settings.setAllowUniversalAccessFromFileURLs(false);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(11)
    public final void b_() {
        if (hz.a(11)) {
            getSettings().setDisplayZoomControls(false);
        }
    }

    public final void e() {
        dr.a((WebView) this);
        new Object[1][0] = getClass().toString();
    }

    public final void f() {
        dr.b((WebView) this);
        new Object[1][0] = getClass().toString();
    }

    public void g() {
        ee.a((View) this);
        if (this.c != null) {
            for (String removeJavascriptInterface : this.c.keySet()) {
                removeJavascriptInterface(removeJavascriptInterface);
            }
            this.c.clear();
        }
        destroy();
        new Object[1][0] = getClass().toString();
    }

    @SuppressLint({"JavascriptInterface", "AddJavascriptInterface"})
    public void addJavascriptInterface(Object obj, String str) {
        super.addJavascriptInterface(obj, str);
        if (this.c == null) {
            this.c = new HashMap();
        }
        this.c.put(str, obj);
    }

    @TargetApi(11)
    public void removeJavascriptInterface(String str) {
        if (hz.a(11)) {
            super.removeJavascriptInterface(str);
        }
    }

    @Nullable
    public final Object a(@NonNull String str) {
        if (this.c != null) {
            return this.c.get(str);
        }
        return null;
    }

    public final void b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(b());
        sb.append("<body style='margin:0; padding:0;'>");
        loadDataWithBaseURL("https://yandex.ru", sb.toString(), WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
    }
}
