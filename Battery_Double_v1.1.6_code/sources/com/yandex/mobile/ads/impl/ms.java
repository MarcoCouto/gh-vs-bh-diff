package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class ms implements mq {
    @NonNull
    private final em a;

    public ms(@NonNull em emVar) {
        this.a = emVar;
    }

    public final void a(@NonNull on onVar, @NonNull mr mrVar) {
        this.a.setClickListener(new mv(onVar, mrVar));
    }
}
