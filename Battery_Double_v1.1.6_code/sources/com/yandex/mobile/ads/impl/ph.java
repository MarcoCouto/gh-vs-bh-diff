package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.impl.hr.c;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;

public final class ph implements cd<or> {
    private final bt a = new bt();
    @NonNull
    private final pi b;

    public ph(@NonNull Context context) {
        this.b = new pi(context);
    }

    @Nullable
    private or a(@NonNull String str) {
        try {
            or a2 = this.b.a(str);
            try {
                a2.a("status", c.SUCCESS);
                return a2;
            } catch (z | JSONException unused) {
                return a2;
            }
        } catch (z | JSONException unused2) {
            return null;
        }
    }

    @Nullable
    public final /* synthetic */ Object b(@NonNull rr rrVar) {
        String a2 = bt.a(rrVar);
        if (!TextUtils.isEmpty(a2)) {
            return a(a2);
        }
        return null;
    }
}
