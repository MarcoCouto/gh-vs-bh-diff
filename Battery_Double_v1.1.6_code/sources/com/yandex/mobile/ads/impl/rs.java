package com.yandex.mobile.ads.impl;

import android.net.Uri;
import android.text.TextUtils;
import java.util.Collections;
import java.util.Map;

public abstract class rs<T> implements Comparable<rs<T>> {
    private final com.yandex.mobile.ads.impl.ie.a a = null;
    private final int b;
    private final String c;
    private final int d;
    private final com.yandex.mobile.ads.impl.ru.a e;
    private Integer f;
    private rt g;
    private boolean h = true;
    private boolean i;
    private boolean j;
    private boolean k;
    private rw l;
    private com.yandex.mobile.ads.impl.rl.a m;
    private Object n;

    public enum a {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    public static String k() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }

    /* access modifiers changed from: protected */
    public abstract ru<T> a(rr rrVar);

    /* access modifiers changed from: protected */
    public sf a(sf sfVar) {
        return sfVar;
    }

    /* access modifiers changed from: protected */
    public abstract void b(T t);

    public byte[] c() throws ry {
        return null;
    }

    public /* synthetic */ int compareTo(Object obj) {
        rs rsVar = (rs) obj;
        a o = o();
        a o2 = rsVar.o();
        if (o == o2) {
            return this.f.intValue() - rsVar.f.intValue();
        }
        return o2.ordinal() - o.ordinal();
    }

    public rs(int i2, String str, com.yandex.mobile.ads.impl.ru.a aVar) {
        int i3 = 0;
        this.i = false;
        this.j = false;
        this.k = false;
        this.m = null;
        this.b = i2;
        this.c = str;
        this.e = aVar;
        this.l = new rn();
        if (!TextUtils.isEmpty(str)) {
            Uri parse = Uri.parse(str);
            if (parse != null) {
                String host = parse.getHost();
                if (host != null) {
                    i3 = host.hashCode();
                }
            }
        }
        this.d = i3;
    }

    public final int d() {
        return this.b;
    }

    public final rs<?> a(Object obj) {
        this.n = obj;
        return this;
    }

    public final Object e() {
        return this.n;
    }

    public final int f() {
        return this.d;
    }

    public final rs<?> a(rw rwVar) {
        this.l = rwVar;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        if (this.g != null) {
            this.g.b(this);
        }
    }

    public final rs<?> a(rt rtVar) {
        this.g = rtVar;
        return this;
    }

    public final rs<?> b(int i2) {
        this.f = Integer.valueOf(i2);
        return this;
    }

    public String b() {
        return this.c;
    }

    public final rs<?> a(com.yandex.mobile.ads.impl.rl.a aVar) {
        this.m = aVar;
        return this;
    }

    public final com.yandex.mobile.ads.impl.rl.a h() {
        return this.m;
    }

    public final void i() {
        this.i = true;
    }

    public final boolean j() {
        return this.i;
    }

    public Map<String, String> a() throws ry {
        return Collections.emptyMap();
    }

    public final rs<?> l() {
        this.h = false;
        return this;
    }

    public final boolean m() {
        return this.h;
    }

    public final boolean n() {
        return this.k;
    }

    public a o() {
        return a.NORMAL;
    }

    public final int p() {
        return this.l.a();
    }

    public final rw q() {
        return this.l;
    }

    public final void r() {
        this.j = true;
    }

    public final boolean s() {
        return this.j;
    }

    public final void b(sf sfVar) {
        if (this.e != null) {
            this.e.a(sfVar);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("0x");
        sb.append(Integer.toHexString(this.d));
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(this.i ? "[X] " : "[ ] ");
        sb3.append(b());
        sb3.append(" ");
        sb3.append(sb2);
        sb3.append(" ");
        sb3.append(o());
        sb3.append(" ");
        sb3.append(this.f);
        return sb3.toString();
    }
}
