package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

final class ob {
    @NonNull
    private final de a;

    ob(@NonNull de deVar) {
        this.a = deVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.a.a(str);
        }
    }
}
