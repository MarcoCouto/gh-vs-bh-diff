package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.r;
import java.util.List;

public final class oe {
    @NonNull
    private final od a;

    public oe(@NonNull fl flVar, @NonNull co coVar, @NonNull oa oaVar, @NonNull ag agVar, @NonNull r rVar) {
        od odVar = new od(flVar, coVar, oaVar, agVar, rVar);
        this.a = odVar;
    }

    public final void a(@NonNull Context context, @Nullable List<ot> list) {
        if (list != null && !list.isEmpty()) {
            for (ot otVar : list) {
                oc a2 = this.a.a(context, otVar);
                if (a2 != null) {
                    a2.a(context, otVar);
                }
            }
        }
    }
}
