package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.net.URI;

public final class eb {
    @NonNull
    private final Context a;

    public eb(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @Nullable
    public final String a(@Nullable String str) {
        fw a2 = fv.a().a(this.a);
        boolean z = a2 != null && a2.o();
        if (TextUtils.isEmpty(str) || !z) {
            return str;
        }
        String decode = Uri.decode(str.trim());
        if (!TextUtils.isEmpty(decode) && decode.startsWith("//")) {
            decode = "https:".concat(String.valueOf(decode));
        }
        return dz.a(b(decode));
    }

    @Nullable
    private static String b(@Nullable String str) {
        try {
            Uri parse = Uri.parse(str);
            URI uri = new URI(parse.getScheme(), parse.getAuthority(), parse.getPath(), parse.getQuery(), parse.getFragment());
            return uri.toASCIIString();
        } catch (Exception unused) {
            new Object[1][0] = str;
            return str;
        }
    }
}
