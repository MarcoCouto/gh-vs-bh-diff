package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class fv {
    private static final Object a = new Object();
    @Nullable
    private static volatile fv b;
    @Nullable
    private fw c;
    @NonNull
    private ss d = new su();
    @Nullable
    private Boolean e;
    private boolean f = true;
    private boolean g;

    public static fv a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new fv();
                }
            }
        }
        return b;
    }

    private fv() {
    }

    @Nullable
    public final fw a(@NonNull Context context) {
        fw fwVar;
        synchronized (a) {
            if (this.c == null) {
                this.c = hz.b(context);
            }
            fwVar = this.c;
        }
        return fwVar;
    }

    public final void a(@NonNull Context context, @NonNull fw fwVar) {
        synchronized (a) {
            this.c = fwVar;
            hz.a(context, fwVar);
        }
    }

    public final boolean b() {
        boolean z;
        synchronized (a) {
            z = this.f;
        }
        return z;
    }

    @NonNull
    public final synchronized ss c() {
        ss ssVar;
        synchronized (a) {
            ssVar = this.d;
        }
        return ssVar;
    }

    public final void a(boolean z) {
        synchronized (a) {
            this.g = z;
        }
    }

    public final boolean d() {
        boolean z;
        synchronized (a) {
            z = this.g;
        }
        return z;
    }

    public final void b(boolean z) {
        synchronized (a) {
            this.e = Boolean.valueOf(z);
        }
    }

    @Nullable
    public final Boolean e() {
        Boolean bool;
        synchronized (a) {
            bool = this.e;
        }
        return bool;
    }
}
