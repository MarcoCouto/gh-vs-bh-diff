package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.yandex.mobile.ads.nativeads.MediaView;

public final class lk implements lf<op>, li<op> {
    @Nullable
    private final lx a;
    @Nullable
    private final ly b;

    public final /* synthetic */ void a(@NonNull Object obj) {
        op opVar = (op) obj;
        a(opVar.b());
        MediaView mediaView = (MediaView) a((md<V, T>) this.b);
        if (mediaView != null) {
            this.b.b(mediaView, opVar);
            mediaView.setVisibility(0);
        }
    }

    public final /* synthetic */ boolean b(@NonNull Object obj) {
        op opVar = (op) obj;
        return a((md<V, T>) this.a, (T) opVar.b()) || a((md<V, T>) this.b, (T) opVar);
    }

    public final /* synthetic */ void c(@NonNull Object obj) {
        op opVar = (op) obj;
        a(opVar.b());
        MediaView mediaView = (MediaView) a((md<V, T>) this.b);
        if (mediaView != null) {
            this.b.a(opVar);
            mediaView.setVisibility(0);
        }
    }

    public lk(@Nullable lx lxVar, @Nullable ly lyVar) {
        this.a = lxVar;
        this.b = lyVar;
    }

    public final void a() {
        ImageView imageView = (ImageView) a((md<V, T>) this.a);
        if (imageView != null) {
            this.a.a(imageView);
        }
        MediaView mediaView = (MediaView) a((md<V, T>) this.b);
        if (mediaView != null) {
            this.b.a(mediaView);
        }
    }

    private void a(@Nullable om omVar) {
        ImageView imageView = (ImageView) a((md<V, T>) this.a);
        if (imageView != null && omVar != null) {
            this.a.b(imageView, omVar);
            imageView.setVisibility(0);
        }
    }

    public final void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        if (this.a != null) {
            this.a.a(ojVar, mjVar);
        }
        if (this.b != null) {
            this.b.a(ojVar, mjVar);
        }
    }

    private static <V extends View, T> boolean a(@Nullable md<V, T> mdVar, @Nullable T t) {
        View a2 = a(mdVar);
        return (a2 == null || t == null || !mdVar.a(a2, t)) ? false : true;
    }

    @Nullable
    private static <V extends View, T> V a(@Nullable md<V, T> mdVar) {
        if (mdVar != null) {
            return mdVar.a();
        }
        return null;
    }

    public final boolean b() {
        return (this.b != null && this.b.b()) || (this.a != null && this.a.b());
    }

    public final boolean c() {
        return (this.b != null && this.b.c()) || (this.a != null && this.a.c());
    }

    public final boolean d() {
        return (this.b != null && this.b.d()) || (this.a != null && this.a.d());
    }
}
