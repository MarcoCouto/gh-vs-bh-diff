package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.Map;

final class gl {
    gl() {
    }

    static boolean a(@NonNull Map<String, String> map) {
        String str = (String) map.get("yandex_mobile_metrica_uuid");
        String str2 = (String) map.get("yandex_mobile_metrica_get_ad_url");
        if (!(!TextUtils.isEmpty(str)) || !(!TextUtils.isEmpty(str2))) {
            return false;
        }
        return true;
    }
}
