package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;

@SuppressLint({"ViewConstructor"})
public final class ik extends ju {
    public ik(@NonNull Context context, @NonNull w wVar) {
        super(context, wVar);
        layout(0, 0, ee.a(context), ee.b(context));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.e != null) {
            this.e.onAdLoaded();
        }
    }
}
