package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public class lh<V extends View, T> implements lf<T> {
    @NonNull
    private final md<V, T> a;

    public lh(@NonNull md<V, T> mdVar) {
        this.a = mdVar;
    }

    public final void a() {
        View a2 = this.a.a();
        if (a2 != null) {
            this.a.a(a2);
        }
    }

    public final void a(@NonNull T t) {
        View a2 = this.a.a();
        if (a2 != null) {
            this.a.b(a2, t);
            a2.setVisibility(0);
        }
    }

    public final void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        this.a.a(ojVar, mjVar);
    }

    public final boolean b(@NonNull T t) {
        View a2 = this.a.a();
        return a2 != null && this.a.a(a2, t);
    }

    public final boolean b() {
        return this.a.b();
    }

    public final boolean c() {
        return this.a.c();
    }

    public final boolean d() {
        return this.a.d();
    }
}
