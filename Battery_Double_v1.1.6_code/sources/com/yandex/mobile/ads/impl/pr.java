package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.z;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class pr {
    /* access modifiers changed from: private */
    @NonNull
    public final pk a;
    @NonNull
    private final Map<String, pq> b = new HashMap<String, pq>() {
        {
            put("close", new ps());
            put("deeplink", new pv(pr.this.a));
            put("feedback", new pt(pr.this.a));
            put("shortcut", new pu(pr.this.a));
        }
    };

    public pr(@NonNull eb ebVar) {
        this.a = new pk(ebVar);
    }

    @Nullable
    public final pq a(@NonNull JSONObject jSONObject) throws JSONException, z {
        return (pq) this.b.get(pi.a(jSONObject, "type"));
    }
}
