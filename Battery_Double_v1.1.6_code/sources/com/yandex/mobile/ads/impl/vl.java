package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.models.ad.Creative;
import com.yandex.mobile.ads.video.models.ad.Icon;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

final class vl {
    vl() {
    }

    @NonNull
    private static Set<String> a(@NonNull Creative creative) {
        HashSet hashSet = new HashSet();
        for (Icon program : creative.getIcons()) {
            hashSet.add(program.getProgram());
        }
        return hashSet;
    }

    @NonNull
    static List<Icon> a(@NonNull Creative creative, @NonNull List<Creative> list) {
        ArrayList<Icon> arrayList = new ArrayList<>();
        for (Creative icons : list) {
            arrayList.addAll(icons.getIcons());
        }
        Set a = a(creative);
        ArrayList arrayList2 = new ArrayList();
        for (Icon icon : arrayList) {
            String program = icon.getProgram();
            if (!a.contains(program)) {
                arrayList2.add(icon);
                a.add(program);
            }
        }
        return arrayList2;
    }
}
