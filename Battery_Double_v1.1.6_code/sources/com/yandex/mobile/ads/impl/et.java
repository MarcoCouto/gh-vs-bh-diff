package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View.MeasureSpec;
import com.yandex.mobile.ads.impl.es.a;

public final class et implements es {
    @NonNull
    private final a a = new a();
    @NonNull
    private final z b;

    private static boolean a(int i) {
        return i == Integer.MIN_VALUE || i == 0;
    }

    public et(float f) {
        this.b = new z(f);
    }

    @NonNull
    public final a a(int i, int i2) {
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 1073741824 && a(mode2)) {
            i2 = a(this.b.b(size), size2, mode2);
        } else if (mode2 == 1073741824 && a(mode)) {
            i = a(this.b.a(size2), size, mode);
        }
        this.a.a = i;
        this.a.b = i2;
        return this.a;
    }

    private static int a(int i, int i2, int i3) {
        return MeasureSpec.makeMeasureSpec(b(i, i2, i3), 1073741824);
    }

    private static int b(int i, int i2, int i3) {
        return i3 == Integer.MIN_VALUE ? Math.min(i2, i) : i;
    }
}
