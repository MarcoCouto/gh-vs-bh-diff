package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;

final class ei extends WebChromeClient {
    ei() {
    }

    public final boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
        Object[] objArr = {consoleMessage.message(), consoleMessage.sourceId(), Integer.valueOf(consoleMessage.lineNumber())};
        return true;
    }
}
