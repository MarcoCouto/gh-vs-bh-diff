package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class ip extends ep implements jj {
    /* access modifiers changed from: private */
    @NonNull
    public final jk a = new jk(this);
    @Nullable
    private Map<String, String> b;
    @NonNull
    protected final w f;

    public class a {
        final WeakReference<Context> a;

        @JavascriptInterface
        public final String getBannerInfo() {
            return "{\"isDelicate\": false}";
        }

        public a(Context context) {
            this.a = new WeakReference<>(context);
        }

        @JavascriptInterface
        public final void onAdRender(int i, String str) {
            ip.this.a.a(i, str);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"AddJavascriptInterface"})
    public abstract void a(@NonNull Context context);

    protected ip(@NonNull Context context, @NonNull w wVar) {
        super(context);
        this.f = wVar;
        a(context);
    }

    public final void d() {
        if (i()) {
            this.a.a();
        } else {
            super.d();
        }
    }

    public final void b(int i, String str) {
        StringBuilder sb = new StringBuilder("onHtmlWebViewRender, height = ");
        sb.append(i);
        sb.append(", testTag = ");
        sb.append(str);
        a(i, str);
        super.d();
    }

    /* access modifiers changed from: protected */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.b());
        sb.append(i() ? eq.c : "");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final boolean i() {
        return "partner-code".equals(this.f.l());
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        new Object[1][0] = configuration;
        a("AdPerformActionsJSI");
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void a(int i, String str) {
        if (!TextUtils.isEmpty(str) && !"undefined".equals(str)) {
            this.b = new HashMap();
            this.b.put("test-tag", str);
        }
    }

    @NonNull
    public final Map<String, String> j() {
        return this.b != null ? this.b : Collections.emptyMap();
    }

    public void setHtmlWebViewListener(@NonNull el elVar) {
        super.setHtmlWebViewListener(elVar);
        this.a.a(elVar);
    }

    public final void g() {
        this.a.b();
        super.g();
    }
}
