package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.be;
import java.util.List;

final class nr {
    nr() {
    }

    @NonNull
    static nq a(@NonNull be beVar, @NonNull List<bn> list) {
        int i;
        if (list.isEmpty()) {
            i = 50;
        } else {
            int c = ((bn) list.get(0)).c();
            for (bn c2 : list) {
                c = Math.max(c, c2.c());
            }
            i = c;
        }
        return new nq(beVar, i);
    }
}
