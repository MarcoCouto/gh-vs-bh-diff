package com.yandex.mobile.ads.impl;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public final class sl implements sk {
    private final a a;
    private final SSLSocketFactory b;

    public interface a {
        String a();
    }

    public sl() {
        this(0);
    }

    private sl(byte b2) {
        this((SSLSocketFactory) null);
    }

    public sl(SSLSocketFactory sSLSocketFactory) {
        this.a = null;
        this.b = sSLSocketFactory;
    }

    public final HttpResponse a(rs<?> rsVar, Map<String, String> map) throws IOException, ry {
        String str;
        String b2 = rsVar.b();
        HashMap hashMap = new HashMap();
        hashMap.putAll(rsVar.a());
        hashMap.putAll(map);
        if (this.a != null) {
            str = this.a.a();
            if (str == null) {
                throw new IOException("URL blocked by rewriter: ".concat(String.valueOf(b2)));
            }
        } else {
            str = b2;
        }
        URL url = new URL(str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        int p = rsVar.p();
        httpURLConnection.setConnectTimeout(p);
        httpURLConnection.setReadTimeout(p);
        httpURLConnection.setUseCaches(false);
        boolean z = true;
        httpURLConnection.setDoInput(true);
        if ("https".equals(url.getProtocol()) && this.b != null) {
            ((HttpsURLConnection) httpURLConnection).setSSLSocketFactory(this.b);
        }
        for (String str2 : hashMap.keySet()) {
            httpURLConnection.addRequestProperty(str2, (String) hashMap.get(str2));
        }
        switch (rsVar.d()) {
            case -1:
                break;
            case 0:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                break;
            case 1:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                a(httpURLConnection, rsVar);
                break;
            case 2:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_PUT);
                a(httpURLConnection, rsVar);
                break;
            case 3:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_DELETE);
                break;
            case 4:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_HEAD);
                break;
            case 5:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_OPTIONS);
                break;
            case 6:
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_TRACE);
                break;
            case 7:
                httpURLConnection.setRequestMethod("PATCH");
                a(httpURLConnection, rsVar);
                break;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
        ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 1, 1);
        if (httpURLConnection.getResponseCode() != -1) {
            BasicStatusLine basicStatusLine = new BasicStatusLine(protocolVersion, httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage());
            BasicHttpResponse basicHttpResponse = new BasicHttpResponse(basicStatusLine);
            int d = rsVar.d();
            int statusCode = basicStatusLine.getStatusCode();
            if (d == 4 || ((100 <= statusCode && statusCode < 200) || statusCode == 204 || statusCode == 304)) {
                z = false;
            }
            if (z) {
                basicHttpResponse.setEntity(a(httpURLConnection));
            }
            for (Entry entry : httpURLConnection.getHeaderFields().entrySet()) {
                if (entry.getKey() != null) {
                    basicHttpResponse.addHeader(new BasicHeader((String) entry.getKey(), (String) ((List) entry.getValue()).get(0)));
                }
            }
            return basicHttpResponse;
        }
        throw new IOException("Could not retrieve response code from HttpUrlConnection.");
    }

    private static HttpEntity a(HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
        try {
            inputStream = httpURLConnection.getInputStream();
        } catch (IOException unused) {
            inputStream = httpURLConnection.getErrorStream();
        }
        basicHttpEntity.setContent(inputStream);
        basicHttpEntity.setContentLength((long) httpURLConnection.getContentLength());
        basicHttpEntity.setContentEncoding(httpURLConnection.getContentEncoding());
        basicHttpEntity.setContentType(httpURLConnection.getContentType());
        return basicHttpEntity;
    }

    private static void a(HttpURLConnection httpURLConnection, rs<?> rsVar) throws IOException, ry {
        byte[] c = rsVar.c();
        if (c != null) {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty("Content-Type", rs.k());
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(c);
            dataOutputStream.close();
        }
    }
}
