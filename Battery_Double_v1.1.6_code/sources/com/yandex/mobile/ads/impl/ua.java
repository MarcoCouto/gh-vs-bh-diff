package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.models.vmap.b;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Random;

public final class ua {
    @NonNull
    private final tp a = new tp();
    @NonNull
    private final tz b = new tz();

    @NonNull
    public static rs<tl> a(@NonNull Context context, @NonNull fl flVar, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull RequestListener<tl> requestListener) {
        b bVar = new b(vastRequestConfiguration);
        ve veVar = new ve(bVar);
        Builder appendQueryParameter = Uri.parse(bVar.a().a()).buildUpon().appendQueryParameter(HttpRequest.PARAM_CHARSET, "UTF-8").appendQueryParameter("rnd", Integer.toString(new Random().nextInt(89999999) + 10000000));
        tz.a(appendQueryParameter, vastRequestConfiguration.getParameters());
        tz.a(appendQueryParameter, "video-session-id", bVar.d());
        tz.a(appendQueryParameter, "uuid", flVar.d());
        new ty(context, flVar).a(context, appendQueryParameter);
        uf ufVar = new uf(context, appendQueryParameter.build().toString(), new tx.b(requestListener), vastRequestConfiguration, veVar);
        return ufVar;
    }
}
