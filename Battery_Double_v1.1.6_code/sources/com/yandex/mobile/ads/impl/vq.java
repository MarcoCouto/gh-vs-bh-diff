package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.ArrayList;
import java.util.List;

final class vq {
    @NonNull
    private final vn a;
    /* access modifiers changed from: private */
    @NonNull
    public final List<VideoAd> b = new ArrayList();
    /* access modifiers changed from: private */
    @Nullable
    public RequestListener<List<VideoAd>> c;
    /* access modifiers changed from: private */
    public int d;

    private class a implements RequestListener<List<VideoAd>> {
        private a() {
        }

        /* synthetic */ a(vq vqVar, byte b) {
            this();
        }

        public final /* synthetic */ void onSuccess(@NonNull Object obj) {
            List list = (List) obj;
            vq.this.d = vq.this.d - 1;
            vq.this.b.addAll(list);
            a();
        }

        public final void onFailure(@NonNull VideoAdError videoAdError) {
            vq.this.d = vq.this.d - 1;
            a();
        }

        private void a() {
            if (vq.this.d == 0 && vq.this.c != null) {
                vq.this.c.onSuccess(vq.this.b);
            }
        }
    }

    vq(@NonNull Context context, @NonNull vb vbVar) {
        this.a = new vn(context, vbVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull List<VideoAd> list, @NonNull RequestListener<List<VideoAd>> requestListener) {
        if (list.isEmpty()) {
            requestListener.onSuccess(this.b);
            return;
        }
        this.c = requestListener;
        for (VideoAd videoAd : list) {
            this.d++;
            this.a.a(context, videoAd, new a(this, 0));
        }
    }
}
