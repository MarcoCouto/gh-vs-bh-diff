package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.precache.DownloadManager;
import com.smaato.sdk.video.vast.model.Ad;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import com.yandex.mobile.ads.nativeads.z;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class pi {
    @NonNull
    private final pl a;
    @NonNull
    private final pp b = new pp(this.d);
    @NonNull
    private final pk c = new pk(this.d);
    @NonNull
    private final eb d;

    public pi(@NonNull Context context) {
        this.d = new eb(context);
        this.a = new pl(context, this.b);
    }

    @NonNull
    public final or a(@NonNull String str) throws JSONException, z {
        or a2 = a(new JSONObject(str));
        boolean z = false;
        if (a2 != null) {
            List c2 = a2.c();
            if (c2 != null && !c2.isEmpty()) {
                z = true;
            }
        }
        if (z) {
            return a2;
        }
        throw new z("Native Ad json has not required attributes");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0074 A[LOOP:1: B:10:0x0044->B:24:0x0074, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007a A[SYNTHETIC] */
    @VisibleForTesting
    private or a(JSONObject jSONObject) throws JSONException, z {
        boolean z;
        or orVar = (or) ib.a(or.class, new Object[0]);
        if (orVar != null) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("native");
            if (pj.a(jSONObject2, "ads")) {
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if ("ads".equals(str)) {
                        ArrayList arrayList = new ArrayList();
                        JSONArray jSONArray = jSONObject2.getJSONArray("ads");
                        int i = 0;
                        while (i < jSONArray.length()) {
                            oq f = f(jSONArray.getJSONObject(i));
                            if (f != null) {
                                List c2 = f.c();
                                on a2 = f.a();
                                NativeAdType b2 = f.b();
                                if (!c2.isEmpty()) {
                                    if ((a2 != null) && b2 != null) {
                                        z = true;
                                        if (!z) {
                                            arrayList.add(f);
                                            i++;
                                        } else {
                                            throw new z("Native Ad json has not required attributes");
                                        }
                                    }
                                }
                            }
                            z = false;
                            if (!z) {
                            }
                        }
                        orVar.b(arrayList);
                    } else if ("assets".equals(str)) {
                        List g = g(jSONObject2);
                        if (g.isEmpty()) {
                            g = null;
                        }
                        orVar.a(g);
                    } else if (DownloadManager.SETTINGS.equals(str)) {
                        orVar.a(b(jSONObject2));
                    } else if ("showNotices".equals(str)) {
                        orVar.c(c(jSONObject2));
                    } else if ("ver".equals(str)) {
                        orVar.a(a(jSONObject2, str));
                    } else if ("renderTrackingUrls".equals(str)) {
                        orVar.d(d(jSONObject2));
                    }
                }
            } else {
                throw new z("Native Ad json has not required attributes");
            }
        }
        return orVar;
    }

    private static os b(JSONObject jSONObject) throws JSONException, z {
        os osVar = (os) ib.a(os.class, new Object[0]);
        if (osVar != null) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(DownloadManager.SETTINGS);
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                if ("templateType".equals(str)) {
                    osVar.a(a(jSONObject2, str));
                } else if ("highlightingEnabled".equals(str)) {
                    osVar.a(jSONObject2.getBoolean(str));
                }
            }
        }
        return osVar;
    }

    private List<bn> c(JSONObject jSONObject) throws JSONException, z {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("showNotices");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(e(jSONArray.getJSONObject(i)));
        }
        return arrayList;
    }

    @NonNull
    private List<String> d(@NonNull JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("renderTrackingUrls");
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(this.d.a(jSONArray.getString(i)));
        }
        return arrayList;
    }

    @VisibleForTesting
    private bn e(JSONObject jSONObject) throws z, JSONException {
        bn bnVar = (bn) ib.a(bn.class, new Object[0]);
        if (bnVar != null) {
            if (pj.a(jSONObject, "delay", "url")) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if ("delay".equals(str)) {
                        bnVar.a(jSONObject.getLong(str));
                    } else if ("url".equals(str)) {
                        bnVar.a(this.c.a(jSONObject, str));
                    } else if ("visibilityPercent".equals(str)) {
                        bnVar.a(Math.max(Math.min(jSONObject.optInt(str, 0), 100), 0));
                    }
                }
            } else {
                throw new z("Native Ad json has not required attributes");
            }
        }
        return bnVar;
    }

    @VisibleForTesting
    private oq f(JSONObject jSONObject) throws JSONException, z {
        oq oqVar = (oq) ib.a(oq.class, new Object[0]);
        if (oqVar != null) {
            if (pj.a(jSONObject, Ad.AD_TYPE, "assets", "link")) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (Ad.AD_TYPE.equals(str)) {
                        oqVar.a(a(jSONObject, str));
                    } else if ("assets".equals(str)) {
                        oqVar.a(g(jSONObject));
                    } else if ("link".equals(str)) {
                        oqVar.a(this.b.a(jSONObject.getJSONObject(str)));
                    } else if ("showNotice".equals(str)) {
                        oqVar.a(e(jSONObject.getJSONObject(str)));
                    } else if (String.VIDEO_INFO.equals(str)) {
                        oqVar.d(jSONObject.optString(str, null));
                    } else if ("hideConditions".equals(str)) {
                        new pm();
                        oqVar.a(pm.a(jSONObject, str));
                    } else if ("showConditions".equals(str)) {
                        new pm();
                        oqVar.b(pm.a(jSONObject, str));
                    } else if ("renderTrackingUrl".equals(str)) {
                        oqVar.c(this.c.a(jSONObject, str));
                    }
                }
            } else {
                throw new z("Native Ad json has not required attributes");
            }
        }
        return oqVar;
    }

    @NonNull
    private List<oj> g(JSONObject jSONObject) throws JSONException, z {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("assets");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            boolean z = jSONObject2.getBoolean("required");
            try {
                arrayList.add(this.a.a(jSONObject2));
            } catch (z | JSONException e) {
                if (z) {
                    throw e;
                }
            }
        }
        if (!pj.a(arrayList)) {
            return arrayList;
        }
        throw new z("Native Ad json has not required attributes");
    }

    @NonNull
    public static String a(@NonNull JSONObject jSONObject, @NonNull String str) throws JSONException, z {
        String string = jSONObject.getString(str);
        if (!TextUtils.isEmpty(string) && !"null".equals(string)) {
            return string;
        }
        throw new z("Native Ad json has not required attributes");
    }
}
