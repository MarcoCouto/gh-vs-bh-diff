package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.impl.hr.c;
import com.yandex.mobile.ads.nativeads.NativeAdType;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class bk {
    @NonNull
    private final ct a = new ct();
    @NonNull
    private fl b;

    public bk(@NonNull fl flVar) {
        this.b = flVar;
    }

    public final void a(@NonNull Context context, @NonNull w wVar) {
        a(context, wVar, b.ADAPTER_REQUEST, Collections.emptyMap());
    }

    public final void b(@NonNull Context context, @NonNull w wVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("status", c.SUCCESS.a());
        a(context, wVar, b.ADAPTER_RESPONSE, hashMap);
    }

    public final void c(@NonNull Context context, @NonNull w wVar) {
        HashMap hashMap = new HashMap();
        new bj();
        hashMap.put("reward_info", bj.a(wVar));
        a(context, wVar, b.REWARD, hashMap);
    }

    private void a(@NonNull Context context, @NonNull w wVar, @NonNull b bVar, @NonNull Map<String, Object> map) {
        Map a2 = a(wVar);
        a2.putAll(map);
        hp.a(context).a(new hr(bVar, a2));
    }

    @NonNull
    private Map<String, Object> a(@NonNull w wVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("block_id", wVar.d());
        hashMap.put("adapter", "Yandex");
        com.yandex.mobile.ads.b a2 = wVar.a();
        hashMap.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, a2 != null ? a2.a() : null);
        hashMap.putAll(ct.a(this.b.c()));
        if (wVar.p() != null && (wVar.p() instanceof or)) {
            hashMap.put("native_ad_type", a(((or) wVar.p()).c()));
        }
        ea eaVar = new ea(hashMap);
        eaVar.a("ad_source", wVar.k());
        return eaVar.a();
    }

    @NonNull
    private static String a(@Nullable List<oq> list) {
        String str = "";
        if (list == null || list.isEmpty()) {
            return str;
        }
        NativeAdType b2 = ((oq) list.get(0)).b();
        return b2 != null ? b2.getValue() : str;
    }
}
