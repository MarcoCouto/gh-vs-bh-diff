package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.av.a;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qa implements pz<ok> {
    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, z {
        if (!jSONObject.has("value") || !jSONObject.isNull("value")) {
            return new ok(a.TEXT, pi.a(jSONObject, "value"));
        }
        return new ok(a.IMAGE, null);
    }
}
