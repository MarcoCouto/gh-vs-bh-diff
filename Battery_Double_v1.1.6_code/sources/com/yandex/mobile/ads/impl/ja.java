package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

final class ja implements iy {
    ja() {
    }

    @NonNull
    public final iv a(@NonNull ep epVar, @NonNull el elVar, @NonNull eh ehVar, @NonNull jf jfVar) {
        jc jcVar = new jc(epVar, elVar, ehVar, jfVar);
        jcVar.a(elVar);
        return jcVar;
    }

    @NonNull
    public final iu a(@NonNull ep epVar, @NonNull el elVar, @NonNull jg jgVar, @NonNull eo eoVar, @NonNull jf jfVar) {
        jb jbVar = new jb(epVar, elVar, jgVar, eoVar, jfVar);
        jbVar.a(elVar);
        return jbVar;
    }

    @NonNull
    public final iv a(@NonNull ep epVar, @NonNull el elVar) {
        je jeVar = new je(epVar, elVar);
        jeVar.a(elVar);
        return jeVar;
    }
}
