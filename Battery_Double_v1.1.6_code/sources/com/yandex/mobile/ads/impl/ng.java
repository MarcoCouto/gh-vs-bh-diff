package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.NativeGenericAd;
import com.yandex.mobile.ads.nativeads.bf;
import com.yandex.mobile.ads.nativeads.j;
import com.yandex.mobile.ads.nativeads.o;
import com.yandex.mobile.ads.nativeads.p;
import com.yandex.mobile.ads.nativeads.t;
import com.yandex.mobile.ads.nativeads.w;
import java.util.List;

final class ng implements na {
    @NonNull
    private final nm a = new nm();

    ng() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    public final void a(@NonNull Context context, @NonNull p pVar, @NonNull j jVar, @NonNull t tVar, @NonNull nc ncVar) {
        NativeGenericAd nativeGenericAd;
        List c = pVar.c().c();
        if (c != null && !c.isEmpty()) {
            oq oqVar = (oq) c.get(0);
            if (oqVar != null) {
                nl a2 = nm.a(oqVar.b());
                if (a2 != null) {
                    bf a3 = tVar.b().a(oqVar);
                    Context context2 = context;
                    nativeGenericAd = a2.a(context2, oqVar, new w(context, oqVar, jVar, a3), jVar, o.a(pVar, o.a(pVar, oqVar), a3, tVar));
                    if (nativeGenericAd == null) {
                        ncVar.a(nativeGenericAd);
                        return;
                    } else {
                        ncVar.a(u.a);
                        return;
                    }
                }
            }
        }
        nativeGenericAd = null;
        if (nativeGenericAd == null) {
        }
    }
}
