package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;

public final class ee {
    @Deprecated
    public static int a(@NonNull Context context, float f) {
        new dt();
        return dt.a(context, f);
    }

    @Deprecated
    public static int a(@NonNull Context context, int i) {
        new dt();
        return Math.round(((float) i) / context.getResources().getDisplayMetrics().density);
    }

    public static int a(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int b(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int c(Context context) {
        return (int) (((((float) a(context)) + 0.0f) / e(context)) + 0.5f);
    }

    public static int d(Context context) {
        return (int) (((((float) b(context)) + 0.0f) / e(context)) + 0.5f);
    }

    public static float e(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:1|2|(1:4)|5|6|(1:8)|9) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0013 */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0017 A[Catch:{ Exception -> 0x001d }] */
    public static void a(View view) {
        if (view != null) {
            if (view.getParent() instanceof ViewGroup) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            if (view instanceof ViewGroup) {
                ((ViewGroup) view).removeAllViews();
            }
        }
    }

    @NonNull
    public static Rect b(@NonNull View view) {
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        int i = iArr[0];
        int width = view.getWidth() + i;
        int i2 = iArr[1];
        return new Rect(i, i2, width, view.getHeight() + i2);
    }

    public static Bitmap c(View view) {
        if (view != null) {
            try {
                view.setDrawingCacheEnabled(true);
                float e = (1.0f / e(view.getContext())) / 5.0f;
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(view.getDrawingCache(), Math.round(((float) view.getWidth()) * e), Math.round(((float) view.getHeight()) * e), true);
                view.setDrawingCacheEnabled(false);
                return createScaledBitmap;
            } catch (Exception unused) {
            }
        }
        return null;
    }

    public static boolean a(int i) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        return fArr[2] < 0.5f;
    }

    public static int a(int i, float f) {
        return Color.argb(Math.max(0, Math.min(255, (int) (((100.0f - f) * 255.0f) / 100.0f))), Color.red(i), Color.green(i), Color.blue(i));
    }

    public static void a(final View view, final OnPreDrawListener onPreDrawListener) {
        view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public final boolean onPreDraw() {
                onPreDrawListener.onPreDraw();
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    public static boolean d(@NonNull View view) {
        if (view.isShown()) {
            return !(VERSION.SDK_INT < 11 || (view.getAlpha() > 0.0f ? 1 : (view.getAlpha() == 0.0f ? 0 : -1)) != 0);
        }
    }

    public static boolean a(@NonNull View view, int i) {
        return view.getWidth() < i || view.getHeight() < i;
    }

    public static boolean b(@Nullable View view, int i) {
        return e(view) >= i;
    }

    public static int e(@Nullable View view) {
        int i;
        int i2;
        if (view != null && view.isShown()) {
            Rect rect = new Rect();
            if (view.getGlobalVisibleRect(rect)) {
                int width = rect.width();
                int height = rect.height();
                if (view.getLayoutParams().width > 0) {
                    i = view.getLayoutParams().width;
                } else {
                    i = view.getWidth();
                }
                if (view.getLayoutParams().height > 0) {
                    i2 = view.getLayoutParams().height;
                } else {
                    i2 = view.getHeight();
                }
                return ((width * height) * 100) / (i2 * i);
            }
        }
        return 0;
    }
}
