package com.yandex.mobile.ads.impl;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class qt {
    private final int a;

    public qt(@NonNull Context context) {
        this.a = a(context);
    }

    private static int a(@NonNull Context context) {
        int round = Math.round(context.getResources().getDimension(17104896));
        try {
            return VERSION.SDK_INT >= 11 ? ((ActivityManager) context.getSystemService("activity")).getLauncherLargeIconSize() : round;
        } catch (Exception unused) {
            return round;
        }
    }

    @NonNull
    public final List<om> a(@NonNull oq oqVar) {
        ArrayList arrayList = new ArrayList();
        a(oqVar.a(), arrayList);
        List<oj> c = oqVar.c();
        if (c != null) {
            for (oj d : c) {
                a(d.d(), arrayList);
            }
        }
        return arrayList;
    }

    @NonNull
    private List<om> a(@Nullable on onVar, @NonNull List<om> list) {
        if (onVar != null) {
            List<ot> a2 = onVar.a();
            if (a2 != null) {
                for (ot otVar : a2) {
                    if (otVar instanceof ow) {
                        list.add(a(((ow) otVar).b()));
                    }
                }
            }
        }
        return list;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final om a(@NonNull String str) {
        om omVar = new om();
        omVar.a(str);
        omVar.b(this.a);
        omVar.a(this.a);
        return omVar;
    }
}
