package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import com.yandex.mobile.ads.nativeads.MediaView;

public final class mf extends ly {
    @NonNull
    private final me<ImageView, om> a;

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        MediaView mediaView = (MediaView) view;
        this.a.a();
        super.a(mediaView);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        op opVar = (op) obj;
        if (opVar.a() != null || opVar.b() == null) {
            return false;
        }
        return this.a.b(opVar.b());
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        op opVar = (op) obj;
        if (opVar.a() == null && opVar.b() != null) {
            this.a.a(opVar.b());
        }
    }

    public mf(@NonNull MediaView mediaView, @NonNull lx lxVar) {
        super(mediaView);
        this.a = new me<>(lxVar);
    }

    public final void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        this.a.a(ojVar, mjVar);
    }

    public final void a(@NonNull op opVar) {
        if (opVar.a() == null && opVar.b() != null) {
            this.a.a(opVar.b());
        }
    }
}
