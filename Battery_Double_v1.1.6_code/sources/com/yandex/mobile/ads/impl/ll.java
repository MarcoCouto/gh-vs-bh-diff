package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.j;

final class ll {
    @NonNull
    private final j a;
    @NonNull
    private final ln b;

    ll(@NonNull j jVar, @NonNull ln lnVar) {
        this.a = jVar;
        this.b = lnVar;
    }

    @Nullable
    static lf<String> a(@Nullable TextView textView) {
        md mcVar = textView != null ? new mc(textView) : null;
        if (mcVar != null) {
            return new lh(mcVar);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final lf<om> a(@Nullable ImageView imageView) {
        md lxVar = imageView != null ? new lx(imageView, this.a) : null;
        if (lxVar != null) {
            return new lj(lxVar);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final lf<op> a(@Nullable ImageView imageView, @Nullable MediaView mediaView) {
        lx lxVar = imageView != null ? new lx(imageView, this.a) : null;
        ly a2 = mediaView != null ? this.b.a(mediaView, this.a) : null;
        if (lxVar == null && a2 == null) {
            return null;
        }
        return new lk(lxVar, a2);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final lf<ol> b(@Nullable TextView textView) {
        md lwVar = textView != null ? new lw(textView, this.a) : null;
        if (lwVar != null) {
            return new lj(lwVar);
        }
        return null;
    }
}
