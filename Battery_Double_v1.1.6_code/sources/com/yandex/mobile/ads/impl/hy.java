package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class hy {
    @NonNull
    private final hw a = new hw();

    @NonNull
    public static String a(@Nullable Context context) {
        hv hvVar;
        if (context != null) {
            fw a2 = fv.a().a(context);
            if (a2 != null && a2.i()) {
                hvVar = new ht();
                return hvVar.a();
            }
        }
        hvVar = new hu();
        return hvVar.a();
    }
}
