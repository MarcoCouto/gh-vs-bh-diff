package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

public final class tc implements ta {
    @NonNull
    private final de a;
    @NonNull
    private final bq b;

    public tc(@NonNull Context context, @NonNull fl flVar, @NonNull bq bqVar) {
        this.a = new de(context, flVar);
        this.b = bqVar;
    }

    public final void a() {
        this.a.a(this.b.a());
    }
}
