package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.webkit.WebSettings;
import com.yandex.mobile.ads.impl.ip.a;
import java.io.File;

@SuppressLint({"ViewConstructor"})
public class ju extends ip {
    @NonNull
    private final er a = new er(this);

    public ju(@NonNull Context context, @NonNull w wVar) {
        super(context, wVar);
    }

    /* access modifiers changed from: protected */
    public final void a_(Context context) {
        super.a_(context);
        WebSettings settings = getSettings();
        settings.setAppCachePath(new File(getContext().getCacheDir(), "com.yandex.mobile.ads.cache").getAbsolutePath());
        settings.setDatabasePath(getContext().getDatabasePath("com.yandex.mobile.ads.db").getAbsolutePath());
        settings.setAppCacheMaxSize(8388608);
        settings.setAppCacheEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setCacheMode(-1);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"AddJavascriptInterface"})
    public final void a(@NonNull Context context) {
        addJavascriptInterface(new a(context), "AdPerformActionsJSI");
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.a.a(j());
    }
}
