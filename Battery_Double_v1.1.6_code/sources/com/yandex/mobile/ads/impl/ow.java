package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class ow extends ot {
    @NonNull
    private final String a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;

    public ow(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4) {
        super(str);
        this.a = str2;
        this.b = str3;
        this.c = str4;
    }

    @NonNull
    public final String b() {
        return this.a;
    }

    @NonNull
    public final String c() {
        return this.b;
    }

    @NonNull
    public final String d() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        ow owVar = (ow) obj;
        if (this.a.equals(owVar.a) && this.b.equals(owVar.b)) {
            return this.c.equals(owVar.c);
        }
        return false;
    }

    public final int hashCode() {
        return (((((super.hashCode() * 31) + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }
}
