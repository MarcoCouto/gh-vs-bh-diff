package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public interface lf<T> {
    void a();

    void a(@NonNull oj ojVar, @NonNull mj mjVar);

    void a(@NonNull T t);

    boolean b();

    boolean b(@NonNull T t);

    boolean c();

    boolean d();
}
