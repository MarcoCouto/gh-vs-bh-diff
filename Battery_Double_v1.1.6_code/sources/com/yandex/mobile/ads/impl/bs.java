package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class bs {

    public interface a<T> {
        @Nullable
        T a(String str);
    }

    public static String a(Map<String, String> map, rk rkVar) {
        return (String) map.get(rkVar.a());
    }

    public static boolean b(@NonNull Map<String, String> map, @NonNull rk rkVar) {
        String a2 = a(map, rkVar);
        if (a2 == null) {
            return false;
        }
        return Boolean.parseBoolean(a2);
    }

    @Nullable
    public static String d(@NonNull Map<String, String> map, @NonNull rk rkVar) {
        List e = e(map, rkVar);
        if (e.isEmpty()) {
            return null;
        }
        return (String) e.get(0);
    }

    @NonNull
    public static List<String> e(Map<String, String> map, rk rkVar) {
        return a(map, rkVar, new a<String>() {
            @Nullable
            public final /* bridge */ /* synthetic */ Object a(String str) {
                return str;
            }
        });
    }

    @NonNull
    public static <T> List<T> a(Map<String, String> map, rk rkVar, a<T> aVar) {
        ArrayList arrayList = new ArrayList();
        String a2 = a(map, rkVar);
        if (!TextUtils.isEmpty(a2)) {
            for (String trim : (String[]) hz.a((T[]) a2.split(","))) {
                try {
                    String decode = URLDecoder.decode(trim.trim(), "UTF-8");
                    if (aVar.a(decode) != null) {
                        arrayList.add(aVar.a(decode));
                    }
                } catch (Exception unused) {
                }
            }
        }
        return arrayList;
    }

    public static int c(Map<String, String> map, rk rkVar) {
        return dr.b(a(map, rkVar));
    }
}
