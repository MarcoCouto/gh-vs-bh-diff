package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.nativeads.MediaView;

public final class mg extends ly {
    @NonNull
    private final me<em, oo> a;

    public final void a(@NonNull op opVar) {
    }

    public final /* bridge */ /* synthetic */ void a(@NonNull View view) {
        MediaView mediaView = (MediaView) view;
        this.a.a();
        super.a(mediaView);
    }

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        op opVar = (op) obj;
        if (opVar.a() != null) {
            return this.a.b(opVar.a());
        }
        return false;
    }

    public final /* synthetic */ void b(@NonNull View view, @NonNull Object obj) {
        op opVar = (op) obj;
        if (opVar.a() != null) {
            this.a.a(opVar.a());
        }
    }

    public mg(@NonNull MediaView mediaView, @NonNull ma maVar) {
        super(mediaView);
        this.a = new me<>(maVar);
    }

    public final void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        this.a.a(ojVar, mjVar);
    }
}
