package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public final class er {
    /* access modifiers changed from: private */
    @NonNull
    public final ep a;
    @NonNull
    private final jv b = new jv();

    public er(@NonNull ep epVar) {
        this.a = epVar;
    }

    public final void a(@Nullable final Map<String, String> map) {
        this.b.a(new Runnable() {
            public final void run() {
                er.this.a.setVisibility(0);
                er.a(er.this, map);
            }
        });
    }

    static /* synthetic */ void a(er erVar, Map map) {
        el elVar = erVar.a.e;
        if (elVar != null) {
            elVar.onAdLoaded();
            elVar.a(erVar.a, map);
        }
    }
}
