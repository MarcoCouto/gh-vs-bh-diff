package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.b;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.HashMap;
import java.util.List;

public final class vf implements hs<VideoAdRequest, List<VideoAd>> {
    public final /* synthetic */ hr a(@Nullable ru ruVar, int i, @NonNull Object obj) {
        VideoAdRequest videoAdRequest = (VideoAdRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", videoAdRequest.getBlocksInfo().getPartnerId());
        hashMap.put("block_id", videoAdRequest.getBlockId());
        hashMap.put("page_ref", videoAdRequest.getPageRef());
        hashMap.put("target_ref", videoAdRequest.getTargetRef());
        if (i != -1) {
            hashMap.put("code", Integer.valueOf(i));
        }
        return new hr(b.VAST_RESPONSE, hashMap);
    }

    public final /* synthetic */ hr a(Object obj) {
        VideoAdRequest videoAdRequest = (VideoAdRequest) obj;
        HashMap hashMap = new HashMap();
        hashMap.put("partner_id", videoAdRequest.getBlocksInfo().getPartnerId());
        hashMap.put("block_id", videoAdRequest.getBlockId());
        hashMap.put("page_ref", videoAdRequest.getPageRef());
        hashMap.put("target_ref", videoAdRequest.getTargetRef());
        return new hr(b.VAST_REQUEST, hashMap);
    }
}
