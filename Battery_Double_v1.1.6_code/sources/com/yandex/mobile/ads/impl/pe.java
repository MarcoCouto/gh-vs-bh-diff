package com.yandex.mobile.ads.impl;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;

public final class pe {
    @NonNull
    private static final Object a = new Object();
    private static volatile pe b;
    @NonNull
    private final sm c;
    @NonNull
    private final b d;
    @NonNull
    private final pd e;

    @TargetApi(12)
    static class a implements com.yandex.mobile.ads.impl.sm.b {
        @NonNull
        private final LruCache<String, Bitmap> a;

        a(@NonNull LruCache<String, Bitmap> lruCache) {
            this.a = lruCache;
        }

        public final Bitmap a(String str) {
            return (Bitmap) this.a.get(str);
        }

        public final void a(String str, Bitmap bitmap) {
            this.a.put(str, bitmap);
        }
    }

    public interface b {
        @Nullable
        Bitmap a(@NonNull String str);

        void a(@NonNull String str, @NonNull Bitmap bitmap);
    }

    @TargetApi(12)
    @NonNull
    public static pe a(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new pe(context);
                }
            }
        }
        return b;
    }

    private pe(@NonNull Context context) {
        AnonymousClass1 r1 = new LruCache<String, Bitmap>(dw.a(context)) {
            /* access modifiers changed from: protected */
            public final /* synthetic */ int sizeOf(Object obj, Object obj2) {
                String str = (String) obj;
                Bitmap bitmap = (Bitmap) obj2;
                if (bitmap != null) {
                    return bitmap.getByteCount() / 1024;
                }
                return super.sizeOf(str, null);
            }
        };
        rt a2 = bv.a(context, 4);
        a2.a();
        a aVar = new a(r1);
        pb pbVar = new pb();
        this.d = new pf(r1, pbVar);
        this.e = new pd(dy.i(context));
        this.c = new pc(a2, aVar, pbVar, this.e);
    }

    @NonNull
    public final b a() {
        return this.d;
    }

    @NonNull
    public final sm b() {
        return this.c;
    }
}
