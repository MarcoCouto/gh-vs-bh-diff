package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.yandex.mobile.ads.impl.ir.a;
import java.lang.ref.WeakReference;

public final class fd<T extends View & com.yandex.mobile.ads.impl.ir.a> {
    @NonNull
    private final T a;
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    @NonNull
    private final fc c;
    @NonNull
    private final fe d;
    @Nullable
    private Runnable e;

    @VisibleForTesting
    static class a<T extends View & com.yandex.mobile.ads.impl.ir.a> implements Runnable {
        @NonNull
        private final WeakReference<fe> a;
        @NonNull
        private final WeakReference<T> b;
        @NonNull
        private final Handler c;
        @NonNull
        private final fc d;

        a(@NonNull T t, @NonNull fe feVar, @NonNull Handler handler, @NonNull fc fcVar) {
            this.b = new WeakReference<>(t);
            this.a = new WeakReference<>(feVar);
            this.c = handler;
            this.d = fcVar;
        }

        public final void run() {
            View view = (View) this.b.get();
            fe feVar = (fe) this.a.get();
            if (view != null && feVar != null) {
                feVar.a(fc.a(view));
                this.c.postDelayed(this, 200);
            }
        }
    }

    public fd(@NonNull T t, @NonNull fc fcVar, @NonNull fe feVar) {
        this.a = t;
        this.c = fcVar;
        this.d = feVar;
    }

    public final void a() {
        if (this.e == null) {
            this.e = new a(this.a, this.d, this.b, this.c);
            this.b.post(this.e);
        }
    }

    public final void b() {
        this.b.removeCallbacksAndMessages(null);
        this.e = null;
    }
}
