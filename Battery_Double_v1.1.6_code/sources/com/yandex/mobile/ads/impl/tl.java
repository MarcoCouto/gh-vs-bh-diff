package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class tl {
    @NonNull
    private final tk a;
    @Nullable
    private final String b;

    public tl(@NonNull tk tkVar, @Nullable String str) {
        this.a = tkVar;
        this.b = str;
    }

    @NonNull
    public final tk a() {
        return this.a;
    }

    @Nullable
    public final String b() {
        return this.b;
    }
}
