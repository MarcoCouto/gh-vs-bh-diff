package com.yandex.mobile.ads.impl;

import android.content.Intent;

public final class ry extends sf {
    private Intent b;

    public ry() {
    }

    public ry(rr rrVar) {
        super(rrVar);
    }

    public final String getMessage() {
        if (this.b != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }
}
