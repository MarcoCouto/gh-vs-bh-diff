package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public final class as implements at, jf {
    @NonNull
    private final a a;
    @NonNull
    private final au b;
    @Nullable
    private List<bn> c;
    @Nullable
    private w d;
    @NonNull
    private final ap e = ap.a();

    public interface a {
        void f();
    }

    public as(@NonNull a aVar, @NonNull cq cqVar) {
        this.a = aVar;
        this.b = new au(cqVar);
    }

    public final void a(@NonNull List<bn> list) {
        this.c = list;
        this.b.a();
    }

    public final void a(@NonNull w wVar) {
        this.d = wVar;
    }

    public final void a() {
        if (e()) {
            d();
            this.a.f();
        }
    }

    private void d() {
        if (this.d != null && this.d.m() != null) {
            this.e.b(this.d.m());
        }
    }

    public final void b() {
        if (!e()) {
            this.b.b();
        }
    }

    public final void d_() {
        if (!e()) {
            d();
            this.b.c();
            this.a.f();
        }
    }

    private boolean e() {
        return this.c != null && !this.c.isEmpty();
    }
}
