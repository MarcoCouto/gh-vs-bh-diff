package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.mobile.ads.AdEventListener;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.impl.ag.b;
import com.yandex.mobile.ads.impl.sv.a;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class aa<T> implements AdEventListener, ac, b, a<w<T>>, x.a {
    /* access modifiers changed from: protected */
    @NonNull
    public final Handler a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: protected */
    @NonNull
    public final Context b;
    @NonNull
    protected final s c = new s(this);
    @NonNull
    protected final x d = new x(this.a);
    @NonNull
    protected final ai e;
    @NonNull
    protected final fl f;
    @Nullable
    protected w<T> g;
    @NonNull
    private final gq h;
    @NonNull
    private final Executor i;
    @NonNull
    private final ag j;
    @NonNull
    private final dq k;
    @NonNull
    private final gk l;
    @NonNull
    private final de m;
    /* access modifiers changed from: private */
    @NonNull
    public final fn n;
    /* access modifiers changed from: private */
    @NonNull
    public final fr o;
    @NonNull
    private t p;
    private boolean q;
    private long r;
    @Nullable
    private AdEventListener s;
    @NonNull
    private gj t;
    /* access modifiers changed from: private */
    @NonNull
    public ft u;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract bw<T> a(String str, String str2);

    protected aa(@NonNull Context context, @NonNull com.yandex.mobile.ads.b bVar) {
        this.b = context;
        this.d.a(this);
        this.p = t.NOT_STARTED;
        this.j = ag.a();
        this.k = new Cdo();
        this.f = new fl(bVar);
        this.i = Executors.newSingleThreadExecutor(new dx("YandexMobileAds.BaseController"));
        this.e = new ai(context, this.f);
        this.l = new gk(this.f);
        this.t = gh.a(context);
        this.h = new gq();
        this.m = new de(context, this.f);
        this.n = new fn();
        this.o = new fr(this.n);
        this.u = fp.a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        return;
     */
    public synchronized void a(@Nullable AdRequest adRequest) {
        if (b()) {
            if (d(adRequest)) {
                a(t.NOT_STARTED);
                b(adRequest);
                return;
            }
            i();
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(@Nullable AdRequest adRequest) {
        a(adRequest, this.k);
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(@Nullable final AdRequest adRequest, @NonNull final dq dqVar) {
        this.a.post(new Runnable() {
            public final void run() {
                aa.this.c(adRequest);
                if (aa.this.n()) {
                    aa.this.a(t.LOADING);
                    aa.this.l.a(aa.this.t, (gk.a) new gk.a(dqVar) {
                        public final void a() {
                            aa.this.h.a(aa.this.b, new gv(r4) {
                                public final void a(@Nullable go goVar) {
                                    if (goVar != null) {
                                        aa.this.f.a(goVar.a());
                                        aa.this.f.b(goVar.b());
                                    }
                                    aa.this.i.execute(new Runnable(r4) {
                                        public final void run() {
                                            ai aiVar = aa.this.e;
                                            aa.this.n;
                                            aiVar.a(new ai.a() {
                                                public final void a() {
                                                    aa.this.a(r3);
                                                }

                                                public final void a(@NonNull sf sfVar) {
                                                    aa.this.b(sfVar);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }

                        public final void a(@NonNull String str) {
                            aa.this.onAdFailedToLoad(u.a(str));
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final void a(@NonNull final dq dqVar) {
        this.i.execute(new Runnable() {
            public final void run() {
                aa.this.o.a(aa.this.b, aa.this.u, new fu() {
                    public final void a(@Nullable String str) {
                        aa.this.f.f(str);
                        aa.this.b(dqVar);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final synchronized void b(@NonNull final dq dqVar) {
        this.i.execute(new Runnable() {
            public final void run() {
                if (!aa.this.a_()) {
                    String a2 = dqVar.a(aa.this.f);
                    if (!TextUtils.isEmpty(a2)) {
                        aa.this.f.a(dqVar.a());
                        dq dqVar = dqVar;
                        Context context = aa.this.b;
                        fl flVar = aa.this.f;
                        aa.this.n;
                        bw a3 = aa.this.a(a2, dqVar.a(context, flVar));
                        a3.a((Object) dr.a(this));
                        aa.this.c.a(a3);
                        return;
                    }
                    aa.this.onAdFailedToLoad(u.o);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final synchronized void c(AdRequest adRequest) {
        this.f.a(adRequest);
    }

    /* access modifiers changed from: protected */
    public final synchronized AdRequest j() {
        return this.f.c();
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(@NonNull t tVar) {
        new StringBuilder("assignLoadingState, state = ").append(tVar);
        this.p = tVar;
    }

    private synchronized boolean a() {
        return this.p == t.ERRONEOUSLY_LOADED;
    }

    @VisibleForTesting
    private synchronized boolean b() {
        new StringBuilder("isLoading, state = ").append(this.p);
        return this.p != t.LOADING;
    }

    public final synchronized boolean k() {
        return this.p == t.SUCCESSFULLY_LOADED;
    }

    public final synchronized boolean l() {
        return this.p == t.CANCELLED;
    }

    @NonNull
    public final Context m() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public boolean n() {
        boolean z;
        boolean z2;
        if (o()) {
            if (this.f.b() == null) {
                onAdFailedToLoad(u.q);
                z = false;
            } else {
                z = true;
            }
            if (z) {
                if (!dr.b(this.b)) {
                    onAdFailedToLoad(u.b);
                    z2 = false;
                } else {
                    z2 = true;
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public final boolean o() {
        return z() && p() && A() && g();
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public final boolean p() {
        try {
            du.a().a(this.b);
            return true;
        } catch (du.a e2) {
            onAdFailedToLoad(u.a(e2.getMessage()));
            return false;
        }
    }

    private boolean g() {
        if (this.f.c() != null) {
            return true;
        }
        onAdFailedToLoad(u.n);
        return false;
    }

    private boolean A() {
        if (this.f.n()) {
            return true;
        }
        onAdFailedToLoad(u.p);
        return false;
    }

    public final void a_(String str) {
        this.f.b(str);
    }

    public final String q() {
        return this.f.e();
    }

    public void a(@Nullable AdEventListener adEventListener) {
        this.s = adEventListener;
    }

    @Nullable
    public AdEventListener h() {
        return this.s;
    }

    public final void b(@Nullable al alVar) {
        this.f.a(alVar);
    }

    @NonNull
    public final fl r() {
        return this.f;
    }

    public void onAdFailedToLoad(@NonNull final AdRequestError adRequestError) {
        Cif.b(adRequestError.getDescription(), new Object[0]);
        a(t.ERRONEOUSLY_LOADED);
        this.a.post(new Runnable() {
            public final void run() {
                aa.this.a(adRequestError);
            }
        });
    }

    public void onAdLoaded() {
        s();
        i();
    }

    public final void s() {
        a(t.SUCCESSFULLY_LOADED);
        this.r = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: protected */
    public synchronized void i() {
        if (this.s != null) {
            this.s.onAdLoaded();
        }
    }

    public synchronized void onAdClosed() {
        if (this.s != null) {
            this.s.onAdClosed();
        }
    }

    public synchronized void onAdOpened() {
        if (this.s != null) {
            this.s.onAdOpened();
        }
    }

    public synchronized void onAdLeftApplication() {
        if (this.s != null) {
            this.s.onAdLeftApplication();
        }
    }

    public synchronized void d() {
        if (!a_()) {
            this.q = true;
            u();
            this.l.a(this.t);
            v();
            w();
            this.d.a(null);
            this.c.b();
            this.g = null;
            new Object[1][0] = getClass().toString();
        }
    }

    public final synchronized boolean a_() {
        return this.q;
    }

    public void a(int i2, @Nullable Bundle bundle) {
        switch (i2) {
            case 6:
                onAdOpened();
                return;
            case 7:
                onAdLeftApplication();
                break;
            case 8:
                onAdClosed();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(String str) {
        if (this.g != null) {
            this.m.a(str, this.g, new dj(this.b, this.f.k(), this.d));
        }
    }

    public void e() {
        d();
        new StringBuilder("onDestroy(), clazz = ").append(getClass());
    }

    /* access modifiers changed from: protected */
    public synchronized boolean d(AdRequest adRequest) {
        if (this.g == null || this.r <= 0 || SystemClock.elapsedRealtime() - this.r > ((long) this.g.v()) || ((adRequest != null && !adRequest.equals(this.f.c())) || a())) {
            return true;
        }
        return false;
    }

    public void a(@NonNull Intent intent) {
        new StringBuilder("action = ").append(intent.getAction());
    }

    public final void t() {
        new StringBuilder("registerPhoneStateTracker(), clazz = ").append(getClass());
        this.j.a(this, this.b);
    }

    public final void u() {
        new StringBuilder("unregisterPhoneStateTracker(), clazz = ").append(getClass());
        this.j.b(this, this.b);
    }

    /* access modifiers changed from: protected */
    public final void v() {
        this.h.a(this.b);
    }

    /* access modifiers changed from: protected */
    public final void w() {
        this.o.a(this.u);
    }

    /* access modifiers changed from: protected */
    public final boolean x() {
        return !this.j.a(this.b);
    }

    public final void a_(boolean z) {
        this.f.a(z);
    }

    @Nullable
    public final w<T> y() {
        return this.g;
    }

    public synchronized void a(@NonNull w<T> wVar) {
        this.g = wVar;
    }

    public final void a(@NonNull sf sfVar) {
        b(sfVar);
    }

    /* access modifiers changed from: private */
    public void b(@NonNull sf sfVar) {
        if (sfVar instanceof r) {
            onAdFailedToLoad(s.a(((r) sfVar).a()));
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(@NonNull AdRequestError adRequestError) {
        if (this.s != null) {
            this.s.onAdFailedToLoad(adRequestError);
        }
    }

    public void c(@NonNull dq dqVar) {
        a(this.f.c(), dqVar);
    }

    @VisibleForTesting
    private boolean z() {
        if (!du.b()) {
            onAdFailedToLoad(u.s);
            return false;
        } else if (aq.a()) {
            return true;
        } else {
            onAdFailedToLoad(u.r);
            return false;
        }
    }
}
