package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;

final class mp implements ml {
    private static final Object a = new Object();
    private static volatile mp b;

    public final void a(@NonNull Context context, @NonNull mo moVar) {
    }

    public final void b(@NonNull Context context, @NonNull mo moVar) {
    }

    mp() {
    }

    @NonNull
    public static mp a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new mp();
                }
            }
        }
        return b;
    }
}
