package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.b;

public final class fl {
    @Nullable
    private al a;
    @NonNull
    private final b b;
    @Nullable
    private AdRequest c;
    @Nullable
    private af d;
    @Nullable
    private ae e;
    @Nullable
    private String f;
    @Nullable
    private String g;
    @Nullable
    private String h;
    @Nullable
    private String i;
    @Nullable
    private String j;
    @Nullable
    private gm k;
    @Nullable
    private gm l;
    @Nullable
    private String[] m;
    @Nullable
    private int n;
    @Nullable
    private String[] o;
    @Nullable
    private String p;
    @Nullable
    private String q;
    private boolean r;
    private boolean s;
    private int t = 1;
    private int u = hn.b;

    public fl(@NonNull b bVar) {
        this.b = bVar;
    }

    @NonNull
    public final b a() {
        return this.b;
    }

    @Nullable
    public final al b() {
        return this.a;
    }

    public final void a(@Nullable al alVar) {
        if (alVar == null) {
            throw new IllegalArgumentException("Ad size can't be null or empty.");
        } else if (this.a == null) {
            this.a = alVar;
        } else {
            throw new IllegalArgumentException("Ad size can't be set twice.");
        }
    }

    @Nullable
    public final AdRequest c() {
        return this.c;
    }

    public final void a(@Nullable AdRequest adRequest) {
        this.c = adRequest;
    }

    @Nullable
    public final synchronized String d() {
        return this.f;
    }

    public final synchronized void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f = str;
        }
    }

    @Nullable
    public final String e() {
        return this.g;
    }

    @Nullable
    public final synchronized String f() {
        return this.h;
    }

    public final synchronized void c(@Nullable String str) {
        this.h = str;
    }

    public final synchronized void d(@Nullable String str) {
        this.i = str;
    }

    @Nullable
    public final synchronized String g() {
        return this.i;
    }

    @Nullable
    public final String h() {
        return this.j;
    }

    @Nullable
    public final gm i() {
        return this.k;
    }

    public final synchronized void a(@Nullable gm gmVar) {
        this.k = gmVar;
    }

    @Nullable
    public final gm j() {
        return this.l;
    }

    public final synchronized void b(@Nullable gm gmVar) {
        this.l = gmVar;
    }

    public final boolean k() {
        return this.r;
    }

    public final void a(boolean z) {
        this.r = z;
    }

    public final int l() {
        return this.t;
    }

    public final int m() {
        return this.u;
    }

    public final boolean n() {
        return !TextUtils.isEmpty(this.g);
    }

    public final void a(@NonNull String[] strArr) {
        this.m = strArr;
    }

    @Nullable
    public final String[] o() {
        return this.m;
    }

    public final void a(@NonNull ae aeVar) {
        this.e = aeVar;
    }

    @Nullable
    public final ae p() {
        return this.e;
    }

    public final void a(@NonNull af afVar) {
        this.d = afVar;
    }

    @Nullable
    public final af q() {
        return this.d;
    }

    public final void a(@NonNull int i2) {
        this.n = i2;
    }

    @Nullable
    public final int r() {
        return this.n;
    }

    public final void b(@NonNull String[] strArr) {
        this.o = strArr;
    }

    @Nullable
    public final String[] s() {
        return this.o;
    }

    @Nullable
    public final String t() {
        return this.p;
    }

    public final void e(@Nullable String str) {
        this.p = str;
    }

    public final void b(boolean z) {
        this.s = z;
    }

    public final boolean u() {
        return this.s;
    }

    @Nullable
    public final String v() {
        return this.q;
    }

    public final void f(@Nullable String str) {
        this.q = str;
    }

    public final void b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Block ID can't be null or empty.");
        } else if (TextUtils.isEmpty(this.g)) {
            this.g = str;
        } else {
            throw new IllegalArgumentException("Block ID can't be set twice.");
        }
    }
}
