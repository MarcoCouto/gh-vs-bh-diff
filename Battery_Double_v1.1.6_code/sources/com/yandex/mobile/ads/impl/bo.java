package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public class bo implements Parcelable {
    public static final Creator<bo> CREATOR = new Creator<bo>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bo[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new bo(parcel);
        }
    };
    private final int a;
    @NonNull
    private final String b;

    public int describeContents() {
        return 0;
    }

    public bo(int i, @NonNull String str) {
        this.a = i;
        this.b = str;
    }

    public final int a() {
        return this.a;
    }

    @NonNull
    public final String b() {
        return this.b;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
    }

    protected bo(@NonNull Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readString();
    }
}
