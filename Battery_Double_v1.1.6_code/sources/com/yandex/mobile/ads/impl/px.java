package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import com.yandex.mobile.ads.nativeads.z;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

final class px {
    @NonNull
    private final pw a = new pw();
    @NonNull
    private final pk b;

    px(@NonNull pk pkVar) {
        this.b = pkVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final oy a(@NonNull JSONObject jSONObject) throws JSONException, z {
        HashMap hashMap;
        String a2 = pi.a(jSONObject, "package");
        String a3 = this.b.a(jSONObject, "url");
        JSONObject optJSONObject = jSONObject.optJSONObject("extras");
        if (optJSONObject != null) {
            Iterator keys = optJSONObject.keys();
            if (keys.hasNext()) {
                hashMap = new HashMap();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    if (!optJSONObject.isNull(str)) {
                        hashMap.put(str, optJSONObject.get(str));
                    }
                }
                return new oy(a2, a3, hashMap);
            }
        }
        hashMap = null;
        return new oy(a2, a3, hashMap);
    }
}
