package com.yandex.mobile.ads.impl;

public final class pd {
    private final int a;

    pd(int i) {
        this.a = i;
    }

    /* access modifiers changed from: 0000 */
    public final int a(int i) {
        return i > 0 ? Math.min(i, this.a) : this.a;
    }
}
