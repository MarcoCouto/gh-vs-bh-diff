package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public final class ri {
    @NonNull
    private final hp a;
    @NonNull
    private final ct b;
    @NonNull
    private final fl c;
    @NonNull
    private final List<oj> d;

    public ri(@NonNull Context context, @NonNull fl flVar, @Nullable List<oj> list) {
        this.c = flVar;
        if (list == null) {
            list = Collections.emptyList();
        }
        this.d = list;
        this.a = hp.a(context);
        this.b = new ct();
    }

    public final void a(@NonNull List<String> list) {
        List<oj> list2 = this.d;
        ArrayList arrayList = new ArrayList();
        for (oj a2 : list2) {
            arrayList.add(a2.a());
        }
        ArrayList arrayList2 = new ArrayList(list);
        arrayList2.removeAll(arrayList);
        if (!arrayList2.isEmpty()) {
            HashMap hashMap = new HashMap();
            String e = this.c.e();
            if (e != null) {
                hashMap.put("block_id", e);
            }
            hashMap.put("assets", arrayList2.toArray());
            hashMap.putAll(ct.a(this.c.c()));
            this.a.a(new hr(b.REQUIRED_ASSET_MISSING, hashMap));
        }
    }
}
