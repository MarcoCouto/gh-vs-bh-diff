package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qd implements pz<op> {
    @NonNull
    private final qj a = new qj();
    @NonNull
    private final qi b;

    public qd(@NonNull Context context) {
        this.b = new qi(context);
    }

    @Nullable
    private static <T> T a(@NonNull JSONObject jSONObject, @NonNull String str, @NonNull qh<T> qhVar) throws JSONException, z {
        if (a(jSONObject, str)) {
            return qhVar.a(jSONObject.getJSONObject(str));
        }
        return null;
    }

    private static boolean a(@NonNull JSONObject jSONObject, @NonNull String str) {
        return jSONObject.has(str) && !jSONObject.isNull(str);
    }

    @NonNull
    public final /* synthetic */ Object a(@NonNull JSONObject jSONObject) throws JSONException, z {
        if (a(jSONObject, "value")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("value");
            oo ooVar = (oo) a(jSONObject2, "media", this.a);
            om omVar = (om) a(jSONObject2, MessengerShareContentUtility.MEDIA_IMAGE, this.b);
            if (ooVar != null || omVar != null) {
                return new op(ooVar, omVar);
            }
            throw new z("Native Ad json has not required attributes");
        }
        throw new z("Native Ad json has not required attributes");
    }
}
