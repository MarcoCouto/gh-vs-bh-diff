package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;

final class jz implements jw {
    @NonNull
    private final w a;
    /* access modifiers changed from: private */
    @NonNull
    public final View b;
    @NonNull
    private final Handler c = new Handler(Looper.getMainLooper());
    @NonNull
    private final ka d;
    private boolean e;

    jz(@NonNull w wVar, @NonNull View view, @NonNull ka kaVar) {
        this.a = wVar;
        this.b = view;
        this.b.setVisibility(8);
        this.d = kaVar;
    }

    @NonNull
    public final View a() {
        return this.b;
    }

    public final void a(boolean z) {
        this.e = z;
        e();
        this.b.setVisibility(z ? 8 : 0);
    }

    public final void c() {
        e();
    }

    public final boolean d() {
        return this.e && this.a.z();
    }

    private void e() {
        this.c.removeCallbacksAndMessages(null);
    }

    public final void b() {
        this.c.postDelayed(new Runnable() {
            public final void run() {
                jz.this.b.setVisibility(0);
            }
        }, 200);
    }
}
