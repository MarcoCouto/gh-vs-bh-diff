package com.yandex.mobile.ads.impl;

import android.os.Process;
import com.yandex.mobile.ads.impl.rl.a;
import java.util.concurrent.BlockingQueue;

public final class rm extends Thread {
    private static final boolean a = rx.b;
    private final BlockingQueue<rs<?>> b;
    /* access modifiers changed from: private */
    public final BlockingQueue<rs<?>> c;
    private final rl d;
    private final rv e;
    private volatile boolean f = false;

    public rm(BlockingQueue<rs<?>> blockingQueue, BlockingQueue<rs<?>> blockingQueue2, rl rlVar, rv rvVar) {
        this.b = blockingQueue;
        this.c = blockingQueue2;
        this.d = rlVar;
        this.e = rvVar;
    }

    public final void a() {
        this.f = true;
        interrupt();
    }

    public final void run() {
        if (a) {
            rx.a("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.d.a();
        while (true) {
            try {
                final rs rsVar = (rs) this.b.take();
                if (rsVar.j()) {
                    rsVar.g();
                } else {
                    a a2 = this.d.a(rsVar.b());
                    if (a2 == null) {
                        this.c.put(rsVar);
                    } else {
                        if (a2.e < System.currentTimeMillis()) {
                            rsVar.a(a2);
                            this.c.put(rsVar);
                        } else {
                            ru a3 = rsVar.a(new rr(a2.a, a2.g));
                            if (!(a2.f < System.currentTimeMillis())) {
                                this.e.a(rsVar, a3);
                            } else {
                                rsVar.a(a2);
                                a3.d = true;
                                this.e.a(rsVar, a3, new Runnable() {
                                    public final void run() {
                                        try {
                                            rm.this.c.put(rsVar);
                                        } catch (InterruptedException unused) {
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            } catch (InterruptedException unused) {
                if (this.f) {
                    return;
                }
            }
        }
    }
}
