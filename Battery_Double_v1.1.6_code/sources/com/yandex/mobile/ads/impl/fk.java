package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class fk implements fh {
    private final boolean a;

    public fk(boolean z) {
        this.a = z;
    }

    @NonNull
    public final String a() {
        return String.format("viewable: %s", new Object[]{Boolean.valueOf(this.a)});
    }
}
