package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class mt implements mq {
    @NonNull
    private final View a;

    public mt(@NonNull View view) {
        this.a = view;
    }

    public final void a(@NonNull on onVar, @NonNull mr mrVar) {
        this.a.setOnClickListener(mrVar.a(onVar));
        this.a.setOnTouchListener(mx.a(this.a.getContext()));
    }
}
