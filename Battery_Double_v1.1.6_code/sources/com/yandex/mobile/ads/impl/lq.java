package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.j;

public final class lq implements lo {
    @NonNull
    private final lr a = new lr();
    @NonNull
    private final ls b = new ls();

    @Nullable
    public final ly a(@NonNull MediaView mediaView, @NonNull j jVar, @NonNull as asVar, @Nullable op opVar) {
        if (opVar != null) {
            if (opVar.a() != null) {
                em emVar = new em(mediaView.getContext(), asVar);
                mediaView.removeAllViews();
                mediaView.addView(emVar, new LayoutParams(-1, -1));
                return new mg(mediaView, new ma(emVar));
            } else if (opVar.b() != null) {
                ImageView imageView = new ImageView(mediaView.getContext());
                mediaView.removeAllViews();
                imageView.setAdjustViewBounds(true);
                imageView.setScaleType(ScaleType.FIT_CENTER);
                mediaView.addView(imageView, new LayoutParams(-1, -1));
                return new mf(mediaView, new lx(imageView, jVar));
            }
        }
        return null;
    }
}
