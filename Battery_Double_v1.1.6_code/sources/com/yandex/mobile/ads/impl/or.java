package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class or {
    private String a;
    private List<oj> b;
    private List<oq> c;
    private os d;
    private List<bn> e;
    @Nullable
    private List<String> f;
    private Map<String, Object> g = new HashMap();

    public final void a(List<oj> list) {
        this.b = list;
    }

    public final void b(List<oq> list) {
        this.c = list;
    }

    public final void a(os osVar) {
        this.d = osVar;
    }

    public final void c(List<bn> list) {
        this.e = list;
    }

    public final void d(@NonNull List<String> list) {
        this.f = list;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final void a(String str, Object obj) {
        this.g.put(str, obj);
    }

    @NonNull
    public final Map<String, Object> a() {
        return this.g;
    }

    public final List<oj> b() {
        return this.b;
    }

    public final List<oq> c() {
        return this.c;
    }

    public final os d() {
        return this.d;
    }

    @Nullable
    public final List<bn> e() {
        return this.e;
    }

    @Nullable
    public final List<String> f() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        or orVar = (or) obj;
        if (this.a == null ? orVar.a != null : !this.a.equals(orVar.a)) {
            return false;
        }
        if (this.b == null ? orVar.b != null : !this.b.equals(orVar.b)) {
            return false;
        }
        if (this.c == null ? orVar.c != null : !this.c.equals(orVar.c)) {
            return false;
        }
        if (this.d == null ? orVar.d != null : !this.d.equals(orVar.d)) {
            return false;
        }
        if (this.e == null ? orVar.e != null : !this.e.equals(orVar.e)) {
            return false;
        }
        if (this.f == null ? orVar.f != null : !this.f.equals(orVar.f)) {
            return false;
        }
        if (this.g != null) {
            return this.g.equals(orVar.g);
        }
        return orVar.g == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31;
        if (this.g != null) {
            i = this.g.hashCode();
        }
        return hashCode + i;
    }
}
