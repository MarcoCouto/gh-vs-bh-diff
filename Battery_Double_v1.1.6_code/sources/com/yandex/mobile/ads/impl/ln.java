package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.j;

public final class ln {
    @NonNull
    private final lg a;
    @NonNull
    private final as b;
    @NonNull
    private final lo c;

    public ln(@NonNull lg lgVar, @NonNull as asVar, @NonNull lo loVar) {
        this.a = lgVar;
        this.b = asVar;
        this.c = loVar;
    }

    @Nullable
    public final ly a(@NonNull MediaView mediaView, @NonNull j jVar) {
        return this.c.a(mediaView, jVar, this.b, this.a.a());
    }
}
