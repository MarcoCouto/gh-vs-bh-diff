package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.view.View;

public final class me<V extends View, T> {
    @NonNull
    private final md<V, T> a;

    me(@NonNull md<V, T> mdVar) {
        this.a = mdVar;
    }

    public final void a() {
        View a2 = this.a.a();
        if (a2 != null) {
            this.a.a(a2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull oj ojVar, @NonNull mj mjVar) {
        if (this.a.a() != null) {
            this.a.a(ojVar, mjVar);
        }
    }

    public final void a(@NonNull T t) {
        View a2 = this.a.a();
        if (a2 != null) {
            this.a.b(a2, t);
            a2.setVisibility(0);
        }
    }

    public final boolean b(@NonNull T t) {
        View a2 = this.a.a();
        return a2 != null && this.a.a(a2, t);
    }
}
