package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class fr {
    @NonNull
    private final Object a = new Object();
    @NonNull
    private final fq b;
    @NonNull
    private final List<fu> c = new CopyOnWriteArrayList();

    public fr(@NonNull fn fnVar) {
        this.b = new fq(fnVar);
    }

    public final void a(@NonNull Context context, @NonNull ft ftVar, @NonNull fu fuVar) {
        synchronized (this.a) {
            boolean a2 = fn.a(context);
            fw a3 = fv.a().a(context);
            if (!a2 && a3 != null && a3.l()) {
                synchronized (this.a) {
                    this.c.add(fuVar);
                    ftVar.b(fuVar);
                }
            } else {
                fuVar.a(null);
            }
        }
    }

    public final void a(@NonNull ft ftVar) {
        synchronized (this.a) {
            for (fu a2 : this.c) {
                ftVar.a(a2);
            }
            this.c.clear();
        }
    }
}
