package com.yandex.mobile.ads.impl;

import android.support.annotation.Nullable;
import java.util.List;

public final class on {
    @Nullable
    private final List<ot> a;
    @Nullable
    private final di b;
    @Nullable
    private final String c;
    @Nullable
    private final String d;

    public on(@Nullable List<ot> list, @Nullable di diVar, @Nullable String str, @Nullable String str2) {
        this.a = list;
        this.b = diVar;
        this.c = str;
        this.d = str2;
    }

    @Nullable
    public final List<ot> a() {
        return this.a;
    }

    @Nullable
    public final di b() {
        return this.b;
    }

    @Nullable
    public final String c() {
        return this.c;
    }

    @Nullable
    public final String d() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        on onVar = (on) obj;
        if (this.a == null ? onVar.a != null : !this.a.equals(onVar.a)) {
            return false;
        }
        if (this.b == null ? onVar.b != null : !this.b.equals(onVar.b)) {
            return false;
        }
        if (this.c == null ? onVar.c != null : !this.c.equals(onVar.c)) {
            return false;
        }
        if (this.d != null) {
            return this.d.equals(onVar.d);
        }
        return onVar.d == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return hashCode + i;
    }
}
