package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.internal.view.SupportMenu;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.places.model.PlaceFields;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class dy {
    private static final Comparator<ScanResult> a = new Comparator<ScanResult>() {
        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return ((ScanResult) obj).level - ((ScanResult) obj2).level;
        }
    };

    @SuppressLint({"MissingPermission"})
    public static Integer a(Context context) {
        try {
            String substring = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getNetworkOperator().substring(0, 3);
            if (!TextUtils.isEmpty(substring)) {
                return Integer.valueOf(Integer.parseInt(substring));
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    @SuppressLint({"MissingPermission"})
    public static Integer b(Context context) {
        try {
            String substring = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getNetworkOperator().substring(3);
            if (!TextUtils.isEmpty(substring)) {
                return Integer.valueOf(Integer.parseInt(substring));
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public static String c(Context context) {
        try {
            String networkOperatorName = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getNetworkOperatorName();
            if (!TextUtils.isEmpty(networkOperatorName)) {
                return networkOperatorName;
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    @Nullable
    public static String d(@NonNull Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo.getType() != 0) {
                return activeNetworkInfo.getTypeName();
            }
            return activeNetworkInfo.getSubtypeName();
        } catch (Exception unused) {
            return null;
        }
    }

    @SuppressLint({"MissingPermission"})
    public static Integer e(Context context) {
        try {
            int cid = ((GsmCellLocation) ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getCellLocation()).getCid();
            if (-1 != cid) {
                return Integer.valueOf(cid & SupportMenu.USER_MASK);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    @SuppressLint({"MissingPermission"})
    public static Integer f(Context context) {
        try {
            int lac = ((GsmCellLocation) ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getCellLocation()).getLac();
            if (-1 != lac) {
                return Integer.valueOf(lac & SupportMenu.USER_MASK);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public static String g(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi");
            if (!wifiManager.isWifiEnabled()) {
                return null;
            }
            List scanResults = wifiManager.getScanResults();
            ScanResult[] scanResultArr = (ScanResult[]) hz.a((T[]) scanResults.toArray(new ScanResult[scanResults.size()]));
            Arrays.sort(scanResultArr, Collections.reverseOrder(a));
            if (scanResultArr.length > 0) {
                StringBuilder sb = new StringBuilder();
                int min = Math.min(3, scanResultArr.length);
                int i = 0;
                while (i < min) {
                    sb.append(scanResultArr[i].BSSID);
                    sb.append(",");
                    sb.append(scanResultArr[i].level);
                    i++;
                    sb.append(i < min ? ";" : "");
                }
                return sb.toString();
            }
            return null;
        } catch (Exception unused) {
        }
    }

    public static Integer h(Context context) {
        try {
            Intent registerReceiver = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver == null) {
                return null;
            }
            int intExtra = registerReceiver.getIntExtra("level", -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra < 0 || intExtra2 <= 0) {
                return null;
            }
            return Integer.valueOf(Math.round((((float) intExtra) / ((float) intExtra2)) * 100.0f));
        } catch (Exception unused) {
            return null;
        }
    }

    public static int i(@NonNull Context context) {
        Point j = j(context);
        return Math.min(j.x, j.y);
    }

    public static Point j(Context context) {
        int i;
        int i2;
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (VERSION.SDK_INT >= 17) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getRealMetrics(displayMetrics);
            i = displayMetrics.widthPixels;
            i2 = displayMetrics.heightPixels;
        } else {
            if (VERSION.SDK_INT >= 14) {
                try {
                    Method method = Display.class.getMethod("getRawHeight", new Class[0]);
                    int intValue = ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(defaultDisplay, new Object[0])).intValue();
                    i2 = ((Integer) method.invoke(defaultDisplay, new Object[0])).intValue();
                    i = intValue;
                } catch (Exception unused) {
                }
            }
            int width = defaultDisplay.getWidth();
            i2 = defaultDisplay.getHeight();
            i = width;
        }
        return new Point(i, i2);
    }
}
