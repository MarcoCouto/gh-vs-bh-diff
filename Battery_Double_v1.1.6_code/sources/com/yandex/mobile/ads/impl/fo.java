package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.p.Ucc;
import org.json.JSONObject;

final class fo {

    public interface a {
        void a();

        void a(@Nullable String str);
    }

    private static class b implements Ucc {
        @NonNull
        private final a a;
        @NonNull
        private final fs b = new fs();

        b(@NonNull a aVar) {
            this.a = aVar;
        }

        public final void onError(@Nullable String str) {
            this.a.a();
        }

        public final void onResult(@Nullable JSONObject jSONObject) {
            String str;
            if (jSONObject != null) {
                str = ca.a(jSONObject.toString());
            } else {
                str = null;
            }
            this.a.a(str);
        }
    }

    fo() {
    }
}
