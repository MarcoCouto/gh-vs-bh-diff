package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public class bq implements Parcelable {
    public static final Creator<bq> CREATOR = new Creator<bq>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new bq[i];
        }

        public final /* synthetic */ Object createFromParcel(@NonNull Parcel parcel) {
            return new bq(parcel);
        }
    };
    @NonNull
    private final String a;

    public int describeContents() {
        return 0;
    }

    public bq(@NonNull String str) {
        this.a = str;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.a);
    }

    protected bq(@NonNull Parcel parcel) {
        this.a = parcel.readString();
    }
}
