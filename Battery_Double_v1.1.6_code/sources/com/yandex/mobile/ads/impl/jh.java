package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.sv.a;

public final class jh extends bw<String> {
    @NonNull
    private final bt a = new bt();

    public jh(@NonNull Context context, @NonNull fl flVar, @NonNull String str, @NonNull String str2, @NonNull a<w<String>> aVar) {
        super(context, flVar, str, str2, aVar, new jm());
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final /* synthetic */ Object a_(@NonNull rr rrVar) {
        return bt.a(rrVar);
    }
}
