package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.MediaView;
import com.yandex.mobile.ads.nativeads.j;

public final class lp implements lo {
    @NonNull
    public final ly a(@NonNull MediaView mediaView, @NonNull j jVar, @NonNull as asVar, @Nullable op opVar) {
        mediaView.removeAllViews();
        return new lz(mediaView);
    }
}
