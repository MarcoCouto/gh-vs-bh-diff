package com.yandex.mobile.ads.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import com.yandex.mobile.ads.nativeads.Rating;

public final class mx implements OnTouchListener {
    private static volatile mx a;
    private static final Object b = new Object();
    private final Handler c = new Handler();
    private final GestureDetector d;
    private boolean e;

    public static mx a(@NonNull Context context) {
        if (a == null) {
            synchronized (b) {
                if (a == null) {
                    a = new mx(context);
                }
            }
        }
        return a;
    }

    private mx(@NonNull Context context) {
        this.d = new GestureDetector(context, new SimpleOnGestureListener()) {
            public final boolean onTouchEvent(MotionEvent motionEvent) {
                return super.onTouchEvent(motionEvent);
            }
        };
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final boolean onTouch(final View view, MotionEvent motionEvent) {
        if (!(view instanceof TextView) && !(view instanceof Rating)) {
            return false;
        }
        if (motionEvent.getAction() == 1) {
            if (VERSION.SDK_INT >= 11 && view != null && !this.e) {
                view.setAlpha(view.getAlpha() / 2.0f);
                this.e = true;
            }
            this.c.postDelayed(new Runnable() {
                public final void run() {
                    mx.a(mx.this, view);
                }
            }, 100);
        }
        return this.d.onTouchEvent(motionEvent);
    }

    static /* synthetic */ void a(mx mxVar, View view) {
        if (VERSION.SDK_INT >= 11 && view != null && mxVar.e) {
            view.setAlpha(view.getAlpha() * 2.0f);
            mxVar.e = false;
        }
    }
}
