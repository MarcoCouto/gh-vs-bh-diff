package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;

public final class lc {
    @NonNull
    public static ld a(@NonNull w<or> wVar) {
        bl n = wVar.n();
        if (n != null) {
            return new lb(wVar, n);
        }
        return new le();
    }
}
