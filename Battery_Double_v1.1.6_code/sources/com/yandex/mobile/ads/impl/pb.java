package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.widget.ImageView.ScaleType;

public final class pb {
    @NonNull
    static String a(@NonNull String str) {
        return b(str, ScaleType.CENTER_INSIDE);
    }

    @NonNull
    static String a(@NonNull String str, @NonNull ScaleType scaleType) {
        return b(str, scaleType);
    }

    @NonNull
    private static String b(@NonNull String str, @NonNull ScaleType scaleType) {
        StringBuilder sb = new StringBuilder("#S");
        sb.append(scaleType.ordinal());
        sb.append(str);
        return sb.toString();
    }
}
