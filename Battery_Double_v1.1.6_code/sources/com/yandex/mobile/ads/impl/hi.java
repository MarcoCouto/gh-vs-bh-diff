package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

final class hi {
    @NonNull
    private final Context a;
    @NonNull
    private final String b;
    @NonNull
    private final ib c = new ib();

    hi(@NonNull Context context, @NonNull String str) {
        this.a = context.getApplicationContext();
        this.b = str;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final hf a() {
        Class a2 = ib.a(this.b);
        if (a2 != null) {
            Object a3 = ib.a(a2, "getFusedLocationProviderClient", this.a);
            if (a3 != null) {
                return new hf(a3);
            }
        }
        return null;
    }
}
