package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.j;

public final class mk {
    @NonNull
    private final j a;
    @NonNull
    private final rd b = new rd();

    public mk(@NonNull j jVar) {
        this.a = jVar;
    }

    public final boolean a(@Nullable Drawable drawable, @NonNull om omVar) {
        Bitmap a2 = this.a.a(omVar);
        if (drawable == null || a2 == null) {
            return false;
        }
        return rd.a(drawable).a(drawable, a2);
    }
}
