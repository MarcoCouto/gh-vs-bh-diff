package com.yandex.mobile.ads.impl;

import android.net.TrafficStats;
import android.os.Build.VERSION;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public final class rq extends Thread {
    private final BlockingQueue<rs<?>> a;
    private final rp b;
    private final rl c;
    private final rv d;
    private volatile boolean e = false;

    public rq(BlockingQueue<rs<?>> blockingQueue, rp rpVar, rl rlVar, rv rvVar) {
        this.a = blockingQueue;
        this.b = rpVar;
        this.c = rlVar;
        this.d = rvVar;
    }

    public final void a() {
        this.e = true;
        interrupt();
    }

    public final void run() {
        Process.setThreadPriority(10);
        while (true) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                rs rsVar = (rs) this.a.take();
                try {
                    if (rsVar.j()) {
                        rsVar.g();
                    } else {
                        if (VERSION.SDK_INT >= 14) {
                            TrafficStats.setThreadStatsTag(rsVar.f());
                        }
                        rr a2 = this.b.a(rsVar);
                        if (!a2.d || !rsVar.s()) {
                            ru a3 = rsVar.a(a2);
                            if (rsVar.m() && a3.b != null) {
                                this.c.a(rsVar.b(), a3.b);
                            }
                            rsVar.r();
                            this.d.a(rsVar, a3);
                        } else {
                            rsVar.g();
                        }
                    }
                } catch (sf e2) {
                    e2.a(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.d.a(rsVar, rsVar.a(e2));
                } catch (Exception e3) {
                    rx.a(e3, "Unhandled exception %s", e3.toString());
                    sf sfVar = new sf((Throwable) e3);
                    sfVar.a(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.d.a(rsVar, sfVar);
                }
            } catch (InterruptedException unused) {
                if (this.e) {
                    return;
                }
            }
        }
    }
}
