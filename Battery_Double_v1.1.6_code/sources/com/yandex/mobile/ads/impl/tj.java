package com.yandex.mobile.ads.impl;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VmapLoader.OnVmapLoadedListener;
import com.yandex.mobile.ads.video.models.vmap.Vmap;

public final class tj implements RequestListener<Vmap> {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    @NonNull
    private final Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @Nullable
    public OnVmapLoadedListener c;

    public final /* synthetic */ void onSuccess(@NonNull Object obj) {
        final Vmap vmap = (Vmap) obj;
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (tj.a) {
                    if (tj.this.c != null) {
                        tj.this.c.onVmapLoaded(vmap);
                    }
                }
            }
        });
    }

    public final void onFailure(@NonNull final VideoAdError videoAdError) {
        this.b.post(new Runnable() {
            public final void run() {
                synchronized (tj.a) {
                    if (tj.this.c != null) {
                        tj.this.c.onVmapFailedToLoad(new ti(videoAdError.getCode(), videoAdError.getDescription()));
                    }
                }
            }
        });
    }

    public final void a() {
        this.b.removeCallbacksAndMessages(null);
    }

    public final void a(@Nullable OnVmapLoadedListener onVmapLoadedListener) {
        synchronized (a) {
            this.c = onVmapLoadedListener;
        }
    }
}
