package com.yandex.mobile.ads.impl;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import java.lang.ref.WeakReference;

public final class x extends ResultReceiver {
    private WeakReference<a> a = new WeakReference<>(null);

    public interface a {
        void a(int i, @Nullable Bundle bundle);
    }

    public x(Handler handler) {
        super(handler);
    }

    public final void a(a aVar) {
        this.a = new WeakReference<>(aVar);
    }

    /* access modifiers changed from: protected */
    public final void onReceiveResult(int i, @Nullable Bundle bundle) {
        if (this.a != null) {
            a aVar = (a) this.a.get();
            if (aVar != null) {
                aVar.a(i, bundle);
                new Object[1][0] = Integer.valueOf(i);
            }
        }
    }
}
