package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.nativeads.z;
import org.json.JSONException;
import org.json.JSONObject;

public final class qc implements pz<om> {
    @NonNull
    private final pk a;

    public qc(@NonNull Context context) {
        this.a = new pk(new eb(context));
    }

    @NonNull
    /* renamed from: b */
    public final om a(@NonNull JSONObject jSONObject) throws JSONException, z {
        if (!jSONObject.has("value") || jSONObject.isNull("value")) {
            throw new z("Native Ad json has not required attributes");
        }
        om omVar = new om();
        JSONObject jSONObject2 = jSONObject.getJSONObject("value");
        omVar.a(this.a.a(jSONObject2, "url"));
        omVar.a(jSONObject2.getInt("w"));
        omVar.b(jSONObject2.getInt("h"));
        String optString = jSONObject2.optString("sizeType");
        if (!TextUtils.isEmpty(optString)) {
            omVar.b(optString);
        }
        return omVar;
    }
}
