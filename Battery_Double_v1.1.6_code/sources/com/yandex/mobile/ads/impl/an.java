package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.List;

public final class an {
    @NonNull
    private final ap a = ap.a();
    @NonNull
    private final ao b = new ao();

    @NonNull
    public final String a(@NonNull Context context) {
        return a(context, this.a.b());
    }

    @NonNull
    public final String b(@NonNull Context context) {
        return a(context, this.a.c());
    }

    private static String a(@NonNull Context context, @NonNull List list) {
        return TextUtils.join(",", list.subList(list.size() - Math.min(ao.a(context), list.size()), list.size()));
    }
}
