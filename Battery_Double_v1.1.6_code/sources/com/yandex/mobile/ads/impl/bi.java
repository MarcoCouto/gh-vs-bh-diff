package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.yandex.mobile.ads.impl.hr.b;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class bi {
    @NonNull
    private final fl a;
    @NonNull
    private final ct b = new ct();
    @Nullable
    private final w c;

    public bi(@NonNull fl flVar, @Nullable w wVar) {
        this.a = flVar;
        this.c = wVar;
    }

    public final void a(@NonNull Context context, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        a(context, b.CLICK, bmVar, map);
    }

    public final void b(@NonNull Context context, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        a(context, b.IMPRESSION_TRACKING_START, bmVar, map);
        a(context, b.IMPRESSION_TRACKING_SUCCESS, bmVar, map);
    }

    public final void c(@NonNull Context context, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_AUTO_REFRESH, bmVar, map);
    }

    public final void a(@NonNull Context context, @NonNull bm bmVar) {
        a(context, b.ADAPTER_REQUEST, bmVar, Collections.emptyMap());
    }

    public final void d(@NonNull Context context, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_RESPONSE, bmVar, map);
    }

    public final void e(@NonNull Context context, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_ACTION, bmVar, map);
    }

    public final void f(@NonNull Context context, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        a(context, b.ADAPTER_INVALID, bmVar, map);
    }

    public final void a(@NonNull Context context, @NonNull bm bmVar, @Nullable w wVar) {
        HashMap hashMap = new HashMap();
        new bj();
        hashMap.put("reward_info", bj.a(wVar));
        a(context, b.REWARD, bmVar, hashMap);
    }

    private void a(@NonNull Context context, @NonNull b bVar, @NonNull bm bmVar, @NonNull Map<String, Object> map) {
        Map a2 = a(bmVar);
        a2.putAll(map);
        hp.a(context).a(new hr(bVar, a2));
    }

    @NonNull
    private Map<String, Object> a(@NonNull bm bmVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("block_id", this.a.e());
        hashMap.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, this.a.a().a());
        hashMap.put("adapter", bmVar.a());
        hashMap.put("adapter_parameters", bmVar.b());
        hashMap.putAll(ct.a(this.a.c()));
        ea eaVar = new ea(hashMap);
        eaVar.a("ad_source", this.c != null ? this.c.k() : null);
        return eaVar.a();
    }
}
