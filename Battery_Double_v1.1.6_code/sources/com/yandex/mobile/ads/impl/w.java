package com.yandex.mobile.ads.impl;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.b;
import java.util.ArrayList;
import java.util.List;

public final class w<T> implements Parcelable {
    public static final Creator<w> CREATOR = new Creator<w>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new w[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new w(parcel);
        }
    };
    public static final Integer a = Integer.valueOf(100);
    private static final Integer b = Integer.valueOf(1000);
    @Nullable
    private final b c;
    @Nullable
    private final String d;
    @Nullable
    private final String e;
    @Nullable
    private final String f;
    @NonNull
    private final al g;
    @Nullable
    private final List<String> h;
    @Nullable
    private final List<String> i;
    @Nullable
    private di j;
    @Nullable
    private final List<Long> k;
    @Nullable
    private final List<Integer> l;
    @Nullable
    private final String m;
    @Nullable
    private final String n;
    @Nullable
    private final String o;
    @Nullable
    private final bl p;
    @Nullable
    private final bp q;
    @Nullable
    private final T r;
    private final boolean s;
    private final boolean t;
    private final int u;
    private final int v;
    private final int w;
    private final int x;
    private final int y;
    private final int z;

    public static class a<T> {
        /* access modifiers changed from: private */
        @Nullable
        public b a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public String d;
        /* access modifiers changed from: private */
        @Nullable
        public com.yandex.mobile.ads.impl.al.a e;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> f;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> g;
        /* access modifiers changed from: private */
        @Nullable
        public di h;
        /* access modifiers changed from: private */
        @Nullable
        public List<Long> i;
        /* access modifiers changed from: private */
        @Nullable
        public List<Integer> j;
        /* access modifiers changed from: private */
        @Nullable
        public String k;
        /* access modifiers changed from: private */
        @Nullable
        public bl l;
        /* access modifiers changed from: private */
        @Nullable
        public bp m;
        /* access modifiers changed from: private */
        @Nullable
        public T n;
        /* access modifiers changed from: private */
        @Nullable
        public String o;
        /* access modifiers changed from: private */
        @Nullable
        public String p;
        /* access modifiers changed from: private */
        public int q;
        /* access modifiers changed from: private */
        public int r;
        /* access modifiers changed from: private */
        public int s;
        /* access modifiers changed from: private */
        public int t;
        /* access modifiers changed from: private */
        public int u;
        /* access modifiers changed from: private */
        public int v;
        /* access modifiers changed from: private */
        public boolean w;
        /* access modifiers changed from: private */
        public boolean x;

        @NonNull
        public final a<T> a(@NonNull b bVar) {
            this.a = bVar;
            return this;
        }

        @NonNull
        public final a<T> a(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final a<T> b(@NonNull String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public final a<T> c(@Nullable String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public final a<T> a(int i2) {
            this.q = i2;
            return this;
        }

        @NonNull
        public final a<T> b(int i2) {
            this.r = i2;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable com.yandex.mobile.ads.impl.al.a aVar) {
            this.e = aVar;
            return this;
        }

        @NonNull
        public final a<T> a(@NonNull List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public final a<T> b(@NonNull List<String> list) {
            this.g = list;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable di diVar) {
            this.h = diVar;
            return this;
        }

        @NonNull
        public final a<T> c(@NonNull List<Long> list) {
            this.i = list;
            return this;
        }

        @NonNull
        public final a<T> d(@NonNull List<Integer> list) {
            this.j = list;
            return this;
        }

        @NonNull
        public final a<T> c(int i2) {
            this.t = i2;
            return this;
        }

        @NonNull
        public final a<T> d(int i2) {
            this.u = i2;
            return this;
        }

        @NonNull
        public final a<T> e(int i2) {
            this.v = i2;
            return this;
        }

        @NonNull
        public final a<T> f(int i2) {
            this.s = i2;
            return this;
        }

        @NonNull
        public final a<T> d(@Nullable String str) {
            this.k = str;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable bl blVar) {
            this.l = blVar;
            return this;
        }

        @NonNull
        public final a<T> a(@Nullable T t2) {
            this.n = t2;
            return this;
        }

        @NonNull
        public final a<T> a(@NonNull bp bpVar) {
            this.m = bpVar;
            return this;
        }

        @NonNull
        public final a<T> a(boolean z) {
            this.w = z;
            return this;
        }

        @NonNull
        public final a<T> b(boolean z) {
            this.x = z;
            return this;
        }

        @NonNull
        public final a<T> e(@Nullable String str) {
            this.o = str;
            return this;
        }

        @NonNull
        public final a<T> f(@Nullable String str) {
            this.p = str;
            return this;
        }

        @NonNull
        public final w<T> a() {
            return new w<>(this, 0);
        }
    }

    public final int describeContents() {
        return 0;
    }

    /* synthetic */ w(a aVar, byte b2) {
        this(aVar);
    }

    private w(@NonNull a<T> aVar) {
        this.c = aVar.a;
        this.f = aVar.d;
        this.d = aVar.b;
        this.e = aVar.c;
        this.y = aVar.q;
        this.z = aVar.r;
        this.g = new al(this.y, this.z, aVar.e != null ? aVar.e : com.yandex.mobile.ads.impl.al.a.FIXED);
        this.h = aVar.f;
        this.i = aVar.g;
        this.k = aVar.i;
        this.l = aVar.j;
        this.j = aVar.h;
        this.u = aVar.s;
        this.v = aVar.t;
        this.w = aVar.u;
        this.x = aVar.v;
        this.m = aVar.o;
        this.n = aVar.k;
        this.o = aVar.p;
        this.r = aVar.n;
        this.p = aVar.l;
        this.q = aVar.m;
        this.s = aVar.w;
        this.t = aVar.x;
    }

    @Nullable
    public final b a() {
        return this.c;
    }

    @Nullable
    public final String b() {
        return this.d;
    }

    @Nullable
    public final String c() {
        return this.e;
    }

    @Nullable
    public final String d() {
        return this.f;
    }

    @NonNull
    public final al e() {
        return this.g;
    }

    @Nullable
    public final List<String> f() {
        return this.h;
    }

    @Nullable
    public final List<String> g() {
        return this.i;
    }

    @Nullable
    public final di h() {
        return this.j;
    }

    @Nullable
    public final List<Long> i() {
        return this.k;
    }

    @Nullable
    public final List<Integer> j() {
        return this.l;
    }

    @Nullable
    public final String k() {
        return this.m;
    }

    @Nullable
    public final String l() {
        return this.n;
    }

    @Nullable
    public final String m() {
        return this.o;
    }

    @Nullable
    public final bl n() {
        return this.p;
    }

    @Nullable
    public final bp o() {
        return this.q;
    }

    @Nullable
    public final T p() {
        return this.r;
    }

    public final int q() {
        return this.y;
    }

    public final int r() {
        return this.z;
    }

    public final int s() {
        return this.v;
    }

    public final int t() {
        return this.u;
    }

    public final int u() {
        return this.v * b.intValue();
    }

    public final int v() {
        return this.w * b.intValue();
    }

    public final boolean w() {
        return this.z == 0;
    }

    public final boolean x() {
        return this.v > 0;
    }

    public final boolean y() {
        return this.s;
    }

    public final boolean z() {
        return this.t;
    }

    public final void writeToParcel(@NonNull Parcel parcel, int i2) {
        parcel.writeInt(this.c == null ? -1 : this.c.ordinal());
        parcel.writeString(this.f);
        parcel.writeString(this.d);
        parcel.writeString(this.n);
        parcel.writeParcelable(this.g, i2);
        parcel.writeInt(this.y);
        parcel.writeInt(this.z);
        parcel.writeStringList(this.h);
        parcel.writeStringList(this.i);
        parcel.writeList(this.k);
        parcel.writeList(this.l);
        parcel.writeInt(this.u);
        parcel.writeInt(this.v);
        parcel.writeInt(this.w);
        parcel.writeInt(this.x);
        parcel.writeString(this.m);
        parcel.writeString(this.n);
        parcel.writeString(this.o);
        parcel.writeParcelable(this.p, i2);
        parcel.writeParcelable(this.q, i2);
        parcel.writeSerializable(this.r.getClass());
        parcel.writeValue(this.r);
        parcel.writeByte(this.s ? (byte) 1 : 0);
        parcel.writeByte(this.t ? (byte) 1 : 0);
    }

    protected w(@NonNull Parcel parcel) {
        b bVar;
        int readInt = parcel.readInt();
        T t2 = null;
        if (readInt == -1) {
            bVar = null;
        } else {
            bVar = b.values()[readInt];
        }
        this.c = bVar;
        this.f = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.g = (al) parcel.readParcelable(al.class.getClassLoader());
        this.y = parcel.readInt();
        this.z = parcel.readInt();
        this.h = parcel.createStringArrayList();
        this.i = parcel.createStringArrayList();
        this.k = new ArrayList();
        parcel.readList(this.k, Long.class.getClassLoader());
        this.l = new ArrayList();
        parcel.readList(this.l, Integer.class.getClassLoader());
        this.u = parcel.readInt();
        this.v = parcel.readInt();
        this.w = parcel.readInt();
        this.x = parcel.readInt();
        this.m = parcel.readString();
        this.n = parcel.readString();
        this.o = parcel.readString();
        this.p = (bl) parcel.readParcelable(bl.class.getClassLoader());
        this.q = (bp) parcel.readParcelable(bp.class.getClassLoader());
        Class cls = (Class) parcel.readSerializable();
        if (cls != null) {
            t2 = parcel.readValue(cls.getClassLoader());
        }
        this.r = t2;
        boolean z2 = false;
        this.s = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z2 = true;
        }
        this.t = z2;
    }
}
