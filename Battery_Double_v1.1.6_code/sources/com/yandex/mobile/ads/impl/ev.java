package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import org.json.JSONObject;

final class ev {
    private final ep a;

    ev(@NonNull ep epVar) {
        this.a = epVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        this.a.b(str);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        b("notifyReadyEvent();");
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ey eyVar, @NonNull String str) {
        StringBuilder sb = new StringBuilder("notifyErrorEvent(");
        sb.append(JSONObject.quote(eyVar.a()));
        sb.append(", ");
        sb.append(JSONObject.quote(str));
        sb.append(")");
        b(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull ey eyVar) {
        StringBuilder sb = new StringBuilder("nativeCallComplete(");
        sb.append(JSONObject.quote(eyVar.a()));
        sb.append(")");
        b(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull fh... fhVarArr) {
        if (fhVarArr.length > 0) {
            String str = "";
            StringBuilder sb = new StringBuilder("fireChangeEvent({");
            for (fh fhVar : fhVarArr) {
                sb.append(str);
                sb.append(fhVar.a());
                str = ", ";
            }
            sb.append("})");
            b(sb.toString());
        }
    }

    private void b(@NonNull String str) {
        c(String.format("window.mraidbridge.%s", new Object[]{str}));
    }

    private void c(@NonNull String str) {
        this.a.loadUrl("javascript: ".concat(String.valueOf(str)));
        new Object[1][0] = str;
    }
}
