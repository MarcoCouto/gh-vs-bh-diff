package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.view.View;
import com.yandex.mobile.ads.impl.ir.a;

public final class fc {
    @NonNull
    private final ir a = new ir();

    @NonNull
    public static <T extends View & a> ff a(@NonNull T t) {
        int i;
        RectF rectF = null;
        if (ir.a(t)) {
            i = ee.e((View) t);
            Rect rect = new Rect();
            if (t.getLocalVisibleRect(rect)) {
                rect.offset(t.getLeft(), t.getTop());
            } else {
                rect = null;
            }
            Context context = t.getContext();
            if (rect != null) {
                int a2 = ee.a(context, rect.left);
                int a3 = ee.a(context, rect.top);
                int a4 = ee.a(context, rect.right);
                int a5 = ee.a(context, rect.bottom);
                int i2 = a5 - a3;
                if (a4 - a2 > 0 && i2 > 0) {
                    rectF = new RectF((float) a2, (float) a3, (float) a4, (float) a5);
                }
            }
        } else {
            i = 0;
        }
        return new ff(i, rectF);
    }
}
