package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.location.Location;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.tapjoy.TapjoyConstants;
import com.yandex.mobile.ads.MobileAds;

final class ty {
    @NonNull
    private final fl a;
    @NonNull
    private final fn b = new fn();
    @NonNull
    private final fx c = new fx();
    @NonNull
    private final gz d;

    ty(@NonNull Context context, @NonNull fl flVar) {
        this.a = flVar;
        this.d = gz.a(context);
    }

    private static void a(@NonNull Builder builder, @NonNull String str, @Nullable gm gmVar) {
        if (gmVar != null && !gmVar.b()) {
            a(builder, str, gmVar.a());
        }
    }

    private static void a(@NonNull Builder builder, @NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Context context, @NonNull Builder builder) {
        a(builder, "app_id", context.getPackageName());
        a(builder, "app_version_code", ds.a(context));
        a(builder, "app_version_name", ds.b(context));
        a(builder, "sdk_version", MobileAds.getLibraryVersion());
        a(builder, TapjoyConstants.TJC_DEVICE_TYPE_NAME, this.c.a(context));
        a(builder, "locale", fz.a(context));
        a(builder, "manufacturer", Build.MANUFACTURER);
        a(builder, "model", Build.MODEL);
        a(builder, "os_name", "android");
        a(builder, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
        if (!fn.a(context)) {
            Location a2 = this.d.a();
            if (a2 != null) {
                a(builder, "location_timestamp", String.valueOf(a2.getTime()));
                a(builder, "lat", String.valueOf(a2.getLatitude()));
                a(builder, "lon", String.valueOf(a2.getLongitude()));
                a(builder, "precision", String.valueOf(a2.getAccuracy()));
            }
        }
        if (!fn.a(context)) {
            a(builder, "device-id", this.a.g());
            a(builder, "google_aid", this.a.i());
            a(builder, "huawei_oaid", this.a.j());
        }
    }
}
