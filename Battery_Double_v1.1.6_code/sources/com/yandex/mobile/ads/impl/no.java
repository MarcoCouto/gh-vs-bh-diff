package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.widget.FrameLayout;
import com.yandex.mobile.ads.impl.am.a;
import java.util.ArrayList;
import java.util.List;

public final class no {
    @NonNull
    private static final List<a> a = new ArrayList<a>() {
        {
            add(a.SUCCESS);
            add(a.APPLICATION_INACTIVE);
            add(a.NOT_ADDED_TO_HIERARCHY);
        }
    };
    @NonNull
    private final np b = new np();

    public final void a(@NonNull am amVar, @NonNull FrameLayout frameLayout) {
        this.b.a(amVar, frameLayout, !a.contains(amVar.b()));
    }
}
