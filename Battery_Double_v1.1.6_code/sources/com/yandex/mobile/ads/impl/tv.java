package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.rt.a;
import com.yandex.mobile.ads.impl.tx.b;
import com.yandex.mobile.ads.video.BlocksInfoRequest;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VastRequestConfiguration;
import com.yandex.mobile.ads.video.VmapRequestConfiguration;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import com.yandex.mobile.ads.video.models.vmap.Vmap;
import com.yandex.mobile.ads.video.tracking.Tracker.ErrorListener;
import java.util.List;

public final class tv {
    public static final a a = new a() {
        public final boolean a(rs<?> rsVar) {
            return true;
        }
    };
    @NonNull
    private static final Object b = new Object();
    private static volatile tv c;
    private final rt d;

    private tv(rt rtVar) {
        this.d = rtVar;
    }

    @NonNull
    public static tv a(@Nullable Context context) {
        if (c == null) {
            synchronized (b) {
                if (c == null) {
                    rt rtVar = new rt(new so(), new sg(new sr(context, new sy()).a()), 1);
                    rtVar.a();
                    c = new tv(rtVar);
                }
            }
        }
        return c;
    }

    public final void a(@NonNull BlocksInfoRequest blocksInfoRequest, @NonNull fl flVar) {
        String partnerId = blocksInfoRequest.getPartnerId();
        String categoryId = blocksInfoRequest.getCategoryId();
        Builder buildUpon = Uri.parse(tx.a(flVar)).buildUpon();
        buildUpon.appendPath("v1").appendPath("vcset").appendPath(partnerId).appendQueryParameter("video-category-id", categoryId).appendQueryParameter("uuid", flVar.d());
        this.d.a((rs<T>) new ud<T>(blocksInfoRequest, buildUpon.build().toString(), new b(blocksInfoRequest.getRequestListener()), new vd()));
    }

    public final void a(@NonNull tt ttVar, @NonNull fl flVar) {
        this.d.a((rs<T>) c.a(ttVar, flVar));
    }

    public final void a(String str, ErrorListener errorListener) {
        this.d.a((rs<T>) new ue<T>(str, new tx.a(errorListener)));
    }

    public final void a(@NonNull Context context, @NonNull fl flVar, @NonNull VmapRequestConfiguration vmapRequestConfiguration, @NonNull RequestListener<Vmap> requestListener) {
        this.d.a(new ub().a(context, flVar, vmapRequestConfiguration, requestListener));
    }

    public final void a(@NonNull Context context, @NonNull fl flVar, @NonNull VastRequestConfiguration vastRequestConfiguration, @NonNull RequestListener<tl> requestListener) {
        new ua();
        this.d.a(ua.a(context, flVar, vastRequestConfiguration, requestListener));
    }

    public final void a(@NonNull Context context, @NonNull VideoAd videoAd, @NonNull vb vbVar, @NonNull RequestListener<List<VideoAd>> requestListener) {
        new uc();
        Context context2 = context;
        uh uhVar = new uh(context2, videoAd.getVastAdTagUri(), new b(requestListener), videoAd, new vh(vbVar));
        this.d.a((rs<T>) uhVar);
    }
}
