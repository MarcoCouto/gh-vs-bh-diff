package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.yandex.mobile.ads.impl.in.b;
import com.yandex.mobile.ads.impl.in.c;
import com.yandex.mobile.ads.impl.in.d;

final class kc implements kd {
    @NonNull
    private final jw a;

    public final void a(@NonNull Context context, @NonNull n nVar, @NonNull al alVar) {
    }

    kc(@NonNull jw jwVar) {
        this.a = jwVar;
    }

    public final void a(@NonNull RelativeLayout relativeLayout) {
        if (VERSION.SDK_INT >= 16) {
            relativeLayout.setBackground(b.b);
        } else {
            relativeLayout.setBackgroundDrawable(b.b);
        }
    }

    @NonNull
    public final View a(@NonNull View view, @NonNull w<String> wVar) {
        Context context = view.getContext();
        LayoutParams a2 = d.a();
        RelativeLayout a3 = c.a(context);
        a3.setLayoutParams(a2);
        a3.addView(view, d.a());
        a3.addView(this.a.a(), d.a(context, view));
        return a3;
    }

    public final void a() {
        this.a.b();
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final void b() {
        this.a.c();
    }

    public final boolean c() {
        return this.a.d();
    }
}
