package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.nativeads.a;
import com.yandex.mobile.ads.nativeads.ag;
import com.yandex.mobile.ads.nativeads.b;
import com.yandex.mobile.ads.nativeads.bl;
import com.yandex.mobile.ads.nativeads.r;

public final class ny implements a {
    @NonNull
    private final w a;
    @NonNull
    private final fl b;
    @NonNull
    private final r c;
    @NonNull
    private final ob d;
    @NonNull
    private final b e;
    @NonNull
    private final bl f;
    @Nullable
    private hr.a g;

    public ny(@NonNull Context context, @NonNull w wVar, @NonNull fl flVar, @NonNull r rVar, @NonNull bl blVar) {
        this.a = wVar;
        this.b = flVar;
        this.c = rVar;
        this.f = blVar;
        this.d = new ob(new de(context, flVar));
        this.e = new b(rVar);
    }

    public final void a(@NonNull hr.a aVar) {
        this.g = aVar;
    }

    public final void a(@NonNull Context context, @NonNull on onVar, @NonNull ag agVar) {
        this.c.a(onVar);
        de deVar = new de(context, this.b);
        x a2 = this.e.a();
        cp cpVar = new cp(context, this.a, this.b, this.g);
        oa oaVar = new oa(deVar, this.a, this.f.a(context, this.b, a2), cpVar);
        oe oeVar = new oe(this.b, cpVar, oaVar, agVar, this.c);
        this.d.a(onVar.c());
        oeVar.a(context, onVar.a());
        oaVar.a(onVar.d());
    }
}
