package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.sv.a;

public final class ak implements a<fw> {
    @NonNull
    private final Context a;
    @NonNull
    private final aj b;
    @NonNull
    private final ai.a c;

    public final /* bridge */ /* synthetic */ void a(@NonNull Object obj) {
        this.b.a(this.a, (fw) obj);
        this.c.a();
    }

    ak(@NonNull Context context, @NonNull aj ajVar, @NonNull ai.a aVar) {
        this.a = context.getApplicationContext();
        this.b = ajVar;
        this.c = aVar;
    }

    public final void a(sf sfVar) {
        this.c.a(sfVar);
    }
}
