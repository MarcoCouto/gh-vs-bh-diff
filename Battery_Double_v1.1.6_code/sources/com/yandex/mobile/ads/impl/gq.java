package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class gq {
    @NonNull
    private final Object a = new Object();
    @NonNull
    private final List<gv> b = new CopyOnWriteArrayList();

    public final void a(@NonNull Context context, @NonNull gv gvVar) {
        synchronized (this.a) {
            this.b.add(gvVar);
            gp.a(context).a(gvVar);
        }
    }

    public final void a(@NonNull Context context) {
        synchronized (this.a) {
            gp a2 = gp.a(context);
            for (gv b2 : this.b) {
                a2.b(b2);
            }
            this.b.clear();
        }
    }
}
