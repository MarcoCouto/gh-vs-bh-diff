package com.yandex.mobile.ads.impl;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import com.yandex.mobile.ads.nativeads.j;

public final class lx extends md<ImageView, om> {
    @NonNull
    private final j a;
    @NonNull
    private final mk b;

    public final /* synthetic */ boolean a(@NonNull View view, @NonNull Object obj) {
        om omVar = (om) obj;
        return this.b.a(((ImageView) view).getDrawable(), omVar);
    }

    public lx(@NonNull ImageView imageView, @NonNull j jVar) {
        super(imageView);
        this.a = jVar;
        this.b = new mk(jVar);
    }

    public final void a(@NonNull ImageView imageView) {
        imageView.setImageDrawable(null);
        super.a(imageView);
    }

    /* renamed from: a */
    public final void b(@NonNull ImageView imageView, @NonNull om omVar) {
        Bitmap a2 = this.a.a(omVar);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
    }
}
