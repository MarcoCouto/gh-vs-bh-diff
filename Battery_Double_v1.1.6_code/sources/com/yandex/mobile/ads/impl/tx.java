package com.yandex.mobile.ads.impl;

import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.mobile.ads.MobileAds;
import com.yandex.mobile.ads.video.RequestListener;
import com.yandex.mobile.ads.video.VideoAdError;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.blocksinfo.BlocksInfo;
import com.yandex.mobile.ads.video.tracking.Tracker.ErrorListener;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Random;

public final class tx {
    private static Random a;

    public static class a implements com.yandex.mobile.ads.impl.sv.a<Void> {
        private final ErrorListener a;

        public final /* bridge */ /* synthetic */ void a(Object obj) {
        }

        public a(ErrorListener errorListener) {
            this.a = errorListener;
        }

        public final void a(@NonNull sf sfVar) {
            VideoAdError videoAdError;
            if (this.a != null) {
                if (sfVar == null) {
                    this.a.onTrackingError(VideoAdError.createInternalError("Tracking error"));
                    return;
                }
                if (sfVar.a == null) {
                    videoAdError = VideoAdError.createConnectionError(sfVar.getMessage());
                } else {
                    videoAdError = VideoAdError.createInternalError("Tracking error");
                }
                this.a.onTrackingError(videoAdError);
            }
        }
    }

    public static class b<T> implements com.yandex.mobile.ads.impl.sv.a<T> {
        private final RequestListener<T> a;

        public b(RequestListener<T> requestListener) {
            this.a = requestListener;
        }

        public final void a(T t) {
            if (this.a != null) {
                this.a.onSuccess(t);
            }
        }

        public final void a(@NonNull sf sfVar) {
            VideoAdError videoAdError;
            if (this.a != null) {
                if (sfVar instanceof tn) {
                    videoAdError = VideoAdError.createNoAdError((tn) sfVar);
                } else if (sfVar instanceof to) {
                    videoAdError = VideoAdError.createInternalError((to) sfVar);
                } else {
                    rr rrVar = sfVar.a;
                    if (rrVar == null) {
                        videoAdError = VideoAdError.createConnectionError(sfVar.getMessage());
                    } else if (rrVar.a >= 500) {
                        videoAdError = VideoAdError.createRetriableError("Server temporarily unavailable. Please, try again later.");
                    } else {
                        String str = "Network Error. ";
                        if (rrVar != null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(" Code: ");
                            sb.append(rrVar.a);
                            sb.append(".");
                            String sb2 = sb.toString();
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(sb2);
                            sb3.append(" Data: \n");
                            sb3.append(new String(rrVar.b));
                            str = sb3.toString();
                        }
                        videoAdError = VideoAdError.createInternalError(str);
                    }
                }
                this.a.onFailure(videoAdError);
            }
        }
    }

    private static class c {
        private static void a(Builder builder, String str, String str2) {
            if (!TextUtils.isEmpty(str2)) {
                builder.appendQueryParameter(str, str2);
            }
        }

        static /* synthetic */ ui a(tt ttVar, fl flVar) {
            VideoAdRequest a = ttVar.a();
            BlocksInfo blocksInfo = a.getBlocksInfo();
            Builder buildUpon = Uri.parse(tx.a(flVar)).buildUpon();
            buildUpon.appendPath("v1").appendPath("getvideo").appendQueryParameter("page_id", blocksInfo.getPartnerId()).appendQueryParameter("imp-id", a.getBlockId()).appendQueryParameter("target-ref", a.getTargetRef()).appendQueryParameter("page-ref", a.getPageRef()).appendQueryParameter("rnd", Integer.toString(tx.a().nextInt(89999999) + 10000000)).appendQueryParameter("video-session-id", a.getBlocksInfo().getSessionId()).appendQueryParameter(HttpRequest.PARAM_CHARSET, a.getCharset().getValue());
            a(buildUpon, "video-api-version", String.format("android-v%s", new Object[]{MobileAds.getLibraryVersion()}));
            a(buildUpon, "video-width", a.getPlayerWidthPix());
            a(buildUpon, "video-height", a.getPlayerHeightPix());
            a(buildUpon, "video-content-id", a.getVideoContentId());
            a(buildUpon, "video-content-name", a.getVideoContentName());
            a(buildUpon, "video-publisher-id", a.getPublisherId());
            a(buildUpon, "video-publisher-name", a.getPublisherName());
            a(buildUpon, "video-maxbitrate", a.getMaxBitrate());
            a(buildUpon, "video-genre-id", a.getGenreId());
            a(buildUpon, "video-genre-name", a.getGenreName());
            a(buildUpon, "tags-list", a.getTagsList());
            a(buildUpon, "ext-param", a.getExtParams());
            buildUpon.appendQueryParameter("uuid", flVar.d());
            return new ui(a, buildUpon.build().toString(), new b(ttVar), new vf());
        }
    }

    static Random a() {
        if (a == null) {
            return new Random();
        }
        return a;
    }

    static /* synthetic */ String a(fl flVar) {
        String f = flVar.f();
        return TextUtils.isEmpty(f) ? "https://mobile.yandexadexchange.net" : f;
    }
}
