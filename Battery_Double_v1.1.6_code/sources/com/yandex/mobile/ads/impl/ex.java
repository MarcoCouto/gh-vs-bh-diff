package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.AppodealNetworks;
import com.yandex.mobile.ads.impl.ru.b;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public final class ex implements fe {
    /* access modifiers changed from: private */
    public final ev a;
    /* access modifiers changed from: private */
    public final ep b;
    /* access modifiers changed from: private */
    public final fb c = new fb(new a(this, 0));
    private final ez d;
    @NonNull
    private fj e;
    @NonNull
    private final fc f;
    @NonNull
    private final fd g;
    @NonNull
    private final String h;
    @NonNull
    private final ir i;
    @Nullable
    private fa j;
    @Nullable
    private jg k;
    @Nullable
    private jf l;
    @Nullable
    private eo m;
    @Nullable
    private eh n;
    @Nullable
    private ff o;

    private class a implements ek {
        private a() {
        }

        /* synthetic */ a(ex exVar, byte b) {
            this();
        }

        public final void d() {
            ex.this.b.d();
        }

        public final void a(@NonNull Context context, @NonNull String str) {
            ex.this.b.a(context, str);
        }
    }

    public ex(@NonNull ep epVar) {
        this.b = epVar;
        this.b.setWebViewClient(this.c);
        this.a = new ev(this.b);
        this.d = new ez();
        this.i = new ir();
        this.e = fj.LOADING;
        this.f = new fc();
        this.g = new fd(epVar, this.f, this);
        this.h = dr.a(this);
    }

    public final void a(@NonNull fa faVar) {
        this.j = faVar;
    }

    public final void a(@NonNull jg jgVar) {
        this.k = jgVar;
    }

    public final void a(@NonNull jf jfVar) {
        this.l = jfVar;
    }

    public final void a(@NonNull eo eoVar) {
        this.m = eoVar;
    }

    public final void a(@NonNull eh ehVar) {
        this.n = ehVar;
    }

    public final void a(@NonNull final String str) {
        Context context = this.b.getContext();
        ez ezVar = this.d;
        String str2 = this.h;
        AnonymousClass1 r3 = new com.yandex.mobile.ads.impl.ez.a() {
            public final void a(@NonNull String str) {
                ex.this.c.a(str);
                ex.this.a.a(str);
            }
        };
        fw a2 = fv.a().a(context);
        if (a2 == null || TextUtils.isEmpty(a2.f())) {
            r3.a(ez.a);
            return;
        }
        sq sqVar = new sq(a2.f(), new b<String>(r3) {
            final /* synthetic */ a a;

            {
                this.a = r2;
            }

            public final /* bridge */ /* synthetic */ void a(Object obj) {
                this.a.a((String) obj);
            }
        }, new com.yandex.mobile.ads.impl.ru.a(r3) {
            final /* synthetic */ a a;

            {
                this.a = r2;
            }

            public final void a(@NonNull sf sfVar) {
                this.a.a(ez.a);
            }
        });
        sqVar.a((Object) str2);
        ah.a().a(context, (rs) sqVar);
    }

    public final void a() {
        fi fiVar = new fi(this.b);
        fk fkVar = new fk(ir.a(this.b));
        fg c2 = c(fc.a(this.b));
        this.e = fj.DEFAULT;
        this.a.a(this.e, fkVar, c2, fiVar);
        this.a.a();
        if (this.j != null) {
            this.j.a();
        }
    }

    public final void a(boolean z) {
        fk fkVar = new fk(z);
        this.a.a(fkVar);
        if (z) {
            this.g.a();
            return;
        }
        this.g.b();
        b(fc.a(this.b));
    }

    public final void a(@NonNull ff ffVar) {
        b(ffVar);
    }

    private void b(@NonNull ff ffVar) {
        if (!ffVar.equals(this.o)) {
            this.o = ffVar;
            fg c2 = c(ffVar);
            this.a.a(c2);
        }
    }

    @NonNull
    private static fg c(@NonNull ff ffVar) {
        return new fg(ffVar.a(), ffVar.b());
    }

    public final void b() {
        if (fj.DEFAULT == this.e) {
            a(fj.HIDDEN);
        }
    }

    public final void b(String str) {
        try {
            URI uri = new URI(str);
            String scheme = uri.getScheme();
            String host = uri.getHost();
            if (AppodealNetworks.MRAID.equals(scheme) || "mobileads".equals(scheme)) {
                HashMap hashMap = new HashMap();
                for (NameValuePair nameValuePair : URLEncodedUtils.parse(uri, "UTF-8")) {
                    hashMap.put(nameValuePair.getName(), nameValuePair.getValue());
                }
                ey a2 = ey.a(host);
                try {
                    if (this.j != null) {
                        switch (a2) {
                            case CLOSE:
                                if (fj.DEFAULT == this.e) {
                                    a(fj.HIDDEN);
                                    if (this.m != null) {
                                        this.m.h();
                                        break;
                                    }
                                }
                                break;
                            case USE_CUSTOM_CLOSE:
                                if (this.m != null) {
                                    this.m.a(Boolean.parseBoolean((String) hashMap.get("shouldUseCustomClose")));
                                    break;
                                }
                                break;
                            case OPEN:
                                if (this.j != null) {
                                    String str2 = (String) hashMap.get("url");
                                    if (!TextUtils.isEmpty(str2)) {
                                        this.j.a(str2);
                                        new Object[1][0] = str2;
                                        break;
                                    } else {
                                        throw new ew(String.format("Mraid open command sent an invalid URL: %s", new Object[]{str2}));
                                    }
                                }
                                break;
                            case AD_VIDEO_COMPLETE:
                                if (this.n != null) {
                                    this.n.a();
                                    break;
                                }
                                break;
                            case IMPRESSION_TRACKING_START:
                                if (this.l != null) {
                                    this.l.b();
                                    break;
                                }
                                break;
                            case IMPRESSION_TRACKING_SUCCESS:
                                if (this.l != null) {
                                    this.l.d_();
                                    break;
                                }
                                break;
                            case REWARDED_AD_COMPLETE:
                                if (this.k != null) {
                                    this.k.i();
                                    break;
                                }
                                break;
                            default:
                                throw new ew("Unspecified MRAID Javascript command");
                        }
                        this.a.a(a2);
                    } else {
                        throw new ew("Invalid state to execute this command");
                    }
                } catch (ew e2) {
                    this.a.a(a2, e2.getMessage());
                }
            }
        } catch (URISyntaxException unused) {
            this.a.a(ey.UNSPECIFIED, "Mraid command sent an invalid URL");
        }
    }

    private void a(@NonNull fj fjVar) {
        this.e = fjVar;
        this.a.a(this.e);
    }

    public final void c() {
        this.g.b();
        ez.a(this.b.getContext(), this.h);
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
    }
}
