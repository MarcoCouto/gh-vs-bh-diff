package com.yandex.mobile.ads.impl;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

public final class rd {
    public static rc a(@NonNull Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return new re();
        }
        return new rg();
    }
}
