package com.yandex.mobile.ads.impl;

import com.yandex.mobile.ads.impl.sv.a;
import com.yandex.mobile.ads.video.VideoAdRequest;
import com.yandex.mobile.ads.video.models.ad.VideoAd;
import java.util.List;

public final class ui extends ug<VideoAdRequest, List<VideoAd>> {
    /* access modifiers changed from: protected */
    public final /* synthetic */ ru c(Object obj) {
        List list = (List) obj;
        if (!list.isEmpty()) {
            return ru.a(list, null);
        }
        return ru.a(new tn());
    }

    public ui(VideoAdRequest videoAdRequest, String str, a<List<VideoAd>> aVar, hs<VideoAdRequest, List<VideoAd>> hsVar) {
        super(str, aVar, videoAdRequest.getContext(), videoAdRequest, hsVar, 0);
    }

    /* access modifiers changed from: protected */
    public final ru<List<VideoAd>> a(Exception exc) {
        return ru.a(new tn());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(String str) throws Exception {
        return new uk().b(str);
    }
}
