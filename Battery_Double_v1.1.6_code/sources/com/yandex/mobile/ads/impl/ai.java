package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

public final class ai {
    @NonNull
    private final Context a;
    @NonNull
    private final fl b;
    @NonNull
    private final da c;
    @NonNull
    private final aj d = aj.a();
    @NonNull
    private final ah e = ah.a();

    public interface a {
        void a();

        void a(@NonNull sf sfVar);
    }

    public ai(@NonNull Context context, @NonNull fl flVar) {
        this.a = context.getApplicationContext();
        this.b = flVar;
        this.c = new da(context);
    }

    public final void a(@NonNull a aVar) {
        String str;
        if (this.c.a()) {
            ak akVar = new ak(this.a, this.d, aVar);
            Context context = this.a;
            String f = this.b.f();
            if (!TextUtils.isEmpty(f)) {
                String d2 = ad.a(fn.a(context)).b(this.b.d()).a(this.b.i()).b(this.b.j()).a().b(context).a(context, this.b.g()).f(context).b().c().d();
                StringBuilder sb = new StringBuilder(f);
                sb.append(f.endsWith("/") ? "" : "/");
                sb.append("v1/startup");
                sb.append("?");
                sb.append(d2);
                str = sb.toString();
            } else {
                str = null;
            }
            if (!TextUtils.isEmpty(str)) {
                by byVar = new by(str, this.c, akVar);
                byVar.a((Object) this);
                this.e.a(this.a, (rs) byVar);
                return;
            }
            akVar.a(new r());
            return;
        }
        aVar.a();
    }

    public final void a() {
        this.e.a(this.a, (Object) this);
    }
}
