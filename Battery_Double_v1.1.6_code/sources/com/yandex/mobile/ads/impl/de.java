package com.yandex.mobile.ads.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class de {
    private static final ExecutorService a = Executors.newCachedThreadPool(new dx("YandexMobileAds.UrlTracker"));
    @NonNull
    private final Context b;
    @Nullable
    private fl c;
    @NonNull
    private final eb d = new eb(this.b);

    private static class a implements Runnable {
        @NonNull
        private final String a;
        @NonNull
        private final dn b;
        @NonNull
        private final eb c;

        a(@NonNull String str, @NonNull dn dnVar, @NonNull eb ebVar) {
            this.a = str;
            this.b = dnVar;
            this.c = ebVar;
        }

        public final void run() {
            String a2 = this.c.a(this.a);
            if (!TextUtils.isEmpty(a2)) {
                this.b.a(a2);
            }
        }
    }

    public de(@NonNull Context context, @NonNull fl flVar) {
        this.b = context.getApplicationContext();
        this.c = flVar;
    }

    public final void a(@Nullable String str) {
        a(str, new dl(this.b));
    }

    public final void a(@Nullable String str, @NonNull w wVar, @NonNull dk dkVar) {
        a(str, wVar, dkVar, new cp(this.b, wVar, this.c, null));
    }

    public final void a(@Nullable String str, @NonNull w wVar, @NonNull dk dkVar, @NonNull co coVar) {
        a(str, new dm(this.b, wVar, coVar, dkVar));
    }

    private void a(@Nullable String str, @NonNull dn dnVar) {
        if (!TextUtils.isEmpty(str)) {
            a.execute(new a(str, dnVar, this.d));
        }
    }
}
