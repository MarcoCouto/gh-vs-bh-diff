package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.mobile.ads.impl.hr.a;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class qv implements a {
    @NonNull
    private final qy a = new qy();
    @Nullable
    private w<or> b;

    public final void a(@NonNull w<or> wVar) {
        this.b = wVar;
    }

    @NonNull
    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        w<or> wVar = this.b;
        if (wVar != null) {
            List a2 = qy.a(wVar);
            if (!a2.isEmpty()) {
                hashMap.put("image_sizes", a2);
            }
            List b2 = qy.b(wVar);
            if (!b2.isEmpty()) {
                hashMap.put("native_ad_types", b2);
            }
        }
        return hashMap;
    }
}
