package com.yandex.mobile.ads.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;
import java.util.Map;

public final class bm {
    @NonNull
    private final String a;
    @NonNull
    private final Map<String, String> b;
    @Nullable
    private final List<String> c;
    @Nullable
    private final List<String> d;
    @Nullable
    private final List<String> e;

    public static class a {
        /* access modifiers changed from: private */
        @NonNull
        public final String a;
        /* access modifiers changed from: private */
        @NonNull
        public final Map<String, String> b;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> c;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> d;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> e;

        public a(@NonNull String str, @NonNull Map<String, String> map) {
            this.a = str;
            this.b = map;
        }

        @NonNull
        public final bm a() {
            return new bm(this, 0);
        }

        @NonNull
        public final a a(@Nullable List<String> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public final a b(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public final a c(@Nullable List<String> list) {
            this.e = list;
            return this;
        }
    }

    /* synthetic */ bm(a aVar, byte b2) {
        this(aVar);
    }

    private bm(@NonNull a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
    }

    @NonNull
    public final String a() {
        return this.a;
    }

    @NonNull
    public final Map<String, String> b() {
        return this.b;
    }

    @Nullable
    public final List<String> c() {
        return this.c;
    }

    @Nullable
    public final List<String> d() {
        return this.d;
    }

    @Nullable
    public final List<String> e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bm bmVar = (bm) obj;
        if (!this.a.equals(bmVar.a) || !this.b.equals(bmVar.b)) {
            return false;
        }
        if (this.c == null ? bmVar.c != null : !this.c.equals(bmVar.c)) {
            return false;
        }
        if (this.d == null ? bmVar.d != null : !this.d.equals(bmVar.d)) {
            return false;
        }
        if (this.e != null) {
            return this.e.equals(bmVar.e);
        }
        return bmVar.e == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31;
        if (this.e != null) {
            i = this.e.hashCode();
        }
        return hashCode + i;
    }
}
