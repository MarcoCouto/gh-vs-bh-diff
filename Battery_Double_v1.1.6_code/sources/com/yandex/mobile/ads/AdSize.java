package com.yandex.mobile.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.yandex.mobile.ads.impl.al.a;

public final class AdSize extends c {
    public static final AdSize BANNER_240x400 = new AdSize(PsExtractor.VIDEO_STREAM_MASK, ErrorCode.GENERAL_LINEAR_ERROR);
    public static final AdSize BANNER_300x250 = new AdSize(300, 250);
    public static final AdSize BANNER_300x300 = new AdSize(300, 300);
    public static final AdSize BANNER_320x100 = new AdSize(ModuleDescriptor.MODULE_VERSION, 100);
    public static final AdSize BANNER_320x50 = new AdSize(ModuleDescriptor.MODULE_VERSION, 50);
    public static final AdSize BANNER_400x240 = new AdSize(ErrorCode.GENERAL_LINEAR_ERROR, PsExtractor.VIDEO_STREAM_MASK);
    public static final AdSize BANNER_728x90 = new AdSize(728, 90);
    public static final int FULL_HEIGHT = -2;
    public static final AdSize FULL_SCREEN = new AdSize(-1, -2);
    public static final int FULL_WIDTH = -1;
    private static final long serialVersionUID = 2680092174282737642L;

    public AdSize(int i, int i2) {
        this(i, i2, a.FIXED);
    }

    private AdSize(int i, int i2, @NonNull a aVar) {
        super(i, i2, aVar);
    }

    public static AdSize flexibleSize() {
        return new AdSize(-1, 0, a.SCREEN);
    }

    public static AdSize flexibleSize(int i) {
        return new AdSize(i, 0, a.FLEXIBLE);
    }

    public static AdSize flexibleSize(int i, int i2) {
        return new AdSize(i, i2, a.FLEXIBLE);
    }

    public static AdSize stickySize(int i) {
        return new AdSize(i, 0, a.STICKY);
    }

    public final int getHeight() {
        return super.getHeight();
    }

    public final int getHeightInPixels(@NonNull Context context) {
        return super.getHeightInPixels(context);
    }

    public final int getWidth() {
        return super.getWidth();
    }

    public final int getWidthInPixels(@NonNull Context context) {
        return super.getWidthInPixels(context);
    }

    public final int getHeight(@NonNull Context context) {
        return super.getHeight(context);
    }

    public final int getWidth(@NonNull Context context) {
        return super.getWidth(context);
    }
}
