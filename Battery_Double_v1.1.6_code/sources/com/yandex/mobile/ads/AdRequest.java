package com.yandex.mobile.ads;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;
import java.util.Map;

public final class AdRequest {
    @Nullable
    private final String a;
    @Nullable
    private final String b;
    @Nullable
    private final String c;
    @Nullable
    private final List<String> d;
    @Nullable
    private final Location e;
    @Nullable
    private final Map<String, String> f;

    public static final class Builder {
        /* access modifiers changed from: private */
        @Nullable
        public String a;
        /* access modifiers changed from: private */
        @Nullable
        public String b;
        /* access modifiers changed from: private */
        @Nullable
        public Location c;
        /* access modifiers changed from: private */
        @Nullable
        public String d;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> e;
        /* access modifiers changed from: private */
        @Nullable
        public Map<String, String> f;

        @NonNull
        public final AdRequest build() {
            return new AdRequest(this, 0);
        }

        @NonNull
        public final Builder withAge(@NonNull String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public final Builder withContextQuery(@NonNull String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public final Builder withContextTags(@NonNull List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final Builder withGender(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final Builder withLocation(@NonNull Location location) {
            this.c = location;
            return this;
        }

        @NonNull
        public final Builder withParameters(@NonNull Map<String, String> map) {
            this.f = map;
            return this;
        }
    }

    /* synthetic */ AdRequest(Builder builder, byte b2) {
        this(builder);
    }

    private AdRequest(@NonNull Builder builder) {
        this.a = builder.a;
        this.b = builder.b;
        this.c = builder.d;
        this.d = builder.e;
        this.e = builder.c;
        this.f = builder.f;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Nullable
    public final String getAge() {
        return this.a;
    }

    @Nullable
    public final String getGender() {
        return this.b;
    }

    @Nullable
    public final String getContextQuery() {
        return this.c;
    }

    @Nullable
    public final List<String> getContextTags() {
        return this.d;
    }

    @Nullable
    public final Location getLocation() {
        return this.e;
    }

    @Nullable
    public final Map<String, String> getParameters() {
        return this.f;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdRequest adRequest = (AdRequest) obj;
        if (this.a == null ? adRequest.a != null : !this.a.equals(adRequest.a)) {
            return false;
        }
        if (this.b == null ? adRequest.b != null : !this.b.equals(adRequest.b)) {
            return false;
        }
        if (this.c == null ? adRequest.c != null : !this.c.equals(adRequest.c)) {
            return false;
        }
        if (this.d == null ? adRequest.d != null : !this.d.equals(adRequest.d)) {
            return false;
        }
        if (this.f != null) {
            return this.f.equals(adRequest.f);
        }
        return adRequest.f == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }
}
