package com.yandex.mobile.ads;

import android.support.annotation.NonNull;

public interface AdEventListener {

    public static class SimpleAdEventListener implements AdEventListener {
        public void onAdClosed() {
        }

        public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
        }

        public void onAdLeftApplication() {
        }

        public void onAdLoaded() {
        }

        public void onAdOpened() {
        }
    }

    void onAdClosed();

    void onAdFailedToLoad(@NonNull AdRequestError adRequestError);

    void onAdLeftApplication();

    void onAdLoaded();

    void onAdOpened();
}
