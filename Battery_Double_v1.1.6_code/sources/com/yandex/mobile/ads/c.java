package com.yandex.mobile.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.mobile.ads.impl.al;
import com.yandex.mobile.ads.impl.al.a;
import java.io.Serializable;

public abstract class c implements Serializable {
    private static final long serialVersionUID = -7571518881522543353L;
    @NonNull
    private final al a;

    protected c(int i, int i2, @NonNull a aVar) {
        this.a = new al(i, i2, aVar);
    }

    public int getHeight() {
        return this.a.b();
    }

    public int getHeightInPixels(@NonNull Context context) {
        return this.a.c(context);
    }

    public int getWidth() {
        return this.a.a();
    }

    public int getWidthInPixels(@NonNull Context context) {
        return this.a.d(context);
    }

    public int getHeight(@NonNull Context context) {
        return this.a.a(context);
    }

    public int getWidth(@NonNull Context context) {
        return this.a.b(context);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final al a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.a.equals(((c) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.a.toString();
    }
}
