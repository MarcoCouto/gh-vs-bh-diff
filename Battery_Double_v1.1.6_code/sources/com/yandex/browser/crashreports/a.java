package com.yandex.browser.crashreports;

import android.os.Debug;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class a {
    /* access modifiers changed from: private */
    public static final long a = TimeUnit.SECONDS.toMillis(1);
    private final C0080a b;
    /* access modifiers changed from: private */
    public final Handler c = new Handler(Looper.getMainLooper());
    private final Thread d = new b();
    /* access modifiers changed from: private */
    public final AtomicBoolean e = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final Runnable f = new Runnable() {
        public void run() {
            a.this.e.set(true);
        }
    };

    /* renamed from: com.yandex.browser.crashreports.a$a reason: collision with other inner class name */
    public interface C0080a {
        @WorkerThread
        void a();
    }

    private class b extends Thread {
        public b() {
        }

        public void run() {
            boolean z = false;
            int i = 0;
            while (!isInterrupted()) {
                if (!z) {
                    a.this.e.set(false);
                    a.this.c.post(a.this.f);
                    i = 0;
                }
                try {
                    Thread.sleep(a.a);
                    if (!a.this.e.get()) {
                        i++;
                        if (i == 4 && !Debug.isDebuggerConnected()) {
                            a.this.b();
                        }
                        z = true;
                    } else {
                        z = false;
                    }
                } catch (InterruptedException unused) {
                    return;
                }
            }
        }
    }

    public a(C0080a aVar) {
        this.b = aVar;
    }

    public void a() {
        try {
            this.d.setName("CR-WatchDog");
        } catch (SecurityException unused) {
        }
        this.d.start();
    }

    @VisibleForTesting
    public void b() {
        this.b.a();
    }
}
