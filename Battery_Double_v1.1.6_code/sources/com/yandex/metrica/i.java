package com.yandex.metrica;

import java.util.Map;

public class i {
    private String a;
    private String b;
    private Map<String, String> c;

    public String a() {
        return this.a;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public Map<String, String> c() {
        return this.c;
    }

    public void a(Map<String, String> map) {
        this.c = map;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        i iVar = (i) obj;
        if (this.a == null ? iVar.a != null : !this.a.equals(iVar.a)) {
            return false;
        }
        if (this.b == null ? iVar.b != null : !this.b.equals(iVar.b)) {
            return false;
        }
        if (this.c != null) {
            z = this.c.equals(iVar.c);
        } else if (iVar.c != null) {
            z = false;
        }
        return z;
    }
}
