package com.yandex.metrica.impl.ac;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.metrica.impl.ob.ado;
import com.yandex.metrica.impl.ob.eh;
import com.yandex.metrica.impl.ob.ej;
import com.yandex.metrica.impl.ob.em;
import com.yandex.metrica.impl.ob.pc;
import com.yandex.metrica.impl.ob.pd;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class a implements pd {
    private volatile String a = null;
    private volatile Boolean b = null;
    @NonNull
    private final ado c = new ado();

    /* renamed from: com.yandex.metrica.impl.ac.a$a reason: collision with other inner class name */
    private interface C0084a extends IInterface {

        /* renamed from: com.yandex.metrica.impl.ac.a$a$a reason: collision with other inner class name */
        public static abstract class C0085a extends Binder implements C0084a {

            /* renamed from: com.yandex.metrica.impl.ac.a$a$a$a reason: collision with other inner class name */
            private static class C0086a implements C0084a {
                private IBinder a;

                C0086a(IBinder iBinder) {
                    this.a = iBinder;
                }

                public IBinder asBinder() {
                    return this.a;
                }

                public String a() throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    try {
                        obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        this.a.transact(1, obtain, obtain2, 0);
                        obtain2.readException();
                        return obtain2.readString();
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }

                public boolean a(boolean z) throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    try {
                        obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        obtain.writeInt(z ? 1 : 0);
                        boolean z2 = false;
                        this.a.transact(2, obtain, obtain2, 0);
                        obtain2.readException();
                        if (obtain2.readInt() != 0) {
                            z2 = true;
                        }
                        return z2;
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }
            }

            public static C0084a a(IBinder iBinder) {
                if (iBinder == null) {
                    return null;
                }
                IInterface queryLocalInterface = iBinder.queryLocalInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                if (queryLocalInterface == null || !(queryLocalInterface instanceof C0084a)) {
                    return new C0086a(iBinder);
                }
                return (C0084a) queryLocalInterface;
            }

            public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
                switch (i) {
                    case 1:
                        parcel.enforceInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        String a = a();
                        parcel2.writeNoException();
                        parcel2.writeString(a);
                        return true;
                    case 2:
                        parcel.enforceInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        boolean a2 = a(parcel.readInt() != 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(a2 ? 1 : 0);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            }
        }

        String a() throws RemoteException;

        boolean a(boolean z) throws RemoteException;
    }

    private class b implements ServiceConnection {
        private boolean b;
        private final BlockingQueue<IBinder> c;

        public void onServiceDisconnected(ComponentName componentName) {
        }

        private b() {
            this.b = false;
            this.c = new LinkedBlockingQueue();
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.c.put(iBinder);
            } catch (InterruptedException unused) {
            }
        }

        public IBinder a() throws InterruptedException {
            if (!this.b) {
                this.b = true;
                return (IBinder) this.c.take();
            }
            throw new IllegalStateException();
        }
    }

    private void a(String str) {
        eh.a().b((ej) new em(str));
        this.a = str;
    }

    private void a(Boolean bool) {
        this.b = bool;
    }

    private synchronized boolean a() {
        return (this.a == null || this.b == null) ? false : true;
    }

    private boolean b(Context context) {
        try {
            return Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", new Class[]{Context.class}).invoke(null, new Object[]{context}).equals(Integer.valueOf(0));
        } catch (Exception unused) {
            return false;
        }
    }

    private void c(Context context) {
        try {
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context});
            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            String str = (String) cls.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
            Boolean bool = (Boolean) cls.getMethod(RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0]);
            synchronized (this) {
                a(str);
                a(bool);
            }
        } catch (Throwable unused) {
        }
    }

    private void d(Context context) {
        b bVar = new b();
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        if (this.c.c(context, intent, 0) != null && context.bindService(intent, bVar, 1)) {
            try {
                C0084a a2 = C0085a.a(bVar.a());
                String a3 = a2.a();
                Boolean valueOf = Boolean.valueOf(a2.a(true));
                synchronized (this) {
                    a(a3);
                    a(valueOf);
                }
            } catch (Throwable th) {
                context.unbindService(bVar);
                throw th;
            }
            context.unbindService(bVar);
        }
    }

    @Nullable
    public pc a(@NonNull Context context) {
        if (!a()) {
            Context applicationContext = context.getApplicationContext();
            if (b(applicationContext)) {
                c(applicationContext);
            }
            if (!a()) {
                d(applicationContext);
            }
            if (!a()) {
                return null;
            }
        }
        return new pc(com.yandex.metrica.impl.ob.pc.a.GOOGLE, this.a, this.b);
    }
}
