package com.yandex.metrica.impl.ac;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.an;

public class NativeCrashesHelper {
    private String a;
    private final Context b;
    private boolean c;
    private boolean d;
    @NonNull
    private final an e;

    private static native void cancelSetUpNativeUncaughtExceptionHandler();

    private static native void logsEnabled(boolean z);

    private static native void setUpNativeUncaughtExceptionHandler(String str);

    public NativeCrashesHelper(@NonNull Context context) {
        this(context, new an());
    }

    @VisibleForTesting
    NativeCrashesHelper(@NonNull Context context, @NonNull an anVar) {
        this.b = context;
        this.e = anVar;
    }

    public synchronized void a(boolean z) {
        if (z) {
            try {
                c();
            } catch (Throwable th) {
                throw th;
            }
        } else {
            e();
        }
    }

    private void b() {
        if (!this.d && a()) {
            b(false);
            StringBuilder sb = new StringBuilder();
            sb.append(this.e.a(this.b).getAbsolutePath());
            sb.append("/");
            sb.append("YandexMetricaNativeCrashes");
            this.a = sb.toString();
        }
        this.d = true;
    }

    private void c() {
        try {
            b();
            if (d()) {
                setUpNativeUncaughtExceptionHandler(this.a);
                this.c = true;
            }
        } catch (Throwable unused) {
            this.c = false;
        }
    }

    private boolean d() {
        return this.a != null;
    }

    private void e() {
        try {
            if (d() && this.c) {
                cancelSetUpNativeUncaughtExceptionHandler();
            }
        } catch (Throwable unused) {
        }
        this.c = false;
    }

    private boolean b(boolean z) {
        try {
            logsEnabled(z);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a() {
        try {
            System.loadLibrary("YandexMetricaNativeModule");
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }
}
