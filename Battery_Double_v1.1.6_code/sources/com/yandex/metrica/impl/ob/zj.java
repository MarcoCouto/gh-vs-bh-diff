package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public class zj implements zl<List<zi>> {
    /* access modifiers changed from: private */
    @NonNull
    public final ze a;
    /* access modifiers changed from: private */
    @NonNull
    public so b;

    zj(@NonNull ze zeVar, @NonNull so soVar) {
        this.a = zeVar;
        this.b = soVar;
    }

    @Nullable
    /* renamed from: a */
    public List<zi> d() {
        ArrayList arrayList = new ArrayList();
        if (this.a.h()) {
            if (dl.a(23)) {
                arrayList.addAll(g());
                if (arrayList.size() == 0) {
                    arrayList.add(b());
                }
            } else {
                arrayList.add(b());
            }
        }
        return arrayList;
    }

    private zi b() {
        zi ziVar = new zi(c(), e(), h(), f(), null);
        return ziVar;
    }

    @Nullable
    private Integer c() {
        return (Integer) dl.a((aca<T, S>) new aca<TelephonyManager, Integer>() {
            public Integer a(@NonNull TelephonyManager telephonyManager) throws Throwable {
                String simOperator = telephonyManager.getSimOperator();
                String substring = !TextUtils.isEmpty(simOperator) ? simOperator.substring(0, 3) : null;
                if (TextUtils.isEmpty(substring)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(substring));
            }
        }, this.a.c(), "getting SimMcc", "TelephonyManager");
    }

    @Nullable
    private Integer e() {
        return (Integer) dl.a((aca<T, S>) new aca<TelephonyManager, Integer>() {
            public Integer a(@NonNull TelephonyManager telephonyManager) throws Throwable {
                String simOperator = telephonyManager.getSimOperator();
                String substring = !TextUtils.isEmpty(simOperator) ? simOperator.substring(3) : null;
                if (TextUtils.isEmpty(substring)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(substring));
            }
        }, this.a.c(), "getting SimMnc", "TelephonyManager");
    }

    @Nullable
    private String f() {
        return (String) dl.a((aca<T, S>) new aca<TelephonyManager, String>() {
            public String a(@NonNull TelephonyManager telephonyManager) throws Throwable {
                return telephonyManager.getSimOperatorName();
            }
        }, this.a.c(), "getting SimOperatorName", "TelephonyManager");
    }

    @TargetApi(23)
    @NonNull
    private List<zi> g() {
        ArrayList arrayList = new ArrayList();
        if (this.b.d(this.a.d())) {
            try {
                List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(this.a.d()).getActiveSubscriptionInfoList();
                if (activeSubscriptionInfoList != null) {
                    for (SubscriptionInfo ziVar : activeSubscriptionInfoList) {
                        arrayList.add(new zi(ziVar));
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return arrayList;
    }

    private boolean h() {
        return ((Boolean) dl.a(new aca<TelephonyManager, Boolean>() {
            public Boolean a(@NonNull TelephonyManager telephonyManager) throws Throwable {
                if (zj.this.b.d(zj.this.a.d())) {
                    return Boolean.valueOf(telephonyManager.isNetworkRoaming());
                }
                return null;
            }
        }, this.a.c(), "getting NetworkRoaming", "TelephonyManager", Boolean.valueOf(false))).booleanValue();
    }
}
