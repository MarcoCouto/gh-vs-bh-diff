package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class py {
    @NonNull
    public final rk a;
    @Nullable
    public final pw b;

    public py(@NonNull rk rkVar, @Nullable pw pwVar) {
        this.a = rkVar;
        this.b = pwVar;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GplCollectingConfig{providerAccessFlags=");
        sb.append(this.a);
        sb.append(", arguments=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        py pyVar = (py) obj;
        if (!this.a.equals(pyVar.a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(pyVar.b);
        } else if (pyVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0);
    }
}
