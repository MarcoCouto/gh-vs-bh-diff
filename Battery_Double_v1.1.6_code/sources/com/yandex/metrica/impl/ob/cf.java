package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

public class cf {
    @NonNull
    public sp a(@NonNull final so soVar) {
        if (dl.a(29)) {
            return new sp() {
                public boolean a(@NonNull Context context) {
                    return soVar.b(context) && soVar.e(context);
                }
            };
        }
        if (dl.a(23)) {
            return new sp() {
                public boolean a(@NonNull Context context) {
                    return soVar.c(context) && soVar.e(context);
                }
            };
        }
        return new sp() {
            public boolean a(@NonNull Context context) {
                return soVar.e(context);
            }
        };
    }

    @NonNull
    public sp b(@NonNull final so soVar) {
        if (dl.a(29)) {
            return new sp() {
                public boolean a(@NonNull Context context) {
                    return soVar.b(context) && soVar.f(context);
                }
            };
        }
        if (dl.a(23)) {
            return new sp() {
                public boolean a(@NonNull Context context) {
                    return soVar.c(context) && soVar.f(context);
                }
            };
        }
        return new sp() {
            public boolean a(@NonNull Context context) {
                return soVar.f(context);
            }
        };
    }

    @NonNull
    public sp c(@NonNull final so soVar) {
        return new sp() {
            public boolean a(@NonNull Context context) {
                return soVar.b(context);
            }
        };
    }

    @NonNull
    public sp d(@NonNull final so soVar) {
        return new sp() {
            public boolean a(@NonNull Context context) {
                return soVar.c(context);
            }
        };
    }
}
