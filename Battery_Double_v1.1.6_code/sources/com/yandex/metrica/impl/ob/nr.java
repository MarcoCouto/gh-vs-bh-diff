package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a;

public class nr implements np<ql, C0105a> {
    @NonNull
    private final nk a;
    @NonNull
    private final nm b;

    public nr() {
        this(new nk(), new nm());
    }

    @VisibleForTesting
    nr(@NonNull nk nkVar, @NonNull nm nmVar) {
        this.a = nkVar;
        this.b = nmVar;
    }

    @NonNull
    /* renamed from: a */
    public C0105a b(@NonNull ql qlVar) {
        C0105a aVar = new C0105a();
        aVar.b = qlVar.a;
        aVar.c = qlVar.b;
        aVar.d = qlVar.c;
        aVar.e = qlVar.d;
        aVar.f = qlVar.e;
        aVar.g = qlVar.f;
        aVar.h = qlVar.g;
        aVar.k = qlVar.h;
        aVar.i = qlVar.i;
        aVar.j = qlVar.j;
        if (qlVar.k != null) {
            aVar.l = this.a.b(qlVar.k);
        }
        if (qlVar.l != null) {
            aVar.m = this.a.b(qlVar.l);
        }
        if (qlVar.m != null) {
            aVar.n = this.a.b(qlVar.m);
        }
        if (qlVar.n != null) {
            aVar.o = this.a.b(qlVar.n);
        }
        if (qlVar.o != null) {
            aVar.p = this.b.b(qlVar.o);
        }
        return aVar;
    }

    @NonNull
    public ql a(@NonNull C0105a aVar) {
        C0105a aVar2 = aVar;
        py pyVar = null;
        pt a2 = aVar2.l != null ? this.a.a(aVar2.l) : null;
        pt a3 = aVar2.m != null ? this.a.a(aVar2.m) : null;
        pt a4 = aVar2.n != null ? this.a.a(aVar2.n) : null;
        pt a5 = aVar2.o != null ? this.a.a(aVar2.o) : null;
        if (aVar2.p != null) {
            pyVar = this.b.a(aVar2.p);
        }
        ql qlVar = new ql(aVar2.b, aVar2.c, aVar2.d, aVar2.e, aVar2.f, aVar2.g, aVar2.h, aVar2.k, aVar2.i, aVar2.j, a2, a3, a4, a5, pyVar);
        return qlVar;
    }
}
