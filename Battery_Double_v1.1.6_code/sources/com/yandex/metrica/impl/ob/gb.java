package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class gb {
    @NonNull
    private final mo a;
    @NonNull
    private final abt b;
    @NonNull
    private final dj c;
    @Nullable
    private yd d;
    private long e;

    public gb(@NonNull Context context, @NonNull fb fbVar) {
        this(new mo(lv.a(context).b(fbVar)), new abs(), new dj());
    }

    public gb(@NonNull mo moVar, @NonNull abt abt, @NonNull dj djVar) {
        this.a = moVar;
        this.b = abt;
        this.c = djVar;
        this.e = this.a.k();
    }

    public boolean a(@Nullable Boolean bool) {
        return aau.c(bool) && this.d != null && this.c.b(this.e, this.d.a, "should report diagnostic");
    }

    public void a() {
        this.e = this.b.a();
        this.a.f(this.e).q();
    }

    public void a(@Nullable yd ydVar) {
        this.d = ydVar;
    }
}
