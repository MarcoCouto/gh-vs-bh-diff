package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public interface nh<S, P> {
    @NonNull
    S a(@NonNull P p);

    @NonNull
    P b(@NonNull S s);
}
