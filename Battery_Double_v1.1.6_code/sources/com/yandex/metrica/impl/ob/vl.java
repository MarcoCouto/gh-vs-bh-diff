package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.yandex.metrica.j;

public class vl {
    @WorkerThread
    public dr a(@NonNull Context context) {
        return dr.b(context);
    }

    @Nullable
    @WorkerThread
    public dr a() {
        return dr.e();
    }

    public void a(@NonNull Context context, @NonNull j jVar) {
        dr.a(context, jVar);
    }

    public void b(@NonNull Context context, @NonNull j jVar) {
        dr.b(context, jVar);
    }

    public void b() {
        dr.b();
    }

    public boolean c() {
        return dr.c();
    }

    public boolean d() {
        return dr.g();
    }

    public boolean e() {
        return dr.e() != null;
    }

    public bj f() {
        return dr.f();
    }

    public void a(@Nullable Location location) {
        dr.a(location);
    }

    public void a(boolean z) {
        dr.a(z);
    }

    public void a(@NonNull Context context, boolean z) {
        dr.b(context).b(z);
    }

    public void b(@NonNull Context context, boolean z) {
        dr.b(context).d(z);
    }

    public dr g() {
        return dr.d();
    }
}
