package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ew.a;

public class fy {
    @NonNull
    private a a;

    public fy(@NonNull a aVar) {
        this.a = aVar;
    }

    public void a(@NonNull a aVar) {
        this.a = this.a.b(aVar);
    }

    @NonNull
    public a a() {
        return this.a;
    }
}
