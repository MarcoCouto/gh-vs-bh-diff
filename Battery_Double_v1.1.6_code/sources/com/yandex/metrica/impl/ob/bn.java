package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.IMetricaService;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

public class bn {
    public static final long a = TimeUnit.SECONDS.toMillis(10);
    private final Context b;
    private final act c;
    private boolean d;
    private final List<a> e = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public volatile IMetricaService f = null;
    private final Object g = new Object();
    private final Runnable h = new Runnable() {
        public void run() {
            bn.this.i();
        }
    };
    private final ServiceConnection i = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            bn.this.f = com.yandex.metrica.IMetricaService.a.a(iBinder);
            bn.this.j();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            bn.this.f = null;
            bn.this.k();
        }
    };

    interface a {
        void a();

        void b();
    }

    public Context a() {
        return this.b;
    }

    public bn(Context context, act act) {
        this.b = context.getApplicationContext();
        this.c = act;
        this.d = false;
    }

    public synchronized void b() {
        if (this.f == null) {
            try {
                this.b.bindService(dc.b(this.b), this.i, 1);
            } catch (Throwable unused) {
            }
        }
    }

    public void c() {
        a(this.c);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull act act) {
        synchronized (this.g) {
            act.b(this.h);
            if (!this.d) {
                act.a(this.h, a);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void d() {
        this.c.b(this.h);
    }

    public boolean e() {
        return this.f != null;
    }

    public IMetricaService f() {
        return this.f;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:1|2|(2:6|7)|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0015 */
    public synchronized void i() {
        if (this.b != null && e()) {
            this.b.unbindService(this.i);
            this.f = null;
        }
        this.f = null;
        k();
    }

    /* access modifiers changed from: private */
    public void j() {
        for (a a2 : this.e) {
            a2.a();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        for (a b2 : this.e) {
            b2.b();
        }
    }

    public void a(a aVar) {
        this.e.add(aVar);
    }

    public void g() {
        synchronized (this.g) {
            this.d = true;
        }
        d();
    }

    public void h() {
        this.d = false;
        c();
    }
}
