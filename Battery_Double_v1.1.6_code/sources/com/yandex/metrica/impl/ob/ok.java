package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a;
import com.yandex.metrica.impl.ob.xi.a.b;

public class ok implements np<b, C0103a.b> {
    @NonNull
    /* renamed from: a */
    public C0103a.b b(@NonNull b bVar) {
        C0103a.b bVar2 = new C0103a.b();
        bVar2.b = bVar.a.toString();
        if (bVar.b != null) {
            bVar2.c = bVar.b;
        }
        if (bVar.c != null) {
            bVar2.d = bVar.c;
        }
        return bVar2;
    }

    @NonNull
    public b a(@NonNull C0103a.b bVar) {
        String str = bVar.b;
        byte[] bArr = null;
        byte[] bArr2 = dl.a(bVar.c) ? null : bVar.c;
        if (!dl.a(bVar.d)) {
            bArr = bVar.d;
        }
        return new b(str, bArr2, bArr);
    }
}
