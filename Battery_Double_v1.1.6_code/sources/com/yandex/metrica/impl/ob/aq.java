package com.yandex.metrica.impl.ob;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeUnit;

class aq {
    private static final Long f = Long.valueOf(TimeUnit.SECONDS.toMillis(5));
    @NonNull
    private final Context a;
    @Nullable
    private final ActivityManager b;
    @NonNull
    private final act c;
    private volatile boolean d;
    private final Set<a> e;
    private final Runnable g;

    interface a {
        void a(boolean z);
    }

    public aq(@NonNull Context context, @NonNull act act) {
        this(context, (ActivityManager) context.getSystemService("activity"), act);
    }

    public void a() {
        d();
    }

    public void b() {
        this.c.b(this.g);
    }

    private void c() {
        this.c.a(this.g, f.longValue());
    }

    public boolean a(@Nullable a aVar) {
        if (aVar != null) {
            this.e.add(aVar);
        }
        return this.d;
    }

    public void b(@NonNull a aVar) {
        this.e.remove(aVar);
    }

    /* access modifiers changed from: private */
    public void d() {
        e();
        c();
    }

    private void e() {
        boolean g2 = g();
        if (this.d != g2) {
            this.d = g2;
            f();
        }
    }

    private void f() {
        for (a a2 : this.e) {
            a2.a(this.d);
        }
    }

    private boolean g() {
        List<RunningServiceInfo> list = (List) dl.a((aca<T, S>) new aca<ActivityManager, List<RunningServiceInfo>>() {
            public List<RunningServiceInfo> a(ActivityManager activityManager) throws Throwable {
                return activityManager.getRunningServices(Integer.MAX_VALUE);
            }
        }, this.b, "getRunningServices", "ActivityManager");
        if (list != null) {
            for (RunningServiceInfo a2 : list) {
                if (a(a2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean a(@NonNull RunningServiceInfo runningServiceInfo) {
        return this.a.getPackageName().equals(runningServiceInfo.service.getPackageName()) && runningServiceInfo.foreground;
    }

    @VisibleForTesting
    aq(@NonNull Context context, @Nullable ActivityManager activityManager, @NonNull act act) {
        this.e = new CopyOnWriteArraySet();
        this.g = new Runnable() {
            public void run() {
                aq.this.d();
            }
        };
        this.a = context;
        this.b = activityManager;
        this.c = act;
    }
}
