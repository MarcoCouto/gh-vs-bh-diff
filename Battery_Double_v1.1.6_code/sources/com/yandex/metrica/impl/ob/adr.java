package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class adr<T> implements adw<T> {
    @NonNull
    private final String a;

    public adr(@NonNull String str) {
        this.a = str;
    }

    public adu a(@Nullable T t) {
        if (t != null) {
            return adu.a(this);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(" is null.");
        return adu.a(this, sb.toString());
    }
}
