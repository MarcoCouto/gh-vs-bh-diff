package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

public class ql {
    public final long a;
    public final float b;
    public final int c;
    public final int d;
    public final long e;
    public final int f;
    public final boolean g;
    public final long h;
    public final boolean i;
    public final boolean j;
    @Nullable
    public final pt k;
    @Nullable
    public final pt l;
    @Nullable
    public final pt m;
    @Nullable
    public final pt n;
    @Nullable
    public final py o;

    public ql(long j2, float f2, int i2, int i3, long j3, int i4, boolean z, long j4, boolean z2, boolean z3, @Nullable pt ptVar, @Nullable pt ptVar2, @Nullable pt ptVar3, @Nullable pt ptVar4, @Nullable py pyVar) {
        this.a = j2;
        this.b = f2;
        this.c = i2;
        this.d = i3;
        this.e = j3;
        this.f = i4;
        this.g = z;
        this.h = j4;
        this.i = z2;
        this.j = z3;
        this.k = ptVar;
        this.l = ptVar2;
        this.m = ptVar3;
        this.n = ptVar4;
        this.o = pyVar;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Arguments{updateTimeInterval=");
        sb.append(this.a);
        sb.append(", updateDistanceInterval=");
        sb.append(this.b);
        sb.append(", recordsCountToForceFlush=");
        sb.append(this.c);
        sb.append(", maxBatchSize=");
        sb.append(this.d);
        sb.append(", maxAgeToForceFlush=");
        sb.append(this.e);
        sb.append(", maxRecordsToStoreLocally=");
        sb.append(this.f);
        sb.append(", collectionEnabled=");
        sb.append(this.g);
        sb.append(", lbsUpdateTimeInterval=");
        sb.append(this.h);
        sb.append(", lbsCollectionEnabled=");
        sb.append(this.i);
        sb.append(", passiveCollectionEnabled=");
        sb.append(this.j);
        sb.append(", wifiAccessConfig=");
        sb.append(this.k);
        sb.append(", lbsAccessConfig=");
        sb.append(this.l);
        sb.append(", gpsAccessConfig=");
        sb.append(this.m);
        sb.append(", passiveAccessConfig=");
        sb.append(this.n);
        sb.append(", gplConfig=");
        sb.append(this.o);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ql qlVar = (ql) obj;
        if (this.a != qlVar.a || Float.compare(qlVar.b, this.b) != 0 || this.c != qlVar.c || this.d != qlVar.d || this.e != qlVar.e || this.f != qlVar.f || this.g != qlVar.g || this.h != qlVar.h || this.i != qlVar.i || this.j != qlVar.j) {
            return false;
        }
        if (this.k == null ? qlVar.k != null : !this.k.equals(qlVar.k)) {
            return false;
        }
        if (this.l == null ? qlVar.l != null : !this.l.equals(qlVar.l)) {
            return false;
        }
        if (this.m == null ? qlVar.m != null : !this.m.equals(qlVar.m)) {
            return false;
        }
        if (this.n == null ? qlVar.n != null : !this.n.equals(qlVar.n)) {
            return false;
        }
        if (this.o != null) {
            z = this.o.equals(qlVar.o);
        } else if (qlVar.o != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i2 = 0;
        int floatToIntBits = ((((((((((((((((((((((((((((int) (this.a ^ (this.a >>> 32))) * 31) + (this.b != 0.0f ? Float.floatToIntBits(this.b) : 0)) * 31) + this.c) * 31) + this.d) * 31) + ((int) (this.e ^ (this.e >>> 32)))) * 31) + this.f) * 31) + (this.g ? 1 : 0)) * 31) + ((int) ((this.h >>> 32) ^ this.h))) * 31) + (this.i ? 1 : 0)) * 31) + (this.j ? 1 : 0)) * 31) + (this.k != null ? this.k.hashCode() : 0)) * 31) + (this.l != null ? this.l.hashCode() : 0)) * 31) + (this.m != null ? this.m.hashCode() : 0)) * 31) + (this.n != null ? this.n.hashCode() : 0)) * 31;
        if (this.o != null) {
            i2 = this.o.hashCode();
        }
        return floatToIntBits + i2;
    }
}
