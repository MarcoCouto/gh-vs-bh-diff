package com.yandex.metrica.impl.ob;

import java.io.IOException;

public final class a {
    private final byte[] a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g = Integer.MAX_VALUE;
    private int h;
    private int i = 64;
    private int j = 67108864;

    public static long a(long j2) {
        return (-(j2 & 1)) ^ (j2 >>> 1);
    }

    public static int c(int i2) {
        return (-(i2 & 1)) ^ (i2 >>> 1);
    }

    public static a a(byte[] bArr, int i2, int i3) {
        return new a(bArr, i2, i3);
    }

    public int a() throws IOException {
        if (s()) {
            this.f = 0;
            return 0;
        }
        this.f = n();
        if (this.f != 0) {
            return this.f;
        }
        throw d.d();
    }

    public void a(int i2) throws d {
        if (this.f != i2) {
            throw d.e();
        }
    }

    public boolean b(int i2) throws IOException {
        switch (g.a(i2)) {
            case 0:
                g();
                return true;
            case 1:
                q();
                return true;
            case 2:
                h(n());
                return true;
            case 3:
                b();
                a(g.a(g.b(i2), 4));
                return true;
            case 4:
                return false;
            case 5:
                p();
                return true;
            default:
                throw d.f();
        }
    }

    public void b() throws IOException {
        int a2;
        do {
            a2 = a();
            if (a2 == 0) {
                return;
            }
        } while (b(a2));
    }

    public double c() throws IOException {
        return Double.longBitsToDouble(q());
    }

    public float d() throws IOException {
        return Float.intBitsToFloat(p());
    }

    public long e() throws IOException {
        return o();
    }

    public long f() throws IOException {
        return o();
    }

    public int g() throws IOException {
        return n();
    }

    public boolean h() throws IOException {
        return n() != 0;
    }

    public String i() throws IOException {
        int n = n();
        if (n > this.c - this.e || n <= 0) {
            return new String(g(n), "UTF-8");
        }
        String str = new String(this.a, this.e, n, "UTF-8");
        this.e += n;
        return str;
    }

    public void a(e eVar) throws IOException {
        int n = n();
        if (this.h < this.i) {
            int d2 = d(n);
            this.h++;
            eVar.a(this);
            a(0);
            this.h--;
            e(d2);
            return;
        }
        throw d.g();
    }

    public byte[] j() throws IOException {
        int n = n();
        if (n > this.c - this.e || n <= 0) {
            return g(n);
        }
        byte[] bArr = new byte[n];
        System.arraycopy(this.a, this.e, bArr, 0, n);
        this.e += n;
        return bArr;
    }

    public int k() throws IOException {
        return n();
    }

    public int l() throws IOException {
        return c(n());
    }

    public long m() throws IOException {
        return a(o());
    }

    public int n() throws IOException {
        byte b2;
        byte u = u();
        if (u >= 0) {
            return u;
        }
        byte b3 = u & Byte.MAX_VALUE;
        byte u2 = u();
        if (u2 >= 0) {
            b2 = b3 | (u2 << 7);
        } else {
            byte b4 = b3 | ((u2 & Byte.MAX_VALUE) << 7);
            byte u3 = u();
            if (u3 >= 0) {
                b2 = b4 | (u3 << 14);
            } else {
                byte b5 = b4 | ((u3 & Byte.MAX_VALUE) << 14);
                byte u4 = u();
                if (u4 >= 0) {
                    b2 = b5 | (u4 << 21);
                } else {
                    byte b6 = b5 | ((u4 & Byte.MAX_VALUE) << 21);
                    byte u5 = u();
                    b2 = b6 | (u5 << 28);
                    if (u5 < 0) {
                        for (int i2 = 0; i2 < 5; i2++) {
                            if (u() >= 0) {
                                return b2;
                            }
                        }
                        throw d.c();
                    }
                }
            }
        }
        return b2;
    }

    public long o() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte u = u();
            j2 |= ((long) (u & Byte.MAX_VALUE)) << i2;
            if ((u & 128) == 0) {
                return j2;
            }
        }
        throw d.c();
    }

    public int p() throws IOException {
        return (u() & 255) | ((u() & 255) << 8) | ((u() & 255) << 16) | ((u() & 255) << 24);
    }

    public long q() throws IOException {
        byte u = u();
        byte u2 = u();
        return ((((long) u2) & 255) << 8) | (((long) u) & 255) | ((((long) u()) & 255) << 16) | ((((long) u()) & 255) << 24) | ((((long) u()) & 255) << 32) | ((((long) u()) & 255) << 40) | ((((long) u()) & 255) << 48) | ((((long) u()) & 255) << 56);
    }

    private a(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        this.c = i3 + i2;
        this.e = i2;
    }

    public int d(int i2) throws d {
        if (i2 >= 0) {
            int i3 = i2 + this.e;
            int i4 = this.g;
            if (i3 <= i4) {
                this.g = i3;
                v();
                return i4;
            }
            throw d.a();
        }
        throw d.b();
    }

    private void v() {
        this.c += this.d;
        int i2 = this.c;
        if (i2 > this.g) {
            this.d = i2 - this.g;
            this.c -= this.d;
            return;
        }
        this.d = 0;
    }

    public void e(int i2) {
        this.g = i2;
        v();
    }

    public int r() {
        if (this.g == Integer.MAX_VALUE) {
            return -1;
        }
        return this.g - this.e;
    }

    public boolean s() {
        return this.e == this.c;
    }

    public int t() {
        return this.e - this.b;
    }

    public void f(int i2) {
        if (i2 > this.e - this.b) {
            StringBuilder sb = new StringBuilder();
            sb.append("Position ");
            sb.append(i2);
            sb.append(" is beyond current ");
            sb.append(this.e - this.b);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0) {
            this.e = this.b + i2;
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Bad position ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public byte u() throws IOException {
        if (this.e != this.c) {
            byte[] bArr = this.a;
            int i2 = this.e;
            this.e = i2 + 1;
            return bArr[i2];
        }
        throw d.a();
    }

    public byte[] g(int i2) throws IOException {
        if (i2 < 0) {
            throw d.b();
        } else if (this.e + i2 > this.g) {
            h(this.g - this.e);
            throw d.a();
        } else if (i2 <= this.c - this.e) {
            byte[] bArr = new byte[i2];
            System.arraycopy(this.a, this.e, bArr, 0, i2);
            this.e += i2;
            return bArr;
        } else {
            throw d.a();
        }
    }

    public void h(int i2) throws IOException {
        if (i2 < 0) {
            throw d.b();
        } else if (this.e + i2 > this.g) {
            h(this.g - this.e);
            throw d.a();
        } else if (i2 <= this.c - this.e) {
            this.e += i2;
        } else {
            throw d.a();
        }
    }
}
