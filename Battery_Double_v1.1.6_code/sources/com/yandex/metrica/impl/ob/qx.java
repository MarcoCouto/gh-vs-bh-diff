package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationListener;
import android.os.Looper;
import android.support.annotation.NonNull;
import java.util.concurrent.TimeUnit;

public abstract class qx {
    static final long a = TimeUnit.SECONDS.toMillis(1);
    @NonNull
    protected final Context b;
    @NonNull
    protected final sp c;
    @NonNull
    protected final LocationListener d;
    @NonNull
    protected final Looper e;

    public abstract boolean a();

    public abstract void b();

    public abstract void c();

    public qx(@NonNull Context context, @NonNull LocationListener locationListener, @NonNull sp spVar, @NonNull Looper looper) {
        this.b = context;
        this.d = locationListener;
        this.c = spVar;
        this.e = looper;
    }
}
