package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class xk {
    public final boolean a;
    public final boolean b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final boolean j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public final boolean o;
    public final boolean p;
    public final boolean q;
    public final boolean r;
    public final boolean s;
    public final boolean t;
    public final boolean u;
    public final boolean v;
    public final boolean w;

    public static class a {
        /* access modifiers changed from: private */
        public boolean a = b.a;
        /* access modifiers changed from: private */
        public boolean b = b.b;
        /* access modifiers changed from: private */
        public boolean c = b.c;
        /* access modifiers changed from: private */
        public boolean d = b.d;
        /* access modifiers changed from: private */
        public boolean e = b.e;
        /* access modifiers changed from: private */
        public boolean f = b.f;
        /* access modifiers changed from: private */
        public boolean g = b.g;
        /* access modifiers changed from: private */
        public boolean h = b.h;
        /* access modifiers changed from: private */
        public boolean i = b.i;
        /* access modifiers changed from: private */
        public boolean j = b.j;
        /* access modifiers changed from: private */
        public boolean k = b.k;
        /* access modifiers changed from: private */
        public boolean l = b.l;
        /* access modifiers changed from: private */
        public boolean m = b.p;
        /* access modifiers changed from: private */
        public boolean n = b.m;
        /* access modifiers changed from: private */
        public boolean o = b.n;
        /* access modifiers changed from: private */
        public boolean p = b.o;
        /* access modifiers changed from: private */
        public boolean q = b.q;
        /* access modifiers changed from: private */
        public boolean r = b.r;
        /* access modifiers changed from: private */
        public boolean s = b.s;
        /* access modifiers changed from: private */
        public boolean t = b.t;
        /* access modifiers changed from: private */
        public boolean u = b.u;
        /* access modifiers changed from: private */
        public boolean v = b.v;
        /* access modifiers changed from: private */
        public boolean w = b.w;

        public a a(boolean z) {
            this.a = z;
            return this;
        }

        public a b(boolean z) {
            this.b = z;
            return this;
        }

        public a c(boolean z) {
            this.c = z;
            return this;
        }

        public a d(boolean z) {
            this.d = z;
            return this;
        }

        public a e(boolean z) {
            this.e = z;
            return this;
        }

        public a f(boolean z) {
            this.g = z;
            return this;
        }

        public a g(boolean z) {
            this.h = z;
            return this;
        }

        public a h(boolean z) {
            this.i = z;
            return this;
        }

        public a i(boolean z) {
            this.j = z;
            return this;
        }

        public a j(boolean z) {
            this.k = z;
            return this;
        }

        public a k(boolean z) {
            this.l = z;
            return this;
        }

        public a l(boolean z) {
            this.n = z;
            return this;
        }

        public a m(boolean z) {
            this.o = z;
            return this;
        }

        public a n(boolean z) {
            this.p = z;
            return this;
        }

        public a o(boolean z) {
            this.m = z;
            return this;
        }

        public a p(boolean z) {
            this.f = z;
            return this;
        }

        public a q(boolean z) {
            this.q = z;
            return this;
        }

        public a r(boolean z) {
            this.r = z;
            return this;
        }

        public a s(boolean z) {
            this.s = z;
            return this;
        }

        public a t(boolean z) {
            this.t = z;
            return this;
        }

        public a u(boolean z) {
            this.u = z;
            return this;
        }

        public a v(boolean z) {
            this.w = z;
            return this;
        }

        public a w(boolean z) {
            this.v = z;
            return this;
        }

        public xk a() {
            return new xk(this);
        }
    }

    public static final class b {
        public static final boolean a = x.b;
        public static final boolean b = x.c;
        public static final boolean c = x.d;
        public static final boolean d = x.e;
        public static final boolean e = x.o;
        public static final boolean f = x.p;
        public static final boolean g = x.q;
        public static final boolean h = x.f;
        public static final boolean i = x.g;
        public static final boolean j = x.h;
        public static final boolean k = x.i;
        public static final boolean l = x.j;
        public static final boolean m = x.k;
        public static final boolean n = x.l;
        public static final boolean o = x.m;
        public static final boolean p = x.n;
        public static final boolean q = x.r;
        public static final boolean r = x.s;
        public static final boolean s = x.t;
        public static final boolean t = x.u;
        public static final boolean u = x.v;
        public static final boolean v = x.x;
        public static final boolean w = x.w;
        private static final com.yandex.metrica.impl.ob.ve.a.b x = new com.yandex.metrica.impl.ob.ve.a.b();
    }

    public xk(@NonNull a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.o = aVar.h;
        this.p = aVar.i;
        this.q = aVar.j;
        this.r = aVar.k;
        this.s = aVar.l;
        this.t = aVar.m;
        this.u = aVar.n;
        this.v = aVar.o;
        this.w = aVar.p;
        this.h = aVar.q;
        this.i = aVar.r;
        this.j = aVar.s;
        this.k = aVar.t;
        this.l = aVar.u;
        this.m = aVar.v;
        this.n = aVar.w;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CollectingFlags{easyCollectingEnabled=");
        sb.append(this.a);
        sb.append(", packageInfoCollectingEnabled=");
        sb.append(this.b);
        sb.append(", permissionsCollectingEnabled=");
        sb.append(this.c);
        sb.append(", featuresCollectingEnabled=");
        sb.append(this.d);
        sb.append(", sdkFingerprintingCollectingEnabled=");
        sb.append(this.e);
        sb.append(", identityLightCollectingEnabled=");
        sb.append(this.f);
        sb.append(", bleCollectingEnabled=");
        sb.append(this.g);
        sb.append(", locationCollectionEnabled=");
        sb.append(this.h);
        sb.append(", lbsCollectionEnabled=");
        sb.append(this.i);
        sb.append(", wakeupEnabled=");
        sb.append(this.j);
        sb.append(", gplCollectingEnabled=");
        sb.append(this.k);
        sb.append(", uiParsing=");
        sb.append(this.l);
        sb.append(", uiCollectingForBridge=");
        sb.append(this.m);
        sb.append(", uiEventSending=");
        sb.append(this.n);
        sb.append(", androidId=");
        sb.append(this.o);
        sb.append(", googleAid=");
        sb.append(this.p);
        sb.append(", wifiAround=");
        sb.append(this.q);
        sb.append(", wifiConnected=");
        sb.append(this.r);
        sb.append(", ownMacs=");
        sb.append(this.s);
        sb.append(", accessPoint=");
        sb.append(this.t);
        sb.append(", cellsAround=");
        sb.append(this.u);
        sb.append(", simInfo=");
        sb.append(this.v);
        sb.append(", simImei=");
        sb.append(this.w);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        xk xkVar = (xk) obj;
        if (this.a != xkVar.a || this.b != xkVar.b || this.c != xkVar.c || this.d != xkVar.d || this.e != xkVar.e || this.f != xkVar.f || this.g != xkVar.g || this.h != xkVar.h || this.i != xkVar.i || this.j != xkVar.j || this.k != xkVar.k || this.l != xkVar.l || this.m != xkVar.m || this.n != xkVar.n || this.o != xkVar.o || this.p != xkVar.p || this.q != xkVar.q || this.r != xkVar.r || this.s != xkVar.s || this.t != xkVar.t || this.u != xkVar.u || this.v != xkVar.v) {
            return false;
        }
        if (this.w != xkVar.w) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return ((((((((((((((((((((((((((((((((((((((((((((this.a ? 1 : 0) * true) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0)) * 31) + (this.j ? 1 : 0)) * 31) + (this.k ? 1 : 0)) * 31) + (this.l ? 1 : 0)) * 31) + (this.m ? 1 : 0)) * 31) + (this.n ? 1 : 0)) * 31) + (this.o ? 1 : 0)) * 31) + (this.p ? 1 : 0)) * 31) + (this.q ? 1 : 0)) * 31) + (this.r ? 1 : 0)) * 31) + (this.s ? 1 : 0)) * 31) + (this.t ? 1 : 0)) * 31) + (this.u ? 1 : 0)) * 31) + (this.v ? 1 : 0)) * 31) + (this.w ? 1 : 0);
    }
}
