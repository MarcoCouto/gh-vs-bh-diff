package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener.Error;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ck {
    private final boolean a;
    /* access modifiers changed from: private */
    public final cs b;
    /* access modifiers changed from: private */
    public final mn c;
    private String d;
    private Map<String, String> e;
    private DeferredDeeplinkParametersListener f;

    public ck(cs csVar, mn mnVar, @NonNull act act) {
        this(csVar, mnVar, new vw(csVar.b()), act);
    }

    @VisibleForTesting
    ck(cs csVar, mn mnVar, @NonNull final vw vwVar, @NonNull act act) {
        this.b = csVar;
        this.c = mnVar;
        this.d = mnVar.c();
        this.a = mnVar.d();
        if (this.a) {
            this.c.n(null);
            this.d = null;
        } else {
            e(b(this.d));
        }
        if (!this.c.e()) {
            act.a((Runnable) new Runnable() {
                public void run() {
                    vwVar.a(new vv() {
                        public void a(@NonNull vu vuVar) {
                            ck.this.b.a(vuVar);
                            ck.this.d(vuVar.a);
                            a();
                        }

                        public void a(@NonNull Throwable th) {
                            ck.this.b.a((vu) null);
                            a();
                        }

                        private void a() {
                            ck.this.c.g();
                        }
                    });
                }
            });
        }
    }

    public void a(String str) {
        this.b.a(str);
        d(str);
    }

    /* access modifiers changed from: private */
    public void d(@Nullable String str) {
        if (!(this.a || TextUtils.isEmpty(str)) && TextUtils.isEmpty(this.d)) {
            synchronized (this) {
                this.d = str;
                this.c.n(this.d);
                e(b(str));
                a();
            }
        }
    }

    private void e(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.e = c(str);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final String b(String str) {
        return (String) f(str).get("appmetrica_deep_link");
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final Map<String, String> c(String str) {
        Map f2 = f(Uri.decode(str));
        HashMap hashMap = new HashMap(f2.size());
        for (Entry entry : f2.entrySet()) {
            hashMap.put(Uri.decode((String) entry.getKey()), Uri.decode((String) entry.getValue()));
        }
        return hashMap;
    }

    private static Map<String, String> f(String str) {
        String[] split;
        HashMap hashMap = new HashMap();
        if (str != null) {
            String g = g(str);
            if (h(g)) {
                for (String str2 : g.split(RequestParameters.AMPERSAND)) {
                    int indexOf = str2.indexOf(RequestParameters.EQUAL);
                    if (indexOf >= 0) {
                        hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
                    } else {
                        hashMap.put(str2, "");
                    }
                }
            }
        }
        return hashMap;
    }

    private static String g(String str) {
        int lastIndexOf = str.lastIndexOf(63);
        return lastIndexOf >= 0 ? str.substring(lastIndexOf + 1) : str;
    }

    private static boolean h(String str) {
        return str.contains(RequestParameters.EQUAL);
    }

    private void a() {
        if (dl.a((Map) this.e)) {
            if (this.d != null) {
                a(Error.PARSE_ERROR);
            }
        } else if (this.f != null) {
            this.f.onParametersLoaded(this.e);
            this.f = null;
        }
    }

    private void a(Error error) {
        if (this.f != null) {
            this.f.onError(error, this.d);
            this.f = null;
        }
    }

    public synchronized void a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        try {
            this.f = deferredDeeplinkParametersListener;
            if (this.a) {
                a(Error.NOT_A_FIRST_LAUNCH);
            } else {
                a();
            }
            this.c.f();
        } catch (Throwable th) {
            this.c.f();
            throw th;
        }
    }
}
