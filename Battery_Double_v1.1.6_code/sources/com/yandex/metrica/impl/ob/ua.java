package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.uy.a.C0096a;

public abstract class ua {
    @NonNull
    private final tz a;

    public abstract C0096a a(@NonNull uq uqVar, @Nullable C0096a aVar, @NonNull ty tyVar);

    ua(@NonNull tz tzVar) {
        this.a = tzVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public tz a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@Nullable C0096a aVar) {
        return aVar == null || aVar.d.c;
    }
}
