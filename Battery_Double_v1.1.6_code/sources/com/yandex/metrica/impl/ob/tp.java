package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.metrica.i;
import com.yandex.metrica.impl.ob.uu.c;
import com.yandex.metrica.impl.ob.uu.c.e.a;
import com.yandex.metrica.impl.ob.uu.c.e.a.C0094a;
import com.yandex.metrica.impl.ob.uu.c.e.a.b;
import com.yandex.metrica.impl.ob.uu.c.e.a.b.C0095a;
import com.yandex.metrica.impl.ob.uu.d;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class tp {
    private static final ts a = new tv();
    private static final tx b = new tw();
    private static final tq c = new tu();
    private static final tn d = new tt();
    private static Map<ap, Integer> e;
    @NonNull
    private final ts f;
    @NonNull
    private final tx g;
    @NonNull
    private final tn h;
    @NonNull
    private final tq i;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(ap.FIRST_OCCURRENCE, Integer.valueOf(1));
        hashMap.put(ap.NON_FIRST_OCCURENCE, Integer.valueOf(0));
        hashMap.put(ap.UNKNOWN, Integer.valueOf(-1));
        e = Collections.unmodifiableMap(hashMap);
    }

    public tp() {
        this(a, b, d, c);
    }

    public tp(@NonNull ts tsVar) {
        this(tsVar, b, d, c);
    }

    public tp(@NonNull tx txVar) {
        this(a, txVar, d, c);
    }

    public tp(@NonNull tq tqVar) {
        this(a, b, d, tqVar);
    }

    public tp(@NonNull ts tsVar, @NonNull tx txVar) {
        this(tsVar, txVar, d, c);
    }

    public tp(@NonNull ts tsVar, @NonNull tx txVar, @NonNull tn tnVar, @NonNull tq tqVar) {
        this.f = tsVar;
        this.g = txVar;
        this.h = tnVar;
        this.i = tqVar;
    }

    @NonNull
    public a a(@NonNull to toVar) {
        a aVar = new a();
        b a2 = a(toVar.o, toVar.p, toVar.i, toVar.h, toVar.q);
        c.b b2 = b(toVar.g);
        C0094a a3 = a(toVar.m);
        if (a2 != null) {
            aVar.h = a2;
        }
        if (b2 != null) {
            aVar.g = b2;
        }
        String a4 = this.f.a(toVar.a);
        if (a4 != null) {
            aVar.e = a4;
        }
        aVar.f = this.g.a(toVar);
        if (toVar.l != null) {
            aVar.i = toVar.l;
        }
        if (a3 != null) {
            aVar.j = a3;
        }
        Integer a5 = this.i.a(toVar);
        if (a5 != null) {
            aVar.d = a5.intValue();
        }
        if (toVar.c != null) {
            aVar.b = (long) toVar.c.intValue();
        }
        if (toVar.d != null) {
            aVar.p = (long) toVar.d.intValue();
        }
        if (toVar.e != null) {
            aVar.q = (long) toVar.e.intValue();
        }
        if (toVar.f != null) {
            aVar.c = toVar.f.longValue();
        }
        if (toVar.n != null) {
            aVar.k = toVar.n.intValue();
        }
        aVar.l = this.h.a(toVar.s);
        aVar.m = f(toVar.g);
        if (toVar.r != null) {
            aVar.n = toVar.r.getBytes();
        }
        Integer num = null;
        if (toVar.t != null) {
            num = (Integer) e.get(toVar.t);
        }
        if (num != null) {
            aVar.o = num.intValue();
        }
        if (toVar.u != null) {
            aVar.r = ci.a(toVar.u);
        }
        int i2 = 3;
        if (toVar.v != null) {
            i2 = ci.a(toVar.v);
        }
        aVar.s = i2;
        return aVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public b a(@Nullable Integer num, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4) {
        b bVar = new b();
        if (num != null) {
            bVar.d = num.intValue();
        }
        if (str != null) {
            bVar.e = str;
        }
        uu.a[] d2 = d(str3);
        if (d2 != null) {
            bVar.b = d2;
        }
        if (!TextUtils.isEmpty(str2)) {
            bVar.c = c(str2);
        }
        if (!TextUtils.isEmpty(str4)) {
            bVar.f = e(str4);
        }
        return bVar;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public C0094a a(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                i a2 = abv.a(str);
                C0094a aVar = new C0094a();
                if (!TextUtils.isEmpty(a2.a())) {
                    aVar.b = a2.a();
                }
                if (!TextUtils.isEmpty(a2.b())) {
                    aVar.c = a2.b();
                }
                if (!dl.a(a2.c())) {
                    aVar.d = abc.b(a2.c());
                }
                return aVar;
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public c.b b(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                abc.a aVar = new abc.a(str);
                if (aVar.c("lon") && aVar.c("lat")) {
                    c.b bVar = new c.b();
                    try {
                        bVar.c = aVar.getDouble("lon");
                        bVar.b = aVar.getDouble("lat");
                        bVar.h = aVar.optInt(LocationConst.ALTITUDE);
                        bVar.f = aVar.optInt("direction");
                        bVar.e = aVar.optInt("precision");
                        bVar.g = aVar.optInt(LocationConst.SPEED);
                        bVar.d = TimeUnit.MILLISECONDS.toSeconds(aVar.optLong("timestamp"));
                        if (aVar.c("provider")) {
                            String a2 = aVar.a("provider");
                            if ("gps".equals(a2)) {
                                bVar.i = 1;
                            } else if ("network".equals(a2)) {
                                bVar.i = 2;
                            }
                        }
                        if (!aVar.c("original_provider")) {
                            return bVar;
                        }
                        bVar.j = aVar.a("original_provider");
                        return bVar;
                    } catch (Throwable unused) {
                        return bVar;
                    }
                }
            }
        } catch (Throwable unused2) {
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public d[] c(@NonNull String str) {
        try {
            return ci.a(new JSONArray(str));
        } catch (Throwable unused) {
            return new d[0];
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        return new com.yandex.metrica.impl.ob.uu.a[]{com.yandex.metrica.impl.ob.ci.a(new org.json.JSONObject(r4))};
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0012 */
    @Nullable
    @VisibleForTesting
    public uu.a[] d(String str) {
        if (!TextUtils.isEmpty(str)) {
            return ci.b(new JSONArray(str));
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public C0095a e(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                C0095a aVar = new C0095a();
                aVar.b = jSONObject.optString("ssid");
                switch (jSONObject.optInt("state", -1)) {
                    case 0:
                    case 1:
                    case 2:
                    case 4:
                        aVar.c = 1;
                        break;
                    case 3:
                        aVar.c = 2;
                        break;
                }
                return aVar;
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int f(@Nullable String str) {
        if (str != null) {
            try {
                return new lg().a(Boolean.valueOf(new abc.a(str).getBoolean(String.ENABLED))).intValue();
            } catch (Throwable unused) {
            }
        }
        return -1;
    }
}
