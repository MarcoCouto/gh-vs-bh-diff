package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class km {
    @NonNull
    private final Context a;
    @NonNull
    private final kk b;
    /* access modifiers changed from: private */
    @NonNull
    public final kl c;
    @NonNull
    private final a d;
    @NonNull
    private final kh e;

    public static class a {
        public yb a(@NonNull Context context) {
            return (yb) com.yandex.metrica.impl.ob.op.a.a(yb.class).a(context).a();
        }
    }

    public km(@NonNull Context context, @NonNull act act, @NonNull kg kgVar) {
        this(context, act, kgVar, new kl(context));
    }

    public void a() {
        a(this.d.a(this.a));
    }

    private void a(@NonNull yb ybVar) {
        if (ybVar.q != null) {
            boolean z = ybVar.q.b;
            Long a2 = this.e.a(ybVar.q.c);
            if (!ybVar.o.j || a2 == null || a2.longValue() <= 0) {
                b();
            } else {
                this.b.a(a2.longValue(), z);
            }
        }
    }

    private void b() {
        this.b.a();
    }

    public void a(@Nullable final kn knVar) {
        yb a2 = this.d.a(this.a);
        if (a2.q != null) {
            long j = a2.q.a;
            if (j > 0) {
                this.c.a(this.a.getPackageName());
                this.b.a(j, (com.yandex.metrica.impl.ob.kk.a) new com.yandex.metrica.impl.ob.kk.a() {
                    public void a() {
                        km.this.c.a();
                        km.this.b(knVar);
                    }
                });
            } else {
                b(knVar);
            }
        } else {
            b(knVar);
        }
        a(a2);
    }

    /* access modifiers changed from: private */
    public void b(@Nullable kn knVar) {
        if (knVar != null) {
            knVar.a();
        }
    }

    private km(@NonNull Context context, @NonNull act act, @NonNull kg kgVar, @NonNull kl klVar) {
        this(context, new kk(act, kgVar), klVar, new a(), new kh(context));
    }

    @VisibleForTesting
    km(@NonNull Context context, @NonNull kk kkVar, @NonNull kl klVar, @NonNull a aVar, @NonNull kh khVar) {
        this.a = context;
        this.b = kkVar;
        this.c = klVar;
        this.d = aVar;
        this.e = khVar;
    }
}
