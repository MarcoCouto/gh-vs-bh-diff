package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.profile.UserProfile;
import java.util.Map;

public class vp implements IReporter {
    static final adw<String> a = new ads(new adq("Event name"));
    static final adw<String> b = new ads(new adq("Error message"));
    static final adw<Throwable> c = new ads(new adr("Unhandled exception"));
    static final adw<UserProfile> d = new ads(new adr("User profile"));
    static final adw<Revenue> e = new ads(new adr("Revenue"));

    public void pauseSession() {
    }

    public void resumeSession() {
    }

    public void sendEventsBuffer() {
    }

    public void setStatisticsSending(boolean z) {
    }

    public void setUserProfileID(@Nullable String str) {
    }

    public void reportEvent(@NonNull String str) throws adt {
        a.a(str);
    }

    public void reportEvent(@NonNull String str, @Nullable String str2) throws adt {
        a.a(str);
    }

    public void reportEvent(@NonNull String str, @Nullable Map<String, Object> map) throws adt {
        a.a(str);
    }

    public void reportError(@NonNull String str, @Nullable Throwable th) throws adt {
        b.a(str);
    }

    public void reportUnhandledException(@NonNull Throwable th) throws adt {
        c.a(th);
    }

    public void reportUserProfile(@NonNull UserProfile userProfile) throws adt {
        d.a(userProfile);
    }

    public void reportRevenue(@NonNull Revenue revenue) throws adt {
        e.a(revenue);
    }
}
