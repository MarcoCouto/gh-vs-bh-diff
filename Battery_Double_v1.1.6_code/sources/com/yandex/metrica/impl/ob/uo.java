package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.uy.a.C0096a;

public class uo extends ud<String> {
    private final adn<String> a;

    public uo(@NonNull String str, @NonNull String str2, @NonNull adn<String> adn, @NonNull adw<String> adw, @NonNull ua uaVar) {
        super(0, str, str2, adw, uaVar);
        this.a = adn;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0096a aVar) {
        String str = (String) this.a.a(b());
        aVar.e.b = str == null ? new byte[0] : str.getBytes();
    }
}
