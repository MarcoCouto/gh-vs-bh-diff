package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.google.android.exoplayer2.offline.DownloadService;
import java.util.LinkedList;
import java.util.List;

public class fd {
    private final fe a;
    private final sy b;
    private List<g> c;

    public static class a {
        @NonNull
        private final fe a;

        public a(@NonNull fe feVar) {
            this.a = feVar;
        }

        /* access modifiers changed from: 0000 */
        public fd a(@NonNull sy syVar) {
            return new fd(this.a, syVar);
        }
    }

    static class b extends g {
        private final tc a;
        private final mo b;
        private final mq c;

        b(fe feVar) {
            super(feVar);
            this.a = new tc(feVar.k(), feVar.c().toString());
            this.b = feVar.y();
            this.c = feVar.a;
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return this.a.e();
        }

        /* access modifiers changed from: protected */
        public void b() {
            d();
            c();
            g();
            this.a.g();
        }

        private void g() {
            com.yandex.metrica.impl.ob.i.a a2 = this.a.a();
            if (a2 != null) {
                this.b.a(a2);
            }
            String a3 = this.a.a((String) null);
            if (!TextUtils.isEmpty(a3) && TextUtils.isEmpty(this.b.f())) {
                this.b.a(a3);
            }
            long c2 = this.a.c(Long.MIN_VALUE);
            if (c2 != Long.MIN_VALUE && this.b.a(Long.MIN_VALUE) == Long.MIN_VALUE) {
                this.b.b(c2);
            }
            this.b.q();
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void c() {
            jx jxVar = new jx(this.b, DownloadService.KEY_FOREGROUND);
            if (!jxVar.i()) {
                long d = this.a.d(-1);
                if (-1 != d) {
                    jxVar.d(d);
                }
                boolean booleanValue = this.a.a(true).booleanValue();
                if (booleanValue) {
                    jxVar.a(booleanValue);
                }
                long a2 = this.a.a(Long.MIN_VALUE);
                if (a2 != Long.MIN_VALUE) {
                    jxVar.e(a2);
                }
                long f = this.a.f(0);
                if (f != 0) {
                    jxVar.a(f);
                }
                long h = this.a.h(0);
                if (h != 0) {
                    jxVar.b(h);
                }
                jxVar.h();
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void d() {
            jx jxVar = new jx(this.b, "background");
            if (!jxVar.i()) {
                long e = this.a.e(-1);
                if (e != -1) {
                    jxVar.d(e);
                }
                long b2 = this.a.b(Long.MIN_VALUE);
                if (b2 != Long.MIN_VALUE) {
                    jxVar.e(b2);
                }
                long g = this.a.g(0);
                if (g != 0) {
                    jxVar.a(g);
                }
                long i = this.a.i(0);
                if (i != 0) {
                    jxVar.b(i);
                }
                jxVar.h();
            }
        }
    }

    static class c extends h {
        c(fe feVar, sy syVar) {
            super(feVar, syVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return e() instanceof fq;
        }

        /* access modifiers changed from: protected */
        public void b() {
            c().a();
        }
    }

    static class d extends g {
        private final sz a;
        private final mm b;

        d(fe feVar, sz szVar) {
            super(feVar);
            this.a = szVar;
            this.b = feVar.u();
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return "DONE".equals(this.a.c(null)) || "DONE".equals(this.a.b(null));
        }

        /* access modifiers changed from: protected */
        public void b() {
            if ("DONE".equals(this.a.c(null))) {
                this.b.b();
            }
            String e = this.a.e(null);
            if (!TextUtils.isEmpty(e)) {
                this.b.c(e);
            }
            if ("DONE".equals(this.a.b(null))) {
                this.b.a();
            }
            this.a.d();
            this.a.e();
            this.a.c();
        }
    }

    static class e extends h {
        e(fe feVar, sy syVar) {
            super(feVar, syVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return e().u().a((String) null) == null;
        }

        /* access modifiers changed from: protected */
        public void b() {
            sy c = c();
            if (e() instanceof fq) {
                c.c();
            } else {
                c.b();
            }
        }
    }

    static class f extends g {
        @Deprecated
        static final th a = new th("SESSION_SLEEP_START");
        @Deprecated
        static final th b = new th("SESSION_ID");
        @Deprecated
        static final th c = new th("SESSION_COUNTER_ID");
        @Deprecated
        static final th d = new th("SESSION_INIT_TIME");
        @Deprecated
        static final th e = new th("SESSION_IS_ALIVE_REPORT_NEEDED");
        @Deprecated
        static final th f = new th("BG_SESSION_ID");
        @Deprecated
        static final th g = new th("BG_SESSION_SLEEP_START");
        @Deprecated
        static final th h = new th("BG_SESSION_COUNTER_ID");
        @Deprecated
        static final th i = new th("BG_SESSION_INIT_TIME");
        @Deprecated
        static final th j = new th("BG_SESSION_IS_ALIVE_REPORT_NEEDED");
        private final mo k;

        /* access modifiers changed from: protected */
        public boolean a() {
            return true;
        }

        f(fe feVar) {
            super(feVar);
            this.k = feVar.y();
        }

        /* access modifiers changed from: protected */
        public void b() {
            d();
            c();
            g();
        }

        private void g() {
            this.k.r(a.b());
            this.k.r(b.b());
            this.k.r(c.b());
            this.k.r(d.b());
            this.k.r(e.b());
            this.k.r(f.b());
            this.k.r(g.b());
            this.k.r(h.b());
            this.k.r(i.b());
            this.k.r(j.b());
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void c() {
            long b2 = this.k.b(a.b(), -2147483648L);
            if (b2 != -2147483648L) {
                jx jxVar = new jx(this.k, DownloadService.KEY_FOREGROUND);
                if (!jxVar.i()) {
                    if (b2 != 0) {
                        jxVar.b(b2);
                    }
                    long b3 = this.k.b(b.b(), -1);
                    if (-1 != b3) {
                        jxVar.d(b3);
                    }
                    boolean b4 = this.k.b(e.b(), true);
                    if (b4) {
                        jxVar.a(b4);
                    }
                    long b5 = this.k.b(d.b(), Long.MIN_VALUE);
                    if (b5 != Long.MIN_VALUE) {
                        jxVar.e(b5);
                    }
                    long b6 = this.k.b(c.b(), 0);
                    if (b6 != 0) {
                        jxVar.a(b6);
                    }
                    jxVar.h();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void d() {
            long b2 = this.k.b(g.b(), -2147483648L);
            if (b2 != -2147483648L) {
                jx jxVar = new jx(this.k, "background");
                if (!jxVar.i()) {
                    if (b2 != 0) {
                        jxVar.b(b2);
                    }
                    long b3 = this.k.b(f.b(), -1);
                    if (b3 != -1) {
                        jxVar.d(b3);
                    }
                    boolean b4 = this.k.b(j.b(), true);
                    if (b4) {
                        jxVar.a(b4);
                    }
                    long b5 = this.k.b(i.b(), Long.MIN_VALUE);
                    if (b5 != Long.MIN_VALUE) {
                        jxVar.e(b5);
                    }
                    long b6 = this.k.b(h.b(), 0);
                    if (b6 != 0) {
                        jxVar.a(b6);
                    }
                    jxVar.h();
                }
            }
        }
    }

    private static abstract class g {
        private final fe a;

        /* access modifiers changed from: protected */
        public abstract boolean a();

        /* access modifiers changed from: protected */
        public abstract void b();

        g(fe feVar) {
            this.a = feVar;
        }

        /* access modifiers changed from: 0000 */
        public fe e() {
            return this.a;
        }

        /* access modifiers changed from: 0000 */
        public void f() {
            if (a()) {
                b();
            }
        }
    }

    private static abstract class h extends g {
        private sy a;

        h(fe feVar, sy syVar) {
            super(feVar);
            this.a = syVar;
        }

        public sy c() {
            return this.a;
        }
    }

    private fd(fe feVar, sy syVar) {
        this.a = feVar;
        this.b = syVar;
        b();
    }

    private void b() {
        this.c = new LinkedList();
        this.c.add(new c(this.a, this.b));
        this.c.add(new e(this.a, this.b));
        this.c.add(new d(this.a, this.a.v()));
        this.c.add(new b(this.a));
        this.c.add(new f(this.a));
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (!a(this.a.c().a())) {
            for (g f2 : this.c) {
                f2.f();
            }
        }
    }

    private boolean a(String str) {
        return sy.a.values().contains(str);
    }
}
