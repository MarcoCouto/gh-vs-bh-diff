package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class pt {
    @NonNull
    public final rk a;
    @Nullable
    public final ps b;

    public pt(@NonNull rk rkVar, @Nullable ps psVar) {
        this.a = rkVar;
        this.b = psVar;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ForcedCollectingConfig{providerAccessFlags=");
        sb.append(this.a);
        sb.append(", arguments=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        pt ptVar = (pt) obj;
        if (!this.a.equals(ptVar.a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(ptVar.b);
        } else if (ptVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0);
    }
}
