package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.us.d;
import java.util.List;

public class lh implements nq<StackTraceElement, d> {
    @NonNull
    private li a = new li();

    @NonNull
    /* renamed from: a */
    public d[] b(@NonNull List<StackTraceElement> list) {
        d[] dVarArr = new d[list.size()];
        int i = 0;
        for (StackTraceElement a2 : list) {
            dVarArr[i] = this.a.b(a2);
            i++;
        }
        return dVarArr;
    }

    @NonNull
    public List<StackTraceElement> a(d[] dVarArr) {
        throw new UnsupportedOperationException();
    }
}
