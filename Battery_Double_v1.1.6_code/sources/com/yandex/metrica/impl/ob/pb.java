package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

public final class pb {
    private final String a;
    private final int b;
    private final boolean c;

    public pb(@NonNull JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.getString("name");
        this.c = jSONObject.getBoolean("required");
        this.b = jSONObject.optInt("version", -1);
    }

    public pb(String str, int i, boolean z) {
        this.a = str;
        this.b = i;
        this.c = z;
    }

    public pb(String str, boolean z) {
        this(str, -1, z);
    }

    public JSONObject a() throws JSONException {
        JSONObject put = new JSONObject().put("name", this.a).put("required", this.c);
        if (this.b != -1) {
            put.put("version", this.b);
        }
        return put;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        pb pbVar = (pb) obj;
        if (this.b != pbVar.b || this.c != pbVar.c) {
            return false;
        }
        if (this.a != null) {
            z = this.a.equals(pbVar.a);
        } else if (pbVar.a != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return ((((this.a != null ? this.a.hashCode() : 0) * 31) + this.b) * 31) + (this.c ? 1 : 0);
    }
}
