package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class lg {
    @NonNull
    public Integer a(@Nullable Boolean bool) {
        if (Boolean.TRUE.equals(bool)) {
            return Integer.valueOf(1);
        }
        if (Boolean.FALSE.equals(bool)) {
            return Integer.valueOf(0);
        }
        return Integer.valueOf(-1);
    }
}
