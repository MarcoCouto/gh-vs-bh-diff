package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.f;

public class cq {
    @NonNull
    private String a;
    @NonNull
    private cp b;

    public cq(@NonNull cp cpVar, @NonNull String str) {
        this.b = cpVar;
        this.a = str;
    }

    @NonNull
    public ay a() {
        return this.b.b(f.a(this.a).b());
    }
}
