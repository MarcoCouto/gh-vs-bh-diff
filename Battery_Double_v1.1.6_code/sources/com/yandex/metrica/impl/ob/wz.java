package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.profile.UserProfile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class wz implements ay {
    @NonNull
    private final List<ww> a;
    @Nullable
    private volatile ay b;
    @NonNull
    private final vj c;

    public wz() {
        this(vj.a());
    }

    @VisibleForTesting
    wz(@NonNull vj vjVar) {
        this.a = new ArrayList();
        this.c = vjVar;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull Context context) {
        this.b = this.c.a(context, "20799a27-fa80-4b36-b2db-0f8141f24180");
        for (ww a2 : this.a) {
            a2.a(this.b);
        }
        this.a.clear();
    }

    public void a(@NonNull final ld ldVar) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.a(ldVar);
            }
        });
    }

    public void a(@NonNull final ky kyVar) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.a(kyVar);
            }
        });
    }

    public void e() {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.e();
            }
        });
    }

    public void a(@Nullable final String str, @Nullable final String str2) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.a(str, str2);
            }
        });
    }

    public void b(@NonNull final String str, @Nullable final String str2) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.b(str, str2);
            }
        });
    }

    public void sendEventsBuffer() {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.sendEventsBuffer();
            }
        });
    }

    public void reportEvent(@NonNull final String str) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportEvent(str);
            }
        });
    }

    public void reportEvent(@NonNull final String str, @Nullable final String str2) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportEvent(str, str2);
            }
        });
    }

    public void reportEvent(@NonNull final String str, @Nullable final Map<String, Object> map) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportEvent(str, map);
            }
        });
    }

    public void reportError(@NonNull final String str, @Nullable final Throwable th) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportError(str, th);
            }
        });
    }

    public void reportUnhandledException(@NonNull final Throwable th) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportUnhandledException(th);
            }
        });
    }

    public void resumeSession() {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.resumeSession();
            }
        });
    }

    public void pauseSession() {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.pauseSession();
            }
        });
    }

    public void setUserProfileID(@Nullable final String str) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.setUserProfileID(str);
            }
        });
    }

    public void reportUserProfile(@NonNull final UserProfile userProfile) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportUserProfile(userProfile);
            }
        });
    }

    public void reportRevenue(@NonNull final Revenue revenue) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.reportRevenue(revenue);
            }
        });
    }

    public void setStatisticsSending(final boolean z) {
        a((ww) new ww() {
            public void a(@NonNull ay ayVar) {
                ayVar.setStatisticsSending(z);
            }
        });
    }

    private synchronized void a(@NonNull ww wwVar) {
        if (this.b == null) {
            this.a.add(wwVar);
        } else {
            wwVar.a(this.b);
        }
    }
}
