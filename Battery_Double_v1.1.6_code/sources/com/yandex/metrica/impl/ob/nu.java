package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.m;
import com.yandex.metrica.impl.ob.ve.a.m.C0110a;
import java.util.List;

public class nu implements np<rr, m> {
    @NonNull
    private final no a;

    public nu() {
        this(new no());
    }

    @NonNull
    /* renamed from: a */
    public m b(@NonNull rr rrVar) {
        C0110a[] aVarArr;
        m mVar = new m();
        mVar.b = rrVar.a;
        mVar.c = rrVar.b;
        if (rrVar.c == null) {
            aVarArr = new C0110a[0];
        } else {
            aVarArr = this.a.b(rrVar.c);
        }
        mVar.d = aVarArr;
        return mVar;
    }

    @NonNull
    public rr a(@NonNull m mVar) {
        List list;
        long j = mVar.b;
        boolean z = mVar.c;
        if (dl.a((T[]) mVar.d)) {
            list = null;
        } else {
            list = this.a.a(mVar.d);
        }
        return new rr(j, z, list);
    }

    @VisibleForTesting
    nu(@NonNull no noVar) {
        this.a = noVar;
    }
}
