package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a.b;

public class nm implements np<py, b> {
    @NonNull
    private final nl a;

    public nm() {
        this(new nl());
    }

    @VisibleForTesting
    nm(@NonNull nl nlVar) {
        this.a = nlVar;
    }

    @NonNull
    /* renamed from: a */
    public b b(@NonNull py pyVar) {
        b bVar = new b();
        bVar.b = pyVar.a.a;
        bVar.c = pyVar.a.b;
        if (pyVar.b != null) {
            bVar.d = this.a.b(pyVar.b);
        }
        return bVar;
    }

    @NonNull
    public py a(@NonNull b bVar) {
        return new py(new rk(bVar.b, bVar.c), bVar.d != null ? this.a.a(bVar.d) : null);
    }
}
