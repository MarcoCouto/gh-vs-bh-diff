package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public class sk {
    @NonNull
    public final List<sr> a;
    @Nullable
    public final m b;
    @NonNull
    public final List<String> c;

    public sk(@NonNull List<sr> list, @Nullable m mVar, @NonNull List<String> list2) {
        this.a = list;
        this.b = mVar;
        this.c = list2;
    }
}
