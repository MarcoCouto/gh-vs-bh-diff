package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.abc.a;
import java.util.Collection;
import java.util.List;
import org.json.JSONObject;

public class yl {
    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        try {
            JSONObject optJSONObject = ((JSONObject) aVar.a("query_hosts", new JSONObject())).optJSONObject("list");
            if (optJSONObject != null) {
                String a = a(optJSONObject, "get_ad");
                if (!TextUtils.isEmpty(a)) {
                    yxVar.b(a);
                }
                List b = b(optJSONObject, "report");
                if (!dl.a((Collection) b)) {
                    yxVar.b(b);
                }
                String a2 = a(optJSONObject, "report_ad");
                if (!TextUtils.isEmpty(a2)) {
                    yxVar.c(a2);
                }
                List b2 = b(optJSONObject, "location");
                if (!dl.a((Collection) b2)) {
                    yxVar.c(b2);
                }
                List b3 = b(optJSONObject, "startup");
                if (!dl.a((Collection) b3)) {
                    yxVar.a(b3);
                }
                List b4 = b(optJSONObject, "diagnostic");
                if (!dl.a((Collection) b4)) {
                    yxVar.e(b4);
                }
            }
        } catch (Throwable unused) {
        }
    }

    private String a(JSONObject jSONObject, String str) {
        try {
            return jSONObject.getJSONObject(str).getJSONArray("urls").getString(0);
        } catch (Throwable unused) {
            return "";
        }
    }

    private List<String> b(JSONObject jSONObject, String str) {
        try {
            return abc.a(jSONObject.getJSONObject(str).getJSONArray("urls"));
        } catch (Throwable unused) {
            return null;
        }
    }
}
