package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.pc.a;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class wg {
    public static final Map<a, String> a;
    public static final Map<a, String> b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(a.GOOGLE, "adv_id");
        hashMap.put(a.HMS, "oaid");
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(a.GOOGLE, "limit_ad_tracking");
        hashMap2.put(a.HMS, "limit_oaid_tracking");
        b = Collections.unmodifiableMap(hashMap2);
    }
}
