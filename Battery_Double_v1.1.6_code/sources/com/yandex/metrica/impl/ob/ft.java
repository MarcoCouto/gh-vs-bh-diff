package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ew.a;
import com.yandex.metrica.impl.ob.fk;
import com.yandex.metrica.impl.ob.fn;
import java.util.ArrayList;
import java.util.List;

public class ft<COMPONENT extends fn & fk> implements fg, fm, xy {
    @NonNull
    private final Context a;
    @NonNull
    private final fb b;
    @NonNull
    private final gk<COMPONENT> c;
    @NonNull
    private final yc d;
    @NonNull
    private final fy e;
    @Nullable
    private COMPONENT f;
    @Nullable
    private fl g;
    private List<xy> h;
    @NonNull
    private final fc<gg> i;

    public ft(@NonNull Context context, @NonNull fb fbVar, @NonNull ew ewVar, @NonNull gk<COMPONENT> gkVar) {
        this(context, fbVar, ewVar, new fy(ewVar.b), gkVar, new fc(), xt.a());
    }

    public ft(@NonNull Context context, @NonNull fb fbVar, @NonNull ew ewVar, @NonNull fy fyVar, @NonNull gk<COMPONENT> gkVar, @NonNull fc<gg> fcVar, @NonNull xt xtVar) {
        this.h = new ArrayList();
        this.a = context;
        this.b = fbVar;
        this.e = fyVar;
        this.c = gkVar;
        this.i = fcVar;
        this.d = xtVar.a(this.a, a(), this, ewVar.a);
    }

    public void a(@NonNull aa aaVar, @NonNull ew ewVar) {
        fn fnVar;
        c();
        if (al.d(aaVar.g())) {
            fnVar = e();
        } else {
            fnVar = d();
        }
        if (!al.a(aaVar.g())) {
            a(ewVar.b);
        }
        fnVar.a(aaVar);
    }

    private void c() {
        e().a();
    }

    public synchronized void a(@NonNull a aVar) {
        this.e.a(aVar);
        if (this.g != null) {
            this.g.a(aVar);
        }
        if (this.f != null) {
            ((fk) this.f).a(aVar);
        }
    }

    public synchronized void a(@NonNull gg ggVar) {
        this.i.a(ggVar);
    }

    public synchronized void b(@NonNull gg ggVar) {
        this.i.b(ggVar);
    }

    private COMPONENT d() {
        if (this.f == null) {
            synchronized (this) {
                this.f = this.c.d(this.a, this.b, this.e.a(), this.d);
                this.h.add(this.f);
            }
        }
        return this.f;
    }

    private fl e() {
        if (this.g == null) {
            synchronized (this) {
                this.g = this.c.c(this.a, this.b, this.e.a(), this.d);
                this.h.add(this.g);
            }
        }
        return this.g;
    }

    @VisibleForTesting
    @NonNull
    public final fb a() {
        return this.b;
    }

    public synchronized void a(@Nullable yb ybVar) {
        for (xy a2 : this.h) {
            a2.a(ybVar);
        }
    }

    public synchronized void a(@NonNull xv xvVar, @Nullable yb ybVar) {
        for (xy a2 : this.h) {
            a2.a(xvVar, ybVar);
        }
    }

    public void b() {
        if (this.f != null) {
            ((fg) this.f).b();
        }
        if (this.g != null) {
            this.g.b();
        }
    }

    public void a(@NonNull ew ewVar) {
        this.d.a(ewVar.a);
        a(ewVar.b);
    }
}
