package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.CounterConfiguration.a;

public class cy {
    @NonNull
    private final String a;
    @NonNull
    private final Context b;
    @Nullable
    private final a c;
    @NonNull
    private final da d;

    public cy(@NonNull String str, @NonNull Context context, @NonNull a aVar, @NonNull da daVar) {
        this.a = str;
        this.b = context;
        switch (aVar) {
            case MAIN:
                this.c = a.SELF_DIAGNOSTIC_MAIN;
                break;
            case MANUAL:
                this.c = a.SELF_DIAGNOSTIC_MANUAL;
                break;
            default:
                this.c = null;
                break;
        }
        this.d = daVar;
    }

    public void a(@NonNull aa aaVar) {
        if (this.c != null) {
            try {
                CounterConfiguration counterConfiguration = new CounterConfiguration(this.a);
                counterConfiguration.a(this.c);
                this.d.a(aaVar.a(new co(new eu(this.b, (ResultReceiver) null), counterConfiguration).b()));
            } catch (Throwable unused) {
            }
        }
    }
}
