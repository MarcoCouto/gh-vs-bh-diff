package com.yandex.metrica.impl.ob;

import android.os.Handler;
import java.lang.ref.WeakReference;

class bg implements Runnable {
    private final WeakReference<Handler> a;
    private final WeakReference<o> b;

    bg(Handler handler, o oVar) {
        this.a = new WeakReference<>(handler);
        this.b = new WeakReference<>(oVar);
    }

    public void run() {
        Handler handler = (Handler) this.a.get();
        o oVar = (o) this.b.get();
        if (handler != null && oVar != null && oVar.c()) {
            bf.a(handler, oVar, this);
        }
    }
}
