package com.yandex.metrica.impl.ob;

public class rk {
    public final boolean a;
    public final boolean b;

    public rk(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ProviderAccessFlags{lastKnownEnabled=");
        sb.append(this.a);
        sb.append(", scanningEnabled=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        rk rkVar = (rk) obj;
        if (this.a != rkVar.a) {
            return false;
        }
        if (this.b != rkVar.b) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return ((this.a ? 1 : 0) * true) + (this.b ? 1 : 0);
    }
}
