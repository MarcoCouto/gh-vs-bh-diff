package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.vt.a;

public class fa {
    /* access modifiers changed from: 0000 */
    @NonNull
    public hl<hk, ey> a(@NonNull ey eyVar) {
        return new hl<>(new hd(eyVar), eyVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public di<ey> a(@NonNull ey eyVar, @NonNull yc ycVar) {
        return new di<>(eyVar, new yg(ycVar));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public a a(@NonNull final yc ycVar) {
        return new a() {
            public boolean a(@NonNull vu vuVar, @NonNull bd bdVar) {
                if (!TextUtils.isEmpty(vuVar.a)) {
                    ycVar.a(vuVar.a, bdVar);
                }
                return false;
            }
        };
    }
}
