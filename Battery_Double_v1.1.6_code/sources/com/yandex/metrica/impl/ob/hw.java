package com.yandex.metrica.impl.ob;

import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.pa.a;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class hw extends hu {
    @NonNull
    private final ado a;

    public hw(fe feVar) {
        this(feVar, new ado());
    }

    @VisibleForTesting
    public hw(fe feVar, @NonNull ado ado) {
        super(feVar);
        this.a = ado;
    }

    public boolean a(@NonNull aa aaVar) {
        fe a2 = a();
        if (a2.u().d() && a2.t()) {
            mo y = a2.y();
            HashSet b = b();
            try {
                ArrayList c = c();
                if (aav.a(b, c)) {
                    a2.m();
                } else {
                    JSONArray jSONArray = new JSONArray();
                    Iterator it = c.iterator();
                    while (it.hasNext()) {
                        jSONArray.put(((pb) it.next()).a());
                    }
                    a2.e().d(aa.a(aaVar, new JSONObject().put(SettingsJsonConstants.FEATURES_KEY, jSONArray).toString()));
                    y.b(jSONArray.toString());
                }
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public HashSet<pb> b() {
        String e = a().y().e();
        if (TextUtils.isEmpty(e)) {
            return null;
        }
        try {
            HashSet<pb> hashSet = new HashSet<>();
            JSONArray jSONArray = new JSONArray(e);
            for (int i = 0; i < jSONArray.length(); i++) {
                hashSet.add(new pb(jSONArray.getJSONObject(i)));
            }
            return hashSet;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public ArrayList<pb> c() {
        try {
            fe a2 = a();
            PackageInfo a3 = this.a.a(a2.k(), a2.k().getPackageName(), 16384);
            ArrayList<pb> arrayList = new ArrayList<>();
            pa a4 = a.a();
            if (!(a3 == null || a3.reqFeatures == null)) {
                for (FeatureInfo b : a3.reqFeatures) {
                    arrayList.add(a4.b(b));
                }
            }
            return arrayList;
        } catch (Throwable unused) {
            return null;
        }
    }
}
