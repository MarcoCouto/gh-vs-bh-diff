package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a.C0106a;

public class nk implements np<pt, C0106a> {
    @NonNull
    private final nj a;

    public nk() {
        this(new nj());
    }

    @VisibleForTesting
    nk(@NonNull nj njVar) {
        this.a = njVar;
    }

    @NonNull
    /* renamed from: a */
    public C0106a b(@NonNull pt ptVar) {
        C0106a aVar = new C0106a();
        aVar.b = ptVar.a.a;
        aVar.c = ptVar.a.b;
        if (ptVar.b != null) {
            aVar.d = this.a.b(ptVar.b);
        }
        return aVar;
    }

    @NonNull
    public pt a(@NonNull C0106a aVar) {
        return new pt(new rk(aVar.b, aVar.c), aVar.d != null ? this.a.a(aVar.d) : null);
    }
}
