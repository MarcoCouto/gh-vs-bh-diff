package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class bw {
    @NonNull
    private final fq a;

    public bw(@NonNull fq fqVar) {
        this.a = fqVar;
    }

    public void a(@NonNull String str) {
        this.a.a(al.b(str, abl.h()));
    }

    public void b(@NonNull String str) {
        this.a.a(al.a(str, abl.h()));
    }
}
