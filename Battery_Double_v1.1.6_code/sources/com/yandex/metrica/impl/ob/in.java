package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.al.a;
import java.util.List;

public abstract class in<BaseHandler> {
    private final ix a;

    public abstract void a(a aVar, @NonNull List<BaseHandler> list);

    public in(ix ixVar) {
        this.a = ixVar;
    }

    public ix a() {
        return this.a;
    }
}
