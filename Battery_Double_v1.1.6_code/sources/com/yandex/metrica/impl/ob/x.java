package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class x {
    private static final Object j = new Object();
    @SuppressLint({"StaticFieldLeak"})
    private static volatile x k;
    @NonNull
    public final String a = "android";
    public final String b = Build.MANUFACTURER;
    public final String c = Build.MODEL;
    public final String d = VERSION.RELEASE;
    public final int e = VERSION.SDK_INT;
    @NonNull
    public final b f;
    @NonNull
    public final String g;
    @NonNull
    public final String h;
    @NonNull
    public final List<String> i;
    @NonNull
    private final a l;

    public static class a {
        @Nullable
        private String a;
        @NonNull
        private Context b;
        /* access modifiers changed from: private */
        public yb c;

        a(@NonNull Context context) {
            this.b = context;
            eh.a().b((ej) new en(this.a));
            eh.a().a(this, er.class, el.a((ek<T>) new ek<er>() {
                public void a(er erVar) {
                    synchronized (a.this) {
                        a.this.c = erVar.b;
                    }
                }
            }).a());
            this.a = b(this.c) ? a(context) : null;
        }

        @Nullable
        @SuppressLint({"HardwareIds"})
        private String a(@NonNull Context context) {
            try {
                return Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            } catch (Throwable unused) {
                return null;
            }
        }

        @Nullable
        public String a(@NonNull yb ybVar) {
            if (TextUtils.isEmpty(this.a) && b(ybVar)) {
                this.a = a(this.b);
            }
            return this.a;
        }

        private synchronized boolean b(@NonNull yb ybVar) {
            if (ybVar == null) {
                ybVar = this.c;
            }
            return c(ybVar);
        }

        private boolean c(@NonNull yb ybVar) {
            return ybVar != null && ybVar.o.o;
        }
    }

    public static final class b {
        public final int a;
        public final int b;
        public final int c;
        public final float d;

        b(@NonNull Point point, int i, float f) {
            this.a = Math.max(point.x, point.y);
            this.b = Math.min(point.x, point.y);
            this.c = i;
            this.d = f;
        }
    }

    public static x a(@NonNull Context context) {
        if (k == null) {
            synchronized (j) {
                if (k == null) {
                    k = new x(context.getApplicationContext());
                }
            }
        }
        return k;
    }

    private x(@NonNull Context context) {
        this.l = new a(context);
        this.f = new b(cg.b(context), context.getResources().getDisplayMetrics().densityDpi, context.getResources().getDisplayMetrics().density);
        this.g = cg.a(context).name().toLowerCase(Locale.US);
        this.h = String.valueOf(com.yandex.metrica.impl.ob.cg.b.c());
        this.i = Collections.unmodifiableList(new ArrayList<String>() {
            {
                if (com.yandex.metrica.impl.ob.cg.b.a()) {
                    add("Superuser.apk");
                }
                if (com.yandex.metrica.impl.ob.cg.b.b()) {
                    add("su.so");
                }
            }
        });
    }

    @Nullable
    public String a() {
        return this.l.a((yb) null);
    }

    @Nullable
    public String a(@NonNull yb ybVar) {
        return this.l.a(ybVar);
    }
}
