package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.IReporter;
import java.util.HashMap;
import java.util.Map;

public class hv extends hu {
    @NonNull
    private final IReporter a;

    public hv(@NonNull fe feVar, @NonNull IReporter iReporter) {
        super(feVar);
        this.a = iReporter;
    }

    public boolean a(@NonNull aa aaVar) {
        kv a2 = kv.a(aaVar.g());
        HashMap hashMap = new HashMap();
        hashMap.put("type", a2.a);
        hashMap.put("delivery_method", a2.b);
        this.a.reportEvent("crash_saved", (Map<String, Object>) hashMap);
        return false;
    }
}
