package com.yandex.metrica.impl.ob;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.ironsource.sdk.constants.Constants.ControllerParameters;
import com.tapjoy.TapjoyConstants;
import java.io.IOException;
import java.util.Arrays;

public interface ve {

    public static final class a extends e {
        public String A;
        public String B;
        public long C;
        public long D;
        public boolean E;
        public f F;
        public c G;
        public C0102a H;
        public int I;
        public int J;
        public l K;
        public k L;
        public k M;
        public String N;
        public String b;
        public long c;
        public String[] d;
        public String e;
        public String f;
        public String[] g;
        public String[] h;
        public String[] i;
        public b j;
        public d[] k;
        public m l;
        public i m;
        public g[] n;
        public String o;
        public String p;
        public boolean q;
        public String r;
        public String s;
        public String[] t;
        public j u;
        public boolean v;
        public e[] w;
        public h x;
        public String y;
        public String z;

        /* renamed from: com.yandex.metrica.impl.ob.ve$a$a reason: collision with other inner class name */
        public static final class C0102a extends e {
            public b b;
            public C0103a[] c;
            public long d;
            public long e;

            /* renamed from: com.yandex.metrica.impl.ob.ve$a$a$a reason: collision with other inner class name */
            public static final class C0103a extends e {
                private static volatile C0103a[] g;
                public String b;
                public String c;
                public C0104a d;
                public b e;
                public c f;

                /* renamed from: com.yandex.metrica.impl.ob.ve$a$a$a$a reason: collision with other inner class name */
                public static final class C0104a extends e {
                    public int b;
                    public byte[] c;
                    public byte[] d;

                    public C0104a() {
                        d();
                    }

                    public C0104a d() {
                        this.b = 0;
                        this.c = g.h;
                        this.d = g.h;
                        this.a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            bVar.a(2, this.c);
                        }
                        if (!Arrays.equals(this.d, g.h)) {
                            bVar.a(3, this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.d(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            c2 += b.b(2, this.c);
                        }
                        return !Arrays.equals(this.d, g.h) ? c2 + b.b(3, this.d) : c2;
                    }

                    /* renamed from: b */
                    public C0104a a(a aVar) throws IOException {
                        while (true) {
                            int a = aVar.a();
                            if (a == 0) {
                                return this;
                            }
                            if (a == 8) {
                                this.b = aVar.g();
                            } else if (a == 18) {
                                this.c = aVar.j();
                            } else if (a == 26) {
                                this.d = aVar.j();
                            } else if (!g.a(aVar, a)) {
                                return this;
                            }
                        }
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.ve$a$a$a$b */
                public static final class b extends e {
                    public String b;
                    public byte[] c;
                    public byte[] d;

                    public b() {
                        d();
                    }

                    public b d() {
                        this.b = "";
                        this.c = g.h;
                        this.d = g.h;
                        this.a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            bVar.a(2, this.c);
                        }
                        if (!Arrays.equals(this.d, g.h)) {
                            bVar.a(3, this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            c2 += b.b(2, this.c);
                        }
                        return !Arrays.equals(this.d, g.h) ? c2 + b.b(3, this.d) : c2;
                    }

                    /* renamed from: b */
                    public b a(a aVar) throws IOException {
                        while (true) {
                            int a = aVar.a();
                            if (a == 0) {
                                return this;
                            }
                            if (a == 10) {
                                this.b = aVar.i();
                            } else if (a == 18) {
                                this.c = aVar.j();
                            } else if (a == 26) {
                                this.d = aVar.j();
                            } else if (!g.a(aVar, a)) {
                                return this;
                            }
                        }
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.ve$a$a$a$c */
                public static final class c extends e {
                    public String b;
                    public String c;

                    public c() {
                        d();
                    }

                    public c d() {
                        this.b = "";
                        this.c = "";
                        this.a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!this.c.equals("")) {
                            bVar.a(2, this.c);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b);
                        return !this.c.equals("") ? c2 + b.b(2, this.c) : c2;
                    }

                    /* renamed from: b */
                    public c a(a aVar) throws IOException {
                        while (true) {
                            int a = aVar.a();
                            if (a == 0) {
                                return this;
                            }
                            if (a == 10) {
                                this.b = aVar.i();
                            } else if (a == 18) {
                                this.c = aVar.i();
                            } else if (!g.a(aVar, a)) {
                                return this;
                            }
                        }
                    }
                }

                public static C0103a[] d() {
                    if (g == null) {
                        synchronized (c.a) {
                            if (g == null) {
                                g = new C0103a[0];
                            }
                        }
                    }
                    return g;
                }

                public C0103a() {
                    e();
                }

                public C0103a e() {
                    this.b = "";
                    this.c = "";
                    this.d = null;
                    this.e = null;
                    this.f = null;
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    if (!this.b.equals("")) {
                        bVar.a(1, this.b);
                    }
                    if (!this.c.equals("")) {
                        bVar.a(2, this.c);
                    }
                    if (this.d != null) {
                        bVar.a(3, (e) this.d);
                    }
                    if (this.e != null) {
                        bVar.a(4, (e) this.e);
                    }
                    if (this.f != null) {
                        bVar.a(5, (e) this.f);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c();
                    if (!this.b.equals("")) {
                        c2 += b.b(1, this.b);
                    }
                    if (!this.c.equals("")) {
                        c2 += b.b(2, this.c);
                    }
                    if (this.d != null) {
                        c2 += b.b(3, (e) this.d);
                    }
                    if (this.e != null) {
                        c2 += b.b(4, (e) this.e);
                    }
                    return this.f != null ? c2 + b.b(5, (e) this.f) : c2;
                }

                /* renamed from: b */
                public C0103a a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a == 0) {
                            return this;
                        }
                        if (a == 10) {
                            this.b = aVar.i();
                        } else if (a == 18) {
                            this.c = aVar.i();
                        } else if (a == 26) {
                            if (this.d == null) {
                                this.d = new C0104a();
                            }
                            aVar.a((e) this.d);
                        } else if (a == 34) {
                            if (this.e == null) {
                                this.e = new b();
                            }
                            aVar.a((e) this.e);
                        } else if (a == 42) {
                            if (this.f == null) {
                                this.f = new c();
                            }
                            aVar.a((e) this.f);
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    }
                }
            }

            /* renamed from: com.yandex.metrica.impl.ob.ve$a$a$b */
            public static final class b extends e {
                public int b;
                public int c;
                public int d;
                public int e;
                public long f;

                public b() {
                    d();
                }

                public b d() {
                    this.b = 1;
                    this.c = 2;
                    this.d = 3;
                    this.e = 1;
                    this.f = 300000;
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    if (this.b != 1) {
                        bVar.a(1, this.b);
                    }
                    if (this.c != 2) {
                        bVar.a(2, this.c);
                    }
                    if (this.d != 3) {
                        bVar.a(3, this.d);
                    }
                    if (this.e != 1) {
                        bVar.a(4, this.e);
                    }
                    if (this.f != 300000) {
                        bVar.b(5, this.f);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c();
                    if (this.b != 1) {
                        c2 += b.d(1, this.b);
                    }
                    if (this.c != 2) {
                        c2 += b.d(2, this.c);
                    }
                    if (this.d != 3) {
                        c2 += b.d(3, this.d);
                    }
                    if (this.e != 1) {
                        c2 += b.d(4, this.e);
                    }
                    return this.f != 300000 ? c2 + b.e(5, this.f) : c2;
                }

                /* renamed from: b */
                public b a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a != 0) {
                            if (a != 8) {
                                if (a != 16) {
                                    if (a != 24) {
                                        if (a == 32) {
                                            int g = aVar.g();
                                            switch (g) {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    this.e = g;
                                                    break;
                                            }
                                        } else if (a == 40) {
                                            this.f = aVar.f();
                                        } else if (!g.a(aVar, a)) {
                                            return this;
                                        }
                                    } else {
                                        int g2 = aVar.g();
                                        switch (g2) {
                                            case 1:
                                            case 2:
                                            case 3:
                                                this.d = g2;
                                                break;
                                        }
                                    }
                                } else {
                                    int g3 = aVar.g();
                                    switch (g3) {
                                        case 1:
                                        case 2:
                                            this.c = g3;
                                            break;
                                    }
                                }
                            } else {
                                int g4 = aVar.g();
                                switch (g4) {
                                    case 1:
                                    case 2:
                                    case 3:
                                        this.b = g4;
                                        break;
                                }
                            }
                        } else {
                            return this;
                        }
                    }
                }
            }

            public C0102a() {
                d();
            }

            public C0102a d() {
                this.b = null;
                this.c = C0103a.d();
                this.d = 1200000;
                this.e = 259200000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != null) {
                    bVar.a(1, (e) this.b);
                }
                if (this.c != null && this.c.length > 0) {
                    for (C0103a aVar : this.c) {
                        if (aVar != null) {
                            bVar.a(2, (e) aVar);
                        }
                    }
                }
                bVar.b(3, this.d);
                bVar.b(4, this.e);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != null) {
                    c2 += b.b(1, (e) this.b);
                }
                if (this.c != null && this.c.length > 0) {
                    for (C0103a aVar : this.c) {
                        if (aVar != null) {
                            c2 += b.b(2, (e) aVar);
                        }
                    }
                }
                return c2 + b.e(3, this.d) + b.e(4, this.e);
            }

            /* renamed from: b */
            public C0102a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        if (this.b == null) {
                            this.b = new b();
                        }
                        aVar.a((e) this.b);
                    } else if (a == 18) {
                        int b2 = g.b(aVar, 18);
                        int length = this.c == null ? 0 : this.c.length;
                        C0103a[] aVarArr = new C0103a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.c, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0103a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0103a();
                        aVar.a((e) aVarArr[length]);
                        this.c = aVarArr;
                    } else if (a == 24) {
                        this.d = aVar.f();
                    } else if (a == 32) {
                        this.e = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class b extends e {
            public boolean b;
            public boolean c;
            public boolean d;
            public boolean e;
            public boolean f;
            public boolean g;
            public boolean h;
            public boolean i;
            public boolean j;
            public boolean k;
            public boolean l;
            public boolean m;
            public boolean n;
            public boolean o;
            public boolean p;
            public boolean q;
            public boolean r;
            public boolean s;
            public boolean t;
            public boolean u;
            public boolean v;
            public boolean w;
            public boolean x;

            public b() {
                d();
            }

            public b d() {
                this.b = false;
                this.c = false;
                this.d = false;
                this.e = false;
                this.f = false;
                this.g = false;
                this.h = false;
                this.i = false;
                this.j = false;
                this.k = false;
                this.l = false;
                this.m = false;
                this.n = false;
                this.o = false;
                this.p = true;
                this.q = false;
                this.r = false;
                this.s = false;
                this.t = false;
                this.u = false;
                this.v = false;
                this.w = false;
                this.x = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.a(4, this.e);
                bVar.a(5, this.f);
                bVar.a(6, this.g);
                bVar.a(8, this.h);
                bVar.a(9, this.i);
                bVar.a(10, this.j);
                bVar.a(11, this.k);
                bVar.a(12, this.l);
                bVar.a(13, this.m);
                bVar.a(14, this.n);
                bVar.a(15, this.o);
                bVar.a(16, this.p);
                bVar.a(17, this.q);
                bVar.a(18, this.r);
                bVar.a(19, this.s);
                bVar.a(20, this.t);
                bVar.a(21, this.u);
                bVar.a(22, this.v);
                bVar.a(23, this.w);
                bVar.a(24, this.x);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d) + b.b(4, this.e) + b.b(5, this.f) + b.b(6, this.g) + b.b(8, this.h) + b.b(9, this.i) + b.b(10, this.j) + b.b(11, this.k) + b.b(12, this.l) + b.b(13, this.m) + b.b(14, this.n) + b.b(15, this.o) + b.b(16, this.p) + b.b(17, this.q) + b.b(18, this.r) + b.b(19, this.s) + b.b(20, this.t) + b.b(21, this.u) + b.b(22, this.v) + b.b(23, this.w) + b.b(24, this.x);
            }

            /* renamed from: b */
            public b a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    switch (a) {
                        case 0:
                            return this;
                        case 8:
                            this.b = aVar.h();
                            break;
                        case 16:
                            this.c = aVar.h();
                            break;
                        case 24:
                            this.d = aVar.h();
                            break;
                        case 32:
                            this.e = aVar.h();
                            break;
                        case 40:
                            this.f = aVar.h();
                            break;
                        case 48:
                            this.g = aVar.h();
                            break;
                        case 64:
                            this.h = aVar.h();
                            break;
                        case 72:
                            this.i = aVar.h();
                            break;
                        case 80:
                            this.j = aVar.h();
                            break;
                        case 88:
                            this.k = aVar.h();
                            break;
                        case 96:
                            this.l = aVar.h();
                            break;
                        case 104:
                            this.m = aVar.h();
                            break;
                        case 112:
                            this.n = aVar.h();
                            break;
                        case 120:
                            this.o = aVar.h();
                            break;
                        case 128:
                            this.p = aVar.h();
                            break;
                        case 136:
                            this.q = aVar.h();
                            break;
                        case 144:
                            this.r = aVar.h();
                            break;
                        case 152:
                            this.s = aVar.h();
                            break;
                        case 160:
                            this.t = aVar.h();
                            break;
                        case 168:
                            this.u = aVar.h();
                            break;
                        case 176:
                            this.v = aVar.h();
                            break;
                        case 184:
                            this.w = aVar.h();
                            break;
                        case PsExtractor.AUDIO_STREAM /*192*/:
                            this.x = aVar.h();
                            break;
                        default:
                            if (g.a(aVar, a)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }
        }

        public static final class c extends e {
            public long b;

            public c() {
                d();
            }

            public c d() {
                this.b = 900;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b);
            }

            /* renamed from: b */
            public c a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class d extends e {
            private static volatile d[] d;
            public C0105a b;
            public b c;

            /* renamed from: com.yandex.metrica.impl.ob.ve$a$d$a reason: collision with other inner class name */
            public static final class C0105a extends e {
                public long b;
                public float c;
                public int d;
                public int e;
                public long f;
                public int g;
                public boolean h;
                public boolean i;
                public boolean j;
                public long k;
                public C0106a l;
                public C0106a m;
                public C0106a n;
                public C0106a o;
                public b p;

                /* renamed from: com.yandex.metrica.impl.ob.ve$a$d$a$a reason: collision with other inner class name */
                public static final class C0106a extends e {
                    public boolean b;
                    public boolean c;
                    public C0107a d;

                    /* renamed from: com.yandex.metrica.impl.ob.ve$a$d$a$a$a reason: collision with other inner class name */
                    public static final class C0107a extends e {
                        public long b;
                        public long c;

                        public C0107a() {
                            d();
                        }

                        public C0107a d() {
                            this.b = 0;
                            this.c = 0;
                            this.a = -1;
                            return this;
                        }

                        public void a(b bVar) throws IOException {
                            bVar.b(1, this.b);
                            bVar.b(2, this.c);
                            super.a(bVar);
                        }

                        /* access modifiers changed from: protected */
                        public int c() {
                            return super.c() + b.e(1, this.b) + b.e(2, this.c);
                        }

                        /* renamed from: b */
                        public C0107a a(a aVar) throws IOException {
                            while (true) {
                                int a = aVar.a();
                                if (a == 0) {
                                    return this;
                                }
                                if (a == 8) {
                                    this.b = aVar.f();
                                } else if (a == 16) {
                                    this.c = aVar.f();
                                } else if (!g.a(aVar, a)) {
                                    return this;
                                }
                            }
                        }
                    }

                    public C0106a() {
                        d();
                    }

                    public C0106a d() {
                        this.b = true;
                        this.c = false;
                        this.d = null;
                        this.a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        bVar.a(2, this.c);
                        if (this.d != null) {
                            bVar.a(3, (e) this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c);
                        return this.d != null ? c2 + b.b(3, (e) this.d) : c2;
                    }

                    /* renamed from: b */
                    public C0106a a(a aVar) throws IOException {
                        while (true) {
                            int a = aVar.a();
                            if (a == 0) {
                                return this;
                            }
                            if (a == 8) {
                                this.b = aVar.h();
                            } else if (a == 16) {
                                this.c = aVar.h();
                            } else if (a == 26) {
                                if (this.d == null) {
                                    this.d = new C0107a();
                                }
                                aVar.a((e) this.d);
                            } else if (!g.a(aVar, a)) {
                                return this;
                            }
                        }
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.ve$a$d$a$b */
                public static final class b extends e {
                    public boolean b;
                    public boolean c;
                    public C0108a d;

                    /* renamed from: com.yandex.metrica.impl.ob.ve$a$d$a$b$a reason: collision with other inner class name */
                    public static final class C0108a extends e {
                        public long b;
                        public long c;
                        public int d;

                        public C0108a() {
                            d();
                        }

                        public C0108a d() {
                            this.b = 0;
                            this.c = 0;
                            this.d = 0;
                            this.a = -1;
                            return this;
                        }

                        public void a(b bVar) throws IOException {
                            bVar.b(1, this.b);
                            bVar.b(2, this.c);
                            bVar.a(3, this.d);
                            super.a(bVar);
                        }

                        /* access modifiers changed from: protected */
                        public int c() {
                            return super.c() + b.e(1, this.b) + b.e(2, this.c) + b.d(3, this.d);
                        }

                        /* renamed from: b */
                        public C0108a a(a aVar) throws IOException {
                            while (true) {
                                int a = aVar.a();
                                if (a == 0) {
                                    return this;
                                }
                                if (a == 8) {
                                    this.b = aVar.f();
                                } else if (a != 16) {
                                    if (a == 24) {
                                        int g = aVar.g();
                                        switch (g) {
                                            case 0:
                                            case 1:
                                            case 2:
                                            case 3:
                                                this.d = g;
                                                break;
                                        }
                                    } else if (!g.a(aVar, a)) {
                                        return this;
                                    }
                                } else {
                                    this.c = aVar.f();
                                }
                            }
                        }
                    }

                    public b() {
                        d();
                    }

                    public b d() {
                        this.b = true;
                        this.c = false;
                        this.d = null;
                        this.a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        bVar.a(2, this.c);
                        if (this.d != null) {
                            bVar.a(3, (e) this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c);
                        return this.d != null ? c2 + b.b(3, (e) this.d) : c2;
                    }

                    /* renamed from: b */
                    public b a(a aVar) throws IOException {
                        while (true) {
                            int a = aVar.a();
                            if (a == 0) {
                                return this;
                            }
                            if (a == 8) {
                                this.b = aVar.h();
                            } else if (a == 16) {
                                this.c = aVar.h();
                            } else if (a == 26) {
                                if (this.d == null) {
                                    this.d = new C0108a();
                                }
                                aVar.a((e) this.d);
                            } else if (!g.a(aVar, a)) {
                                return this;
                            }
                        }
                    }
                }

                public C0105a() {
                    d();
                }

                public C0105a d() {
                    this.b = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
                    this.c = 10.0f;
                    this.d = 20;
                    this.e = 200;
                    this.f = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
                    this.g = 10000;
                    this.h = false;
                    this.i = false;
                    this.j = true;
                    this.k = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
                    this.l = null;
                    this.m = null;
                    this.n = null;
                    this.o = null;
                    this.p = null;
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.b(1, this.b);
                    bVar.a(2, this.c);
                    bVar.a(3, this.d);
                    bVar.a(4, this.e);
                    bVar.b(5, this.f);
                    bVar.a(6, this.g);
                    bVar.a(7, this.h);
                    bVar.a(8, this.i);
                    bVar.b(9, this.k);
                    if (this.l != null) {
                        bVar.a(10, (e) this.l);
                    }
                    if (this.m != null) {
                        bVar.a(11, (e) this.m);
                    }
                    if (this.n != null) {
                        bVar.a(12, (e) this.n);
                    }
                    bVar.a(13, this.j);
                    if (this.o != null) {
                        bVar.a(14, (e) this.o);
                    }
                    if (this.p != null) {
                        bVar.a(15, (e) this.p);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c() + b.e(1, this.b) + b.b(2, this.c) + b.d(3, this.d) + b.d(4, this.e) + b.e(5, this.f) + b.d(6, this.g) + b.b(7, this.h) + b.b(8, this.i) + b.e(9, this.k);
                    if (this.l != null) {
                        c2 += b.b(10, (e) this.l);
                    }
                    if (this.m != null) {
                        c2 += b.b(11, (e) this.m);
                    }
                    if (this.n != null) {
                        c2 += b.b(12, (e) this.n);
                    }
                    int b2 = c2 + b.b(13, this.j);
                    if (this.o != null) {
                        b2 += b.b(14, (e) this.o);
                    }
                    return this.p != null ? b2 + b.b(15, (e) this.p) : b2;
                }

                /* renamed from: b */
                public C0105a a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        switch (a) {
                            case 0:
                                return this;
                            case 8:
                                this.b = aVar.f();
                                break;
                            case 21:
                                this.c = aVar.d();
                                break;
                            case 24:
                                this.d = aVar.g();
                                break;
                            case 32:
                                this.e = aVar.g();
                                break;
                            case 40:
                                this.f = aVar.f();
                                break;
                            case 48:
                                this.g = aVar.g();
                                break;
                            case 56:
                                this.h = aVar.h();
                                break;
                            case 64:
                                this.i = aVar.h();
                                break;
                            case 72:
                                this.k = aVar.f();
                                break;
                            case 82:
                                if (this.l == null) {
                                    this.l = new C0106a();
                                }
                                aVar.a((e) this.l);
                                break;
                            case 90:
                                if (this.m == null) {
                                    this.m = new C0106a();
                                }
                                aVar.a((e) this.m);
                                break;
                            case 98:
                                if (this.n == null) {
                                    this.n = new C0106a();
                                }
                                aVar.a((e) this.n);
                                break;
                            case 104:
                                this.j = aVar.h();
                                break;
                            case 114:
                                if (this.o == null) {
                                    this.o = new C0106a();
                                }
                                aVar.a((e) this.o);
                                break;
                            case 122:
                                if (this.p == null) {
                                    this.p = new b();
                                }
                                aVar.a((e) this.p);
                                break;
                            default:
                                if (g.a(aVar, a)) {
                                    break;
                                } else {
                                    return this;
                                }
                        }
                    }
                }
            }

            public static final class b extends e {
                public int[] b;
                public int[] c;

                public b() {
                    d();
                }

                public b d() {
                    this.b = g.a;
                    this.c = g.a;
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    if (this.b != null && this.b.length > 0) {
                        for (int a : this.b) {
                            bVar.a(1, a);
                        }
                    }
                    if (this.c != null && this.c.length > 0) {
                        for (int a2 : this.c) {
                            bVar.a(2, a2);
                        }
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c();
                    if (this.b != null && this.b.length > 0) {
                        int i = 0;
                        for (int d : this.b) {
                            i += b.d(d);
                        }
                        c2 = c2 + i + (this.b.length * 1);
                    }
                    if (this.c == null || this.c.length <= 0) {
                        return c2;
                    }
                    int i2 = 0;
                    for (int d2 : this.c) {
                        i2 += b.d(d2);
                    }
                    return c2 + i2 + (this.c.length * 1);
                }

                /* renamed from: b */
                public b a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a == 0) {
                            return this;
                        }
                        if (a == 8) {
                            int b2 = g.b(aVar, 8);
                            int[] iArr = new int[b2];
                            int i = 0;
                            for (int i2 = 0; i2 < b2; i2++) {
                                if (i2 != 0) {
                                    aVar.a();
                                }
                                int g = aVar.g();
                                switch (g) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                        int i3 = i + 1;
                                        iArr[i] = g;
                                        i = i3;
                                        break;
                                }
                            }
                            if (i != 0) {
                                int length = this.b == null ? 0 : this.b.length;
                                if (length == 0 && i == iArr.length) {
                                    this.b = iArr;
                                } else {
                                    int[] iArr2 = new int[(length + i)];
                                    if (length != 0) {
                                        System.arraycopy(this.b, 0, iArr2, 0, length);
                                    }
                                    System.arraycopy(iArr, 0, iArr2, length, i);
                                    this.b = iArr2;
                                }
                            }
                        } else if (a == 10) {
                            int d = aVar.d(aVar.n());
                            int t = aVar.t();
                            int i4 = 0;
                            while (aVar.r() > 0) {
                                switch (aVar.g()) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                        i4++;
                                        break;
                                }
                            }
                            if (i4 != 0) {
                                aVar.f(t);
                                int length2 = this.b == null ? 0 : this.b.length;
                                int[] iArr3 = new int[(i4 + length2)];
                                if (length2 != 0) {
                                    System.arraycopy(this.b, 0, iArr3, 0, length2);
                                }
                                while (aVar.r() > 0) {
                                    int g2 = aVar.g();
                                    switch (g2) {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                            int i5 = length2 + 1;
                                            iArr3[length2] = g2;
                                            length2 = i5;
                                            break;
                                    }
                                }
                                this.b = iArr3;
                            }
                            aVar.e(d);
                        } else if (a == 16) {
                            int b3 = g.b(aVar, 16);
                            int[] iArr4 = new int[b3];
                            int i6 = 0;
                            for (int i7 = 0; i7 < b3; i7++) {
                                if (i7 != 0) {
                                    aVar.a();
                                }
                                int g3 = aVar.g();
                                switch (g3) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                        int i8 = i6 + 1;
                                        iArr4[i6] = g3;
                                        i6 = i8;
                                        break;
                                }
                            }
                            if (i6 != 0) {
                                int length3 = this.c == null ? 0 : this.c.length;
                                if (length3 == 0 && i6 == iArr4.length) {
                                    this.c = iArr4;
                                } else {
                                    int[] iArr5 = new int[(length3 + i6)];
                                    if (length3 != 0) {
                                        System.arraycopy(this.c, 0, iArr5, 0, length3);
                                    }
                                    System.arraycopy(iArr4, 0, iArr5, length3, i6);
                                    this.c = iArr5;
                                }
                            }
                        } else if (a == 18) {
                            int d2 = aVar.d(aVar.n());
                            int t2 = aVar.t();
                            int i9 = 0;
                            while (aVar.r() > 0) {
                                switch (aVar.g()) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                        i9++;
                                        break;
                                }
                            }
                            if (i9 != 0) {
                                aVar.f(t2);
                                int length4 = this.c == null ? 0 : this.c.length;
                                int[] iArr6 = new int[(i9 + length4)];
                                if (length4 != 0) {
                                    System.arraycopy(this.c, 0, iArr6, 0, length4);
                                }
                                while (aVar.r() > 0) {
                                    int g4 = aVar.g();
                                    switch (g4) {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                            int i10 = length4 + 1;
                                            iArr6[length4] = g4;
                                            length4 = i10;
                                            break;
                                    }
                                }
                                this.c = iArr6;
                            }
                            aVar.e(d2);
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    }
                }
            }

            public static d[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new d[0];
                        }
                    }
                }
                return d;
            }

            public d() {
                e();
            }

            public d e() {
                this.b = null;
                this.c = null;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != null) {
                    bVar.a(1, (e) this.b);
                }
                if (this.c != null) {
                    bVar.a(2, (e) this.c);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != null) {
                    c2 += b.b(1, (e) this.b);
                }
                return this.c != null ? c2 + b.b(2, (e) this.c) : c2;
            }

            /* renamed from: b */
            public d a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        if (this.b == null) {
                            this.b = new C0105a();
                        }
                        aVar.a((e) this.b);
                    } else if (a == 18) {
                        if (this.c == null) {
                            this.c = new b();
                        }
                        aVar.a((e) this.c);
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class e extends e {
            private static volatile e[] d;
            public String b;
            public boolean c;

            public static e[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new e[0];
                        }
                    }
                }
                return d;
            }

            public e() {
                e();
            }

            public e e() {
                this.b = "";
                this.c = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public e a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class f extends e {
            public long b;
            public long c;

            public f() {
                d();
            }

            public f d() {
                this.b = 86400;
                this.c = 432000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.b(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b) + b.e(2, this.c);
            }

            /* renamed from: b */
            public f a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 16) {
                        this.c = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class g extends e {
            private static volatile g[] h;
            public String b;
            public String c;
            public String d;
            public C0109a[] e;
            public long f;
            public int[] g;

            /* renamed from: com.yandex.metrica.impl.ob.ve$a$g$a reason: collision with other inner class name */
            public static final class C0109a extends e {
                private static volatile C0109a[] d;
                public String b;
                public String c;

                public static C0109a[] d() {
                    if (d == null) {
                        synchronized (c.a) {
                            if (d == null) {
                                d = new C0109a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0109a() {
                    e();
                }

                public C0109a e() {
                    this.b = "";
                    this.c = "";
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.a(1, this.b);
                    bVar.a(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.b(1, this.b) + b.b(2, this.c);
                }

                /* renamed from: b */
                public C0109a a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a == 0) {
                            return this;
                        }
                        if (a == 10) {
                            this.b = aVar.i();
                        } else if (a == 18) {
                            this.c = aVar.i();
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    }
                }
            }

            public static g[] d() {
                if (h == null) {
                    synchronized (c.a) {
                        if (h == null) {
                            h = new g[0];
                        }
                    }
                }
                return h;
            }

            public g() {
                e();
            }

            public g e() {
                this.b = "";
                this.c = "";
                this.d = "";
                this.e = C0109a.d();
                this.f = 0;
                this.g = g.a;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    for (C0109a aVar : this.e) {
                        if (aVar != null) {
                            bVar.a(4, (e) aVar);
                        }
                    }
                }
                bVar.b(5, this.f);
                if (this.g != null && this.g.length > 0) {
                    for (int a : this.g) {
                        bVar.a(6, a);
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    int i = c2;
                    for (C0109a aVar : this.e) {
                        if (aVar != null) {
                            i += b.b(4, (e) aVar);
                        }
                    }
                    c2 = i;
                }
                int e2 = c2 + b.e(5, this.f);
                if (this.g == null || this.g.length <= 0) {
                    return e2;
                }
                int i2 = 0;
                for (int d2 : this.g) {
                    i2 += b.d(d2);
                }
                return e2 + i2 + (this.g.length * 1);
            }

            /* renamed from: b */
            public g a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 18) {
                        this.c = aVar.i();
                    } else if (a == 26) {
                        this.d = aVar.i();
                    } else if (a == 34) {
                        int b2 = g.b(aVar, 34);
                        int length = this.e == null ? 0 : this.e.length;
                        C0109a[] aVarArr = new C0109a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.e, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0109a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0109a();
                        aVar.a((e) aVarArr[length]);
                        this.e = aVarArr;
                    } else if (a == 40) {
                        this.f = aVar.f();
                    } else if (a == 48) {
                        int b3 = g.b(aVar, 48);
                        int[] iArr = new int[b3];
                        int i = 0;
                        for (int i2 = 0; i2 < b3; i2++) {
                            if (i2 != 0) {
                                aVar.a();
                            }
                            int g2 = aVar.g();
                            switch (g2) {
                                case 1:
                                case 2:
                                    int i3 = i + 1;
                                    iArr[i] = g2;
                                    i = i3;
                                    break;
                            }
                        }
                        if (i != 0) {
                            int length2 = this.g == null ? 0 : this.g.length;
                            if (length2 == 0 && i == iArr.length) {
                                this.g = iArr;
                            } else {
                                int[] iArr2 = new int[(length2 + i)];
                                if (length2 != 0) {
                                    System.arraycopy(this.g, 0, iArr2, 0, length2);
                                }
                                System.arraycopy(iArr, 0, iArr2, length2, i);
                                this.g = iArr2;
                            }
                        }
                    } else if (a == 50) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i4 = 0;
                        while (aVar.r() > 0) {
                            switch (aVar.g()) {
                                case 1:
                                case 2:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            aVar.f(t);
                            int length3 = this.g == null ? 0 : this.g.length;
                            int[] iArr3 = new int[(i4 + length3)];
                            if (length3 != 0) {
                                System.arraycopy(this.g, 0, iArr3, 0, length3);
                            }
                            while (aVar.r() > 0) {
                                int g3 = aVar.g();
                                switch (g3) {
                                    case 1:
                                    case 2:
                                        int i5 = length3 + 1;
                                        iArr3[length3] = g3;
                                        length3 = i5;
                                        break;
                                }
                            }
                            this.g = iArr3;
                        }
                        aVar.e(d2);
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class h extends e {
            public long b;
            public long c;
            public long d;
            public long e;

            public h() {
                d();
            }

            public h d() {
                this.b = 432000000;
                this.c = 86400000;
                this.d = TapjoyConstants.TIMER_INCREMENT;
                this.e = 3600000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != 432000000) {
                    bVar.b(1, this.b);
                }
                if (this.c != 86400000) {
                    bVar.b(2, this.c);
                }
                if (this.d != TapjoyConstants.TIMER_INCREMENT) {
                    bVar.b(3, this.d);
                }
                if (this.e != 3600000) {
                    bVar.b(4, this.e);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != 432000000) {
                    c2 += b.e(1, this.b);
                }
                if (this.c != 86400000) {
                    c2 += b.e(2, this.c);
                }
                if (this.d != TapjoyConstants.TIMER_INCREMENT) {
                    c2 += b.e(3, this.d);
                }
                return this.e != 3600000 ? c2 + b.e(4, this.e) : c2;
            }

            /* renamed from: b */
            public h a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 16) {
                        this.c = aVar.f();
                    } else if (a == 24) {
                        this.d = aVar.f();
                    } else if (a == 32) {
                        this.e = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class i extends e {
            public long b;
            public String c;
            public int[] d;
            public long e;
            public int f;

            public i() {
                d();
            }

            public i d() {
                this.b = 0;
                this.c = "";
                this.d = g.a;
                this.e = 259200;
                this.f = 10;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.a(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    for (int a : this.d) {
                        bVar.a(3, a);
                    }
                }
                bVar.b(4, this.e);
                bVar.a(5, this.f);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.e(1, this.b) + b.b(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    int i = 0;
                    for (int d2 : this.d) {
                        i += b.d(d2);
                    }
                    c2 = c2 + i + (this.d.length * 1);
                }
                return c2 + b.e(4, this.e) + b.d(5, this.f);
            }

            /* renamed from: b */
            public i a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 18) {
                        this.c = aVar.i();
                    } else if (a == 24) {
                        int b2 = g.b(aVar, 24);
                        int length = this.d == null ? 0 : this.d.length;
                        int[] iArr = new int[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = aVar.g();
                            aVar.a();
                            length++;
                        }
                        iArr[length] = aVar.g();
                        this.d = iArr;
                    } else if (a == 26) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i = 0;
                        while (aVar.r() > 0) {
                            aVar.g();
                            i++;
                        }
                        aVar.f(t);
                        int length2 = this.d == null ? 0 : this.d.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.d, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = aVar.g();
                            length2++;
                        }
                        this.d = iArr2;
                        aVar.e(d2);
                    } else if (a == 32) {
                        this.e = aVar.f();
                    } else if (a == 40) {
                        this.f = aVar.g();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class j extends e {
            public long b;

            public j() {
                d();
            }

            public j d() {
                this.b = 18000000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b);
            }

            /* renamed from: b */
            public j a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class k extends e {
            public boolean b;
            public boolean c;
            public boolean d;
            public boolean e;
            public int f;
            public int g;
            public int h;
            public int i;
            public boolean j;
            public boolean k;
            public boolean l;

            public k() {
                d();
            }

            public k d() {
                this.b = true;
                this.c = true;
                this.d = true;
                this.e = true;
                this.f = 10000;
                this.g = 1000;
                this.h = 1000;
                this.i = ControllerParameters.GLOBAL_RUNTIME;
                this.j = false;
                this.k = false;
                this.l = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.a(4, this.e);
                bVar.a(5, this.f);
                bVar.a(6, this.g);
                bVar.a(7, this.h);
                bVar.a(8, this.i);
                bVar.a(9, this.j);
                bVar.a(10, this.k);
                bVar.a(11, this.l);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d) + b.b(4, this.e) + b.d(5, this.f) + b.d(6, this.g) + b.d(7, this.h) + b.d(8, this.i) + b.b(9, this.j) + b.b(10, this.k) + b.b(11, this.l);
            }

            /* renamed from: b */
            public k a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    switch (a) {
                        case 0:
                            return this;
                        case 8:
                            this.b = aVar.h();
                            break;
                        case 16:
                            this.c = aVar.h();
                            break;
                        case 24:
                            this.d = aVar.h();
                            break;
                        case 32:
                            this.e = aVar.h();
                            break;
                        case 40:
                            this.f = aVar.g();
                            break;
                        case 48:
                            this.g = aVar.g();
                            break;
                        case 56:
                            this.h = aVar.g();
                            break;
                        case 64:
                            this.i = aVar.g();
                            break;
                        case 72:
                            this.j = aVar.h();
                            break;
                        case 80:
                            this.k = aVar.h();
                            break;
                        case 88:
                            this.l = aVar.h();
                            break;
                        default:
                            if (g.a(aVar, a)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }
        }

        public static final class l extends e {
            public int b;
            public int c;
            public int d;
            public long e;
            public boolean f;

            public l() {
                d();
            }

            public l d() {
                this.b = 10000;
                this.c = 1000;
                this.d = 100;
                this.e = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
                this.f = true;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.b(4, this.e);
                bVar.a(5, this.f);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.d(1, this.b) + b.d(2, this.c) + b.d(3, this.d) + b.e(4, this.e) + b.b(5, this.f);
            }

            /* renamed from: b */
            public l a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.g();
                    } else if (a == 16) {
                        this.c = aVar.g();
                    } else if (a == 24) {
                        this.d = aVar.g();
                    } else if (a == 32) {
                        this.e = aVar.f();
                    } else if (a == 40) {
                        this.f = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class m extends e {
            public long b;
            public boolean c;
            public C0110a[] d;

            /* renamed from: com.yandex.metrica.impl.ob.ve$a$m$a reason: collision with other inner class name */
            public static final class C0110a extends e {
                private static volatile C0110a[] d;
                public long b;
                public long c;

                public static C0110a[] d() {
                    if (d == null) {
                        synchronized (c.a) {
                            if (d == null) {
                                d = new C0110a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0110a() {
                    e();
                }

                public C0110a e() {
                    this.b = 0;
                    this.c = 0;
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.b(1, this.b);
                    bVar.b(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.e(1, this.b) + b.e(2, this.c);
                }

                /* renamed from: b */
                public C0110a a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a == 0) {
                            return this;
                        }
                        if (a == 8) {
                            this.b = aVar.f();
                        } else if (a == 16) {
                            this.c = aVar.f();
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    }
                }
            }

            public m() {
                d();
            }

            public m d() {
                this.b = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
                this.c = false;
                this.d = C0110a.d();
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.a(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    for (C0110a aVar : this.d) {
                        if (aVar != null) {
                            bVar.a(5, (e) aVar);
                        }
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.e(1, this.b) + b.b(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    for (C0110a aVar : this.d) {
                        if (aVar != null) {
                            c2 += b.b(5, (e) aVar);
                        }
                    }
                }
                return c2;
            }

            /* renamed from: b */
            public m a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (a == 42) {
                        int b2 = g.b(aVar, 42);
                        int length = this.d == null ? 0 : this.d.length;
                        C0110a[] aVarArr = new C0110a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0110a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0110a();
                        aVar.a((e) aVarArr[length]);
                        this.d = aVarArr;
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = "";
            this.c = 0;
            this.d = g.f;
            this.e = "";
            this.f = "";
            this.g = g.f;
            this.h = g.f;
            this.i = g.f;
            this.j = null;
            this.k = d.d();
            this.l = null;
            this.m = null;
            this.n = g.d();
            this.o = "";
            this.p = "";
            this.q = false;
            this.r = "";
            this.s = "";
            this.t = g.f;
            this.u = null;
            this.v = false;
            this.w = e.d();
            this.x = null;
            this.y = "";
            this.z = "";
            this.A = "";
            this.B = "";
            this.C = 0;
            this.D = 0;
            this.E = false;
            this.F = null;
            this.G = null;
            this.H = null;
            this.I = 600;
            this.J = 1;
            this.K = null;
            this.L = null;
            this.M = null;
            this.N = "null";
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (!this.b.equals("")) {
                bVar.a(1, this.b);
            }
            bVar.b(3, this.c);
            if (this.d != null && this.d.length > 0) {
                for (String str : this.d) {
                    if (str != null) {
                        bVar.a(4, str);
                    }
                }
            }
            if (!this.e.equals("")) {
                bVar.a(5, this.e);
            }
            if (!this.f.equals("")) {
                bVar.a(6, this.f);
            }
            if (this.g != null && this.g.length > 0) {
                for (String str2 : this.g) {
                    if (str2 != null) {
                        bVar.a(7, str2);
                    }
                }
            }
            if (this.h != null && this.h.length > 0) {
                for (String str3 : this.h) {
                    if (str3 != null) {
                        bVar.a(8, str3);
                    }
                }
            }
            if (this.i != null && this.i.length > 0) {
                for (String str4 : this.i) {
                    if (str4 != null) {
                        bVar.a(9, str4);
                    }
                }
            }
            if (this.j != null) {
                bVar.a(10, (e) this.j);
            }
            if (this.k != null && this.k.length > 0) {
                for (d dVar : this.k) {
                    if (dVar != null) {
                        bVar.a(11, (e) dVar);
                    }
                }
            }
            if (this.l != null) {
                bVar.a(12, (e) this.l);
            }
            if (this.m != null) {
                bVar.a(13, (e) this.m);
            }
            if (this.n != null && this.n.length > 0) {
                for (g gVar : this.n) {
                    if (gVar != null) {
                        bVar.a(14, (e) gVar);
                    }
                }
            }
            if (!this.o.equals("")) {
                bVar.a(15, this.o);
            }
            if (!this.p.equals("")) {
                bVar.a(16, this.p);
            }
            bVar.a(17, this.q);
            if (!this.r.equals("")) {
                bVar.a(18, this.r);
            }
            if (!this.s.equals("")) {
                bVar.a(19, this.s);
            }
            if (this.t != null && this.t.length > 0) {
                for (String str5 : this.t) {
                    if (str5 != null) {
                        bVar.a(20, str5);
                    }
                }
            }
            if (this.u != null) {
                bVar.a(21, (e) this.u);
            }
            if (this.v) {
                bVar.a(22, this.v);
            }
            if (this.w != null && this.w.length > 0) {
                for (e eVar : this.w) {
                    if (eVar != null) {
                        bVar.a(23, (e) eVar);
                    }
                }
            }
            if (this.x != null) {
                bVar.a(24, (e) this.x);
            }
            if (!this.y.equals("")) {
                bVar.a(25, this.y);
            }
            if (!this.z.equals("")) {
                bVar.a(26, this.z);
            }
            if (!this.B.equals("")) {
                bVar.a(27, this.B);
            }
            bVar.b(28, this.C);
            bVar.b(29, this.D);
            if (this.E) {
                bVar.a(30, this.E);
            }
            if (!this.A.equals("")) {
                bVar.a(31, this.A);
            }
            if (this.F != null) {
                bVar.a(32, (e) this.F);
            }
            if (this.G != null) {
                bVar.a(33, (e) this.G);
            }
            if (this.H != null) {
                bVar.a(34, (e) this.H);
            }
            bVar.a(35, this.I);
            bVar.a(36, this.J);
            if (this.K != null) {
                bVar.a(37, (e) this.K);
            }
            if (this.L != null) {
                bVar.a(38, (e) this.L);
            }
            if (this.M != null) {
                bVar.a(39, (e) this.M);
            }
            bVar.a(40, this.N);
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (!this.b.equals("")) {
                c2 += b.b(1, this.b);
            }
            int e2 = c2 + b.e(3, this.c);
            if (this.d != null && this.d.length > 0) {
                int i2 = 0;
                int i3 = 0;
                for (String str : this.d) {
                    if (str != null) {
                        i3++;
                        i2 += b.b(str);
                    }
                }
                e2 = e2 + i2 + (i3 * 1);
            }
            if (!this.e.equals("")) {
                e2 += b.b(5, this.e);
            }
            if (!this.f.equals("")) {
                e2 += b.b(6, this.f);
            }
            if (this.g != null && this.g.length > 0) {
                int i4 = 0;
                int i5 = 0;
                for (String str2 : this.g) {
                    if (str2 != null) {
                        i5++;
                        i4 += b.b(str2);
                    }
                }
                e2 = e2 + i4 + (i5 * 1);
            }
            if (this.h != null && this.h.length > 0) {
                int i6 = 0;
                int i7 = 0;
                for (String str3 : this.h) {
                    if (str3 != null) {
                        i7++;
                        i6 += b.b(str3);
                    }
                }
                e2 = e2 + i6 + (i7 * 1);
            }
            if (this.i != null && this.i.length > 0) {
                int i8 = 0;
                int i9 = 0;
                for (String str4 : this.i) {
                    if (str4 != null) {
                        i9++;
                        i8 += b.b(str4);
                    }
                }
                e2 = e2 + i8 + (i9 * 1);
            }
            if (this.j != null) {
                e2 += b.b(10, (e) this.j);
            }
            if (this.k != null && this.k.length > 0) {
                int i10 = e2;
                for (d dVar : this.k) {
                    if (dVar != null) {
                        i10 += b.b(11, (e) dVar);
                    }
                }
                e2 = i10;
            }
            if (this.l != null) {
                e2 += b.b(12, (e) this.l);
            }
            if (this.m != null) {
                e2 += b.b(13, (e) this.m);
            }
            if (this.n != null && this.n.length > 0) {
                int i11 = e2;
                for (g gVar : this.n) {
                    if (gVar != null) {
                        i11 += b.b(14, (e) gVar);
                    }
                }
                e2 = i11;
            }
            if (!this.o.equals("")) {
                e2 += b.b(15, this.o);
            }
            if (!this.p.equals("")) {
                e2 += b.b(16, this.p);
            }
            int b2 = e2 + b.b(17, this.q);
            if (!this.r.equals("")) {
                b2 += b.b(18, this.r);
            }
            if (!this.s.equals("")) {
                b2 += b.b(19, this.s);
            }
            if (this.t != null && this.t.length > 0) {
                int i12 = 0;
                int i13 = 0;
                for (String str5 : this.t) {
                    if (str5 != null) {
                        i13++;
                        i12 += b.b(str5);
                    }
                }
                b2 = b2 + i12 + (i13 * 2);
            }
            if (this.u != null) {
                b2 += b.b(21, (e) this.u);
            }
            if (this.v) {
                b2 += b.b(22, this.v);
            }
            if (this.w != null && this.w.length > 0) {
                for (e eVar : this.w) {
                    if (eVar != null) {
                        b2 += b.b(23, (e) eVar);
                    }
                }
            }
            if (this.x != null) {
                b2 += b.b(24, (e) this.x);
            }
            if (!this.y.equals("")) {
                b2 += b.b(25, this.y);
            }
            if (!this.z.equals("")) {
                b2 += b.b(26, this.z);
            }
            if (!this.B.equals("")) {
                b2 += b.b(27, this.B);
            }
            int e3 = b2 + b.e(28, this.C) + b.e(29, this.D);
            if (this.E) {
                e3 += b.b(30, this.E);
            }
            if (!this.A.equals("")) {
                e3 += b.b(31, this.A);
            }
            if (this.F != null) {
                e3 += b.b(32, (e) this.F);
            }
            if (this.G != null) {
                e3 += b.b(33, (e) this.G);
            }
            if (this.H != null) {
                e3 += b.b(34, (e) this.H);
            }
            int d2 = e3 + b.d(35, this.I) + b.d(36, this.J);
            if (this.K != null) {
                d2 += b.b(37, (e) this.K);
            }
            if (this.L != null) {
                d2 += b.b(38, (e) this.L);
            }
            if (this.M != null) {
                d2 += b.b(39, (e) this.M);
            }
            return d2 + b.b(40, this.N);
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                switch (a) {
                    case 0:
                        return this;
                    case 10:
                        this.b = aVar.i();
                        break;
                    case 24:
                        this.c = aVar.f();
                        break;
                    case 34:
                        int b2 = g.b(aVar, 34);
                        int length = this.d == null ? 0 : this.d.length;
                        String[] strArr = new String[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, strArr, 0, length);
                        }
                        while (length < strArr.length - 1) {
                            strArr[length] = aVar.i();
                            aVar.a();
                            length++;
                        }
                        strArr[length] = aVar.i();
                        this.d = strArr;
                        break;
                    case 42:
                        this.e = aVar.i();
                        break;
                    case 50:
                        this.f = aVar.i();
                        break;
                    case 58:
                        int b3 = g.b(aVar, 58);
                        int length2 = this.g == null ? 0 : this.g.length;
                        String[] strArr2 = new String[(b3 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.g, 0, strArr2, 0, length2);
                        }
                        while (length2 < strArr2.length - 1) {
                            strArr2[length2] = aVar.i();
                            aVar.a();
                            length2++;
                        }
                        strArr2[length2] = aVar.i();
                        this.g = strArr2;
                        break;
                    case 66:
                        int b4 = g.b(aVar, 66);
                        int length3 = this.h == null ? 0 : this.h.length;
                        String[] strArr3 = new String[(b4 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.h, 0, strArr3, 0, length3);
                        }
                        while (length3 < strArr3.length - 1) {
                            strArr3[length3] = aVar.i();
                            aVar.a();
                            length3++;
                        }
                        strArr3[length3] = aVar.i();
                        this.h = strArr3;
                        break;
                    case 74:
                        int b5 = g.b(aVar, 74);
                        int length4 = this.i == null ? 0 : this.i.length;
                        String[] strArr4 = new String[(b5 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.i, 0, strArr4, 0, length4);
                        }
                        while (length4 < strArr4.length - 1) {
                            strArr4[length4] = aVar.i();
                            aVar.a();
                            length4++;
                        }
                        strArr4[length4] = aVar.i();
                        this.i = strArr4;
                        break;
                    case 82:
                        if (this.j == null) {
                            this.j = new b();
                        }
                        aVar.a((e) this.j);
                        break;
                    case 90:
                        int b6 = g.b(aVar, 90);
                        int length5 = this.k == null ? 0 : this.k.length;
                        d[] dVarArr = new d[(b6 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.k, 0, dVarArr, 0, length5);
                        }
                        while (length5 < dVarArr.length - 1) {
                            dVarArr[length5] = new d();
                            aVar.a((e) dVarArr[length5]);
                            aVar.a();
                            length5++;
                        }
                        dVarArr[length5] = new d();
                        aVar.a((e) dVarArr[length5]);
                        this.k = dVarArr;
                        break;
                    case 98:
                        if (this.l == null) {
                            this.l = new m();
                        }
                        aVar.a((e) this.l);
                        break;
                    case 106:
                        if (this.m == null) {
                            this.m = new i();
                        }
                        aVar.a((e) this.m);
                        break;
                    case 114:
                        int b7 = g.b(aVar, 114);
                        int length6 = this.n == null ? 0 : this.n.length;
                        g[] gVarArr = new g[(b7 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.n, 0, gVarArr, 0, length6);
                        }
                        while (length6 < gVarArr.length - 1) {
                            gVarArr[length6] = new g();
                            aVar.a((e) gVarArr[length6]);
                            aVar.a();
                            length6++;
                        }
                        gVarArr[length6] = new g();
                        aVar.a((e) gVarArr[length6]);
                        this.n = gVarArr;
                        break;
                    case 122:
                        this.o = aVar.i();
                        break;
                    case TsExtractor.TS_STREAM_TYPE_HDMV_DTS /*130*/:
                        this.p = aVar.i();
                        break;
                    case 136:
                        this.q = aVar.h();
                        break;
                    case 146:
                        this.r = aVar.i();
                        break;
                    case 154:
                        this.s = aVar.i();
                        break;
                    case 162:
                        int b8 = g.b(aVar, 162);
                        int length7 = this.t == null ? 0 : this.t.length;
                        String[] strArr5 = new String[(b8 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.t, 0, strArr5, 0, length7);
                        }
                        while (length7 < strArr5.length - 1) {
                            strArr5[length7] = aVar.i();
                            aVar.a();
                            length7++;
                        }
                        strArr5[length7] = aVar.i();
                        this.t = strArr5;
                        break;
                    case 170:
                        if (this.u == null) {
                            this.u = new j();
                        }
                        aVar.a((e) this.u);
                        break;
                    case 176:
                        this.v = aVar.h();
                        break;
                    case 186:
                        int b9 = g.b(aVar, 186);
                        int length8 = this.w == null ? 0 : this.w.length;
                        e[] eVarArr = new e[(b9 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.w, 0, eVarArr, 0, length8);
                        }
                        while (length8 < eVarArr.length - 1) {
                            eVarArr[length8] = new e();
                            aVar.a((e) eVarArr[length8]);
                            aVar.a();
                            length8++;
                        }
                        eVarArr[length8] = new e();
                        aVar.a((e) eVarArr[length8]);
                        this.w = eVarArr;
                        break;
                    case 194:
                        if (this.x == null) {
                            this.x = new h();
                        }
                        aVar.a((e) this.x);
                        break;
                    case 202:
                        this.y = aVar.i();
                        break;
                    case 210:
                        this.z = aVar.i();
                        break;
                    case 218:
                        this.B = aVar.i();
                        break;
                    case 224:
                        this.C = aVar.f();
                        break;
                    case 232:
                        this.D = aVar.f();
                        break;
                    case PsExtractor.VIDEO_STREAM_MASK /*240*/:
                        this.E = aVar.h();
                        break;
                    case 250:
                        this.A = aVar.i();
                        break;
                    case 258:
                        if (this.F == null) {
                            this.F = new f();
                        }
                        aVar.a((e) this.F);
                        break;
                    case 266:
                        if (this.G == null) {
                            this.G = new c();
                        }
                        aVar.a((e) this.G);
                        break;
                    case 274:
                        if (this.H == null) {
                            this.H = new C0102a();
                        }
                        aVar.a((e) this.H);
                        break;
                    case 280:
                        this.I = aVar.g();
                        break;
                    case 288:
                        this.J = aVar.g();
                        break;
                    case 298:
                        if (this.K == null) {
                            this.K = new l();
                        }
                        aVar.a((e) this.K);
                        break;
                    case 306:
                        if (this.L == null) {
                            this.L = new k();
                        }
                        aVar.a((e) this.L);
                        break;
                    case 314:
                        if (this.M == null) {
                            this.M = new k();
                        }
                        aVar.a((e) this.M);
                        break;
                    case 322:
                        this.N = aVar.i();
                        break;
                    default:
                        if (g.a(aVar, a)) {
                            break;
                        } else {
                            return this;
                        }
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
