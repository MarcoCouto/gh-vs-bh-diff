package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.i.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ac {
    @NonNull
    private final Context a;
    @NonNull
    private final p b;
    @NonNull
    private final k c;
    @NonNull
    private final qp d;
    @NonNull
    private final Cdo e;
    @NonNull
    private final dn f;
    /* access modifiers changed from: private */
    public ContentValues g;
    private we h;

    public ac(Context context) {
        this(context, as.a().n(), as.a().o(), qp.a(context), dn.a(context));
    }

    @VisibleForTesting
    ac(@NonNull Context context, @NonNull p pVar, @NonNull k kVar, @NonNull qp qpVar, @NonNull dn dnVar) {
        this.a = context;
        this.b = pVar;
        this.c = kVar;
        this.d = qpVar;
        this.f = dnVar;
        this.e = dnVar.d();
    }

    public ac a(ContentValues contentValues) {
        this.g = contentValues;
        return this;
    }

    public ac a(@NonNull we weVar) {
        this.h = weVar;
        return this;
    }

    public void a() {
        d();
    }

    private void d() {
        JSONObject jSONObject = new JSONObject();
        try {
            a(jSONObject);
        } catch (Throwable unused) {
            jSONObject = new JSONObject();
        }
        this.g.put("report_request_parameters", jSONObject.toString());
    }

    private void a(@NonNull JSONObject jSONObject) throws JSONException {
        jSONObject.putOpt("dId", this.h.s()).putOpt("uId", this.h.u()).putOpt("appVer", this.h.r()).putOpt("appBuild", this.h.q()).putOpt("analyticsSdkVersionName", this.h.j()).putOpt("kitBuildNumber", this.h.k()).putOpt("kitBuildType", this.h.l()).putOpt("osVer", this.h.o()).putOpt("osApiLev", Integer.valueOf(this.h.p())).putOpt("lang", this.h.B()).putOpt("root", this.h.v()).putOpt("app_debuggable", this.h.G()).putOpt("app_framework", this.h.w()).putOpt("attribution_id", Integer.valueOf(this.h.Y())).putOpt("commit_hash", this.h.F());
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull dp dpVar) throws JSONException {
        abc.a(jSONObject, dpVar);
    }

    private void e() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(String.ENABLED, this.h.Q());
            dp b2 = b();
            if (b2 != null) {
                a(jSONObject, b2);
            }
            this.g.put("location_info", jSONObject.toString());
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public dp b() {
        Location location;
        dp dpVar = null;
        if (this.h.Q()) {
            location = this.h.R();
            if (location == null) {
                location = this.d.a();
            } else {
                dpVar = dp.a(location);
            }
        } else {
            location = null;
        }
        return (dpVar != null || location == null) ? dpVar : dp.b(location);
    }

    private void f() {
        JSONArray jSONArray = (JSONArray) this.e.a();
        if (jSONArray != null) {
            this.g.put("wifi_network_info", jSONArray.toString());
        }
    }

    private void a(za zaVar) {
        zaVar.a((zc) new zc() {
            public void a(zb[] zbVarArr) {
                ac.this.g.put("cell_info", abc.a(zbVarArr).toString());
            }
        });
    }

    private void a(a aVar) {
        this.g.put("app_environment", aVar.a);
        this.g.put("app_environment_revision", Long.valueOf(aVar.b));
    }

    private void g() {
        zk l = as.a().l();
        l.a((zn) new zn() {
            public void a(zm zmVar) {
                zb b = zmVar.b();
                if (b != null) {
                    ac.this.g.put("cellular_connection_type", b.g());
                }
            }
        });
        a((za) l);
    }

    public void a(@NonNull aci aci, @NonNull a aVar) {
        aa aaVar = aci.a;
        this.g.put("name", aaVar.d());
        this.g.put("value", aaVar.e());
        this.g.put("type", Integer.valueOf(aaVar.g()));
        this.g.put("custom_type", Integer.valueOf(aaVar.h()));
        this.g.put("error_environment", aaVar.j());
        this.g.put("user_info", aaVar.l());
        this.g.put("truncated", Integer.valueOf(aaVar.o()));
        this.g.put(TapjoyConstants.TJC_CONNECTION_TYPE, Integer.valueOf(cg.e(this.a)));
        this.g.put("profile_id", aaVar.p());
        this.g.put("encrypting_mode", Integer.valueOf(aci.b.a()));
        this.g.put("first_occurrence_status", Integer.valueOf(aci.a.q().d));
        a(aVar);
        e();
        h();
        i();
        j();
    }

    private void h() {
        g();
        f();
        c();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void c() {
        String b2 = this.f.b(this.a);
        if (!TextUtils.isEmpty(b2)) {
            int c2 = this.f.c(this.a);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ssid", b2);
                jSONObject.put("state", c2);
                this.g.put("wifi_access_point", jSONObject.toString());
            } catch (Throwable unused) {
            }
        }
    }

    private void i() {
        this.g.put("battery_charge_type", Integer.valueOf(this.b.d().a()));
    }

    private void j() {
        this.g.put("collection_mode", qm.a.a(this.c.c()).a());
    }
}
