package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashSet;

public class df {
    private final a a;
    @Nullable
    private Boolean b;
    private final HashSet<String> c = new HashSet<>();
    private final HashSet<String> d = new HashSet<>();

    public interface a {
        @Nullable
        Boolean a();

        void a(boolean z);
    }

    public static class b implements a {
        private final mq a;

        public b(@NonNull mq mqVar) {
            this.a = mqVar;
        }

        public void a(boolean z) {
            this.a.e(z).q();
        }

        @Nullable
        public Boolean a() {
            return this.a.h();
        }
    }

    public df(@NonNull a aVar) {
        this.a = aVar;
        this.b = this.a.a();
    }

    public synchronized void a(@Nullable Boolean bool) {
        if (dl.a((Object) bool) || this.b == null) {
            this.b = Boolean.valueOf(aau.c(bool));
            this.a.a(this.b.booleanValue());
        }
    }

    public synchronized void a(@NonNull String str, @Nullable Boolean bool) {
        if (dl.a((Object) bool) || (!this.d.contains(str) && !this.c.contains(str))) {
            if (((Boolean) abw.b(bool, Boolean.valueOf(true))).booleanValue()) {
                this.d.add(str);
                this.c.remove(str);
            } else {
                this.c.add(str);
                this.d.remove(str);
            }
        }
    }

    public synchronized boolean a() {
        boolean z;
        z = this.b == null ? this.d.isEmpty() && this.c.isEmpty() : this.b.booleanValue();
        return z;
    }

    public synchronized boolean b() {
        return this.b == null ? this.d.isEmpty() : this.b.booleanValue();
    }

    public synchronized boolean c() {
        return e();
    }

    public synchronized boolean d() {
        return e();
    }

    private boolean e() {
        if (this.b == null) {
            return !this.c.isEmpty() || this.d.isEmpty();
        }
        return this.b.booleanValue();
    }
}
