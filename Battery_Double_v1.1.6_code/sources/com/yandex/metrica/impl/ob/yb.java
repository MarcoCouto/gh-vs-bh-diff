package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class yb {
    @Nullable
    public final xo A;
    @Nullable
    public final List<sr> B;
    @NonNull
    public final xr C;
    @Nullable
    public final xn D;
    @NonNull
    public final xq E;
    public final long F;
    public final long G;
    public final boolean H;
    @Nullable
    public final xi I;
    @Nullable
    public final aap J;
    @Nullable
    public final aah K;
    @Nullable
    public final aah L;
    @Nullable
    public final String a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final String d;
    @Nullable
    public final List<String> e;
    @Nullable
    public final String f;
    @Nullable
    public final String g;
    @Nullable
    public final String h;
    @Nullable
    public final List<String> i;
    @Nullable
    public final List<String> j;
    @Nullable
    public final List<String> k;
    @Nullable
    public final List<String> l;
    @Nullable
    public final String m;
    @Nullable
    public final String n;
    @NonNull
    public final xk o;
    @NonNull
    public final List<qm> p;
    @Nullable
    public final rr q;
    @Nullable
    public final xs r;
    @Nullable
    public final String s;
    @Nullable
    public final String t;
    public final long u;
    public final boolean v;
    public final boolean w;
    @Nullable
    public final List<xp> x;
    @Nullable
    public final String y;
    @Nullable
    public final yd z;

    public static class a {
        @Nullable
        xq A;
        @Nullable
        rr B;
        @Nullable
        aap C;
        @Nullable
        aah D;
        @Nullable
        aah E;
        /* access modifiers changed from: private */
        @Nullable
        public List<xp> F;
        /* access modifiers changed from: private */
        @Nullable
        public String G;
        /* access modifiers changed from: private */
        @Nullable
        public List<sr> H;
        /* access modifiers changed from: private */
        @NonNull
        public xr I;
        /* access modifiers changed from: private */
        public long J;
        /* access modifiers changed from: private */
        public long K;
        /* access modifiers changed from: private */
        @Nullable
        public xn L;
        @Nullable
        String a;
        @Nullable
        String b;
        @Nullable
        String c;
        @Nullable
        String d;
        @Nullable
        List<String> e;
        @Nullable
        String f;
        @Nullable
        String g;
        @Nullable
        String h;
        @Nullable
        List<String> i;
        @Nullable
        List<String> j;
        @Nullable
        List<String> k;
        @Nullable
        List<String> l;
        @Nullable
        String m;
        @Nullable
        String n;
        @NonNull
        final xk o;
        @Nullable
        List<qm> p;
        @Nullable
        xs q;
        @Nullable
        xo r;
        long s;
        boolean t;
        @Nullable
        String u;
        @Nullable
        String v;
        boolean w;
        @Nullable
        yd x;
        boolean y;
        @Nullable
        xi z;

        public a(@NonNull xk xkVar) {
            this.o = xkVar;
        }

        public a a(@Nullable String str) {
            this.a = str;
            return this;
        }

        public a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        public a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        public a d(@Nullable String str) {
            this.d = str;
            return this;
        }

        public a a(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        public a e(@Nullable String str) {
            this.f = str;
            return this;
        }

        public a f(@Nullable String str) {
            this.g = str;
            return this;
        }

        public a g(@Nullable String str) {
            this.h = str;
            return this;
        }

        public a b(@Nullable List<String> list) {
            this.i = list;
            return this;
        }

        public a c(@Nullable List<String> list) {
            this.j = list;
            return this;
        }

        public a d(@Nullable List<String> list) {
            this.k = list;
            return this;
        }

        public a e(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        public a h(@Nullable String str) {
            this.m = str;
            return this;
        }

        public a i(@Nullable String str) {
            this.n = str;
            return this;
        }

        public a f(@Nullable List<qm> list) {
            this.p = list;
            return this;
        }

        public a a(@Nullable xs xsVar) {
            this.q = xsVar;
            return this;
        }

        public a a(@Nullable xo xoVar) {
            this.r = xoVar;
            return this;
        }

        public a j(@Nullable String str) {
            this.u = str;
            return this;
        }

        public a k(@Nullable String str) {
            this.v = str;
            return this;
        }

        public a a(long j2) {
            this.s = j2;
            return this;
        }

        public a a(boolean z2) {
            this.t = z2;
            return this;
        }

        public a b(boolean z2) {
            this.w = z2;
            return this;
        }

        public a g(@Nullable List<xp> list) {
            this.F = list;
            return this;
        }

        public a l(@Nullable String str) {
            this.G = str;
            return this;
        }

        public a h(@Nullable List<sr> list) {
            this.H = list;
            return this;
        }

        public a a(@NonNull xr xrVar) {
            this.I = xrVar;
            return this;
        }

        public a b(long j2) {
            this.J = j2;
            return this;
        }

        public a c(long j2) {
            this.K = j2;
            return this;
        }

        public a a(yd ydVar) {
            this.x = ydVar;
            return this;
        }

        public a c(boolean z2) {
            this.y = z2;
            return this;
        }

        public a a(@Nullable xn xnVar) {
            this.L = xnVar;
            return this;
        }

        public a a(@Nullable xi xiVar) {
            this.z = xiVar;
            return this;
        }

        public a a(@Nullable xq xqVar) {
            this.A = xqVar;
            return this;
        }

        public a a(@Nullable rr rrVar) {
            this.B = rrVar;
            return this;
        }

        public a a(@NonNull aap aap) {
            this.C = aap;
            return this;
        }

        public a a(@NonNull aah aah) {
            this.D = aah;
            return this;
        }

        public a b(@NonNull aah aah) {
            this.E = aah;
            return this;
        }

        @NonNull
        public yb a() {
            return new yb(this);
        }
    }

    private yb(@NonNull a aVar) {
        List<String> list;
        List<String> list2;
        List<String> list3;
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        List<xp> list4 = null;
        this.e = aVar.e == null ? null : Collections.unmodifiableList(aVar.e);
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i == null ? null : Collections.unmodifiableList(aVar.i);
        if (aVar.j == null) {
            list = null;
        } else {
            list = Collections.unmodifiableList(aVar.j);
        }
        this.j = list;
        if (aVar.k == null) {
            list2 = null;
        } else {
            list2 = Collections.unmodifiableList(aVar.k);
        }
        this.k = list2;
        if (aVar.l == null) {
            list3 = null;
        } else {
            list3 = Collections.unmodifiableList(aVar.l);
        }
        this.l = list3;
        this.m = aVar.m;
        this.n = aVar.n;
        this.o = aVar.o;
        this.p = aVar.p == null ? new ArrayList<>() : aVar.p;
        this.r = aVar.q;
        this.A = aVar.r;
        this.s = aVar.u;
        this.t = aVar.v;
        this.u = aVar.s;
        this.v = aVar.t;
        this.w = aVar.w;
        if (aVar.F != null) {
            list4 = Collections.unmodifiableList(aVar.F);
        }
        this.x = list4;
        this.y = aVar.G;
        this.B = aVar.H;
        this.C = aVar.I;
        this.z = aVar.x;
        this.F = aVar.J;
        this.G = aVar.K;
        this.H = aVar.y;
        this.D = aVar.L;
        this.I = aVar.z;
        this.q = aVar.B;
        if (aVar.A == null) {
            com.yandex.metrica.impl.ob.ve.a aVar2 = new com.yandex.metrica.impl.ob.ve.a();
            this.E = new xq(aVar2.I, aVar2.J);
        } else {
            this.E = aVar.A;
        }
        this.J = aVar.C;
        this.K = aVar.D;
        this.L = aVar.E;
    }

    public a a() {
        return new a(this.o).a(this.a).b(this.b).c(this.c).d(this.d).c(this.j).d(this.k).h(this.m).a(this.e).b(this.i).e(this.f).f(this.g).g(this.h).e(this.l).j(this.s).k(this.t).f(this.p).a(this.r).i(this.n).b(this.w).a(this.u).a(this.v).g(this.x).l(this.y).h(this.B).a(this.A).a(this.C).b(this.F).c(this.G).a(this.z).c(this.H).a(this.D).a(this.I).a(this.E).a(this.q).a(this.E).a(this.J).a(this.K).b(this.L);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StartupState{uuid='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", deviceID='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", deviceID2='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", deviceIDHash='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", reportUrls=");
        sb.append(this.e);
        sb.append(", getAdUrl='");
        sb.append(this.f);
        sb.append('\'');
        sb.append(", reportAdUrl='");
        sb.append(this.g);
        sb.append('\'');
        sb.append(", sdkListUrl='");
        sb.append(this.h);
        sb.append('\'');
        sb.append(", locationUrls=");
        sb.append(this.i);
        sb.append(", hostUrlsFromStartup=");
        sb.append(this.j);
        sb.append(", hostUrlsFromClient=");
        sb.append(this.k);
        sb.append(", diagnosticUrls=");
        sb.append(this.l);
        sb.append(", encodedClidsFromResponse='");
        sb.append(this.m);
        sb.append('\'');
        sb.append(", lastStartupRequestClids='");
        sb.append(this.n);
        sb.append('\'');
        sb.append(", collectingFlags=");
        sb.append(this.o);
        sb.append(", locationCollectionConfigs=");
        sb.append(this.p);
        sb.append(", wakeupConfig=");
        sb.append(this.q);
        sb.append(", socketConfig=");
        sb.append(this.r);
        sb.append(", distributionReferrer='");
        sb.append(this.s);
        sb.append('\'');
        sb.append(", referrerSource='");
        sb.append(this.t);
        sb.append('\'');
        sb.append(", obtainTime=");
        sb.append(this.u);
        sb.append(", hadFirstStartup=");
        sb.append(this.v);
        sb.append(", startupResponseClidsMatchClientClids=");
        sb.append(this.w);
        sb.append(", requests=");
        sb.append(this.x);
        sb.append(", countryInit='");
        sb.append(this.y);
        sb.append('\'');
        sb.append(", statSending=");
        sb.append(this.z);
        sb.append(", permissionsCollectingConfig=");
        sb.append(this.A);
        sb.append(", permissions=");
        sb.append(this.B);
        sb.append(", sdkFingerprintingConfig=");
        sb.append(this.C);
        sb.append(", identityLightCollectingConfig=");
        sb.append(this.D);
        sb.append(", retryPolicyConfig=");
        sb.append(this.E);
        sb.append(", obtainServerTime=");
        sb.append(this.F);
        sb.append(", firstStartupServerTime=");
        sb.append(this.G);
        sb.append(", outdated=");
        sb.append(this.H);
        sb.append(", bleCollectingConfig=");
        sb.append(this.I);
        sb.append(", uiParsingConfig=");
        sb.append(this.J);
        sb.append(", uiEventCollectingConfig=");
        sb.append(this.K);
        sb.append(", uiCollectingForBridgeConfig=");
        sb.append(this.L);
        sb.append('}');
        return sb.toString();
    }
}
