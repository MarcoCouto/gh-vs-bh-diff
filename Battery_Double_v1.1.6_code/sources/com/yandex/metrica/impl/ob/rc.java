package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

public class rc implements ph, qn {
    @Nullable
    private pt a;

    public rc(@Nullable pt ptVar) {
        this.a = ptVar;
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean b() {
        pt ptVar = this.a;
        return ptVar != null && ptVar.a.a;
    }

    public void a(@Nullable pt ptVar) {
        this.a = ptVar;
    }
}
