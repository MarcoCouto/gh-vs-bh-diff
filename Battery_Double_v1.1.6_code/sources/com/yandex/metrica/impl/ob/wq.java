package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.h.b;
import com.yandex.metrica.impl.ob.op.a;

public class wq {
    /* access modifiers changed from: private */
    @NonNull
    public final wp a;
    @NonNull
    private final mx<wr> b;
    @NonNull
    private final dj c;
    @NonNull
    private final act d;
    @NonNull
    private final b e;
    @NonNull
    private final h f;
    /* access modifiers changed from: private */
    @NonNull
    public final wo g;
    /* access modifiers changed from: private */
    public boolean h;
    @Nullable
    private xr i;
    private boolean j;
    private long k;
    private long l;
    private long m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    private final Object q;

    public wq(@NonNull Context context, @NonNull act act) {
        this(new wp(context, null, act), a.a(wr.class).a(context), new dj(), act, as.a().j());
    }

    @VisibleForTesting
    wq(@NonNull wp wpVar, @NonNull mx<wr> mxVar, @NonNull dj djVar, @NonNull act act, @NonNull h hVar) {
        this.p = false;
        this.q = new Object();
        this.a = wpVar;
        this.b = mxVar;
        this.g = new wo(mxVar, new a() {
            public void a() {
                wq.this.c();
                wq.this.h = false;
            }
        });
        this.c = djVar;
        this.d = act;
        this.e = new b() {
            public void a() {
                wq.this.p = true;
                wq.this.a.a(wq.this.g);
            }
        };
        this.f = hVar;
    }

    public void a(@Nullable yb ybVar) {
        c();
        b(ybVar);
    }

    private void d() {
        if (this.o) {
            f();
        } else {
            g();
        }
    }

    private void e() {
        if (this.k - this.l >= this.i.b) {
            b();
        }
    }

    private void f() {
        if (this.c.b(this.m, this.i.d, "should retry sdk collecting")) {
            b();
        }
    }

    private void g() {
        if (this.c.b(this.m, this.i.a, "should collect sdk as usual")) {
            b();
        }
    }

    public void a() {
        synchronized (this.q) {
            if (this.j && this.i != null) {
                if (this.n) {
                    d();
                } else {
                    e();
                }
            }
        }
    }

    public void b(@Nullable yb ybVar) {
        boolean c2 = c(ybVar);
        synchronized (this.q) {
            if (ybVar != null) {
                try {
                    this.j = ybVar.o.e;
                    this.i = ybVar.C;
                    this.k = ybVar.F;
                    this.l = ybVar.G;
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
            this.a.a(ybVar);
        }
        if (c2) {
            a();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (!this.h) {
            this.h = true;
            if (!this.p) {
                this.f.a(this.i.c, this.d, this.e);
            } else {
                this.a.a(this.g);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        wr wrVar = (wr) this.b.a();
        this.m = wrVar.c;
        this.n = wrVar.d;
        this.o = wrVar.e;
    }

    private boolean c(@Nullable yb ybVar) {
        boolean z = false;
        if (ybVar == null) {
            return false;
        }
        if ((!this.j && ybVar.o.e) || this.i == null || !this.i.equals(ybVar.C) || this.k != ybVar.F || this.l != ybVar.G || this.a.b(ybVar)) {
            z = true;
        }
        return z;
    }
}
