package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.k.a;
import com.yandex.metrica.impl.ob.p.a.C0090a;
import java.util.List;

public class qu {
    @NonNull
    public final List<C0090a> a;
    @NonNull
    public final List<a> b;

    public qu(@NonNull List<C0090a> list, @NonNull List<a> list2) {
        this.a = list;
        this.b = list2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Preconditions{possibleChargeTypes=");
        sb.append(this.a);
        sb.append(", appStatuses=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
