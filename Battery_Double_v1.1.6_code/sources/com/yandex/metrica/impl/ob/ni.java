package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a.b;
import com.yandex.metrica.impl.ob.xk.a;

public class ni implements np<xk, b> {
    @NonNull
    /* renamed from: a */
    public b b(@NonNull xk xkVar) {
        b bVar = new b();
        bVar.e = xkVar.d;
        bVar.d = xkVar.c;
        bVar.c = xkVar.b;
        bVar.b = xkVar.a;
        bVar.o = xkVar.e;
        bVar.p = xkVar.f;
        bVar.q = xkVar.g;
        bVar.f = xkVar.o;
        bVar.g = xkVar.p;
        bVar.k = xkVar.u;
        bVar.j = xkVar.s;
        bVar.n = xkVar.t;
        bVar.m = xkVar.w;
        bVar.l = xkVar.v;
        bVar.h = xkVar.q;
        bVar.t = xkVar.j;
        bVar.i = xkVar.r;
        bVar.s = xkVar.i;
        bVar.r = xkVar.h;
        bVar.u = xkVar.k;
        bVar.v = xkVar.l;
        bVar.w = xkVar.n;
        bVar.x = xkVar.m;
        return bVar;
    }

    @NonNull
    public xk a(@NonNull b bVar) {
        return new a().a(bVar.b).n(bVar.m).m(bVar.l).l(bVar.k).k(bVar.j).o(bVar.n).j(bVar.i).i(bVar.h).h(bVar.g).g(bVar.f).d(bVar.e).e(bVar.o).p(bVar.p).f(bVar.q).c(bVar.d).b(bVar.c).o(bVar.n).s(bVar.t).r(bVar.s).q(bVar.r).t(bVar.u).u(bVar.v).v(bVar.w).w(bVar.x).a();
    }
}
