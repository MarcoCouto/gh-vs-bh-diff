package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.al.a;

public class ack {
    @NonNull
    private final abi<acl, acj> a;
    @NonNull
    private final abi<a, acj> b;

    public ack() {
        this(new ach(), new acm(), new ace());
    }

    public ack(@NonNull acj acj, @NonNull acj acj2, @NonNull acj acj3) {
        this.a = new abi<>(acj);
        this.a.a(acl.NONE, acj);
        this.a.a(acl.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER, acj2);
        this.a.a(acl.AES_VALUE_ENCRYPTION, acj3);
        this.b = new abi<>(acj);
        this.b.a(a.EVENT_TYPE_IDENTITY, acj3);
    }

    @NonNull
    public acj a(acl acl) {
        return (acj) this.a.a(acl);
    }

    @NonNull
    public acj a(@NonNull aa aaVar) {
        return a(a.a(aaVar.g()));
    }

    @NonNull
    private acj a(@NonNull a aVar) {
        return (acj) this.b.a(aVar);
    }
}
