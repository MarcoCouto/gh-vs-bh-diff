package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public abstract class gc implements gg {
    @NonNull
    private final Context a;
    @NonNull
    private final ft b;
    @NonNull
    private final rp c;

    /* access modifiers changed from: protected */
    public abstract void b(@NonNull aa aaVar, @NonNull ew ewVar);

    public gc(@NonNull Context context, @NonNull ft ftVar) {
        this(context, ftVar, new rp(qp.a(context), as.a().l(), dn.a(context), new mq(lv.a(context).c())));
    }

    public void a(@NonNull aa aaVar, @NonNull ew ewVar) {
        b(aaVar, ewVar);
    }

    public void a() {
        this.b.b(this);
        this.c.a((Object) this);
    }

    @NonNull
    public ft b() {
        return this.b;
    }

    @VisibleForTesting
    gc(@NonNull Context context, @NonNull ft ftVar, @NonNull rp rpVar) {
        this.a = context.getApplicationContext();
        this.b = ftVar;
        this.c = rpVar;
        this.b.a((gg) this);
        this.c.b(this);
    }

    @NonNull
    public rp c() {
        return this.c;
    }
}
