package com.yandex.metrica.impl.ob;

import android.support.annotation.VisibleForTesting;
import android.util.Log;

public abstract class aat {
    private volatile boolean a = false;

    private static String e(String str) {
        return str == null ? "" : str;
    }

    /* access modifiers changed from: 0000 */
    public abstract String d(String str, Object[] objArr);

    /* access modifiers changed from: 0000 */
    public abstract String f();

    /* access modifiers changed from: 0000 */
    public abstract String g();

    public void a() {
        this.a = true;
    }

    public void b() {
        this.a = false;
    }

    public boolean c() {
        return this.a;
    }

    public aat(boolean z) {
        this.a = z;
    }

    public void a(String str) {
        a(4, str);
    }

    public void b(String str) {
        a(5, str);
    }

    public void c(String str) {
        a(6, str);
    }

    public void a(String str, Object... objArr) {
        a(4, str, objArr);
    }

    public void b(String str, Object... objArr) {
        a(5, str, objArr);
    }

    public void c(String str, Object... objArr) {
        Log.println(5, f(), e(str, objArr));
    }

    public void a(Throwable th, String str, Object... objArr) {
        String f = f();
        StringBuilder sb = new StringBuilder();
        sb.append(e(str, objArr));
        sb.append("\n");
        sb.append(Log.getStackTraceString(th));
        Log.println(6, f, sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, String str) {
        if (d()) {
            Log.println(i, f(), d(str));
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, String str, Object... objArr) {
        if (d()) {
            Log.println(i, f(), e(str, objArr));
        }
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.a;
    }

    private String d(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(g());
        sb.append(e(str));
        return sb.toString();
    }

    private String e(String str, Object[] objArr) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(g());
            sb.append(d(e(str), objArr));
            return sb.toString();
        } catch (Throwable unused) {
            return e();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public String e() {
        return g();
    }
}
