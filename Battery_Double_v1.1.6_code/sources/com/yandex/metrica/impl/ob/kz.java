package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class kz {
    @NonNull
    public final ky a;
    @Nullable
    public final String b;
    @Nullable
    public final Boolean c;

    public kz(@NonNull ky kyVar, @Nullable String str, @Nullable Boolean bool) {
        this.a = kyVar;
        this.b = str;
        this.c = bool;
    }
}
