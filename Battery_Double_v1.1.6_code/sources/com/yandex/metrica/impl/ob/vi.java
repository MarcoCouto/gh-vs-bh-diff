package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.f;
import com.yandex.metrica.g;
import com.yandex.metrica.profile.UserProfile;
import java.util.List;
import java.util.Map;

public class vi implements ay {
    @NonNull
    private final vl a;
    @NonNull
    private final vq b;
    @NonNull
    private final act c;
    @NonNull
    private final Context d;
    @NonNull
    private final vn e;
    @NonNull
    private final f f;
    @NonNull
    private final g g;

    public vi(@NonNull act act, @NonNull Context context, @NonNull String str) {
        this(act, context, str, new vl());
    }

    private vi(@NonNull act act, @NonNull Context context, @NonNull String str, @NonNull vl vlVar) {
        this(act, context, new vq(), vlVar, new vn(), new g(vlVar), f.a(str).b());
    }

    @VisibleForTesting
    vi(@NonNull act act, @NonNull Context context, @NonNull vq vqVar, @NonNull vl vlVar, @NonNull vn vnVar, @NonNull g gVar, @NonNull f fVar) {
        this.c = act;
        this.d = context;
        this.b = vqVar;
        this.a = vlVar;
        this.e = vnVar;
        this.g = gVar;
        this.f = fVar;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    @NonNull
    public final ay a() {
        return this.a.a(this.d).b(this.f);
    }

    public void a(@NonNull final ld ldVar) {
        this.g.a(ldVar);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().a(ldVar);
            }
        });
    }

    public void a(@NonNull final ky kyVar) {
        this.g.a(kyVar);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().a(kyVar);
            }
        });
    }

    public void e() {
        this.g.d();
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().e();
            }
        });
    }

    public void sendEventsBuffer() {
        this.b.sendEventsBuffer();
        this.g.a();
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().sendEventsBuffer();
            }
        });
    }

    public void a(@Nullable final String str, @Nullable final String str2) {
        this.b.a(str, str2);
        this.g.a(str, str2);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().a(str, str2);
            }
        });
    }

    public void b(@NonNull final String str, @Nullable final String str2) {
        this.b.b(str, str2);
        this.g.c(str, str2);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().b(str, str2);
            }
        });
    }

    public void reportEvent(@NonNull final String str) {
        this.b.reportEvent(str);
        this.g.a(str);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportEvent(str);
            }
        });
    }

    public void reportEvent(@NonNull final String str, @Nullable final String str2) {
        this.b.reportEvent(str, str2);
        this.g.b(str, str2);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportEvent(str, str2);
            }
        });
    }

    public void reportEvent(@NonNull final String str, @Nullable Map<String, Object> map) {
        this.b.reportEvent(str, (Map) map);
        this.g.a(str, map);
        final List c2 = dl.c(map);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportEvent(str, dl.a(c2));
            }
        });
    }

    public void reportError(@NonNull final String str, @Nullable Throwable th) {
        this.b.reportError(str, th);
        final Throwable a2 = this.g.a(str, th);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportError(str, a2);
            }
        });
    }

    public void reportUnhandledException(@NonNull final Throwable th) {
        this.b.reportUnhandledException(th);
        this.g.a(th);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportUnhandledException(th);
            }
        });
    }

    public void resumeSession() {
        this.b.resumeSession();
        this.g.b();
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().resumeSession();
            }
        });
    }

    public void pauseSession() {
        this.b.pauseSession();
        this.g.c();
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().pauseSession();
            }
        });
    }

    public void setUserProfileID(@Nullable final String str) {
        this.b.setUserProfileID(str);
        this.g.b(str);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().setUserProfileID(str);
            }
        });
    }

    public void reportUserProfile(@NonNull final UserProfile userProfile) {
        this.b.reportUserProfile(userProfile);
        this.g.a(userProfile);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportUserProfile(userProfile);
            }
        });
    }

    public void reportRevenue(@NonNull final Revenue revenue) {
        this.b.reportRevenue(revenue);
        this.g.a(revenue);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().reportRevenue(revenue);
            }
        });
    }

    public void setStatisticsSending(final boolean z) {
        this.b.setStatisticsSending(z);
        this.g.a(z);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.a().setStatisticsSending(z);
            }
        });
    }

    public void a(@NonNull String str) {
        final f b2 = f.a(str).b();
        this.g.a((ReporterConfig) b2);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.b(b2);
            }
        });
    }

    public void a(@NonNull f fVar) {
        final f a2 = this.e.a(fVar);
        this.g.a((ReporterConfig) a2);
        this.c.a((Runnable) new Runnable() {
            public void run() {
                vi.this.b(a2);
            }
        });
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void b(@NonNull f fVar) {
        this.a.a(this.d).a(fVar);
    }
}
