package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a.i;

public class oc implements np<xs, i> {
    @NonNull
    /* renamed from: a */
    public i b(@NonNull xs xsVar) {
        i iVar = new i();
        iVar.b = xsVar.a;
        iVar.c = xsVar.b;
        iVar.d = aav.a(xsVar.c);
        iVar.e = xsVar.d;
        iVar.f = xsVar.e;
        return iVar;
    }

    @NonNull
    public xs a(@NonNull i iVar) {
        xs xsVar = new xs(iVar.b, iVar.c, aav.a(iVar.d), iVar.e, iVar.f);
        return xsVar;
    }
}
