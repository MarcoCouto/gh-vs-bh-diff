package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.p.a.C0090a;
import com.yandex.metrica.impl.ob.qm.a;
import org.json.JSONArray;

public class qk {
    @Nullable
    private Long a;
    private long b;
    private long c;
    @Nullable
    private JSONArray d;
    @Nullable
    private JSONArray e;
    @Nullable
    private C0090a f;
    @Nullable
    private a g;

    @Nullable
    public Long a() {
        return this.a;
    }

    public void a(@Nullable Long l) {
        this.a = l;
    }

    public long b() {
        return this.b;
    }

    public void a(long j) {
        this.b = j;
    }

    @Nullable
    public JSONArray c() {
        return this.d;
    }

    public void a(@Nullable JSONArray jSONArray) {
        this.d = jSONArray;
    }

    @Nullable
    public JSONArray d() {
        return this.e;
    }

    public void b(@Nullable JSONArray jSONArray) {
        this.e = jSONArray;
    }

    public long e() {
        return this.c;
    }

    public void b(long j) {
        this.c = j;
    }

    @Nullable
    public C0090a f() {
        return this.f;
    }

    public void a(@NonNull C0090a aVar) {
        this.f = aVar;
    }

    @Nullable
    public a g() {
        return this.g;
    }

    public void a(@NonNull a aVar) {
        this.g = aVar;
    }
}
