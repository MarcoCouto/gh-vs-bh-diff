package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class jq {
    @NonNull
    private final jy a;
    @Nullable
    private final Long b;
    @Nullable
    private final Long c;
    @Nullable
    private final Integer d;
    @Nullable
    private final Long e;
    @Nullable
    private final Boolean f;
    @Nullable
    private final Long g;
    @Nullable
    private final Long h;

    static final class a {
        @Nullable
        public Long a;
        /* access modifiers changed from: private */
        @NonNull
        public jy b;
        /* access modifiers changed from: private */
        @Nullable
        public Long c;
        /* access modifiers changed from: private */
        @Nullable
        public Long d;
        /* access modifiers changed from: private */
        @Nullable
        public Integer e;
        /* access modifiers changed from: private */
        @Nullable
        public Long f;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean g;
        /* access modifiers changed from: private */
        @Nullable
        public Long h;

        private a(js jsVar) {
            this.b = jsVar.a();
            this.e = jsVar.b();
        }

        public a a(Long l) {
            this.c = l;
            return this;
        }

        public a b(Long l) {
            this.d = l;
            return this;
        }

        public a c(Long l) {
            this.f = l;
            return this;
        }

        public a a(Boolean bool) {
            this.g = bool;
            return this;
        }

        public a d(Long l) {
            this.h = l;
            return this;
        }

        public a e(Long l) {
            this.a = l;
            return this;
        }

        public jq a() {
            return new jq(this);
        }
    }

    private jq(a aVar) {
        this.a = aVar.b;
        this.d = aVar.e;
        this.b = aVar.c;
        this.c = aVar.d;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.a;
    }

    public static final a a(js jsVar) {
        return new a(jsVar);
    }

    public jy a() {
        return this.a;
    }

    public long a(long j) {
        return this.b == null ? j : this.b.longValue();
    }

    public long b(long j) {
        return this.c == null ? j : this.c.longValue();
    }

    public int a(int i) {
        return this.d == null ? i : this.d.intValue();
    }

    public long c(long j) {
        return this.e == null ? j : this.e.longValue();
    }

    public boolean a(boolean z) {
        return this.f == null ? z : this.f.booleanValue();
    }

    public long d(long j) {
        return this.g == null ? j : this.g.longValue();
    }

    public long e(long j) {
        return this.h == null ? j : this.h.longValue();
    }
}
