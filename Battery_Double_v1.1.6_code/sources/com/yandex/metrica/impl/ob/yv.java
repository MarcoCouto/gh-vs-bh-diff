package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinMediationProvider;
import com.facebook.share.internal.ShareConstants;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.abc.a;
import com.yandex.metrica.impl.ob.ve.a.C0102a;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a.C0104a;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a.c;
import com.yandex.metrica.impl.ob.ve.a.C0102a.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class yv {
    private static final Map<String, Integer> a;
    private static final Map<String, Integer> b;
    private static final Map<String, Integer> c;
    private static final Map<String, Integer> d;
    @NonNull
    private final of e;

    static {
        HashMap hashMap = new HashMap(3);
        hashMap.put("all_matches", Integer.valueOf(1));
        hashMap.put("first_match", Integer.valueOf(2));
        hashMap.put("match_lost", Integer.valueOf(3));
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap(2);
        hashMap2.put("aggressive", Integer.valueOf(1));
        hashMap2.put("sticky", Integer.valueOf(2));
        b = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap(3);
        hashMap3.put("one", Integer.valueOf(1));
        hashMap3.put("few", Integer.valueOf(2));
        hashMap3.put(AppLovinMediationProvider.MAX, Integer.valueOf(3));
        c = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("low_power", Integer.valueOf(1));
        hashMap4.put("balanced", Integer.valueOf(2));
        hashMap4.put("low_latency", Integer.valueOf(3));
        d = Collections.unmodifiableMap(hashMap4);
    }

    public yv() {
        this(new of());
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        yxVar.a(this.e.a(a((JSONObject) aVar)));
    }

    @NonNull
    private C0102a a(@NonNull JSONObject jSONObject) {
        C0102a aVar = new C0102a();
        JSONObject optJSONObject = jSONObject.optJSONObject("ble_collecting");
        if (optJSONObject != null) {
            aVar.b = b(optJSONObject.optJSONObject("scan_settings"));
            aVar.c = a(optJSONObject.optJSONArray(ShareConstants.WEB_DIALOG_PARAM_FILTERS));
            aVar.d = abw.a(abc.a(optJSONObject, "same_beacon_min_reporting_interval"), TimeUnit.SECONDS, aVar.d);
            aVar.e = abw.a(abc.a(optJSONObject, "first_delay_seconds"), TimeUnit.SECONDS, aVar.e);
        } else {
            aVar.b = new b();
        }
        return aVar;
    }

    @NonNull
    private b b(@Nullable JSONObject jSONObject) {
        b bVar = new b();
        if (jSONObject != null) {
            Integer a2 = a(jSONObject, "callback_type", a);
            if (a2 != null) {
                bVar.b = a2.intValue();
            }
            Integer a3 = a(jSONObject, "match_mode", b);
            if (a3 != null) {
                bVar.c = a3.intValue();
            }
            Integer a4 = a(jSONObject, "num_of_matches", c);
            if (a4 != null) {
                bVar.d = a4.intValue();
            }
            Integer a5 = a(jSONObject, "scan_mode", d);
            if (a5 != null) {
                bVar.e = a5.intValue();
            }
            bVar.f = abw.a(abc.a(jSONObject, "report_delay"), TimeUnit.SECONDS, bVar.f);
        }
        return bVar;
    }

    @NonNull
    private C0103a[] a(@Nullable JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                C0103a c2 = c(jSONArray.optJSONObject(i));
                if (c2 != null) {
                    arrayList.add(c2);
                }
            }
        }
        return (C0103a[]) arrayList.toArray(new C0103a[arrayList.size()]);
    }

    @Nullable
    private C0103a c(@Nullable JSONObject jSONObject) {
        C0103a aVar;
        boolean z = true;
        if (jSONObject != null) {
            aVar = new C0103a();
            String optString = jSONObject.optString("device_address", null);
            if (optString != null) {
                aVar.b = optString;
                z = false;
            }
            String optString2 = jSONObject.optString(TapjoyConstants.TJC_DEVICE_NAME, null);
            if (optString2 != null) {
                aVar.c = optString2;
                z = false;
            }
            C0104a d2 = d(jSONObject.optJSONObject("manufacturer_data"));
            if (d2 != null) {
                aVar.d = d2;
                z = false;
            }
            C0103a.b e2 = e(jSONObject.optJSONObject("service_data"));
            if (e2 != null) {
                aVar.e = e2;
                z = false;
            }
            c f = f(jSONObject.optJSONObject("service_uuid"));
            if (f != null) {
                aVar.f = f;
                z = false;
            }
        } else {
            aVar = null;
        }
        if (z) {
            return null;
        }
        return aVar;
    }

    @Nullable
    private C0104a d(@Nullable JSONObject jSONObject) {
        if (jSONObject != null) {
            Integer c2 = abc.c(jSONObject, "id");
            if (c2 != null) {
                C0104a aVar = new C0104a();
                aVar.b = c2.intValue();
                aVar.c = abc.a(jSONObject, "data", aVar.c);
                aVar.d = abc.a(jSONObject, "data_mask", aVar.d);
                return aVar;
            }
        }
        return null;
    }

    @Nullable
    private C0103a.b e(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("uuid", null);
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        C0103a.b bVar = new C0103a.b();
        bVar.b = optString;
        bVar.c = abc.a(jSONObject, "data", bVar.c);
        bVar.d = abc.a(jSONObject, "data_mask", bVar.d);
        return bVar;
    }

    @Nullable
    private c f(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("uuid", null);
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        c cVar = new c();
        cVar.b = optString;
        cVar.c = jSONObject.optString("data_mask", cVar.c);
        return cVar;
    }

    @Nullable
    private Integer a(@NonNull JSONObject jSONObject, @NonNull String str, Map<String, Integer> map) {
        if (jSONObject.has(str)) {
            return (Integer) map.get(jSONObject.optString(str));
        }
        return null;
    }

    @VisibleForTesting
    public yv(@NonNull of ofVar) {
        this.e = ofVar;
    }
}
