package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.al.a;
import java.util.List;

public class im extends in<hu> {
    public im(@NonNull ix ixVar) {
        super(ixVar);
    }

    public void a(@NonNull a aVar, @NonNull List<hu> list) {
        if (a(aVar)) {
            list.add(a().h());
        }
        if (b(aVar)) {
            list.add(a().d());
        }
    }

    private boolean a(@NonNull a aVar) {
        return al.b(aVar);
    }

    private boolean b(@NonNull a aVar) {
        return al.a(aVar);
    }
}
