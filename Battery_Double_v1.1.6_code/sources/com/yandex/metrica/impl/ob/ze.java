package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.telephony.CellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class ze extends za implements r {
    /* access modifiers changed from: private */
    @Nullable
    public final TelephonyManager a;
    /* access modifiers changed from: private */
    public PhoneStateListener b;
    /* access modifiers changed from: private */
    public boolean c;
    private yb d;
    private final com.yandex.metrica.impl.ob.r.a<zm> e;
    private final com.yandex.metrica.impl.ob.r.a<zb[]> f;
    @NonNull
    private final act g;
    private final Context h;
    private final zd i;
    private final zj j;
    private final zg k;
    @NonNull
    private final su l;
    @NonNull
    private so m;

    private class a extends PhoneStateListener {
        private a() {
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            ze.this.c(signalStrength);
        }
    }

    protected ze(@NonNull Context context, @NonNull act act) {
        this(context, new su(), act);
    }

    protected ze(@NonNull Context context, @NonNull su suVar, @NonNull act act) {
        this(context, suVar, new so(suVar.a()), act);
    }

    protected ze(@NonNull Context context, @NonNull su suVar, @NonNull so soVar, @NonNull act act) {
        TelephonyManager telephonyManager;
        this.c = false;
        this.e = new com.yandex.metrica.impl.ob.r.a<>();
        this.f = new com.yandex.metrica.impl.ob.r.a<>();
        this.h = context;
        try {
            telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        } catch (Throwable unused) {
            telephonyManager = null;
        }
        this.a = telephonyManager;
        this.g = act;
        this.g.a((Runnable) new Runnable() {
            public void run() {
                ze.this.b = new a();
            }
        });
        this.i = new zd(this, soVar);
        this.j = new zj(this, soVar);
        this.k = new zg(this, soVar);
        this.l = suVar;
        this.m = soVar;
    }

    public synchronized void a() {
        this.g.a((Runnable) new Runnable() {
            public void run() {
                if (!ze.this.c) {
                    ze.this.c = true;
                    if (ze.this.b != null && ze.this.a != null) {
                        try {
                            ze.this.a.listen(ze.this.b, 256);
                        } catch (Throwable unused) {
                        }
                    }
                }
            }
        });
    }

    public synchronized void b() {
        this.g.a((Runnable) new Runnable() {
            public void run() {
                if (ze.this.c) {
                    ze.this.c = false;
                    eh.a().a((Object) ze.this);
                    if (ze.this.b != null && ze.this.a != null) {
                        try {
                            ze.this.a.listen(ze.this.b, 0);
                        } catch (Throwable unused) {
                        }
                    }
                }
            }
        });
    }

    public synchronized void a(zn znVar) {
        if (znVar != null) {
            znVar.a(e());
        }
    }

    public synchronized void a(zc zcVar) {
        if (zcVar != null) {
            zcVar.a(j());
        }
    }

    @Nullable
    public TelephonyManager c() {
        return this.a;
    }

    public Context d() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public synchronized zm e() {
        zm zmVar;
        if (!this.e.b()) {
            if (!this.e.c()) {
                zmVar = (zm) this.e.a();
            }
        }
        zmVar = new zm(this.i, this.j, this.k);
        zb b2 = zmVar.b();
        if (b2 != null && b2.a() == null && !this.e.b()) {
            zb b3 = ((zm) this.e.a()).b();
            if (b3 != null) {
                zmVar.b().a(b3.a());
            }
        }
        this.e.a(zmVar);
        return zmVar;
    }

    private synchronized zb[] j() {
        zb[] zbVarArr;
        if (!this.f.b()) {
            if (!this.f.c()) {
                zbVarArr = (zb[]) this.f.a();
            }
        }
        zbVarArr = f();
        this.f.a(zbVarArr);
        return zbVarArr;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    @NonNull
    public zb[] f() {
        ArrayList arrayList = new ArrayList();
        if (dl.a(17) && this.m.a(this.h)) {
            try {
                List allCellInfo = this.a.getAllCellInfo();
                if (!dl.a((Collection) allCellInfo)) {
                    for (int i2 = 0; i2 < allCellInfo.size(); i2++) {
                        zb a2 = a((CellInfo) allCellInfo.get(i2));
                        if (a2 != null) {
                            arrayList.add(a2);
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
        if (arrayList.size() >= 1) {
            return (zb[]) arrayList.toArray(new zb[arrayList.size()]);
        }
        zb b2 = e().b();
        if (b2 == null) {
            return new zb[0];
        }
        return new zb[]{b2};
    }

    @Nullable
    @TargetApi(17)
    private zb a(CellInfo cellInfo) {
        return zb.a(cellInfo);
    }

    /* access modifiers changed from: private */
    public synchronized void c(SignalStrength signalStrength) {
        if (!this.e.b() && !this.e.c()) {
            zb b2 = ((zm) this.e.a()).b();
            if (b2 != null) {
                b2.a(Integer.valueOf(a(signalStrength)));
            }
        }
    }

    @VisibleForTesting
    static int a(SignalStrength signalStrength) {
        if (signalStrength.isGsm()) {
            return b(signalStrength);
        }
        int cdmaDbm = signalStrength.getCdmaDbm();
        int evdoDbm = signalStrength.getEvdoDbm();
        if (-120 == evdoDbm) {
            return cdmaDbm;
        }
        return -120 == cdmaDbm ? evdoDbm : Math.min(cdmaDbm, evdoDbm);
    }

    @VisibleForTesting
    static int b(SignalStrength signalStrength) {
        int gsmSignalStrength = signalStrength.getGsmSignalStrength();
        if (99 == gsmSignalStrength) {
            return -1;
        }
        return (gsmSignalStrength * 2) - 113;
    }

    private synchronized boolean k() {
        return this.d != null;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean g() {
        return k() && this.d.o.w;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean h() {
        return k() && this.d.o.v;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean i() {
        return k() && this.d.o.u;
    }

    public void a(@NonNull yb ybVar) {
        this.d = ybVar;
        this.l.a(ybVar);
        this.m.a(this.l.a());
    }

    public void a(boolean z) {
        this.l.a(z);
        this.m.a(this.l.a());
    }
}
