package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationListener;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.gpllibrary.a;
import com.yandex.metrica.gpllibrary.b;

public class qa extends qx implements pz {
    @NonNull
    private final b f;
    @Nullable
    private volatile a.b g;

    public qa(@NonNull Context context, @NonNull rj rjVar, @NonNull Looper looper, @NonNull so soVar) {
        this(context, rjVar, looper, soVar, new cf());
    }

    private qa(@NonNull Context context, @NonNull rj rjVar, @NonNull Looper looper, @NonNull so soVar, @NonNull cf cfVar) {
        this(context, looper, (LocationListener) new qt(rjVar), cfVar.d(soVar));
    }

    @VisibleForTesting
    qa(@NonNull Context context, @NonNull Looper looper, @NonNull LocationListener locationListener, @NonNull sp spVar) {
        this(context, looper, locationListener, spVar, a(context, locationListener, looper));
    }

    @VisibleForTesting
    qa(@NonNull Context context, @NonNull Looper looper, @NonNull LocationListener locationListener, @NonNull sp spVar, @NonNull b bVar) {
        super(context, locationListener, spVar, looper);
        this.f = bVar;
    }

    public boolean a() {
        a.b bVar = this.g;
        if (bVar != null && this.c.a(this.b)) {
            try {
                this.f.a(bVar);
                return true;
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    public void b() {
        try {
            this.f.b();
        } catch (Throwable unused) {
        }
    }

    public void c() {
        if (this.c.a(this.b)) {
            try {
                this.f.a();
            } catch (Throwable unused) {
            }
        }
    }

    @NonNull
    private static b a(@NonNull Context context, @NonNull LocationListener locationListener, @NonNull Looper looper) {
        if (cx.b("com.google.android.gms.location.LocationRequest")) {
            try {
                a aVar = new a(context, locationListener, looper, a);
                return aVar;
            } catch (Throwable unused) {
            }
        }
        return new pp();
    }

    public void a(@Nullable py pyVar) {
        if (pyVar == null || pyVar.b == null) {
            this.g = null;
        } else {
            this.g = pyVar.b.a;
        }
    }
}
