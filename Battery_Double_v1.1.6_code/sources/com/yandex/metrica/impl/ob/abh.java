package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import com.yandex.metrica.MetricaService;

public class abh {
    private static final ado a = new ado();

    public static final class a implements Runnable {
        final Context a;

        public a(Context context) {
            this.a = context;
        }

        public void run() {
            abh.a(this.a);
        }
    }

    public static void a(Context context, ComponentName componentName) {
        a.a(context, componentName, 1, 1);
    }

    public static void a(Context context) {
        b(context);
    }

    @SuppressLint({"InlinedApi"})
    public static void b(Context context) {
        ServiceInfo[] serviceInfoArr;
        try {
            PackageInfo a2 = a.a(context, context.getPackageName(), 516);
            if (a2.services != null) {
                for (ServiceInfo serviceInfo : a2.services) {
                    if (MetricaService.class.getName().equals(serviceInfo.name) && !serviceInfo.enabled) {
                        a(context, new ComponentName(context, MetricaService.class));
                    }
                }
            }
        } catch (Throwable unused) {
        }
    }
}
