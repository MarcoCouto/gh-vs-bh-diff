package com.yandex.metrica.impl.ob;

import android.os.SystemClock;

class ar {
    private long a = (SystemClock.elapsedRealtime() - 2000000);
    private boolean b = true;

    ar() {
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        boolean z = this.b;
        this.b = false;
        return a(z);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.b = true;
        this.a = SystemClock.elapsedRealtime();
    }

    private boolean a(boolean z) {
        return z && SystemClock.elapsedRealtime() - this.a > 1000;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.b;
    }
}
