package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.pc.a;
import java.util.Map;

public class wh implements wj<Map<a, pc>> {
    @NonNull
    private final Map<a, String> a;
    @NonNull
    private final Map<a, String> b;

    wh() {
        this(wg.a, wg.b);
    }

    @VisibleForTesting
    wh(@NonNull Map<a, String> map, @NonNull Map<a, String> map2) {
        this.a = map;
        this.b = map2;
    }

    public void a(@NonNull Builder builder, @NonNull Map<a, pc> map) {
        a[] values;
        for (a aVar : a.values()) {
            String str = (String) this.a.get(aVar);
            String str2 = (String) this.b.get(aVar);
            pc pcVar = (pc) map.get(aVar);
            if (pcVar == null) {
                builder.appendQueryParameter(str, "");
                builder.appendQueryParameter(str2, "");
            } else {
                builder.appendQueryParameter(str, pcVar.b);
                builder.appendQueryParameter(str2, a(pcVar.c));
            }
        }
    }

    @VisibleForTesting
    static String a(Boolean bool) {
        if (bool == null) {
            return "";
        }
        return bool.booleanValue() ? "1" : "0";
    }
}
