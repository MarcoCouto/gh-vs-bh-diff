package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import org.json.JSONObject;

public class aal {
    @NonNull
    private final aai a;
    private final aak b;

    public aal(@NonNull aai aai, boolean z) {
        this(aai, new aak(z));
    }

    @VisibleForTesting
    aal(@NonNull aai aai, @NonNull aak aak) {
        this.a = aai;
        this.b = aak;
        this.b.a();
    }

    public void a(@NonNull JSONObject jSONObject) {
        this.b.b();
        this.a.a(jSONObject);
    }

    public void a(@NonNull String str) {
        this.b.b();
        this.a.a(str);
    }

    public void a(boolean z) {
        this.b.a(z);
    }
}
