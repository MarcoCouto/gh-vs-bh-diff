package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Base64;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;

class xb extends xd {
    @NonNull
    private xs c;
    @NonNull
    private xc d;

    xb(@NonNull Socket socket, @NonNull Uri uri, @NonNull xg xgVar, @NonNull xs xsVar, @NonNull xc xcVar) {
        super(socket, uri, xgVar);
        this.c = xsVar;
        this.d = xcVar;
    }

    public void a() {
        if (this.c.b.equals(this.b.getQueryParameter("t"))) {
            try {
                final byte[] b = b();
                a("HTTP/1.1 200 OK", (Map<String, String>) new HashMap<String, String>() {
                    {
                        put("Content-Type", "text/plain; charset=utf-8");
                        put("Access-Control-Allow-Origin", "*");
                        put("Access-Control-Allow-Methods", HttpRequest.METHOD_GET);
                        put(HttpRequest.HEADER_CONTENT_LENGTH, String.valueOf(b.length));
                    }
                }, b);
            } catch (Throwable unused) {
            }
        } else {
            this.a.a("request_with_wrong_token");
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() throws JSONException {
        return Base64.encode(new acf().a(this.d.a().getBytes()), 0);
    }
}
