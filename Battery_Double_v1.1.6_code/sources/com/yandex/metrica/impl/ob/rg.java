package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

class rg {
    @NonNull
    private final qv a;
    @NonNull
    private final qg b;
    @NonNull
    private final rm c;
    @NonNull
    private final pn d;
    @NonNull
    private final rh e;

    public rg(@NonNull rh rhVar) {
        ri riVar = new ri(rhVar.a.a);
        rm rmVar = new rm(rhVar.a.a, rhVar.b, rhVar.c, rhVar.d, rhVar.e, rhVar.a.b);
        this(rhVar, riVar, rmVar, new pn(rhVar.c, rhVar.d, rhVar.e));
    }

    private rg(@NonNull rh rhVar, @NonNull ri riVar, @NonNull rm rmVar, @NonNull pn pnVar) {
        this(rhVar, riVar, qw.a(rhVar, rmVar, pnVar, riVar.a()), rmVar, pnVar);
    }

    private rg(@NonNull rh rhVar, @NonNull ri riVar, @NonNull qw qwVar, @NonNull rm rmVar, @NonNull pn pnVar) {
        this(rhVar, riVar, qwVar, new rj(rhVar.a.a, rhVar.c, rmVar, pnVar), rmVar, pnVar);
    }

    private rg(@NonNull rh rhVar, @NonNull ri riVar, @NonNull qw qwVar, @NonNull rj rjVar, @NonNull rm rmVar, @NonNull pn pnVar) {
        this(rhVar, new qv(qwVar, rjVar), new qg(qh.a(rhVar, rmVar, pnVar, riVar.b(), riVar.c())), rmVar, pnVar);
    }

    @VisibleForTesting
    rg(@NonNull rh rhVar, @NonNull qv qvVar, @NonNull qg qgVar, @NonNull rm rmVar, @NonNull pn pnVar) {
        this.e = rhVar;
        this.a = qvVar;
        this.b = qgVar;
        this.c = rmVar;
        this.d = pnVar;
    }

    public void a() {
        this.a.a();
        this.b.d();
    }

    @Nullable
    public Location b() {
        return this.a.b();
    }

    public void c() {
        this.c.a();
    }

    public void d() {
        this.a.c();
        this.b.a();
    }

    public void e() {
        this.a.d();
        this.b.b();
    }

    public void a(@NonNull yb ybVar) {
        this.c.a(ybVar);
    }

    public void a(@Nullable ql qlVar) {
        this.c.a(qlVar);
        this.d.a(qlVar);
        this.a.a(qlVar);
        this.b.a(qlVar);
    }
}
