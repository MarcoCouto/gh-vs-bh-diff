package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.ve.a.d;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a.C0106a;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a.C0106a.C0107a;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a.b.C0108a;
import com.yandex.metrica.impl.ob.ve.a.d.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class yn {
    private static final Map<String, Integer> a = Collections.unmodifiableMap(new HashMap<String, Integer>() {
        {
            put("FOREGROUND", Integer.valueOf(1));
            put("BACKGROUND", Integer.valueOf(0));
            put("VISIBLE", Integer.valueOf(2));
        }
    });

    public void a(@NonNull yx yxVar, @NonNull JSONObject jSONObject) {
        ns nsVar = new ns();
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("location_collecting");
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    d a2 = a(optJSONObject, yxVar.a());
                    if (a2 != null) {
                        arrayList.add(nsVar.a(a2));
                    }
                }
            }
        }
        yxVar.f((List<qm>) arrayList);
    }

    @Nullable
    private d a(@NonNull JSONObject jSONObject, @NonNull xk xkVar) {
        d dVar = new d();
        JSONObject optJSONObject = jSONObject.optJSONObject("config");
        dVar.b = new C0105a();
        if (optJSONObject != null) {
            dVar.b.b = abw.a(abc.a(optJSONObject, "min_update_interval_seconds"), TimeUnit.SECONDS, dVar.b.b);
            dVar.b.c = ((Float) abw.b(abc.e(optJSONObject, "min_update_distance_meters"), Float.valueOf(dVar.b.c))).floatValue();
            dVar.b.d = ((Integer) abw.b(abc.c(optJSONObject, "records_count_to_force_flush"), Integer.valueOf(dVar.b.d))).intValue();
            dVar.b.e = ((Integer) abw.b(abc.c(optJSONObject, "max_records_count_in_batch"), Integer.valueOf(dVar.b.e))).intValue();
            dVar.b.f = abw.a(abc.a(optJSONObject, "max_age_seconds_to_force_flush"), TimeUnit.SECONDS, dVar.b.f);
            dVar.b.g = ((Integer) abw.b(abc.c(optJSONObject, "max_records_to_store_locally"), Integer.valueOf(dVar.b.g))).intValue();
            dVar.b.k = abw.a(abc.a(optJSONObject, "lbs_min_update_interval_seconds"), TimeUnit.SECONDS, dVar.b.k);
            boolean z = false;
            dVar.b.h = ((Boolean) abw.b(abc.d(optJSONObject, "location_collecting_enabled"), Boolean.valueOf(dVar.b.h))).booleanValue() && xkVar.h;
            dVar.b.i = ((Boolean) abw.b(abc.d(optJSONObject, "lbs_collecting_enabled"), Boolean.valueOf(dVar.b.i))).booleanValue() && xkVar.i;
            C0105a aVar = dVar.b;
            if (((Boolean) abw.b(abc.d(optJSONObject, "passive_collecting_enabled"), Boolean.valueOf(dVar.b.j))).booleanValue() && xkVar.h) {
                z = true;
            }
            aVar.j = z;
            if (xkVar.q) {
                dVar.b.l = b(optJSONObject.optJSONObject("wifi_access_config"));
            }
            if (dVar.b.i) {
                dVar.b.m = b(optJSONObject.optJSONObject("lbs_access_config"));
            }
            if (dVar.b.h) {
                dVar.b.n = b(optJSONObject.optJSONObject("gps_access_config"));
            }
            if (dVar.b.j) {
                dVar.b.o = a(optJSONObject.optJSONObject("passive_access_config"), true);
            }
            if (xkVar.k) {
                dVar.b.p = a(optJSONObject.optJSONObject("gpl_access_config"));
            }
        }
        dVar.c = new b();
        JSONObject optJSONObject2 = jSONObject.optJSONObject("preconditions");
        if (optJSONObject2 != null) {
            dVar.c.b = d(optJSONObject2);
            dVar.c.c = e(optJSONObject2);
        }
        return dVar;
    }

    @NonNull
    private static C0105a.b a(@Nullable JSONObject jSONObject) {
        C0105a.b bVar = new C0105a.b();
        bVar.c = ((Boolean) abw.b(abc.d(jSONObject, "scanning_enabled"), Boolean.valueOf(bVar.c))).booleanValue();
        bVar.b = ((Boolean) abw.b(abc.d(jSONObject, "last_known_enabled"), Boolean.valueOf(bVar.b))).booleanValue();
        if (bVar.c) {
            Integer num = null;
            String b = abc.b(jSONObject, "priority");
            Long a2 = abc.a(jSONObject, "duration_seconds");
            Long a3 = abc.a(jSONObject, "interval_seconds");
            if (b != null) {
                if (b.equals("PRIORITY_NO_POWER")) {
                    num = Integer.valueOf(0);
                } else if (b.equals("PRIORITY_LOW_POWER")) {
                    num = Integer.valueOf(1);
                } else if (b.equals("PRIORITY_BALANCED_POWER_ACCURACY")) {
                    num = Integer.valueOf(2);
                } else if (b.equals("PRIORITY_HIGH_ACCURACY")) {
                    num = Integer.valueOf(3);
                }
            }
            if (!(num == null || a2 == null || a3 == null)) {
                C0108a aVar = new C0108a();
                aVar.b = a2.longValue();
                aVar.c = a3.longValue();
                aVar.d = num.intValue();
                bVar.d = aVar;
            }
        }
        return bVar;
    }

    @NonNull
    private static C0106a b(@Nullable JSONObject jSONObject) {
        return a(jSONObject, false);
    }

    @NonNull
    private static C0106a a(@Nullable JSONObject jSONObject, boolean z) {
        C0106a aVar = new C0106a();
        aVar.b = ((Boolean) abw.b(abc.d(jSONObject, "last_known_enabled"), Boolean.valueOf(aVar.b))).booleanValue();
        aVar.c = ((Boolean) abw.b(abc.d(jSONObject, "scanning_enabled"), Boolean.valueOf(z))).booleanValue();
        if (jSONObject != null && aVar.c) {
            aVar.d = c(jSONObject);
        }
        return aVar;
    }

    @NonNull
    private static C0107a c(@NonNull JSONObject jSONObject) {
        C0107a aVar = new C0107a();
        aVar.b = ((Long) abw.b(abc.a(jSONObject, "duration_seconds"), Long.valueOf(aVar.b))).longValue();
        aVar.c = ((Long) abw.b(abc.a(jSONObject, "interval_seconds"), Long.valueOf(aVar.c))).longValue();
        return aVar;
    }

    private int[] d(@NonNull JSONObject jSONObject) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        JSONArray optJSONArray = jSONObject.optJSONArray("charge_types");
        int i = 0;
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                String optString = optJSONArray.optString(i2);
                if ("USB".equals(optString)) {
                    arrayList.add(Integer.valueOf(1));
                } else if ("AC".equals(optString)) {
                    arrayList.add(Integer.valueOf(3));
                } else if ("NONE".equals(optString)) {
                    arrayList.add(Integer.valueOf(0));
                } else if ("WIRELESS".equals(optString)) {
                    arrayList.add(Integer.valueOf(2));
                }
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (Integer intValue : arrayList) {
            iArr[i] = intValue.intValue();
            i++;
        }
        return iArr;
    }

    private static int[] e(@NonNull JSONObject jSONObject) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        JSONArray optJSONArray = jSONObject.optJSONArray("app_statuses");
        int i = 0;
        if (optJSONArray != null) {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                Integer num = (Integer) a.get(optJSONArray.optString(i2, ""));
                if (num != null) {
                    arrayList.add(num);
                }
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (Integer intValue : arrayList) {
            iArr[i] = intValue.intValue();
            i++;
        }
        return iArr;
    }
}
