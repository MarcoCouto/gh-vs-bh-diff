package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration.a;

public class ge {
    @Nullable
    private final String a;
    @NonNull
    private final String b;
    @Nullable
    private final Integer c;
    @Nullable
    private final String d;
    @NonNull
    private final a e;

    public ge(@Nullable String str, @NonNull String str2, @Nullable Integer num, @Nullable String str3, @NonNull a aVar) {
        this.a = str;
        this.b = str2;
        this.c = num;
        this.d = str3;
        this.e = aVar;
    }

    @Nullable
    public String a() {
        return this.a;
    }

    @NonNull
    public String b() {
        return this.b;
    }

    @Nullable
    public Integer c() {
        return this.c;
    }

    @Nullable
    public String d() {
        return this.d;
    }

    @NonNull
    public a e() {
        return this.e;
    }

    @NonNull
    public static ge a(@NonNull et etVar) {
        ge geVar = new ge(etVar.h().e(), etVar.g().i(), etVar.g().f(), etVar.g().g(), etVar.h().r());
        return geVar;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ge geVar = (ge) obj;
        if (this.a == null ? geVar.a != null : !this.a.equals(geVar.a)) {
            return false;
        }
        if (!this.b.equals(geVar.b)) {
            return false;
        }
        if (this.c == null ? geVar.c != null : !this.c.equals(geVar.c)) {
            return false;
        }
        if (this.d == null ? geVar.d != null : !this.d.equals(geVar.d)) {
            return false;
        }
        if (this.e != geVar.e) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((this.a != null ? this.a.hashCode() : 0) * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return ((hashCode + i) * 31) + this.e.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ClientDescription{mApiKey='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", mPackageName='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", mProcessID=");
        sb.append(this.c);
        sb.append(", mProcessSessionID='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", mReporterType=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
