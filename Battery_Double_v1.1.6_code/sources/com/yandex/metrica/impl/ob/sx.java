package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class sx {
    private Context a;

    public sx(Context context) {
        this.a = context;
    }

    public void a() {
        SharedPreferences a2 = ti.a(this.a, "_bidoptpreferences");
        if (a2.getAll().size() > 0) {
            b(a2);
            a(a2);
            a2.edit().clear().apply();
        }
    }

    private void a(SharedPreferences sharedPreferences) {
        Map all = sharedPreferences.getAll();
        if (all.size() > 0) {
            for (String str : a(all, tf.e.a())) {
                String string = sharedPreferences.getString(new th(tf.e.a(), str).b(), null);
                tf tfVar = new tf(this.a, str);
                if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(tfVar.b(null))) {
                    tfVar.i(string).j();
                }
            }
        }
    }

    private List<String> a(Map<String, ?> map, String str) {
        ArrayList arrayList = new ArrayList();
        for (String str2 : map.keySet()) {
            if (str2.startsWith(str)) {
                arrayList.add(str2.replace(str, ""));
            }
        }
        return arrayList;
    }

    private void b(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString(tf.d.a(), null);
        tf tfVar = new tf(this.a);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(tfVar.a((String) null))) {
            tfVar.j(string).j();
            sharedPreferences.edit().remove(tf.d.a()).apply();
        }
    }

    public void b() {
        lx e = lv.a(this.a).e();
        SharedPreferences a2 = ti.a(this.a, "_startupserviceinfopreferences");
        b(e, a2);
        c(e, a2);
        a(e, this.a.getPackageName());
        a(e, a2);
    }

    private void a(lx lxVar, SharedPreferences sharedPreferences) {
        for (String a2 : a(sharedPreferences.getAll(), tf.e.a())) {
            a(lxVar, a2);
        }
    }

    private void a(lx lxVar, String str) {
        mr mrVar = new mr(lxVar, str);
        tf tfVar = new tf(this.a, str);
        String b = tfVar.b(null);
        if (!TextUtils.isEmpty(b)) {
            mrVar.a(b);
        }
        String a2 = tfVar.a();
        if (!TextUtils.isEmpty(a2)) {
            mrVar.h(a2);
        }
        String d = tfVar.d(null);
        if (!TextUtils.isEmpty(d)) {
            mrVar.g(d);
        }
        String f = tfVar.f(null);
        if (!TextUtils.isEmpty(f)) {
            mrVar.e(f);
        }
        String g = tfVar.g(null);
        if (!TextUtils.isEmpty(g)) {
            mrVar.d(g);
        }
        String c = tfVar.c(null);
        if (!TextUtils.isEmpty(c)) {
            mrVar.f(c);
        }
        long a3 = tfVar.a(-1);
        if (a3 != -1) {
            mrVar.a(a3);
        }
        String e = tfVar.e(null);
        if (!TextUtils.isEmpty(e)) {
            mrVar.c(e);
        }
        mrVar.q();
        tfVar.b();
    }

    private void b(lx lxVar, SharedPreferences sharedPreferences) {
        mr mrVar = new mr(lxVar, null);
        String string = sharedPreferences.getString(tf.d.a(), null);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(mrVar.a().b)) {
            mrVar.b(string).q();
            sharedPreferences.edit().remove(tf.d.a()).apply();
        }
    }

    private void c(lx lxVar, SharedPreferences sharedPreferences) {
        mr mrVar = new mr(lxVar, this.a.getPackageName());
        boolean z = sharedPreferences.getBoolean(tf.f.a(), false);
        if (z) {
            mrVar.a(z).q();
        }
    }
}
