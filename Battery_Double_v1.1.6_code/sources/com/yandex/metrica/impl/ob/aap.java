package com.yandex.metrica.impl.ob;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class aap implements Parcelable {
    public static final Creator<aap> CREATOR = new Creator<aap>() {
        /* renamed from: a */
        public aap createFromParcel(Parcel parcel) {
            return new aap(parcel);
        }

        /* renamed from: a */
        public aap[] newArray(int i) {
            return new aap[i];
        }
    };
    public final int a;
    public final int b;
    public final int c;
    public final long d;
    public final boolean e;

    public int describeContents() {
        return 0;
    }

    public aap(int i, int i2, int i3, long j, boolean z) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = j;
        this.e = z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UiParsingConfig{tooLongTextBound=");
        sb.append(this.a);
        sb.append(", truncatedTextBound=");
        sb.append(this.b);
        sb.append(", maxVisitedChildrenInLevel=");
        sb.append(this.c);
        sb.append(", afterCreateTimeout=");
        sb.append(this.d);
        sb.append(", relativeTextSizeCalculation=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        aap aap = (aap) obj;
        if (this.a != aap.a || this.b != aap.b || this.c != aap.c || this.d != aap.d) {
            return false;
        }
        if (this.e != aap.e) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((((((this.a * 31) + this.b) * 31) + this.c) * 31) + ((int) (this.d ^ (this.d >>> 32)))) * 31) + (this.e ? 1 : 0);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeLong(this.d);
        parcel.writeByte(this.e ? (byte) 1 : 0);
    }

    protected aap(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readLong();
        this.e = parcel.readByte() != 0;
    }
}
