package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import io.fabric.sdk.android.services.events.EventsFilesManager;

public class fb {
    @NonNull
    private final String a;
    @Nullable
    private final String b;

    public fb(@NonNull String str, @Nullable String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        return this.a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(this.b);
        return sb.toString();
    }

    public String c() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(dl.b(this.b));
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        fb fbVar = (fb) obj;
        if (this.a == null ? fbVar.a != null : !this.a.equals(fbVar.a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(fbVar.b);
        } else if (fbVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
