package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.uiaccessor.a.C0115a;
import com.yandex.metrica.uiaccessor.b;

public class aao implements dk {
    /* access modifiers changed from: private */
    @NonNull
    public final aan a;
    @NonNull
    private final act b;
    /* access modifiers changed from: private */
    @NonNull
    public final mn c;
    /* access modifiers changed from: private */
    @NonNull
    public final dm d;
    /* access modifiers changed from: private */
    @NonNull
    public final zo e;
    /* access modifiers changed from: private */
    @NonNull
    public final aar f;
    /* access modifiers changed from: private */
    @NonNull
    public final aaj g;
    @Nullable
    private Runnable h;
    @NonNull
    private final b i;
    @NonNull
    private final av j;
    @Nullable
    private aag k;
    /* access modifiers changed from: private */
    @NonNull
    public final abt l;
    /* access modifiers changed from: private */
    @NonNull
    public final ay m;
    /* access modifiers changed from: private */
    @NonNull
    public final aaa n;
    /* access modifiers changed from: private */
    public long o;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public b a(@NonNull final aby<Activity> aby) {
            com.yandex.metrica.uiaccessor.a aVar;
            try {
                aVar = new com.yandex.metrica.uiaccessor.a(new C0115a() {
                });
            } catch (Throwable unused) {
                aVar = null;
            }
            return aVar == null ? new b() {
                public void a(@NonNull Activity activity) throws Throwable {
                }
            } : aVar;
        }
    }

    static /* synthetic */ long i(aao aao) {
        long j2 = aao.o + 1;
        aao.o = j2;
        return j2;
    }

    public aao(@NonNull act act, @NonNull mn mnVar, @NonNull dm dmVar, @NonNull av avVar, @Nullable aag aag) {
        aag aag2 = aag;
        mn mnVar2 = mnVar;
        this(act, mnVar2, dmVar, avVar, aag, new aan(aag2 == null ? null : aag2.d), new zo(1, mnVar2), new aar(), new aaj(), new a(), new abs(), xa.a(), new aaa());
    }

    @VisibleForTesting
    aao(@NonNull act act, @NonNull mn mnVar, @NonNull dm dmVar, @NonNull av avVar, @Nullable aag aag, @NonNull aan aan, @NonNull zo zoVar, @NonNull aar aar, @NonNull aaj aaj, @NonNull a aVar, @NonNull abt abt, @NonNull ay ayVar, @NonNull aaa aaa) {
        this.b = act;
        this.c = mnVar;
        this.k = aag;
        this.a = aan;
        this.d = dmVar;
        this.j = avVar;
        this.e = zoVar;
        this.f = aar;
        this.g = aaj;
        this.i = aVar.a(new aby<Activity>() {
            public void a(Activity activity) {
                aao.this.a(activity);
            }
        });
        this.l = abt;
        this.m = ayVar;
        this.n = aaa;
        this.o = this.c.g(0);
    }

    private void a(@NonNull Activity activity, long j2, @Nullable aal aal, boolean z) {
        if (aal != null) {
            if (this.k == null) {
                aal.a(String.format("no %s_config", new Object[]{"ui_access"}));
            } else if (!this.k.a) {
                aal.a(String.format("feature %s disabled", new Object[]{"ui_parsing"}));
            } else if (this.k.d == null) {
                aal.a(String.format("no %s_config", new Object[]{"ui_parsing"}));
            } else if (!this.k.c) {
                aal.a(String.format("feature %s disabled", new Object[]{"ui_collecting_for_bridge"}));
            } else if (this.k.f == null) {
                aal.a(String.format("no %s_config", new Object[]{"ui_collecting_for_bridge"}));
            }
        }
        if (this.k != null && this.k.a) {
            try {
                this.i.a(activity);
            } catch (Throwable unused) {
            }
            Runnable a2 = a(activity, this.k, aal, z);
            if (this.h != null) {
                this.b.b(this.h);
            }
            this.h = a2;
            this.b.a(a2, j2);
        }
    }

    public void a(@NonNull Activity activity) {
        if (this.k != null && this.k.d != null) {
            a(activity, this.k.d.d, (aal) null, false);
        }
    }

    public void a(@NonNull Activity activity, @Nullable aal aal, boolean z) {
        a(activity, 0, aal, z);
    }

    public void a(@NonNull aag aag) {
        this.a.a(aag.d);
        aag aag2 = this.k;
        this.k = aag;
        if (aag2 == null) {
            Activity a2 = this.j.a();
            if (a2 != null) {
                a(a2, (aal) null, false);
            }
        }
    }

    @NonNull
    public zo a() {
        return this.e;
    }

    @NonNull
    private Runnable a(@NonNull Activity activity, @NonNull aag aag, @Nullable aal aal, boolean z) {
        final aag aag2 = aag;
        final boolean z2 = z;
        final Activity activity2 = activity;
        final aal aal2 = aal;
        AnonymousClass2 r0 = new Runnable() {
            public void run() {
                boolean z = false;
                if (aag2.c && aag2.f != null && (aao.this.c.a(false) || z2)) {
                    z = true;
                }
                zt a2 = aao.this.a(z);
                try {
                    long a3 = aao.this.l.a();
                    aaf a4 = aao.this.a.a(activity2, a2);
                    if (z) {
                        aao.this.g.a(aao.this.e, activity2, a2.a(), aag2.f, aao.this.o);
                        aao.this.a(aal2);
                    }
                    if (aag2.b && aag2.e != null) {
                        aao.this.d.a(aao.this.f.a(activity2, a4, aag2.e, aao.this.o));
                    }
                    aao.this.c.h(aao.i(aao.this));
                    aao.this.m.reportEvent("ui_parsing_time", aao.this.n.a(aao.this.l.a() - a3).toString());
                } catch (Throwable th) {
                    aao.this.m.reportError("ui_parsing", th);
                    aao.this.a(aal2, th);
                }
            }
        };
        return r0;
    }

    /* access modifiers changed from: private */
    @NonNull
    public zt a(boolean z) {
        if (z) {
            return new zz();
        }
        return new zp();
    }

    /* access modifiers changed from: private */
    public void a(@Nullable aal aal) {
        if (aal != null) {
            aal.a(this.e.b());
        }
    }

    /* access modifiers changed from: private */
    public void a(@Nullable aal aal, @NonNull Throwable th) {
        if (aal != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("exception: ");
            sb.append(th.getMessage());
            aal.a(sb.toString());
        }
    }
}
