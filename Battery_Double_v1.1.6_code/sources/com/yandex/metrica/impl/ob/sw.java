package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;

public class sw implements sq {
    @NonNull
    private final Context a;
    @NonNull
    private final String b;
    @NonNull
    private final ado c;

    public sw(@NonNull Context context) {
        this(context, context.getPackageName(), new ado());
    }

    public sw(@NonNull Context context, @NonNull String str, @NonNull ado ado) {
        this.a = context;
        this.b = str;
        this.c = ado;
    }

    @NonNull
    public List<sr> a() {
        ArrayList arrayList = new ArrayList();
        PackageInfo a2 = this.c.a(this.a, this.b, 4096);
        if (a2 != null) {
            for (String srVar : a2.requestedPermissions) {
                arrayList.add(new sr(srVar, true));
            }
        }
        return arrayList;
    }
}
