package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.uy.a.C0096a;

public abstract class ud<T> extends ui {
    @NonNull
    private final T a;

    /* access modifiers changed from: protected */
    public abstract void a(@NonNull C0096a aVar);

    ud(int i, @NonNull String str, @NonNull T t, @NonNull adw<String> adw, @NonNull ua uaVar) {
        super(i, str, adw, uaVar);
        this.a = t;
    }

    @NonNull
    public T b() {
        return this.a;
    }

    public void a(@NonNull uq uqVar) {
        if (f()) {
            C0096a b = b(uqVar);
            if (b != null) {
                a(b);
            }
        }
    }

    @Nullable
    private C0096a b(@NonNull uq uqVar) {
        return e().a(uqVar, uqVar.a(d(), c()), this);
    }
}
