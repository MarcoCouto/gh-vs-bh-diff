package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.vx;
import com.yandex.metrica.impl.ob.vy;
import com.yandex.metrica.impl.ob.vy.c;
import com.yandex.metrica.impl.ob.vy.d;

public abstract class wa<T extends vy, IA, A extends vx<IA, A>, L extends d<T, c<A>>> {
    @Nullable
    private T a;
    @NonNull
    private L b;
    @NonNull
    private c<A> c;

    public wa(@NonNull L l, @NonNull yb ybVar, @NonNull A a2) {
        this.b = l;
        eh.a().a(this, ep.class, el.a((ek<T>) new ek<ep>() {
            public void a(ep epVar) {
                wa.this.a();
            }
        }).a());
        a(new c<>(ybVar, a2));
    }

    public synchronized void a() {
        this.a = null;
    }

    /* access modifiers changed from: protected */
    public synchronized void a(@NonNull c<A> cVar) {
        this.c = cVar;
    }

    public synchronized void a(@NonNull IA ia) {
        if (!((vx) this.c.b).a(ia)) {
            a(new c<>(b(), ((vx) this.c.b).b(ia)));
            a();
        }
    }

    public synchronized void a(@NonNull yb ybVar) {
        a(new c<>(ybVar, c()));
        a();
    }

    @NonNull
    public synchronized yb b() {
        return this.c.a;
    }

    @VisibleForTesting(otherwise = 4)
    @NonNull
    public synchronized A c() {
        return (vx) this.c.b;
    }

    @NonNull
    public synchronized T d() {
        if (this.a == null) {
            this.a = this.b.a(this.c);
        }
        return this.a;
    }
}
