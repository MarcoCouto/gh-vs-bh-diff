package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class we extends wb {
    private boolean a;
    private Location b;
    private boolean c;
    private boolean d;
    private boolean e;
    private int f;
    private int g;
    private boolean h;
    private int i;
    private Boolean j;
    private d k;
    private String l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private String q;
    private List<String> r;
    private boolean s;
    private int t;
    private long u;
    private long v;
    private boolean w;
    private long x;
    @Nullable
    private List<String> y;

    public static final class a extends com.yandex.metrica.impl.ob.vy.a<com.yandex.metrica.impl.ob.ew.a, a> {
        @Nullable
        public final String a;
        @Nullable
        public final Location b;
        public final boolean f;
        public final boolean g;
        public final boolean h;
        public final int i;
        public final int j;
        public final int k;
        public final boolean l;
        public final boolean m;
        public final boolean n;
        @Nullable
        public final Map<String, String> o;
        public final int p;

        /* access modifiers changed from: 0000 */
        public boolean a(@Nullable Location location, @Nullable Location location2) {
            boolean z = true;
            if (location == location2) {
                return true;
            }
            if (location == null || location2 == null || location.getTime() != location2.getTime()) {
                return false;
            }
            if ((dl.a(17) && location.getElapsedRealtimeNanos() != location2.getElapsedRealtimeNanos()) || Double.compare(location2.getLatitude(), location.getLatitude()) != 0 || Double.compare(location2.getLongitude(), location.getLongitude()) != 0 || Double.compare(location2.getAltitude(), location.getAltitude()) != 0 || Float.compare(location2.getSpeed(), location.getSpeed()) != 0 || Float.compare(location2.getBearing(), location.getBearing()) != 0 || Float.compare(location2.getAccuracy(), location.getAccuracy()) != 0) {
                return false;
            }
            if (dl.a(26) && (Float.compare(location2.getVerticalAccuracyMeters(), location.getVerticalAccuracyMeters()) != 0 || Float.compare(location2.getSpeedAccuracyMetersPerSecond(), location.getSpeedAccuracyMetersPerSecond()) != 0 || Float.compare(location2.getBearingAccuracyDegrees(), location.getBearingAccuracyDegrees()) != 0)) {
                return false;
            }
            if (location.getProvider() == null ? location2.getProvider() != null : !location.getProvider().equals(location2.getProvider())) {
                return false;
            }
            if (location.getExtras() != null) {
                z = location.getExtras().equals(location2.getExtras());
            } else if (location2.getExtras() != null) {
                z = false;
            }
            return z;
        }

        public a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
            com.yandex.metrica.impl.ob.ew.a aVar2 = aVar;
            this(aVar2.a, aVar2.b, aVar2.c, aVar2.d, aVar2.e, aVar2.f, aVar2.g, aVar2.h, aVar2.n, aVar2.i, aVar2.j, aVar2.k, aVar2.l, aVar2.m, aVar2.o, aVar2.p);
        }

        a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map, @Nullable Integer num4) {
            super(str, str2, str3);
            this.a = str4;
            Boolean bool7 = bool;
            this.f = ((Boolean) abw.b(bool, Boolean.valueOf(true))).booleanValue();
            this.b = location;
            Boolean bool8 = bool2;
            this.g = ((Boolean) abw.b(bool2, Boolean.valueOf(false))).booleanValue();
            Boolean bool9 = bool3;
            this.h = ((Boolean) abw.b(bool3, Boolean.valueOf(false))).booleanValue();
            Boolean bool10 = bool4;
            this.n = ((Boolean) abw.b(bool4, Boolean.valueOf(false))).booleanValue();
            this.i = Math.max(10, ((Integer) abw.b(num, Integer.valueOf(10))).intValue());
            this.j = ((Integer) abw.b(num2, Integer.valueOf(7))).intValue();
            this.k = ((Integer) abw.b(num3, Integer.valueOf(90))).intValue();
            this.l = ((Boolean) abw.b(bool5, Boolean.valueOf(false))).booleanValue();
            this.m = ((Boolean) abw.b(bool6, Boolean.valueOf(true))).booleanValue();
            this.o = map;
            this.p = ((Integer) abw.b(num4, Integer.valueOf(1000))).intValue();
        }

        @NonNull
        /* renamed from: a */
        public a b(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
            com.yandex.metrica.impl.ob.ew.a aVar2 = aVar;
            a aVar3 = new a((String) abw.a(aVar2.a, this.c), (String) abw.a(aVar2.b, this.d), (String) abw.a(aVar2.c, this.e), (String) abw.a(aVar2.d, this.a), (Boolean) abw.a(aVar2.e, Boolean.valueOf(this.f)), (Location) abw.a(aVar2.f, this.b), (Boolean) abw.a(aVar2.g, Boolean.valueOf(this.g)), (Boolean) abw.a(aVar2.h, Boolean.valueOf(this.h)), aVar2.n, (Integer) abw.a(aVar2.i, Integer.valueOf(this.i)), (Integer) abw.a(aVar2.j, Integer.valueOf(this.j)), (Integer) abw.a(aVar2.k, Integer.valueOf(this.k)), (Boolean) abw.a(aVar2.l, Boolean.valueOf(this.l)), (Boolean) abw.a(aVar2.m, Boolean.valueOf(this.m)), (Map) abw.a(aVar2.o, this.o), (Integer) abw.a(aVar2.p, Integer.valueOf(this.p)));
            return aVar3;
        }

        /* renamed from: b */
        public boolean a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
            boolean z = false;
            if (aVar.a != null && !aVar.a.equals(this.c)) {
                return false;
            }
            if (aVar.b != null && !aVar.b.equals(this.d)) {
                return false;
            }
            if (aVar.c != null && !aVar.c.equals(this.e)) {
                return false;
            }
            if (aVar.e != null && this.f != aVar.e.booleanValue()) {
                return false;
            }
            if (aVar.g != null && this.g != aVar.g.booleanValue()) {
                return false;
            }
            if (aVar.h != null && this.h != aVar.h.booleanValue()) {
                return false;
            }
            if (aVar.i != null && this.i != aVar.i.intValue()) {
                return false;
            }
            if (aVar.j != null && this.j != aVar.j.intValue()) {
                return false;
            }
            if (aVar.k != null && this.k != aVar.k.intValue()) {
                return false;
            }
            if (aVar.l != null && this.l != aVar.l.booleanValue()) {
                return false;
            }
            if (aVar.m != null && this.m != aVar.m.booleanValue()) {
                return false;
            }
            if (aVar.n != null && this.n != aVar.n.booleanValue()) {
                return false;
            }
            if (aVar.d != null && (this.a == null || !this.a.equals(aVar.d))) {
                return false;
            }
            if (aVar.o != null && (this.o == null || !this.o.equals(aVar.o))) {
                return false;
            }
            if (aVar.p != null && this.p != aVar.p.intValue()) {
                return false;
            }
            if (aVar.f == null || a(this.b, aVar.f)) {
                z = true;
            }
            return z;
        }
    }

    public static abstract class b implements d {
        @NonNull
        protected final df a;

        public b(@NonNull df dfVar) {
            this.a = dfVar;
        }

        public boolean a(@Nullable Boolean bool) {
            return ((Boolean) abw.b(bool, Boolean.valueOf(true))).booleanValue();
        }
    }

    public static class c extends a<we, a> {
        @NonNull
        private final fe c;
        @NonNull
        private final d d;

        public c(@NonNull fe feVar, @NonNull d dVar) {
            super(feVar.k(), feVar.c().b());
            this.c = feVar;
            this.d = dVar;
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public we b() {
            return new we();
        }

        @NonNull
        /* renamed from: a */
        public we c(@NonNull com.yandex.metrica.impl.ob.vy.c<a> cVar) {
            we weVar = (we) super.c(cVar);
            weVar.n(((a) cVar.b).a);
            weVar.k(this.c.x());
            weVar.d(this.c.p());
            weVar.b(this.c.A().a());
            weVar.e(((a) cVar.b).f);
            weVar.a(((a) cVar.b).b);
            weVar.f(((a) cVar.b).g);
            weVar.g(((a) cVar.b).h);
            weVar.a(((a) cVar.b).i);
            weVar.c(((a) cVar.b).j);
            weVar.b(((a) cVar.b).k);
            weVar.i(((a) cVar.b).l);
            weVar.h(((a) cVar.b).n);
            weVar.a(Boolean.valueOf(((a) cVar.b).m), this.d);
            weVar.c((long) ((a) cVar.b).p);
            a(weVar, cVar.a, ((a) cVar.b).o);
            return weVar;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void a(@NonNull we weVar, @NonNull yb ybVar, @Nullable Map<String, String> map) {
            a(weVar, ybVar);
            b(weVar, ybVar);
            weVar.a(ybVar.m);
            weVar.j(a(map, abq.a(ybVar.n)));
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@Nullable Map<String, String> map, @Nullable Map<String, String> map2) {
            return map == null || map.isEmpty() || map.equals(map2);
        }

        /* access modifiers changed from: 0000 */
        public void a(we weVar, yb ybVar) {
            weVar.a(ybVar.e);
        }

        /* access modifiers changed from: 0000 */
        public void b(we weVar, yb ybVar) {
            weVar.a(ybVar.o.a);
            weVar.b(ybVar.o.b);
            weVar.c(ybVar.o.c);
            if (ybVar.A != null) {
                weVar.a(ybVar.A.a);
                weVar.b(ybVar.A.b);
            }
            weVar.d(ybVar.o.d);
        }
    }

    public interface d {
        boolean a(@Nullable Boolean bool);
    }

    @VisibleForTesting
    we() {
    }

    @NonNull
    public String a() {
        return (String) abw.b(this.q, "");
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.q = str;
    }

    public void a(List<String> list) {
        this.r = list;
    }

    public List<String> b() {
        return this.r;
    }

    public String c() {
        return this.l;
    }

    public boolean I() {
        return this.m;
    }

    public void a(boolean z) {
        this.m = z;
    }

    public boolean J() {
        return this.n;
    }

    public boolean K() {
        return this.o;
    }

    public boolean L() {
        return this.p;
    }

    public void a(long j2) {
        this.u = j2;
    }

    public long M() {
        return this.u;
    }

    public void b(long j2) {
        this.v = j2;
    }

    public long N() {
        return this.v;
    }

    public void b(boolean z) {
        this.n = z;
    }

    public void c(boolean z) {
        this.o = z;
    }

    public void d(boolean z) {
        this.p = z;
    }

    public boolean O() {
        return g() && !dl.a((Collection) b()) && ac();
    }

    /* access modifiers changed from: private */
    public void n(String str) {
        this.l = str;
    }

    public boolean P() {
        return this.s;
    }

    /* access modifiers changed from: private */
    public void k(boolean z) {
        this.s = z;
    }

    public boolean Q() {
        return this.a;
    }

    public void e(boolean z) {
        this.a = z;
    }

    public Location R() {
        return this.b;
    }

    public void a(Location location) {
        this.b = location;
    }

    public boolean S() {
        return this.c;
    }

    public void f(boolean z) {
        this.c = z;
    }

    public boolean T() {
        return this.d;
    }

    public void g(boolean z) {
        this.d = z;
    }

    public boolean U() {
        return this.e;
    }

    public void h(boolean z) {
        this.e = z;
    }

    public int V() {
        return this.f;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public int W() {
        return this.g;
    }

    public void b(int i2) {
        this.g = i2;
    }

    public void i(boolean z) {
        this.h = z;
    }

    public int X() {
        return this.i;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public int Y() {
        return this.t;
    }

    public void d(int i2) {
        this.t = i2;
    }

    public long Z() {
        return this.x;
    }

    public void c(long j2) {
        this.x = j2;
    }

    public boolean aa() {
        return this.k.a(this.j);
    }

    @Nullable
    public List<String> ab() {
        return this.y;
    }

    public void b(@NonNull List<String> list) {
        this.y = list;
    }

    public void a(@Nullable Boolean bool, @NonNull d dVar) {
        this.j = bool;
        this.k = dVar;
    }

    public boolean ac() {
        return this.w;
    }

    public void j(boolean z) {
        this.w = z;
    }
}
