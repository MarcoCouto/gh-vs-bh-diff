package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class zk extends za {
    private int a;
    private za b;

    public zk(@NonNull Context context, @NonNull act act) {
        this(context.getApplicationContext(), new ado(), act);
    }

    @VisibleForTesting
    zk(Context context, @NonNull ado ado, @NonNull act act) {
        if (ado.b(context, "android.hardware.telephony")) {
            this.b = new ze(context, act);
        } else {
            this.b = new zf();
        }
    }

    public synchronized void a() {
        this.a++;
        if (this.a == 1) {
            this.b.a();
        }
    }

    public synchronized void b() {
        this.a--;
        if (this.a == 0) {
            this.b.b();
        }
    }

    public synchronized void a(zn znVar) {
        this.b.a(znVar);
    }

    public synchronized void a(zc zcVar) {
        this.b.a(zcVar);
    }

    public void a(boolean z) {
        this.b.a(z);
    }

    public void a(@NonNull yb ybVar) {
        this.b.a(ybVar);
    }
}
