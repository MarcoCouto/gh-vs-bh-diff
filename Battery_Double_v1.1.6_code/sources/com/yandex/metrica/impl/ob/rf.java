package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.Arrays;
import java.util.List;

public class rf {
    @Nullable
    protected final pt a;
    @NonNull
    protected final qw b;
    @NonNull
    private final cf c;
    @NonNull
    private final rc d;

    public rf(@NonNull qw qwVar, @Nullable pt ptVar) {
        this(qwVar, ptVar, new cf());
    }

    @VisibleForTesting
    rf(@NonNull qw qwVar, @Nullable pt ptVar, @NonNull cf cfVar) {
        this.b = qwVar;
        this.a = ptVar;
        this.c = cfVar;
        this.d = d();
    }

    @NonNull
    private rd b() {
        return new rd();
    }

    @NonNull
    private re c() {
        return new re();
    }

    @NonNull
    private rc d() {
        return new rc(this.a);
    }

    @NonNull
    private qx b(@NonNull rj rjVar) {
        rn rnVar = new rn(this.b.a.a, this.b.a.b.b(), this.b.c, rjVar, this.c.c(this.b.a.c), "passive");
        return rnVar;
    }

    @NonNull
    public qy a(@NonNull rj rjVar) {
        return new qy(b(rjVar), this.d, c(), b());
    }

    @NonNull
    public List<qn> a() {
        return Arrays.asList(new qn[]{this.d});
    }
}
