package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

public class rt implements aw {
    @NonNull
    private final mq a;
    @NonNull
    private final dj b;
    @Nullable
    private volatile pt c;
    @NonNull
    private final dn d;
    @NonNull
    private final abt e;
    @NonNull
    private final a f;
    @NonNull
    private final rs g;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public CountDownLatch a() {
            return new CountDownLatch(1);
        }
    }

    public rt(@NonNull Context context, @Nullable pt ptVar) {
        this(context, ptVar, dn.a(context));
    }

    private rt(@NonNull Context context, @Nullable pt ptVar, @NonNull dn dnVar) {
        this(dnVar, new mq(lv.a(context).c()), new dj(), new abs(), new a(), ptVar, new rs(null, dnVar.d()));
    }

    @VisibleForTesting
    rt(@NonNull dn dnVar, @NonNull mq mqVar, @NonNull dj djVar, @NonNull abt abt, @NonNull a aVar, @Nullable pt ptVar, @NonNull rs rsVar) {
        this.d = dnVar;
        this.a = mqVar;
        this.b = djVar;
        this.f = aVar;
        this.c = ptVar;
        this.e = abt;
        this.g = rsVar;
    }

    public void b() {
        pt ptVar = this.c;
        if (ptVar != null && ptVar.b != null && this.b.a(this.a.j(0), ptVar.b.b, "last wifi scan attempt time")) {
            CountDownLatch a2 = this.f.a();
            if (this.d.a(a2, (y<JSONArray>) this.g)) {
                this.a.k(this.e.b());
                try {
                    a2.await(5, TimeUnit.SECONDS);
                } catch (Throwable unused) {
                }
            }
        }
    }

    public void a(@Nullable pt ptVar) {
        if (!dl.a((Object) this.c, (Object) ptVar)) {
            this.c = ptVar;
            a();
        }
    }

    public void a() {
        pt ptVar = this.c;
        if (ptVar != null && ptVar.a.a) {
            this.g.a(this.d.b());
        }
    }
}
