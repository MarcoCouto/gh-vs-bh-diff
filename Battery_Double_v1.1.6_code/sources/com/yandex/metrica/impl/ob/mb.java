package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.SparseArray;

public class mb {
    private final String a;
    private final mj b;
    private final mj c;
    private final SparseArray<mj> d;
    private final mc e;

    public static class a {
        public mb a(@NonNull String str, @NonNull mj mjVar, @NonNull mj mjVar2, @NonNull SparseArray<mj> sparseArray, @NonNull mc mcVar) {
            mb mbVar = new mb(str, mjVar, mjVar2, sparseArray, mcVar);
            return mbVar;
        }
    }

    private mb(String str, mj mjVar, mj mjVar2, SparseArray<mj> sparseArray, mc mcVar) {
        this.a = str;
        this.b = mjVar;
        this.c = mjVar2;
        this.d = sparseArray;
        this.e = mcVar;
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
        try {
            if (this.e != null && !this.e.a(sQLiteDatabase)) {
                c(sQLiteDatabase);
            }
        } catch (Throwable unused) {
        }
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        a(this.b, sQLiteDatabase);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(mj mjVar, SQLiteDatabase sQLiteDatabase) {
        try {
            mjVar.a(sQLiteDatabase);
        } catch (Throwable unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    public void a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        boolean z;
        if (i2 > i) {
            z = false;
            int i3 = i + 1;
            while (i3 <= i2) {
                try {
                    mj mjVar = (mj) this.d.get(i3);
                    if (mjVar != null) {
                        mjVar.a(sQLiteDatabase);
                    }
                    i3++;
                } catch (Throwable unused) {
                }
            }
            if (!(this.e.a(sQLiteDatabase) ^ true) && !z) {
                c(sQLiteDatabase);
                return;
            }
            return;
        }
        z = true;
        if (!(!this.e.a(sQLiteDatabase)) && !z) {
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void c(SQLiteDatabase sQLiteDatabase) {
        b(this.c, sQLiteDatabase);
        a(this.b, sQLiteDatabase);
    }

    private void b(mj mjVar, SQLiteDatabase sQLiteDatabase) {
        try {
            mjVar.a(sQLiteDatabase);
        } catch (Throwable unused) {
        }
    }
}
