package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class qj extends pl {
    /* access modifiers changed from: protected */
    @NonNull
    public String b() {
        return "lbs";
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String c() {
        return "network";
    }

    public qj(@NonNull qw qwVar, @NonNull mq mqVar, @Nullable pt ptVar) {
        this(qwVar, mqVar, ptVar, new cf());
    }

    @VisibleForTesting
    qj(@NonNull qw qwVar, @NonNull mq mqVar, @Nullable pt ptVar, @NonNull cf cfVar) {
        super(qwVar, mqVar, ptVar, cfVar);
    }

    @NonNull
    public qe a() {
        return new qe() {
            public long a() {
                return qj.this.a.l(0);
            }

            public void a(long j) {
                qj.this.a.m(j);
            }
        };
    }

    /* access modifiers changed from: protected */
    @NonNull
    public sp a(@NonNull so soVar) {
        return this.d.d(soVar);
    }
}
