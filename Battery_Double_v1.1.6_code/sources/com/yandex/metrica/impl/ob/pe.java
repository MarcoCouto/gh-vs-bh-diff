package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class pe {
    /* access modifiers changed from: private */
    public static final List<? extends pd> a = Arrays.asList(new pd[]{new com.yandex.metrica.impl.ac.a(), new pf()});
    /* access modifiers changed from: private */
    public final Object b;
    /* access modifiers changed from: private */
    public yb c;
    private volatile FutureTask<Map<com.yandex.metrica.impl.ob.pc.a, pc>> d;
    @NonNull
    private final c e;
    /* access modifiers changed from: private */
    @NonNull
    public final Collection<? extends pd> f;
    @Nullable
    private Context g;
    @NonNull
    private act h;
    /* access modifiers changed from: private */
    public volatile Map<com.yandex.metrica.impl.ob.pc.a, pc> i;

    private static class a {
        @SuppressLint({"StaticFieldLeak"})
        static final pe a = new pe(new b(), dr.k().b(), pe.a);
    }

    static class b implements c {
        public boolean a(@Nullable yb ybVar) {
            return true;
        }

        b() {
        }
    }

    interface c {
        boolean a(@Nullable yb ybVar);
    }

    private static class d {
        @SuppressLint({"StaticFieldLeak"})
        static final pe a = new pe(new e(), as.a().k().i(), pe.a);
    }

    static class e implements c {
        e() {
        }

        public boolean a(@Nullable yb ybVar) {
            return ybVar != null && (ybVar.o.p || !ybVar.v);
        }
    }

    private interface f<T> {
        T b(Future<Map<com.yandex.metrica.impl.ob.pc.a, pc>> future) throws InterruptedException, ExecutionException;
    }

    public static pe a() {
        return d.a;
    }

    public static pe b() {
        return a.a;
    }

    private pe(@NonNull c cVar, @NonNull act act, @NonNull Collection<? extends pd> collection) {
        this.b = new Object();
        this.i = null;
        this.e = cVar;
        this.f = collection;
        this.h = act;
        eh.a().a(this, er.class, el.a((ek<T>) new ek<er>() {
            public void a(er erVar) {
                synchronized (pe.this.b) {
                    pe.this.c = erVar.b;
                }
            }
        }).a());
    }

    public void a(@NonNull Context context) {
        this.g = context.getApplicationContext();
    }

    public void b(@NonNull final Context context) {
        this.g = context.getApplicationContext();
        if (this.d == null) {
            synchronized (this.b) {
                if (this.d == null && this.e.a(this.c)) {
                    this.d = new FutureTask<>(new Callable<Map<com.yandex.metrica.impl.ob.pc.a, pc>>() {
                        /* renamed from: a */
                        public Map<com.yandex.metrica.impl.ob.pc.a, pc> call() {
                            EnumMap enumMap = new EnumMap(com.yandex.metrica.impl.ob.pc.a.class);
                            for (pd a2 : pe.this.f) {
                                pc a3 = a2.a(context);
                                if (a3 != null) {
                                    enumMap.put(a3.a, a3);
                                }
                            }
                            pe.this.i = enumMap.isEmpty() ? null : Collections.unmodifiableMap(enumMap);
                            return pe.this.i;
                        }
                    });
                    this.h.a((Runnable) this.d);
                }
            }
        }
    }

    public void a(@NonNull Context context, @NonNull yb ybVar) {
        this.c = ybVar;
        b(context);
    }

    private void g() {
        if (this.g != null && !c()) {
            c(this.g);
        }
    }

    private <T> T a(Context context, f<T> fVar) {
        b(context);
        try {
            return fVar.b(this.d);
        } catch (InterruptedException | ExecutionException unused) {
            return null;
        }
    }

    @NonNull
    public Map<com.yandex.metrica.impl.ob.pc.a, pc> c(@NonNull Context context) {
        if (!this.e.a(this.c)) {
            return Collections.emptyMap();
        }
        Map<com.yandex.metrica.impl.ob.pc.a, pc> map = (Map) a(context, (f<T>) new f<Map<com.yandex.metrica.impl.ob.pc.a, pc>>() {
            /* renamed from: a */
            public Map<com.yandex.metrica.impl.ob.pc.a, pc> b(Future<Map<com.yandex.metrica.impl.ob.pc.a, pc>> future) throws InterruptedException, ExecutionException {
                return (Map) future.get();
            }
        });
        if (map == null) {
            map = Collections.emptyMap();
        }
        return map;
    }

    public synchronized boolean c() {
        return this.i != null;
    }

    @Deprecated
    public String d() {
        g();
        if (this.i == null || !this.i.containsKey(com.yandex.metrica.impl.ob.pc.a.GOOGLE)) {
            return null;
        }
        return ((pc) this.i.get(com.yandex.metrica.impl.ob.pc.a.GOOGLE)).b;
    }

    @Deprecated
    public Boolean e() {
        g();
        if (this.i == null || !this.i.containsKey(com.yandex.metrica.impl.ob.pc.a.GOOGLE)) {
            return null;
        }
        return ((pc) this.i.get(com.yandex.metrica.impl.ob.pc.a.GOOGLE)).c;
    }
}
