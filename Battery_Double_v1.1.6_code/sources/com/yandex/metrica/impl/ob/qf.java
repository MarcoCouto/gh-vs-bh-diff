package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Deprecated
public class qf {
    static final Set<String> a = new HashSet(Arrays.asList(new String[]{"gps"}));
    @NonNull
    private Context b;
    @Nullable
    private LocationManager c;
    @NonNull
    private so d;

    public qf(@NonNull Context context, @Nullable LocationManager locationManager, @NonNull so soVar) {
        this.b = context;
        this.c = locationManager;
        this.d = soVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0024 A[SYNTHETIC] */
    @Nullable
    public Location a() {
        List<String> list;
        Location location;
        Location location2;
        if (this.c == null) {
            return null;
        }
        boolean a2 = this.d.a(this.b);
        boolean b2 = this.d.b(this.b);
        try {
            list = this.c.getAllProviders();
        } catch (Throwable unused) {
            list = null;
        }
        if (list == null) {
            return null;
        }
        Location location3 = null;
        for (String str : list) {
            if (!a.contains(str)) {
                if (a2) {
                    try {
                        if (!"passive".equals(str) || b2) {
                            location2 = this.c.getLastKnownLocation(str);
                            location = location2;
                            if (location == null) {
                                if (qr.a(location, location3, qr.c, 200)) {
                                    location3 = location;
                                }
                            }
                        }
                    } catch (Throwable unused2) {
                        location = null;
                    }
                }
                location2 = null;
                location = location2;
                if (location == null) {
                }
            }
        }
        return location3;
    }
}
