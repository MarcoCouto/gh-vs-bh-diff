package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.impl.ob.we.d;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class fe implements fk, fn, oy {
    protected mq a;
    private final Context b;
    private final fb c;
    private final mo d;
    private final mm e;
    private final cr f;
    private final lr g;
    private final hj h;
    private final hg i;
    private final i j;
    @NonNull
    private final a k;
    private volatile ju l;
    private final fv m;
    @NonNull
    private final jf n;
    @NonNull
    private final abl o;
    @NonNull
    private final abb p;
    /* access modifiers changed from: private */
    @NonNull
    public final fw q;
    @NonNull
    private final com.yandex.metrica.impl.ob.fd.a r;
    @NonNull
    private final ox s;
    @NonNull
    private final ou t;
    @NonNull
    private final oz u;
    @NonNull
    private final s v;
    @NonNull
    private final dj w;

    static class a {
        private final HashMap<String, i> a = new HashMap<>();

        a() {
        }

        public synchronized i a(@NonNull fb fbVar, @NonNull abl abl, mo moVar) {
            i iVar;
            iVar = (i) this.a.get(fbVar.toString());
            if (iVar == null) {
                com.yandex.metrica.impl.ob.i.a d = moVar.d();
                iVar = new i(d.a, d.b, abl);
                this.a.put(fbVar.toString(), iVar);
            }
            return iVar;
        }

        public synchronized boolean a(com.yandex.metrica.impl.ob.i.a aVar, mo moVar) {
            if (aVar.b <= moVar.d().b) {
                return false;
            }
            moVar.a(aVar).q();
            return true;
        }

        public synchronized void b(com.yandex.metrica.impl.ob.i.a aVar, mo moVar) {
            moVar.a(aVar).q();
        }
    }

    public ju d() {
        return this.l;
    }

    @NonNull
    public fw e() {
        return this.q;
    }

    public fe(@NonNull Context context, @NonNull yb ybVar, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.ew.a aVar, @NonNull d dVar, @NonNull ye yeVar) {
        a aVar2 = new a();
        dj djVar = new dj();
        Context context2 = context;
        ff ffVar = new ff(context, fbVar, aVar, yeVar, ybVar, dVar, as.a().k().g(), dl.c(context2, fbVar.b()));
        this(context2, fbVar, aVar2, djVar, ffVar);
    }

    @VisibleForTesting
    fe(@NonNull Context context, @NonNull fb fbVar, @NonNull a aVar, @NonNull dj djVar, @NonNull ff ffVar) {
        this.b = context.getApplicationContext();
        this.c = fbVar;
        this.k = aVar;
        this.w = djVar;
        this.o = ffVar.a().a();
        this.p = ffVar.a().b();
        this.d = ffVar.b().b();
        this.e = ffVar.b().a();
        this.a = ffVar.b().c();
        this.j = aVar.a(this.c, this.o, this.d);
        this.n = ffVar.c();
        this.g = ffVar.a(this);
        this.f = ffVar.b(this);
        this.m = ffVar.c(this);
        this.r = ffVar.e(this);
        this.u = ffVar.a(this.g, this.m);
        this.t = ffVar.a(this.g);
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.u);
        arrayList.add(this.t);
        this.s = ffVar.a((List<ov>) arrayList, (oy) this);
        D();
        this.l = ffVar.a(this, this.d, new com.yandex.metrica.impl.ob.ju.a() {
            public void a(@NonNull aa aaVar, @NonNull jv jvVar) {
                fe.this.q.a(aaVar, jvVar);
            }
        });
        if (this.p.c()) {
            this.p.a("Read app environment for component %s. Value: %s", this.c.toString(), this.j.b().a);
        }
        this.q = ffVar.a(this.d, this.l, this.g, this.j, this.f);
        this.i = ffVar.d(this);
        this.h = ffVar.a(this, this.i);
        this.v = ffVar.a(this.d);
        this.g.a();
    }

    /* access modifiers changed from: protected */
    public hg f() {
        return this.i;
    }

    public void a(@NonNull aa aaVar) {
        if (this.o.c()) {
            this.o.a(aaVar, "Event received on service");
        }
        if (dl.a(this.c.a())) {
            this.h.b(aaVar);
        }
    }

    public synchronized void a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
        this.m.a(aVar);
        b(aVar);
    }

    public synchronized void g() {
        this.f.e();
    }

    @Nullable
    public String h() {
        return this.d.f();
    }

    @NonNull
    public we i() {
        return (we) this.m.d();
    }

    public lr j() {
        return this.g;
    }

    public fb c() {
        return this.c;
    }

    public synchronized void a(@Nullable yb ybVar) {
        this.m.a(ybVar);
        this.s.a();
    }

    public synchronized void a(@NonNull xv xvVar, @Nullable yb ybVar) {
    }

    public Context k() {
        return this.b;
    }

    @NonNull
    public abl l() {
        return this.o;
    }

    public void m() {
        this.q.b();
    }

    public void b(aa aaVar) {
        this.j.a(aaVar.k());
        com.yandex.metrica.impl.ob.i.a b2 = this.j.b();
        if (this.k.a(b2, this.d) && this.o.c()) {
            this.o.a("Save new app environment for %s. Value: %s", c(), b2.a);
        }
    }

    public void n() {
        this.j.a();
        this.k.b(this.j.b(), this.d);
    }

    public void a(String str) {
        this.d.a(str).q();
    }

    public void o() {
        this.d.d(p() + 1).q();
        this.m.a();
    }

    public int p() {
        return this.d.i();
    }

    public boolean q() {
        return this.q.c() && i().g();
    }

    public boolean r() {
        we i2 = i();
        return i2.K() && i2.g() && this.w.a(this.q.d(), i2.M(), "need to check permissions");
    }

    public boolean s() {
        we i2 = i();
        return i2.K() && this.w.a(this.q.d(), i2.N(), "should force send permissions");
    }

    public boolean t() {
        return this.q.e() && i().L() && i().g();
    }

    public mm u() {
        return this.e;
    }

    @Deprecated
    public final sz v() {
        return new sz(this.b, this.c.a());
    }

    public boolean w() {
        return this.a.a();
    }

    public boolean x() {
        boolean z = false;
        boolean b2 = this.e.b(false);
        boolean z2 = this.m.b().w;
        if (b2 && z2) {
            z = true;
        }
        return !z;
    }

    public void b() {
        dl.a((Closeable) this.f);
        dl.a((Closeable) this.g);
    }

    private void D() {
        long libraryApiLevel = (long) YandexMetrica.getLibraryApiLevel();
        if (this.d.g() < libraryApiLevel) {
            this.r.a(new sy(v())).a();
            this.d.d(libraryApiLevel).q();
        }
    }

    private void b(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
        if (aau.a(aVar.l)) {
            this.o.a();
        } else if (aau.c(aVar.l)) {
            this.o.b();
        }
    }

    public mo y() {
        return this.d;
    }

    public void b(@Nullable String str) {
        this.d.d(str).q();
    }

    @NonNull
    public jf z() {
        return this.n;
    }

    @NonNull
    public s A() {
        return this.v;
    }

    @Nullable
    public String B() {
        return this.d.h();
    }

    @NonNull
    public ox C() {
        return this.s;
    }

    @NonNull
    public com.yandex.metrica.CounterConfiguration.a a() {
        return com.yandex.metrica.CounterConfiguration.a.MANUAL;
    }
}
