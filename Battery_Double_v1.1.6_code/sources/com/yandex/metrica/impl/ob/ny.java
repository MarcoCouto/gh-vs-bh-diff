package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.vc.a.C0099a;
import com.yandex.metrica.impl.ob.vc.a.C0099a.C0100a;
import com.yandex.metrica.impl.ob.vf.a;
import com.yandex.metrica.impl.ob.vf.a.C0111a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ny implements np<a, vc.a> {
    private static final Map<Integer, cg.a> a = Collections.unmodifiableMap(new HashMap<Integer, cg.a>() {
        {
            put(Integer.valueOf(1), cg.a.WIFI);
            put(Integer.valueOf(2), cg.a.CELL);
        }
    });
    private static final Map<cg.a, Integer> b = Collections.unmodifiableMap(new HashMap<cg.a, Integer>() {
        {
            put(cg.a.WIFI, Integer.valueOf(1));
            put(cg.a.CELL, Integer.valueOf(2));
        }
    });

    @NonNull
    /* renamed from: a */
    public vc.a b(@NonNull a aVar) {
        vc.a aVar2 = new vc.a();
        Set a2 = aVar.a();
        aVar2.c = (String[]) a2.toArray(new String[a2.size()]);
        aVar2.b = b(aVar);
        return aVar2;
    }

    @NonNull
    public a a(@NonNull vc.a aVar) {
        return new a(b(aVar), Arrays.asList(aVar.c));
    }

    @NonNull
    private List<C0111a> b(@NonNull vc.a aVar) {
        C0099a[] aVarArr;
        ArrayList arrayList = new ArrayList();
        for (C0099a aVar2 : aVar.b) {
            C0111a aVar3 = new C0111a(aVar2.b, aVar2.c, aVar2.d, a(aVar2.e), aVar2.f, a(aVar2.g));
            arrayList.add(aVar3);
        }
        return arrayList;
    }

    @NonNull
    private acb<String, String> a(@NonNull C0100a[] aVarArr) {
        acb<String, String> acb = new acb<>();
        for (C0100a aVar : aVarArr) {
            acb.a(aVar.b, aVar.c);
        }
        return acb;
    }

    private C0099a[] b(@NonNull a aVar) {
        List b2 = aVar.b();
        C0099a[] aVarArr = new C0099a[b2.size()];
        for (int i = 0; i < b2.size(); i++) {
            aVarArr[i] = a((C0111a) b2.get(i));
        }
        return aVarArr;
    }

    @NonNull
    private C0099a a(@NonNull C0111a aVar) {
        C0099a aVar2 = new C0099a();
        aVar2.b = aVar.a;
        aVar2.c = aVar.b;
        aVar2.e = b(aVar);
        aVar2.d = aVar.c;
        aVar2.f = aVar.e;
        aVar2.g = a(aVar.f);
        return aVar2;
    }

    @NonNull
    private C0100a[] b(@NonNull C0111a aVar) {
        C0100a[] aVarArr = new C0100a[aVar.d.a()];
        int i = 0;
        for (Entry entry : aVar.d.b()) {
            for (String str : (Collection) entry.getValue()) {
                C0100a aVar2 = new C0100a();
                aVar2.b = (String) entry.getKey();
                aVar2.c = str;
                aVarArr[i] = aVar2;
                i++;
            }
        }
        return aVarArr;
    }

    @NonNull
    private List<cg.a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    private int[] a(@NonNull List<cg.a> list) {
        int[] iArr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            iArr[i] = ((Integer) b.get(list.get(i))).intValue();
        }
        return iArr;
    }
}
