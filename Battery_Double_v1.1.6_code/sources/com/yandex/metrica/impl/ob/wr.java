package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.Collections;
import java.util.List;

public class wr {
    @NonNull
    public final List<wu> a;
    @NonNull
    public final String b;
    public final long c;
    public final boolean d;
    public final boolean e;

    public wr(@NonNull List<wu> list, @NonNull String str, long j, boolean z, boolean z2) {
        this.a = Collections.unmodifiableList(list);
        this.b = str;
        this.c = j;
        this.d = z;
        this.e = z2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SdkFingerprintingState{sdkItemList=");
        sb.append(this.a);
        sb.append(", etag='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", lastAttemptTime=");
        sb.append(this.c);
        sb.append(", hasFirstCollectionOccurred=");
        sb.append(this.d);
        sb.append(", shouldRetry=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
