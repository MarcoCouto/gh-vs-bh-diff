package com.yandex.metrica.impl.ob;

public class xq {
    public final int a;
    public final int b;

    public xq(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RetryPolicyConfig{maxIntervalSeconds=");
        sb.append(this.a);
        sb.append(", exponentialMultiplier=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        xq xqVar = (xq) obj;
        if (this.a != xqVar.a) {
            return false;
        }
        if (this.b != xqVar.b) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (this.a * 31) + this.b;
    }
}
