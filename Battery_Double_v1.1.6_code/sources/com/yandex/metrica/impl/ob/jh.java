package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.va.a;

class jh implements nh<jg, a> {
    jh() {
    }

    @NonNull
    /* renamed from: a */
    public a b(@NonNull jg jgVar) {
        a aVar = new a();
        aVar.e = new int[jgVar.c().size()];
        int i = 0;
        for (Integer intValue : jgVar.c()) {
            aVar.e[i] = intValue.intValue();
            i++;
        }
        aVar.d = jgVar.d();
        aVar.c = jgVar.e();
        aVar.b = jgVar.b();
        return aVar;
    }

    @NonNull
    public jg a(@NonNull a aVar) {
        return new jg(aVar.b, aVar.c, aVar.d, aVar.e);
    }
}
