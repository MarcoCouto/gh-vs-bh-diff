package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.r.a;

public abstract class at<T> {
    @NonNull
    protected final a<T> a = new a<>();
    protected long b;
    @Nullable
    private au c;

    /* access modifiers changed from: protected */
    public abstract boolean b(@NonNull T t);

    public at(long j) {
        this.b = j;
    }

    public void a(@NonNull T t) {
        if (b(t)) {
            this.a.a(t);
            if (this.c != null) {
                this.c.c();
            }
        }
    }

    @Nullable
    public T a() {
        return this.a.a();
    }

    public void a(@NonNull au auVar) {
        this.c = auVar;
    }
}
