package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;

public class et {
    @Nullable
    private final eu a;
    @NonNull
    private final CounterConfiguration b;

    public et(@NonNull Bundle bundle) {
        this.a = eu.a(bundle);
        this.b = CounterConfiguration.c(bundle);
    }

    public et(@NonNull eu euVar, @NonNull CounterConfiguration counterConfiguration) {
        this.a = euVar;
        this.b = counterConfiguration;
    }

    @NonNull
    public eu g() {
        return this.a;
    }

    @NonNull
    public CounterConfiguration h() {
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ClientConfiguration{mProcessConfiguration=");
        sb.append(this.a);
        sb.append(", mCounterConfiguration=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }

    public static boolean a(@Nullable et etVar, @NonNull Context context) {
        return etVar == null || etVar.g() == null || !context.getPackageName().equals(etVar.g().i()) || etVar.g().h() != 87;
    }
}
