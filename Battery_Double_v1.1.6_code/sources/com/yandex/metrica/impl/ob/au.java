package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.TimeUnit;

public class au {
    @NonNull
    private final act a;
    /* access modifiers changed from: private */
    @NonNull
    public final aw b;
    @Nullable
    private Runnable c;

    public au(@NonNull act act, @NonNull aw awVar) {
        this.a = act;
        this.b = awVar;
    }

    public void a() {
        c();
    }

    public void b() {
        if (this.c != null) {
            this.a.b(this.c);
        }
    }

    public void c() {
        if (this.c != null) {
            this.a.b(this.c);
        }
        this.c = new Runnable() {
            public void run() {
                au.this.b.a();
            }
        };
        this.a.a(this.c, 90, TimeUnit.SECONDS);
    }
}
