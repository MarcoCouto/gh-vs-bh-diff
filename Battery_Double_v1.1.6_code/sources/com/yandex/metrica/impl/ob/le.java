package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.us.a;
import com.yandex.metrica.impl.ob.us.e;

public class le implements nh<ky, a> {
    @NonNull
    private lj a;

    public le(@NonNull lj ljVar) {
        this.a = ljVar;
    }

    @NonNull
    /* renamed from: a */
    public a b(@NonNull ky kyVar) {
        a aVar = new a();
        aVar.b = this.a.b(kyVar.a);
        aVar.c = new e[kyVar.b.size()];
        int i = 0;
        for (lc a2 : kyVar.b) {
            aVar.c[i] = this.a.b(a2);
            i++;
        }
        return aVar;
    }

    @NonNull
    public ky a(@NonNull a aVar) {
        throw new UnsupportedOperationException();
    }
}
