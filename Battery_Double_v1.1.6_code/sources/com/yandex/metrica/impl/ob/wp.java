package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class wp {
    @NonNull
    private act a;
    @NonNull
    private final mx<wr> b;
    @NonNull
    private a c;
    @NonNull
    private ng d;
    @NonNull
    private final b e;
    @NonNull
    private final abs f;
    @NonNull
    private final sd g;
    /* access modifiers changed from: private */
    @Nullable
    public String h;

    public static class a {
        @NonNull
        private final wv a;

        public a() {
            this(new wv());
        }

        @VisibleForTesting
        a(@NonNull wv wvVar) {
            this.a = wvVar;
        }

        @NonNull
        public List<wu> a(@Nullable byte[] bArr) {
            List<wu> list;
            List<wu> arrayList = new ArrayList<>();
            if (dl.a(bArr)) {
                return arrayList;
            }
            try {
                list = this.a.a(new String(bArr, "UTF-8"));
            } catch (UnsupportedEncodingException unused) {
                list = arrayList;
            }
            return list;
        }
    }

    static class b {
        b() {
        }

        @Nullable
        public HttpURLConnection a(@NonNull String str, @NonNull String str2) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                if (!TextUtils.isEmpty(str)) {
                    httpURLConnection.setRequestProperty(HttpRequest.HEADER_IF_NONE_MATCH, str);
                }
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setConnectTimeout(com.yandex.metrica.impl.ob.sg.a.a);
                httpURLConnection.setReadTimeout(com.yandex.metrica.impl.ob.sg.a.a);
                httpURLConnection.connect();
                return httpURLConnection;
            } catch (Throwable unused) {
                return null;
            }
        }
    }

    public wp(@NonNull Context context, @Nullable String str, @NonNull act act) {
        this(str, com.yandex.metrica.impl.ob.op.a.a(wr.class).a(context), new a(), new b(), act, new ng(), new abs(), new sd(context));
    }

    @VisibleForTesting
    wp(@Nullable String str, @NonNull mx mxVar, @NonNull a aVar, @NonNull b bVar, @NonNull act act, @NonNull ng ngVar, @NonNull abs abs, @NonNull sd sdVar) {
        this.h = str;
        this.b = mxVar;
        this.c = aVar;
        this.e = bVar;
        this.a = act;
        this.d = ngVar;
        this.f = abs;
        this.g = sdVar;
    }

    public void a(@NonNull final wo woVar) {
        this.a.a((Runnable) new Runnable() {
            public void run() {
                wp.this.a(woVar, wp.this.h);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull wo woVar, String str) {
        wr wrVar = (wr) this.b.a();
        wr wrVar2 = null;
        if (this.g.a() && str != null) {
            try {
                HttpURLConnection a2 = this.e.a(wrVar.b, str);
                if (a2 != null) {
                    wrVar2 = a(a2, wrVar);
                }
            } catch (Throwable unused) {
            }
        }
        if (wrVar2 != null) {
            woVar.a(wrVar2);
        } else {
            woVar.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public wr a(@NonNull HttpURLConnection httpURLConnection, @NonNull wr wrVar) throws IOException {
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            String a2 = dh.a(httpURLConnection.getHeaderField(HttpRequest.HEADER_ETAG));
            try {
                wr wrVar2 = new wr(this.c.a(this.d.a(ax.b(httpURLConnection.getInputStream()), "af9202nao18gswqp")), a2, this.f.a(), true, false);
                return wrVar2;
            } catch (IOException unused) {
            }
        } else if (responseCode == 304) {
            wr wrVar3 = new wr(wrVar.a, wrVar.b, this.f.a(), true, false);
            return wrVar3;
        }
        return null;
    }

    public void a(@Nullable yb ybVar) {
        if (ybVar != null) {
            this.h = ybVar.h;
        }
    }

    public boolean b(@NonNull yb ybVar) {
        boolean z = true;
        if (this.h != null) {
            return !this.h.equals(ybVar.h);
        }
        if (ybVar.h == null) {
            z = false;
        }
        return z;
    }
}
