package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.Closeable;

public class gv implements fk, fl, ha {
    @NonNull
    private final Context a;
    @NonNull
    private final fb b;
    @NonNull
    private final ag c;
    @NonNull
    private gw d;
    @NonNull
    private gb e;
    @NonNull
    private final com.yandex.metrica.CounterConfiguration.a f;

    static class a {
        a() {
        }

        public gw a(@NonNull Context context, @NonNull fb fbVar, @NonNull yb ybVar, @NonNull com.yandex.metrica.impl.ob.gz.a aVar) {
            return new gw(new com.yandex.metrica.impl.ob.gz.b(context, fbVar.b()), ybVar, aVar);
        }
    }

    static class b {
        b() {
        }

        public ag<gv> a(@NonNull gv gvVar, @NonNull ye yeVar, @NonNull hb hbVar, @NonNull mo moVar) {
            return new ag<>(gvVar, yeVar.a(), hbVar, moVar);
        }
    }

    public void a(@NonNull xv xvVar, @Nullable yb ybVar) {
    }

    public gv(@NonNull Context context, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.ew.a aVar, @NonNull yb ybVar, @NonNull ye yeVar, @NonNull com.yandex.metrica.CounterConfiguration.a aVar2) {
        Context context2 = context;
        this(context2, fbVar, aVar, ybVar, yeVar, aVar2, new hb(), new b(), new a(), new gb(context, fbVar), new mo(lv.a(context).b(fbVar)));
    }

    public gv(@NonNull Context context, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.ew.a aVar, @NonNull yb ybVar, @NonNull ye yeVar, @NonNull com.yandex.metrica.CounterConfiguration.a aVar2, @NonNull hb hbVar, @NonNull b bVar, @NonNull a aVar3, @NonNull gb gbVar, @NonNull mo moVar) {
        this.a = context;
        this.b = fbVar;
        this.e = gbVar;
        this.f = aVar2;
        this.c = bVar.a(this, yeVar, hbVar, moVar);
        synchronized (this) {
            this.e.a(ybVar.z);
            this.d = aVar3.a(context, fbVar, ybVar, new com.yandex.metrica.impl.ob.gz.a(aVar));
        }
    }

    @NonNull
    public fb c() {
        return this.b;
    }

    public void a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
        this.d.a(aVar);
    }

    public void a(@NonNull aa aaVar) {
        this.c.a(aaVar);
    }

    public void a() {
        if (this.e.a(((gz) this.d.d()).c())) {
            a(al.a());
            this.e.a();
        }
    }

    public synchronized void a(@Nullable yb ybVar) {
        this.d.a(ybVar);
        this.e.a(ybVar.z);
    }

    @NonNull
    public gz d() {
        return (gz) this.d.d();
    }

    public void b() {
        dl.a((Closeable) this.c);
    }
}
