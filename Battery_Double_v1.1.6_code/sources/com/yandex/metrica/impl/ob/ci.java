package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.SparseArray;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.al.a;
import com.yandex.metrica.impl.ob.p.a.C0090a;
import com.yandex.metrica.impl.ob.uu.c.C0093c;
import com.yandex.metrica.impl.ob.uu.c.e;
import com.yandex.metrica.impl.ob.uu.c.e.b;
import com.yandex.metrica.impl.ob.uu.c.f;
import com.yandex.metrica.impl.ob.uu.c.g;
import com.yandex.metrica.impl.ob.uu.d;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class ci {
    private static Map<jy, Integer> a;
    private static SparseArray<jy> b;
    private static final Map<a, Integer> c;
    private static final Map<a, tp> d;

    public static void a(e eVar) {
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(jy.FOREGROUND, Integer.valueOf(0));
        hashMap.put(jy.BACKGROUND, Integer.valueOf(1));
        a = Collections.unmodifiableMap(hashMap);
        SparseArray<jy> sparseArray = new SparseArray<>();
        sparseArray.put(0, jy.FOREGROUND);
        sparseArray.put(1, jy.BACKGROUND);
        b = sparseArray;
        HashMap hashMap2 = new HashMap();
        hashMap2.put(a.EVENT_TYPE_INIT, Integer.valueOf(1));
        hashMap2.put(a.EVENT_TYPE_REGULAR, Integer.valueOf(4));
        hashMap2.put(a.EVENT_TYPE_SEND_REFERRER, Integer.valueOf(5));
        hashMap2.put(a.EVENT_TYPE_ALIVE, Integer.valueOf(7));
        hashMap2.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED, Integer.valueOf(3));
        hashMap2.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, Integer.valueOf(26));
        hashMap2.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, Integer.valueOf(26));
        hashMap2.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, Integer.valueOf(26));
        hashMap2.put(a.EVENT_TYPE_ANR, Integer.valueOf(25));
        hashMap2.put(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, Integer.valueOf(3));
        hashMap2.put(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, Integer.valueOf(3));
        hashMap2.put(a.EVENT_TYPE_EXCEPTION_USER, Integer.valueOf(6));
        hashMap2.put(a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, Integer.valueOf(27));
        hashMap2.put(a.EVENT_TYPE_IDENTITY, Integer.valueOf(8));
        hashMap2.put(a.EVENT_TYPE_IDENTITY_LIGHT, Integer.valueOf(28));
        hashMap2.put(a.EVENT_TYPE_STATBOX, Integer.valueOf(11));
        hashMap2.put(a.EVENT_TYPE_SET_USER_INFO, Integer.valueOf(12));
        hashMap2.put(a.EVENT_TYPE_REPORT_USER_INFO, Integer.valueOf(12));
        hashMap2.put(a.EVENT_TYPE_FIRST_ACTIVATION, Integer.valueOf(13));
        hashMap2.put(a.EVENT_TYPE_START, Integer.valueOf(2));
        hashMap2.put(a.EVENT_TYPE_APP_OPEN, Integer.valueOf(16));
        hashMap2.put(a.EVENT_TYPE_APP_UPDATE, Integer.valueOf(17));
        hashMap2.put(a.EVENT_TYPE_PERMISSIONS, Integer.valueOf(18));
        hashMap2.put(a.EVENT_TYPE_APP_FEATURES, Integer.valueOf(19));
        hashMap2.put(a.EVENT_TYPE_SEND_USER_PROFILE, Integer.valueOf(20));
        hashMap2.put(a.EVENT_TYPE_SEND_REVENUE_EVENT, Integer.valueOf(21));
        hashMap2.put(a.EVENT_TYPE_CLEANUP, Integer.valueOf(29));
        hashMap2.put(a.EVENT_TYPE_VIEW_TREE, Integer.valueOf(30));
        c = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap();
        tk tkVar = new tk();
        tm tmVar = new tm();
        tl tlVar = new tl();
        tj tjVar = new tj();
        tp tpVar = new tp((tx) new tx() {
            @NonNull
            public byte[] a(@NonNull to toVar) {
                if (TextUtils.isEmpty(toVar.b)) {
                    return new byte[0];
                }
                return dh.c(ax.c(toVar.b));
            }
        });
        tp tpVar2 = new tp((tx) tmVar);
        tp tpVar3 = new tp((tx) tjVar);
        tp tpVar4 = new tp((ts) tkVar);
        hashMap3.put(a.EVENT_TYPE_REGULAR, tpVar2);
        hashMap3.put(a.EVENT_TYPE_SEND_REFERRER, new tp((tx) new tx() {
            @NonNull
            public byte[] a(@NonNull to toVar) {
                byte[] bArr;
                if (!TextUtils.isEmpty(toVar.b)) {
                    try {
                        vu a = vu.a(Base64.decode(toVar.b, 0));
                        uv uvVar = new uv();
                        if (a.a == null) {
                            bArr = new byte[0];
                        } else {
                            bArr = a.a.getBytes();
                        }
                        uvVar.b = bArr;
                        uvVar.d = a.b;
                        uvVar.c = a.c;
                        return e.a((e) uvVar);
                    } catch (Throwable unused) {
                    }
                }
                return new byte[0];
            }
        }));
        hashMap3.put(a.EVENT_TYPE_ALIVE, new tp(tkVar, tlVar));
        hashMap3.put(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, tpVar);
        hashMap3.put(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, tpVar);
        hashMap3.put(a.EVENT_TYPE_EXCEPTION_USER, tpVar2);
        hashMap3.put(a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, tpVar3);
        hashMap3.put(a.EVENT_TYPE_IDENTITY, new tp(new tv(), tmVar, new tr(), new tu()));
        hashMap3.put(a.EVENT_TYPE_STATBOX, tpVar2);
        hashMap3.put(a.EVENT_TYPE_SET_USER_INFO, tpVar2);
        hashMap3.put(a.EVENT_TYPE_REPORT_USER_INFO, tpVar2);
        hashMap3.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED, tpVar2);
        hashMap3.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, tpVar3);
        hashMap3.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, tpVar3);
        hashMap3.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, tpVar3);
        hashMap3.put(a.EVENT_TYPE_ANR, tpVar3);
        hashMap3.put(a.EVENT_TYPE_START, new tp(new tk(), tjVar));
        hashMap3.put(a.EVENT_TYPE_CUSTOM_EVENT, new tp((tq) new tq() {
            @Nullable
            public Integer a(@NonNull to toVar) {
                return toVar.k;
            }
        }));
        hashMap3.put(a.EVENT_TYPE_APP_OPEN, tpVar2);
        hashMap3.put(a.EVENT_TYPE_PERMISSIONS, tpVar4);
        hashMap3.put(a.EVENT_TYPE_APP_FEATURES, tpVar4);
        hashMap3.put(a.EVENT_TYPE_SEND_USER_PROFILE, tpVar3);
        hashMap3.put(a.EVENT_TYPE_SEND_REVENUE_EVENT, tpVar3);
        hashMap3.put(a.EVENT_TYPE_VIEW_TREE, tpVar2);
        d = Collections.unmodifiableMap(hashMap3);
    }

    @NonNull
    public static g a(ContentValues contentValues) {
        return a(contentValues.getAsLong("start_time"), contentValues.getAsLong("server_time_offset"), contentValues.getAsBoolean("obtained_before_first_sync"));
    }

    public static f a(zi ziVar) {
        f fVar = new f();
        if (ziVar.a() != null) {
            fVar.b = ziVar.a().intValue();
        }
        if (ziVar.b() != null) {
            fVar.c = ziVar.b().intValue();
        }
        if (!TextUtils.isEmpty(ziVar.d())) {
            fVar.d = ziVar.d();
        }
        fVar.e = ziVar.c();
        if (!TextUtils.isEmpty(ziVar.e())) {
            fVar.f = ziVar.e();
        }
        return fVar;
    }

    @NonNull
    static jy a(int i) {
        jy jyVar = (jy) b.get(i);
        return jyVar == null ? jy.FOREGROUND : jyVar;
    }

    public static d[] a(JSONArray jSONArray) {
        try {
            d[] dVarArr = new d[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    dVarArr[i] = b(jSONArray.getJSONObject(i));
                    i++;
                } catch (Throwable unused) {
                    return dVarArr;
                }
            }
            return dVarArr;
        } catch (Throwable unused2) {
            return null;
        }
    }

    @NonNull
    private static d b(JSONObject jSONObject) {
        try {
            d dVar = new d();
            dVar.b = jSONObject.getString("mac");
            dVar.c = jSONObject.getInt("signal_strength");
            dVar.d = jSONObject.getString("ssid");
            dVar.e = jSONObject.optBoolean("is_connected");
            dVar.f = jSONObject.optLong("last_visible_offset_seconds", 0);
            return dVar;
        } catch (Throwable unused) {
            d dVar2 = new d();
            dVar2.b = jSONObject.optString("mac");
            return dVar2;
        }
    }

    private static g a(@Nullable Long l) {
        g gVar = new g();
        if (l != null) {
            gVar.b = l.longValue();
            gVar.c = abu.a(l.longValue());
        }
        return gVar;
    }

    @NonNull
    private static g a(@Nullable Long l, @Nullable Long l2, @Nullable Boolean bool) {
        g a2 = a(l);
        if (l2 != null) {
            a2.d = l2.longValue();
        }
        if (bool != null) {
            a2.e = bool.booleanValue();
        }
        return a2;
    }

    @NonNull
    public static b a(@NonNull String str, int i, @NonNull g gVar) {
        b bVar = new b();
        bVar.b = gVar;
        bVar.c = str;
        bVar.d = i;
        return bVar;
    }

    static int a(@NonNull jy jyVar) {
        Integer num = (Integer) a.get(jyVar);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @Nullable
    public static C0093c[] a(Context context) {
        List c2 = dn.a(context).c();
        if (dl.a((Collection) c2)) {
            return null;
        }
        C0093c[] cVarArr = new C0093c[c2.size()];
        for (int i = 0; i < c2.size(); i++) {
            C0093c cVar = new C0093c();
            bx bxVar = (bx) c2.get(i);
            cVar.b = bxVar.a;
            cVar.c = bxVar.b;
            cVarArr[i] = cVar;
        }
        return cVarArr;
    }

    @NonNull
    public static tp a(@Nullable a aVar, boolean z) {
        tp tpVar;
        if (aVar != null) {
            switch (aVar) {
                case EVENT_TYPE_INIT:
                case EVENT_TYPE_FIRST_ACTIVATION:
                case EVENT_TYPE_APP_UPDATE:
                    if (!z) {
                        tpVar = new tp((tx) new tl());
                        break;
                    } else {
                        tpVar = new tp((tx) new tm());
                        break;
                    }
                default:
                    tpVar = (tp) d.get(aVar);
                    break;
            }
        } else {
            tpVar = null;
        }
        return tpVar == null ? new tp() : tpVar;
    }

    @Nullable
    public static uu.a[] b(@NonNull JSONArray jSONArray) {
        try {
            uu.a[] aVarArr = new uu.a[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        aVarArr[i] = a(optJSONObject);
                    }
                    i++;
                } catch (Throwable unused) {
                    return aVarArr;
                }
            }
            return aVarArr;
        } catch (Throwable unused2) {
            return null;
        }
    }

    @NonNull
    public static uu.a a(JSONObject jSONObject) {
        uu.a aVar = new uu.a();
        int optInt = jSONObject.optInt("signal_strength", aVar.c);
        if (optInt != -1) {
            aVar.c = optInt;
        }
        aVar.b = jSONObject.optInt("cell_id", aVar.b);
        aVar.d = jSONObject.optInt("lac", aVar.d);
        aVar.e = jSONObject.optInt(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, aVar.e);
        aVar.f = jSONObject.optInt("operator_id", aVar.f);
        aVar.g = jSONObject.optString("operator_name", aVar.g);
        aVar.h = jSONObject.optBoolean("is_connected", aVar.h);
        aVar.i = jSONObject.optInt("cell_type", 0);
        aVar.j = jSONObject.optInt("pci", aVar.j);
        aVar.k = jSONObject.optLong("last_visible_time_offset", aVar.k);
        return aVar;
    }

    @Nullable
    public static Integer a(@Nullable a aVar) {
        if (aVar == null) {
            return null;
        }
        return (Integer) c.get(aVar);
    }

    public static int a(@NonNull C0090a aVar) {
        switch (aVar) {
            case NONE:
                return 1;
            case USB:
                return 2;
            case AC:
                return 4;
            case WIRELESS:
                return 3;
            default:
                return 0;
        }
    }

    public static int a(@NonNull qm.a aVar) {
        switch (aVar) {
            case VISIBLE:
                return 2;
            case FOREGROUND:
                return 0;
            case BACKGROUND:
                return 1;
            default:
                return 3;
        }
    }
}
