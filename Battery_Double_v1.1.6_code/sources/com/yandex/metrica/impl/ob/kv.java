package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.metrica.impl.ob.al.a;

public class kv {
    private static SparseArray<kv> c = new SparseArray<>();
    public final String a;
    public final String b;

    static {
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED.a(), new kv("jvm", "binder"));
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF.a(), new kv("jvm", "binder"));
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.a(), new kv("jvm", "intent"));
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE.a(), new kv("jvm", ParametersKeys.FILE));
        c.put(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH.a(), new kv("jni_native", ParametersKeys.FILE));
        c.put(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH.a(), new kv("jni_native", ParametersKeys.FILE));
    }

    private kv(@NonNull String str, @NonNull String str2) {
        this.a = str;
        this.b = str2;
    }

    public static kv a(int i) {
        return (kv) c.get(i);
    }
}
