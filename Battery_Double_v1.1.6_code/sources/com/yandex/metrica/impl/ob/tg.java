package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class tg extends ta {
    private th d;

    /* access modifiers changed from: protected */
    public String f() {
        return "_serviceproviderspreferences";
    }

    public tg(Context context) {
        this(context, null);
    }

    public tg(Context context, String str) {
        super(context, str);
        this.d = new th("LOCATION_TRACKING_ENABLED");
    }

    public boolean a() {
        return this.c.getBoolean(this.d.b(), false);
    }

    public void b() {
        h(this.d.b()).j();
    }
}
