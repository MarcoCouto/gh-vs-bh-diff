package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.uz.a;
import com.yandex.metrica.impl.ob.uz.a.b;
import java.util.ArrayList;

public class ne implements nh<sk, a> {
    @NonNull
    private final nf a;

    public ne() {
        this(new nf());
    }

    @VisibleForTesting
    ne(@NonNull nf nfVar) {
        this.a = nfVar;
    }

    @NonNull
    /* renamed from: a */
    public a b(@NonNull sk skVar) {
        a aVar = new a();
        aVar.b = new b[skVar.a.size()];
        int i = 0;
        int i2 = 0;
        for (sr a2 : skVar.a) {
            aVar.b[i2] = a(a2);
            i2++;
        }
        if (skVar.b != null) {
            aVar.c = this.a.b(skVar.b);
        }
        aVar.d = new String[skVar.c.size()];
        for (String str : skVar.c) {
            aVar.d[i] = str;
            i++;
        }
        return aVar;
    }

    @NonNull
    public sk a(@NonNull a aVar) {
        ArrayList arrayList = new ArrayList();
        for (b a2 : aVar.b) {
            arrayList.add(a(a2));
        }
        m mVar = null;
        if (aVar.c != null) {
            mVar = this.a.a(aVar.c);
        }
        ArrayList arrayList2 = new ArrayList();
        for (String add : aVar.d) {
            arrayList2.add(add);
        }
        return new sk(arrayList, mVar, arrayList2);
    }

    private b a(@NonNull sr srVar) {
        b bVar = new b();
        bVar.b = srVar.a;
        bVar.c = srVar.b;
        return bVar;
    }

    private sr a(@NonNull b bVar) {
        return new sr(bVar.b, bVar.c);
    }
}
