package com.yandex.metrica.impl.ob;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public abstract class sg {
    private final String a;

    public static final class a {
        public static final int a = ((int) TimeUnit.SECONDS.toMillis(30));
    }

    public abstract boolean b();

    public sg(String str) {
        this.a = str;
    }

    public HttpURLConnection a() throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.a).openConnection();
        httpURLConnection.setConnectTimeout(a.a);
        httpURLConnection.setReadTimeout(a.a);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestProperty("Accept", "application/json");
        httpURLConnection.setRequestProperty("User-Agent", cx.a("com.yandex.mobile.metrica.sdk"));
        return httpURLConnection;
    }
}
