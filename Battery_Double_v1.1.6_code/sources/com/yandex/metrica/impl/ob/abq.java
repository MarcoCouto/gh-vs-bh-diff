package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class abq {
    @NonNull
    public static String a(Map<String, String> map) {
        String str = "";
        if (dl.a((Map) map)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (Entry entry : map.entrySet()) {
            if (!TextUtils.isEmpty((CharSequence) entry.getKey())) {
                sb.append((String) entry.getKey());
                sb.append(":");
                sb.append((String) entry.getValue());
                sb.append(",");
            }
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    @NonNull
    public static Map<String, String> a(@Nullable String str) {
        String[] split;
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(str)) {
            for (String str2 : str.split(",")) {
                int indexOf = str2.indexOf(":");
                if (indexOf != -1) {
                    String substring = str2.substring(0, indexOf);
                    if (!TextUtils.isEmpty(substring)) {
                        hashMap.put(substring, str2.substring(indexOf + 1));
                    }
                }
            }
        }
        return hashMap;
    }

    public static boolean b(String str) {
        return b(a(str));
    }

    public static boolean b(Map<String, String> map) {
        if (map.isEmpty()) {
            return false;
        }
        for (Entry value : map.entrySet()) {
            try {
                Integer.parseInt((String) value.getValue());
            } catch (Throwable unused) {
                return false;
            }
        }
        return true;
    }

    public static Map<String, String> c(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                if (d((String) entry.getKey()) && c((String) entry.getValue())) {
                    hashMap.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return hashMap;
    }

    private static boolean c(String str) {
        return !TextUtils.isEmpty(str) && abk.a(str, -1) != -1;
    }

    private static boolean d(String str) {
        return !TextUtils.isEmpty(str) && !str.contains(":") && !str.contains(",") && !str.contains(RequestParameters.AMPERSAND);
    }
}
