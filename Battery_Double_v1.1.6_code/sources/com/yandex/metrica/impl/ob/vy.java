package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Locale;
import java.util.Map;

public class vy {
    private String a;
    private x b;
    private final String c = "3.13.1";
    private final String d = "72430";
    @NonNull
    private final String e = a();
    private final String f = "android";
    private final String g = "2";
    @NonNull
    private String h = cx.b();
    @Nullable
    private final String i = "92eae106af06d42d4d1aed7145c2da6c30b52a3d";
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    @NonNull
    private Map<com.yandex.metrica.impl.ob.pc.a, pc> q;
    private String r = com.yandex.metrica.b.PHONE.name().toLowerCase(Locale.US);
    private String s;
    private yb t;

    public static abstract class a<I, O> implements vx<I, O> {
        @Nullable
        public final String c;
        @Nullable
        public final String d;
        @Nullable
        public final String e;

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3) {
            this.c = str;
            this.d = str2;
            this.e = str3;
        }
    }

    protected static abstract class b<T extends vy, A extends a> implements d<T, c<A>> {
        @NonNull
        final Context a;
        @NonNull
        final String b;

        /* access modifiers changed from: protected */
        @NonNull
        public abstract T b();

        protected b(@NonNull Context context, @NonNull String str) {
            this.a = context;
            this.b = str;
        }

        @NonNull
        /* renamed from: c */
        public T a(@NonNull c<A> cVar) {
            T b2 = b();
            x a2 = x.a(this.a);
            b2.a(a2);
            b2.a(cVar.a);
            b2.k(a(this.a, ((a) cVar.b).c));
            b2.i((String) abw.b(a2.a(cVar.a), ""));
            c(b2, cVar);
            a(b2, this.b, ((a) cVar.b).d, this.a);
            b(b2, this.b, ((a) cVar.b).e, this.a);
            b2.b(this.b);
            b2.a(pe.a().c(this.a));
            b2.j(bi.a(this.a).a());
            return b2;
        }

        private void a(@NonNull T t, @NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (TextUtils.isEmpty(str2)) {
                str2 = dl.b(context, str);
            }
            t.d(str2);
        }

        private void b(@NonNull T t, @NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (TextUtils.isEmpty(str2)) {
                str2 = dl.a(context, str);
            }
            t.c(str2);
        }

        private synchronized void c(@NonNull T t, @NonNull c<A> cVar) {
            t.e(a(cVar));
            a(t, cVar);
            b(t, cVar);
        }

        /* access modifiers changed from: 0000 */
        public void a(T t, @NonNull c<A> cVar) {
            t.f(cVar.a.b);
            t.h(cVar.a.d);
        }

        /* access modifiers changed from: 0000 */
        public void b(T t, @NonNull c<A> cVar) {
            t.g(cVar.a.c);
        }

        private String a(@NonNull c<A> cVar) {
            return cVar.a.a;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public String a(@NonNull Context context, @Nullable String str) {
            return str == null ? x.a(context).g : str;
        }
    }

    public static class c<A> {
        @NonNull
        public final yb a;
        @NonNull
        public final A b;

        public c(@NonNull yb ybVar, A a2) {
            this.a = ybVar;
            this.b = a2;
        }
    }

    public interface d<T extends vy, D> {
        @NonNull
        T a(D d);
    }

    @Nullable
    public String F() {
        return "92eae106af06d42d4d1aed7145c2da6c30b52a3d";
    }

    public String i() {
        return "2";
    }

    public String j() {
        return "3.13.1";
    }

    public String k() {
        return "72430";
    }

    public String m() {
        return "android";
    }

    public String d() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public yb e() {
        return this.t;
    }

    @NonNull
    public xq f() {
        return this.t.E;
    }

    public synchronized boolean g() {
        return !dh.a(u(), s(), this.o);
    }

    /* access modifiers changed from: protected */
    public void a(x xVar) {
        this.b = xVar;
    }

    /* access modifiers changed from: protected */
    public void a(yb ybVar) {
        this.t = ybVar;
    }

    @NonNull
    public String h() {
        return (String) abw.b(this.b.b, "");
    }

    @NonNull
    public String l() {
        return this.e;
    }

    @NonNull
    public String n() {
        return this.b.c;
    }

    @NonNull
    public String o() {
        return this.b.d;
    }

    public int p() {
        return this.b.e;
    }

    /* access modifiers changed from: protected */
    public void c(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.k = str;
        }
    }

    public String q() {
        return (String) abw.b(this.k, "");
    }

    public String r() {
        return (String) abw.b(this.j, "");
    }

    /* access modifiers changed from: protected */
    public void d(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.j = str;
        }
    }

    @NonNull
    public synchronized String s() {
        return (String) abw.b(this.m, "");
    }

    @NonNull
    public synchronized String t() {
        return (String) abw.b(this.n, "");
    }

    @NonNull
    public synchronized String u() {
        return (String) abw.b(this.l, "");
    }

    /* access modifiers changed from: protected */
    public synchronized void e(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.l = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void f(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.m = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void g(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.n = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void h(String str) {
        this.o = str;
    }

    public void i(String str) {
        this.p = str;
    }

    @NonNull
    public String v() {
        return this.b.h;
    }

    @NonNull
    public String w() {
        return this.h;
    }

    public int x() {
        return this.b.f.a;
    }

    public int y() {
        return this.b.f.b;
    }

    public int z() {
        return this.b.f.c;
    }

    public float A() {
        return this.b.f.d;
    }

    @NonNull
    public String B() {
        return (String) abw.b(this.s, "");
    }

    /* access modifiers changed from: 0000 */
    public final void j(String str) {
        this.s = str;
    }

    @NonNull
    public String C() {
        return this.p;
    }

    @NonNull
    public String D() {
        return (String) abw.b(this.r, com.yandex.metrica.b.PHONE.name().toLowerCase(Locale.US));
    }

    /* access modifiers changed from: 0000 */
    public void k(String str) {
        this.r = str;
    }

    @NonNull
    public Map<com.yandex.metrica.impl.ob.pc.a, pc> E() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Map<com.yandex.metrica.impl.ob.pc.a, pc> map) {
        this.q = map;
    }

    @NonNull
    private static String a() {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty("public")) {
            sb.append("public");
        }
        if (!TextUtils.isEmpty("binary")) {
            sb.append("_binary");
        }
        if (!TextUtils.isEmpty("")) {
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        }
        return sb.toString();
    }
}
