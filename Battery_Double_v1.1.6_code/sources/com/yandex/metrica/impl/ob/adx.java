package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.UUID;

public class adx implements adw<String> {
    public adu a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return adu.a(this, "ApiKey is empty. Please, read official documentation how to obtain one: https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/concepts/android-initialize-docpage/");
        }
        try {
            UUID.fromString(str);
            return adu.a(this);
        } catch (Throwable unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Invalid ApiKey=");
            sb.append(str);
            sb.append(". ");
            sb.append("Please, read official documentation how to obtain one:");
            sb.append(" ");
            sb.append("https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/concepts/android-initialize-docpage/");
            return adu.a(this, sb.toString());
        }
    }
}
