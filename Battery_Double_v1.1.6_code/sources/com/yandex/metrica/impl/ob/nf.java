package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.m.a;
import com.yandex.metrica.impl.ob.uz.a.C0097a;

public class nf implements nh<m, C0097a> {
    @NonNull
    /* renamed from: a */
    public C0097a b(@NonNull m mVar) {
        C0097a aVar = new C0097a();
        if (mVar.a != null) {
            switch (mVar.a) {
                case ACTIVE:
                    aVar.b = 1;
                    break;
                case WORKING_SET:
                    aVar.b = 2;
                    break;
                case FREQUENT:
                    aVar.b = 3;
                    break;
                case RARE:
                    aVar.b = 4;
                    break;
            }
        }
        if (mVar.b != null) {
            if (mVar.b.booleanValue()) {
                aVar.c = 1;
            } else {
                aVar.c = 0;
            }
        }
        return aVar;
    }

    @NonNull
    public m a(@NonNull C0097a aVar) {
        a aVar2;
        Boolean bool = null;
        switch (aVar.b) {
            case 1:
                aVar2 = a.ACTIVE;
                break;
            case 2:
                aVar2 = a.WORKING_SET;
                break;
            case 3:
                aVar2 = a.FREQUENT;
                break;
            case 4:
                aVar2 = a.RARE;
                break;
            default:
                aVar2 = null;
                break;
        }
        switch (aVar.c) {
            case 0:
                bool = Boolean.valueOf(false);
                break;
            case 1:
                bool = Boolean.valueOf(true);
                break;
        }
        return new m(aVar2, bool);
    }
}
