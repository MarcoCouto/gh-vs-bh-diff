package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.view.View;

public interface aaq<T extends View> {
    @NonNull
    c a();

    boolean a(@NonNull T t);
}
