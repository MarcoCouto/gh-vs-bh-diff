package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

public class by extends acx {
    @NonNull
    private final Executor a;
    private Executor b;
    private final BlockingQueue<a> c = new LinkedBlockingQueue();
    private final Object d = new Object();
    private final Object e = new Object();
    private volatile a f;
    @NonNull
    private sd g;
    @NonNull
    private final mq h;

    private static class a {
        @NonNull
        final ca a;
        @NonNull
        private final String b;

        private a(@NonNull ca caVar) {
            this.a = caVar;
            this.b = caVar.n();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.b.equals(((a) obj).b);
        }

        public int hashCode() {
            return this.b.hashCode();
        }
    }

    public by(@NonNull Context context, @NonNull Executor executor, @NonNull mq mqVar) {
        this.a = executor;
        this.h = mqVar;
        this.b = new acp();
        this.g = new sd(context);
    }

    public void a(ca caVar) {
        synchronized (this.d) {
            a aVar = new a(caVar);
            if (c() && !a(aVar)) {
                aVar.a.C();
                this.c.offer(aVar);
            }
        }
    }

    public void a() {
        synchronized (this.e) {
            a aVar = this.f;
            if (aVar != null) {
                aVar.a.w();
                aVar.a.D();
            }
            while (!this.c.isEmpty()) {
                try {
                    ((a) this.c.take()).a.D();
                } catch (InterruptedException unused) {
                }
            }
            b();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0043 A[SYNTHETIC] */
    public void run() {
        ca caVar = null;
        while (c()) {
            try {
                synchronized (this.e) {
                }
                this.f = (a) this.c.take();
                ca caVar2 = this.f.a;
                try {
                    c(caVar2).execute(b(caVar2));
                    synchronized (this.e) {
                        this.f = null;
                        if (caVar2 != null) {
                            caVar2.D();
                        }
                    }
                    caVar = caVar2;
                } catch (InterruptedException unused) {
                    caVar = caVar2;
                    synchronized (this.e) {
                        this.f = null;
                        if (caVar != null) {
                            caVar.D();
                        }
                    }
                } catch (Throwable th) {
                    ca caVar3 = caVar2;
                    th = th;
                    caVar = caVar3;
                    synchronized (this.e) {
                        this.f = null;
                        if (caVar != null) {
                            caVar.D();
                        }
                    }
                    throw th;
                }
            } catch (InterruptedException unused2) {
            } catch (Throwable th2) {
                th = th2;
                synchronized (this.e) {
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public cd b(@NonNull ca caVar) {
        cd cdVar = new cd(this.g, new se(new sf(this.h, caVar.E()), caVar.F()), caVar, this, "NetworkTaskQueue");
        return cdVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Executor c(ca caVar) {
        if (caVar.o()) {
            return this.a;
        }
        return this.b;
    }

    private boolean a(a aVar) {
        return this.c.contains(aVar) || aVar.equals(this.f);
    }
}
