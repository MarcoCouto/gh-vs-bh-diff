package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;

class ml {
    private static volatile SparseArray<th> a = new SparseArray<>();

    ml() {
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public synchronized String a(int i) {
        if (a.get(i) == null) {
            SparseArray<th> sparseArray = a;
            StringBuilder sb = new StringBuilder();
            sb.append("EVENT_NUMBER_OF_TYPE_");
            sb.append(i);
            sparseArray.put(i, new th(sb.toString()));
        }
        return ((th) a.get(i)).b();
    }
}
