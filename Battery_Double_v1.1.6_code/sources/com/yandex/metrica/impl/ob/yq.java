package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.metrica.impl.ob.abc.a;
import org.json.JSONArray;
import org.json.JSONObject;

public class yq {
    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject(NativeProtocol.RESULT_ARGS_PERMISSIONS);
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        String optString = optJSONObject2.optString("name");
                        boolean optBoolean = optJSONObject2.optBoolean(String.ENABLED);
                        if (TextUtils.isEmpty(optString)) {
                            yxVar.a("", false);
                        } else {
                            yxVar.a(optString, optBoolean);
                        }
                    }
                }
            }
        }
    }
}
