package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanSettings;
import android.bluetooth.le.ScanSettings.Builder;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.xi.b;
import com.yandex.metrica.impl.ob.xi.b.C0114b;
import com.yandex.metrica.impl.ob.xi.b.a;
import com.yandex.metrica.impl.ob.xi.b.c;
import com.yandex.metrica.impl.ob.xi.b.d;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@TargetApi(21)
public class ea {
    private static final Map<d, Integer> a;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(d.LOW_POWER, Integer.valueOf(0));
        hashMap.put(d.BALANCED, Integer.valueOf(1));
        hashMap.put(d.LOW_LATENCY, Integer.valueOf(2));
        a = Collections.unmodifiableMap(hashMap);
    }

    @NonNull
    public ScanSettings a(@NonNull b bVar) {
        Builder builder = new Builder();
        builder.setScanMode(a(bVar.d));
        builder.setReportDelay(bVar.e);
        if (dl.a(23)) {
            builder.setCallbackType(a(bVar.a));
            builder.setMatchMode(a(bVar.b));
            builder.setNumOfMatches(a(bVar.c));
        }
        return builder.build();
    }

    private int a(@NonNull d dVar) {
        Integer num = (Integer) a.get(dVar);
        if (num == null) {
            return 2;
        }
        return num.intValue();
    }

    @TargetApi(23)
    private int a(@NonNull a aVar) {
        switch (aVar) {
            case MATCH_LOST:
                return 4;
            case FIRST_MATCH:
                return 2;
            default:
                return 1;
        }
    }

    @TargetApi(23)
    private int a(@NonNull C0114b bVar) {
        return C0114b.AGGRESSIVE.equals(bVar) ? 1 : 2;
    }

    @TargetApi(23)
    private int a(@NonNull c cVar) {
        switch (cVar) {
            case ONE_AD:
                return 1;
            case FEW_AD:
                return 2;
            default:
                return 3;
        }
    }
}
