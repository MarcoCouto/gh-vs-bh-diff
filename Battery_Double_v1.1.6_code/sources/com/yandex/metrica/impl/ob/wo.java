package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class wo {
    @NonNull
    private mx<wr> a;
    @NonNull
    private wr b;
    @NonNull
    private abs c;
    @NonNull
    private wt d;
    @NonNull
    private a e;

    interface a {
        void a();
    }

    public wo(@NonNull mx<wr> mxVar, @NonNull a aVar) {
        this(mxVar, aVar, new abs(), new wt(mxVar));
    }

    @VisibleForTesting
    wo(@NonNull mx<wr> mxVar, @NonNull a aVar, @NonNull abs abs, @NonNull wt wtVar) {
        this.a = mxVar;
        this.b = (wr) this.a.a();
        this.c = abs;
        this.d = wtVar;
        this.e = aVar;
    }

    public void a(@NonNull wr wrVar) {
        this.a.a(wrVar);
        this.b = wrVar;
        this.d.a();
        this.e.a();
    }

    public void a() {
        wr wrVar = new wr(this.b.a, this.b.b, this.c.a(), true, true);
        this.a.a(wrVar);
        this.b = wrVar;
        this.e.a();
    }
}
