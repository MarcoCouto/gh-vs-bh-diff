package com.yandex.metrica.impl.ob;

public class pc {
    public final a a;
    public final String b;
    public final Boolean c;

    public enum a {
        GOOGLE,
        HMS
    }

    public pc(a aVar, String str, Boolean bool) {
        this.a = aVar;
        this.b = str;
        this.c = bool;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdTrackingInfo{provider=");
        sb.append(this.a);
        sb.append(", advId='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", limitedAdTracking=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
