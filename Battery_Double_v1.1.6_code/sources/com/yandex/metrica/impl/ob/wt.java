package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class wt {
    @NonNull
    private final mx<wr> a;
    @NonNull
    private final ws b;

    public wt(@NonNull mx<wr> mxVar) {
        this(mxVar, new ws());
    }

    public wt(@NonNull mx<wr> mxVar, @NonNull ws wsVar) {
        this.a = mxVar;
        this.b = wsVar;
    }

    public void a() {
        xa.a().reportEvent("sdk_list", this.b.b(this.b.a(((wr) this.a.a()).a)).toString());
    }
}
