package com.yandex.metrica.impl.ob;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.DigitalClock;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextClock;
import android.widget.TextView;
import java.util.HashSet;
import java.util.Set;

public class zu implements aae {
    private final Set<Class> a = new HashSet();

    public zu() {
        this.a.add(EditText.class);
        this.a.add(Chronometer.class);
        this.a.add(DigitalClock.class);
        if (VERSION.SDK_INT >= 17) {
            this.a.add(TextClock.class);
        }
        this.a.add(RadioButton.class);
        this.a.add(CheckBox.class);
    }

    public boolean a(@NonNull TextView textView) {
        for (Class isInstance : this.a) {
            if (isInstance.isInstance(textView)) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    public c a() {
        return c.IRRELEVANT_CLASS;
    }
}
