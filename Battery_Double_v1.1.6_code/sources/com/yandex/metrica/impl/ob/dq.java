package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.p.Ucc;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public final class dq {
    static vk a = new vk(dr.a());

    @Deprecated
    public static void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        a.a(iIdentifierCallback, list);
    }

    public static void a(@NonNull Context context, @NonNull IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        a.a(context, iIdentifierCallback, list);
    }

    public static boolean a() {
        return a.a();
    }

    @Nullable
    public static Future<String> b() {
        return a.b();
    }

    @NonNull
    public static DeviceInfo a(Context context) {
        return a.a(context);
    }

    @NonNull
    public static String b(Context context) {
        return a.b(context);
    }

    @Nullable
    public static Integer c(Context context) {
        return a.c(context);
    }

    @Nullable
    @Deprecated
    public static String c() {
        return a.d();
    }

    @Nullable
    public static String d(@NonNull Context context) {
        return a.d(context);
    }

    @Nullable
    public static String e(@NonNull Context context) {
        return a.e(context);
    }

    @NonNull
    public static String f(@NonNull Context context) {
        return a.f(context);
    }

    public static void a(int i, String str, String str2, Map<String, String> map) {
        a.a(i, str, str2, map);
    }

    @Nullable
    public static Future<Boolean> d() {
        return a.c();
    }

    public static void e() {
        a.e();
    }

    @NonNull
    public static String a(@Nullable String str) {
        return a.a(str);
    }

    @NonNull
    public static String a(int i) {
        return a.a(i);
    }

    @NonNull
    public static YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return a.a(yandexMetricaConfig, str);
    }

    @NonNull
    public static YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return a.a(yandexMetricaConfig, list);
    }

    public static void a(@NonNull Context context, @NonNull Object obj) {
        a.a(context, obj);
    }

    public static void b(@NonNull Context context, @NonNull Object obj) {
        a.b(context, obj);
    }

    @Nullable
    public static Location g(Context context) {
        return a.g(context);
    }

    public static void a(Context context, boolean z) {
        a.a(context, z);
    }

    public static void a(@NonNull Ucc ucc, boolean z) {
        a.a(ucc, z);
    }
}
