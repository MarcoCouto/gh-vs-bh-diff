package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;

public abstract class xd {
    @NonNull
    xg a;
    @NonNull
    Uri b;
    @NonNull
    private Socket c;

    /* access modifiers changed from: 0000 */
    public abstract void a();

    xd(@NonNull Socket socket, @NonNull Uri uri, @NonNull xg xgVar) {
        this.c = socket;
        this.b = uri;
        this.a = xgVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, @NonNull Map<String, String> map, @NonNull byte[] bArr) {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(this.c.getOutputStream());
            try {
                bufferedOutputStream2.write(str.getBytes());
                a(bufferedOutputStream2);
                for (Entry entry : map.entrySet()) {
                    a((OutputStream) bufferedOutputStream2, (String) entry.getKey(), (String) entry.getValue());
                }
                a(bufferedOutputStream2);
                bufferedOutputStream2.write(bArr);
                bufferedOutputStream2.flush();
                this.a.a(this.c.getLocalPort());
                dl.a((Closeable) bufferedOutputStream2);
            } catch (IOException e) {
                e = e;
                bufferedOutputStream = bufferedOutputStream2;
                try {
                    this.a.a("io_exception_during_sync", (Throwable) e);
                    dl.a((Closeable) bufferedOutputStream);
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream2 = bufferedOutputStream;
                    dl.a((Closeable) bufferedOutputStream2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                dl.a((Closeable) bufferedOutputStream2);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            this.a.a("io_exception_during_sync", (Throwable) e);
            dl.a((Closeable) bufferedOutputStream);
        }
    }

    private void a(@NonNull OutputStream outputStream, @NonNull String str, @NonNull String str2) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(": ");
        sb.append(str2);
        outputStream.write(sb.toString().getBytes());
        a(outputStream);
    }

    private void a(@NonNull OutputStream outputStream) throws IOException {
        outputStream.write("\r\n".getBytes());
    }
}
