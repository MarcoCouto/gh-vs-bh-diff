package com.yandex.metrica.impl.ob;

import android.support.annotation.VisibleForTesting;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class eh {
    private final acx a = acy.a("YMM-BD", new Runnable() {
        public void run() {
            while (eh.this.b) {
                try {
                    ((a) eh.this.c.take()).a();
                } catch (InterruptedException unused) {
                }
            }
        }
    });
    /* access modifiers changed from: private */
    public volatile boolean b = true;
    /* access modifiers changed from: private */
    public final BlockingQueue<a> c = new LinkedBlockingQueue();
    private ConcurrentHashMap<Class, CopyOnWriteArrayList<el<? extends ej>>> d = new ConcurrentHashMap<>();
    private WeakHashMap<Object, CopyOnWriteArrayList<c>> e = new WeakHashMap<>();
    private ConcurrentHashMap<Class, ej> f = new ConcurrentHashMap<>();

    private static class a {
        private final ej a;
        private final el<? extends ej> b;

        private a(ej ejVar, el<? extends ej> elVar) {
            this.a = ejVar;
            this.b = elVar;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            try {
                if (!this.b.b(this.a)) {
                    this.b.a(this.a);
                }
            } catch (Throwable unused) {
            }
        }
    }

    private static final class b {
        /* access modifiers changed from: private */
        public static final eh a = new eh();
    }

    private static class c {
        final CopyOnWriteArrayList<el<? extends ej>> a;
        final el<? extends ej> b;

        private c(CopyOnWriteArrayList<el<? extends ej>> copyOnWriteArrayList, el<? extends ej> elVar) {
            this.a = copyOnWriteArrayList;
            this.b = elVar;
        }

        /* access modifiers changed from: protected */
        public void a() {
            this.a.remove(this.b);
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            super.finalize();
            a();
        }
    }

    public static final eh a() {
        return b.a;
    }

    @VisibleForTesting
    eh() {
        this.a.start();
    }

    public synchronized void a(ej ejVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.d.get(ejVar.getClass());
        if (copyOnWriteArrayList != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                a(ejVar, (el) it.next());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(ej ejVar, el<? extends ej> elVar) {
        this.c.add(new a(ejVar, elVar));
    }

    public synchronized void b(ej ejVar) {
        a(ejVar);
        this.f.put(ejVar.getClass(), ejVar);
    }

    public synchronized void a(Class<? extends ej> cls) {
        this.f.remove(cls);
    }

    public synchronized void a(Object obj, Class cls, el<? extends ej> elVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.d.get(cls);
        if (copyOnWriteArrayList == null) {
            copyOnWriteArrayList = new CopyOnWriteArrayList();
            this.d.put(cls, copyOnWriteArrayList);
        }
        copyOnWriteArrayList.add(elVar);
        CopyOnWriteArrayList copyOnWriteArrayList2 = (CopyOnWriteArrayList) this.e.get(obj);
        if (copyOnWriteArrayList2 == null) {
            copyOnWriteArrayList2 = new CopyOnWriteArrayList();
            this.e.put(obj, copyOnWriteArrayList2);
        }
        copyOnWriteArrayList2.add(new c(copyOnWriteArrayList, elVar));
        ej ejVar = (ej) this.f.get(cls);
        if (ejVar != null) {
            a(ejVar, elVar);
        }
    }

    public synchronized void a(Object obj) {
        List<c> list = (List) this.e.remove(obj);
        if (list != null) {
            for (c a2 : list) {
                a2.a();
            }
        }
    }
}
