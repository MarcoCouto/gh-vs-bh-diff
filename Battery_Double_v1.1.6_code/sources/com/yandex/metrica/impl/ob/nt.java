package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.k.a;
import com.yandex.metrica.impl.ob.p.a.C0090a;
import com.yandex.metrica.impl.ob.ve.a.d.b;
import java.util.ArrayList;
import java.util.Arrays;

public class nt implements np<qu, b> {
    @NonNull
    /* renamed from: a */
    public b b(@NonNull qu quVar) {
        b bVar = new b();
        bVar.b = new int[quVar.a.size()];
        int i = 0;
        int i2 = 0;
        for (C0090a a : quVar.a) {
            bVar.b[i2] = a(a);
            i2++;
        }
        bVar.c = new int[quVar.b.size()];
        for (a a2 : quVar.b) {
            bVar.c[i] = a(a2);
            i++;
        }
        return bVar;
    }

    @NonNull
    public qu a(@NonNull b bVar) {
        ArrayList arrayList = new ArrayList();
        if (bVar.b.length == 0) {
            arrayList.addAll(Arrays.asList(C0090a.values()));
        } else {
            for (int a : bVar.b) {
                arrayList.add(a(a));
            }
        }
        ArrayList arrayList2 = new ArrayList();
        if (bVar.c.length == 0) {
            arrayList2.addAll(Arrays.asList(a.values()));
        } else {
            for (int b : bVar.c) {
                arrayList2.add(b(b));
            }
        }
        return new qu(arrayList, arrayList2);
    }

    private int a(@NonNull C0090a aVar) {
        switch (aVar) {
            case AC:
                return 3;
            case USB:
                return 1;
            case WIRELESS:
                return 2;
            case NONE:
                return 0;
            default:
                return 4;
        }
    }

    @NonNull
    private C0090a a(int i) {
        switch (i) {
            case 0:
                return C0090a.NONE;
            case 1:
                return C0090a.USB;
            case 2:
                return C0090a.WIRELESS;
            case 3:
                return C0090a.AC;
            default:
                return C0090a.UNKNOWN;
        }
    }

    private int a(@NonNull a aVar) {
        switch (aVar) {
            case BACKGROUND:
                return 0;
            case FOREGROUND:
                return 1;
            case VISIBLE:
                return 2;
            default:
                return 3;
        }
    }

    @NonNull
    private a b(int i) {
        switch (i) {
            case 0:
                return a.BACKGROUND;
            case 1:
                return a.FOREGROUND;
            case 2:
                return a.VISIBLE;
            default:
                return a.UNKNOWN;
        }
    }
}
