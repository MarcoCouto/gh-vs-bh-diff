package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a.C0104a;
import com.yandex.metrica.impl.ob.xi.a.C0113a;

public class og implements np<C0113a, C0104a> {
    @NonNull
    /* renamed from: a */
    public C0104a b(@NonNull C0113a aVar) {
        C0104a aVar2 = new C0104a();
        aVar2.b = aVar.a;
        if (aVar.b != null) {
            aVar2.c = aVar.b;
        }
        if (aVar.c != null) {
            aVar2.d = aVar.c;
        }
        return aVar2;
    }

    @NonNull
    public C0113a a(@NonNull C0104a aVar) {
        int i = aVar.b;
        byte[] bArr = null;
        byte[] bArr2 = dl.a(aVar.c) ? null : aVar.c;
        if (!dl.a(aVar.d)) {
            bArr = aVar.d;
        }
        return new C0113a(i, bArr2, bArr);
    }
}
