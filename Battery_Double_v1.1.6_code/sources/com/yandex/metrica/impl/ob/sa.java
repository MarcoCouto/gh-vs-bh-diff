package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.uu.b;
import com.yandex.metrica.impl.ob.uu.b.C0092b;
import com.yandex.metrica.impl.ob.uu.b.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

public class sa {
    @NonNull
    private final lz a;
    @NonNull
    private final ly b;
    @NonNull
    private final rx c;
    @NonNull
    private final rv d;

    public sa(@NonNull Context context) {
        this(lv.a(context).g(), lv.a(context).h(), new pr(context), new rw(), new ru());
    }

    public rz a(int i) {
        Map b2 = this.a.b(i);
        Map b3 = this.b.b(i);
        b bVar = new b();
        bVar.b = a(b2);
        bVar.c = b(b3);
        rz rzVar = new rz(b2.isEmpty() ? -1 : ((Long) Collections.max(b2.keySet())).longValue(), b3.isEmpty() ? -1 : ((Long) Collections.max(b3.keySet())).longValue(), bVar);
        return rzVar;
    }

    public void a(rz rzVar) {
        if (rzVar.a >= 0) {
            this.a.b(rzVar.a);
        }
        if (rzVar.b >= 0) {
            this.b.b(rzVar.b);
        }
    }

    private C0092b[] a(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            C0092b a2 = this.c.a(((Long) entry.getKey()).longValue(), (String) entry.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return (C0092b[]) arrayList.toArray(new C0092b[arrayList.size()]);
    }

    private a[] b(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            a a2 = this.d.a(((Long) entry.getKey()).longValue(), (String) entry.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return (a[]) arrayList.toArray(new a[arrayList.size()]);
    }

    @VisibleForTesting
    sa(@NonNull lz lzVar, @NonNull ly lyVar, @NonNull pr prVar, @NonNull rw rwVar, @NonNull ru ruVar) {
        this(lzVar, lyVar, new rx(prVar, rwVar), new rv(prVar, ruVar));
    }

    @VisibleForTesting
    sa(@NonNull lz lzVar, @NonNull ly lyVar, @NonNull rx rxVar, @NonNull rv rvVar) {
        this.a = lzVar;
        this.b = lyVar;
        this.c = rxVar;
        this.d = rvVar;
    }
}
