package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.ca.a;
import java.util.Collection;

public class ry extends cb<wk> {
    @NonNull
    private Context j;
    @NonNull
    private sc k;
    @NonNull
    private final df l;
    @NonNull
    private ql m;
    @NonNull
    private mq n;
    @NonNull
    private final sa o;
    @NonNull
    private final qz p;
    private long q;
    private rz r;

    public ry(@NonNull Context context, @NonNull sc scVar, @NonNull df dfVar, @NonNull qz qzVar) {
        this(context, scVar, dfVar, qzVar, new mq(lv.a(context).c()), new wk(), new sa(context));
    }

    public boolean a() {
        if (!this.l.d() && !TextUtils.isEmpty(this.k.s()) && !TextUtils.isEmpty(this.k.u()) && !dl.a((Collection) s())) {
            return J();
        }
        return false;
    }

    public boolean b() {
        boolean b = super.b();
        L();
        return b;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((wk) this.i).a(builder, this.k);
    }

    /* access modifiers changed from: protected */
    public void G() {
        I();
    }

    /* access modifiers changed from: protected */
    public void H() {
        I();
    }

    private void I() {
        this.o.a(this.r);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    private boolean J() {
        this.r = this.o.a(this.m.d);
        if (!this.r.a()) {
            return c(e.a((e) this.r.c));
        }
        return false;
    }

    private void K() {
        this.q = this.n.b(-1) + 1;
        ((wk) this.i).a(this.q);
    }

    private void L() {
        this.n.c(this.q).q();
    }

    @VisibleForTesting
    ry(@NonNull Context context, @NonNull sc scVar, @NonNull df dfVar, @NonNull qz qzVar, @NonNull mq mqVar, @NonNull wk wkVar, @NonNull sa saVar) {
        super(wkVar);
        this.j = context;
        this.k = scVar;
        this.l = dfVar;
        this.p = qzVar;
        this.m = this.k.a();
        this.n = mqVar;
        this.o = saVar;
        K();
        a(this.k.b());
    }

    public void D() {
        if (x()) {
            this.p.a();
        }
    }

    @Nullable
    public a E() {
        return a.LOCATION;
    }

    @Nullable
    public xq F() {
        return this.k.f();
    }
}
