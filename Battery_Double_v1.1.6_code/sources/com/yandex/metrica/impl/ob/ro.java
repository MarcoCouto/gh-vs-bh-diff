package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public abstract class ro implements ph {
    @NonNull
    private final dj a;
    @NonNull
    private final qe b;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String c();

    public ro(@NonNull qe qeVar, @NonNull dj djVar) {
        this.b = qeVar;
        this.a = djVar;
    }

    /* access modifiers changed from: protected */
    public boolean a(long j) {
        dj djVar = this.a;
        long a2 = this.b.a();
        StringBuilder sb = new StringBuilder();
        sb.append("last ");
        sb.append(c());
        sb.append(" scan attempt");
        return djVar.a(a2, j, sb.toString());
    }
}
