package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import com.tapjoy.TapjoyConstants;

public class abb extends abe {
    private static final abb a = new abb();

    public String f() {
        return "AppMetricaInternal";
    }

    public abb(@Nullable String str) {
        super(str);
    }

    private abb() {
        this("");
    }

    public static abb h() {
        return a;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return super.d() && "publicBinaryProd".startsWith(TapjoyConstants.LOG_LEVEL_INTERNAL);
    }
}
