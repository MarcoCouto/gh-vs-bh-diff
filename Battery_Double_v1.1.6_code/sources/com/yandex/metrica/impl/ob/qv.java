package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.Arrays;
import java.util.List;

public class qv implements aw {
    @NonNull
    private rj a;
    @NonNull
    private qw b;
    @NonNull
    private final List<qy> c;
    @NonNull
    private final List<qn> d;
    @NonNull
    private final List<qn> e;
    @NonNull
    private final List<qn> f;
    @NonNull
    private final List<pz> g;
    @NonNull
    private final au h;
    private boolean i;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public au a(@NonNull act act, @NonNull aw awVar) {
            return new au(act, awVar);
        }
    }

    public qv(@NonNull qw qwVar, @NonNull rj rjVar) {
        this(qwVar, rjVar, new mq(lv.a(qwVar.a.a).c()));
    }

    @VisibleForTesting
    qv(@NonNull qw qwVar, @NonNull rj rjVar, @NonNull mq mqVar) {
        py pyVar = null;
        pt ptVar = qwVar.d == null ? null : qwVar.d.l;
        pt ptVar2 = qwVar.d == null ? null : qwVar.d.m;
        pt ptVar3 = qwVar.d == null ? null : qwVar.d.n;
        if (qwVar.d != null) {
            pyVar = qwVar.d.o;
        }
        this(qwVar, rjVar, mqVar, ptVar, ptVar2, ptVar3, pyVar);
    }

    private qv(@NonNull qw qwVar, @NonNull rj rjVar, @NonNull mq mqVar, @Nullable pt ptVar, @Nullable pt ptVar2, @Nullable pt ptVar3, @Nullable py pyVar) {
        this(qwVar, rjVar, (pl) new qc(qwVar, mqVar, ptVar2), (pl) new qj(qwVar, mqVar, ptVar), new rf(qwVar, ptVar3), new qb(qwVar, mqVar, rjVar, pyVar), new a());
    }

    @VisibleForTesting
    qv(@NonNull qw qwVar, @NonNull rj rjVar, @NonNull pl plVar, @NonNull pl plVar2, @NonNull rf rfVar, @NonNull qb qbVar, @NonNull a aVar) {
        this.b = qwVar;
        if (this.b.d != null) {
            this.i = this.b.d.g;
        }
        this.a = rjVar;
        this.c = Arrays.asList(new qy[]{plVar.a(rjVar), plVar2.a(rjVar), rfVar.a(rjVar), qbVar.a()});
        this.d = plVar2.d();
        this.e = plVar.d();
        this.f = rfVar.a();
        this.g = qbVar.b();
        this.h = aVar.a(this.b.a.b, this);
        this.a.b().a(this.h);
    }

    public void a() {
        if (this.i) {
            for (qy d2 : this.c) {
                d2.d();
            }
        }
    }

    @Nullable
    public Location b() {
        if (this.i) {
            return this.a.a();
        }
        return null;
    }

    public void c() {
        if (this.i) {
            this.h.a();
            for (qy b2 : this.c) {
                b2.b();
            }
        }
    }

    public void d() {
        this.h.b();
        for (qy c2 : this.c) {
            c2.c();
        }
    }

    public void a(@Nullable ql qlVar) {
        pt ptVar;
        pt ptVar2;
        pt ptVar3;
        this.i = qlVar != null && qlVar.g;
        this.a.a(qlVar);
        py pyVar = null;
        if (qlVar == null) {
            ptVar = null;
        } else {
            ptVar = qlVar.l;
        }
        if (qlVar == null) {
            ptVar2 = null;
        } else {
            ptVar2 = qlVar.m;
        }
        if (qlVar == null) {
            ptVar3 = null;
        } else {
            ptVar3 = qlVar.n;
        }
        if (qlVar != null) {
            pyVar = qlVar.o;
        }
        for (qn a2 : this.d) {
            a2.a(ptVar);
        }
        for (qn a3 : this.e) {
            a3.a(ptVar2);
        }
        for (qn a4 : this.f) {
            a4.a(ptVar3);
        }
        for (pz a5 : this.g) {
            a5.a(pyVar);
        }
        e();
    }

    private void e() {
        for (qy a2 : this.c) {
            a2.a();
        }
        a();
    }
}
