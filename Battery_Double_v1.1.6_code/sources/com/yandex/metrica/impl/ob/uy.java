package com.yandex.metrica.impl.ob;

import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.Arrays;

public interface uy {

    public static final class a extends e {
        public C0096a[] b;

        /* renamed from: com.yandex.metrica.impl.ob.uy$a$a reason: collision with other inner class name */
        public static final class C0096a extends e {
            private static volatile C0096a[] f;
            public byte[] b;
            public int c;
            public b d;
            public c e;

            public static C0096a[] d() {
                if (f == null) {
                    synchronized (c.a) {
                        if (f == null) {
                            f = new C0096a[0];
                        }
                    }
                }
                return f;
            }

            public C0096a() {
                e();
            }

            public C0096a e() {
                this.b = g.h;
                this.c = 0;
                this.d = null;
                this.e = null;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                if (this.d != null) {
                    bVar.a(3, (e) this.d);
                }
                if (this.e != null) {
                    bVar.a(4, (e) this.e);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b) + b.d(2, this.c);
                if (this.d != null) {
                    c2 += b.b(3, (e) this.d);
                }
                return this.e != null ? c2 + b.b(4, (e) this.e) : c2;
            }

            /* renamed from: b */
            public C0096a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a != 10) {
                        if (a == 16) {
                            int g = aVar.g();
                            switch (g) {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                    this.c = g;
                                    break;
                            }
                        } else if (a == 26) {
                            if (this.d == null) {
                                this.d = new b();
                            }
                            aVar.a((e) this.d);
                        } else if (a == 34) {
                            if (this.e == null) {
                                this.e = new c();
                            }
                            aVar.a((e) this.e);
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    } else {
                        this.b = aVar.j();
                    }
                }
            }
        }

        public static final class b extends e {
            public boolean b;
            public boolean c;

            public b() {
                d();
            }

            public b d() {
                this.b = false;
                this.c = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b) {
                    bVar.a(1, this.b);
                }
                if (this.c) {
                    bVar.a(2, this.c);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b) {
                    c2 += b.b(1, this.b);
                }
                return this.c ? c2 + b.b(2, this.c) : c2;
            }

            /* renamed from: b */
            public b a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.h();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class c extends e {
            public byte[] b;
            public double c;
            public double d;
            public boolean e;

            public c() {
                d();
            }

            public c d() {
                this.b = g.h;
                this.c = Utils.DOUBLE_EPSILON;
                this.d = Utils.DOUBLE_EPSILON;
                this.e = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (!Arrays.equals(this.b, g.h)) {
                    bVar.a(1, this.b);
                }
                if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(Utils.DOUBLE_EPSILON)) {
                    bVar.a(2, this.c);
                }
                if (Double.doubleToLongBits(this.d) != Double.doubleToLongBits(Utils.DOUBLE_EPSILON)) {
                    bVar.a(3, this.d);
                }
                if (this.e) {
                    bVar.a(4, this.e);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (!Arrays.equals(this.b, g.h)) {
                    c2 += b.b(1, this.b);
                }
                if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(Utils.DOUBLE_EPSILON)) {
                    c2 += b.b(2, this.c);
                }
                if (Double.doubleToLongBits(this.d) != Double.doubleToLongBits(Utils.DOUBLE_EPSILON)) {
                    c2 += b.b(3, this.d);
                }
                return this.e ? c2 + b.b(4, this.e) : c2;
            }

            /* renamed from: b */
            public c a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.j();
                    } else if (a == 17) {
                        this.c = aVar.c();
                    } else if (a == 25) {
                        this.d = aVar.c();
                    } else if (a == 32) {
                        this.e = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = C0096a.d();
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (C0096a aVar : this.b) {
                    if (aVar != null) {
                        bVar.a(1, (e) aVar);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null && this.b.length > 0) {
                for (C0096a aVar : this.b) {
                    if (aVar != null) {
                        c2 += b.b(1, (e) aVar);
                    }
                }
            }
            return c2;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    int b2 = g.b(aVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    C0096a[] aVarArr = new C0096a[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, aVarArr, 0, length);
                    }
                    while (length < aVarArr.length - 1) {
                        aVarArr[length] = new C0096a();
                        aVar.a((e) aVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    aVarArr[length] = new C0096a();
                    aVar.a((e) aVarArr[length]);
                    this.b = aVarArr;
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }
}
