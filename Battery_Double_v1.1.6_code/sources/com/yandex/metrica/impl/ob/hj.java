package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.fe;
import com.yandex.metrica.impl.ob.hq;

public class hj<T extends hq, C extends fe> extends hc<T, C> {
    public hj(hi<T> hiVar, C c) {
        super(hiVar, c);
    }

    public boolean b(aa aaVar) {
        return a(aaVar, new a<T>() {
            public boolean a(T t, aa aaVar) {
                return t.a(aaVar);
            }
        });
    }
}
