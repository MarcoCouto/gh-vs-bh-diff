package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class or {
    @NonNull
    private final oq a;
    private final ot b;
    private final long c;
    private final boolean d;
    private final long e;

    public or(@NonNull JSONObject jSONObject, long j) throws JSONException {
        this.a = new oq(jSONObject.optString("device_id", null), jSONObject.optString("device_id_hash", null));
        if (jSONObject.has("device_snapshot_key")) {
            this.b = new ot(jSONObject.optString("device_snapshot_key", null));
        } else {
            this.b = null;
        }
        this.c = jSONObject.optLong("last_elections_time", -1);
        this.d = d();
        this.e = j;
    }

    public or(@NonNull oq oqVar, @NonNull ot otVar, long j) {
        this.a = oqVar;
        this.b = otVar;
        this.c = j;
        this.d = d();
        this.e = -1;
    }

    public String a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("device_id", this.a.a);
        jSONObject.put("device_id_hash", this.a.b);
        if (this.b != null) {
            jSONObject.put("device_snapshot_key", this.b.b());
        }
        jSONObject.put("last_elections_time", this.c);
        return jSONObject.toString();
    }

    @NonNull
    public oq b() {
        return this.a;
    }

    @Nullable
    public ot c() {
        return this.b;
    }

    private boolean d() {
        boolean z = false;
        if (this.c <= -1) {
            return false;
        }
        if (System.currentTimeMillis() - this.c < 604800000) {
            z = true;
        }
        return z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Credentials{mIdentifiers=");
        sb.append(this.a);
        sb.append(", mDeviceSnapshot=");
        sb.append(this.b);
        sb.append(", mLastElectionsTime=");
        sb.append(this.c);
        sb.append(", mFresh=");
        sb.append(this.d);
        sb.append(", mLastModified=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
