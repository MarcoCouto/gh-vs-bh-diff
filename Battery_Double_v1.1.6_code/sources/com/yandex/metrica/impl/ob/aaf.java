package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import java.util.List;
import org.json.JSONObject;

class aaf {
    @Nullable
    private zv a;

    aaf() {
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull zx zxVar, @NonNull View view, @NonNull zt ztVar) {
        this.a = zxVar.a(null, view, 0);
        a(zxVar, this.a, view, 1, ztVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public JSONObject a(@NonNull aah aah, int i) {
        JSONObject jSONObject = this.a != null ? this.a.a(aah, i, 0).c : null;
        return jSONObject == null ? new JSONObject() : jSONObject;
    }

    private void a(@NonNull zx zxVar, @NonNull zv zvVar, @NonNull View view, int i, @NonNull zt ztVar) {
        List<View> a2 = zxVar.a(view, i);
        if (a2.size() == 0) {
            ztVar.a(zvVar.a());
            return;
        }
        for (View view2 : a2) {
            zv a3 = zxVar.a(zvVar.a(), view2, i);
            zvVar.a(a3);
            a(zxVar, a3, view2, i + 1, ztVar);
        }
    }
}
