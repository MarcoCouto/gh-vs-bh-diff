package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class fw {
    @NonNull
    private final mo a;
    @NonNull
    private ju b;
    @NonNull
    private lr c;
    @NonNull
    private final ack d;
    @NonNull
    private final i e;
    @NonNull
    private final fi f;
    @NonNull
    private a g;
    @NonNull
    private final abt h;
    private final int i;
    private long j;
    private long k;
    private int l;

    public interface a {
        void a();
    }

    public fw(@NonNull mo moVar, @NonNull ju juVar, @NonNull lr lrVar, @NonNull i iVar, @NonNull ack ack, int i2, @NonNull a aVar) {
        this(moVar, juVar, lrVar, iVar, ack, i2, aVar, new fi(moVar), new abs());
    }

    @VisibleForTesting
    public fw(@NonNull mo moVar, @NonNull ju juVar, @NonNull lr lrVar, @NonNull i iVar, @NonNull ack ack, int i2, @NonNull a aVar, @NonNull fi fiVar, @NonNull abt abt) {
        this.a = moVar;
        this.b = juVar;
        this.c = lrVar;
        this.e = iVar;
        this.d = ack;
        this.i = i2;
        this.f = fiVar;
        this.h = abt;
        this.g = aVar;
        this.j = this.a.a(0);
        this.k = this.a.b();
        this.l = this.a.c();
    }

    public void a(aa aaVar) {
        this.b.c(aaVar);
    }

    public void b(aa aaVar) {
        e(aaVar);
        f();
    }

    public void c(aa aaVar) {
        e(aaVar);
        a();
    }

    public void d(aa aaVar) {
        e(aaVar);
        b();
    }

    public void e(aa aaVar) {
        a(aaVar, this.b.d(aaVar));
    }

    public void f(@NonNull aa aaVar) {
        a(aaVar, this.b.e(aaVar));
    }

    @VisibleForTesting
    public void a(@NonNull aa aaVar, @NonNull jv jvVar) {
        if (TextUtils.isEmpty(aaVar.l())) {
            aaVar.a(this.a.f());
        }
        aaVar.d(this.a.h());
        jv jvVar2 = jvVar;
        this.c.a(this.d.a(aaVar).a(aaVar), aaVar.g(), jvVar2, this.e.b(), this.f);
        this.g.a();
    }

    private void f() {
        this.j = this.h.b();
        this.a.b(this.j).q();
    }

    public void a() {
        this.k = this.h.b();
        this.a.c(this.k).q();
    }

    public void b() {
        this.l = this.i;
        this.a.c(this.l).q();
    }

    public boolean c() {
        return this.h.b() - this.j > jr.a;
    }

    public long d() {
        return this.k;
    }

    public boolean e() {
        return this.l < this.i;
    }
}
