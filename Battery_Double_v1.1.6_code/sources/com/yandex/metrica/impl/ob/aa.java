package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Pair;
import com.facebook.internal.NativeProtocol;
import com.yandex.metrica.impl.ob.al.a;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class aa implements Parcelable {
    public static final Creator<aa> CREATOR = new Creator<aa>() {
        /* renamed from: a */
        public aa createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(ab.class.getClassLoader());
            aa a = new aa().a(readBundle.getInt("CounterReport.Type", a.EVENT_TYPE_UNDEFINED.a())).b(readBundle.getInt("CounterReport.CustomType")).c(dh.b(readBundle.getString("CounterReport.Value"), "")).a(readBundle.getString("CounterReport.UserInfo")).e(readBundle.getString("CounterReport.Environment")).b(readBundle.getString("CounterReport.Event")).a(aa.d(readBundle)).c(readBundle.getInt("CounterReport.TRUNCATED")).d(readBundle.getString("CounterReport.ProfileID")).a(readBundle.getLong("CounterReport.CreationElapsedRealtime")).b(readBundle.getLong("CounterReport.CreationTimestamp")).a(ap.a(Integer.valueOf(readBundle.getInt("CounterReport.UniquenessStatus"))));
            ba baVar = (ba) readBundle.getParcelable("CounterReport.IdentifiersData");
            if (baVar != null) {
                a.a(baVar);
            }
            return a;
        }

        /* renamed from: a */
        public aa[] newArray(int i) {
            return new aa[i];
        }
    };
    String a;
    String b;
    int c;
    int d;
    int e;
    private String f;
    private String g;
    @Nullable
    private Pair<String, String> h;
    private String i;
    private long j;
    private long k;
    @NonNull
    private ap l;
    @Nullable
    private ba m;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        Bundle bundle = new Bundle();
        bundle.putString("CounterReport.Event", this.a);
        bundle.putString("CounterReport.Value", this.b);
        bundle.putInt("CounterReport.Type", this.c);
        bundle.putInt("CounterReport.CustomType", this.d);
        bundle.putInt("CounterReport.TRUNCATED", this.e);
        bundle.putString("CounterReport.ProfileID", this.i);
        bundle.putInt("CounterReport.UniquenessStatus", this.l.d);
        if (this.m != null) {
            bundle.putParcelable("CounterReport.IdentifiersData", this.m);
        }
        if (this.g != null) {
            bundle.putString("CounterReport.Environment", this.g);
        }
        if (this.f != null) {
            bundle.putString("CounterReport.UserInfo", this.f);
        }
        if (this.h != null) {
            a(bundle, this.h);
        }
        bundle.putLong("CounterReport.CreationElapsedRealtime", this.j);
        bundle.putLong("CounterReport.CreationTimestamp", this.k);
        parcel.writeBundle(bundle);
    }

    public aa() {
        this("", 0);
    }

    public aa(@Nullable aa aaVar) {
        this.l = ap.UNKNOWN;
        if (aaVar != null) {
            this.a = aaVar.d();
            this.b = aaVar.e();
            this.c = aaVar.g();
            this.d = aaVar.h();
            this.f = aaVar.l();
            this.g = aaVar.j();
            this.h = aaVar.k();
            this.e = aaVar.o();
            this.i = aaVar.i;
            this.j = aaVar.r();
            this.k = aaVar.s();
            this.l = aaVar.l;
            this.m = aaVar.m;
        }
    }

    public aa(String str, int i2) {
        this("", str, i2);
    }

    public aa(String str, String str2, int i2) {
        this(str, str2, i2, new abs());
    }

    @VisibleForTesting
    public aa(String str, String str2, int i2, abs abs) {
        this.l = ap.UNKNOWN;
        this.a = str2;
        this.c = i2;
        this.b = str;
        this.j = abs.c();
        this.k = abs.a();
    }

    public String d() {
        return this.a;
    }

    public aa b(String str) {
        this.a = str;
        return this;
    }

    public String e() {
        return this.b;
    }

    public byte[] f() {
        return Base64.decode(this.b, 0);
    }

    public aa c(String str) {
        this.b = str;
        return this;
    }

    public aa a(@Nullable byte[] bArr) {
        this.b = new String(Base64.encode(bArr, 0));
        return this;
    }

    public int g() {
        return this.c;
    }

    public aa a(int i2) {
        this.c = i2;
        return this;
    }

    public int h() {
        return this.d;
    }

    public aa b(int i2) {
        this.d = i2;
        return this;
    }

    @Nullable
    public ba i() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public String j() {
        return this.g;
    }

    public Pair<String, String> k() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public aa e(String str) {
        this.g = str;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public aa b(String str, String str2) {
        if (this.h == null) {
            this.h = new Pair<>(str, str2);
        }
        return this;
    }

    /* access modifiers changed from: private */
    public aa a(@Nullable Pair<String, String> pair) {
        this.h = pair;
        return this;
    }

    public String l() {
        return this.f;
    }

    public aa a(String str) {
        this.f = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public aa c(int i2) {
        this.e = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public aa a(long j2) {
        this.j = j2;
        return this;
    }

    /* access modifiers changed from: protected */
    public aa b(long j2) {
        this.k = j2;
        return this;
    }

    /* access modifiers changed from: protected */
    public aa a(@NonNull ba baVar) {
        this.m = baVar;
        return this;
    }

    public boolean m() {
        return this.a == null;
    }

    public boolean n() {
        return a.EVENT_TYPE_UNDEFINED.a() == this.c;
    }

    public int o() {
        return this.e;
    }

    @Nullable
    public String p() {
        return this.i;
    }

    public aa d(@Nullable String str) {
        this.i = str;
        return this;
    }

    @NonNull
    public ap q() {
        return this.l;
    }

    @NonNull
    public aa a(@NonNull ap apVar) {
        this.l = apVar;
        return this;
    }

    public long r() {
        return this.j;
    }

    public long s() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Bundle a(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putParcelable("CounterReport.Object", this);
        return bundle;
    }

    public String toString() {
        return String.format(Locale.US, "[event: %s, type: %s, value: %s]", new Object[]{this.a, a.a(this.c).b(), this.b});
    }

    private static void a(@NonNull Bundle bundle, @NonNull Pair<String, String> pair) {
        bundle.putString("CounterReport.AppEnvironmentDiffKey", (String) pair.first);
        bundle.putString("CounterReport.AppEnvironmentDiffValue", (String) pair.second);
    }

    /* access modifiers changed from: private */
    @Nullable
    public static Pair<String, String> d(Bundle bundle) {
        if (!bundle.containsKey("CounterReport.AppEnvironmentDiffKey") || !bundle.containsKey("CounterReport.AppEnvironmentDiffValue")) {
            return null;
        }
        return new Pair<>(bundle.getString("CounterReport.AppEnvironmentDiffKey"), bundle.getString("CounterReport.AppEnvironmentDiffValue"));
    }

    @NonNull
    public static aa b(Bundle bundle) {
        if (bundle != null) {
            try {
                aa aaVar = (aa) bundle.getParcelable("CounterReport.Object");
                if (aaVar != null) {
                    return aaVar;
                }
            } catch (Throwable unused) {
                return new aa();
            }
        }
        return new aa();
    }

    public static aa a(aa aaVar, a aVar) {
        aa a2 = a(aaVar);
        a2.a(aVar.a());
        return a2;
    }

    public static aa a(@NonNull aa aaVar) {
        aa aaVar2 = new aa(aaVar);
        aaVar2.b("");
        aaVar2.c("");
        return aaVar2;
    }

    public static aa b(aa aaVar) {
        return a(aaVar, a.EVENT_TYPE_ALIVE);
    }

    public static aa a(aa aaVar, @NonNull am amVar) {
        aa a2 = a(aaVar, a.EVENT_TYPE_START);
        a2.a(e.a((e) new ak().b(new aj(amVar.a()))));
        return a2;
    }

    public static aa c(aa aaVar) {
        return a(aaVar, a.EVENT_TYPE_INIT);
    }

    public static aa a(@NonNull Context context) {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            Integer c2 = as.a().n().c();
            if (c2 != null) {
                jSONObject2.put("battery", c2);
            }
            jSONObject2.put("boot_time_seconds", abu.d());
            jSONObject.put("dfid", jSONObject2);
        } catch (Throwable unused) {
        }
        aa b2 = new aa().b("");
        b2.a(a.EVENT_TYPE_IDENTITY_LIGHT.a()).c(jSONObject.toString());
        return b2;
    }

    public static aa a(aa aaVar, fe feVar) {
        bc a2 = new bc(feVar.k()).a();
        try {
            if (feVar.w()) {
                a2.e();
            }
            we i2 = feVar.i();
            if (i2.I()) {
                a2.a(i2.J());
            }
            a2.c();
        } catch (Throwable unused) {
        }
        aa a3 = a(aaVar);
        a3.a(a.EVENT_TYPE_IDENTITY.a()).c(a2.g());
        return a3;
    }

    public static aa a(aa aaVar, @NonNull Collection<sr> collection, @Nullable m mVar, @NonNull j jVar, @NonNull List<String> list) {
        String str;
        aa a2 = a(aaVar);
        try {
            JSONArray jSONArray = new JSONArray();
            for (sr srVar : collection) {
                jSONArray.put(new JSONObject().put("name", srVar.a).put("granted", srVar.b));
            }
            JSONObject jSONObject = new JSONObject();
            if (mVar != null) {
                jSONObject.put("background_restricted", mVar.b);
                jSONObject.put("app_standby_bucket", jVar.a(mVar.a));
            }
            str = new JSONObject().put(NativeProtocol.RESULT_ARGS_PERMISSIONS, jSONArray).put("background_restrictions", jSONObject).put("available_providers", new JSONArray(list)).toString();
        } catch (Throwable unused) {
            str = "";
        }
        return a2.a(a.EVENT_TYPE_PERMISSIONS.a()).c(str);
    }

    public static aa a(aa aaVar, String str) {
        return a(aaVar).a(a.EVENT_TYPE_APP_FEATURES.a()).c(str);
    }

    public static aa d(aa aaVar) {
        return a(aaVar, a.EVENT_TYPE_FIRST_ACTIVATION);
    }

    public static aa e(aa aaVar) {
        return a(aaVar, a.EVENT_TYPE_APP_UPDATE);
    }

    public static aa t() {
        return new aa().a(a.EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG.a());
    }
}
