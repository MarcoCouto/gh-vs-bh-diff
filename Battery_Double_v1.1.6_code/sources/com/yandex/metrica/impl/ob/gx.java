package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.gy.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Collection;

public class gx extends ce<wi> {
    @NonNull
    private final ha j;
    @NonNull
    private final aa k;
    @NonNull
    private final hb l;
    @NonNull
    private final a m;
    @NonNull
    private final abt n;
    @NonNull
    private aay o;
    @NonNull
    private final String p;
    @NonNull
    private final mo q;
    @Nullable
    private gz r;

    public gx(@NonNull ha haVar, @NonNull aa aaVar, @NonNull hb hbVar, @NonNull mo moVar) {
        this(haVar, aaVar, hbVar, moVar, new a(), new abs(), new aay(), new wi());
    }

    public gx(@NonNull ha haVar, @NonNull aa aaVar, @NonNull hb hbVar, @NonNull mo moVar, @NonNull a aVar, @NonNull abt abt, @NonNull aay aay, @NonNull wi wiVar) {
        super(new af(), wiVar);
        this.j = haVar;
        this.k = aaVar;
        this.l = hbVar;
        this.q = moVar;
        this.m = aVar;
        this.n = abt;
        this.o = aay;
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append("@");
        sb.append(Integer.toHexString(hashCode()));
        this.p = sb.toString();
    }

    public boolean a() {
        this.r = this.j.d();
        if (!(this.r.g() && !dl.a((Collection) this.r.a()))) {
            return false;
        }
        a(this.r.a());
        byte[] a = this.m.a(this.k, this.r, this.l, this.q).a();
        byte[] bArr = null;
        try {
            bArr = this.o.a(a);
        } catch (Throwable unused) {
        }
        if (!dl.a(bArr)) {
            a(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
            a = bArr;
        }
        a(a);
        return true;
    }

    public void d() {
        super.d();
        a(this.n.a());
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((wi) this.i).a(builder, this.r);
    }

    @NonNull
    public ca.a E() {
        return ca.a.DIAGNOSTIC;
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    @NonNull
    public String n() {
        return this.p;
    }
}
