package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.fm;

public interface fh<T extends fm> {
    T b(@NonNull Context context, @NonNull fb fbVar, @NonNull ew ewVar);
}
