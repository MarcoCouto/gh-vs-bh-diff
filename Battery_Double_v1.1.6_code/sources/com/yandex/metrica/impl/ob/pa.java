package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.pm.FeatureInfo;
import android.support.annotation.NonNull;

public abstract class pa {

    public static class a {
        public static pa a() {
            if (dl.a(24)) {
                return new b();
            }
            return new c();
        }
    }

    public static class b extends pa {
        @TargetApi(24)
        public pb a(@NonNull FeatureInfo featureInfo) {
            return new pb(featureInfo.name, featureInfo.version, c(featureInfo));
        }
    }

    public static class c extends pa {
        public pb a(@NonNull FeatureInfo featureInfo) {
            return new pb(featureInfo.name, c(featureInfo));
        }
    }

    /* access modifiers changed from: protected */
    public abstract pb a(FeatureInfo featureInfo);

    public pb b(@NonNull FeatureInfo featureInfo) {
        if (featureInfo.name != null) {
            return a(featureInfo);
        }
        if (featureInfo.reqGlEsVersion == 0) {
            return a(featureInfo);
        }
        return new pb("openGlFeature", featureInfo.reqGlEsVersion, c(featureInfo));
    }

    /* access modifiers changed from: 0000 */
    public boolean c(FeatureInfo featureInfo) {
        return (featureInfo.flags & 1) != 0;
    }
}
