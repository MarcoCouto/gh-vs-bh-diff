package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.vy.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class wf extends wb {
    @Nullable
    private List<String> a;
    @Nullable
    private List<String> b;
    @Nullable
    private String c;
    @Nullable
    private String d;
    @Nullable
    private Map<String, String> e;
    @Nullable
    private List<String> f;
    private boolean g;
    private boolean h;
    private String i;
    private long j;

    public static class a extends com.yandex.metrica.impl.ob.vy.a<a, a> implements vx<a, a> {
        @Nullable
        public final String a;
        @Nullable
        public final String b;
        @Nullable
        public final Map<String, String> f;
        public final boolean g;
        @Nullable
        public final List<String> h;

        /* renamed from: d */
        public boolean a(@NonNull a aVar) {
            return false;
        }

        public a(@NonNull et etVar) {
            this(etVar.h().d(), etVar.h().g(), etVar.h().h(), etVar.g().d(), etVar.g().e(), etVar.g().c(), etVar.g().a(), etVar.g().b());
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable Map<String, String> map, boolean z, @Nullable List<String> list) {
            super(str, str2, str3);
            this.a = str4;
            this.b = str5;
            this.f = map;
            this.g = z;
            this.h = list;
        }

        public a() {
            this(null, null, null, null, null, null, false, null);
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@NonNull a aVar) {
            return aVar.g ? aVar.g : this.g;
        }

        /* access modifiers changed from: 0000 */
        public List<String> b(@NonNull a aVar) {
            return aVar.g ? aVar.h : this.h;
        }

        @NonNull
        /* renamed from: c */
        public a b(@NonNull a aVar) {
            a aVar2 = new a((String) abw.a(this.c, aVar.c), (String) abw.a(this.d, aVar.d), (String) abw.a(this.e, aVar.e), (String) abw.a(this.a, aVar.a), (String) abw.a(this.b, aVar.b), (Map) abw.a(this.f, aVar.f), a(aVar), b(aVar));
            return aVar2;
        }
    }

    public static class b extends a<wf, a> {
        public b(@NonNull Context context, @NonNull String str) {
            this(context, str, new ado());
        }

        protected b(@NonNull Context context, @NonNull String str, @NonNull ado ado) {
            super(context, str, ado);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public wf b() {
            return new wf();
        }

        /* renamed from: a */
        public wf c(@NonNull c<a> cVar) {
            wf wfVar = (wf) super.c(cVar);
            a(wfVar, cVar.a);
            wfVar.n((String) abw.a(((a) cVar.b).a, cVar.a.s));
            wfVar.o((String) abw.a(((a) cVar.b).b, cVar.a.t));
            wfVar.b(((a) cVar.b).f);
            wfVar.b(((a) cVar.b).g);
            wfVar.c(((a) cVar.b).h);
            wfVar.a(cVar.a.v);
            wfVar.a(cVar.a.y);
            wfVar.a(cVar.a.G);
            return wfVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull wf wfVar, @NonNull yb ybVar) {
            wfVar.b(ybVar.j);
            wfVar.a(ybVar.k);
        }
    }

    private wf() {
        this.j = 0;
    }

    public List<String> a() {
        ArrayList arrayList = new ArrayList();
        if (!dl.a((Collection) this.a)) {
            arrayList.addAll(this.a);
        }
        if (!dl.a((Collection) this.b)) {
            arrayList.addAll(this.b);
        }
        arrayList.add("https://startup.mobile.yandex.net/");
        return arrayList;
    }

    public boolean b() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.h = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j2) {
        if (this.j == 0) {
            this.j = j2;
        }
    }

    public long c() {
        return this.j;
    }

    public long b(long j2) {
        a(j2);
        return c();
    }

    public List<String> I() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable List<String> list) {
        this.b = list;
    }

    @Nullable
    public Map<String, String> J() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable Map<String, String> map) {
        this.e = map;
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable List<String> list) {
        this.a = list;
    }

    @Nullable
    public String K() {
        return this.c;
    }

    /* access modifiers changed from: private */
    public void n(@Nullable String str) {
        this.c = str;
    }

    @Nullable
    public String L() {
        return this.d;
    }

    /* access modifiers changed from: private */
    public void o(@Nullable String str) {
        this.d = str;
    }

    @Nullable
    public List<String> M() {
        return this.f;
    }

    public void c(@Nullable List<String> list) {
        this.f = list;
    }

    @Nullable
    public boolean N() {
        return this.g;
    }

    public void b(boolean z) {
        this.g = z;
    }

    public String O() {
        return this.i;
    }

    public void a(String str) {
        this.i = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StartupRequestConfig{mStartupHostsFromStartup=");
        sb.append(this.a);
        sb.append(", mStartupHostsFromClient=");
        sb.append(this.b);
        sb.append(", mDistributionReferrer='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", mClidsFromClient=");
        sb.append(this.e);
        sb.append(", mNewCustomHosts=");
        sb.append(this.f);
        sb.append(", mHasNewCustomHosts=");
        sb.append(this.g);
        sb.append(", mSuccessfulStartup=");
        sb.append(this.h);
        sb.append(", mCountryInit='");
        sb.append(this.i);
        sb.append('\'');
        sb.append(", mFirstStartupTime='");
        sb.append(this.j);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
