package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public abstract class ye {
    @NonNull
    final yc a;

    public abstract <C extends fk> yh a();

    protected ye(@NonNull yc ycVar) {
        this.a = ycVar;
    }
}
