package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.Pair;
import com.yandex.metrica.impl.ob.ve.a.g;
import com.yandex.metrica.impl.ob.ve.a.g.C0109a;
import com.yandex.metrica.impl.ob.xp.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class nz implements np<xp, g> {
    private static final Map<Integer, a> a = Collections.unmodifiableMap(new HashMap<Integer, a>() {
        {
            put(Integer.valueOf(1), a.WIFI);
            put(Integer.valueOf(2), a.CELL);
        }
    });
    private static final Map<a, Integer> b = Collections.unmodifiableMap(new HashMap<a, Integer>() {
        {
            put(a.WIFI, Integer.valueOf(1));
            put(a.CELL, Integer.valueOf(2));
        }
    });

    @NonNull
    /* renamed from: a */
    public g b(@NonNull xp xpVar) {
        g gVar = new g();
        gVar.b = xpVar.a;
        gVar.c = xpVar.b;
        gVar.d = xpVar.c;
        gVar.e = a(xpVar.d);
        gVar.f = xpVar.e == null ? 0 : xpVar.e.longValue();
        gVar.g = b(xpVar.f);
        return gVar;
    }

    @NonNull
    public xp a(@NonNull g gVar) {
        xp xpVar = new xp(gVar.b, gVar.c, gVar.d, a(gVar.e), Long.valueOf(gVar.f), a(gVar.g));
        return xpVar;
    }

    @NonNull
    private C0109a[] a(@NonNull List<Pair<String, String>> list) {
        C0109a[] aVarArr = new C0109a[list.size()];
        int i = 0;
        for (Pair pair : list) {
            C0109a aVar = new C0109a();
            aVar.b = (String) pair.first;
            aVar.c = (String) pair.second;
            aVarArr[i] = aVar;
            i++;
        }
        return aVarArr;
    }

    @NonNull
    private List<Pair<String, String>> a(@NonNull C0109a[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (C0109a aVar : aVarArr) {
            arrayList.add(new Pair(aVar.b, aVar.c));
        }
        return arrayList;
    }

    @NonNull
    private List<a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    private int[] b(@NonNull List<a> list) {
        int[] iArr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            iArr[i] = ((Integer) b.get(list.get(i))).intValue();
        }
        return iArr;
    }
}
