package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

public class acz {
    /* access modifiers changed from: 0000 */
    @NonNull
    public acs a() {
        return new acs("YMM-MC");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Executor b() {
        return new adb();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs c() {
        return new acs("YMM-MSTE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs d() {
        return new acs("YMM-TP");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs e() {
        return new acs("YMM-UH-1");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs f() {
        return new acs("YMM-CSE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs g() {
        return new acs("YMM-CTH");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs h() {
        return new acs("YMM-SDCT");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acs i() {
        return new acs("YMM-DE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public acx a(@NonNull Runnable runnable) {
        return acy.a("YMM-IB", runnable);
    }
}
