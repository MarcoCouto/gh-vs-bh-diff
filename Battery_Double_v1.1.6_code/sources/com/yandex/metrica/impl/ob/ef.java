package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class ef implements abx<Thread, StackTraceElement[], lc> {
    @NonNull
    public lc a(@NonNull Thread thread, @Nullable StackTraceElement[] stackTraceElementArr) {
        List asList;
        String name = thread.getName();
        int priority = thread.getPriority();
        long id = thread.getId();
        String a = a(thread);
        Integer valueOf = Integer.valueOf(thread.getState().ordinal());
        if (stackTraceElementArr == null) {
            asList = null;
        } else {
            asList = Arrays.asList(stackTraceElementArr);
        }
        lc lcVar = new lc(name, priority, id, a, valueOf, asList);
        return lcVar;
    }

    @NonNull
    static String a(@NonNull Thread thread) {
        ThreadGroup threadGroup = thread.getThreadGroup();
        return threadGroup != null ? threadGroup.getName() : "";
    }
}
