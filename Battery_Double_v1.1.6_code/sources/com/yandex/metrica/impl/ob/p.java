package com.yandex.metrica.impl.ob;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class p implements fg {
    public static final C0090a a = C0090a.UNKNOWN;
    @NonNull
    private final Context b;
    /* access modifiers changed from: private */
    @Nullable
    public volatile a c;
    private final List<b> d = new ArrayList();
    private final BroadcastReceiver e = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            C0090a aVar;
            a a2 = p.this.c;
            if (a2 == null) {
                aVar = null;
            } else {
                aVar = a2.b;
            }
            a a3 = p.this.a(intent);
            p.this.c = a3;
            if (aVar != a3.b) {
                p.this.e();
            }
        }
    };

    public static class a {
        @Nullable
        public final Integer a;
        @NonNull
        public final C0090a b;

        /* renamed from: com.yandex.metrica.impl.ob.p$a$a reason: collision with other inner class name */
        public enum C0090a {
            UNKNOWN(-1),
            NONE(0),
            USB(1),
            WIRELESS(2),
            AC(3);
            
            private final int f;

            private C0090a(int i) {
                this.f = i;
            }

            public int a() {
                return this.f;
            }

            public static C0090a a(Integer num) {
                C0090a[] values;
                if (num != null) {
                    for (C0090a aVar : values()) {
                        if (aVar.a() == num.intValue()) {
                            return aVar;
                        }
                    }
                }
                return UNKNOWN;
            }
        }

        public a(@Nullable Integer num, @NonNull C0090a aVar) {
            this.a = num;
            this.b = aVar;
        }
    }

    public interface b {
        void a(@NonNull C0090a aVar);
    }

    public p(@NonNull Context context) {
        this.b = context;
    }

    public void a() {
        this.c = f();
    }

    public void b() {
        this.c = null;
        g();
    }

    @Nullable
    public Integer c() {
        a aVar = this.c;
        if (aVar == null) {
            return null;
        }
        return aVar.a;
    }

    @NonNull
    public C0090a d() {
        a aVar = this.c;
        return aVar == null ? C0090a.UNKNOWN : aVar.b;
    }

    public synchronized void a(@NonNull b bVar) {
        this.d.add(bVar);
        bVar.a(d());
    }

    public synchronized void b(@NonNull b bVar) {
        this.d.remove(bVar);
    }

    /* access modifiers changed from: private */
    public synchronized void e() {
        C0090a d2 = d();
        for (b a2 : this.d) {
            a2.a(d2);
        }
    }

    private synchronized a f() {
        return a(this.b.registerReceiver(this.e, new IntentFilter("android.intent.action.BATTERY_CHANGED")));
    }

    private synchronized void g() {
        this.b.unregisterReceiver(this.e);
    }

    /* access modifiers changed from: private */
    @NonNull
    public a a(@Nullable Intent intent) {
        Integer num;
        C0090a aVar = a;
        if (intent != null) {
            Integer b2 = b(intent);
            aVar = c(intent);
            num = b2;
        } else {
            num = null;
        }
        return new a(num, aVar);
    }

    @Nullable
    private Integer b(@NonNull Intent intent) {
        int intExtra = intent.getIntExtra("level", -1);
        int intExtra2 = intent.getIntExtra("scale", -1);
        if (intExtra <= 0 || intExtra2 <= 0) {
            return null;
        }
        return Integer.valueOf((intExtra * 100) / intExtra2);
    }

    @NonNull
    private C0090a c(@NonNull Intent intent) {
        int intExtra = intent.getIntExtra("plugged", -1);
        C0090a aVar = C0090a.NONE;
        if (intExtra == 4) {
            return C0090a.WIRELESS;
        }
        switch (intExtra) {
            case 1:
                return C0090a.AC;
            case 2:
                return C0090a.USB;
            default:
                return aVar;
        }
    }
}
