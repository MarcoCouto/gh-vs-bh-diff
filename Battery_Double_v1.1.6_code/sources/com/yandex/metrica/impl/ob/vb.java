package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface vb {

    public static final class a extends e {
        public C0098a[] b;

        /* renamed from: com.yandex.metrica.impl.ob.vb$a$a reason: collision with other inner class name */
        public static final class C0098a extends e {
            private static volatile C0098a[] d;
            public String b;
            public boolean c;

            public static C0098a[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new C0098a[0];
                        }
                    }
                }
                return d;
            }

            public C0098a() {
                e();
            }

            public C0098a e() {
                this.b = "";
                this.c = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public C0098a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = C0098a.d();
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (C0098a aVar : this.b) {
                    if (aVar != null) {
                        bVar.a(1, (e) aVar);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c = super.c();
            if (this.b != null && this.b.length > 0) {
                for (C0098a aVar : this.b) {
                    if (aVar != null) {
                        c += b.b(1, (e) aVar);
                    }
                }
            }
            return c;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    int b2 = g.b(aVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    C0098a[] aVarArr = new C0098a[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, aVarArr, 0, length);
                    }
                    while (length < aVarArr.length - 1) {
                        aVarArr[length] = new C0098a();
                        aVar.a((e) aVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    aVarArr[length] = new C0098a();
                    aVar.a((e) aVarArr[length]);
                    this.b = aVarArr;
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
