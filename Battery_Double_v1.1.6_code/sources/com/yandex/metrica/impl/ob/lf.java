package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.us.b;

public class lf implements nh<kz, b> {
    @NonNull
    private final le a;
    @NonNull
    private final lg b;

    public lf() {
        this(new le(new lj()), new lg());
    }

    @VisibleForTesting
    lf(@NonNull le leVar, @NonNull lg lgVar) {
        this.a = leVar;
        this.b = lgVar;
    }

    @NonNull
    /* renamed from: a */
    public b b(@NonNull kz kzVar) {
        b bVar = new b();
        bVar.b = this.a.b(kzVar.a);
        if (kzVar.b != null) {
            bVar.c = kzVar.b;
        }
        bVar.d = this.b.a(kzVar.c).intValue();
        return bVar;
    }

    @NonNull
    public kz a(@NonNull b bVar) {
        throw new UnsupportedOperationException();
    }
}
