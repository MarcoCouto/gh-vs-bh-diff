package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.Pair;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.ux.a;

class cv {
    @NonNull
    private final Revenue a;
    private final adn<String> b = new adk(30720, "revenue payload", this.e);
    private final adn<String> c = new adm(new adk(184320, "receipt data", this.e), "<truncated data was not sent, see METRIKALIB-4568>");
    private final adn<String> d = new adm(new adl(1000, "receipt signature", this.e), "<truncated data was not sent, see METRIKALIB-4568>");
    @NonNull
    private final abl e;

    cv(@NonNull Revenue revenue, @NonNull abl abl) {
        this.e = abl;
        this.a = revenue;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Pair<byte[], Integer> a() {
        ux uxVar = new ux();
        uxVar.d = this.a.currency.getCurrencyCode().getBytes();
        if (dl.a((Object) this.a.price)) {
            uxVar.c = this.a.price.doubleValue();
        }
        if (dl.a((Object) this.a.priceMicros)) {
            uxVar.h = this.a.priceMicros.longValue();
        }
        uxVar.e = dh.d(new adl(200, "revenue productID", this.e).a(this.a.productID));
        uxVar.b = ((Integer) abw.b(this.a.quantity, Integer.valueOf(1))).intValue();
        uxVar.f = dh.d((String) this.b.a(this.a.payload));
        int i = 0;
        if (dl.a((Object) this.a.receipt)) {
            a aVar = new a();
            String str = (String) this.c.a(this.a.receipt.data);
            if (adg.a(this.a.receipt.data, str)) {
                i = this.a.receipt.data.length() + 0;
            }
            String str2 = (String) this.d.a(this.a.receipt.signature);
            aVar.b = dh.d(str);
            aVar.c = dh.d(str2);
            uxVar.g = aVar;
        }
        return new Pair<>(e.a((e) uxVar), Integer.valueOf(i));
    }
}
