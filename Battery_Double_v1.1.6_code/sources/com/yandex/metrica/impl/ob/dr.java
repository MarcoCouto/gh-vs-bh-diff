package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.f;
import com.yandex.metrica.impl.ob.ab.a;
import com.yandex.metrica.j;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class dr implements a {
    @SuppressLint({"StaticFieldLeak"})
    private static dr a;
    private static ae b = new ae();
    private static volatile boolean c;
    /* access modifiers changed from: private */
    public static final EnumMap<Reason, AppMetricaDeviceIDListener.Reason> d = new EnumMap<>(Reason.class);
    private static acr e = new acr();
    private final Context f;
    private final cp g;
    private bj h;
    private bt i;
    private final xw j;
    private final ck k;
    private la l;
    private final eu m;
    /* access modifiers changed from: private */
    public IIdentifierCallback n;
    private final mn o;
    private cs p = new cs(this.m, this.f, k().b());
    @NonNull
    private v q;

    static {
        d.put(Reason.UNKNOWN, AppMetricaDeviceIDListener.Reason.UNKNOWN);
        d.put(Reason.INVALID_RESPONSE, AppMetricaDeviceIDListener.Reason.INVALID_RESPONSE);
        d.put(Reason.NETWORK, AppMetricaDeviceIDListener.Reason.NETWORK);
    }

    private dr(Context context) {
        this.f = context.getApplicationContext();
        lx f2 = lv.a(this.f).f();
        abl.a(context.getApplicationContext());
        cx.a();
        pe.b().a(this.f);
        Handler a2 = k().b().a();
        this.m = new eu(this.f, (ResultReceiver) new ab(a2, this));
        this.o = new mn(f2);
        b.a(this.p);
        new u(this.o).a(this.f);
        this.j = new xw(this.p, this.o, a2);
        this.p.a((xx) this.j);
        this.k = new ck(this.p, this.o, k().b());
        cp cpVar = new cp(this.f, this.m, this.p, a2, this.j);
        this.g = cpVar;
        this.q = new v();
        this.q.a();
    }

    public static act a() {
        return k().a();
    }

    public static synchronized void a(@NonNull Context context, @NonNull j jVar) {
        synchronized (dr.class) {
            a(context);
            if (((Boolean) abw.b(jVar.crashReporting, Boolean.valueOf(true))).booleanValue() && d().i == null) {
                d().i = new bt(Thread.getDefaultUncaughtExceptionHandler(), context);
                Thread.setDefaultUncaughtExceptionHandler(d().i);
            }
        }
    }

    public static synchronized void b(@NonNull Context context, @NonNull j jVar) {
        synchronized (dr.class) {
            abl a2 = abd.a(jVar.apiKey);
            abb b2 = abd.b(jVar.apiKey);
            boolean d2 = b.d();
            j a3 = b.a(jVar);
            a(context);
            if (a.h == null) {
                a.b(jVar);
                a.j.a(a2);
                a.a(jVar);
                a.m.a(jVar);
                a.a(a3, d2);
                StringBuilder sb = new StringBuilder();
                sb.append("Activate AppMetrica with APIKey ");
                sb.append(dl.b(a3.apiKey));
                Log.i("AppMetrica", sb.toString());
                if (aau.a(a3.logs)) {
                    a2.a();
                    b2.a();
                    abd.a().a();
                    abd.b().a();
                } else {
                    a2.b();
                    b2.b();
                    abd.a().b();
                    abd.b().b();
                }
            } else if (a2.c()) {
                a2.b("Appmetrica already has been activated!");
            }
        }
    }

    public static synchronized void a(@NonNull final Context context) {
        synchronized (dr.class) {
            if (a == null) {
                a = new dr(context.getApplicationContext());
                a.m();
                a().a((Runnable) new Runnable() {
                    public void run() {
                        xa.a(context);
                    }
                });
            }
        }
    }

    public static void b() {
        c = true;
    }

    public static boolean c() {
        return c;
    }

    public static synchronized dr d() {
        dr drVar;
        synchronized (dr.class) {
            drVar = a;
        }
        return drVar;
    }

    public static dr b(Context context) {
        a(context.getApplicationContext());
        return d();
    }

    @Nullable
    public static dr e() {
        return d();
    }

    public static synchronized bj f() {
        bj bjVar;
        synchronized (dr.class) {
            bjVar = d().h;
        }
        return bjVar;
    }

    public static synchronized boolean g() {
        boolean z;
        synchronized (dr.class) {
            z = (a == null || a.h == null) ? false : true;
        }
        return z;
    }

    private void m() {
        be.a();
        k().b().a((Runnable) new abh.a(this.f));
    }

    private void a(j jVar) {
        if (jVar != null) {
            this.j.a(jVar.d);
            this.j.a(jVar.b);
            this.j.a(jVar.c);
            if (dl.a((Object) jVar.c)) {
                this.j.b(bd.API.d);
            }
        }
    }

    public void a(@NonNull f fVar) {
        this.g.a(fVar);
    }

    @NonNull
    public ay b(@NonNull f fVar) {
        return this.g.b(fVar);
    }

    public void a(String str) {
        this.k.a(str);
    }

    @VisibleForTesting
    static bm h() {
        return g() ? d().h : b;
    }

    public static void a(Location location) {
        h().a(location);
    }

    public static void a(boolean z) {
        h().a(z);
    }

    public void b(boolean z) {
        h().a(z);
    }

    public void c(boolean z) {
        h().b(z);
    }

    public void d(boolean z) {
        h().setStatisticsSending(z);
    }

    public String i() {
        return this.j.b();
    }

    public String j() {
        return this.j.a();
    }

    public void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        this.j.a(iIdentifierCallback, list, this.m.c());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(j jVar, boolean z) {
        this.p.a(jVar.locationTracking, jVar.statisticsSending, (Boolean) null);
        this.h = this.g.a(jVar, z, this.o);
        this.j.c();
    }

    public void a(int i2, @NonNull Bundle bundle) {
        this.j.a(i2, bundle);
    }

    public void a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        this.k.a(deferredDeeplinkParametersListener);
    }

    public void a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        this.n = new IIdentifierCallback() {
            public void onReceive(Map<String, String> map) {
                dr.this.n = null;
                appMetricaDeviceIDListener.onLoaded((String) map.get("appmetrica_device_id_hash"));
            }

            public void onRequestError(Reason reason) {
                dr.this.n = null;
                appMetricaDeviceIDListener.onError((AppMetricaDeviceIDListener.Reason) dr.d.get(reason));
            }
        };
        this.j.a(this.n, Collections.singletonList("appmetrica_device_id_hash"), this.m.c());
    }

    private void b(@NonNull j jVar) {
        if (this.i != null) {
            this.i.a((la) new lb(new cq(this.g, "20799a27-fa80-4b36-b2db-0f8141f24180"), new la.a() {
                public boolean a(Throwable th) {
                    return cx.a(th);
                }
            }, null));
            this.i.a((la) new lb(new cq(this.g, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"), new la.a() {
                public boolean a(Throwable th) {
                    return cx.b(th);
                }
            }, null));
            if (this.l == null) {
                this.l = new lb(new cq(this.g, jVar.apiKey), new la.a() {
                    public boolean a(Throwable th) {
                        return true;
                    }
                }, jVar.n);
            }
            this.i.a(this.l);
        }
    }

    public static acr k() {
        return e;
    }
}
