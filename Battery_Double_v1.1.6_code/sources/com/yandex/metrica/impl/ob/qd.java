package com.yandex.metrica.impl.ob;

public class qd {
    public final long a;
    public final long b;

    public qd(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IntervalRange{minInterval=");
        sb.append(this.a);
        sb.append(", maxInterval=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
