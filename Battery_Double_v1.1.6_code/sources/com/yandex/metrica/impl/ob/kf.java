package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;

public class kf {
    @NonNull
    private final c a;

    @TargetApi(26)
    static class a implements c {
        @NonNull
        private final kc a;

        public a(@NonNull Context context) {
            this.a = new kc(context);
        }

        @NonNull
        public kd a() {
            return this.a;
        }
    }

    static class b implements c {
        @NonNull
        private final ke a;

        public b(@NonNull Context context) {
            this.a = new ke(context);
        }

        @NonNull
        public kd a() {
            return this.a;
        }
    }

    interface c {
        @NonNull
        kd a();
    }

    public kf(@NonNull Context context) {
        this(VERSION.SDK_INT >= 26 ? new a(context) : new b(context));
    }

    kf(@NonNull c cVar) {
        this.a = cVar;
    }

    public kd a() {
        return this.a.a();
    }
}
