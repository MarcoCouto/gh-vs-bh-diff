package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.io.File;

public class kw implements Runnable {
    @NonNull
    private final Context a;
    @NonNull
    private final File b;
    @NonNull
    private final aby<ln> c;

    public kw(@NonNull Context context, @NonNull File file, @NonNull aby<ln> aby) {
        this.a = context;
        this.b = file;
        this.c = aby;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0013 */
    public void run() {
        if (this.b.exists()) {
            try {
                a(ax.a(this.a, this.b));
            } catch (Throwable unused) {
            }
            try {
                this.b.delete();
                return;
            } catch (Throwable unused2) {
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                this.c.a(new ln(str));
            } catch (Throwable unused) {
            }
        }
    }
}
