package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.cg.a;
import java.util.EnumSet;

public class sd {
    private static final EnumSet<a> a = EnumSet.of(a.OFFLINE);
    private aaz b = new aaw();
    private final Context c;

    public sd(@NonNull Context context) {
        this.c = context;
    }

    public boolean a() {
        return !a.contains(this.b.a(this.c));
    }
}
