package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class aac implements zq {
    private final int a;

    public aac(int i) {
        this.a = i;
    }

    public void a(@NonNull aad aad) {
        if (aad.a.length() > this.a) {
            int length = aad.a.length() - this.a;
            aad.a = aad.a.substring(0, this.a);
            aad.c = Integer.valueOf(aad.a.length() + length);
        }
    }
}
