package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class vu {
    @NonNull
    public final String a;
    public final long b;
    public final long c;

    @Nullable
    public static vu a(@NonNull byte[] bArr) throws d {
        if (dl.a(bArr)) {
            return null;
        }
        return new vu(bArr);
    }

    private vu(@NonNull byte[] bArr) throws d {
        uw a2 = uw.a(bArr);
        this.a = a2.b;
        this.b = a2.d;
        this.c = a2.c;
    }

    public vu(@NonNull String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }

    public byte[] a() {
        uw uwVar = new uw();
        uwVar.b = this.a;
        uwVar.d = this.b;
        uwVar.c = this.c;
        return e.a((e) uwVar);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        vu vuVar = (vu) obj;
        if (this.b == vuVar.b && this.c == vuVar.c) {
            return this.a.equals(vuVar.a);
        }
        return false;
    }

    public int hashCode() {
        return (((this.a.hashCode() * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ReferrerInfo{installReferrer='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", referrerClickTimestampSeconds=");
        sb.append(this.b);
        sb.append(", installBeginTimestampSeconds=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
