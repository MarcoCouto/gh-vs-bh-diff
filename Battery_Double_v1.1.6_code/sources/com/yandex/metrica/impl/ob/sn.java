package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

class sn implements sm {
    private final boolean a;

    sn(boolean z) {
        this.a = z;
    }

    public boolean a(@NonNull String str) {
        if ("android.permission.ACCESS_FINE_LOCATION".equals(str) || "android.permission.ACCESS_COARSE_LOCATION".equals(str)) {
            return this.a;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LocationFlagStrategy{mEnabled=");
        sb.append(this.a);
        sb.append('}');
        return sb.toString();
    }
}
