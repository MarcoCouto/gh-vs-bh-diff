package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class gf {
    private final Object a;
    private final gl b;
    private final HashMap<ge, gg> c;
    private final acb<a, ge> d;
    @NonNull
    private final Context e;
    private volatile int f;
    @NonNull
    private final gi g;

    private static final class a {
        @NonNull
        private final String a;
        @Nullable
        private final Integer b;
        @Nullable
        private final String c;

        a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
            this.a = str;
            this.b = num;
            this.c = str2;
        }

        a(@NonNull ge geVar) {
            this(geVar.b(), geVar.c(), geVar.d());
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
            if (this.c != null) {
                i = this.c.hashCode();
            }
            return hashCode + i;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (!this.a.equals(aVar.a)) {
                return false;
            }
            if (this.b == null ? aVar.b != null : !this.b.equals(aVar.b)) {
                return false;
            }
            if (this.c != null) {
                z = this.c.equals(aVar.c);
            } else if (aVar.c != null) {
                z = false;
            }
            return z;
        }
    }

    public gf(@NonNull Context context, @NonNull gl glVar) {
        this(context, glVar, new gi());
    }

    @VisibleForTesting
    gf(@NonNull Context context, @NonNull gl glVar, @NonNull gi giVar) {
        this.a = new Object();
        this.c = new HashMap<>();
        this.d = new acb<>();
        this.f = 0;
        this.e = context.getApplicationContext();
        this.b = glVar;
        this.g = giVar;
    }

    public gg a(@NonNull ge geVar, @NonNull ew ewVar) {
        gg ggVar;
        synchronized (this.a) {
            ggVar = (gg) this.c.get(geVar);
            if (ggVar == null) {
                ggVar = this.g.a(geVar).a(this.e, this.b, geVar, ewVar);
                this.c.put(geVar, ggVar);
                this.d.a(new a(geVar), geVar);
                this.f++;
            }
        }
        return ggVar;
    }

    public void a(@NonNull String str, int i, String str2) {
        a(str, Integer.valueOf(i), str2);
    }

    private void a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
        synchronized (this.a) {
            Collection<ge> b2 = this.d.b(new a(str, num, str2));
            if (!dl.a((Collection) b2)) {
                this.f -= b2.size();
                ArrayList arrayList = new ArrayList(b2.size());
                for (ge remove : b2) {
                    arrayList.add(this.c.remove(remove));
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((gg) it.next()).a();
                }
            }
        }
    }

    public int a() {
        return this.f;
    }
}
