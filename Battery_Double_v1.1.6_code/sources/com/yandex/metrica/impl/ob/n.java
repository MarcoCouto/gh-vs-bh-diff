package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.m.a;

public class n {
    @NonNull
    private final Context a;
    /* access modifiers changed from: private */
    @NonNull
    public final j b;

    public n(@NonNull Context context) {
        this(context, new j());
    }

    @VisibleForTesting
    n(@NonNull Context context, @NonNull j jVar) {
        this.a = context;
        this.b = jVar;
    }

    @Nullable
    public m a() {
        if (dl.a(28)) {
            return b();
        }
        return null;
    }

    @TargetApi(28)
    @NonNull
    private m b() {
        return new m((a) dl.a((aca<T, S>) new aca<UsageStatsManager, a>() {
            public a a(@NonNull UsageStatsManager usageStatsManager) {
                return n.this.b.a(usageStatsManager.getAppStandbyBucket());
            }
        }, (UsageStatsManager) this.a.getSystemService("usagestats"), "getting app standby bucket", "usageStatsManager"), (Boolean) dl.a((aca<T, S>) new aca<ActivityManager, Boolean>() {
            public Boolean a(@NonNull ActivityManager activityManager) throws Throwable {
                return Boolean.valueOf(activityManager.isBackgroundRestricted());
            }
        }, (ActivityManager) this.a.getSystemService("activity"), "getting is background restricted", "activityManager"));
    }
}
