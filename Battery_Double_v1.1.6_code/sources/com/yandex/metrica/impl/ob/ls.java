package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.metrica.impl.ob.mb.a;
import com.yandex.metrica.impl.ob.mi.b;
import com.yandex.metrica.impl.ob.mi.d;
import com.yandex.metrica.impl.ob.mi.h;
import java.util.HashMap;

public class ls {
    @NonNull
    private final lt a;
    @NonNull
    private final lw b;
    @NonNull
    private final a c;

    public ls(@NonNull lt ltVar, @NonNull lw lwVar) {
        this(ltVar, lwVar, new a());
    }

    public ls(@NonNull lt ltVar, @NonNull lw lwVar, @NonNull a aVar) {
        this.a = ltVar;
        this.b = lwVar;
        this.c = aVar;
    }

    public mb a() {
        return this.c.a(ParametersKeys.MAIN, this.a.c(), this.a.d(), this.a.a(), new md(ParametersKeys.MAIN, this.b.a()));
    }

    public mb b() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", d.a);
        hashMap.put("binary_data", b.a);
        hashMap.put("startup", h.a);
        hashMap.put("l_dat", mi.a.a);
        hashMap.put("lbs_dat", mi.a.a);
        return this.c.a("metrica.db", this.a.g(), this.a.h(), this.a.b(), new md("metrica.db", hashMap));
    }

    public mb c() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", d.a);
        return this.c.a("client storage", this.a.e(), this.a.f(), new SparseArray(), new md("metrica.db", hashMap));
    }
}
