package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.io.File;

public class kx implements Runnable {
    @NonNull
    private final File a;
    @NonNull
    private final aby<File> b;

    public kx(@NonNull File file, @NonNull aby<File> aby) {
        this.a = file;
        this.b = aby;
    }

    public void run() {
        if (this.a.exists() && this.a.isDirectory()) {
            File[] listFiles = this.a.listFiles();
            if (listFiles != null) {
                for (File a2 : listFiles) {
                    this.b.a(a2);
                }
            }
        }
    }
}
