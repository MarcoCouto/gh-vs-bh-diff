package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.tapjoy.TJAdUnitConstants.String;

public class qm {
    @NonNull
    public final ql a;
    @NonNull
    public final qu b;

    public enum a {
        UNKNOWN("unknown"),
        FOREGROUND("fg"),
        BACKGROUND("bg"),
        VISIBLE(String.VISIBLE);
        
        private final String e;

        private a(String str) {
            this.e = str;
        }

        @NonNull
        public String toString() {
            return this.e;
        }

        @NonNull
        public String a() {
            return this.e;
        }

        @NonNull
        public static a a(@Nullable String str) {
            a[] values;
            a aVar = UNKNOWN;
            for (a aVar2 : values()) {
                if (aVar2.e.equals(str)) {
                    aVar = aVar2;
                }
            }
            return aVar;
        }

        @NonNull
        public static a a(@Nullable com.yandex.metrica.impl.ob.k.a aVar) {
            a aVar2 = UNKNOWN;
            if (aVar == null) {
                return aVar2;
            }
            switch (aVar) {
                case VISIBLE:
                    return VISIBLE;
                case FOREGROUND:
                    return FOREGROUND;
                case BACKGROUND:
                    return BACKGROUND;
                default:
                    return aVar2;
            }
        }
    }

    public qm(@NonNull ql qlVar, @NonNull qu quVar) {
        this.a = qlVar;
        this.b = quVar;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LocationCollectionConfig{arguments=");
        sb.append(this.a);
        sb.append(", preconditions=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
