package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class qt implements LocationListener {
    @NonNull
    private final rj a;

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public qt(@NonNull rj rjVar) {
        this.a = rjVar;
    }

    public void onLocationChanged(@Nullable Location location) {
        if (location != null) {
            this.a.a(location);
        }
    }
}
