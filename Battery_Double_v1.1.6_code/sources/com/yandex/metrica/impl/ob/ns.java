package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.d;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a;
import com.yandex.metrica.impl.ob.ve.a.d.b;

public class ns implements np<qm, d> {
    @NonNull
    private final nr a;
    @NonNull
    private final nt b;

    public ns() {
        this(new nr(), new nt());
    }

    @VisibleForTesting
    ns(@NonNull nr nrVar, @NonNull nt ntVar) {
        this.a = nrVar;
        this.b = ntVar;
    }

    @NonNull
    /* renamed from: a */
    public d b(@NonNull qm qmVar) {
        d dVar = new d();
        dVar.b = this.a.b(qmVar.a);
        dVar.c = this.b.b(qmVar.b);
        return dVar;
    }

    @NonNull
    public qm a(@NonNull d dVar) {
        return new qm(this.a.a((C0105a) abw.b(dVar.b, new C0105a())), this.b.a((b) abw.b(dVar.c, new b())));
    }
}
