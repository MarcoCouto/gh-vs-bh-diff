package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.vy.c;

public class wb extends vy {
    @NonNull
    private String a;
    @NonNull
    private String b;

    protected static abstract class a<T extends wb, A extends com.yandex.metrica.impl.ob.vy.a> extends b<T, A> {
        private final ado c;

        protected a(@NonNull Context context, @NonNull String str) {
            this(context, str, new ado());
        }

        protected a(@NonNull Context context, @NonNull String str, @NonNull ado ado) {
            super(context, str);
            this.c = ado;
        }

        @NonNull
        /* renamed from: b */
        public T c(@NonNull c<A> cVar) {
            T t = (wb) super.a(cVar);
            String packageName = this.a.getPackageName();
            ApplicationInfo b = this.c.b(this.a, this.b, 0);
            if (b != null) {
                t.l(a(b));
                t.m(b(b));
            } else if (TextUtils.equals(packageName, this.b)) {
                t.l(a(this.a.getApplicationInfo()));
                t.m(b(this.a.getApplicationInfo()));
            } else {
                t.l("0");
                t.m("0");
            }
            return t;
        }

        @NonNull
        private String a(@NonNull ApplicationInfo applicationInfo) {
            return (applicationInfo.flags & 2) != 0 ? "1" : "0";
        }

        @NonNull
        private String b(@NonNull ApplicationInfo applicationInfo) {
            return (applicationInfo.flags & 1) != 0 ? "1" : "0";
        }
    }

    @NonNull
    public String G() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public void l(@NonNull String str) {
        this.a = str;
    }

    public String H() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void m(@NonNull String str) {
        this.b = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CoreRequestConfig{mAppDebuggable='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", mAppSystem='");
        sb.append(this.b);
        sb.append('\'');
        sb.append("} ");
        sb.append(super.toString());
        return sb.toString();
    }
}
