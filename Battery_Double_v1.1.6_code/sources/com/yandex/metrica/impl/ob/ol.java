package com.yandex.metrica.impl.ob;

import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a;
import com.yandex.metrica.impl.ob.xi.a.c;

public class ol implements np<c, C0103a.c> {
    @NonNull
    /* renamed from: a */
    public C0103a.c b(@NonNull c cVar) {
        C0103a.c cVar2 = new C0103a.c();
        cVar2.b = cVar.a.toString();
        if (cVar.b != null) {
            cVar2.c = cVar.b.toString();
        }
        return cVar2;
    }

    @NonNull
    public c a(@NonNull C0103a.c cVar) {
        return new c(ParcelUuid.fromString(cVar.b), TextUtils.isEmpty(cVar.c) ? null : ParcelUuid.fromString(cVar.c));
    }
}
