package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class hb {
    @Nullable
    private Long a;
    private int b;
    @NonNull
    private abt c;

    public static class a {
        public final long a;
        public final long b;
        public final int c;

        public a(long j, long j2, int i) {
            this.a = j;
            this.c = i;
            this.b = j2;
        }
    }

    public hb() {
        this(new abs());
    }

    public hb(@NonNull abt abt) {
        this.c = abt;
    }

    public a a() {
        if (this.a == null) {
            this.a = Long.valueOf(this.c.b());
        }
        a aVar = new a(this.a.longValue(), this.a.longValue(), this.b);
        this.b++;
        return aVar;
    }
}
