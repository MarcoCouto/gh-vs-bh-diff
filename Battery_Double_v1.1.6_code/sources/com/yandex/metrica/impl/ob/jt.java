package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class jt {
    private final mo a;

    public jt(@NonNull mo moVar) {
        this.a = moVar;
    }

    public long a() {
        long j = this.a.j();
        long j2 = 10000000000L;
        if (j >= 10000000000L) {
            j2 = 1 + j;
        }
        this.a.e(j2).q();
        return j2;
    }
}
