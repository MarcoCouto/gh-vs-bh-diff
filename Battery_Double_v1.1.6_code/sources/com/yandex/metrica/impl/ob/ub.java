package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.uy.a.C0096a;

public class ub extends ud<Boolean> {
    public ub(@NonNull String str, boolean z, @NonNull adw<String> adw, @NonNull ua uaVar) {
        super(3, str, Boolean.valueOf(z), adw, uaVar);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0096a aVar) {
        aVar.e.e = ((Boolean) b()).booleanValue();
    }
}
