package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a.e;
import java.util.ArrayList;
import java.util.List;

public class nv implements nq<sr, e> {
    @NonNull
    /* renamed from: a */
    public e[] b(@NonNull List<sr> list) {
        e[] eVarArr = new e[list.size()];
        for (int i = 0; i < list.size(); i++) {
            eVarArr[i] = a((sr) list.get(i));
        }
        return eVarArr;
    }

    @NonNull
    public List<sr> a(@NonNull e[] eVarArr) {
        ArrayList arrayList = new ArrayList(eVarArr.length);
        for (e a : eVarArr) {
            arrayList.add(a(a));
        }
        return arrayList;
    }

    @NonNull
    private e a(@NonNull sr srVar) {
        e eVar = new e();
        eVar.b = srVar.a;
        eVar.c = srVar.b;
        return eVar;
    }

    @NonNull
    private sr a(@NonNull e eVar) {
        return new sr(eVar.b, eVar.c);
    }
}
