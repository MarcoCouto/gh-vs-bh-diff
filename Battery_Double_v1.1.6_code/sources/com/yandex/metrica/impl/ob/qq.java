package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

public class qq {
    @NonNull
    private final acu a = as.a().k().e();
    @NonNull
    private final lz b;
    @NonNull
    private final ly c;
    @NonNull
    private final su d;
    @NonNull
    private final so e;

    public qq(@NonNull Context context) {
        this.b = lv.a(context).g();
        this.c = lv.a(context).h();
        this.d = new su();
        this.e = new so(this.d.a());
    }

    @NonNull
    public acu a() {
        return this.a;
    }

    @NonNull
    public lz b() {
        return this.b;
    }

    @NonNull
    public ly c() {
        return this.c;
    }

    @NonNull
    public su d() {
        return this.d;
    }

    @NonNull
    public so e() {
        return this.e;
    }
}
