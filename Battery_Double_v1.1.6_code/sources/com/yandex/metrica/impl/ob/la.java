package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.c;

public abstract class la {
    private final a a;
    @Nullable
    private final c b;

    public interface a {
        boolean a(Throwable th);
    }

    /* access modifiers changed from: 0000 */
    public abstract void b(@NonNull ld ldVar);

    la(a aVar, @Nullable c cVar) {
        this.a = aVar;
        this.b = cVar;
    }

    public void a(@NonNull ld ldVar) {
        if (this.a.a(ldVar.a())) {
            Throwable a2 = ldVar.a();
            if (!(this.b == null || a2 == null)) {
                a2 = this.b.a(a2);
                if (a2 == null) {
                    return;
                }
            }
            ld ldVar2 = new ld(a2, ldVar.c, ldVar.d, ldVar.e, ldVar.f);
            b(ldVar2);
        }
    }
}
