package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class dj {
    @NonNull
    private final abt a;

    private boolean a(long j, long j2, long j3, @NonNull String str) {
        boolean z = true;
        if (j < j2) {
            return true;
        }
        if (j - j2 < j3) {
            z = false;
        }
        return z;
    }

    public dj() {
        this(new abs());
    }

    @VisibleForTesting
    dj(@NonNull abt abt) {
        this.a = abt;
    }

    public boolean a(long j, long j2, @NonNull String str) {
        return a(this.a.b(), j, j2, str);
    }

    public boolean b(long j, long j2, @NonNull String str) {
        return a(this.a.a(), j, j2, str);
    }
}
