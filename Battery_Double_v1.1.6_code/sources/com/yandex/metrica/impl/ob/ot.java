package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.x.b;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class ot {
    private final String a;
    private final String b;
    private final String c;
    private final Point d;

    @SuppressLint({"NewApi", "HardwareIds", "ObsoleteSdkInt"})
    public ot(@NonNull Context context, @Nullable String str, @NonNull so soVar) {
        this.a = Build.MANUFACTURER;
        this.b = Build.MODEL;
        this.c = a(context, str, soVar);
        b bVar = x.a(context).f;
        this.d = new Point(bVar.a, bVar.b);
    }

    @SuppressLint({"HardwareIds", "ObsoleteSdkInt", "MissingPermission", "NewApi"})
    @NonNull
    private String a(@NonNull Context context, @Nullable String str, @NonNull so soVar) {
        if (dl.a(28)) {
            if (soVar.d(context)) {
                try {
                    return Build.getSerial();
                } catch (Throwable unused) {
                }
            }
            return (String) abw.b(str, "");
        } else if (dl.a(8)) {
            return Build.SERIAL;
        } else {
            return (String) abw.b(str, "");
        }
    }

    @NonNull
    public String a() {
        return this.c;
    }

    public ot(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        this.a = jSONObject.getString("manufacturer");
        this.b = jSONObject.getString("model");
        this.c = jSONObject.getString("serial");
        this.d = new Point(jSONObject.getInt("width"), jSONObject.getInt("height"));
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("manufacturer", this.a);
        jSONObject.put("model", this.b);
        jSONObject.put("serial", this.c);
        jSONObject.put("width", this.d.x);
        jSONObject.put("height", this.d.y);
        return jSONObject;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ot otVar = (ot) obj;
        if (this.a == null ? otVar.a != null : !this.a.equals(otVar.a)) {
            return false;
        }
        if (this.b == null ? otVar.b != null : !this.b.equals(otVar.b)) {
            return false;
        }
        if (this.d != null) {
            z = this.d.equals(otVar.d);
        } else if (otVar.d != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSnapshot{mManufacturer='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", mModel='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", mSerial='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", mScreenSize=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
}
