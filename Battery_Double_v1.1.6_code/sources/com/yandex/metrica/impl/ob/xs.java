package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class xs {
    public final long a;
    public final String b;
    public final List<Integer> c;
    public final long d;
    public final int e;

    public xs(long j, @NonNull String str, @NonNull List<Integer> list, long j2, int i) {
        this.a = j;
        this.b = str;
        this.c = Collections.unmodifiableList(list);
        this.d = j2;
        this.e = i;
    }

    @Nullable
    public static xs a(@Nullable String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            xs xsVar = new xs(jSONObject.getLong("seconds_to_live"), jSONObject.getString(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY), a(jSONObject.getJSONArray("ports")), jSONObject.getLong("first_delay_seconds"), jSONObject.getInt("launch_delay_seconds"));
            return xsVar;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static ArrayList<Integer> a(JSONArray jSONArray) throws JSONException {
        ArrayList<Integer> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(Integer.valueOf(jSONArray.getInt(i)));
        }
        return arrayList;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        xs xsVar = (xs) obj;
        if (this.a != xsVar.a || this.d != xsVar.d || this.e != xsVar.e) {
            return false;
        }
        if (this.b == null ? xsVar.b != null : !this.b.equals(xsVar.b)) {
            return false;
        }
        if (this.c != null) {
            z = this.c.equals(xsVar.c);
        } else if (xsVar.c != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((int) (this.a ^ (this.a >>> 32))) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return ((((hashCode + i) * 31) + ((int) (this.d ^ (this.d >>> 32)))) * 31) + this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SocketConfig{secondsToLive=");
        sb.append(this.a);
        sb.append(", token='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", ports=");
        sb.append(this.c);
        sb.append(", firstDelaySeconds=");
        sb.append(this.d);
        sb.append(", launchDelaySeconds=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
