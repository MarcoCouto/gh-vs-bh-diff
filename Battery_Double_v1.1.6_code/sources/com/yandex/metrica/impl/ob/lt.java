package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.yandex.metrica.impl.ob.mk.a;
import com.yandex.metrica.impl.ob.mk.aa;
import com.yandex.metrica.impl.ob.mk.ab;
import com.yandex.metrica.impl.ob.mk.ac;
import com.yandex.metrica.impl.ob.mk.b;
import com.yandex.metrica.impl.ob.mk.c;
import com.yandex.metrica.impl.ob.mk.d;
import com.yandex.metrica.impl.ob.mk.e;
import com.yandex.metrica.impl.ob.mk.f;
import com.yandex.metrica.impl.ob.mk.g;
import com.yandex.metrica.impl.ob.mk.h;
import com.yandex.metrica.impl.ob.mk.i;
import com.yandex.metrica.impl.ob.mk.j;
import com.yandex.metrica.impl.ob.mk.k;
import com.yandex.metrica.impl.ob.mk.l;
import com.yandex.metrica.impl.ob.mk.m;
import com.yandex.metrica.impl.ob.mk.n;
import com.yandex.metrica.impl.ob.mk.o;
import com.yandex.metrica.impl.ob.mk.p;
import com.yandex.metrica.impl.ob.mk.q;
import com.yandex.metrica.impl.ob.mk.r;
import com.yandex.metrica.impl.ob.mk.s;
import com.yandex.metrica.impl.ob.mk.t;
import com.yandex.metrica.impl.ob.mk.u;
import com.yandex.metrica.impl.ob.mk.v;
import com.yandex.metrica.impl.ob.mk.w;
import com.yandex.metrica.impl.ob.mk.x;
import com.yandex.metrica.impl.ob.mk.y;
import com.yandex.metrica.impl.ob.mk.z;

public class lt {
    @NonNull
    private final SparseArray<mj> a = new SparseArray<>();
    @NonNull
    private final SparseArray<mj> b;
    @NonNull
    private final mj c;
    @NonNull
    private final mj d;
    @NonNull
    private final mj e;
    @NonNull
    private final mj f;
    @NonNull
    private final mj g;
    @NonNull
    private final mj h;

    public lt() {
        this.a.put(6, new v());
        this.a.put(7, new z());
        this.a.put(14, new o());
        this.a.put(29, new p());
        this.a.put(37, new q());
        this.a.put(39, new r());
        this.a.put(45, new s());
        this.a.put(47, new t());
        this.a.put(50, new u());
        this.a.put(60, new w());
        this.a.put(66, new x());
        this.a.put(67, new y());
        this.a.put(73, new aa());
        this.a.put(77, new ab());
        this.a.put(87, new ac());
        this.b = new SparseArray<>();
        this.b.put(12, new g());
        this.b.put(29, new h());
        this.b.put(47, new i());
        this.b.put(50, new j());
        this.b.put(55, new k());
        this.b.put(60, new l());
        this.b.put(63, new m());
        this.b.put(67, new n());
        this.c = new c();
        this.d = new d();
        this.e = new a();
        this.f = new b();
        this.g = new e();
        this.h = new f();
    }

    @NonNull
    public SparseArray<mj> a() {
        return this.a;
    }

    @NonNull
    public SparseArray<mj> b() {
        return this.b;
    }

    @NonNull
    public mj c() {
        return this.c;
    }

    @NonNull
    public mj d() {
        return this.d;
    }

    @NonNull
    public mj e() {
        return this.e;
    }

    @NonNull
    public mj f() {
        return this.f;
    }

    @NonNull
    public mj g() {
        return this.g;
    }

    @NonNull
    public mj h() {
        return this.h;
    }
}
