package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;
import java.util.Map;

public class ew {
    public final com.yandex.metrica.impl.ob.wf.a a;
    public final a b;
    @Nullable
    public final ResultReceiver c;

    public static class a implements vx<a, a> {
        @Nullable
        public final String a;
        @Nullable
        public final String b;
        @Nullable
        public final String c;
        @Nullable
        public final String d;
        @Nullable
        public final Boolean e;
        @Nullable
        public final Location f;
        @Nullable
        public final Boolean g;
        @Nullable
        public final Boolean h;
        @Nullable
        public final Integer i;
        @Nullable
        public final Integer j;
        @Nullable
        public final Integer k;
        @Nullable
        public final Boolean l;
        @Nullable
        public final Boolean m;
        @Nullable
        public final Boolean n;
        @Nullable
        public final Map<String, String> o;
        @Nullable
        public final Integer p;
        @Nullable
        public final Boolean q;
        @Nullable
        public final Boolean r;

        a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map, @Nullable Integer num4, @Nullable Boolean bool7, @Nullable Boolean bool8) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = bool;
            this.f = location;
            this.g = bool2;
            this.h = bool3;
            this.n = bool4;
            this.i = num;
            this.j = num2;
            this.k = num3;
            this.l = bool5;
            this.m = bool6;
            this.o = map;
            this.p = num4;
            this.q = bool7;
            this.r = bool8;
        }

        public a(@NonNull CounterConfiguration counterConfiguration, @Nullable Map<String, String> map) {
            this(counterConfiguration.d(), counterConfiguration.g(), counterConfiguration.h(), counterConfiguration.e(), counterConfiguration.f(), counterConfiguration.k(), counterConfiguration.o(), counterConfiguration.l(), counterConfiguration.i(), counterConfiguration.c(), counterConfiguration.b(), counterConfiguration.a(), counterConfiguration.j(), counterConfiguration.q(), map, counterConfiguration.p(), counterConfiguration.m(), counterConfiguration.n());
        }

        public a() {
            this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }

        @NonNull
        /* renamed from: a */
        public a b(@NonNull a aVar) {
            a aVar2 = aVar;
            a aVar3 = new a((String) abw.a(this.a, aVar2.a), (String) abw.a(this.b, aVar2.b), (String) abw.a(this.c, aVar2.c), (String) abw.a(this.d, aVar2.d), (Boolean) abw.a(this.e, aVar2.e), (Location) abw.a(this.f, aVar2.f), (Boolean) abw.a(this.g, aVar2.g), (Boolean) abw.a(this.h, aVar2.h), (Boolean) abw.a(this.n, aVar2.n), (Integer) abw.a(this.i, aVar2.i), (Integer) abw.a(this.j, aVar2.j), (Integer) abw.a(this.k, aVar2.k), (Boolean) abw.a(this.l, aVar2.l), (Boolean) abw.a(this.m, aVar2.m), (Map) abw.a(this.o, aVar2.o), (Integer) abw.a(this.p, aVar2.p), (Boolean) abw.a(this.q, aVar2.q), (Boolean) abw.a(this.r, aVar2.r));
            return aVar3;
        }

        /* renamed from: b */
        public boolean a(@NonNull a aVar) {
            return equals(aVar);
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == null ? aVar.a != null : !this.a.equals(aVar.a)) {
                return false;
            }
            if (this.b == null ? aVar.b != null : !this.b.equals(aVar.b)) {
                return false;
            }
            if (this.c == null ? aVar.c != null : !this.c.equals(aVar.c)) {
                return false;
            }
            if (this.d == null ? aVar.d != null : !this.d.equals(aVar.d)) {
                return false;
            }
            if (this.e == null ? aVar.e != null : !this.e.equals(aVar.e)) {
                return false;
            }
            if (this.f == null ? aVar.f != null : !this.f.equals(aVar.f)) {
                return false;
            }
            if (this.g == null ? aVar.g != null : !this.g.equals(aVar.g)) {
                return false;
            }
            if (this.h == null ? aVar.h != null : !this.h.equals(aVar.h)) {
                return false;
            }
            if (this.i == null ? aVar.i != null : !this.i.equals(aVar.i)) {
                return false;
            }
            if (this.j == null ? aVar.j != null : !this.j.equals(aVar.j)) {
                return false;
            }
            if (this.k == null ? aVar.k != null : !this.k.equals(aVar.k)) {
                return false;
            }
            if (this.l == null ? aVar.l != null : !this.l.equals(aVar.l)) {
                return false;
            }
            if (this.m == null ? aVar.m != null : !this.m.equals(aVar.m)) {
                return false;
            }
            if (this.n == null ? aVar.n != null : !this.n.equals(aVar.n)) {
                return false;
            }
            if (this.o == null ? aVar.o != null : !this.o.equals(aVar.o)) {
                return false;
            }
            if (this.p == null ? aVar.p != null : !this.p.equals(aVar.p)) {
                return false;
            }
            if (this.q != null) {
                z = this.q.equals(aVar.q);
            } else if (aVar.q != null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i2 = 0;
            int hashCode = (((((((((((((((((((((((((((((((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31) + (this.h != null ? this.h.hashCode() : 0)) * 31) + (this.i != null ? this.i.hashCode() : 0)) * 31) + (this.j != null ? this.j.hashCode() : 0)) * 31) + (this.k != null ? this.k.hashCode() : 0)) * 31) + (this.l != null ? this.l.hashCode() : 0)) * 31) + (this.m != null ? this.m.hashCode() : 0)) * 31) + (this.n != null ? this.n.hashCode() : 0)) * 31) + (this.o != null ? this.o.hashCode() : 0)) * 31) + (this.p != null ? this.p.hashCode() : 0)) * 31;
            if (this.q != null) {
                i2 = this.q.hashCode();
            }
            return hashCode + i2;
        }
    }

    public ew(@NonNull et etVar) {
        this(new com.yandex.metrica.impl.ob.wf.a(etVar), new a(etVar.h(), etVar.g().c()), etVar.g().j());
    }

    public ew(@NonNull com.yandex.metrica.impl.ob.wf.a aVar, @NonNull a aVar2, @Nullable ResultReceiver resultReceiver) {
        this.a = aVar;
        this.b = aVar2;
        this.c = resultReceiver;
    }
}
