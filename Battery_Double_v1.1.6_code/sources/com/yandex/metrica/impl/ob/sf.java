package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ca.a;

public class sf {
    @NonNull
    private final mq a;
    @NonNull
    private final a b;

    public sf(@NonNull mq mqVar, @NonNull a aVar) {
        this.a = mqVar;
        this.b = aVar;
    }

    public int a() {
        return this.a.a(this.b, 1);
    }

    public long b() {
        return this.a.a(this.b, 0);
    }

    public void a(int i) {
        this.a.b(this.b, i);
    }

    public void a(long j) {
        this.a.b(this.b, j);
    }
}
