package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class adi {
    private final adl a;
    private final adl b;
    private final ade c;
    @NonNull
    private final abl d;
    private final String e;

    public adi(int i, int i2, int i3, @NonNull String str, @NonNull abl abl) {
        ade ade = new ade(i);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("map key");
        adl adl = new adl(i2, sb.toString(), abl);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("map value");
        this(ade, adl, new adl(i3, sb2.toString(), abl), str, abl);
    }

    @VisibleForTesting
    adi(@NonNull ade ade, @NonNull adl adl, @NonNull adl adl2, @NonNull String str, @NonNull abl abl) {
        this.c = ade;
        this.a = adl;
        this.b = adl2;
        this.e = str;
        this.d = abl;
    }

    public adl a() {
        return this.a;
    }

    public adl b() {
        return this.b;
    }

    public ade c() {
        return this.c;
    }

    public void a(@NonNull String str) {
        if (this.d.c()) {
            this.d.b("The %s has reached the limit of %d items. Item with key %s will be ignored", this.e, Integer.valueOf(this.c.a()), str);
        }
    }
}
