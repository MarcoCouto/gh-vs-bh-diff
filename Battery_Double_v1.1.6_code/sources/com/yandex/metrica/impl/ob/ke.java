package com.yandex.metrica.impl.ob;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NotificationCompat;
import com.yandex.metrica.ConfigurationService;

public class ke implements kd, kg {
    /* access modifiers changed from: private */
    @NonNull
    public final Context a;
    @Nullable
    private final AlarmManager b;
    /* access modifiers changed from: private */
    @NonNull
    public abt c;

    public ke(@NonNull Context context) {
        this(context, (AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM), new abs());
    }

    public void a(final long j, boolean z) {
        dl.a((abz<T>) new abz<AlarmManager>() {
            public void a(@NonNull AlarmManager alarmManager) throws Throwable {
                alarmManager.set(3, ke.this.c.c() + j, ke.this.a(ke.this.a));
            }
        }, this.b, "scheduling wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    public void a() {
        dl.a((abz<T>) new abz<AlarmManager>() {
            public void a(@NonNull AlarmManager alarmManager) throws Throwable {
                alarmManager.cancel(ke.this.a(ke.this.a));
            }
        }, this.b, "cancelling scheduled wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    public void a(@NonNull Bundle bundle) {
        try {
            this.a.startService(new Intent().setComponent(new ComponentName(this.a.getPackageName(), ConfigurationService.class.getName())).setAction("com.yandex.metrica.configuration.ACTION_INIT").putExtras(bundle));
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public PendingIntent a(@NonNull Context context) {
        return PendingIntent.getService(context, 7695435, b(context), 134217728);
    }

    @NonNull
    private Intent b(@NonNull Context context) {
        return new Intent(context, ConfigurationService.class).setAction("com.yandex.metrica.configuration.ACTION_SCHEDULED_START");
    }

    @VisibleForTesting
    ke(@NonNull Context context, @Nullable AlarmManager alarmManager, @NonNull abt abt) {
        this.a = context;
        this.b = alarmManager;
        this.c = abt;
    }
}
