package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.Pair;
import com.yandex.metrica.impl.ob.xp.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class yr {
    private static final Map<String, a> a = Collections.unmodifiableMap(new HashMap<String, a>() {
        {
            put("wifi", a.WIFI);
            put("cell", a.CELL);
        }
    });

    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull abc.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("requests");
        if (optJSONObject != null && optJSONObject.has("list")) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                ArrayList arrayList = new ArrayList(optJSONArray.length());
                for (int i = 0; i < optJSONArray.length(); i++) {
                    try {
                        arrayList.add(a(optJSONArray.getJSONObject(i)));
                    } catch (Throwable unused) {
                    }
                }
                if (!arrayList.isEmpty()) {
                    yxVar.d((List<xp>) arrayList);
                }
            }
        }
    }

    @NonNull
    private xp a(@NonNull JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject("headers");
        ArrayList arrayList = new ArrayList(jSONObject2.length());
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            JSONArray jSONArray = jSONObject2.getJSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(new Pair(str, jSONArray.getString(i)));
            }
        }
        xp xpVar = new xp(abc.b(jSONObject, "id"), abc.b(jSONObject, "url"), abc.b(jSONObject, "method"), arrayList, Long.valueOf(jSONObject.getLong("delay_seconds")), b(jSONObject));
        return xpVar;
    }

    @NonNull
    private static List<a> b(@NonNull JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has("accept_network_types")) {
            JSONArray jSONArray = jSONObject.getJSONArray("accept_network_types");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(a.get(jSONArray.getString(i)));
            }
        }
        return arrayList;
    }
}
