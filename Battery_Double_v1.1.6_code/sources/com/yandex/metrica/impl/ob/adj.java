package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class adj {
    @NonNull
    private final adi a;
    @NonNull
    private final adh b;

    public adj(@NonNull abl abl, @NonNull String str) {
        adi adi = new adi(30, 50, 4000, str, abl);
        this(adi, new adh(4500, str, abl));
    }

    @VisibleForTesting
    adj(@NonNull adi adi, @NonNull adh adh) {
        this.a = adi;
        this.b = adh;
    }

    public boolean a(@Nullable abj abj, @NonNull String str, @Nullable String str2) {
        if (abj != null) {
            String a2 = this.a.a().a(str);
            String a3 = this.a.b().a(str2);
            if (abj.containsKey(a2)) {
                String str3 = (String) abj.get(a2);
                if (a3 == null || !a3.equals(str3)) {
                    return a(abj, a2, a3, str3);
                }
            } else if (a3 != null) {
                return a(abj, a2, a3, null);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean a(@NonNull abj abj, @NonNull String str, @Nullable String str2, @Nullable String str3) {
        if (abj.size() >= this.a.c().a()) {
            if (this.a.c().a() != abj.size() || !abj.containsKey(str)) {
                this.a.a(str);
                return false;
            }
        }
        if (!this.b.a(abj, str, str2)) {
            abj.put(str, str2);
            return true;
        }
        this.b.a(str);
        return false;
    }
}
