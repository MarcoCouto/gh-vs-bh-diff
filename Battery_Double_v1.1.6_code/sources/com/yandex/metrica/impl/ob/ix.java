package com.yandex.metrica.impl.ob;

public class ix {
    private final hz a;
    private final id b;
    private final Cif c;
    private final ih d;
    private final je e;
    private final ii f;
    private final ig g;
    private final ij h;
    private final hs i;
    private final hr j;
    private final hx k;
    private final ic l;
    private final ib m;
    private final hw n;
    private final ik o;
    private final ht p;
    private final hy q;
    private final hv r;

    public ix(fe feVar) {
        this.a = new hz(feVar);
        this.b = new id(feVar);
        this.c = new Cif(feVar);
        this.d = new ih(feVar);
        this.e = new je(feVar);
        this.f = new ii(feVar);
        this.g = new ig(feVar);
        this.h = new ij(feVar);
        this.i = new hs(feVar);
        this.j = new hr(feVar);
        this.k = new hx(feVar);
        this.l = new ic(feVar);
        this.m = new ib(feVar, new ss());
        this.n = new hw(feVar);
        this.o = new ik(feVar);
        this.p = new ht(feVar);
        this.q = new hy(feVar);
        this.r = new hv(feVar, xa.a());
    }

    public hz a() {
        return this.a;
    }

    public id b() {
        return this.b;
    }

    public Cif c() {
        return this.c;
    }

    public ih d() {
        return this.d;
    }

    public je e() {
        return this.e;
    }

    public ii f() {
        return this.f;
    }

    public ig g() {
        return this.g;
    }

    public ij h() {
        return this.h;
    }

    public hs i() {
        return this.i;
    }

    public hr j() {
        return this.j;
    }

    public hx k() {
        return this.k;
    }

    public ic l() {
        return this.l;
    }

    public ib m() {
        return this.m;
    }

    public hw n() {
        return this.n;
    }

    public ik o() {
        return this.o;
    }

    public ht p() {
        return this.p;
    }

    public hy q() {
        return this.q;
    }

    public hv r() {
        return this.r;
    }
}
