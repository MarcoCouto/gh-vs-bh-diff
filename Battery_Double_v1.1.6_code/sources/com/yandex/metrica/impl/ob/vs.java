package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class vs implements InvocationHandler {
    @NonNull
    private Object a;
    @NonNull
    private final vv b;

    vs(@NonNull Object obj, @NonNull vv vvVar) {
        this.a = obj;
        this.b = vvVar;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        if (!"onInstallReferrerSetupFinished".equals(method.getName())) {
            vv vvVar = this.b;
            StringBuilder sb = new StringBuilder();
            sb.append("Unexpected method called ");
            sb.append(method.getName());
            vvVar.a((Throwable) new IllegalArgumentException(sb.toString()));
        } else if (objArr.length != 1) {
            this.b.a((Throwable) new IllegalArgumentException("Args size is not equal to one."));
        } else if (objArr[0].equals(Integer.valueOf(0))) {
            try {
                Object invoke = this.a.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(this.a, new Object[0]);
                vv vvVar2 = this.b;
                vu vuVar = new vu((String) invoke.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(invoke, new Object[0]), ((Long) invoke.getClass().getMethod("getReferrerClickTimestampSeconds", new Class[0]).invoke(invoke, new Object[0])).longValue(), ((Long) invoke.getClass().getMethod("getInstallBeginTimestampSeconds", new Class[0]).invoke(invoke, new Object[0])).longValue());
                vvVar2.a(vuVar);
            } catch (Throwable th) {
                this.b.a(th);
            }
        } else {
            vv vvVar3 = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Referrer check failed with error ");
            sb2.append(objArr[0]);
            vvVar3.a((Throwable) new IllegalStateException(sb2.toString()));
        }
        return null;
    }
}
