package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.qm.a;
import org.json.JSONArray;

public class qg {
    @NonNull
    private final qh a;
    @NonNull
    private final qi b;
    @NonNull
    private final abs c;
    @Nullable
    private ql d;
    @NonNull
    private final p e;
    @NonNull
    private final dn f;
    @NonNull
    private final rt g;
    @NonNull
    private final k h;
    private boolean i;
    private final Runnable j;

    public qg(@NonNull qh qhVar) {
        this(qhVar, new rt(qhVar.a.a, qhVar.f == null ? null : qhVar.f.k));
    }

    private qg(@NonNull qh qhVar, @NonNull rt rtVar) {
        this(qhVar, new qi(qhVar.a.a), new abs(), as.a().n(), as.a().o(), dn.a(qhVar.a.a), rtVar, new au(qhVar.a.b, rtVar));
    }

    qg(@NonNull qh qhVar, @NonNull qi qiVar, @NonNull abs abs, @NonNull p pVar, @NonNull k kVar, @NonNull dn dnVar, @NonNull rt rtVar, @NonNull au auVar) {
        this.j = new Runnable() {
            public void run() {
                qg.this.d();
                qg.this.e();
            }
        };
        this.a = qhVar;
        this.b = qiVar;
        this.c = abs;
        this.d = this.a.f;
        this.e = pVar;
        this.h = kVar;
        this.f = dnVar;
        this.g = rtVar;
        this.f.d().a(auVar);
    }

    public void a() {
        c();
    }

    public void b() {
        f();
    }

    public void c() {
        boolean z = this.d != null && this.d.i;
        if (this.i != z) {
            this.i = z;
            if (this.i) {
                e();
            } else {
                f();
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.d != null && this.d.h > 0) {
            this.a.a.b.a(this.j, this.d.h);
        }
    }

    private void f() {
        this.a.a.b.b(this.j);
    }

    public void a(@Nullable ql qlVar) {
        this.d = qlVar;
        this.g.a(qlVar == null ? null : qlVar.k);
        c();
    }

    public void d() {
        final qk qkVar = new qk();
        qkVar.a(this.c.a());
        qkVar.b(this.c.c());
        this.g.b();
        qkVar.a((JSONArray) this.f.d().a());
        this.a.c.a((zc) new zc() {
            public void a(zb[] zbVarArr) {
                qkVar.b(abc.a(zbVarArr));
            }
        });
        qkVar.a(this.e.d());
        qkVar.a(a.a(this.h.c()));
        this.b.a(qkVar);
        this.a.d.a();
        this.a.e.a();
    }
}
