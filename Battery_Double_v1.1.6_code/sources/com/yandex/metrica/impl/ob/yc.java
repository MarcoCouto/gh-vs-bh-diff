package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.IParamsCallback;
import com.yandex.metrica.impl.ob.pc.a;
import com.yandex.metrica.impl.ob.wf.b;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class yc implements fj {
    private static final a[] a = {a.GOOGLE, a.HMS};
    @NonNull
    private final Context b;
    @NonNull
    private final fb c;
    @NonNull
    private final ya d;
    @NonNull
    private volatile mx<yb> e;
    @Nullable
    private volatile de f;
    @NonNull
    private xu g;

    public yc(@NonNull Context context, @NonNull String str, @NonNull wf.a aVar, @NonNull ya yaVar) {
        this(context, new ex(str), aVar, yaVar, op.a.a(yb.class).a(context), new aba());
    }

    private yc(@NonNull Context context, @NonNull fb fbVar, @NonNull wf.a aVar, @NonNull ya yaVar, @NonNull mx<yb> mxVar, @NonNull aba aba) {
        this(context, fbVar, aVar, yaVar, mxVar, (yb) mxVar.a(), aba);
    }

    private yc(@NonNull Context context, @NonNull fb fbVar, @NonNull wf.a aVar, @NonNull ya yaVar, @NonNull mx<yb> mxVar, @NonNull yb ybVar, @NonNull aba aba) {
        xu xuVar = new xu(new b(context, fbVar.b()), ybVar, aVar);
        this(context, fbVar, yaVar, mxVar, ybVar, aba, xuVar);
    }

    @VisibleForTesting
    yc(@NonNull Context context, @NonNull fb fbVar, @NonNull ya yaVar, @NonNull mx<yb> mxVar, @NonNull yb ybVar, @NonNull aba aba, @NonNull xu xuVar) {
        this.b = context;
        this.c = fbVar;
        this.d = yaVar;
        this.e = mxVar;
        this.g = xuVar;
        a(ybVar, aba);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0074  */
    private void a(@NonNull yb ybVar, @NonNull aba aba) {
        boolean z;
        yb.a a2 = ybVar.a();
        String str = "";
        pc a3 = a(((wf) this.g.d()).E());
        if (a3 != null) {
            str = aba.a(a3.b);
            if (!TextUtils.equals(ybVar.c, str)) {
                a2 = a2.c(str);
            } else {
                z = false;
                if (!a(ybVar.a) || !b(ybVar.b)) {
                    if (!a(ybVar.a)) {
                        a2 = a2.a(aba.a());
                    }
                    if (!b(ybVar.b)) {
                        a2 = a2.b(str).d("");
                    }
                    z = true;
                }
                if (!z) {
                    yb a4 = a2.a();
                    d(a4);
                    b(a4);
                    return;
                }
                b(ybVar);
                return;
            }
        } else {
            a2 = a2.c(str);
        }
        z = true;
        if (!a(ybVar.a)) {
        }
        if (!b(ybVar.b)) {
        }
        z = true;
        if (!z) {
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public pc a(@NonNull Map<a, pc> map) {
        for (a aVar : a) {
            pc pcVar = (pc) map.get(aVar);
            if (pcVar != null && !TextUtils.isEmpty(pcVar.b)) {
                return pcVar;
            }
        }
        return null;
    }

    @NonNull
    public fb c() {
        return this.c;
    }

    @Nullable
    public synchronized de a() {
        if (!b()) {
            return null;
        }
        if (this.f == null) {
            this.f = new de(this, d());
        }
        return this.f;
    }

    public synchronized boolean a(@Nullable List<String> list, @NonNull Map<String, String> map) {
        boolean z = false;
        if (list == null) {
            return false;
        }
        yb b2 = this.g.b();
        for (String str : list) {
            z = str.equals("yandex_mobile_metrica_uuid") ? z | (!a(b2.a)) : str.equals("yandex_mobile_metrica_device_id") ? z | (!b(b2.b)) : str.equals("appmetrica_device_id_hash") ? z | (!c(b2.d)) : str.equals("yandex_mobile_metrica_get_ad_url") ? z | (!d(b2.f)) : str.equals("yandex_mobile_metrica_report_ad_url") ? z | (!e(b2.g)) : str.equals(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS) ? z | (!b(map)) : true;
        }
        return z;
    }

    public synchronized boolean b() {
        boolean z;
        z = e().H;
        if (!z) {
            z = !a(((Long) abw.b(Long.valueOf(e().u), Long.valueOf(0))).longValue());
            if (!z && !b(((wf) this.g.d()).J())) {
                z = true;
            }
        }
        return z;
    }

    private boolean b(@Nullable Map<String, String> map) {
        if (dl.a((Map) map)) {
            return true;
        }
        return map.equals(abq.a(e().n));
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        return false;
     */
    @VisibleForTesting
    public synchronized boolean a(long j) {
        if (!((wf) this.g.d()).g()) {
            return false;
        }
        long b2 = abu.b() - j;
        if (b2 <= 86400 && b2 >= 0) {
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull yb ybVar) {
        this.e.a(ybVar);
    }

    @NonNull
    public wf d() {
        return (wf) this.g.d();
    }

    private synchronized void f() {
        this.f = null;
    }

    public void a(@NonNull yx yxVar, @NonNull wf wfVar, @Nullable Map<String, List<String>> map) {
        yb a2;
        synchronized (this) {
            Long l = (Long) abw.b(yw.a(map), Long.valueOf(0));
            a(yxVar.r(), l);
            a2 = a(yxVar, wfVar, l);
            new os().a(this.b, new oq(a2.b, a2.d), new so(sl.b().a(a2).a()));
            f();
            d(a2);
        }
        c(a2);
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    @NonNull
    public yb a(@NonNull yx yxVar, @NonNull wf wfVar, @Nullable Long l) {
        String a2 = abq.a(wfVar.J());
        String a3 = a(yxVar.j(), e().m);
        String str = e().b;
        if (TextUtils.isEmpty(str)) {
            str = yxVar.h();
        }
        yb e2 = e();
        return new yb.a(yxVar.a()).a(abu.b()).b(str).c(e().c).d(yxVar.i()).a(e().a).e(yxVar.d()).c(yxVar.b()).d(wfVar.I()).a(yxVar.e()).b(yxVar.g()).f(yxVar.f()).g(yxVar.c()).e(yxVar.u()).h(a3).i(a2).b(a(wfVar.J(), a3)).a(yxVar.l()).f(yxVar.p()).a(yxVar.q()).g(yxVar.s()).l(yxVar.t()).a(yxVar.m()).h(yxVar.o()).a(yxVar.n()).a(yxVar.v()).a(true).b(((Long) abw.b(l, Long.valueOf(abu.b() * 1000))).longValue()).c(((wf) this.g.d()).b(l.longValue())).c(false).j(e2.s).k(e2.t).a(yxVar.w()).a(yxVar.x()).a(yxVar.y()).a(yxVar.z()).a(yxVar.A()).b(yxVar.B()).a();
    }

    private void c(@NonNull yb ybVar) {
        this.d.a(this.c.b(), ybVar);
        b(ybVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(yb ybVar) {
        eh.a().b((ej) new er(this.c.b(), ybVar));
        if (!TextUtils.isEmpty(ybVar.a)) {
            eh.a().b((ej) new es(ybVar.a, this.c.b()));
        }
        if (!TextUtils.isEmpty(ybVar.b)) {
            eh.a().b((ej) new eo(ybVar.b));
        }
        if (ybVar.r == null) {
            eh.a().a(eq.class);
        } else {
            eh.a().b((ej) new eq(ybVar.r));
        }
    }

    private void a(@Nullable Long l, @NonNull Long l2) {
        abp.a().a(l2.longValue(), l);
    }

    private void d(@NonNull yb ybVar) {
        this.g.a(ybVar);
        a(ybVar);
        e(ybVar);
    }

    @Nullable
    private static String a(@Nullable String str, @Nullable String str2) {
        if (abq.b(str)) {
            return str;
        }
        if (abq.b(str2)) {
            return str2;
        }
        return null;
    }

    private boolean a(Map<String, String> map, @Nullable String str) {
        Map a2 = abq.a(str);
        if (dl.a((Map) map)) {
            return dl.a(a2);
        }
        return a2.equals(map);
    }

    @Deprecated
    private void e(yb ybVar) {
        if (!TextUtils.isEmpty(ybVar.b)) {
            try {
                Intent intent = new Intent("com.yandex.metrica.intent.action.SYNC");
                intent.putExtra("CAUSE", "CAUSE_DEVICE_ID");
                intent.putExtra("SYNC_TO_PKG", this.c.b());
                intent.putExtra("SYNC_DATA", ybVar.b);
                intent.putExtra("SYNC_DATA_2", ybVar.a);
                this.b.sendBroadcast(intent);
            } catch (Throwable unused) {
            }
        }
    }

    @NonNull
    public yb e() {
        return this.g.b();
    }

    public void a(@NonNull xv xvVar) {
        f();
        this.d.a(c().b(), xvVar, e());
    }

    public synchronized void a(@NonNull wf.a aVar) {
        this.g.a(aVar);
        a((wf) this.g.d());
    }

    private void a(wf wfVar) {
        if (wfVar.N()) {
            boolean z = false;
            List M = wfVar.M();
            yb.a aVar = null;
            if (dl.a((Collection) M) && !dl.a((Collection) wfVar.I())) {
                aVar = e().a().d(null);
                z = true;
            }
            if (!dl.a((Collection) M) && !dl.a((Object) M, (Object) wfVar.I())) {
                aVar = e().a().d(M);
                z = true;
            }
            if (z) {
                d(aVar.a());
            }
        }
    }

    public synchronized void a(String str, @NonNull bd bdVar) {
        d(e().a().j(str).k(bdVar.d).a());
    }

    private boolean a(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean b(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean c(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean d(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean e(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }
}
