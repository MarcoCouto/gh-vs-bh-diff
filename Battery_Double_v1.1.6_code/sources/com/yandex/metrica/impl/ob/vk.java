package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.interact.CellularNetworkInfo;
import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.j;
import com.yandex.metrica.p.Ucc;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import org.json.JSONObject;

public class vk {
    /* access modifiers changed from: private */
    @NonNull
    public final vl a;
    @NonNull
    private final act b;
    @NonNull
    private final vg c;
    @NonNull
    private final adw<Context> d;
    @NonNull
    private final adw<String> e;
    @NonNull
    private final a f;

    @Deprecated
    static class a {
        a() {
        }

        public qf a(@NonNull Context context, @Nullable LocationManager locationManager) {
            return new qf(context, locationManager, new so(new sj()));
        }
    }

    public void a(@NonNull Context context, @NonNull Object obj) {
    }

    public void b(@NonNull Context context, @NonNull Object obj) {
    }

    public vk(@NonNull act act) {
        this(act, new vl());
    }

    public vk(@NonNull act act, @NonNull vl vlVar) {
        this(act, vlVar, new vg(vlVar), new ads(new adr("Context")), new ads(new adr("Event name")), new a());
    }

    public vk(@NonNull act act, @NonNull vl vlVar, @NonNull vg vgVar, @NonNull adw<Context> adw, @NonNull adw<String> adw2, @NonNull a aVar) {
        this.a = vlVar;
        this.b = act;
        this.c = vgVar;
        this.d = adw;
        this.e = adw2;
        this.f = aVar;
    }

    @Deprecated
    public void a(final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.b.a((Runnable) new abn() {
            public void a() throws Exception {
                if (vk.this.a.e()) {
                    vk.this.a.a().a(iIdentifierCallback, list);
                }
            }
        });
    }

    public void a(@NonNull final Context context, @NonNull final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.d.a(context);
        this.b.a((Runnable) new abn() {
            public void a() throws Exception {
                vk.this.a.a(context).a(iIdentifierCallback, list);
            }
        });
    }

    public boolean a() {
        return this.a.c();
    }

    @Nullable
    public Future<String> b() {
        return this.b.a((Callable<T>) new abm<String>() {
            /* renamed from: a */
            public String b() {
                return pe.b().d();
            }
        });
    }

    @Nullable
    public Future<Boolean> c() {
        return this.b.a((Callable<T>) new abm<Boolean>() {
            /* renamed from: a */
            public Boolean b() {
                return pe.b().e();
            }
        });
    }

    @NonNull
    public DeviceInfo a(Context context) {
        this.d.a(context);
        return DeviceInfo.getInstance(context);
    }

    @NonNull
    public String b(Context context) {
        this.d.a(context);
        return new CellularNetworkInfo(context).getCelluralInfo();
    }

    @Nullable
    public Integer c(Context context) {
        this.d.a(context);
        return cg.c(context);
    }

    @Nullable
    @Deprecated
    public String d() {
        if (this.a.e()) {
            return this.a.a().j();
        }
        return null;
    }

    @Nullable
    public String d(@NonNull Context context) {
        this.d.a(context);
        return this.a.a(context).j();
    }

    @Nullable
    public String e(@NonNull Context context) {
        this.d.a(context);
        return this.a.a(context).i();
    }

    @NonNull
    public String f(@NonNull Context context) {
        this.d.a(context);
        return context.getPackageName();
    }

    public void a(int i, @NonNull String str, @Nullable String str2, @Nullable Map<String, String> map) {
        this.c.a();
        this.e.a(str);
        act act = this.b;
        final int i2 = i;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        AnonymousClass5 r1 = new abn() {
            public void a() throws Exception {
                vk.this.a.f().a(i2, str3, str4, map2);
            }
        };
        act.a((Runnable) r1);
    }

    public void e() {
        this.c.a();
        this.b.a((Runnable) new abn() {
            public void a() throws Exception {
                vk.this.a.f().sendEventsBuffer();
            }
        });
    }

    @NonNull
    public String a(@Nullable String str) {
        return cx.a(str);
    }

    @NonNull
    public String a(int i) {
        return cj.a(i);
    }

    @NonNull
    public YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return j.b(yandexMetricaConfig).a(Collections.singletonList(str)).b();
    }

    @NonNull
    public YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return j.b(yandexMetricaConfig).a(list).b();
    }

    @Nullable
    @Deprecated
    public Location g(@NonNull Context context) {
        LocationManager locationManager;
        this.d.a(context);
        try {
            locationManager = (LocationManager) context.getSystemService("location");
        } catch (Throwable unused) {
            locationManager = null;
        }
        return this.f.a(context, locationManager).a();
    }

    public void a(@NonNull final Context context, final boolean z) {
        this.d.a(context);
        this.b.a((Runnable) new abn() {
            public void a() throws Exception {
                vk.this.a.a(context).c(z);
            }
        });
    }

    public void a(@NonNull final Ucc ucc, final boolean z) {
        if (!this.a.d()) {
            ucc.onError("Main API key is not activated");
        } else {
            this.b.a((Runnable) new abn() {
                public void a() throws Exception {
                    vk.this.a.f().a((aai) new aai() {
                        public void a(@NonNull JSONObject jSONObject) {
                            ucc.onResult(jSONObject);
                        }

                        public void a(@NonNull String str) {
                            ucc.onError(str);
                        }
                    }, z);
                }
            });
        }
    }
}
