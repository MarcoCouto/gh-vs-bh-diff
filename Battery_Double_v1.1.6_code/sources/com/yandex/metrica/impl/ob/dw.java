package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.ConfigurationServiceReceiver;

@TargetApi(26)
public class dw implements dx {
    @NonNull
    private final dt a;
    /* access modifiers changed from: private */
    @NonNull
    public final ea b;
    /* access modifiers changed from: private */
    @NonNull
    public final dy c;
    /* access modifiers changed from: private */
    @NonNull
    public final PendingIntent d;

    public dw(@NonNull Context context) {
        this(new dt(context), new ea(), new dy(), PendingIntent.getBroadcast(context.getApplicationContext(), 7695436, new Intent("com.yandex.metrica.configuration.service.PLC").setClass(context, ConfigurationServiceReceiver.class), 134217728));
    }

    @SuppressLint({"MissingPermission"})
    public synchronized void a(@NonNull final xi xiVar) {
        BluetoothLeScanner a2 = this.a.a();
        if (a2 != null) {
            a();
            Integer num = (Integer) dl.a((aca<T, S>) new aca<BluetoothLeScanner, Integer>() {
                public Integer a(@NonNull BluetoothLeScanner bluetoothLeScanner) throws Exception {
                    return Integer.valueOf(bluetoothLeScanner.startScan(dw.this.c.a(xiVar.b), dw.this.b.a(xiVar.a), dw.this.d));
                }
            }, a2, "startScan", "BluetoothLeScanner");
        }
    }

    @SuppressLint({"MissingPermission"})
    public synchronized void a() {
        BluetoothLeScanner a2 = this.a.a();
        if (a2 != null) {
            dl.a((abz<T>) new abz<BluetoothLeScanner>() {
                public void a(@NonNull BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.stopScan(dw.this.d);
                }
            }, a2, "stopScan", "BluetoothLeScanner");
        }
    }

    @VisibleForTesting
    public dw(@NonNull dt dtVar, @NonNull ea eaVar, @NonNull dy dyVar, @NonNull PendingIntent pendingIntent) {
        this.a = dtVar;
        this.b = eaVar;
        this.c = dyVar;
        this.d = pendingIntent;
    }
}
