package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.mi.b;
import com.yandex.metrica.impl.ob.mi.d;
import com.yandex.metrica.impl.ob.mi.f;
import com.yandex.metrica.impl.ob.mi.g;
import java.util.HashMap;
import java.util.List;

public class lw {
    @NonNull
    private final HashMap<String, List<String>> a = new HashMap<>();

    public lw() {
        this.a.put("reports", f.a);
        this.a.put("sessions", g.a);
        this.a.put("preferences", d.a);
        this.a.put("binary_data", b.a);
    }

    @NonNull
    public HashMap<String, List<String>> a() {
        return this.a;
    }
}
