package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class kr extends kp {
    @NonNull
    private final km a;
    @Nullable
    private final du b;

    public kr(@NonNull Context context, @NonNull km kmVar, @Nullable du duVar) {
        super(context);
        this.a = kmVar;
        this.b = duVar;
    }

    public void a(@Nullable Bundle bundle, @Nullable kn knVar) {
        this.a.a();
        if (this.b != null) {
            this.b.a(a());
        }
        if (knVar != null) {
            knVar.a();
        }
    }
}
