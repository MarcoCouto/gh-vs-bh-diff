package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.ShareConstants;
import com.startapp.sdk.adsbase.model.AdPreferences;
import org.json.JSONArray;
import org.json.JSONObject;

public class aam {
    @NonNull
    public final String m;
    @NonNull
    public final String n;
    @Nullable
    public final c o;
    public final int p;
    public final boolean q;
    @NonNull
    public final d r;
    @NonNull
    public final a s;

    enum a {
        LIST("LIST"),
        LABEL("LABEL"),
        BUTTON("BUTTON"),
        CONTAINER("CONTAINER"),
        TOOLBAR("TOOLBAR"),
        INPUT("INPUT"),
        IMAGE(ShareConstants.IMAGE_URL),
        WEBVIEW("WEBVIEW"),
        OTHER("OTHER");
        
        /* access modifiers changed from: private */
        @NonNull
        public final String j;

        private a(String str) {
            this.j = str;
        }
    }

    enum b {
        TEXT(AdPreferences.TYPE_TEXT),
        HTML("HTML");
        
        @NonNull
        final String c;

        private b(String str) {
            this.c = str;
        }
    }

    enum c {
        TEXT_TOO_LONG("TEXT_TOO_LONG"),
        REGEXP_NOT_MATCHED("REGEXP_NOT_MATCHED"),
        IRRELEVANT_CLASS("IRRELEVANT_CLASS");
        
        /* access modifiers changed from: private */
        @NonNull
        public final String d;

        private c(String str) {
            this.d = str;
        }
    }

    enum d {
        VIEW_CONTAINER("VIEW_CONTAINER"),
        VIEW("VIEW");
        
        /* access modifiers changed from: private */
        @NonNull
        public final String c;

        private d(String str) {
            this.c = str;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public JSONArray a(@NonNull aah aah) {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return false;
    }

    public aam(@NonNull String str, @NonNull String str2, @Nullable c cVar, int i, boolean z, @NonNull d dVar, @NonNull a aVar) {
        this.m = str;
        this.n = str2;
        this.o = cVar;
        this.p = i;
        this.q = z;
        this.r = dVar;
        this.s = aVar;
    }

    @NonNull
    public JSONObject c(@NonNull aah aah) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("t", this.r.c);
            c b2 = b(aah);
            if (b2 == null) {
                jSONObject.put("cnt", a(aah));
            }
            if (aah.e) {
                JSONObject put = new JSONObject().put(UserDataStore.CITY, this.s.j).put("cn", this.m).put("rid", this.n).put("d", this.p).put("lc", this.q).put("if", b2 != null);
                if (b2 != null) {
                    put.put("fr", b2.d);
                }
                jSONObject.put("i", put);
            }
        } catch (Throwable unused) {
        }
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public c b(@NonNull aah aah) {
        return this.o;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UiElement{mClassName='");
        sb.append(this.m);
        sb.append('\'');
        sb.append(", mId='");
        sb.append(this.n);
        sb.append('\'');
        sb.append(", mFilterReason=");
        sb.append(this.o);
        sb.append(", mDepth=");
        sb.append(this.p);
        sb.append(", mListItem=");
        sb.append(this.q);
        sb.append(", mViewType=");
        sb.append(this.r);
        sb.append(", mClassType=");
        sb.append(this.s);
        sb.append('}');
        return sb.toString();
    }
}
