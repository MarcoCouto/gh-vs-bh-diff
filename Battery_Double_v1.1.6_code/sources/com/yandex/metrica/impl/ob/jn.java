package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface jn<A> {
    @Nullable
    jo a();

    @NonNull
    jo a(@NonNull A a);
}
