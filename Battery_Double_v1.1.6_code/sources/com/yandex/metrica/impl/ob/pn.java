package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class pn {
    @Nullable
    private ql a;
    @NonNull
    private final lz b;
    @NonNull
    private final ly c;

    public pn(@Nullable ql qlVar, @NonNull lz lzVar, @NonNull ly lyVar) {
        this.a = qlVar;
        this.b = lzVar;
        this.c = lyVar;
    }

    public void a() {
        if (this.a != null) {
            b(this.a);
            c(this.a);
        }
    }

    private void b(@NonNull ql qlVar) {
        if (this.b.a() > ((long) qlVar.f)) {
            this.b.c((int) (((float) qlVar.f) * 0.1f));
        }
    }

    private void c(@NonNull ql qlVar) {
        if (this.c.a() > ((long) qlVar.f)) {
            this.c.c((int) (((float) qlVar.f) * 0.1f));
        }
    }

    public void a(@Nullable ql qlVar) {
        this.a = qlVar;
    }
}
