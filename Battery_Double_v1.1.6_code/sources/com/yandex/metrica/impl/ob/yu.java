package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.yandex.metrica.impl.ob.abc.a;
import com.yandex.metrica.impl.ob.ve.a.i;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class yu {
    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        if (yxVar.C()) {
            JSONObject optJSONObject = aVar.optJSONObject("socket");
            if (optJSONObject != null) {
                long optLong = optJSONObject.optLong("seconds_to_live");
                long optLong2 = optJSONObject.optLong("first_delay_seconds", new i().e);
                int optInt = optJSONObject.optInt("launch_delay_seconds", new i().f);
                String optString = optJSONObject.optString(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY);
                JSONArray optJSONArray = optJSONObject.optJSONArray("ports");
                if (optLong > 0 && !TextUtils.isEmpty(optString) && optJSONArray != null && optJSONArray.length() > 0) {
                    ArrayList arrayList = new ArrayList(optJSONArray.length());
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        int optInt2 = optJSONArray.optInt(i);
                        if (optInt2 != 0) {
                            arrayList.add(Integer.valueOf(optInt2));
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        xs xsVar = new xs(optLong, optString, arrayList, optLong2, optInt);
                        yxVar.a(xsVar);
                    }
                }
            }
        }
    }
}
