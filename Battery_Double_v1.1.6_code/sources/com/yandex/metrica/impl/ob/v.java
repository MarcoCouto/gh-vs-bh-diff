package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class v {
    @Nullable
    private Long a;
    @NonNull
    private abs b;

    public v() {
        this(new abs());
    }

    public void a() {
        this.a = Long.valueOf(this.b.c());
    }

    @VisibleForTesting
    v(@NonNull abs abs) {
        this.b = abs;
    }
}
