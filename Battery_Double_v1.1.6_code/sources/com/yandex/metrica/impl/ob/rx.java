package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.uu.b.C0092b;

public class rx {
    @NonNull
    private final pr a;
    @NonNull
    private final rw b;

    public rx(@NonNull pr prVar, @NonNull rw rwVar) {
        this.a = prVar;
        this.b = rwVar;
    }

    @Nullable
    public C0092b a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            rb a2 = this.a.a(j, str);
            if (a2 != null) {
                return this.b.a(a2);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
