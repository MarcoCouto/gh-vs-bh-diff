package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.j;
import com.yandex.metrica.j.a;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ae implements bm {
    private Location a;
    private Boolean b;
    private Boolean c;
    private Boolean d;
    private Map<String, String> e = new LinkedHashMap();
    private Map<String, String> f = new LinkedHashMap();
    private boolean g;
    private boolean h;
    private cs i;

    private static boolean a(Object obj) {
        return obj == null;
    }

    public Location a() {
        return this.a;
    }

    public Boolean b() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = Boolean.valueOf(z);
        f();
    }

    public void b(boolean z) {
        this.c = Boolean.valueOf(z);
        f();
    }

    public Boolean c() {
        return this.d;
    }

    public void setStatisticsSending(boolean z) {
        this.d = Boolean.valueOf(z);
        f();
    }

    public void a(Location location) {
        this.a = location;
    }

    public boolean d() {
        return this.g;
    }

    private void e() {
        this.a = null;
        this.b = null;
        this.d = null;
        this.e.clear();
        this.f.clear();
        this.g = false;
    }

    public j a(j jVar) {
        if (this.h) {
            return jVar;
        }
        a b2 = b(jVar);
        a(jVar, b2);
        this.h = true;
        e();
        return b2.b();
    }

    private a b(j jVar) {
        a a2 = j.a(jVar.apiKey);
        a2.a(jVar.b, jVar.j);
        a2.c(jVar.a);
        a2.a(jVar.preloadInfo);
        a2.a(jVar.location);
        a2.a(jVar.m);
        a(a2, jVar);
        a(this.e, a2);
        a(jVar.i, a2);
        b(this.f, a2);
        b(jVar.h, a2);
        return a2;
    }

    private void a(@NonNull a aVar, @NonNull j jVar) {
        if (dl.a((Object) jVar.d)) {
            aVar.a(jVar.d);
        }
        if (dl.a((Object) jVar.appVersion)) {
            aVar.a(jVar.appVersion);
        }
        if (dl.a((Object) jVar.f)) {
            aVar.d(jVar.f.intValue());
        }
        if (dl.a((Object) jVar.e)) {
            aVar.b(jVar.e.intValue());
        }
        if (dl.a((Object) jVar.g)) {
            aVar.c(jVar.g.intValue());
        }
        if (dl.a((Object) jVar.logs) && jVar.logs.booleanValue()) {
            aVar.a();
        }
        if (dl.a((Object) jVar.sessionTimeout)) {
            aVar.a(jVar.sessionTimeout.intValue());
        }
        if (dl.a((Object) jVar.crashReporting)) {
            aVar.a(jVar.crashReporting.booleanValue());
        }
        if (dl.a((Object) jVar.nativeCrashReporting)) {
            aVar.b(jVar.nativeCrashReporting.booleanValue());
        }
        if (dl.a((Object) jVar.locationTracking)) {
            aVar.d(jVar.locationTracking.booleanValue());
        }
        if (dl.a((Object) jVar.installedAppCollecting)) {
            aVar.e(jVar.installedAppCollecting.booleanValue());
        }
        if (dl.a((Object) jVar.c)) {
            aVar.b(jVar.c);
        }
        if (dl.a((Object) jVar.firstActivationAsUpdate)) {
            aVar.g(jVar.firstActivationAsUpdate.booleanValue());
        }
        if (dl.a((Object) jVar.statisticsSending)) {
            aVar.f(jVar.statisticsSending.booleanValue());
        }
        if (dl.a((Object) jVar.l)) {
            aVar.c(jVar.l.booleanValue());
        }
        if (dl.a((Object) jVar.maxReportsInDatabaseCount)) {
            aVar.e(jVar.maxReportsInDatabaseCount.intValue());
        }
        if (dl.a((Object) jVar.n)) {
            aVar.a(jVar.n);
        }
    }

    private void a(@Nullable Map<String, String> map, @NonNull a aVar) {
        if (!dl.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                aVar.b((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    private void b(@Nullable Map<String, String> map, @NonNull a aVar) {
        if (!dl.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                aVar.a((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    private void a(j jVar, a aVar) {
        Boolean b2 = b();
        if (a((Object) jVar.locationTracking) && dl.a((Object) b2)) {
            aVar.d(b2.booleanValue());
        }
        Location a2 = a();
        if (a((Object) jVar.location) && dl.a((Object) a2)) {
            aVar.a(a2);
        }
        Boolean c2 = c();
        if (a((Object) jVar.statisticsSending) && dl.a((Object) c2)) {
            aVar.f(c2.booleanValue());
        }
    }

    public void a(cs csVar) {
        this.i = csVar;
    }

    private void f() {
        if (this.i != null) {
            this.i.a(this.b, this.d, this.c);
        }
    }
}
