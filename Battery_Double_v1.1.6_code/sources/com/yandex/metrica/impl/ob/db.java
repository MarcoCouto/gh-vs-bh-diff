package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.SparseArray;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

public class db extends bu {
    /* access modifiers changed from: private */
    public final mq a;
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;

    static class a implements a {
        @NonNull
        private final mx<Collection<sr>> a;
        @NonNull
        private final mx<yb> b;
        @NonNull
        private final os c;

        public a(@NonNull mx<Collection<sr>> mxVar, @NonNull mx<yb> mxVar2, @NonNull os osVar) {
            this.a = mxVar;
            this.b = mxVar2;
            this.c = osVar;
        }

        public void a(@NonNull Context context) {
            c(context);
            com.yandex.metrica.impl.ob.yb.a a2 = ((yb) this.b.a()).a();
            a(context, a2);
            a(a2);
            this.b.a(a2.a());
            b(context);
        }

        private void b(@NonNull Context context) {
            context.getSharedPreferences("com.yandex.metrica.configuration", 0).edit().clear().apply();
        }

        private void a(@NonNull com.yandex.metrica.impl.ob.yb.a aVar) {
            aVar.c(true);
        }

        private void a(@NonNull Context context, @NonNull com.yandex.metrica.impl.ob.yb.a aVar) {
            oq a2 = this.c.a(context);
            if (a2 != null) {
                aVar.b(a2.a).d(a2.b);
            }
        }

        private void c(@NonNull Context context) {
            ma d = lv.a(context).d();
            List a2 = d.a();
            if (a2 != null) {
                this.a.a(a2);
                d.b();
            }
        }
    }

    static class b implements a {
        @NonNull
        private mx a;
        @NonNull
        private mr b;

        public b(@NonNull mx mxVar, @NonNull mr mrVar) {
            this.a = mxVar;
            this.b = mrVar;
        }

        public void a(Context context) {
            this.a.a(this.b.a());
        }
    }

    static class c implements a {
        @NonNull
        private final mq a;
        @NonNull
        private final tc b;

        public c(@NonNull mq mqVar, @NonNull tc tcVar) {
            this.a = mqVar;
            this.b = tcVar;
        }

        public void a(Context context) {
            Boolean b2 = this.b.b();
            this.b.d().j();
            if (b2 != null) {
                this.a.a(b2.booleanValue()).q();
            }
        }
    }

    static class d implements a {
        @NonNull
        private final mx<Collection<sr>> a;
        @NonNull
        private final mx<sk> b;

        d(@NonNull mx<Collection<sr>> mxVar, @NonNull mx<sk> mxVar2) {
            this.a = mxVar;
            this.b = mxVar2;
        }

        public void a(Context context) {
            this.b.a(new sk(new ArrayList((Collection) this.a.a()), null, new ArrayList()));
        }
    }

    static class e implements a {
        @NonNull
        private final mx<yb> a;

        e(@NonNull mx<yb> mxVar) {
            this.a = mxVar;
        }

        public void a(Context context) {
            this.a.a(((yb) this.a.a()).a().c(true).a());
        }
    }

    static class f implements a {
        private tf a;
        private mr b;

        f(@NonNull Context context) {
            this.a = new tf(context);
            this.b = new mr(lv.a(context).e(), context.getPackageName());
        }

        public void a(Context context) {
            String a2 = this.a.a((String) null);
            if (!TextUtils.isEmpty(a2)) {
                this.b.b(a2).q();
                tf.a(context);
            }
        }
    }

    static class g implements a {
        g() {
        }

        public void a(Context context) {
            tc tcVar = new tc(context, context.getPackageName());
            SharedPreferences a = ti.a(context, "_boundentrypreferences");
            String string = a.getString(tc.d.a(), null);
            long j = a.getLong(tc.e.a(), -1);
            if (string != null && j != -1) {
                tcVar.a(new com.yandex.metrica.impl.ob.i.a(string, j)).j();
                a.edit().remove(tc.d.a()).remove(tc.e.a()).apply();
            }
        }
    }

    static class h implements a {
        h() {
        }

        public void a(Context context) {
            mq mqVar = new mq(lv.a(context).c());
            c(context, mqVar);
            b(context, mqVar);
            a(context, mqVar);
            mqVar.q();
            sx sxVar = new sx(context);
            sxVar.a();
            sxVar.b();
            b(context);
        }

        private void b(Context context) {
            new os().a(context, new oq((String) abw.b(new mr(lv.a(context).e(), context.getPackageName()).a().b, ""), null), new so(new sj()));
        }

        private void a(Context context, mq mqVar) {
            tc tcVar = new tc(context, new fs(context.getPackageName(), null).toString());
            Boolean b = tcVar.b();
            tcVar.d();
            if (b != null) {
                mqVar.a(b.booleanValue());
            }
            String b2 = tcVar.b((String) null);
            if (!TextUtils.isEmpty(b2)) {
                mqVar.a(b2);
            }
            tcVar.d().c().j();
        }

        private void b(Context context, mq mqVar) {
            te teVar = new te(context, context.getPackageName());
            long a = teVar.a(0);
            if (a != 0) {
                mqVar.a(a);
            }
            teVar.a();
        }

        private void c(Context context, mq mqVar) {
            tg tgVar = new tg(context);
            if (tgVar.a()) {
                mqVar.b(true);
                tgVar.b();
            }
        }
    }

    static class i implements a {
        i() {
        }

        public void a(Context context) {
            a(context, new mq(lv.a(context).c()));
        }

        private void a(Context context, mq mqVar) {
            int i = (new mr(lv.a(context).e(), context.getPackageName()).a().u > 0 ? 1 : (new mr(lv.a(context).e(), context.getPackageName()).a().u == 0 ? 0 : -1));
            boolean z = true;
            boolean z2 = i > 0;
            if (mqVar.c(-1) <= 0) {
                z = false;
            }
            if (z2 || z) {
                mqVar.d(false).q();
            }
        }
    }

    static class j implements a {
        j() {
        }

        public void a(Context context) {
            mr mrVar = new mr(lv.a(context).e(), context.getPackageName());
            String i = mrVar.i(null);
            if (i != null) {
                mrVar.a(Collections.singletonList(i));
            }
            String j = mrVar.j(null);
            if (j != null) {
                mrVar.b(Collections.singletonList(j));
            }
        }
    }

    static class k implements a {

        static class a implements FilenameFilter {
            final Iterable<FilenameFilter> a;

            a(Iterable<FilenameFilter> iterable) {
                this.a = iterable;
            }

            public boolean accept(File file, String str) {
                for (FilenameFilter accept : this.a) {
                    if (accept.accept(file, str)) {
                        return true;
                    }
                }
                return false;
            }
        }

        static class b implements FilenameFilter {
            private final FilenameFilter a;

            b(FilenameFilter filenameFilter) {
                this.a = filenameFilter;
            }

            public boolean accept(File file, String str) {
                if (str.startsWith("db_metrica_")) {
                    try {
                        return this.a.accept(file, k.a(str));
                    } catch (Throwable unused) {
                    }
                }
                return false;
            }
        }

        static class c implements FilenameFilter {
            c() {
            }

            public boolean accept(File file, String str) {
                return str.endsWith("null");
            }
        }

        static class d implements FilenameFilter {
            private final String a;

            d(@NonNull String str) {
                this.a = str;
            }

            public boolean accept(File file, String str) {
                return !str.contains(this.a);
            }
        }

        k() {
        }

        public void a(Context context) {
            b(context);
            d(context);
        }

        private void d(Context context) {
            new mr(lv.a(context).e(), context.getPackageName()).r(new th("LAST_STARTUP_CLIDS_SAVE_TIME").b()).q();
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull Context context) {
            File[] listFiles;
            File c2 = c(context);
            for (File file : c2.listFiles(new a(Arrays.asList(new FilenameFilter[]{new b(new d(context.getPackageName())), new b(new c())})))) {
                try {
                    if (!file.delete()) {
                        xa.a().reportEvent("Can not delete file", new JSONObject().put("fileName", file.getName()).toString());
                    }
                } catch (Throwable th) {
                    xa.a().reportError("Can not delete file", th);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public File c(@NonNull Context context) {
            if (dl.a(21)) {
                return context.getNoBackupFilesDir();
            }
            return new File(context.getFilesDir().getParentFile(), "databases");
        }

        @NonNull
        static String a(@NonNull String str) {
            return str.endsWith("-journal") ? str.replace("-journal", "") : str;
        }
    }

    static class l implements a {
        l() {
        }

        public void a(Context context) {
            mx a = com.yandex.metrica.impl.ob.op.a.a(yb.class).a(context);
            yb ybVar = (yb) a.a();
            a.a(ybVar.a().a(ybVar.u > 0).c(true).a());
        }
    }

    public db(@NonNull Context context) {
        this.b = context;
        this.a = new mq(lv.a(context).c());
    }

    /* access modifiers changed from: 0000 */
    public SparseArray<a> a() {
        return new SparseArray<a>() {
            {
                put(29, new f(db.this.b));
                put(39, new g());
                put(47, new h());
                put(60, new i());
                put(62, new j());
                put(66, new k());
                put(67, new b(com.yandex.metrica.impl.ob.op.a.a(yb.class).a(db.this.b), new mr(lv.a(db.this.b).e(), db.this.b.getPackageName())));
                put(68, new l());
                put(72, new a(com.yandex.metrica.impl.ob.op.a.b(sr.class).a(db.this.b), com.yandex.metrica.impl.ob.op.a.a(yb.class).a(db.this.b), new os()));
                put(73, new c(db.this.a, new tc(db.this.b, new fs(db.this.b.getPackageName(), null).toString())));
                put(82, new d(com.yandex.metrica.impl.ob.op.a.b(sr.class).a(db.this.b), com.yandex.metrica.impl.ob.op.a.a(sk.class).a(db.this.b)));
                put(87, new e(com.yandex.metrica.impl.ob.op.a.a(yb.class).a(db.this.b)));
            }
        };
    }

    /* access modifiers changed from: protected */
    public int a(td tdVar) {
        int a2 = tdVar.a();
        return a2 == -1 ? this.a.a(-1) : a2;
    }

    /* access modifiers changed from: protected */
    public void a(td tdVar, int i2) {
        this.a.b(i2).q();
        tdVar.b().j();
    }
}
