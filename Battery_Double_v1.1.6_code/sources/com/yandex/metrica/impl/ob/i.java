package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;
import java.util.Map;

public class i {
    private abj a;
    private long b;
    private boolean c;
    @NonNull
    private final adj d;

    public static final class a {
        public final String a;
        public final long b;

        public a(String str, long j) {
            this.a = str;
            this.b = j;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b != aVar.b) {
                return false;
            }
            if (this.a == null ? aVar.a != null : !this.a.equals(aVar.a)) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return ((this.a != null ? this.a.hashCode() : 0) * 31) + ((int) (this.b ^ (this.b >>> 32)));
        }
    }

    public i(String str, long j, @NonNull abl abl) {
        this(str, j, new adj(abl, "[App Environment]"));
    }

    @VisibleForTesting
    i(String str, long j, @NonNull adj adj) {
        this.b = j;
        try {
            this.a = new abj(str);
        } catch (Throwable unused) {
            this.a = new abj();
        }
        this.d = adj;
    }

    public synchronized void a() {
        this.a = new abj();
    }

    public synchronized void a(@NonNull Pair<String, String> pair) {
        if (this.d.a(this.a, (String) pair.first, (String) pair.second)) {
            this.c = true;
        }
    }

    public synchronized a b() {
        if (this.c) {
            this.b++;
            this.c = false;
        }
        return new a(abc.b((Map) this.a), this.b);
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder("Map size ");
        sb.append(this.a.size());
        sb.append(". Is changed ");
        sb.append(this.c);
        sb.append(". Current revision ");
        sb.append(this.b);
        return sb.toString();
    }
}
