package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class kq extends kp {
    @NonNull
    private final km a;

    public kq(@NonNull Context context, @NonNull km kmVar) {
        super(context);
        this.a = kmVar;
    }

    public void a(@Nullable Bundle bundle, @Nullable kn knVar) {
        this.a.a(knVar);
    }
}
