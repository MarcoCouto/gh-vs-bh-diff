package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public abstract class pl {
    @NonNull
    protected final mq a;
    @NonNull
    protected final qw b;
    @Nullable
    protected final pt c;
    @NonNull
    protected final cf d;
    @NonNull
    private final qe e = a();
    @NonNull
    private final pi f = new pi(this.c, this.e, b());
    @NonNull
    private final pj g = new pj(this.b.a.b, this.c);

    /* access modifiers changed from: protected */
    @NonNull
    public abstract qe a();

    /* access modifiers changed from: protected */
    @NonNull
    public abstract sp a(@NonNull so soVar);

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String b();

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String c();

    public pl(@NonNull qw qwVar, @NonNull mq mqVar, @Nullable pt ptVar, @NonNull cf cfVar) {
        this.b = qwVar;
        this.a = mqVar;
        this.c = ptVar;
        this.d = cfVar;
    }

    @NonNull
    private qx b(@NonNull rj rjVar) {
        rn rnVar = new rn(this.b.a.a, this.b.a.b.b(), this.b.c, rjVar, a(this.b.a.c), c());
        return rnVar;
    }

    @NonNull
    public qy a(@NonNull rj rjVar) {
        return new qy(b(rjVar), this.f, new pk(this.e), this.g);
    }

    @NonNull
    public List<qn> d() {
        return Arrays.asList(new qn[]{this.f, this.g});
    }
}
