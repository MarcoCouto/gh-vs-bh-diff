package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Pair;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ac.NativeCrashesHelper;
import com.yandex.metrica.impl.ob.al.a;
import com.yandex.metrica.impl.ob.ct.c;
import com.yandex.metrica.impl.ob.ct.d;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class cs implements az {
    private final Context a;
    private bn b;
    private final NativeCrashesHelper c;
    private bj d;
    @NonNull
    private final w e;
    private xx f;
    private final ll g;
    @NonNull
    private final lf h;
    private final ct i;

    cs(eu euVar, Context context, act act) {
        this(euVar, context, new NativeCrashesHelper(context), new bn(context, act), new ll(), new lf());
    }

    @VisibleForTesting
    cs(eu euVar, Context context, @NonNull NativeCrashesHelper nativeCrashesHelper, @NonNull bn bnVar, @NonNull ll llVar, @NonNull lf lfVar) {
        this.b = bnVar;
        this.a = context;
        this.c = nativeCrashesHelper;
        this.e = new w(euVar);
        this.g = llVar;
        this.h = lfVar;
        this.i = new ct(this);
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable bj bjVar) {
        this.d = bjVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(xx xxVar) {
        this.f = xxVar;
        this.e.b(xxVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z, co coVar) {
        this.c.a(z);
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Boolean bool, @Nullable Boolean bool2, @Nullable Boolean bool3) {
        if (dl.a((Object) bool)) {
            this.e.h().a(bool.booleanValue());
        }
        if (dl.a((Object) bool2)) {
            this.e.h().h(bool2.booleanValue());
        }
        if (dl.a((Object) bool3)) {
            this.e.h().b(bool3.booleanValue());
        }
        a(aa.t(), (co) this.e);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, co coVar) {
        a(al.a(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, str, c(coVar)), coVar);
    }

    public void c() {
        this.b.g();
    }

    public void d() {
        this.b.h();
    }

    /* access modifiers changed from: private */
    public aa b(aa aaVar, co coVar) {
        if (aaVar.g() == a.EVENT_TYPE_EXCEPTION_USER.a() || aaVar.g() == a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.a()) {
            aaVar.e(coVar.e());
        }
        return aaVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(aa aaVar, co coVar) {
        a(b(aaVar, coVar), coVar, null);
    }

    public Future<Void> a(aa aaVar, final co coVar, final Map<String, Object> map) {
        this.b.d();
        d dVar = new d(aaVar, coVar);
        if (!dl.a((Map) map)) {
            dVar.a((c) new c() {
                public aa a(aa aaVar) {
                    return cs.this.b(aaVar.c(abc.b(map)), coVar);
                }
            });
        }
        return a(dVar);
    }

    public Future<Void> a(@NonNull eu euVar) {
        return this.i.a(euVar);
    }

    public Future<Void> b(@NonNull eu euVar) {
        return this.i.b(euVar);
    }

    public void a(@NonNull List<String> list, @NonNull ResultReceiver resultReceiver, @Nullable Map<String, String> map) {
        a(al.a(a.EVENT_TYPE_STARTUP, abd.a()).a(new ba(list, map, resultReceiver)), (co) this.e);
    }

    public void a(String str) {
        a(al.f(str, abd.a()), (co) this.e);
    }

    public void a(@Nullable vu vuVar) {
        a(al.a(vuVar, abd.a()), (co) this.e);
    }

    public void a(co coVar) {
        a(al.a(coVar.f(), c(coVar)), coVar);
    }

    public void a(List<String> list) {
        this.e.g().a(list);
    }

    public void a(Map<String, String> map) {
        this.e.g().a(map);
    }

    public void b(String str) {
        this.e.g().a(str);
    }

    public void c(String str) {
        this.e.g().b(str);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull kz kzVar, @NonNull co coVar) {
        a(al.a(e.a((e) this.h.b(kzVar)), c(coVar)), coVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, @NonNull ld ldVar, @NonNull co coVar) {
        a(al.a(str, e.a((e) this.g.b(ldVar)), c(coVar)), coVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ld ldVar, co coVar) {
        this.b.d();
        t b2 = al.b(ldVar.a, e.a((e) this.g.b(ldVar)), c(coVar));
        b2.e(coVar.e());
        try {
            a(new d(b2, coVar).a(b2.a()).a(true)).get();
        } catch (InterruptedException | ExecutionException unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar) {
        this.b.d();
    }

    /* access modifiers changed from: 0000 */
    public void b(o oVar) {
        this.b.c();
    }

    public void a(IMetricaService iMetricaService, aa aaVar, co coVar) throws RemoteException {
        b(iMetricaService, aaVar, coVar);
        e();
    }

    public void a(@NonNull IMetricaService iMetricaService, @NonNull eu euVar) throws RemoteException {
        iMetricaService.b(c(euVar));
    }

    public void b(@NonNull IMetricaService iMetricaService, @NonNull eu euVar) throws RemoteException {
        iMetricaService.c(c(euVar));
    }

    @NonNull
    private Bundle c(@NonNull eu euVar) {
        Bundle bundle = new Bundle();
        euVar.b(bundle);
        return bundle;
    }

    public void a(String str, String str2, co coVar) {
        a(new d(t.a(str, str2), coVar));
    }

    public void b(co coVar) {
        a(new d(t.b(), coVar));
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull final uy.a aVar, @NonNull co coVar) {
        a(new d(t.c(), coVar).a((c) new c() {
            public aa a(aa aaVar) {
                return aaVar.c(new String(Base64.encode(e.a((e) aVar), 0)));
            }
        }));
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable final String str, @NonNull co coVar) {
        a(new d(t.a(str, c(coVar)), coVar).a((c) new c() {
            public aa a(aa aaVar) {
                return aaVar.c(str);
            }
        }));
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull final cv cvVar, @NonNull co coVar) {
        a(new d(t.a(c(coVar)), coVar).a((c) new c() {
            public aa a(aa aaVar) {
                Pair a2 = cvVar.a();
                return aaVar.c(new String(Base64.encode((byte[]) a2.first, 0))).c(((Integer) a2.second).intValue());
            }
        }));
    }

    private void e() {
        if (this.d == null || this.d.f()) {
            this.b.c();
        }
    }

    private static void b(IMetricaService iMetricaService, aa aaVar, co coVar) throws RemoteException {
        iMetricaService.a(aaVar.a(coVar.b()));
    }

    private Future<Void> a(d dVar) {
        dVar.a().a(this.f);
        return this.i.a(dVar);
    }

    public bn a() {
        return this.b;
    }

    public Context b() {
        return this.a;
    }

    @NonNull
    private abl c(@NonNull co coVar) {
        return abd.a(coVar.h().e());
    }
}
