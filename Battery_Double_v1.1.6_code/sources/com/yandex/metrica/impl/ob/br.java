package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.net.Uri;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class br implements bs {
    private final acb<String, Integer> a = new acb<>();
    private final Map<b, a> b = new LinkedHashMap();
    private final Map<b, a> c = new LinkedHashMap();

    interface a {
        boolean a(@NonNull Intent intent, @NonNull br brVar);
    }

    interface b {
        void a();
    }

    public void a() {
    }

    public void a(Intent intent, int i) {
    }

    public void a(Intent intent, int i, int i2) {
    }

    public void b() {
    }

    public void a(Intent intent) {
        if (intent != null) {
            d(intent);
        }
    }

    public void b(Intent intent) {
        if (intent != null) {
            d(intent);
        }
    }

    private void d(@NonNull Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            this.a.a(action, Integer.valueOf(h(intent)));
        }
        a(intent, this.b);
    }

    private void a(@NonNull Intent intent, @NonNull Map<b, a> map) {
        for (Entry entry : map.entrySet()) {
            if (((a) entry.getValue()).a(intent, this)) {
                ((b) entry.getKey()).a();
            }
        }
    }

    public void c(Intent intent) {
        if (intent != null) {
            e(intent);
        }
    }

    private void e(@NonNull Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            this.a.b(action, Integer.valueOf(h(intent)));
        }
        a(intent, this.c);
    }

    public void a(@NonNull b bVar) {
        this.b.put(bVar, c());
    }

    public void b(@NonNull b bVar) {
        this.c.put(bVar, c());
    }

    public void c(@NonNull b bVar) {
        this.b.put(bVar, new a() {
            public boolean a(@NonNull Intent intent, @NonNull br brVar) {
                return br.this.f(intent);
            }
        });
    }

    public void d(@NonNull b bVar) {
        this.b.put(bVar, new a() {
            public boolean a(@NonNull Intent intent, @NonNull br brVar) {
                return br.this.g(intent);
            }
        });
    }

    public void e(@NonNull b bVar) {
        this.c.put(bVar, new a() {
            public boolean a(@NonNull Intent intent, @NonNull br brVar) {
                return br.this.g(intent) && br.this.f();
            }
        });
    }

    @NonNull
    private a c() {
        return new a() {
            public boolean a(@NonNull Intent intent, @NonNull br brVar) {
                return br.this.a(intent.getAction());
            }
        };
    }

    /* access modifiers changed from: private */
    public boolean a(@Nullable String str) {
        return "com.yandex.metrica.ACTION_C_BG_L".equals(str);
    }

    /* access modifiers changed from: private */
    public boolean f(@NonNull Intent intent) {
        return g(intent) && d();
    }

    private boolean b(@Nullable String str) {
        return "com.yandex.metrica.IMetricaService".equals(str);
    }

    /* access modifiers changed from: private */
    public boolean g(@NonNull Intent intent) {
        if (b(intent.getAction())) {
            return !a(h(intent));
        }
        return false;
    }

    private boolean d() {
        return e() == 1;
    }

    private int e() {
        Collection<Integer> g = g();
        int i = 0;
        if (!dl.a((Collection) g)) {
            for (Integer intValue : g) {
                if (!a(intValue.intValue())) {
                    i++;
                }
            }
        }
        return i;
    }

    /* access modifiers changed from: private */
    public boolean f() {
        return e() == 0;
    }

    private Collection<Integer> g() {
        return this.a.a("com.yandex.metrica.IMetricaService");
    }

    private boolean a(int i) {
        return i == Process.myPid();
    }

    private int h(@NonNull Intent intent) {
        Uri data = intent.getData();
        if (data != null && data.getPath().equals("/client")) {
            try {
                return Integer.parseInt(data.getQueryParameter("pid"));
            } catch (Throwable unused) {
            }
        }
        return -1;
    }
}
