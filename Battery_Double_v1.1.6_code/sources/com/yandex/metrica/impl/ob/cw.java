package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.integralads.avid.library.inmobi.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class cw {
    @VisibleForTesting
    static final Map<String, Integer> a = new HashMap();

    static {
        a.put("1.00", Integer.valueOf(1));
        a.put("1.10", Integer.valueOf(2));
        a.put("1.11", Integer.valueOf(3));
        a.put("1.20", Integer.valueOf(4));
        a.put("1.21", Integer.valueOf(5));
        a.put("1.22", Integer.valueOf(6));
        a.put("1.23", Integer.valueOf(7));
        a.put("1.24", Integer.valueOf(8));
        a.put("1.26", Integer.valueOf(9));
        a.put("1.27", Integer.valueOf(10));
        a.put("1.40", Integer.valueOf(11));
        a.put("1.41", Integer.valueOf(12));
        a.put("1.42", Integer.valueOf(13));
        a.put("1.50", Integer.valueOf(14));
        a.put("1.51", Integer.valueOf(15));
        a.put("1.60", Integer.valueOf(16));
        a.put("1.61", Integer.valueOf(17));
        a.put("1.62", Integer.valueOf(18));
        a.put("1.63", Integer.valueOf(19));
        a.put("1.64", Integer.valueOf(20));
        a.put("1.65", Integer.valueOf(21));
        a.put("1.66", Integer.valueOf(22));
        a.put("1.67", Integer.valueOf(23));
        a.put("1.68", Integer.valueOf(24));
        a.put("1.69", Integer.valueOf(25));
        a.put("1.70", Integer.valueOf(26));
        a.put("1.71", Integer.valueOf(27));
        a.put("1.72", Integer.valueOf(28));
        a.put("1.80", Integer.valueOf(29));
        a.put("1.81", Integer.valueOf(30));
        a.put("1.82", Integer.valueOf(31));
        a.put("2.00", Integer.valueOf(32));
        a.put("2.10", Integer.valueOf(33));
        a.put("2.11", Integer.valueOf(34));
        a.put("2.20", Integer.valueOf(35));
        a.put("2.21", Integer.valueOf(36));
        a.put("2.22", Integer.valueOf(37));
        a.put("2.23", Integer.valueOf(38));
        a.put("2.30", Integer.valueOf(39));
        a.put("2.31", Integer.valueOf(40));
        a.put("2.32", Integer.valueOf(41));
        a.put("2.33", Integer.valueOf(42));
        a.put("2.40", Integer.valueOf(43));
        a.put("2.41", Integer.valueOf(44));
        a.put("2.42", Integer.valueOf(45));
        a.put("2.43", Integer.valueOf(46));
        a.put("2.50", Integer.valueOf(47));
        a.put("2.51", Integer.valueOf(48));
        a.put("2.52", Integer.valueOf(49));
        a.put("2.60", Integer.valueOf(50));
        a.put("2.61", Integer.valueOf(51));
        a.put("2.62", Integer.valueOf(52));
        a.put("2.63", Integer.valueOf(53));
        a.put("2.64", Integer.valueOf(54));
        a.put("2.70", Integer.valueOf(55));
        a.put("2.71", Integer.valueOf(56));
        a.put("2.72", Integer.valueOf(57));
        a.put("2.73", Integer.valueOf(58));
        a.put("2.74", Integer.valueOf(59));
        a.put("2.75", Integer.valueOf(60));
        a.put("2.76", Integer.valueOf(61));
        a.put("2.77", Integer.valueOf(62));
        a.put("2.78", Integer.valueOf(63));
        a.put("2.80", Integer.valueOf(64));
        a.put("2.81-RC1", Integer.valueOf(65));
        a.put("3.0.0", Integer.valueOf(66));
        a.put("3.1.0", Integer.valueOf(67));
        a.put("3.2.0", Integer.valueOf(68));
        a.put("3.2.1", Integer.valueOf(69));
        a.put("3.2.2", Integer.valueOf(70));
        a.put("3.3.0", Integer.valueOf(71));
        a.put("3.4.0", Integer.valueOf(72));
        a.put("3.5.0", Integer.valueOf(73));
        a.put("3.5.1", Integer.valueOf(74));
        a.put("3.5.2", Integer.valueOf(75));
        a.put("3.5.3", Integer.valueOf(76));
        a.put("3.6.0", Integer.valueOf(77));
        a.put("3.6.1", Integer.valueOf(78));
        a.put("3.6.2", Integer.valueOf(79));
        a.put("3.6.3", Integer.valueOf(80));
        a.put(BuildConfig.VERSION_NAME, Integer.valueOf(81));
        a.put("3.7.0", Integer.valueOf(82));
        a.put("3.7.1", Integer.valueOf(83));
        a.put("3.7.2", Integer.valueOf(84));
        a.put("3.8.0", Integer.valueOf(85));
        a.put("3.8.1", Integer.valueOf(85));
        a.put("3.9.0", Integer.valueOf(86));
        a.put("3.9.1", Integer.valueOf(86));
        a.put("3.9.2", Integer.valueOf(86));
        a.put("3.10.0", Integer.valueOf(87));
        a.put("3.11.0-RC1", Integer.valueOf(87));
        a.put("3.11.0", Integer.valueOf(87));
        a.put("3.12.0-RC1", Integer.valueOf(87));
        a.put("3.12.0", Integer.valueOf(87));
        a.put("3.13.0", Integer.valueOf(87));
        a.put("3.13.1", Integer.valueOf(87));
    }

    @NonNull
    static String a(int i) {
        String str = "unknown";
        ArrayList arrayList = new ArrayList();
        for (Entry entry : a.entrySet()) {
            if (((Integer) entry.getValue()).intValue() == i) {
                arrayList.add(entry.getKey());
            }
        }
        if (arrayList.size() == 1) {
            return (String) arrayList.get(0);
        }
        if (arrayList.size() == 0) {
            return str;
        }
        return String.format("One of %s", new Object[]{arrayList});
    }
}
