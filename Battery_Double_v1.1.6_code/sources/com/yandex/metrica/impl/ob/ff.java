package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.we.c;
import com.yandex.metrica.impl.ob.we.d;
import java.util.List;

class ff {
    @NonNull
    protected final Context a;
    @NonNull
    private final a b;
    @NonNull
    private final b c;
    @NonNull
    private final fb d;
    @NonNull
    private final com.yandex.metrica.impl.ob.ew.a e;
    @NonNull
    private final ye f;
    @NonNull
    private final yb g;
    @NonNull
    private final d h;
    @NonNull
    private final ack i;
    @NonNull
    private final act j;
    private final int k;

    static class a {
        @Nullable
        private final String a;

        a(@Nullable String str) {
            this.a = str;
        }

        /* access modifiers changed from: 0000 */
        public abl a() {
            return abd.a(this.a);
        }

        /* access modifiers changed from: 0000 */
        public abb b() {
            return abd.b(this.a);
        }
    }

    static class b {
        @NonNull
        private final fb a;
        @NonNull
        private final lv b;

        b(@NonNull Context context, @NonNull fb fbVar) {
            this(fbVar, lv.a(context));
        }

        @VisibleForTesting
        b(@NonNull fb fbVar, @NonNull lv lvVar) {
            this.a = fbVar;
            this.b = lvVar;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public mm a() {
            return new mm(this.b.b(this.a));
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public mo b() {
            return new mo(this.b.b(this.a));
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public mq c() {
            return new mq(this.b.c());
        }
    }

    ff(@NonNull Context context, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.ew.a aVar, @NonNull ye yeVar, @NonNull yb ybVar, @NonNull d dVar, @NonNull act act, int i2) {
        com.yandex.metrica.impl.ob.ew.a aVar2 = aVar;
        this(context, fbVar, aVar2, yeVar, ybVar, dVar, act, new ack(), i2, new a(aVar2.d), new b(context, fbVar));
    }

    @VisibleForTesting
    ff(@NonNull Context context, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.ew.a aVar, @NonNull ye yeVar, @NonNull yb ybVar, @NonNull d dVar, @NonNull act act, @NonNull ack ack, int i2, @NonNull a aVar2, @NonNull b bVar) {
        this.a = context;
        this.d = fbVar;
        this.e = aVar;
        this.f = yeVar;
        this.g = ybVar;
        this.h = dVar;
        this.j = act;
        this.i = ack;
        this.k = i2;
        this.b = aVar2;
        this.c = bVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public a a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public b b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public jf c() {
        return new jf(this.a, this.d, this.k);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public lr a(@NonNull fe feVar) {
        return new lr(feVar, lv.a(this.a).a(this.d));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public cr<fe> b(@NonNull fe feVar) {
        return new cr<>(feVar, this.f.a(), this.j);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public fv c(@NonNull fe feVar) {
        return new fv(new c(feVar, this.h), this.g, new com.yandex.metrica.impl.ob.we.a(this.e));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public ju a(@NonNull fe feVar, @NonNull mo moVar, @NonNull com.yandex.metrica.impl.ob.ju.a aVar) {
        return new ju(feVar, new jt(moVar), aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public fw a(@NonNull mo moVar, @NonNull ju juVar, @NonNull lr lrVar, @NonNull i iVar, @NonNull final cr crVar) {
        fw fwVar = new fw(moVar, juVar, lrVar, iVar, this.i, this.k, new com.yandex.metrica.impl.ob.fw.a() {
            public void a() {
                crVar.b();
            }
        });
        return fwVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public hg d(@NonNull fe feVar) {
        return new hg(feVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public hj<hu, fe> a(@NonNull fe feVar, @NonNull hg hgVar) {
        return new hj<>(hgVar, feVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public com.yandex.metrica.impl.ob.fd.a e(@NonNull fe feVar) {
        return new com.yandex.metrica.impl.ob.fd.a(feVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public ou a(@NonNull lr lrVar) {
        return new ou(lrVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public oz a(@NonNull lr lrVar, @NonNull fv fvVar) {
        return new oz(lrVar, fvVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public ox a(@NonNull List<ov> list, @NonNull oy oyVar) {
        return new ox(list, oyVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public s a(@NonNull mo moVar) {
        return new s(this.a, moVar);
    }
}
