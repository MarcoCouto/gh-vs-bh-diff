package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public final class uf {
    @NonNull
    private final adw<String> a;
    @NonNull
    private final tz b;
    @NonNull
    private final String c;

    public uf(@NonNull String str, @NonNull adw<String> adw, @NonNull tz tzVar) {
        this.c = str;
        this.a = adw;
        this.b = tzVar;
    }

    @NonNull
    public String a() {
        return this.c;
    }

    @NonNull
    public tz b() {
        return this.b;
    }

    @NonNull
    public adw<String> c() {
        return this.a;
    }
}
