package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a.m.C0110a;
import java.util.ArrayList;
import java.util.List;

public class no implements nq<qd, C0110a> {
    @NonNull
    /* renamed from: a */
    public C0110a[] b(@NonNull List<qd> list) {
        C0110a[] aVarArr = new C0110a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVarArr[i] = a((qd) list.get(i));
        }
        return aVarArr;
    }

    @NonNull
    public List<qd> a(@NonNull C0110a[] aVarArr) {
        ArrayList arrayList = new ArrayList();
        for (C0110a a : aVarArr) {
            arrayList.add(a(a));
        }
        return arrayList;
    }

    @NonNull
    private C0110a a(@NonNull qd qdVar) {
        C0110a aVar = new C0110a();
        aVar.b = qdVar.a;
        aVar.c = qdVar.b;
        return aVar;
    }

    @NonNull
    private qd a(@NonNull C0110a aVar) {
        return new qd(aVar.b, aVar.c);
    }
}
