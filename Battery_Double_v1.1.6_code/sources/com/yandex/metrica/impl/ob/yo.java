package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.applovin.sdk.AppLovinMediationProvider;
import com.yandex.metrica.impl.ob.abc.a;
import com.yandex.metrica.impl.ob.ve.a.m;
import com.yandex.metrica.impl.ob.ve.a.m.C0110a;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class yo {
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        if (yxVar.a().j) {
            yxVar.a(new nu().a(a(aVar)));
        }
    }

    @NonNull
    private m a(@NonNull a aVar) {
        JSONArray jSONArray;
        m mVar = new m();
        JSONObject optJSONObject = aVar.optJSONObject("wakeup");
        if (optJSONObject == null) {
            return mVar;
        }
        mVar.b = abw.a(abc.a(optJSONObject, "collection_duration_seconds"), TimeUnit.SECONDS, mVar.b);
        mVar.c = abc.a(optJSONObject, "aggressive_relaunch", mVar.c);
        if (optJSONObject == null) {
            jSONArray = null;
        } else {
            jSONArray = optJSONObject.optJSONArray("collection_interval_ranges_seconds");
        }
        mVar.d = a(jSONArray, mVar.d);
        return mVar;
    }

    private C0110a[] a(@Nullable JSONArray jSONArray, C0110a[] aVarArr) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return aVarArr;
        }
        try {
            C0110a[] aVarArr2 = new C0110a[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    aVarArr2[i] = new C0110a();
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    aVarArr2[i].b = TimeUnit.SECONDS.toMillis(jSONObject.getLong("min"));
                    aVarArr2[i].c = TimeUnit.SECONDS.toMillis(jSONObject.getLong(AppLovinMediationProvider.MAX));
                    i++;
                } catch (Throwable unused) {
                }
            }
            return aVarArr2;
        } catch (Throwable unused2) {
            return aVarArr;
        }
    }
}
