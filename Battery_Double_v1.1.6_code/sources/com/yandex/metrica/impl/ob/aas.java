package com.yandex.metrica.impl.ob;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.widget.TextView;

public class aas {
    public int a(@NonNull TextView textView) {
        int i;
        if (TextUtils.isEmpty(textView.getText()) || textView.getVisibility() != 0) {
            return 0;
        }
        String charSequence = textView.getText().toString();
        Layout layout = textView.getLayout();
        if (layout == null) {
            return charSequence.length();
        }
        if (VERSION.SDK_INT >= 16) {
            i = textView.getMaxLines() - 1;
        } else {
            i = textView.getLineCount() - 1;
        }
        TruncateAt ellipsize = textView.getEllipsize();
        if (ellipsize == null) {
            return charSequence.length();
        }
        if (i != 0 && ellipsize != TruncateAt.END) {
            return charSequence.length();
        }
        return charSequence.length() - layout.getEllipsisCount(i);
    }
}
