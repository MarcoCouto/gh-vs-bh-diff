package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class dg extends cu<String> {
    public dg(@NonNull Context context, @NonNull String str) {
        super(context, str, "string");
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public String b(int i) {
        return this.a.getString(i);
    }
}
