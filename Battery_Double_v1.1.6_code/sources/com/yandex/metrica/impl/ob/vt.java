package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class vt {
    private static final EnumSet<b> a = EnumSet.of(b.HAS_FROM_PLAY_SERVICES, b.HAS_FROM_RECEIVER_ONLY, b.RECEIVER);
    private static final EnumSet<b> b = EnumSet.of(b.HAS_FROM_PLAY_SERVICES, b.HAS_FROM_RECEIVER_ONLY);
    private final Set<a> c;
    @Nullable
    private vu d;
    @Nullable
    private vu e;
    private boolean f;
    @NonNull
    private final mq g;
    private b h;

    public interface a {
        boolean a(@NonNull vu vuVar, @NonNull bd bdVar);
    }

    private enum b {
        EMPTY,
        RECEIVER,
        WAIT_FOR_RECEIVER_ONLY,
        HAS_FROM_PLAY_SERVICES,
        HAS_FROM_RECEIVER_ONLY
    }

    @WorkerThread
    public vt(@NonNull Context context) {
        this(new mq(lv.a(context).c()));
    }

    @VisibleForTesting
    vt(@NonNull mq mqVar) {
        this.c = new HashSet();
        this.h = b.EMPTY;
        this.g = mqVar;
        this.f = this.g.d();
        if (!this.f) {
            String b2 = this.g.b();
            if (!TextUtils.isEmpty(b2)) {
                vu vuVar = new vu(b2, 0, 0);
                this.d = vuVar;
            }
            this.e = this.g.c();
            this.h = b.values()[this.g.d(0)];
        }
    }

    public synchronized void a(@Nullable vu vuVar) {
        if (!b.contains(this.h)) {
            this.e = vuVar;
            this.g.a(vuVar).q();
            a(b(vuVar));
        }
    }

    public synchronized void a(@Nullable String str) {
        if (!a.contains(this.h) && !TextUtils.isEmpty(str)) {
            vu vuVar = new vu(str, 0, 0);
            this.d = vuVar;
            this.g.a(str).q();
            a(a());
        }
    }

    public synchronized void a(@NonNull a aVar) {
        if (!this.f) {
            this.c.add(aVar);
            b();
        }
    }

    private b b(vu vuVar) {
        switch (this.h) {
            case EMPTY:
                return vuVar == null ? b.WAIT_FOR_RECEIVER_ONLY : b.HAS_FROM_PLAY_SERVICES;
            case RECEIVER:
                return vuVar == null ? b.HAS_FROM_RECEIVER_ONLY : b.HAS_FROM_PLAY_SERVICES;
            default:
                return this.h;
        }
    }

    private b a() {
        int i = AnonymousClass1.a[this.h.ordinal()];
        if (i == 1) {
            return b.RECEIVER;
        }
        if (i != 3) {
            return this.h;
        }
        return b.HAS_FROM_RECEIVER_ONLY;
    }

    private void a(@NonNull b bVar) {
        if (bVar != this.h) {
            this.h = bVar;
            this.g.e(this.h.ordinal()).q();
            b();
        }
    }

    private void b() {
        switch (this.h) {
            case HAS_FROM_PLAY_SERVICES:
                a(this.e, bd.GPL);
                return;
            case HAS_FROM_RECEIVER_ONLY:
                a(this.d, bd.BROADCAST);
                return;
            default:
                return;
        }
    }

    private synchronized void a(@Nullable vu vuVar, @NonNull bd bdVar) {
        if (vuVar != null) {
            if (!this.c.isEmpty() && !this.f) {
                boolean z = false;
                for (a a2 : this.c) {
                    if (a2.a(vuVar, bdVar)) {
                        z = true;
                    }
                }
                if (z) {
                    c();
                    this.c.clear();
                }
            }
        }
    }

    private void c() {
        this.f = true;
        this.g.e().f().q();
    }
}
