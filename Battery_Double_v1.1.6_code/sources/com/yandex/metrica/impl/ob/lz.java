package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class lz extends lo {
    @NonNull
    public String e() {
        return "l_dat";
    }

    lz(@NonNull Context context, @NonNull lu luVar) {
        this(luVar, new mq(lv.a(context).c()));
    }

    @VisibleForTesting
    lz(@NonNull lu luVar, @NonNull mq mqVar) {
        super(luVar, mqVar);
    }

    /* access modifiers changed from: protected */
    public long c(long j) {
        return c().d(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public mq d(long j) {
        return c().e(j);
    }
}
