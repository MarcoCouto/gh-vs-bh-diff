package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class qc extends pl {
    /* access modifiers changed from: protected */
    @NonNull
    public String b() {
        return "gps";
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String c() {
        return "gps";
    }

    public qc(@NonNull qw qwVar, @NonNull mq mqVar, @Nullable pt ptVar) {
        this(qwVar, mqVar, ptVar, new cf());
    }

    @VisibleForTesting
    qc(@NonNull qw qwVar, @NonNull mq mqVar, @Nullable pt ptVar, @NonNull cf cfVar) {
        super(qwVar, mqVar, ptVar, cfVar);
    }

    @NonNull
    public qe a() {
        return new qe() {
            public long a() {
                return qc.this.a.n(0);
            }

            public void a(long j) {
                qc.this.a.o(j);
            }
        };
    }

    /* access modifiers changed from: protected */
    @NonNull
    public sp a(@NonNull so soVar) {
        return this.d.c(soVar);
    }
}
