package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.IParamsCallback;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class xz {
    private final Map<String, String> a = new HashMap();
    private List<String> b;
    private Map<String, String> c;
    private long d;
    private boolean e;
    @Nullable
    private aag f;
    @NonNull
    private final List<dk> g = new ArrayList();
    private final mn h;

    public xz(mn mnVar) {
        this.h = mnVar;
        Map<String, String> map = null;
        a("yandex_mobile_metrica_device_id", this.h.b((String) null));
        a("appmetrica_device_id_hash", this.h.c((String) null));
        a("yandex_mobile_metrica_uuid", this.h.a((String) null));
        a("yandex_mobile_metrica_get_ad_url", this.h.d((String) null));
        a("yandex_mobile_metrica_report_ad_url", this.h.e((String) null));
        b(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, this.h.f((String) null));
        this.b = this.h.b();
        String p = this.h.p(null);
        if (p != null) {
            map = abq.a(p);
        }
        this.c = map;
        this.d = this.h.a(0);
        this.f = this.h.i();
        h();
    }

    public void a(@Nullable Map<String, String> map) {
        if (!dl.a((Map) map) && !dl.a((Object) map, (Object) this.c)) {
            this.c = new HashMap(map);
            this.e = true;
            h();
        }
    }

    public boolean a() {
        String str = (String) this.a.get(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS);
        if (str != null && str.isEmpty()) {
            return dl.a((Map) this.c);
        }
        return true;
    }

    private void a(@NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.a.put(str, str2);
        }
    }

    private void b(@NonNull String str, @Nullable String str2) {
        if (str2 != null) {
            this.a.put(str, str2);
        }
    }

    private void a(@Nullable String str) {
        if (TextUtils.isEmpty((CharSequence) this.a.get("yandex_mobile_metrica_uuid")) && !TextUtils.isEmpty(str)) {
            a("yandex_mobile_metrica_uuid", str);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull List<String> list, Map<String, String> map) {
        for (String str : list) {
            String str2 = (String) this.a.get(str);
            if (str2 != null) {
                map.put(str, str2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean b() {
        return a(Arrays.asList(new String[]{IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, "appmetrica_device_id_hash", "yandex_mobile_metrica_device_id", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url", "yandex_mobile_metrica_uuid"}));
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean a(@NonNull List<String> list) {
        boolean z;
        z = true;
        boolean z2 = !b(list);
        boolean i = i();
        boolean z3 = !g();
        if (!z2 && !i && !this.e && !z3) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        return false;
     */
    public synchronized boolean b(@NonNull List<String> list) {
        for (String str : list) {
            if (IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS.equals(str)) {
                String str2 = (String) this.a.get(str);
                if (str2 == null || (str2.isEmpty() && !dl.a((Map) this.c))) {
                }
            } else {
                if (!"yandex_mobile_metrica_get_ad_url".equals(str)) {
                    if (!"yandex_mobile_metrica_report_ad_url".equals(str)) {
                        if (TextUtils.isEmpty((CharSequence) this.a.get(str))) {
                            return false;
                        }
                    }
                }
                if (this.a.get(str) == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private synchronized boolean g() {
        return this.f != null && this.f.a();
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull Bundle bundle) {
        f(bundle);
        g(bundle);
        e(bundle);
        b(bundle);
        c(bundle);
        h();
    }

    public void a(@NonNull dk dkVar) {
        this.g.add(dkVar);
    }

    private void h() {
        this.h.g((String) this.a.get("yandex_mobile_metrica_uuid")).h((String) this.a.get("yandex_mobile_metrica_device_id")).i((String) this.a.get("appmetrica_device_id_hash")).j((String) this.a.get("yandex_mobile_metrica_get_ad_url")).k((String) this.a.get("yandex_mobile_metrica_report_ad_url")).d(this.d).l((String) this.a.get(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS)).o(abq.a(this.c)).a(this.f).q();
    }

    private void b(Bundle bundle) {
        if (d(bundle)) {
            this.a.put(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, bundle.getString("Clids"));
            this.e = false;
        }
    }

    private void c(Bundle bundle) {
        aag aag = (aag) bundle.getParcelable("UiAcessConfig");
        if (aag != null && aag.a()) {
            this.f = aag;
            for (dk a2 : this.g) {
                a2.a(this.f);
            }
        }
    }

    private boolean d(@NonNull Bundle bundle) {
        Map a2 = abq.a(bundle.getString("RequestClids"));
        if (dl.a((Map) this.c)) {
            return dl.a(a2);
        }
        return this.c.equals(a2);
    }

    /* access modifiers changed from: 0000 */
    public void a(long j) {
        this.h.e(j).q();
    }

    private boolean i() {
        long b2 = abu.b() - this.h.b(0);
        return b2 > 86400 || b2 < 0;
    }

    /* access modifiers changed from: 0000 */
    public List<String> c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void c(List<String> list) {
        this.b = list;
        this.h.a(this.b);
    }

    private void e(Bundle bundle) {
        b(bundle.getLong("ServerTimeOffset"));
    }

    private synchronized void f(@NonNull Bundle bundle) {
        a(bundle.getString("Uuid"));
        a("yandex_mobile_metrica_device_id", bundle.getString("DeviceId"));
        a("appmetrica_device_id_hash", bundle.getString("DeviceIdHash"));
    }

    private synchronized void g(Bundle bundle) {
        String string = bundle.getString("AdUrlGet");
        if (string != null) {
            b(string);
        }
        String string2 = bundle.getString("AdUrlReport");
        if (string2 != null) {
            c(string2);
        }
    }

    private synchronized void b(String str) {
        this.a.put("yandex_mobile_metrica_get_ad_url", str);
    }

    private synchronized void c(String str) {
        this.a.put("yandex_mobile_metrica_report_ad_url", str);
    }

    private synchronized void b(long j) {
        this.d = j;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return (String) this.a.get("yandex_mobile_metrica_uuid");
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return (String) this.a.get("yandex_mobile_metrica_device_id");
    }

    public aag f() {
        return this.f;
    }
}
