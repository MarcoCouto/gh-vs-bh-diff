package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public interface op<T> {

    public static class a {
        private final HashMap<Class<?>, op<?>> a;
        private final op<yb> b;
        private final op<com.yandex.metrica.impl.ob.vf.a> c;
        private final op<List<sr>> d;
        private final op<sk> e;
        private final op<wr> f;

        /* renamed from: com.yandex.metrica.impl.ob.op$a$a reason: collision with other inner class name */
        private static final class C0089a {
            static final a a = new a();
        }

        public static <T> op<T> a(Class<T> cls) {
            return C0089a.a.c(cls);
        }

        public static <T> op<Collection<T>> b(Class<T> cls) {
            return C0089a.a.d(cls);
        }

        private a() {
            this.a = new HashMap<>();
            this.b = new op<yb>() {
                public mx<yb> a(@NonNull Context context) {
                    return new my("startup_state", lv.a(context).b(), new oo().a(), new od());
                }
            };
            this.c = new op<com.yandex.metrica.impl.ob.vf.a>() {
                public mx<com.yandex.metrica.impl.ob.vf.a> a(@NonNull Context context) {
                    return new my("provided_request_state", lv.a(context).b(), new oo().e(), new ny());
                }
            };
            this.d = new op<List<sr>>() {
                public mx<List<sr>> a(@NonNull Context context) {
                    return new my("permission_list", lv.a(context).b(), new oo().b(), new nw());
                }
            };
            this.e = new op<sk>() {
                public mx<sk> a(@NonNull Context context) {
                    return new my("app_permissions_state", lv.a(context).b(), new oo().c(), new ne());
                }
            };
            this.f = new op<wr>() {
                public mx<wr> a(@NonNull Context context) {
                    return new my("sdk_fingerprinting", lv.a(context).b(), new oo().d(), new ob());
                }
            };
            this.a.put(yb.class, this.b);
            this.a.put(com.yandex.metrica.impl.ob.vf.a.class, this.c);
            this.a.put(sr.class, this.d);
            this.a.put(sk.class, this.e);
            this.a.put(wr.class, this.f);
        }

        /* access modifiers changed from: 0000 */
        public <T> op<T> c(Class<T> cls) {
            return (op) this.a.get(cls);
        }

        /* access modifiers changed from: 0000 */
        public <T> op<Collection<T>> d(Class<T> cls) {
            return (op) this.a.get(cls);
        }
    }

    mx<T> a(@NonNull Context context);
}
