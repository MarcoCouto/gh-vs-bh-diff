package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.k.b;
import com.yandex.metrica.impl.ob.op.a;
import com.yandex.metrica.impl.ob.p.a.C0090a;
import java.util.List;

public class qo implements fg, b, p.b {
    @NonNull
    private List<qm> a;
    @NonNull
    private final p b;
    @NonNull
    private final qp c;
    @NonNull
    private final k d;
    @Nullable
    private volatile ql e;

    public qo(@NonNull Context context) {
        this(as.a().n(), qp.a(context), a.a(yb.class).a(context), as.a().o());
    }

    @VisibleForTesting
    qo(@NonNull p pVar, @NonNull qp qpVar, @NonNull mx<yb> mxVar, @NonNull k kVar) {
        synchronized (this) {
            this.b = pVar;
            this.c = qpVar;
            this.d = kVar;
            this.a = ((yb) mxVar.a()).p;
            this.b.a((p.b) this);
            this.d.a((b) this);
        }
    }

    public synchronized void a(@NonNull yb ybVar) {
        this.a = ybVar.p;
        this.e = d();
        this.c.a(ybVar, this.e);
    }

    public synchronized void a(@NonNull k.a aVar) {
        c();
    }

    public synchronized void a(@NonNull C0090a aVar) {
        c();
    }

    public synchronized void a() {
        c();
    }

    private void c() {
        ql d2 = d();
        if (!dl.a((Object) this.e, (Object) d2)) {
            this.c.a(d2);
            this.e = d2;
        }
    }

    @Nullable
    private ql d() {
        k.a c2 = this.d.c();
        C0090a d2 = this.b.d();
        for (qm qmVar : this.a) {
            if (qmVar.b.a.contains(d2) && qmVar.b.b.contains(c2)) {
                return qmVar.a;
            }
        }
        return null;
    }

    public synchronized void b() {
        this.d.b((b) this);
        this.b.b((p.b) this);
    }
}
