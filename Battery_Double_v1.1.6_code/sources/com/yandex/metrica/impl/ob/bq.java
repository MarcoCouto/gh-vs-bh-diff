package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.MetricaService.c;
import com.yandex.metrica.impl.ob.xh.a;
import java.io.File;

public class bq implements bo {
    /* access modifiers changed from: private */
    @Nullable
    public yb a;
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;
    @NonNull
    private final c c;
    /* access modifiers changed from: private */
    @NonNull
    public final a d;
    /* access modifiers changed from: private */
    @Nullable
    public xh e;
    @NonNull
    private bb f;
    @NonNull
    private gf g;
    @NonNull
    private final gl h;
    @NonNull
    private final br i;
    @Nullable
    private rp j;
    @NonNull
    private final mq k;
    @NonNull
    private cl l;
    private lm m;

    public bq(@NonNull Context context, @NonNull c cVar) {
        this(context, cVar, new gl(context));
    }

    private bq(@NonNull Context context, @NonNull c cVar, @NonNull gl glVar) {
        this(context, cVar, glVar, new gf(context, glVar), new br(), new a(), new mq(lv.a(context).c()), new an());
    }

    @VisibleForTesting
    bq(@NonNull Context context, @NonNull c cVar, @NonNull gl glVar, @NonNull gf gfVar, @NonNull br brVar, @NonNull a aVar, @NonNull mq mqVar, @NonNull an anVar) {
        this.b = context;
        this.c = cVar;
        this.g = gfVar;
        this.h = glVar;
        this.i = brVar;
        this.d = aVar;
        this.k = mqVar;
        this.m = new lm(anVar.b(this.b), new aby<File>() {
            public void a(File file) {
                bq.this.a(file);
            }
        });
    }

    public void a() {
        new db(this.b).a(this.b);
        as.a().b();
        abp.a().a(this.b);
        this.j = new rp(qp.a(this.b), as.a().l(), dn.a(this.b), this.k);
        c();
        eh.a().a(this, er.class, el.a((ek<T>) new ek<er>() {
            public void a(er erVar) {
                bq.this.b(erVar.b);
            }
        }).a(new ei<er>() {
            public boolean a(er erVar) {
                return !bq.this.b.getPackageName().equals(erVar.a);
            }
        }).a());
        this.a = (yb) op.a.a(yb.class).a(this.b).a();
        this.f = new bb(this.k, this.a.D);
        pe.a().a(this.b, this.a);
        f();
        this.l = new cl(this.b, this.g);
        this.m.a();
        xa.b(this.b);
    }

    private void c() {
        this.i.a((b) new b() {
            public void a() {
                bq.this.j();
            }
        });
        this.i.b((b) new b() {
            public void a() {
                bq.this.i();
            }
        });
        this.i.c((b) new b() {
            public void a() {
                bq.this.c(bq.this.a);
                bq.this.h();
                bq.this.e = bq.this.d.a(bq.this.b);
            }
        });
        this.i.d((b) new b() {
            public void a() {
                bq.this.d();
            }
        });
        this.i.e((b) new b() {
            public void a() {
                bq.this.e();
            }
        });
    }

    public void a(Intent intent, int i2) {
        b(intent, i2);
    }

    public void a(Intent intent, int i2, int i3) {
        b(intent, i3);
    }

    public void a(Intent intent) {
        this.i.a(intent);
    }

    public void b(Intent intent) {
        this.i.b(intent);
    }

    public void c(Intent intent) {
        String str;
        this.i.c(intent);
        if (intent != null) {
            String action = intent.getAction();
            Uri data = intent.getData();
            if (data == null) {
                str = null;
            } else {
                str = data.getEncodedAuthority();
            }
            if ("com.yandex.metrica.IMetricaService".equals(action)) {
                a(data, str);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(@NonNull yb ybVar) {
        this.a = ybVar;
        g();
        c(ybVar);
        as.a().a(ybVar);
        this.f.a(this.a.D);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.e != null) {
            this.e.b();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.e != null) {
            this.e.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@Nullable Uri uri, @Nullable String str) {
        if (uri != null && uri.getPath().equals("/client")) {
            int parseInt = Integer.parseInt(uri.getQueryParameter("pid"));
            this.g.a(str, parseInt, uri.getQueryParameter("psid"));
            as.a().o().c(parseInt);
        }
        if (this.g.a() <= 0) {
            e();
        }
    }

    public void b() {
        this.m.b();
        this.h.b();
        dn.a();
        eh.a().a((Object) this);
        as.a().p();
    }

    @Deprecated
    public void a(String str, int i2, String str2, Bundle bundle) {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        this.l.a(new aa(str2, str, i2), bundle);
    }

    public void a(Bundle bundle) {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        this.f.a();
        this.l.a(aa.b(bundle), bundle);
    }

    public void b(@NonNull Bundle bundle) {
        Integer d2 = d(bundle);
        if (d2 != null) {
            as.a().o().a(d2.intValue());
        }
    }

    public void c(@NonNull Bundle bundle) {
        Integer d2 = d(bundle);
        if (d2 != null) {
            as.a().o().b(d2.intValue());
        }
    }

    private Integer d(@NonNull Bundle bundle) {
        bundle.setClassLoader(eu.class.getClassLoader());
        eu a2 = eu.a(bundle);
        if (a2 == null) {
            return null;
        }
        return a2.f();
    }

    public void a(@NonNull File file) {
        this.l.a(file);
    }

    private void f() {
        if (this.a != null) {
            a(this.a);
        }
        c(this.a);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull yb ybVar) {
        xs xsVar = ybVar.r;
        if (xsVar == null) {
            eh.a().a(eq.class);
        } else {
            eh.a().b((ej) new eq(xsVar));
        }
    }

    private void b(Intent intent, int i2) {
        if (intent != null) {
            intent.getExtras().setClassLoader(CounterConfiguration.class.getClassLoader());
            e(intent);
        }
        this.c.a(i2);
    }

    private boolean d(Intent intent) {
        return intent == null || intent.getData() == null;
    }

    private void e(Intent intent) {
        if (!d(intent)) {
            Bundle extras = intent.getExtras();
            et etVar = new et(extras);
            if (!et.a(etVar, this.b)) {
                aa b2 = aa.b(extras);
                if (!b2.m() && !b2.n()) {
                    try {
                        this.l.a(ge.a(etVar), b2, new ew(etVar));
                    } catch (Throwable unused) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(@NonNull yb ybVar) {
        if (this.j != null) {
            this.j.a(ybVar);
        }
    }

    private void g() {
        final kb kbVar = new kb(this.b);
        as.a().k().i().a((Runnable) new Runnable() {
            public void run() {
                try {
                    kbVar.a();
                } catch (Throwable unused) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.a != null) {
            as.a().g().a(this.a);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.j != null) {
            this.j.a((Object) this);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.j != null) {
            this.j.b(this);
        }
    }
}
