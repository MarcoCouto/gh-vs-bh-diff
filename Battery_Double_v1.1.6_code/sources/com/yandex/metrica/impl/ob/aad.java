package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils.TruncateAt;
import org.json.JSONArray;
import org.json.JSONObject;

public class aad extends aam {
    @NonNull
    public String a;
    public final int b;
    @Nullable
    public Integer c;
    public final boolean d;
    @NonNull
    public final a e;
    @Nullable
    public final Float f;
    @Nullable
    public final Float g;
    @Nullable
    public final Float h;
    @Nullable
    public final String i;
    @Nullable
    public final Boolean j;
    @Nullable
    public final Boolean k;
    @Nullable
    public Integer l;

    /* renamed from: com.yandex.metrica.impl.ob.aad$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[TruncateAt.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            a[TruncateAt.START.ordinal()] = 1;
            a[TruncateAt.END.ordinal()] = 2;
            a[TruncateAt.MIDDLE.ordinal()] = 3;
            try {
                a[TruncateAt.MARQUEE.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    enum a {
        START("START"),
        END("END"),
        MIDDLE("MIDDLE"),
        MARQUEE("MARQUEE"),
        NONE("NONE"),
        UNKNOWN("UNKNOWN");
        
        @NonNull
        final String g;

        private a(String str) {
            this.g = str;
        }

        @NonNull
        static a a(@Nullable TruncateAt truncateAt) {
            if (truncateAt == null) {
                return NONE;
            }
            switch (AnonymousClass1.a[truncateAt.ordinal()]) {
                case 1:
                    return START;
                case 2:
                    return END;
                case 3:
                    return MIDDLE;
                case 4:
                    return MARQUEE;
                default:
                    return UNKNOWN;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return true;
    }

    aad(@NonNull String str, @NonNull String str2, @Nullable c cVar, int i2, boolean z, @NonNull a aVar, @NonNull String str3, @Nullable Float f2, @Nullable Float f3, @Nullable Float f4, @Nullable String str4, @Nullable Boolean bool, @Nullable Boolean bool2, boolean z2, int i3, @NonNull a aVar2) {
        super(str, str2, cVar, i2, z, d.VIEW, aVar);
        this.a = str3;
        this.b = i3;
        this.e = aVar2;
        this.d = z2;
        this.f = f2;
        this.g = f3;
        this.h = f4;
        this.i = str4;
        this.j = bool;
        this.k = bool2;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public JSONArray a(@NonNull aah aah) {
        JSONArray jSONArray = new JSONArray();
        try {
            JSONObject jSONObject = new JSONObject();
            String str = this.a;
            if (this.a.length() > aah.i) {
                this.c = Integer.valueOf(this.a.length());
                str = this.a.substring(0, aah.i);
            }
            jSONObject.put("t", b.TEXT.c);
            jSONObject.put("vl", str);
            jSONObject.put("i", a(aah, str));
            jSONArray.put(jSONObject);
        } catch (Throwable unused) {
        }
        return jSONArray;
    }

    @NonNull
    private JSONObject a(@NonNull aah aah, @NonNull String str) {
        int i2;
        JSONObject jSONObject = new JSONObject();
        try {
            if (aah.a) {
                jSONObject.putOpt("sp", this.f).putOpt("sd", this.g).putOpt("ss", this.h);
            }
            if (aah.b) {
                jSONObject.put("rts", this.l);
            }
            if (aah.d) {
                jSONObject.putOpt("c", this.i).putOpt("ib", this.j).putOpt("ii", this.k);
            }
            if (aah.c) {
                jSONObject.put("vtl", this.b).put("iv", this.d).put("tst", this.e.g);
            }
            if (this.c != null) {
                i2 = this.c.intValue();
            } else {
                i2 = this.a.length();
            }
            if (aah.g) {
                jSONObject.put("tl", str.length()).put("otl", i2);
            }
        } catch (Throwable unused) {
        }
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public c b(@NonNull aah aah) {
        c b2 = super.b(aah);
        return (b2 != null || this.a.length() <= aah.h) ? b2 : c.TEXT_TOO_LONG;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TextViewElement{mText='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", mVisibleTextLength=");
        sb.append(this.b);
        sb.append(", mOriginalTextLength=");
        sb.append(this.c);
        sb.append(", mIsVisible=");
        sb.append(this.d);
        sb.append(", mTextShorteningType=");
        sb.append(this.e);
        sb.append(", mSizePx=");
        sb.append(this.f);
        sb.append(", mSizeDp=");
        sb.append(this.g);
        sb.append(", mSizeSp=");
        sb.append(this.h);
        sb.append(", mColor='");
        sb.append(this.i);
        sb.append('\'');
        sb.append(", mIsBold=");
        sb.append(this.j);
        sb.append(", mIsItalic=");
        sb.append(this.k);
        sb.append(", mRelativeTextSize=");
        sb.append(this.l);
        sb.append(", mClassName='");
        sb.append(this.m);
        sb.append('\'');
        sb.append(", mId='");
        sb.append(this.n);
        sb.append('\'');
        sb.append(", mFilterReason=");
        sb.append(this.o);
        sb.append(", mDepth=");
        sb.append(this.p);
        sb.append(", mListItem=");
        sb.append(this.q);
        sb.append(", mViewType=");
        sb.append(this.r);
        sb.append(", mClassType=");
        sb.append(this.s);
        sb.append('}');
        return sb.toString();
    }
}
