package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class kl {
    @NonNull
    private Context a;
    @NonNull
    private ServiceConnection b;
    @NonNull
    private final ado c;

    public kl(@NonNull Context context) {
        this(context, new ServiceConnection() {
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            }

            public void onServiceDisconnected(ComponentName componentName) {
            }
        }, new ado());
    }

    public void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            Intent a2 = a(this.a, str);
            if (a2 != null) {
                this.a.bindService(a2, this.b, 1);
            }
        }
    }

    public void a() {
        this.a.unbindService(this.b);
    }

    @Nullable
    private Intent a(@NonNull Context context, @NonNull String str) {
        try {
            ResolveInfo resolveService = context.getPackageManager().resolveService(a(context).setPackage(str), 0);
            if (resolveService != null) {
                return new Intent().setClassName(resolveService.serviceInfo.packageName, resolveService.serviceInfo.name).setAction("com.yandex.metrica.ACTION_C_BG_L");
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    @NonNull
    private Intent a(@NonNull Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("metrica://");
        sb.append(context.getPackageName());
        Intent intent = new Intent("com.yandex.metrica.IMetricaService", Uri.parse(sb.toString()));
        a(intent);
        return intent;
    }

    @SuppressLint({"ObsoleteSdkInt"})
    private void a(@NonNull Intent intent) {
        if (VERSION.SDK_INT >= 12) {
            b(intent);
        }
    }

    @TargetApi(12)
    private void b(@NonNull Intent intent) {
        intent.addFlags(32);
    }

    @VisibleForTesting
    kl(@NonNull Context context, @NonNull ServiceConnection serviceConnection, @NonNull ado ado) {
        this.a = context;
        this.b = serviceConnection;
        this.c = ado;
    }
}
