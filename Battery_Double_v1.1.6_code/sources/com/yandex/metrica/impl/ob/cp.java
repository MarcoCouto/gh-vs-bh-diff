package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import com.yandex.metrica.f;
import com.yandex.metrica.j;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class cp {
    @NonNull
    private Context a;
    @NonNull
    private eu b;
    @NonNull
    private cs c;
    @NonNull
    private Handler d;
    @NonNull
    private xw e;
    private Map<String, ay> f = new HashMap();
    private final adw<String> g = new ads(new ady(this.f));
    private final List<String> h = Arrays.asList(new String[]{"20799a27-fa80-4b36-b2db-0f8141f24180", "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"});

    public cp(@NonNull Context context, @NonNull eu euVar, @NonNull cs csVar, @NonNull Handler handler, @NonNull xw xwVar) {
        this.a = context;
        this.b = euVar;
        this.c = csVar;
        this.d = handler;
        this.e = xwVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public bj a(@NonNull j jVar, boolean z, @NonNull mn mnVar) {
        this.g.a(jVar.apiKey);
        bj bjVar = new bj(this.a, this.b, jVar, this.c, this.e, new cq(this, "20799a27-fa80-4b36-b2db-0f8141f24180"), new cq(this, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"), mnVar);
        a((o) bjVar);
        bjVar.a(jVar, z);
        bjVar.a_();
        this.c.a(bjVar);
        this.f.put(jVar.apiKey, bjVar);
        return bjVar;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull f fVar) {
        if (this.f.containsKey(fVar.apiKey)) {
            abl a2 = abd.a(fVar.apiKey);
            if (a2.c()) {
                a2.b("Reporter with apiKey=%s already exists.", fVar.apiKey);
            }
        } else {
            b(fVar);
            StringBuilder sb = new StringBuilder();
            sb.append("Activate reporter with APIKey ");
            sb.append(dl.b(fVar.apiKey));
            Log.i("AppMetrica", sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public synchronized ay b(@NonNull f fVar) {
        ay ayVar;
        ayVar = (ay) this.f.get(fVar.apiKey);
        if (ayVar == 0) {
            if (!this.h.contains(fVar.apiKey)) {
                this.e.c();
            }
            bk bkVar = new bk(this.a, this.b, fVar, this.c);
            a((o) bkVar);
            bkVar.a_();
            this.f.put(fVar.apiKey, bkVar);
            ayVar = bkVar;
        }
        return ayVar;
    }

    private void a(@NonNull o oVar) {
        oVar.a(new bf(this.d, oVar));
        oVar.a((xx) this.e);
    }
}
