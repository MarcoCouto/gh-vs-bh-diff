package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.IMetricaService;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class ct implements a {
    /* access modifiers changed from: private */
    public final az a;
    /* access modifiers changed from: private */
    public final bn b;
    /* access modifiers changed from: private */
    public final Object c;
    private final act d;
    /* access modifiers changed from: private */
    @NonNull
    public final an e;

    @VisibleForTesting
    class a extends b {
        private final ku e;
        private final abg f;

        a(ct ctVar, @NonNull d dVar) {
            this(dVar, new ku(), new abg());
        }

        @VisibleForTesting
        a(d dVar, @NonNull ku kuVar, @NonNull abg abg) {
            super(dVar);
            this.e = kuVar;
            this.f = abg;
        }

        /* renamed from: a */
        public Void call() {
            if (this.f.a("Metrica")) {
                b(this.b);
                return null;
            }
            ct.this.b.c();
            return super.call();
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            a(this.b);
            return false;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void a(@NonNull d dVar) {
            if (dVar.d().o() == 0) {
                Context b = ct.this.a.b();
                Intent b2 = dc.b(b);
                dVar.d().a(com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.a());
                b2.putExtras(dVar.d().a(dVar.a().b()));
                try {
                    b.startService(b2);
                } catch (Throwable unused) {
                    b(dVar);
                }
            } else {
                b(dVar);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull d dVar) {
            PrintWriter printWriter;
            File b = ct.this.e.b(ct.this.b.a());
            if (this.e.a(b)) {
                eu g = dVar.a().g();
                Integer f2 = g.f();
                String g2 = g.g();
                an d = ct.this.e;
                StringBuilder sb = new StringBuilder();
                sb.append(f2);
                sb.append("-");
                sb.append(g2);
                try {
                    printWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(d.a(b, sb.toString()))));
                    try {
                        printWriter.write(new ln(dVar.a, dVar.a(), dVar.e).j());
                    } catch (Throwable th) {
                        th = th;
                        dl.a((Closeable) printWriter);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    printWriter = null;
                    dl.a((Closeable) printWriter);
                    throw th;
                }
                dl.a((Closeable) printWriter);
            }
        }
    }

    @VisibleForTesting
    class b extends e {
        final d b;

        @VisibleForTesting
        b(d dVar) {
            super();
            this.b = dVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull IMetricaService iMetricaService) throws RemoteException {
            a(iMetricaService, this.b);
        }

        /* access modifiers changed from: 0000 */
        public void a(Throwable th) {
            d dVar = this.b;
        }

        private void a(IMetricaService iMetricaService, d dVar) throws RemoteException {
            ct.this.a.a(iMetricaService, dVar.b(), dVar.b);
        }
    }

    public interface c {
        aa a(aa aaVar);
    }

    public static class d {
        /* access modifiers changed from: private */
        public aa a;
        /* access modifiers changed from: private */
        public co b;
        private boolean c = false;
        private c d;
        /* access modifiers changed from: private */
        @Nullable
        public HashMap<com.yandex.metrica.impl.ob.t.a, Integer> e;

        d(aa aaVar, co coVar) {
            this.a = aaVar;
            this.b = new co(new eu(coVar.g()), new CounterConfiguration(coVar.h()));
        }

        /* access modifiers changed from: 0000 */
        public d a(c cVar) {
            this.d = cVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public d a(@NonNull HashMap<com.yandex.metrica.impl.ob.t.a, Integer> hashMap) {
            this.e = hashMap;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public d a(boolean z) {
            this.c = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public co a() {
            return this.b;
        }

        /* access modifiers changed from: 0000 */
        public aa b() {
            return this.d != null ? this.d.a(this.a) : this.a;
        }

        /* access modifiers changed from: 0000 */
        public boolean c() {
            return this.c;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public aa d() {
            return this.a;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ReportToSend{mReport=");
            sb.append(this.a);
            sb.append(", mEnvironment=");
            sb.append(this.b);
            sb.append(", mCrash=");
            sb.append(this.c);
            sb.append(", mAction=");
            sb.append(this.d);
            sb.append(", mTrimmedFields=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }

    private abstract class e implements Callable<Void> {
        /* access modifiers changed from: 0000 */
        public abstract void a(@NonNull IMetricaService iMetricaService) throws RemoteException;

        /* access modifiers changed from: 0000 */
        public void a(Throwable th) {
        }

        private e() {
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:1|2|3|(2:5|6)|8|9|(1:15)(1:13)) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
            return null;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0012 */
        /* renamed from: a */
        public Void call() {
            int i = 0;
            do {
                try {
                    IMetricaService f = ct.this.b.f();
                    if (f != null) {
                        a(f);
                    }
                    i++;
                    if (b() || bt.a.get()) {
                        return null;
                    }
                } catch (Throwable th) {
                    a(th);
                    return null;
                }
            } while (i < 20);
            return null;
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            ct.this.b.b();
            c();
            return true;
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(4:5|6|7|8) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0020 */
        private void c() {
            synchronized (ct.this.c) {
                if (!ct.this.b.e()) {
                    ct.this.c.wait(500, 0);
                    ct.this.c.notifyAll();
                }
            }
        }
    }

    public void b() {
    }

    public ct(az azVar) {
        this(azVar, dr.k().c(), new an());
    }

    public ct(@NonNull az azVar, @NonNull act act, @NonNull an anVar) {
        this.c = new Object();
        this.a = azVar;
        this.d = act;
        this.e = anVar;
        this.b = azVar.a();
        this.b.a((a) this);
    }

    public Future<Void> a(d dVar) {
        return this.d.a(dVar.c() ? new a<>(this, dVar) : new b<>(dVar));
    }

    public Future<Void> a(@NonNull final eu euVar) {
        return this.d.a((Callable<T>) new e() {
            /* access modifiers changed from: 0000 */
            public void a(@NonNull IMetricaService iMetricaService) throws RemoteException {
                ct.this.a.a(iMetricaService, euVar);
            }
        });
    }

    public Future<Void> b(@NonNull final eu euVar) {
        return this.d.a((Callable<T>) new e() {
            /* access modifiers changed from: 0000 */
            public void a(@NonNull IMetricaService iMetricaService) throws RemoteException {
                ct.this.a.b(iMetricaService, euVar);
            }
        });
    }

    public void a() {
        synchronized (this.c) {
            this.c.notifyAll();
        }
    }
}
