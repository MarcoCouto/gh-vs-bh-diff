package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.abc.a;
import org.json.JSONObject;

public class ys {
    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        ve.a aVar2 = new ve.a();
        JSONObject optJSONObject = aVar.optJSONObject("retry_policy");
        int i = aVar2.I;
        int i2 = aVar2.J;
        if (optJSONObject != null) {
            i = optJSONObject.optInt("max_interval_seconds", aVar2.I);
            i2 = optJSONObject.optInt("exponential_multiplier", aVar2.J);
        }
        yxVar.a(new xq(i, i2));
    }
}
