package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.uu.c.C0093c;
import com.yandex.metrica.impl.ob.uu.c.d;
import com.yandex.metrica.impl.ob.uu.c.e;
import com.yandex.metrica.impl.ob.uu.c.f;
import com.yandex.metrica.impl.ob.uu.c.g;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

class cn extends cb<wm> {
    com.yandex.metrica.impl.ob.uu.c j;
    lr k;
    List<Long> l;
    int m;
    int n;
    @NonNull
    private final fe o;
    private final Map<String, String> p;
    private wc q;
    private c r;
    private final adn<byte[]> s;
    private final abl t;
    @Nullable
    private we u;
    @NonNull
    private final mo v;
    private int w;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        public cn a(fe feVar) {
            return new cn(feVar);
        }
    }

    static final class b {
        final e a;
        final com.yandex.metrica.impl.ob.i.a b;
        final boolean c;

        b(e eVar, com.yandex.metrica.impl.ob.i.a aVar, boolean z) {
            this.a = eVar;
            this.b = aVar;
            this.c = z;
        }
    }

    static final class c {
        final List<e> a;
        final List<Long> b;
        final JSONObject c;

        c(List<e> list, List<Long> list2, JSONObject jSONObject) {
            this.a = list;
            this.b = list2;
            this.c = jSONObject;
        }
    }

    public cn(fe feVar) {
        this(feVar, feVar.j(), feVar.l(), new mo(lv.a(feVar.k()).b(feVar.c())));
    }

    private cn(@NonNull fe feVar, @NonNull lr lrVar, @NonNull abl abl, @NonNull mo moVar) {
        this(feVar, abl, lrVar, new wm(), moVar, new add(245760, "event value in ReportTask", abl));
    }

    @VisibleForTesting
    cn(@NonNull fe feVar, @NonNull abl abl, @NonNull lr lrVar, @NonNull wm wmVar, @NonNull mo moVar, @NonNull add add) {
        super(wmVar);
        this.p = new LinkedHashMap();
        this.m = 0;
        this.n = -1;
        this.o = feVar;
        this.k = lrVar;
        this.t = abl;
        this.s = add;
        this.v = moVar;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((wm) this.i).a(builder, this.u);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ContentValues contentValues) {
        this.p.clear();
        for (Entry entry : contentValues.valueSet()) {
            this.p.put(entry.getKey(), entry.getValue().toString());
        }
        String asString = contentValues.getAsString("report_request_parameters");
        if (!TextUtils.isEmpty(asString)) {
            try {
                this.q = new wc(new com.yandex.metrica.impl.ob.abc.a(asString));
                ((wm) this.i).a(this.q);
            } catch (Throwable unused) {
                K();
            }
        } else {
            K();
        }
    }

    private void K() {
        this.q = new wc();
        ((wm) this.i).a(this.q);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public com.yandex.metrica.impl.ob.uu.c a(c cVar, C0093c[] cVarArr, @NonNull List<String> list) {
        com.yandex.metrica.impl.ob.uu.c cVar2 = new com.yandex.metrica.impl.ob.uu.c();
        d dVar = new d();
        dVar.b = abw.a(this.q.b, this.u.u());
        dVar.c = abw.a(this.q.a, this.u.s());
        this.m += b.b(4, (e) dVar);
        cVar2.c = dVar;
        a(cVar2);
        cVar2.b = (e[]) cVar.a.toArray(new e[cVar.a.size()]);
        cVar2.d = a(cVar.c);
        cVar2.e = cVarArr;
        cVar2.h = (String[]) list.toArray(new String[list.size()]);
        this.m += b.h(8);
        return cVar2;
    }

    /* access modifiers changed from: 0000 */
    public void a(final com.yandex.metrica.impl.ob.uu.c cVar) {
        as.a().l().a((zn) new zn() {
            public void a(zm zmVar) {
                b(zmVar, cVar);
                a(zmVar, cVar);
            }

            private void a(zm zmVar, com.yandex.metrica.impl.ob.uu.c cVar) {
                List a2 = zmVar.a();
                if (!dl.a((Collection) a2)) {
                    cVar.g = new f[a2.size()];
                    for (int i = 0; i < a2.size(); i++) {
                        cVar.g[i] = ci.a((zi) a2.get(i));
                        cn.this.m += b.b((e) cVar.g[i]);
                        cn.this.m += b.h(10);
                    }
                }
            }

            private void b(zm zmVar, com.yandex.metrica.impl.ob.uu.c cVar) {
                List c = zmVar.c();
                if (!dl.a((Collection) c)) {
                    cVar.f = new String[c.size()];
                    for (int i = 0; i < c.size(); i++) {
                        String str = (String) c.get(i);
                        if (!TextUtils.isEmpty(str)) {
                            cVar.f[i] = str;
                            cn.this.m += b.b(cVar.f[i]);
                            cn.this.m += b.h(9);
                        }
                    }
                }
            }
        });
    }

    public boolean a() {
        List d = this.o.j().d();
        if (d.isEmpty()) {
            return false;
        }
        a((ContentValues) d.get(0));
        this.u = this.o.i();
        List ab = this.u.ab();
        if (dl.a((Collection) ab)) {
            return false;
        }
        a(this.u.b());
        if (!this.u.O() || dl.a((Collection) s())) {
            return false;
        }
        this.l = null;
        C0093c[] I = I();
        this.r = a(this.u);
        if (this.r.a.isEmpty()) {
            return false;
        }
        this.w = this.v.l() + 1;
        ((wm) this.i).a(this.w);
        this.j = a(this.r, I, ab);
        this.l = this.r.b;
        c(e.a((e) this.j));
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public C0093c[] I() {
        C0093c[] a2 = ci.a(this.o.k());
        if (a2 != null) {
            for (C0093c b2 : a2) {
                this.m += b.b((e) b2);
            }
        }
        return a2;
    }

    private com.yandex.metrica.impl.ob.uu.c.a[] a(JSONObject jSONObject) {
        int length = jSONObject.length();
        if (length <= 0) {
            return null;
        }
        com.yandex.metrica.impl.ob.uu.c.a[] aVarArr = new com.yandex.metrica.impl.ob.uu.c.a[length];
        Iterator keys = jSONObject.keys();
        int i = 0;
        while (keys.hasNext()) {
            String str = (String) keys.next();
            try {
                com.yandex.metrica.impl.ob.uu.c.a aVar = new com.yandex.metrica.impl.ob.uu.c.a();
                aVar.b = str;
                aVar.c = jSONObject.getString(str);
                aVarArr[i] = aVar;
            } catch (Throwable unused) {
            }
            i++;
        }
        return aVarArr;
    }

    /* access modifiers changed from: protected */
    public void H() {
        b(true);
    }

    /* access modifiers changed from: protected */
    public void G() {
        b(false);
    }

    private void b(boolean z) {
        L();
        e[] eVarArr = this.j.b;
        for (int i = 0; i < eVarArr.length; i++) {
            e eVar = eVarArr[i];
            this.k.a(((Long) this.l.get(i)).longValue(), ci.a(eVar.c.d).a(), eVar.d.length, z);
            ci.a(eVar);
        }
        this.k.a(this.o.d().a());
    }

    private void L() {
        this.v.e(this.w);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    public void f() {
        if (x()) {
            M();
        }
        this.r = null;
    }

    private void M() {
        if (this.t.c()) {
            for (int i = 0; i < this.r.a.size(); i++) {
                this.t.a((e) this.r.a.get(i), "Event sent");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d3, code lost:
        r14 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e8, code lost:
        r14 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:27:0x00c5, B:38:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d3 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:27:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ff A[LOOP:1: B:50:0x00f9->B:52:0x00ff, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c9 A[SYNTHETIC] */
    public c a(@NonNull we weVar) {
        JSONObject jSONObject;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        JSONObject jSONObject2 = new JSONObject();
        ArrayList<Throwable> arrayList3 = new ArrayList<>();
        Cursor cursor = null;
        try {
            Cursor cursor2 = N();
            if (cursor2 != null) {
                JSONObject jSONObject3 = jSONObject2;
                com.yandex.metrica.impl.ob.i.a aVar = null;
                while (true) {
                    try {
                        if (!cursor2.moveToNext()) {
                            break;
                        }
                        ContentValues contentValues = new ContentValues();
                        aax.a(cursor2, contentValues);
                        Long asLong = contentValues.getAsLong("id");
                        if (asLong == null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("no session_id in values: ");
                            sb.append(contentValues.toString());
                            xa.a().b("protobuf_serialization_error", sb.toString());
                        } else {
                            jy a2 = jy.a(contentValues.getAsInteger("type"));
                            g a3 = ci.a(contentValues);
                            com.yandex.metrica.impl.ob.uu.c.e.b a4 = ci.a(weVar.B(), ci.a(a2), a3);
                            this.m += b.d(1, Long.MAX_VALUE);
                            this.m += b.b(2, (e) a4);
                            if (this.m >= 250880) {
                                break;
                            }
                            b a5 = a(asLong.longValue(), a4, weVar, arrayList3);
                            if (a5 == null) {
                                continue;
                            } else {
                                if (aVar != null) {
                                    if (!aVar.equals(a5.b)) {
                                        break;
                                    }
                                } else {
                                    aVar = a5.b;
                                }
                                arrayList2.add(asLong);
                                arrayList.add(a5.a);
                                if (!TextUtils.isEmpty(a5.b.a)) {
                                    try {
                                        jSONObject = new JSONObject(a5.b.a);
                                    } catch (Throwable th) {
                                    }
                                    if (!a5.c) {
                                        jSONObject2 = jSONObject;
                                        break;
                                    }
                                    jSONObject3 = jSONObject;
                                }
                                jSONObject = jSONObject3;
                                try {
                                    if (!a5.c) {
                                    }
                                } catch (Throwable th2) {
                                }
                            }
                        }
                    } catch (Throwable th22) {
                    }
                }
                jSONObject2 = jSONObject3;
            } else {
                xa.a().b("protobuf_serialization_error", "no sessions cursor");
            }
            dl.a(cursor2);
        } catch (Throwable th3) {
            th = th3;
            arrayList3.add(th);
            dl.a(cursor);
            while (r14.hasNext()) {
            }
            return new c(arrayList, arrayList2, jSONObject2);
        }
        while (r14.hasNext()) {
            xa.a().reportError("protobuf_serialization_error", r3);
        }
        return new c(arrayList, arrayList2, jSONObject2);
    }

    @NonNull
    private com.yandex.metrica.impl.ob.i.a b(@NonNull ContentValues contentValues) {
        return new com.yandex.metrica.impl.ob.i.a((String) abw.b(contentValues.getAsString("app_environment"), ""), ((Long) abw.b(contentValues.getAsLong("app_environment_revision"), Long.valueOf(0))).longValue());
    }

    private int a(@NonNull com.yandex.metrica.impl.ob.i.a aVar) {
        int i = 0;
        try {
            com.yandex.metrica.impl.ob.uu.c.a[] a2 = a(new JSONObject(aVar.a));
            if (a2 != null) {
                int i2 = 0;
                for (com.yandex.metrica.impl.ob.uu.c.a b2 : a2) {
                    i2 += b.b(7, (e) b2);
                }
                i = i2;
            }
            return i;
        } catch (Throwable unused) {
            return 0;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public b a(long j2, com.yandex.metrica.impl.ob.uu.c.e.b bVar, @NonNull we weVar, @NonNull List<Throwable> list) {
        Cursor cursor;
        boolean z;
        e eVar = new e();
        eVar.b = j2;
        eVar.c = bVar;
        b bVar2 = null;
        try {
            cursor = a(j2, ci.a(bVar.d));
            if (cursor != null) {
                try {
                    ArrayList arrayList = new ArrayList();
                    com.yandex.metrica.impl.ob.i.a aVar = null;
                    while (true) {
                        z = false;
                        if (!cursor.moveToNext()) {
                            break;
                        }
                        ContentValues contentValues = new ContentValues();
                        aax.a(cursor, contentValues);
                        com.yandex.metrica.impl.ob.uu.c.e.a a2 = a(contentValues, weVar, list);
                        if (a2 != null) {
                            com.yandex.metrica.impl.ob.i.a b2 = b(contentValues);
                            if (aVar != null) {
                                if (!aVar.equals(b2)) {
                                    z = true;
                                    break;
                                }
                            } else {
                                if (this.n < 0) {
                                    this.n = a(b2);
                                    this.m += this.n;
                                }
                                aVar = b2;
                            }
                            a(a2);
                            this.m += b.b(3, (e) a2);
                            if (this.m >= 250880) {
                                break;
                            }
                            arrayList.add(a2);
                        }
                    }
                    if (arrayList.size() > 0) {
                        eVar.d = (com.yandex.metrica.impl.ob.uu.c.e.a[]) arrayList.toArray(new com.yandex.metrica.impl.ob.uu.c.e.a[arrayList.size()]);
                        bVar2 = new b(eVar, aVar, z);
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        list.add(th);
                        dl.a(cursor);
                        return bVar2;
                    } catch (Throwable th2) {
                        th = th2;
                        dl.a(cursor);
                        throw th;
                    }
                }
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("no reports cursor for session: ");
                sb.append(eVar);
                xa.a().b("protobuf_serialization_error", sb.toString());
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            dl.a(cursor);
            throw th;
        }
        dl.a(cursor);
        return bVar2;
    }

    private void a(@NonNull com.yandex.metrica.impl.ob.uu.c.e.a aVar) {
        byte[] bArr = (byte[]) this.s.a(aVar.f);
        if (aVar.f != bArr) {
            aVar.k += d(aVar.f) - d(bArr);
            aVar.f = bArr;
        }
    }

    private int d(@Nullable byte[] bArr) {
        if (bArr == null) {
            return 0;
        }
        return bArr.length;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public com.yandex.metrica.impl.ob.uu.c.e.a a(ContentValues contentValues, @NonNull we weVar, @NonNull List<Throwable> list) {
        try {
            to toVar = new to(contentValues);
            return ci.a(toVar.j, weVar.P()).a(toVar);
        } catch (Throwable th) {
            list.add(th);
            return null;
        }
    }

    @Nullable
    private Cursor N() {
        return this.k.a(this.p);
    }

    @Nullable
    private Cursor a(long j2, @NonNull jy jyVar) {
        return this.k.a(j2, jyVar);
    }

    public void C() {
        this.o.C().c();
    }

    public void D() {
        this.o.j().c();
        this.o.C().b();
        if (x()) {
            this.o.C().a();
        }
    }

    @NonNull
    public String n() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.n());
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(this.o.c().a());
        return sb.toString();
    }

    public static a J() {
        return new a();
    }

    @Nullable
    public com.yandex.metrica.impl.ob.ca.a E() {
        return com.yandex.metrica.impl.ob.ca.a.REPORT;
    }

    @Nullable
    public xq F() {
        return this.o.i().f();
    }
}
