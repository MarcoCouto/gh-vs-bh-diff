package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class pi extends ro implements qn {
    @Nullable
    private volatile pt a;
    @NonNull
    private final String b;

    public pi(@Nullable pt ptVar, @NonNull qe qeVar, @NonNull String str) {
        this(ptVar, qeVar, str, new dj());
    }

    @VisibleForTesting
    pi(@Nullable pt ptVar, @NonNull qe qeVar, @NonNull String str, @NonNull dj djVar) {
        super(qeVar, djVar);
        this.a = ptVar;
        this.b = str;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String c() {
        return this.b;
    }

    public boolean a() {
        pt ptVar = this.a;
        return (ptVar == null || ptVar.b == null || !a(ptVar.b.b)) ? false : true;
    }

    public boolean b() {
        pt ptVar = this.a;
        return ptVar != null && ptVar.a.a;
    }

    public void a(@Nullable pt ptVar) {
        this.a = ptVar;
    }
}
