package com.yandex.metrica.impl.ob;

public class jv {
    private long a;
    private long b;
    private long c;
    private jy d;

    public long a() {
        return this.a;
    }

    public jv a(long j) {
        this.a = j;
        return this;
    }

    public jy b() {
        return this.d;
    }

    public jv a(jy jyVar) {
        this.d = jyVar;
        return this;
    }

    public long c() {
        return this.b;
    }

    public jv b(long j) {
        this.b = j;
        return this;
    }

    public long d() {
        return this.c;
    }

    public jv c(long j) {
        this.c = j;
        return this;
    }
}
