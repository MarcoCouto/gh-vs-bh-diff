package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.fk;
import java.io.Closeable;

public class di<C extends fk> implements Closeable {
    final Object a = new Object();
    boolean b = false;
    @NonNull
    private C c;
    @NonNull
    private final yh d;

    /* access modifiers changed from: 0000 */
    public void a() {
    }

    public di(@NonNull C c2, @NonNull yh yhVar) {
        this.c = c2;
        this.d = yhVar;
    }

    public void close() {
        synchronized (this.a) {
            if (!this.b) {
                a();
                this.b = true;
            }
        }
    }

    public void e() {
        synchronized (this.a) {
            if (!this.b) {
                f();
                a();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        synchronized (this.a) {
            if (!this.b) {
                c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ca caVar) {
        by r = as.a().r();
        if (r != null) {
            r.a(caVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.d.a();
    }

    @NonNull
    public C g() {
        return this.c;
    }
}
