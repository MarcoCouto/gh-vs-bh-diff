package com.yandex.metrica.impl.ob;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class acy implements ThreadFactory {
    private static final AtomicInteger a = new AtomicInteger(0);
    private final String b;

    public acy(String str) {
        this.b = str;
    }

    /* renamed from: a */
    public acx newThread(Runnable runnable) {
        return new acx(runnable, c());
    }

    private String c() {
        return a(this.b);
    }

    public acw a() {
        return new acw(c());
    }

    public static acx a(String str, Runnable runnable) {
        return new acy(str).newThread(runnable);
    }

    public static String a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("-");
        sb.append(b());
        return sb.toString();
    }

    public static int b() {
        return a.incrementAndGet();
    }
}
