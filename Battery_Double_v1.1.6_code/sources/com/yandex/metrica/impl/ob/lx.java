package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

public class lx {
    public static final String a = "lx";
    /* access modifiers changed from: private */
    public final Map<String, Object> b;
    /* access modifiers changed from: private */
    public final Map<String, Object> c;
    private final String d;
    private final a e;
    /* access modifiers changed from: private */
    public volatile boolean f;
    private final me g;

    private class a extends acx {
        public a(String str) {
            super(str);
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|(2:11|12)|13|14|15) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0032 */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x001c A[SYNTHETIC] */
        public void run() {
            HashMap hashMap;
            synchronized (lx.this.b) {
                lx.this.c();
                lx.this.f = true;
                lx.this.b.notifyAll();
            }
            while (c()) {
                synchronized (this) {
                    if (lx.this.c.size() == 0) {
                        wait();
                    }
                    hashMap = new HashMap(lx.this.c);
                    lx.this.c.clear();
                }
                if (hashMap.size() <= 0) {
                    lx.this.a((Map<String, Object>) hashMap);
                    hashMap.clear();
                }
            }
        }
    }

    public lx(lu luVar, String str) {
        this(str, (me) new mh(luVar));
    }

    protected lx(String str, me meVar) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.g = meVar;
        this.d = str;
        this.e = new a(String.format(Locale.US, "YMM-DW-%s", new Object[]{Integer.valueOf(acy.b())}));
        this.e.start();
    }

    /* JADX WARNING: type inference failed for: r4v4, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v6, types: [java.lang.Boolean] */
    /* JADX WARNING: type inference failed for: r4v7, types: [java.lang.Boolean] */
    /* JADX WARNING: type inference failed for: r4v8, types: [java.lang.Integer] */
    /* JADX WARNING: type inference failed for: r4v9, types: [java.lang.Long] */
    /* JADX WARNING: type inference failed for: r4v10, types: [java.lang.Float] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 5 */
    public void c() {
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor;
        Throwable th;
        Cursor cursor2 = null;
        try {
            sQLiteDatabase = this.g.a();
            if (sQLiteDatabase != null) {
                try {
                    cursor = sQLiteDatabase.query(a(), new String[]{ParametersKeys.KEY, "value", "type"}, null, null, null, null, null);
                    while (cursor.moveToNext()) {
                        try {
                            String string = cursor.getString(cursor.getColumnIndex(ParametersKeys.KEY));
                            String string2 = cursor.getString(cursor.getColumnIndex("value"));
                            int i = cursor.getInt(cursor.getColumnIndex("type"));
                            if (!TextUtils.isEmpty(string)) {
                                switch (i) {
                                    case 1:
                                        if (!"true".equals(string2)) {
                                            if ("false".equals(string2)) {
                                                string2 = Boolean.FALSE;
                                                break;
                                            }
                                        } else {
                                            string2 = Boolean.TRUE;
                                            break;
                                        }
                                    case 2:
                                        string2 = abk.c(string2);
                                        break;
                                    case 3:
                                        string2 = abk.a(string2);
                                        break;
                                    case 4:
                                        break;
                                    case 5:
                                        string2 = abk.b(string2);
                                        break;
                                    default:
                                        string2 = 0;
                                        break;
                                }
                                if (string2 != 0) {
                                    this.b.put(string, string2);
                                }
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            dl.a(cursor);
                            this.g.a(sQLiteDatabase);
                            throw th;
                        }
                    }
                    cursor2 = cursor;
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    cursor = null;
                    th = th4;
                    dl.a(cursor);
                    this.g.a(sQLiteDatabase);
                    throw th;
                }
            }
            dl.a(cursor2);
        } catch (Throwable th5) {
            cursor = null;
            th = th5;
            sQLiteDatabase = null;
            dl.a(cursor);
            this.g.a(sQLiteDatabase);
            throw th;
        }
        this.g.a(sQLiteDatabase);
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.d;
    }

    public void b() {
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    /* access modifiers changed from: private */
    public void a(Map<String, Object> map) {
        ContentValues[] contentValuesArr = new ContentValues[map.size()];
        int i = 0;
        for (Entry entry : map.entrySet()) {
            ContentValues contentValues = new ContentValues();
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            contentValues.put(ParametersKeys.KEY, str);
            if (value == this) {
                contentValues.putNull("value");
            } else if (value instanceof String) {
                contentValues.put("value", (String) value);
                contentValues.put("type", Integer.valueOf(4));
            } else if (value instanceof Long) {
                contentValues.put("value", (Long) value);
                contentValues.put("type", Integer.valueOf(3));
            } else if (value instanceof Integer) {
                contentValues.put("value", (Integer) value);
                contentValues.put("type", Integer.valueOf(2));
            } else if (value instanceof Boolean) {
                contentValues.put("value", String.valueOf(((Boolean) value).booleanValue()));
                contentValues.put("type", Integer.valueOf(1));
            } else if (value instanceof Float) {
                contentValues.put("value", (Float) value);
                contentValues.put("type", Integer.valueOf(5));
            }
            contentValuesArr[i] = contentValues;
            i++;
        }
        a(contentValuesArr);
    }

    private void a(ContentValues[] contentValuesArr) {
        SQLiteDatabase sQLiteDatabase;
        if (contentValuesArr != null) {
            try {
                sQLiteDatabase = this.g.a();
                if (sQLiteDatabase != null) {
                    try {
                        sQLiteDatabase.beginTransaction();
                        for (ContentValues contentValues : contentValuesArr) {
                            if (contentValues.getAsString("value") == null) {
                                sQLiteDatabase.delete(a(), "key = ?", new String[]{contentValues.getAsString(ParametersKeys.KEY)});
                            } else {
                                sQLiteDatabase.insertWithOnConflict(a(), null, contentValues, 5);
                            }
                        }
                        sQLiteDatabase.setTransactionSuccessful();
                    } catch (Throwable th) {
                        th = th;
                        dl.a(sQLiteDatabase);
                        this.g.a(sQLiteDatabase);
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase = null;
                dl.a(sQLiteDatabase);
                this.g.a(sQLiteDatabase);
                throw th;
            }
            dl.a(sQLiteDatabase);
            this.g.a(sQLiteDatabase);
        }
    }

    @Nullable
    public String a(String str, String str2) {
        Object c2 = c(str);
        return c2 instanceof String ? (String) c2 : str2;
    }

    public int a(String str, int i) {
        Object c2 = c(str);
        return c2 instanceof Integer ? ((Integer) c2).intValue() : i;
    }

    public long a(String str, long j) {
        Object c2 = c(str);
        return c2 instanceof Long ? ((Long) c2).longValue() : j;
    }

    public boolean a(String str, boolean z) {
        Object c2 = c(str);
        return c2 instanceof Boolean ? ((Boolean) c2).booleanValue() : z;
    }

    public lx a(String str) {
        synchronized (this.b) {
            d();
            this.b.remove(str);
        }
        synchronized (this.e) {
            this.c.put(str, this);
            this.e.notifyAll();
        }
        return this;
    }

    public synchronized lx b(String str, String str2) {
        a(str, (Object) str2);
        return this;
    }

    public lx b(String str, long j) {
        a(str, (Object) Long.valueOf(j));
        return this;
    }

    public synchronized lx b(String str, int i) {
        a(str, (Object) Integer.valueOf(i));
        return this;
    }

    public lx b(String str, boolean z) {
        a(str, (Object) Boolean.valueOf(z));
        return this;
    }

    public boolean b(@NonNull String str) {
        boolean containsKey;
        synchronized (this.b) {
            d();
            containsKey = this.b.containsKey(str);
        }
        return containsKey;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(String str, Object obj) {
        synchronized (this.b) {
            d();
            this.b.put(str, obj);
        }
        synchronized (this.e) {
            this.c.put(str, obj);
            this.e.notifyAll();
        }
    }

    private Object c(String str) {
        Object obj;
        synchronized (this.b) {
            d();
            obj = this.b.get(str);
        }
        return obj;
    }

    private void d() {
        if (!this.f) {
            try {
                this.b.wait();
            } catch (InterruptedException unused) {
            }
        }
    }
}
