package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ey;
import com.yandex.metrica.impl.ob.hk;

public class hl<T extends hk, C extends ey> extends hc<T, C> {
    public hl(@NonNull hi<T> hiVar, @NonNull C c) {
        super(hiVar, c);
    }

    public boolean a(@NonNull aa aaVar, @NonNull final gj gjVar) {
        return a(aaVar, new a<T>() {
            public boolean a(T t, aa aaVar) {
                return t.a(aaVar, gjVar);
            }
        });
    }
}
