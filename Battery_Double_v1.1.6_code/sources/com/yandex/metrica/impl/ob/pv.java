package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class pv extends ro implements pz {
    @Nullable
    private volatile py a;

    /* access modifiers changed from: protected */
    @NonNull
    public String c() {
        return "fused";
    }

    public pv(@Nullable py pyVar, @NonNull qe qeVar) {
        this(pyVar, qeVar, new dj());
    }

    @VisibleForTesting
    pv(@Nullable py pyVar, @NonNull qe qeVar, @NonNull dj djVar) {
        super(qeVar, djVar);
        this.a = pyVar;
    }

    public boolean a() {
        py pyVar = this.a;
        return (pyVar == null || pyVar.b == null || !a(pyVar.b.c)) ? false : true;
    }

    public boolean b() {
        py pyVar = this.a;
        return pyVar != null && pyVar.a.a;
    }

    public void a(@Nullable py pyVar) {
        this.a = pyVar;
    }
}
