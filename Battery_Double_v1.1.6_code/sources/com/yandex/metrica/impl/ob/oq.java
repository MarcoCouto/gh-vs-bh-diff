package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

@Deprecated
public class oq {
    @Nullable
    public final String a;
    @Nullable
    public final String b;

    public oq(@Nullable String str, @Nullable String str2) {
        this.a = str;
        this.b = str2;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        oq oqVar = (oq) obj;
        if (this.a == null ? oqVar.a != null : !this.a.equals(oqVar.a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(oqVar.b);
        } else if (oqVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppMetricaDeviceIdentifiers{deviceID='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", deviceIDHash='");
        sb.append(this.b);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
