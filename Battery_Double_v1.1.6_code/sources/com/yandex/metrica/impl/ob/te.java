package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class te extends ta {
    private static final th d = new th("PREF_KEY_OFFSET");
    private th e = new th(d.a(), null);

    /* access modifiers changed from: protected */
    public String f() {
        return "_servertimeoffset";
    }

    public te(Context context, String str) {
        super(context, str);
    }

    public long a(int i) {
        return this.c.getLong(this.e.b(), (long) i);
    }

    public void a() {
        h(this.e.b()).j();
    }
}
