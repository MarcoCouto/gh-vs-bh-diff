package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.fk;

public class hc<T, C extends fk> {
    private final hi<T> a;
    private final C b;

    protected interface a<T> {
        boolean a(T t, aa aaVar);
    }

    protected hc(hi<T> hiVar, C c) {
        this.a = hiVar;
        this.b = c;
    }

    /* access modifiers changed from: protected */
    public boolean a(@NonNull aa aaVar, @NonNull a<T> aVar) {
        for (Object a2 : a(aaVar).a()) {
            if (aVar.a(a2, aaVar)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public hf<T> a(aa aaVar) {
        return this.a.a(aaVar.g());
    }
}
