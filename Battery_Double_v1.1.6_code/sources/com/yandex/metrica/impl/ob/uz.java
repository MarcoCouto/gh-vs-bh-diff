package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface uz {

    public static final class a extends e {
        public b[] b;
        public C0097a c;
        public String[] d;

        /* renamed from: com.yandex.metrica.impl.ob.uz$a$a reason: collision with other inner class name */
        public static final class C0097a extends e {
            public int b;
            public int c;

            public C0097a() {
                d();
            }

            public C0097a d() {
                this.b = 0;
                this.c = -1;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(2, this.b);
                bVar.a(3, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.d(2, this.b) + b.d(3, this.c);
            }

            /* renamed from: b */
            public C0097a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a != 0) {
                        if (a != 16) {
                            if (a == 24) {
                                int g = aVar.g();
                                switch (g) {
                                    case -1:
                                    case 0:
                                    case 1:
                                        this.c = g;
                                        break;
                                }
                            } else if (!g.a(aVar, a)) {
                                return this;
                            }
                        } else {
                            int g2 = aVar.g();
                            switch (g2) {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                    this.b = g2;
                                    break;
                            }
                        }
                    } else {
                        return this;
                    }
                }
            }
        }

        public static final class b extends e {
            private static volatile b[] d;
            public String b;
            public boolean c;

            public static b[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new b[0];
                        }
                    }
                }
                return d;
            }

            public b() {
                e();
            }

            public b e() {
                this.b = "";
                this.c = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public b a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = b.d();
            this.c = null;
            this.d = g.f;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (b bVar2 : this.b) {
                    if (bVar2 != null) {
                        bVar.a(1, (e) bVar2);
                    }
                }
            }
            if (this.c != null) {
                bVar.a(2, (e) this.c);
            }
            if (this.d != null && this.d.length > 0) {
                for (String str : this.d) {
                    if (str != null) {
                        bVar.a(3, str);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null && this.b.length > 0) {
                int i = c2;
                for (b bVar : this.b) {
                    if (bVar != null) {
                        i += b.b(1, (e) bVar);
                    }
                }
                c2 = i;
            }
            if (this.c != null) {
                c2 += b.b(2, (e) this.c);
            }
            if (this.d == null || this.d.length <= 0) {
                return c2;
            }
            int i2 = 0;
            int i3 = 0;
            for (String str : this.d) {
                if (str != null) {
                    i3++;
                    i2 += b.b(str);
                }
            }
            return c2 + i2 + (i3 * 1);
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    int b2 = g.b(aVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    b[] bVarArr = new b[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, bVarArr, 0, length);
                    }
                    while (length < bVarArr.length - 1) {
                        bVarArr[length] = new b();
                        aVar.a((e) bVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    bVarArr[length] = new b();
                    aVar.a((e) bVarArr[length]);
                    this.b = bVarArr;
                } else if (a == 18) {
                    if (this.c == null) {
                        this.c = new C0097a();
                    }
                    aVar.a((e) this.c);
                } else if (a == 26) {
                    int b3 = g.b(aVar, 26);
                    int length2 = this.d == null ? 0 : this.d.length;
                    String[] strArr = new String[(b3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.d, 0, strArr, 0, length2);
                    }
                    while (length2 < strArr.length - 1) {
                        strArr[length2] = aVar.i();
                        aVar.a();
                        length2++;
                    }
                    strArr[length2] = aVar.i();
                    this.d = strArr;
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
