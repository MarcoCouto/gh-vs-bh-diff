package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.p.a.C0090a;
import com.yandex.metrica.impl.ob.qm.a;

public class rb {
    @NonNull
    public final a a;
    @Nullable
    private Long b;
    private long c;
    private long d;
    @NonNull
    private Location e;
    @NonNull
    private C0090a f;

    public rb(@NonNull a aVar, long j, long j2, @NonNull Location location, @NonNull C0090a aVar2) {
        this(aVar, j, j2, location, aVar2, null);
    }

    public rb(@NonNull a aVar, long j, long j2, @NonNull Location location, @NonNull C0090a aVar2, @Nullable Long l) {
        this.a = aVar;
        this.b = l;
        this.c = j;
        this.d = j2;
        this.e = location;
        this.f = aVar2;
    }

    @Nullable
    public Long a() {
        return this.b;
    }

    public long b() {
        return this.c;
    }

    @NonNull
    public Location c() {
        return this.e;
    }

    public long d() {
        return this.d;
    }

    @NonNull
    public C0090a e() {
        return this.f;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LocationWrapper{collectionMode=");
        sb.append(this.a);
        sb.append(", mIncrementalId=");
        sb.append(this.b);
        sb.append(", mReceiveTimestamp=");
        sb.append(this.c);
        sb.append(", mReceiveElapsedRealtime=");
        sb.append(this.d);
        sb.append(", mLocation=");
        sb.append(this.e);
        sb.append(", mChargeType=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
