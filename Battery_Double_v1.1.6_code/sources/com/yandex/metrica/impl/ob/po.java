package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

class po {
    @NonNull
    public final Context a;
    @NonNull
    public final acu b;
    @NonNull
    public final so c;

    po(@NonNull Context context, @NonNull acu acu, @NonNull so soVar) {
        this.a = context;
        this.b = acu;
        this.c = soVar;
    }
}
