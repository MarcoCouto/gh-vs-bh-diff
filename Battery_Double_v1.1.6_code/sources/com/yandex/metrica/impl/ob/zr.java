package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class zr {
    @NonNull
    private final Map<Float, Integer> a = new HashMap();

    public zr(@NonNull TreeSet<Float> treeSet) {
        int i = 0;
        for (Float put : treeSet.descendingSet()) {
            this.a.put(put, Integer.valueOf(i));
            i++;
        }
    }

    public void a(@NonNull aad aad) {
        aad.l = (Integer) this.a.get(aad.f);
    }
}
