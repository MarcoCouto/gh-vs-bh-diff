package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

class bt implements UncaughtExceptionHandler {
    public static final AtomicBoolean a = new AtomicBoolean();
    private final CopyOnWriteArrayList<la> b;
    private final UncaughtExceptionHandler c;
    private final eg d;
    @NonNull
    private final am e;

    public bt(UncaughtExceptionHandler uncaughtExceptionHandler, @NonNull Context context) {
        this(uncaughtExceptionHandler, new am(context));
    }

    @VisibleForTesting
    bt(UncaughtExceptionHandler uncaughtExceptionHandler, @NonNull am amVar) {
        this.d = new eg();
        this.b = new CopyOnWriteArrayList<>();
        this.c = uncaughtExceptionHandler;
        this.e = amVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            a.set(true);
            ld ldVar = new ld(th, new ky(new ee().a(thread), this.d.a(thread)), null, this.e.a(), this.e.b());
            a(ldVar);
        } finally {
            if (this.c != null) {
                this.c.uncaughtException(thread, th);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull ld ldVar) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((la) it.next()).a(ldVar);
        }
    }

    public void a(la laVar) {
        this.b.add(laVar);
    }
}
