package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public abstract class hk {
    @NonNull
    private final ey a;

    public abstract boolean a(@NonNull aa aaVar, @NonNull gj gjVar);

    hk(@NonNull ey eyVar) {
        this.a = eyVar;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public ey a() {
        return this.a;
    }
}
