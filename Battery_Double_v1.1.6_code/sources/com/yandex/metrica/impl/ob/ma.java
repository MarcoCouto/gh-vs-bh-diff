package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public class ma {
    @NonNull
    private final me a;
    @NonNull
    private String b;

    public ma(lu luVar, @NonNull String str) {
        this((me) new mh(luVar), str);
    }

    @VisibleForTesting
    ma(@NonNull me meVar, @NonNull String str) {
        this.a = meVar;
        this.b = str;
    }

    @Nullable
    public List<sr> a() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        try {
            sQLiteDatabase = this.a.a();
            if (sQLiteDatabase != null) {
                try {
                    cursor = sQLiteDatabase.query(this.b, null, null, null, null, null, null);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                ArrayList arrayList = new ArrayList();
                                do {
                                    arrayList.add(new sr(cursor.getString(cursor.getColumnIndex("name")), cursor.getLong(cursor.getColumnIndex("granted")) == 1));
                                } while (cursor.moveToNext());
                                this.a.a(sQLiteDatabase);
                                dl.a(cursor);
                                return arrayList;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            this.a.a(sQLiteDatabase);
                            dl.a(cursor);
                            throw th;
                        }
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    cursor = null;
                    th = th4;
                    this.a.a(sQLiteDatabase);
                    dl.a(cursor);
                    throw th;
                }
                this.a.a(sQLiteDatabase);
                dl.a(cursor);
                return null;
            }
            cursor = null;
        } catch (Throwable th5) {
            cursor = null;
            th = th5;
            sQLiteDatabase = null;
            this.a.a(sQLiteDatabase);
            dl.a(cursor);
            throw th;
        }
        this.a.a(sQLiteDatabase);
        dl.a(cursor);
        return null;
    }

    public void b() {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        SQLiteDatabase sQLiteDatabase2 = null;
        try {
            sQLiteDatabase = this.a.a();
            if (sQLiteDatabase != null) {
                try {
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
                } catch (Throwable th2) {
                    th = th2;
                    this.a.a(sQLiteDatabase);
                    throw th;
                }
            }
            this.a.a(sQLiteDatabase);
        } catch (Throwable th3) {
            Throwable th4 = th3;
            sQLiteDatabase = null;
            th = th4;
            this.a.a(sQLiteDatabase);
            throw th;
        }
    }
}
