package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class xt {
    /* access modifiers changed from: private */
    public final acb<String, xy> a = new acb<>();
    /* access modifiers changed from: private */
    public final HashMap<String, yc> b = new HashMap<>();
    private final ya c = new ya() {
        public void a(@NonNull String str, @NonNull yb ybVar) {
            for (xy a2 : a(str)) {
                a2.a(ybVar);
            }
        }

        public void a(@NonNull String str, @NonNull xv xvVar, @Nullable yb ybVar) {
            for (xy a2 : a(str)) {
                a2.a(xvVar, ybVar);
            }
        }

        @NonNull
        public List<xy> a(@NonNull String str) {
            synchronized (xt.this.b) {
                Collection a2 = xt.this.a.a(str);
                if (a2 == null) {
                    ArrayList arrayList = new ArrayList();
                    return arrayList;
                }
                ArrayList arrayList2 = new ArrayList(a2);
                return arrayList2;
            }
        }
    };

    private static final class a {
        static final xt a = new xt();
    }

    public static final xt a() {
        return a.a;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public yc a(@NonNull Context context, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.wf.a aVar) {
        yc ycVar = (yc) this.b.get(fbVar.b());
        boolean z = true;
        if (ycVar == null) {
            synchronized (this.b) {
                ycVar = (yc) this.b.get(fbVar.b());
                if (ycVar == null) {
                    yc b2 = b(context, fbVar, aVar);
                    this.b.put(fbVar.b(), b2);
                    ycVar = b2;
                    z = false;
                }
            }
        }
        if (z) {
            ycVar.a(aVar);
        }
        return ycVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public yc b(@NonNull Context context, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.wf.a aVar) {
        return new yc(context, fbVar.b(), aVar, this.c);
    }

    @NonNull
    public yc a(@NonNull Context context, @NonNull fb fbVar, @NonNull xy xyVar, @NonNull com.yandex.metrica.impl.ob.wf.a aVar) {
        yc a2;
        synchronized (this.b) {
            this.a.a(fbVar.b(), xyVar);
            a2 = a(context, fbVar, aVar);
        }
        return a2;
    }
}
