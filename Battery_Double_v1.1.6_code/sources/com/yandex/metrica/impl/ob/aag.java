package com.yandex.metrica.impl.ob;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class aag implements Parcelable {
    public static final Creator<aag> CREATOR = new Creator<aag>() {
        /* renamed from: a */
        public aag createFromParcel(Parcel parcel) {
            return new aag(parcel);
        }

        /* renamed from: a */
        public aag[] newArray(int i) {
            return new aag[i];
        }
    };
    public final boolean a;
    public final boolean b;
    public final boolean c;
    @Nullable
    public final aap d;
    @Nullable
    public final aah e;
    @Nullable
    public final aah f;

    public int describeContents() {
        return 0;
    }

    public aag(@NonNull yb ybVar) {
        this(ybVar.o.l, ybVar.o.n, ybVar.o.m, ybVar.J, ybVar.K, ybVar.L);
    }

    public aag(boolean z, boolean z2, boolean z3, @Nullable aap aap, @Nullable aah aah, @Nullable aah aah2) {
        this.a = z;
        this.b = z2;
        this.c = z3;
        this.d = aap;
        this.e = aah;
        this.f = aah2;
    }

    public boolean a() {
        return (this.d == null || this.e == null || this.f == null) ? false : true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UiAccessConfig{uiParsingEnabled=");
        sb.append(this.a);
        sb.append(", uiEventSendingEnabled=");
        sb.append(this.b);
        sb.append(", uiCollectingForBridgeEnabled=");
        sb.append(this.c);
        sb.append(", uiParsingConfig=");
        sb.append(this.d);
        sb.append(", uiEventSendingConfig=");
        sb.append(this.e);
        sb.append(", uiCollectingForBridgeConfig=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        aag aag = (aag) obj;
        if (this.a != aag.a || this.b != aag.b || this.c != aag.c) {
            return false;
        }
        if (this.d == null ? aag.d != null : !this.d.equals(aag.d)) {
            return false;
        }
        if (this.e == null ? aag.e != null : !this.e.equals(aag.e)) {
            return false;
        }
        if (this.f != null) {
            z = this.f.equals(aag.f);
        } else if (aag.f != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((this.a ? 1 : 0) * true) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.a ? (byte) 1 : 0);
        parcel.writeByte(this.b ? (byte) 1 : 0);
        parcel.writeByte(this.c ? (byte) 1 : 0);
        parcel.writeParcelable(this.d, i);
        parcel.writeParcelable(this.e, i);
        parcel.writeParcelable(this.f, i);
    }

    protected aag(Parcel parcel) {
        boolean z = false;
        this.a = parcel.readByte() != 0;
        this.b = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z = true;
        }
        this.c = z;
        this.d = (aap) parcel.readParcelable(aap.class.getClassLoader());
        this.e = (aah) parcel.readParcelable(aah.class.getClassLoader());
        this.f = (aah) parcel.readParcelable(aah.class.getClassLoader());
    }
}
