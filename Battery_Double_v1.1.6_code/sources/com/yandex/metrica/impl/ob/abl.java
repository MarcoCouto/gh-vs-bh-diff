package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.uu.c.e;
import com.yandex.metrica.impl.ob.uu.c.e.a;

public class abl extends abe {
    private static final int[] a = {3, 6, 4};
    private static final abl b = new abl();

    public String f() {
        return "AppMetrica";
    }

    public abl(@Nullable String str) {
        super(str);
    }

    public abl() {
        this("");
    }

    public static abl h() {
        return b;
    }

    public void a(aa aaVar, String str) {
        if (al.b(aaVar.g())) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(aaVar.d());
            if (al.c(aaVar.g()) && !TextUtils.isEmpty(aaVar.e())) {
                sb.append(" with value ");
                sb.append(aaVar.e());
            }
            a(sb.toString());
        }
    }

    public void a(a aVar, String str) {
        if (a(aVar)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": ");
            sb.append(b(aVar));
            a(sb.toString());
        }
    }

    private boolean a(a aVar) {
        for (int i : a) {
            if (aVar.d == i) {
                return true;
            }
        }
        return false;
    }

    private String b(a aVar) {
        if (aVar.d == 3 && TextUtils.isEmpty(aVar.e)) {
            return "Native crash of app";
        }
        if (aVar.d != 4) {
            return aVar.e;
        }
        StringBuilder sb = new StringBuilder(aVar.e);
        if (aVar.f != null) {
            String str = new String(aVar.f);
            if (!TextUtils.isEmpty(str)) {
                sb.append(" with value ");
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public void a(e eVar, String str) {
        for (a a2 : eVar.d) {
            a(a2, str);
        }
    }
}
