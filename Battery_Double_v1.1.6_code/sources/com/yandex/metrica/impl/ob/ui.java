package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.uy.a.C0096a;
import com.yandex.metrica.impl.ob.uy.a.b;
import com.yandex.metrica.impl.ob.uy.a.c;

public abstract class ui implements ty, ur {
    @NonNull
    private final String a;
    private final int b;
    @NonNull
    private final adw<String> c;
    @NonNull
    private final ua d;
    @NonNull
    private abl e = abd.a();

    ui(int i, @NonNull String str, @NonNull adw<String> adw, @NonNull ua uaVar) {
        this.b = i;
        this.a = str;
        this.c = adw;
        this.d = uaVar;
    }

    @NonNull
    public String c() {
        return this.a;
    }

    public int d() {
        return this.b;
    }

    @NonNull
    public ua e() {
        return this.d;
    }

    @NonNull
    public final C0096a a() {
        C0096a aVar = new C0096a();
        aVar.c = d();
        aVar.b = c().getBytes();
        aVar.e = new c();
        aVar.d = new b();
        return aVar;
    }

    public void a(@NonNull abl abl) {
        this.e = abl;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        adu a2 = this.c.a(c());
        if (a2.a()) {
            return true;
        }
        if (this.e.c()) {
            abl abl = this.e;
            StringBuilder sb = new StringBuilder();
            sb.append("Attribute ");
            sb.append(c());
            sb.append(" of type ");
            sb.append(up.a(d()));
            sb.append(" is skipped because ");
            sb.append(a2.b());
            abl.b(sb.toString());
        }
        return false;
    }
}
