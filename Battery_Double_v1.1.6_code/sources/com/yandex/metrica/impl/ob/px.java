package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class px extends pq implements pz {
    @Nullable
    private volatile py a;

    public px(@NonNull act act, @Nullable py pyVar) {
        super(act);
        this.a = pyVar;
    }

    public void a(@NonNull Runnable runnable) {
        py pyVar = this.a;
        if (pyVar != null && pyVar.b != null) {
            a(runnable, pyVar.b.b);
        }
    }

    public void a(@Nullable py pyVar) {
        this.a = pyVar;
    }
}
