package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

class cm implements Runnable {
    @NonNull
    private final Context a;
    private final aa b;
    private final Bundle c;
    @NonNull
    private final gf d;

    cm(@NonNull Context context, aa aaVar, Bundle bundle, @NonNull gf gfVar) {
        this.a = context;
        this.b = aaVar;
        this.c = bundle;
        this.d = gfVar;
    }

    public void run() {
        et etVar = new et(this.c);
        if (!et.a(etVar, this.a)) {
            ge a2 = ge.a(etVar);
            if (a2 != null) {
                ew ewVar = new ew(etVar);
                this.d.a(a2, ewVar).a(this.b, ewVar);
            }
        }
    }
}
