package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.yandex.metrica.impl.ob.cg.a;
import com.yandex.metrica.impl.ob.vf.a.C0111a;
import com.yandex.metrica.impl.ob.vf.a.b;
import com.yandex.metrica.impl.ob.vf.a.b.C0112a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class wy {
    private static final Map<C0112a, String> a = Collections.unmodifiableMap(new HashMap<C0112a, String>() {
        {
            put(C0112a.COMPLETE, "complete");
            put(C0112a.ERROR, "error");
            put(C0112a.OFFLINE, "offline");
            put(C0112a.INCOMPATIBLE_NETWORK_TYPE, "incompatible_network_type");
        }
    });
    private static final Map<a, String> b = Collections.unmodifiableMap(new HashMap<a, String>() {
        {
            put(a.WIFI, "wifi");
            put(a.CELL, "cell");
            put(a.OFFLINE, "offline");
            put(a.UNDEFINED, "undefined");
        }
    });

    public String a(@NonNull b bVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("id", bVar.a().a);
            jSONObject.putOpt("url", bVar.a().b);
            jSONObject.putOpt("status", a.get(bVar.b()));
            jSONObject.putOpt("code", bVar.d());
            if (!dl.a(bVar.e())) {
                jSONObject.putOpt(TtmlNode.TAG_BODY, a(bVar.e()));
            } else if (!dl.a(bVar.h())) {
                jSONObject.putOpt(TtmlNode.TAG_BODY, a(bVar.h()));
            }
            jSONObject.putOpt("headers", a(bVar.f()));
            jSONObject.putOpt("error", a(bVar.g()));
            jSONObject.putOpt("network_type", b.get(bVar.c()));
            return jSONObject.toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    public String a(@NonNull C0111a aVar) {
        try {
            return new JSONObject().put("id", aVar.a).toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    @Nullable
    private JSONObject a(@Nullable Map<String, List<String>> map) throws JSONException {
        if (dl.a((Map) map)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            if (!dl.a((Collection) entry.getValue())) {
                List<String> a2 = dl.a((List) entry.getValue(), 10);
                ArrayList arrayList = new ArrayList();
                for (String str2 : a2) {
                    if (!TextUtils.isEmpty(str2)) {
                        arrayList.add(dl.a(str2, 100));
                    }
                }
                jSONObject.putOpt(str, TextUtils.join(",", arrayList));
            }
        }
        return jSONObject;
    }

    @Nullable
    private String a(@NonNull byte[] bArr) {
        return Base64.encodeToString(bArr, 0);
    }

    @Nullable
    private String a(@Nullable Throwable th) {
        if (th == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(th.toString());
        sb.append("\n");
        sb.append(Log.getStackTraceString(th));
        return sb.toString();
    }
}
