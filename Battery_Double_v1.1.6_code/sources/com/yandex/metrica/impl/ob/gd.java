package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.ew.a;

class gd extends gq {
    gd() {
    }

    @NonNull
    /* renamed from: a */
    public fe d(@NonNull Context context, @NonNull fb fbVar, @NonNull a aVar, @NonNull yc ycVar) {
        ev evVar = new ev(context, ycVar.e(), fbVar, aVar, new ga(as.a().i()), new yi());
        return evVar;
    }

    @NonNull
    /* renamed from: b */
    public gv c(@NonNull Context context, @NonNull fb fbVar, @NonNull a aVar, @NonNull yc ycVar) {
        gv gvVar = new gv(context, fbVar, aVar, ycVar.e(), new yi(), CounterConfiguration.a.APPMETRICA);
        return gvVar;
    }
}
