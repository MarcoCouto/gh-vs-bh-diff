package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a;
import com.yandex.metrica.impl.ob.xi.a;
import com.yandex.metrica.impl.ob.xi.a.C0113a;
import com.yandex.metrica.impl.ob.xi.a.b;
import com.yandex.metrica.impl.ob.xi.a.c;

public class oh implements np<a, C0103a> {
    @NonNull
    private final og a;
    @NonNull
    private final ok b;
    @NonNull
    private final ol c;

    public oh() {
        this(new og(), new ok(), new ol());
    }

    @NonNull
    /* renamed from: a */
    public C0103a b(@NonNull a aVar) {
        C0103a aVar2 = new C0103a();
        if (!TextUtils.isEmpty(aVar.a)) {
            aVar2.b = aVar.a;
        }
        if (!TextUtils.isEmpty(aVar.b)) {
            aVar2.c = aVar.b;
        }
        if (aVar.c != null) {
            aVar2.d = this.a.b(aVar.c);
        }
        if (aVar.d != null) {
            aVar2.e = this.b.b(aVar.d);
        }
        if (aVar.e != null) {
            aVar2.f = this.c.b(aVar.e);
        }
        return aVar2;
    }

    @NonNull
    public a a(@NonNull C0103a aVar) {
        C0113a aVar2;
        b bVar;
        c cVar;
        String str = TextUtils.isEmpty(aVar.b) ? null : aVar.b;
        String str2 = TextUtils.isEmpty(aVar.c) ? null : aVar.c;
        if (aVar.d == null) {
            aVar2 = null;
        } else {
            aVar2 = this.a.a(aVar.d);
        }
        if (aVar.e == null) {
            bVar = null;
        } else {
            bVar = this.b.a(aVar.e);
        }
        if (aVar.f == null) {
            cVar = null;
        } else {
            cVar = this.c.a(aVar.f);
        }
        a aVar3 = new a(str, str2, aVar2, bVar, cVar);
        return aVar3;
    }

    @VisibleForTesting
    oh(@NonNull og ogVar, @NonNull ok okVar, @NonNull ol olVar) {
        this.a = ogVar;
        this.b = okVar;
        this.c = olVar;
    }
}
