package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.io.File;
import java.io.FilenameFilter;

public class bv {
    @NonNull
    private final Context a;
    @NonNull
    private final String b;
    @NonNull
    private final bw c;
    /* access modifiers changed from: private */
    @NonNull
    public final mo d;
    @NonNull
    private final an e;

    public bv(@NonNull Context context, @NonNull bw bwVar, @NonNull mo moVar) {
        this(context, new an(), bwVar, moVar);
    }

    @VisibleForTesting
    bv(@NonNull Context context, @NonNull an anVar, @NonNull bw bwVar, @NonNull mo moVar) {
        this.a = context;
        this.e = anVar;
        this.b = anVar.c(context).getAbsolutePath();
        this.c = bwVar;
        this.d = moVar;
    }

    public synchronized void a() {
        if (ax.a() && !this.d.m()) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.a.getFilesDir().getAbsolutePath());
            sb.append("/");
            sb.append("YandexMetricaNativeCrashes");
            a(sb.toString(), new aby<Boolean>() {
                public void a(Boolean bool) {
                    bv.this.d.n();
                }
            });
        }
        a(this.b, new aby<Boolean>() {
            public void a(Boolean bool) {
            }
        });
    }

    private void a(@NonNull String str, @NonNull aby<Boolean> aby) {
        String[] a2;
        for (String str2 : a(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("/");
            sb.append(str2);
            a(sb.toString(), aby, false);
        }
    }

    private String[] a(String str) {
        File a2 = this.e.a(str);
        if (!a2.mkdir() && !a2.exists()) {
            return new String[0];
        }
        String[] list = a2.list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.endsWith(".dmp");
            }
        });
        if (list == null) {
            list = new String[0];
        }
        return list;
    }

    public void a(@NonNull String str, @NonNull aby<Boolean> aby, boolean z) {
        try {
            String b2 = ax.b(ax.a(str));
            if (b2 != null) {
                if (z) {
                    this.c.a(b2);
                } else {
                    this.c.b(b2);
                }
            }
            aby.a(Boolean.valueOf(true));
        } catch (Throwable th) {
            this.e.a(str).delete();
            throw th;
        }
        this.e.a(str).delete();
    }
}
