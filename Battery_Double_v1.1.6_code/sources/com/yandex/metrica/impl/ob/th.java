package com.yandex.metrica.impl.ob;

public class th {
    private final String a;
    private final String b;

    public th(String str) {
        this(str, null);
    }

    public th(String str, String str2) {
        this.a = str;
        this.b = a(str2);
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public final String a(String str) {
        if (str == null) {
            return this.a;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(str);
        return sb.toString();
    }
}
