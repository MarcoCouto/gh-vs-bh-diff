package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.al.a;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class hg extends hi<hu> {
    private final ix a;
    private final Map<a, iq<hu>> b = b();
    @Nullable
    private in<hu> c = new im(this.a);
    @Nullable
    private in<hu> d;

    public hg(fe feVar) {
        this.a = new ix(feVar);
    }

    private HashMap<a, iq<hu>> b() {
        HashMap<a, iq<hu>> hashMap = new HashMap<>();
        hashMap.put(a.EVENT_TYPE_ACTIVATION, new il(this.a));
        hashMap.put(a.EVENT_TYPE_START, new ja(this.a));
        hashMap.put(a.EVENT_TYPE_REGULAR, new iu(this.a));
        is isVar = new is(this.a);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_USER, isVar);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, isVar);
        hashMap.put(a.EVENT_TYPE_SEND_REFERRER, isVar);
        hashMap.put(a.EVENT_TYPE_STATBOX, isVar);
        hashMap.put(a.EVENT_TYPE_CUSTOM_EVENT, isVar);
        hashMap.put(a.EVENT_TYPE_APP_OPEN, new iw(this.a));
        hashMap.put(a.EVENT_TYPE_PURGE_BUFFER, new it(this.a));
        hashMap.put(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, new iz(this.a, this.a.l()));
        hashMap.put(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, new ip(this.a));
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, new jc(this.a));
        jb jbVar = new jb(this.a);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED, jbVar);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, jbVar);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, jbVar);
        hashMap.put(a.EVENT_TYPE_ANR, isVar);
        hashMap.put(a.EVENT_TYPE_IDENTITY, new ir(this.a));
        hashMap.put(a.EVENT_TYPE_SET_USER_INFO, new iy(this.a));
        hashMap.put(a.EVENT_TYPE_REPORT_USER_INFO, new iz(this.a, this.a.g()));
        hashMap.put(a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, new iz(this.a, this.a.i()));
        hashMap.put(a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, new iz(this.a, this.a.j()));
        hashMap.put(a.EVENT_TYPE_SEND_USER_PROFILE, isVar);
        hashMap.put(a.EVENT_TYPE_SET_USER_PROFILE_ID, new iz(this.a, this.a.o()));
        hashMap.put(a.EVENT_TYPE_SEND_REVENUE_EVENT, isVar);
        hashMap.put(a.EVENT_TYPE_IDENTITY_LIGHT, isVar);
        hashMap.put(a.EVENT_TYPE_CLEANUP, isVar);
        hashMap.put(a.EVENT_TYPE_VIEW_TREE, isVar);
        return hashMap;
    }

    public void a(a aVar, iq<hu> iqVar) {
        this.b.put(aVar, iqVar);
    }

    public ix a() {
        return this.a;
    }

    public hf<hu> a(int i) {
        LinkedList linkedList = new LinkedList();
        a a2 = a.a(i);
        if (this.c != null) {
            this.c.a(a2, linkedList);
        }
        iq iqVar = (iq) this.b.get(a2);
        if (iqVar != null) {
            iqVar.a(linkedList);
        }
        if (this.d != null) {
            this.d.a(a2, linkedList);
        }
        return new he(linkedList);
    }
}
