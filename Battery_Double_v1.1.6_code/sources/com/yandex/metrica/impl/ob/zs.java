package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseIntArray;

public class zs implements zw {
    private final SparseIntArray a = new SparseIntArray();
    private final int b;

    public zs(int i) {
        this.b = i;
    }

    public void a(@NonNull aam aam) {
        this.a.put(aam.p, this.a.get(aam.p) + 1);
    }

    public int a(int i) {
        return this.b - ((Integer) abw.b(Integer.valueOf(this.a.get(i)), Integer.valueOf(0))).intValue();
    }
}
