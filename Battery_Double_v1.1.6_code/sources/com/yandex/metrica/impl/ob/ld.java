package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import java.util.List;

public class ld {
    @Nullable
    public final String a;
    @Nullable
    public final Throwable b;
    @Nullable
    public final ky c;
    @Nullable
    public final List<StackTraceElement> d;
    @Nullable
    public final String e;
    @Nullable
    public final Boolean f;

    public ld(@Nullable Throwable th, @Nullable ky kyVar, @Nullable List<StackTraceElement> list, @Nullable String str, @Nullable Boolean bool) {
        this.b = th;
        if (th == null) {
            this.a = "";
        } else {
            this.a = th.getClass().getName();
        }
        this.c = kyVar;
        this.d = list;
        this.e = str;
        this.f = bool;
    }

    @Nullable
    @Deprecated
    public Throwable a() {
        return this.b;
    }

    public String toString() {
        StackTraceElement[] b2;
        StringBuilder sb = new StringBuilder();
        if (this.b != null) {
            for (StackTraceElement stackTraceElement : dl.b(this.b)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("at ");
                sb2.append(stackTraceElement.getClassName());
                sb2.append(".");
                sb2.append(stackTraceElement.getMethodName());
                sb2.append("(");
                sb2.append(stackTraceElement.getFileName());
                sb2.append(":");
                sb2.append(stackTraceElement.getLineNumber());
                sb2.append(")\n");
                sb.append(sb2.toString());
            }
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("UnhandledException{errorName='");
        sb3.append(this.a);
        sb3.append('\'');
        sb3.append(", exception=");
        sb3.append(this.b);
        sb3.append("\n");
        sb3.append(sb.toString());
        sb3.append('}');
        return sb3.toString();
    }
}
