package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.ej;

public class el<T extends ej> {
    @NonNull
    private final ek<T> a;
    @Nullable
    private final ei<T> b;

    public static final class a<T extends ej> {
        @NonNull
        final ek<T> a;
        @Nullable
        ei<T> b;

        a(@NonNull ek<T> ekVar) {
            this.a = ekVar;
        }

        @NonNull
        public a<T> a(@NonNull ei<T> eiVar) {
            this.b = eiVar;
            return this;
        }

        @NonNull
        public el<T> a() {
            return new el<>(this);
        }
    }

    private el(@NonNull a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
    }

    public void a(@NonNull ej ejVar) {
        this.a.a(ejVar);
    }

    /* access modifiers changed from: 0000 */
    public final boolean b(@NonNull ej ejVar) {
        if (this.b == null) {
            return false;
        }
        return this.b.a(ejVar);
    }

    @NonNull
    public static <T extends ej> a<T> a(@NonNull ek<T> ekVar) {
        return new a<>(ekVar);
    }
}
