package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.browser.crashreports.a.C0080a;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.a.C0082a;
import com.yandex.metrica.j;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class bj extends o implements av, bm, dm {
    private static final adw<String> f = new ads(new adq("Deeplink"));
    private static final adw<String> g = new ads(new adq("Referral url"));
    private static final Long h = Long.valueOf(TimeUnit.SECONDS.toMillis(5));
    @NonNull
    private final com.yandex.metrica.a i;
    @NonNull
    private final vr j;
    @NonNull
    private final j k;
    @NonNull
    private final xw l;
    @NonNull
    private com.yandex.browser.crashreports.a m;
    @NonNull
    private final aao n;
    @NonNull
    private final mn o;
    @NonNull
    private final a p;
    @Nullable
    private volatile Activity q;
    private final Object r;
    private final AtomicBoolean s;
    /* access modifiers changed from: private */
    public final eg t;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        public aal a(@NonNull aai aai, boolean z) {
            return new aal(aai, z);
        }
    }

    static class b {
        b() {
        }

        /* access modifiers changed from: 0000 */
        public aao a(@NonNull act act, @NonNull mn mnVar, @NonNull bj bjVar, @NonNull xw xwVar) {
            aao aao = new aao(act, mnVar, bjVar, bjVar, xwVar.d());
            return aao;
        }
    }

    public void b(boolean z) {
    }

    @Nullable
    public Activity a() {
        Activity activity;
        synchronized (this.r) {
            activity = this.q;
        }
        return activity;
    }

    public bj(@NonNull Context context, @NonNull eu euVar, @NonNull j jVar, @NonNull cs csVar, @NonNull xw xwVar, @NonNull cq cqVar, @NonNull cq cqVar2, @NonNull mn mnVar) {
        long j2;
        Context context2 = context;
        j jVar2 = jVar;
        co coVar = new co(euVar, new CounterConfiguration(jVar2));
        if (jVar2.sessionTimeout == null) {
            j2 = TimeUnit.SECONDS.toMillis(10);
        } else {
            j2 = (long) jVar2.sessionTimeout.intValue();
        }
        this(context, jVar, csVar, coVar, new com.yandex.metrica.a(j2), new vr(context2), xwVar, new bh(), cqVar, cqVar2, mnVar, dr.a(), new am(context2), new b(), new a());
    }

    @VisibleForTesting
    bj(@NonNull Context context, @NonNull j jVar, @NonNull cs csVar, @NonNull co coVar, @NonNull com.yandex.metrica.a aVar, @NonNull vr vrVar, @NonNull xw xwVar, @NonNull bh bhVar, @NonNull cq cqVar, @NonNull cq cqVar2, @NonNull mn mnVar, @NonNull act act, @NonNull am amVar, @NonNull b bVar, @NonNull a aVar2) {
        j jVar2 = jVar;
        xw xwVar2 = xwVar;
        mn mnVar2 = mnVar;
        act act2 = act;
        super(context, csVar, coVar, amVar);
        this.r = new Object();
        this.s = new AtomicBoolean(false);
        this.t = new eg();
        this.b.a(new ch(jVar2.preloadInfo, this.c));
        this.i = aVar;
        this.j = vrVar;
        this.k = jVar2;
        this.n = bVar.a(act2, mnVar2, this, xwVar2);
        this.l = xwVar2;
        this.o = mnVar2;
        this.p = aVar2;
        this.l.a((dk) this.n);
        boolean booleanValue = ((Boolean) abw.b(jVar2.nativeCrashReporting, Boolean.valueOf(true))).booleanValue();
        this.e.a(booleanValue, this.b);
        if (this.c.c()) {
            this.c.a("Set report native crashes enabled: %b", Boolean.valueOf(booleanValue));
        }
        this.j.a(aVar, this.k, this.k.m, xwVar.b(), this.c);
        this.m = a(act2, bhVar, cqVar, cqVar2);
        if (aau.a(jVar2.l)) {
            g();
        }
        h();
    }

    public void reportError(String str, Throwable th) {
        super.reportError(str, th);
    }

    public final void g() {
        if (this.s.compareAndSet(false, true)) {
            this.m.a();
        }
    }

    public void a(Activity activity) {
        if (activity != null) {
            if (activity.getIntent() != null) {
                String dataString = activity.getIntent().getDataString();
                if (!TextUtils.isEmpty(dataString)) {
                    this.e.a(al.g(dataString, this.c), this.b);
                }
                g(dataString);
            }
        } else if (this.c.c()) {
            this.c.b("Null activity parameter for reportAppOpen(Activity)");
        }
    }

    private void g(@Nullable String str) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("App opened ");
            sb.append(" via deeplink: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    public void e(String str) {
        f.a(str);
        this.e.a(al.g(str, this.c), this.b);
        g(str);
    }

    public void f(String str) {
        g.a(str);
        this.e.a(al.h(str, this.c), this.b);
        h(str);
    }

    private void h(@Nullable String str) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Referral URL received: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    public void a(Application application, @NonNull act act) {
        if (VERSION.SDK_INT >= 14) {
            if (this.c.c()) {
                this.c.a("Enable activity auto tracking");
            }
            b(application, act);
        } else if (this.c.c()) {
            this.c.b("Could not enable activity auto tracking. API level should be more than 14 (ICE_CREAM_SANDWICH)");
        }
    }

    @TargetApi(14)
    private void b(Application application, @NonNull act act) {
        application.registerActivityLifecycleCallbacks(new ad(this, act));
    }

    public void b(Activity activity) {
        a(d(activity));
        this.i.a();
        this.n.a(activity);
        synchronized (this.r) {
            this.q = activity;
        }
    }

    public void c(Activity activity) {
        b(d(activity));
        this.i.b();
        synchronized (this.r) {
            this.q = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public String d(Activity activity) {
        if (activity != null) {
            return activity.getClass().getSimpleName();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void a(j jVar, boolean z) {
        if (z) {
            b();
        }
        b(jVar.i);
        a(jVar.h);
    }

    public void a(Location location) {
        this.b.h().a(location);
        if (this.c.c()) {
            abl abl = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("Set location: %s");
            sb.append(location.toString());
            abl.a(sb.toString(), new Object[0]);
        }
    }

    public void a(boolean z) {
        this.b.h().a(z);
    }

    @NonNull
    private com.yandex.browser.crashreports.a a(@NonNull act act, @NonNull bh bhVar, @NonNull cq cqVar, @NonNull cq cqVar2) {
        final act act2 = act;
        final bh bhVar2 = bhVar;
        final cq cqVar3 = cqVar;
        final cq cqVar4 = cqVar2;
        AnonymousClass1 r1 = new C0080a() {
            public void a() {
                final ky a2 = bj.this.t.a();
                act2.a((Runnable) new Runnable() {
                    public void run() {
                        bj.this.a(a2);
                        if (bhVar2.a(a2.a.f)) {
                            cqVar3.a().a(a2);
                        }
                        if (bhVar2.b(a2.a.f)) {
                            cqVar4.a().a(a2);
                        }
                    }
                });
            }
        };
        return new com.yandex.browser.crashreports.a(r1);
    }

    private void h() {
        this.e.b(this.b.g());
        this.i.a(new C0082a() {
            public void a() {
                bj.this.e.a(bj.this.b.g());
            }

            public void b() {
                bj.this.e.b(bj.this.b.g());
            }
        }, h.longValue());
    }

    public void a(@NonNull JSONObject jSONObject) {
        this.e.a(al.a(jSONObject, this.c), this.b);
    }

    public void a(@NonNull aai aai, boolean z) {
        aal a2 = this.p.a(aai, z);
        zo a3 = this.n.a();
        Activity activity = this.q;
        if ((z || a3.a().isEmpty()) && activity != null) {
            a2.a(true);
            this.n.a(activity, a2, true);
        } else {
            a2.a(a3.b());
        }
        this.o.b(true);
    }
}
