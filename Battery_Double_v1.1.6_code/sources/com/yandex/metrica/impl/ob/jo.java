package com.yandex.metrica.impl.ob;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONObject;

public class jo {
    private final fe a;
    private final jw b;
    private final jq c;
    private long d;
    private long e;
    private AtomicLong f;
    private boolean g;
    private volatile a h;
    private long i;
    private long j;
    private abs k;

    static class a {
        private final String a;
        private final String b;
        private final String c;
        private final String d;
        private final String e;
        private final int f;
        private final int g;

        a(JSONObject jSONObject) {
            this.a = jSONObject.optString("analyticsSdkVersionName", null);
            this.b = jSONObject.optString("kitBuildNumber", null);
            this.c = jSONObject.optString("appVer", null);
            this.d = jSONObject.optString("appBuild", null);
            this.e = jSONObject.optString("osVer", null);
            this.f = jSONObject.optInt("osApiLev", -1);
            this.g = jSONObject.optInt("attribution_id", 0);
        }

        /* access modifiers changed from: 0000 */
        public boolean a(we weVar) {
            return TextUtils.equals(weVar.j(), this.a) && TextUtils.equals(weVar.k(), this.b) && TextUtils.equals(weVar.r(), this.c) && TextUtils.equals(weVar.q(), this.d) && TextUtils.equals(weVar.o(), this.e) && this.f == weVar.p() && this.g == weVar.Y();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("SessionRequestParams{mKitVersionName='");
            sb.append(this.a);
            sb.append('\'');
            sb.append(", mKitBuildNumber='");
            sb.append(this.b);
            sb.append('\'');
            sb.append(", mAppVersion='");
            sb.append(this.c);
            sb.append('\'');
            sb.append(", mAppBuild='");
            sb.append(this.d);
            sb.append('\'');
            sb.append(", mOsVersion='");
            sb.append(this.e);
            sb.append('\'');
            sb.append(", mApiLevel=");
            sb.append(this.f);
            sb.append('}');
            return sb.toString();
        }
    }

    jo(fe feVar, jw jwVar, jq jqVar) {
        this(feVar, jwVar, jqVar, new abs());
    }

    jo(fe feVar, jw jwVar, jq jqVar, abs abs) {
        this.a = feVar;
        this.b = jwVar;
        this.c = jqVar;
        this.k = abs;
        i();
    }

    private void i() {
        this.e = this.c.b(this.k.c());
        this.d = this.c.a(-1);
        this.f = new AtomicLong(this.c.c(0));
        this.g = this.c.a(true);
        this.i = this.c.d(0);
        this.j = this.c.e(this.i - this.e);
    }

    /* access modifiers changed from: protected */
    public jy a() {
        return this.c.a();
    }

    /* access modifiers changed from: protected */
    public int b() {
        return this.c.a(this.a.i().V());
    }

    public long c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public long d() {
        return Math.max(this.i - TimeUnit.MILLISECONDS.toSeconds(this.e), this.j);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(long j2) {
        boolean z = this.d >= 0;
        boolean j3 = j();
        boolean z2 = !a(j2, this.k.c());
        if (!z || !j3 || !z2) {
            return false;
        }
        return true;
    }

    private boolean j() {
        a k2 = k();
        if (k2 != null) {
            return k2.a(this.a.i());
        }
        return false;
    }

    private long d(long j2) {
        return TimeUnit.MILLISECONDS.toSeconds(j2 - this.e);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(long j2, long j3) {
        long j4 = this.i;
        boolean z = TimeUnit.MILLISECONDS.toSeconds(j3) < j4;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j2) - j4;
        long d2 = d(j2);
        if (z || seconds >= ((long) b()) || d2 >= jr.c) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void e() {
        this.b.a();
        this.h = null;
    }

    /* access modifiers changed from: 0000 */
    public void b(long j2) {
        jw jwVar = this.b;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j2);
        this.i = seconds;
        jwVar.b(seconds).h();
    }

    /* access modifiers changed from: 0000 */
    public long c(long j2) {
        jw jwVar = this.b;
        long d2 = d(j2);
        this.j = d2;
        jwVar.c(d2);
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public long f() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public long g() {
        long andIncrement = this.f.getAndIncrement();
        this.b.a(this.f.get()).h();
        return andIncrement;
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return this.g && c() > 0;
    }

    public void a(boolean z) {
        if (this.g != z) {
            this.g = z;
            this.b.a(this.g).h();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:3|4|(3:6|7|(1:9))|10|11) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0033 */
    private a k() {
        if (this.h == null) {
            synchronized (this) {
                if (this.h == null) {
                    String asString = this.a.j().b(c(), a()).getAsString("report_request_parameters");
                    if (!TextUtils.isEmpty(asString)) {
                        this.h = new a(new JSONObject(asString));
                    }
                }
            }
        }
        return this.h;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Session{mId=");
        sb.append(this.d);
        sb.append(", mInitTime=");
        sb.append(this.e);
        sb.append(", mCurrentReportId=");
        sb.append(this.f);
        sb.append(", mSessionRequestParams=");
        sb.append(this.h);
        sb.append(", mSleepStartSeconds=");
        sb.append(this.i);
        sb.append('}');
        return sb.toString();
    }
}
