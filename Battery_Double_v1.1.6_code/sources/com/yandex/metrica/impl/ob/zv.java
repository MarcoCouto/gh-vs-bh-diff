package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class zv {
    @NonNull
    private final List<zv> a = new ArrayList();
    @NonNull
    private final aam b;

    static class a {
        final int a;
        final int b;
        @Nullable
        final JSONObject c;

        a(int i, int i2, @Nullable JSONObject jSONObject) {
            this.a = i;
            this.b = i2;
            this.c = jSONObject;
        }
    }

    public zv(@NonNull aam aam) {
        this.b = aam;
    }

    public void a(@NonNull zv zvVar) {
        this.a.add(zvVar);
    }

    @NonNull
    public aam a() {
        return this.b;
    }

    @NonNull
    public a a(@NonNull aah aah, int i, int i2) {
        int i3;
        JSONObject jSONObject = new JSONObject();
        int i4 = i2 + 1;
        try {
            if (aah.f || this.b.a()) {
                jSONObject = this.b.c(aah);
            }
            i3 = jSONObject.toString().getBytes().length + i;
            try {
                JSONArray jSONArray = new JSONArray();
                jSONObject.put("ch", jSONArray);
                i3 += ",\"\":[]".length() + "ch".length();
                if (i3 <= aah.k) {
                    if (i4 <= aah.j) {
                        Iterator it = this.a.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            a a2 = ((zv) it.next()).a(aah, jSONArray, i3, i4);
                            if (a2.a == 0) {
                                break;
                            }
                            i4 += a2.b;
                            i3 += a2.a;
                        }
                        return new a(i3 - i, i4 - i2, jSONObject);
                    }
                }
                return new a(0, 0, null);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            i3 = i;
        }
    }

    @NonNull
    private a a(@NonNull aah aah, @NonNull JSONArray jSONArray, int i, int i2) {
        a a2 = a(aah, i + 1, i2);
        if (a2.c != null) {
            jSONArray.put(a2.c);
        }
        return a2;
    }
}
