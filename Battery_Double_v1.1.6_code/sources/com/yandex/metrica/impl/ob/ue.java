package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.uy.a.C0096a;

public final class ue extends ud<Double> {
    public ue(@NonNull String str, double d) {
        super(2, str, Double.valueOf(d), new ug(), new uc(new uh(new ade(100))));
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0096a aVar) {
        aVar.e.d += ((Double) b()).doubleValue();
    }
}
