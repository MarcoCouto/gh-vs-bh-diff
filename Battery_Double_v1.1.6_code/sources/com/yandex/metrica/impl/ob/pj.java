package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class pj extends pq implements qn {
    @Nullable
    private volatile pt a;

    public pj(@NonNull act act, @Nullable pt ptVar) {
        super(act);
        this.a = ptVar;
    }

    public void a(@NonNull Runnable runnable) {
        pt ptVar = this.a;
        if (ptVar != null && ptVar.b != null) {
            a(runnable, ptVar.b.a);
        }
    }

    public void a(@Nullable pt ptVar) {
        this.a = ptVar;
    }
}
