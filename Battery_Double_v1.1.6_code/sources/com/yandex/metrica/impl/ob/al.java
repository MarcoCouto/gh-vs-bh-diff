package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.InputDeviceCompat;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public final class al {
    public static final EnumSet<a> a = EnumSet.of(a.EVENT_TYPE_INIT, new a[]{a.EVENT_TYPE_FIRST_ACTIVATION, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_APP_UPDATE, a.EVENT_TYPE_CLEANUP});
    private static final EnumSet<a> b = EnumSet.of(a.EVENT_TYPE_UNDEFINED, new a[]{a.EVENT_TYPE_PURGE_BUFFER, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, a.EVENT_TYPE_ACTIVATION, a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH});
    private static final EnumSet<a> c = EnumSet.of(a.EVENT_TYPE_SET_USER_INFO, new a[]{a.EVENT_TYPE_REPORT_USER_INFO, a.EVENT_TYPE_IDENTITY, a.EVENT_TYPE_UNDEFINED, a.EVENT_TYPE_INIT, a.EVENT_TYPE_APP_UPDATE, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_ALIVE, a.EVENT_TYPE_STARTUP, a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, a.EVENT_TYPE_ACTIVATION, a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH});
    private static final EnumSet<a> d = EnumSet.of(a.EVENT_TYPE_UPDATE_FOREGROUND_TIME, a.EVENT_TYPE_SET_USER_INFO, a.EVENT_TYPE_REPORT_USER_INFO, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE);
    private static final EnumSet<a> e = EnumSet.of(a.EVENT_TYPE_EXCEPTION_UNHANDLED, new a[]{a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, a.EVENT_TYPE_EXCEPTION_USER, a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, a.EVENT_TYPE_REGULAR});
    private static final EnumSet<a> f = EnumSet.of(a.EVENT_TYPE_DIAGNOSTIC, a.EVENT_TYPE_DIAGNOSTIC_STATBOX, a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING);
    private static final EnumSet<a> g = EnumSet.of(a.EVENT_TYPE_REGULAR);
    private static final EnumSet<a> h = EnumSet.of(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH);

    @SuppressLint({"UseSparseArrays"})
    public enum a {
        EVENT_TYPE_UNDEFINED(-1, "Unrecognized action"),
        EVENT_TYPE_INIT(0, "First initialization event"),
        EVENT_TYPE_REGULAR(1, "Regular event"),
        EVENT_TYPE_UPDATE_FOREGROUND_TIME(3, "Update foreground time"),
        EVENT_TYPE_EXCEPTION_USER(5, "Error from developer"),
        EVENT_TYPE_ALIVE(7, "App is still alive"),
        EVENT_TYPE_SET_USER_INFO(9, "User info"),
        EVENT_TYPE_REPORT_USER_INFO(10, "Report user info"),
        EVENT_TYPE_SEND_USER_PROFILE(40961, "Send user profile"),
        EVENT_TYPE_SET_USER_PROFILE_ID(40962, "Set user profile ID"),
        EVENT_TYPE_SEND_REVENUE_EVENT(40976, "Send revenue event"),
        EVENT_TYPE_PURGE_BUFFER(256, "Forcible buffer clearing"),
        EVENT_TYPE_PREV_SESSION_NATIVE_CRASH(768, "Native crash of app, read from file"),
        EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH(769, "Native crash of app, caught by FileObserver"),
        EVENT_TYPE_STARTUP(1536, "Sending the startup due to lack of data"),
        EVENT_TYPE_IDENTITY(1792, "System identification"),
        EVENT_TYPE_IDENTITY_LIGHT(1793, "Identity light"),
        EVENT_TYPE_DIAGNOSTIC(2048, "Diagnostic event"),
        EVENT_TYPE_DIAGNOSTIC_STATBOX(2049, "Diagnostic statbox event"),
        EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING(2050, "Disable stat send"),
        EVENT_TYPE_STATBOX(2304, "Event with statistical data"),
        EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST(4096, "Referrer received"),
        EVENT_TYPE_SEND_REFERRER(FragmentTransaction.TRANSIT_FRAGMENT_OPEN, "Send referrer"),
        EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES(InputDeviceCompat.SOURCE_TOUCHSCREEN, "Referrer obtained"),
        EVENT_TYPE_APP_ENVIRONMENT_UPDATED(5376, "App Environment Updated"),
        EVENT_TYPE_APP_ENVIRONMENT_CLEARED(5632, "App Environment Cleared"),
        EVENT_TYPE_EXCEPTION_UNHANDLED(5888, "Crash of App"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE(5889, "Crash of App, read from file"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT(5890, "Crash of App, passed to server via intent"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF(5891, "Crash of App"),
        EVENT_TYPE_EXCEPTION_USER_PROTOBUF(5892, "Error from developer"),
        EVENT_TYPE_ANR(5968, "ANR"),
        EVENT_TYPE_ACTIVATION(6144, "Activation of metrica"),
        EVENT_TYPE_FIRST_ACTIVATION(6145, "First activation of metrica"),
        EVENT_TYPE_START(6400, "Start of new session"),
        EVENT_TYPE_CUSTOM_EVENT(8192, "Custom event"),
        EVENT_TYPE_APP_OPEN(8208, "App open event"),
        EVENT_TYPE_APP_UPDATE(8224, "App update event"),
        EVENT_TYPE_PERMISSIONS(12288, "Permissions changed event"),
        EVENT_TYPE_APP_FEATURES(12289, "Features, required by application"),
        EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG(16384, "Update pre-activation config"),
        EVENT_TYPE_CLEANUP(12290, "Cleanup database"),
        EVENT_TYPE_VIEW_TREE(12291, "View tree event");
        
        static final HashMap<Integer, a> R = null;
        private final int S;
        private final String T;

        static {
            int i;
            a[] values;
            R = new HashMap<>();
            for (a aVar : values()) {
                R.put(Integer.valueOf(aVar.a()), aVar);
            }
        }

        private a(int i, String str) {
            this.S = i;
            this.T = str;
        }

        public int a() {
            return this.S;
        }

        public String b() {
            return this.T;
        }

        @NonNull
        public static a a(int i) {
            a aVar = (a) R.get(Integer.valueOf(i));
            return aVar == null ? EVENT_TYPE_UNDEFINED : aVar;
        }
    }

    public static boolean a(a aVar) {
        return !b.contains(aVar);
    }

    public static boolean b(a aVar) {
        return !c.contains(aVar);
    }

    public static boolean a(int i) {
        return d.contains(a.a(i));
    }

    public static boolean b(int i) {
        return e.contains(a.a(i));
    }

    public static boolean c(int i) {
        return g.contains(a.a(i));
    }

    public static boolean d(int i) {
        return f.contains(a.a(i));
    }

    public static boolean e(int i) {
        return !h.contains(a.a(i));
    }

    public static aa a(@NonNull String str, @NonNull abl abl) {
        return a(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, str, abl);
    }

    public static aa b(@NonNull String str, @NonNull abl abl) {
        return a(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, str, abl);
    }

    static aa a(a aVar, String str, @NonNull abl abl) {
        return new t(str, "", aVar.a(), abl);
    }

    public static aa a(a aVar, @NonNull abl abl) {
        return new t("", aVar.a(), abl);
    }

    public static aa c(String str, @NonNull abl abl) {
        return new t(str, a.EVENT_TYPE_REGULAR.a(), abl);
    }

    static aa a(String str, String str2, @NonNull abl abl) {
        return new t(str2, str, a.EVENT_TYPE_REGULAR.a(), abl);
    }

    static aa b(String str, String str2, @NonNull abl abl) {
        return new t(str2, str, a.EVENT_TYPE_STATBOX.a(), abl);
    }

    static aa a(@NonNull String str, @Nullable String str2) {
        return new aa(str2, str, a.EVENT_TYPE_DIAGNOSTIC.a());
    }

    public static aa a() {
        aa a2 = new aa().a(a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.a());
        try {
            a2.c(abc.a().toString());
        } catch (Throwable unused) {
        }
        return a2;
    }

    static aa a(byte[] bArr, @NonNull abl abl) {
        return new t(bArr, "", a.EVENT_TYPE_ANR.a(), abl);
    }

    static aa a(String str, byte[] bArr, @NonNull abl abl) {
        return new t(bArr, str, a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.a(), abl);
    }

    static aa d(String str, @NonNull abl abl) {
        return new t(str, a.EVENT_TYPE_START.a(), abl);
    }

    static aa e(String str, @NonNull abl abl) {
        return new t(str, a.EVENT_TYPE_UPDATE_FOREGROUND_TIME.a(), abl);
    }

    public static t b(String str, byte[] bArr, @NonNull abl abl) {
        return a(bArr, str, a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, abl);
    }

    public static aa a(String str, byte[] bArr, int i, @NonNull HashMap<com.yandex.metrica.impl.ob.t.a, Integer> hashMap, @NonNull abl abl) {
        return a(bArr, str, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, abl).a(hashMap).c(i);
    }

    private static t a(byte[] bArr, String str, a aVar, @NonNull abl abl) {
        return new t(bArr, str, aVar.a(), abl);
    }

    public static aa f(String str, @NonNull abl abl) {
        return new t("", str, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST.a(), abl);
    }

    public static aa a(@Nullable vu vuVar, @NonNull abl abl) {
        if (vuVar == null) {
            return new t("", a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.a(), abl);
        }
        return new t(vuVar.a, a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.a(), abl).a(vuVar.a());
    }

    static aa g(String str, @NonNull abl abl) {
        return c(MraidJsMethods.OPEN, str, abl);
    }

    static aa h(String str, @NonNull abl abl) {
        return c("referral", str, abl);
    }

    static aa c(String str, String str2, @NonNull abl abl) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", str);
        hashMap.put("link", str2);
        return new t(abc.b((Map) hashMap), "", a.EVENT_TYPE_APP_OPEN.a(), abl);
    }

    public static aa a(ch chVar, @NonNull abl abl) {
        return new t(chVar == null ? "" : chVar.a(), "", a.EVENT_TYPE_ACTIVATION.a(), abl);
    }

    public static aa i(@NonNull String str, @NonNull abl abl) {
        return new t(str, "", a.EVENT_TYPE_CLEANUP.a(), abl);
    }

    static aa a(@NonNull JSONObject jSONObject, @NonNull abl abl) {
        return new t(jSONObject.toString(), "view_tree", a.EVENT_TYPE_VIEW_TREE.a(), abl);
    }

    static aa a(int i, String str, String str2, Map<String, Object> map, @NonNull abl abl) {
        t tVar = new t(str2, str, a.EVENT_TYPE_CUSTOM_EVENT.a(), i, abl);
        return tVar.e(abc.b((Map) map));
    }
}
