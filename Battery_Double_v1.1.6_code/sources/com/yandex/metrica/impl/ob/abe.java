package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Locale;

public abstract class abe extends aat {
    private static String a = "";
    @NonNull
    private final String b;

    public abe(@Nullable String str) {
        super(false);
        this.b = String.format("[%s] ", new Object[]{dl.b(str)});
    }

    @NonNull
    public String g() {
        String b2 = dh.b(a, "");
        String b3 = dh.b(this.b, "");
        StringBuilder sb = new StringBuilder();
        sb.append(b2);
        sb.append(b3);
        return sb.toString();
    }

    public static void a(Context context) {
        a = String.format("[%s] : ", new Object[]{context.getPackageName()});
    }

    /* access modifiers changed from: 0000 */
    public String d(String str, Object[] objArr) {
        return String.format(Locale.US, str, objArr);
    }
}
