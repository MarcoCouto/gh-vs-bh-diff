package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class fi {
    @NonNull
    private final mo a;

    public fi(@NonNull mo moVar) {
        this.a = moVar;
    }

    public int a() {
        int a2 = this.a.a();
        this.a.b(a2 + 1).q();
        return a2;
    }

    public int a(int i) {
        int a2 = this.a.a(i);
        this.a.a(i, a2 + 1).q();
        return a2;
    }
}
