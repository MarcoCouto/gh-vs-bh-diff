package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.io.File;

public class fq extends fe {
    @NonNull
    private final vt b;
    @NonNull
    private final a c;
    @NonNull
    private final df d;
    @NonNull
    private final bv e;
    @NonNull
    private final lm f;
    @NonNull
    private final bw g;

    public class a implements com.yandex.metrica.impl.ob.vt.a {
        public a() {
        }

        public boolean a(@NonNull vu vuVar, @NonNull bd bdVar) {
            fq.this.a(new aa().a(vuVar.a()).a(com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_SEND_REFERRER.a()));
            return true;
        }
    }

    public fq(@NonNull Context context, @NonNull yb ybVar, @NonNull fb fbVar, @NonNull com.yandex.metrica.impl.ob.ew.a aVar, @NonNull vt vtVar, @NonNull df dfVar, @NonNull ye yeVar) {
        a aVar2 = new a();
        dj djVar = new dj();
        an anVar = new an();
        Context context2 = context;
        fr frVar = new fr(context, fbVar, aVar, yeVar, ybVar, new fp(dfVar), as.a().k().g(), dl.c(context2, fbVar.b()));
        this(context2, fbVar, aVar2, djVar, anVar, frVar, vtVar, dfVar, aVar.q);
    }

    @VisibleForTesting
    fq(@NonNull Context context, @NonNull fb fbVar, @NonNull a aVar, @NonNull dj djVar, @NonNull an anVar, @NonNull fr frVar, @NonNull vt vtVar, @NonNull df dfVar, @Nullable Boolean bool) {
        super(context, fbVar, aVar, djVar, frVar);
        this.b = vtVar;
        hg f2 = f();
        f2.a(com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_REGULAR, new iv(f2.a()));
        this.c = frVar.a(this);
        this.b.a((com.yandex.metrica.impl.ob.vt.a) this.c);
        this.d = dfVar;
        this.g = frVar.b(this);
        this.e = frVar.a(this.g, y());
        this.f = frVar.a(anVar, (aby<File>) new aby<File>() {
            public void a(File file) {
                fq.this.a(file);
            }
        });
        if (Boolean.TRUE.equals(bool)) {
            this.f.a();
            this.e.a();
        }
    }

    public synchronized void a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
        super.a(aVar);
        D();
        this.d.a(aVar.m);
    }

    private void D() {
        this.a.a(i().U()).q();
    }

    public void b() {
        this.f.b();
        super.b();
    }

    @NonNull
    public com.yandex.metrica.CounterConfiguration.a a() {
        return com.yandex.metrica.CounterConfiguration.a.MAIN;
    }

    /* access modifiers changed from: private */
    public void a(File file) {
        this.e.a(file.getAbsolutePath(), new aby<Boolean>() {
            public void a(Boolean bool) {
            }
        }, true);
    }
}
