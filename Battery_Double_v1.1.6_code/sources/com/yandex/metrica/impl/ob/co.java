package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;

class co extends et {
    protected ai a;
    protected ch b;
    private ar c = new ar();

    protected co(@NonNull eu euVar, @NonNull CounterConfiguration counterConfiguration) {
        super(euVar, counterConfiguration);
    }

    /* access modifiers changed from: 0000 */
    public void a(adj adj) {
        this.a = new ai(adj);
    }

    /* access modifiers changed from: 0000 */
    public Bundle b() {
        Bundle bundle = new Bundle();
        h().a(bundle);
        g().b(bundle);
        return bundle;
    }

    /* access modifiers changed from: 0000 */
    public void a(xx xxVar) {
        b(xxVar);
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.c.b();
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return this.c.a();
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.c.c();
    }

    /* access modifiers changed from: 0000 */
    public void b(xx xxVar) {
        if (xxVar != null) {
            h().c(xxVar.a());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2) {
        this.a.a(str, str2);
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return this.a.a();
    }

    /* access modifiers changed from: 0000 */
    public ch f() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(ch chVar) {
        this.b = chVar;
    }
}
