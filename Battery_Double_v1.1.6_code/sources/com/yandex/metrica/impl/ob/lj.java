package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.us.e;

public class lj implements nh<lc, e> {
    @NonNull
    private lh a = new lh();

    @NonNull
    /* renamed from: a */
    public e b(@NonNull lc lcVar) {
        e eVar = new e();
        eVar.f = lcVar.e == null ? -1 : lcVar.e.intValue();
        eVar.e = lcVar.d;
        eVar.c = lcVar.b;
        eVar.b = lcVar.a;
        eVar.d = lcVar.c;
        eVar.g = this.a.b(lcVar.f);
        return eVar;
    }

    @NonNull
    public lc a(@NonNull e eVar) {
        throw new UnsupportedOperationException();
    }
}
