package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.SystemClock;
import com.tapjoy.TJAdUnitConstants;

class bf {
    private final Handler a;
    private final o b;
    private final bg c;

    bf(Handler handler, o oVar) {
        this.a = handler;
        this.b = oVar;
        this.c = new bg(handler, oVar);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        b(this.a, this.b, this.c);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a(this.a, this.b, this.c);
    }

    static void a(Handler handler, o oVar, Runnable runnable) {
        b(handler, oVar, runnable);
        handler.postAtTime(runnable, b(oVar), a(oVar));
    }

    private static long a(o oVar) {
        return SystemClock.uptimeMillis() + ((long) c(oVar));
    }

    private static void b(Handler handler, o oVar, Runnable runnable) {
        handler.removeCallbacks(runnable, b(oVar));
    }

    private static String b(o oVar) {
        return oVar.d().h().e();
    }

    private static int c(o oVar) {
        return ((Integer) abw.b(oVar.d().h().c(), Integer.valueOf(10))).intValue() * TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL;
    }
}
