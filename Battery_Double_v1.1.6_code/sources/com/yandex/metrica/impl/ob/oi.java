package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.C0102a.C0103a;
import com.yandex.metrica.impl.ob.xi.a;
import java.util.ArrayList;
import java.util.List;

public class oi implements nq<a, C0103a> {
    @NonNull
    private final oh a;

    public oi() {
        this(new oh());
    }

    @NonNull
    public List<a> a(@NonNull C0103a[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (C0103a a2 : aVarArr) {
            arrayList.add(this.a.a(a2));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public C0103a[] b(@NonNull List<a> list) {
        C0103a[] aVarArr = new C0103a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVarArr[i] = this.a.b((a) list.get(i));
        }
        return aVarArr;
    }

    @VisibleForTesting
    oi(@NonNull oh ohVar) {
        this.a = ohVar;
    }
}
