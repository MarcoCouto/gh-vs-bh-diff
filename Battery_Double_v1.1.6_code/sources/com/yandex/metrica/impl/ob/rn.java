package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

class rn extends qx {
    @Nullable
    private final LocationManager f;
    /* access modifiers changed from: private */
    @NonNull
    public final String g;

    public rn(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull rj rjVar, @NonNull sp spVar, @NonNull String str) {
        this(context, looper, locationManager, spVar, str, (LocationListener) new qt(rjVar));
    }

    @VisibleForTesting
    rn(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull sp spVar, @NonNull String str, @NonNull LocationListener locationListener) {
        super(context, locationListener, spVar, looper);
        this.f = locationManager;
        this.g = str;
    }

    public boolean a() {
        if (!this.c.a(this.b)) {
            return false;
        }
        return a(this.g, 0.0f, a, this.d, this.e);
    }

    public void b() {
        if (this.f != null) {
            try {
                this.f.removeUpdates(this.d);
            } catch (Throwable unused) {
            }
        }
    }

    @SuppressLint({"MissingPermission"})
    public void c() {
        if (this.c.a(this.b)) {
            AnonymousClass1 r0 = new aca<LocationManager, Location>() {
                public Location a(LocationManager locationManager) throws Throwable {
                    return locationManager.getLastKnownLocation(rn.this.g);
                }
            };
            LocationManager locationManager = this.f;
            StringBuilder sb = new StringBuilder();
            sb.append("getting last known location for provider ");
            sb.append(this.g);
            this.d.onLocationChanged((Location) dl.a((aca<T, S>) r0, locationManager, sb.toString(), "location manager"));
        }
    }

    private boolean a(String str, float f2, long j, @NonNull LocationListener locationListener, @NonNull Looper looper) {
        if (this.f != null) {
            try {
                this.f.requestLocationUpdates(str, j, f2, locationListener, looper);
                return true;
            } catch (Throwable unused) {
            }
        }
        return false;
    }
}
