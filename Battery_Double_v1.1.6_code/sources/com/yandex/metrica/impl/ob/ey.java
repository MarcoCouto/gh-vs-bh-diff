package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.vt.a;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ey implements fk, fm, xy {
    @NonNull
    private final Context a;
    @NonNull
    private final fb b;
    @NonNull
    private final xt c;
    @NonNull
    private final yc d;
    @NonNull
    private final fy e;
    @NonNull
    private final hl<hk, ey> f;
    @NonNull
    private final di<ey> g;
    @NonNull
    private List<ba> h;
    @Nullable
    private yb i;
    @NonNull
    private final fc<gj> j;
    @NonNull
    private final vt k;
    private final a l;
    @Nullable
    private vf m;
    private final Object n;

    public ey(@NonNull Context context, @NonNull xt xtVar, @NonNull fb fbVar, @NonNull ew ewVar, @NonNull vt vtVar) {
        this(context, xtVar, fbVar, ewVar, new fy(ewVar.b), vtVar, new fc(), new fa());
    }

    @VisibleForTesting
    ey(@NonNull Context context, @NonNull xt xtVar, @NonNull fb fbVar, @NonNull ew ewVar, @NonNull fy fyVar, @NonNull vt vtVar, @NonNull fc<gj> fcVar, @NonNull fa faVar) {
        this.h = new ArrayList();
        this.i = null;
        this.n = new Object();
        this.a = context.getApplicationContext();
        this.b = fbVar;
        this.c = xtVar;
        this.e = fyVar;
        this.j = fcVar;
        this.f = faVar.a(this);
        this.d = this.c.a(this.a, this.b, this, ewVar.a);
        this.g = faVar.a(this, this.d);
        this.k = vtVar;
        this.l = faVar.a(this.d);
        this.k.a(this.l);
    }

    public void a(@NonNull ew.a aVar) {
        this.e.a(aVar);
    }

    public synchronized void a(@NonNull gj gjVar) {
        this.j.a(gjVar);
        if (this.i != null) {
            a(this.i, (xy) gjVar);
        }
    }

    public synchronized void b(@NonNull gj gjVar) {
        this.j.b(gjVar);
    }

    public void a(@NonNull aa aaVar, @NonNull gj gjVar) {
        this.f.a(aaVar, gjVar);
    }

    @NonNull
    public ew.a a() {
        return this.e.a();
    }

    @NonNull
    public fb c() {
        return this.b;
    }

    public void a(@Nullable yb ybVar) {
        this.i = ybVar;
        b(ybVar);
        if (ybVar != null) {
            if (this.m == null) {
                this.m = as.a().f();
            }
            this.m.a(ybVar);
        }
    }

    public void a(@NonNull xv xvVar, @Nullable yb ybVar) {
        synchronized (this.n) {
            for (ba c2 : this.h) {
                ab.a(c2.c(), xvVar, ybVar);
            }
            this.h.clear();
        }
    }

    private void b(yb ybVar) {
        synchronized (this.n) {
            for (xy a2 : this.j.a()) {
                a(ybVar, a2);
            }
            ArrayList arrayList = new ArrayList();
            for (ba baVar : this.h) {
                if (baVar.a(ybVar)) {
                    ab.a(baVar.c(), ybVar);
                } else {
                    arrayList.add(baVar);
                }
            }
            this.h = new ArrayList(arrayList);
            if (!arrayList.isEmpty()) {
                this.g.e();
            }
        }
    }

    private void a(@Nullable yb ybVar, @NonNull xy xyVar) {
        xyVar.a(ybVar);
    }

    public void b() {
        dl.a((Closeable) this.g);
    }

    @NonNull
    public Context d() {
        return this.a;
    }

    public void a(@Nullable ba baVar) {
        ResultReceiver resultReceiver;
        Map hashMap = new HashMap();
        List list = null;
        if (baVar != null) {
            list = baVar.a();
            ResultReceiver c2 = baVar.c();
            resultReceiver = c2;
            hashMap = baVar.b();
        } else {
            resultReceiver = null;
        }
        boolean a2 = this.d.a(list, hashMap);
        if (!a2) {
            ab.a(resultReceiver, this.d.e());
        }
        if (this.d.b()) {
            synchronized (this.n) {
                if (a2 && baVar != null) {
                    this.h.add(baVar);
                }
            }
            this.g.e();
        } else if (a2) {
            ab.a(resultReceiver, this.d.e());
        }
    }

    @NonNull
    public vt e() {
        return this.k;
    }

    public void a(@NonNull ew ewVar) {
        this.d.a(ewVar.a);
        a(ewVar.b);
    }
}
