package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.MetricaService;
import com.yandex.metrica.impl.ob.ao.c;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class xh implements xg, Runnable {
    /* access modifiers changed from: private */
    public final ServiceConnection a;
    private final Handler b;
    private HashMap<String, xe> c;
    /* access modifiers changed from: private */
    public final Context d;
    private boolean e;
    private ServerSocket f;
    /* access modifiers changed from: private */
    public final xc g;
    /* access modifiers changed from: private */
    public xs h;
    private acx i;
    private long j;
    private long k;
    @NonNull
    private final abt l;
    @NonNull
    private final abr m;
    @NonNull
    private final c n;

    public static class a {
        public xh a(@NonNull Context context) {
            return new xh(context);
        }
    }

    public xh(Context context) {
        this(context, as.a().h(), as.a().k().i(), new abs(), new abr());
    }

    @VisibleForTesting
    xh(@NonNull Context context, @NonNull ao aoVar, @NonNull act act, @NonNull abt abt, @NonNull abr abr) {
        this.a = new ServiceConnection() {
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            }

            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        this.b = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message message) {
                super.handleMessage(message);
                if (message.what == 100) {
                    xh.this.e();
                    try {
                        xh.this.d.unbindService(xh.this.a);
                    } catch (Throwable unused) {
                        xa.a().reportEvent("socket_unbind_has_thrown_exception");
                    }
                }
            }
        };
        this.c = new HashMap<String, xe>() {
            {
                put(TtmlNode.TAG_P, new xe() {
                    @NonNull
                    public xd a(@NonNull Socket socket, @NonNull Uri uri) {
                        xb xbVar = new xb(socket, uri, xh.this, xh.this.h, xh.this.g);
                        return xbVar;
                    }
                });
            }
        };
        this.g = new xc();
        this.d = context;
        this.l = abt;
        this.m = abr;
        this.n = aoVar.a(new Runnable() {
            public void run() {
                xh.this.h();
            }
        }, act);
        g();
    }

    private void g() {
        eh.a().a(this, es.class, el.a((ek<T>) new ek<es>() {
            public void a(es esVar) {
                xh.this.g.a(esVar.a);
            }
        }).a(new ei<es>() {
            public boolean a(es esVar) {
                return !xh.this.d.getPackageName().equals(esVar.b);
            }
        }).a());
        eh.a().a(this, eo.class, el.a((ek<T>) new ek<eo>() {
            public void a(eo eoVar) {
                xh.this.g.b(eoVar.a);
            }
        }).a());
        eh.a().a(this, em.class, el.a((ek<T>) new ek<em>() {
            public void a(em emVar) {
                xh.this.g.c(emVar.a);
            }
        }).a());
        eh.a().a(this, en.class, el.a((ek<T>) new ek<en>() {
            public void a(en enVar) {
                xh.this.g.d(enVar.a);
            }
        }).a());
        eh.a().a(this, eq.class, el.a((ek<T>) new ek<eq>() {
            public void a(eq eqVar) {
                xh.this.a(eqVar.a);
                xh.this.c();
            }
        }).a());
    }

    public void a() {
        if (this.e) {
            b();
            this.b.sendMessageDelayed(this.b.obtainMessage(100), TimeUnit.SECONDS.toMillis(this.h.a));
            this.k = this.l.a();
        }
    }

    public void b() {
        this.b.removeMessages(100);
        this.k = 0;
    }

    public synchronized void c() {
        if (!(this.e || this.h == null || !this.n.a(this.h.e))) {
            this.e = true;
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        d();
        this.i = as.a().k().a(this);
        this.i.start();
        this.j = this.l.a();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(xs xsVar) {
        this.h = xsVar;
        if (this.h != null) {
            this.n.a(this.h.d);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d() {
        Intent intent = new Intent(this.d, MetricaService.class);
        intent.setAction("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER");
        try {
            if (!this.d.bindService(intent, this.a, 1)) {
                xa.a().reportEvent("socket_bind_has_failed");
            }
        } catch (Throwable unused) {
            xa.a().reportEvent("socket_bind_has_thrown_exception");
        }
    }

    public synchronized void e() {
        try {
            this.e = false;
            if (this.i != null) {
                this.i.b();
                this.i = null;
            }
            if (this.f != null) {
                this.f.close();
                this.f = null;
            }
        } catch (IOException unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x003f A[SYNTHETIC, Splitter:B:28:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0018 A[SYNTHETIC] */
    public void run() {
        ServerSocket serverSocket;
        Socket socket;
        this.f = f();
        if (dl.a(26)) {
            TrafficStats.setThreadStatsTag(40230);
        }
        if (this.f != null) {
            while (this.e) {
                synchronized (this) {
                    serverSocket = this.f;
                }
                if (serverSocket != null) {
                    try {
                        socket = serverSocket.accept();
                        try {
                            if (dl.a(26)) {
                                TrafficStats.tagSocket(socket);
                            }
                            a(socket);
                            if (socket == null) {
                            }
                        } catch (Throwable th) {
                            th = th;
                            if (socket != null) {
                                try {
                                    socket.close();
                                } catch (IOException unused) {
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        socket = null;
                        if (socket != null) {
                        }
                        throw th;
                    }
                    try {
                        socket.close();
                    } catch (IOException unused2) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ServerSocket f() {
        Iterator it = this.h.c.iterator();
        ServerSocket serverSocket = null;
        Integer num = null;
        while (serverSocket == null && it.hasNext()) {
            try {
                Integer num2 = (Integer) it.next();
                if (num2 != null) {
                    try {
                        serverSocket = b(num2.intValue());
                    } catch (SocketException unused) {
                        num = num2;
                        a("port_already_in_use", num.intValue());
                    } catch (IOException unused2) {
                    }
                }
                num = num2;
            } catch (SocketException unused3) {
                a("port_already_in_use", num.intValue());
            } catch (IOException unused4) {
            }
        }
        return serverSocket;
    }

    /* access modifiers changed from: 0000 */
    public ServerSocket b(int i2) throws IOException {
        return new ServerSocket(i2);
    }

    private Map<String, Object> c(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("port", String.valueOf(i2));
        return hashMap;
    }

    private Map<String, Object> d(int i2) {
        Map<String, Object> c2 = c(i2);
        c2.put("idle_interval", Double.valueOf(a(this.j)));
        c2.put("background_interval", Double.valueOf(a(this.k)));
        return c2;
    }

    private double a(long j2) {
        long j3 = 0;
        if (j2 != 0) {
            j3 = this.m.e(j2, TimeUnit.MILLISECONDS);
        }
        return (double) j3;
    }

    private void a(@NonNull Socket socket) {
        new xf(socket, this, this.c).a();
    }

    public void a(@NonNull String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("uri", str2);
        ay a2 = xa.a();
        StringBuilder sb = new StringBuilder();
        sb.append("socket_");
        sb.append(str);
        a2.reportEvent(sb.toString(), (Map<String, Object>) hashMap);
    }

    public void a(@NonNull String str) {
        xa.a().reportEvent(b(str));
    }

    public void a(@NonNull String str, Throwable th) {
        xa.a().reportError(b(str), th);
    }

    public void a(@NonNull String str, int i2) {
        xa.a().reportEvent(b(str), c(i2));
    }

    public void a(int i2) {
        xa.a().reportEvent(b("sync_succeed"), d(i2));
    }

    private String b(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("socket_");
        sb.append(str);
        return sb.toString();
    }
}
