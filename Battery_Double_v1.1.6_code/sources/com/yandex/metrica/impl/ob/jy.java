package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public enum jy {
    FOREGROUND(0),
    BACKGROUND(1);
    
    private final int c;

    private jy(int i) {
        this.c = i;
    }

    public int a() {
        return this.c;
    }

    @NonNull
    public static jy a(Integer num) {
        jy jyVar = FOREGROUND;
        if (num == null) {
            return jyVar;
        }
        switch (num.intValue()) {
            case 0:
                return FOREGROUND;
            case 1:
                return BACKGROUND;
            default:
                return jyVar;
        }
    }
}
