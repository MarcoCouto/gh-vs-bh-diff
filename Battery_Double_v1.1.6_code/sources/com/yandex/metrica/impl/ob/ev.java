package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.ew.a;
import com.yandex.metrica.impl.ob.we.d;

public class ev extends fe {
    public ev(@NonNull Context context, @NonNull yb ybVar, @NonNull fb fbVar, @NonNull a aVar, @NonNull d dVar, @NonNull ye yeVar) {
        a aVar2 = new a();
        dj djVar = new dj();
        Context context2 = context;
        ff ffVar = new ff(context, fbVar, aVar, yeVar, ybVar, dVar, as.a().k().g(), dl.c(context2, fbVar.b()));
        this(context2, fbVar, aVar2, djVar, ffVar);
    }

    @VisibleForTesting
    ev(@NonNull Context context, @NonNull fb fbVar, @NonNull a aVar, @NonNull dj djVar, @NonNull ff ffVar) {
        super(context, fbVar, aVar, djVar, ffVar);
    }

    @NonNull
    public CounterConfiguration.a a() {
        return CounterConfiguration.a.APPMETRICA;
    }
}
