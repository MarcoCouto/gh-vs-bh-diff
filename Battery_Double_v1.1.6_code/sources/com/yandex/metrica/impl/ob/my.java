package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.e;

public class my<T, P extends e> implements mx<T> {
    @NonNull
    private final String a;
    @NonNull
    private final lp b;
    @NonNull
    private final mw<P> c;
    @NonNull
    private final nh<T, P> d;

    public my(@NonNull String str, @NonNull lp lpVar, @NonNull mw<P> mwVar, @NonNull nh<T, P> nhVar) {
        this.a = str;
        this.b = lpVar;
        this.c = mwVar;
        this.d = nhVar;
    }

    public void a(@NonNull T t) {
        this.b.a(this.a, this.c.a(this.d.b(t)));
    }

    @NonNull
    public T a() {
        try {
            byte[] a2 = this.b.a(this.a);
            if (dl.a(a2)) {
                return this.d.a(this.c.c());
            }
            return this.d.a(this.c.b(a2));
        } catch (Throwable unused) {
            return this.d.a(this.c.c());
        }
    }
}
