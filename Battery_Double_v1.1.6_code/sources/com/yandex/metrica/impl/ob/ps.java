package com.yandex.metrica.impl.ob;

public class ps {
    public final long a;
    public final long b;

    public ps(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ForcedCollectingArguments{durationSeconds=");
        sb.append(this.a);
        sb.append(", intervalSeconds=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ps psVar = (ps) obj;
        if (this.a != psVar.a) {
            return false;
        }
        if (this.b != psVar.b) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((int) (this.a ^ (this.a >>> 32))) * 31) + ((int) (this.b ^ (this.b >>> 32)));
    }
}
