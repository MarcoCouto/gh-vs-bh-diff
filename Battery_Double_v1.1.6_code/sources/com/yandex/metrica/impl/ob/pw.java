package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.gpllibrary.a.b;

public class pw {
    @NonNull
    public final b a;
    public final long b;
    public final long c;

    public pw(@NonNull b bVar, long j, long j2) {
        this.a = bVar;
        this.b = j;
        this.c = j2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GplArguments{priority=");
        sb.append(this.a);
        sb.append(", durationSeconds=");
        sb.append(this.b);
        sb.append(", intervalSeconds=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        pw pwVar = (pw) obj;
        if (this.b != pwVar.b || this.c != pwVar.c) {
            return false;
        }
        if (this.a != pwVar.a) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((this.a.hashCode() * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)));
    }
}
