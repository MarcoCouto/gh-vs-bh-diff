package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.vb.a;
import com.yandex.metrica.impl.ob.vb.a.C0098a;
import java.util.ArrayList;
import java.util.List;

public class nw implements nh<List<sr>, a> {
    @NonNull
    /* renamed from: a */
    public a b(@NonNull List<sr> list) {
        a aVar = new a();
        aVar.b = new C0098a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVar.b[i] = a((sr) list.get(i));
        }
        return aVar;
    }

    @NonNull
    public List<sr> a(@NonNull a aVar) {
        ArrayList arrayList = new ArrayList(aVar.b.length);
        for (C0098a a : aVar.b) {
            arrayList.add(a(a));
        }
        return arrayList;
    }

    @NonNull
    private C0098a a(@NonNull sr srVar) {
        C0098a aVar = new C0098a();
        aVar.b = srVar.a;
        aVar.c = srVar.b;
        return aVar;
    }

    @NonNull
    private sr a(@NonNull C0098a aVar) {
        return new sr(aVar.b, aVar.c);
    }
}
