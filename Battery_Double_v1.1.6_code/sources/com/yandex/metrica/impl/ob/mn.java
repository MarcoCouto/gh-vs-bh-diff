package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;

public class mn extends mp {
    static final th a = new th(IronSourceConstants.TYPE_UUID);
    static final th b = new th("DEVICE_ID_POSSIBLE");
    static final th c = new th("DEVICE_ID");
    static final th d = new th("DEVICE_ID_HASH");
    static final th e = new th("AD_URL_GET");
    static final th f = new th("AD_URL_REPORT");
    static final th g = new th("CUSTOM_HOSTS");
    static final th h = new th("SERVER_TIME_OFFSET");
    static final th i = new th("STARTUP_REQUEST_TIME");
    static final th j = new th("CLIDS");
    static final th k = new th("CLIENT_CLIDS");
    static final th l = new th("REFERRER");
    static final th m = new th("DEFERRED_DEEP_LINK_WAS_CHECKED");
    static final th n = new th("REFERRER_FROM_PLAY_SERVICES_WAS_CHECKED");
    static final th o = new th("DEPRECATED_NATIVE_CRASHES_CHECKED");
    static final th p = new th("API_LEVEL");
    static final th q = new th("ADS_REQUESTED_CONTEXT");
    static final th r = new th("CONTEXT_HISTORY");
    static final th s = new th("ACCESS_CONFIG");
    static final th t = new th("LAST_UI_CONTEXT_ID");

    public mn(lx lxVar) {
        super(lxVar);
    }

    public String a(String str) {
        return c(a.b(), str);
    }

    public String b(String str) {
        return c(c.b(), str);
    }

    public String c(String str) {
        return c(d.b(), str);
    }

    public String a() {
        return c(b.b(), "");
    }

    public String d(String str) {
        return c(e.b(), str);
    }

    public String e(String str) {
        return c(f.b(), str);
    }

    public List<String> b() {
        String c2 = c(g.b(), null);
        if (TextUtils.isEmpty(c2)) {
            return null;
        }
        return abc.c(c2);
    }

    public long a(long j2) {
        return b(h.a(), j2);
    }

    public long b(long j2) {
        return b(i.b(), j2);
    }

    public String f(String str) {
        return c(j.b(), str);
    }

    public long c(long j2) {
        return b(p.b(), j2);
    }

    public String c() {
        return c(l.b(), null);
    }

    public boolean d() {
        return b(m.b(), false);
    }

    public boolean e() {
        return b(n.b(), false);
    }

    public mn g(String str) {
        return (mn) b(a.b(), str);
    }

    public mn h(String str) {
        return (mn) b(c.b(), str);
    }

    public mn i(String str) {
        return (mn) b(d.b(), str);
    }

    public mn j(String str) {
        return (mn) b(e.b(), str);
    }

    public mn a(List<String> list) {
        return (mn) b(g.b(), abc.a(list));
    }

    public mn k(String str) {
        return (mn) b(f.b(), str);
    }

    public mn d(long j2) {
        return (mn) a(h.b(), j2);
    }

    public mn e(long j2) {
        return (mn) a(i.b(), j2);
    }

    public mn l(String str) {
        return (mn) b(j.b(), str);
    }

    public mn f(long j2) {
        return (mn) a(p.b(), j2);
    }

    public mn m(String str) {
        return (mn) b(b.b(), str);
    }

    public mn n(String str) {
        return (mn) b(l.b(), str);
    }

    public mn f() {
        return (mn) a(m.b(), true);
    }

    public mn g() {
        return (mn) a(n.b(), true);
    }

    public mn o(@Nullable String str) {
        return (mn) b(k.b(), str);
    }

    @Nullable
    public String p(@Nullable String str) {
        return c(k.b(), str);
    }

    public boolean a(boolean z) {
        return b(q.b(), z);
    }

    public mn b(boolean z) {
        return (mn) a(q.b(), z);
    }

    public mn b(@NonNull List<String> list) {
        return (mn) a(r.b(), list);
    }

    @NonNull
    public List<String> h() {
        LinkedList linkedList = new LinkedList();
        List b2 = b(r.b(), (List<String>) linkedList);
        return b2 == null ? linkedList : b2;
    }

    public long g(long j2) {
        return b(t.b(), j2);
    }

    public mn h(long j2) {
        return (mn) a(t.b(), j2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x000e  */
    public mn a(@Nullable aag aag) {
        String str;
        if (aag != null) {
            try {
                str = abc.a(aag).toString();
            } catch (Throwable unused) {
            }
            if (str != null) {
                b(s.b(), str);
            }
            return this;
        }
        str = null;
        if (str != null) {
        }
        return this;
    }

    @Nullable
    public aag i() {
        try {
            String c2 = c(s.b(), null);
            if (c2 != null) {
                return abc.b(new JSONObject(c2));
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }
}
