package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class gi {
    @NonNull
    public gh a(@NonNull ge geVar) {
        switch (geVar.e()) {
            case COMMUTATION:
                return new go();
            case MAIN:
                return new gm();
            case SELF_DIAGNOSTIC_MAIN:
                return new gt();
            case SELF_DIAGNOSTIC_MANUAL:
                return new gu();
            case MANUAL:
                return new gq();
            case APPMETRICA:
                return new gd();
            default:
                return new gm();
        }
    }
}
