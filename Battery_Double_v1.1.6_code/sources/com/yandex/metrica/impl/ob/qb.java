package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class qb {
    /* access modifiers changed from: private */
    @NonNull
    public final mq a;
    @NonNull
    private final qw b;
    @Nullable
    private final py c;
    @NonNull
    private final qe d = c();
    @NonNull
    private final px e = d();
    @NonNull
    private final pv f = b(this.d);
    @NonNull
    private final qa g;

    public qb(@NonNull qw qwVar, @NonNull mq mqVar, @NonNull rj rjVar, @Nullable py pyVar) {
        this.b = qwVar;
        this.a = mqVar;
        this.c = pyVar;
        this.g = a(rjVar);
        this.g.a(this.c);
    }

    @NonNull
    private qe c() {
        return new qe() {
            public long a() {
                return qb.this.a.p(0);
            }

            public void a(long j) {
                qb.this.a.q(j);
            }
        };
    }

    @NonNull
    private px d() {
        return new px(this.b.a.b, this.c);
    }

    @NonNull
    private pk a(@NonNull qe qeVar) {
        return new pk(qeVar);
    }

    @NonNull
    private pv b(@NonNull qe qeVar) {
        return new pv(this.c, qeVar);
    }

    @NonNull
    private qa a(@NonNull rj rjVar) {
        return new qa(this.b.a.a, rjVar, this.b.a.b.b(), this.b.a.c);
    }

    @NonNull
    public qy a() {
        return new qy(this.g, this.f, a(this.d), this.e);
    }

    @NonNull
    public List<pz> b() {
        return Arrays.asList(new pz[]{this.f, this.e, this.g});
    }
}
