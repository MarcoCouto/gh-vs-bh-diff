package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class bp implements bo {
    private act a;
    /* access modifiers changed from: private */
    public bo b;

    public bp(@NonNull bo boVar) {
        this(as.a().k().a(), boVar);
    }

    public void a() {
        this.a.a((Runnable) new abn() {
            public void a() {
                bp.this.b.a();
            }
        });
    }

    public void a(final Intent intent, final int i) {
        this.a.a((Runnable) new abn() {
            public void a() {
                bp.this.b.a(intent, i);
            }
        });
    }

    public void a(final Intent intent, final int i, final int i2) {
        this.a.a((Runnable) new abn() {
            public void a() {
                bp.this.b.a(intent, i, i2);
            }
        });
    }

    public void a(final Intent intent) {
        this.a.a((Runnable) new abn() {
            public void a() {
                bp.this.b.a(intent);
            }
        });
    }

    public void b(final Intent intent) {
        this.a.a((Runnable) new abn() {
            public void a() {
                bp.this.b.b(intent);
            }
        });
    }

    public void c(final Intent intent) {
        this.a.a((Runnable) new abn() {
            public void a() {
                bp.this.b.c(intent);
            }
        });
    }

    public void b() {
        this.a.a((Runnable) new abn() {
            public void a() throws Exception {
                bp.this.b.b();
            }
        });
    }

    public void a(String str, int i, String str2, Bundle bundle) {
        act act = this.a;
        final String str3 = str;
        final int i2 = i;
        final String str4 = str2;
        final Bundle bundle2 = bundle;
        AnonymousClass10 r1 = new abn() {
            public void a() throws RemoteException {
                bp.this.b.a(str3, i2, str4, bundle2);
            }
        };
        act.a((Runnable) r1);
    }

    public void a(final Bundle bundle) {
        this.a.a((Runnable) new abn() {
            public void a() throws Exception {
                bp.this.b.a(bundle);
            }
        });
    }

    public void b(@NonNull final Bundle bundle) {
        this.a.a((Runnable) new abn() {
            public void a() throws Exception {
                bp.this.b.b(bundle);
            }
        });
    }

    public void c(@NonNull final Bundle bundle) {
        this.a.a((Runnable) new abn() {
            public void a() throws Exception {
                bp.this.b.c(bundle);
            }
        });
    }

    @VisibleForTesting
    bp(@NonNull act act, @NonNull bo boVar) {
        this.a = act;
        this.b = boVar;
    }
}
