package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
class st implements sq {
    private final Context a;
    private final ado b;

    public st(Context context) {
        this(context, new ado());
    }

    @VisibleForTesting
    public st(Context context, @NonNull ado ado) {
        this.a = context;
        this.b = ado;
    }

    @NonNull
    public List<sr> a() {
        ArrayList arrayList = new ArrayList();
        PackageInfo a2 = this.b.a(this.a, this.a.getPackageName(), 4096);
        if (a2 != null) {
            for (int i = 0; i < a2.requestedPermissions.length; i++) {
                String str = a2.requestedPermissions[i];
                if ((a2.requestedPermissionsFlags[i] & 2) != 0) {
                    arrayList.add(new sr(str, true));
                } else {
                    arrayList.add(new sr(str, false));
                }
            }
        }
        return arrayList;
    }
}
