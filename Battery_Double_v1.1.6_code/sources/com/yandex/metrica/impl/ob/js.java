package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class js {
    @NonNull
    private final jy a;
    @Nullable
    private final Integer b;

    static final class a {
        /* access modifiers changed from: private */
        @NonNull
        public jy a;
        /* access modifiers changed from: private */
        @Nullable
        public Integer b;

        private a(jy jyVar) {
            this.a = jyVar;
        }

        public a a(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        public js a() {
            return new js(this);
        }
    }

    private js(a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
    }

    public static final a a(jy jyVar) {
        return new a(jyVar);
    }

    @NonNull
    public jy a() {
        return this.a;
    }

    @Nullable
    public Integer b() {
        return this.b;
    }
}
