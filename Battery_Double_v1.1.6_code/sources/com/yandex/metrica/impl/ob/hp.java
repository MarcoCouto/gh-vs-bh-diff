package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.ew.a;

public class hp extends hk {
    private final df a;
    @NonNull
    private final rp b;

    public hp(@NonNull ey eyVar, @NonNull df dfVar, @NonNull rp rpVar) {
        super(eyVar);
        this.a = dfVar;
        this.b = rpVar;
    }

    public boolean a(@NonNull aa aaVar, @NonNull gj gjVar) {
        a a2 = gjVar.b().a();
        this.a.a(a2.m);
        a(a2.e, a2.r);
        return false;
    }

    private void a(@Nullable Boolean bool, @Nullable Boolean bool2) {
        if (aau.a(bool) || (bool == null && aau.a(bool2))) {
            this.b.a(true);
        } else if (aau.c(bool)) {
            this.b.a(false);
        }
    }
}
