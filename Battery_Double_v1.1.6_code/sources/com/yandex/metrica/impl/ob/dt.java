package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.environment.ConnectivityService;

@TargetApi(21)
public class dt {
    @NonNull
    private final Context a;

    public dt(@NonNull Context context) {
        this.a = context;
    }

    @Nullable
    public BluetoothLeScanner a() {
        BluetoothManager bluetoothManager = (BluetoothManager) this.a.getSystemService(ConnectivityService.NETWORK_TYPE_BLUETOOTH);
        if (bluetoothManager == null) {
            return null;
        }
        BluetoothAdapter bluetoothAdapter = (BluetoothAdapter) dl.a((aca<T, S>) new aca<BluetoothManager, BluetoothAdapter>() {
            public BluetoothAdapter a(@NonNull BluetoothManager bluetoothManager) throws Throwable {
                return bluetoothManager.getAdapter();
            }
        }, bluetoothManager, "getting adapter", "BluetoothManager");
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            return (BluetoothLeScanner) dl.a((aca<T, S>) new aca<BluetoothAdapter, BluetoothLeScanner>() {
                public BluetoothLeScanner a(@NonNull BluetoothAdapter bluetoothAdapter) throws Throwable {
                    return bluetoothAdapter.getBluetoothLeScanner();
                }
            }, bluetoothAdapter, "Get bluetooth LE scanner", "BluetoothAdapter");
        }
        return null;
    }
}
