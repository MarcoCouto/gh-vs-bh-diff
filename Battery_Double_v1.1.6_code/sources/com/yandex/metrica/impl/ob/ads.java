package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ads<T> implements adw<T> {
    @NonNull
    private final adw<T> a;

    public ads(@NonNull adw<T> adw) {
        this.a = adw;
    }

    public adu a(@Nullable T t) {
        adu a2 = this.a.a(t);
        if (a2.a()) {
            return a2;
        }
        throw new adt(a2.b());
    }
}
