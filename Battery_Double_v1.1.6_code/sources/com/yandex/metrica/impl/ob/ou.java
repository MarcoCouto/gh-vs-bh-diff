package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.al.a;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class ou implements ov, ow {
    private final Set<Integer> a = new HashSet();
    private AtomicLong b;

    public ou(@NonNull lr lrVar) {
        this.a.add(Integer.valueOf(a.EVENT_TYPE_FIRST_ACTIVATION.a()));
        this.a.add(Integer.valueOf(a.EVENT_TYPE_APP_UPDATE.a()));
        this.a.add(Integer.valueOf(a.EVENT_TYPE_INIT.a()));
        this.a.add(Integer.valueOf(a.EVENT_TYPE_IDENTITY.a()));
        this.a.add(Integer.valueOf(a.EVENT_TYPE_SEND_REFERRER.a()));
        lrVar.a((ow) this);
        this.b = new AtomicLong(lrVar.a(this.a));
    }

    public boolean a() {
        return this.b.get() > 0;
    }

    public void a(@NonNull List<Integer> list) {
        int i = 0;
        for (Integer intValue : list) {
            if (this.a.contains(Integer.valueOf(intValue.intValue()))) {
                i++;
            }
        }
        this.b.addAndGet((long) i);
    }

    public void b(@NonNull List<Integer> list) {
        int i = 0;
        for (Integer intValue : list) {
            if (this.a.contains(Integer.valueOf(intValue.intValue()))) {
                i++;
            }
        }
        this.b.addAndGet((long) (-i));
    }
}
