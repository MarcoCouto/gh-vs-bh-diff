package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class se {
    private long a;
    private int b;
    @NonNull
    private final sf c;
    @Nullable
    private final xq d;
    @NonNull
    private final dj e;
    @NonNull
    private final abt f;

    public se(@NonNull sf sfVar, @Nullable xq xqVar) {
        this(sfVar, xqVar, new dj(), new abs());
    }

    @VisibleForTesting
    se(@NonNull sf sfVar, @Nullable xq xqVar, @NonNull dj djVar, @NonNull abt abt) {
        this.d = xqVar;
        this.c = sfVar;
        this.e = djVar;
        this.f = abt;
        d();
    }

    public void a() {
        this.b = 1;
        this.a = 0;
        this.c.a(this.b);
        this.c.a(this.a);
    }

    public void b() {
        this.a = this.f.b();
        this.b++;
        this.c.a(this.a);
        this.c.a(this.b);
    }

    public boolean c() {
        if (this.d == null || this.a == 0) {
            return true;
        }
        return this.e.a(this.a, (long) a(this.d), "last send attempt");
    }

    private int a(@NonNull xq xqVar) {
        int i = xqVar.b * ((1 << (this.b - 1)) - 1);
        if (i <= xqVar.a) {
            return i;
        }
        return xqVar.a;
    }

    private void d() {
        this.b = this.c.a();
        this.a = this.c.b();
    }
}
