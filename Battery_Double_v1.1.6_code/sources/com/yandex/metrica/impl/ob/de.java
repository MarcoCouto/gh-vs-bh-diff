package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.ca.a;
import io.fabric.sdk.android.services.network.HttpRequest;

public class de extends ce<wn> {
    private final yc j;
    private boolean k;
    private xv l;
    @NonNull
    private final wf m;

    public boolean o() {
        return true;
    }

    public de(yc ycVar, wf wfVar) {
        this(ycVar, wfVar, new wn(new wd()));
    }

    public de(yc ycVar, wf wfVar, @NonNull wn wnVar) {
        super(new dd(ycVar, wfVar), wnVar);
        this.k = false;
        this.j = ycVar;
        this.m = wfVar;
        a(this.m.a());
    }

    public boolean a() {
        if (this.h >= 0) {
            return false;
        }
        b(false);
        a(HttpRequest.HEADER_ACCEPT_ENCODING, "encrypted");
        return this.j.b();
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((wn) this.i).a(builder, this.m);
    }

    public boolean b() {
        if (G()) {
            return true;
        }
        if (200 != this.e) {
            return false;
        }
        boolean b = super.b();
        if (b) {
            return b;
        }
        this.l = xv.PARSE;
        return b;
    }

    public void a(Throwable th) {
        this.l = xv.NETWORK;
    }

    public void g() {
        super.g();
        this.l = xv.NETWORK;
    }

    public void f() {
        if (!x() && y()) {
            if (this.l == null) {
                this.l = xv.UNKNOWN;
            }
            this.j.a(this.l);
        }
    }

    public synchronized void b(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean G() {
        return this.k;
    }

    @NonNull
    public String n() {
        StringBuilder sb = new StringBuilder();
        sb.append("Startup task for component: ");
        sb.append(this.j.c().toString());
        return sb.toString();
    }

    @Nullable
    public a E() {
        return a.STARTUP;
    }

    @Nullable
    public xq F() {
        return this.m.f();
    }
}
