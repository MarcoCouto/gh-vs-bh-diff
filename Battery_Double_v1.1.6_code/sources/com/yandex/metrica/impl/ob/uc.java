package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.uy.a.C0096a;
import com.yandex.metrica.impl.ob.uy.a.b;

public class uc extends ua {
    public uc(@NonNull tz tzVar) {
        super(tzVar);
    }

    public C0096a a(@NonNull uq uqVar, @Nullable C0096a aVar, @NonNull ty tyVar) {
        if (a(aVar)) {
            return a().a(uqVar, tyVar.a());
        }
        aVar.d = new b();
        return aVar;
    }
}
