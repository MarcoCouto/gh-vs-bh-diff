package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.wl;
import java.io.IOException;

public abstract class cb<T extends wl> extends ce<T> {
    @NonNull
    private final acn j;
    @NonNull
    private final aay k;
    @NonNull
    private final abt l;

    /* access modifiers changed from: protected */
    public abstract void G();

    /* access modifiers changed from: protected */
    public abstract void H();

    public cb(@NonNull T t) {
        this(new af(), new acg(), new aay(), new abs(), t);
    }

    public cb(@NonNull bz bzVar, @NonNull acn acn, @NonNull aay aay, @NonNull abt abt, @NonNull T t) {
        super(bzVar, t);
        this.j = acn;
        this.k = aay;
        this.l = abt;
        t.a(this.j);
    }

    public void d() {
        super.d();
        a(this.l.a());
    }

    public boolean b() {
        boolean b = super.b();
        if (b) {
            G();
        } else if (p()) {
            H();
        }
        return b;
    }

    public boolean c(@NonNull byte[] bArr) {
        boolean z = false;
        try {
            byte[] a = this.k.a(bArr);
            if (a != null) {
                byte[] a2 = this.j.a(a);
                if (a2 != null) {
                    a(a2);
                    z = true;
                }
            }
            return z;
        } catch (IOException unused) {
            return false;
        }
    }

    public void a(byte[] bArr) {
        super.a(bArr);
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return k() == 400;
    }
}
