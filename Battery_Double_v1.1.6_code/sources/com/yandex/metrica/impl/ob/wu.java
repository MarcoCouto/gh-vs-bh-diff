package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.List;

public class wu {
    @NonNull
    public final String a;
    @NonNull
    public final List<String> b;

    public wu(@NonNull String str, @NonNull List<String> list) {
        this.a = str;
        this.b = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SdkItem{name='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", classes=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
