package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

public class sh extends sg {
    public boolean b() {
        return true;
    }

    public sh(String str) {
        super(a(str));
    }

    private static String a(@NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            Uri parse = Uri.parse(str);
            if ("http".equals(parse.getScheme())) {
                return parse.buildUpon().scheme("https").build().toString();
            }
        }
        return str;
    }
}
