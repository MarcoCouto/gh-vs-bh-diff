package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.startapp.networkTest.c.a;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.Map;

public class wd {
    private final Map<String, String> a = new HashMap();

    public wd() {
        this.a.put(TapjoyConstants.TJC_ANDROID_ID, a.a);
        this.a.put("wakeup", "wu");
        this.a.put("easy_collecting", "ec");
        this.a.put("access_point", "ap");
        this.a.put("cells_around", "ca");
        this.a.put("google_aid", "g");
        this.a.put("own_macs", "om");
        this.a.put("sim_imei", "sm");
        this.a.put("sim_info", "si");
        this.a.put("wifi_around", "wa");
        this.a.put("wifi_connected", "wc");
        this.a.put("features_collecting", "fc");
        this.a.put("location_collecting", "lc");
        this.a.put("lbs_collecting", "lbs");
        this.a.put("package_info", "pi");
        this.a.put("permissions_collecting", "pc");
        this.a.put("sdk_list", "sl");
        this.a.put("socket", "s");
        this.a.put("telephony_restricted_to_location_tracking", "trtlt");
        this.a.put("identity_light_collecting", "ilc");
        this.a.put("ble_collecting", "bc");
        this.a.put("gpl_collecting", "gplc");
        this.a.put("retry_policy", "rp");
        this.a.put("ui_parsing", "up");
        this.a.put("ui_collecting_for_bridge", "ucfb");
        this.a.put("ui_event_sending", "ues");
    }

    @NonNull
    public String a(@NonNull String str) {
        return this.a.containsKey(str) ? (String) this.a.get(str) : str;
    }
}
