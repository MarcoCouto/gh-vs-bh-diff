package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class acb<K, V> {
    private final HashMap<K, Collection<V>> a = new HashMap<>();

    public int a() {
        int i = 0;
        for (Collection size : this.a.values()) {
            i += size.size();
        }
        return i;
    }

    @Nullable
    public Collection<V> a(@Nullable K k) {
        return (Collection) this.a.get(k);
    }

    @Nullable
    public Collection<V> a(@Nullable K k, @Nullable V v) {
        Collection collection;
        Collection collection2 = (Collection) this.a.get(k);
        if (collection2 == null) {
            collection = c();
        } else {
            collection = a(collection2);
        }
        collection.add(v);
        return (Collection) this.a.put(k, collection);
    }

    @Nullable
    public Collection<V> b(@Nullable K k) {
        return (Collection) this.a.remove(k);
    }

    @Nullable
    public Collection<V> b(@Nullable K k, @Nullable V v) {
        Collection collection = (Collection) this.a.get(k);
        if (collection == null || !collection.remove(v)) {
            return null;
        }
        return a(collection);
    }

    @NonNull
    private Collection<V> c() {
        return new ArrayList();
    }

    @NonNull
    private Collection<V> a(@NonNull Collection<V> collection) {
        return new ArrayList(collection);
    }

    @NonNull
    public Set<? extends Entry<K, ? extends Collection<V>>> b() {
        return this.a.entrySet();
    }

    public String toString() {
        return this.a.toString();
    }
}
