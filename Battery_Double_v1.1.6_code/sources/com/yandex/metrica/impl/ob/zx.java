package com.yandex.metrica.impl.ob;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toolbar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

class zx {
    @NonNull
    private static final List<Class> a = new ArrayList();
    @NonNull
    private static final List<Class> b = new ArrayList();
    @NonNull
    private final TreeSet<Float> c;
    @NonNull
    private final List<aad> d;
    @NonNull
    private final List<aae> e;
    @NonNull
    private final List<zq> f;
    @NonNull
    private final aas g;
    @NonNull
    private final zs h;
    @NonNull
    private final aap i;

    static {
        a.add(ListView.class);
        a.add(GridView.class);
        a(a, "android.support.v7.widget.RecyclerView");
        a(a, "android.support.v4.view.ViewPager");
        a(a, "androidx.viewpager2.widget.ViewPager2");
        a(b, "android.support.design.widget.CoordinatorLayout");
        a(b, "android.support.v4.widget.DrawerLayout");
        a(b, "android.support.v4.widget.SlidingPaneLayout");
        a(b, "android.support.v4.widget.SwipeRefreshLayout");
        a(b, "android.support.v4.widget.NestedScrollView");
        a(b, "android.support.constraint.ConstraintLayout");
        a(b, "android.support.v7.widget.ContentFrameLayout");
        b.add(AbsoluteLayout.class);
        b.add(FrameLayout.class);
        b.add(LinearLayout.class);
        b.add(RelativeLayout.class);
        b.add(TableLayout.class);
        b.add(ScrollView.class);
        if (dl.a(14)) {
            b.add(GridLayout.class);
        }
    }

    zx(@NonNull aap aap) {
        aas aas = new aas();
        zs zsVar = new zs(aap.c);
        List asList = Arrays.asList(new aae[]{new zu(), new aab(aap.a), new zy(Collections.emptyList())});
        List asList2 = Arrays.asList(new zq[]{new aac(aap.b)});
        this(aap, aas, zsVar, asList, asList2);
    }

    @VisibleForTesting
    zx(@NonNull aap aap, @NonNull aas aas, @NonNull zs zsVar, @NonNull List<aae> list, @NonNull List<zq> list2) {
        this.c = new TreeSet<>();
        this.d = new ArrayList();
        this.i = aap;
        this.g = aas;
        this.h = zsVar;
        this.e = list;
        this.f = list2;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public zv a(@Nullable aam aam, @NonNull View view, int i2) {
        d dVar;
        c cVar;
        c cVar2;
        String name = view.getClass().getName();
        String hexString = Integer.toHexString(view.getId());
        boolean z = aam != null && (aam.q || aam.s == a.LIST);
        if (view instanceof ViewGroup) {
            dVar = d.VIEW_CONTAINER;
        } else {
            dVar = d.VIEW;
        }
        d dVar2 = dVar;
        a b2 = b(view);
        aam aam2 = null;
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            Iterator it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    cVar2 = null;
                    break;
                }
                aae aae = (aae) it.next();
                if (aae.a(textView)) {
                    cVar2 = aae.a();
                    break;
                }
            }
            if (cVar2 == null) {
                aam2 = a(textView, name, hexString, i2, z, b2);
            }
            cVar = cVar2;
        } else {
            boolean z2 = view instanceof WebView;
            cVar = null;
        }
        aam aam3 = aam2 == null ? new aam(name, hexString, cVar, i2, z, dVar2, b2) : aam2;
        this.h.a(aam3);
        return new zv(aam3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00bf A[LOOP:0: B:37:0x00b9->B:39:0x00bf, LOOP_END] */
    @NonNull
    private aam a(@NonNull TextView textView, @NonNull String str, @NonNull String str2, int i2, boolean z, @NonNull a aVar) {
        String charSequence;
        Float f2;
        Float f3;
        Float f4;
        Float f5;
        Typeface typeface;
        Boolean bool;
        Boolean bool2;
        CharSequence text = textView.getText();
        if (text == null) {
            charSequence = "";
        } else {
            charSequence = text.toString();
        }
        String str3 = charSequence;
        boolean z2 = textView.getVisibility() == 0;
        try {
            DisplayMetrics displayMetrics = textView.getContext().getResources().getDisplayMetrics();
            f4 = Float.valueOf(textView.getTextSize());
            try {
                f3 = Float.valueOf(f4.floatValue() / displayMetrics.density);
            } catch (Throwable unused) {
                f3 = null;
                f2 = null;
                f5 = f4;
                Float f6 = f3;
                this.c.add(f5);
                int a2 = this.g.a(textView);
                a a3 = a.a(textView.getEllipsize());
                typeface = textView.getTypeface();
                typeface = Typeface.create(Typeface.DEFAULT, 0);
                if (typeface == null) {
                }
                aad aad = new aad(str, str2, null, i2, z, aVar, str3, f5, f6, f2, Integer.toHexString(textView.getCurrentTextColor()), bool2, bool, z2, a2, a3);
                for (zq a4 : this.f) {
                }
                this.d.add(aad);
                return aad;
            }
            try {
                f2 = Float.valueOf(f4.floatValue() / displayMetrics.scaledDensity);
            } catch (Throwable unused2) {
                f2 = null;
                f5 = f4;
                Float f62 = f3;
                this.c.add(f5);
                int a22 = this.g.a(textView);
                a a32 = a.a(textView.getEllipsize());
                typeface = textView.getTypeface();
                typeface = Typeface.create(Typeface.DEFAULT, 0);
                if (typeface == null) {
                }
                aad aad2 = new aad(str, str2, null, i2, z, aVar, str3, f5, f62, f2, Integer.toHexString(textView.getCurrentTextColor()), bool2, bool, z2, a22, a32);
                while (r2.hasNext()) {
                }
                this.d.add(aad2);
                return aad2;
            }
        } catch (Throwable unused3) {
            f4 = null;
            f3 = null;
            f2 = null;
            f5 = f4;
            Float f622 = f3;
            this.c.add(f5);
            int a222 = this.g.a(textView);
            a a322 = a.a(textView.getEllipsize());
            typeface = textView.getTypeface();
            typeface = Typeface.create(Typeface.DEFAULT, 0);
            if (typeface == null) {
            }
            aad aad22 = new aad(str, str2, null, i2, z, aVar, str3, f5, f622, f2, Integer.toHexString(textView.getCurrentTextColor()), bool2, bool, z2, a222, a322);
            while (r2.hasNext()) {
            }
            this.d.add(aad22);
            return aad22;
        }
        f5 = f4;
        Float f6222 = f3;
        if (f5 != null && this.i.e) {
            this.c.add(f5);
        }
        int a2222 = this.g.a(textView);
        a a3222 = a.a(textView.getEllipsize());
        typeface = textView.getTypeface();
        if (typeface == null && !dl.a(20)) {
            typeface = Typeface.create(Typeface.DEFAULT, 0);
        }
        if (typeface == null) {
            bool2 = Boolean.valueOf(typeface.isBold());
            bool = Boolean.valueOf(typeface.isItalic());
        } else {
            bool2 = null;
            bool = null;
        }
        aad aad222 = new aad(str, str2, null, i2, z, aVar, str3, f5, f6222, f2, Integer.toHexString(textView.getCurrentTextColor()), bool2, bool, z2, a2222, a3222);
        while (r2.hasNext()) {
            a4.a(aad222);
        }
        this.d.add(aad222);
        return aad222;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public TreeSet<Float> a() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<aad> b() {
        return this.d;
    }

    private boolean a(@NonNull View view) {
        return a(a, view);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public List<View> a(@NonNull View view, int i2) {
        ArrayList arrayList = new ArrayList();
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int min = Math.min(this.h.a(i2), viewGroup.getChildCount());
            for (int i3 = 0; i3 < min; i3++) {
                arrayList.add(viewGroup.getChildAt(i3));
            }
        }
        return arrayList;
    }

    @Nullable
    private static Class a(@NonNull String str) {
        try {
            return Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    @NonNull
    private a b(@NonNull View view) {
        if (a(view)) {
            return a.LIST;
        }
        if (view instanceof ImageView) {
            return a.IMAGE;
        }
        if (view instanceof WebView) {
            return a.WEBVIEW;
        }
        if (e(view)) {
            return a.CONTAINER;
        }
        if (view instanceof Button) {
            return a.BUTTON;
        }
        if (d(view)) {
            return a.INPUT;
        }
        if (c(view)) {
            return a.TOOLBAR;
        }
        if (!(view instanceof TextView) || ((TextView) view).getInputType() != 0) {
            return a.OTHER;
        }
        return a.LABEL;
    }

    private boolean c(@NonNull View view) {
        if (dl.a(21)) {
            return view instanceof Toolbar;
        }
        return false;
    }

    private boolean d(@NonNull View view) {
        return (view instanceof TextView) && ((TextView) view).getInputType() != 0;
    }

    private boolean e(@NonNull View view) {
        return a(b, view);
    }

    private boolean a(@NonNull List<Class> list, @NonNull View view) {
        for (Class isInstance : list) {
            if (isInstance.isInstance(view)) {
                return true;
            }
        }
        return false;
    }

    private static void a(@NonNull List<Class> list, @NonNull String str) {
        Class a2 = a(str);
        if (a2 != null) {
            list.add(a2);
        }
    }
}
