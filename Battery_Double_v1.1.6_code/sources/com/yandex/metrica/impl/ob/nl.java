package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.gpllibrary.a.b;
import com.yandex.metrica.impl.ob.ve.a.d.C0105a.b.C0108a;

public class nl implements np<pw, C0108a> {
    @NonNull
    /* renamed from: a */
    public C0108a b(@NonNull pw pwVar) {
        C0108a aVar = new C0108a();
        aVar.b = pwVar.b;
        aVar.c = pwVar.c;
        aVar.d = a(pwVar.a);
        return aVar;
    }

    @NonNull
    public pw a(@NonNull C0108a aVar) {
        pw pwVar = new pw(a(aVar.d), aVar.b, aVar.c);
        return pwVar;
    }

    private int a(@NonNull b bVar) {
        switch (bVar) {
            case PRIORITY_LOW_POWER:
                return 1;
            case PRIORITY_BALANCED_POWER_ACCURACY:
                return 2;
            case PRIORITY_HIGH_ACCURACY:
                return 3;
            default:
                return 0;
        }
    }

    @NonNull
    private b a(int i) {
        switch (i) {
            case 1:
                return b.PRIORITY_LOW_POWER;
            case 2:
                return b.PRIORITY_BALANCED_POWER_ACCURACY;
            case 3:
                return b.PRIORITY_HIGH_ACCURACY;
            default:
                return b.PRIORITY_NO_POWER;
        }
    }
}
