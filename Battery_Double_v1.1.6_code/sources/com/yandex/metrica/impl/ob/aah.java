package com.yandex.metrica.impl.ob;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class aah implements Parcelable {
    public static final Creator<aah> CREATOR = new Creator<aah>() {
        /* renamed from: a */
        public aah createFromParcel(Parcel parcel) {
            return new aah(parcel);
        }

        /* renamed from: a */
        public aah[] newArray(int i) {
            return new aah[i];
        }
    };
    public final boolean a;
    public final boolean b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final int h;
    public final int i;
    public final int j;
    public final int k;

    public int describeContents() {
        return 0;
    }

    public aah(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3, int i4, int i5) {
        this.a = z;
        this.b = z2;
        this.c = z3;
        this.d = z4;
        this.e = z5;
        this.f = z6;
        this.g = z7;
        this.h = i2;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeByte(this.a ? (byte) 1 : 0);
        parcel.writeByte(this.b ? (byte) 1 : 0);
        parcel.writeByte(this.c ? (byte) 1 : 0);
        parcel.writeByte(this.d ? (byte) 1 : 0);
        parcel.writeByte(this.e ? (byte) 1 : 0);
        parcel.writeByte(this.f ? (byte) 1 : 0);
        parcel.writeByte(this.g ? (byte) 1 : 0);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
        parcel.writeInt(this.j);
        parcel.writeInt(this.k);
    }

    protected aah(Parcel parcel) {
        boolean z = false;
        this.a = parcel.readByte() != 0;
        this.b = parcel.readByte() != 0;
        this.c = parcel.readByte() != 0;
        this.d = parcel.readByte() != 0;
        this.e = parcel.readByte() != 0;
        this.f = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z = true;
        }
        this.g = z;
        this.h = parcel.readInt();
        this.i = parcel.readInt();
        this.j = parcel.readInt();
        this.k = parcel.readInt();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UiCollectingConfig{textSizeCollecting=");
        sb.append(this.a);
        sb.append(", relativeTextSizeCollecting=");
        sb.append(this.b);
        sb.append(", textVisibilityCollecting=");
        sb.append(this.c);
        sb.append(", textStyleCollecting=");
        sb.append(this.d);
        sb.append(", infoCollecting=");
        sb.append(this.e);
        sb.append(", nonContentViewCollecting=");
        sb.append(this.f);
        sb.append(", textLengthCollecting=");
        sb.append(this.g);
        sb.append(", tooLongTextBound=");
        sb.append(this.h);
        sb.append(", truncatedTextBound=");
        sb.append(this.i);
        sb.append(", maxEntitiesCount=");
        sb.append(this.j);
        sb.append(", maxFullContentLength=");
        sb.append(this.k);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        aah aah = (aah) obj;
        if (this.a != aah.a || this.b != aah.b || this.c != aah.c || this.d != aah.d || this.e != aah.e || this.f != aah.f || this.g != aah.g || this.h != aah.h || this.i != aah.i || this.j != aah.j) {
            return false;
        }
        if (this.k != aah.k) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return ((((((((((((((((((((this.a ? 1 : 0) * true) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + this.h) * 31) + this.i) * 31) + this.j) * 31) + this.k;
    }
}
