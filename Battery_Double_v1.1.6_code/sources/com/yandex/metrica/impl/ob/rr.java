package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import java.util.List;

public class rr {
    public final long a;
    public final boolean b;
    @Nullable
    public final List<qd> c;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WakeupConfig{collectionDuration=");
        sb.append(this.a);
        sb.append(", aggressiveRelaunch=");
        sb.append(this.b);
        sb.append(", collectionIntervalRanges=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }

    public rr(long j, boolean z, @Nullable List<qd> list) {
        this.a = j;
        this.b = z;
        this.c = list;
    }
}
