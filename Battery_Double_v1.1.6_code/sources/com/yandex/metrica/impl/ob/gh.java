package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.gg;

public interface gh<C extends gg> {
    @NonNull
    C a(@NonNull Context context, @NonNull gl glVar, @NonNull ge geVar, @NonNull ew ewVar);
}
