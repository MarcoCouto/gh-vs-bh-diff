package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

class adz implements adw<Integer> {
    adz() {
    }

    public adu a(@Nullable Integer num) {
        if (num == null || num.intValue() > 0) {
            return adu.a(this);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid quantity value ");
        sb.append(num);
        return adu.a(this, sb.toString());
    }
}
