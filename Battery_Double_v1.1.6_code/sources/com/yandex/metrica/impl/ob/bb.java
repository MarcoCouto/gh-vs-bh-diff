package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class bb {
    @NonNull
    private final abt a;
    @NonNull
    private final dj b;
    @NonNull
    private final mq c;
    private long d;
    @Nullable
    private xn e;

    public bb(@NonNull mq mqVar, @Nullable xn xnVar) {
        this(mqVar, xnVar, new abs(), new dj());
    }

    @VisibleForTesting
    bb(@NonNull mq mqVar, @Nullable xn xnVar, @NonNull abt abt, @NonNull dj djVar) {
        this.c = mqVar;
        this.e = xnVar;
        this.d = this.c.h(0);
        this.a = abt;
        this.b = djVar;
    }

    public void a() {
        if (this.e != null && this.b.a(this.d, this.e.a, "should send EVENT_IDENTITY_LIGHT")) {
            b();
            this.d = this.a.b();
            this.c.i(this.d);
        }
    }

    private void b() {
        xa.a().e();
    }

    public void a(@Nullable xn xnVar) {
        this.e = xnVar;
    }
}
