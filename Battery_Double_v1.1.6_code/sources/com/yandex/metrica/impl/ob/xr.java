package com.yandex.metrica.impl.ob;

public class xr {
    public final long a;
    public final long b;
    public final long c;
    public final long d;

    public xr(long j, long j2, long j3, long j4) {
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = j4;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SdkFingerprintingConfig{minCollectingInterval=");
        sb.append(this.a);
        sb.append(", minFirstCollectingDelay=");
        sb.append(this.b);
        sb.append(", minCollectingDelayAfterLaunch=");
        sb.append(this.c);
        sb.append(", minRequestRetryInterval=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        xr xrVar = (xr) obj;
        if (this.a != xrVar.a || this.b != xrVar.b || this.c != xrVar.c) {
            return false;
        }
        if (this.d != xrVar.d) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((((((int) (this.a ^ (this.a >>> 32))) * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31) + ((int) (this.d ^ (this.d >>> 32)));
    }
}
