package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

public class m {
    @Nullable
    public final a a;
    @Nullable
    public final Boolean b;

    public enum a {
        ACTIVE,
        WORKING_SET,
        FREQUENT,
        RARE
    }

    public m(@Nullable a aVar, @Nullable Boolean bool) {
        this.a = aVar;
        this.b = bool;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        m mVar = (m) obj;
        if (this.a != mVar.a) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(mVar.b);
        } else if (mVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
