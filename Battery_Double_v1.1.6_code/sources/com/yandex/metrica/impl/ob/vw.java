package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import java.lang.reflect.Proxy;

public class vw {
    @NonNull
    private final Context a;

    public vw(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @SuppressLint({"PrivateApi"})
    public void a(@NonNull final vv vvVar) {
        try {
            Class cls = Class.forName("com.android.installreferrer.api.InstallReferrerClient");
            Object invoke = cls.getDeclaredMethod("newBuilder", new Class[]{Context.class}).invoke(cls, new Object[]{this.a});
            Class cls2 = Class.forName("com.android.installreferrer.api.InstallReferrerStateListener");
            final Object invoke2 = invoke.getClass().getDeclaredMethod("build", new Class[0]).invoke(invoke, new Object[0]);
            Object newProxyInstance = Proxy.newProxyInstance(cls2.getClassLoader(), new Class[]{cls2}, new vs(invoke2, new vv() {
                public void a(@NonNull vu vuVar) {
                    vvVar.a(vuVar);
                    a();
                }

                public void a(@NonNull Throwable th) {
                    vvVar.a(th);
                    a();
                }

                public void a() {
                    if (invoke2 != null) {
                        try {
                            invoke2.getClass().getDeclaredMethod("endConnection", new Class[0]).invoke(invoke2, new Object[0]);
                        } catch (Throwable unused) {
                        }
                    }
                }
            }));
            invoke2.getClass().getDeclaredMethod("startConnection", new Class[]{cls2}).invoke(invoke2, new Object[]{newProxyInstance});
        } catch (Throwable th) {
            vvVar.a(th);
        }
    }
}
