package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.us.d;

public class li implements nh<StackTraceElement, d> {
    @NonNull
    /* renamed from: a */
    public d b(@NonNull StackTraceElement stackTraceElement) {
        d dVar = new d();
        dVar.b = stackTraceElement.getClassName();
        dVar.c = (String) abw.b(stackTraceElement.getFileName(), "");
        dVar.d = stackTraceElement.getLineNumber();
        dVar.e = stackTraceElement.getMethodName();
        dVar.f = stackTraceElement.isNativeMethod();
        return dVar;
    }

    @NonNull
    public StackTraceElement a(@NonNull d dVar) {
        throw new UnsupportedOperationException();
    }
}
