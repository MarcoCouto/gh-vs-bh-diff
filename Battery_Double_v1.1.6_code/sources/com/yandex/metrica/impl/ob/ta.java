package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class ta {
    private static final th d = new th("UNDEFINED_");
    protected th a;
    protected final String b;
    protected final SharedPreferences c;
    private final Map<String, Object> e = new HashMap();
    private boolean f = false;

    /* access modifiers changed from: protected */
    public abstract String f();

    public ta(Context context, String str) {
        this.b = str;
        this.c = a(context);
        this.a = new th(d.a(), this.b);
    }

    private SharedPreferences a(Context context) {
        return ti.a(context, f());
    }

    /* access modifiers changed from: protected */
    public <T extends ta> T a(String str, Object obj) {
        synchronized (this) {
            if (obj != null) {
                try {
                    this.e.put(str, obj);
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends ta> T h(String str) {
        synchronized (this) {
            this.e.put(str, this);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends ta> T h() {
        synchronized (this) {
            this.f = true;
            this.e.clear();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public String i() {
        return this.b;
    }

    public void j() {
        synchronized (this) {
            a();
            this.e.clear();
            this.f = false;
        }
    }

    private void a() {
        Editor edit = this.c.edit();
        if (this.f) {
            edit.clear();
            a(edit);
            return;
        }
        for (Entry entry : this.e.entrySet()) {
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            if (value == this) {
                edit.remove(str);
            } else if (value instanceof String) {
                edit.putString(str, (String) value);
            } else if (value instanceof Long) {
                edit.putLong(str, ((Long) value).longValue());
            } else if (value instanceof Integer) {
                edit.putInt(str, ((Integer) value).intValue());
            } else if (value instanceof Boolean) {
                edit.putBoolean(str, ((Boolean) value).booleanValue());
            } else if (value != null) {
                throw new UnsupportedOperationException();
            }
        }
        a(edit);
    }

    private void a(Editor editor) {
        editor.apply();
    }
}
