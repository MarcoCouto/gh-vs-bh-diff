package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class vf {
    private static final Map<com.yandex.metrica.impl.ob.xp.a, com.yandex.metrica.impl.ob.cg.a> a = Collections.unmodifiableMap(new HashMap<com.yandex.metrica.impl.ob.xp.a, com.yandex.metrica.impl.ob.cg.a>() {
        {
            put(com.yandex.metrica.impl.ob.xp.a.CELL, com.yandex.metrica.impl.ob.cg.a.CELL);
            put(com.yandex.metrica.impl.ob.xp.a.WIFI, com.yandex.metrica.impl.ob.cg.a.WIFI);
        }
    });
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;
    @NonNull
    private final mx<a> c;
    @NonNull
    private final act d;
    /* access modifiers changed from: private */
    @NonNull
    public final wx e;
    /* access modifiers changed from: private */
    @NonNull
    public final df f;
    /* access modifiers changed from: private */
    @NonNull
    public final aaz g;
    private a h;
    private boolean i;

    public static class a {
        @NonNull
        private final List<C0111a> a;
        @NonNull
        private final LinkedHashMap<String, Object> b = new LinkedHashMap<>();

        /* renamed from: com.yandex.metrica.impl.ob.vf$a$a reason: collision with other inner class name */
        public static class C0111a {
            @NonNull
            public final String a;
            @NonNull
            public final String b;
            @NonNull
            public final String c;
            @NonNull
            public final acb<String, String> d;
            public final long e;
            @NonNull
            public final List<com.yandex.metrica.impl.ob.cg.a> f;

            public C0111a(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull acb<String, String> acb, long j, @NonNull List<com.yandex.metrica.impl.ob.cg.a> list) {
                this.a = str;
                this.b = str2;
                this.c = str3;
                this.e = j;
                this.f = list;
                this.d = acb;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                return this.a.equals(((C0111a) obj).a);
            }

            public int hashCode() {
                return this.a.hashCode();
            }
        }

        public static class b {
            @Nullable
            byte[] a;
            @Nullable
            byte[] b;
            /* access modifiers changed from: private */
            @NonNull
            public final C0111a c;
            @Nullable
            private C0112a d;
            @Nullable
            private com.yandex.metrica.impl.ob.cg.a e;
            @Nullable
            private Integer f;
            @Nullable
            private Map<String, List<String>> g;
            @Nullable
            private Throwable h;

            /* renamed from: com.yandex.metrica.impl.ob.vf$a$b$a reason: collision with other inner class name */
            public enum C0112a {
                OFFLINE,
                INCOMPATIBLE_NETWORK_TYPE,
                COMPLETE,
                ERROR
            }

            public b(@NonNull C0111a aVar) {
                this.c = aVar;
            }

            @NonNull
            public C0111a a() {
                return this.c;
            }

            @Nullable
            public C0112a b() {
                return this.d;
            }

            public void a(@NonNull C0112a aVar) {
                this.d = aVar;
            }

            @Nullable
            public com.yandex.metrica.impl.ob.cg.a c() {
                return this.e;
            }

            public void a(@Nullable com.yandex.metrica.impl.ob.cg.a aVar) {
                this.e = aVar;
            }

            @Nullable
            public Integer d() {
                return this.f;
            }

            public void a(@Nullable Integer num) {
                this.f = num;
            }

            @Nullable
            public byte[] e() {
                return this.a;
            }

            public void a(@Nullable byte[] bArr) {
                this.a = bArr;
            }

            @Nullable
            public Map<String, List<String>> f() {
                return this.g;
            }

            public void a(@Nullable Map<String, List<String>> map) {
                this.g = map;
            }

            @Nullable
            public Throwable g() {
                return this.h;
            }

            public void a(@Nullable Throwable th) {
                this.h = th;
            }

            @Nullable
            public byte[] h() {
                return this.b;
            }

            public void b(@Nullable byte[] bArr) {
                this.b = bArr;
            }
        }

        public a(@NonNull List<C0111a> list, @NonNull List<String> list2) {
            this.a = list;
            if (!dl.a((Collection) list2)) {
                for (String put : list2) {
                    this.b.put(put, new Object());
                }
            }
        }

        public boolean a(@NonNull C0111a aVar) {
            if (this.b.get(aVar.a) != null || this.a.contains(aVar)) {
                return false;
            }
            this.a.add(aVar);
            return true;
        }

        @NonNull
        public Set<String> a() {
            HashSet hashSet = new HashSet();
            int i = 0;
            for (String add : this.b.keySet()) {
                hashSet.add(add);
                i++;
                if (i > 1000) {
                    break;
                }
            }
            return hashSet;
        }

        @NonNull
        public List<C0111a> b() {
            return this.a;
        }

        public void b(@NonNull C0111a aVar) {
            this.b.put(aVar.a, new Object());
            this.a.remove(aVar);
        }
    }

    public vf(@NonNull Context context, @NonNull mx<a> mxVar, @NonNull df dfVar, @NonNull wx wxVar, @NonNull act act) {
        this(context, mxVar, dfVar, wxVar, act, new aaw());
    }

    @VisibleForTesting
    public vf(@NonNull Context context, @NonNull mx<a> mxVar, @NonNull df dfVar, @NonNull wx wxVar, @NonNull act act, @NonNull aaz aaz) {
        this.i = false;
        this.b = context;
        this.c = mxVar;
        this.f = dfVar;
        this.e = wxVar;
        this.h = (a) this.c.a();
        this.d = act;
        this.g = aaz;
    }

    public synchronized void a() {
        this.d.a((Runnable) new Runnable() {
            public void run() {
                vf.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.i) {
            this.h = (a) this.c.a();
            c();
            this.i = true;
        }
    }

    private void c() {
        for (C0111a b2 : this.h.b()) {
            b(b2);
        }
    }

    public synchronized void a(@NonNull final yb ybVar) {
        final List<xp> list = ybVar.x;
        this.d.a((Runnable) new Runnable() {
            public void run() {
                vf.this.a(list, ybVar.u);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@Nullable List<xp> list, long j) {
        if (!dl.a((Collection) list)) {
            for (xp xpVar : list) {
                if (!(xpVar.a == null || xpVar.b == null || xpVar.c == null || xpVar.e == null || xpVar.e.longValue() < 0 || dl.a((Collection) xpVar.f))) {
                    C0111a aVar = new C0111a(xpVar.a, xpVar.b, xpVar.c, a(xpVar.d), TimeUnit.SECONDS.toMillis(xpVar.e.longValue() + j), b(xpVar.f));
                    a(aVar);
                }
            }
        }
    }

    private acb<String, String> a(List<Pair<String, String>> list) {
        acb<String, String> acb = new acb<>();
        for (Pair pair : list) {
            acb.a(pair.first, pair.second);
        }
        return acb;
    }

    private boolean a(@NonNull C0111a aVar) {
        boolean a2 = this.h.a(aVar);
        if (a2) {
            b(aVar);
            this.e.a(aVar);
        }
        d();
        return a2;
    }

    private void b(@NonNull final C0111a aVar) {
        this.d.a(new Runnable() {
            public void run() {
                if (!vf.this.f.c()) {
                    vf.this.e.b(aVar);
                    b bVar = new b(aVar);
                    com.yandex.metrica.impl.ob.cg.a a2 = vf.this.g.a(vf.this.b);
                    bVar.a(a2);
                    if (a2 == com.yandex.metrica.impl.ob.cg.a.OFFLINE) {
                        bVar.a(C0112a.OFFLINE);
                    } else if (!aVar.f.contains(a2)) {
                        bVar.a(C0112a.INCOMPATIBLE_NETWORK_TYPE);
                    } else {
                        bVar.a(C0112a.ERROR);
                        try {
                            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(aVar.b).openConnection();
                            for (Entry entry : aVar.d.b()) {
                                httpURLConnection.setRequestProperty((String) entry.getKey(), TextUtils.join(",", (Iterable) entry.getValue()));
                            }
                            httpURLConnection.setInstanceFollowRedirects(true);
                            httpURLConnection.setRequestMethod(aVar.c);
                            httpURLConnection.setConnectTimeout(com.yandex.metrica.impl.ob.sg.a.a);
                            httpURLConnection.setReadTimeout(com.yandex.metrica.impl.ob.sg.a.a);
                            httpURLConnection.connect();
                            int responseCode = httpURLConnection.getResponseCode();
                            bVar.a(C0112a.COMPLETE);
                            bVar.a(Integer.valueOf(responseCode));
                            ax.a(httpURLConnection, bVar, "[ProvidedRequestService]", 102400);
                            bVar.a(httpURLConnection.getHeaderFields());
                        } catch (Throwable th) {
                            bVar.a(th);
                        }
                    }
                    vf.this.a(bVar);
                }
            }
        }, Math.max(h.a, Math.max(aVar.e - System.currentTimeMillis(), 0)));
    }

    /* access modifiers changed from: private */
    public synchronized void a(@NonNull b bVar) {
        this.h.b(bVar.c);
        d();
        this.e.a(bVar);
    }

    private void d() {
        this.c.a(this.h);
    }

    @NonNull
    private List<com.yandex.metrica.impl.ob.cg.a> b(@NonNull List<com.yandex.metrica.impl.ob.xp.a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (com.yandex.metrica.impl.ob.xp.a aVar : list) {
            arrayList.add(a.get(aVar));
        }
        return arrayList;
    }
}
