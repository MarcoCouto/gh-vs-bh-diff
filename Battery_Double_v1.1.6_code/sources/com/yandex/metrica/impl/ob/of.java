package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.C0102a;

public class of implements np<xi, C0102a> {
    @NonNull
    private final oj a;
    @NonNull
    private final oi b;

    public of() {
        this(new oj(), new oi());
    }

    @NonNull
    /* renamed from: a */
    public C0102a b(@NonNull xi xiVar) {
        C0102a aVar = new C0102a();
        aVar.b = this.a.b(xiVar.a);
        aVar.c = this.b.b(xiVar.b);
        aVar.d = xiVar.c;
        aVar.e = xiVar.d;
        return aVar;
    }

    @NonNull
    public xi a(@NonNull C0102a aVar) {
        xi xiVar = new xi(this.a.a(aVar.b), this.b.a(aVar.c), aVar.d, aVar.e);
        return xiVar;
    }

    @VisibleForTesting
    of(@NonNull oj ojVar, @NonNull oi oiVar) {
        this.a = ojVar;
        this.b = oiVar;
    }
}
