package com.yandex.metrica.impl.ob;

import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class xi {
    @NonNull
    public final b a;
    @NonNull
    public final List<a> b;
    public final long c;
    public final long d;

    public static class a {
        @Nullable
        public final String a;
        @Nullable
        public final String b;
        @Nullable
        public final C0113a c;
        @Nullable
        public final b d;
        @Nullable
        public final c e;

        /* renamed from: com.yandex.metrica.impl.ob.xi$a$a reason: collision with other inner class name */
        public static class C0113a {
            public final int a;
            @Nullable
            public final byte[] b;
            @Nullable
            public final byte[] c;

            public C0113a(int i, @Nullable byte[] bArr, @Nullable byte[] bArr2) {
                this.a = i;
                this.b = bArr;
                this.c = bArr2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                C0113a aVar = (C0113a) obj;
                if (this.a == aVar.a && Arrays.equals(this.b, aVar.b)) {
                    return Arrays.equals(this.c, aVar.c);
                }
                return false;
            }

            public int hashCode() {
                return (((this.a * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c);
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("ManufacturerData{manufacturerId=");
                sb.append(this.a);
                sb.append(", data=");
                sb.append(Arrays.toString(this.b));
                sb.append(", dataMask=");
                sb.append(Arrays.toString(this.c));
                sb.append('}');
                return sb.toString();
            }
        }

        public static class b {
            @NonNull
            public final ParcelUuid a;
            @Nullable
            public final byte[] b;
            @Nullable
            public final byte[] c;

            public b(@NonNull String str, @Nullable byte[] bArr, @Nullable byte[] bArr2) {
                this.a = ParcelUuid.fromString(str);
                this.b = bArr;
                this.c = bArr2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                b bVar = (b) obj;
                if (this.a.equals(bVar.a) && Arrays.equals(this.b, bVar.b)) {
                    return Arrays.equals(this.c, bVar.c);
                }
                return false;
            }

            public int hashCode() {
                return (((this.a.hashCode() * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c);
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("ServiceData{uuid=");
                sb.append(this.a);
                sb.append(", data=");
                sb.append(Arrays.toString(this.b));
                sb.append(", dataMask=");
                sb.append(Arrays.toString(this.c));
                sb.append('}');
                return sb.toString();
            }
        }

        public static class c {
            @NonNull
            public final ParcelUuid a;
            @Nullable
            public final ParcelUuid b;

            public c(@NonNull ParcelUuid parcelUuid, @Nullable ParcelUuid parcelUuid2) {
                this.a = parcelUuid;
                this.b = parcelUuid2;
            }

            public boolean equals(Object obj) {
                boolean z = true;
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                c cVar = (c) obj;
                if (!this.a.equals(cVar.a)) {
                    return false;
                }
                if (this.b != null) {
                    z = this.b.equals(cVar.b);
                } else if (cVar.b != null) {
                    z = false;
                }
                return z;
            }

            public int hashCode() {
                return (this.a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0);
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("ServiceUuid{uuid=");
                sb.append(this.a);
                sb.append(", uuidMask=");
                sb.append(this.b);
                sb.append('}');
                return sb.toString();
            }
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable C0113a aVar, @Nullable b bVar, @Nullable c cVar) {
            this.a = str;
            this.b = str2;
            this.c = aVar;
            this.d = bVar;
            this.e = cVar;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == null ? aVar.a != null : !this.a.equals(aVar.a)) {
                return false;
            }
            if (this.b == null ? aVar.b != null : !this.b.equals(aVar.b)) {
                return false;
            }
            if (this.c == null ? aVar.c != null : !this.c.equals(aVar.c)) {
                return false;
            }
            if (this.d == null ? aVar.d != null : !this.d.equals(aVar.d)) {
                return false;
            }
            if (this.e != null) {
                z = this.e.equals(aVar.e);
            } else if (aVar.e != null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = (((((((this.a != null ? this.a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31;
            if (this.e != null) {
                i = this.e.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Filter{deviceAddress='");
            sb.append(this.a);
            sb.append('\'');
            sb.append(", deviceName='");
            sb.append(this.b);
            sb.append('\'');
            sb.append(", data=");
            sb.append(this.c);
            sb.append(", serviceData=");
            sb.append(this.d);
            sb.append(", serviceUuid=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }

    public static class b {
        @NonNull
        public final a a;
        @NonNull
        public final C0114b b;
        @NonNull
        public final c c;
        @NonNull
        public final d d;
        public final long e;

        public enum a {
            ALL_MATCHES,
            FIRST_MATCH,
            MATCH_LOST
        }

        /* renamed from: com.yandex.metrica.impl.ob.xi$b$b reason: collision with other inner class name */
        public enum C0114b {
            AGGRESSIVE,
            STICKY
        }

        public enum c {
            ONE_AD,
            FEW_AD,
            MAX_AD
        }

        public enum d {
            LOW_POWER,
            BALANCED,
            LOW_LATENCY
        }

        public b(@NonNull a aVar, @NonNull C0114b bVar, @NonNull c cVar, @NonNull d dVar, long j) {
            this.a = aVar;
            this.b = bVar;
            this.c = cVar;
            this.d = dVar;
            this.e = j;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.e != bVar.e || this.a != bVar.a || this.b != bVar.b || this.c != bVar.c) {
                return false;
            }
            if (this.d != bVar.d) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return (((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + ((int) (this.e ^ (this.e >>> 32)));
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Settings{callbackType=");
            sb.append(this.a);
            sb.append(", matchMode=");
            sb.append(this.b);
            sb.append(", numOfMatches=");
            sb.append(this.c);
            sb.append(", scanMode=");
            sb.append(this.d);
            sb.append(", reportDelay=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }

    public xi(@NonNull b bVar, @NonNull List<a> list, long j, long j2) {
        this.a = bVar;
        this.b = list;
        this.c = j;
        this.d = j2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        xi xiVar = (xi) obj;
        if (this.c == xiVar.c && this.d == xiVar.d && this.a.equals(xiVar.a)) {
            return this.b.equals(xiVar.b);
        }
        return false;
    }

    public int hashCode() {
        return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31) + ((int) (this.d ^ (this.d >>> 32)));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BleCollectingConfig{settings=");
        sb.append(this.a);
        sb.append(", scanFilters=");
        sb.append(this.b);
        sb.append(", sameBeaconMinReportingInterval=");
        sb.append(this.c);
        sb.append(", firstDelay=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
}
