package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class jl extends jk {
    jl(@NonNull fe feVar, @NonNull jt jtVar) {
        this(feVar, jtVar, new jx(feVar.y(), "background"));
    }

    @VisibleForTesting
    jl(@NonNull fe feVar, @NonNull jt jtVar, @NonNull jx jxVar) {
        super(feVar, jtVar, jxVar, js.a(jy.BACKGROUND).a(3600).a());
    }
}
