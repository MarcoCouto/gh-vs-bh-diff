package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.uy.a;
import com.yandex.metrica.profile.UserProfile;
import com.yandex.metrica.profile.UserProfileUpdate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class o implements ay {
    private static final Collection<Integer> f = new HashSet(Arrays.asList(new Integer[]{Integer.valueOf(14), Integer.valueOf(15)}));
    private static final adw<a> g = new adw<a>() {
        public adu a(@NonNull a aVar) {
            if (dl.a((T[]) aVar.b)) {
                return adu.a(this, "attributes list is empty");
            }
            return adu.a(this);
        }
    };
    private static final adw<Revenue> h = new aea();
    protected final Context a;
    protected final co b;
    @NonNull
    protected abl c = abd.a(this.b.h().e());
    @NonNull
    protected abb d;
    protected final cs e;
    private bf i;
    @NonNull
    private final am j;

    o(Context context, cs csVar, @NonNull co coVar, @NonNull am amVar) {
        this.a = context.getApplicationContext();
        this.e = csVar;
        this.b = coVar;
        this.j = amVar;
        this.b.a(new adj(this.c, "Crash Environment"));
        this.c = abd.a(this.b.h().e());
        this.d = abd.b(this.b.h().e());
        if (aau.a(this.b.h().j())) {
            this.c.a();
            this.d.a();
        }
    }

    /* access modifiers changed from: protected */
    public void a_() {
        this.e.a(this.b);
    }

    /* access modifiers changed from: 0000 */
    public void a(xx xxVar) {
        this.b.b(xxVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(bf bfVar) {
        this.i = bfVar;
    }

    public void c(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.b.a(str, str2);
        } else if (this.c.c()) {
            this.c.b("Invalid Error Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    public void a(Map<String, String> map) {
        if (!dl.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                c((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    public void b(Map<String, String> map) {
        if (!dl.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                d((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    public void d(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.e.a(str, str2, this.b);
        } else if (this.c.c()) {
            this.c.b("Invalid App Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    public void b() {
        this.e.b(this.b);
    }

    public void resumeSession() {
        a((String) null);
        if (this.c.c()) {
            this.c.a("Resume session");
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.e.a(this);
        this.i.b();
        this.e.a(al.d(str, this.c), this.b);
        a(this.b.d());
    }

    private void a(boolean z) {
        if (z) {
            this.e.a(al.a(al.a.EVENT_TYPE_PURGE_BUFFER, this.c), this.b);
        }
    }

    public void pauseSession() {
        if (this.c.c()) {
            this.c.a("Pause session");
        }
        b((String) null);
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        if (!this.b.a()) {
            this.e.b(this);
            this.i.a();
            this.b.c();
            this.e.a(al.e(str, this.c), this.b);
        }
    }

    public void reportEvent(@NonNull String str) {
        if (this.c.c()) {
            e(str);
        }
        a(al.c(str, this.c));
    }

    public void reportEvent(@NonNull String str, String str2) {
        if (this.c.c()) {
            e(str, str2);
        }
        a(al.a(str, str2, this.c));
    }

    public void reportEvent(@NonNull String str, @Nullable Map<String, Object> map) {
        String str2;
        Map b2 = dl.b(map);
        this.e.a(al.c(str, this.c), d(), b2);
        if (this.c.c()) {
            if (b2 == null) {
                str2 = null;
            } else {
                str2 = b2.toString();
            }
            e(str, str2);
        }
    }

    private void e(String str) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Event received: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    private void e(String str, String str2) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Event received: ");
            sb.append(d(str));
            sb.append(". With value: ");
            sb.append(d(str2));
            this.c.a(sb.toString());
        }
    }

    private void f(@Nullable String str, @Nullable String str2) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Statbox event received ");
            sb.append(" with name: ");
            sb.append(d(str));
            sb.append(" with value: ");
            String d2 = d(str2);
            if (d2.length() > 100) {
                sb.append(d2.substring(0, 100));
                sb.append("...");
            } else {
                sb.append(d2);
            }
            this.c.a(sb.toString());
        }
    }

    public void a(int i2, String str, String str2, Map<String, String> map) {
        Map map2;
        if (!a(i2)) {
            if (map == null) {
                map2 = null;
            } else {
                map2 = new HashMap(map);
            }
            a(al.a(i2, str, str2, map2, this.c));
        }
    }

    public void b(@NonNull String str, @Nullable String str2) {
        a(al.a(str, str2));
    }

    private boolean a(int i2) {
        return !f.contains(Integer.valueOf(i2)) && i2 >= 1 && i2 <= 99;
    }

    public void reportError(@NonNull String str, @Nullable Throwable th) {
        this.e.a(str, a(th), this.b);
        if (this.c.c()) {
            this.c.a("Error received: %s", d(str));
        }
    }

    @NonNull
    private ld a(@Nullable Throwable th) {
        Object[] objArr;
        Throwable th2;
        List list = null;
        if (th == null) {
            objArr = null;
            th2 = null;
        } else if (th instanceof ks) {
            objArr = th.getStackTrace();
            th2 = null;
        } else {
            th2 = th;
            objArr = null;
        }
        if (objArr != null) {
            list = Arrays.asList(objArr);
        }
        ld ldVar = new ld(th2, null, list, this.j.a(), this.j.b());
        return ldVar;
    }

    public void sendEventsBuffer() {
        this.e.a(al.a(al.a.EVENT_TYPE_PURGE_BUFFER, this.c), this.b);
    }

    public void a(@Nullable String str, @Nullable String str2) {
        a(al.b(str, str2, this.c));
        f(str, str2);
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        boolean z = !f();
        if (z) {
            this.e.a(al.e("", this.c), this.b);
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public co d() {
        return this.b;
    }

    private void a(aa aaVar) {
        this.e.a(aaVar, this.b);
    }

    public void c(@Nullable String str) {
        f(str);
    }

    private void f(String str) {
        this.e.a(ax.b(str), this.b);
        if (this.c.c()) {
            this.c.a("Error received: native");
        }
    }

    public void reportUnhandledException(@NonNull Throwable th) {
        ld ldVar = new ld(th, null, null, this.j.a(), this.j.b());
        b(ldVar);
    }

    public void a(@NonNull ld ldVar) {
        b(ldVar);
    }

    private void b(@NonNull ld ldVar) {
        this.e.a(ldVar, this.b);
        c(ldVar);
    }

    private void c(@NonNull ld ldVar) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Unhandled exception received: ");
            sb.append(ldVar.toString());
            this.c.a(sb.toString());
        }
    }

    public void reportRevenue(@NonNull Revenue revenue) {
        a(revenue);
    }

    private void a(@NonNull Revenue revenue) {
        adu a2 = h.a(revenue);
        if (a2.a()) {
            this.e.a(new cv(revenue, this.c), this.b);
            b(revenue);
        } else if (this.c.c()) {
            abl abl = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("Passed revenue is not valid. Reason: ");
            sb.append(a2.b());
            abl.b(sb.toString());
        }
    }

    private void b(@NonNull Revenue revenue) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Revenue received ");
            sb.append("for productID: ");
            sb.append(d(revenue.productID));
            sb.append(" of quantity: ");
            if (revenue.quantity != null) {
                sb.append(revenue.quantity);
            } else {
                sb.append("<null>");
            }
            sb.append(" with price: ");
            sb.append(revenue.price);
            sb.append(" ");
            sb.append(revenue.currency);
            this.c.a(sb.toString());
        }
    }

    public void reportUserProfile(@NonNull UserProfile userProfile) {
        a(userProfile);
    }

    private void a(@NonNull UserProfile userProfile) {
        uq uqVar = new uq();
        for (UserProfileUpdate userProfileUpdatePatcher : userProfile.getUserProfileUpdates()) {
            ur userProfileUpdatePatcher2 = userProfileUpdatePatcher.getUserProfileUpdatePatcher();
            userProfileUpdatePatcher2.a(this.c);
            userProfileUpdatePatcher2.a(uqVar);
        }
        a c2 = uqVar.c();
        adu a2 = g.a(c2);
        if (a2.a()) {
            this.e.a(c2, this.b);
            g();
        } else if (this.c.c()) {
            abl abl = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("UserInfo wasn't sent because ");
            sb.append(a2.b());
            abl.b(sb.toString());
        }
    }

    private void g() {
        if (this.c.c()) {
            this.c.a(new StringBuilder("User profile received").toString());
        }
    }

    public void setUserProfileID(@Nullable String str) {
        this.e.b(str, this.b);
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Set user profile ID: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    public void setStatisticsSending(boolean z) {
        this.b.h().h(z);
    }

    public void a(@NonNull ky kyVar) {
        this.e.a(new kz(kyVar, this.j.a(), this.j.b()), this.b);
    }

    public void e() {
        this.e.a(aa.a(this.a), this.b);
    }

    public boolean f() {
        return this.b.a();
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String d(@Nullable String str) {
        if (str == null) {
            return "<null>";
        }
        return str.isEmpty() ? "<empty>" : str;
    }
}
