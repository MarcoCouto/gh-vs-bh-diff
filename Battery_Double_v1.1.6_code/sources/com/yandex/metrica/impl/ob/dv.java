package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ve.a.C0102a;

@TargetApi(21)
public class dv implements dx {
    private static final long f = new C0102a().d;
    @NonNull
    private final dt a;
    /* access modifiers changed from: private */
    @NonNull
    public final ea b;
    /* access modifiers changed from: private */
    @NonNull
    public final dy c;
    /* access modifiers changed from: private */
    @NonNull
    public ScanCallback d;
    private long e;

    public dv(@NonNull Context context) {
        this(new dt(context), new ea(), new dy(), new eb(f));
    }

    public synchronized void a(@NonNull final xi xiVar) {
        BluetoothLeScanner a2 = this.a.a();
        if (a2 != null) {
            a();
            long j = xiVar.c;
            if (this.e != j) {
                this.e = j;
                this.d = new eb(this.e);
            }
            dl.a((abz<T>) new abz<BluetoothLeScanner>() {
                public void a(@NonNull BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.startScan(dv.this.c.a(xiVar.b), dv.this.b.a(xiVar.a), dv.this.d);
                }
            }, a2, "startScan", "BluetoothLeScanner");
        }
    }

    public synchronized void a() {
        BluetoothLeScanner a2 = this.a.a();
        if (a2 != null) {
            dl.a((abz<T>) new abz<BluetoothLeScanner>() {
                public void a(@NonNull BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.stopScan(dv.this.d);
                }
            }, a2, "stopScan", "BluetoothLeScanner");
        }
    }

    @VisibleForTesting
    public dv(@NonNull dt dtVar, @NonNull ea eaVar, @NonNull dy dyVar, @NonNull ScanCallback scanCallback) {
        this.e = f;
        this.a = dtVar;
        this.b = eaVar;
        this.c = dyVar;
        this.d = scanCallback;
    }
}
