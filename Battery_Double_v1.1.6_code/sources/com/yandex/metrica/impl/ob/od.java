package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ve.a;
import com.yandex.metrica.impl.ob.ve.a.d;
import com.yandex.metrica.impl.ob.ve.a.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class od implements np<yb, a> {
    private ns a = new ns();
    private nu b = new nu();
    private oc c = new oc();
    private nx d = new nx();
    private nz e = new nz();
    private ni f = new ni();
    private nv g = new nv();
    private oe h = new oe();
    private oa i = new oa();
    private nn j = new nn();
    private of k = new of();
    private on l = new on();
    private om m = new om();

    @NonNull
    /* renamed from: a */
    public a b(@NonNull yb ybVar) {
        a aVar = new a();
        aVar.C = ybVar.F;
        aVar.D = ybVar.G;
        aVar.k = new d[ybVar.p.size()];
        int i2 = 0;
        for (qm a2 : ybVar.p) {
            aVar.k[i2] = this.a.b(a2);
            i2++;
        }
        if (ybVar.a != null) {
            aVar.b = ybVar.a;
        }
        if (ybVar.b != null) {
            aVar.y = ybVar.b;
        }
        if (ybVar.c != null) {
            aVar.A = ybVar.c;
        }
        if (ybVar.d != null) {
            aVar.z = ybVar.d;
        }
        if (ybVar.j != null) {
            aVar.h = (String[]) ybVar.j.toArray(new String[ybVar.j.size()]);
        }
        if (ybVar.k != null) {
            aVar.i = (String[]) ybVar.k.toArray(new String[ybVar.k.size()]);
        }
        if (ybVar.e != null) {
            aVar.d = (String[]) ybVar.e.toArray(new String[ybVar.e.size()]);
        }
        if (ybVar.i != null) {
            aVar.g = (String[]) ybVar.i.toArray(new String[ybVar.i.size()]);
        }
        if (ybVar.l != null) {
            aVar.t = (String[]) ybVar.l.toArray(new String[ybVar.l.size()]);
        }
        if (ybVar.q != null) {
            aVar.l = this.b.b(ybVar.q);
        }
        if (ybVar.r != null) {
            aVar.m = this.c.b(ybVar.r);
        }
        if (ybVar.A != null) {
            aVar.F = this.d.b(ybVar.A);
        }
        if (ybVar.m != null) {
            aVar.o = ybVar.m;
        }
        if (ybVar.f != null) {
            aVar.e = ybVar.f;
        }
        if (ybVar.g != null) {
            aVar.f = ybVar.g;
        }
        if (ybVar.h != null) {
            aVar.B = ybVar.h;
        }
        if (ybVar.s != null) {
            aVar.r = ybVar.s;
        }
        if (ybVar.t != null) {
            aVar.N = ybVar.t;
        }
        aVar.j = this.f.b(ybVar.o);
        if (ybVar.n != null) {
            aVar.p = ybVar.n;
        }
        aVar.q = ybVar.w;
        aVar.c = ybVar.u;
        aVar.v = ybVar.v;
        aVar.I = ybVar.E.a;
        aVar.J = ybVar.E.b;
        if (ybVar.x != null) {
            aVar.n = a(ybVar.x);
        }
        if (ybVar.y != null) {
            aVar.s = ybVar.y;
        }
        if (ybVar.B != null) {
            aVar.w = this.g.b(ybVar.B);
        }
        if (ybVar.C != null) {
            aVar.x = this.i.b(ybVar.C);
        }
        if (ybVar.z != null) {
            aVar.u = this.h.b(ybVar.z);
        }
        aVar.E = ybVar.H;
        if (ybVar.D != null) {
            aVar.G = this.j.b(ybVar.D);
        }
        if (ybVar.I != null) {
            aVar.H = this.k.b(ybVar.I);
        }
        if (ybVar.J != null) {
            aVar.K = this.l.b(ybVar.J);
        }
        if (ybVar.K != null) {
            aVar.M = this.m.b(ybVar.K);
        }
        if (ybVar.L != null) {
            aVar.L = this.m.b(ybVar.L);
        }
        return aVar;
    }

    @NonNull
    public yb a(@NonNull a aVar) {
        yb.a a2 = new yb.a(this.f.a(aVar.j)).a(aVar.b).b(aVar.y).c(aVar.A).d(aVar.z).h(aVar.o).e(aVar.e).a(Arrays.asList(aVar.d)).b(Arrays.asList(aVar.g)).d(Arrays.asList(aVar.i)).c(Arrays.asList(aVar.h)).f(aVar.f).g(aVar.B).e(Arrays.asList(aVar.t)).j(aVar.r).k(aVar.N).i(aVar.p).b(aVar.q).a(aVar.c).a(aVar.v).g(a(aVar.n)).b(aVar.C).c(aVar.D).l(aVar.s).c(aVar.E).a(new xq(aVar.I, aVar.J));
        if (aVar.k != null) {
            ArrayList arrayList = new ArrayList();
            for (d a3 : aVar.k) {
                arrayList.add(this.a.a(a3));
            }
            a2.f((List<qm>) arrayList);
        }
        if (aVar.l != null) {
            a2.a(this.b.a(aVar.l));
        }
        if (aVar.m != null) {
            a2.a(this.c.a(aVar.m));
        }
        if (aVar.F != null) {
            a2.a(this.d.a(aVar.F));
        }
        if (aVar.w != null) {
            a2.h(this.g.a(aVar.w));
        }
        if (aVar.x != null) {
            a2.a(this.i.a(aVar.x));
        }
        if (aVar.u != null) {
            a2.a(this.h.a(aVar.u));
        }
        if (aVar.G != null) {
            a2.a(this.j.a(aVar.G));
        }
        if (aVar.H != null) {
            a2.a(this.k.a(aVar.H));
        }
        if (aVar.K != null) {
            a2.a(this.l.a(aVar.K));
        }
        if (aVar.M != null) {
            a2.a(this.m.a(aVar.M));
        }
        if (aVar.L != null) {
            a2.b(this.m.a(aVar.L));
        }
        return a2.a();
    }

    @NonNull
    private g[] a(@NonNull List<xp> list) {
        g[] gVarArr = new g[list.size()];
        int i2 = 0;
        for (xp a2 : list) {
            gVarArr[i2] = this.e.b(a2);
            i2++;
        }
        return gVarArr;
    }

    @NonNull
    private List<xp> a(@NonNull g[] gVarArr) {
        ArrayList arrayList = new ArrayList(gVarArr.length);
        for (g a2 : gVarArr) {
            arrayList.add(this.e.a(a2));
        }
        return arrayList;
    }
}
