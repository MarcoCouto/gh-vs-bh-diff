package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class tm implements tx {
    @NonNull
    private final ack a;

    public tm() {
        this(new ack());
    }

    @VisibleForTesting
    tm(@NonNull ack ack) {
        this.a = ack;
    }

    @NonNull
    public byte[] a(@NonNull to toVar) {
        byte[] bArr;
        if (toVar.b != null) {
            bArr = dh.c(toVar.b);
        } else {
            bArr = new byte[0];
        }
        return this.a.a(toVar.s).a(bArr);
    }
}
