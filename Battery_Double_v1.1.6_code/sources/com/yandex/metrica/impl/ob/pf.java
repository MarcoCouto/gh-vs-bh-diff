package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.CountDownLatch;

public class pf implements pd {
    private static final Intent a = new Intent("com.uodis.opendevice.OPENIDS_SERVICE").setPackage("com.huawei.hwid");
    private final ado b = new ado();

    private class a implements ServiceConnection {
        private pg b;
        private final CountDownLatch c;

        public void onServiceDisconnected(ComponentName componentName) {
        }

        private a() {
            this.c = new CountDownLatch(1);
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.b = com.yandex.metrica.impl.ob.pg.a.a(iBinder);
            this.c.countDown();
        }

        public pg a() throws InterruptedException {
            this.c.await();
            return this.b;
        }
    }

    @Nullable
    public pc a(@NonNull Context context) {
        a aVar = new a();
        if (this.b.c(context, a, 0) != null && context.bindService(a, aVar, 1)) {
            try {
                pg a2 = aVar.a();
                return new pc(com.yandex.metrica.impl.ob.pc.a.HMS, a2.a(), Boolean.valueOf(a2.b()));
            } catch (Throwable unused) {
            } finally {
                context.unbindService(aVar);
            }
        }
        return null;
    }
}
