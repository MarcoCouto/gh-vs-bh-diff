package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.vd.a;
import com.yandex.metrica.impl.ob.vd.a.C0101a;
import java.util.ArrayList;
import java.util.List;

public class ob implements nh<wr, a> {
    @NonNull
    /* renamed from: a */
    public a b(@NonNull wr wrVar) {
        a aVar = new a();
        aVar.b = new C0101a[wrVar.a.size()];
        for (int i = 0; i < wrVar.a.size(); i++) {
            aVar.b[i] = a((wu) wrVar.a.get(i));
        }
        aVar.c = wrVar.b;
        aVar.d = wrVar.c;
        aVar.e = wrVar.d;
        aVar.f = wrVar.e;
        return aVar;
    }

    @NonNull
    public wr a(@NonNull a aVar) {
        ArrayList arrayList = new ArrayList(aVar.b.length);
        for (C0101a a : aVar.b) {
            arrayList.add(a(a));
        }
        wr wrVar = new wr(arrayList, aVar.c, aVar.d, aVar.e, aVar.f);
        return wrVar;
    }

    @NonNull
    private C0101a a(@NonNull wu wuVar) {
        C0101a aVar = new C0101a();
        aVar.b = wuVar.a;
        List<String> list = wuVar.b;
        aVar.c = new String[list.size()];
        int i = 0;
        for (String str : list) {
            aVar.c[i] = str;
            i++;
        }
        return aVar;
    }

    @NonNull
    private wu a(@NonNull C0101a aVar) {
        ArrayList arrayList = new ArrayList();
        if (aVar.c != null && aVar.c.length > 0) {
            arrayList = new ArrayList(aVar.c.length);
            for (String add : aVar.c) {
                arrayList.add(add);
            }
        }
        return new wu(dh.a(aVar.b), arrayList);
    }
}
