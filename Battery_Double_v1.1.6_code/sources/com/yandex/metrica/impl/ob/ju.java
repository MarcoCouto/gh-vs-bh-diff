package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class ju {
    @NonNull
    private final fe a;
    @NonNull
    private final jt b;
    @NonNull
    private final a c;
    @NonNull
    private final am d;
    @NonNull
    private final jn<jp> e;
    @NonNull
    private final jn<jp> f;
    @Nullable
    private jo g;
    @Nullable
    private b h;

    public interface a {
        void a(@NonNull aa aaVar, @NonNull jv jvVar);
    }

    public enum b {
        EMPTY,
        BACKGROUND,
        FOREGROUND
    }

    public ju(@NonNull fe feVar, @NonNull jt jtVar, @NonNull a aVar) {
        this(feVar, jtVar, aVar, new jm(feVar, jtVar), new jl(feVar, jtVar), new am(feVar.k()));
    }

    @VisibleForTesting
    public ju(@NonNull fe feVar, @NonNull jt jtVar, @NonNull a aVar, @NonNull jn<jp> jnVar, @NonNull jn<jp> jnVar2, @NonNull am amVar) {
        this.h = null;
        this.a = feVar;
        this.c = aVar;
        this.e = jnVar;
        this.f = jnVar2;
        this.b = jtVar;
        this.d = amVar;
    }

    public synchronized void a(@NonNull aa aaVar) {
        g(aaVar);
        switch (this.h) {
            case FOREGROUND:
                if (!a(this.g, aaVar)) {
                    this.g = f(aaVar);
                    break;
                } else {
                    this.g.b(aaVar.r());
                    break;
                }
            case BACKGROUND:
                c(this.g, aaVar);
                this.g = f(aaVar);
                break;
            case EMPTY:
                this.g = f(aaVar);
                break;
        }
    }

    public synchronized void b(@NonNull aa aaVar) {
        c(aaVar).a(false);
        if (this.h != b.EMPTY) {
            c(this.g, aaVar);
        }
        this.h = b.EMPTY;
    }

    @NonNull
    public synchronized jo c(@NonNull aa aaVar) {
        g(aaVar);
        if (this.h != b.EMPTY && !a(this.g, aaVar)) {
            this.h = b.EMPTY;
            this.g = null;
        }
        switch (this.h) {
            case FOREGROUND:
                return this.g;
            case BACKGROUND:
                this.g.b(aaVar.r());
                return this.g;
            default:
                this.g = i(aaVar);
                return this.g;
        }
    }

    @NonNull
    public jv d(@NonNull aa aaVar) {
        return a(c(aaVar), aaVar.r());
    }

    public synchronized long a() {
        return this.g == null ? 10000000000L : this.g.c() - 1;
    }

    @NonNull
    public jv a(long j) {
        long a2 = this.b.a();
        this.a.j().a(a2, jy.BACKGROUND, j);
        return new jv().a(a2).a(jy.BACKGROUND).b(0).c(0);
    }

    @NonNull
    private jo f(@NonNull aa aaVar) {
        long r = aaVar.r();
        jo a2 = this.e.a(new jp(r, aaVar.s()));
        this.h = b.FOREGROUND;
        this.a.C().a();
        this.c.a(aa.a(aaVar, this.d), a(a2, r));
        return a2;
    }

    private void g(@NonNull aa aaVar) {
        if (this.h == null) {
            jo a2 = this.e.a();
            if (a(a2, aaVar)) {
                this.g = a2;
                this.h = b.FOREGROUND;
                return;
            }
            jo a3 = this.f.a();
            if (a(a3, aaVar)) {
                this.g = a3;
                this.h = b.BACKGROUND;
                return;
            }
            this.g = null;
            this.h = b.EMPTY;
        }
    }

    @Nullable
    private jo h(@NonNull aa aaVar) {
        if (this.h != null) {
            return this.g;
        }
        jo a2 = this.e.a();
        if (!b(a2, aaVar)) {
            return a2;
        }
        jo a3 = this.f.a();
        if (!b(a3, aaVar)) {
            return a3;
        }
        return null;
    }

    private boolean a(@Nullable jo joVar, @NonNull aa aaVar) {
        if (joVar == null) {
            return false;
        }
        if (joVar.a(aaVar.r())) {
            return true;
        }
        c(joVar, aaVar);
        return false;
    }

    private boolean b(@Nullable jo joVar, @NonNull aa aaVar) {
        if (joVar == null) {
            return false;
        }
        return joVar.a(aaVar.r());
    }

    private void c(@NonNull jo joVar, @Nullable aa aaVar) {
        if (joVar.h()) {
            this.c.a(aa.b(aaVar), a(joVar));
            joVar.a(false);
        }
        joVar.e();
    }

    @NonNull
    private jo i(@NonNull aa aaVar) {
        this.h = b.BACKGROUND;
        long r = aaVar.r();
        jo a2 = this.f.a(new jp(r, aaVar.s()));
        if (this.a.u().d()) {
            this.c.a(aa.a(aaVar, this.d), a(a2, aaVar.r()));
        } else if (aaVar.g() == com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_FIRST_ACTIVATION.a()) {
            this.c.a(aaVar, a(a2, r));
            this.c.a(aa.a(aaVar, this.d), a(a2, r));
        }
        return a2;
    }

    @NonNull
    private jv a(@NonNull jo joVar) {
        return new jv().a(joVar.c()).a(joVar.a()).b(joVar.g()).c(joVar.d());
    }

    @NonNull
    private jv a(@NonNull jo joVar, long j) {
        return new jv().a(joVar.c()).b(joVar.g()).c(joVar.c(j)).a(joVar.a());
    }

    @NonNull
    public jv e(@NonNull aa aaVar) {
        jo h2 = h(aaVar);
        if (h2 != null) {
            return new jv().a(h2.c()).b(h2.g()).c(h2.f()).a(h2.a());
        }
        return a(aaVar.s());
    }
}
