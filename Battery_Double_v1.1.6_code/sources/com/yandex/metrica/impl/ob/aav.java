package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class aav {
    public static boolean a(@Nullable Collection<?> collection, @Nullable Collection<?> collection2) {
        HashSet hashSet;
        if (collection == null && collection2 == null) {
            return true;
        }
        if (collection == null || collection2 == null || collection.size() != collection2.size()) {
            return false;
        }
        if (collection instanceof HashSet) {
            hashSet = (HashSet) collection;
        } else if (collection2 instanceof HashSet) {
            HashSet hashSet2 = (HashSet) collection2;
            collection2 = collection;
            hashSet = hashSet2;
        } else {
            hashSet = new HashSet(collection);
        }
        for (Object contains : collection2) {
            if (!hashSet.contains(contains)) {
                return false;
            }
        }
        return true;
    }

    @NonNull
    public static List<Integer> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(Integer.valueOf(valueOf));
        }
        return arrayList;
    }

    @NonNull
    public static int[] a(@NonNull List<Integer> list) {
        int[] iArr = new int[list.size()];
        int i = 0;
        for (Integer intValue : list) {
            iArr[i] = intValue.intValue();
            i++;
        }
        return iArr;
    }

    public static <K, V> void a(@NonNull Map<K, V> map, @Nullable K k, @Nullable V v) {
        if (k != null && v != null) {
            map.put(k, v);
        }
    }
}
