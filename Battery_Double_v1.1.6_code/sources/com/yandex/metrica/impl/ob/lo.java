package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public abstract class lo {
    @NonNull
    private final lu a;
    @NonNull
    private final mq b;
    @NonNull
    private final AtomicLong c;
    @NonNull
    private final AtomicLong d;
    @NonNull
    private final AtomicLong e;
    @NonNull
    private final ContentValues f = new ContentValues();

    /* access modifiers changed from: protected */
    public abstract long c(long j);

    /* access modifiers changed from: protected */
    public abstract mq d(long j);

    public abstract String e();

    lo(@NonNull lu luVar, @NonNull mq mqVar) {
        this.a = luVar;
        this.b = mqVar;
        this.c = new AtomicLong(f());
        this.d = new AtomicLong(a(Long.MAX_VALUE));
        this.e = new AtomicLong(c(-1));
    }

    public long a() {
        return this.c.get();
    }

    public long b() {
        return this.d.get();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public mq c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public lu d() {
        return this.a;
    }

    private long f() {
        try {
            SQLiteDatabase readableDatabase = d().getReadableDatabase();
            if (readableDatabase != null) {
                return aax.a(readableDatabase, e());
            }
        } catch (Throwable unused) {
        }
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j, String str) {
        this.c.incrementAndGet();
        this.e.incrementAndGet();
        d(this.e.get()).q();
        if (this.d.get() > j) {
            this.d.set(j);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.c.getAndAdd((long) (-i));
        this.d.set(a(Long.MAX_VALUE));
    }

    public long a(long j) {
        Cursor cursor;
        String format = String.format(Locale.US, "Select min(%s) from %s", new Object[]{"timestamp", e()});
        Cursor cursor2 = null;
        try {
            SQLiteDatabase readableDatabase = d().getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.rawQuery(format, null);
                try {
                    if (cursor.moveToFirst()) {
                        long j2 = cursor.getLong(0);
                        if (j2 != 0) {
                            j = j2;
                        }
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor2 = cursor;
                    dl.a(cursor2);
                    throw th;
                }
            } else {
                cursor = null;
            }
            dl.a(cursor);
        } catch (Throwable th2) {
            th = th2;
            dl.a(cursor2);
            throw th;
        }
        return j;
    }

    public synchronized void b(long j, String str) {
        try {
            SQLiteDatabase writableDatabase = d().getWritableDatabase();
            if (writableDatabase != null) {
                if (writableDatabase.insert(e(), null, c(j, str)) != -1) {
                    a(j, str);
                }
            }
        } catch (Throwable unused) {
        }
    }

    @NonNull
    private ContentValues c(long j, @NonNull String str) {
        this.f.clear();
        this.f.put("incremental_id", Long.valueOf(this.e.get() + 1));
        this.f.put("timestamp", Long.valueOf(j));
        this.f.put("data", str);
        return this.f;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0057 */
    @NonNull
    public synchronized Map<Long, String> b(int i) {
        LinkedHashMap linkedHashMap;
        Cursor cursor;
        linkedHashMap = new LinkedHashMap();
        try {
            SQLiteDatabase readableDatabase = d().getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.query(e(), new String[]{"incremental_id", "data"}, null, null, null, null, "incremental_id ASC", String.valueOf(i));
                while (true) {
                    try {
                        if (cursor.moveToNext()) {
                            this.f.clear();
                            aax.a(cursor, this.f);
                            aav.a(linkedHashMap, this.f.getAsLong("incremental_id"), this.f.getAsString("data"));
                        }
                    } catch (Throwable th) {
                        th = th;
                        dl.a(cursor);
                        throw th;
                    }
                }
            } else {
                cursor = null;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            dl.a(cursor);
            throw th;
        }
        dl.a(cursor);
        return linkedHashMap;
    }

    public synchronized int b(long j) {
        int i;
        String format = String.format(Locale.US, "%s <= ?", new Object[]{"incremental_id"});
        try {
            SQLiteDatabase writableDatabase = d().getWritableDatabase();
            if (writableDatabase != null) {
                i = writableDatabase.delete(e(), format, new String[]{String.valueOf(j)});
                if (i > 0) {
                    try {
                        a(i);
                    } catch (Throwable unused) {
                    }
                }
            }
        } catch (Throwable unused2) {
        }
        i = 0;
        return i;
    }

    public synchronized int c(int i) {
        int i2;
        if (i < 1) {
            return 0;
        }
        String format = String.format(Locale.US, "%1$s <= (select max(%1$s) from (select %1$s from %2$s order by %1$s limit ?))", new Object[]{"incremental_id", e()});
        try {
            SQLiteDatabase writableDatabase = d().getWritableDatabase();
            if (writableDatabase != null) {
                i2 = writableDatabase.delete(e(), format, new String[]{String.valueOf(i)});
                if (i2 > 0) {
                    try {
                        a(i2);
                    } catch (Throwable unused) {
                    }
                }
            }
        } catch (Throwable unused2) {
        }
        i2 = 0;
        return i2;
    }
}
