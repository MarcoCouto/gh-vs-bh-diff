package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.abc.a;
import com.yandex.metrica.impl.ob.ve.a.h;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class yt {
    @NonNull
    private final oa a;

    yt() {
        this(new oa());
    }

    @VisibleForTesting
    yt(@NonNull oa oaVar) {
        this.a = oaVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        h hVar = new h();
        JSONObject optJSONObject = aVar.optJSONObject("sdk_list");
        if (optJSONObject != null) {
            hVar.b = abw.a(abc.a(optJSONObject, "min_collecting_interval_seconds"), TimeUnit.SECONDS, hVar.b);
            hVar.c = abw.a(abc.a(optJSONObject, "min_first_collecting_delay_seconds"), TimeUnit.SECONDS, hVar.c);
            hVar.d = abw.a(abc.a(optJSONObject, "min_collecting_delay_after_launch_seconds"), TimeUnit.SECONDS, hVar.d);
            hVar.e = abw.a(abc.a(optJSONObject, "min_request_retry_interval_seconds"), TimeUnit.SECONDS, hVar.e);
        }
        yxVar.a(this.a.a(hVar));
    }
}
