package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.i;
import com.yandex.metrica.impl.ob.abv.a;

public class ii extends hu {
    public ii(fe feVar) {
        super(feVar);
    }

    public boolean a(@NonNull aa aaVar) {
        b(aaVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void b(aa aaVar) {
        String l = aaVar.l();
        i a = abv.a(l);
        String h = a().h();
        i a2 = abv.a(h);
        if (!a.equals(a2)) {
            if (a(a, a2)) {
                aaVar.a(h);
                a(aaVar, a.LOGOUT);
            } else if (c(a, a2)) {
                a(aaVar, a.LOGIN);
            } else if (b(a, a2)) {
                a(aaVar, a.SWITCH);
            } else {
                a(aaVar, a.UPDATE);
            }
            a().a(l);
        }
    }

    private void a(aa aaVar, a aVar) {
        aaVar.c(abv.a(aVar));
        a().e().e(aaVar);
    }

    private boolean a(i iVar, i iVar2) {
        return TextUtils.isEmpty(iVar.a()) && !TextUtils.isEmpty(iVar2.a());
    }

    private boolean b(i iVar, i iVar2) {
        return !TextUtils.isEmpty(iVar.a()) && !iVar.a().equals(iVar2.a());
    }

    private boolean c(i iVar, i iVar2) {
        return !TextUtils.isEmpty(iVar.a()) && TextUtils.isEmpty(iVar2.a());
    }
}
