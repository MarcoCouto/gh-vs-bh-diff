package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.SparseArray;
import com.yandex.metrica.impl.ob.uy.a;
import com.yandex.metrica.impl.ob.uy.a.C0096a;
import java.util.ArrayList;
import java.util.HashMap;

public class uq {
    private static final int[] a = {0, 1, 2, 3};
    private final SparseArray<HashMap<String, C0096a>> b;
    private int c;

    public uq() {
        this(a);
    }

    @VisibleForTesting
    uq(int[] iArr) {
        this.b = new SparseArray<>();
        this.c = 0;
        for (int put : iArr) {
            this.b.put(put, new HashMap());
        }
    }

    @Nullable
    public C0096a a(int i, @NonNull String str) {
        return (C0096a) ((HashMap) this.b.get(i)).get(str);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull C0096a aVar) {
        ((HashMap) this.b.get(aVar.c)).put(new String(aVar.b), aVar);
    }

    public int a() {
        return this.c;
    }

    public void b() {
        this.c++;
    }

    @NonNull
    public a c() {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.b.size(); i++) {
            for (C0096a add : ((HashMap) this.b.get(this.b.keyAt(i))).values()) {
                arrayList.add(add);
            }
        }
        aVar.b = (C0096a[]) arrayList.toArray(new C0096a[arrayList.size()]);
        return aVar;
    }
}
