package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;

public abstract class mp {
    public static final String u = "mp";
    private final lx a;
    private final String b;

    public mp(lx lxVar) {
        this(lxVar, null);
    }

    public mp(lx lxVar, String str) {
        this.a = lxVar;
        this.b = str;
    }

    public String p() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public th q(String str) {
        return new th(str, p());
    }

    /* access modifiers changed from: protected */
    public <T extends mp> T b(String str, String str2) {
        synchronized (this) {
            this.a.b(str, str2);
        }
        return this;
    }

    public <T extends mp> T a(String str, long j) {
        synchronized (this) {
            this.a.b(str, j);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public <T extends mp> T a(String str, int i) {
        synchronized (this) {
            this.a.b(str, i);
        }
        return this;
    }

    public <T extends mp> T a(String str, boolean z) {
        synchronized (this) {
            this.a.b(str, z);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends mp> T a(String str, String[] strArr) {
        String str2;
        try {
            JSONArray jSONArray = new JSONArray();
            for (String put : strArr) {
                jSONArray.put(put);
            }
            str2 = jSONArray.toString();
        } catch (Throwable unused) {
            str2 = null;
        }
        this.a.b(str, str2);
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends mp> T a(String str, List<String> list) {
        return a(str, (String[]) list.toArray(new String[list.size()]));
    }

    public <T extends mp> T r(String str) {
        synchronized (this) {
            this.a.a(str);
        }
        return this;
    }

    public void q() {
        synchronized (this) {
            this.a.b();
        }
    }

    public long b(String str, long j) {
        return this.a.a(str, j);
    }

    /* access modifiers changed from: 0000 */
    public int b(String str, int i) {
        return this.a.a(str, i);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String s(@NonNull String str) {
        return this.a.a(str, (String) null);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String c(@NonNull String str, @Nullable String str2) {
        return this.a.a(str, str2);
    }

    public boolean b(String str, boolean z) {
        return this.a.a(str, z);
    }

    public boolean t(@NonNull String str) {
        return this.a.b(str);
    }

    /* access modifiers changed from: 0000 */
    public String[] b(String str, String[] strArr) {
        String a2 = this.a.a(str, (String) null);
        if (TextUtils.isEmpty(a2)) {
            return strArr;
        }
        try {
            JSONArray jSONArray = new JSONArray(a2);
            String[] strArr2 = new String[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    strArr2[i] = jSONArray.optString(i);
                    i++;
                } catch (Throwable unused) {
                }
            }
            return strArr2;
        } catch (Throwable unused2) {
            return strArr;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public List<String> b(@NonNull String str, @Nullable List<String> list) {
        String[] b2 = b(str, list == null ? null : (String[]) list.toArray(new String[list.size()]));
        if (b2 == null) {
            return null;
        }
        return Arrays.asList(b2);
    }
}
