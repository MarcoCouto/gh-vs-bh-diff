package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.uu.b.C0092b;
import java.util.concurrent.TimeUnit;

public class rw {
    @NonNull
    public C0092b a(@NonNull rb rbVar) {
        C0092b bVar = new C0092b();
        Location c = rbVar.c();
        bVar.b = rbVar.a() == null ? bVar.b : rbVar.a().longValue();
        bVar.d = TimeUnit.MILLISECONDS.toSeconds(c.getTime());
        bVar.l = ci.a(rbVar.a);
        bVar.c = TimeUnit.MILLISECONDS.toSeconds(rbVar.b());
        bVar.m = TimeUnit.MILLISECONDS.toSeconds(rbVar.d());
        bVar.e = c.getLatitude();
        bVar.f = c.getLongitude();
        bVar.g = Math.round(c.getAccuracy());
        bVar.h = Math.round(c.getBearing());
        bVar.i = Math.round(c.getSpeed());
        bVar.j = (int) Math.round(c.getAltitude());
        bVar.k = a(c.getProvider());
        bVar.n = ci.a(rbVar.e());
        return bVar;
    }

    private static int a(@NonNull String str) {
        if ("gps".equals(str)) {
            return 1;
        }
        if ("network".equals(str)) {
            return 2;
        }
        return "fused".equals(str) ? 3 : 0;
    }
}
