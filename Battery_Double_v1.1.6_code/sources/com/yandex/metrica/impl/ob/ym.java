package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.abc.a;
import com.yandex.metrica.impl.ob.ve.a.c;
import org.json.JSONObject;

public class ym {
    @NonNull
    private final nn a;

    ym() {
        this(new nn());
    }

    @VisibleForTesting
    ym(@NonNull nn nnVar) {
        this.a = nnVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull yx yxVar, @NonNull a aVar) {
        if (yxVar.a().f) {
            c cVar = new c();
            JSONObject optJSONObject = aVar.optJSONObject("identity_light_collecting");
            if (optJSONObject != null) {
                cVar.b = optJSONObject.optLong("min_interval_seconds", cVar.b);
            }
            yxVar.a(this.a.a(cVar));
        }
    }
}
