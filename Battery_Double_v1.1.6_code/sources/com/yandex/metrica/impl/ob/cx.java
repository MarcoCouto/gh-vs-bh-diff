package com.yandex.metrica.impl.ob;

import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;
import com.tapjoy.TapjoyConstants;
import java.util.regex.Pattern;

public class cx {
    private static final Pattern a = Pattern.compile(".*at com\\.yandex\\.metrica\\.push\\.*");
    private static final Pattern b = Pattern.compile(".*at com\\.yandex\\.metrica\\.(?!push)");

    @VisibleForTesting
    static class a {
        @NonNull
        static final String a = new a().a();

        a() {
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        @NonNull
        public String a() {
            String str = "native";
            if (a("com.unity3d.player.UnityPlayer")) {
                return TapjoyConstants.TJC_PLUGIN_UNITY;
            }
            if (a("mono.MonoPackageManager")) {
                return "xamarin";
            }
            if (a("org.apache.cordova.CordovaPlugin")) {
                return "cordova";
            }
            return a("com.facebook.react.ReactRootView") ? "react" : str;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public boolean a(String str) {
            return cx.b(str);
        }
    }

    static void a() {
        StringBuilder sb = new StringBuilder();
        sb.append("Initializing of Metrica, ");
        sb.append(dh.b("release"));
        sb.append(" type, Version ");
        sb.append("3.13.1");
        sb.append(", API Level ");
        sb.append(87);
        sb.append(", Dated ");
        sb.append("03.03.2020");
        sb.append(".");
        Log.i("AppMetrica", sb.toString());
    }

    @NonNull
    public static String b() {
        return a.a;
    }

    public static String a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("/");
        sb.append("3.13.1");
        sb.append(".");
        sb.append("72430");
        sb.append(" (");
        sb.append(c());
        sb.append("; Android ");
        sb.append(VERSION.RELEASE);
        sb.append(")");
        return sb.toString();
    }

    public static String c() {
        if (Build.MODEL.startsWith(Build.MANUFACTURER)) {
            return dh.b(Build.MODEL);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(dh.b(Build.MANUFACTURER));
        sb.append(" ");
        sb.append(Build.MODEL);
        return sb.toString();
    }

    static boolean a(Throwable th) {
        String a2 = dl.a(th);
        return !TextUtils.isEmpty(a2) && b.matcher(a2).find();
    }

    static boolean b(Throwable th) {
        String a2 = dl.a(th);
        return !TextUtils.isEmpty(a2) && a.matcher(a2).find();
    }

    public static boolean b(String str) {
        try {
            return Class.forName(str) != null;
        } catch (Throwable unused) {
            return false;
        }
    }
}
