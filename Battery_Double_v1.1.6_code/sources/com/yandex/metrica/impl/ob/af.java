package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class af implements bz {
    @NonNull
    private final a a;

    static class a {

        /* renamed from: com.yandex.metrica.impl.ob.af$a$a reason: collision with other inner class name */
        static class C0087a {
            @NonNull
            final String a;

            public C0087a(@NonNull String str) {
                this.a = str;
            }
        }

        a() {
        }

        public C0087a a(@Nullable byte[] bArr) {
            try {
                if (!dl.a(bArr)) {
                    return new C0087a(new JSONObject(new String(bArr, "UTF-8")).optString("status"));
                }
                return null;
            } catch (Throwable unused) {
                return null;
            }
        }
    }

    public af() {
        this(new a());
    }

    @VisibleForTesting
    af(@NonNull a aVar) {
        this.a = aVar;
    }

    public boolean a(int i, @Nullable byte[] bArr, @Nullable Map<String, List<String>> map) {
        if (200 == i) {
            C0087a a2 = this.a.a(bArr);
            if (a2 != null) {
                return "accepted".equals(a2.a);
            }
        }
        return false;
    }
}
