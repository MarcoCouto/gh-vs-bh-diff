package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.uy.a.C0096a;

public class uj extends ud<Double> {
    public uj(@NonNull String str, double d, @NonNull adw<String> adw, @NonNull ua uaVar) {
        super(1, str, Double.valueOf(d), adw, uaVar);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0096a aVar) {
        aVar.e.c = ((Double) b()).doubleValue();
    }
}
