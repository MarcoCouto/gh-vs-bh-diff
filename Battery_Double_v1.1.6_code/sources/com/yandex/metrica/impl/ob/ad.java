package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.support.annotation.NonNull;

@TargetApi(14)
public class ad implements ActivityLifecycleCallbacks {
    /* access modifiers changed from: private */
    public final bj a;
    @NonNull
    private final act b;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public ad(bj bjVar, @NonNull act act) {
        this.a = bjVar;
        this.b = act;
    }

    public void onActivityResumed(final Activity activity) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                ad.this.a.b(activity);
            }
        });
    }

    public void onActivityPaused(final Activity activity) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                ad.this.a.c(activity);
            }
        });
    }
}
