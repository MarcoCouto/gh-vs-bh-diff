package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.ViewGroup;

public class aan {
    @Nullable
    private aap a;
    @NonNull
    private final b b;
    @NonNull
    private final a c;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public zr a(@NonNull zx zxVar) {
            return new zr(zxVar.a());
        }
    }

    static class b {
        b() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public zx a(@NonNull aap aap) {
            return new zx(aap);
        }
    }

    public aan(@Nullable aap aap) {
        this(aap, new b(), new a());
    }

    @VisibleForTesting
    aan(@Nullable aap aap, @NonNull b bVar, @NonNull a aVar) {
        this.a = aap;
        this.b = bVar;
        this.c = aVar;
    }

    @NonNull
    public aaf a(@NonNull Activity activity, @NonNull zt ztVar) {
        View view;
        aaf aaf = new aaf();
        if (this.a != null) {
            try {
                view = (ViewGroup) activity.findViewById(16908290);
            } catch (Throwable unused) {
                view = null;
            }
            if (view != null) {
                zx a2 = this.b.a(this.a);
                aaf.a(a2, view, ztVar);
                if (this.a.e) {
                    zr a3 = this.c.a(a2);
                    for (aad a4 : a2.b()) {
                        a3.a(a4);
                    }
                }
            }
        }
        return aaf;
    }

    public void a(@Nullable aap aap) {
        this.a = aap;
    }
}
