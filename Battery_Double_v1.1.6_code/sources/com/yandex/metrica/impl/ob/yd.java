package com.yandex.metrica.impl.ob;

public class yd {
    public final long a;

    public yd(long j) {
        this.a = j;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StatSending{disabledReportingInterval=");
        sb.append(this.a);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        if (this.a != ((yd) obj).a) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (int) (this.a ^ (this.a >>> 32));
    }
}
