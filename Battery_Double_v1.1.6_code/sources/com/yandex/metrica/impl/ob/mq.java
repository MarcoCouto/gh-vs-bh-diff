package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.yandex.metrica.impl.ob.ca.a;

public class mq extends mp {
    static final th A = new th("LAST_STARTUP_SEND_ATTEMPT_TIME");
    private static final th B = new th("LAST_MIGRATION_VERSION");
    private static final th C = new th("LAST_WIFI_SCANNING_ATTEMPT_TIME");
    private static final th D = new th("LAST_LBS_SCANNING_ATTEMPT_TIME");
    private static final th E = new th("LAST_GPS_SCANNING_ATTEMPT_TIME");
    private static final th F = new th("LAST_FUSED_SCANNING_ATTEMPT_TIME");
    static final th a = new th("LOCATION_TRACKING_ENABLED");
    @Deprecated
    public static final th b = new th("COLLECT_INSTALLED_APPS");
    public static final th c = new th("INSTALLED_APP_COLLECTING");
    static final th d = new th("REFERRER");
    static final th e = new th("REFERRER_FROM_PLAY_SERVICES");
    static final th f = new th("REFERRER_HANDLED");
    static final th g = new th("REFERRER_HOLDER_STATE");
    static final th h = new th("PREF_KEY_OFFSET");
    static final th i = new th("UNCHECKED_TIME");
    static final th j = new th("L_REQ_NUM");
    static final th k = new th("L_ID");
    static final th l = new th("LBS_ID");
    static final th m = new th("STATISTICS_RESTRICTED_IN_MAIN");
    static final th n = new th("SDKFCE");
    static final th o = new th("FST");
    static final th p = new th("LSST");
    static final th q = new th("FSDKFCO");
    static final th r = new th("SRSDKFC");
    static final th s = new th("LSDKFCAT");
    static final th t = new th("LAST_IDENTITY_LIGHT_SEND_TIME");
    static final th v = new th("NEXT_REPORT_SEND_ATTEMPT_NUMBER");
    static final th w = new th("NEXT_LOCATION_SEND_ATTEMPT_NUMBER");
    static final th x = new th("NEXT_STARTUP_SEND_ATTEMPT_NUMBER");
    static final th y = new th("LAST_REPORT_SEND_ATTEMPT_TIME");
    static final th z = new th("LAST_LOCATION_SEND_ATTEMPT_TIME");

    public mq(lx lxVar) {
        super(lxVar);
    }

    public boolean a() {
        return b(c.b(), false);
    }

    public String b() {
        return c(d.b(), null);
    }

    public vu c() {
        return b(c(e.b(), null));
    }

    private vu b(String str) {
        if (str == null) {
            return null;
        }
        try {
            return vu.a(Base64.encode(str.getBytes(), 0));
        } catch (d unused) {
            return null;
        }
    }

    public boolean d() {
        return b(f.b(), false);
    }

    public mq a(boolean z2) {
        return (mq) a(c.b(), z2);
    }

    public mq a(String str) {
        return (mq) b(d.b(), str);
    }

    public mq a(vu vuVar) {
        return (mq) b(e.b(), b(vuVar));
    }

    private String b(@Nullable vu vuVar) {
        if (vuVar == null) {
            return null;
        }
        return new String(Base64.encode(vuVar.a(), 0));
    }

    public mq e() {
        return (mq) a(f.b(), true);
    }

    public mq f() {
        return (mq) r(d.b()).r(e.b());
    }

    public int a(int i2) {
        return b(B.b(), i2);
    }

    public mq b(int i2) {
        return (mq) a(B.b(), i2);
    }

    public void b(boolean z2) {
        a(a.b(), z2).q();
    }

    public boolean g() {
        return b(a.b(), false);
    }

    public long c(int i2) {
        return b(h.b(), (long) i2);
    }

    public mq a(long j2) {
        return (mq) a(h.b(), j2);
    }

    public long b(long j2) {
        return b(j.b(), j2);
    }

    public mq c(long j2) {
        return (mq) a(j.b(), j2);
    }

    public long d(long j2) {
        return b(k.b(), j2);
    }

    public mq e(long j2) {
        return (mq) a(k.b(), j2);
    }

    public long f(long j2) {
        return b(l.b(), j2);
    }

    public mq g(long j2) {
        return (mq) a(l.b(), j2);
    }

    public boolean c(boolean z2) {
        return b(i.b(), z2);
    }

    public mq d(boolean z2) {
        return (mq) a(i.b(), z2);
    }

    public int d(int i2) {
        return b(g.b(), i2);
    }

    public mq e(int i2) {
        return (mq) a(g.b(), i2);
    }

    @Nullable
    public Boolean h() {
        if (t(m.b())) {
            return Boolean.valueOf(b(m.b(), true));
        }
        return null;
    }

    public mq e(boolean z2) {
        return (mq) a(m.b(), z2);
    }

    public long h(long j2) {
        return b(t.b(), j2);
    }

    public mq i(long j2) {
        return (mq) a(t.b(), j2);
    }

    public int a(@NonNull a aVar, int i2) {
        th a2 = a(aVar);
        return a2 == null ? i2 : b(a2.b(), i2);
    }

    public mq b(@NonNull a aVar, int i2) {
        th a2 = a(aVar);
        return a2 != null ? (mq) a(a2.b(), i2) : this;
    }

    public long a(@NonNull a aVar, long j2) {
        th b2 = b(aVar);
        return b2 == null ? j2 : b(b2.b(), j2);
    }

    public mq b(@NonNull a aVar, long j2) {
        th b2 = b(aVar);
        return b2 != null ? (mq) a(b2.b(), j2) : this;
    }

    public long j(long j2) {
        return b(C.b(), j2);
    }

    public mq k(long j2) {
        return (mq) a(C.b(), j2);
    }

    public long l(long j2) {
        return b(D.b(), j2);
    }

    public mq m(long j2) {
        return (mq) a(D.b(), j2);
    }

    public long n(long j2) {
        return b(E.b(), j2);
    }

    public mq o(long j2) {
        return (mq) a(E.b(), j2);
    }

    public long p(long j2) {
        return b(F.b(), j2);
    }

    public mq q(long j2) {
        return (mq) a(F.b(), j2);
    }

    private th a(@NonNull a aVar) {
        switch (aVar) {
            case REPORT:
                return v;
            case STARTUP:
                return x;
            case LOCATION:
                return w;
            default:
                return null;
        }
    }

    private th b(@NonNull a aVar) {
        switch (aVar) {
            case REPORT:
                return y;
            case STARTUP:
                return A;
            case LOCATION:
                return z;
            default:
                return null;
        }
    }
}
