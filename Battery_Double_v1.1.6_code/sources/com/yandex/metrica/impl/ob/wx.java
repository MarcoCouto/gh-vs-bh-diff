package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.d;
import com.yandex.metrica.impl.ob.vf.a.C0111a;
import com.yandex.metrica.impl.ob.vf.a.b;

public class wx {
    @NonNull
    private final wy a;
    @NonNull
    private final d b;

    public wx() {
        this(new wy(), xa.a());
    }

    public void a(@NonNull b bVar) {
        this.b.a("provided_request_result", this.a.a(bVar));
    }

    public void a(@NonNull C0111a aVar) {
        this.b.a("provided_request_schedule", this.a.a(aVar));
    }

    public void b(@NonNull C0111a aVar) {
        this.b.a("provided_request_send", this.a.a(aVar));
    }

    @VisibleForTesting
    wx(@NonNull wy wyVar, @NonNull d dVar) {
        this.a = wyVar;
        this.b = dVar;
    }
}
