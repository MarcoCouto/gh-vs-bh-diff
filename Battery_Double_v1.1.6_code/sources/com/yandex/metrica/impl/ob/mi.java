package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.util.SparseArray;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.YandexMetrica;
import java.util.List;
import java.util.Locale;

public final class mi {
    public static final Boolean a = Boolean.valueOf(false);
    public static final int b = YandexMetrica.getLibraryApiLevel();
    public static final SparseArray<mj> c = e.a();
    public static final SparseArray<mj> d = e.b();
    private static final lt e = new lt();
    private static final lw f = new lw();
    private static final ls g = new ls(e, f);

    public interface a {
        public static final List<String> a = dl.a("incremental_id", "timestamp", "data");

        /* renamed from: com.yandex.metrica.impl.ob.mi$a$a reason: collision with other inner class name */
        public interface C0088a {
            public static final String a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", new Object[]{"lbs_dat"});
            public static final String b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", new Object[]{"lbs_dat"});
        }

        public interface b {
            public static final String a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", new Object[]{"l_dat"});
            public static final String b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", new Object[]{"l_dat"});
        }
    }

    public static final class b {
        public static final List<String> a = dl.a("data_key", "value");
    }

    public interface c {
        public static final List<String> a = dl.a(ParametersKeys.KEY, "value", "type");
    }

    public static final class d implements c {
    }

    public static final class e {
        public static ContentValues a() {
            ContentValues contentValues = new ContentValues();
            contentValues.put("API_LEVEL", Integer.valueOf(YandexMetrica.getLibraryApiLevel()));
            return contentValues;
        }
    }

    public static final class f {
        public static final List<String> a = dl.a("id", "number", "global_number", "number_of_type", "name", "value", "type", LocationConst.TIME, TapjoyConstants.TJC_SESSION_ID, "wifi_network_info", "cell_info", "location_info", "error_environment", "user_info", "session_type", "app_environment", "app_environment_revision", "truncated", TapjoyConstants.TJC_CONNECTION_TYPE, "cellular_connection_type", "custom_type", "wifi_access_point", "encrypting_mode", "profile_id", "first_occurrence_status", "battery_charge_type", "collection_mode");
        public static final String b;

        static {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,global_number INTEGER,number_of_type INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT ");
            sb.append(jy.FOREGROUND.a());
            sb.append(",");
            sb.append("app_environment");
            sb.append(" TEXT DEFAULT '");
            sb.append("{}");
            sb.append("',");
            sb.append("app_environment_revision");
            sb.append(" INTEGER DEFAULT ");
            sb.append(0);
            sb.append(",");
            sb.append("truncated");
            sb.append(" INTEGER DEFAULT 0,");
            sb.append(TapjoyConstants.TJC_CONNECTION_TYPE);
            sb.append(" INTEGER DEFAULT ");
            sb.append(2);
            sb.append(",");
            sb.append("cellular_connection_type");
            sb.append(" TEXT,");
            sb.append("custom_type");
            sb.append(" INTEGER DEFAULT 0, ");
            sb.append("wifi_access_point");
            sb.append(" TEXT, ");
            sb.append("encrypting_mode");
            sb.append(" INTEGER DEFAULT ");
            sb.append(acl.NONE.a());
            sb.append(", ");
            sb.append("profile_id");
            sb.append(" TEXT, ");
            sb.append("first_occurrence_status");
            sb.append(" INTEGER DEFAULT ");
            sb.append(ap.UNKNOWN.d);
            sb.append(", ");
            sb.append("battery_charge_type");
            sb.append(" INTEGER DEFAULT ");
            sb.append(p.a.a());
            sb.append(", ");
            sb.append("collection_mode");
            sb.append(" TEXT )");
            b = sb.toString();
        }
    }

    public static final class g {
        public static final List<String> a = dl.a("id", "start_time", "network_info", "report_request_parameters", "server_time_offset", "type", "obtained_before_first_sync");
        public static final String b;
        public static final String c = String.format(Locale.US, "SELECT DISTINCT %s  FROM %s WHERE %s >=0 AND (SELECT count() FROM %5$s WHERE %5$s.%6$s = %2$s.%3$s AND %5$s.%7$s = %2$s.%4$s) > 0 ORDER BY %3$s LIMIT 1", new Object[]{"report_request_parameters", "sessions", "id", "type", "reports", TapjoyConstants.TJC_SESSION_ID, "session_type"});
        public static final String d = String.format(Locale.US, "(select count(%s.%s) from %s where %s.%s = %s.%s) = 0 and cast(%s as integer) < ?", new Object[]{"reports", "id", "reports", "reports", TapjoyConstants.TJC_SESSION_ID, "sessions", "id", "id"});

        static {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE IF NOT EXISTS sessions (id INTEGER,start_time INTEGER,network_info TEXT,report_request_parameters TEXT,server_time_offset INTEGER,type INTEGER DEFAULT ");
            sb.append(jy.FOREGROUND.a());
            sb.append(",");
            sb.append("obtained_before_first_sync");
            sb.append(" INTEGER DEFAULT 0 )");
            b = sb.toString();
        }
    }

    public static final class h implements c {
    }

    public static ls a() {
        return g;
    }
}
