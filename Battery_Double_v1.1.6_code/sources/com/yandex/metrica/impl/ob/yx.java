package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class yx {
    @NonNull
    private aah A;
    @NonNull
    private aah B;
    private boolean C;
    private a a;
    @NonNull
    private xk b = new com.yandex.metrica.impl.ob.xk.a().a();
    private List<String> c;
    private String d = "";
    private List<String> e;
    private String f = "";
    private List<String> g;
    private String h;
    private String i;
    private String j;
    private String k;
    private xs l = null;
    @Nullable
    private xo m = null;
    @Nullable
    private List<qm> n;
    @Nullable
    private rr o;
    private Long p;
    private List<xp> q;
    private String r;
    private List<String> s;
    private yd t;
    @Nullable
    private xr u;
    @NonNull
    private xi v;
    @NonNull
    private xq w;
    private List<sr> x = new ArrayList();
    @Nullable
    private xn y;
    @NonNull
    private aap z;

    public enum a {
        BAD,
        OK
    }

    @NonNull
    public xk a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull xk xkVar) {
        this.b = xkVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, boolean z2) {
        this.x.add(new sr(str, z2));
    }

    /* access modifiers changed from: 0000 */
    public void a(List<String> list) {
        this.c = list;
    }

    public List<String> b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable String str) {
        this.h = str;
    }

    @Nullable
    public String c() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.d = str;
    }

    public String d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public void b(List<String> list) {
        this.e = list;
    }

    public List<String> e() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void c(String str) {
        this.f = str;
    }

    public String f() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void c(List<String> list) {
        this.g = list;
    }

    public List<String> g() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void d(String str) {
        this.i = str;
    }

    public String h() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public void e(String str) {
        this.j = str;
    }

    public String i() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public void f(String str) {
        this.k = str;
    }

    public String j() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public void a(a aVar) {
        this.a = aVar;
    }

    public a k() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public void a(xs xsVar) {
        this.l = xsVar;
    }

    public xs l() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull xo xoVar) {
        this.m = xoVar;
    }

    @Nullable
    public xo m() {
        return this.m;
    }

    @NonNull
    public xr n() {
        return this.u;
    }

    public List<sr> o() {
        return this.x;
    }

    @Nullable
    public List<qm> p() {
        return this.n;
    }

    public rr q() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public void a(Long l2) {
        this.p = l2;
    }

    public Long r() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public void d(List<xp> list) {
        this.q = list;
    }

    public List<xp> s() {
        return this.q;
    }

    public String t() {
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public void g(String str) {
        this.r = str;
    }

    public List<String> u() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public void e(List<String> list) {
        this.s = list;
    }

    public yd v() {
        return this.t;
    }

    /* access modifiers changed from: 0000 */
    public void a(yd ydVar) {
        this.t = ydVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull xr xrVar) {
        this.u = xrVar;
    }

    /* access modifiers changed from: 0000 */
    public void f(@NonNull List<qm> list) {
        this.n = list;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull rr rrVar) {
        this.o = rrVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable xn xnVar) {
        this.y = xnVar;
    }

    @Nullable
    public xn w() {
        return this.y;
    }

    @NonNull
    public xi x() {
        return this.v;
    }

    public void a(@NonNull xi xiVar) {
        this.v = xiVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull xq xqVar) {
        this.w = xqVar;
    }

    @Nullable
    public xq y() {
        return this.w;
    }

    public void a(@NonNull aap aap) {
        this.z = aap;
    }

    public aap z() {
        return this.z;
    }

    public void a(@NonNull aah aah) {
        this.A = aah;
    }

    public aah A() {
        return this.A;
    }

    public void b(@NonNull aah aah) {
        this.B = aah;
    }

    public aah B() {
        return this.B;
    }

    public void a(boolean z2) {
        this.C = z2;
    }

    public boolean C() {
        return this.C;
    }
}
