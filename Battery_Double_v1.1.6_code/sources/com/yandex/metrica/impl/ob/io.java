package com.yandex.metrica.impl.ob;

public class io {
    private final hn a;
    private final ho b;
    private final hm c;
    private final hp d;

    public io(ey eyVar) {
        this.a = new hn(eyVar);
        this.b = new ho(eyVar);
        this.c = new hm(eyVar);
        this.d = new hp(eyVar, as.a().i(), new rp(qp.a(eyVar.d()), as.a().l(), dn.a(eyVar.d()), new mq(lv.a(eyVar.d()).c())));
    }

    public hn a() {
        return this.a;
    }

    public hm b() {
        return this.c;
    }

    public ho c() {
        return this.b;
    }

    public hk d() {
        return this.d;
    }
}
