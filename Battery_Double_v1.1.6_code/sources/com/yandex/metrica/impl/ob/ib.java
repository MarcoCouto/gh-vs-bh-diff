package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.op.a;
import java.util.List;

public class ib extends hu {
    private final ss a;
    @NonNull
    private final mx<sk> b;
    @NonNull
    private final n c;
    @NonNull
    private final j d;
    @NonNull
    private final l e;

    public ib(fe feVar, ss ssVar) {
        this(feVar, ssVar, a.a(sk.class).a(feVar.k()), new n(feVar.k()), new j(), new l(feVar.k()));
    }

    @VisibleForTesting
    ib(fe feVar, ss ssVar, @NonNull mx<sk> mxVar, @NonNull n nVar, @NonNull j jVar, @NonNull l lVar) {
        super(feVar);
        this.a = ssVar;
        this.b = mxVar;
        this.c = nVar;
        this.d = jVar;
        this.e = lVar;
    }

    public boolean a(@NonNull aa aaVar) {
        fe a2 = a();
        a2.c().toString();
        if (a2.u().d() && a2.r()) {
            sk skVar = (sk) this.b.a();
            sk a3 = a(skVar);
            if (a3 != null) {
                a(a3, aaVar, a2.e());
                this.b.a(a3);
            } else if (a2.s()) {
                a(skVar, aaVar, a2.e());
            }
        }
        return false;
    }

    @Nullable
    private sk a(@NonNull sk skVar) {
        List<sr> list = skVar.a;
        m mVar = skVar.b;
        m a2 = this.c.a();
        List<String> list2 = skVar.c;
        List a3 = this.e.a();
        List<sr> a4 = this.a.a(a().k(), list);
        if (a4 == null && dl.a((Object) mVar, (Object) a2) && aav.a(list2, a3)) {
            return null;
        }
        if (a4 != null) {
            list = a4;
        }
        return new sk(list, a2, a3);
    }

    private void a(@NonNull sk skVar, @NonNull aa aaVar, @NonNull fw fwVar) {
        fwVar.c(aa.a(aaVar, skVar.a, skVar.b, this.d, skVar.c));
    }
}
