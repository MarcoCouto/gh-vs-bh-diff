package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class acs implements acu {
    @NonNull
    private final Looper a;
    @NonNull
    private final Handler b;
    @NonNull
    private final acw c;

    public acs(@NonNull String str) {
        this(a(str));
    }

    @NonNull
    public Handler a() {
        return this.b;
    }

    @NonNull
    public Looper b() {
        return this.a;
    }

    public void a(@NonNull Runnable runnable) {
        this.b.post(runnable);
    }

    public <T> Future<T> a(Callable<T> callable) {
        FutureTask futureTask = new FutureTask(callable);
        a((Runnable) futureTask);
        return futureTask;
    }

    public void a(@NonNull Runnable runnable, long j) {
        a(runnable, j, TimeUnit.MILLISECONDS);
    }

    public void a(@NonNull Runnable runnable, long j, @NonNull TimeUnit timeUnit) {
        this.b.postDelayed(runnable, timeUnit.toMillis(j));
    }

    public void b(@NonNull Runnable runnable) {
        this.b.removeCallbacks(runnable);
    }

    public boolean c() {
        return this.c.c();
    }

    private static acw a(@NonNull String str) {
        acw a2 = new acy(str).a();
        a2.start();
        return a2;
    }

    @VisibleForTesting
    acs(@NonNull acw acw) {
        this(acw, acw.getLooper(), new Handler(acw.getLooper()));
    }

    @VisibleForTesting
    public acs(@NonNull acw acw, @NonNull Looper looper, @NonNull Handler handler) {
        this.c = acw;
        this.a = looper;
        this.b = handler;
    }
}
