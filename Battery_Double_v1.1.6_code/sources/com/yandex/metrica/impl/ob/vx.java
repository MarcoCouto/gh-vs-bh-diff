package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public interface vx<I, O> {
    boolean a(@NonNull I i);

    @NonNull
    O b(@NonNull I i);
}
