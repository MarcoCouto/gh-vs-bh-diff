package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;

public class tj implements tx {
    @NonNull
    private final ack a;

    public tj() {
        this(new ack());
    }

    @VisibleForTesting
    tj(@NonNull ack ack) {
        this.a = ack;
    }

    @NonNull
    public byte[] a(@NonNull to toVar) {
        byte[] bArr;
        byte[] bArr2 = new byte[0];
        if (toVar.b != null) {
            try {
                bArr = Base64.decode(toVar.b, 0);
            } catch (Throwable unused) {
            }
            return this.a.a(toVar.s).a(bArr);
        }
        bArr = bArr2;
        return this.a.a(toVar.s).a(bArr);
    }
}
