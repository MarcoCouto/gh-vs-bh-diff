package com.yandex.metrica.impl.ob;

import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.Arrays;

public final class ux extends e {
    public int b;
    public double c;
    public byte[] d;
    public byte[] e;
    public byte[] f;
    public a g;
    public long h;

    public static final class a extends e {
        public byte[] b;
        public byte[] c;

        public a() {
            d();
        }

        public a d() {
            this.b = g.h;
            this.c = g.h;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (!Arrays.equals(this.b, g.h)) {
                bVar.a(1, this.b);
            }
            if (!Arrays.equals(this.c, g.h)) {
                bVar.a(2, this.c);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (!Arrays.equals(this.b, g.h)) {
                c2 += b.b(1, this.b);
            }
            return !Arrays.equals(this.c, g.h) ? c2 + b.b(2, this.c) : c2;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    this.b = aVar.j();
                } else if (a == 18) {
                    this.c = aVar.j();
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public ux() {
        d();
    }

    public ux d() {
        this.b = 1;
        this.c = Utils.DOUBLE_EPSILON;
        this.d = g.h;
        this.e = g.h;
        this.f = g.h;
        this.g = null;
        this.h = 0;
        this.a = -1;
        return this;
    }

    public void a(b bVar) throws IOException {
        if (this.b != 1) {
            bVar.b(1, this.b);
        }
        if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(Utils.DOUBLE_EPSILON)) {
            bVar.a(2, this.c);
        }
        bVar.a(3, this.d);
        if (!Arrays.equals(this.e, g.h)) {
            bVar.a(4, this.e);
        }
        if (!Arrays.equals(this.f, g.h)) {
            bVar.a(5, this.f);
        }
        if (this.g != null) {
            bVar.a(6, (e) this.g);
        }
        if (this.h != 0) {
            bVar.b(7, this.h);
        }
        super.a(bVar);
    }

    /* access modifiers changed from: protected */
    public int c() {
        int c2 = super.c();
        if (this.b != 1) {
            c2 += b.e(1, this.b);
        }
        if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(Utils.DOUBLE_EPSILON)) {
            c2 += b.b(2, this.c);
        }
        int b2 = c2 + b.b(3, this.d);
        if (!Arrays.equals(this.e, g.h)) {
            b2 += b.b(4, this.e);
        }
        if (!Arrays.equals(this.f, g.h)) {
            b2 += b.b(5, this.f);
        }
        if (this.g != null) {
            b2 += b.b(6, (e) this.g);
        }
        return this.h != 0 ? b2 + b.e(7, this.h) : b2;
    }

    /* renamed from: b */
    public ux a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.b = aVar.k();
            } else if (a2 == 17) {
                this.c = aVar.c();
            } else if (a2 == 26) {
                this.d = aVar.j();
            } else if (a2 == 34) {
                this.e = aVar.j();
            } else if (a2 == 42) {
                this.f = aVar.j();
            } else if (a2 == 50) {
                if (this.g == null) {
                    this.g = new a();
                }
                aVar.a((e) this.g);
            } else if (a2 == 56) {
                this.h = aVar.f();
            } else if (!g.a(aVar, a2)) {
                return this;
            }
        }
    }
}
