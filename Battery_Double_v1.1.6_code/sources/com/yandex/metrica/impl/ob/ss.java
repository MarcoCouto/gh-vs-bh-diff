package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.List;

public class ss {
    @Nullable
    public List<sr> a(Context context, @Nullable List<sr> list) {
        List<sr> a = a(context).a();
        if (aav.a(a, list)) {
            return null;
        }
        return a;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public sq a(Context context) {
        if (VERSION.SDK_INT >= 16) {
            return new st(context);
        }
        return new sw(context);
    }
}
