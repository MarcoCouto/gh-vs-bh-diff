package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.vy.c;
import java.util.List;

public class gz extends wb {
    @Nullable
    private List<String> a;
    @NonNull
    private String b;
    private Boolean c;

    public static final class a extends com.yandex.metrica.impl.ob.vy.a<com.yandex.metrica.impl.ob.ew.a, a> {
        @NonNull
        public final String a;
        public final boolean b;

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull String str4, @Nullable Boolean bool) {
            super(str, str2, str3);
            this.a = str4;
            this.b = ((Boolean) abw.b(bool, Boolean.valueOf(true))).booleanValue();
        }

        public a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
            this(aVar.a, aVar.b, aVar.c, aVar.d, aVar.m);
        }

        @NonNull
        /* renamed from: a */
        public a b(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
            a aVar2 = new a((String) abw.a(aVar.a, this.c), (String) abw.a(aVar.b, this.d), (String) abw.a(aVar.c, this.e), (String) abw.b(aVar.d, this.a), (Boolean) abw.a(aVar.m, Boolean.valueOf(this.b)));
            return aVar2;
        }

        /* renamed from: b */
        public boolean a(@NonNull com.yandex.metrica.impl.ob.ew.a aVar) {
            boolean z = false;
            if (aVar.a != null && !aVar.a.equals(this.c)) {
                return false;
            }
            if (aVar.b != null && !aVar.b.equals(this.d)) {
                return false;
            }
            if (aVar.c != null && !aVar.c.equals(this.e)) {
                return false;
            }
            if (aVar.d == null || aVar.d.equals(this.a)) {
                z = true;
            }
            return z;
        }
    }

    public static class b extends a<gz, a> {
        public b(@NonNull Context context, @NonNull String str) {
            super(context, str);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public gz b() {
            return new gz();
        }

        @NonNull
        /* renamed from: a */
        public gz c(@NonNull c<a> cVar) {
            gz gzVar = (gz) super.c(cVar);
            gzVar.a(cVar.a.l);
            gzVar.a(((a) cVar.b).a);
            gzVar.a(Boolean.valueOf(((a) cVar.b).b));
            return gzVar;
        }
    }

    @Nullable
    public List<String> a() {
        return this.a;
    }

    public void a(@Nullable List<String> list) {
        this.a = list;
    }

    @NonNull
    public String b() {
        return this.b;
    }

    public void a(@NonNull String str) {
        this.b = str;
    }

    @Nullable
    public Boolean c() {
        return this.c;
    }

    public void a(Boolean bool) {
        this.c = bool;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DiagnosticRequestConfig{mDiagnosticHosts=");
        sb.append(this.a);
        sb.append(", mApiKey='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", statisticsSending=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
