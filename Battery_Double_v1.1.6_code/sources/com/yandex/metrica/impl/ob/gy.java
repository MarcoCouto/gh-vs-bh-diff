package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.uu.c;
import com.yandex.metrica.impl.ob.uu.c.e;
import com.yandex.metrica.impl.ob.uu.c.e.b;
import com.yandex.metrica.impl.ob.uu.c.g;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class gy {
    public static final Map<Integer, Integer> a = Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
        {
            put(Integer.valueOf(com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_DIAGNOSTIC.a()), Integer.valueOf(22));
            put(Integer.valueOf(com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_DIAGNOSTIC_STATBOX.a()), Integer.valueOf(23));
            put(Integer.valueOf(com.yandex.metrica.impl.ob.al.a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.a()), Integer.valueOf(24));
        }
    });
    @NonNull
    private final aa b;
    @NonNull
    private final gz c;
    @NonNull
    private final hb d;
    @NonNull
    private final adk e;
    @NonNull
    private final adk f;
    @NonNull
    private final abt g;
    @NonNull
    private final fi h;

    public static class a {
        public gy a(@NonNull aa aaVar, @NonNull gz gzVar, @NonNull hb hbVar, @NonNull mo moVar) {
            return new gy(aaVar, gzVar, hbVar, moVar);
        }
    }

    public gy(@NonNull aa aaVar, @NonNull gz gzVar, @NonNull hb hbVar, @NonNull mo moVar) {
        this(aaVar, gzVar, hbVar, new fi(moVar), new adk(1024, "diagnostic event name"), new adk(204800, "diagnostic event value"), new abs());
    }

    public gy(@NonNull aa aaVar, @NonNull gz gzVar, @NonNull hb hbVar, @NonNull fi fiVar, @NonNull adk adk, @NonNull adk adk2, @NonNull abt abt) {
        this.b = aaVar;
        this.c = gzVar;
        this.d = hbVar;
        this.h = fiVar;
        this.f = adk;
        this.e = adk2;
        this.g = abt;
    }

    public byte[] a() {
        c cVar = new c();
        e eVar = new e();
        int i = 0;
        cVar.b = new e[]{eVar};
        com.yandex.metrica.impl.ob.hb.a a2 = this.d.a();
        eVar.b = a2.a;
        eVar.c = new b();
        eVar.c.d = 2;
        eVar.c.b = new g();
        eVar.c.b.b = a2.b;
        eVar.c.b.c = abu.a(a2.b);
        eVar.c.c = this.c.B();
        com.yandex.metrica.impl.ob.uu.c.e.a aVar = new com.yandex.metrica.impl.ob.uu.c.e.a();
        eVar.d = new com.yandex.metrica.impl.ob.uu.c.e.a[]{aVar};
        aVar.b = (long) a2.c;
        aVar.q = (long) this.h.a(this.b.g());
        aVar.c = this.g.b() - a2.b;
        aVar.d = ((Integer) a.get(Integer.valueOf(this.b.g()))).intValue();
        if (!TextUtils.isEmpty(this.b.d())) {
            aVar.e = this.f.a(this.b.d());
        }
        if (!TextUtils.isEmpty(this.b.e())) {
            String e2 = this.b.e();
            String a3 = this.e.a(e2);
            if (!TextUtils.isEmpty(a3)) {
                aVar.f = a3.getBytes();
            }
            int length = e2.getBytes().length;
            if (aVar.f != null) {
                i = aVar.f.length;
            }
            aVar.k = length - i;
        }
        return e.a((e) cVar);
    }
}
