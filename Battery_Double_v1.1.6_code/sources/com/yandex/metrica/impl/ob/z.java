package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

public abstract class z<T> implements y<T> {
    @Nullable
    private y<T> a;

    public abstract void b(@Nullable T t);

    public z(@Nullable y<T> yVar) {
        this.a = yVar;
    }

    public void a(@Nullable T t) {
        b(t);
        if (this.a != null) {
            this.a.a(t);
        }
    }
}
