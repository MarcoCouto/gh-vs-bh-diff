package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class gn extends gc {
    gn(@NonNull Context context, @NonNull ft ftVar) {
        super(context, ftVar);
    }

    /* access modifiers changed from: protected */
    public void b(@NonNull aa aaVar, @NonNull ew ewVar) {
        a(((Boolean) abw.b(ewVar.b.e, Boolean.valueOf(true))).booleanValue());
        b().a(aaVar, ewVar);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(boolean z) {
        c().a(z);
    }
}
