package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.us.c;

public class ll implements nh<ld, c> {
    @NonNull
    private final lk a;
    @NonNull
    private final le b;
    @NonNull
    private final lg c;
    @NonNull
    private lh d;

    public ll() {
        this(new lk(), new le(new lj()), new lg(), new lh());
    }

    @VisibleForTesting
    ll(@NonNull lk lkVar, @NonNull le leVar, @NonNull lg lgVar, @NonNull lh lhVar) {
        this.b = leVar;
        this.a = lkVar;
        this.c = lgVar;
        this.d = lhVar;
    }

    @NonNull
    /* renamed from: a */
    public c b(@NonNull ld ldVar) {
        c cVar = new c();
        if (ldVar.b != null) {
            cVar.b = this.a.b(ldVar.b);
        }
        if (ldVar.c != null) {
            cVar.c = this.b.b(ldVar.c);
        }
        if (ldVar.d != null) {
            cVar.f = this.d.b(ldVar.d);
        }
        if (ldVar.e != null) {
            cVar.d = ldVar.e;
        }
        cVar.e = this.c.a(ldVar.f).intValue();
        return cVar;
    }

    @NonNull
    public ld a(@NonNull c cVar) {
        throw new UnsupportedOperationException();
    }
}
