package com.yandex.metrica;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.abl;
import com.yandex.metrica.impl.ob.as;
import com.yandex.metrica.impl.ob.bo;
import com.yandex.metrica.impl.ob.bp;
import com.yandex.metrica.impl.ob.bq;
import com.yandex.metrica.impl.ob.cg;
import com.yandex.metrica.impl.ob.da;
import com.yandex.metrica.impl.ob.eh;
import com.yandex.metrica.impl.ob.ej;
import com.yandex.metrica.impl.ob.ep;

public class MetricaService extends Service {
    private c a = new c() {
        public void a(int i) {
            MetricaService.this.stopSelfResult(i);
        }
    };
    /* access modifiers changed from: private */
    public bo b;
    private final com.yandex.metrica.IMetricaService.a c = new com.yandex.metrica.IMetricaService.a() {
        @Deprecated
        public void a(String str, int i, String str2, Bundle bundle) throws RemoteException {
            MetricaService.this.b.a(str, i, str2, bundle);
        }

        public void a(Bundle bundle) throws RemoteException {
            MetricaService.this.b.a(bundle);
        }

        public void b(@NonNull Bundle bundle) throws RemoteException {
            MetricaService.this.b.b(bundle);
        }

        public void c(@NonNull Bundle bundle) throws RemoteException {
            MetricaService.this.b.c(bundle);
        }
    };

    static class a extends Binder {
        a() {
        }
    }

    static class b extends Binder {
        b() {
        }
    }

    public interface c {
        void a(int i);
    }

    public void onCreate() {
        super.onCreate();
        as.a(getApplicationContext());
        a(getResources().getConfiguration());
        abl.a(getApplicationContext());
        this.b = new bp(new bq(getApplicationContext(), this.a));
        this.b.a();
        as.a().a(new da(this.b));
    }

    public void onStart(Intent intent, int i) {
        this.b.a(intent, i);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        this.b.a(intent, i, i2);
        return 2;
    }

    public IBinder onBind(Intent intent) {
        IBinder iBinder;
        String action = intent.getAction();
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            iBinder = new b();
        } else if ("com.yandex.metrica.ACTION_C_BG_L".equals(action)) {
            iBinder = new a();
        } else {
            iBinder = this.c;
        }
        this.b.a(intent);
        return iBinder;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        this.b.b(intent);
    }

    public void onDestroy() {
        this.b.b();
        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        this.b.c(intent);
        String action = intent.getAction();
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            return false;
        }
        if (!"com.yandex.metrica.ACTION_C_BG_L".equals(action) && a(intent)) {
            return false;
        }
        return true;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a(configuration);
    }

    private void a(Configuration configuration) {
        eh.a().b((ej) new ep(cg.a(configuration.locale)));
    }

    private boolean a(Intent intent) {
        return intent == null || intent.getData() == null;
    }
}
