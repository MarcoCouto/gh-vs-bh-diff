package com.yandex.metrica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.abd;
import com.yandex.metrica.impl.ob.abl;
import com.yandex.metrica.impl.ob.adr;
import com.yandex.metrica.impl.ob.ads;
import com.yandex.metrica.impl.ob.adw;
import com.yandex.metrica.impl.ob.dr;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class MetricaEventHandler extends BroadcastReceiver {
    public static final Set<BroadcastReceiver> a = new HashSet();
    private static final adw<BroadcastReceiver[]> b = new ads(new adr("Broadcast receivers"));

    public void onReceive(Context context, Intent intent) {
        if (a(intent)) {
            a(context, intent);
        }
        abl a2 = abd.a();
        for (BroadcastReceiver broadcastReceiver : a) {
            String format = String.format("Sending referrer to %s", new Object[]{broadcastReceiver.getClass().getName()});
            if (a2.c()) {
                a2.a(format);
            }
            broadcastReceiver.onReceive(context, intent);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(Intent intent) {
        return "com.android.vending.INSTALL_REFERRER".equals(intent.getAction());
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra(TapjoyConstants.TJC_REFERRER);
        if (!TextUtils.isEmpty(stringExtra)) {
            dr.b(context).a(stringExtra);
        }
    }

    static void a(BroadcastReceiver... broadcastReceiverArr) {
        b.a(broadcastReceiverArr);
        Collections.addAll(a, broadcastReceiverArr);
    }
}
