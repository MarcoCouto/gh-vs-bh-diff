package com.yandex.metrica;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.ReporterConfig.Builder;
import com.yandex.metrica.impl.ob.dl;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class f extends ReporterConfig {
    @Nullable
    public final Integer a;
    @Nullable
    public final Integer b;
    public final Map<String, String> c;

    public static class a {
        Builder a;
        Integer b;
        Integer c;
        LinkedHashMap<String, String> d = new LinkedHashMap<>();

        public a(String str) {
            this.a = ReporterConfig.newConfigBuilder(str);
        }

        @NonNull
        public a a(int i) {
            this.a.withSessionTimeout(i);
            return this;
        }

        @NonNull
        public a a() {
            this.a.withLogs();
            return this;
        }

        @NonNull
        public a b(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public a c(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public a a(String str, String str2) {
            this.d.put(str, str2);
            return this;
        }

        @NonNull
        public a a(boolean z) {
            this.a.withStatisticsSending(z);
            return this;
        }

        @NonNull
        public a d(int i) {
            this.a.withMaxReportsInDatabaseCount(i);
            return this;
        }

        @NonNull
        public f b() {
            return new f(this);
        }
    }

    f(@NonNull a aVar) {
        super(aVar.a);
        this.b = aVar.b;
        this.a = aVar.c;
        this.c = aVar.d == null ? null : Collections.unmodifiableMap(aVar.d);
    }

    private f(ReporterConfig reporterConfig) {
        super(reporterConfig);
        if (reporterConfig instanceof f) {
            f fVar = (f) reporterConfig;
            this.a = fVar.a;
            this.b = fVar.b;
            this.c = fVar.c;
            return;
        }
        this.a = null;
        this.b = null;
        this.c = null;
    }

    public static f a(@NonNull ReporterConfig reporterConfig) {
        return new f(reporterConfig);
    }

    public static a a(@NonNull f fVar) {
        a a2 = a(fVar.apiKey);
        if (dl.a((Object) fVar.sessionTimeout)) {
            a2.a(fVar.sessionTimeout.intValue());
        }
        if (dl.a((Object) fVar.logs) && fVar.logs.booleanValue()) {
            a2.a();
        }
        if (dl.a((Object) fVar.statisticsSending)) {
            a2.a(fVar.statisticsSending.booleanValue());
        }
        if (dl.a((Object) fVar.maxReportsInDatabaseCount)) {
            a2.d(fVar.maxReportsInDatabaseCount.intValue());
        }
        if (dl.a((Object) fVar.a)) {
            a2.c(fVar.a.intValue());
        }
        if (dl.a((Object) fVar.b)) {
            a2.b(fVar.b.intValue());
        }
        if (dl.a((Object) fVar.c)) {
            for (Entry entry : fVar.c.entrySet()) {
                a2.a((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return a2;
    }

    public static a a(@NonNull String str) {
        return new a(str);
    }
}
