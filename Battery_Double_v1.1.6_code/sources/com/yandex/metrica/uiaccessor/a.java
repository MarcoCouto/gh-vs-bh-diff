package com.yandex.metrica.uiaccessor;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks;

public class a implements b {
    @NonNull
    private final C0115a a;
    @Nullable
    private FragmentLifecycleCallbacks b;

    /* renamed from: com.yandex.metrica.uiaccessor.a$a reason: collision with other inner class name */
    public interface C0115a {
    }

    public a(@NonNull C0115a aVar) throws Throwable {
        this.a = aVar;
    }

    public void a(@NonNull final Activity activity) throws Throwable {
        if (activity instanceof FragmentActivity) {
            if (this.b == null) {
                this.b = new FragmentLifecycleCallbacks() {
                };
            }
            FragmentManager supportFragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
            supportFragmentManager.unregisterFragmentLifecycleCallbacks(this.b);
            supportFragmentManager.registerFragmentLifecycleCallbacks(this.b, true);
        }
    }
}
