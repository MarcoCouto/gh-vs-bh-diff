package com.yandex.metrica;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.app.job.JobWorkItem;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import com.yandex.metrica.impl.ob.as;
import com.yandex.metrica.impl.ob.du;
import com.yandex.metrica.impl.ob.dw;
import com.yandex.metrica.impl.ob.kc;
import com.yandex.metrica.impl.ob.ki;
import com.yandex.metrica.impl.ob.km;
import com.yandex.metrica.impl.ob.kn;
import com.yandex.metrica.impl.ob.ko;
import com.yandex.metrica.impl.ob.kp;
import com.yandex.metrica.impl.ob.kq;
import com.yandex.metrica.impl.ob.kr;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@TargetApi(26)
public class ConfigurationJobService extends JobService {
    @NonNull
    SparseArray<kp> a = new SparseArray<>();
    @NonNull
    Map<String, kp> b = new HashMap();
    private ki c;
    @Nullable
    private String d;

    public boolean complexJob(int i) {
        return i == 1512302347;
    }

    public void onCreate() {
        super.onCreate();
        as.a(getApplicationContext());
        Context applicationContext = getApplicationContext();
        this.d = String.format(Locale.US, "[ConfigurationJobService:%s]", new Object[]{applicationContext.getPackageName()});
        this.c = new ki();
        km kmVar = new km(getApplicationContext(), this.c.a(), new kc(applicationContext));
        du duVar = new du(applicationContext, new dw(applicationContext));
        this.a.append(1512302345, new kq(getApplicationContext(), kmVar));
        this.a.append(1512302346, new kr(getApplicationContext(), kmVar, duVar));
        this.b.put("com.yandex.metrica.configuration.service.PLC", new ko(applicationContext, this.c.a()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        jobFinished(r3, false);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0018 */
    public boolean onStartJob(@Nullable JobParameters jobParameters) {
        if (jobParameters == null) {
            return false;
        }
        if (!complexJob(jobParameters.getJobId())) {
            return b(jobParameters);
        }
        a(jobParameters);
        return true;
    }

    /* access modifiers changed from: private */
    public void a(@NonNull final JobParameters jobParameters) {
        this.c.a().a((Runnable) new Runnable() {
            public void run() {
                ConfigurationJobService.this.c(jobParameters);
            }
        });
    }

    private boolean b(@NonNull final JobParameters jobParameters) {
        kp kpVar = (kp) this.a.get(jobParameters.getJobId());
        if (kpVar == null) {
            return false;
        }
        this.c.a(kpVar, jobParameters.getTransientExtras(), new kn() {
            public void a() {
                try {
                    ConfigurationJobService.this.jobFinished(jobParameters, false);
                } catch (Throwable unused) {
                }
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    public void c(@NonNull final JobParameters jobParameters) {
        while (true) {
            try {
                final JobWorkItem dequeueWork = jobParameters.dequeueWork();
                if (dequeueWork != null) {
                    Intent intent = dequeueWork.getIntent();
                    if (intent != null) {
                        kp kpVar = (kp) this.b.get(intent.getAction());
                        if (kpVar != null) {
                            this.c.a(kpVar, intent.getExtras(), new kn() {
                                public void a() {
                                    try {
                                        jobParameters.completeWork(dequeueWork);
                                        ConfigurationJobService.this.a(jobParameters);
                                    } catch (Throwable unused) {
                                    }
                                }
                            });
                        } else {
                            jobParameters.completeWork(dequeueWork);
                        }
                    } else {
                        jobParameters.completeWork(dequeueWork);
                    }
                } else {
                    return;
                }
            } catch (Throwable unused) {
                jobFinished(jobParameters, true);
                return;
            }
        }
    }

    public boolean onStopJob(@Nullable JobParameters jobParameters) {
        return jobParameters != null && complexJob(jobParameters.getJobId());
    }
}
