package com.yandex.metrica.gpllibrary;

import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

class GplLocationCallback extends LocationCallback {
    private static final String TAG = "[GplLocationCallback]";
    @NonNull
    private final LocationListener mLocationListener;

    GplLocationCallback(@NonNull LocationListener locationListener) {
        this.mLocationListener = locationListener;
    }

    public void onLocationResult(LocationResult locationResult) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onLocationResult: ");
        sb.append(locationResult);
        Log.d(str, sb.toString());
        this.mLocationListener.onLocationChanged(locationResult.getLastLocation());
    }
}
