package com.yandex.metrica.gpllibrary;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.LocationListener;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

public class a implements b {
    @NonNull
    private final FusedLocationProviderClient a;
    @NonNull
    private final LocationListener b;
    @NonNull
    private final LocationCallback c;
    @NonNull
    private final Looper d;
    private final long e;

    /* renamed from: com.yandex.metrica.gpllibrary.a$a reason: collision with other inner class name */
    static class C0083a {
        @NonNull
        private final Context a;

        C0083a(@NonNull Context context) {
            this.a = context;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public FusedLocationProviderClient a() throws Throwable {
            return new FusedLocationProviderClient(this.a);
        }
    }

    public enum b {
        PRIORITY_NO_POWER,
        PRIORITY_LOW_POWER,
        PRIORITY_BALANCED_POWER_ACCURACY,
        PRIORITY_HIGH_ACCURACY
    }

    public a(@NonNull Context context, @NonNull LocationListener locationListener, @NonNull Looper looper, long j) throws Throwable {
        this(new C0083a(context), locationListener, looper, j);
    }

    @VisibleForTesting
    a(@NonNull C0083a aVar, @NonNull LocationListener locationListener, @NonNull Looper looper, long j) throws Throwable {
        this.a = aVar.a();
        this.b = locationListener;
        this.d = looper;
        this.e = j;
        this.c = new GplLocationCallback(this.b);
    }

    @SuppressLint({"MissingPermission"})
    public void a() throws Throwable {
        Log.d("[GplLibraryWrapper]", "updateLastKnownLocation");
        this.a.getLastLocation().addOnSuccessListener(new GplOnSuccessListener(this.b));
    }

    @SuppressLint({"MissingPermission"})
    public void a(@NonNull b bVar) throws Throwable {
        Log.d("[GplLibraryWrapper]", "startLocationUpdates");
        this.a.requestLocationUpdates(LocationRequest.create().setInterval(this.e).setPriority(b(bVar)), this.c, this.d);
    }

    public void b() throws Throwable {
        Log.d("[GplLibraryWrapper]", "stopLocationUpdates");
        this.a.removeLocationUpdates(this.c);
    }

    private int b(@NonNull b bVar) {
        switch (bVar) {
            case PRIORITY_LOW_POWER:
                return 104;
            case PRIORITY_BALANCED_POWER_ACCURACY:
                return 102;
            case PRIORITY_HIGH_ACCURACY:
                return 100;
            default:
                return 105;
        }
    }
}
