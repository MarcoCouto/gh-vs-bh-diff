package com.yandex.metrica.gpllibrary;

import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.tasks.OnSuccessListener;

class GplOnSuccessListener implements OnSuccessListener<Location> {
    private static final String TAG = "[GplOnSuccessListener]";
    @NonNull
    private final LocationListener mLocationListener;

    GplOnSuccessListener(@NonNull LocationListener locationListener) {
        this.mLocationListener = locationListener;
    }

    public void onSuccess(Location location) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onSuccess: ");
        sb.append(location);
        Log.d(str, sb.toString());
        this.mLocationListener.onLocationChanged(location);
    }
}
