package com.yandex.metrica;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.act;
import com.yandex.metrica.impl.ob.dr;
import java.util.HashSet;
import java.util.Set;

public class a {
    @NonNull
    private final act a;
    private final long b;
    private final Set<b> c;

    /* renamed from: com.yandex.metrica.a$a reason: collision with other inner class name */
    public interface C0082a {
        void a();

        void b();
    }

    private class b {
        @NonNull
        final act a;
        @NonNull
        final C0082a b;
        private final long d;
        private boolean e = true;
        private final Runnable f = new Runnable() {
            public void run() {
                b.this.b.b();
            }
        };

        b(C0082a aVar, @NonNull act act, @NonNull long j) {
            this.b = aVar;
            this.a = act;
            this.d = j;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            if (this.e) {
                this.e = false;
                this.a.b(this.f);
                this.b.a();
            }
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            if (!this.e) {
                this.e = true;
                this.a.a(this.f, this.d);
            }
        }
    }

    public a(long j) {
        this(j, dr.k().b());
    }

    a(long j, @NonNull act act) {
        this.c = new HashSet();
        this.a = act;
        this.b = j;
    }

    public synchronized void a() {
        for (b a2 : this.c) {
            a2.a();
        }
    }

    public synchronized void b() {
        for (b b2 : this.c) {
            b2.b();
        }
    }

    public synchronized void a(@NonNull C0082a aVar, long j) {
        Set<b> set = this.c;
        b bVar = new b(aVar, this.a, j);
        set.add(bVar);
    }
}
