package com.yandex.metrica;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PreloadInfo {
    private String a;
    private Map<String, String> b;

    public static class Builder {
        /* access modifiers changed from: private */
        public String a;
        /* access modifiers changed from: private */
        public Map<String, String> b;

        private Builder(String str) {
            this.a = str;
            this.b = new HashMap();
        }

        public Builder setAdditionalParams(String str, String str2) {
            if (!(str == null || str2 == null)) {
                this.b.put(str, str2);
            }
            return this;
        }

        public PreloadInfo build() {
            return new PreloadInfo(this);
        }
    }

    private PreloadInfo(Builder builder) {
        this.a = builder.a;
        this.b = Collections.unmodifiableMap(builder.b);
    }

    public static Builder newBuilder(String str) {
        return new Builder(str);
    }

    public String getTrackingId() {
        return this.a;
    }

    public Map<String, String> getAdditionalParams() {
        return this.b;
    }
}
