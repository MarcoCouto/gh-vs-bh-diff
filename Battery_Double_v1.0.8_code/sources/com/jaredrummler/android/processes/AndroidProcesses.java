package com.jaredrummler.android.processes;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Process;
import android.util.Log;
import com.jaredrummler.android.processes.models.AndroidAppProcess;
import com.jaredrummler.android.processes.models.AndroidProcess;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AndroidProcesses {
    private static final int AID_READPROC = 3009;
    public static final String TAG = "AndroidProcesses";
    private static boolean loggingEnabled;

    public static final class ProcessComparator implements Comparator<AndroidProcess> {
        public int compare(AndroidProcess androidProcess, AndroidProcess androidProcess2) {
            return androidProcess.name.compareToIgnoreCase(androidProcess2.name);
        }
    }

    public static void setLoggingEnabled(boolean z) {
        loggingEnabled = z;
    }

    public static boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    public static void log(String str, Object... objArr) {
        if (loggingEnabled) {
            String str2 = TAG;
            if (objArr.length != 0) {
                str = String.format(str, objArr);
            }
            Log.d(str2, str);
        }
    }

    public static void log(Throwable th, String str, Object... objArr) {
        if (loggingEnabled) {
            String str2 = TAG;
            if (objArr.length != 0) {
                str = String.format(str, objArr);
            }
            Log.d(str2, str, th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0045, code lost:
        if (r3 == null) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005b A[SYNTHETIC, Splitter:B:33:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x006c A[SYNTHETIC, Splitter:B:41:0x006c] */
    public static boolean isProcessInfoHidden() {
        boolean z = false;
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader("/proc/mounts"));
            while (true) {
                try {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        break;
                    }
                    String[] split = readLine.split("\\s+");
                    if (split.length == 6 && split[1].equals("/proc")) {
                        if (split[3].contains("hidepid=1") || split[3].contains("hidepid=2")) {
                            z = true;
                        }
                        if (bufferedReader2 != null) {
                            try {
                                bufferedReader2.close();
                            } catch (IOException unused) {
                            }
                        }
                        return z;
                    }
                } catch (IOException unused2) {
                    bufferedReader = bufferedReader2;
                    try {
                        Log.d(TAG, "Error reading /proc/mounts. Checking if UID 'readproc' exists.");
                        if (bufferedReader != null) {
                            bufferedReader.close();
                        }
                        if (Process.getUidForName("readproc") == AID_READPROC) {
                        }
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        bufferedReader2 = bufferedReader;
                        if (bufferedReader2 != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (bufferedReader2 != null) {
                        try {
                            bufferedReader2.close();
                        } catch (IOException unused3) {
                        }
                    }
                    throw th;
                }
            }
        } catch (IOException unused4) {
            Log.d(TAG, "Error reading /proc/mounts. Checking if UID 'readproc' exists.");
            if (bufferedReader != null) {
            }
            if (Process.getUidForName("readproc") == AID_READPROC) {
            }
            return z;
        }
        if (Process.getUidForName("readproc") == AID_READPROC) {
            z = true;
        }
        return z;
    }

    public static List<AndroidProcess> getRunningProcesses() {
        File[] listFiles;
        ArrayList arrayList = new ArrayList();
        for (File file : new File("/proc").listFiles()) {
            if (file.isDirectory()) {
                try {
                    int parseInt = Integer.parseInt(file.getName());
                    try {
                        arrayList.add(new AndroidProcess(parseInt));
                    } catch (IOException e) {
                        log(e, "Error reading from /proc/%d.", Integer.valueOf(parseInt));
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        return arrayList;
    }

    public static List<AndroidAppProcess> getRunningAppProcesses() {
        File[] listFiles;
        ArrayList arrayList = new ArrayList();
        for (File file : new File("/proc").listFiles()) {
            if (file.isDirectory()) {
                try {
                    int parseInt = Integer.parseInt(file.getName());
                    try {
                        arrayList.add(new AndroidAppProcess(parseInt));
                    } catch (IOException e) {
                        log(e, "Error reading from /proc/%d.", Integer.valueOf(parseInt));
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        return arrayList;
    }

    public static List<AndroidAppProcess> getRunningForegroundApps(Context context) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = new File("/proc").listFiles();
        PackageManager packageManager = context.getPackageManager();
        for (File file : listFiles) {
            if (file.isDirectory()) {
                try {
                    int parseInt = Integer.parseInt(file.getName());
                    try {
                        AndroidAppProcess androidAppProcess = new AndroidAppProcess(parseInt);
                        if (androidAppProcess.foreground && ((androidAppProcess.uid < 1000 || androidAppProcess.uid > 9999) && !androidAppProcess.name.contains(":") && packageManager.getLaunchIntentForPackage(androidAppProcess.getPackageName()) != null)) {
                            arrayList.add(androidAppProcess);
                        }
                    } catch (IOException e) {
                        log(e, "Error reading from /proc/%d.", Integer.valueOf(parseInt));
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        return arrayList;
    }

    public static boolean isMyProcessInTheForeground() {
        try {
            return new AndroidAppProcess(Process.myPid()).foreground;
        } catch (Exception e) {
            log(e, "Error finding our own process", new Object[0]);
            return false;
        }
    }

    public static List<RunningAppProcessInfo> getRunningAppProcessInfo(Context context) {
        if (VERSION.SDK_INT < 22) {
            return ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        }
        List<AndroidAppProcess> runningAppProcesses = getRunningAppProcesses();
        ArrayList arrayList = new ArrayList();
        for (AndroidAppProcess androidAppProcess : runningAppProcesses) {
            RunningAppProcessInfo runningAppProcessInfo = new RunningAppProcessInfo(androidAppProcess.name, androidAppProcess.pid, null);
            runningAppProcessInfo.uid = androidAppProcess.uid;
            arrayList.add(runningAppProcessInfo);
        }
        return arrayList;
    }

    AndroidProcesses() {
        throw new AssertionError("no instances");
    }
}
