package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.buffer.BarBuffer;
import com.github.mikephil.charting.buffer.HorizontalBarBuffer;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.List;

public class HorizontalBarChartRenderer extends BarChartRenderer {
    private RectF mBarShadowRectBuffer = new RectF();

    public HorizontalBarChartRenderer(BarDataProvider barDataProvider, ChartAnimator chartAnimator, ViewPortHandler viewPortHandler) {
        super(barDataProvider, chartAnimator, viewPortHandler);
        this.mValuePaint.setTextAlign(Align.LEFT);
    }

    public void initBuffers() {
        BarData barData = this.mChart.getBarData();
        this.mBarBuffers = new HorizontalBarBuffer[barData.getDataSetCount()];
        for (int i = 0; i < this.mBarBuffers.length; i++) {
            IBarDataSet iBarDataSet = (IBarDataSet) barData.getDataSetByIndex(i);
            this.mBarBuffers[i] = new HorizontalBarBuffer(iBarDataSet.getEntryCount() * 4 * (iBarDataSet.isStacked() ? iBarDataSet.getStackSize() : 1), barData.getDataSetCount(), iBarDataSet.isStacked());
        }
    }

    /* access modifiers changed from: protected */
    public void drawDataSet(Canvas canvas, IBarDataSet iBarDataSet, int i) {
        IBarDataSet iBarDataSet2 = iBarDataSet;
        int i2 = i;
        Transformer transformer = this.mChart.getTransformer(iBarDataSet.getAxisDependency());
        this.mBarBorderPaint.setColor(iBarDataSet.getBarBorderColor());
        this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(iBarDataSet.getBarBorderWidth()));
        int i3 = 0;
        boolean z = true;
        boolean z2 = iBarDataSet.getBarBorderWidth() > 0.0f;
        float phaseX = this.mAnimator.getPhaseX();
        float phaseY = this.mAnimator.getPhaseY();
        if (this.mChart.isDrawBarShadowEnabled()) {
            this.mShadowPaint.setColor(iBarDataSet.getBarShadowColor());
            float barWidth = this.mChart.getBarData().getBarWidth() / 2.0f;
            int min = Math.min((int) Math.ceil((double) (((float) iBarDataSet.getEntryCount()) * phaseX)), iBarDataSet.getEntryCount());
            for (int i4 = 0; i4 < min; i4++) {
                float x = ((BarEntry) iBarDataSet2.getEntryForIndex(i4)).getX();
                this.mBarShadowRectBuffer.top = x - barWidth;
                this.mBarShadowRectBuffer.bottom = x + barWidth;
                transformer.rectValueToPixel(this.mBarShadowRectBuffer);
                if (!this.mViewPortHandler.isInBoundsTop(this.mBarShadowRectBuffer.bottom)) {
                    Canvas canvas2 = canvas;
                } else if (!this.mViewPortHandler.isInBoundsBottom(this.mBarShadowRectBuffer.top)) {
                    break;
                } else {
                    this.mBarShadowRectBuffer.left = this.mViewPortHandler.contentLeft();
                    this.mBarShadowRectBuffer.right = this.mViewPortHandler.contentRight();
                    canvas.drawRect(this.mBarShadowRectBuffer, this.mShadowPaint);
                }
            }
        }
        Canvas canvas3 = canvas;
        BarBuffer barBuffer = this.mBarBuffers[i2];
        barBuffer.setPhases(phaseX, phaseY);
        barBuffer.setDataSet(i2);
        barBuffer.setInverted(this.mChart.isInverted(iBarDataSet.getAxisDependency()));
        barBuffer.setBarWidth(this.mChart.getBarData().getBarWidth());
        barBuffer.feed(iBarDataSet2);
        transformer.pointValuesToPixel(barBuffer.buffer);
        if (iBarDataSet.getColors().size() != 1) {
            z = false;
        }
        if (z) {
            this.mRenderPaint.setColor(iBarDataSet.getColor());
        }
        while (i3 < barBuffer.size()) {
            int i5 = i3 + 3;
            if (this.mViewPortHandler.isInBoundsTop(barBuffer.buffer[i5])) {
                int i6 = i3 + 1;
                if (this.mViewPortHandler.isInBoundsBottom(barBuffer.buffer[i6])) {
                    if (!z) {
                        this.mRenderPaint.setColor(iBarDataSet2.getColor(i3 / 4));
                    }
                    int i7 = i3 + 2;
                    canvas3.drawRect(barBuffer.buffer[i3], barBuffer.buffer[i6], barBuffer.buffer[i7], barBuffer.buffer[i5], this.mRenderPaint);
                    if (z2) {
                        canvas.drawRect(barBuffer.buffer[i3], barBuffer.buffer[i6], barBuffer.buffer[i7], barBuffer.buffer[i5], this.mBarBorderPaint);
                    }
                }
                i3 += 4;
                canvas3 = canvas;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:92:0x028a  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x028d  */
    public void drawValues(Canvas canvas) {
        List list;
        float f;
        IBarDataSet iBarDataSet;
        int i;
        float[] fArr;
        BarEntry barEntry;
        float f2;
        float[] fArr2;
        BarEntry barEntry2;
        float f3;
        float f4;
        float f5;
        String str;
        float f6;
        float[] fArr3;
        float f7;
        List list2;
        IValueFormatter iValueFormatter;
        int i2;
        BarBuffer barBuffer;
        IValueFormatter iValueFormatter2;
        float f8;
        if (isDrawingValuesAllowed(this.mChart)) {
            List dataSets = this.mChart.getBarData().getDataSets();
            float convertDpToPixel = Utils.convertDpToPixel(5.0f);
            boolean isDrawValueAboveBarEnabled = this.mChart.isDrawValueAboveBarEnabled();
            int i3 = 0;
            while (i3 < this.mChart.getBarData().getDataSetCount()) {
                IBarDataSet iBarDataSet2 = (IBarDataSet) dataSets.get(i3);
                if (shouldDrawValues(iBarDataSet2)) {
                    boolean isInverted = this.mChart.isInverted(iBarDataSet2.getAxisDependency());
                    applyValueTextStyle(iBarDataSet2);
                    float f9 = 2.0f;
                    float calcTextHeight = ((float) Utils.calcTextHeight(this.mValuePaint, "10")) / 2.0f;
                    IValueFormatter valueFormatter = iBarDataSet2.getValueFormatter();
                    BarBuffer barBuffer2 = this.mBarBuffers[i3];
                    float phaseY = this.mAnimator.getPhaseY();
                    if (!iBarDataSet2.isStacked()) {
                        int i4 = 0;
                        while (((float) i4) < ((float) barBuffer2.buffer.length) * this.mAnimator.getPhaseX()) {
                            int i5 = i4 + 1;
                            float f10 = (barBuffer2.buffer[i5] + barBuffer2.buffer[i4 + 3]) / f9;
                            if (!this.mViewPortHandler.isInBoundsTop(barBuffer2.buffer[i5])) {
                                break;
                            }
                            if (this.mViewPortHandler.isInBoundsX(barBuffer2.buffer[i4]) && this.mViewPortHandler.isInBoundsBottom(barBuffer2.buffer[i5])) {
                                BarEntry barEntry3 = (BarEntry) iBarDataSet2.getEntryForIndex(i4 / 4);
                                float y = barEntry3.getY();
                                String formattedValue = valueFormatter.getFormattedValue(y, barEntry3, i3, this.mViewPortHandler);
                                float calcTextWidth = (float) Utils.calcTextWidth(this.mValuePaint, formattedValue);
                                float f11 = isDrawValueAboveBarEnabled ? convertDpToPixel : -(calcTextWidth + convertDpToPixel);
                                if (isDrawValueAboveBarEnabled) {
                                    iValueFormatter2 = valueFormatter;
                                    f8 = -(calcTextWidth + convertDpToPixel);
                                } else {
                                    iValueFormatter2 = valueFormatter;
                                    f8 = convertDpToPixel;
                                }
                                if (isInverted) {
                                    f11 = (-f11) - calcTextWidth;
                                    f8 = (-f8) - calcTextWidth;
                                }
                                float f12 = barBuffer2.buffer[i4 + 2];
                                if (y < 0.0f) {
                                    f11 = f8;
                                }
                                float f13 = f12 + f11;
                                float f14 = f10 + calcTextHeight;
                                int valueTextColor = iBarDataSet2.getValueTextColor(i4 / 2);
                                String str2 = formattedValue;
                                i2 = i4;
                                float f15 = f13;
                                BarBuffer barBuffer3 = barBuffer2;
                                float f16 = f14;
                                list2 = dataSets;
                                iValueFormatter = iValueFormatter2;
                                barBuffer = barBuffer3;
                                drawValue(canvas, str2, f15, f16, valueTextColor);
                            } else {
                                i2 = i4;
                                iValueFormatter = valueFormatter;
                                list2 = dataSets;
                                barBuffer = barBuffer2;
                            }
                            i4 = i2 + 4;
                            barBuffer2 = barBuffer;
                            valueFormatter = iValueFormatter;
                            dataSets = list2;
                            f9 = 2.0f;
                        }
                    } else {
                        IValueFormatter iValueFormatter3 = valueFormatter;
                        list = dataSets;
                        BarBuffer barBuffer4 = barBuffer2;
                        Transformer transformer = this.mChart.getTransformer(iBarDataSet2.getAxisDependency());
                        int i6 = 0;
                        int i7 = 0;
                        while (((float) i6) < ((float) iBarDataSet2.getEntryCount()) * this.mAnimator.getPhaseX()) {
                            BarEntry barEntry4 = (BarEntry) iBarDataSet2.getEntryForIndex(i6);
                            int valueTextColor2 = iBarDataSet2.getValueTextColor(i6);
                            float[] yVals = barEntry4.getYVals();
                            if (yVals == null) {
                                int i8 = i7 + 1;
                                if (!this.mViewPortHandler.isInBoundsTop(barBuffer4.buffer[i8])) {
                                    break;
                                } else if (this.mViewPortHandler.isInBoundsX(barBuffer4.buffer[i7]) && this.mViewPortHandler.isInBoundsBottom(barBuffer4.buffer[i8])) {
                                    String formattedValue2 = iValueFormatter3.getFormattedValue(barEntry4.getY(), barEntry4, i3, this.mViewPortHandler);
                                    float calcTextWidth2 = (float) Utils.calcTextWidth(this.mValuePaint, formattedValue2);
                                    if (isDrawValueAboveBarEnabled) {
                                        str = formattedValue2;
                                        f6 = convertDpToPixel;
                                    } else {
                                        str = formattedValue2;
                                        f6 = -(calcTextWidth2 + convertDpToPixel);
                                    }
                                    if (isDrawValueAboveBarEnabled) {
                                        fArr3 = yVals;
                                        f7 = -(calcTextWidth2 + convertDpToPixel);
                                    } else {
                                        fArr3 = yVals;
                                        f7 = convertDpToPixel;
                                    }
                                    if (isInverted) {
                                        f6 = (-f6) - calcTextWidth2;
                                        f7 = (-f7) - calcTextWidth2;
                                    }
                                    float f17 = barBuffer4.buffer[i7 + 2];
                                    if (barEntry4.getY() < 0.0f) {
                                        f6 = f7;
                                    }
                                    float f18 = barBuffer4.buffer[i8] + calcTextHeight;
                                    String str3 = str;
                                    iBarDataSet = iBarDataSet2;
                                    fArr = fArr3;
                                    i = i6;
                                    drawValue(canvas, str3, f17 + f6, f18, valueTextColor2);
                                    f = calcTextHeight;
                                }
                            } else {
                                i = i6;
                                iBarDataSet = iBarDataSet2;
                                fArr = yVals;
                                float[] fArr4 = new float[(fArr.length * 2)];
                                float f19 = -barEntry4.getNegativeSum();
                                f = calcTextHeight;
                                float f20 = 0.0f;
                                int i9 = 0;
                                int i10 = 0;
                                while (i9 < fArr4.length) {
                                    float f21 = fArr[i10];
                                    if (f21 >= 0.0f) {
                                        f5 = f20 + f21;
                                        f3 = f19;
                                        f4 = f5;
                                    } else {
                                        f3 = f19 - f21;
                                        float f22 = f19;
                                        f4 = f20;
                                        f5 = f22;
                                    }
                                    fArr4[i9] = f5 * phaseY;
                                    i9 += 2;
                                    i10++;
                                    f20 = f4;
                                    f19 = f3;
                                }
                                transformer.pointValuesToPixel(fArr4);
                                int i11 = 0;
                                while (true) {
                                    if (i11 >= fArr4.length) {
                                        break;
                                    }
                                    float f23 = fArr[i11 / 2];
                                    String formattedValue3 = iValueFormatter3.getFormattedValue(f23, barEntry4, i3, this.mViewPortHandler);
                                    float calcTextWidth3 = (float) Utils.calcTextWidth(this.mValuePaint, formattedValue3);
                                    float f24 = isDrawValueAboveBarEnabled ? convertDpToPixel : -(calcTextWidth3 + convertDpToPixel);
                                    if (isDrawValueAboveBarEnabled) {
                                        barEntry = barEntry4;
                                        f2 = -(calcTextWidth3 + convertDpToPixel);
                                    } else {
                                        barEntry = barEntry4;
                                        f2 = convertDpToPixel;
                                    }
                                    if (isInverted) {
                                        f24 = (-f24) - calcTextWidth3;
                                        f2 = (-f2) - calcTextWidth3;
                                    }
                                    float f25 = fArr4[i11];
                                    if (f23 < 0.0f) {
                                        f24 = f2;
                                    }
                                    float f26 = f24 + f25;
                                    float f27 = (barBuffer4.buffer[i7 + 1] + barBuffer4.buffer[i7 + 3]) / 2.0f;
                                    if (!this.mViewPortHandler.isInBoundsTop(f27)) {
                                        break;
                                    }
                                    if (this.mViewPortHandler.isInBoundsX(f26) && this.mViewPortHandler.isInBoundsBottom(f27)) {
                                        barEntry2 = barEntry;
                                        fArr2 = fArr4;
                                        drawValue(canvas, formattedValue3, f26, f27 + f, valueTextColor2);
                                    } else {
                                        fArr2 = fArr4;
                                        barEntry2 = barEntry;
                                    }
                                    i11 += 2;
                                    barEntry4 = barEntry2;
                                    fArr4 = fArr2;
                                }
                                if (fArr != null) {
                                    i7 += 4;
                                } else {
                                    i7 += 4 * fArr.length;
                                }
                                i6 = i + 1;
                                iBarDataSet2 = iBarDataSet;
                                calcTextHeight = f;
                            }
                            if (fArr != null) {
                            }
                            i6 = i + 1;
                            iBarDataSet2 = iBarDataSet;
                            calcTextHeight = f;
                        }
                        i3++;
                        dataSets = list;
                    }
                }
                list = dataSets;
                i3++;
                dataSets = list;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawValue(Canvas canvas, String str, float f, float f2, int i) {
        this.mValuePaint.setColor(i);
        canvas.drawText(str, f, f2, this.mValuePaint);
    }

    /* access modifiers changed from: protected */
    public void prepareBarHighlight(float f, float f2, float f3, float f4, Transformer transformer) {
        this.mBarRect.set(f2, f - f4, f3, f + f4);
        transformer.rectToPixelPhaseHorizontal(this.mBarRect, this.mAnimator.getPhaseY());
    }

    /* access modifiers changed from: protected */
    public void setHighlightDrawPos(Highlight highlight, RectF rectF) {
        highlight.setDraw(rectF.centerY(), rectF.right);
    }

    /* access modifiers changed from: protected */
    public boolean isDrawingValuesAllowed(ChartInterface chartInterface) {
        return ((float) chartInterface.getData().getEntryCount()) < ((float) chartInterface.getMaxVisibleCount()) * this.mViewPortHandler.getScaleY();
    }
}
