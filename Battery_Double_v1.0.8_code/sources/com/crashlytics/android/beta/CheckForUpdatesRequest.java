package com.crashlytics.android.beta;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class CheckForUpdatesRequest extends AbstractSpiCall {
    static final String BETA_SOURCE = "3";
    static final String BUILD_VERSION = "build_version";
    static final String DISPLAY_VERSION = "display_version";
    static final String HEADER_BETA_TOKEN = "X-CRASHLYTICS-BETA-TOKEN";
    static final String INSTANCE = "instance";
    static final String SDK_ANDROID_DIR_TOKEN_TYPE = "3";
    static final String SOURCE = "source";
    private final CheckForUpdatesResponseTransform responseTransform;

    static String createBetaTokenHeaderValueFor(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("3:");
        sb.append(str);
        return sb.toString();
    }

    public CheckForUpdatesRequest(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, CheckForUpdatesResponseTransform checkForUpdatesResponseTransform) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.GET);
        this.responseTransform = checkForUpdatesResponseTransform;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010b  */
    public CheckForUpdatesResponse invoke(String str, String str2, BuildProperties buildProperties) {
        HttpRequest httpRequest;
        String str3;
        Logger logger;
        String str4;
        StringBuilder sb;
        try {
            Map queryParamsFor = getQueryParamsFor(buildProperties);
            HttpRequest httpRequest2 = getHttpRequest(queryParamsFor);
            try {
                httpRequest = applyHeadersTo(httpRequest2, str, str2);
                try {
                    Logger logger2 = Fabric.getLogger();
                    String str5 = Beta.TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Checking for updates from ");
                    sb2.append(getUrl());
                    logger2.d(str5, sb2.toString());
                    Logger logger3 = Fabric.getLogger();
                    String str6 = Beta.TAG;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Checking for updates query params are: ");
                    sb3.append(queryParamsFor);
                    logger3.d(str6, sb3.toString());
                    if (httpRequest.ok()) {
                        Fabric.getLogger().d(Beta.TAG, "Checking for updates was successful");
                        CheckForUpdatesResponse fromJson = this.responseTransform.fromJson(new JSONObject(httpRequest.body()));
                        if (httpRequest != null) {
                            String header = httpRequest.header(AbstractSpiCall.HEADER_REQUEST_ID);
                            Logger logger4 = Fabric.getLogger();
                            String str7 = Fabric.TAG;
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Checking for updates request ID: ");
                            sb4.append(header);
                            logger4.d(str7, sb4.toString());
                        }
                        return fromJson;
                    }
                    Logger logger5 = Fabric.getLogger();
                    String str8 = Beta.TAG;
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Checking for updates failed. Response code: ");
                    sb5.append(httpRequest.code());
                    logger5.e(str8, sb5.toString());
                    if (httpRequest != null) {
                        str4 = httpRequest.header(AbstractSpiCall.HEADER_REQUEST_ID);
                        logger = Fabric.getLogger();
                        str3 = Fabric.TAG;
                        sb = new StringBuilder();
                        sb.append("Checking for updates request ID: ");
                        sb.append(str4);
                        logger.d(str3, sb.toString());
                    }
                    return null;
                } catch (Exception e) {
                    e = e;
                    try {
                        Logger logger6 = Fabric.getLogger();
                        String str9 = Beta.TAG;
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("Error while checking for updates from ");
                        sb6.append(getUrl());
                        logger6.e(str9, sb6.toString(), e);
                        if (httpRequest != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (httpRequest != null) {
                        }
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                httpRequest = httpRequest2;
                Logger logger62 = Fabric.getLogger();
                String str92 = Beta.TAG;
                StringBuilder sb62 = new StringBuilder();
                sb62.append("Error while checking for updates from ");
                sb62.append(getUrl());
                logger62.e(str92, sb62.toString(), e);
                if (httpRequest != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                httpRequest = httpRequest2;
                if (httpRequest != null) {
                }
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            httpRequest = null;
            Logger logger622 = Fabric.getLogger();
            String str922 = Beta.TAG;
            StringBuilder sb622 = new StringBuilder();
            sb622.append("Error while checking for updates from ");
            sb622.append(getUrl());
            logger622.e(str922, sb622.toString(), e);
            if (httpRequest != null) {
                str4 = httpRequest.header(AbstractSpiCall.HEADER_REQUEST_ID);
                logger = Fabric.getLogger();
                str3 = Fabric.TAG;
                sb = new StringBuilder();
                sb.append("Checking for updates request ID: ");
                sb.append(str4);
                logger.d(str3, sb.toString());
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            httpRequest = null;
            if (httpRequest != null) {
                String header2 = httpRequest.header(AbstractSpiCall.HEADER_REQUEST_ID);
                Logger logger7 = Fabric.getLogger();
                String str10 = Fabric.TAG;
                StringBuilder sb7 = new StringBuilder();
                sb7.append("Checking for updates request ID: ");
                sb7.append(header2);
                logger7.d(str10, sb7.toString());
            }
            throw th;
        }
    }

    private HttpRequest applyHeadersTo(HttpRequest httpRequest, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(AbstractSpiCall.CRASHLYTICS_USER_AGENT);
        sb.append(this.kit.getVersion());
        return httpRequest.header("Accept", "application/json").header("User-Agent", sb.toString()).header(AbstractSpiCall.HEADER_DEVELOPER_TOKEN, "470fa2b4ae81cd56ecbcda9735803434cec591fa").header(AbstractSpiCall.HEADER_CLIENT_TYPE, "android").header(AbstractSpiCall.HEADER_CLIENT_VERSION, this.kit.getVersion()).header(AbstractSpiCall.HEADER_API_KEY, str).header(HEADER_BETA_TOKEN, createBetaTokenHeaderValueFor(str2));
    }

    private Map<String, String> getQueryParamsFor(BuildProperties buildProperties) {
        HashMap hashMap = new HashMap();
        hashMap.put(BUILD_VERSION, buildProperties.versionCode);
        hashMap.put(DISPLAY_VERSION, buildProperties.versionName);
        hashMap.put(INSTANCE, buildProperties.buildId);
        hashMap.put("source", "3");
        return hashMap;
    }
}
