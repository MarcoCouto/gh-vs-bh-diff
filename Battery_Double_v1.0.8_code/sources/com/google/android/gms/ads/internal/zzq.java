package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzabl;
import com.google.android.gms.internal.ads.zzadh;
import com.google.android.gms.internal.ads.zzael;
import com.google.android.gms.internal.ads.zzaig;
import com.google.android.gms.internal.ads.zzajh;
import com.google.android.gms.internal.ads.zzaji;
import com.google.android.gms.internal.ads.zzakb;
import com.google.android.gms.internal.ads.zzakk;
import com.google.android.gms.internal.ads.zzane;
import com.google.android.gms.internal.ads.zzang;
import com.google.android.gms.internal.ads.zzaqw;
import com.google.android.gms.internal.ads.zzhs;
import com.google.android.gms.internal.ads.zzjj;
import com.google.android.gms.internal.ads.zzjn;
import com.google.android.gms.internal.ads.zzlo;
import com.google.android.gms.internal.ads.zznx;
import com.google.android.gms.internal.ads.zzod;
import com.google.android.gms.internal.ads.zzoo;
import com.google.android.gms.internal.ads.zzoq;
import com.google.android.gms.internal.ads.zzov;
import com.google.android.gms.internal.ads.zzox;
import com.google.android.gms.internal.ads.zzoy;
import com.google.android.gms.internal.ads.zzoz;
import com.google.android.gms.internal.ads.zzpa;
import com.google.android.gms.internal.ads.zzpb;
import com.google.android.gms.internal.ads.zzpw;
import com.google.android.gms.internal.ads.zzqs;
import com.google.android.gms.internal.ads.zzrc;
import com.google.android.gms.internal.ads.zzwy;
import com.google.android.gms.internal.ads.zzxn;
import com.google.android.gms.internal.ads.zzxz;
import com.google.android.gms.internal.ads.zzyc;
import com.google.android.gms.internal.ads.zzyf;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONObject;

@zzadh
@ParametersAreNonnullByDefault
public final class zzq extends zzd implements zzpa {
    private boolean zzvm;
    /* access modifiers changed from: private */
    public zzajh zzwr;
    private boolean zzws = false;

    public zzq(Context context, zzw zzw, zzjn zzjn, String str, zzxn zzxn, zzang zzang) {
        super(context, zzjn, str, zzxn, zzang, zzw);
    }

    private static zzajh zza(zzaji zzaji, int i) {
        zzaji zzaji2 = zzaji;
        zzjj zzjj = zzaji2.zzcgs.zzccv;
        List<String> list = zzaji2.zzcos.zzbsn;
        List<String> list2 = zzaji2.zzcos.zzbso;
        List<String> list3 = zzaji2.zzcos.zzces;
        int i2 = zzaji2.zzcos.orientation;
        long j = zzaji2.zzcos.zzbsu;
        String str = zzaji2.zzcgs.zzccy;
        boolean z = zzaji2.zzcos.zzceq;
        zzwy zzwy = zzaji2.zzcod;
        long j2 = zzaji2.zzcos.zzcer;
        zzjn zzjn = zzaji2.zzacv;
        long j3 = j2;
        zzwy zzwy2 = zzwy;
        long j4 = zzaji2.zzcos.zzcep;
        long j5 = zzaji2.zzcoh;
        long j6 = zzaji2.zzcoi;
        String str2 = zzaji2.zzcos.zzcev;
        JSONObject jSONObject = zzaji2.zzcob;
        zzaig zzaig = zzaji2.zzcos.zzcfe;
        List<String> list4 = zzaji2.zzcos.zzcff;
        List<String> list5 = zzaji2.zzcos.zzcff;
        boolean z2 = zzaji2.zzcos.zzcfh;
        zzael zzael = zzaji2.zzcos.zzcfi;
        List<String> list6 = zzaji2.zzcos.zzbsr;
        String str3 = zzaji2.zzcos.zzcfl;
        long j7 = j6;
        zzhs zzhs = zzaji2.zzcoq;
        boolean z3 = zzaji2.zzcos.zzzl;
        zzhs zzhs2 = zzhs;
        boolean z4 = zzaji2.zzcor;
        String str4 = str3;
        boolean z5 = zzaji2.zzcos.zzcfp;
        List<String> list7 = zzaji2.zzcos.zzbsp;
        zzaig zzaig2 = zzaig;
        List<String> list8 = list4;
        List<String> list9 = list5;
        boolean z6 = z2;
        zzael zzael2 = zzael;
        List<String> list10 = list6;
        String str5 = str4;
        boolean z7 = z5;
        List<String> list11 = list7;
        JSONObject jSONObject2 = jSONObject;
        int i3 = i;
        String str6 = str2;
        boolean z8 = z3;
        zzwy zzwy3 = zzwy2;
        long j8 = j3;
        zzjn zzjn2 = zzjn;
        long j9 = j4;
        long j10 = j5;
        long j11 = j7;
        zzhs zzhs3 = zzhs2;
        boolean z9 = z8;
        boolean z10 = z4;
        zzajh zzajh = new zzajh(zzjj, null, list, i3, list2, list3, i2, j, str, z, null, null, null, zzwy3, null, j8, zzjn2, j9, j10, j11, str6, jSONObject2, null, zzaig2, list8, list9, z6, zzael2, null, list10, str5, zzhs3, z9, z10, z7, list11, zzaji2.zzcos.zzzm, zzaji2.zzcos.zzcfq);
        return zzajh;
    }

    private final void zza(zzov zzov) {
        zzakk.zzcrm.post(new zzs(this, zzov));
    }

    private final boolean zzb(zzajh zzajh, zzajh zzajh2) {
        Handler handler;
        Runnable zzu;
        zzov zzov;
        zzajh zzajh3 = zzajh2;
        View view = null;
        zzd(null);
        if (!this.zzvw.zzfo()) {
            zzakb.zzdk("Native ad does not have custom rendering mode.");
        } else {
            try {
                zzyf zzmu = zzajh3.zzbtx != null ? zzajh3.zzbtx.zzmu() : null;
                zzxz zzmo = zzajh3.zzbtx != null ? zzajh3.zzbtx.zzmo() : null;
                zzyc zzmp = zzajh3.zzbtx != null ? zzajh3.zzbtx.zzmp() : null;
                zzqs zzmt = zzajh3.zzbtx != null ? zzajh3.zzbtx.zzmt() : null;
                String zzc = zzc(zzajh2);
                if (zzmu != null && this.zzvw.zzadg != null) {
                    String headline = zzmu.getHeadline();
                    List images = zzmu.getImages();
                    String body = zzmu.getBody();
                    zzpw zzjz = zzmu.zzjz() != null ? zzmu.zzjz() : null;
                    String callToAction = zzmu.getCallToAction();
                    String advertiser = zzmu.getAdvertiser();
                    double starRating = zzmu.getStarRating();
                    String store = zzmu.getStore();
                    String price = zzmu.getPrice();
                    zzlo videoController = zzmu.getVideoController();
                    if (zzmu.zzmw() != null) {
                        view = (View) ObjectWrapper.unwrap(zzmu.zzmw());
                    }
                    zzov = new zzov(headline, images, body, zzjz, callToAction, advertiser, starRating, store, price, null, videoController, view, zzmu.zzke(), zzc, zzmu.getExtras());
                    zzoy zzoy = new zzoy(this.zzvw.zzrt, (zzpa) this, this.zzvw.zzacq, zzmu, (zzpb) zzov);
                    zzov.zzb((zzoz) zzoy);
                } else if (zzmo == null || this.zzvw.zzadg == null) {
                    if (zzmo != null && this.zzvw.zzade != null) {
                        String headline2 = zzmo.getHeadline();
                        List images2 = zzmo.getImages();
                        String body2 = zzmo.getBody();
                        zzpw zzjz2 = zzmo.zzjz() != null ? zzmo.zzjz() : null;
                        String callToAction2 = zzmo.getCallToAction();
                        double starRating2 = zzmo.getStarRating();
                        String store2 = zzmo.getStore();
                        String price2 = zzmo.getPrice();
                        Bundle extras = zzmo.getExtras();
                        zzlo videoController2 = zzmo.getVideoController();
                        if (zzmo.zzmw() != null) {
                            view = (View) ObjectWrapper.unwrap(zzmo.zzmw());
                        }
                        zzoo zzoo = new zzoo(headline2, images2, body2, zzjz2, callToAction2, starRating2, store2, price2, null, extras, videoController2, view, zzmo.zzke(), zzc);
                        zzoy zzoy2 = new zzoy(this.zzvw.zzrt, (zzpa) this, this.zzvw.zzacq, zzmo, (zzpb) zzoo);
                        zzoo.zzb((zzoz) zzoy2);
                        handler = zzakk.zzcrm;
                        zzu = new zzt(this, zzoo);
                    } else if (zzmp != null && this.zzvw.zzadg != null) {
                        String headline3 = zzmp.getHeadline();
                        List images3 = zzmp.getImages();
                        String body3 = zzmp.getBody();
                        zzpw zzkg = zzmp.zzkg() != null ? zzmp.zzkg() : null;
                        String callToAction3 = zzmp.getCallToAction();
                        String advertiser2 = zzmp.getAdvertiser();
                        zzlo videoController3 = zzmp.getVideoController();
                        if (zzmp.zzmw() != null) {
                            view = (View) ObjectWrapper.unwrap(zzmp.zzmw());
                        }
                        zzov zzov2 = new zzov(headline3, images3, body3, zzkg, callToAction3, advertiser2, -1.0d, null, null, null, videoController3, view, zzmp.zzke(), zzc, zzmp.getExtras());
                        zzyc zzyc = zzmp;
                        zzov = zzov2;
                        zzoy zzoy3 = new zzoy(this.zzvw.zzrt, (zzpa) this, this.zzvw.zzacq, zzyc, (zzpb) zzov2);
                        zzov.zzb((zzoz) zzoy3);
                    } else if (zzmp != null && this.zzvw.zzadf != null) {
                        String headline4 = zzmp.getHeadline();
                        List images4 = zzmp.getImages();
                        String body4 = zzmp.getBody();
                        zzpw zzkg2 = zzmp.zzkg() != null ? zzmp.zzkg() : null;
                        String callToAction4 = zzmp.getCallToAction();
                        String advertiser3 = zzmp.getAdvertiser();
                        Bundle extras2 = zzmp.getExtras();
                        zzlo videoController4 = zzmp.getVideoController();
                        if (zzmp.zzmw() != null) {
                            view = (View) ObjectWrapper.unwrap(zzmp.zzmw());
                        }
                        zzoq zzoq = new zzoq(headline4, images4, body4, zzkg2, callToAction4, advertiser3, null, extras2, videoController4, view, zzmp.zzke(), zzc);
                        zzyc zzyc2 = zzmp;
                        zzoq zzoq2 = zzoq;
                        zzoy zzoy4 = new zzoy(this.zzvw.zzrt, (zzpa) this, this.zzvw.zzacq, zzyc2, (zzpb) zzoq);
                        zzoq2.zzb((zzoz) zzoy4);
                        handler = zzakk.zzcrm;
                        zzu = new zzu(this, zzoq2);
                    } else if (zzmt == null || this.zzvw.zzadi == null || this.zzvw.zzadi.get(zzmt.getCustomTemplateId()) == null) {
                        zzakb.zzdk("No matching mapper/listener for retrieved native ad template.");
                        zzi(0);
                        return false;
                    } else {
                        zzakk.zzcrm.post(new zzv(this, zzmt));
                        return super.zza(zzajh, zzajh2);
                    }
                    handler.post(zzu);
                    return super.zza(zzajh, zzajh2);
                } else {
                    String headline5 = zzmo.getHeadline();
                    List images5 = zzmo.getImages();
                    String body5 = zzmo.getBody();
                    zzpw zzjz3 = zzmo.zzjz() != null ? zzmo.zzjz() : null;
                    String callToAction5 = zzmo.getCallToAction();
                    double starRating3 = zzmo.getStarRating();
                    String store3 = zzmo.getStore();
                    String price3 = zzmo.getPrice();
                    zzlo videoController5 = zzmo.getVideoController();
                    if (zzmo.zzmw() != null) {
                        view = (View) ObjectWrapper.unwrap(zzmo.zzmw());
                    }
                    zzov = new zzov(headline5, images5, body5, zzjz3, callToAction5, null, starRating3, store3, price3, null, videoController5, view, zzmo.zzke(), zzc, zzmo.getExtras());
                    zzoy zzoy5 = new zzoy(this.zzvw.zzrt, (zzpa) this, this.zzvw.zzacq, zzmo, (zzpb) zzov);
                    zzov.zzb((zzoz) zzoy5);
                }
                zza(zzov);
                return super.zza(zzajh, zzajh2);
            } catch (RemoteException e) {
                zzakb.zzd("#007 Could not call remote method.", e);
            }
        }
        zzi(0);
        return false;
    }

    private final boolean zzc(zzajh zzajh, zzajh zzajh2) {
        View zze = zzas.zze(zzajh2);
        if (zze == null) {
            return false;
        }
        View nextView = this.zzvw.zzacs.getNextView();
        if (nextView != null) {
            if (nextView instanceof zzaqw) {
                ((zzaqw) nextView).destroy();
            }
            this.zzvw.zzacs.removeView(nextView);
        }
        if (!zzas.zzf(zzajh2)) {
            try {
                zzg(zze);
            } catch (Throwable th) {
                zzbv.zzeo().zza(th, "AdLoaderManager.swapBannerViews");
                zzakb.zzc("Could not add mediation view to view hierarchy.", th);
                return false;
            }
        }
        if (this.zzvw.zzacs.getChildCount() > 1) {
            this.zzvw.zzacs.showNext();
        }
        if (zzajh != null) {
            View nextView2 = this.zzvw.zzacs.getNextView();
            if (nextView2 != null) {
                this.zzvw.zzacs.removeView(nextView2);
            }
            this.zzvw.zzfn();
        }
        this.zzvw.zzacs.setMinimumWidth(zzbk().widthPixels);
        this.zzvw.zzacs.setMinimumHeight(zzbk().heightPixels);
        this.zzvw.zzacs.requestLayout();
        this.zzvw.zzacs.setVisibility(0);
        return true;
    }

    @Nullable
    private final zzwy zzcw() {
        if (this.zzvw.zzacw == null || !this.zzvw.zzacw.zzceq) {
            return null;
        }
        return this.zzvw.zzacw.zzcod;
    }

    @Nullable
    public final zzlo getVideoController() {
        return null;
    }

    public final void pause() {
        if (!this.zzws) {
            throw new IllegalStateException("Native Ad does not support pause().");
        }
        super.pause();
    }

    public final void resume() {
        if (!this.zzws) {
            throw new IllegalStateException("Native Ad does not support resume().");
        }
        super.resume();
    }

    public final void setManualImpressionsEnabled(boolean z) {
        Preconditions.checkMainThread("setManualImpressionsEnabled must be called from the main thread.");
        this.zzvm = z;
    }

    public final void showInterstitial() {
        throw new IllegalStateException("Interstitial is not supported by AdLoaderManager.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026  */
    public final void zza(zzaji zzaji, zznx zznx) {
        zzajh zza;
        this.zzwr = null;
        if (zzaji.errorCode != -2) {
            zza = zza(zzaji, zzaji.errorCode);
        } else {
            if (!zzaji.zzcos.zzceq) {
                zzakb.zzdk("partialAdState is not mediation");
                zza = zza(zzaji, 0);
            }
            if (this.zzwr == null) {
                zzakk.zzcrm.post(new zzr(this));
                return;
            }
            if (zzaji.zzacv != null) {
                this.zzvw.zzacv = zzaji.zzacv;
            }
            this.zzvw.zzadv = 0;
            zzbw zzbw = this.zzvw;
            zzbv.zzej();
            zzbw.zzacu = zzabl.zza(this.zzvw.zzrt, this, zzaji, this.zzvw.zzacq, null, this.zzwh, this, zznx);
            return;
        }
        this.zzwr = zza;
        if (this.zzwr == null) {
        }
    }

    public final void zza(zzod zzod) {
        throw new IllegalStateException("CustomRendering is not supported by AdLoaderManager.");
    }

    public final void zza(zzox zzox) {
        zzane.zzd("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void zza(zzoz zzoz) {
        zzane.zzd("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006f  */
    public final boolean zza(@Nullable zzajh zzajh, zzajh zzajh2) {
        String str;
        boolean z;
        if (!this.zzvw.zzfo()) {
            throw new IllegalStateException("AdLoader API does not support custom rendering.");
        }
        if (!zzajh2.zzceq) {
            zzi(0);
            str = "newState is not mediation.";
        } else {
            if (zzajh2.zzbtw != null && zzajh2.zzbtw.zzmf()) {
                if (this.zzvw.zzfo() && this.zzvw.zzacs != null) {
                    this.zzvw.zzacs.zzfr().zzdb(zzajh2.zzcev);
                }
                if (super.zza(zzajh, zzajh2)) {
                    if (!this.zzvw.zzfo() || zzc(zzajh, zzajh2)) {
                        if (!this.zzvw.zzfp()) {
                            super.zza(zzajh2, false);
                        }
                        z = true;
                        if (z) {
                            return false;
                        }
                        this.zzws = true;
                    } else {
                        zzi(0);
                    }
                }
                z = false;
                if (z) {
                }
            } else if (zzajh2.zzbtw == null || !zzajh2.zzbtw.zzmg()) {
                zzi(0);
                str = "Response is neither banner nor native.";
            } else if (!zzb(zzajh, zzajh2)) {
                return false;
            }
            zze(new ArrayList(Arrays.asList(new Integer[]{Integer.valueOf(2)})));
            return true;
        }
        zzakb.zzdk(str);
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzjj zzjj, zzajh zzajh, boolean z) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void zzb(@Nullable IObjectWrapper iObjectWrapper) {
        Object unwrap = iObjectWrapper != null ? ObjectWrapper.unwrap(iObjectWrapper) : null;
        if (unwrap instanceof zzoz) {
            ((zzoz) unwrap).zzkl();
        }
        super.zzb(this.zzvw.zzacw, false);
    }

    public final boolean zzb(zzjj zzjj) {
        zzq zzq = this;
        zzjj zzjj2 = zzjj;
        if (zzq.zzvw.zzadn != null && zzq.zzvw.zzadn.size() == 1 && ((Integer) zzq.zzvw.zzadn.get(0)).intValue() == 2) {
            zzakb.e("Requesting only banner Ad from AdLoader or calling loadAd on returned banner is not yet supported");
            zzq.zzi(0);
            return false;
        } else if (zzq.zzvw.zzadm == null) {
            return super.zzb(zzjj);
        } else {
            if (zzjj2.zzaqb != zzq.zzvm) {
                int i = zzjj2.versionCode;
                long j = zzjj2.zzapw;
                Bundle bundle = zzjj2.extras;
                int i2 = zzjj2.zzapx;
                List<String> list = zzjj2.zzapy;
                boolean z = zzjj2.zzapz;
                int i3 = zzjj2.zzaqa;
                boolean z2 = zzjj2.zzaqb || zzq.zzvm;
                zzjj zzjj3 = new zzjj(i, j, bundle, i2, list, z, i3, z2, zzjj2.zzaqc, zzjj2.zzaqd, zzjj2.zzaqe, zzjj2.zzaqf, zzjj2.zzaqg, zzjj2.zzaqh, zzjj2.zzaqi, zzjj2.zzaqj, zzjj2.zzaqk, zzjj2.zzaql);
                zzjj2 = zzjj3;
                zzq = this;
            }
            return super.zzb(zzjj2);
        }
    }

    /* access modifiers changed from: protected */
    public final void zzbq() {
        super.zzbq();
        zzajh zzajh = this.zzvw.zzacw;
        if (!(zzajh == null || zzajh.zzbtw == null || !zzajh.zzbtw.zzmf() || this.zzvw.zzadm == null)) {
            try {
                this.zzvw.zzadm.zza(this, ObjectWrapper.wrap(this.zzvw.zzrt));
                super.zzb(this.zzvw.zzacw, false);
            } catch (RemoteException e) {
                zzakb.zzd("#007 Could not call remote method.", e);
            }
        }
    }

    public final void zzce() {
        if (this.zzvw.zzacw == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzvw.zzacw.zzbty) || this.zzvw.zzacw.zzbtw == null || !this.zzvw.zzacw.zzbtw.zzmg()) {
            super.zzce();
        } else {
            zzbs();
        }
    }

    public final void zzcj() {
        if (this.zzvw.zzacw == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzvw.zzacw.zzbty) || this.zzvw.zzacw.zzbtw == null || !this.zzvw.zzacw.zzbtw.zzmg()) {
            super.zzcj();
        } else {
            zzbr();
        }
    }

    public final void zzcr() {
        zzane.zzd("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void zzcs() {
        zzane.zzd("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final void zzct() {
        zzane.zzd("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    public final boolean zzcu() {
        if (zzcw() != null) {
            return zzcw().zzbta;
        }
        return false;
    }

    public final boolean zzcv() {
        if (zzcw() != null) {
            return zzcw().zzbtb;
        }
        return false;
    }

    public final void zzd(@Nullable List<String> list) {
        Preconditions.checkMainThread("setNativeTemplates must be called on the main UI thread.");
        this.zzvw.zzads = list;
    }

    public final void zze(List<Integer> list) {
        Preconditions.checkMainThread("setAllowedAdTypes must be called on the main UI thread.");
        this.zzvw.zzadn = list;
    }

    public final void zzi(View view) {
        zzane.zzd("#005 Unexpected call to an abstract (unimplemented) method.", null);
    }

    @Nullable
    public final zzrc zzr(String str) {
        Preconditions.checkMainThread("getOnCustomClickListener must be called on the main UI thread.");
        return (zzrc) this.zzvw.zzadh.get(str);
    }
}
