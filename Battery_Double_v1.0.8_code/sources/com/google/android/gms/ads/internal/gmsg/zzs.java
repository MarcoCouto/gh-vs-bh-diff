package com.google.android.gms.ads.internal.gmsg;

import com.facebook.internal.NativeProtocol;
import com.google.android.gms.internal.ads.zzaqw;
import java.util.Map;

final class zzs implements zzv<zzaqw> {
    zzs() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzaqw zzaqw = (zzaqw) obj;
        String str = (String) map.get(NativeProtocol.WEB_DIALOG_ACTION);
        if ("pause".equals(str)) {
            zzaqw.zzcl();
            return;
        }
        if ("resume".equals(str)) {
            zzaqw.zzcm();
        }
    }
}
