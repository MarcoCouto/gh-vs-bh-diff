package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.support.annotation.Keep;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzadh;
import com.google.android.gms.internal.ads.zzakb;
import com.google.android.gms.internal.ads.zzaki;
import com.google.android.gms.internal.ads.zzakk;
import com.google.android.gms.internal.ads.zzamy;
import com.google.android.gms.internal.ads.zzang;
import com.google.android.gms.internal.ads.zzue;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Keep
@KeepName
@zzadh
public class HttpClient implements zzv<zzue> {
    private final Context mContext;
    private final zzang zzyf;

    @zzadh
    @VisibleForTesting
    static class zza {
        private final String mKey;
        private final String mValue;

        public zza(String str, String str2) {
            this.mKey = str;
            this.mValue = str2;
        }

        public final String getKey() {
            return this.mKey;
        }

        public final String getValue() {
            return this.mValue;
        }
    }

    @zzadh
    @VisibleForTesting
    static class zzb {
        private final String zzbmm;
        private final URL zzbmn;
        private final ArrayList<zza> zzbmo;
        private final String zzbmp;

        zzb(String str, URL url, ArrayList<zza> arrayList, String str2) {
            this.zzbmm = str;
            this.zzbmn = url;
            this.zzbmo = arrayList;
            this.zzbmp = str2;
        }

        public final String zzkv() {
            return this.zzbmm;
        }

        public final URL zzkw() {
            return this.zzbmn;
        }

        public final ArrayList<zza> zzkx() {
            return this.zzbmo;
        }

        public final String zzky() {
            return this.zzbmp;
        }
    }

    @zzadh
    @VisibleForTesting
    class zzc {
        private final zzd zzbmq;
        private final boolean zzbmr;
        private final String zzbms;

        public zzc(HttpClient httpClient, boolean z, zzd zzd, String str) {
            this.zzbmr = z;
            this.zzbmq = zzd;
            this.zzbms = str;
        }

        public final String getReason() {
            return this.zzbms;
        }

        public final boolean isSuccess() {
            return this.zzbmr;
        }

        public final zzd zzkz() {
            return this.zzbmq;
        }
    }

    @zzadh
    @VisibleForTesting
    static class zzd {
        private final String zzbhy;
        private final String zzbmm;
        private final int zzbmt;
        private final List<zza> zzcf;

        zzd(String str, int i, List<zza> list, String str2) {
            this.zzbmm = str;
            this.zzbmt = i;
            this.zzcf = list;
            this.zzbhy = str2;
        }

        public final String getBody() {
            return this.zzbhy;
        }

        public final int getResponseCode() {
            return this.zzbmt;
        }

        public final String zzkv() {
            return this.zzbmm;
        }

        public final Iterable<zza> zzla() {
            return this.zzcf;
        }
    }

    public HttpClient(Context context, zzang zzang) {
        this.mContext = context;
        this.zzyf = zzang;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0104  */
    private final zzc zza(zzb zzb2) {
        HttpURLConnection httpURLConnection;
        byte[] bArr;
        try {
            httpURLConnection = (HttpURLConnection) zzb2.zzkw().openConnection();
            try {
                zzbv.zzek().zza(this.mContext, this.zzyf.zzcw, false, httpURLConnection);
                ArrayList zzkx = zzb2.zzkx();
                int size = zzkx.size();
                int i = 0;
                while (i < size) {
                    Object obj = zzkx.get(i);
                    i++;
                    zza zza2 = (zza) obj;
                    httpURLConnection.addRequestProperty(zza2.getKey(), zza2.getValue());
                }
                if (!TextUtils.isEmpty(zzb2.zzky())) {
                    httpURLConnection.setDoOutput(true);
                    bArr = zzb2.zzky().getBytes();
                    httpURLConnection.setFixedLengthStreamingMode(bArr.length);
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                    bufferedOutputStream.write(bArr);
                    bufferedOutputStream.close();
                } else {
                    bArr = null;
                }
                zzamy zzamy = new zzamy();
                zzamy.zza(httpURLConnection, bArr);
                ArrayList arrayList = new ArrayList();
                if (httpURLConnection.getHeaderFields() != null) {
                    for (Entry entry : httpURLConnection.getHeaderFields().entrySet()) {
                        for (String zza3 : (List) entry.getValue()) {
                            arrayList.add(new zza((String) entry.getKey(), zza3));
                        }
                    }
                }
                String zzkv = zzb2.zzkv();
                int responseCode = httpURLConnection.getResponseCode();
                zzbv.zzek();
                zzd zzd2 = new zzd(zzkv, responseCode, arrayList, zzakk.zza(new InputStreamReader(httpURLConnection.getInputStream())));
                zzamy.zza(httpURLConnection, zzd2.getResponseCode());
                zzamy.zzdg(zzd2.getBody());
                zzc zzc2 = new zzc(this, true, zzd2, null);
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                return zzc2;
            } catch (Exception e) {
                e = e;
                try {
                    zzc zzc3 = new zzc(this, false, null, e.toString());
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return zzc3;
                } catch (Throwable th) {
                    th = th;
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            httpURLConnection = null;
            zzc zzc32 = new zzc(this, false, null, e.toString());
            if (httpURLConnection != null) {
            }
            return zzc32;
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    private static JSONObject zza(zzd zzd2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("http_request_id", zzd2.zzkv());
            if (zzd2.getBody() != null) {
                jSONObject.put(TtmlNode.TAG_BODY, zzd2.getBody());
            }
            JSONArray jSONArray = new JSONArray();
            for (zza zza2 : zzd2.zzla()) {
                jSONArray.put(new JSONObject().put("key", zza2.getKey()).put(Param.VALUE, zza2.getValue()));
            }
            jSONObject.put("headers", jSONArray);
            jSONObject.put("response_code", zzd2.getResponseCode());
            return jSONObject;
        } catch (JSONException e) {
            zzakb.zzb("Error constructing JSON for http response.", e);
            return jSONObject;
        }
    }

    private static zzb zzc(JSONObject jSONObject) {
        String optString = jSONObject.optString("http_request_id");
        String optString2 = jSONObject.optString("url");
        URL url = null;
        String optString3 = jSONObject.optString("post_body", null);
        try {
            url = new URL(optString2);
        } catch (MalformedURLException e) {
            zzakb.zzb("Error constructing http request.", e);
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(new zza(optJSONObject.optString("key"), optJSONObject.optString(Param.VALUE)));
            }
        }
        return new zzb(optString, url, arrayList, optString3);
    }

    @Keep
    @KeepName
    public JSONObject send(JSONObject jSONObject) {
        String str;
        JSONObject jSONObject2 = new JSONObject();
        try {
            str = jSONObject.optString("http_request_id");
            try {
                zzc zza2 = zza(zzc(jSONObject));
                if (zza2.isSuccess()) {
                    jSONObject2.put("response", zza(zza2.zzkz()));
                    jSONObject2.put("success", true);
                    return jSONObject2;
                }
                jSONObject2.put("response", new JSONObject().put("http_request_id", str));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", zza2.getReason());
                return jSONObject2;
            } catch (Exception e) {
                e = e;
                zzakb.zzb("Error executing http request.", e);
                try {
                    jSONObject2.put("response", new JSONObject().put("http_request_id", str));
                    jSONObject2.put("success", false);
                    jSONObject2.put("reason", e.toString());
                    return jSONObject2;
                } catch (JSONException e2) {
                    zzakb.zzb("Error executing http request.", e2);
                    return jSONObject2;
                }
            }
        } catch (Exception e3) {
            e = e3;
            str = "";
            zzakb.zzb("Error executing http request.", e);
            jSONObject2.put("response", new JSONObject().put("http_request_id", str));
            jSONObject2.put("success", false);
            jSONObject2.put("reason", e.toString());
            return jSONObject2;
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzaki.zzb(new zzw(this, map, (zzue) obj));
    }
}
