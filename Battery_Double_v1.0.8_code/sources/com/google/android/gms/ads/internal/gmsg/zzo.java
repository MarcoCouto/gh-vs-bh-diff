package com.google.android.gms.ads.internal.gmsg;

import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.internal.ads.zzaqw;
import java.util.Map;

final class zzo implements zzv<zzaqw> {
    zzo() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzaqw) obj).zzu(AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(map.get("custom_close")));
    }
}
