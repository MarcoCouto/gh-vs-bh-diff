package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.applinks.AppLinkData;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.gms.ads.internal.gmsg.zzv;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzadh;
import com.google.android.gms.internal.ads.zzajh;
import com.google.android.gms.internal.ads.zzakb;
import com.google.android.gms.internal.ads.zzaqw;
import com.google.android.gms.internal.ads.zzasc;
import com.google.android.gms.internal.ads.zzasd;
import com.google.android.gms.internal.ads.zzoo;
import com.google.android.gms.internal.ads.zzoq;
import com.google.android.gms.internal.ads.zzpw;
import com.google.android.gms.internal.ads.zzpx;
import com.google.android.gms.internal.ads.zzxe;
import com.google.android.gms.internal.ads.zzxz;
import com.google.android.gms.internal.ads.zzyc;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
@ParametersAreNonnullByDefault
public final class zzas {
    @VisibleForTesting
    static zzv<zzaqw> zza(@Nullable zzxz zzxz, @Nullable zzyc zzyc, zzac zzac) {
        return new zzax(zzxz, zzac, zzyc);
    }

    private static String zza(@Nullable Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap == null) {
            zzakb.zzdk("Bitmap is null. Returning empty string");
            return "";
        }
        bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        String valueOf = String.valueOf("data:image/png;base64,");
        String valueOf2 = String.valueOf(encodeToString);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @VisibleForTesting
    private static String zza(@Nullable zzpw zzpw) {
        if (zzpw == null) {
            zzakb.zzdk("Image is null. Returning empty string");
            return "";
        }
        try {
            Uri uri = zzpw.getUri();
            if (uri != null) {
                return uri.toString();
            }
        } catch (RemoteException unused) {
            zzakb.zzdk("Unable to get image uri. Trying data uri next");
        }
        return zzb(zzpw);
    }

    private static JSONObject zza(@Nullable Bundle bundle, String str) throws JSONException {
        String valueOf;
        String str2;
        JSONObject jSONObject = new JSONObject();
        if (bundle == null || TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str3 = (String) keys.next();
            if (bundle.containsKey(str3)) {
                if (MessengerShareContentUtility.MEDIA_IMAGE.equals(jSONObject2.getString(str3))) {
                    Object obj = bundle.get(str3);
                    if (obj instanceof Bitmap) {
                        valueOf = zza((Bitmap) obj);
                    } else {
                        str2 = "Invalid type. An image type extra should return a bitmap";
                        zzakb.zzdk(str2);
                    }
                } else if (bundle.get(str3) instanceof Bitmap) {
                    str2 = "Invalid asset type. Bitmap should be returned only for image type";
                    zzakb.zzdk(str2);
                } else {
                    valueOf = String.valueOf(bundle.get(str3));
                }
                jSONObject.put(str3, valueOf);
            }
        }
        return jSONObject;
    }

    static final /* synthetic */ void zza(zzoo zzoo, String str, zzaqw zzaqw, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("headline", zzoo.getHeadline());
            jSONObject.put(TtmlNode.TAG_BODY, zzoo.getBody());
            jSONObject.put("call_to_action", zzoo.getCallToAction());
            jSONObject.put(Param.PRICE, zzoo.getPrice());
            jSONObject.put("star_rating", String.valueOf(zzoo.getStarRating()));
            jSONObject.put("store", zzoo.getStore());
            jSONObject.put(SettingsJsonConstants.APP_ICON_KEY, zza(zzoo.zzjz()));
            JSONArray jSONArray = new JSONArray();
            List<Object> images = zzoo.getImages();
            if (images != null) {
                for (Object zzd : images) {
                    jSONArray.put(zza(zzd(zzd)));
                }
            }
            jSONObject.put("images", jSONArray);
            jSONObject.put(AppLinkData.ARGUMENTS_EXTRAS_KEY, zza(zzoo.getExtras(), str));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("assets", jSONObject);
            jSONObject2.put("template_id", "2");
            zzaqw.zzb("google.afma.nativeExpressAds.loadAssets", jSONObject2);
        } catch (JSONException e) {
            zzakb.zzc("Exception occurred when loading assets", e);
        }
    }

    static final /* synthetic */ void zza(zzoq zzoq, String str, zzaqw zzaqw, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("headline", zzoq.getHeadline());
            jSONObject.put(TtmlNode.TAG_BODY, zzoq.getBody());
            jSONObject.put("call_to_action", zzoq.getCallToAction());
            jSONObject.put("advertiser", zzoq.getAdvertiser());
            jSONObject.put("logo", zza(zzoq.zzkg()));
            JSONArray jSONArray = new JSONArray();
            List<Object> images = zzoq.getImages();
            if (images != null) {
                for (Object zzd : images) {
                    jSONArray.put(zza(zzd(zzd)));
                }
            }
            jSONObject.put("images", jSONArray);
            jSONObject.put(AppLinkData.ARGUMENTS_EXTRAS_KEY, zza(zzoq.getExtras(), str));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("assets", jSONObject);
            jSONObject2.put("template_id", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            zzaqw.zzb("google.afma.nativeExpressAds.loadAssets", jSONObject2);
        } catch (JSONException e) {
            zzakb.zzc("Exception occurred when loading assets", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0134  */
    public static boolean zza(zzaqw zzaqw, zzxe zzxe, CountDownLatch countDownLatch) {
        String str;
        zzasc zzuf;
        zzasd zzau;
        zzaqw zzaqw2 = zzaqw;
        zzxe zzxe2 = zzxe;
        CountDownLatch countDownLatch2 = countDownLatch;
        boolean z = false;
        try {
            View view = zzaqw.getView();
            if (view == null) {
                str = "AdWebView is null";
            } else {
                view.setVisibility(4);
                List<String> list = zzxe2.zzbtw.zzbsi;
                if (list != null) {
                    if (!list.isEmpty()) {
                        zzaqw2.zza("/nativeExpressAssetsLoaded", (zzv<? super zzaqw>) new zzav<Object>(countDownLatch2));
                        zzaqw2.zza("/nativeExpressAssetsLoadingFailed", (zzv<? super zzaqw>) new zzaw<Object>(countDownLatch2));
                        zzxz zzmo = zzxe2.zzbtx.zzmo();
                        zzyc zzmp = zzxe2.zzbtx.zzmp();
                        View view2 = null;
                        if (list.contains("2") && zzmo != null) {
                            String headline = zzmo.getHeadline();
                            List images = zzmo.getImages();
                            String body = zzmo.getBody();
                            zzpw zzjz = zzmo.zzjz();
                            String callToAction = zzmo.getCallToAction();
                            double starRating = zzmo.getStarRating();
                            String store = zzmo.getStore();
                            String price = zzmo.getPrice();
                            Bundle extras = zzmo.getExtras();
                            if (zzmo.zzmw() != null) {
                                view2 = (View) ObjectWrapper.unwrap(zzmo.zzmw());
                            }
                            zzoo zzoo = new zzoo(headline, images, body, zzjz, callToAction, starRating, store, price, null, extras, null, view2, zzmo.zzke(), null);
                            String str2 = zzxe2.zzbtw.zzbsh;
                            zzuf = zzaqw.zzuf();
                            zzau = new zzat(zzoo, str2, zzaqw2);
                        } else if (!list.contains(AppEventsConstants.EVENT_PARAM_VALUE_YES) || zzmp == null) {
                            str = "No matching template id and mapper";
                        } else {
                            String headline2 = zzmp.getHeadline();
                            List images2 = zzmp.getImages();
                            String body2 = zzmp.getBody();
                            zzpw zzkg = zzmp.zzkg();
                            String callToAction2 = zzmp.getCallToAction();
                            String advertiser = zzmp.getAdvertiser();
                            Bundle extras2 = zzmp.getExtras();
                            if (zzmp.zzmw() != null) {
                                view2 = (View) ObjectWrapper.unwrap(zzmp.zzmw());
                            }
                            zzoq zzoq = new zzoq(headline2, images2, body2, zzkg, callToAction2, advertiser, null, extras2, null, view2, zzmp.zzke(), null);
                            String str3 = zzxe2.zzbtw.zzbsh;
                            zzuf = zzaqw.zzuf();
                            zzau = new zzau(zzoq, str3, zzaqw2);
                        }
                        zzuf.zza(zzau);
                        String str4 = zzxe2.zzbtw.zzbsf;
                        String str5 = zzxe2.zzbtw.zzbsg;
                        if (str5 != null) {
                            zzaqw2.loadDataWithBaseURL(str5, str4, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8", null);
                        } else {
                            zzaqw2.loadData(str4, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8");
                        }
                        z = true;
                        if (!z) {
                            countDownLatch.countDown();
                        }
                        return z;
                    }
                }
                str = "No template ids present in mediation response";
            }
            zzakb.zzdk(str);
        } catch (RemoteException e) {
            zzakb.zzc("Unable to invoke load assets", e);
        } catch (RuntimeException e2) {
            RuntimeException runtimeException = e2;
            countDownLatch.countDown();
            throw runtimeException;
        }
        if (!z) {
        }
        return z;
    }

    private static String zzb(zzpw zzpw) {
        try {
            IObjectWrapper zzjy = zzpw.zzjy();
            if (zzjy == null) {
                zzakb.zzdk("Drawable is null. Returning empty string");
                return "";
            }
            Drawable drawable = (Drawable) ObjectWrapper.unwrap(zzjy);
            if (drawable instanceof BitmapDrawable) {
                return zza(((BitmapDrawable) drawable).getBitmap());
            }
            zzakb.zzdk("Drawable is not an instance of BitmapDrawable. Returning empty string");
            return "";
        } catch (RemoteException unused) {
            zzakb.zzdk("Unable to get drawable. Returning empty string");
            return "";
        }
    }

    @Nullable
    private static zzpw zzd(Object obj) {
        if (obj instanceof IBinder) {
            return zzpx.zzh((IBinder) obj);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static void zzd(zzaqw zzaqw) {
        OnClickListener onClickListener = zzaqw.getOnClickListener();
        if (onClickListener != null) {
            onClickListener.onClick(zzaqw.getView());
        }
    }

    @Nullable
    public static View zze(@Nullable zzajh zzajh) {
        if (zzajh == null) {
            zzakb.e("AdState is null");
            return null;
        } else if (zzf(zzajh) && zzajh.zzbyo != null) {
            return zzajh.zzbyo.getView();
        } else {
            try {
                IObjectWrapper view = zzajh.zzbtx != null ? zzajh.zzbtx.getView() : null;
                if (view != null) {
                    return (View) ObjectWrapper.unwrap(view);
                }
                zzakb.zzdk("View in mediation adapter is null.");
                return null;
            } catch (RemoteException e) {
                zzakb.zzc("Could not get View from mediation adapter.", e);
                return null;
            }
        }
    }

    public static boolean zzf(@Nullable zzajh zzajh) {
        return (zzajh == null || !zzajh.zzceq || zzajh.zzbtw == null || zzajh.zzbtw.zzbsf == null) ? false : true;
    }
}
