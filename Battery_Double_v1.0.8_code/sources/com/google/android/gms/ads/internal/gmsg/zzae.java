package com.google.android.gms.ads.internal.gmsg;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzakk;
import com.google.android.gms.internal.ads.zzci;
import com.google.android.gms.internal.ads.zzkb;
import com.google.android.gms.internal.ads.zznk;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@VisibleForTesting
public final class zzae {
    private final Context mContext;
    private final View mView;
    private final zzci zzbne;

    public zzae(Context context, zzci zzci, View view) {
        this.mContext = context;
        this.zzbne = zzci;
        this.mView = view;
    }

    private static Intent zza(Intent intent, ResolveInfo resolveInfo) {
        Intent intent2 = new Intent(intent);
        intent2.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        return intent2;
    }

    @VisibleForTesting
    private final ResolveInfo zza(Intent intent, ArrayList<ResolveInfo> arrayList) {
        ResolveInfo resolveInfo = null;
        try {
            PackageManager packageManager = this.mContext.getPackageManager();
            if (packageManager == null) {
                return null;
            }
            List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 65536);
            if (queryIntentActivities != null && resolveActivity != null) {
                int i = 0;
                while (true) {
                    if (i >= queryIntentActivities.size()) {
                        break;
                    }
                    ResolveInfo resolveInfo2 = (ResolveInfo) queryIntentActivities.get(i);
                    if (resolveActivity != null && resolveActivity.activityInfo.name.equals(resolveInfo2.activityInfo.name)) {
                        resolveInfo = resolveActivity;
                        break;
                    }
                    i++;
                }
            }
            arrayList.addAll(queryIntentActivities);
            return resolveInfo;
        } catch (Throwable th) {
            zzbv.zzeo().zza(th, "OpenSystemBrowserHandler.getDefaultBrowserResolverForIntent");
            return null;
        }
    }

    @VisibleForTesting
    private final ResolveInfo zzb(Intent intent) {
        return zza(intent, new ArrayList<>());
    }

    private static Intent zze(Uri uri) {
        if (uri == null) {
            return null;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(ErrorDialogData.BINDER_CRASH);
        intent.setData(uri);
        intent.setAction("android.intent.action.VIEW");
        return intent;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00bd  */
    @VisibleForTesting
    public final Intent zzi(Map<String, String> map) {
        boolean z;
        ResolveInfo zza;
        Builder buildUpon;
        String str;
        ActivityManager activityManager = (ActivityManager) this.mContext.getSystemService("activity");
        String str2 = (String) map.get("u");
        Uri uri = null;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        Uri parse = Uri.parse(zzad.zza(this.mContext, this.zzbne, str2, this.mView, null));
        boolean parseBoolean = Boolean.parseBoolean((String) map.get("use_first_package"));
        boolean parseBoolean2 = Boolean.parseBoolean((String) map.get("use_running_process"));
        if (!Boolean.parseBoolean((String) map.get("use_custom_tabs"))) {
            if (!((Boolean) zzkb.zzik().zzd(zznk.zzbdz)).booleanValue()) {
                z = false;
                if (!"http".equalsIgnoreCase(parse.getScheme())) {
                    buildUpon = parse.buildUpon();
                    str = "https";
                } else {
                    if ("https".equalsIgnoreCase(parse.getScheme())) {
                        buildUpon = parse.buildUpon();
                        str = "http";
                    }
                    ArrayList arrayList = new ArrayList();
                    Intent zze = zze(parse);
                    Intent zze2 = zze(uri);
                    if (z) {
                        zzbv.zzek();
                        zzakk.zzb(this.mContext, zze);
                        zzbv.zzek();
                        zzakk.zzb(this.mContext, zze2);
                    }
                    zza = zza(zze, arrayList);
                    if (zza != null) {
                        return zza(zze, zza);
                    }
                    if (zze2 != null) {
                        ResolveInfo zzb = zzb(zze2);
                        if (zzb != null) {
                            Intent zza2 = zza(zze, zzb);
                            if (zzb(zza2) != null) {
                                return zza2;
                            }
                        }
                    }
                    if (arrayList.size() == 0) {
                        return zze;
                    }
                    if (parseBoolean2 && activityManager != null) {
                        List runningAppProcesses = activityManager.getRunningAppProcesses();
                        if (runningAppProcesses != null) {
                            ArrayList arrayList2 = arrayList;
                            int size = arrayList2.size();
                            int i = 0;
                            while (i < size) {
                                Object obj = arrayList2.get(i);
                                i++;
                                ResolveInfo resolveInfo = (ResolveInfo) obj;
                                Iterator it = runningAppProcesses.iterator();
                                while (true) {
                                    if (it.hasNext()) {
                                        if (((RunningAppProcessInfo) it.next()).processName.equals(resolveInfo.activityInfo.packageName)) {
                                            return zza(zze, resolveInfo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return parseBoolean ? zza(zze, (ResolveInfo) arrayList.get(0)) : zze;
                }
                uri = buildUpon.scheme(str).build();
                ArrayList arrayList3 = new ArrayList();
                Intent zze3 = zze(parse);
                Intent zze22 = zze(uri);
                if (z) {
                }
                zza = zza(zze3, arrayList3);
                if (zza != null) {
                }
            }
        }
        z = true;
        if (!"http".equalsIgnoreCase(parse.getScheme())) {
        }
        uri = buildUpon.scheme(str).build();
        ArrayList arrayList32 = new ArrayList();
        Intent zze32 = zze(parse);
        Intent zze222 = zze(uri);
        if (z) {
        }
        zza = zza(zze32, arrayList32);
        if (zza != null) {
        }
    }
}
