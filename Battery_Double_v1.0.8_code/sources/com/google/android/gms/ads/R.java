package com.google.android.gms.ads;

public final class R {

    public static final class attr {
        public static final int adSize = 2130968610;
        public static final int adSizes = 2130968611;
        public static final int adUnitId = 2130968612;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131689797;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.mansoon.BatteryDouble.R.attr.adSize, com.mansoon.BatteryDouble.R.attr.adSizes, com.mansoon.BatteryDouble.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
    }
}
