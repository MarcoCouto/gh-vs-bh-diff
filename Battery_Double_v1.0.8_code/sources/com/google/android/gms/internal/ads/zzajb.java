package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;

@zzadh
public final class zzajb {
    public static Uri zzb(Uri uri, Context context) {
        if (!zzbv.zzfh().zzx(context) || !TextUtils.isEmpty(uri.getQueryParameter("fbs_aeid"))) {
            return uri;
        }
        String zzab = zzbv.zzfh().zzab(context);
        Uri zzb = zzb(uri.toString(), "fbs_aeid", zzab);
        zzbv.zzfh().zze(context, zzab);
        return zzb;
    }

    @VisibleForTesting
    private static Uri zzb(String str, String str2, String str3) {
        int indexOf = str.indexOf("&adurl");
        if (indexOf == -1) {
            indexOf = str.indexOf("?adurl");
        }
        if (indexOf == -1) {
            return Uri.parse(str).buildUpon().appendQueryParameter(str2, str3).build();
        }
        int i = indexOf + 1;
        StringBuilder sb = new StringBuilder(str.substring(0, i));
        sb.append(str2);
        sb.append("=");
        sb.append(str3);
        sb.append("&");
        sb.append(str.substring(i));
        return Uri.parse(sb.toString());
    }

    public static String zzb(String str, Context context) {
        if (zzbv.zzfh().zzs(context)) {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            String zzab = zzbv.zzfh().zzab(context);
            if (zzab == null) {
                return str;
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzaxr)).booleanValue()) {
                String str2 = (String) zzkb.zzik().zzd(zznk.zzaxs);
                if (str.contains(str2)) {
                    if (zzbv.zzek().zzcx(str)) {
                        zzbv.zzfh().zze(context, zzab);
                        return str.replace(str2, zzab);
                    } else if (zzbv.zzek().zzcy(str)) {
                        zzbv.zzfh().zzf(context, zzab);
                        return str.replace(str2, zzab);
                    }
                }
            } else if (!str.contains("fbs_aeid")) {
                if (zzbv.zzek().zzcx(str)) {
                    zzbv.zzfh().zze(context, zzab);
                    return zzb(str, "fbs_aeid", zzab).toString();
                } else if (zzbv.zzek().zzcy(str)) {
                    zzbv.zzfh().zzf(context, zzab);
                    str = zzb(str, "fbs_aeid", zzab).toString();
                }
            }
        }
        return str;
    }

    public static String zzc(String str, Context context) {
        if (zzbv.zzfh().zzs(context)) {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            String zzab = zzbv.zzfh().zzab(context);
            if (zzab == null || !zzbv.zzek().zzcy(str)) {
                return str;
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzaxr)).booleanValue()) {
                String str2 = (String) zzkb.zzik().zzd(zznk.zzaxs);
                if (str.contains(str2)) {
                    return str.replace(str2, zzab);
                }
            } else if (!str.contains("fbs_aeid")) {
                str = zzb(str, "fbs_aeid", zzab).toString();
            }
        }
        return str;
    }
}
