package com.google.android.gms.internal.ads;

final /* synthetic */ class zzayu {
    static final /* synthetic */ int[] zzdnm = new int[zzayw.values().length];
    static final /* synthetic */ int[] zzdnn = new int[zzayv.values().length];

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001f */
    static {
        try {
            zzdnn[zzayv.NIST_P256.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        zzdnn[zzayv.NIST_P384.ordinal()] = 2;
        try {
            zzdnn[zzayv.NIST_P521.ordinal()] = 3;
        } catch (NoSuchFieldError unused2) {
        }
        zzdnm[zzayw.UNCOMPRESSED.ordinal()] = 1;
        try {
            zzdnm[zzayw.COMPRESSED.ordinal()] = 2;
        } catch (NoSuchFieldError unused3) {
        }
    }
}
