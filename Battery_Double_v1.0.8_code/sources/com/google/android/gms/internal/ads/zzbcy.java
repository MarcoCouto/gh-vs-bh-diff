package com.google.android.gms.internal.ads;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import com.google.android.gms.internal.ads.zzbbo.zze;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import sun.misc.Unsafe;

final class zzbcy<T> implements zzbdm<T> {
    private static final Unsafe zzdwf = zzbek.zzagh();
    private final int[] zzdwg;
    private final Object[] zzdwh;
    private final int zzdwi;
    private final int zzdwj;
    private final int zzdwk;
    private final zzbcu zzdwl;
    private final boolean zzdwm;
    private final boolean zzdwn;
    private final boolean zzdwo;
    private final boolean zzdwp;
    private final int[] zzdwq;
    private final int[] zzdwr;
    private final int[] zzdws;
    private final zzbdc zzdwt;
    private final zzbce zzdwu;
    private final zzbee<?, ?> zzdwv;
    private final zzbbd<?> zzdww;
    private final zzbcp zzdwx;

    private zzbcy(int[] iArr, Object[] objArr, int i, int i2, int i3, zzbcu zzbcu, boolean z, boolean z2, int[] iArr2, int[] iArr3, int[] iArr4, zzbdc zzbdc, zzbce zzbce, zzbee<?, ?> zzbee, zzbbd<?> zzbbd, zzbcp zzbcp) {
        zzbcu zzbcu2 = zzbcu;
        zzbbd<?> zzbbd2 = zzbbd;
        this.zzdwg = iArr;
        this.zzdwh = objArr;
        this.zzdwi = i;
        this.zzdwj = i2;
        this.zzdwk = i3;
        this.zzdwn = zzbcu2 instanceof zzbbo;
        this.zzdwo = z;
        this.zzdwm = zzbbd2 != null && zzbbd2.zzh(zzbcu2);
        this.zzdwp = false;
        this.zzdwq = iArr2;
        this.zzdwr = iArr3;
        this.zzdws = iArr4;
        this.zzdwt = zzbdc;
        this.zzdwu = zzbce;
        this.zzdwv = zzbee;
        this.zzdww = zzbbd2;
        this.zzdwl = zzbcu2;
        this.zzdwx = zzbcp;
    }

    private static int zza(int i, byte[] bArr, int i2, int i3, Object obj, zzbae zzbae) throws IOException {
        return zzbad.zza(i, bArr, i2, i3, zzz(obj), zzbae);
    }

    private static int zza(zzbdm<?> zzbdm, int i, byte[] bArr, int i2, int i3, zzbbt<?> zzbbt, zzbae zzbae) throws IOException {
        int zza = zza((zzbdm) zzbdm, bArr, i2, i3, zzbae);
        while (true) {
            zzbbt.add(zzbae.zzdpn);
            if (zza >= i3) {
                break;
            }
            int zza2 = zzbad.zza(bArr, zza, zzbae);
            if (i != zzbae.zzdpl) {
                break;
            }
            zza = zza((zzbdm) zzbdm, bArr, zza2, i3, zzbae);
        }
        return zza;
    }

    private static int zza(zzbdm zzbdm, byte[] bArr, int i, int i2, int i3, zzbae zzbae) throws IOException {
        zzbcy zzbcy = (zzbcy) zzbdm;
        Object newInstance = zzbcy.newInstance();
        int zza = zzbcy.zza((T) newInstance, bArr, i, i2, i3, zzbae);
        zzbcy.zzo(newInstance);
        zzbae.zzdpn = newInstance;
        return zza;
    }

    /* JADX WARNING: type inference failed for: r8v2, types: [int] */
    /* JADX WARNING: type inference failed for: r8v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    private static int zza(zzbdm zzbdm, byte[] bArr, int i, int i2, zzbae zzbae) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            i3 = zzbad.zza((int) b, bArr, i3, zzbae);
            b = zzbae.zzdpl;
        }
        int i4 = i3;
        if (b < 0 || b > i2 - i4) {
            throw zzbbu.zzadl();
        }
        Object newInstance = zzbdm.newInstance();
        int i5 = b + i4;
        zzbdm.zza(newInstance, bArr, i4, i5, zzbae);
        zzbdm.zzo(newInstance);
        zzbae.zzdpn = newInstance;
        return i5;
    }

    private static <UT, UB> int zza(zzbee<UT, UB> zzbee, T t) {
        return zzbee.zzy(zzbee.zzac(t));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b3, code lost:
        r2 = r2 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x013c, code lost:
        r3 = java.lang.Integer.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0149, code lost:
        r3 = java.lang.Long.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x014d, code lost:
        r12.putObject(r1, r9, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x015b, code lost:
        r12.putObject(r1, r9, r2);
        r2 = r4 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x016c, code lost:
        r12.putObject(r1, r9, r2);
        r2 = r4 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0171, code lost:
        r12.putInt(r1, r13, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0174, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0176, code lost:
        return r4;
     */
    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzbae zzbae) throws IOException {
        int i9;
        Object obj;
        Object obj2;
        Object obj3;
        long j2;
        int zzb;
        int i10;
        int i11;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i12 = i;
        int i13 = i3;
        int i14 = i4;
        int i15 = i5;
        long j3 = j;
        int i16 = i8;
        zzbae zzbae2 = zzbae;
        Unsafe unsafe = zzdwf;
        long j4 = (long) (this.zzdwg[i16 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i15 == 1) {
                    obj = Double.valueOf(zzbad.zzg(bArr, i));
                    break;
                }
                break;
            case 52:
                if (i15 == 5) {
                    obj2 = Float.valueOf(zzbad.zzh(bArr, i));
                    break;
                }
                break;
            case 53:
            case 54:
                if (i15 == 0) {
                    zzb = zzbad.zzb(bArr2, i12, zzbae2);
                    j2 = zzbae2.zzdpm;
                    break;
                }
                break;
            case 55:
            case 62:
                if (i15 == 0) {
                    i9 = zzbad.zza(bArr2, i12, zzbae2);
                    i10 = zzbae2.zzdpl;
                    break;
                }
                break;
            case 56:
            case 65:
                if (i15 == 1) {
                    obj = Long.valueOf(zzbad.zzf(bArr, i));
                    break;
                }
                break;
            case 57:
            case 64:
                if (i15 == 5) {
                    obj2 = Integer.valueOf(zzbad.zze(bArr, i));
                    break;
                }
                break;
            case 58:
                if (i15 == 0) {
                    i9 = zzbad.zzb(bArr2, i12, zzbae2);
                    obj3 = Boolean.valueOf(zzbae2.zzdpm != 0);
                    break;
                }
                break;
            case 59:
                if (i15 == 2) {
                    i9 = zzbad.zza(bArr2, i12, zzbae2);
                    i11 = zzbae2.zzdpl;
                    if (i11 == 0) {
                        obj3 = "";
                        break;
                    } else if ((i6 & ErrorDialogData.DYNAMITE_CRASH) == 0 || zzbem.zzf(bArr2, i9, i9 + i11)) {
                        unsafe.putObject(t2, j3, new String(bArr2, i9, i11, zzbbq.UTF_8));
                        break;
                    } else {
                        throw zzbbu.zzads();
                    }
                }
                break;
            case 60:
                if (i15 == 2) {
                    i9 = zza(zzcq(i16), bArr2, i12, i2, zzbae2);
                    Object object = unsafe.getInt(t2, j4) == i14 ? unsafe.getObject(t2, j3) : null;
                    if (object != null) {
                        obj3 = zzbbq.zza(object, zzbae2.zzdpn);
                        break;
                    } else {
                        obj3 = zzbae2.zzdpn;
                        break;
                    }
                }
                break;
            case 61:
                if (i15 == 2) {
                    i9 = zzbad.zza(bArr2, i12, zzbae2);
                    i11 = zzbae2.zzdpl;
                    if (i11 != 0) {
                        unsafe.putObject(t2, j3, zzbah.zzc(bArr2, i9, i11));
                        break;
                    } else {
                        obj3 = zzbah.zzdpq;
                        break;
                    }
                }
                break;
            case 63:
                if (i15 == 0) {
                    int zza = zzbad.zza(bArr2, i12, zzbae2);
                    int i17 = zzbae2.zzdpl;
                    zzbbs zzcs = zzcs(i16);
                    if (zzcs == null || zzcs.zzq(i17) != null) {
                        unsafe.putObject(t2, j3, Integer.valueOf(i17));
                        i9 = zza;
                        break;
                    } else {
                        zzz(t).zzb(i13, Long.valueOf((long) i17));
                        return zza;
                    }
                }
                break;
            case 66:
                if (i15 == 0) {
                    i9 = zzbad.zza(bArr2, i12, zzbae2);
                    i10 = zzbaq.zzbu(zzbae2.zzdpl);
                    break;
                }
                break;
            case 67:
                if (i15 == 0) {
                    zzb = zzbad.zzb(bArr2, i12, zzbae2);
                    j2 = zzbaq.zzl(zzbae2.zzdpm);
                    break;
                }
                break;
            case 68:
                if (i15 == 3) {
                    i9 = zza(zzcq(i16), bArr2, i12, i2, (i13 & -8) | 4, zzbae2);
                    Object object2 = unsafe.getInt(t2, j4) == i14 ? unsafe.getObject(t2, j3) : null;
                    if (object2 != null) {
                        obj3 = zzbbq.zza(object2, zzbae2.zzdpn);
                        break;
                    } else {
                        obj3 = zzbae2.zzdpn;
                        break;
                    }
                }
                break;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0216, code lost:
        if (r11.zzdpm != 0) goto L_0x0218;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0218, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x021a, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x021b, code lost:
        r12.addBoolean(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x021e, code lost:
        if (r2 >= r8) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0220, code lost:
        r3 = com.google.android.gms.internal.ads.zzbad.zza(r7, r2, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0226, code lost:
        if (r9 != r11.zzdpl) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0228, code lost:
        r2 = com.google.android.gms.internal.ads.zzbad.zzb(r7, r3, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0230, code lost:
        if (r11.zzdpm == 0) goto L_0x021a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x037e, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x011b, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0124, code lost:
        if (r2 == 0) goto L_0x0126;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0126, code lost:
        r12.add(com.google.android.gms.internal.ads.zzbah.zzdpq);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x012c, code lost:
        r12.add(com.google.android.gms.internal.ads.zzbah.zzc(r7, r1, r2));
        r1 = r1 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0134, code lost:
        if (r1 >= r8) goto L_0x037e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0136, code lost:
        r2 = com.google.android.gms.internal.ads.zzbad.zza(r7, r1, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x013c, code lost:
        if (r9 != r11.zzdpl) goto L_0x037e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x013e, code lost:
        r1 = com.google.android.gms.internal.ads.zzbad.zza(r7, r2, r11);
        r2 = r11.zzdpl;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0144, code lost:
        if (r2 != 0) goto L_0x012c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x017d  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01be  */
    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, long j, int i7, long j2, zzbae zzbae) throws IOException {
        int i8;
        int i9;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i;
        int i11 = i2;
        int i12 = i3;
        int i13 = i5;
        int i14 = i6;
        long j3 = j2;
        zzbae zzbae2 = zzbae;
        zzbbt zzbbt = (zzbbt) zzdwf.getObject(t2, j3);
        if (!zzbbt.zzaay()) {
            int size = zzbbt.size();
            zzbbt = zzbbt.zzbm(size == 0 ? 10 : size << 1);
            zzdwf.putObject(t2, j3, zzbbt);
        }
        zzbbt zzbbt2 = zzbbt;
        switch (i7) {
            case 18:
            case 35:
                if (i13 != 2) {
                    if (i13 == 1) {
                        zzbay zzbay = (zzbay) zzbbt2;
                        zzbay.zzd(zzbad.zzg(bArr, i));
                        int i15 = i10 + 8;
                        while (i8 < i11) {
                            int zza = zzbad.zza(bArr2, i8, zzbae2);
                            if (i12 != zzbae2.zzdpl) {
                                break;
                            } else {
                                zzbay.zzd(zzbad.zzg(bArr2, zza));
                                i15 = zza + 8;
                            }
                        }
                        break;
                    }
                } else {
                    zzbay zzbay2 = (zzbay) zzbbt2;
                    int zza2 = zzbad.zza(bArr2, i10, zzbae2);
                    int i16 = zzbae2.zzdpl + zza2;
                    while (i8 < i16) {
                        zzbay2.zzd(zzbad.zzg(bArr2, i8));
                        zza2 = i8 + 8;
                    }
                    if (i8 != i16) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 19:
            case 36:
                if (i13 != 2) {
                    if (i13 == 5) {
                        zzbbm zzbbm = (zzbbm) zzbbt2;
                        zzbbm.zzd(zzbad.zzh(bArr, i));
                        int i17 = i10 + 4;
                        while (i8 < i11) {
                            int zza3 = zzbad.zza(bArr2, i8, zzbae2);
                            if (i12 != zzbae2.zzdpl) {
                                break;
                            } else {
                                zzbbm.zzd(zzbad.zzh(bArr2, zza3));
                                i17 = zza3 + 4;
                            }
                        }
                        break;
                    }
                } else {
                    zzbbm zzbbm2 = (zzbbm) zzbbt2;
                    int zza4 = zzbad.zza(bArr2, i10, zzbae2);
                    int i18 = zzbae2.zzdpl + zza4;
                    while (i8 < i18) {
                        zzbbm2.zzd(zzbad.zzh(bArr2, i8));
                        zza4 = i8 + 4;
                    }
                    if (i8 != i18) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 20:
            case 21:
            case 37:
            case 38:
                if (i13 != 2) {
                    if (i13 == 0) {
                        zzbci zzbci = (zzbci) zzbbt2;
                        int zzb = zzbad.zzb(bArr2, i10, zzbae2);
                        while (true) {
                            zzbci.zzw(zzbae2.zzdpm);
                            if (i8 >= i11) {
                                break;
                            } else {
                                int zza5 = zzbad.zza(bArr2, i8, zzbae2);
                                if (i12 != zzbae2.zzdpl) {
                                    break;
                                } else {
                                    zzb = zzbad.zzb(bArr2, zza5, zzbae2);
                                }
                            }
                        }
                    }
                } else {
                    zzbci zzbci2 = (zzbci) zzbbt2;
                    int zza6 = zzbad.zza(bArr2, i10, zzbae2);
                    int i19 = zzbae2.zzdpl + zza6;
                    while (i8 < i19) {
                        zza6 = zzbad.zzb(bArr2, i8, zzbae2);
                        zzbci2.zzw(zzbae2.zzdpm);
                    }
                    if (i8 != i19) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 22:
            case 29:
            case 39:
            case 43:
                if (i13 != 2) {
                    if (i13 == 0) {
                        return zzbad.zza(i12, bArr2, i10, i11, zzbbt2, zzbae2);
                    }
                    i8 = i10;
                    break;
                } else {
                    return zzbad.zza(bArr2, i10, zzbbt2, zzbae2);
                }
            case 23:
            case 32:
            case 40:
            case 46:
                if (i13 != 2) {
                    if (i13 == 1) {
                        zzbci zzbci3 = (zzbci) zzbbt2;
                        zzbci3.zzw(zzbad.zzf(bArr, i));
                        int i20 = i10 + 8;
                        while (i8 < i11) {
                            int zza7 = zzbad.zza(bArr2, i8, zzbae2);
                            if (i12 != zzbae2.zzdpl) {
                                break;
                            } else {
                                zzbci3.zzw(zzbad.zzf(bArr2, zza7));
                                i20 = zza7 + 8;
                            }
                        }
                        break;
                    }
                } else {
                    zzbci zzbci4 = (zzbci) zzbbt2;
                    int zza8 = zzbad.zza(bArr2, i10, zzbae2);
                    int i21 = zzbae2.zzdpl + zza8;
                    while (i8 < i21) {
                        zzbci4.zzw(zzbad.zzf(bArr2, i8));
                        zza8 = i8 + 8;
                    }
                    if (i8 != i21) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 24:
            case 31:
            case 41:
            case 45:
                if (i13 != 2) {
                    if (i13 == 5) {
                        zzbbp zzbbp = (zzbbp) zzbbt2;
                        zzbbp.zzco(zzbad.zze(bArr, i));
                        int i22 = i10 + 4;
                        while (i8 < i11) {
                            int zza9 = zzbad.zza(bArr2, i8, zzbae2);
                            if (i12 != zzbae2.zzdpl) {
                                break;
                            } else {
                                zzbbp.zzco(zzbad.zze(bArr2, zza9));
                                i22 = zza9 + 4;
                            }
                        }
                        break;
                    }
                } else {
                    zzbbp zzbbp2 = (zzbbp) zzbbt2;
                    int zza10 = zzbad.zza(bArr2, i10, zzbae2);
                    int i23 = zzbae2.zzdpl + zza10;
                    while (i8 < i23) {
                        zzbbp2.zzco(zzbad.zze(bArr2, i8));
                        zza10 = i8 + 4;
                    }
                    if (i8 != i23) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 25:
            case 42:
                if (i13 != 2) {
                    if (i13 == 0) {
                        zzbaf zzbaf = (zzbaf) zzbbt2;
                        int zzb2 = zzbad.zzb(bArr2, i10, zzbae2);
                        break;
                    }
                    i8 = i10;
                    break;
                } else {
                    zzbaf zzbaf2 = (zzbaf) zzbbt2;
                    int zza11 = zzbad.zza(bArr2, i10, zzbae2);
                    int i24 = zzbae2.zzdpl + zza11;
                    while (i9 < i24) {
                        zza11 = zzbad.zzb(bArr2, i9, zzbae2);
                        zzbaf2.addBoolean(zzbae2.zzdpm != 0);
                    }
                    if (i9 != i24) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 26:
                if (i13 == 2) {
                    if ((j & 536870912) == 0) {
                        int zza12 = zzbad.zza(bArr2, i10, zzbae2);
                        int i25 = zzbae2.zzdpl;
                        if (i25 != 0) {
                            String str = new String(bArr2, zza12, i25, zzbbq.UTF_8);
                            zzbbt2.add(str);
                            zza12 += i25;
                            if (i8 < i11) {
                                int zza13 = zzbad.zza(bArr2, i8, zzbae2);
                                if (i12 == zzbae2.zzdpl) {
                                    zza12 = zzbad.zza(bArr2, zza13, zzbae2);
                                    i25 = zzbae2.zzdpl;
                                    if (i25 != 0) {
                                        str = new String(bArr2, zza12, i25, zzbbq.UTF_8);
                                        zzbbt2.add(str);
                                        zza12 += i25;
                                        if (i8 < i11) {
                                        }
                                    }
                                }
                            }
                        }
                        zzbbt2.add("");
                        if (i8 < i11) {
                        }
                    } else {
                        int zza14 = zzbad.zza(bArr2, i10, zzbae2);
                        int i26 = zzbae2.zzdpl;
                        if (i26 != 0) {
                            int i27 = zza14 + i26;
                            if (!zzbem.zzf(bArr2, zza14, i27)) {
                                throw zzbbu.zzads();
                            }
                            String str2 = new String(bArr2, zza14, i26, zzbbq.UTF_8);
                            zzbbt2.add(str2);
                            zza14 = i27;
                            if (i8 < i11) {
                                int zza15 = zzbad.zza(bArr2, i8, zzbae2);
                                if (i12 == zzbae2.zzdpl) {
                                    zza14 = zzbad.zza(bArr2, zza15, zzbae2);
                                    int i28 = zzbae2.zzdpl;
                                    if (i28 != 0) {
                                        i27 = zza14 + i28;
                                        if (zzbem.zzf(bArr2, zza14, i27)) {
                                            str2 = new String(bArr2, zza14, i28, zzbbq.UTF_8);
                                            zzbbt2.add(str2);
                                            zza14 = i27;
                                            if (i8 < i11) {
                                            }
                                        }
                                        throw zzbbu.zzads();
                                    }
                                }
                            }
                        }
                        zzbbt2.add("");
                        if (i8 < i11) {
                        }
                    }
                }
                break;
            case 27:
                if (i13 == 2) {
                    return zza(zzcq(i14), i12, bArr2, i10, i11, zzbbt2, zzbae2);
                }
                i8 = i10;
                break;
            case 28:
                if (i13 == 2) {
                    int zza16 = zzbad.zza(bArr2, i10, zzbae2);
                    int i29 = zzbae2.zzdpl;
                    break;
                }
            case 30:
            case 44:
                if (i13 != 2) {
                    if (i13 == 0) {
                        i9 = zzbad.zza(i12, bArr2, i10, i11, zzbbt2, zzbae2);
                    }
                    i8 = i10;
                    break;
                } else {
                    i9 = zzbad.zza(bArr2, i10, zzbbt2, zzbae2);
                }
                zzbbo zzbbo = (zzbbo) t2;
                zzbef zzbef = zzbbo.zzdtt;
                if (zzbef == zzbef.zzagc()) {
                    zzbef = null;
                }
                zzbef zzbef2 = (zzbef) zzbdo.zza(i4, zzbbt2, zzcs(i14), zzbef, this.zzdwv);
                if (zzbef2 != null) {
                    zzbbo.zzdtt = zzbef2;
                    break;
                }
                break;
            case 33:
            case 47:
                if (i13 != 2) {
                    if (i13 == 0) {
                        zzbbp zzbbp3 = (zzbbp) zzbbt2;
                        int zza17 = zzbad.zza(bArr2, i10, zzbae2);
                        while (true) {
                            zzbbp3.zzco(zzbaq.zzbu(zzbae2.zzdpl));
                            if (i8 >= i11) {
                                break;
                            } else {
                                int zza18 = zzbad.zza(bArr2, i8, zzbae2);
                                if (i12 != zzbae2.zzdpl) {
                                    break;
                                } else {
                                    zza17 = zzbad.zza(bArr2, zza18, zzbae2);
                                }
                            }
                        }
                    }
                } else {
                    zzbbp zzbbp4 = (zzbbp) zzbbt2;
                    int zza19 = zzbad.zza(bArr2, i10, zzbae2);
                    int i30 = zzbae2.zzdpl + zza19;
                    while (i8 < i30) {
                        zza19 = zzbad.zza(bArr2, i8, zzbae2);
                        zzbbp4.zzco(zzbaq.zzbu(zzbae2.zzdpl));
                    }
                    if (i8 != i30) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 34:
            case 48:
                if (i13 != 2) {
                    if (i13 == 0) {
                        zzbci zzbci5 = (zzbci) zzbbt2;
                        int zzb3 = zzbad.zzb(bArr2, i10, zzbae2);
                        while (true) {
                            zzbci5.zzw(zzbaq.zzl(zzbae2.zzdpm));
                            if (i8 >= i11) {
                                break;
                            } else {
                                int zza20 = zzbad.zza(bArr2, i8, zzbae2);
                                if (i12 != zzbae2.zzdpl) {
                                    break;
                                } else {
                                    zzb3 = zzbad.zzb(bArr2, zza20, zzbae2);
                                }
                            }
                        }
                    }
                } else {
                    zzbci zzbci6 = (zzbci) zzbbt2;
                    i8 = zzbad.zza(bArr2, i10, zzbae2);
                    int i31 = zzbae2.zzdpl + i8;
                    while (i8 < i31) {
                        i8 = zzbad.zzb(bArr2, i8, zzbae2);
                        zzbci6.zzw(zzbaq.zzl(zzbae2.zzdpm));
                    }
                    if (i8 != i31) {
                        throw zzbbu.zzadl();
                    }
                }
                break;
            case 49:
                if (i13 == 3) {
                    zzbdm zzcq = zzcq(i14);
                    int i32 = (i12 & -8) | 4;
                    zzbdm zzbdm = zzcq;
                    byte[] bArr3 = bArr2;
                    int i33 = i10;
                    while (true) {
                        i8 = zza(zzbdm, bArr3, i33, i11, i32, zzbae2);
                        zzbbt2.add(zzbae2.zzdpn);
                        if (i8 >= i11) {
                            break;
                        } else {
                            i33 = zzbad.zza(bArr2, i8, zzbae2);
                            if (i12 != zzbae2.zzdpl) {
                                break;
                            } else {
                                zzbdm = zzcq;
                                bArr3 = bArr2;
                            }
                        }
                    }
                }
        }
    }

    /* JADX WARNING: type inference failed for: r9v4, types: [int] */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    private final <K, V> int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, long j, zzbae zzbae) throws IOException {
        Unsafe unsafe = zzdwf;
        Object zzcr = zzcr(i3);
        Object object = unsafe.getObject(t, j);
        if (this.zzdwx.zzu(object)) {
            Object zzw = this.zzdwx.zzw(zzcr);
            this.zzdwx.zzb(zzw, object);
            unsafe.putObject(t, j, zzw);
            object = zzw;
        }
        zzbcn zzx = this.zzdwx.zzx(zzcr);
        Map zzs = this.zzdwx.zzs(object);
        int zza = zzbad.zza(bArr, i, zzbae);
        int i5 = zzbae.zzdpl;
        if (i5 < 0 || i5 > i2 - zza) {
            throw zzbbu.zzadl();
        }
        int i6 = i5 + zza;
        K k = zzx.zzdvz;
        V v = zzx.zzdwb;
        while (zza < i6) {
            int i7 = zza + 1;
            byte b = bArr[zza];
            if (b < 0) {
                i7 = zzbad.zza((int) b, bArr, i7, zzbae);
                b = zzbae.zzdpl;
            }
            int i8 = i7;
            int i9 = b & 7;
            switch (b >>> 3) {
                case 1:
                    if (i9 == zzx.zzdvy.zzagm()) {
                        zza = zza(bArr, i8, i2, zzx.zzdvy, null, zzbae);
                        k = zzbae.zzdpn;
                        break;
                    }
                case 2:
                    if (i9 == zzx.zzdwa.zzagm()) {
                        zza = zza(bArr, i8, i2, zzx.zzdwa, zzx.zzdwb.getClass(), zzbae);
                        v = zzbae.zzdpn;
                        break;
                    }
                default:
                    zza = zzbad.zza(b, bArr, i8, i2, zzbae);
                    break;
            }
        }
        if (zza != i6) {
            throw zzbbu.zzadr();
        }
        zzs.put(k, v);
        return i6;
    }

    /* JADX WARNING: type inference failed for: r33v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v0 */
    /* JADX WARNING: type inference failed for: r12v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v11, types: [byte, int] */
    /* JADX WARNING: type inference failed for: r5v5, types: [int] */
    /* JADX WARNING: type inference failed for: r12v2 */
    /* JADX WARNING: type inference failed for: r1v12 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r1v13 */
    /* JADX WARNING: type inference failed for: r7v6 */
    /* JADX WARNING: type inference failed for: r0v14, types: [int] */
    /* JADX WARNING: type inference failed for: r1v14, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v4 */
    /* JADX WARNING: type inference failed for: r1v15 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r30v0 */
    /* JADX WARNING: type inference failed for: r7v9 */
    /* JADX WARNING: type inference failed for: r30v1 */
    /* JADX WARNING: type inference failed for: r30v2 */
    /* JADX WARNING: type inference failed for: r30v3 */
    /* JADX WARNING: type inference failed for: r19v0 */
    /* JADX WARNING: type inference failed for: r11v5 */
    /* JADX WARNING: type inference failed for: r30v4 */
    /* JADX WARNING: type inference failed for: r12v5 */
    /* JADX WARNING: type inference failed for: r1v18 */
    /* JADX WARNING: type inference failed for: r2v8, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v9, types: [int] */
    /* JADX WARNING: type inference failed for: r2v9, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v10, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v11, types: [int] */
    /* JADX WARNING: type inference failed for: r30v5 */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: type inference failed for: r1v23 */
    /* JADX WARNING: type inference failed for: r7v17 */
    /* JADX WARNING: type inference failed for: r30v6 */
    /* JADX WARNING: type inference failed for: r1v24, types: [int] */
    /* JADX WARNING: type inference failed for: r2v13, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v9 */
    /* JADX WARNING: type inference failed for: r1v25 */
    /* JADX WARNING: type inference failed for: r11v10 */
    /* JADX WARNING: type inference failed for: r7v20 */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r11v11 */
    /* JADX WARNING: type inference failed for: r1v26 */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r11v12 */
    /* JADX WARNING: type inference failed for: r11v13 */
    /* JADX WARNING: type inference failed for: r12v12, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v14 */
    /* JADX WARNING: type inference failed for: r12v13, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v14 */
    /* JADX WARNING: type inference failed for: r11v15 */
    /* JADX WARNING: type inference failed for: r1v30 */
    /* JADX WARNING: type inference failed for: r11v16 */
    /* JADX WARNING: type inference failed for: r12v15, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v16 */
    /* JADX WARNING: type inference failed for: r11v17 */
    /* JADX WARNING: type inference failed for: r11v18 */
    /* JADX WARNING: type inference failed for: r12v17, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v19 */
    /* JADX WARNING: type inference failed for: r11v20 */
    /* JADX WARNING: type inference failed for: r12v18, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v21 */
    /* JADX WARNING: type inference failed for: r12v19, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v22 */
    /* JADX WARNING: type inference failed for: r12v20, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v23 */
    /* JADX WARNING: type inference failed for: r12v21, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v24 */
    /* JADX WARNING: type inference failed for: r12v22, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v23 */
    /* JADX WARNING: type inference failed for: r11v25 */
    /* JADX WARNING: type inference failed for: r12v24 */
    /* JADX WARNING: type inference failed for: r11v26 */
    /* JADX WARNING: type inference failed for: r11v27 */
    /* JADX WARNING: type inference failed for: r12v25, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v28, types: [int] */
    /* JADX WARNING: type inference failed for: r12v26, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v29 */
    /* JADX WARNING: type inference failed for: r12v27, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v30 */
    /* JADX WARNING: type inference failed for: r11v31 */
    /* JADX WARNING: type inference failed for: r12v28, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v32 */
    /* JADX WARNING: type inference failed for: r12v29 */
    /* JADX WARNING: type inference failed for: r12v30 */
    /* JADX WARNING: type inference failed for: r1v48, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r11v33 */
    /* JADX WARNING: type inference failed for: r11v34 */
    /* JADX WARNING: type inference failed for: r12v31 */
    /* JADX WARNING: type inference failed for: r5v17 */
    /* JADX WARNING: type inference failed for: r1v53, types: [int] */
    /* JADX WARNING: type inference failed for: r5v18 */
    /* JADX WARNING: type inference failed for: r12v32 */
    /* JADX WARNING: type inference failed for: r12v33 */
    /* JADX WARNING: type inference failed for: r1v54 */
    /* JADX WARNING: type inference failed for: r12v34 */
    /* JADX WARNING: type inference failed for: r1v55 */
    /* JADX WARNING: type inference failed for: r7v41 */
    /* JADX WARNING: type inference failed for: r7v42 */
    /* JADX WARNING: type inference failed for: r30v7 */
    /* JADX WARNING: type inference failed for: r30v8 */
    /* JADX WARNING: type inference failed for: r30v9 */
    /* JADX WARNING: type inference failed for: r30v10 */
    /* JADX WARNING: type inference failed for: r1v56 */
    /* JADX WARNING: type inference failed for: r12v35 */
    /* JADX WARNING: type inference failed for: r1v57 */
    /* JADX WARNING: type inference failed for: r12v36 */
    /* JADX WARNING: type inference failed for: r12v37 */
    /* JADX WARNING: type inference failed for: r11v37 */
    /* JADX WARNING: type inference failed for: r12v38 */
    /* JADX WARNING: type inference failed for: r12v39 */
    /* JADX WARNING: type inference failed for: r12v40 */
    /* JADX WARNING: type inference failed for: r12v41 */
    /* JADX WARNING: type inference failed for: r12v42 */
    /* JADX WARNING: type inference failed for: r11v38 */
    /* JADX WARNING: type inference failed for: r12v43 */
    /* JADX WARNING: type inference failed for: r11v39 */
    /* JADX WARNING: type inference failed for: r12v44 */
    /* JADX WARNING: type inference failed for: r12v45 */
    /* JADX WARNING: type inference failed for: r12v46 */
    /* JADX WARNING: type inference failed for: r12v47 */
    /* JADX WARNING: type inference failed for: r12v48 */
    /* JADX WARNING: type inference failed for: r12v49 */
    /* JADX WARNING: type inference failed for: r12v50 */
    /* JADX WARNING: type inference failed for: r12v51 */
    /* JADX WARNING: type inference failed for: r11v40 */
    /* JADX WARNING: type inference failed for: r12v52 */
    /* JADX WARNING: type inference failed for: r11v41 */
    /* JADX WARNING: type inference failed for: r12v53 */
    /* JADX WARNING: type inference failed for: r12v54 */
    /* JADX WARNING: type inference failed for: r12v55 */
    /* JADX WARNING: type inference failed for: r12v56 */
    /* JADX WARNING: type inference failed for: r11v42 */
    /* JADX WARNING: type inference failed for: r12v57 */
    /* JADX WARNING: type inference failed for: r12v58 */
    /* JADX WARNING: type inference failed for: r12v59 */
    /* JADX WARNING: type inference failed for: r11v43 */
    /* JADX WARNING: type inference failed for: r11v44 */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x030a, code lost:
        if (r0 == r15) goto L_0x032d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x032b, code lost:
        if (r0 == r15) goto L_0x032d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x032f, code lost:
        r12 = r33;
        r13 = r35;
        r9 = r37;
        r15 = r14;
        r6 = r17;
        r7 = r24;
        r10 = r29;
        r1 = r30;
        r8 = -1;
        r11 = r36;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c9, code lost:
        r0 = r20;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0128, code lost:
        r1 = r9.zzdpn;
        r12 = r12;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x012a, code lost:
        r10.putObject(r14, r7, r1);
        r12 = r12;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01c7, code lost:
        r0 = r3;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01dc, code lost:
        r10.putInt(r14, r7, r1);
        r12 = r12;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01f2, code lost:
        r10.putLong(r14, r7, r4);
        r6 = r6 | r23;
        r1 = r11;
        r0 = r17;
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0229, code lost:
        r6 = r6 | r23;
        r12 = r12;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x022b, code lost:
        r1 = r11;
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0233, code lost:
        r2 = r0;
        r17 = r6;
        r29 = r10;
        r7 = r11;
        r14 = r15;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=null, for r0v11, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r33v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v2
  assigns: []
  uses: []
  mth insns count: 479
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0353 A[ADDED_TO_REGION] */
    /* JADX WARNING: Unknown variable types count: 56 */
    private final int zza(T t, byte[] r33, int i, int i2, int i3, zzbae zzbae) throws IOException {
        Unsafe unsafe;
        int i4;
        zzbcy zzbcy;
        int i5;
        ? r7;
        int i6;
        int i7;
        int i8;
        T t2;
        ? r5;
        int i9;
        ? r12;
        ? r1;
        ? r122;
        int i10;
        int i11;
        int i12;
        ? r72;
        int zza;
        ? r30;
        ? r302;
        int i13;
        ? r303;
        zzbcy zzbcy2;
        ? r304;
        ? r123;
        ? r13;
        ? r11;
        int i14;
        ? r124;
        int i15;
        ? r125;
        long j;
        long j2;
        ? r126;
        long j3;
        int i16;
        int zza2;
        int i17;
        long j4;
        Object zza3;
        ? r127;
        ? r128;
        zzbcy zzbcy3 = this;
        T t3 = t;
        ? r129 = r33;
        int i18 = i2;
        int i19 = i3;
        zzbae zzbae2 = zzbae;
        Unsafe unsafe2 = zzdwf;
        int i20 = -1;
        int i21 = i;
        int i22 = -1;
        ? r14 = 0;
        int i23 = 0;
        while (true) {
            if (i21 < i18) {
                int i24 = i21 + 1;
                ? r0 = r129[i21];
                if (r0 < 0) {
                    i9 = zzbad.zza((int) r0, (byte[]) r129, i24, zzbae2);
                    r5 = zzbae2.zzdpl;
                } else {
                    r5 = r0;
                    i9 = i24;
                }
                int i25 = r5 >>> 3;
                int i26 = r5 & 7;
                int zzcw = zzbcy3.zzcw(i25);
                if (zzcw != i20) {
                    int i27 = zzbcy3.zzdwg[zzcw + 1];
                    int i28 = (i27 & 267386880) >>> 20;
                    ? r19 = r5;
                    int i29 = i9;
                    long j5 = (long) (i27 & 1048575);
                    if (i28 <= 17) {
                        int i30 = zzbcy3.zzdwg[zzcw + 2];
                        boolean z = true;
                        int i31 = 1 << (i30 >>> 20);
                        int i32 = i30 & 1048575;
                        if (i32 != i22) {
                            if (i22 != -1) {
                                unsafe2.putInt(t3, (long) i22, i23);
                            }
                            i23 = unsafe2.getInt(t3, (long) i32);
                            i22 = i32;
                        }
                        switch (i28) {
                            case 0:
                                i11 = i22;
                                r11 = r19;
                                i14 = i29;
                                long j6 = j5;
                                ? r1210 = r33;
                                if (i26 == 1) {
                                    zzbek.zza((Object) t3, j6, zzbad.zzg(r1210, i14));
                                    i21 = i14 + 8;
                                    r124 = r1210;
                                    break;
                                }
                                break;
                            case 1:
                                i11 = i22;
                                r11 = r19;
                                i14 = i29;
                                long j7 = j5;
                                ? r1211 = r33;
                                if (i26 == 5) {
                                    zzbek.zza((Object) t3, j7, zzbad.zzh(r1211, i14));
                                    i21 = i14 + 4;
                                    r124 = r1211;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                i11 = i22;
                                r11 = r19;
                                i14 = i29;
                                j = j5;
                                ? r1212 = r33;
                                if (i26 == 0) {
                                    i15 = zzbad.zzb(r1212, i14, zzbae2);
                                    j2 = zzbae2.zzdpm;
                                    r125 = r1212;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                i11 = i22;
                                r11 = r19;
                                i14 = i29;
                                j3 = j5;
                                ? r1213 = r33;
                                if (i26 == 0) {
                                    zza2 = zzbad.zza(r1213, i14, zzbae2);
                                    i16 = zzbae2.zzdpl;
                                    r126 = r1213;
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                long j8 = j5;
                                ? r1214 = r33;
                                if (i26 == 1) {
                                    long j9 = j8;
                                    int i33 = i17;
                                    unsafe2.putLong(t3, j9, zzbad.zzf(r1214, i17));
                                    i21 = i33 + 8;
                                    r124 = r1214;
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                long j10 = j5;
                                ? r1215 = r33;
                                if (i26 == 5) {
                                    unsafe2.putInt(t3, j10, zzbad.zze(r1215, i17));
                                    i21 = i17 + 4;
                                    r124 = r1215;
                                    break;
                                }
                                break;
                            case 7:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                long j11 = j5;
                                ? r1216 = r33;
                                if (i26 == 0) {
                                    i21 = zzbad.zzb(r1216, i17, zzbae2);
                                    if (zzbae2.zzdpm == 0) {
                                        z = false;
                                    }
                                    zzbek.zza((Object) t3, j11, z);
                                    r124 = r1216;
                                    break;
                                }
                                break;
                            case 8:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                j4 = j5;
                                ? r1217 = r33;
                                if (i26 == 2) {
                                    if ((i27 & ErrorDialogData.DYNAMITE_CRASH) != 0) {
                                        i21 = zzbad.zzd(r1217, i17, zzbae2);
                                        r128 = r1217;
                                        break;
                                    } else {
                                        i21 = zzbad.zzc(r1217, i17, zzbae2);
                                        r128 = r1217;
                                        break;
                                    }
                                }
                                break;
                            case 9:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                j4 = j5;
                                ? r1218 = r33;
                                if (i26 == 2) {
                                    i21 = zza(zzbcy3.zzcq(zzcw), (byte[]) r1218, i17, i18, zzbae2);
                                    if ((i23 & i31) != 0) {
                                        zza3 = zzbbq.zza(unsafe2.getObject(t3, j4), zzbae2.zzdpn);
                                        r127 = r1218;
                                        break;
                                    } else {
                                        zza3 = zzbae2.zzdpn;
                                        r127 = r1218;
                                        break;
                                    }
                                }
                                break;
                            case 10:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                j4 = j5;
                                ? r1219 = r33;
                                if (i26 == 2) {
                                    i21 = zzbad.zze(r1219, i17, zzbae2);
                                    r128 = r1219;
                                    break;
                                }
                                break;
                            case 12:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                long j12 = j5;
                                ? r1220 = r33;
                                if (i26 == 0) {
                                    i21 = zzbad.zza(r1220, i17, zzbae2);
                                    int i34 = zzbae2.zzdpl;
                                    zzbbs zzcs = zzbcy3.zzcs(zzcw);
                                    if (zzcs != null && zzcs.zzq(i34) == null) {
                                        zzz(t).zzb(r11, Long.valueOf((long) i34));
                                        ? r1221 = r1220;
                                        break;
                                    } else {
                                        unsafe2.putInt(t3, j12, i34);
                                        r124 = r1220;
                                        break;
                                    }
                                }
                                break;
                            case 15:
                                i11 = i22;
                                r11 = r19;
                                i17 = i29;
                                j3 = j5;
                                ? r1222 = r33;
                                if (i26 == 0) {
                                    zza2 = zzbad.zza(r1222, i17, zzbae2);
                                    i16 = zzbaq.zzbu(zzbae2.zzdpl);
                                    r126 = r1222;
                                    break;
                                }
                                break;
                            case 16:
                                i11 = i22;
                                r11 = r19;
                                j = j5;
                                ? r1223 = r33;
                                if (i26 == 0) {
                                    i15 = zzbad.zzb(r1223, i29, zzbae2);
                                    j2 = zzbaq.zzl(zzbae2.zzdpm);
                                    r125 = r1223;
                                    break;
                                }
                                break;
                            case 17:
                                if (i26 != 3) {
                                    i11 = i22;
                                    r11 = r19;
                                    ? r1224 = r33;
                                    break;
                                } else {
                                    ? r1225 = r33;
                                    i11 = i22;
                                    j4 = j5;
                                    ? r112 = r19;
                                    i21 = zza(zzbcy3.zzcq(zzcw), (byte[]) r1225, i29, i18, (i25 << 3) | 4, zzbae2);
                                    if ((i23 & i31) != 0) {
                                        zza3 = zzbbq.zza(unsafe2.getObject(t3, j4), zzbae2.zzdpn);
                                        r127 = r1225;
                                        r11 = r112;
                                        break;
                                    } else {
                                        zza3 = zzbae2.zzdpn;
                                        r127 = r1225;
                                        r11 = r112;
                                        break;
                                    }
                                }
                            default:
                                i11 = i22;
                                r11 = r19;
                                i14 = i29;
                                ? r1226 = r33;
                                break;
                        }
                    } else {
                        int i35 = i25;
                        i11 = i22;
                        int i36 = i28;
                        ? r113 = r19;
                        i9 = i29;
                        long j13 = j5;
                        if (i36 != 27) {
                            i12 = i23;
                            if (i36 <= 49) {
                                int i37 = i9;
                                unsafe = unsafe2;
                                ? r305 = r113;
                                i21 = zzbcy3.zza(t3, (byte[]) r129, i9, i18, (int) r113, i35, i26, zzcw, (long) i27, i36, j13, zzbae);
                                if (i21 == i37) {
                                    i5 = i21;
                                    r72 = r305;
                                    i4 = i3;
                                    zzbcy = this;
                                    if (r72 == i4 || i4 == 0) {
                                        zza = zza((int) r72, (byte[]) r33, i5, i2, (Object) t, zzbae);
                                        ? r1227 = r33;
                                        i18 = i2;
                                        zzbae2 = zzbae;
                                        i19 = i4;
                                        ? r15 = r72;
                                        zzbcy3 = zzbcy;
                                        i23 = i12;
                                        i22 = i11;
                                        unsafe2 = unsafe;
                                        i20 = -1;
                                        t3 = t;
                                        r12 = r122;
                                        r1 = i10;
                                        r129 = r12;
                                        r14 = r1;
                                    } else {
                                        i7 = i12;
                                        i8 = i11;
                                        i6 = -1;
                                        r7 = r72;
                                    }
                                } else {
                                    t3 = t;
                                    r12 = r33;
                                    i18 = i2;
                                    zzbae2 = zzbae;
                                    i23 = i12;
                                    i22 = i11;
                                    unsafe2 = unsafe;
                                    r1 = r305;
                                    i20 = -1;
                                    i19 = i3;
                                    zzbcy3 = this;
                                    r129 = r12;
                                    r14 = r1;
                                }
                            } else {
                                int i38 = zzcw;
                                int i39 = i26;
                                i13 = i9;
                                long j14 = j13;
                                unsafe = unsafe2;
                                ? r306 = r113;
                                int i40 = i35;
                                int i41 = i36;
                                if (i41 != 50) {
                                    zzbcy = this;
                                    zza = zzbcy.zza(t, (byte[]) r33, i13, i2, (int) r306, i40, i39, i27, i41, j14, i38, zzbae);
                                } else if (i39 == 2) {
                                    zzbcy = this;
                                    zza = zzbcy.zza(t, r33, i13, i2, i38, i40, j14, zzbae);
                                } else {
                                    zzbcy2 = this;
                                    r302 = r306;
                                    i5 = i13;
                                    r30 = r302;
                                    int i42 = r30;
                                    i4 = i3;
                                    r72 = i42;
                                    if (r72 == i4) {
                                    }
                                    zza = zza((int) r72, (byte[]) r33, i5, i2, (Object) t, zzbae);
                                    ? r12272 = r33;
                                    i18 = i2;
                                    zzbae2 = zzbae;
                                    i19 = i4;
                                    ? r152 = r72;
                                    zzbcy3 = zzbcy;
                                    i23 = i12;
                                    i22 = i11;
                                    unsafe2 = unsafe;
                                    i20 = -1;
                                    t3 = t;
                                    r12 = r122;
                                    r1 = i10;
                                    r129 = r12;
                                    r14 = r1;
                                }
                                i5 = zza;
                                r30 = r306;
                                int i422 = r30;
                                i4 = i3;
                                r72 = i422;
                                if (r72 == i4) {
                                }
                                zza = zza((int) r72, (byte[]) r33, i5, i2, (Object) t, zzbae);
                                ? r122722 = r33;
                                i18 = i2;
                                zzbae2 = zzbae;
                                i19 = i4;
                                ? r1522 = r72;
                                zzbcy3 = zzbcy;
                                i23 = i12;
                                i22 = i11;
                                unsafe2 = unsafe;
                                i20 = -1;
                                t3 = t;
                                r12 = r122;
                                r1 = i10;
                                r129 = r12;
                                r14 = r1;
                            }
                        } else if (i26 == 2) {
                            zzbbt zzbbt = (zzbbt) unsafe2.getObject(t3, j13);
                            if (!zzbbt.zzaay()) {
                                int size = zzbbt.size();
                                zzbbt = zzbbt.zzbm(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t3, j13, zzbbt);
                            }
                            zzbbt zzbbt2 = zzbbt;
                            zzbdm zzcq = zzbcy3.zzcq(zzcw);
                            i10 = r113;
                            int i43 = i23;
                            i21 = zza(zzcq, (int) i10, (byte[]) r129, i9, i18, zzbbt2, zzbae2);
                            i23 = i43;
                            r123 = r129;
                            r13 = i10;
                        } else {
                            i12 = i23;
                            unsafe = unsafe2;
                            r304 = r113;
                        }
                    }
                    i22 = i11;
                    i20 = -1;
                    i19 = i3;
                    r12 = r123;
                    r1 = r13;
                    r129 = r12;
                    r14 = r1;
                } else {
                    r304 = r5;
                    i12 = i23;
                    i11 = i22;
                    unsafe = unsafe2;
                }
                zzbcy2 = zzbcy3;
                i13 = i9;
                r302 = r303;
                i5 = i13;
                r30 = r302;
                int i4222 = r30;
                i4 = i3;
                r72 = i4222;
                if (r72 == i4) {
                }
                zza = zza((int) r72, (byte[]) r33, i5, i2, (Object) t, zzbae);
                ? r1227222 = r33;
                i18 = i2;
                zzbae2 = zzbae;
                i19 = i4;
                ? r15222 = r72;
                zzbcy3 = zzbcy;
                i23 = i12;
                i22 = i11;
                unsafe2 = unsafe;
                i20 = -1;
                t3 = t;
                r12 = r122;
                r1 = i10;
                r129 = r12;
                r14 = r1;
            } else {
                int i44 = i23;
                int i45 = i22;
                unsafe = unsafe2;
                i4 = i19;
                zzbcy = zzbcy3;
                i5 = i21;
                r7 = r14;
                i6 = i20;
                i7 = i44;
                i8 = i45;
            }
        }
        if (i8 != i6) {
            long j15 = (long) i8;
            t2 = t;
            unsafe.putInt(t2, j15, i7);
        } else {
            t2 = t;
        }
        if (zzbcy.zzdwr != null) {
            zzbef zzbef = null;
            for (int zza4 : zzbcy.zzdwr) {
                zzbef = (zzbef) zzbcy.zza((Object) t2, zza4, (UB) zzbef, zzbcy.zzdwv);
            }
            if (zzbef != null) {
                zzbcy.zzdwv.zzf(t2, zzbef);
            }
        }
        if (i4 == 0) {
            if (i5 != i2) {
                throw zzbbu.zzadr();
            }
        } else if (i5 > i2 || r7 != i4) {
            throw zzbbu.zzadr();
        }
        return i5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        r2 = java.lang.Long.valueOf(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004c, code lost:
        r2 = java.lang.Integer.valueOf(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0050, code lost:
        r6.zzdpn = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0052, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006d, code lost:
        r6.zzdpn = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        return r2 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007a, code lost:
        r6.zzdpn = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007e, code lost:
        return r2 + 8;
     */
    private static int zza(byte[] bArr, int i, int i2, zzbes zzbes, Class<?> cls, zzbae zzbae) throws IOException {
        int zzb;
        Object obj;
        Object obj2;
        int i3;
        int zza;
        long j;
        switch (zzbes) {
            case BOOL:
                zzb = zzbad.zzb(bArr, i, zzbae);
                Object valueOf = Boolean.valueOf(zzbae.zzdpm != 0);
                break;
            case BYTES:
                return zzbad.zze(bArr, i, zzbae);
            case DOUBLE:
                obj = Double.valueOf(zzbad.zzg(bArr, i));
                break;
            case FIXED32:
            case SFIXED32:
                obj2 = Integer.valueOf(zzbad.zze(bArr, i));
                break;
            case FIXED64:
            case SFIXED64:
                obj = Long.valueOf(zzbad.zzf(bArr, i));
                break;
            case FLOAT:
                obj2 = Float.valueOf(zzbad.zzh(bArr, i));
                break;
            case ENUM:
            case INT32:
            case UINT32:
                zza = zzbad.zza(bArr, i, zzbae);
                i3 = zzbae.zzdpl;
                break;
            case INT64:
            case UINT64:
                zzb = zzbad.zzb(bArr, i, zzbae);
                j = zzbae.zzdpm;
                break;
            case MESSAGE:
                return zza(zzbdg.zzaeo().zze(cls), bArr, i, i2, zzbae);
            case SINT32:
                zza = zzbad.zza(bArr, i, zzbae);
                i3 = zzbaq.zzbu(zzbae.zzdpl);
                break;
            case SINT64:
                zzb = zzbad.zzb(bArr, i, zzbae);
                j = zzbaq.zzl(zzbae.zzdpm);
                break;
            case STRING:
                return zzbad.zzd(bArr, i, zzbae);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    static <T> zzbcy<T> zza(Class<T> cls, zzbcs zzbcs, zzbdc zzbdc, zzbce zzbce, zzbee<?, ?> zzbee, zzbbd<?> zzbbd, zzbcp zzbcp) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        zzbcs zzbcs2 = zzbcs;
        if (zzbcs2 instanceof zzbdi) {
            zzbdi zzbdi = (zzbdi) zzbcs2;
            boolean z = zzbdi.zzaeh() == zze.zzduj;
            if (zzbdi.getFieldCount() == 0) {
                i3 = 0;
                i2 = 0;
                i = 0;
            } else {
                int zzaer = zzbdi.zzaer();
                int zzaes = zzbdi.zzaes();
                i3 = zzbdi.zzaew();
                i2 = zzaer;
                i = zzaes;
            }
            int[] iArr = new int[(i3 << 2)];
            Object[] objArr = new Object[(i3 << 1)];
            int[] iArr2 = zzbdi.zzaet() > 0 ? new int[zzbdi.zzaet()] : null;
            int[] iArr3 = zzbdi.zzaeu() > 0 ? new int[zzbdi.zzaeu()] : null;
            zzbdj zzaeq = zzbdi.zzaeq();
            if (zzaeq.next()) {
                int zzaci = zzaeq.zzaci();
                int i7 = 0;
                int i8 = 0;
                int i9 = 0;
                while (true) {
                    if (zzaci >= zzbdi.zzaex() || i7 >= ((zzaci - i2) << 2)) {
                        if (zzaeq.zzafb()) {
                            i6 = (int) zzbek.zza(zzaeq.zzafc());
                            i5 = (int) zzbek.zza(zzaeq.zzafd());
                            i4 = 0;
                        } else {
                            i6 = (int) zzbek.zza(zzaeq.zzafe());
                            if (zzaeq.zzaff()) {
                                i5 = (int) zzbek.zza(zzaeq.zzafg());
                                i4 = zzaeq.zzafh();
                            } else {
                                i5 = 0;
                                i4 = 0;
                            }
                        }
                        iArr[i7] = zzaeq.zzaci();
                        int i10 = i7 + 1;
                        iArr[i10] = (zzaeq.zzafj() ? ErrorDialogData.DYNAMITE_CRASH : 0) | (zzaeq.zzafi() ? ErrorDialogData.BINDER_CRASH : 0) | (zzaeq.zzaez() << 20) | i6;
                        iArr[i7 + 2] = i5 | (i4 << 20);
                        if (zzaeq.zzafm() != null) {
                            int i11 = (i7 / 4) << 1;
                            objArr[i11] = zzaeq.zzafm();
                            if (zzaeq.zzafk() != null) {
                                objArr[i11 + 1] = zzaeq.zzafk();
                            } else if (zzaeq.zzafl() != null) {
                                objArr[i11 + 1] = zzaeq.zzafl();
                            }
                        } else if (zzaeq.zzafk() != null) {
                            objArr[((i7 / 4) << 1) + 1] = zzaeq.zzafk();
                        } else if (zzaeq.zzafl() != null) {
                            objArr[((i7 / 4) << 1) + 1] = zzaeq.zzafl();
                        }
                        int zzaez = zzaeq.zzaez();
                        if (zzaez == zzbbj.MAP.ordinal()) {
                            int i12 = i8 + 1;
                            iArr2[i8] = i7;
                            i8 = i12;
                        } else if (zzaez >= 18 && zzaez <= 49) {
                            int i13 = i9 + 1;
                            iArr3[i9] = iArr[i10] & 1048575;
                            i9 = i13;
                        }
                        if (!zzaeq.next()) {
                            break;
                        }
                        zzaci = zzaeq.zzaci();
                    } else {
                        for (int i14 = 0; i14 < 4; i14++) {
                            iArr[i7 + i14] = -1;
                        }
                    }
                    i7 += 4;
                }
            }
            zzbcy zzbcy = new zzbcy(iArr, objArr, i2, i, zzbdi.zzaex(), zzbdi.zzaej(), z, false, zzbdi.zzaev(), iArr2, iArr3, zzbdc, zzbce, zzbee, zzbbd, zzbcp);
            return zzbcy;
        }
        ((zzbdz) zzbcs2).zzaeh();
        throw new NoSuchMethodError();
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzbbs<?> zzbbs, UB ub, zzbee<UT, UB> zzbee) {
        zzbcn zzx = this.zzdwx.zzx(zzcr(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (zzbbs.zzq(((Integer) entry.getValue()).intValue()) == null) {
                if (ub == null) {
                    ub = zzbee.zzagb();
                }
                zzbam zzbo = zzbah.zzbo(zzbcm.zza(zzx, entry.getKey(), entry.getValue()));
                try {
                    zzbcm.zza(zzbo.zzabj(), zzx, entry.getKey(), entry.getValue());
                    zzbee.zza(ub, i2, zzbo.zzabi());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzbee<UT, UB> zzbee) {
        int i2 = this.zzdwg[i];
        Object zzp = zzbek.zzp(obj, (long) (zzct(i) & 1048575));
        if (zzp == null) {
            return ub;
        }
        zzbbs zzcs = zzcs(i);
        if (zzcs == null) {
            return ub;
        }
        return zza(i, i2, this.zzdwx.zzs(zzp), zzcs, ub, zzbee);
    }

    private static void zza(int i, Object obj, zzbey zzbey) throws IOException {
        if (obj instanceof String) {
            zzbey.zzf(i, (String) obj);
        } else {
            zzbey.zza(i, (zzbah) obj);
        }
    }

    private static <UT, UB> void zza(zzbee<UT, UB> zzbee, T t, zzbey zzbey) throws IOException {
        zzbee.zza(zzbee.zzac(t), zzbey);
    }

    private final <K, V> void zza(zzbey zzbey, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzbey.zza(i, this.zzdwx.zzx(zzcr(i2)), this.zzdwx.zzt(obj));
        }
    }

    private final void zza(Object obj, int i, zzbdl zzbdl) throws IOException {
        long j;
        Object zzabs;
        if (zzcv(i)) {
            j = (long) (i & 1048575);
            zzabs = zzbdl.zzabr();
        } else if (this.zzdwn) {
            j = (long) (i & 1048575);
            zzabs = zzbdl.readString();
        } else {
            j = (long) (i & 1048575);
            zzabs = zzbdl.zzabs();
        }
        zzbek.zza(obj, j, zzabs);
    }

    private final void zza(T t, T t2, int i) {
        long zzct = (long) (zzct(i) & 1048575);
        if (zza(t2, i)) {
            Object zzp = zzbek.zzp(t, zzct);
            Object zzp2 = zzbek.zzp(t2, zzct);
            if (zzp == null || zzp2 == null) {
                if (zzp2 != null) {
                    zzbek.zza((Object) t, zzct, zzp2);
                    zzb(t, i);
                }
                return;
            }
            zzbek.zza((Object) t, zzct, zzbbq.zza(zzp, zzp2));
            zzb(t, i);
        }
    }

    private final boolean zza(T t, int i) {
        if (this.zzdwo) {
            int zzct = zzct(i);
            long j = (long) (zzct & 1048575);
            switch ((zzct & 267386880) >>> 20) {
                case 0:
                    return zzbek.zzo(t, j) != Utils.DOUBLE_EPSILON;
                case 1:
                    return zzbek.zzn(t, j) != 0.0f;
                case 2:
                    return zzbek.zzl(t, j) != 0;
                case 3:
                    return zzbek.zzl(t, j) != 0;
                case 4:
                    return zzbek.zzk(t, j) != 0;
                case 5:
                    return zzbek.zzl(t, j) != 0;
                case 6:
                    return zzbek.zzk(t, j) != 0;
                case 7:
                    return zzbek.zzm(t, j);
                case 8:
                    Object zzp = zzbek.zzp(t, j);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzbah) {
                        return !zzbah.zzdpq.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzbek.zzp(t, j) != null;
                case 10:
                    return !zzbah.zzdpq.equals(zzbek.zzp(t, j));
                case 11:
                    return zzbek.zzk(t, j) != 0;
                case 12:
                    return zzbek.zzk(t, j) != 0;
                case 13:
                    return zzbek.zzk(t, j) != 0;
                case 14:
                    return zzbek.zzl(t, j) != 0;
                case 15:
                    return zzbek.zzk(t, j) != 0;
                case 16:
                    return zzbek.zzl(t, j) != 0;
                case 17:
                    return zzbek.zzp(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zzcu = zzcu(i);
            return (zzbek.zzk(t, (long) (zzcu & 1048575)) & (1 << (zzcu >>> 20))) != 0;
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzbek.zzk(t, (long) (zzcu(i2) & 1048575)) == i;
    }

    private final boolean zza(T t, int i, int i2, int i3) {
        return this.zzdwo ? zza(t, i) : (i2 & i3) != 0;
    }

    private static boolean zza(Object obj, int i, zzbdm zzbdm) {
        return zzbdm.zzaa(zzbek.zzp(obj, (long) (i & 1048575)));
    }

    private final void zzb(T t, int i) {
        if (!this.zzdwo) {
            int zzcu = zzcu(i);
            long j = (long) (zzcu & 1048575);
            zzbek.zzb((Object) t, j, zzbek.zzk(t, j) | (1 << (zzcu >>> 20)));
        }
    }

    private final void zzb(T t, int i, int i2) {
        zzbek.zzb((Object) t, (long) (zzcu(i2) & 1048575), i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0344, code lost:
        r14 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x051d, code lost:
        r5 = r12 + 4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0523  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    private final void zzb(T t, zzbey zzbey) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z;
        T t2 = t;
        zzbey zzbey2 = zzbey;
        if (this.zzdwm) {
            zzbbg zzm = this.zzdww.zzm(t2);
            if (!zzm.isEmpty()) {
                it = zzm.iterator();
                entry = (Entry) it.next();
                int i6 = -1;
                length = this.zzdwg.length;
                Unsafe unsafe = zzdwf;
                entry2 = entry;
                i = 0;
                int i7 = 0;
                while (i < length) {
                    int zzct = zzct(i);
                    int i8 = this.zzdwg[i];
                    int i9 = (267386880 & zzct) >>> 20;
                    if (this.zzdwo || i9 > 17) {
                        i2 = i;
                        i3 = 0;
                    } else {
                        int i10 = this.zzdwg[i + 2];
                        int i11 = i10 & 1048575;
                        if (i11 != i6) {
                            i2 = i;
                            i7 = unsafe.getInt(t2, (long) i11);
                            i6 = i11;
                        } else {
                            i2 = i;
                        }
                        i3 = 1 << (i10 >>> 20);
                    }
                    while (entry2 != null && this.zzdww.zza(entry2) <= i8) {
                        this.zzdww.zza(zzbey2, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    long j = (long) (zzct & 1048575);
                    switch (i9) {
                        case 0:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zza(i8, zzbek.zzo(t2, j));
                                break;
                            }
                        case 1:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zza(i8, zzbek.zzn(t2, j));
                                break;
                            }
                        case 2:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzi(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 3:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zza(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 4:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzm(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 5:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzc(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 6:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzp(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 7:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzf(i8, zzbek.zzm(t2, j));
                                break;
                            }
                        case 8:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zza(i8, unsafe.getObject(t2, j), zzbey2);
                                break;
                            }
                        case 9:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zza(i8, unsafe.getObject(t2, j), zzcq(i4));
                                break;
                            }
                        case 10:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zza(i8, (zzbah) unsafe.getObject(t2, j));
                                break;
                            }
                        case 11:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzn(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 12:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzx(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 13:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzw(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 14:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzj(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 15:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzo(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 16:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzb(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 17:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbey2.zzb(i8, unsafe.getObject(t2, j), zzcq(i4));
                                break;
                            }
                        case 18:
                            i4 = i2;
                            zzbdo.zza(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 19:
                            i4 = i2;
                            zzbdo.zzb(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 20:
                            i4 = i2;
                            zzbdo.zzc(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 21:
                            i4 = i2;
                            zzbdo.zzd(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 22:
                            i4 = i2;
                            zzbdo.zzh(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 23:
                            i4 = i2;
                            zzbdo.zzf(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 24:
                            i4 = i2;
                            zzbdo.zzk(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 25:
                            i4 = i2;
                            zzbdo.zzn(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 26:
                            i5 = i2;
                            zzbdo.zza(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2);
                            break;
                        case 27:
                            i5 = i2;
                            zzbdo.zza(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, zzcq(i5));
                            break;
                        case 28:
                            i5 = i2;
                            zzbdo.zzb(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2);
                            break;
                        case 29:
                            i4 = i2;
                            z = false;
                            zzbdo.zzi(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 30:
                            i4 = i2;
                            z = false;
                            zzbdo.zzm(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 31:
                            i4 = i2;
                            z = false;
                            zzbdo.zzl(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 32:
                            i4 = i2;
                            z = false;
                            zzbdo.zzg(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 33:
                            i4 = i2;
                            z = false;
                            zzbdo.zzj(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 34:
                            i4 = i2;
                            z = false;
                            zzbdo.zze(this.zzdwg[i4], (List) unsafe.getObject(t2, j), zzbey2, false);
                            break;
                        case 35:
                            i5 = i2;
                            zzbdo.zza(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 36:
                            i5 = i2;
                            zzbdo.zzb(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 37:
                            i5 = i2;
                            zzbdo.zzc(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 38:
                            i5 = i2;
                            zzbdo.zzd(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 39:
                            i5 = i2;
                            zzbdo.zzh(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 40:
                            i5 = i2;
                            zzbdo.zzf(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 41:
                            i5 = i2;
                            zzbdo.zzk(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 42:
                            i5 = i2;
                            zzbdo.zzn(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 43:
                            i5 = i2;
                            zzbdo.zzi(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 44:
                            i5 = i2;
                            zzbdo.zzm(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 45:
                            i5 = i2;
                            zzbdo.zzl(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 46:
                            i5 = i2;
                            zzbdo.zzg(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 47:
                            i5 = i2;
                            zzbdo.zzj(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 48:
                            i5 = i2;
                            zzbdo.zze(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, true);
                            break;
                        case 49:
                            i5 = i2;
                            zzbdo.zzb(this.zzdwg[i5], (List) unsafe.getObject(t2, j), zzbey2, zzcq(i5));
                            break;
                        case 50:
                            i5 = i2;
                            zza(zzbey2, i8, unsafe.getObject(t2, j), i5);
                            break;
                        case 51:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zza(i8, zzf(t2, j));
                                break;
                            }
                            break;
                        case 52:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zza(i8, zzg(t2, j));
                                break;
                            }
                            break;
                        case 53:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzi(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 54:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zza(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 55:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzm(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 56:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzc(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 57:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzp(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 58:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzf(i8, zzj(t2, j));
                                break;
                            }
                            break;
                        case 59:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zza(i8, unsafe.getObject(t2, j), zzbey2);
                                break;
                            }
                            break;
                        case 60:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zza(i8, unsafe.getObject(t2, j), zzcq(i5));
                                break;
                            }
                            break;
                        case 61:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zza(i8, (zzbah) unsafe.getObject(t2, j));
                                break;
                            }
                            break;
                        case 62:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzn(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 63:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzx(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 64:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzw(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 65:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzj(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 66:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzo(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 67:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzb(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 68:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbey2.zzb(i8, unsafe.getObject(t2, j), zzcq(i5));
                                break;
                            }
                            break;
                        default:
                            i5 = i2;
                            break;
                    }
                }
                while (entry2 != null) {
                    this.zzdww.zza(zzbey2, entry2);
                    entry2 = it.hasNext() ? (Entry) it.next() : null;
                }
                zza(this.zzdwv, t2, zzbey2);
            }
        }
        it = null;
        entry = null;
        int i62 = -1;
        length = this.zzdwg.length;
        Unsafe unsafe2 = zzdwf;
        entry2 = entry;
        i = 0;
        int i72 = 0;
        while (i < length) {
        }
        while (entry2 != null) {
        }
        zza(this.zzdwv, t2, zzbey2);
    }

    private final void zzb(T t, T t2, int i) {
        int zzct = zzct(i);
        int i2 = this.zzdwg[i];
        long j = (long) (zzct & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzbek.zzp(t, j);
            Object zzp2 = zzbek.zzp(t2, j);
            if (zzp == null || zzp2 == null) {
                if (zzp2 != null) {
                    zzbek.zza((Object) t, j, zzp2);
                    zzb(t, i2, i);
                }
                return;
            }
            zzbek.zza((Object) t, j, zzbbq.zza(zzp, zzp2));
            zzb(t, i2, i);
        }
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    private final zzbdm zzcq(int i) {
        int i2 = (i / 4) << 1;
        zzbdm zzbdm = (zzbdm) this.zzdwh[i2];
        if (zzbdm != null) {
            return zzbdm;
        }
        zzbdm zze = zzbdg.zzaeo().zze((Class) this.zzdwh[i2 + 1]);
        this.zzdwh[i2] = zze;
        return zze;
    }

    private final Object zzcr(int i) {
        return this.zzdwh[(i / 4) << 1];
    }

    private final zzbbs<?> zzcs(int i) {
        return (zzbbs) this.zzdwh[((i / 4) << 1) + 1];
    }

    private final int zzct(int i) {
        return this.zzdwg[i + 1];
    }

    private final int zzcu(int i) {
        return this.zzdwg[i + 2];
    }

    private static boolean zzcv(int i) {
        return (i & ErrorDialogData.DYNAMITE_CRASH) != 0;
    }

    private final int zzcw(int i) {
        if (i >= this.zzdwi) {
            if (i < this.zzdwk) {
                int i2 = (i - this.zzdwi) << 2;
                if (this.zzdwg[i2] == i) {
                    return i2;
                }
                return -1;
            } else if (i <= this.zzdwj) {
                int i3 = this.zzdwk - this.zzdwi;
                int length = (this.zzdwg.length / 4) - 1;
                while (i3 <= length) {
                    int i4 = (length + i3) >>> 1;
                    int i5 = i4 << 2;
                    int i6 = this.zzdwg[i5];
                    if (i == i6) {
                        return i5;
                    }
                    if (i < i6) {
                        length = i4 - 1;
                    } else {
                        i3 = i4 + 1;
                    }
                }
            }
        }
        return -1;
    }

    private static <E> List<E> zze(Object obj, long j) {
        return (List) zzbek.zzp(obj, j);
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzbek.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzbek.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzbek.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzbek.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzbek.zzp(t, j)).booleanValue();
    }

    private static zzbef zzz(Object obj) {
        zzbbo zzbbo = (zzbbo) obj;
        zzbef zzbef = zzbbo.zzdtt;
        if (zzbef != zzbef.zzagc()) {
            return zzbef;
        }
        zzbef zzagd = zzbef.zzagd();
        zzbbo.zzdtt = zzagd;
        return zzagd;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005c, code lost:
        if (com.google.android.gms.internal.ads.zzbdo.zzd(com.google.android.gms.internal.ads.zzbek.zzp(r10, r6), com.google.android.gms.internal.ads.zzbek.zzp(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0070, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzl(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0082, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0096, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzl(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a8, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ba, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cc, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e2, code lost:
        if (com.google.android.gms.internal.ads.zzbdo.zzd(com.google.android.gms.internal.ads.zzbek.zzp(r10, r6), com.google.android.gms.internal.ads.zzbek.zzp(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f8, code lost:
        if (com.google.android.gms.internal.ads.zzbdo.zzd(com.google.android.gms.internal.ads.zzbek.zzp(r10, r6), com.google.android.gms.internal.ads.zzbek.zzp(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x010e, code lost:
        if (com.google.android.gms.internal.ads.zzbdo.zzd(com.google.android.gms.internal.ads.zzbek.zzp(r10, r6), com.google.android.gms.internal.ads.zzbek.zzp(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0120, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzm(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzm(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0132, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0145, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzl(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0156, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0169, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzl(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x017c, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzl(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x018d, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzk(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01a0, code lost:
        if (com.google.android.gms.internal.ads.zzbek.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbek.zzl(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01a2, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.ads.zzbdo.zzd(com.google.android.gms.internal.ads.zzbek.zzp(r10, r6), com.google.android.gms.internal.ads.zzbek.zzp(r11, r6)) != false) goto L_0x01a3;
     */
    public final boolean equals(T t, T t2) {
        int length = this.zzdwg.length;
        int i = 0;
        while (true) {
            boolean z = true;
            if (i < length) {
                int zzct = zzct(i);
                long j = (long) (zzct & 1048575);
                switch ((zzct & 267386880) >>> 20) {
                    case 0:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 1:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 2:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 3:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 4:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 5:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 6:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 7:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 8:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 9:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 10:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 11:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 12:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 13:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 14:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 15:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 16:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 17:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                    case 50:
                        z = zzbdo.zzd(zzbek.zzp(t, j), zzbek.zzp(t2, j));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long zzcu = (long) (zzcu(i) & 1048575);
                        if (zzbek.zzk(t, zzcu) == zzbek.zzk(t2, zzcu)) {
                            break;
                        }
                }
                if (!z) {
                    return false;
                }
                i += 4;
            } else if (!this.zzdwv.zzac(t).equals(this.zzdwv.zzac(t2))) {
                return false;
            } else {
                if (this.zzdwm) {
                    return this.zzdww.zzm(t).equals(this.zzdww.zzm(t2));
                }
                return true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0061, code lost:
        r3 = com.google.android.gms.internal.ads.zzbek.zzp(r9, r5);
        r2 = r2 * 53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0093, code lost:
        r2 = r2 * 53;
        r3 = zzh(r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a8, code lost:
        r2 = r2 * 53;
        r3 = zzi(r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ce, code lost:
        if (r3 != null) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d1, code lost:
        r2 = r2 * 53;
        r3 = com.google.android.gms.internal.ads.zzbek.zzp(r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00d7, code lost:
        r3 = r3.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e0, code lost:
        if (r3 != null) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e2, code lost:
        r7 = r3.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e6, code lost:
        r2 = (r2 * 53) + r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ea, code lost:
        r2 = r2 * 53;
        r3 = ((java.lang.String) com.google.android.gms.internal.ads.zzbek.zzp(r9, r5)).hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00fd, code lost:
        r3 = com.google.android.gms.internal.ads.zzbbq.zzar(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0116, code lost:
        r3 = java.lang.Float.floatToIntBits(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0121, code lost:
        r3 = java.lang.Double.doubleToLongBits(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0125, code lost:
        r3 = com.google.android.gms.internal.ads.zzbbq.zzv(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0129, code lost:
        r2 = r2 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x012a, code lost:
        r1 = r1 + 4;
     */
    public final int hashCode(T t) {
        double d;
        int i;
        float f;
        int i2;
        boolean z;
        int i3;
        Object obj;
        int i4 = 0;
        int length = this.zzdwg.length;
        int i5 = 0;
        while (i4 < length) {
            int zzct = zzct(i4);
            int i6 = this.zzdwg[i4];
            long j = (long) (1048575 & zzct);
            int i7 = 37;
            switch ((zzct & 267386880) >>> 20) {
                case 0:
                    i = i5 * 53;
                    d = zzbek.zzo(t, j);
                    break;
                case 1:
                    i2 = i5 * 53;
                    f = zzbek.zzn(t, j);
                    break;
                case 2:
                case 3:
                case 5:
                case 14:
                case 16:
                    i = i5 * 53;
                    long j2 = zzbek.zzl(t, j);
                    break;
                case 4:
                case 6:
                case 11:
                case 12:
                case 13:
                case 15:
                    int i8 = i5 * 53;
                    int i9 = zzbek.zzk(t, j);
                    break;
                case 7:
                    i3 = i5 * 53;
                    z = zzbek.zzm(t, j);
                    break;
                case 8:
                    break;
                case 9:
                    obj = zzbek.zzp(t, j);
                    break;
                case 10:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                    break;
                case 17:
                    obj = zzbek.zzp(t, j);
                    break;
                case 51:
                    if (!zza(t, i6, i4)) {
                        break;
                    } else {
                        i = i5 * 53;
                        d = zzf(t, j);
                        break;
                    }
                case 52:
                    if (!zza(t, i6, i4)) {
                        break;
                    } else {
                        i2 = i5 * 53;
                        f = zzg(t, j);
                        break;
                    }
                case 53:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 54:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 55:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 56:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 57:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 58:
                    if (!zza(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        z = zzj(t, j);
                        break;
                    }
                case 59:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 60:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 61:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 62:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 63:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 64:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 65:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 66:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 67:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
                case 68:
                    if (!zza(t, i6, i4)) {
                        break;
                    }
                    break;
            }
        }
        int hashCode = (i5 * 53) + this.zzdwv.zzac(t).hashCode();
        return this.zzdwm ? (hashCode * 53) + this.zzdww.zzm(t).hashCode() : hashCode;
    }

    public final T newInstance() {
        return this.zzdwt.newInstance(this.zzdwl);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:174|175|(1:177)|178|(5:198|180|(3:182|(1:184)|232)|(1:186)|187)(1:228)) */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02a8, code lost:
        r10.zzu(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x02b6, code lost:
        r10.zzt(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02c4, code lost:
        r10.zzr(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02d2, code lost:
        r10.zzs(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x02e0, code lost:
        r10.zzq(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x02ee, code lost:
        r10.zzp(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x032e, code lost:
        r4 = com.google.android.gms.internal.ads.zzbdo.zza(r4, r6, r5, r15, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0332, code lost:
        r15 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x03f3, code lost:
        com.google.android.gms.internal.ads.zzbek.zza((java.lang.Object) r2, r6, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0406, code lost:
        zzb(r2, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:?, code lost:
        r12.zza(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0518, code lost:
        if (r15 == null) goto L_0x051a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x051a, code lost:
        r15 = r12.zzad(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0523, code lost:
        if (r12.zza(r15, r10) == false) goto L_0x0525;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0527, code lost:
        if (r1.zzdwr != null) goto L_0x0529;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0529, code lost:
        r3 = r1.zzdwr;
        r4 = r3.length;
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x052e, code lost:
        if (r5 < r4) goto L_0x0530;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0530, code lost:
        r15 = zza((java.lang.Object) r2, r3[r5], (UB) r15, r12);
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x0539, code lost:
        if (r15 != null) goto L_0x053b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x053b, code lost:
        r12.zzf(r2, r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x053e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b6, code lost:
        zzb(r2, r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0104, code lost:
        r4 = com.google.android.gms.internal.ads.zzbdo.zza(r4, r7, r15, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0235, code lost:
        r10.zzae(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0243, code lost:
        r10.zzad(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0251, code lost:
        r10.zzac(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x025f, code lost:
        r10.zzab(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x027e, code lost:
        r10.zzz(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x028c, code lost:
        r10.zzw(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x029a, code lost:
        r10.zzv(r4);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:174:0x0515 */
    public final void zza(T t, zzbdl zzbdl, zzbbb zzbbb) throws IOException {
        long j;
        Object zza;
        int zzabu;
        List zza2;
        List zza3;
        List zza4;
        List zza5;
        List zza6;
        List zza7;
        List zza8;
        List zza9;
        List zza10;
        List list;
        zzbbs zzbbs;
        List zza11;
        List zza12;
        List zza13;
        List zza14;
        T t2 = t;
        zzbdl zzbdl2 = zzbdl;
        zzbbb zzbbb2 = zzbbb;
        if (zzbbb2 == null) {
            throw new NullPointerException();
        }
        zzbee<?, ?> zzbee = this.zzdwv;
        zzbbd<?> zzbbd = this.zzdww;
        zzbbg zzbbg = null;
        Object obj = null;
        while (true) {
            try {
                int zzaci = zzbdl.zzaci();
                int zzcw = zzcw(zzaci);
                if (zzcw >= 0) {
                    int zzct = zzct(zzcw);
                    switch ((267386880 & zzct) >>> 20) {
                        case 0:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.readDouble());
                            break;
                        case 1:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.readFloat());
                            break;
                        case 2:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.zzabm());
                            break;
                        case 3:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.zzabl());
                            break;
                        case 4:
                            zzbek.zzb((Object) t2, (long) (zzct & 1048575), zzbdl.zzabn());
                            break;
                        case 5:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.zzabo());
                            break;
                        case 6:
                            zzbek.zzb((Object) t2, (long) (zzct & 1048575), zzbdl.zzabp());
                            break;
                        case 7:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.zzabq());
                            break;
                        case 8:
                            zza((Object) t2, zzct, zzbdl2);
                            break;
                        case 9:
                            if (!zza(t2, zzcw)) {
                                zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl2.zza(zzcq(zzcw), zzbbb2));
                                break;
                            } else {
                                j = (long) (zzct & 1048575);
                                zza = zzbbq.zza(zzbek.zzp(t2, j), zzbdl2.zza(zzcq(zzcw), zzbbb2));
                                break;
                            }
                        case 10:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) zzbdl.zzabs());
                            break;
                        case 11:
                            zzbek.zzb((Object) t2, (long) (zzct & 1048575), zzbdl.zzabt());
                            break;
                        case 12:
                            zzabu = zzbdl.zzabu();
                            zzbbs zzcs = zzcs(zzcw);
                            if (zzcs == null || zzcs.zzq(zzabu) != null) {
                                zzbek.zzb((Object) t2, (long) (zzct & 1048575), zzabu);
                                break;
                            }
                        case 13:
                            zzbek.zzb((Object) t2, (long) (zzct & 1048575), zzbdl.zzabv());
                            break;
                        case 14:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.zzabw());
                            break;
                        case 15:
                            zzbek.zzb((Object) t2, (long) (zzct & 1048575), zzbdl.zzabx());
                            break;
                        case 16:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl.zzaby());
                            break;
                        case 17:
                            if (!zza(t2, zzcw)) {
                                zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl2.zzb(zzcq(zzcw), zzbbb2));
                                break;
                            } else {
                                j = (long) (zzct & 1048575);
                                zza = zzbbq.zza(zzbek.zzp(t2, j), zzbdl2.zzb(zzcq(zzcw), zzbbb2));
                                break;
                            }
                        case 18:
                            zza2 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 19:
                            zza3 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 20:
                            zza4 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 21:
                            zza5 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 22:
                            zza6 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 23:
                            zza7 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 24:
                            zza8 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 25:
                            zza9 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 26:
                            if (!zzcv(zzct)) {
                                zzbdl2.readStringList(this.zzdwu.zza(t2, (long) (zzct & 1048575)));
                                break;
                            } else {
                                zzbdl2.zzx(this.zzdwu.zza(t2, (long) (zzct & 1048575)));
                                break;
                            }
                        case 27:
                            zzbdl2.zza(this.zzdwu.zza(t2, (long) (zzct & 1048575)), zzcq(zzcw), zzbbb2);
                            break;
                        case 28:
                            zzbdl2.zzy(this.zzdwu.zza(t2, (long) (zzct & 1048575)));
                            break;
                        case 29:
                            zza10 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 30:
                            list = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            zzbdl2.zzaa(list);
                            zzbbs = zzcs(zzcw);
                            break;
                        case 31:
                            zza11 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 32:
                            zza12 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 33:
                            zza13 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 34:
                            zza14 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 35:
                            zza2 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 36:
                            zza3 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 37:
                            zza4 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 38:
                            zza5 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 39:
                            zza6 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 40:
                            zza7 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 41:
                            zza8 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 42:
                            zza9 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 43:
                            zza10 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 44:
                            list = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            zzbdl2.zzaa(list);
                            zzbbs = zzcs(zzcw);
                            break;
                        case 45:
                            zza11 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 46:
                            zza12 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 47:
                            zza13 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 48:
                            zza14 = this.zzdwu.zza(t2, (long) (zzct & 1048575));
                            break;
                        case 49:
                            zzbdl2.zzb(this.zzdwu.zza(t2, (long) (zzct & 1048575)), zzcq(zzcw), zzbbb2);
                            break;
                        case 50:
                            Object zzcr = zzcr(zzcw);
                            long zzct2 = (long) (zzct(zzcw) & 1048575);
                            Object zzp = zzbek.zzp(t2, zzct2);
                            if (zzp == null) {
                                zzp = this.zzdwx.zzw(zzcr);
                                zzbek.zza((Object) t2, zzct2, zzp);
                            } else if (this.zzdwx.zzu(zzp)) {
                                Object zzw = this.zzdwx.zzw(zzcr);
                                this.zzdwx.zzb(zzw, zzp);
                                zzbek.zza((Object) t2, zzct2, zzw);
                                zzp = zzw;
                            }
                            zzbdl2.zza(this.zzdwx.zzs(zzp), this.zzdwx.zzx(zzcr), zzbbb2);
                            break;
                        case 51:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Double.valueOf(zzbdl.readDouble()));
                            break;
                        case 52:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Float.valueOf(zzbdl.readFloat()));
                            break;
                        case 53:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Long.valueOf(zzbdl.zzabm()));
                            break;
                        case 54:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Long.valueOf(zzbdl.zzabl()));
                            break;
                        case 55:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Integer.valueOf(zzbdl.zzabn()));
                            break;
                        case 56:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Long.valueOf(zzbdl.zzabo()));
                            break;
                        case 57:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Integer.valueOf(zzbdl.zzabp()));
                            break;
                        case 58:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Boolean.valueOf(zzbdl.zzabq()));
                            break;
                        case 59:
                            zza((Object) t2, zzct, zzbdl2);
                            break;
                        case 60:
                            if (!zza(t2, zzaci, zzcw)) {
                                zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl2.zza(zzcq(zzcw), zzbbb2));
                                zzb(t2, zzcw);
                                break;
                            } else {
                                long j2 = (long) (zzct & 1048575);
                                zzbek.zza((Object) t2, j2, zzbbq.zza(zzbek.zzp(t2, j2), zzbdl2.zza(zzcq(zzcw), zzbbb2)));
                                break;
                            }
                        case 61:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) zzbdl.zzabs());
                            break;
                        case 62:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Integer.valueOf(zzbdl.zzabt()));
                            break;
                        case 63:
                            zzabu = zzbdl.zzabu();
                            zzbbs zzcs2 = zzcs(zzcw);
                            if (zzcs2 != null) {
                                if (zzcs2.zzq(zzabu) != null) {
                                }
                            }
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Integer.valueOf(zzabu));
                            break;
                        case 64:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Integer.valueOf(zzbdl.zzabv()));
                            break;
                        case 65:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Long.valueOf(zzbdl.zzabw()));
                            break;
                        case 66:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Integer.valueOf(zzbdl.zzabx()));
                            break;
                        case 67:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), (Object) Long.valueOf(zzbdl.zzaby()));
                            break;
                        case 68:
                            zzbek.zza((Object) t2, (long) (zzct & 1048575), zzbdl2.zzb(zzcq(zzcw), zzbbb2));
                            break;
                        default:
                            if (obj == null) {
                                obj = zzbee.zzagb();
                                break;
                            }
                            if (zzbee.zza(obj, zzbdl2)) {
                                break;
                            } else {
                                if (this.zzdwr != null) {
                                    for (int zza15 : this.zzdwr) {
                                        obj = zza((Object) t2, zza15, (UB) obj, zzbee);
                                    }
                                }
                                if (obj != null) {
                                    zzbee.zzf(t2, obj);
                                }
                                return;
                            }
                    }
                } else if (zzaci == Integer.MAX_VALUE) {
                    if (this.zzdwr != null) {
                        for (int zza16 : this.zzdwr) {
                            obj = zza((Object) t2, zza16, (UB) obj, zzbee);
                        }
                    }
                    if (obj != null) {
                        zzbee.zzf(t2, obj);
                    }
                    return;
                } else {
                    Object zza17 = !this.zzdwm ? null : zzbbd.zza(zzbbb2, this.zzdwl, zzaci);
                    if (zza17 != null) {
                        if (zzbbg == null) {
                            zzbbg = zzbbd.zzn(t2);
                        }
                        zzbbg zzbbg2 = zzbbg;
                        obj = zzbbd.zza(zzbdl2, zza17, zzbbb2, zzbbg2, obj, zzbee);
                        zzbbg = zzbbg2;
                    } else {
                        zzbee.zza(zzbdl2);
                        if (obj == null) {
                            obj = zzbee.zzad(t2);
                        }
                        if (!zzbee.zza(obj, zzbdl2)) {
                            if (this.zzdwr != null) {
                                for (int zza18 : this.zzdwr) {
                                    obj = zza((Object) t2, zza18, (UB) obj, zzbee);
                                }
                            }
                            if (obj != null) {
                                zzbee.zzf(t2, obj);
                            }
                            return;
                        }
                    }
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                if (this.zzdwr != null) {
                    for (int zza19 : this.zzdwr) {
                        obj = zza((Object) t2, zza19, (UB) obj, zzbee);
                    }
                }
                if (obj != null) {
                    zzbee.zzf(t2, obj);
                }
                throw th2;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0385, code lost:
        r15.zzb(r9, com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r8 & 1048575)), zzcq(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x03a0, code lost:
        r15.zzb(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x03b1, code lost:
        r15.zzo(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x03c2, code lost:
        r15.zzj(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x03d3, code lost:
        r15.zzw(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x03e4, code lost:
        r15.zzx(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x03f5, code lost:
        r15.zzn(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0400, code lost:
        r15.zza(r9, (com.google.android.gms.internal.ads.zzbah) com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r8 & 1048575)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0413, code lost:
        r15.zza(r9, com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r8 & 1048575)), zzcq(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0428, code lost:
        zza(r9, com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r8 & 1048575)), r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x043f, code lost:
        r15.zzf(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0450, code lost:
        r15.zzp(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0460, code lost:
        r15.zzc(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0470, code lost:
        r15.zzm(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0480, code lost:
        r15.zza(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0490, code lost:
        r15.zzi(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x04a0, code lost:
        r15.zza(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x04b0, code lost:
        r15.zza(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x0843, code lost:
        r15.zzb(r10, com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r9 & 1048575)), zzcq(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x085e, code lost:
        r15.zzb(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x086f, code lost:
        r15.zzo(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x0880, code lost:
        r15.zzj(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x0891, code lost:
        r15.zzw(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x08a2, code lost:
        r15.zzx(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x08b3, code lost:
        r15.zzn(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x08be, code lost:
        r15.zza(r10, (com.google.android.gms.internal.ads.zzbah) com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r9 & 1048575)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x08d1, code lost:
        r15.zza(r10, com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r9 & 1048575)), zzcq(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x08e6, code lost:
        zza(r10, com.google.android.gms.internal.ads.zzbek.zzp(r14, (long) (r9 & 1048575)), r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x08fd, code lost:
        r15.zzf(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:324:0x090e, code lost:
        r15.zzp(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x091e, code lost:
        r15.zzc(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:332:0x092e, code lost:
        r15.zzm(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x093e, code lost:
        r15.zza(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x094e, code lost:
        r15.zzi(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:344:0x095e, code lost:
        r15.zza(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x096e, code lost:
        r15.zza(r10, r11);
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x04b9  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x04f7  */
    /* JADX WARNING: Removed duplicated region for block: B:351:0x0977  */
    public final void zza(T t, zzbey zzbey) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        double d;
        float f;
        long j;
        long j2;
        int i2;
        long j3;
        int i3;
        boolean z;
        int i4;
        int i5;
        int i6;
        long j4;
        int i7;
        long j5;
        Entry entry3;
        Iterator it2;
        int length2;
        double d2;
        float f2;
        long j6;
        long j7;
        int i8;
        long j8;
        int i9;
        boolean z2;
        int i10;
        int i11;
        int i12;
        long j9;
        int i13;
        long j10;
        if (zzbey.zzacn() == zze.zzdum) {
            zza(this.zzdwv, t, zzbey);
            if (this.zzdwm) {
                zzbbg zzm = this.zzdww.zzm(t);
                if (!zzm.isEmpty()) {
                    it2 = zzm.descendingIterator();
                    entry3 = (Entry) it2.next();
                    length2 = this.zzdwg.length - 4;
                    while (length2 >= 0) {
                        int zzct = zzct(length2);
                        int i14 = this.zzdwg[length2];
                        while (entry3 != null && this.zzdww.zza(entry3) > i14) {
                            this.zzdww.zza(zzbey, entry3);
                            entry3 = it2.hasNext() ? (Entry) it2.next() : null;
                        }
                        switch ((zzct & 267386880) >>> 20) {
                            case 0:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    d2 = zzbek.zzo(t, (long) (zzct & 1048575));
                                }
                            case 1:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    f2 = zzbek.zzn(t, (long) (zzct & 1048575));
                                }
                            case 2:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    j6 = zzbek.zzl(t, (long) (zzct & 1048575));
                                }
                            case 3:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    j7 = zzbek.zzl(t, (long) (zzct & 1048575));
                                }
                            case 4:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    i8 = zzbek.zzk(t, (long) (zzct & 1048575));
                                }
                            case 5:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    j8 = zzbek.zzl(t, (long) (zzct & 1048575));
                                }
                            case 6:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    i9 = zzbek.zzk(t, (long) (zzct & 1048575));
                                }
                            case 7:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    z2 = zzbek.zzm(t, (long) (zzct & 1048575));
                                }
                            case 8:
                                if (!zza(t, length2)) {
                                    break;
                                }
                            case 9:
                                if (!zza(t, length2)) {
                                    break;
                                }
                            case 10:
                                if (!zza(t, length2)) {
                                    break;
                                }
                            case 11:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    i10 = zzbek.zzk(t, (long) (zzct & 1048575));
                                }
                            case 12:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    i11 = zzbek.zzk(t, (long) (zzct & 1048575));
                                }
                            case 13:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    i12 = zzbek.zzk(t, (long) (zzct & 1048575));
                                }
                            case 14:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    j9 = zzbek.zzl(t, (long) (zzct & 1048575));
                                }
                            case 15:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    i13 = zzbek.zzk(t, (long) (zzct & 1048575));
                                }
                            case 16:
                                if (!zza(t, length2)) {
                                    break;
                                } else {
                                    j10 = zzbek.zzl(t, (long) (zzct & 1048575));
                                }
                            case 17:
                                if (!zza(t, length2)) {
                                    break;
                                }
                            case 18:
                                zzbdo.zza(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 19:
                                zzbdo.zzb(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 20:
                                zzbdo.zzc(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 21:
                                zzbdo.zzd(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 22:
                                zzbdo.zzh(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 23:
                                zzbdo.zzf(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 24:
                                zzbdo.zzk(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 25:
                                zzbdo.zzn(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 26:
                                zzbdo.zza(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey);
                                break;
                            case 27:
                                zzbdo.zza(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, zzcq(length2));
                                break;
                            case 28:
                                zzbdo.zzb(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey);
                                break;
                            case 29:
                                zzbdo.zzi(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 30:
                                zzbdo.zzm(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 31:
                                zzbdo.zzl(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 32:
                                zzbdo.zzg(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 33:
                                zzbdo.zzj(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 34:
                                zzbdo.zze(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, false);
                                break;
                            case 35:
                                zzbdo.zza(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 36:
                                zzbdo.zzb(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 37:
                                zzbdo.zzc(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 38:
                                zzbdo.zzd(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 39:
                                zzbdo.zzh(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 40:
                                zzbdo.zzf(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 41:
                                zzbdo.zzk(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 42:
                                zzbdo.zzn(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 43:
                                zzbdo.zzi(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 44:
                                zzbdo.zzm(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 45:
                                zzbdo.zzl(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 46:
                                zzbdo.zzg(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 47:
                                zzbdo.zzj(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 48:
                                zzbdo.zze(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, true);
                                break;
                            case 49:
                                zzbdo.zzb(this.zzdwg[length2], (List) zzbek.zzp(t, (long) (zzct & 1048575)), zzbey, zzcq(length2));
                                break;
                            case 50:
                                zza(zzbey, i14, zzbek.zzp(t, (long) (zzct & 1048575)), length2);
                                break;
                            case 51:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    d2 = zzf(t, (long) (zzct & 1048575));
                                }
                            case 52:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    f2 = zzg(t, (long) (zzct & 1048575));
                                }
                            case 53:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    j6 = zzi(t, (long) (zzct & 1048575));
                                }
                            case 54:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    j7 = zzi(t, (long) (zzct & 1048575));
                                }
                            case 55:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    i8 = zzh(t, (long) (zzct & 1048575));
                                }
                            case 56:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    j8 = zzi(t, (long) (zzct & 1048575));
                                }
                            case 57:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    i9 = zzh(t, (long) (zzct & 1048575));
                                }
                            case 58:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    z2 = zzj(t, (long) (zzct & 1048575));
                                }
                            case 59:
                                if (!zza(t, i14, length2)) {
                                    break;
                                }
                            case 60:
                                if (!zza(t, i14, length2)) {
                                    break;
                                }
                            case 61:
                                if (!zza(t, i14, length2)) {
                                    break;
                                }
                            case 62:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    i10 = zzh(t, (long) (zzct & 1048575));
                                }
                            case 63:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    i11 = zzh(t, (long) (zzct & 1048575));
                                }
                            case 64:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    i12 = zzh(t, (long) (zzct & 1048575));
                                }
                            case 65:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    j9 = zzi(t, (long) (zzct & 1048575));
                                }
                            case 66:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    i13 = zzh(t, (long) (zzct & 1048575));
                                }
                            case 67:
                                if (!zza(t, i14, length2)) {
                                    break;
                                } else {
                                    j10 = zzi(t, (long) (zzct & 1048575));
                                }
                            case 68:
                                if (!zza(t, i14, length2)) {
                                    break;
                                }
                        }
                        length2 -= 4;
                    }
                    while (entry3 != null) {
                        this.zzdww.zza(zzbey, entry3);
                        entry3 = it2.hasNext() ? (Entry) it2.next() : null;
                    }
                }
            }
            it2 = null;
            entry3 = null;
            length2 = this.zzdwg.length - 4;
            while (length2 >= 0) {
            }
            while (entry3 != null) {
            }
        } else if (this.zzdwo) {
            if (this.zzdwm) {
                zzbbg zzm2 = this.zzdww.zzm(t);
                if (!zzm2.isEmpty()) {
                    it = zzm2.iterator();
                    entry = (Entry) it.next();
                    length = this.zzdwg.length;
                    entry2 = entry;
                    i = 0;
                    while (i < length) {
                        int zzct2 = zzct(i);
                        int i15 = this.zzdwg[i];
                        while (entry2 != null && this.zzdww.zza(entry2) <= i15) {
                            this.zzdww.zza(zzbey, entry2);
                            entry2 = it.hasNext() ? (Entry) it.next() : null;
                        }
                        switch ((zzct2 & 267386880) >>> 20) {
                            case 0:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    d = zzbek.zzo(t, (long) (zzct2 & 1048575));
                                }
                            case 1:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    f = zzbek.zzn(t, (long) (zzct2 & 1048575));
                                }
                            case 2:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    j = zzbek.zzl(t, (long) (zzct2 & 1048575));
                                }
                            case 3:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    j2 = zzbek.zzl(t, (long) (zzct2 & 1048575));
                                }
                            case 4:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    i2 = zzbek.zzk(t, (long) (zzct2 & 1048575));
                                }
                            case 5:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    j3 = zzbek.zzl(t, (long) (zzct2 & 1048575));
                                }
                            case 6:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    i3 = zzbek.zzk(t, (long) (zzct2 & 1048575));
                                }
                            case 7:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    z = zzbek.zzm(t, (long) (zzct2 & 1048575));
                                }
                            case 8:
                                if (!zza(t, i)) {
                                    break;
                                }
                            case 9:
                                if (!zza(t, i)) {
                                    break;
                                }
                            case 10:
                                if (!zza(t, i)) {
                                    break;
                                }
                            case 11:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    i4 = zzbek.zzk(t, (long) (zzct2 & 1048575));
                                }
                            case 12:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    i5 = zzbek.zzk(t, (long) (zzct2 & 1048575));
                                }
                            case 13:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    i6 = zzbek.zzk(t, (long) (zzct2 & 1048575));
                                }
                            case 14:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    j4 = zzbek.zzl(t, (long) (zzct2 & 1048575));
                                }
                            case 15:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    i7 = zzbek.zzk(t, (long) (zzct2 & 1048575));
                                }
                            case 16:
                                if (!zza(t, i)) {
                                    break;
                                } else {
                                    j5 = zzbek.zzl(t, (long) (zzct2 & 1048575));
                                }
                            case 17:
                                if (!zza(t, i)) {
                                    break;
                                }
                            case 18:
                                zzbdo.zza(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 19:
                                zzbdo.zzb(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 20:
                                zzbdo.zzc(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 21:
                                zzbdo.zzd(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 22:
                                zzbdo.zzh(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 23:
                                zzbdo.zzf(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 24:
                                zzbdo.zzk(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 25:
                                zzbdo.zzn(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 26:
                                zzbdo.zza(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey);
                                break;
                            case 27:
                                zzbdo.zza(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, zzcq(i));
                                break;
                            case 28:
                                zzbdo.zzb(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey);
                                break;
                            case 29:
                                zzbdo.zzi(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 30:
                                zzbdo.zzm(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 31:
                                zzbdo.zzl(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 32:
                                zzbdo.zzg(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 33:
                                zzbdo.zzj(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 34:
                                zzbdo.zze(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, false);
                                break;
                            case 35:
                                zzbdo.zza(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 36:
                                zzbdo.zzb(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 37:
                                zzbdo.zzc(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 38:
                                zzbdo.zzd(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 39:
                                zzbdo.zzh(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 40:
                                zzbdo.zzf(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 41:
                                zzbdo.zzk(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 42:
                                zzbdo.zzn(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 43:
                                zzbdo.zzi(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 44:
                                zzbdo.zzm(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 45:
                                zzbdo.zzl(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 46:
                                zzbdo.zzg(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 47:
                                zzbdo.zzj(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 48:
                                zzbdo.zze(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, true);
                                break;
                            case 49:
                                zzbdo.zzb(this.zzdwg[i], (List) zzbek.zzp(t, (long) (zzct2 & 1048575)), zzbey, zzcq(i));
                                break;
                            case 50:
                                zza(zzbey, i15, zzbek.zzp(t, (long) (zzct2 & 1048575)), i);
                                break;
                            case 51:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    d = zzf(t, (long) (zzct2 & 1048575));
                                }
                            case 52:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    f = zzg(t, (long) (zzct2 & 1048575));
                                }
                            case 53:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    j = zzi(t, (long) (zzct2 & 1048575));
                                }
                            case 54:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    j2 = zzi(t, (long) (zzct2 & 1048575));
                                }
                            case 55:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    i2 = zzh(t, (long) (zzct2 & 1048575));
                                }
                            case 56:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    j3 = zzi(t, (long) (zzct2 & 1048575));
                                }
                            case 57:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    i3 = zzh(t, (long) (zzct2 & 1048575));
                                }
                            case 58:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    z = zzj(t, (long) (zzct2 & 1048575));
                                }
                            case 59:
                                if (!zza(t, i15, i)) {
                                    break;
                                }
                            case 60:
                                if (!zza(t, i15, i)) {
                                    break;
                                }
                            case 61:
                                if (!zza(t, i15, i)) {
                                    break;
                                }
                            case 62:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    i4 = zzh(t, (long) (zzct2 & 1048575));
                                }
                            case 63:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    i5 = zzh(t, (long) (zzct2 & 1048575));
                                }
                            case 64:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    i6 = zzh(t, (long) (zzct2 & 1048575));
                                }
                            case 65:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    j4 = zzi(t, (long) (zzct2 & 1048575));
                                }
                            case 66:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    i7 = zzh(t, (long) (zzct2 & 1048575));
                                }
                            case 67:
                                if (!zza(t, i15, i)) {
                                    break;
                                } else {
                                    j5 = zzi(t, (long) (zzct2 & 1048575));
                                }
                            case 68:
                                if (!zza(t, i15, i)) {
                                    break;
                                }
                        }
                        i += 4;
                    }
                    while (entry2 != null) {
                        this.zzdww.zza(zzbey, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    zza(this.zzdwv, t, zzbey);
                }
            }
            it = null;
            entry = null;
            length = this.zzdwg.length;
            entry2 = entry;
            i = 0;
            while (i < length) {
            }
            while (entry2 != null) {
            }
            zza(this.zzdwv, t, zzbey);
        } else {
            zzb(t, zzbey);
        }
    }

    /* JADX WARNING: type inference failed for: r26v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v0 */
    /* JADX WARNING: type inference failed for: r2v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v5, types: [byte, int] */
    /* JADX WARNING: type inference failed for: r16v0, types: [int] */
    /* JADX WARNING: type inference failed for: r12v2 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r0v8, types: [int] */
    /* JADX WARNING: type inference failed for: r1v2, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v4, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v3, types: [int] */
    /* JADX WARNING: type inference failed for: r2v5, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v7, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v6, types: [int] */
    /* JADX WARNING: type inference failed for: r12v6 */
    /* JADX WARNING: type inference failed for: r1v9, types: [int] */
    /* JADX WARNING: type inference failed for: r2v8, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r16v1 */
    /* JADX WARNING: type inference failed for: r1v25, types: [int] */
    /* JADX WARNING: type inference failed for: r16v2 */
    /* JADX WARNING: type inference failed for: r12v7 */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: type inference failed for: r12v9 */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r12v12 */
    /* JADX WARNING: type inference failed for: r12v13 */
    /* JADX WARNING: type inference failed for: r12v14 */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0069, code lost:
        if (r7 == 0) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
        r1 = r11.zzdpn;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0075, code lost:
        r9.putObject(r14, r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00cd, code lost:
        if (r7 == 0) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00cf, code lost:
        r0 = com.google.android.gms.internal.ads.zzbad.zza(r12, r10, r11);
        r1 = r11.zzdpl;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d5, code lost:
        r9.putInt(r14, r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e2, code lost:
        r9.putLong(r14, r2, r4);
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f3, code lost:
        r0 = r10 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0100, code lost:
        r0 = r10 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0190, code lost:
        if (r0 == r14) goto L_0x015d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01ac, code lost:
        if (r0 == r14) goto L_0x015d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0012, code lost:
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0012, code lost:
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0012, code lost:
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0012, code lost:
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0012, code lost:
        r12 = r12;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=null, for r0v5, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r26v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v2
  assigns: []
  uses: []
  mth insns count: 208
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 16 */
    public final void zza(T t, byte[] r26, int i, int i2, zzbae zzbae) throws IOException {
        ? r16;
        int i3;
        ? r12;
        Unsafe unsafe;
        int i4;
        int i5;
        int i6;
        long j;
        zzbcy zzbcy = this;
        T t2 = t;
        ? r122 = r26;
        int i7 = i2;
        zzbae zzbae2 = zzbae;
        if (zzbcy.zzdwo) {
            Unsafe unsafe2 = zzdwf;
            int i8 = i;
            while (i8 < i7) {
                int i9 = i8 + 1;
                ? r0 = r122[i8];
                if (r0 < 0) {
                    i3 = zzbad.zza((int) r0, (byte[]) r122, i9, zzbae2);
                    r16 = zzbae2.zzdpl;
                } else {
                    r16 = r0;
                    i3 = i9;
                }
                int i10 = r16 >>> 3;
                int i11 = r16 & 7;
                int zzcw = zzbcy.zzcw(i10);
                if (zzcw >= 0) {
                    int i12 = zzbcy.zzdwg[zzcw + 1];
                    int i13 = (267386880 & i12) >>> 20;
                    long j2 = (long) (1048575 & i12);
                    if (i13 <= 17) {
                        boolean z = true;
                        switch (i13) {
                            case 0:
                                if (i11 == 1) {
                                    zzbek.zza((Object) t2, j2, zzbad.zzg(r122, i3));
                                    break;
                                }
                                break;
                            case 1:
                                if (i11 == 5) {
                                    zzbek.zza((Object) t2, j2, zzbad.zzh(r122, i3));
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                if (i11 == 0) {
                                    i6 = zzbad.zzb(r122, i3, zzbae2);
                                    j = zzbae2.zzdpm;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                break;
                            case 5:
                            case 14:
                                if (i11 == 1) {
                                    unsafe2.putLong(t2, j2, zzbad.zzf(r122, i3));
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                if (i11 == 5) {
                                    unsafe2.putInt(t2, j2, zzbad.zze(r122, i3));
                                    break;
                                }
                                break;
                            case 7:
                                if (i11 == 0) {
                                    i8 = zzbad.zzb(r122, i3, zzbae2);
                                    if (zzbae2.zzdpm == 0) {
                                        z = false;
                                    }
                                    zzbek.zza((Object) t2, j2, z);
                                    r12 = r122;
                                    break;
                                }
                                break;
                            case 8:
                                if (i11 == 2) {
                                    if ((536870912 & i12) != 0) {
                                        i8 = zzbad.zzd(r122, i3, zzbae2);
                                        break;
                                    } else {
                                        i8 = zzbad.zzc(r122, i3, zzbae2);
                                        break;
                                    }
                                }
                                break;
                            case 9:
                                if (i11 == 2) {
                                    i8 = zza(zzbcy.zzcq(zzcw), (byte[]) r122, i3, i7, zzbae2);
                                    Object object = unsafe2.getObject(t2, j2);
                                    if (object != null) {
                                        Object zza = zzbbq.zza(object, zzbae2.zzdpn);
                                        break;
                                    }
                                }
                                break;
                            case 10:
                                if (i11 == 2) {
                                    i8 = zzbad.zze(r122, i3, zzbae2);
                                    break;
                                }
                                break;
                            case 12:
                                break;
                            case 15:
                                if (i11 == 0) {
                                    int zza2 = zzbad.zza(r122, i3, zzbae2);
                                    int i14 = zzbaq.zzbu(zzbae2.zzdpl);
                                    break;
                                }
                                break;
                            case 16:
                                if (i11 == 0) {
                                    i6 = zzbad.zzb(r122, i3, zzbae2);
                                    j = zzbaq.zzl(zzbae2.zzdpm);
                                    break;
                                }
                                break;
                        }
                    } else {
                        if (i13 != 27) {
                            if (i13 <= 49) {
                                unsafe = unsafe2;
                                int i15 = i3;
                                i8 = zzbcy.zza(t2, (byte[]) r122, i3, i7, (int) r16, i10, i11, zzcw, (long) i12, i13, j2, zzbae);
                                if (i8 != i15) {
                                    t2 = t;
                                    r12 = r26;
                                    i7 = i2;
                                    zzbae2 = zzbae;
                                    unsafe2 = unsafe;
                                    zzbcy = this;
                                }
                            } else {
                                long j3 = j2;
                                unsafe = unsafe2;
                                int i16 = i3;
                                int i17 = i13;
                                if (i17 != 50) {
                                    int i18 = i16;
                                    i8 = zza(t, (byte[]) r26, i18, i2, (int) r16, i10, i11, i12, i17, j3, zzcw, zzbae);
                                } else if (i11 == 2) {
                                    int i19 = i16;
                                    i8 = zza(t, r26, i19, i2, zzcw, i10, j3, zzbae);
                                } else {
                                    i5 = i16;
                                    i4 = i5;
                                    i8 = zza((int) r16, (byte[]) r26, i4, i2, (Object) t, zzbae);
                                    zzbcy = this;
                                    t2 = t;
                                    r12 = r26;
                                    i7 = i2;
                                    zzbae2 = zzbae;
                                    unsafe2 = unsafe;
                                }
                            }
                            i4 = i8;
                            i8 = zza((int) r16, (byte[]) r26, i4, i2, (Object) t, zzbae);
                            zzbcy = this;
                            t2 = t;
                            r12 = r26;
                            i7 = i2;
                            zzbae2 = zzbae;
                            unsafe2 = unsafe;
                        } else if (i11 == 2) {
                            zzbbt zzbbt = (zzbbt) unsafe2.getObject(t2, j2);
                            if (!zzbbt.zzaay()) {
                                int size = zzbbt.size();
                                zzbbt = zzbbt.zzbm(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t2, j2, zzbbt);
                            }
                            i8 = zza(zzbcy.zzcq(zzcw), (int) r16, (byte[]) r122, i3, i7, zzbbt, zzbae2);
                            r12 = r122;
                        }
                        r122 = r12;
                    }
                }
                unsafe = unsafe2;
                i5 = i3;
                i4 = i5;
                i8 = zza((int) r16, (byte[]) r26, i4, i2, (Object) t, zzbae);
                zzbcy = this;
                t2 = t;
                r12 = r26;
                i7 = i2;
                zzbae2 = zzbae;
                unsafe2 = unsafe;
                r122 = r12;
            }
            if (i8 != i7) {
                throw zzbbu.zzadr();
            }
            return;
        }
        zza(t, (byte[]) r26, i, i7, 0, zzbae);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:92:0x011a, code lost:
        continue;
     */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0108 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x011a A[SYNTHETIC] */
    public final boolean zzaa(T t) {
        int[] iArr;
        int i;
        boolean z;
        boolean z2;
        T t2 = t;
        int i2 = 1;
        if (this.zzdwq == null || this.zzdwq.length == 0) {
            return true;
        }
        int[] iArr2 = this.zzdwq;
        int length = iArr2.length;
        int i3 = -1;
        int i4 = 0;
        int i5 = 0;
        while (i4 < length) {
            int i6 = iArr2[i4];
            int zzcw = zzcw(i6);
            int zzct = zzct(zzcw);
            if (!this.zzdwo) {
                int i7 = this.zzdwg[zzcw + 2];
                int i8 = i7 & 1048575;
                i = i2 << (i7 >>> 20);
                if (i8 != i3) {
                    iArr = iArr2;
                    i5 = zzdwf.getInt(t2, (long) i8);
                    i3 = i8;
                } else {
                    iArr = iArr2;
                }
            } else {
                iArr = iArr2;
                i = 0;
            }
            if (((268435456 & zzct) != 0) && !zza(t2, zzcw, i5, i)) {
                return false;
            }
            int i9 = (267386880 & zzct) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 != 60 && i9 != 68) {
                        switch (i9) {
                            case 49:
                                break;
                            case 50:
                                Map zzt = this.zzdwx.zzt(zzbek.zzp(t2, (long) (zzct & 1048575)));
                                if (!zzt.isEmpty()) {
                                    if (this.zzdwx.zzx(zzcr(zzcw)).zzdwa.zzagl() == zzbex.MESSAGE) {
                                        zzbdm zzbdm = null;
                                        Iterator it = zzt.values().iterator();
                                        while (true) {
                                            if (it.hasNext()) {
                                                Object next = it.next();
                                                if (zzbdm == null) {
                                                    zzbdm = zzbdg.zzaeo().zze(next.getClass());
                                                }
                                                if (!zzbdm.zzaa(next)) {
                                                    z2 = false;
                                                }
                                            }
                                        }
                                    }
                                }
                                z2 = true;
                                if (!z2) {
                                    return false;
                                }
                                continue;
                        }
                    } else if (zza(t2, i6, zzcw) && !zza((Object) t2, zzct, zzcq(zzcw))) {
                        return false;
                    }
                }
                List list = (List) zzbek.zzp(t2, (long) (zzct & 1048575));
                if (!list.isEmpty()) {
                    zzbdm zzcq = zzcq(zzcw);
                    int i10 = 0;
                    while (true) {
                        if (i10 < list.size()) {
                            if (!zzcq.zzaa(list.get(i10))) {
                                z = false;
                            } else {
                                i10++;
                            }
                        }
                    }
                    if (z) {
                        return false;
                    }
                }
                z = true;
                if (z) {
                }
            } else if (zza(t2, zzcw, i5, i) && !zza((Object) t2, zzct, zzcq(zzcw))) {
                return false;
            }
            i4++;
            iArr2 = iArr;
            i2 = 1;
        }
        return !this.zzdwm || this.zzdww.zzm(t2).isInitialized();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        com.google.android.gms.internal.ads.zzbek.zza((java.lang.Object) r7, r2, com.google.android.gms.internal.ads.zzbek.zzp(r8, r2));
        zzb(r7, r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008f, code lost:
        com.google.android.gms.internal.ads.zzbek.zza((java.lang.Object) r7, r2, com.google.android.gms.internal.ads.zzbek.zzp(r8, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b9, code lost:
        com.google.android.gms.internal.ads.zzbek.zzb((java.lang.Object) r7, r2, com.google.android.gms.internal.ads.zzbek.zzk(r8, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ce, code lost:
        com.google.android.gms.internal.ads.zzbek.zza((java.lang.Object) r7, r2, com.google.android.gms.internal.ads.zzbek.zzl(r8, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f1, code lost:
        zzb(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00f4, code lost:
        r0 = r0 + 4;
     */
    public final void zzc(T t, T t2) {
        if (t2 == null) {
            throw new NullPointerException();
        }
        int i = 0;
        while (i < this.zzdwg.length) {
            int zzct = zzct(i);
            long j = (long) (1048575 & zzct);
            int i2 = this.zzdwg[i];
            switch ((zzct & 267386880) >>> 20) {
                case 0:
                    if (!zza(t2, i)) {
                        break;
                    } else {
                        zzbek.zza((Object) t, j, zzbek.zzo(t2, j));
                    }
                case 1:
                    if (!zza(t2, i)) {
                        break;
                    } else {
                        zzbek.zza((Object) t, j, zzbek.zzn(t2, j));
                    }
                case 2:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 3:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 4:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 5:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 6:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 7:
                    if (!zza(t2, i)) {
                        break;
                    } else {
                        zzbek.zza((Object) t, j, zzbek.zzm(t2, j));
                    }
                case 8:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 9:
                case 17:
                    zza(t, t2, i);
                    break;
                case 10:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 11:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 12:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 13:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 14:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 15:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 16:
                    if (!zza(t2, i)) {
                        break;
                    }
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.zzdwu.zza(t, t2, j);
                    break;
                case 50:
                    zzbdo.zza(this.zzdwx, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (!zza(t2, i2, i)) {
                        break;
                    }
                case 60:
                case 68:
                    zzb(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (!zza(t2, i2, i)) {
                        break;
                    }
            }
        }
        if (!this.zzdwo) {
            zzbdo.zza(this.zzdwv, t, t2);
            if (this.zzdwm) {
                zzbdo.zza(this.zzdww, t, t2);
            }
        }
    }

    public final void zzo(T t) {
        if (this.zzdwr != null) {
            for (int zzct : this.zzdwr) {
                long zzct2 = (long) (zzct(zzct) & 1048575);
                Object zzp = zzbek.zzp(t, zzct2);
                if (zzp != null) {
                    zzbek.zza((Object) t, zzct2, this.zzdwx.zzv(zzp));
                }
            }
        }
        if (this.zzdws != null) {
            for (int i : this.zzdws) {
                this.zzdwu.zzb(t, (long) i);
            }
        }
        this.zzdwv.zzo(t);
        if (this.zzdwm) {
            this.zzdww.zzo(t);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01d8, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01e9, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01fa, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x020b, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x020d, code lost:
        r2.putInt(r1, (long) r14, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0211, code lost:
        r3 = (com.google.android.gms.internal.ads.zzbav.zzcd(r3) + com.google.android.gms.internal.ads.zzbav.zzcf(r4)) + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0296, code lost:
        r13 = r13 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x029f, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzc(r3, (com.google.android.gms.internal.ads.zzbcu) com.google.android.gms.internal.ads.zzbek.zzp(r1, r4), zzcq(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x02b8, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzf(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02c7, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzs(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02d2, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzh(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x02dd, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzu(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x02ec, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzv(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x02fb, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzr(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0306, code lost:
        r4 = com.google.android.gms.internal.ads.zzbek.zzp(r1, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x030a, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzc(r3, (com.google.android.gms.internal.ads.zzbah) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0317, code lost:
        r3 = com.google.android.gms.internal.ads.zzbdo.zzc(r3, com.google.android.gms.internal.ads.zzbek.zzp(r1, r4), zzcq(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0331, code lost:
        if ((r4 instanceof com.google.android.gms.internal.ads.zzbah) != false) goto L_0x030a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0334, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzg(r3, (java.lang.String) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0342, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzg(r3, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x034e, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzt(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x035a, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzg(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x036a, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzq(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x037a, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zze(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x038a, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzd(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0396, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzb(r3, 0.0f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x03a2, code lost:
        r3 = com.google.android.gms.internal.ads.zzbav.zzb(r3, (double) com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x03aa, code lost:
        r12 = r12 + 4;
        r3 = 267386880;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x0418, code lost:
        if (zza(r1, r14, r3) != false) goto L_0x06ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x0438, code lost:
        if (zza(r1, r14, r3) != false) goto L_0x06f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0440, code lost:
        if (zza(r1, r14, r3) != false) goto L_0x0702;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x0460, code lost:
        if (zza(r1, r14, r3) != false) goto L_0x0727;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x0468, code lost:
        if (zza(r1, r14, r3) != false) goto L_0x0736;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0478, code lost:
        if ((r6 instanceof com.google.android.gms.internal.ads.zzbah) != false) goto L_0x072b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x0480, code lost:
        if (zza(r1, r14, r3) != false) goto L_0x075d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x0518, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x052a, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x053c, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x054e, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x0560, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x0572, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x0584, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x0596, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x05a7, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x05b8, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x05c9, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x05da, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:319:0x05eb, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:323:0x05fc, code lost:
        if (r0.zzdwp != false) goto L_0x05fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:324:0x05fe, code lost:
        r2.putInt(r1, (long) r6, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:0x0602, code lost:
        r6 = (com.google.android.gms.internal.ads.zzbav.zzcd(r14) + com.google.android.gms.internal.ads.zzbav.zzcf(r9)) + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x06af, code lost:
        r4 = r4 + r6;
        r6 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:341:0x06bd, code lost:
        r4 = r4 + r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:343:0x06bf, code lost:
        r9 = false;
        r18 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:345:0x06c8, code lost:
        if ((r12 & r16) != 0) goto L_0x06ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:346:0x06ca, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzc(r14, (com.google.android.gms.internal.ads.zzbcu) r2.getObject(r1, r9), zzcq(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:0x06e1, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzf(r14, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:0x06ee, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzs(r14, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:0x06f5, code lost:
        if ((r12 & r16) != 0) goto L_0x06f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x06f7, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzh(r14, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x0700, code lost:
        if ((r12 & r16) != 0) goto L_0x0702;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:0x0702, code lost:
        r9 = com.google.android.gms.internal.ads.zzbav.zzu(r14, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:0x0707, code lost:
        r4 = r4 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:365:0x0711, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzv(r14, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x071e, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzr(r14, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:371:0x0725, code lost:
        if ((r12 & r16) != 0) goto L_0x0727;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:372:0x0727, code lost:
        r6 = r2.getObject(r1, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:373:0x072b, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzc(r14, (com.google.android.gms.internal.ads.zzbah) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:375:0x0734, code lost:
        if ((r12 & r16) != 0) goto L_0x0736;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x0736, code lost:
        r6 = com.google.android.gms.internal.ads.zzbdo.zzc(r14, r2.getObject(r1, r9), zzcq(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ab, code lost:
        if ((r4 instanceof com.google.android.gms.internal.ads.zzbah) != false) goto L_0x030a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:380:0x074e, code lost:
        if ((r6 instanceof com.google.android.gms.internal.ads.zzbah) != false) goto L_0x072b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:381:0x0751, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzg(r14, (java.lang.String) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:383:0x075b, code lost:
        if ((r12 & r16) != 0) goto L_0x075d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:384:0x075d, code lost:
        r6 = com.google.android.gms.internal.ads.zzbav.zzg(r14, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:0x07ad, code lost:
        r4 = r4 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:409:0x07cf, code lost:
        r3 = r3 + 4;
        r11 = r6;
        r6 = r9;
        r9 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0127, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0139, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x014b, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x015d, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x016f, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0181, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0193, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01a5, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01b6, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01c7, code lost:
        if (r0.zzdwp != false) goto L_0x020d;
     */
    public final int zzy(T t) {
        int i;
        int i2;
        long j;
        boolean z;
        boolean z2;
        int i3;
        int zzg;
        Object obj;
        int i4;
        int i5;
        int i6;
        int i7;
        long j2;
        boolean z3;
        int i8;
        int i9;
        int zzb;
        long j3;
        long j4;
        int i10;
        Object obj2;
        int i11;
        int i12;
        int i13;
        long j5;
        int i14;
        T t2 = t;
        int i15 = 267386880;
        if (this.zzdwo) {
            Unsafe unsafe = zzdwf;
            int i16 = 0;
            int i17 = 0;
            while (i16 < this.zzdwg.length) {
                int zzct = zzct(i16);
                int i18 = (zzct & i15) >>> 20;
                int i19 = this.zzdwg[i16];
                long j6 = (long) (zzct & 1048575);
                int i20 = (i18 < zzbbj.DOUBLE_LIST_PACKED.id() || i18 > zzbbj.SINT64_LIST_PACKED.id()) ? 0 : this.zzdwg[i16 + 2] & 1048575;
                switch (i18) {
                    case 0:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 1:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 2:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            j3 = zzbek.zzl(t2, j6);
                            break;
                        }
                    case 3:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            j4 = zzbek.zzl(t2, j6);
                            break;
                        }
                    case 4:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            i10 = zzbek.zzk(t2, j6);
                            break;
                        }
                    case 5:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 6:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 7:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 8:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            obj2 = zzbek.zzp(t2, j6);
                            break;
                        }
                    case 9:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 10:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 11:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            i11 = zzbek.zzk(t2, j6);
                            break;
                        }
                    case 12:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            i12 = zzbek.zzk(t2, j6);
                            break;
                        }
                    case 13:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 14:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 15:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            i13 = zzbek.zzk(t2, j6);
                            break;
                        }
                    case 16:
                        if (!zza(t2, i16)) {
                            break;
                        } else {
                            j5 = zzbek.zzl(t2, j6);
                            break;
                        }
                    case 17:
                        if (!zza(t2, i16)) {
                            break;
                        }
                        break;
                    case 18:
                    case 23:
                    case 32:
                        zzb = zzbdo.zzw(i19, zze(t2, j6), false);
                        break;
                    case 19:
                    case 24:
                    case 31:
                        zzb = zzbdo.zzv(i19, zze(t2, j6), false);
                        break;
                    case 20:
                        zzb = zzbdo.zzo(i19, zze(t2, j6), false);
                        break;
                    case 21:
                        zzb = zzbdo.zzp(i19, zze(t2, j6), false);
                        break;
                    case 22:
                        zzb = zzbdo.zzs(i19, zze(t2, j6), false);
                        break;
                    case 25:
                        zzb = zzbdo.zzx(i19, zze(t2, j6), false);
                        break;
                    case 26:
                        zzb = zzbdo.zzc(i19, zze(t2, j6));
                        break;
                    case 27:
                        zzb = zzbdo.zzc(i19, zze(t2, j6), zzcq(i16));
                        break;
                    case 28:
                        zzb = zzbdo.zzd(i19, zze(t2, j6));
                        break;
                    case 29:
                        zzb = zzbdo.zzt(i19, zze(t2, j6), false);
                        break;
                    case 30:
                        zzb = zzbdo.zzr(i19, zze(t2, j6), false);
                        break;
                    case 33:
                        zzb = zzbdo.zzu(i19, zze(t2, j6), false);
                        break;
                    case 34:
                        zzb = zzbdo.zzq(i19, zze(t2, j6), false);
                        break;
                    case 35:
                        i14 = zzbdo.zzan((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        i14 = zzbdo.zzam((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        i14 = zzbdo.zzaf((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        i14 = zzbdo.zzag((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        i14 = zzbdo.zzaj((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        i14 = zzbdo.zzan((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        i14 = zzbdo.zzam((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        i14 = zzbdo.zzao((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        i14 = zzbdo.zzak((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        i14 = zzbdo.zzai((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        i14 = zzbdo.zzam((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        i14 = zzbdo.zzan((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        i14 = zzbdo.zzal((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        i14 = zzbdo.zzah((List) unsafe.getObject(t2, j6));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        zzb = zzbdo.zzd(i19, zze(t2, j6), zzcq(i16));
                        break;
                    case 50:
                        zzb = this.zzdwx.zzb(i19, zzbek.zzp(t2, j6), zzcr(i16));
                        break;
                    case 51:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 52:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 53:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            j3 = zzi(t2, j6);
                            break;
                        }
                    case 54:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            j4 = zzi(t2, j6);
                            break;
                        }
                    case 55:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            i10 = zzh(t2, j6);
                            break;
                        }
                    case 56:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 57:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 58:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 59:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            obj2 = zzbek.zzp(t2, j6);
                            break;
                        }
                    case 60:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 61:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 62:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            i11 = zzh(t2, j6);
                            break;
                        }
                    case 63:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            i12 = zzh(t2, j6);
                            break;
                        }
                    case 64:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 65:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 66:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            i13 = zzh(t2, j6);
                            break;
                        }
                    case 67:
                        if (!zza(t2, i19, i16)) {
                            break;
                        } else {
                            j5 = zzi(t2, j6);
                            break;
                        }
                    case 68:
                        if (!zza(t2, i19, i16)) {
                            break;
                        }
                        break;
                }
            }
            return i17 + zza(this.zzdwv, t2);
        }
        Unsafe unsafe2 = zzdwf;
        int i21 = -1;
        int i22 = 0;
        int i23 = 0;
        int i24 = 0;
        while (i22 < this.zzdwg.length) {
            int zzct2 = zzct(i22);
            int i25 = this.zzdwg[i22];
            int i26 = (zzct2 & 267386880) >>> 20;
            if (i26 <= 17) {
                i2 = this.zzdwg[i22 + 2];
                int i27 = i2 & 1048575;
                i = 1 << (i2 >>> 20);
                if (i27 != i21) {
                    i24 = unsafe2.getInt(t2, (long) i27);
                    i21 = i27;
                }
            } else {
                i2 = (!this.zzdwp || i26 < zzbbj.DOUBLE_LIST_PACKED.id() || i26 > zzbbj.SINT64_LIST_PACKED.id()) ? 0 : this.zzdwg[i22 + 2] & 1048575;
                i = 0;
            }
            long j7 = (long) (zzct2 & 1048575);
            switch (i26) {
                case 0:
                    z2 = false;
                    z = false;
                    j = 0;
                    if ((i24 & i) != 0) {
                        i23 += zzbav.zzb(i25, (double) Utils.DOUBLE_EPSILON);
                        break;
                    }
                case 1:
                    z2 = false;
                    j = 0;
                    if ((i24 & i) != 0) {
                        z = false;
                        i23 += zzbav.zzb(i25, 0.0f);
                        break;
                    }
                case 2:
                    z2 = false;
                    j = 0;
                    if ((i24 & i) != 0) {
                        i3 = zzbav.zzd(i25, unsafe2.getLong(t2, j7));
                    }
                    z = false;
                    break;
                case 3:
                    z2 = false;
                    j = 0;
                    if ((i24 & i) != 0) {
                        i3 = zzbav.zze(i25, unsafe2.getLong(t2, j7));
                    }
                    z = false;
                    break;
                case 4:
                    z2 = false;
                    j = 0;
                    if ((i24 & i) != 0) {
                        i3 = zzbav.zzq(i25, unsafe2.getInt(t2, j7));
                    }
                    z = false;
                    break;
                case 5:
                    z2 = false;
                    if ((i24 & i) != 0) {
                        i23 += zzbav.zzg(i25, 0);
                        j = 0;
                        z = false;
                        break;
                    }
                    break;
                case 6:
                    if ((i24 & i) != 0) {
                        z2 = false;
                        i23 += zzbav.zzt(i25, 0);
                        break;
                    }
                case 7:
                    break;
                case 8:
                    if ((i24 & i) != 0) {
                        obj = unsafe2.getObject(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 9:
                    break;
                case 10:
                    break;
                case 11:
                    if ((i24 & i) != 0) {
                        i4 = unsafe2.getInt(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 12:
                    if ((i24 & i) != 0) {
                        i5 = unsafe2.getInt(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 13:
                    break;
                case 14:
                    break;
                case 15:
                    if ((i24 & i) != 0) {
                        i7 = unsafe2.getInt(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 16:
                    if ((i24 & i) != 0) {
                        j2 = unsafe2.getLong(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 17:
                    break;
                case 18:
                    zzg = zzbdo.zzw(i25, (List) unsafe2.getObject(t2, j7), false);
                case 19:
                case 24:
                case 31:
                    z3 = false;
                    i8 = zzbdo.zzv(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 20:
                    z3 = false;
                    i8 = zzbdo.zzo(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 21:
                    z3 = false;
                    i8 = zzbdo.zzp(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 22:
                    z3 = false;
                    i8 = zzbdo.zzs(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 23:
                case 32:
                    z3 = false;
                    i8 = zzbdo.zzw(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 25:
                    z3 = false;
                    i8 = zzbdo.zzx(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 26:
                    zzg = zzbdo.zzc(i25, (List) unsafe2.getObject(t2, j7));
                case 27:
                    zzg = zzbdo.zzc(i25, (List) unsafe2.getObject(t2, j7), zzcq(i22));
                case 28:
                    zzg = zzbdo.zzd(i25, (List) unsafe2.getObject(t2, j7));
                case 29:
                    zzg = zzbdo.zzt(i25, (List) unsafe2.getObject(t2, j7), false);
                case 30:
                    z3 = false;
                    i8 = zzbdo.zzr(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 33:
                    z3 = false;
                    i8 = zzbdo.zzu(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 34:
                    z3 = false;
                    i8 = zzbdo.zzq(i25, (List) unsafe2.getObject(t2, j7), false);
                    break;
                case 35:
                    i9 = zzbdo.zzan((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 36:
                    i9 = zzbdo.zzam((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 37:
                    i9 = zzbdo.zzaf((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 38:
                    i9 = zzbdo.zzag((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 39:
                    i9 = zzbdo.zzaj((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 40:
                    i9 = zzbdo.zzan((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 41:
                    i9 = zzbdo.zzam((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 42:
                    i9 = zzbdo.zzao((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 43:
                    i9 = zzbdo.zzak((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 44:
                    i9 = zzbdo.zzai((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 45:
                    i9 = zzbdo.zzam((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 46:
                    i9 = zzbdo.zzan((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 47:
                    i9 = zzbdo.zzal((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 48:
                    i9 = zzbdo.zzah((List) unsafe2.getObject(t2, j7));
                    if (i9 > 0) {
                        break;
                    }
                    z2 = false;
                    break;
                case 49:
                    zzg = zzbdo.zzd(i25, (List) unsafe2.getObject(t2, j7), zzcq(i22));
                case 50:
                    zzg = this.zzdwx.zzb(i25, unsafe2.getObject(t2, j7), zzcr(i22));
                case 51:
                    if (zza(t2, i25, i22)) {
                        zzg = zzbav.zzb(i25, (double) Utils.DOUBLE_EPSILON);
                    }
                    z2 = false;
                    break;
                case 52:
                    if (zza(t2, i25, i22)) {
                        i6 = zzbav.zzb(i25, 0.0f);
                    }
                    z2 = false;
                    break;
                case 53:
                    if (zza(t2, i25, i22)) {
                        zzg = zzbav.zzd(i25, zzi(t2, j7));
                    }
                    z2 = false;
                    break;
                case 54:
                    if (zza(t2, i25, i22)) {
                        zzg = zzbav.zze(i25, zzi(t2, j7));
                    }
                    z2 = false;
                    break;
                case 55:
                    if (zza(t2, i25, i22)) {
                        zzg = zzbav.zzq(i25, zzh(t2, j7));
                    }
                    z2 = false;
                    break;
                case 56:
                    if (zza(t2, i25, i22)) {
                        zzg = zzbav.zzg(i25, 0);
                    }
                    z2 = false;
                    break;
                case 57:
                    if (zza(t2, i25, i22)) {
                        i6 = zzbav.zzt(i25, 0);
                    }
                    z2 = false;
                    break;
                case 58:
                    break;
                case 59:
                    if (zza(t2, i25, i22)) {
                        obj = unsafe2.getObject(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 60:
                    break;
                case 61:
                    break;
                case 62:
                    if (zza(t2, i25, i22)) {
                        i4 = zzh(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 63:
                    if (zza(t2, i25, i22)) {
                        i5 = zzh(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 64:
                    break;
                case 65:
                    break;
                case 66:
                    if (zza(t2, i25, i22)) {
                        i7 = zzh(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 67:
                    if (zza(t2, i25, i22)) {
                        j2 = zzi(t2, j7);
                        break;
                    }
                    z2 = false;
                    break;
                case 68:
                    break;
            }
        }
        int zza = i23 + zza(this.zzdwv, t2);
        if (this.zzdwm) {
            zza += this.zzdww.zzm(t2).zzacw();
        }
        return zza;
    }
}
