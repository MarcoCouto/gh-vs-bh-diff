package com.google.android.gms.internal.ads;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zzbfa {
    private final ByteBuffer zzebj;

    private zzbfa(ByteBuffer byteBuffer) {
        this.zzebj = byteBuffer;
        this.zzebj.order(ByteOrder.LITTLE_ENDIAN);
    }

    private zzbfa(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    private static int zza(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) < 65536) {
                                StringBuilder sb = new StringBuilder(39);
                                sb.append("Unpaired surrogate at index ");
                                sb.append(i2);
                                throw new IllegalArgumentException(sb.toString());
                            }
                            i2++;
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        long j = ((long) i3) + 4294967296L;
        StringBuilder sb2 = new StringBuilder(54);
        sb2.append("UTF-8 length does not fit in int: ");
        sb2.append(j);
        throw new IllegalArgumentException(sb2.toString());
    }

    private static void zza(CharSequence charSequence, ByteBuffer byteBuffer) {
        int i;
        int i2;
        int i3;
        CharSequence charSequence2 = charSequence;
        ByteBuffer byteBuffer2 = byteBuffer;
        if (byteBuffer.isReadOnly()) {
            throw new ReadOnlyBufferException();
        }
        char c = 2048;
        if (byteBuffer.hasArray()) {
            try {
                byte[] array = byteBuffer.array();
                int arrayOffset = byteBuffer.arrayOffset() + byteBuffer.position();
                int remaining = byteBuffer.remaining();
                int length = charSequence.length();
                int i4 = remaining + arrayOffset;
                int i5 = 0;
                while (i5 < length) {
                    int i6 = i5 + arrayOffset;
                    if (i6 >= i4) {
                        break;
                    }
                    char charAt = charSequence2.charAt(i5);
                    if (charAt >= 128) {
                        break;
                    }
                    array[i6] = (byte) charAt;
                    i5++;
                }
                if (i5 == length) {
                    i = arrayOffset + length;
                } else {
                    i = arrayOffset + i5;
                    while (i5 < length) {
                        char charAt2 = charSequence2.charAt(i5);
                        if (charAt2 >= 128 || i >= i4) {
                            if (charAt2 < c && i <= i4 - 2) {
                                int i7 = i + 1;
                                array[i] = (byte) (960 | (charAt2 >>> 6));
                                i2 = i7 + 1;
                                array[i7] = (byte) ((charAt2 & '?') | 128);
                            } else if ((charAt2 < 55296 || 57343 < charAt2) && i <= i4 - 3) {
                                int i8 = i + 1;
                                array[i] = (byte) (480 | (charAt2 >>> 12));
                                int i9 = i8 + 1;
                                array[i8] = (byte) (((charAt2 >>> 6) & 63) | 128);
                                i3 = i9 + 1;
                                array[i9] = (byte) ((charAt2 & '?') | 128);
                            } else if (i <= i4 - 4) {
                                int i10 = i5 + 1;
                                if (i10 != charSequence.length()) {
                                    char charAt3 = charSequence2.charAt(i10);
                                    if (!Character.isSurrogatePair(charAt2, charAt3)) {
                                        i5 = i10;
                                    } else {
                                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                                        int i11 = i + 1;
                                        array[i] = (byte) (240 | (codePoint >>> 18));
                                        int i12 = i11 + 1;
                                        array[i11] = (byte) (((codePoint >>> 12) & 63) | 128);
                                        int i13 = i12 + 1;
                                        array[i12] = (byte) (((codePoint >>> 6) & 63) | 128);
                                        i2 = i13 + 1;
                                        array[i13] = (byte) ((codePoint & 63) | 128);
                                        i5 = i10;
                                    }
                                }
                                int i14 = i5 - 1;
                                StringBuilder sb = new StringBuilder(39);
                                sb.append("Unpaired surrogate at index ");
                                sb.append(i14);
                                throw new IllegalArgumentException(sb.toString());
                            } else {
                                StringBuilder sb2 = new StringBuilder(37);
                                sb2.append("Failed writing ");
                                sb2.append(charAt2);
                                sb2.append(" at index ");
                                sb2.append(i);
                                throw new ArrayIndexOutOfBoundsException(sb2.toString());
                            }
                            i = i2;
                            i5++;
                            c = 2048;
                        } else {
                            i3 = i + 1;
                            array[i] = (byte) charAt2;
                        }
                        i = i3;
                        i5++;
                        c = 2048;
                    }
                }
                byteBuffer2.position(i - byteBuffer.arrayOffset());
            } catch (ArrayIndexOutOfBoundsException e) {
                ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException = e;
                BufferOverflowException bufferOverflowException = new BufferOverflowException();
                bufferOverflowException.initCause(arrayIndexOutOfBoundsException);
                throw bufferOverflowException;
            }
        } else {
            int length2 = charSequence.length();
            int i15 = 0;
            while (i15 < length2) {
                char charAt4 = charSequence2.charAt(i15);
                if (charAt4 < 128) {
                    byteBuffer2.put((byte) charAt4);
                } else if (charAt4 < 2048) {
                    byteBuffer2.put((byte) ((charAt4 >>> 6) | 960));
                    byteBuffer2.put((byte) ((charAt4 & '?') | 128));
                } else if (charAt4 < 55296 || 57343 < charAt4) {
                    byteBuffer2.put((byte) ((charAt4 >>> 12) | 480));
                    byteBuffer2.put((byte) (((charAt4 >>> 6) & 63) | 128));
                    byteBuffer2.put((byte) ((charAt4 & '?') | 128));
                    i15++;
                } else {
                    int i16 = i15 + 1;
                    if (i16 != charSequence.length()) {
                        char charAt5 = charSequence2.charAt(i16);
                        if (!Character.isSurrogatePair(charAt4, charAt5)) {
                            i15 = i16;
                        } else {
                            int codePoint2 = Character.toCodePoint(charAt4, charAt5);
                            byteBuffer2.put((byte) ((codePoint2 >>> 18) | PsExtractor.VIDEO_STREAM_MASK));
                            byteBuffer2.put((byte) (((codePoint2 >>> 12) & 63) | 128));
                            byteBuffer2.put((byte) (((codePoint2 >>> 6) & 63) | 128));
                            byteBuffer2.put((byte) ((codePoint2 & 63) | 128));
                            i15 = i16;
                            i15++;
                        }
                    }
                    int i17 = i15 - 1;
                    StringBuilder sb3 = new StringBuilder(39);
                    sb3.append("Unpaired surrogate at index ");
                    sb3.append(i17);
                    throw new IllegalArgumentException(sb3.toString());
                }
                i15++;
            }
        }
    }

    public static int zzb(int i, zzbfi zzbfi) {
        int zzcd = zzcd(i);
        int zzacw = zzbfi.zzacw();
        return zzcd + zzcl(zzacw) + zzacw;
    }

    public static int zzb(int i, byte[] bArr) {
        return zzcd(i) + zzv(bArr);
    }

    public static int zzcd(int i) {
        return zzcl(i << 3);
    }

    public static int zzce(int i) {
        if (i >= 0) {
            return zzcl(i);
        }
        return 10;
    }

    public static int zzcl(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    public static int zzd(int i, long j) {
        return zzcd(i) + zzy(j);
    }

    private final void zzdd(int i) throws IOException {
        byte b = (byte) i;
        if (!this.zzebj.hasRemaining()) {
            throw new zzbfb(this.zzebj.position(), this.zzebj.limit());
        }
        this.zzebj.put(b);
    }

    public static int zze(int i, long j) {
        return zzcd(i) + zzy(j);
    }

    public static int zzeo(String str) {
        int zza = zza(str);
        return zzcl(zza) + zza;
    }

    public static int zzg(int i, String str) {
        return zzcd(i) + zzeo(str);
    }

    public static zzbfa zzj(byte[] bArr, int i, int i2) {
        return new zzbfa(bArr, 0, i2);
    }

    public static int zzq(int i, int i2) {
        return zzcd(i) + zzce(i2);
    }

    public static zzbfa zzu(byte[] bArr) {
        return zzj(bArr, 0, bArr.length);
    }

    public static int zzv(byte[] bArr) {
        return zzcl(bArr.length) + bArr.length;
    }

    private final void zzx(long j) throws IOException {
        while ((j & -128) != 0) {
            zzdd((((int) j) & 127) | 128);
            j >>>= 7;
        }
        zzdd((int) j);
    }

    public static int zzy(long j) {
        if ((j & -128) == 0) {
            return 1;
        }
        if ((j & -16384) == 0) {
            return 2;
        }
        if ((j & -2097152) == 0) {
            return 3;
        }
        if ((j & -268435456) == 0) {
            return 4;
        }
        if ((j & -34359738368L) == 0) {
            return 5;
        }
        if ((j & -4398046511104L) == 0) {
            return 6;
        }
        if ((j & -562949953421312L) == 0) {
            return 7;
        }
        if ((j & -72057594037927936L) == 0) {
            return 8;
        }
        return (j & Long.MIN_VALUE) == 0 ? 9 : 10;
    }

    public final void zza(int i, long j) throws IOException {
        zzl(i, 0);
        zzx(j);
    }

    public final void zza(int i, zzbfi zzbfi) throws IOException {
        zzl(i, 2);
        if (zzbfi.zzebt < 0) {
            zzbfi.zzacw();
        }
        zzde(zzbfi.zzebt);
        zzbfi.zza(this);
    }

    public final void zza(int i, byte[] bArr) throws IOException {
        zzl(i, 2);
        zzde(bArr.length);
        zzw(bArr);
    }

    public final void zzacl() {
        if (this.zzebj.remaining() != 0) {
            throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", new Object[]{Integer.valueOf(this.zzebj.remaining())}));
        }
    }

    public final void zzde(int i) throws IOException {
        while ((i & -128) != 0) {
            zzdd((i & 127) | 128);
            i >>>= 7;
        }
        zzdd(i);
    }

    public final void zzf(int i, String str) throws IOException {
        zzl(i, 2);
        try {
            int zzcl = zzcl(str.length());
            if (zzcl == zzcl(str.length() * 3)) {
                int position = this.zzebj.position();
                if (this.zzebj.remaining() < zzcl) {
                    throw new zzbfb(position + zzcl, this.zzebj.limit());
                }
                this.zzebj.position(position + zzcl);
                zza((CharSequence) str, this.zzebj);
                int position2 = this.zzebj.position();
                this.zzebj.position(position);
                zzde((position2 - position) - zzcl);
                this.zzebj.position(position2);
                return;
            }
            zzde(zza(str));
            zza((CharSequence) str, this.zzebj);
        } catch (BufferOverflowException e) {
            zzbfb zzbfb = new zzbfb(this.zzebj.position(), this.zzebj.limit());
            zzbfb.initCause(e);
            throw zzbfb;
        }
    }

    public final void zzf(int i, boolean z) throws IOException {
        zzl(i, 0);
        byte b = z ? (byte) 1 : 0;
        if (!this.zzebj.hasRemaining()) {
            throw new zzbfb(this.zzebj.position(), this.zzebj.limit());
        }
        this.zzebj.put(b);
    }

    public final void zzi(int i, long j) throws IOException {
        zzl(i, 0);
        zzx(j);
    }

    public final void zzl(int i, int i2) throws IOException {
        zzde((i << 3) | i2);
    }

    public final void zzm(int i, int i2) throws IOException {
        zzl(i, 0);
        if (i2 >= 0) {
            zzde(i2);
        } else {
            zzx((long) i2);
        }
    }

    public final void zzw(byte[] bArr) throws IOException {
        int length = bArr.length;
        if (this.zzebj.remaining() >= length) {
            this.zzebj.put(bArr, 0, length);
            return;
        }
        throw new zzbfb(this.zzebj.position(), this.zzebj.limit());
    }
}
