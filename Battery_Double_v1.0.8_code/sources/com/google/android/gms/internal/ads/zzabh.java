package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.SystemClock;
import javax.annotation.concurrent.GuardedBy;

@zzadh
public abstract class zzabh extends zzajx {
    protected final Context mContext;
    protected final Object mLock = new Object();
    protected final zzabm zzbzd;
    protected final zzaji zzbze;
    @GuardedBy("mLock")
    protected zzaej zzbzf;
    protected final Object zzbzh = new Object();

    protected zzabh(Context context, zzaji zzaji, zzabm zzabm) {
        super(true);
        this.mContext = context;
        this.zzbze = zzaji;
        this.zzbzf = zzaji.zzcos;
        this.zzbzd = zzabm;
    }

    public void onStop() {
    }

    /* access modifiers changed from: protected */
    public abstract zzajh zzaa(int i);

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[Catch:{ zzabk -> 0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[Catch:{ zzabk -> 0x0014 }] */
    public final void zzdn() {
        synchronized (this.mLock) {
            zzakb.zzck("AdRendererBackgroundTask started.");
            int i = this.zzbze.errorCode;
            try {
                zze(SystemClock.elapsedRealtime());
            } catch (zzabk e) {
                int errorCode = e.getErrorCode();
                if (errorCode != 3) {
                    if (errorCode != -1) {
                        zzakb.zzdk(e.getMessage());
                        this.zzbzf = this.zzbzf != null ? new zzaej(errorCode) : new zzaej(errorCode, this.zzbzf.zzbsu);
                        zzakk.zzcrm.post(new zzabi(this));
                        i = errorCode;
                    }
                }
                zzakb.zzdj(e.getMessage());
                this.zzbzf = this.zzbzf != null ? new zzaej(errorCode) : new zzaej(errorCode, this.zzbzf.zzbsu);
                zzakk.zzcrm.post(new zzabi(this));
                i = errorCode;
            }
            zzakk.zzcrm.post(new zzabj(this, zzaa(i)));
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zze(long j) throws zzabk;
}
