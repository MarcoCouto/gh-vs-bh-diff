package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;
import java.util.List;

public abstract class zzya extends zzek implements zzxz {
    public zzya() {
        super("com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005f, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0081, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a3, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00be, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c5, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        IInterface iInterface;
        boolean z;
        switch (i) {
            case 2:
                str = getHeadline();
                break;
            case 3:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                break;
            case 4:
                str = getBody();
                break;
            case 5:
                iInterface = zzjz();
                break;
            case 6:
                str = getCallToAction();
                break;
            case 7:
                double starRating = getStarRating();
                parcel2.writeNoException();
                parcel2.writeDouble(starRating);
                break;
            case 8:
                str = getStore();
                break;
            case 9:
                str = getPrice();
                break;
            case 10:
                recordImpression();
                break;
            case 11:
                zzj(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 12:
                zzk(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 13:
                z = getOverrideImpressionRecording();
                break;
            case 14:
                z = getOverrideClickHandling();
                break;
            case 15:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzel.zzb(parcel2, extras);
                break;
            case 16:
                zzl(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 17:
                iInterface = getVideoController();
                break;
            case 18:
                iInterface = zzmv();
                break;
            case 19:
                iInterface = zzkf();
                break;
            case 20:
                iInterface = zzmw();
                break;
            case 21:
                iInterface = zzke();
                break;
            case 22:
                zzb(Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()));
                break;
            default:
                return false;
        }
    }
}
