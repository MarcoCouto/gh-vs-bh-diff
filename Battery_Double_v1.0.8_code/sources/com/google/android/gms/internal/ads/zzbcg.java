package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class zzbcg extends zzbce {
    private static final Class<?> zzdvs = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private zzbcg() {
        super();
    }

    /* JADX WARNING: type inference failed for: r1v4, types: [com.google.android.gms.internal.ads.zzbcc, com.google.android.gms.internal.ads.zzbab] */
    /* JADX WARNING: type inference failed for: r1v5, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v2, types: [java.util.List<L>] */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 3 */
    private static <L> List<L> zza(Object obj, long j, int i) {
        ? r1;
        List<L> zzc = zzc(obj, j);
        if (zzc.isEmpty()) {
            List<L> zzbcc = zzc instanceof zzbcd ? new zzbcc<>(i) : new ArrayList<>(i);
            zzbek.zza(obj, j, (Object) zzbcc);
            return zzbcc;
        }
        if (zzdvs.isAssignableFrom(zzc.getClass())) {
            ArrayList arrayList = new ArrayList(zzc.size() + i);
            arrayList.addAll(zzc);
            r1 = arrayList;
        } else if (!(zzc instanceof zzbeh)) {
            return zzc;
        } else {
            ? zzbcc2 = new zzbcc(zzc.size() + i);
            zzbcc2.addAll((zzbeh) zzc);
            r1 = zzbcc2;
        }
        zzbek.zza(obj, j, (Object) r1);
        return r1;
    }

    private static <E> List<E> zzc(Object obj, long j) {
        return (List) zzbek.zzp(obj, j);
    }

    /* access modifiers changed from: 0000 */
    public final <L> List<L> zza(Object obj, long j) {
        return zza(obj, j, 10);
    }

    /* access modifiers changed from: 0000 */
    public final <E> void zza(Object obj, Object obj2, long j) {
        List zzc = zzc(obj2, j);
        List zza = zza(obj, j, zzc.size());
        int size = zza.size();
        int size2 = zzc.size();
        if (size > 0 && size2 > 0) {
            zza.addAll(zzc);
        }
        if (size > 0) {
            zzc = zza;
        }
        zzbek.zza(obj, j, (Object) zzc);
    }

    /* access modifiers changed from: 0000 */
    public final void zzb(Object obj, long j) {
        Object obj2;
        List list = (List) zzbek.zzp(obj, j);
        if (list instanceof zzbcd) {
            obj2 = ((zzbcd) list).zzadx();
        } else if (!zzdvs.isAssignableFrom(list.getClass())) {
            obj2 = Collections.unmodifiableList(list);
        } else {
            return;
        }
        zzbek.zza(obj, j, obj2);
    }
}
