package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzhu.zza.zzb;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
public final class zzadk extends zzajx implements zzadx {
    private final Context mContext;
    @VisibleForTesting
    private zzwy zzbtj;
    @VisibleForTesting
    private zzaef zzbuc;
    @VisibleForTesting
    private zzaej zzbzf;
    /* access modifiers changed from: private */
    public Runnable zzbzg;
    /* access modifiers changed from: private */
    public final Object zzbzh = new Object();
    private final zzadj zzccf;
    /* access modifiers changed from: private */
    public final zzaeg zzccg;
    private final zzhs zzcch;
    private final zzhx zzcci;
    @GuardedBy("mCancelLock")
    @VisibleForTesting
    zzalc zzccj;

    public zzadk(Context context, zzaeg zzaeg, zzadj zzadj, zzhx zzhx) {
        zzhs zzhs;
        zzht zzht;
        this.zzccf = zzadj;
        this.mContext = context;
        this.zzccg = zzaeg;
        this.zzcci = zzhx;
        this.zzcch = new zzhs(this.zzcci);
        this.zzcch.zza((zzht) new zzadl(this));
        zzit zzit = new zzit();
        zzit.zzaot = Integer.valueOf(this.zzccg.zzacr.zzcve);
        zzit.zzaou = Integer.valueOf(this.zzccg.zzacr.zzcvf);
        zzit.zzaov = Integer.valueOf(this.zzccg.zzacr.zzcvg ? 0 : 2);
        this.zzcch.zza((zzht) new zzadm(zzit));
        if (this.zzccg.zzccw != null) {
            this.zzcch.zza((zzht) new zzadn(this));
        }
        zzjn zzjn = this.zzccg.zzacv;
        if (zzjn.zzarc && "interstitial_mb".equals(zzjn.zzarb)) {
            zzhs = this.zzcch;
            zzht = zzado.zzccm;
        } else if (zzjn.zzarc && "reward_mb".equals(zzjn.zzarb)) {
            zzhs = this.zzcch;
            zzht = zzadp.zzccm;
        } else if (zzjn.zzare || zzjn.zzarc) {
            zzhs = this.zzcch;
            zzht = zzadr.zzccm;
        } else {
            zzhs = this.zzcch;
            zzht = zzadq.zzccm;
        }
        zzhs.zza(zzht);
        this.zzcch.zza(zzb.AD_REQUEST);
    }

    @VisibleForTesting
    private final zzjn zza(zzaef zzaef) throws zzadu {
        zzjn[] zzjnArr;
        if (((this.zzbuc == null || this.zzbuc.zzadn == null || this.zzbuc.zzadn.size() <= 1) ? false : true) && this.zzbtj != null && !this.zzbtj.zzbte) {
            return null;
        }
        if (this.zzbzf.zzarf) {
            for (zzjn zzjn : zzaef.zzacv.zzard) {
                if (zzjn.zzarf) {
                    return new zzjn(zzjn, zzaef.zzacv.zzard);
                }
            }
        }
        if (this.zzbzf.zzcet == null) {
            throw new zzadu("The ad response must specify one of the supported ad sizes.", 0);
        }
        String[] split = this.zzbzf.zzcet.split("x");
        if (split.length != 2) {
            String str = "Invalid ad size format from the ad response: ";
            String valueOf = String.valueOf(this.zzbzf.zzcet);
            throw new zzadu(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), 0);
        }
        try {
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            zzjn[] zzjnArr2 = zzaef.zzacv.zzard;
            int length = zzjnArr2.length;
            for (int i = 0; i < length; i++) {
                zzjn zzjn2 = zzjnArr2[i];
                float f = this.mContext.getResources().getDisplayMetrics().density;
                int i2 = zzjn2.width == -1 ? (int) (((float) zzjn2.widthPixels) / f) : zzjn2.width;
                int i3 = zzjn2.height == -2 ? (int) (((float) zzjn2.heightPixels) / f) : zzjn2.height;
                if (parseInt == i2 && parseInt2 == i3 && !zzjn2.zzarf) {
                    return new zzjn(zzjn2, zzaef.zzacv.zzard);
                }
            }
            String str2 = "The ad size from the ad response was not one of the requested sizes: ";
            String valueOf2 = String.valueOf(this.zzbzf.zzcet);
            throw new zzadu(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), 0);
        } catch (NumberFormatException unused) {
            String str3 = "Invalid ad size number from the ad response: ";
            String valueOf3 = String.valueOf(this.zzbzf.zzcet);
            throw new zzadu(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3), 0);
        }
    }

    /* access modifiers changed from: private */
    public final void zzc(int i, String str) {
        int i2 = i;
        if (i2 == 3 || i2 == -1) {
            zzakb.zzdj(str);
        } else {
            zzakb.zzdk(str);
        }
        this.zzbzf = this.zzbzf == null ? new zzaej(i2) : new zzaej(i2, this.zzbzf.zzbsu);
        zzaji zzaji = new zzaji(this.zzbuc != null ? this.zzbuc : new zzaef(this.zzccg, -1, null, null, null), this.zzbzf, this.zzbtj, null, i2, -1, this.zzbzf.zzceu, null, this.zzcch, null);
        this.zzccf.zza(zzaji);
    }

    public final void onStop() {
        synchronized (this.zzbzh) {
            if (this.zzccj != null) {
                this.zzccj.cancel();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final zzalc zza(zzang zzang, zzaol<zzaef> zzaol) {
        Context context = this.mContext;
        if (new zzadw(context).zza(zzang)) {
            zzakb.zzck("Fetching ad response from local ad request service.");
            zzaec zzaec = new zzaec(context, zzaol, this);
            zzaec.zznt();
            return zzaec;
        }
        zzakb.zzck("Fetching ad response from remote ad request service.");
        zzkb.zzif();
        if (zzamu.zzbe(context)) {
            return new zzaed(context, zzang, zzaol, this);
        }
        zzakb.zzdk("Failed to connect to remote ad request service.");
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01e9  */
    public final void zza(@NonNull zzaej zzaej) {
        Boolean bool;
        JSONObject jSONObject;
        Bundle bundle;
        zzakb.zzck("Received ad response.");
        this.zzbzf = zzaej;
        long elapsedRealtime = zzbv.zzer().elapsedRealtime();
        synchronized (this.zzbzh) {
            bool = null;
            this.zzccj = null;
        }
        zzbv.zzeo().zzqh().zzae(this.zzbzf.zzcdr);
        if (((Boolean) zzkb.zzik().zzd(zznk.zzayy)).booleanValue()) {
            if (this.zzbzf.zzced) {
                zzbv.zzeo().zzqh().zzcp(this.zzbuc.zzacp);
            } else {
                zzbv.zzeo().zzqh().zzcq(this.zzbuc.zzacp);
            }
        }
        try {
            if (this.zzbzf.errorCode == -2 || this.zzbzf.errorCode == -3) {
                if (this.zzbzf.errorCode != -3) {
                    if (TextUtils.isEmpty(this.zzbzf.zzceo)) {
                        throw new zzadu("No fill from ad server.", 3);
                    }
                    zzbv.zzeo().zzqh().zzab(this.zzbzf.zzcdd);
                    if (this.zzbzf.zzceq) {
                        this.zzbtj = new zzwy(this.zzbzf.zzceo);
                        zzbv.zzeo().zzaa(this.zzbtj.zzbss);
                    } else {
                        zzbv.zzeo().zzaa(this.zzbzf.zzbss);
                    }
                    if (!TextUtils.isEmpty(this.zzbzf.zzcds)) {
                        if (((Boolean) zzkb.zzik().zzd(zznk.zzbdj)).booleanValue()) {
                            zzakb.zzck("Received cookie from server. Setting webview cookie in CookieManager.");
                            CookieManager zzax = zzbv.zzem().zzax(this.mContext);
                            if (zzax != null) {
                                zzax.setCookie("googleads.g.doubleclick.net", this.zzbzf.zzcds);
                            }
                        }
                    }
                }
                zzjn zza = this.zzbuc.zzacv.zzard != null ? zza(this.zzbuc) : null;
                zzbv.zzeo().zzqh().zzac(this.zzbzf.zzcfa);
                zzbv.zzeo().zzqh().zzad(this.zzbzf.zzcfm);
                if (!TextUtils.isEmpty(this.zzbzf.zzcey)) {
                    try {
                        jSONObject = new JSONObject(this.zzbzf.zzcey);
                    } catch (Exception e) {
                        zzakb.zzb("Error parsing the JSON for Active View.", e);
                    }
                    if (this.zzbzf.zzcfo == 2) {
                        bool = Boolean.valueOf(true);
                        zzjj zzjj = this.zzbuc.zzccv;
                        Bundle bundle2 = zzjj.zzaqg != null ? zzjj.zzaqg : new Bundle();
                        if (bundle2.getBundle(AdMobAdapter.class.getName()) != null) {
                            bundle = bundle2.getBundle(AdMobAdapter.class.getName());
                        } else {
                            Bundle bundle3 = new Bundle();
                            bundle2.putBundle(AdMobAdapter.class.getName(), bundle3);
                            bundle = bundle3;
                        }
                        bundle.putBoolean("render_test_label", true);
                    }
                    if (this.zzbzf.zzcfo == 1) {
                        bool = Boolean.valueOf(false);
                    }
                    zzaji zzaji = new zzaji(this.zzbuc, this.zzbzf, this.zzbtj, zza, -2, elapsedRealtime, this.zzbzf.zzceu, jSONObject, this.zzcch, this.zzbzf.zzcfo != 0 ? Boolean.valueOf(zzamm.zzo(this.zzbuc.zzccv)) : bool);
                    this.zzccf.zza(zzaji);
                    zzakk.zzcrm.removeCallbacks(this.zzbzg);
                    return;
                }
                jSONObject = null;
                if (this.zzbzf.zzcfo == 2) {
                }
                if (this.zzbzf.zzcfo == 1) {
                }
                zzaji zzaji2 = new zzaji(this.zzbuc, this.zzbzf, this.zzbtj, zza, -2, elapsedRealtime, this.zzbzf.zzceu, jSONObject, this.zzcch, this.zzbzf.zzcfo != 0 ? Boolean.valueOf(zzamm.zzo(this.zzbuc.zzccv)) : bool);
                this.zzccf.zza(zzaji2);
                zzakk.zzcrm.removeCallbacks(this.zzbzg);
                return;
            }
            int i = this.zzbzf.errorCode;
            StringBuilder sb = new StringBuilder(66);
            sb.append("There was a problem getting an ad response. ErrorCode: ");
            sb.append(i);
            throw new zzadu(sb.toString(), this.zzbzf.errorCode);
        } catch (JSONException e2) {
            zzakb.zzb("Could not parse mediation config.", e2);
            String str = "Could not parse mediation config: ";
            String valueOf = String.valueOf(this.zzbzf.zzceo);
            throw new zzadu(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), 0);
        } catch (zzadu e3) {
            zzc(e3.getErrorCode(), e3.getMessage());
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzb(zzii zzii) {
        zzii.zzanm.zzamu = this.zzccg.zzccw.packageName;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzc(zzii zzii) {
        zzii.zzanh = this.zzccg.zzcdi;
    }

    public final void zzdn() {
        zzakb.zzck("AdLoaderBackgroundTask started.");
        this.zzbzg = new zzads(this);
        zzakk.zzcrm.postDelayed(this.zzbzg, ((Long) zzkb.zzik().zzd(zznk.zzban)).longValue());
        long elapsedRealtime = zzbv.zzer().elapsedRealtime();
        if (((Boolean) zzkb.zzik().zzd(zznk.zzbak)).booleanValue() && this.zzccg.zzccv.extras != null) {
            String string = this.zzccg.zzccv.extras.getString("_ad");
            if (string != null) {
                zzaef zzaef = new zzaef(this.zzccg, elapsedRealtime, null, null, null);
                this.zzbuc = zzaef;
                zza(zzafs.zza(this.mContext, this.zzbuc, string));
                return;
            }
        }
        zzaop zzaop = new zzaop();
        zzaki.zzb(new zzadt(this, zzaop));
        String zzz = zzbv.zzfh().zzz(this.mContext);
        String zzaa = zzbv.zzfh().zzaa(this.mContext);
        String zzab = zzbv.zzfh().zzab(this.mContext);
        zzbv.zzfh().zzg(this.mContext, zzab);
        zzaef zzaef2 = new zzaef(this.zzccg, elapsedRealtime, zzz, zzaa, zzab);
        this.zzbuc = zzaef2;
        zzaop.zzk(this.zzbuc);
    }
}
