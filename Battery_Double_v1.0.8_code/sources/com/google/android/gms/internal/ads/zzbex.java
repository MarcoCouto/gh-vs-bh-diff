package com.google.android.gms.internal.ads;

import com.github.mikephil.charting.utils.Utils;

public enum zzbex {
    INT(Integer.valueOf(0)),
    LONG(Long.valueOf(0)),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(Utils.DOUBLE_EPSILON)),
    BOOLEAN(Boolean.valueOf(false)),
    STRING(""),
    BYTE_STRING(zzbah.zzdpq),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzdvg;

    private zzbex(Object obj) {
        this.zzdvg = obj;
    }
}
