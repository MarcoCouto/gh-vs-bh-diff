package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;

@zzadh
@ParametersAreNonnullByDefault
public final class zzxa extends zzxu {
    private final Object mLock = new Object();
    @GuardedBy("mLock")
    private zzxf zzbtf;
    @GuardedBy("mLock")
    private zzwz zzbtg;

    public final void onAdClicked() {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzce();
            }
        }
    }

    public final void onAdClosed() {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzcf();
            }
        }
    }

    public final void onAdFailedToLoad(int i) {
        synchronized (this.mLock) {
            if (this.zzbtf != null) {
                this.zzbtf.zzx(i == 3 ? 1 : 2);
                this.zzbtf = null;
            }
        }
    }

    public final void onAdImpression() {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzcj();
            }
        }
    }

    public final void onAdLeftApplication() {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzcg();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        return;
     */
    public final void onAdLoaded() {
        synchronized (this.mLock) {
            if (this.zzbtf != null) {
                this.zzbtf.zzx(0);
                this.zzbtf = null;
            } else if (this.zzbtg != null) {
                this.zzbtg.zzci();
            }
        }
    }

    public final void onAdOpened() {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzch();
            }
        }
    }

    public final void onAppEvent(String str, String str2) {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzb(str, str2);
            }
        }
    }

    public final void onVideoEnd() {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zzcd();
            }
        }
    }

    public final void zza(@Nullable zzwz zzwz) {
        synchronized (this.mLock) {
            this.zzbtg = zzwz;
        }
    }

    public final void zza(zzxf zzxf) {
        synchronized (this.mLock) {
            this.zzbtf = zzxf;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        return;
     */
    public final void zza(zzxw zzxw) {
        synchronized (this.mLock) {
            if (this.zzbtf != null) {
                this.zzbtf.zza(0, zzxw);
                this.zzbtf = null;
            } else if (this.zzbtg != null) {
                this.zzbtg.zzci();
            }
        }
    }

    public final void zzb(zzqs zzqs, String str) {
        synchronized (this.mLock) {
            if (this.zzbtg != null) {
                this.zzbtg.zza(zzqs, str);
            }
        }
    }

    public final void zzbj(String str) {
    }
}
