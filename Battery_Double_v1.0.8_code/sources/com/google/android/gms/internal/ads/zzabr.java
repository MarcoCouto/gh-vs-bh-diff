package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.common.util.VisibleForTesting;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

@zzadh
public final class zzabr extends zzabh {
    /* access modifiers changed from: private */
    public final zzaqw zzbnd;
    private zzwy zzbtj;
    @VisibleForTesting
    private zzww zzbzq;
    protected zzxe zzbzr;
    /* access modifiers changed from: private */
    public boolean zzbzs;
    private final zznx zzvr;
    private zzxn zzwh;

    zzabr(Context context, zzaji zzaji, zzxn zzxn, zzabm zzabm, zznx zznx, zzaqw zzaqw) {
        super(context, zzaji, zzabm);
        this.zzwh = zzxn;
        this.zzbtj = zzaji.zzcod;
        this.zzvr = zznx;
        this.zzbnd = zzaqw;
    }

    public final void onStop() {
        synchronized (this.zzbzh) {
            super.onStop();
            if (this.zzbzq != null) {
                this.zzbzq.cancel();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final zzajh zzaa(int i) {
        zzwy zzwy;
        boolean z;
        String str;
        long j;
        zzael zzael;
        String str2;
        zzwy zzwy2;
        boolean z2;
        String str3;
        long j2;
        int i2;
        zzaef zzaef = this.zzbze.zzcgs;
        zzjj zzjj = zzaef.zzccv;
        zzaqw zzaqw = this.zzbnd;
        List<String> list = this.zzbzf.zzbsn;
        List<String> list2 = this.zzbzf.zzbso;
        List<String> list3 = this.zzbzf.zzces;
        int i3 = this.zzbzf.orientation;
        long j3 = this.zzbzf.zzbsu;
        String str4 = zzaef.zzccy;
        boolean z3 = this.zzbzf.zzceq;
        zzwx zzwx = this.zzbzr != null ? this.zzbzr.zzbtw : null;
        zzxq zzxq = this.zzbzr != null ? this.zzbzr.zzbtx : null;
        String name = this.zzbzr != null ? this.zzbzr.zzbty : AdMobAdapter.class.getName();
        zzwy zzwy3 = this.zzbtj;
        zzxa zzxa = this.zzbzr != null ? this.zzbzr.zzbtz : null;
        zzwx zzwx2 = zzwx;
        zzxq zzxq2 = zzxq;
        long j4 = this.zzbzf.zzcer;
        zzjn zzjn = this.zzbze.zzacv;
        long j5 = j4;
        long j6 = this.zzbzf.zzcep;
        long j7 = this.zzbze.zzcoh;
        long j8 = this.zzbzf.zzceu;
        String str5 = this.zzbzf.zzcev;
        JSONObject jSONObject = this.zzbze.zzcob;
        zzaig zzaig = this.zzbzf.zzcfe;
        List<String> list4 = this.zzbzf.zzcff;
        List<String> list5 = this.zzbzf.zzcfg;
        zzjn zzjn2 = zzjn;
        boolean z4 = this.zzbtj != null ? this.zzbtj.zzbsz : false;
        zzael zzael2 = this.zzbzf.zzcfi;
        if (this.zzbzq != null) {
            List zzme = this.zzbzq.zzme();
            zzael = zzael2;
            String str6 = "";
            if (zzme == null) {
                zzwy = zzwy3;
                str2 = str6.toString();
                str = str4;
                z = z3;
                j = j8;
            } else {
                Iterator it = zzme.iterator();
                while (it.hasNext()) {
                    Iterator it2 = it;
                    zzxe zzxe = (zzxe) it.next();
                    if (zzxe != null) {
                        j2 = j8;
                        if (zzxe.zzbtw == null || TextUtils.isEmpty(zzxe.zzbtw.zzbru)) {
                            zzwy2 = zzwy3;
                            str3 = str4;
                            z2 = z3;
                        } else {
                            String valueOf = String.valueOf(str6);
                            String str7 = zzxe.zzbtw.zzbru;
                            switch (zzxe.zzbtv) {
                                case -1:
                                    i2 = 4;
                                    break;
                                case 0:
                                    str3 = str4;
                                    z2 = z3;
                                    i2 = 0;
                                    break;
                                case 1:
                                    str3 = str4;
                                    z2 = z3;
                                    i2 = 1;
                                    break;
                                case 3:
                                    i2 = 2;
                                    break;
                                case 4:
                                    i2 = 3;
                                    break;
                                case 5:
                                    i2 = 5;
                                    break;
                                default:
                                    i2 = 6;
                                    break;
                            }
                            str3 = str4;
                            z2 = z3;
                            long j9 = zzxe.zzbub;
                            zzwy2 = zzwy3;
                            StringBuilder sb = new StringBuilder(33 + String.valueOf(str7).length());
                            sb.append(str7);
                            sb.append(".");
                            sb.append(i2);
                            sb.append(".");
                            sb.append(j9);
                            String sb2 = sb.toString();
                            StringBuilder sb3 = new StringBuilder(1 + String.valueOf(valueOf).length() + String.valueOf(sb2).length());
                            sb3.append(valueOf);
                            sb3.append(sb2);
                            sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            str6 = sb3.toString();
                        }
                    } else {
                        zzwy2 = zzwy3;
                        str3 = str4;
                        z2 = z3;
                        j2 = j8;
                    }
                    it = it2;
                    j8 = j2;
                    str4 = str3;
                    z3 = z2;
                    zzwy3 = zzwy2;
                }
                zzwy = zzwy3;
                str = str4;
                z = z3;
                j = j8;
                str2 = str6.substring(0, Math.max(0, str6.length() - 1));
            }
        } else {
            zzwy = zzwy3;
            zzael = zzael2;
            str = str4;
            z = z3;
            j = j8;
            str2 = null;
        }
        List<String> list6 = this.zzbzf.zzbsr;
        String str8 = this.zzbzf.zzcfl;
        zzhs zzhs = this.zzbze.zzcoq;
        boolean z5 = this.zzbzf.zzzl;
        boolean z6 = z5;
        zzhs zzhs2 = zzhs;
        zzwx zzwx3 = zzwx2;
        String str9 = str8;
        zzxq zzxq3 = zzxq2;
        zzwy zzwy4 = zzwy;
        zzajh zzajh = new zzajh(zzjj, zzaqw, list, i, list2, list3, i3, j3, str, z, zzwx3, zzxq3, name, zzwy4, zzxa, j5, zzjn2, j6, j7, j, str5, jSONObject, null, zzaig, list4, list5, z4, zzael, str2, list6, str9, zzhs2, z6, this.zzbze.zzcor, this.zzbzf.zzcfp, this.zzbzf.zzbsp, this.zzbzf.zzzm, this.zzbzf.zzcfq);
        return zzajh;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: type inference failed for: r3v3, types: [com.google.android.gms.internal.ads.zzww] */
    /* JADX WARNING: type inference failed for: r18v0, types: [com.google.android.gms.internal.ads.zzxk] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.google.android.gms.internal.ads.zzxh] */
    /* JADX WARNING: type inference failed for: r18v2, types: [com.google.android.gms.internal.ads.zzxk] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.google.android.gms.internal.ads.zzxh] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r18v2, types: [com.google.android.gms.internal.ads.zzxk]
  assigns: [com.google.android.gms.internal.ads.zzxk, com.google.android.gms.internal.ads.zzxh]
  uses: [com.google.android.gms.internal.ads.zzxk, com.google.android.gms.internal.ads.zzww, com.google.android.gms.internal.ads.zzxh]
  mth insns count: 154
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0104  */
    /* JADX WARNING: Unknown variable types count: 3 */
    public final void zze(long j) throws zzabk {
        ? r3;
        boolean z;
        synchronized (this.zzbzh) {
            try {
                if (this.zzbtj.zzbsx != -1) {
                    long j2 = j;
                    ? zzxh = new zzxh(this.mContext, this.zzbze.zzcgs, this.zzwh, this.zzbtj, this.zzbzf.zzare, this.zzbzf.zzarg, this.zzbzf.zzcfj, j2, ((Long) zzkb.zzik().zzd(zznk.zzbao)).longValue(), 2, this.zzbze.zzcor);
                    r3 = zzxh;
                } else {
                    ? zzxk = new zzxk(this.mContext, this.zzbze.zzcgs, this.zzwh, this.zzbtj, this.zzbzf.zzare, this.zzbzf.zzarg, this.zzbzf.zzcfj, j, ((Long) zzkb.zzik().zzd(zznk.zzbao)).longValue(), this.zzvr, this.zzbze.zzcor);
                    r3 = zzxk;
                }
                this.zzbzq = r3;
            } catch (Throwable th) {
                while (true) {
                    throw th;
                }
            }
        }
        ArrayList arrayList = new ArrayList(this.zzbtj.zzbsm);
        Bundle bundle = this.zzbze.zzcgs.zzccv.zzaqg;
        String str = "com.google.ads.mediation.admob.AdMobAdapter";
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle(str);
            if (bundle2 != null) {
                z = bundle2.getBoolean("_skipMediation");
                if (z) {
                    ListIterator listIterator = arrayList.listIterator();
                    while (listIterator.hasNext()) {
                        if (!((zzwx) listIterator.next()).zzbrt.contains(str)) {
                            listIterator.remove();
                        }
                    }
                }
                this.zzbzr = this.zzbzq.zzh(arrayList);
                switch (this.zzbzr.zzbtv) {
                    case 0:
                        if (this.zzbzr.zzbtw != null && this.zzbzr.zzbtw.zzbsf != null) {
                            CountDownLatch countDownLatch = new CountDownLatch(1);
                            zzakk.zzcrm.post(new zzabs(this, countDownLatch));
                            try {
                                countDownLatch.await(10, TimeUnit.SECONDS);
                                synchronized (this.zzbzh) {
                                    try {
                                        if (!this.zzbzs) {
                                            throw new zzabk("View could not be prepared", 0);
                                        } else if (this.zzbnd.isDestroyed()) {
                                            throw new zzabk("Assets not loaded, web view is destroyed", 0);
                                        }
                                    } catch (Throwable th2) {
                                        throw th2;
                                    }
                                }
                                return;
                            } catch (InterruptedException e) {
                                String valueOf = String.valueOf(e);
                                StringBuilder sb = new StringBuilder(38 + String.valueOf(valueOf).length());
                                sb.append("Interrupted while waiting for latch : ");
                                sb.append(valueOf);
                                throw new zzabk(sb.toString(), 0);
                            }
                        } else {
                            return;
                        }
                    case 1:
                        throw new zzabk("No fill from any mediation ad networks.", 3);
                    default:
                        int i = this.zzbzr.zzbtv;
                        StringBuilder sb2 = new StringBuilder(40);
                        sb2.append("Unexpected mediation result: ");
                        sb2.append(i);
                        throw new zzabk(sb2.toString(), 0);
                }
            }
        }
        z = false;
        if (z) {
        }
        this.zzbzr = this.zzbzq.zzh(arrayList);
        switch (this.zzbzr.zzbtv) {
            case 0:
                break;
            case 1:
                break;
        }
    }
}
