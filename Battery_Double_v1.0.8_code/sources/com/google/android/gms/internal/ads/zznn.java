package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.ParametersAreNonnullByDefault;

@zzadh
@ParametersAreNonnullByDefault
public final class zznn {
    @VisibleForTesting
    private Context mContext;
    @VisibleForTesting
    private String zzaej;
    @VisibleForTesting
    private String zzbfx;
    @VisibleForTesting
    private BlockingQueue<zznx> zzbfz = new ArrayBlockingQueue(100);
    @VisibleForTesting
    private ExecutorService zzbga;
    @VisibleForTesting
    private LinkedHashMap<String, String> zzbgb = new LinkedHashMap<>();
    @VisibleForTesting
    private Map<String, zznr> zzbgc = new HashMap();
    private AtomicBoolean zzbgd;
    private File zzbge;

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0097 A[SYNTHETIC, Splitter:B:29:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a6 A[SYNTHETIC, Splitter:B:34:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0000 A[SYNTHETIC] */
    public final void zzjf() {
        while (true) {
            try {
                zznx zznx = (zznx) this.zzbfz.take();
                String zzjk = zznx.zzjk();
                if (!TextUtils.isEmpty(zzjk)) {
                    Map zza = zza(this.zzbgb, zznx.zzjl());
                    Builder buildUpon = Uri.parse(this.zzbfx).buildUpon();
                    for (Entry entry : zza.entrySet()) {
                        buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
                    }
                    StringBuilder sb = new StringBuilder(buildUpon.build().toString());
                    sb.append("&it=");
                    sb.append(zzjk);
                    String sb2 = sb.toString();
                    if (this.zzbgd.get()) {
                        File file = this.zzbge;
                        if (file != null) {
                            FileOutputStream fileOutputStream = null;
                            try {
                                FileOutputStream fileOutputStream2 = new FileOutputStream(file, true);
                                try {
                                    fileOutputStream2.write(sb2.getBytes());
                                    fileOutputStream2.write(10);
                                } catch (IOException e) {
                                    e = e;
                                    fileOutputStream = fileOutputStream2;
                                    try {
                                        zzakb.zzc("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                                        if (fileOutputStream == null) {
                                        }
                                    } catch (Throwable th) {
                                        th = th;
                                        if (fileOutputStream != null) {
                                            try {
                                                fileOutputStream.close();
                                            } catch (IOException e2) {
                                                zzakb.zzc("CsiReporter: Cannot close file: sdk_csi_data.txt.", e2);
                                            }
                                        }
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    fileOutputStream = fileOutputStream2;
                                    if (fileOutputStream != null) {
                                    }
                                    throw th;
                                }
                                try {
                                    fileOutputStream2.close();
                                } catch (IOException e3) {
                                    zzakb.zzc("CsiReporter: Cannot close file: sdk_csi_data.txt.", e3);
                                }
                            } catch (IOException e4) {
                                e = e4;
                                zzakb.zzc("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                                if (fileOutputStream == null) {
                                    fileOutputStream.close();
                                }
                            }
                        } else {
                            zzakb.zzdk("CsiReporter: File doesn't exists. Cannot write CSI data to file.");
                        }
                    } else {
                        zzbv.zzek();
                        zzakk.zzd(this.mContext, this.zzaej, sb2);
                    }
                }
            } catch (InterruptedException e5) {
                zzakb.zzc("CsiReporter:reporter interrupted", e5);
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final Map<String, String> zza(Map<String, String> map, @Nullable Map<String, String> map2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        if (map2 == null) {
            return linkedHashMap;
        }
        for (Entry entry : map2.entrySet()) {
            String str = (String) entry.getKey();
            String str2 = (String) linkedHashMap.get(str);
            linkedHashMap.put(str, zzal(str).zzd(str2, (String) entry.getValue()));
        }
        return linkedHashMap;
    }

    public final void zza(Context context, String str, String str2, Map<String, String> map) {
        this.mContext = context;
        this.zzaej = str;
        this.zzbfx = str2;
        this.zzbgd = new AtomicBoolean(false);
        this.zzbgd.set(((Boolean) zzkb.zzik().zzd(zznk.zzawj)).booleanValue());
        if (this.zzbgd.get()) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null) {
                this.zzbge = new File(externalStorageDirectory, "sdk_csi_data.txt");
            }
        }
        for (Entry entry : map.entrySet()) {
            this.zzbgb.put((String) entry.getKey(), (String) entry.getValue());
        }
        this.zzbga = Executors.newSingleThreadExecutor();
        this.zzbga.execute(new zzno(this));
        this.zzbgc.put(NativeProtocol.WEB_DIALOG_ACTION, zznr.zzbgh);
        this.zzbgc.put("ad_format", zznr.zzbgh);
        this.zzbgc.put("e", zznr.zzbgi);
    }

    public final boolean zza(zznx zznx) {
        return this.zzbfz.offer(zznx);
    }

    public final zznr zzal(String str) {
        zznr zznr = (zznr) this.zzbgc.get(str);
        return zznr != null ? zznr : zznr.zzbgg;
    }

    public final void zzg(@Nullable List<String> list) {
        if (list != null && !list.isEmpty()) {
            this.zzbgb.put("e", TextUtils.join(",", list));
        }
    }
}
