package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.wrappers.Wrappers;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
@ParametersAreNonnullByDefault
public final class zzaii implements zzait {
    /* access modifiers changed from: private */
    public static List<Future<Void>> zzcml = Collections.synchronizedList(new ArrayList());
    private static ScheduledExecutorService zzcmm = Executors.newSingleThreadScheduledExecutor();
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private final zzaiq zzciy;
    /* access modifiers changed from: private */
    @GuardedBy("mLock")
    public final zzbfm zzcmn;
    @GuardedBy("mLock")
    private final LinkedHashMap<String, zzbfu> zzcmo;
    @GuardedBy("mLock")
    private final List<String> zzcmp = new ArrayList();
    @GuardedBy("mLock")
    private final List<String> zzcmq = new ArrayList();
    private final zzaiv zzcmr;
    @VisibleForTesting
    private boolean zzcms;
    private final zzaiw zzcmt;
    private HashSet<String> zzcmu = new HashSet<>();
    private boolean zzcmv = false;
    private boolean zzcmw = false;
    private boolean zzcmx = false;

    public zzaii(Context context, zzang zzang, zzaiq zzaiq, String str, zzaiv zzaiv) {
        Preconditions.checkNotNull(zzaiq, "SafeBrowsing config is not present.");
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.mContext = context;
        this.zzcmo = new LinkedHashMap<>();
        this.zzcmr = zzaiv;
        this.zzciy = zzaiq;
        for (String lowerCase : this.zzciy.zzcnh) {
            this.zzcmu.add(lowerCase.toLowerCase(Locale.ENGLISH));
        }
        this.zzcmu.remove("cookie".toLowerCase(Locale.ENGLISH));
        zzbfm zzbfm = new zzbfm();
        zzbfm.zzamf = Integer.valueOf(8);
        zzbfm.url = str;
        zzbfm.zzech = str;
        zzbfm.zzecj = new zzbfn();
        zzbfm.zzecj.zzcnd = this.zzciy.zzcnd;
        zzbfv zzbfv = new zzbfv();
        zzbfv.zzedv = zzang.zzcw;
        zzbfv.zzedx = Boolean.valueOf(Wrappers.packageManager(this.mContext).isCallerInstantApp());
        long apkVersion = (long) GoogleApiAvailabilityLight.getInstance().getApkVersion(this.mContext);
        if (apkVersion > 0) {
            zzbfv.zzedw = Long.valueOf(apkVersion);
        }
        zzbfm.zzect = zzbfv;
        this.zzcmn = zzbfm;
        this.zzcmt = new zzaiw(this.mContext, this.zzciy.zzcnk, this);
    }

    @Nullable
    private final zzbfu zzci(String str) {
        zzbfu zzbfu;
        synchronized (this.mLock) {
            zzbfu = (zzbfu) this.zzcmo.get(str);
        }
        return zzbfu;
    }

    static final /* synthetic */ Void zzcj(String str) {
        return null;
    }

    @VisibleForTesting
    private final zzanz<Void> zzpk() {
        zzanz<Void> zza;
        zzbfu[] zzbfuArr;
        if (!((this.zzcms && this.zzciy.zzcnj) || (this.zzcmx && this.zzciy.zzcni) || (!this.zzcms && this.zzciy.zzcng))) {
            return zzano.zzi(null);
        }
        synchronized (this.mLock) {
            this.zzcmn.zzeck = new zzbfu[this.zzcmo.size()];
            this.zzcmo.values().toArray(this.zzcmn.zzeck);
            this.zzcmn.zzecu = (String[]) this.zzcmp.toArray(new String[0]);
            this.zzcmn.zzecv = (String[]) this.zzcmq.toArray(new String[0]);
            if (zzais.isEnabled()) {
                String str = this.zzcmn.url;
                String str2 = this.zzcmn.zzecl;
                StringBuilder sb = new StringBuilder(53 + String.valueOf(str).length() + String.valueOf(str2).length());
                sb.append("Sending SB report\n  url: ");
                sb.append(str);
                sb.append("\n  clickUrl: ");
                sb.append(str2);
                sb.append("\n  resources: \n");
                StringBuilder sb2 = new StringBuilder(sb.toString());
                for (zzbfu zzbfu : this.zzcmn.zzeck) {
                    sb2.append("    [");
                    sb2.append(zzbfu.zzedu.length);
                    sb2.append("] ");
                    sb2.append(zzbfu.url);
                }
                zzais.zzck(sb2.toString());
            }
            zzanz zza2 = new zzalt(this.mContext).zza(1, this.zzciy.zzcne, null, zzbfi.zzb(this.zzcmn));
            if (zzais.isEnabled()) {
                zza2.zza(new zzain(this), zzaki.zzcrj);
            }
            zza = zzano.zza(zza2, zzaik.zzcmz, zzaoe.zzcvz);
        }
        return zza;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        return;
     */
    public final void zza(String str, Map<String, String> map, int i) {
        synchronized (this.mLock) {
            if (i == 3) {
                try {
                    this.zzcmx = true;
                } catch (UnsupportedEncodingException unused) {
                    zzais.zzck("Cannot convert string to bytes, skip header.");
                } finally {
                }
            }
            if (!this.zzcmo.containsKey(str)) {
                zzbfu zzbfu = new zzbfu();
                zzbfu.zzedt = Integer.valueOf(i);
                zzbfu.zzedn = Integer.valueOf(this.zzcmo.size());
                zzbfu.url = str;
                zzbfu.zzedo = new zzbfp();
                if (this.zzcmu.size() > 0 && map != null) {
                    ArrayList arrayList = new ArrayList();
                    for (Entry entry : map.entrySet()) {
                        String str2 = entry.getKey() != null ? (String) entry.getKey() : "";
                        String str3 = entry.getValue() != null ? (String) entry.getValue() : "";
                        if (this.zzcmu.contains(str2.toLowerCase(Locale.ENGLISH))) {
                            zzbfo zzbfo = new zzbfo();
                            zzbfo.zzecx = str2.getBytes("UTF-8");
                            zzbfo.zzecy = str3.getBytes("UTF-8");
                            arrayList.add(zzbfo);
                        }
                    }
                    zzbfo[] zzbfoArr = new zzbfo[arrayList.size()];
                    arrayList.toArray(zzbfoArr);
                    zzbfu.zzedo.zzeda = zzbfoArr;
                }
                this.zzcmo.put(str, zzbfu);
            } else if (i == 3) {
                ((zzbfu) this.zzcmo.get(str)).zzedt = Integer.valueOf(i);
            }
        }
    }

    public final String[] zzb(String[] strArr) {
        return (String[]) this.zzcmt.zzc(strArr).toArray(new String[0]);
    }

    public final void zzcf(String str) {
        synchronized (this.mLock) {
            this.zzcmn.zzecl = str;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzcg(String str) {
        synchronized (this.mLock) {
            this.zzcmp.add(str);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzch(String str) {
        synchronized (this.mLock) {
            this.zzcmq.add(str);
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ zzanz zzm(Map map) throws Exception {
        if (map != null) {
            try {
                for (String str : map.keySet()) {
                    JSONArray optJSONArray = new JSONObject((String) map.get(str)).optJSONArray("matches");
                    if (optJSONArray != null) {
                        synchronized (this.mLock) {
                            int length = optJSONArray.length();
                            zzbfu zzci = zzci(str);
                            if (zzci == null) {
                                String str2 = "Cannot find the corresponding resource object for ";
                                String valueOf = String.valueOf(str);
                                zzais.zzck(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                            } else {
                                zzci.zzedu = new String[length];
                                boolean z = false;
                                for (int i = 0; i < length; i++) {
                                    zzci.zzedu[i] = optJSONArray.getJSONObject(i).getString("threat_type");
                                }
                                boolean z2 = this.zzcms;
                                if (length > 0) {
                                    z = true;
                                }
                                this.zzcms = z | z2;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                String str3 = "Failed to get SafeBrowsing metadata";
                if (((Boolean) zzkb.zzik().zzd(zznk.zzbdi)).booleanValue()) {
                    zzakb.zza(str3, e);
                }
                return zzano.zzd(new Exception("Safebrowsing report transmission failed."));
            }
        }
        if (this.zzcms) {
            synchronized (this.mLock) {
                this.zzcmn.zzamf = Integer.valueOf(9);
            }
        }
        return zzpk();
    }

    public final zzaiq zzpg() {
        return this.zzciy;
    }

    public final boolean zzph() {
        return PlatformVersion.isAtLeastKitKat() && this.zzciy.zzcnf && !this.zzcmw;
    }

    public final void zzpi() {
        this.zzcmv = true;
    }

    public final void zzpj() {
        synchronized (this.mLock) {
            zzanz zza = zzano.zza(this.zzcmr.zza(this.mContext, this.zzcmo.keySet()), (zzanj<? super A, ? extends B>) new zzaij<Object,Object>(this), zzaoe.zzcvz);
            zzanz zza2 = zzano.zza(zza, 10, TimeUnit.SECONDS, zzcmm);
            zzano.zza(zza, (zzanl<V>) new zzaim<V>(this, zza2), zzaoe.zzcvz);
            zzcml.add(zza2);
        }
    }

    public final void zzr(View view) {
        if (this.zzciy.zzcnf && !this.zzcmw) {
            zzbv.zzek();
            Bitmap zzt = zzakk.zzt(view);
            if (zzt == null) {
                zzais.zzck("Failed to capture the webview bitmap.");
                return;
            }
            this.zzcmw = true;
            zzakk.zzd(new zzail(this, zzt));
        }
    }
}
