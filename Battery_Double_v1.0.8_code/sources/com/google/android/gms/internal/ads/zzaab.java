package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.CollectionUtils;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.Map;
import java.util.Set;

@zzadh
public final class zzaab extends zzaal {
    private static final Set<String> zzbvy = CollectionUtils.setOf((T[]) new String[]{"top-left", "top-right", "top-center", TtmlNode.CENTER, "bottom-left", "bottom-right", "bottom-center"});
    private final Object mLock = new Object();
    private zzaam zzbmy;
    private final zzaqw zzbnd;
    private final Activity zzbvp;
    private String zzbvz = "top-right";
    private boolean zzbwa = true;
    private int zzbwb = 0;
    private int zzbwc = 0;
    private int zzbwd = 0;
    private int zzbwe = 0;
    private zzasi zzbwf;
    private ImageView zzbwg;
    private LinearLayout zzbwh;
    private PopupWindow zzbwi;
    private RelativeLayout zzbwj;
    private ViewGroup zzbwk;
    private int zzuq = -1;
    private int zzur = -1;

    public zzaab(zzaqw zzaqw, zzaam zzaam) {
        super(zzaqw, "resize");
        this.zzbnd = zzaqw;
        this.zzbvp = zzaqw.zzto();
        this.zzbmy = zzaam;
    }

    private final void zza(int i, int i2) {
        zzb(i, i2 - zzbv.zzek().zzh(this.zzbvp)[0], this.zzuq, this.zzur);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0095, code lost:
        r5 = r9.zzbwc + r9.zzbwe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b6, code lost:
        r5 = ((r9.zzbwc + r9.zzbwe) + r9.zzur) - 50;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e9, code lost:
        if (r0 < 0) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ec, code lost:
        if ((r0 + 50) > r3) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f0, code lost:
        if (r5 < r1[0]) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00f5, code lost:
        if ((r5 + 50) <= r1[1]) goto L_0x00f8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0107  */
    private final int[] zzne() {
        boolean z;
        String str;
        char c;
        int i;
        int i2;
        int[] zzg = zzbv.zzek().zzg(this.zzbvp);
        int[] zzh = zzbv.zzek().zzh(this.zzbvp);
        int i3 = zzg[0];
        int i4 = zzg[1];
        if (this.zzuq < 50 || this.zzuq > i3) {
            str = "Width is too small or too large.";
        } else if (this.zzur < 50 || this.zzur > i4) {
            str = "Height is too small or too large.";
        } else if (this.zzur == i4 && this.zzuq == i3) {
            str = "Cannot resize to a full-screen ad.";
        } else {
            if (this.zzbwa) {
                String str2 = this.zzbvz;
                switch (str2.hashCode()) {
                    case -1364013995:
                        if (str2.equals(TtmlNode.CENTER)) {
                            c = 2;
                            break;
                        }
                    case -1012429441:
                        if (str2.equals("top-left")) {
                            c = 0;
                            break;
                        }
                    case -655373719:
                        if (str2.equals("bottom-left")) {
                            c = 3;
                            break;
                        }
                    case 1163912186:
                        if (str2.equals("bottom-right")) {
                            c = 5;
                            break;
                        }
                    case 1288627767:
                        if (str2.equals("bottom-center")) {
                            c = 4;
                            break;
                        }
                    case 1755462605:
                        if (str2.equals("top-center")) {
                            c = 1;
                            break;
                        }
                    default:
                        c = 65535;
                        break;
                }
                switch (c) {
                    case 0:
                        i2 = this.zzbwb + this.zzbwd;
                        break;
                    case 1:
                        i2 = ((this.zzbwb + this.zzbwd) + (this.zzuq / 2)) - 25;
                        break;
                    case 2:
                        i = ((this.zzbwb + this.zzbwd) + (this.zzuq / 2)) - 25;
                        int i5 = ((this.zzbwc + this.zzbwe) + (this.zzur / 2)) - 25;
                        break;
                    case 3:
                        i = this.zzbwb + this.zzbwd;
                        break;
                    case 4:
                        i = ((this.zzbwb + this.zzbwd) + (this.zzuq / 2)) - 25;
                        break;
                    case 5:
                        i = ((this.zzbwb + this.zzbwd) + this.zzuq) - 50;
                        break;
                    default:
                        i2 = ((this.zzbwb + this.zzbwd) + this.zzuq) - 50;
                        break;
                }
            }
            z = true;
            if (z) {
                return null;
            }
            if (this.zzbwa) {
                return new int[]{this.zzbwb + this.zzbwd, this.zzbwc + this.zzbwe};
            }
            int[] zzg2 = zzbv.zzek().zzg(this.zzbvp);
            int[] zzh2 = zzbv.zzek().zzh(this.zzbvp);
            int i6 = zzg2[0];
            int i7 = this.zzbwb + this.zzbwd;
            int i8 = this.zzbwc + this.zzbwe;
            int i9 = i7 < 0 ? 0 : this.zzuq + i7 > i6 ? i6 - this.zzuq : i7;
            if (i8 < zzh2[0]) {
                i8 = zzh2[0];
            } else if (this.zzur + i8 > zzh2[1]) {
                i8 = zzh2[1] - this.zzur;
            }
            return new int[]{i9, i8};
        }
        zzakb.zzdk(str);
        z = false;
        if (z) {
        }
    }

    public final void zza(int i, int i2, boolean z) {
        synchronized (this.mLock) {
            this.zzbwb = i;
            this.zzbwc = i2;
            if (this.zzbwi != null && z) {
                int[] zzne = zzne();
                if (zzne != null) {
                    PopupWindow popupWindow = this.zzbwi;
                    zzkb.zzif();
                    int zza = zzamu.zza((Context) this.zzbvp, zzne[0]);
                    zzkb.zzif();
                    popupWindow.update(zza, zzamu.zza((Context) this.zzbvp, zzne[1]), this.zzbwi.getWidth(), this.zzbwi.getHeight());
                    zza(zzne[0], zzne[1]);
                } else {
                    zzm(true);
                }
            }
        }
    }

    public final void zzb(int i, int i2) {
        this.zzbwb = i;
        this.zzbwc = i2;
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.zzaqw.zza(com.google.android.gms.internal.ads.zzasi):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0262, code lost:
        r13.zzbwh.setOnClickListener(new com.google.android.gms.internal.ads.zzaac(r13));
        r13.zzbwh.setContentDescription("Close button");
        r13.zzbwj.addView(r13.zzbwh, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        r6 = r13.zzbwi;
        r14 = r14.getDecorView();
        com.google.android.gms.internal.ads.zzkb.zzif();
        r7 = com.google.android.gms.internal.ads.zzamu.zza((android.content.Context) r13.zzbvp, r3[0]);
        com.google.android.gms.internal.ads.zzkb.zzif();
        r6.showAtLocation(r14, 0, r7, com.google.android.gms.internal.ads.zzamu.zza((android.content.Context) r13.zzbvp, r3[1]));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        r14 = r3[0];
        r6 = r3[1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x029f, code lost:
        if (r13.zzbmy == null) goto L_0x02aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x02a1, code lost:
        r13.zzbmy.zza(r14, r6, r13.zzuq, r13.zzur);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x02aa, code lost:
        r13.zzbnd.zza(com.google.android.gms.internal.ads.zzasi.zzi(r4, r5));
        zza(r3[0], r3[1]);
        zzby("resized");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x02c0, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02c1, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x02c2, code lost:
        r1 = "Cannot show popup window: ";
        r14 = java.lang.String.valueOf(r14.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02d0, code lost:
        if (r14.length() != 0) goto L_0x02d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x02d2, code lost:
        r14 = r1.concat(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x02d7, code lost:
        r14 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02dc, code lost:
        zzbw(r14);
        r13.zzbwj.removeView(r13.zzbnd.getView());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x02ec, code lost:
        if (r13.zzbwk != null) goto L_0x02ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02ee, code lost:
        r13.zzbwk.removeView(r13.zzbwg);
        r13.zzbwk.addView(r13.zzbnd.getView());
        r13.zzbnd.zza(r13.zzbwf);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0308, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0242, code lost:
        r6.addRule(11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0249, code lost:
        r6.addRule(14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0250, code lost:
        r6.addRule(9);
     */
    public final void zzk(Map<String, String> map) {
        char c;
        synchronized (this.mLock) {
            if (this.zzbvp == null) {
                zzbw("Not an activity context. Cannot resize.");
            } else if (this.zzbnd.zzud() == null) {
                zzbw("Webview is not yet available, size is not set.");
            } else if (this.zzbnd.zzud().zzvs()) {
                zzbw("Is interstitial. Cannot resize an interstitial.");
            } else if (this.zzbnd.zzuj()) {
                zzbw("Cannot resize an expanded banner.");
            } else {
                if (!TextUtils.isEmpty((CharSequence) map.get(SettingsJsonConstants.ICON_WIDTH_KEY))) {
                    zzbv.zzek();
                    this.zzuq = zzakk.zzcv((String) map.get(SettingsJsonConstants.ICON_WIDTH_KEY));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get(SettingsJsonConstants.ICON_HEIGHT_KEY))) {
                    zzbv.zzek();
                    this.zzur = zzakk.zzcv((String) map.get(SettingsJsonConstants.ICON_HEIGHT_KEY));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("offsetX"))) {
                    zzbv.zzek();
                    this.zzbwd = zzakk.zzcv((String) map.get("offsetX"));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("offsetY"))) {
                    zzbv.zzek();
                    this.zzbwe = zzakk.zzcv((String) map.get("offsetY"));
                }
                if (!TextUtils.isEmpty((CharSequence) map.get("allowOffscreen"))) {
                    this.zzbwa = Boolean.parseBoolean((String) map.get("allowOffscreen"));
                }
                String str = (String) map.get("customClosePosition");
                if (!TextUtils.isEmpty(str)) {
                    this.zzbvz = str;
                }
                if (!(this.zzuq >= 0 && this.zzur >= 0)) {
                    zzbw("Invalid width and height options. Cannot resize.");
                    return;
                }
                Window window = this.zzbvp.getWindow();
                if (window != null) {
                    if (window.getDecorView() != null) {
                        int[] zzne = zzne();
                        if (zzne != null) {
                            zzkb.zzif();
                            int zza = zzamu.zza((Context) this.zzbvp, this.zzuq);
                            zzkb.zzif();
                            int zza2 = zzamu.zza((Context) this.zzbvp, this.zzur);
                            ViewParent parent = this.zzbnd.getView().getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(this.zzbnd.getView());
                                if (this.zzbwi == null) {
                                    this.zzbwk = (ViewGroup) parent;
                                    zzbv.zzek();
                                    Bitmap zzs = zzakk.zzs(this.zzbnd.getView());
                                    this.zzbwg = new ImageView(this.zzbvp);
                                    this.zzbwg.setImageBitmap(zzs);
                                    this.zzbwf = this.zzbnd.zzud();
                                    this.zzbwk.addView(this.zzbwg);
                                } else {
                                    this.zzbwi.dismiss();
                                }
                                this.zzbwj = new RelativeLayout(this.zzbvp);
                                this.zzbwj.setBackgroundColor(0);
                                this.zzbwj.setLayoutParams(new LayoutParams(zza, zza2));
                                zzbv.zzek();
                                this.zzbwi = zzakk.zza((View) this.zzbwj, zza, zza2, false);
                                this.zzbwi.setOutsideTouchable(true);
                                this.zzbwi.setTouchable(true);
                                this.zzbwi.setClippingEnabled(!this.zzbwa);
                                this.zzbwj.addView(this.zzbnd.getView(), -1, -1);
                                this.zzbwh = new LinearLayout(this.zzbvp);
                                zzkb.zzif();
                                int zza3 = zzamu.zza((Context) this.zzbvp, 50);
                                zzkb.zzif();
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(zza3, zzamu.zza((Context) this.zzbvp, 50));
                                String str2 = this.zzbvz;
                                switch (str2.hashCode()) {
                                    case -1364013995:
                                        if (str2.equals(TtmlNode.CENTER)) {
                                            c = 2;
                                            break;
                                        }
                                    case -1012429441:
                                        if (str2.equals("top-left")) {
                                            c = 0;
                                            break;
                                        }
                                    case -655373719:
                                        if (str2.equals("bottom-left")) {
                                            c = 3;
                                            break;
                                        }
                                    case 1163912186:
                                        if (str2.equals("bottom-right")) {
                                            c = 5;
                                            break;
                                        }
                                    case 1288627767:
                                        if (str2.equals("bottom-center")) {
                                            c = 4;
                                            break;
                                        }
                                    case 1755462605:
                                        if (str2.equals("top-center")) {
                                            c = 1;
                                            break;
                                        }
                                }
                                c = 65535;
                                switch (c) {
                                    case 0:
                                        layoutParams.addRule(10);
                                        break;
                                    case 1:
                                        layoutParams.addRule(10);
                                        break;
                                    case 2:
                                        layoutParams.addRule(13);
                                        break;
                                    case 3:
                                        layoutParams.addRule(12);
                                        break;
                                    case 4:
                                        layoutParams.addRule(12);
                                        break;
                                    case 5:
                                        layoutParams.addRule(12);
                                        break;
                                    default:
                                        layoutParams.addRule(10);
                                        break;
                                }
                            } else {
                                zzbw("Webview is detached, probably in the middle of a resize or expand.");
                                return;
                            }
                        } else {
                            zzbw("Resize location out of screen or close button is not visible.");
                            return;
                        }
                    }
                }
                zzbw("Activity context is not ready, cannot get window or decor view.");
            }
        }
    }

    public final void zzm(boolean z) {
        synchronized (this.mLock) {
            if (this.zzbwi != null) {
                this.zzbwi.dismiss();
                this.zzbwj.removeView(this.zzbnd.getView());
                if (this.zzbwk != null) {
                    this.zzbwk.removeView(this.zzbwg);
                    this.zzbwk.addView(this.zzbnd.getView());
                    this.zzbnd.zza(this.zzbwf);
                }
                if (z) {
                    zzby("default");
                    if (this.zzbmy != null) {
                        this.zzbmy.zzcq();
                    }
                }
                this.zzbwi = null;
                this.zzbwj = null;
                this.zzbwk = null;
                this.zzbwh = null;
            }
        }
    }

    public final boolean zznf() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzbwi != null;
        }
        return z;
    }
}
