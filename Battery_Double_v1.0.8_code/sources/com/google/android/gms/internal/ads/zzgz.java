package com.google.android.gms.internal.ads;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import javax.annotation.ParametersAreNonnullByDefault;

@zzadh
@ParametersAreNonnullByDefault
public final class zzgz extends zzgq {
    private MessageDigest zzaje;
    private final int zzajh;
    private final int zzaji;

    public zzgz(int i) {
        int i2 = i / 8;
        if (i % 8 > 0) {
            i2++;
        }
        this.zzajh = i2;
        this.zzaji = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006e, code lost:
        return r1;
     */
    public final byte[] zzx(String str) {
        synchronized (this.mLock) {
            this.zzaje = zzhg();
            int i = 0;
            if (this.zzaje == null) {
                byte[] bArr = new byte[0];
                return bArr;
            }
            this.zzaje.reset();
            this.zzaje.update(str.getBytes(Charset.forName("UTF-8")));
            byte[] digest = this.zzaje.digest();
            byte[] bArr2 = new byte[(digest.length > this.zzajh ? this.zzajh : digest.length)];
            System.arraycopy(digest, 0, bArr2, 0, bArr2.length);
            if (this.zzaji % 8 > 0) {
                long j = 0;
                while (i < bArr2.length) {
                    if (i > 0) {
                        j <<= 8;
                    }
                    i++;
                    j += (long) (bArr2[i] & 255);
                }
                long j2 = j >>> (8 - (this.zzaji % 8));
                for (int i2 = this.zzajh - 1; i2 >= 0; i2--) {
                    bArr2[i2] = (byte) ((int) (j2 & 255));
                    j2 >>>= 8;
                }
            }
        }
    }
}
