package com.google.android.gms.internal.ads;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
public final class zzahx extends zzajx implements zzahw {
    private final Context mContext;
    private final Object mLock;
    private final zzaji zzbze;
    private final long zzclp;
    private final ArrayList<zzahn> zzcmd;
    private final List<zzahq> zzcme;
    private final HashSet<String> zzcmf;
    private final zzago zzcmg;

    public zzahx(Context context, zzaji zzaji, zzago zzago) {
        Context context2 = context;
        zzaji zzaji2 = zzaji;
        zzago zzago2 = zzago;
        this(context2, zzaji2, zzago2, ((Long) zzkb.zzik().zzd(zznk.zzaye)).longValue());
    }

    @VisibleForTesting
    private zzahx(Context context, zzaji zzaji, zzago zzago, long j) {
        this.zzcmd = new ArrayList<>();
        this.zzcme = new ArrayList();
        this.zzcmf = new HashSet<>();
        this.mLock = new Object();
        this.mContext = context;
        this.zzbze = zzaji;
        this.zzcmg = zzago;
        this.zzclp = j;
    }

    private final zzajh zza(int i, @Nullable String str, @Nullable zzwx zzwx) {
        boolean z;
        long j;
        String str2;
        zzjn zzjn;
        String str3;
        long j2;
        int i2;
        zzjj zzjj = this.zzbze.zzcgs.zzccv;
        List<String> list = this.zzbze.zzcos.zzbsn;
        List<String> list2 = this.zzbze.zzcos.zzbso;
        List<String> list3 = this.zzbze.zzcos.zzces;
        int i3 = this.zzbze.zzcos.orientation;
        long j3 = this.zzbze.zzcos.zzbsu;
        String str4 = this.zzbze.zzcgs.zzccy;
        boolean z2 = this.zzbze.zzcos.zzceq;
        zzwy zzwy = this.zzbze.zzcod;
        long j4 = this.zzbze.zzcos.zzcer;
        zzjn zzjn2 = this.zzbze.zzacv;
        long j5 = j4;
        zzwy zzwy2 = zzwy;
        long j6 = this.zzbze.zzcos.zzcep;
        long j7 = this.zzbze.zzcoh;
        long j8 = this.zzbze.zzcos.zzceu;
        String str5 = this.zzbze.zzcos.zzcev;
        JSONObject jSONObject = this.zzbze.zzcob;
        zzaig zzaig = this.zzbze.zzcos.zzcfe;
        List<String> list4 = this.zzbze.zzcos.zzcff;
        List<String> list5 = this.zzbze.zzcos.zzcfg;
        boolean z3 = this.zzbze.zzcos.zzcfh;
        zzael zzael = this.zzbze.zzcos.zzcfi;
        JSONObject jSONObject2 = jSONObject;
        StringBuilder sb = new StringBuilder("");
        if (this.zzcme == null) {
            str3 = sb.toString();
            zzjn = zzjn2;
            z = z2;
            str2 = str5;
            j = j8;
        } else {
            Iterator it = this.zzcme.iterator();
            while (true) {
                int i4 = 1;
                zzjn = zzjn2;
                if (it.hasNext()) {
                    zzahq zzahq = (zzahq) it.next();
                    if (zzahq != null) {
                        Iterator it2 = it;
                        if (!TextUtils.isEmpty(zzahq.zzbru)) {
                            String str6 = zzahq.zzbru;
                            String str7 = str5;
                            switch (zzahq.errorCode) {
                                case 3:
                                    break;
                                case 4:
                                    i4 = 2;
                                    break;
                                case 5:
                                    i4 = 4;
                                    break;
                                case 6:
                                    j2 = j8;
                                    i2 = 0;
                                    break;
                                case 7:
                                    i4 = 3;
                                    break;
                                default:
                                    i4 = 6;
                                    break;
                            }
                            j2 = j8;
                            i2 = i4;
                            long j9 = zzahq.zzbub;
                            boolean z4 = z2;
                            StringBuilder sb2 = new StringBuilder(33 + String.valueOf(str6).length());
                            sb2.append(str6);
                            sb2.append(".");
                            sb2.append(i2);
                            sb2.append(".");
                            sb2.append(j9);
                            sb.append(String.valueOf(sb2.toString()).concat(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
                            zzjn2 = zzjn;
                            it = it2;
                            str5 = str7;
                            j8 = j2;
                            z2 = z4;
                        } else {
                            zzjn2 = zzjn;
                            it = it2;
                        }
                    } else {
                        zzjn2 = zzjn;
                    }
                } else {
                    z = z2;
                    str2 = str5;
                    j = j8;
                    str3 = sb.substring(0, Math.max(0, sb.length() - 1));
                }
            }
        }
        List<String> list6 = this.zzbze.zzcos.zzbsr;
        String str8 = this.zzbze.zzcos.zzcfl;
        zzhs zzhs = this.zzbze.zzcoq;
        boolean z5 = this.zzbze.zzcos.zzzl;
        boolean z6 = this.zzbze.zzcor;
        boolean z7 = this.zzbze.zzcos.zzcfp;
        List<String> list7 = this.zzbze.zzcos.zzbsp;
        boolean z8 = z7;
        JSONObject jSONObject3 = jSONObject2;
        boolean z9 = z6;
        zzjn zzjn3 = zzjn;
        int i5 = i;
        boolean z10 = z5;
        boolean z11 = z;
        zzhs zzhs2 = zzhs;
        String str9 = str2;
        zzwx zzwx2 = zzwx;
        zzwy zzwy3 = zzwy2;
        List<String> list8 = list6;
        String str10 = str;
        long j10 = j5;
        long j11 = j6;
        long j12 = j7;
        long j13 = j;
        List<String> list9 = list8;
        String str11 = str8;
        zzajh zzajh = new zzajh(zzjj, null, list, i5, list2, list3, i3, j3, str4, z11, zzwx2, null, str10, zzwy3, null, j10, zzjn3, j11, j12, j13, str9, jSONObject3, null, zzaig, list4, list5, z3, zzael, str3, list9, str11, zzhs2, z10, z9, z8, list7, this.zzbze.zzcos.zzzm, this.zzbze.zzcos.zzcfq);
        return zzajh;
    }

    public final void onStop() {
    }

    public final void zza(String str, int i) {
    }

    public final void zzcb(String str) {
        synchronized (this.mLock) {
            this.zzcmf.add(str);
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:93:0x016e */
    public final void zzdn() {
        zzahn zzahn;
        Object obj;
        Iterator it;
        Iterator it2 = this.zzbze.zzcod.zzbsm.iterator();
        while (it2.hasNext()) {
            zzwx zzwx = (zzwx) it2.next();
            String str = zzwx.zzbsb;
            for (String str2 : zzwx.zzbrt) {
                if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str2) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str2)) {
                    try {
                        str2 = new JSONObject(str).getString("class_name");
                    } catch (JSONException e) {
                        it = it2;
                        zzakb.zzb("Unable to determine custom event class name, skipping...", e);
                    }
                }
                String str3 = str2;
                Object obj2 = this.mLock;
                synchronized (obj2) {
                    try {
                        zzaib zzca = this.zzcmg.zzca(str3);
                        if (!(zzca == null || zzca.zzpf() == null)) {
                            if (zzca.zzpe() != null) {
                                r1 = r1;
                                obj = obj2;
                                it = it2;
                                zzahn zzahn2 = r1;
                                zzahn zzahn3 = new zzahn(this.mContext, str3, str, zzwx, this.zzbze, zzca, this, this.zzclp);
                                zzahn2.zza(this.zzcmg.zzos());
                                this.zzcmd.add(zzahn2);
                                it2 = it;
                            }
                        }
                        obj = obj2;
                        it = it2;
                        this.zzcme.add(new zzahs().zzcd(zzwx.zzbru).zzcc(str3).zzg(0).zzad(7).zzpd());
                        it2 = it;
                    } catch (Throwable th) {
                        th = th;
                        Throwable th2 = th;
                        throw th2;
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = this.zzcmd;
        int size = arrayList.size();
        int i = 0;
        int i2 = 0;
        while (i2 < size) {
            Object obj3 = arrayList.get(i2);
            i2++;
            zzahn zzahn4 = (zzahn) obj3;
            if (hashSet.add(zzahn4.zzbth)) {
                zzahn4.zzoz();
            }
        }
        ArrayList arrayList2 = this.zzcmd;
        int size2 = arrayList2.size();
        while (true) {
            if (i >= size2) {
                break;
            }
            Object obj4 = arrayList2.get(i);
            i++;
            zzahn = (zzahn) obj4;
            try {
                zzahn.zzoz().get();
                synchronized (this.mLock) {
                    try {
                        if (!TextUtils.isEmpty(zzahn.zzbth)) {
                            this.zzcme.add(zzahn.zzpa());
                        }
                    } catch (Throwable th3) {
                        while (true) {
                            throw th3;
                        }
                    }
                }
                synchronized (this.mLock) {
                    try {
                        if (this.zzcmf.contains(zzahn.zzbth)) {
                            zzamu.zzsy.post(new zzahy(this, zza(-2, zzahn.zzbth, zzahn.zzpb())));
                            return;
                        }
                    } catch (Throwable th4) {
                        throw th4;
                    }
                }
            } catch (InterruptedException ) {
                Thread.currentThread().interrupt();
                synchronized (this.mLock) {
                    try {
                        if (!TextUtils.isEmpty(zzahn.zzbth)) {
                            this.zzcme.add(zzahn.zzpa());
                        }
                    } catch (Throwable th5) {
                        throw th5;
                    }
                }
            } catch (Exception e2) {
                try {
                    zzakb.zzc("Unable to resolve rewarded adapter.", e2);
                    synchronized (this.mLock) {
                        if (!TextUtils.isEmpty(zzahn.zzbth)) {
                            this.zzcme.add(zzahn.zzpa());
                        }
                    }
                } catch (Throwable th6) {
                    while (true) {
                        throw th6;
                    }
                }
            } catch (Throwable th7) {
                throw th7;
            }
        }
        zzamu.zzsy.post(new zzahz(this, zza(3, null, null)));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzl(zzajh zzajh) {
        this.zzcmg.zzot().zzb(zzajh);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzm(zzajh zzajh) {
        this.zzcmg.zzot().zzb(zzajh);
    }
}
