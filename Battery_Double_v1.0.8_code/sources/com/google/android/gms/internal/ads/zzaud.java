package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaud {
    static final /* synthetic */ int[] zzdhh = new int[zzayd.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    static {
        zzdhh[zzayd.LEGACY.ordinal()] = 1;
        zzdhh[zzayd.CRUNCHY.ordinal()] = 2;
        zzdhh[zzayd.TINK.ordinal()] = 3;
        try {
            zzdhh[zzayd.RAW.ordinal()] = 4;
        } catch (NoSuchFieldError unused) {
        }
    }
}
