package com.google.android.gms.internal.ads;

import com.mansoon.BatteryDouble.Config;

public final class zzhu {

    public static final class zza extends zzbbo<zza, C0013zza> implements zzbcw {
        /* access modifiers changed from: private */
        public static final zza zzakg = new zza();
        private static volatile zzbdf<zza> zzakh;

        /* renamed from: com.google.android.gms.internal.ads.zzhu$zza$zza reason: collision with other inner class name */
        public static final class C0013zza extends com.google.android.gms.internal.ads.zzbbo.zza<zza, C0013zza> implements zzbcw {
            private C0013zza() {
                super(zza.zzakg);
            }

            /* synthetic */ C0013zza(zzhv zzhv) {
                this();
            }
        }

        public enum zzb implements zzbbr {
            UNKNOWN_EVENT_TYPE(0),
            AD_REQUEST(1),
            AD_LOADED(2),
            AD_FAILED_TO_LOAD(3),
            AD_FAILED_TO_LOAD_NO_FILL(4),
            AD_IMPRESSION(5),
            AD_FIRST_CLICK(6),
            AD_SUBSEQUENT_CLICK(7),
            REQUEST_WILL_START(8),
            REQUEST_DID_END(9),
            REQUEST_WILL_UPDATE_SIGNALS(10),
            REQUEST_DID_UPDATE_SIGNALS(11),
            REQUEST_WILL_BUILD_URL(12),
            REQUEST_DID_BUILD_URL(13),
            REQUEST_WILL_MAKE_NETWORK_REQUEST(14),
            REQUEST_DID_RECEIVE_NETWORK_RESPONSE(15),
            REQUEST_WILL_PROCESS_RESPONSE(16),
            REQUEST_DID_PROCESS_RESPONSE(17),
            REQUEST_WILL_RENDER(18),
            REQUEST_DID_RENDER(19),
            REQUEST_WILL_UPDATE_GMS_SIGNALS(1000),
            REQUEST_DID_UPDATE_GMS_SIGNALS(1001),
            REQUEST_FAILED_TO_UPDATE_GMS_SIGNALS(1002),
            REQUEST_FAILED_TO_BUILD_URL(1003),
            REQUEST_FAILED_TO_MAKE_NETWORK_REQUEST(1004),
            REQUEST_FAILED_TO_PROCESS_RESPONSE(Config.NOTIFICATION_TEMPERATURE_HIGH),
            REQUEST_FAILED_TO_UPDATE_SIGNALS(1006),
            BANNER_SIZE_INVALID(10000),
            BANNER_SIZE_VALID(10001);
            
            private static final zzbbs<zzb> zzall = null;
            private final int value;

            static {
                zzall = new zzhw();
            }

            private zzb(int i) {
                this.value = i;
            }

            public static zzb zzp(int i) {
                switch (i) {
                    case 0:
                        return UNKNOWN_EVENT_TYPE;
                    case 1:
                        return AD_REQUEST;
                    case 2:
                        return AD_LOADED;
                    case 3:
                        return AD_FAILED_TO_LOAD;
                    case 4:
                        return AD_FAILED_TO_LOAD_NO_FILL;
                    case 5:
                        return AD_IMPRESSION;
                    case 6:
                        return AD_FIRST_CLICK;
                    case 7:
                        return AD_SUBSEQUENT_CLICK;
                    case 8:
                        return REQUEST_WILL_START;
                    case 9:
                        return REQUEST_DID_END;
                    case 10:
                        return REQUEST_WILL_UPDATE_SIGNALS;
                    case 11:
                        return REQUEST_DID_UPDATE_SIGNALS;
                    case 12:
                        return REQUEST_WILL_BUILD_URL;
                    case 13:
                        return REQUEST_DID_BUILD_URL;
                    case 14:
                        return REQUEST_WILL_MAKE_NETWORK_REQUEST;
                    case 15:
                        return REQUEST_DID_RECEIVE_NETWORK_RESPONSE;
                    case 16:
                        return REQUEST_WILL_PROCESS_RESPONSE;
                    case 17:
                        return REQUEST_DID_PROCESS_RESPONSE;
                    case 18:
                        return REQUEST_WILL_RENDER;
                    case 19:
                        return REQUEST_DID_RENDER;
                    default:
                        switch (i) {
                            case 1000:
                                return REQUEST_WILL_UPDATE_GMS_SIGNALS;
                            case 1001:
                                return REQUEST_DID_UPDATE_GMS_SIGNALS;
                            case 1002:
                                return REQUEST_FAILED_TO_UPDATE_GMS_SIGNALS;
                            case 1003:
                                return REQUEST_FAILED_TO_BUILD_URL;
                            case 1004:
                                return REQUEST_FAILED_TO_MAKE_NETWORK_REQUEST;
                            case Config.NOTIFICATION_TEMPERATURE_HIGH /*1005*/:
                                return REQUEST_FAILED_TO_PROCESS_RESPONSE;
                            case 1006:
                                return REQUEST_FAILED_TO_UPDATE_SIGNALS;
                            default:
                                switch (i) {
                                    case 10000:
                                        return BANNER_SIZE_INVALID;
                                    case 10001:
                                        return BANNER_SIZE_VALID;
                                    default:
                                        return null;
                                }
                        }
                }
            }

            public final int zzhq() {
                return this.value;
            }
        }

        static {
            zzbbo.zza(zza.class, zzakg);
        }

        private zza() {
        }

        /* JADX WARNING: type inference failed for: r1v10, types: [com.google.android.gms.internal.ads.zzbdf<com.google.android.gms.internal.ads.zzhu$zza>] */
        /* JADX WARNING: type inference failed for: r1v11, types: [java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v12, types: [com.google.android.gms.internal.ads.zzbdf<com.google.android.gms.internal.ads.zzhu$zza>, com.google.android.gms.internal.ads.zzbbo$zzb] */
        /* JADX WARNING: type inference failed for: r1v15 */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v12, types: [com.google.android.gms.internal.ads.zzbdf<com.google.android.gms.internal.ads.zzhu$zza>, com.google.android.gms.internal.ads.zzbbo$zzb]
  assigns: [com.google.android.gms.internal.ads.zzbbo$zzb]
  uses: [com.google.android.gms.internal.ads.zzbdf<com.google.android.gms.internal.ads.zzhu$zza>]
  mth insns count: 34
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 2 */
        public final Object zza(int i, Object obj, Object obj2) {
            ? r1;
            switch (zzhv.zzakf[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0013zza(null);
                case 3:
                    return zza((zzbcu) zzakg, "\u0001\u0000", (Object[]) null);
                case 4:
                    return zzakg;
                case 5:
                    zzbdf<zza> zzbdf = zzakh;
                    if (zzbdf != null) {
                        return zzbdf;
                    }
                    synchronized (zza.class) {
                        r1 = zzakh;
                        if (r1 == 0) {
                            ? zzb2 = new com.google.android.gms.internal.ads.zzbbo.zzb(zzakg);
                            zzakh = zzb2;
                            r1 = zzb2;
                        }
                    }
                    return r1;
                case 6:
                    return Byte.valueOf(1);
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
