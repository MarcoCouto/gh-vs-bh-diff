package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.IOUtils;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
public final class zzafn extends zzaeo {
    private static final Object sLock = new Object();
    @GuardedBy("sLock")
    private static zzafn zzchh;
    private final Context mContext;
    private final zzafm zzchi;
    private final ScheduledExecutorService zzchj = Executors.newSingleThreadScheduledExecutor();

    private zzafn(Context context, zzafm zzafm) {
        this.mContext = context;
        this.zzchi = zzafm;
    }

    private static zzaej zza(Context context, zzafm zzafm, zzaef zzaef, ScheduledExecutorService scheduledExecutorService) {
        char c;
        Context context2 = context;
        zzafm zzafm2 = zzafm;
        zzaef zzaef2 = zzaef;
        ScheduledExecutorService scheduledExecutorService2 = scheduledExecutorService;
        zzakb.zzck("Starting ad request from service using: google.afma.request.getAdDictionary");
        zznx zznx = new zznx(((Boolean) zzkb.zzik().zzd(zznk.zzawh)).booleanValue(), "load_ad", zzaef2.zzacv.zzarb);
        if (zzaef2.versionCode > 10 && zzaef2.zzcdl != -1) {
            zznx.zza(zznx.zzd(zzaef2.zzcdl), "cts");
        }
        zznv zzjj = zznx.zzjj();
        zzanz zza = zzano.zza(zzafm2.zzche.zzk(context2), ((Long) zzkb.zzik().zzd(zznk.zzbdf)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        zzanz zza2 = zzano.zza(zzafm2.zzchd.zzr(context2), ((Long) zzkb.zzik().zzd(zznk.zzbah)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        zzanz zzcl = zzafm2.zzcgy.zzcl(zzaef2.zzccw.packageName);
        zzanz zzcm = zzafm2.zzcgy.zzcm(zzaef2.zzccw.packageName);
        zzanz zza3 = zzafm2.zzchf.zza(zzaef2.zzccx, zzaef2.zzccw);
        Future zzq = zzbv.zzev().zzq(context2);
        zzanz zzi = zzano.zzi(null);
        Bundle bundle = zzaef2.zzccv.extras;
        boolean z = (bundle == null || bundle.getString("_ad") == null) ? false : true;
        if (zzaef2.zzcdr && !z) {
            zzi = zzafm2.zzchb.zza(zzaef2.applicationInfo);
        }
        zznx zznx2 = zznx;
        zzanz zza4 = zzano.zza(zzi, ((Long) zzkb.zzik().zzd(zznk.zzbco)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        Future zzi2 = zzano.zzi(null);
        if (((Boolean) zzkb.zzik().zzd(zznk.zzayj)).booleanValue()) {
            zzi2 = zzano.zza(zzafm2.zzchf.zzae(context2), ((Long) zzkb.zzik().zzd(zznk.zzayk)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        }
        Bundle bundle2 = (zzaef2.versionCode < 4 || zzaef2.zzcdc == null) ? null : zzaef2.zzcdc;
        ((Boolean) zzkb.zzik().zzd(zznk.zzawx)).booleanValue();
        zzbv.zzek();
        if (zzakk.zzl(context2, "android.permission.ACCESS_NETWORK_STATE") && ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            zzakb.zzck("Device is offline.");
        }
        String uuid = zzaef2.versionCode >= 7 ? zzaef2.zzcdi : UUID.randomUUID().toString();
        new zzaft(context2, uuid, zzaef2.applicationInfo.packageName);
        if (zzaef2.zzccv.extras != null) {
            String string = zzaef2.zzccv.extras.getString("_ad");
            if (string != null) {
                return zzafs.zza(context2, zzaef2, string);
            }
        }
        List<String> zzf = zzafm2.zzcgz.zzf(zzaef2.zzcdj);
        zznv zznv = zzjj;
        String str = uuid;
        Bundle bundle3 = (Bundle) zzano.zza((Future<T>) zza, null, ((Long) zzkb.zzik().zzd(zznk.zzbdf)).longValue(), TimeUnit.MILLISECONDS);
        zzagk zzagk = (zzagk) zzano.zza((Future<T>) zza2, null);
        Location location = (Location) zzano.zza((Future<T>) zza4, null);
        Info info = (Info) zzano.zza(zzi2, null);
        String str2 = (String) zzano.zza((Future<T>) zza3, null);
        String str3 = (String) zzano.zza((Future<T>) zzcl, null);
        String str4 = (String) zzano.zza((Future<T>) zzcm, null);
        zzaga zzaga = (zzaga) zzano.zza(zzq, null);
        if (zzaga == null) {
            zzakb.zzdk("Error fetching device info. This is not recoverable.");
            return new zzaej(0);
        }
        zzafl zzafl = new zzafl();
        zzafl.zzcgs = zzaef2;
        zzafl.zzcgt = zzaga;
        zzafl.zzcgo = zzagk;
        zzafl.zzaqe = location;
        zzafl.zzcgn = bundle3;
        zzafl.zzccx = str2;
        zzafl.zzcgr = info;
        if (zzf == null) {
            zzafl.zzcdj.clear();
        }
        zzafl.zzcdj = zzf;
        zzafl.zzcdc = bundle2;
        zzafl.zzcgp = str3;
        zzafl.zzcgq = str4;
        Context context3 = context;
        zzafl.zzcgu = zzafm2.zzcgx.zze(context3);
        zzafl.zzcgv = zzafm2.zzcgv;
        JSONObject zza5 = zzafs.zza(context3, zzafl);
        if (zza5 == null) {
            return new zzaej(0);
        }
        if (zzaef2.versionCode < 7) {
            try {
                zza5.put("request_id", str);
            } catch (JSONException unused) {
            }
        }
        zznx zznx3 = zznx2;
        zznv zznv2 = zznv;
        zznx3.zza(zznv2, "arc");
        zznx3.zzjj();
        ScheduledExecutorService scheduledExecutorService3 = scheduledExecutorService;
        zzanz zza6 = zzano.zza(zzano.zza(zzafm2.zzchg.zzof().zzf(zza5), zzafo.zzxn, (Executor) scheduledExecutorService3), 10, TimeUnit.SECONDS, scheduledExecutorService3);
        zzanz zzop = zzafm2.zzcha.zzop();
        if (zzop != null) {
            zzanm.zza(zzop, "AdRequestServiceImpl.loadAd.flags");
        }
        zzafz zzafz = (zzafz) zzano.zza((Future<T>) zza6, null);
        if (zzafz == null) {
            return new zzaej(0);
        }
        if (zzafz.getErrorCode() != -2) {
            return new zzaej(zzafz.getErrorCode());
        }
        zznx3.zzjm();
        zzaej zza7 = !TextUtils.isEmpty(zzafz.zzom()) ? zzafs.zza(context3, zzaef2, zzafz.zzom()) : null;
        if (zza7 == null && !TextUtils.isEmpty(zzafz.getUrl())) {
            zza7 = zza(zzaef2, context3, zzaef2.zzacr.zzcw, zzafz.getUrl(), str3, str4, zzafz, zznx3, zzafm2);
        }
        if (zza7 == null) {
            c = 0;
            zza7 = new zzaej(0);
        } else {
            c = 0;
        }
        String[] strArr = new String[1];
        strArr[c] = "tts";
        zznx3.zza(zznv2, strArr);
        zza7.zzcfd = zznx3.zzjk();
        return zza7;
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.zzafx.zza(long, boolean):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00e8, code lost:
        r1 = r7.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r7 = new java.io.InputStreamReader(r12.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        com.google.android.gms.ads.internal.zzbv.zzek();
        r11 = com.google.android.gms.internal.ads.zzakk.zza(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        com.google.android.gms.common.util.IOUtils.closeQuietly((java.io.Closeable) r7);
        r10.zzdg(r11);
        zza(r1, r14, r11, r4);
        r6.zza(r1, r14, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0108, code lost:
        if (r2 == null) goto L_0x0115;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x010a, code lost:
        r2.zza(r5, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0115, code lost:
        r1 = r6.zza(r8, r24.zzon());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0120, code lost:
        if (r3 == null) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0122, code lost:
        r3.zzchc.zzor();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0127, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0128, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0129, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x012b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x012c, code lost:
        r1 = r0;
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        com.google.android.gms.common.util.IOUtils.closeQuietly((java.io.Closeable) r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0131, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x014c, code lost:
        com.google.android.gms.internal.ads.zzakb.zzdk("No location header to follow redirect.");
        r1 = new com.google.android.gms.internal.ads.zzaej(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x015a, code lost:
        if (r3 == null) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x015c, code lost:
        r3.zzchc.zzor();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0161, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x017b, code lost:
        com.google.android.gms.internal.ads.zzakb.zzdk("Too many redirects.");
        r1 = new com.google.android.gms.internal.ads.zzaej(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0189, code lost:
        if (r3 == null) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x018b, code lost:
        r3.zzchc.zzor();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0190, code lost:
        return r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008a A[Catch:{ all -> 0x00c0, all -> 0x01cb }] */
    public static zzaej zza(zzaef zzaef, Context context, String str, String str2, String str3, String str4, zzafz zzafz, zznx zznx, zzafm zzafm) {
        HttpURLConnection httpURLConnection;
        String str5;
        byte[] bArr;
        int responseCode;
        Map headerFields;
        BufferedOutputStream bufferedOutputStream;
        Throwable th;
        zzaef zzaef2 = zzaef;
        zznx zznx2 = zznx;
        zzafm zzafm2 = zzafm;
        zznv zzjj = zznx2 != null ? zznx.zzjj() : null;
        try {
            zzafx zzafx = new zzafx(zzaef2, zzafz.zzoi());
            String str6 = "AdRequestServiceImpl: Sending request: ";
            String valueOf = String.valueOf(str2);
            zzakb.zzck(valueOf.length() != 0 ? str6.concat(valueOf) : new String(str6));
            URL url = new URL(str2);
            long elapsedRealtime = zzbv.zzer().elapsedRealtime();
            boolean z = false;
            int i = 0;
            while (true) {
                if (zzafm2 != null) {
                    zzafm2.zzchc.zzoq();
                }
                httpURLConnection = (HttpURLConnection) url.openConnection();
                try {
                    zzbv.zzek().zza(context, str, z, httpURLConnection);
                    if (zzafz.zzok()) {
                        if (!TextUtils.isEmpty(str3)) {
                            httpURLConnection.addRequestProperty("x-afma-drt-cookie", str3);
                        } else {
                            String str7 = str3;
                        }
                        if (!TextUtils.isEmpty(str4)) {
                            httpURLConnection.addRequestProperty("x-afma-drt-v2-cookie", str4);
                            str5 = zzaef2.zzcds;
                            if (!TextUtils.isEmpty(str5)) {
                                zzakb.zzck("Sending webview cookie in ad request header.");
                                httpURLConnection.addRequestProperty("Cookie", str5);
                            }
                            if (zzafz != null || TextUtils.isEmpty(zzafz.zzoj())) {
                                bArr = null;
                            } else {
                                httpURLConnection.setDoOutput(true);
                                bArr = zzafz.zzoj().getBytes();
                                httpURLConnection.setFixedLengthStreamingMode(bArr.length);
                                try {
                                    bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                                    bufferedOutputStream.write(bArr);
                                    IOUtils.closeQuietly((Closeable) bufferedOutputStream);
                                } catch (Throwable th2) {
                                    th = th2;
                                    bufferedOutputStream = null;
                                    IOUtils.closeQuietly((Closeable) bufferedOutputStream);
                                    throw th;
                                }
                            }
                            zzamy zzamy = new zzamy(zzaef2.zzcdi);
                            zzamy.zza(httpURLConnection, bArr);
                            responseCode = httpURLConnection.getResponseCode();
                            headerFields = httpURLConnection.getHeaderFields();
                            zzamy.zza(httpURLConnection, responseCode);
                            if (responseCode < 200 && responseCode < 300) {
                                break;
                            }
                            zza(url.toString(), headerFields, (String) null, responseCode);
                            if (responseCode >= 300 && responseCode < 400) {
                                String headerField = httpURLConnection.getHeaderField(HttpRequest.HEADER_LOCATION);
                                if (TextUtils.isEmpty(headerField)) {
                                    break;
                                }
                                URL url2 = new URL(headerField);
                                i++;
                                if (i > ((Integer) zzkb.zzik().zzd(zznk.zzbes)).intValue()) {
                                    break;
                                }
                                zzafx.zzl(headerFields);
                                httpURLConnection.disconnect();
                                if (zzafm2 != null) {
                                    zzafm2.zzchc.zzor();
                                }
                                url = url2;
                                zzaef2 = zzaef;
                                z = false;
                            }
                        }
                    } else {
                        String str8 = str3;
                    }
                    String str9 = str4;
                    str5 = zzaef2.zzcds;
                    if (!TextUtils.isEmpty(str5)) {
                    }
                    if (zzafz != null) {
                    }
                    bArr = null;
                    zzamy zzamy2 = new zzamy(zzaef2.zzcdi);
                    zzamy2.zza(httpURLConnection, bArr);
                    responseCode = httpURLConnection.getResponseCode();
                    headerFields = httpURLConnection.getHeaderFields();
                    zzamy2.zza(httpURLConnection, responseCode);
                    if (responseCode < 200) {
                    }
                    zza(url.toString(), headerFields, (String) null, responseCode);
                    if (responseCode >= 300) {
                        break;
                    }
                    break;
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    httpURLConnection.disconnect();
                    if (zzafm2 != null) {
                        zzafm2.zzchc.zzor();
                    }
                    throw th4;
                }
            }
            StringBuilder sb = new StringBuilder(46);
            sb.append("Received error HTTP response code: ");
            sb.append(responseCode);
            zzakb.zzdk(sb.toString());
            zzaej zzaej = new zzaej(0);
            httpURLConnection.disconnect();
            if (zzafm2 != null) {
                zzafm2.zzchc.zzor();
            }
            return zzaej;
        } catch (IOException e) {
            String str10 = "Error while connecting to ad server: ";
            String valueOf2 = String.valueOf(e.getMessage());
            zzakb.zzdk(valueOf2.length() != 0 ? str10.concat(valueOf2) : new String(str10));
            return new zzaej(2);
        }
    }

    public static zzafn zza(Context context, zzafm zzafm) {
        zzafn zzafn;
        synchronized (sLock) {
            if (zzchh == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zznk.initialize(context);
                zzchh = new zzafn(context, zzafm);
                if (context.getApplicationContext() != null) {
                    zzbv.zzek().zzal(context);
                }
                zzajz.zzai(context);
            }
            zzafn = zzchh;
        }
        return zzafn;
    }

    private static void zza(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzakb.isLoggable(2)) {
            StringBuilder sb = new StringBuilder(39 + String.valueOf(str).length());
            sb.append("Http Response: {\n  URL:\n    ");
            sb.append(str);
            sb.append("\n  Headers:");
            zzakb.v(sb.toString());
            if (map != null) {
                for (String str3 : map.keySet()) {
                    StringBuilder sb2 = new StringBuilder(5 + String.valueOf(str3).length());
                    sb2.append("    ");
                    sb2.append(str3);
                    sb2.append(":");
                    zzakb.v(sb2.toString());
                    for (String valueOf : (List) map.get(str3)) {
                        String str4 = "      ";
                        String valueOf2 = String.valueOf(valueOf);
                        zzakb.v(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                    }
                }
            }
            zzakb.v("  Body:");
            if (str2 != null) {
                int i2 = 0;
                while (i2 < Math.min(str2.length(), DefaultOggSeeker.MATCH_BYTE_RANGE)) {
                    int i3 = i2 + 1000;
                    zzakb.v(str2.substring(i2, Math.min(str2.length(), i3)));
                    i2 = i3;
                }
            } else {
                zzakb.v("    null");
            }
            StringBuilder sb3 = new StringBuilder(34);
            sb3.append("  Response Code:\n    ");
            sb3.append(i);
            sb3.append("\n}");
            zzakb.v(sb3.toString());
        }
    }

    public final void zza(zzaef zzaef, zzaeq zzaeq) {
        zzbv.zzeo().zzd(this.mContext, zzaef.zzacr);
        zzanz zzb = zzaki.zzb(new zzafp(this, zzaef, zzaeq));
        zzbv.zzez().zzsa();
        zzbv.zzez().getHandler().postDelayed(new zzafq(this, zzb), ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    }

    public final void zza(zzaey zzaey, zzaet zzaet) {
        zzakb.v("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }

    public final zzaej zzb(zzaef zzaef) {
        return zza(this.mContext, this.zzchi, zzaef, this.zzchj);
    }

    public final void zzb(zzaey zzaey, zzaet zzaet) {
        zzakb.v("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }
}
