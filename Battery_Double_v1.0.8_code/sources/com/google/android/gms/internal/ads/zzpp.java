package com.google.android.gms.internal.ads;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdAssetNames;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;

@zzadh
@ParametersAreNonnullByDefault
public final class zzpp extends zzqg implements OnClickListener, OnTouchListener, OnGlobalLayoutListener, OnScrollChangedListener {
    @VisibleForTesting
    static final String[] zzbjs = {NativeAppInstallAd.ASSET_MEDIA_VIDEO, NativeContentAd.ASSET_MEDIA_VIDEO, UnifiedNativeAdAssetNames.ASSET_MEDIA_VIDEO};
    private final Object mLock = new Object();
    @Nullable
    @GuardedBy("mLock")
    @VisibleForTesting
    private zzoz zzbij;
    @Nullable
    @VisibleForTesting
    private View zzbjx;
    @VisibleForTesting
    private Point zzbjz = new Point();
    @VisibleForTesting
    private Point zzbka = new Point();
    @Nullable
    @VisibleForTesting
    private WeakReference<zzfp> zzbkb = new WeakReference<>(null);
    private final WeakReference<View> zzbke;
    private final Map<String, WeakReference<View>> zzbkf = new HashMap();
    private final Map<String, WeakReference<View>> zzbkg = new HashMap();
    private final Map<String, WeakReference<View>> zzbkh = new HashMap();

    public zzpp(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        zzbv.zzfg();
        zzaor.zza(view, (OnGlobalLayoutListener) this);
        zzbv.zzfg();
        zzaor.zza(view, (OnScrollChangedListener) this);
        view.setOnTouchListener(this);
        view.setOnClickListener(this);
        this.zzbke = new WeakReference<>(view);
        for (Entry entry : hashMap.entrySet()) {
            String str = (String) entry.getKey();
            View view2 = (View) entry.getValue();
            if (view2 != null) {
                this.zzbkf.put(str, new WeakReference(view2));
                if (!NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str) && !UnifiedNativeAdAssetNames.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str)) {
                    view2.setOnTouchListener(this);
                    view2.setClickable(true);
                    view2.setOnClickListener(this);
                }
            }
        }
        this.zzbkh.putAll(this.zzbkf);
        for (Entry entry2 : hashMap2.entrySet()) {
            View view3 = (View) entry2.getValue();
            if (view3 != null) {
                this.zzbkg.put((String) entry2.getKey(), new WeakReference(view3));
                view3.setOnTouchListener(this);
            }
        }
        this.zzbkh.putAll(this.zzbkg);
        zznk.initialize(view.getContext());
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        return;
     */
    public final void zza(zzpd zzpd) {
        View view;
        synchronized (this.mLock) {
            String[] strArr = zzbjs;
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    view = null;
                    break;
                }
                WeakReference weakReference = (WeakReference) this.zzbkh.get(strArr[i]);
                if (weakReference != null) {
                    view = (View) weakReference.get();
                    break;
                }
                i++;
            }
            if (!(view instanceof FrameLayout)) {
                zzpd.zzkq();
                return;
            }
            zzpr zzpr = new zzpr(this, view);
            if (zzpd instanceof zzoy) {
                zzpd.zzb(view, (zzox) zzpr);
            } else {
                zzpd.zza(view, (zzox) zzpr);
            }
        }
    }

    /* access modifiers changed from: private */
    public final boolean zza(String[] strArr) {
        for (String str : strArr) {
            if (this.zzbkf.get(str) != null) {
                return true;
            }
        }
        for (String str2 : strArr) {
            if (this.zzbkg.get(str2) != null) {
                return false;
            }
        }
        return false;
    }

    private final void zzl(@Nullable View view) {
        synchronized (this.mLock) {
            if (this.zzbij != null) {
                zzoz zzkn = this.zzbij instanceof zzoy ? ((zzoy) this.zzbij).zzkn() : this.zzbij;
                if (zzkn != null) {
                    zzkn.zzl(view);
                }
            }
        }
    }

    @VisibleForTesting
    private final int zzv(int i) {
        int zzb;
        synchronized (this.mLock) {
            zzkb.zzif();
            zzb = zzamu.zzb(this.zzbij.getContext(), i);
        }
        return zzb;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        return;
     */
    public final void onClick(View view) {
        zzoz zzoz;
        String str;
        Map<String, WeakReference<View>> map;
        synchronized (this.mLock) {
            if (this.zzbij != null) {
                View view2 = (View) this.zzbke.get();
                if (view2 != null) {
                    Bundle bundle = new Bundle();
                    bundle.putFloat("x", (float) zzv(this.zzbjz.x));
                    bundle.putFloat("y", (float) zzv(this.zzbjz.y));
                    bundle.putFloat("start_x", (float) zzv(this.zzbka.x));
                    bundle.putFloat("start_y", (float) zzv(this.zzbka.y));
                    if (this.zzbjx == null || !this.zzbjx.equals(view)) {
                        this.zzbij.zza(view, this.zzbkh, bundle, view2);
                    } else {
                        if (!(this.zzbij instanceof zzoy)) {
                            zzoz = this.zzbij;
                            str = NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE;
                            map = this.zzbkh;
                        } else if (((zzoy) this.zzbij).zzkn() != null) {
                            zzoz = ((zzoy) this.zzbij).zzkn();
                            str = NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE;
                            map = this.zzbkh;
                        }
                        zzoz.zza(view, str, bundle, map, view2);
                    }
                }
            }
        }
    }

    public final void onGlobalLayout() {
        synchronized (this.mLock) {
            if (this.zzbij != null) {
                View view = (View) this.zzbke.get();
                if (view != null) {
                    this.zzbij.zzc(view, this.zzbkh);
                }
            }
        }
    }

    public final void onScrollChanged() {
        synchronized (this.mLock) {
            if (this.zzbij != null) {
                View view = (View) this.zzbke.get();
                if (view != null) {
                    this.zzbij.zzc(view, this.zzbkh);
                }
            }
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.mLock) {
            if (this.zzbij == null) {
                return false;
            }
            View view2 = (View) this.zzbke.get();
            if (view2 == null) {
                return false;
            }
            int[] iArr = new int[2];
            view2.getLocationOnScreen(iArr);
            Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
            this.zzbjz = point;
            if (motionEvent.getAction() == 0) {
                this.zzbka = point;
            }
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            obtain.setLocation((float) point.x, (float) point.y);
            this.zzbij.zzd(obtain);
            obtain.recycle();
            return false;
        }
    }

    public final void unregisterNativeAd() {
        synchronized (this.mLock) {
            this.zzbjx = null;
            this.zzbij = null;
            this.zzbjz = null;
            this.zzbka = null;
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper) {
        int i;
        View view;
        synchronized (this.mLock) {
            ViewGroup viewGroup = null;
            zzl(null);
            Object unwrap = ObjectWrapper.unwrap(iObjectWrapper);
            if (!(unwrap instanceof zzpd)) {
                zzakb.zzdk("Not an instance of native engine. This is most likely a transient error");
                return;
            }
            zzpd zzpd = (zzpd) unwrap;
            if (!zzpd.zzkk()) {
                zzakb.e("Your account must be enabled to use this feature. Talk to your account manager to request this feature for your account.");
                return;
            }
            View view2 = (View) this.zzbke.get();
            if (!(this.zzbij == null || view2 == null)) {
                if (((Boolean) zzkb.zzik().zzd(zznk.zzbbu)).booleanValue()) {
                    this.zzbij.zzb(view2, this.zzbkh);
                }
            }
            synchronized (this.mLock) {
                i = 0;
                if (this.zzbij instanceof zzpd) {
                    zzpd zzpd2 = (zzpd) this.zzbij;
                    View view3 = (View) this.zzbke.get();
                    if (!(zzpd2 == null || zzpd2.getContext() == null || view3 == null || !zzbv.zzfh().zzu(view3.getContext()))) {
                        zzaix zzks = zzpd2.zzks();
                        if (zzks != null) {
                            zzks.zzx(false);
                        }
                        zzfp zzfp = (zzfp) this.zzbkb.get();
                        if (!(zzfp == null || zzks == null)) {
                            zzfp.zzb(zzks);
                        }
                    }
                }
            }
            if (!(this.zzbij instanceof zzoy) || !((zzoy) this.zzbij).zzkm()) {
                this.zzbij = zzpd;
                if (zzpd instanceof zzoy) {
                    ((zzoy) zzpd).zzc(null);
                }
            } else {
                ((zzoy) this.zzbij).zzc(zzpd);
            }
            String[] strArr = {NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW, UnifiedNativeAdAssetNames.ASSET_ADCHOICES_CONTAINER_VIEW};
            while (true) {
                if (i >= 2) {
                    view = null;
                    break;
                }
                WeakReference weakReference = (WeakReference) this.zzbkh.get(strArr[i]);
                if (weakReference != null) {
                    view = (View) weakReference.get();
                    break;
                }
                i++;
            }
            if (view == null) {
                zzakb.zzdk("Ad choices asset view is not provided.");
            } else {
                if (view instanceof ViewGroup) {
                    viewGroup = (ViewGroup) view;
                }
                if (viewGroup != null) {
                    this.zzbjx = zzpd.zza((OnClickListener) this, true);
                    if (this.zzbjx != null) {
                        this.zzbkh.put(NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE, new WeakReference(this.zzbjx));
                        this.zzbkf.put(NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE, new WeakReference(this.zzbjx));
                        viewGroup.removeAllViews();
                        viewGroup.addView(this.zzbjx);
                    }
                }
            }
            zzpd.zza(view2, this.zzbkf, this.zzbkg, (OnTouchListener) this, (OnClickListener) this);
            zzakk.zzcrm.post(new zzpq(this, zzpd));
            zzl(view2);
            this.zzbij.zzj(view2);
            synchronized (this.mLock) {
                if (this.zzbij instanceof zzpd) {
                    zzpd zzpd3 = (zzpd) this.zzbij;
                    View view4 = (View) this.zzbke.get();
                    if (!(zzpd3 == null || zzpd3.getContext() == null || view4 == null || !zzbv.zzfh().zzu(view4.getContext()))) {
                        zzfp zzfp2 = (zzfp) this.zzbkb.get();
                        if (zzfp2 == null) {
                            zzfp2 = new zzfp(view4.getContext(), view4);
                            this.zzbkb = new WeakReference<>(zzfp2);
                        }
                        zzfp2.zza((zzft) zzpd3.zzks());
                    }
                }
            }
        }
    }

    public final void zzc(IObjectWrapper iObjectWrapper) {
        synchronized (this.mLock) {
            this.zzbij.setClickConfirmingView((View) ObjectWrapper.unwrap(iObjectWrapper));
        }
    }
}
