package com.google.android.gms.internal.ads;

import org.json.JSONObject;

@zzadh
public final class zzabo extends zzajx {
    /* access modifiers changed from: private */
    public final zzabm zzbzd;
    private final zzaji zzbze;
    private final zzaej zzbzf = this.zzbze.zzcos;

    public zzabo(zzaji zzaji, zzabm zzabm) {
        this.zzbze = zzaji;
        this.zzbzd = zzabm;
    }

    public final void onStop() {
    }

    public final void zzdn() {
        zzjj zzjj = this.zzbze.zzcgs.zzccv;
        int i = this.zzbzf.orientation;
        long j = this.zzbzf.zzbsu;
        String str = this.zzbze.zzcgs.zzccy;
        long j2 = this.zzbzf.zzcer;
        zzjn zzjn = this.zzbze.zzacv;
        long j3 = this.zzbzf.zzcep;
        long j4 = this.zzbze.zzcoh;
        long j5 = j2;
        long j6 = this.zzbzf.zzceu;
        String str2 = this.zzbzf.zzcev;
        JSONObject jSONObject = this.zzbze.zzcob;
        boolean z = this.zzbze.zzcos.zzcfh;
        zzael zzael = this.zzbze.zzcos.zzcfi;
        zzhs zzhs = this.zzbze.zzcoq;
        boolean z2 = this.zzbze.zzcos.zzzl;
        boolean z3 = this.zzbze.zzcor;
        boolean z4 = this.zzbze.zzcos.zzcfp;
        String str3 = str2;
        zzjn zzjn2 = zzjn;
        long j7 = j5;
        zzajh zzajh = r1;
        long j8 = j3;
        long j9 = j4;
        long j10 = j6;
        zzajh zzajh2 = new zzajh(zzjj, null, null, 0, null, null, i, j, str, false, null, null, null, null, null, j7, zzjn2, j8, j9, j10, str3, jSONObject, null, null, null, null, z, zzael, null, null, null, zzhs, z2, z3, z4, null, this.zzbze.zzcos.zzzm, this.zzbze.zzcos.zzcfq);
        zzakk.zzcrm.post(new zzabp(this, zzajh));
    }
}
