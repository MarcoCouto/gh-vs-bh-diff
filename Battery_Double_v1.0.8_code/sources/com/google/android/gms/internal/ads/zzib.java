package com.google.android.gms.internal.ads;

import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import java.io.IOException;

public final class zzib extends zzbfc<zzib> {
    public Integer zzalt;
    private Integer zzalu;
    private zzid zzalv;
    public zzie zzalw;
    private zzic[] zzalx;
    private zzif zzaly;
    private zzio zzalz;
    private zzin zzama;
    private zzik zzamb;
    private zzil zzamc;
    private zziu[] zzamd;

    public zzib() {
        this.zzalt = null;
        this.zzalu = null;
        this.zzalv = null;
        this.zzalw = null;
        this.zzalx = zzic.zzhr();
        this.zzaly = null;
        this.zzalz = null;
        this.zzama = null;
        this.zzamb = null;
        this.zzamc = null;
        this.zzamd = zziu.zzhu();
        this.zzebk = null;
        this.zzebt = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zze */
    public final zzib zza(zzbez zzbez) throws IOException {
        int i;
        int zzacc;
        zzbfi zzbfi;
        while (true) {
            int zzabk = zzbez.zzabk();
            switch (zzabk) {
                case 0:
                    return this;
                case 56:
                    i = zzbez.getPosition();
                    zzacc = zzbez.zzacc();
                    if (zzacc < 0 || zzacc > 9) {
                        StringBuilder sb = new StringBuilder(43);
                        sb.append(zzacc);
                        sb.append(" is not a valid enum AdInitiater");
                        break;
                    } else {
                        this.zzalt = Integer.valueOf(zzacc);
                        continue;
                    }
                case 64:
                    i = zzbez.getPosition();
                    try {
                        this.zzalu = Integer.valueOf(zzia.zzd(zzbez.zzacc()));
                        continue;
                    } catch (IllegalArgumentException unused) {
                        zzbez.zzdc(i);
                        zza(zzbez, zzabk);
                        break;
                    }
                case 74:
                    if (this.zzalv == null) {
                        this.zzalv = new zzid();
                    }
                    zzbfi = this.zzalv;
                    break;
                case 82:
                    if (this.zzalw == null) {
                        this.zzalw = new zzie();
                    }
                    zzbfi = this.zzalw;
                    break;
                case 90:
                    int zzb = zzbfl.zzb(zzbez, 90);
                    int length = this.zzalx == null ? 0 : this.zzalx.length;
                    zzic[] zzicArr = new zzic[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzalx, 0, zzicArr, 0, length);
                    }
                    while (length < zzicArr.length - 1) {
                        zzicArr[length] = new zzic();
                        zzbez.zza(zzicArr[length]);
                        zzbez.zzabk();
                        length++;
                    }
                    zzicArr[length] = new zzic();
                    zzbez.zza(zzicArr[length]);
                    this.zzalx = zzicArr;
                    continue;
                case 98:
                    if (this.zzaly == null) {
                        this.zzaly = new zzif();
                    }
                    zzbfi = this.zzaly;
                    break;
                case 106:
                    if (this.zzalz == null) {
                        this.zzalz = new zzio();
                    }
                    zzbfi = this.zzalz;
                    break;
                case 114:
                    if (this.zzama == null) {
                        this.zzama = new zzin();
                    }
                    zzbfi = this.zzama;
                    break;
                case 122:
                    if (this.zzamb == null) {
                        this.zzamb = new zzik();
                    }
                    zzbfi = this.zzamb;
                    break;
                case TsExtractor.TS_STREAM_TYPE_HDMV_DTS /*130*/:
                    if (this.zzamc == null) {
                        this.zzamc = new zzil();
                    }
                    zzbfi = this.zzamc;
                    break;
                case TsExtractor.TS_STREAM_TYPE_DTS /*138*/:
                    int zzb2 = zzbfl.zzb(zzbez, TsExtractor.TS_STREAM_TYPE_DTS);
                    int length2 = this.zzamd == null ? 0 : this.zzamd.length;
                    zziu[] zziuArr = new zziu[(zzb2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.zzamd, 0, zziuArr, 0, length2);
                    }
                    while (length2 < zziuArr.length - 1) {
                        zziuArr[length2] = new zziu();
                        zzbez.zza(zziuArr[length2]);
                        zzbez.zzabk();
                        length2++;
                    }
                    zziuArr[length2] = new zziu();
                    zzbez.zza(zziuArr[length2]);
                    this.zzamd = zziuArr;
                    continue;
                default:
                    if (!super.zza(zzbez, zzabk)) {
                        return this;
                    }
                    continue;
            }
            zzbez.zza(zzbfi);
        }
        StringBuilder sb2 = new StringBuilder(43);
        sb2.append(zzacc);
        sb2.append(" is not a valid enum AdInitiater");
        throw new IllegalArgumentException(sb2.toString());
    }

    public final void zza(zzbfa zzbfa) throws IOException {
        if (this.zzalt != null) {
            zzbfa.zzm(7, this.zzalt.intValue());
        }
        if (this.zzalu != null) {
            zzbfa.zzm(8, this.zzalu.intValue());
        }
        if (this.zzalv != null) {
            zzbfa.zza(9, (zzbfi) this.zzalv);
        }
        if (this.zzalw != null) {
            zzbfa.zza(10, (zzbfi) this.zzalw);
        }
        if (this.zzalx != null && this.zzalx.length > 0) {
            for (zzic zzic : this.zzalx) {
                if (zzic != null) {
                    zzbfa.zza(11, (zzbfi) zzic);
                }
            }
        }
        if (this.zzaly != null) {
            zzbfa.zza(12, (zzbfi) this.zzaly);
        }
        if (this.zzalz != null) {
            zzbfa.zza(13, (zzbfi) this.zzalz);
        }
        if (this.zzama != null) {
            zzbfa.zza(14, (zzbfi) this.zzama);
        }
        if (this.zzamb != null) {
            zzbfa.zza(15, (zzbfi) this.zzamb);
        }
        if (this.zzamc != null) {
            zzbfa.zza(16, (zzbfi) this.zzamc);
        }
        if (this.zzamd != null && this.zzamd.length > 0) {
            for (zziu zziu : this.zzamd) {
                if (zziu != null) {
                    zzbfa.zza(17, (zzbfi) zziu);
                }
            }
        }
        super.zza(zzbfa);
    }

    /* access modifiers changed from: protected */
    public final int zzr() {
        int zzr = super.zzr();
        if (this.zzalt != null) {
            zzr += zzbfa.zzq(7, this.zzalt.intValue());
        }
        if (this.zzalu != null) {
            zzr += zzbfa.zzq(8, this.zzalu.intValue());
        }
        if (this.zzalv != null) {
            zzr += zzbfa.zzb(9, (zzbfi) this.zzalv);
        }
        if (this.zzalw != null) {
            zzr += zzbfa.zzb(10, (zzbfi) this.zzalw);
        }
        if (this.zzalx != null && this.zzalx.length > 0) {
            int i = zzr;
            for (zzic zzic : this.zzalx) {
                if (zzic != null) {
                    i += zzbfa.zzb(11, (zzbfi) zzic);
                }
            }
            zzr = i;
        }
        if (this.zzaly != null) {
            zzr += zzbfa.zzb(12, (zzbfi) this.zzaly);
        }
        if (this.zzalz != null) {
            zzr += zzbfa.zzb(13, (zzbfi) this.zzalz);
        }
        if (this.zzama != null) {
            zzr += zzbfa.zzb(14, (zzbfi) this.zzama);
        }
        if (this.zzamb != null) {
            zzr += zzbfa.zzb(15, (zzbfi) this.zzamb);
        }
        if (this.zzamc != null) {
            zzr += zzbfa.zzb(16, (zzbfi) this.zzamc);
        }
        if (this.zzamd != null && this.zzamd.length > 0) {
            for (zziu zziu : this.zzamd) {
                if (zziu != null) {
                    zzr += zzbfa.zzb(17, (zzbfi) zziu);
                }
            }
        }
        return zzr;
    }
}
