package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.applinks.AppLinkData;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.gms.ads.internal.zzbv;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
public final class zzafs {
    private static final SimpleDateFormat zzcho = new SimpleDateFormat("yyyyMMdd", Locale.US);

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e1 A[Catch:{ JSONException -> 0x0270 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e7 A[Catch:{ JSONException -> 0x0270 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0161 A[Catch:{ JSONException -> 0x0270 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x016a A[Catch:{ JSONException -> 0x0270 }] */
    public static zzaej zza(Context context, zzaef zzaef, String str) {
        int i;
        long j;
        int i2;
        String str2;
        String str3;
        zzaej zzaej;
        long j2;
        int zzrl;
        zzaef zzaef2 = zzaef;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("ad_base_url", null);
            String optString2 = jSONObject.optString("ad_url", null);
            String optString3 = jSONObject.optString("ad_size", null);
            String optString4 = jSONObject.optString("ad_slot_size", optString3);
            boolean z = (zzaef2 == null || zzaef2.zzcdb == 0) ? false : true;
            String optString5 = jSONObject.optString("ad_json", null);
            if (optString5 == null) {
                optString5 = jSONObject.optString("ad_html", null);
            }
            if (optString5 == null) {
                optString5 = jSONObject.optString(TtmlNode.TAG_BODY, null);
            }
            if (optString5 == null && jSONObject.has("ads")) {
                optString5 = jSONObject.toString();
            }
            String optString6 = jSONObject.optString("debug_dialog", null);
            String optString7 = jSONObject.optString("debug_signals", null);
            long j3 = jSONObject.has("interstitial_timeout") ? (long) (jSONObject.getDouble("interstitial_timeout") * 1000.0d) : -1;
            String optString8 = jSONObject.optString("orientation", null);
            if ("portrait".equals(optString8)) {
                zzrl = zzbv.zzem().zzrm();
            } else if ("landscape".equals(optString8)) {
                zzrl = zzbv.zzem().zzrl();
            } else {
                i = -1;
                if (TextUtils.isEmpty(optString5) || TextUtils.isEmpty(optString2)) {
                    i2 = -1;
                    str3 = optString;
                    str2 = optString5;
                    zzaej = null;
                    j = -1;
                } else {
                    i2 = -1;
                    zzaej = zzafn.zza(zzaef2, context, zzaef2.zzacr.zzcw, optString2, null, null, null, null, null);
                    String str4 = zzaej.zzbyq;
                    String str5 = zzaej.zzceo;
                    j = zzaej.zzceu;
                    str2 = str5;
                    str3 = str4;
                }
                if (str2 != null) {
                    return new zzaej(0);
                }
                JSONArray optJSONArray = jSONObject.optJSONArray("click_urls");
                List list = zzaej == null ? null : zzaej.zzbsn;
                if (optJSONArray != null) {
                    list = zza(optJSONArray, list);
                }
                JSONArray optJSONArray2 = jSONObject.optJSONArray("impression_urls");
                List list2 = zzaej == null ? null : zzaej.zzbso;
                if (optJSONArray2 != null) {
                    list2 = zza(optJSONArray2, list2);
                }
                JSONArray optJSONArray3 = jSONObject.optJSONArray("downloaded_impression_urls");
                List list3 = zzaej == null ? null : zzaej.zzbsp;
                List zza = optJSONArray3 != null ? zza(optJSONArray3, list3) : list3;
                JSONArray optJSONArray4 = jSONObject.optJSONArray("manual_impression_urls");
                List list4 = zzaej == null ? null : zzaej.zzces;
                List zza2 = optJSONArray4 != null ? zza(optJSONArray4, list4) : list4;
                if (zzaej != null) {
                    if (zzaej.orientation != i2) {
                        i = zzaej.orientation;
                    }
                    if (zzaej.zzcep > 0) {
                        j2 = zzaej.zzcep;
                        String optString9 = jSONObject.optString("active_view");
                        boolean optBoolean = jSONObject.optBoolean("ad_is_javascript", false);
                        zzaej zzaej2 = new zzaej(zzaef2, str3, str2, list, list2, j2, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), zza2, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j, optString6, optBoolean, !optBoolean ? jSONObject.optString("ad_passback_url", null) : null, optString9, jSONObject.optBoolean("custom_render_allowed", false), z, zzaef2.zzcdd, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), SettingsJsonConstants.ICON_HEIGHT_KEY.equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), zzaig.zza(jSONObject.optJSONArray("rewards")), zza(jSONObject.optJSONArray("video_start_urls"), null), zza(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), zzael.zzl(jSONObject.optJSONObject("auto_protection_configuration")), zzaef2.zzcdr, jSONObject.optString("set_cookie", ""), zza(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optBoolean("render_in_browser", zzaef2.zzbss), optString4, zzaiq.zzo(jSONObject.optJSONObject("safe_browsing")), optString7, jSONObject.optBoolean("content_vertical_opted_out", true), zzaef2.zzced, jSONObject.optBoolean("custom_close_blocked"), 0, jSONObject.optBoolean("enable_omid", false), zza, jSONObject.optBoolean("disable_closable_area", false), jSONObject.optString("omid_settings", null));
                        return zzaej2;
                    }
                }
                j2 = j3;
                String optString92 = jSONObject.optString("active_view");
                boolean optBoolean2 = jSONObject.optBoolean("ad_is_javascript", false);
                zzaej zzaej22 = new zzaej(zzaef2, str3, str2, list, list2, j2, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), zza2, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j, optString6, optBoolean2, !optBoolean2 ? jSONObject.optString("ad_passback_url", null) : null, optString92, jSONObject.optBoolean("custom_render_allowed", false), z, zzaef2.zzcdd, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), SettingsJsonConstants.ICON_HEIGHT_KEY.equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), zzaig.zza(jSONObject.optJSONArray("rewards")), zza(jSONObject.optJSONArray("video_start_urls"), null), zza(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), zzael.zzl(jSONObject.optJSONObject("auto_protection_configuration")), zzaef2.zzcdr, jSONObject.optString("set_cookie", ""), zza(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optBoolean("render_in_browser", zzaef2.zzbss), optString4, zzaiq.zzo(jSONObject.optJSONObject("safe_browsing")), optString7, jSONObject.optBoolean("content_vertical_opted_out", true), zzaef2.zzced, jSONObject.optBoolean("custom_close_blocked"), 0, jSONObject.optBoolean("enable_omid", false), zza, jSONObject.optBoolean("disable_closable_area", false), jSONObject.optString("omid_settings", null));
                return zzaej22;
            }
            i = zzrl;
            if (TextUtils.isEmpty(optString5)) {
            }
            i2 = -1;
            str3 = optString;
            str2 = optString5;
            zzaej = null;
            j = -1;
            if (str2 != null) {
            }
        } catch (JSONException e) {
            String str6 = "Could not parse the inline ad response: ";
            String valueOf = String.valueOf(e.getMessage());
            zzakb.zzdk(valueOf.length() != 0 ? str6.concat(valueOf) : new String(str6));
            return new zzaej(0);
        }
    }

    @Nullable
    private static List<String> zza(@Nullable JSONArray jSONArray, @Nullable List<String> list) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        if (list == null) {
            list = new ArrayList<>();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            list.add(jSONArray.getString(i));
        }
        return list;
    }

    /* JADX WARNING: Removed duplicated region for block: B:304:0x081d A[Catch:{ JSONException -> 0x0934 }] */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x0820 A[Catch:{ JSONException -> 0x0934 }] */
    @Nullable
    public static JSONObject zza(Context context, zzafl zzafl) {
        zzaga zzaga;
        Location location;
        zzaef zzaef;
        JSONObject jSONObject;
        Bundle bundle;
        int i;
        zzaef zzaef2;
        String str;
        Object obj;
        int i2;
        Bundle bundle2;
        boolean z;
        String str2;
        String str3;
        String str4;
        String str5;
        Boolean valueOf;
        String str6;
        int i3;
        zzjn[] zzjnArr;
        String str7;
        Object obj2;
        zzafl zzafl2 = zzafl;
        zzaef zzaef3 = zzafl2.zzcgs;
        Location location2 = zzafl2.zzaqe;
        zzaga zzaga2 = zzafl2.zzcgt;
        Bundle bundle3 = zzafl2.zzcdc;
        JSONObject jSONObject2 = zzafl2.zzcgu;
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("extra_caps", zzkb.zzik().zzd(zznk.zzbbk));
            if (zzafl2.zzcdj.size() > 0) {
                hashMap.put("eid", TextUtils.join(",", zzafl2.zzcdj));
            }
            if (zzaef3.zzccu != null) {
                hashMap.put("ad_pos", zzaef3.zzccu);
            }
            zzjj zzjj = zzaef3.zzccv;
            String zzqn = zzajw.zzqn();
            if (zzqn != null) {
                hashMap.put("abf", zzqn);
            }
            if (zzjj.zzapw != -1) {
                hashMap.put("cust_age", zzcho.format(new Date(zzjj.zzapw)));
            }
            if (zzjj.extras != null) {
                hashMap.put(AppLinkData.ARGUMENTS_EXTRAS_KEY, zzjj.extras);
            }
            int i4 = -1;
            if (zzjj.zzapx != -1) {
                hashMap.put("cust_gender", Integer.valueOf(zzjj.zzapx));
            }
            if (zzjj.zzapy != null) {
                hashMap.put("kw", zzjj.zzapy);
            }
            if (zzjj.zzaqa != -1) {
                hashMap.put("tag_for_child_directed_treatment", Integer.valueOf(zzjj.zzaqa));
            }
            if (zzjj.zzapz) {
                if (((Boolean) zzkb.zzik().zzd(zznk.zzbfp)).booleanValue()) {
                    str7 = "test_request";
                    obj2 = Boolean.valueOf(true);
                } else {
                    str7 = "adtest";
                    obj2 = "on";
                }
                hashMap.put(str7, obj2);
            }
            if (zzjj.versionCode >= 2) {
                if (zzjj.zzaqb) {
                    hashMap.put("d_imp_hdr", Integer.valueOf(1));
                }
                if (!TextUtils.isEmpty(zzjj.zzaqc)) {
                    hashMap.put("ppid", zzjj.zzaqc);
                }
            }
            if (zzjj.versionCode >= 3 && zzjj.zzaqf != null) {
                hashMap.put("url", zzjj.zzaqf);
            }
            if (zzjj.versionCode >= 5) {
                if (zzjj.zzaqh != null) {
                    hashMap.put("custom_targeting", zzjj.zzaqh);
                }
                if (zzjj.zzaqi != null) {
                    hashMap.put("category_exclusions", zzjj.zzaqi);
                }
                if (zzjj.zzaqj != null) {
                    hashMap.put("request_agent", zzjj.zzaqj);
                }
            }
            if (zzjj.versionCode >= 6 && zzjj.zzaqk != null) {
                hashMap.put("request_pkg", zzjj.zzaqk);
            }
            if (zzjj.versionCode >= 7) {
                hashMap.put("is_designed_for_families", Boolean.valueOf(zzjj.zzaql));
            }
            if (zzaef3.zzacv.zzard != null) {
                boolean z2 = false;
                boolean z3 = false;
                for (zzjn zzjn : zzaef3.zzacv.zzard) {
                    if (!zzjn.zzarf && !z2) {
                        hashMap.put("format", zzjn.zzarb);
                        z2 = true;
                    }
                    if (zzjn.zzarf && !z3) {
                        hashMap.put("fluid", SettingsJsonConstants.ICON_HEIGHT_KEY);
                        z3 = true;
                    }
                    if (z2 && z3) {
                        break;
                    }
                }
            } else {
                hashMap.put("format", zzaef3.zzacv.zzarb);
                if (zzaef3.zzacv.zzarf) {
                    hashMap.put("fluid", SettingsJsonConstants.ICON_HEIGHT_KEY);
                }
            }
            if (zzaef3.zzacv.width == -1) {
                hashMap.put("smart_w", MessengerShareContentUtility.WEBVIEW_RATIO_FULL);
            }
            if (zzaef3.zzacv.height == -2) {
                hashMap.put("smart_h", "auto");
            }
            if (zzaef3.zzacv.zzard != null) {
                StringBuilder sb = new StringBuilder();
                zzjn[] zzjnArr2 = zzaef3.zzacv.zzard;
                int length = zzjnArr2.length;
                int i5 = 0;
                boolean z4 = false;
                while (i5 < length) {
                    zzjn zzjn2 = zzjnArr2[i5];
                    if (zzjn2.zzarf) {
                        z4 = true;
                    } else {
                        if (sb.length() != 0) {
                            sb.append("|");
                        }
                        sb.append(zzjn2.width == i4 ? (int) (((float) zzjn2.widthPixels) / zzaga2.zzagu) : zzjn2.width);
                        sb.append("x");
                        sb.append(zzjn2.height == -2 ? (int) (((float) zzjn2.heightPixels) / zzaga2.zzagu) : zzjn2.height);
                    }
                    i5++;
                    i4 = -1;
                }
                if (z4) {
                    if (sb.length() != 0) {
                        i3 = 0;
                        sb.insert(0, "|");
                    } else {
                        i3 = 0;
                    }
                    sb.insert(i3, "320x50");
                }
                hashMap.put("sz", sb);
            }
            if (zzaef3.zzcdb != 0) {
                hashMap.put("native_version", Integer.valueOf(zzaef3.zzcdb));
                hashMap.put("native_templates", zzaef3.zzads);
                String str8 = "native_image_orientation";
                zzpl zzpl = zzaef3.zzadj;
                if (zzpl != null) {
                    switch (zzpl.zzbjo) {
                        case 0:
                            break;
                        case 1:
                            str6 = "portrait";
                            break;
                        case 2:
                            str6 = "landscape";
                            break;
                        default:
                            str6 = "not_set";
                            break;
                    }
                }
                str6 = "any";
                hashMap.put(str8, str6);
                if (!zzaef3.zzcdk.isEmpty()) {
                    hashMap.put("native_custom_templates", zzaef3.zzcdk);
                }
                if (zzaef3.versionCode >= 24) {
                    hashMap.put("max_num_ads", Integer.valueOf(zzaef3.zzceg));
                }
                if (!TextUtils.isEmpty(zzaef3.zzcee)) {
                    try {
                        hashMap.put("native_advanced_settings", new JSONArray(zzaef3.zzcee));
                    } catch (JSONException e) {
                        zzakb.zzc("Problem creating json from native advanced settings", e);
                    }
                }
            }
            if (zzaef3.zzadn != null && zzaef3.zzadn.size() > 0) {
                for (Integer num : zzaef3.zzadn) {
                    if (num.intValue() == 2) {
                        str5 = "iba";
                        valueOf = Boolean.valueOf(true);
                    } else if (num.intValue() == 1) {
                        str5 = "ina";
                        valueOf = Boolean.valueOf(true);
                    }
                    hashMap.put(str5, valueOf);
                }
            }
            if (zzaef3.zzacv.zzarg) {
                hashMap.put("ene", Boolean.valueOf(true));
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzaxv)).booleanValue()) {
                hashMap.put("xsrve", Boolean.valueOf(true));
            }
            if (zzaef3.zzadl != null) {
                hashMap.put("is_icon_ad", Boolean.valueOf(true));
                hashMap.put("icon_ad_expansion_behavior", Integer.valueOf(zzaef3.zzadl.zzasj));
            }
            hashMap.put("slotname", zzaef3.zzacp);
            hashMap.put("pn", zzaef3.applicationInfo.packageName);
            if (zzaef3.zzccw != null) {
                hashMap.put("vc", Integer.valueOf(zzaef3.zzccw.versionCode));
            }
            hashMap.put("ms", zzafl2.zzccx);
            hashMap.put("seq_num", zzaef3.zzccy);
            hashMap.put("session_id", zzaef3.zzccz);
            hashMap.put("js", zzaef3.zzacr.zzcw);
            zzagk zzagk = zzafl2.zzcgo;
            Bundle bundle4 = zzaef3.zzcdw;
            Bundle bundle5 = zzafl2.zzcgn;
            hashMap.put("am", Integer.valueOf(zzaga2.zzcjk));
            hashMap.put("cog", zzv(zzaga2.zzcjl));
            hashMap.put("coh", zzv(zzaga2.zzcjm));
            if (!TextUtils.isEmpty(zzaga2.zzcjn)) {
                hashMap.put("carrier", zzaga2.zzcjn);
            }
            hashMap.put("gl", zzaga2.zzcjo);
            if (zzaga2.zzcjp) {
                hashMap.put("simulator", Integer.valueOf(1));
            }
            if (zzaga2.zzcjq) {
                hashMap.put("is_sidewinder", Integer.valueOf(1));
            }
            hashMap.put("ma", zzv(zzaga2.zzcjr));
            hashMap.put("sp", zzv(zzaga2.zzcjs));
            hashMap.put("hl", zzaga2.zzcjt);
            if (!TextUtils.isEmpty(zzaga2.zzcju)) {
                hashMap.put("mv", zzaga2.zzcju);
            }
            hashMap.put("muv", Integer.valueOf(zzaga2.zzcjw));
            if (zzaga2.zzcjx != -2) {
                hashMap.put("cnt", Integer.valueOf(zzaga2.zzcjx));
            }
            hashMap.put("gnt", Integer.valueOf(zzaga2.zzcjy));
            hashMap.put("pt", Integer.valueOf(zzaga2.zzcjz));
            hashMap.put("rm", Integer.valueOf(zzaga2.zzcka));
            hashMap.put("riv", Integer.valueOf(zzaga2.zzckb));
            Bundle bundle6 = new Bundle();
            bundle6.putString("build_build", zzaga2.zzckg);
            bundle6.putString("build_device", zzaga2.zzckh);
            Bundle bundle7 = new Bundle();
            bundle7.putBoolean("is_charging", zzaga2.zzckd);
            Bundle bundle8 = bundle4;
            bundle7.putDouble("battery_level", zzaga2.zzckc);
            bundle6.putBundle("battery", bundle7);
            Bundle bundle9 = new Bundle();
            bundle9.putInt("active_network_state", zzaga2.zzckf);
            bundle9.putBoolean("active_network_metered", zzaga2.zzcke);
            if (zzagk != null) {
                Bundle bundle10 = new Bundle();
                bundle10.putInt("predicted_latency_micros", zzagk.zzckq);
                bundle10.putLong("predicted_down_throughput_bps", zzagk.zzckr);
                bundle10.putLong("predicted_up_throughput_bps", zzagk.zzcks);
                bundle9.putBundle("predictions", bundle10);
            }
            bundle6.putBundle("network", bundle9);
            Bundle bundle11 = new Bundle();
            bundle11.putBoolean("is_browser_custom_tabs_capable", zzaga2.zzcki);
            bundle6.putBundle("browser", bundle11);
            if (bundle8 != null) {
                String str9 = "android_mem_info";
                Bundle bundle12 = new Bundle();
                zzaef = zzaef3;
                location = location2;
                bundle = bundle3;
                jSONObject = jSONObject2;
                Bundle bundle13 = bundle8;
                bundle12.putString("runtime_free", Long.toString(bundle13.getLong("runtime_free_memory", -1)));
                zzaga = zzaga2;
                bundle12.putString("runtime_max", Long.toString(bundle13.getLong("runtime_max_memory", -1)));
                bundle12.putString("runtime_total", Long.toString(bundle13.getLong("runtime_total_memory", -1)));
                i = 0;
                bundle12.putString("web_view_count", Integer.toString(bundle13.getInt("web_view_count", 0)));
                MemoryInfo memoryInfo = (MemoryInfo) bundle13.getParcelable("debug_memory_info");
                if (memoryInfo != null) {
                    bundle12.putString("debug_info_dalvik_private_dirty", Integer.toString(memoryInfo.dalvikPrivateDirty));
                    bundle12.putString("debug_info_dalvik_pss", Integer.toString(memoryInfo.dalvikPss));
                    bundle12.putString("debug_info_dalvik_shared_dirty", Integer.toString(memoryInfo.dalvikSharedDirty));
                    bundle12.putString("debug_info_native_private_dirty", Integer.toString(memoryInfo.nativePrivateDirty));
                    bundle12.putString("debug_info_native_pss", Integer.toString(memoryInfo.nativePss));
                    bundle12.putString("debug_info_native_shared_dirty", Integer.toString(memoryInfo.nativeSharedDirty));
                    bundle12.putString("debug_info_other_private_dirty", Integer.toString(memoryInfo.otherPrivateDirty));
                    bundle12.putString("debug_info_other_pss", Integer.toString(memoryInfo.otherPss));
                    bundle12.putString("debug_info_other_shared_dirty", Integer.toString(memoryInfo.otherSharedDirty));
                }
                bundle6.putBundle(str9, bundle12);
            } else {
                zzaef = zzaef3;
                location = location2;
                zzaga = zzaga2;
                bundle = bundle3;
                jSONObject = jSONObject2;
                i = 0;
            }
            Bundle bundle14 = new Bundle();
            bundle14.putBundle("parental_controls", bundle5);
            zzaga zzaga3 = zzaga;
            if (!TextUtils.isEmpty(zzaga3.zzcjv)) {
                bundle14.putString("package_version", zzaga3.zzcjv);
            }
            bundle6.putBundle("play_store", bundle14);
            hashMap.put("device", bundle6);
            Bundle bundle15 = new Bundle();
            bundle15.putString("doritos", zzafl2.zzcgp);
            bundle15.putString("doritos_v2", zzafl2.zzcgq);
            if (((Boolean) zzkb.zzik().zzd(zznk.zzayj)).booleanValue()) {
                if (zzafl2.zzcgr != null) {
                    str2 = zzafl2.zzcgr.getId();
                    z = zzafl2.zzcgr.isLimitAdTrackingEnabled();
                } else {
                    z = i;
                    str2 = null;
                }
                if (!TextUtils.isEmpty(str2)) {
                    bundle15.putString("rdid", str2);
                    bundle15.putBoolean("is_lat", z);
                    str3 = "idtype";
                    str4 = "adid";
                } else {
                    zzkb.zzif();
                    bundle15.putString("pdid", zzamu.zzbd(context));
                    str3 = "pdidtype";
                    str4 = "ssaid";
                }
                bundle15.putString(str3, str4);
            }
            hashMap.put("pii", bundle15);
            hashMap.put("platform", Build.MANUFACTURER);
            hashMap.put("submodel", Build.MODEL);
            if (location != null) {
                zza(hashMap, location);
                zzaef2 = zzaef;
            } else {
                zzaef2 = zzaef;
                if (zzaef2.zzccv.versionCode >= 2 && zzaef2.zzccv.zzaqe != null) {
                    zza(hashMap, zzaef2.zzccv.zzaqe);
                }
            }
            if (zzaef2.versionCode >= 2) {
                hashMap.put("quality_signals", zzaef2.zzcda);
            }
            if (zzaef2.versionCode >= 4 && zzaef2.zzcdd) {
                hashMap.put("forceHttps", Boolean.valueOf(zzaef2.zzcdd));
            }
            if (bundle != null) {
                hashMap.put("content_info", bundle);
            }
            if (zzaef2.versionCode >= 5) {
                hashMap.put("u_sd", Float.valueOf(zzaef2.zzagu));
                hashMap.put("sh", Integer.valueOf(zzaef2.zzcdf));
                hashMap.put("sw", Integer.valueOf(zzaef2.zzcde));
            } else {
                hashMap.put("u_sd", Float.valueOf(zzaga3.zzagu));
                hashMap.put("sh", Integer.valueOf(zzaga3.zzcdf));
                hashMap.put("sw", Integer.valueOf(zzaga3.zzcde));
            }
            if (zzaef2.versionCode >= 6) {
                if (!TextUtils.isEmpty(zzaef2.zzcdg)) {
                    try {
                        hashMap.put("view_hierarchy", new JSONObject(zzaef2.zzcdg));
                    } catch (JSONException e2) {
                        zzakb.zzc("Problem serializing view hierarchy to JSON", e2);
                    }
                }
                hashMap.put("correlation_id", Long.valueOf(zzaef2.zzcdh));
            }
            if (zzaef2.versionCode >= 7) {
                hashMap.put("request_id", zzaef2.zzcdi);
            }
            if (zzaef2.versionCode >= 12 && !TextUtils.isEmpty(zzaef2.zzcdm)) {
                hashMap.put("anchor", zzaef2.zzcdm);
            }
            if (zzaef2.versionCode >= 13) {
                hashMap.put("android_app_volume", Float.valueOf(zzaef2.zzcdn));
            }
            if (zzaef2.versionCode >= 18) {
                hashMap.put("android_app_muted", Boolean.valueOf(zzaef2.zzcdt));
            }
            if (zzaef2.versionCode >= 14 && zzaef2.zzcdo > 0) {
                hashMap.put("target_api", Integer.valueOf(zzaef2.zzcdo));
            }
            if (zzaef2.versionCode >= 15) {
                String str10 = "scroll_index";
                int i6 = -1;
                if (zzaef2.zzcdp != -1) {
                    i6 = zzaef2.zzcdp;
                }
                hashMap.put(str10, Integer.valueOf(i6));
            }
            if (zzaef2.versionCode >= 16) {
                hashMap.put("_activity_context", Boolean.valueOf(zzaef2.zzcdq));
            }
            if (zzaef2.versionCode >= 18) {
                if (!TextUtils.isEmpty(zzaef2.zzcdu)) {
                    try {
                        hashMap.put("app_settings", new JSONObject(zzaef2.zzcdu));
                    } catch (JSONException e3) {
                        zzakb.zzc("Problem creating json from app settings", e3);
                    }
                }
                hashMap.put("render_in_browser", Boolean.valueOf(zzaef2.zzbss));
            }
            if (zzaef2.versionCode >= 18) {
                hashMap.put("android_num_video_cache_tasks", Integer.valueOf(zzaef2.zzcdv));
            }
            zzang zzang = zzaef2.zzacr;
            boolean z5 = zzaef2.zzceh;
            boolean z6 = zzafl2.zzcgv;
            boolean z7 = zzaef2.zzcej;
            Bundle bundle16 = new Bundle();
            Bundle bundle17 = new Bundle();
            bundle17.putString("cl", "193400285");
            bundle17.putString("rapid_rc", "dev");
            bundle17.putString("rapid_rollup", HttpRequest.METHOD_HEAD);
            bundle16.putBundle("build_meta", bundle17);
            bundle16.putString("mf", Boolean.toString(((Boolean) zzkb.zzik().zzd(zznk.zzbbm)).booleanValue()));
            bundle16.putBoolean("instant_app", z5);
            bundle16.putBoolean("lite", zzang.zzcvh);
            bundle16.putBoolean("local_service", z6);
            bundle16.putBoolean("is_privileged_process", z7);
            hashMap.put("sdk_env", bundle16);
            hashMap.put("cache_state", jSONObject);
            if (zzaef2.versionCode >= 19) {
                hashMap.put("gct", zzaef2.zzcdx);
            }
            if (zzaef2.versionCode >= 21 && zzaef2.zzcdy) {
                hashMap.put("de", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzayy)).booleanValue()) {
                String str11 = zzaef2.zzacv.zzarb;
                if (!str11.equals("interstitial_mb")) {
                    if (!str11.equals("reward_mb")) {
                        i2 = i;
                        bundle2 = zzaef2.zzcdz;
                        int i7 = bundle2 == null ? 1 : i;
                        if (!(i2 == 0 || i7 == 0)) {
                            Bundle bundle18 = new Bundle();
                            bundle18.putBundle("interstitial_pool", bundle2);
                            hashMap.put("counters", bundle18);
                        }
                    }
                }
                i2 = 1;
                bundle2 = zzaef2.zzcdz;
                if (bundle2 == null) {
                }
                Bundle bundle182 = new Bundle();
                bundle182.putBundle("interstitial_pool", bundle2);
                hashMap.put("counters", bundle182);
            }
            if (zzaef2.zzcea != null) {
                hashMap.put("gmp_app_id", zzaef2.zzcea);
            }
            if (zzaef2.zzceb == null) {
                str = "fbs_aiid";
                obj = "";
            } else if ("TIME_OUT".equals(zzaef2.zzceb)) {
                str = "sai_timeout";
                obj = zzkb.zzik().zzd(zznk.zzaxt);
            } else {
                str = "fbs_aiid";
                obj = zzaef2.zzceb;
            }
            hashMap.put(str, obj);
            if (zzaef2.zzcec != null) {
                hashMap.put("fbs_aeid", zzaef2.zzcec);
            }
            if (zzaef2.versionCode >= 24) {
                hashMap.put("disable_ml", Boolean.valueOf(zzaef2.zzcei));
            }
            String str12 = (String) zzkb.zzik().zzd(zznk.zzavo);
            if (str12 != null && !str12.isEmpty()) {
                if (VERSION.SDK_INT >= ((Integer) zzkb.zzik().zzd(zznk.zzavp)).intValue()) {
                    HashMap hashMap2 = new HashMap();
                    String[] split = str12.split(",");
                    int length2 = split.length;
                    while (i < length2) {
                        String str13 = split[i];
                        hashMap2.put(str13, zzams.zzdd(str13));
                        i++;
                    }
                    hashMap.put("video_decoders", hashMap2);
                }
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzbet)).booleanValue()) {
                hashMap.put("omid_v", zzbv.zzfa().getVersion(context));
            }
            if (zzaef2.zzcek != null && !zzaef2.zzcek.isEmpty()) {
                hashMap.put("android_permissions", zzaef2.zzcek);
            }
            if (zzakb.isLoggable(2)) {
                String str14 = "Ad Request JSON: ";
                String valueOf2 = String.valueOf(zzbv.zzek().zzn(hashMap).toString(2));
                zzakb.v(valueOf2.length() != 0 ? str14.concat(valueOf2) : new String(str14));
            }
            return zzbv.zzek().zzn(hashMap);
        } catch (JSONException e4) {
            String str15 = "Problem serializing ad request to JSON: ";
            String valueOf3 = String.valueOf(e4.getMessage());
            zzakb.zzdk(valueOf3.length() != 0 ? str15.concat(valueOf3) : new String(str15));
            return null;
        }
    }

    private static void zza(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put("lat", valueOf3);
        hashMap2.put("long", valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x015e  */
    public static JSONObject zzb(zzaej zzaej) throws JSONException {
        String str;
        String str2;
        JSONObject jSONObject = new JSONObject();
        if (zzaej.zzbyq != null) {
            jSONObject.put("ad_base_url", zzaej.zzbyq);
        }
        if (zzaej.zzcet != null) {
            jSONObject.put("ad_size", zzaej.zzcet);
        }
        jSONObject.put(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_NATIVE, zzaej.zzare);
        jSONObject.put(zzaej.zzare ? "ad_json" : "ad_html", zzaej.zzceo);
        if (zzaej.zzcev != null) {
            jSONObject.put("debug_dialog", zzaej.zzcev);
        }
        if (zzaej.zzcfl != null) {
            jSONObject.put("debug_signals", zzaej.zzcfl);
        }
        if (zzaej.zzcep != -1) {
            jSONObject.put("interstitial_timeout", ((double) zzaej.zzcep) / 1000.0d);
        }
        if (zzaej.orientation == zzbv.zzem().zzrm()) {
            str = "orientation";
            str2 = "portrait";
        } else {
            if (zzaej.orientation == zzbv.zzem().zzrl()) {
                str = "orientation";
                str2 = "landscape";
            }
            if (zzaej.zzbsn != null) {
                jSONObject.put("click_urls", zzm(zzaej.zzbsn));
            }
            if (zzaej.zzbso != null) {
                jSONObject.put("impression_urls", zzm(zzaej.zzbso));
            }
            if (zzaej.zzbsp != null) {
                jSONObject.put("downloaded_impression_urls", zzm(zzaej.zzbsp));
            }
            if (zzaej.zzces != null) {
                jSONObject.put("manual_impression_urls", zzm(zzaej.zzces));
            }
            if (zzaej.zzcey != null) {
                jSONObject.put("active_view", zzaej.zzcey);
            }
            jSONObject.put("ad_is_javascript", zzaej.zzcew);
            if (zzaej.zzcex != null) {
                jSONObject.put("ad_passback_url", zzaej.zzcex);
            }
            jSONObject.put("mediation", zzaej.zzceq);
            jSONObject.put("custom_render_allowed", zzaej.zzcez);
            jSONObject.put("content_url_opted_out", zzaej.zzcfa);
            jSONObject.put("content_vertical_opted_out", zzaej.zzcfm);
            jSONObject.put("prefetch", zzaej.zzcfb);
            if (zzaej.zzbsu != -1) {
                jSONObject.put("refresh_interval_milliseconds", zzaej.zzbsu);
            }
            if (zzaej.zzcer != -1) {
                jSONObject.put("mediation_config_cache_time_milliseconds", zzaej.zzcer);
            }
            if (!TextUtils.isEmpty(zzaej.zzamj)) {
                jSONObject.put("gws_query_id", zzaej.zzamj);
            }
            jSONObject.put("fluid", !zzaej.zzarf ? SettingsJsonConstants.ICON_HEIGHT_KEY : "");
            jSONObject.put("native_express", zzaej.zzarg);
            if (zzaej.zzcff != null) {
                jSONObject.put("video_start_urls", zzm(zzaej.zzcff));
            }
            if (zzaej.zzcfg != null) {
                jSONObject.put("video_complete_urls", zzm(zzaej.zzcfg));
            }
            if (zzaej.zzcfe != null) {
                zzaig zzaig = zzaej.zzcfe;
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("rb_type", zzaig.type);
                jSONObject2.put("rb_amount", zzaig.zzcmk);
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(jSONObject2);
                jSONObject.put("rewards", jSONArray);
            }
            jSONObject.put("use_displayed_impression", zzaej.zzcfh);
            jSONObject.put("auto_protection_configuration", zzaej.zzcfi);
            jSONObject.put("render_in_browser", zzaej.zzbss);
            jSONObject.put("disable_closable_area", zzaej.zzzm);
            return jSONObject;
        }
        jSONObject.put(str, str2);
        if (zzaej.zzbsn != null) {
        }
        if (zzaej.zzbso != null) {
        }
        if (zzaej.zzbsp != null) {
        }
        if (zzaej.zzces != null) {
        }
        if (zzaej.zzcey != null) {
        }
        jSONObject.put("ad_is_javascript", zzaej.zzcew);
        if (zzaej.zzcex != null) {
        }
        jSONObject.put("mediation", zzaej.zzceq);
        jSONObject.put("custom_render_allowed", zzaej.zzcez);
        jSONObject.put("content_url_opted_out", zzaej.zzcfa);
        jSONObject.put("content_vertical_opted_out", zzaej.zzcfm);
        jSONObject.put("prefetch", zzaej.zzcfb);
        if (zzaej.zzbsu != -1) {
        }
        if (zzaej.zzcer != -1) {
        }
        if (!TextUtils.isEmpty(zzaej.zzamj)) {
        }
        jSONObject.put("fluid", !zzaej.zzarf ? SettingsJsonConstants.ICON_HEIGHT_KEY : "");
        jSONObject.put("native_express", zzaej.zzarg);
        if (zzaej.zzcff != null) {
        }
        if (zzaej.zzcfg != null) {
        }
        if (zzaej.zzcfe != null) {
        }
        jSONObject.put("use_displayed_impression", zzaej.zzcfh);
        jSONObject.put("auto_protection_configuration", zzaej.zzcfi);
        jSONObject.put("render_in_browser", zzaej.zzbss);
        jSONObject.put("disable_closable_area", zzaej.zzzm);
        return jSONObject;
    }

    @Nullable
    private static JSONArray zzm(List<String> list) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return jSONArray;
    }

    private static Integer zzv(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }
}
