package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;
import java.util.List;

public abstract class zzyg extends zzek implements zzyf {
    public zzyg() {
        super("com.google.android.gms.ads.internal.mediation.client.IUnifiedNativeAdMapper");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004a, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0099, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b4, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bb, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003c, code lost:
        r3.writeNoException();
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        IInterface iInterface;
        boolean z;
        switch (i) {
            case 2:
                str = getHeadline();
                break;
            case 3:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                break;
            case 4:
                str = getBody();
                break;
            case 5:
                iInterface = zzjz();
                break;
            case 6:
                str = getCallToAction();
                break;
            case 7:
                str = getAdvertiser();
                break;
            case 8:
                double starRating = getStarRating();
                parcel2.writeNoException();
                parcel2.writeDouble(starRating);
                break;
            case 9:
                str = getStore();
                break;
            case 10:
                str = getPrice();
                break;
            case 11:
                iInterface = getVideoController();
                break;
            case 12:
                iInterface = zzkf();
                break;
            case 13:
                iInterface = zzmv();
                break;
            case 14:
                iInterface = zzmw();
                break;
            case 15:
                iInterface = zzke();
                break;
            case 16:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzel.zzb(parcel2, extras);
                break;
            case 17:
                z = getOverrideImpressionRecording();
                break;
            case 18:
                z = getOverrideClickHandling();
                break;
            case 19:
                recordImpression();
                break;
            case 20:
                zzj(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 21:
                zzb(Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 22:
                zzl(Stub.asInterface(parcel.readStrongBinder()));
                break;
            default:
                return false;
        }
    }
}
