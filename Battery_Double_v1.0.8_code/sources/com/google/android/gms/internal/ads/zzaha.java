package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;

public abstract class zzaha extends zzek implements zzagz {
    public zzaha() {
        super("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    public static zzagz zzy(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
        return queryLocalInterface instanceof zzagz ? (zzagz) queryLocalInterface : new zzahb(iBinder);
    }

    /* JADX WARNING: type inference failed for: r4v2 */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.google.android.gms.internal.ads.zzahe] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.google.android.gms.internal.ads.zzahg] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.google.android.gms.internal.ads.zzahe] */
    /* JADX WARNING: type inference failed for: r4v8, types: [com.google.android.gms.internal.ads.zzagx] */
    /* JADX WARNING: type inference failed for: r4v10, types: [com.google.android.gms.internal.ads.zzagy] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.google.android.gms.internal.ads.zzagx] */
    /* JADX WARNING: type inference failed for: r4v13 */
    /* JADX WARNING: type inference failed for: r4v14 */
    /* JADX WARNING: type inference failed for: r4v15 */
    /* JADX WARNING: type inference failed for: r4v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r4v2
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.zzagy, com.google.android.gms.internal.ads.zzahg, com.google.android.gms.internal.ads.zzahe, com.google.android.gms.internal.ads.zzagx]
  uses: [com.google.android.gms.internal.ads.zzahe, com.google.android.gms.internal.ads.zzagx]
  mth insns count: 64
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 5 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 34) {
            ? r4 = 0;
            switch (i) {
                case 1:
                    zza((zzahk) zzel.zza(parcel, zzahk.CREATOR));
                    break;
                case 2:
                    show();
                    break;
                case 3:
                    IBinder readStrongBinder = parcel.readStrongBinder();
                    if (readStrongBinder != null) {
                        IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
                        r4 = queryLocalInterface instanceof zzahe ? (zzahe) queryLocalInterface : new zzahg(readStrongBinder);
                    }
                    zza((zzahe) r4);
                    break;
                default:
                    switch (i) {
                        case 5:
                            boolean isLoaded = isLoaded();
                            parcel2.writeNoException();
                            zzel.zza(parcel2, isLoaded);
                            break;
                        case 6:
                            pause();
                            break;
                        case 7:
                            resume();
                            break;
                        case 8:
                            destroy();
                            break;
                        case 9:
                            zzd(Stub.asInterface(parcel.readStrongBinder()));
                            break;
                        case 10:
                            zze(Stub.asInterface(parcel.readStrongBinder()));
                            break;
                        case 11:
                            zzf(Stub.asInterface(parcel.readStrongBinder()));
                            break;
                        case 12:
                            String mediationAdapterClassName = getMediationAdapterClassName();
                            parcel2.writeNoException();
                            parcel2.writeString(mediationAdapterClassName);
                            break;
                        case 13:
                            setUserId(parcel.readString());
                            break;
                        case 14:
                            zza(zzky.zzc(parcel.readStrongBinder()));
                            break;
                        case 15:
                            Bundle zzba = zzba();
                            parcel2.writeNoException();
                            zzel.zzb(parcel2, zzba);
                            break;
                        case 16:
                            IBinder readStrongBinder2 = parcel.readStrongBinder();
                            if (readStrongBinder2 != null) {
                                IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedAdSkuListener");
                                r4 = queryLocalInterface2 instanceof zzagx ? (zzagx) queryLocalInterface2 : new zzagy(readStrongBinder2);
                            }
                            zza((zzagx) r4);
                            break;
                        default:
                            return false;
                    }
            }
        } else {
            setImmersiveMode(zzel.zza(parcel));
        }
        parcel2.writeNoException();
        return true;
    }
}
