package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.IOUtils;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

final class zzaew implements Runnable {
    private final /* synthetic */ OutputStream zzcfw;
    private final /* synthetic */ byte[] zzcfx;

    zzaew(zzaev zzaev, OutputStream outputStream, byte[] bArr) {
        this.zzcfw = outputStream;
        this.zzcfx = bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f  */
    public final void run() {
        Closeable closeable;
        Closeable closeable2;
        Throwable e;
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(this.zzcfw);
            try {
                dataOutputStream.writeInt(this.zzcfx.length);
                dataOutputStream.write(this.zzcfx);
                IOUtils.closeQuietly((Closeable) dataOutputStream);
            } catch (IOException e2) {
                e = e2;
                closeable2 = dataOutputStream;
                try {
                    zzakb.zzb("Error transporting the ad response", e);
                    zzbv.zzeo().zza(e, "LargeParcelTeleporter.pipeData.1");
                    if (closeable2 != 0) {
                        IOUtils.closeQuietly((Closeable) this.zzcfw);
                    } else {
                        IOUtils.closeQuietly(closeable2);
                    }
                } catch (Throwable th) {
                    th = th;
                    closeable = closeable2;
                    if (closeable == 0) {
                        closeable = this.zzcfw;
                    }
                    IOUtils.closeQuietly(closeable);
                    throw th;
                }
            }
        } catch (IOException e3) {
            Throwable th2 = e3;
            closeable2 = 0;
            e = th2;
            zzakb.zzb("Error transporting the ad response", e);
            zzbv.zzeo().zza(e, "LargeParcelTeleporter.pipeData.1");
            if (closeable2 != 0) {
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            closeable = 0;
            th = th4;
            if (closeable == 0) {
            }
            IOUtils.closeQuietly(closeable);
            throw th;
        }
    }
}
