package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;
import java.util.List;

public abstract class zzxr extends zzek implements zzxq {
    public zzxr() {
        super("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0070, code lost:
        r11.writeNoException();
        com.google.android.gms.internal.ads.zzel.zzb(r11, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cf, code lost:
        r11.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r11, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01a2, code lost:
        r11.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01dc, code lost:
        r11.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r11, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0223, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        IInterface iInterface;
        boolean z;
        Bundle bundle;
        zzxt zzxt = null;
        switch (i) {
            case 1:
                IObjectWrapper asInterface = Stub.asInterface(parcel.readStrongBinder());
                zzjn zzjn = (zzjn) zzel.zza(parcel, zzjn.CREATOR);
                zzjj zzjj = (zzjj) zzel.zza(parcel, zzjj.CREATOR);
                String readString = parcel.readString();
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzxt = queryLocalInterface instanceof zzxt ? (zzxt) queryLocalInterface : new zzxv(readStrongBinder);
                }
                zza(asInterface, zzjn, zzjj, readString, zzxt);
                break;
            case 2:
                iInterface = getView();
                break;
            case 3:
                IObjectWrapper asInterface2 = Stub.asInterface(parcel.readStrongBinder());
                zzjj zzjj2 = (zzjj) zzel.zza(parcel, zzjj.CREATOR);
                String readString2 = parcel.readString();
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzxt = queryLocalInterface2 instanceof zzxt ? (zzxt) queryLocalInterface2 : new zzxv(readStrongBinder2);
                }
                zza(asInterface2, zzjj2, readString2, zzxt);
                break;
            case 4:
                showInterstitial();
                break;
            case 5:
                destroy();
                break;
            case 6:
                IObjectWrapper asInterface3 = Stub.asInterface(parcel.readStrongBinder());
                zzjn zzjn2 = (zzjn) zzel.zza(parcel, zzjn.CREATOR);
                zzjj zzjj3 = (zzjj) zzel.zza(parcel, zzjj.CREATOR);
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzxt = queryLocalInterface3 instanceof zzxt ? (zzxt) queryLocalInterface3 : new zzxv(readStrongBinder3);
                }
                zza(asInterface3, zzjn2, zzjj3, readString3, readString4, zzxt);
                break;
            case 7:
                IObjectWrapper asInterface4 = Stub.asInterface(parcel.readStrongBinder());
                zzjj zzjj4 = (zzjj) zzel.zza(parcel, zzjj.CREATOR);
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzxt = queryLocalInterface4 instanceof zzxt ? (zzxt) queryLocalInterface4 : new zzxv(readStrongBinder4);
                }
                zza(asInterface4, zzjj4, readString5, readString6, zzxt);
                break;
            case 8:
                pause();
                break;
            case 9:
                resume();
                break;
            case 10:
                zza(Stub.asInterface(parcel.readStrongBinder()), (zzjj) zzel.zza(parcel, zzjj.CREATOR), parcel.readString(), zzaid.zzaa(parcel.readStrongBinder()), parcel.readString());
                break;
            case 11:
                zzc((zzjj) zzel.zza(parcel, zzjj.CREATOR), parcel.readString());
                break;
            case 12:
                showVideo();
                break;
            case 13:
                z = isInitialized();
                break;
            case 14:
                IObjectWrapper asInterface5 = Stub.asInterface(parcel.readStrongBinder());
                zzjj zzjj5 = (zzjj) zzel.zza(parcel, zzjj.CREATOR);
                String readString7 = parcel.readString();
                String readString8 = parcel.readString();
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener");
                    zzxt = queryLocalInterface5 instanceof zzxt ? (zzxt) queryLocalInterface5 : new zzxv(readStrongBinder5);
                }
                zza(asInterface5, zzjj5, readString7, readString8, zzxt, (zzpl) zzel.zza(parcel, zzpl.CREATOR), parcel.createStringArrayList());
                break;
            case 15:
                iInterface = zzmo();
                break;
            case 16:
                iInterface = zzmp();
                break;
            case 17:
                bundle = zzmq();
                break;
            case 18:
                bundle = getInterstitialAdapterInfo();
                break;
            case 19:
                bundle = zzmr();
                break;
            case 20:
                zza((zzjj) zzel.zza(parcel, zzjj.CREATOR), parcel.readString(), parcel.readString());
                break;
            case 21:
                zzi(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 22:
                z = zzms();
                break;
            case 23:
                zza(Stub.asInterface(parcel.readStrongBinder()), zzaid.zzaa(parcel.readStrongBinder()), (List<String>) parcel.createStringArrayList());
                break;
            case 24:
                iInterface = zzmt();
                break;
            case 25:
                setImmersiveMode(zzel.zza(parcel));
                break;
            case 26:
                iInterface = getVideoController();
                break;
            case 27:
                iInterface = zzmu();
                break;
            default:
                return false;
        }
    }
}
