package com.google.android.gms.internal.stable;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import java.util.HashMap;

public final class zze {

    public static class zza implements BaseColumns {
        private static HashMap<Uri, zzh> zzagq = new HashMap<>();

        private static zzh zza(ContentResolver contentResolver, Uri uri) {
            zzh zzh = (zzh) zzagq.get(uri);
            if (zzh == null) {
                zzh zzh2 = new zzh();
                zzagq.put(uri, zzh2);
                contentResolver.registerContentObserver(uri, true, new zzf(null, zzh2));
                return zzh2;
            } else if (!zzh.zzagu.getAndSet(false)) {
                return zzh;
            } else {
                synchronized (zzh) {
                    zzh.zzags.clear();
                    zzh.zzagt = new Object();
                }
                return zzh;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
            r3 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r4 = r11;
            r5 = r12;
            r11 = r4.query(r5, new java.lang.String[]{com.google.firebase.analytics.FirebaseAnalytics.Param.VALUE}, "name=?", new java.lang.String[]{r13}, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
            if (r11 == null) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003a, code lost:
            if (r11.moveToFirst() != false) goto L_0x003d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
            r2 = r11.getString(0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            zza(r1, r0, r13, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
            if (r11 == null) goto L_0x0080;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
            r11.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0049, code lost:
            return r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x004a, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x004c, code lost:
            r12 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x004d, code lost:
            r3 = r11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x004f, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0050, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0051, code lost:
            r3 = r11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
            zza(r1, r0, r13, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0056, code lost:
            if (r11 == null) goto L_0x005b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0058, code lost:
            r11.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x005b, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x005c, code lost:
            r12 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x005e, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x005f, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0060, code lost:
            r11 = "GoogleSettings";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
            r1 = new java.lang.StringBuilder("Can't get key ");
            r1.append(r13);
            r1.append(" from ");
            r1.append(r12);
            android.util.Log.e(r11, r1.toString(), r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x007b, code lost:
            if (r3 != null) goto L_0x007d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x007d, code lost:
            r3.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0080, code lost:
            return r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x0081, code lost:
            if (r3 != null) goto L_0x0083;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0083, code lost:
            r3.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0086, code lost:
            throw r12;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x004c A[ExcHandler: all (th java.lang.Throwable), Splitter:B:17:0x0036] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x007d  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x0083  */
        protected static String zza(ContentResolver contentResolver, Uri uri, String str) {
            zzh zza;
            synchronized (zza.class) {
                zza = zza(contentResolver, uri);
            }
            synchronized (zza) {
                Object obj = zza.zzagt;
                if (zza.zzags.containsKey(str)) {
                    String str2 = (String) zza.zzags.get(str);
                    return str2;
                }
            }
        }

        private static void zza(zzh zzh, Object obj, String str, String str2) {
            synchronized (zzh) {
                if (obj == zzh.zzagt) {
                    zzh.zzags.put(str, str2);
                }
            }
        }
    }
}
