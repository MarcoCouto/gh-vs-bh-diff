package com.google.android.gms.internal.measurement;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public final class zzfc extends zzhh {
    private final zzfd zzaig = new zzfd(this, getContext(), "google_app_measurement_local.db");
    private boolean zzaih;

    zzfc(zzgl zzgl) {
        super(zzgl);
    }

    @WorkerThread
    @VisibleForTesting
    private final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        if (this.zzaih) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.zzaig.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.zzaih = true;
        return null;
    }

    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [boolean, int] */
    /* JADX WARNING: type inference failed for: r7v0 */
    /* JADX WARNING: type inference failed for: r12v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v0, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v1 */
    /* JADX WARNING: type inference failed for: r7v1 */
    /* JADX WARNING: type inference failed for: r12v1 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r9v2 */
    /* JADX WARNING: type inference failed for: r7v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v3 */
    /* JADX WARNING: type inference failed for: r9v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v5 */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v6 */
    /* JADX WARNING: type inference failed for: r12v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r7v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v7 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r9v8 */
    /* JADX WARNING: type inference failed for: r12v4 */
    /* JADX WARNING: type inference failed for: r9v9, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r12v5 */
    /* JADX WARNING: type inference failed for: r12v6 */
    /* JADX WARNING: type inference failed for: r12v8, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r7v7 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r7v9 */
    /* JADX WARNING: type inference failed for: r12v9 */
    /* JADX WARNING: type inference failed for: r7v10 */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r3v19 */
    /* JADX WARNING: type inference failed for: r7v11 */
    /* JADX WARNING: type inference failed for: r7v12 */
    /* JADX WARNING: type inference failed for: r7v13 */
    /* JADX WARNING: type inference failed for: r7v14 */
    /* JADX WARNING: type inference failed for: r9v10 */
    /* JADX WARNING: type inference failed for: r3v20 */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: type inference failed for: r9v12 */
    /* JADX WARNING: type inference failed for: r7v15 */
    /* JADX WARNING: type inference failed for: r7v16 */
    /* JADX WARNING: type inference failed for: r9v13 */
    /* JADX WARNING: type inference failed for: r9v14 */
    /* JADX WARNING: type inference failed for: r7v17 */
    /* JADX WARNING: type inference failed for: r7v18 */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r9v15 */
    /* JADX WARNING: type inference failed for: r9v16 */
    /* JADX WARNING: type inference failed for: r9v17 */
    /* JADX WARNING: type inference failed for: r9v18 */
    /* JADX WARNING: type inference failed for: r9v19 */
    /* JADX WARNING: type inference failed for: r9v20 */
    /* JADX WARNING: type inference failed for: r12v12 */
    /* JADX WARNING: type inference failed for: r12v13 */
    /* JADX WARNING: type inference failed for: r12v14 */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00cf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00d0, code lost:
        r2 = r0;
        r12 = 0;
        r9 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d6, code lost:
        r3 = r0;
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00db, code lost:
        r7 = 0;
        r9 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00dd, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v1, types: [boolean, int]
  assigns: []
  uses: [?[int, short, byte, char], int, boolean]
  mth insns count: 181
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0146 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0146 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0146 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:9:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ee A[SYNTHETIC, Splitter:B:60:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0156  */
    /* JADX WARNING: Unknown variable types count: 19 */
    @WorkerThread
    private final boolean zza(int i, byte[] bArr) {
        int i2;
        int i3;
        ? r3;
        ? r7;
        ? r12;
        ? r9;
        Throwable th;
        ? r92;
        ? r72;
        ? r93;
        ? r73;
        Object obj;
        ? r94;
        ? r74;
        ? r122;
        ? r75;
        Object obj2;
        ? writableDatabase;
        ? r123;
        ? r76;
        ? r77;
        zzab();
        ? r32 = 0;
        if (this.zzaih) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        i2 = 5;
        i3 = 0;
        int i4 = 5;
        r3 = r32;
        while (i3 < i2) {
            r7 = 0;
            try {
                writableDatabase = getWritableDatabase();
                if (writableDatabase == 0) {
                    try {
                        this.zzaih = true;
                        if (writableDatabase != 0) {
                            writableDatabase.close();
                        }
                        return r3;
                    } catch (SQLiteFullException e) {
                        e = e;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        obj2 = e2;
                        r123 = 0;
                        r75 = writableDatabase;
                        r122 = r123;
                        if (r75 != 0) {
                        }
                        zzge().zzim().zzg("Error writing entry to local database", obj2);
                        this.zzaih = true;
                        if (r122 != 0) {
                        }
                        if (r75 == 0) {
                        }
                        i3++;
                        i2 = 5;
                        r3 = 0;
                    }
                } else {
                    writableDatabase.beginTransaction();
                    long j = 0;
                    ? rawQuery = writableDatabase.rawQuery("select count(1) from messages", null);
                    if (rawQuery != 0) {
                        try {
                            if (rawQuery.moveToFirst()) {
                                j = rawQuery.getLong(r3);
                            }
                        } catch (SQLiteFullException e3) {
                            obj = e3;
                            r77 = rawQuery;
                            r94 = writableDatabase;
                            r92 = r93;
                            r72 = r73;
                            zzge().zzim().zzg("Error writing entry to local database", obj);
                            this.zzaih = true;
                            r92 = r93;
                            r72 = r73;
                            if (r73 != 0) {
                            }
                            if (r93 == 0) {
                            }
                            r93.close();
                            i3++;
                            i2 = 5;
                            r3 = 0;
                        } catch (SQLiteDatabaseLockedException unused2) {
                            r76 = rawQuery;
                            r93 = writableDatabase;
                            try {
                                r92 = r93;
                                r72 = r74;
                                SystemClock.sleep((long) i4);
                                r92 = r93;
                                r72 = r74;
                                i4 += 20;
                                if (r74 != 0) {
                                }
                                if (r93 == 0) {
                                }
                                r93.close();
                                i3++;
                                i2 = 5;
                                r3 = 0;
                            } catch (Throwable th2) {
                                th = th2;
                                r12 = r72;
                                r9 = r92;
                                if (r12 != 0) {
                                }
                                if (r9 != 0) {
                                }
                                throw th;
                            }
                        } catch (SQLiteException e4) {
                            obj2 = e4;
                            r123 = rawQuery;
                            r75 = writableDatabase;
                            r122 = r123;
                            if (r75 != 0) {
                            }
                            zzge().zzim().zzg("Error writing entry to local database", obj2);
                            this.zzaih = true;
                            if (r122 != 0) {
                            }
                            if (r75 == 0) {
                            }
                            i3++;
                            i2 = 5;
                            r3 = 0;
                        } catch (Throwable th3) {
                            th = th3;
                            r9 = writableDatabase;
                            r12 = rawQuery;
                            if (r12 != 0) {
                            }
                            if (r9 != 0) {
                            }
                            throw th;
                        }
                    }
                    if (j >= 100000) {
                        zzge().zzim().log("Data loss, local db full");
                        long j2 = (100000 - j) + 1;
                        String[] strArr = new String[1];
                        strArr[r3] = Long.toString(j2);
                        long delete = (long) writableDatabase.delete("messages", "rowid in (select rowid from messages order by rowid asc limit ?)", strArr);
                        if (delete != j2) {
                            zzge().zzim().zzd("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                        }
                    }
                    writableDatabase.insertOrThrow("messages", null, contentValues);
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                    if (writableDatabase != 0) {
                        writableDatabase.close();
                    }
                    return true;
                }
            } catch (SQLiteFullException e5) {
                obj = e5;
                r94 = 0;
                r77 = r7;
                r92 = r93;
                r72 = r73;
                zzge().zzim().zzg("Error writing entry to local database", obj);
                this.zzaih = true;
                r92 = r93;
                r72 = r73;
                if (r73 != 0) {
                    r73.close();
                }
                if (r93 == 0) {
                    i3++;
                    i2 = 5;
                    r3 = 0;
                }
                r93.close();
                i3++;
                i2 = 5;
                r3 = 0;
            } catch (SQLiteDatabaseLockedException unused3) {
                r93 = 0;
                r76 = r7;
                r92 = r93;
                r72 = r74;
                SystemClock.sleep((long) i4);
                r92 = r93;
                r72 = r74;
                i4 += 20;
                if (r74 != 0) {
                    r74.close();
                }
                if (r93 == 0) {
                    i3++;
                    i2 = 5;
                    r3 = 0;
                }
                r93.close();
                i3++;
                i2 = 5;
                r3 = 0;
            } catch (SQLiteException e6) {
                obj2 = e6;
                r122 = 0;
                r75 = r7;
                if (r75 != 0) {
                    try {
                        if (r75.inTransaction()) {
                            r75.endTransaction();
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        r9 = r75;
                        r12 = r122;
                        if (r12 != 0) {
                        }
                        if (r9 != 0) {
                        }
                        throw th;
                    }
                }
                zzge().zzim().zzg("Error writing entry to local database", obj2);
                this.zzaih = true;
                if (r122 != 0) {
                    r122.close();
                }
                if (r75 == 0) {
                    r75.close();
                }
                i3++;
                i2 = 5;
                r3 = 0;
            } catch (Throwable th5) {
                th = th5;
                r9 = 0;
                r12 = 0;
                if (r12 != 0) {
                    r12.close();
                }
                if (r9 != 0) {
                    r9.close();
                }
                throw th;
            }
        }
        zzge().zzip().log("Failed to write entry to local database");
        return false;
        obj = e;
        r77 = r7;
        r94 = writableDatabase;
        r92 = r93;
        r72 = r73;
        zzge().zzim().zzg("Error writing entry to local database", obj);
        this.zzaih = true;
        r92 = r93;
        r72 = r73;
        if (r73 != 0) {
        }
        if (r93 == 0) {
        }
        r93.close();
        i3++;
        i2 = 5;
        r3 = 0;
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    @WorkerThread
    public final void resetAnalyticsData() {
        zzab();
        try {
            int delete = 0 + getWritableDatabase().delete("messages", null, null);
            if (delete > 0) {
                zzge().zzit().zzg("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzge().zzim().zzg("Error resetting local analytics data. error", e);
        }
    }

    public final boolean zza(zzeu zzeu) {
        Parcel obtain = Parcel.obtain();
        zzeu.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(0, marshall);
        }
        zzge().zzip().log("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public final boolean zza(zzjx zzjx) {
        Parcel obtain = Parcel.obtain();
        zzjx.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(1, marshall);
        }
        zzge().zzip().log("User property too long for local database. Sending directly to service");
        return false;
    }

    public final /* bridge */ /* synthetic */ void zzab() {
        super.zzab();
    }

    public final /* bridge */ /* synthetic */ Clock zzbt() {
        return super.zzbt();
    }

    public final boolean zzc(zzed zzed) {
        zzgb();
        byte[] zza = zzka.zza((Parcelable) zzed);
        if (zza.length <= 131072) {
            return zza(2, zza);
        }
        zzge().zzip().log("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    public final /* bridge */ /* synthetic */ void zzfr() {
        super.zzfr();
    }

    public final /* bridge */ /* synthetic */ void zzfs() {
        super.zzfs();
    }

    public final /* bridge */ /* synthetic */ zzdu zzft() {
        return super.zzft();
    }

    public final /* bridge */ /* synthetic */ zzhk zzfu() {
        return super.zzfu();
    }

    public final /* bridge */ /* synthetic */ zzfb zzfv() {
        return super.zzfv();
    }

    public final /* bridge */ /* synthetic */ zzeo zzfw() {
        return super.zzfw();
    }

    public final /* bridge */ /* synthetic */ zzii zzfx() {
        return super.zzfx();
    }

    public final /* bridge */ /* synthetic */ zzif zzfy() {
        return super.zzfy();
    }

    public final /* bridge */ /* synthetic */ zzfc zzfz() {
        return super.zzfz();
    }

    public final /* bridge */ /* synthetic */ zzfe zzga() {
        return super.zzga();
    }

    public final /* bridge */ /* synthetic */ zzka zzgb() {
        return super.zzgb();
    }

    public final /* bridge */ /* synthetic */ zzjh zzgc() {
        return super.zzgc();
    }

    public final /* bridge */ /* synthetic */ zzgg zzgd() {
        return super.zzgd();
    }

    public final /* bridge */ /* synthetic */ zzfg zzge() {
        return super.zzge();
    }

    public final /* bridge */ /* synthetic */ zzfr zzgf() {
        return super.zzgf();
    }

    public final /* bridge */ /* synthetic */ zzef zzgg() {
        return super.zzgg();
    }

    /* access modifiers changed from: protected */
    public final boolean zzhf() {
        return false;
    }

    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable>] */
    /* JADX WARNING: type inference failed for: r9v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v1 */
    /* JADX WARNING: type inference failed for: r9v2 */
    /* JADX WARNING: type inference failed for: r3v5 */
    /* JADX WARNING: type inference failed for: r9v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v4 */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v5 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r9v6, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v7 */
    /* JADX WARNING: type inference failed for: r9v8 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r9v9 */
    /* JADX WARNING: type inference failed for: r9v10 */
    /* JADX WARNING: type inference failed for: r9v17, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r3v22 */
    /* JADX WARNING: type inference failed for: r9v18 */
    /* JADX WARNING: type inference failed for: r9v19 */
    /* JADX WARNING: type inference failed for: r9v20 */
    /* JADX WARNING: type inference failed for: r9v21 */
    /* JADX WARNING: type inference failed for: r9v22 */
    /* JADX WARNING: type inference failed for: r3v26 */
    /* JADX WARNING: type inference failed for: r9v23 */
    /* JADX WARNING: type inference failed for: r9v24 */
    /* JADX WARNING: type inference failed for: r3v27 */
    /* JADX WARNING: type inference failed for: r9v25 */
    /* JADX WARNING: type inference failed for: r9v26 */
    /* JADX WARNING: type inference failed for: r9v27 */
    /* JADX WARNING: type inference failed for: r3v28 */
    /* JADX WARNING: type inference failed for: r9v28 */
    /* JADX WARNING: type inference failed for: r9v29 */
    /* JADX WARNING: type inference failed for: r9v30 */
    /* JADX WARNING: type inference failed for: r3v29 */
    /* JADX WARNING: type inference failed for: r9v31 */
    /* JADX WARNING: type inference failed for: r9v32 */
    /* JADX WARNING: type inference failed for: r9v33 */
    /* JADX WARNING: type inference failed for: r9v34 */
    /* JADX WARNING: type inference failed for: r9v35 */
    /* JADX WARNING: type inference failed for: r9v36 */
    /* JADX WARNING: type inference failed for: r9v37 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:61|62|63|64) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:77|78|79|80) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:47|48|49|50|175) */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0186, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0187, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x018a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x018b, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0192, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0193, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x01f0, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        zzge().zzim().log("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r12.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        zzge().zzim().log("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r12.recycle();
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        zzge().zzim().log("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r12.recycle();
        r13 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00af */
    /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x00e0 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x0114 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r9v1
  assigns: []
  uses: []
  mth insns count: 253
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x01a3 A[SYNTHETIC, Splitter:B:125:0x01a3] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x01cd  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0208  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x01f8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x01f8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x01f8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x002e] */
    /* JADX WARNING: Unknown variable types count: 17 */
    public final List<AbstractSafeParcelable> zzp(int i) {
        ? r9;
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        SQLiteDatabase sQLiteDatabase2;
        ? r92;
        ? r93;
        Throwable th2;
        ? r94;
        Object obj;
        SQLiteDatabase sQLiteDatabase3;
        ? r3;
        ? r95;
        Object obj2;
        Parcel obtain;
        Object obj3;
        Parcel obtain2;
        Parcel obtain3;
        ? r96;
        ? r97;
        zzab();
        ? r32 = 0;
        if (this.zzaih) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!getContext().getDatabasePath("google_app_measurement_local.db").exists()) {
            return arrayList;
        }
        int i2 = 5;
        int i3 = 5;
        int i4 = 0;
        ? r33 = r32;
        while (i4 < i2) {
            try {
                sQLiteDatabase2 = getWritableDatabase();
                if (sQLiteDatabase2 == null) {
                    try {
                        this.zzaih = true;
                        if (sQLiteDatabase2 != null) {
                            sQLiteDatabase2.close();
                        }
                        return r33;
                    } catch (SQLiteFullException e) {
                        e = e;
                        r96 = r33;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        e = e2;
                        r97 = r33;
                        obj2 = e;
                        r95 = r97;
                        if (sQLiteDatabase2 != null) {
                        }
                        zzge().zzim().zzg("Error reading entries from local database", obj2);
                        this.zzaih = true;
                        if (r95 != 0) {
                        }
                        if (sQLiteDatabase2 == null) {
                        }
                        sQLiteDatabase2.close();
                        i4++;
                        i2 = 5;
                        r33 = 0;
                    } catch (Throwable th3) {
                        th2 = th3;
                        r92 = r33;
                        sQLiteDatabase = sQLiteDatabase2;
                        r9 = r92;
                        if (r9 != 0) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    sQLiteDatabase2.beginTransaction();
                    sQLiteDatabase = sQLiteDatabase2;
                    try {
                        ? query = sQLiteDatabase2.query("messages", new String[]{"rowid", "type", "entry"}, null, null, null, null, "rowid asc", Integer.toString(100));
                        long j = -1;
                        while (query.moveToNext()) {
                            try {
                                j = query.getLong(0);
                                int i5 = query.getInt(1);
                                byte[] blob = query.getBlob(2);
                                if (i5 == 0) {
                                    obtain3 = Parcel.obtain();
                                    obtain3.unmarshall(blob, 0, blob.length);
                                    obtain3.setDataPosition(0);
                                    obj3 = (zzeu) zzeu.CREATOR.createFromParcel(obtain3);
                                    obtain3.recycle();
                                    if (obj3 == null) {
                                    }
                                } else if (i5 == 1) {
                                    obtain2 = Parcel.obtain();
                                    obtain2.unmarshall(blob, 0, blob.length);
                                    obtain2.setDataPosition(0);
                                    obj3 = (zzjx) zzjx.CREATOR.createFromParcel(obtain2);
                                    obtain2.recycle();
                                    if (obj3 != null) {
                                    }
                                } else if (i5 == 2) {
                                    obtain = Parcel.obtain();
                                    obtain.unmarshall(blob, 0, blob.length);
                                    obtain.setDataPosition(0);
                                    Object obj4 = (zzed) zzed.CREATOR.createFromParcel(obtain);
                                    obtain.recycle();
                                    if (obj3 != null) {
                                    }
                                } else {
                                    zzge().zzim().log("Unknown record type in local database");
                                }
                                arrayList.add(obj3);
                            } catch (SQLiteFullException e3) {
                                e = e3;
                                sQLiteDatabase2 = sQLiteDatabase;
                                r96 = query;
                            } catch (SQLiteDatabaseLockedException unused2) {
                                sQLiteDatabase3 = sQLiteDatabase;
                                r3 = query;
                                try {
                                    SystemClock.sleep((long) i3);
                                    i3 += 20;
                                    if (r3 != 0) {
                                        r3.close();
                                    }
                                    if (sQLiteDatabase3 == null) {
                                        sQLiteDatabase3.close();
                                    }
                                    i4++;
                                    i2 = 5;
                                    r33 = 0;
                                } catch (Throwable th4) {
                                    th = th4;
                                    r9 = r3;
                                    sQLiteDatabase = sQLiteDatabase3;
                                    if (r9 != 0) {
                                    }
                                    if (sQLiteDatabase != null) {
                                    }
                                    throw th;
                                }
                            } catch (SQLiteException e4) {
                                e = e4;
                                sQLiteDatabase2 = sQLiteDatabase;
                                r97 = query;
                                obj2 = e;
                                r95 = r97;
                                if (sQLiteDatabase2 != null) {
                                    try {
                                        r93 = r95;
                                        if (sQLiteDatabase2.inTransaction()) {
                                            sQLiteDatabase2.endTransaction();
                                        }
                                    } catch (Throwable th5) {
                                        th2 = th5;
                                        r92 = r93;
                                        sQLiteDatabase = sQLiteDatabase2;
                                        r9 = r92;
                                        if (r9 != 0) {
                                        }
                                        if (sQLiteDatabase != null) {
                                        }
                                        throw th;
                                    }
                                }
                                zzge().zzim().zzg("Error reading entries from local database", obj2);
                                this.zzaih = true;
                                if (r95 != 0) {
                                    r95.close();
                                }
                                if (sQLiteDatabase2 == null) {
                                    i4++;
                                    i2 = 5;
                                    r33 = 0;
                                }
                                sQLiteDatabase2.close();
                                i4++;
                                i2 = 5;
                                r33 = 0;
                            } catch (Throwable th6) {
                                th = th6;
                                r9 = query;
                                if (r9 != 0) {
                                    r9.close();
                                }
                                if (sQLiteDatabase != null) {
                                    sQLiteDatabase.close();
                                }
                                throw th;
                            }
                        }
                        if (sQLiteDatabase.delete("messages", "rowid <= ?", new String[]{Long.toString(j)}) < arrayList.size()) {
                            zzge().zzim().log("Fewer entries removed from local database than expected");
                        }
                        sQLiteDatabase.setTransactionSuccessful();
                        sQLiteDatabase.endTransaction();
                        if (query != 0) {
                            query.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return arrayList;
                    } catch (SQLiteFullException e5) {
                        e = e5;
                        sQLiteDatabase2 = sQLiteDatabase;
                        r96 = 0;
                        obj = e;
                        r94 = r96;
                        r93 = r94;
                        zzge().zzim().zzg("Error reading entries from local database", obj);
                        this.zzaih = true;
                        r93 = r94;
                        if (r94 != 0) {
                        }
                        if (sQLiteDatabase2 == null) {
                        }
                        sQLiteDatabase2.close();
                        i4++;
                        i2 = 5;
                        r33 = 0;
                    } catch (SQLiteDatabaseLockedException unused3) {
                        sQLiteDatabase3 = sQLiteDatabase;
                        r3 = 0;
                        SystemClock.sleep((long) i3);
                        i3 += 20;
                        if (r3 != 0) {
                        }
                        if (sQLiteDatabase3 == null) {
                        }
                        i4++;
                        i2 = 5;
                        r33 = 0;
                    } catch (SQLiteException e6) {
                        e = e6;
                        sQLiteDatabase2 = sQLiteDatabase;
                        r97 = 0;
                        obj2 = e;
                        r95 = r97;
                        if (sQLiteDatabase2 != null) {
                        }
                        zzge().zzim().zzg("Error reading entries from local database", obj2);
                        this.zzaih = true;
                        if (r95 != 0) {
                        }
                        if (sQLiteDatabase2 == null) {
                        }
                        sQLiteDatabase2.close();
                        i4++;
                        i2 = 5;
                        r33 = 0;
                    } catch (Throwable th7) {
                        th = th7;
                        th = th;
                        r9 = 0;
                        if (r9 != 0) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteFullException e7) {
                obj = e7;
                sQLiteDatabase2 = null;
                r94 = 0;
                r93 = r94;
                zzge().zzim().zzg("Error reading entries from local database", obj);
                this.zzaih = true;
                r93 = r94;
                if (r94 != 0) {
                }
                if (sQLiteDatabase2 == null) {
                }
                sQLiteDatabase2.close();
                i4++;
                i2 = 5;
                r33 = 0;
            } catch (SQLiteDatabaseLockedException unused4) {
                sQLiteDatabase3 = null;
                r3 = 0;
                SystemClock.sleep((long) i3);
                i3 += 20;
                if (r3 != 0) {
                }
                if (sQLiteDatabase3 == null) {
                }
                i4++;
                i2 = 5;
                r33 = 0;
            } catch (SQLiteException e8) {
                obj2 = e8;
                sQLiteDatabase2 = null;
                r95 = 0;
                if (sQLiteDatabase2 != null) {
                }
                zzge().zzim().zzg("Error reading entries from local database", obj2);
                this.zzaih = true;
                if (r95 != 0) {
                }
                if (sQLiteDatabase2 == null) {
                }
                sQLiteDatabase2.close();
                i4++;
                i2 = 5;
                r33 = 0;
            } catch (Throwable th8) {
                th = th8;
                sQLiteDatabase = null;
                r9 = 0;
                if (r9 != 0) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        zzge().zzip().log("Failed to read events from database in reasonable time");
        return null;
    }
}
