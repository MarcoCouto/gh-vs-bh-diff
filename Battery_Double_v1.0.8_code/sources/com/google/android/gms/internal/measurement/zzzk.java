package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzzq.zza;

final class zzzk extends zzzj<Object> {
    zzzk() {
    }

    /* access modifiers changed from: 0000 */
    public final boolean zza(zzaal zzaal) {
        return zzaal instanceof zza;
    }

    /* access modifiers changed from: 0000 */
    public final zzzm<Object> zzs(Object obj) {
        return ((zza) obj).zzbsb;
    }
}
