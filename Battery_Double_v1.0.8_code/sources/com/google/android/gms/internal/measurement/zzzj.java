package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzzo;

abstract class zzzj<T extends zzzo<T>> {
    zzzj() {
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean zza(zzaal zzaal);

    /* access modifiers changed from: 0000 */
    public abstract zzzm<T> zzs(Object obj);
}
