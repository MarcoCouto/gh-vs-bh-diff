package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.wrappers.InstantApps;
import com.google.firebase.iid.FirebaseInstanceId;
import java.math.BigInteger;
import java.util.Locale;

public final class zzfb extends zzhh {
    private String zzadm;
    private String zzadt;
    private long zzadx;
    private int zzaen;
    private int zzaie;
    private long zzaif;
    private String zztg;
    private String zzth;
    private String zzti;

    zzfb(zzgl zzgl) {
        super(zzgl);
    }

    @WorkerThread
    private final String zzgj() {
        zzab();
        if (zzgg().zzay(this.zzti) && !this.zzacw.isEnabled()) {
            return null;
        }
        try {
            return FirebaseInstanceId.getInstance().getId();
        } catch (IllegalStateException unused) {
            zzge().zzip().log("Failed to retrieve Firebase Instance Id");
            return null;
        }
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    /* access modifiers changed from: 0000 */
    public final String getGmpAppId() {
        zzch();
        return this.zzadm;
    }

    public final /* bridge */ /* synthetic */ void zzab() {
        super.zzab();
    }

    /* access modifiers changed from: 0000 */
    public final String zzah() {
        zzch();
        return this.zzti;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final zzdz zzbi(String str) {
        zzab();
        String zzah = zzah();
        String gmpAppId = getGmpAppId();
        zzch();
        String str2 = this.zzth;
        long zzij = (long) zzij();
        zzch();
        String str3 = this.zzadt;
        zzch();
        zzab();
        if (this.zzaif == 0) {
            this.zzaif = this.zzacw.zzgb().zzd(getContext(), getContext().getPackageName());
        }
        long j = this.zzaif;
        boolean isEnabled = this.zzacw.isEnabled();
        boolean z = true;
        boolean z2 = !zzgf().zzakn;
        String zzgj = zzgj();
        zzch();
        long zzjt = this.zzacw.zzjt();
        int zzik = zzik();
        Boolean zzas = zzgg().zzas("google_analytics_adid_collection_enabled");
        boolean booleanValue = Boolean.valueOf(zzas == null || zzas.booleanValue()).booleanValue();
        Boolean zzas2 = zzgg().zzas("google_analytics_ssaid_collection_enabled");
        if (zzas2 != null && !zzas2.booleanValue()) {
            z = false;
        }
        zzdz zzdz = new zzdz(zzah, gmpAppId, str2, zzij, str3, 12451, j, str, isEnabled, z2, zzgj, 0, zzjt, zzik, booleanValue, Boolean.valueOf(z).booleanValue(), zzgf().zzje());
        return zzdz;
    }

    public final /* bridge */ /* synthetic */ Clock zzbt() {
        return super.zzbt();
    }

    public final /* bridge */ /* synthetic */ void zzfr() {
        super.zzfr();
    }

    public final /* bridge */ /* synthetic */ void zzfs() {
        super.zzfs();
    }

    public final /* bridge */ /* synthetic */ zzdu zzft() {
        return super.zzft();
    }

    public final /* bridge */ /* synthetic */ zzhk zzfu() {
        return super.zzfu();
    }

    public final /* bridge */ /* synthetic */ zzfb zzfv() {
        return super.zzfv();
    }

    public final /* bridge */ /* synthetic */ zzeo zzfw() {
        return super.zzfw();
    }

    public final /* bridge */ /* synthetic */ zzii zzfx() {
        return super.zzfx();
    }

    public final /* bridge */ /* synthetic */ zzif zzfy() {
        return super.zzfy();
    }

    public final /* bridge */ /* synthetic */ zzfc zzfz() {
        return super.zzfz();
    }

    public final /* bridge */ /* synthetic */ zzfe zzga() {
        return super.zzga();
    }

    public final /* bridge */ /* synthetic */ zzka zzgb() {
        return super.zzgb();
    }

    public final /* bridge */ /* synthetic */ zzjh zzgc() {
        return super.zzgc();
    }

    public final /* bridge */ /* synthetic */ zzgg zzgd() {
        return super.zzgd();
    }

    public final /* bridge */ /* synthetic */ zzfg zzge() {
        return super.zzge();
    }

    public final /* bridge */ /* synthetic */ zzfr zzgf() {
        return super.zzgf();
    }

    public final /* bridge */ /* synthetic */ zzef zzgg() {
        return super.zzgg();
    }

    /* access modifiers changed from: protected */
    public final boolean zzhf() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0141 A[Catch:{ IllegalStateException -> 0x0159 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0147 A[Catch:{ IllegalStateException -> 0x0159 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x017c  */
    public final void zzih() {
        boolean z;
        boolean z2;
        String googleAppId;
        zzfi zzir;
        String str;
        String str2 = "unknown";
        String str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str4 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String packageName = getContext().getPackageName();
        PackageManager packageManager = getContext().getPackageManager();
        int i = Integer.MIN_VALUE;
        if (packageManager == null) {
            zzge().zzim().zzg("PackageManager is null, app identity information might be inaccurate. appId", zzfg.zzbm(packageName));
        } else {
            try {
                str2 = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException unused) {
                zzge().zzim().zzg("Error retrieving app installer package name. appId", zzfg.zzbm(packageName));
            }
            if (str2 == null) {
                str2 = "manual_install";
            } else if ("com.android.vending".equals(str2)) {
                str2 = "";
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    if (!TextUtils.isEmpty(applicationLabel)) {
                        str4 = applicationLabel.toString();
                    }
                    String str5 = packageInfo.versionName;
                    try {
                        i = packageInfo.versionCode;
                        str3 = str5;
                    } catch (NameNotFoundException unused2) {
                        str3 = str5;
                        zzge().zzim().zze("Error retrieving package info. appId, appName", zzfg.zzbm(packageName), str4);
                        this.zzti = packageName;
                        this.zzadt = str2;
                        this.zzth = str3;
                        this.zzaie = i;
                        this.zztg = str4;
                        this.zzaif = 0;
                        Status initialize = GoogleServices.initialize(getContext());
                        z = true;
                        if (initialize == null) {
                        }
                        if (!z2) {
                        }
                        if (z2) {
                        }
                        z = false;
                        this.zzadm = "";
                        this.zzadx = 0;
                        googleAppId = GoogleServices.getGoogleAppId();
                        if (TextUtils.isEmpty(googleAppId)) {
                        }
                        this.zzadm = googleAppId;
                        if (z) {
                        }
                        if (VERSION.SDK_INT < 16) {
                        }
                    }
                }
            } catch (NameNotFoundException unused3) {
                zzge().zzim().zze("Error retrieving package info. appId, appName", zzfg.zzbm(packageName), str4);
                this.zzti = packageName;
                this.zzadt = str2;
                this.zzth = str3;
                this.zzaie = i;
                this.zztg = str4;
                this.zzaif = 0;
                Status initialize2 = GoogleServices.initialize(getContext());
                z = true;
                if (initialize2 == null) {
                }
                if (!z2) {
                }
                if (z2) {
                }
                z = false;
                this.zzadm = "";
                this.zzadx = 0;
                googleAppId = GoogleServices.getGoogleAppId();
                if (TextUtils.isEmpty(googleAppId)) {
                }
                this.zzadm = googleAppId;
                if (z) {
                }
                if (VERSION.SDK_INT < 16) {
                }
            }
        }
        this.zzti = packageName;
        this.zzadt = str2;
        this.zzth = str3;
        this.zzaie = i;
        this.zztg = str4;
        this.zzaif = 0;
        Status initialize22 = GoogleServices.initialize(getContext());
        z = true;
        z2 = initialize22 == null && initialize22.isSuccess();
        if (!z2) {
            if (initialize22 == null) {
                zzge().zzim().log("GoogleService failed to initialize (no status)");
            } else {
                zzge().zzim().zze("GoogleService failed to initialize, status", Integer.valueOf(initialize22.getStatusCode()), initialize22.getStatusMessage());
            }
        }
        if (z2) {
            Boolean zzas = zzgg().zzas("firebase_analytics_collection_enabled");
            if (zzgg().zzhg()) {
                zzir = zzge().zzir();
                str = "Collection disabled with firebase_analytics_collection_deactivated=1";
            } else if (zzas != null && !zzas.booleanValue()) {
                zzir = zzge().zzir();
                str = "Collection disabled with firebase_analytics_collection_enabled=0";
            } else if (zzas != null || !GoogleServices.isMeasurementExplicitlyDisabled()) {
                zzge().zzit().log("Collection enabled");
                this.zzadm = "";
                this.zzadx = 0;
                googleAppId = GoogleServices.getGoogleAppId();
                if (TextUtils.isEmpty(googleAppId)) {
                    googleAppId = "";
                }
                this.zzadm = googleAppId;
                if (z) {
                    zzge().zzit().zze("App package, google app id", this.zzti, this.zzadm);
                }
                if (VERSION.SDK_INT < 16) {
                    this.zzaen = InstantApps.isInstantApp(getContext()) ? 1 : 0;
                    return;
                } else {
                    this.zzaen = 0;
                    return;
                }
            } else {
                zzir = zzge().zzir();
                str = "Collection disabled with google_app_measurement_enable=0";
            }
            zzir.log(str);
        }
        z = false;
        this.zzadm = "";
        this.zzadx = 0;
        try {
            googleAppId = GoogleServices.getGoogleAppId();
            if (TextUtils.isEmpty(googleAppId)) {
            }
            this.zzadm = googleAppId;
            if (z) {
            }
        } catch (IllegalStateException e) {
            zzge().zzim().zze("getGoogleAppId or isMeasurementEnabled failed with exception. appId", zzfg.zzbm(packageName), e);
        }
        if (VERSION.SDK_INT < 16) {
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final String zzii() {
        byte[] bArr = new byte[16];
        zzgb().zzlc().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new Object[]{new BigInteger(1, bArr)});
    }

    /* access modifiers changed from: 0000 */
    public final int zzij() {
        zzch();
        return this.zzaie;
    }

    /* access modifiers changed from: 0000 */
    public final int zzik() {
        zzch();
        return this.zzaen;
    }
}
