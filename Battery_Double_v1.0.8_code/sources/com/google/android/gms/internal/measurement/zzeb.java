package com.google.android.gms.internal.measurement;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzeb extends zzjq {
    zzeb(zzjr zzjr) {
        super(zzjr);
    }

    private final Boolean zza(double d, zzkg zzkg) {
        try {
            return zza(new BigDecimal(d), zzkg, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(long j, zzkg zzkg) {
        try {
            return zza(new BigDecimal(j), zzkg, (double) Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @VisibleForTesting
    private static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    private final Boolean zza(String str, int i, boolean z, String str2, List<String> list, String str3) {
        boolean startsWith;
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    zzge().zzip().zzg("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                startsWith = str.startsWith(str2);
                break;
            case 3:
                startsWith = str.endsWith(str2);
                break;
            case 4:
                startsWith = str.contains(str2);
                break;
            case 5:
                startsWith = str.equals(str2);
                break;
            case 6:
                startsWith = list.contains(str);
                break;
            default:
                return null;
        }
        return Boolean.valueOf(startsWith);
    }

    private final Boolean zza(String str, zzkg zzkg) {
        if (!zzka.zzck(str)) {
            return null;
        }
        try {
            return zza(new BigDecimal(str), zzkg, (double) Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @VisibleForTesting
    private final Boolean zza(String str, zzki zzki) {
        List list;
        Preconditions.checkNotNull(zzki);
        if (str == null || zzki.zzash == null || zzki.zzash.intValue() == 0) {
            return null;
        }
        if (zzki.zzash.intValue() == 6) {
            if (zzki.zzask == null || zzki.zzask.length == 0) {
                return null;
            }
        } else if (zzki.zzasi == null) {
            return null;
        }
        int intValue = zzki.zzash.intValue();
        boolean z = zzki.zzasj != null && zzki.zzasj.booleanValue();
        String upperCase = (z || intValue == 1 || intValue == 6) ? zzki.zzasi : zzki.zzasi.toUpperCase(Locale.ENGLISH);
        if (zzki.zzask == null) {
            list = null;
        } else {
            String[] strArr = zzki.zzask;
            if (z) {
                list = Arrays.asList(strArr);
            } else {
                ArrayList arrayList = new ArrayList();
                for (String upperCase2 : strArr) {
                    arrayList.add(upperCase2.toUpperCase(Locale.ENGLISH));
                }
                list = arrayList;
            }
        }
        return zza(str, intValue, z, upperCase, list, intValue == 1 ? upperCase : null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0070, code lost:
        if (r3 != null) goto L_0x0072;
     */
    @VisibleForTesting
    private static Boolean zza(BigDecimal bigDecimal, zzkg zzkg, double d) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        Preconditions.checkNotNull(zzkg);
        if (zzkg.zzarz == null || zzkg.zzarz.intValue() == 0) {
            return null;
        }
        if (zzkg.zzarz.intValue() == 4) {
            if (zzkg.zzasc == null || zzkg.zzasd == null) {
                return null;
            }
        } else if (zzkg.zzasb == null) {
            return null;
        }
        int intValue = zzkg.zzarz.intValue();
        if (zzkg.zzarz.intValue() == 4) {
            if (!zzka.zzck(zzkg.zzasc) || !zzka.zzck(zzkg.zzasd)) {
                return null;
            }
            try {
                BigDecimal bigDecimal5 = new BigDecimal(zzkg.zzasc);
                bigDecimal3 = new BigDecimal(zzkg.zzasd);
                bigDecimal2 = bigDecimal5;
                bigDecimal4 = null;
            } catch (NumberFormatException unused) {
                return null;
            }
        } else if (!zzka.zzck(zzkg.zzasb)) {
            return null;
        } else {
            try {
                bigDecimal4 = new BigDecimal(zzkg.zzasb);
                bigDecimal2 = null;
                bigDecimal3 = null;
            } catch (NumberFormatException unused2) {
                return null;
            }
        }
        if (intValue == 4) {
            if (bigDecimal2 == null) {
                return null;
            }
        }
        boolean z = false;
        switch (intValue) {
            case 1:
                if (bigDecimal.compareTo(bigDecimal4) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 2:
                if (bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 3:
                if (d != Utils.DOUBLE_EPSILON) {
                    if (bigDecimal.compareTo(bigDecimal4.subtract(new BigDecimal(d).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal4.add(new BigDecimal(d).multiply(new BigDecimal(2)))) == -1) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                }
                if (bigDecimal.compareTo(bigDecimal4) == 0) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 4:
                if (!(bigDecimal.compareTo(bigDecimal2) == -1 || bigDecimal.compareTo(bigDecimal3) == 1)) {
                    z = true;
                }
                return Boolean.valueOf(z);
            default:
                return null;
        }
    }

    /* JADX WARNING: type inference failed for: r13v6 */
    /* JADX WARNING: type inference failed for: r13v7, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r3v44 */
    /* JADX WARNING: type inference failed for: r7v29 */
    /* JADX WARNING: type inference failed for: r7v30, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r3v91 */
    /* JADX WARNING: type inference failed for: r13v36 */
    /* JADX WARNING: type inference failed for: r7v71 */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0697, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x06a6, code lost:
        r3 = java.lang.Boolean.valueOf(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x09ba, code lost:
        zzge().zzip().zze("Invalid property filter ID. appId, id", com.google.android.gms.internal.measurement.zzfg.zzbm(r61), java.lang.String.valueOf(r5.zzarp));
        r11.add(java.lang.Integer.valueOf(r10));
        r3 = r51;
        r5 = r52;
        r8 = r53;
        r9 = r54;
        r44 = r56;
        r46 = r57;
        r45 = r58;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x030b  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0357  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0380  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0395  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x03a5  */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x06b6  */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x06b9  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x06bf  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x06d8  */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x098c  */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x098f  */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x0995  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x099e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02a1  */
    /* JADX WARNING: Unknown variable types count: 4 */
    @WorkerThread
    public final zzkm[] zza(String str, zzkn[] zzknArr, zzks[] zzksArr) {
        String str2;
        zzfi zzfi;
        Object obj;
        ArrayMap arrayMap;
        ArrayMap arrayMap2;
        ArrayMap arrayMap3;
        Iterator it;
        Boolean bool;
        zzfi zzip;
        String str3;
        Boolean zza;
        ArrayMap arrayMap4;
        ArrayMap arrayMap5;
        ArrayMap arrayMap6;
        ArrayMap arrayMap7;
        int i;
        int i2;
        String str4;
        ArrayMap arrayMap8;
        long j;
        long j2;
        Long l;
        String str5;
        zzko[] zzkoArr;
        zzeq zzf;
        String str6;
        zzko[] zzkoArr2;
        ArrayMap arrayMap9;
        HashSet hashSet;
        ArrayMap arrayMap10;
        ArrayMap arrayMap11;
        zzeq zzeq;
        Map map;
        String str7;
        Iterator it2;
        HashSet hashSet2;
        ArrayMap arrayMap12;
        ArrayMap arrayMap13;
        ArrayMap arrayMap14;
        Map map2;
        String str8;
        long j3;
        zzko[] zzkoArr3;
        Boolean bool2;
        zzkf[] zzkfArr;
        String str9;
        zzfi zzip2;
        String str10;
        Boolean bool3;
        String zzbj;
        String zzbk;
        String str11;
        Object obj2;
        int i3;
        int i4;
        int i5;
        boolean z;
        Long l2;
        ArrayMap arrayMap15;
        Long l3;
        zzkn zzkn;
        int i6;
        int length;
        int i7;
        int i8;
        ArrayMap arrayMap16;
        zzko[] zzkoArr4;
        boolean z2;
        ArrayMap arrayMap17;
        ArrayMap arrayMap18;
        Iterator it3;
        String str12 = str;
        zzkn[] zzknArr2 = zzknArr;
        zzks[] zzksArr2 = zzksArr;
        Preconditions.checkNotEmpty(str);
        HashSet hashSet3 = new HashSet();
        ArrayMap arrayMap19 = new ArrayMap();
        ArrayMap arrayMap20 = new ArrayMap();
        ArrayMap arrayMap21 = new ArrayMap();
        Map zzbf = zzix().zzbf(str12);
        if (zzbf != null) {
            Iterator it4 = zzbf.keySet().iterator();
            while (it4.hasNext()) {
                int intValue = ((Integer) it4.next()).intValue();
                zzkr zzkr = (zzkr) zzbf.get(Integer.valueOf(intValue));
                BitSet bitSet = (BitSet) arrayMap20.get(Integer.valueOf(intValue));
                BitSet bitSet2 = (BitSet) arrayMap21.get(Integer.valueOf(intValue));
                if (bitSet == null) {
                    bitSet = new BitSet();
                    arrayMap20.put(Integer.valueOf(intValue), bitSet);
                    bitSet2 = new BitSet();
                    arrayMap21.put(Integer.valueOf(intValue), bitSet2);
                }
                Map map3 = zzbf;
                int i9 = 0;
                while (i9 < (zzkr.zzauk.length << 6)) {
                    if (zzka.zza(zzkr.zzauk, i9)) {
                        it3 = it4;
                        arrayMap18 = arrayMap20;
                        arrayMap17 = arrayMap21;
                        zzge().zzit().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue), Integer.valueOf(i9));
                        bitSet2.set(i9);
                        if (zzka.zza(zzkr.zzaul, i9)) {
                            bitSet.set(i9);
                        }
                    } else {
                        it3 = it4;
                        arrayMap18 = arrayMap20;
                        arrayMap17 = arrayMap21;
                    }
                    i9++;
                    it4 = it3;
                    arrayMap20 = arrayMap18;
                    arrayMap21 = arrayMap17;
                }
                Iterator it5 = it4;
                ArrayMap arrayMap22 = arrayMap20;
                ArrayMap arrayMap23 = arrayMap21;
                zzkm zzkm = new zzkm();
                arrayMap19.put(Integer.valueOf(intValue), zzkm);
                zzkm.zzasy = Boolean.valueOf(false);
                zzkm.zzasx = zzkr;
                zzkm.zzasw = new zzkr();
                zzkm.zzasw.zzaul = zzka.zza(bitSet);
                zzkm.zzasw.zzauk = zzka.zza(bitSet2);
                zzbf = map3;
                it4 = it5;
            }
        }
        ArrayMap arrayMap24 = arrayMap20;
        ArrayMap arrayMap25 = arrayMap21;
        if (zzknArr2 != null) {
            ArrayMap arrayMap26 = new ArrayMap();
            int length2 = zzknArr2.length;
            int i10 = 0;
            Long l4 = null;
            zzkn zzkn2 = null;
            long j4 = 0;
            while (i10 < length2) {
                zzkn zzkn3 = zzknArr2[i10];
                String str13 = zzkn3.name;
                zzko[] zzkoArr5 = zzkn3.zzata;
                if (zzgg().zzd(str12, zzew.zzahv)) {
                    zzgb();
                    Long l5 = (Long) zzka.zzb(zzkn3, "_eid");
                    boolean z3 = l5 != null;
                    if (z3) {
                        i5 = i10;
                        if (str13.equals("_ep")) {
                            z = true;
                            if (!z) {
                                zzgb();
                                String str14 = (String) zzka.zzb(zzkn3, "_en");
                                if (TextUtils.isEmpty(str14)) {
                                    zzge().zzim().zzg("Extra parameter without an event name. eventId", l5);
                                    arrayMap15 = arrayMap26;
                                    i = length2;
                                    i2 = i5;
                                } else {
                                    if (zzkn2 == null || l4 == null || l5.longValue() != l4.longValue()) {
                                        Pair zza2 = zzix().zza(str12, l5);
                                        if (zza2 == null || zza2.first == null) {
                                            arrayMap15 = arrayMap26;
                                            i = length2;
                                            i2 = i5;
                                            zzge().zzim().zze("Extra parameter without existing main event. eventName, eventId", str14, l5);
                                        } else {
                                            zzkn zzkn4 = (zzkn) zza2.first;
                                            j4 = ((Long) zza2.second).longValue();
                                            zzgb();
                                            l3 = (Long) zzka.zzb(zzkn4, "_eid");
                                            zzkn = zzkn4;
                                        }
                                    } else {
                                        zzkn zzkn5 = zzkn2;
                                        l3 = l4;
                                        zzkn = zzkn5;
                                    }
                                    j = j4 - 1;
                                    if (j <= 0) {
                                        zzei zzix = zzix();
                                        zzix.zzab();
                                        zzix.zzge().zzit().zzg("Clearing complex main event info. appId", str12);
                                        try {
                                            SQLiteDatabase writableDatabase = zzix.getWritableDatabase();
                                            String str15 = "delete from main_event_params where app_id=?";
                                            arrayMap16 = arrayMap26;
                                            zzkoArr4 = zzkoArr5;
                                            z2 = true;
                                            try {
                                                String[] strArr = new String[1];
                                                i6 = 0;
                                                try {
                                                    strArr[0] = str12;
                                                    writableDatabase.execSQL(str15, strArr);
                                                } catch (SQLiteException e) {
                                                    e = e;
                                                }
                                            } catch (SQLiteException e2) {
                                                e = e2;
                                                i6 = 0;
                                                zzix.zzge().zzim().zzg("Error clearing complex main event", e);
                                                i3 = length2;
                                                boolean z4 = z2;
                                                i4 = i5;
                                                zzkoArr = zzkoArr4;
                                                arrayMap8 = arrayMap16;
                                                zzko[] zzkoArr6 = new zzko[(zzkn.zzata.length + zzkoArr.length)];
                                                zzko[] zzkoArr7 = zzkn.zzata;
                                                length = zzkoArr7.length;
                                                i7 = i6;
                                                i8 = i7;
                                                while (i7 < length) {
                                                }
                                                if (i8 > 0) {
                                                }
                                                str5 = str14;
                                                l = l3;
                                                j2 = 0;
                                                zzkn2 = zzkn;
                                                zzf = zzix().zzf(str12, zzkn3.name);
                                                if (zzf == null) {
                                                }
                                                zzix().zza(zzeq);
                                                long j5 = zzeq.zzafr;
                                                ArrayMap arrayMap27 = arrayMap8;
                                                String str16 = str6;
                                                map = (Map) arrayMap27.get(str16);
                                                if (map == null) {
                                                }
                                                it2 = map.keySet().iterator();
                                                while (it2.hasNext()) {
                                                }
                                                arrayMap7 = arrayMap27;
                                                str4 = str7;
                                                arrayMap5 = arrayMap11;
                                                arrayMap6 = arrayMap10;
                                                hashSet3 = hashSet;
                                                arrayMap4 = arrayMap9;
                                                l4 = l;
                                                j4 = j;
                                                i10 = i2 + 1;
                                                zzknArr2 = zzknArr;
                                                str12 = str4;
                                                length2 = i;
                                                arrayMap26 = arrayMap7;
                                                arrayMap25 = arrayMap6;
                                                arrayMap24 = arrayMap5;
                                                arrayMap19 = arrayMap4;
                                                zzksArr2 = zzksArr;
                                            }
                                        } catch (SQLiteException e3) {
                                            e = e3;
                                            arrayMap16 = arrayMap26;
                                            zzkoArr4 = zzkoArr5;
                                            z2 = true;
                                            i6 = 0;
                                            zzix.zzge().zzim().zzg("Error clearing complex main event", e);
                                            i3 = length2;
                                            boolean z42 = z2;
                                            i4 = i5;
                                            zzkoArr = zzkoArr4;
                                            arrayMap8 = arrayMap16;
                                            zzko[] zzkoArr62 = new zzko[(zzkn.zzata.length + zzkoArr.length)];
                                            zzko[] zzkoArr72 = zzkn.zzata;
                                            length = zzkoArr72.length;
                                            i7 = i6;
                                            i8 = i7;
                                            while (i7 < length) {
                                            }
                                            if (i8 > 0) {
                                            }
                                            str5 = str14;
                                            l = l3;
                                            j2 = 0;
                                            zzkn2 = zzkn;
                                            zzf = zzix().zzf(str12, zzkn3.name);
                                            if (zzf == null) {
                                            }
                                            zzix().zza(zzeq);
                                            long j52 = zzeq.zzafr;
                                            ArrayMap arrayMap272 = arrayMap8;
                                            String str162 = str6;
                                            map = (Map) arrayMap272.get(str162);
                                            if (map == null) {
                                            }
                                            it2 = map.keySet().iterator();
                                            while (it2.hasNext()) {
                                            }
                                            arrayMap7 = arrayMap272;
                                            str4 = str7;
                                            arrayMap5 = arrayMap11;
                                            arrayMap6 = arrayMap10;
                                            hashSet3 = hashSet;
                                            arrayMap4 = arrayMap9;
                                            l4 = l;
                                            j4 = j;
                                            i10 = i2 + 1;
                                            zzknArr2 = zzknArr;
                                            str12 = str4;
                                            length2 = i;
                                            arrayMap26 = arrayMap7;
                                            arrayMap25 = arrayMap6;
                                            arrayMap24 = arrayMap5;
                                            arrayMap19 = arrayMap4;
                                            zzksArr2 = zzksArr;
                                        }
                                        i3 = length2;
                                        boolean z422 = z2;
                                        i4 = i5;
                                        zzkoArr = zzkoArr4;
                                        arrayMap8 = arrayMap16;
                                    } else {
                                        i6 = 0;
                                        i4 = i5;
                                        i3 = length2;
                                        arrayMap8 = arrayMap26;
                                        zzkoArr = zzkoArr5;
                                        zzix().zza(str12, l5, j, zzkn);
                                    }
                                    zzko[] zzkoArr622 = new zzko[(zzkn.zzata.length + zzkoArr.length)];
                                    zzko[] zzkoArr722 = zzkn.zzata;
                                    length = zzkoArr722.length;
                                    i7 = i6;
                                    i8 = i7;
                                    while (i7 < length) {
                                        zzko zzko = zzkoArr722[i7];
                                        zzgb();
                                        if (zzka.zza(zzkn3, zzko.name) == null) {
                                            int i11 = i8 + 1;
                                            zzkoArr622[i8] = zzko;
                                            i8 = i11;
                                        }
                                        i7++;
                                    }
                                    if (i8 > 0) {
                                        int length3 = zzkoArr.length;
                                        int i12 = i6;
                                        while (i12 < length3) {
                                            int i13 = i8 + 1;
                                            zzkoArr622[i8] = zzkoArr[i12];
                                            i12++;
                                            i8 = i13;
                                        }
                                        if (i8 != zzkoArr622.length) {
                                            zzkoArr622 = (zzko[]) Arrays.copyOf(zzkoArr622, i8);
                                        }
                                        zzkoArr = zzkoArr622;
                                    } else {
                                        zzge().zzip().zzg("No unique parameters in main event. eventName", str14);
                                    }
                                    str5 = str14;
                                    l = l3;
                                    j2 = 0;
                                    zzkn2 = zzkn;
                                }
                                arrayMap4 = arrayMap19;
                                str4 = str12;
                                arrayMap5 = arrayMap24;
                                arrayMap6 = arrayMap25;
                                arrayMap7 = arrayMap15;
                                i10 = i2 + 1;
                                zzknArr2 = zzknArr;
                                str12 = str4;
                                length2 = i;
                                arrayMap26 = arrayMap7;
                                arrayMap25 = arrayMap6;
                                arrayMap24 = arrayMap5;
                                arrayMap19 = arrayMap4;
                                zzksArr2 = zzksArr;
                            } else {
                                arrayMap8 = arrayMap26;
                                i3 = length2;
                                zzkoArr = zzkoArr5;
                                i4 = i5;
                                if (z3) {
                                    zzgb();
                                    Object valueOf = Long.valueOf(0);
                                    Object zzb = zzka.zzb(zzkn3, "_epc");
                                    if (zzb == null) {
                                        zzb = valueOf;
                                    }
                                    long longValue = ((Long) zzb).longValue();
                                    if (longValue <= 0) {
                                        zzge().zzip().zzg("Complex event with zero extra param count. eventName", str13);
                                        l2 = l5;
                                        j2 = 0;
                                    } else {
                                        l2 = l5;
                                        j2 = 0;
                                        zzix().zza(str12, l5, longValue, zzkn3);
                                    }
                                    zzkn2 = zzkn3;
                                    j = longValue;
                                    l = l2;
                                    str5 = str13;
                                }
                            }
                            zzf = zzix().zzf(str12, zzkn3.name);
                            if (zzf == null) {
                                zzge().zzip().zze("Event aggregate wasn't created during raw event logging. appId, event", zzfg.zzbm(str), zzga().zzbj(str5));
                                arrayMap11 = arrayMap24;
                                arrayMap10 = arrayMap25;
                                hashSet = hashSet3;
                                arrayMap9 = arrayMap19;
                                zzkoArr2 = zzkoArr;
                                long j6 = j2;
                                str6 = str5;
                                zzeq = new zzeq(str12, zzkn3.name, 1, 1, zzkn3.zzatb.longValue(), 0, null, null, null);
                            } else {
                                hashSet = hashSet3;
                                arrayMap9 = arrayMap19;
                                zzkoArr2 = zzkoArr;
                                str6 = str5;
                                arrayMap11 = arrayMap24;
                                arrayMap10 = arrayMap25;
                                long j7 = j2;
                                zzeq = zzf.zzie();
                            }
                            zzix().zza(zzeq);
                            long j522 = zzeq.zzafr;
                            ArrayMap arrayMap2722 = arrayMap8;
                            String str1622 = str6;
                            map = (Map) arrayMap2722.get(str1622);
                            if (map == null) {
                                str7 = str;
                                map = zzix().zzk(str7, str1622);
                                if (map == null) {
                                    map = new ArrayMap();
                                }
                                arrayMap2722.put(str1622, map);
                            } else {
                                str7 = str;
                            }
                            it2 = map.keySet().iterator();
                            while (it2.hasNext()) {
                                int intValue2 = ((Integer) it2.next()).intValue();
                                HashSet hashSet4 = hashSet;
                                if (hashSet4.contains(Integer.valueOf(intValue2))) {
                                    zzge().zzit().zzg("Skipping failed audience ID", Integer.valueOf(intValue2));
                                    hashSet2 = hashSet4;
                                } else {
                                    ArrayMap arrayMap28 = arrayMap9;
                                    ArrayMap arrayMap29 = arrayMap11;
                                    BitSet bitSet3 = (BitSet) arrayMap29.get(Integer.valueOf(intValue2));
                                    ArrayMap arrayMap30 = arrayMap2722;
                                    ArrayMap arrayMap31 = arrayMap10;
                                    BitSet bitSet4 = (BitSet) arrayMap31.get(Integer.valueOf(intValue2));
                                    if (((zzkm) arrayMap28.get(Integer.valueOf(intValue2))) == null) {
                                        zzkm zzkm2 = new zzkm();
                                        arrayMap28.put(Integer.valueOf(intValue2), zzkm2);
                                        zzkm2.zzasy = Boolean.valueOf(true);
                                        BitSet bitSet5 = new BitSet();
                                        arrayMap29.put(Integer.valueOf(intValue2), bitSet5);
                                        bitSet4 = new BitSet();
                                        arrayMap31.put(Integer.valueOf(intValue2), bitSet4);
                                        bitSet3 = bitSet5;
                                    }
                                    Iterator it6 = ((List) map.get(Integer.valueOf(intValue2))).iterator();
                                    while (it6.hasNext()) {
                                        Map map4 = map;
                                        zzke zzke = (zzke) it6.next();
                                        Iterator it7 = it2;
                                        Iterator it8 = it6;
                                        if (zzge().isLoggable(2)) {
                                            arrayMap14 = arrayMap31;
                                            arrayMap13 = arrayMap29;
                                            arrayMap12 = arrayMap28;
                                            zzge().zzit().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(intValue2), zzke.zzarp, zzga().zzbj(zzke.zzarq));
                                            zzge().zzit().zzg("Filter definition", zzga().zza(zzke));
                                        } else {
                                            arrayMap14 = arrayMap31;
                                            arrayMap12 = arrayMap28;
                                            arrayMap13 = arrayMap29;
                                        }
                                        if (zzke.zzarp == null || zzke.zzarp.intValue() > 256) {
                                            long j8 = j522;
                                            zzko[] zzkoArr8 = zzkoArr2;
                                            String str17 = str;
                                            zzge().zzip().zze("Invalid event filter ID. appId, id", zzfg.zzbm(str), String.valueOf(zzke.zzarp));
                                            zzkoArr2 = zzkoArr8;
                                            map2 = map4;
                                            it2 = it7;
                                            it6 = it8;
                                            arrayMap29 = arrayMap13;
                                            arrayMap28 = arrayMap12;
                                            j522 = j8;
                                            str8 = str17;
                                            arrayMap31 = arrayMap14;
                                        } else {
                                            if (bitSet3.get(zzke.zzarp.intValue())) {
                                                zzge().zzit().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue2), zzke.zzarp);
                                                map2 = map4;
                                                it2 = it7;
                                                it6 = it8;
                                                arrayMap31 = arrayMap14;
                                                arrayMap29 = arrayMap13;
                                                arrayMap28 = arrayMap12;
                                            } else {
                                                if (zzke.zzart != null) {
                                                    Boolean zza3 = zza(j522, zzke.zzart);
                                                    if (zza3 != null) {
                                                        if (!zza3.booleanValue()) {
                                                            j3 = j522;
                                                            bool2 = Boolean.valueOf(false);
                                                            zzkoArr3 = zzkoArr2;
                                                            zzge().zzit().zzg("Event filter result", bool2 == 0 ? "null" : bool2);
                                                            if (bool2 == 0) {
                                                                hashSet4.add(Integer.valueOf(intValue2));
                                                            } else {
                                                                bitSet4.set(zzke.zzarp.intValue());
                                                                if (bool2.booleanValue()) {
                                                                    bitSet3.set(zzke.zzarp.intValue());
                                                                }
                                                            }
                                                            zzkoArr2 = zzkoArr3;
                                                            map2 = map4;
                                                            it2 = it7;
                                                            it6 = it8;
                                                            arrayMap31 = arrayMap14;
                                                            arrayMap29 = arrayMap13;
                                                            arrayMap28 = arrayMap12;
                                                            j522 = j3;
                                                        }
                                                    }
                                                    j3 = j522;
                                                    zzkoArr3 = zzkoArr2;
                                                    bool2 = 0;
                                                    zzge().zzit().zzg("Event filter result", bool2 == 0 ? "null" : bool2);
                                                    if (bool2 == 0) {
                                                    }
                                                    zzkoArr2 = zzkoArr3;
                                                    map2 = map4;
                                                    it2 = it7;
                                                    it6 = it8;
                                                    arrayMap31 = arrayMap14;
                                                    arrayMap29 = arrayMap13;
                                                    arrayMap28 = arrayMap12;
                                                    j522 = j3;
                                                }
                                                HashSet hashSet5 = new HashSet();
                                                for (zzkf zzkf : zzke.zzarr) {
                                                    if (TextUtils.isEmpty(zzkf.zzary)) {
                                                        zzge().zzip().zzg("null or empty param name in filter. event", zzga().zzbj(str1622));
                                                        j3 = j522;
                                                        zzkoArr3 = zzkoArr2;
                                                        break;
                                                    }
                                                    hashSet5.add(zzkf.zzary);
                                                }
                                                ArrayMap arrayMap32 = new ArrayMap();
                                                zzkoArr3 = zzkoArr2;
                                                int length4 = zzkoArr3.length;
                                                int i14 = 0;
                                                while (true) {
                                                    if (i14 < length4) {
                                                        zzko zzko2 = zzkoArr3[i14];
                                                        j3 = j522;
                                                        if (hashSet5.contains(zzko2.name)) {
                                                            if (zzko2.zzate == null) {
                                                                if (zzko2.zzarc == null) {
                                                                    if (zzko2.zzajf == null) {
                                                                        zzip2 = zzge().zzip();
                                                                        str10 = "Unknown value for param. event, param";
                                                                        zzbj = zzga().zzbj(str1622);
                                                                        zzbk = zzga().zzbk(zzko2.name);
                                                                        break;
                                                                    }
                                                                    str11 = zzko2.name;
                                                                    obj2 = zzko2.zzajf;
                                                                } else {
                                                                    str11 = zzko2.name;
                                                                    obj2 = zzko2.zzarc;
                                                                }
                                                            } else {
                                                                str11 = zzko2.name;
                                                                obj2 = zzko2.zzate;
                                                            }
                                                            arrayMap32.put(str11, obj2);
                                                        }
                                                        i14++;
                                                        j522 = j3;
                                                    } else {
                                                        j3 = j522;
                                                        zzkf[] zzkfArr2 = zzke.zzarr;
                                                        int length5 = zzkfArr2.length;
                                                        int i15 = 0;
                                                        while (true) {
                                                            if (i15 >= length5) {
                                                                boolean z5 = true;
                                                                break;
                                                            }
                                                            zzkf zzkf2 = zzkfArr2[i15];
                                                            boolean equals = Boolean.TRUE.equals(zzkf2.zzarx);
                                                            str9 = zzkf2.zzary;
                                                            if (TextUtils.isEmpty(str9)) {
                                                                zzge().zzip().zzg("Event has empty param name. event", zzga().zzbj(str1622));
                                                                break;
                                                            }
                                                            zzkf[] zzkfArr3 = zzkfArr2;
                                                            Object obj3 = arrayMap32.get(str9);
                                                            int i16 = length5;
                                                            if (obj3 instanceof Long) {
                                                                if (zzkf2.zzarw == null) {
                                                                    zzip2 = zzge().zzip();
                                                                    str10 = "No number filter for long param. event, param";
                                                                    break;
                                                                }
                                                                Boolean zza4 = zza(((Long) obj3).longValue(), zzkf2.zzarw);
                                                                if (zza4 == null) {
                                                                    break;
                                                                } else if ((!zza4.booleanValue()) ^ equals) {
                                                                    break;
                                                                } else {
                                                                    i15++;
                                                                    zzkfArr2 = zzkfArr3;
                                                                    length5 = i16;
                                                                }
                                                            } else if (obj3 instanceof Double) {
                                                                if (zzkf2.zzarw == null) {
                                                                    zzip2 = zzge().zzip();
                                                                    str10 = "No number filter for double param. event, param";
                                                                    break;
                                                                }
                                                                Boolean zza5 = zza(((Double) obj3).doubleValue(), zzkf2.zzarw);
                                                                if (zza5 == null) {
                                                                    break;
                                                                } else if ((!zza5.booleanValue()) ^ equals) {
                                                                    break;
                                                                } else {
                                                                    i15++;
                                                                    zzkfArr2 = zzkfArr3;
                                                                    length5 = i16;
                                                                }
                                                            } else if (obj3 instanceof String) {
                                                                if (zzkf2.zzarv == null) {
                                                                    if (zzkf2.zzarw == null) {
                                                                        zzip2 = zzge().zzip();
                                                                        str10 = "No filter for String param. event, param";
                                                                        break;
                                                                    }
                                                                    String str18 = (String) obj3;
                                                                    if (!zzka.zzck(str18)) {
                                                                        zzip2 = zzge().zzip();
                                                                        str10 = "Invalid param value for number filter. event, param";
                                                                        break;
                                                                    }
                                                                    bool3 = zza(str18, zzkf2.zzarw);
                                                                } else {
                                                                    bool3 = zza((String) obj3, zzkf2.zzarv);
                                                                }
                                                                if (bool3 == null) {
                                                                    break;
                                                                } else if ((!bool3.booleanValue()) ^ equals) {
                                                                    break;
                                                                } else {
                                                                    i15++;
                                                                    zzkfArr2 = zzkfArr3;
                                                                    length5 = i16;
                                                                }
                                                            } else if (obj3 == null) {
                                                                zzge().zzit().zze("Missing param for filter. event, param", zzga().zzbj(str1622), zzga().zzbk(str9));
                                                            } else {
                                                                zzip2 = zzge().zzip();
                                                                str10 = "Unknown param type. event, param";
                                                            }
                                                        }
                                                        zzbj = zzga().zzbj(str1622);
                                                        zzbk = zzga().zzbk(str9);
                                                    }
                                                }
                                                zzip2.zze(str10, zzbj, zzbk);
                                                bool2 = 0;
                                                zzge().zzit().zzg("Event filter result", bool2 == 0 ? "null" : bool2);
                                                if (bool2 == 0) {
                                                }
                                                zzkoArr2 = zzkoArr3;
                                                map2 = map4;
                                                it2 = it7;
                                                it6 = it8;
                                                arrayMap31 = arrayMap14;
                                                arrayMap29 = arrayMap13;
                                                arrayMap28 = arrayMap12;
                                                j522 = j3;
                                            }
                                            str8 = str;
                                        }
                                    }
                                    ArrayMap arrayMap33 = arrayMap31;
                                    String str19 = str7;
                                    hashSet2 = hashSet4;
                                    arrayMap9 = arrayMap28;
                                    arrayMap11 = arrayMap29;
                                    arrayMap2722 = arrayMap30;
                                    arrayMap10 = arrayMap33;
                                }
                            }
                            arrayMap7 = arrayMap2722;
                            str4 = str7;
                            arrayMap5 = arrayMap11;
                            arrayMap6 = arrayMap10;
                            hashSet3 = hashSet;
                            arrayMap4 = arrayMap9;
                            l4 = l;
                            j4 = j;
                            i10 = i2 + 1;
                            zzknArr2 = zzknArr;
                            str12 = str4;
                            length2 = i;
                            arrayMap26 = arrayMap7;
                            arrayMap25 = arrayMap6;
                            arrayMap24 = arrayMap5;
                            arrayMap19 = arrayMap4;
                            zzksArr2 = zzksArr;
                        }
                    } else {
                        i5 = i10;
                    }
                    z = false;
                    if (!z) {
                    }
                    zzf = zzix().zzf(str12, zzkn3.name);
                    if (zzf == null) {
                    }
                    zzix().zza(zzeq);
                    long j5222 = zzeq.zzafr;
                    ArrayMap arrayMap27222 = arrayMap8;
                    String str16222 = str6;
                    map = (Map) arrayMap27222.get(str16222);
                    if (map == null) {
                    }
                    it2 = map.keySet().iterator();
                    while (it2.hasNext()) {
                    }
                    arrayMap7 = arrayMap27222;
                    str4 = str7;
                    arrayMap5 = arrayMap11;
                    arrayMap6 = arrayMap10;
                    hashSet3 = hashSet;
                    arrayMap4 = arrayMap9;
                    l4 = l;
                    j4 = j;
                    i10 = i2 + 1;
                    zzknArr2 = zzknArr;
                    str12 = str4;
                    length2 = i;
                    arrayMap26 = arrayMap7;
                    arrayMap25 = arrayMap6;
                    arrayMap24 = arrayMap5;
                    arrayMap19 = arrayMap4;
                    zzksArr2 = zzksArr;
                } else {
                    i4 = i10;
                    arrayMap8 = arrayMap26;
                    i3 = length2;
                    zzkoArr = zzkoArr5;
                }
                j2 = 0;
                str5 = str13;
                j = j4;
                l = l4;
                zzf = zzix().zzf(str12, zzkn3.name);
                if (zzf == null) {
                }
                zzix().zza(zzeq);
                long j52222 = zzeq.zzafr;
                ArrayMap arrayMap272222 = arrayMap8;
                String str162222 = str6;
                map = (Map) arrayMap272222.get(str162222);
                if (map == null) {
                }
                it2 = map.keySet().iterator();
                while (it2.hasNext()) {
                }
                arrayMap7 = arrayMap272222;
                str4 = str7;
                arrayMap5 = arrayMap11;
                arrayMap6 = arrayMap10;
                hashSet3 = hashSet;
                arrayMap4 = arrayMap9;
                l4 = l;
                j4 = j;
                i10 = i2 + 1;
                zzknArr2 = zzknArr;
                str12 = str4;
                length2 = i;
                arrayMap26 = arrayMap7;
                arrayMap25 = arrayMap6;
                arrayMap24 = arrayMap5;
                arrayMap19 = arrayMap4;
                zzksArr2 = zzksArr;
            }
        }
        ArrayMap arrayMap34 = arrayMap19;
        String str20 = str12;
        ArrayMap arrayMap35 = arrayMap24;
        ArrayMap arrayMap36 = arrayMap25;
        zzks[] zzksArr3 = zzksArr2;
        if (zzksArr3 != null) {
            ArrayMap arrayMap37 = new ArrayMap();
            int length6 = zzksArr3.length;
            int i17 = 0;
            while (i17 < length6) {
                zzks zzks = zzksArr3[i17];
                Map map5 = (Map) arrayMap37.get(zzks.name);
                if (map5 == null) {
                    map5 = zzix().zzl(str20, zzks.name);
                    if (map5 == null) {
                        map5 = new ArrayMap();
                    }
                    arrayMap37.put(zzks.name, map5);
                }
                Iterator it9 = map5.keySet().iterator();
                while (it9.hasNext()) {
                    int intValue3 = ((Integer) it9.next()).intValue();
                    if (hashSet3.contains(Integer.valueOf(intValue3))) {
                        zzge().zzit().zzg("Skipping failed audience ID", Integer.valueOf(intValue3));
                    } else {
                        ArrayMap arrayMap38 = arrayMap34;
                        ArrayMap arrayMap39 = arrayMap35;
                        BitSet bitSet6 = (BitSet) arrayMap39.get(Integer.valueOf(intValue3));
                        ArrayMap arrayMap40 = arrayMap37;
                        ArrayMap arrayMap41 = arrayMap36;
                        BitSet bitSet7 = (BitSet) arrayMap41.get(Integer.valueOf(intValue3));
                        if (((zzkm) arrayMap38.get(Integer.valueOf(intValue3))) == null) {
                            zzkm zzkm3 = new zzkm();
                            arrayMap38.put(Integer.valueOf(intValue3), zzkm3);
                            zzkm3.zzasy = Boolean.valueOf(true);
                            bitSet6 = new BitSet();
                            arrayMap39.put(Integer.valueOf(intValue3), bitSet6);
                            bitSet7 = new BitSet();
                            arrayMap41.put(Integer.valueOf(intValue3), bitSet7);
                        }
                        Iterator it10 = ((List) map5.get(Integer.valueOf(intValue3))).iterator();
                        while (true) {
                            if (!it10.hasNext()) {
                                Map map6 = map5;
                                arrayMap36 = arrayMap41;
                                arrayMap34 = arrayMap38;
                                arrayMap35 = arrayMap39;
                                arrayMap37 = arrayMap40;
                                break;
                            }
                            int i18 = length6;
                            zzkh zzkh = (zzkh) it10.next();
                            Map map7 = map5;
                            Iterator it11 = it9;
                            if (zzge().isLoggable(2)) {
                                it = it10;
                                arrayMap3 = arrayMap41;
                                arrayMap2 = arrayMap38;
                                arrayMap = arrayMap39;
                                zzge().zzit().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(intValue3), zzkh.zzarp, zzga().zzbl(zzkh.zzasf));
                                zzge().zzit().zzg("Filter definition", zzga().zza(zzkh));
                            } else {
                                arrayMap3 = arrayMap41;
                                it = it10;
                                arrayMap2 = arrayMap38;
                                arrayMap = arrayMap39;
                            }
                            if (zzkh.zzarp == null) {
                                break;
                            } else if (zzkh.zzarp.intValue() > 256) {
                                break;
                            } else {
                                if (bitSet6.get(zzkh.zzarp.intValue())) {
                                    zzge().zzit().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue3), zzkh.zzarp);
                                } else {
                                    zzkf zzkf3 = zzkh.zzasg;
                                    if (zzkf3 == null) {
                                        zzip = zzge().zzip();
                                        str3 = "Missing property filter. property";
                                    } else {
                                        boolean equals2 = Boolean.TRUE.equals(zzkf3.zzarx);
                                        if (zzks.zzate != null) {
                                            if (zzkf3.zzarw == null) {
                                                zzip = zzge().zzip();
                                                str3 = "No number filter for long property. property";
                                            } else {
                                                zza = zza(zzks.zzate.longValue(), zzkf3.zzarw);
                                            }
                                        } else if (zzks.zzarc != null) {
                                            if (zzkf3.zzarw == null) {
                                                zzip = zzge().zzip();
                                                str3 = "No number filter for double property. property";
                                            } else {
                                                zza = zza(zzks.zzarc.doubleValue(), zzkf3.zzarw);
                                            }
                                        } else if (zzks.zzajf == null) {
                                            zzip = zzge().zzip();
                                            str3 = "User property has no value, property";
                                        } else if (zzkf3.zzarv != null) {
                                            zza = zza(zzks.zzajf, zzkf3.zzarv);
                                        } else if (zzkf3.zzarw == null) {
                                            zzip = zzge().zzip();
                                            str3 = "No string or number filter defined. property";
                                        } else if (zzka.zzck(zzks.zzajf)) {
                                            zza = zza(zzks.zzajf, zzkf3.zzarw);
                                        } else {
                                            zzge().zzip().zze("Invalid user property value for Numeric number filter. property, value", zzga().zzbl(zzks.name), zzks.zzajf);
                                            bool = 0;
                                            zzge().zzit().zzg("Property filter result", bool == 0 ? "null" : bool);
                                            if (bool == 0) {
                                                hashSet3.add(Integer.valueOf(intValue3));
                                            } else {
                                                bitSet7.set(zzkh.zzarp.intValue());
                                                if (bool.booleanValue()) {
                                                    bitSet6.set(zzkh.zzarp.intValue());
                                                }
                                            }
                                        }
                                        bool = zza(zza, equals2);
                                        zzge().zzit().zzg("Property filter result", bool == 0 ? "null" : bool);
                                        if (bool == 0) {
                                        }
                                    }
                                    zzip.zzg(str3, zzga().zzbl(zzks.name));
                                    bool = 0;
                                    zzge().zzit().zzg("Property filter result", bool == 0 ? "null" : bool);
                                    if (bool == 0) {
                                    }
                                }
                                length6 = i18;
                                map5 = map7;
                                it9 = it11;
                                it10 = it;
                                arrayMap41 = arrayMap3;
                                arrayMap38 = arrayMap2;
                                arrayMap39 = arrayMap;
                            }
                        }
                        zzks[] zzksArr4 = zzksArr;
                    }
                }
                ArrayMap arrayMap42 = arrayMap37;
                int i19 = length6;
                ArrayMap arrayMap43 = arrayMap36;
                ArrayMap arrayMap44 = arrayMap35;
                ArrayMap arrayMap45 = arrayMap34;
                i17++;
                zzksArr3 = zzksArr;
            }
        }
        ArrayMap arrayMap46 = arrayMap36;
        ArrayMap arrayMap47 = arrayMap34;
        ArrayMap arrayMap48 = arrayMap35;
        zzkm[] zzkmArr = new zzkm[arrayMap48.size()];
        int i20 = 0;
        for (Integer intValue4 : arrayMap48.keySet()) {
            int intValue5 = intValue4.intValue();
            if (!hashSet3.contains(Integer.valueOf(intValue5))) {
                ArrayMap arrayMap49 = arrayMap47;
                zzkm zzkm4 = (zzkm) arrayMap49.get(Integer.valueOf(intValue5));
                if (zzkm4 == null) {
                    zzkm4 = new zzkm();
                }
                int i21 = i20 + 1;
                zzkmArr[i20] = zzkm4;
                zzkm4.zzarl = Integer.valueOf(intValue5);
                zzkm4.zzasw = new zzkr();
                zzkm4.zzasw.zzaul = zzka.zza((BitSet) arrayMap48.get(Integer.valueOf(intValue5)));
                ArrayMap arrayMap50 = arrayMap46;
                zzkm4.zzasw.zzauk = zzka.zza((BitSet) arrayMap50.get(Integer.valueOf(intValue5)));
                zzei zzix2 = zzix();
                zzkr zzkr2 = zzkm4.zzasw;
                zzix2.zzch();
                zzix2.zzab();
                Preconditions.checkNotEmpty(str);
                Preconditions.checkNotNull(zzkr2);
                try {
                    byte[] bArr = new byte[zzkr2.zzvm()];
                    zzabw zzb2 = zzabw.zzb(bArr, 0, bArr.length);
                    zzkr2.zza(zzb2);
                    zzb2.zzve();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str20);
                    contentValues.put("audience_id", Integer.valueOf(intValue5));
                    contentValues.put("current_results", bArr);
                    try {
                        try {
                            if (zzix2.getWritableDatabase().insertWithOnConflict("audience_filter_values", null, contentValues, 5) == -1) {
                                zzix2.zzge().zzim().zzg("Failed to insert filter results (got -1). appId", zzfg.zzbm(str));
                            }
                        } catch (SQLiteException e4) {
                            e = e4;
                            obj = e;
                            zzfi = zzix2.zzge().zzim();
                            str2 = "Error storing filter results. appId";
                            zzfi.zze(str2, zzfg.zzbm(str), obj);
                            arrayMap47 = arrayMap49;
                            i20 = i21;
                            arrayMap46 = arrayMap50;
                        }
                    } catch (SQLiteException e5) {
                        e = e5;
                        obj = e;
                        zzfi = zzix2.zzge().zzim();
                        str2 = "Error storing filter results. appId";
                        zzfi.zze(str2, zzfg.zzbm(str), obj);
                        arrayMap47 = arrayMap49;
                        i20 = i21;
                        arrayMap46 = arrayMap50;
                    }
                } catch (IOException e6) {
                    obj = e6;
                    zzfi = zzix2.zzge().zzim();
                    str2 = "Configuration loss. Failed to serialize filter results. appId";
                    zzfi.zze(str2, zzfg.zzbm(str), obj);
                    arrayMap47 = arrayMap49;
                    i20 = i21;
                    arrayMap46 = arrayMap50;
                }
                arrayMap47 = arrayMap49;
                i20 = i21;
                arrayMap46 = arrayMap50;
            }
        }
        return (zzkm[]) Arrays.copyOf(zzkmArr, i20);
    }

    /* access modifiers changed from: protected */
    public final boolean zzhf() {
        return false;
    }
}
