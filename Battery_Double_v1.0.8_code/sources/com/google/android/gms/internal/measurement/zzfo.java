package com.google.android.gms.internal.measurement;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

@WorkerThread
final class zzfo implements Runnable {
    private final String packageName;
    private final URL url;
    private final byte[] zzajl;
    private final zzfm zzajm;
    private final Map<String, String> zzajn;
    private final /* synthetic */ zzfk zzajo;

    public zzfo(zzfk zzfk, String str, URL url2, byte[] bArr, Map<String, String> map, zzfm zzfm) {
        this.zzajo = zzfk;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(url2);
        Preconditions.checkNotNull(zzfm);
        this.url = url2;
        this.zzajl = bArr;
        this.zzajm = zzfm;
        this.packageName = str;
        this.zzajn = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c5 A[SYNTHETIC, Splitter:B:44:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0102 A[SYNTHETIC, Splitter:B:57:0x0102] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011e  */
    public final void run() {
        Map map;
        Throwable th;
        int i;
        HttpURLConnection httpURLConnection;
        zzgg zzgd;
        zzfn zzfn;
        Map map2;
        this.zzajo.zzfs();
        OutputStream outputStream = null;
        try {
            httpURLConnection = this.zzajo.zzb(this.url);
            try {
                if (this.zzajn != null) {
                    for (Entry entry : this.zzajn.entrySet()) {
                        httpURLConnection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
                    }
                }
                if (this.zzajl != null) {
                    byte[] zza = this.zzajo.zzgb().zza(this.zzajl);
                    this.zzajo.zzge().zzit().zzg("Uploading data. size", Integer.valueOf(zza.length));
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.addRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
                    httpURLConnection.setFixedLengthStreamingMode(zza.length);
                    httpURLConnection.connect();
                    OutputStream outputStream2 = httpURLConnection.getOutputStream();
                    try {
                        outputStream2.write(zza);
                        outputStream2.close();
                    } catch (IOException e) {
                        map = null;
                        i = 0;
                        th = e;
                        outputStream = outputStream2;
                    } catch (Throwable th2) {
                        th = th2;
                        map = null;
                        i = 0;
                        outputStream = outputStream2;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        zzgg zzgd2 = this.zzajo.zzgd();
                        zzfn zzfn2 = new zzfn(this.packageName, this.zzajm, i, null, null, map);
                        zzgd2.zzc((Runnable) zzfn2);
                        throw th;
                    }
                }
                i = httpURLConnection.getResponseCode();
            } catch (IOException e2) {
                e = e2;
                map2 = null;
                i = 0;
                th = e;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzgd = this.zzajo.zzgd();
                zzfn = new zzfn(this.packageName, this.zzajm, i, th, null, map);
                zzgd.zzc((Runnable) zzfn);
            } catch (Throwable th3) {
                th = th3;
                map = null;
                i = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzgg zzgd22 = this.zzajo.zzgd();
                zzfn zzfn22 = new zzfn(this.packageName, this.zzajm, i, null, null, map);
                zzgd22.zzc((Runnable) zzfn22);
                throw th;
            }
            try {
                map = httpURLConnection.getHeaderFields();
                try {
                    byte[] zza2 = zzfk.zzb(httpURLConnection);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    zzgd = this.zzajo.zzgd();
                    zzfn = new zzfn(this.packageName, this.zzajm, i, null, zza2, map);
                } catch (IOException e3) {
                    e = e3;
                    th = e;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzgd = this.zzajo.zzgd();
                    zzfn = new zzfn(this.packageName, this.zzajm, i, th, null, map);
                    zzgd.zzc((Runnable) zzfn);
                } catch (Throwable th4) {
                    th = th4;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzgg zzgd222 = this.zzajo.zzgd();
                    zzfn zzfn222 = new zzfn(this.packageName, this.zzajm, i, null, null, map);
                    zzgd222.zzc((Runnable) zzfn222);
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                map = null;
                th = e;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzgd = this.zzajo.zzgd();
                zzfn = new zzfn(this.packageName, this.zzajm, i, th, null, map);
                zzgd.zzc((Runnable) zzfn);
            } catch (Throwable th5) {
                th = th5;
                map = null;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzgg zzgd2222 = this.zzajo.zzgd();
                zzfn zzfn2222 = new zzfn(this.packageName, this.zzajm, i, null, null, map);
                zzgd2222.zzc((Runnable) zzfn2222);
                throw th;
            }
        } catch (IOException e5) {
            e = e5;
            httpURLConnection = null;
            map2 = null;
            i = 0;
            th = e;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e6) {
                    this.zzajo.zzge().zzim().zze("Error closing HTTP compressed POST connection output stream. appId", zzfg.zzbm(this.packageName), e6);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            zzgd = this.zzajo.zzgd();
            zzfn = new zzfn(this.packageName, this.zzajm, i, th, null, map);
            zzgd.zzc((Runnable) zzfn);
        } catch (Throwable th6) {
            th = th6;
            httpURLConnection = null;
            map = null;
            i = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e7) {
                    this.zzajo.zzge().zzim().zze("Error closing HTTP compressed POST connection output stream. appId", zzfg.zzbm(this.packageName), e7);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            zzgg zzgd22222 = this.zzajo.zzgd();
            zzfn zzfn22222 = new zzfn(this.packageName, this.zzajm, i, null, null, map);
            zzgd22222.zzc((Runnable) zzfn22222);
            throw th;
        }
        zzgd.zzc((Runnable) zzfn);
    }
}
