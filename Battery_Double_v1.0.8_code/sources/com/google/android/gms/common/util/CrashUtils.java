package com.google.android.gms.common.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Debug;
import android.os.DropBoxManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.measurement.AppMeasurement;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.annotation.concurrent.GuardedBy;

public final class CrashUtils {
    private static final String[] zzzc = {"android.", "com.android.", "dalvik.", "java.", "javax."};
    private static DropBoxManager zzzd = null;
    private static boolean zzze = false;
    private static boolean zzzf = false;
    private static boolean zzzg = false;
    private static int zzzh = -1;
    @GuardedBy("CrashUtils.class")
    private static int zzzi;
    @GuardedBy("CrashUtils.class")
    private static int zzzj;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ErrorDialogData {
        public static final int AVG_CRASH_FREQ = 2;
        public static final int BINDER_CRASH = 268435456;
        public static final int DYNAMITE_CRASH = 536870912;
        public static final int FORCED_SHUSHED_BY_WRAPPER = 4;
        public static final int NONE = 0;
        public static final int POPUP_FREQ = 1;
        public static final int SUPPRESSED = 1073741824;
    }

    public static boolean addDynamiteErrorToDropBox(Context context, Throwable th) {
        return addErrorToDropBoxInternal(context, th, ErrorDialogData.DYNAMITE_CRASH);
    }

    @Deprecated
    public static boolean addErrorToDropBox(Context context, Throwable th) {
        return addDynamiteErrorToDropBox(context, th);
    }

    public static boolean addErrorToDropBoxInternal(Context context, String str, String str2, int i) {
        return zza(context, str, str2, i, null);
    }

    public static boolean addErrorToDropBoxInternal(Context context, Throwable th, int i) {
        boolean z;
        try {
            Preconditions.checkNotNull(context);
            Preconditions.checkNotNull(th);
            if (!isPackageSide()) {
                return false;
            }
            if (!zzdb()) {
                th = zza(th);
                if (th == null) {
                    return false;
                }
            }
            return zza(context, Log.getStackTraceString(th), ProcessUtils.getMyProcessName(), i, th);
        } catch (Exception e) {
            try {
                z = zzdb();
            } catch (Exception e2) {
                Log.e("CrashUtils", "Error determining which process we're running in!", e2);
                z = false;
            }
            if (z) {
                throw e;
            }
            Log.e("CrashUtils", "Error adding exception to DropBox!", e);
            return false;
        }
    }

    private static boolean isPackageSide() {
        if (zzze) {
            return zzzf;
        }
        return false;
    }

    public static boolean isSystemClassPrefixInternal(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        for (String startsWith : zzzc) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    @VisibleForTesting
    public static synchronized void setTestVariables(DropBoxManager dropBoxManager, boolean z, boolean z2, int i) {
        synchronized (CrashUtils.class) {
            zzze = true;
            zzzd = dropBoxManager;
            zzzg = z;
            zzzf = z2;
            zzzh = i;
            zzzi = 0;
            zzzj = 0;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:49|50|51|52|53|54|55|56|57|(4:58|59|(2:60|(1:62)(1:63))|64)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(15:47|48|49|50|51|52|53|54|55|56|57|58|59|(2:60|(1:62)(1:63))|64) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:69|(2:78|79)|80|81) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x0152 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0159 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x018f */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x016c A[Catch:{ IOException -> 0x0177, all -> 0x0174 }, LOOP:0: B:60:0x0166->B:62:0x016c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0170 A[EDGE_INSN: B:63:0x0170->B:64:? ?: BREAK  
EDGE_INSN: B:63:0x0170->B:64:? ?: BREAK  
EDGE_INSN: B:63:0x0170->B:64:? ?: BREAK  , SYNTHETIC, Splitter:B:63:0x0170] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0170 A[EDGE_INSN: B:63:0x0170->B:64:? ?: BREAK  
EDGE_INSN: B:63:0x0170->B:64:? ?: BREAK  , SYNTHETIC, Splitter:B:63:0x0170] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0186 A[SYNTHETIC, Splitter:B:75:0x0186] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x018c A[SYNTHETIC, Splitter:B:78:0x018c] */
    @VisibleForTesting
    private static synchronized String zza(Context context, String str, String str2, int i) {
        int i2;
        synchronized (CrashUtils.class) {
            StringBuilder sb = new StringBuilder(1024);
            sb.append("Process: ");
            sb.append(Strings.nullToEmpty(str2));
            sb.append("\n");
            sb.append("Package: com.google.android.gms");
            int i3 = 12451009;
            String str3 = "12.4.51 (020308-{{cl}})";
            if (zzdb()) {
                try {
                    PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo(context.getPackageName(), 0);
                    int i4 = packageInfo.versionCode;
                    try {
                        if (packageInfo.versionName != null) {
                            str3 = packageInfo.versionName;
                        }
                        i3 = i4;
                    } catch (Exception e) {
                        e = e;
                        i3 = i4;
                        Log.w("CrashUtils", "Error while trying to get the package information! Using static version.", e);
                        sb.append(" v");
                        sb.append(i3);
                        if (!TextUtils.isEmpty(str3)) {
                        }
                        sb.append("\n");
                        sb.append("Build: ");
                        sb.append(Build.FINGERPRINT);
                        sb.append("\n");
                        if (Debug.isDebuggerConnected()) {
                        }
                        if (i != 0) {
                        }
                        sb.append("\n");
                        if (!TextUtils.isEmpty(str)) {
                        }
                        if (zzdb()) {
                        }
                        if (i2 > 0) {
                        }
                        String sb2 = sb.toString();
                        return sb2;
                    }
                } catch (Exception e2) {
                    e = e2;
                    Log.w("CrashUtils", "Error while trying to get the package information! Using static version.", e);
                    sb.append(" v");
                    sb.append(i3);
                    if (!TextUtils.isEmpty(str3)) {
                    }
                    sb.append("\n");
                    sb.append("Build: ");
                    sb.append(Build.FINGERPRINT);
                    sb.append("\n");
                    if (Debug.isDebuggerConnected()) {
                    }
                    if (i != 0) {
                    }
                    sb.append("\n");
                    if (!TextUtils.isEmpty(str)) {
                    }
                    if (zzdb()) {
                    }
                    if (i2 > 0) {
                    }
                    String sb22 = sb.toString();
                    return sb22;
                }
            }
            sb.append(" v");
            sb.append(i3);
            if (!TextUtils.isEmpty(str3)) {
                if (str3.contains("(") && !str3.contains(")")) {
                    if (str3.endsWith("-")) {
                        str3 = String.valueOf(str3).concat("111111111");
                    }
                    str3 = String.valueOf(str3).concat(")");
                }
                sb.append(" (");
                sb.append(str3);
                sb.append(")");
            }
            sb.append("\n");
            sb.append("Build: ");
            sb.append(Build.FINGERPRINT);
            sb.append("\n");
            if (Debug.isDebuggerConnected()) {
                sb.append("Debugger: Connected\n");
            }
            if (i != 0) {
                sb.append("DD-EDD: ");
                sb.append(i);
                sb.append("\n");
            }
            sb.append("\n");
            if (!TextUtils.isEmpty(str)) {
                sb.append(str);
            }
            if (zzdb()) {
                i2 = zzzh >= 0 ? zzzh : Secure.getInt(context.getContentResolver(), "logcat_for_system_app_crash", 0);
            } else {
                i2 = 0;
            }
            if (i2 > 0) {
                sb.append("\n");
                InputStreamReader inputStreamReader = null;
                try {
                    Process start = new ProcessBuilder(new String[]{"/system/bin/logcat", "-v", "time", "-b", "events", "-b", "system", "-b", "main", "-b", AppMeasurement.CRASH_ORIGIN, "-t", String.valueOf(i2)}).redirectErrorStream(true).start();
                    start.getOutputStream().close();
                    start.getErrorStream().close();
                    InputStreamReader inputStreamReader2 = new InputStreamReader(start.getInputStream());
                    try {
                        char[] cArr = new char[8192];
                        while (true) {
                            int read = inputStreamReader2.read(cArr);
                            if (read > 0) {
                                sb.append(cArr, 0, read);
                            } else {
                                try {
                                    break;
                                } catch (IOException unused) {
                                }
                            }
                        }
                        inputStreamReader2.close();
                    } catch (IOException e3) {
                        e = e3;
                        inputStreamReader = inputStreamReader2;
                        try {
                            Log.e("CrashUtils", "Error running logcat", e);
                            if (inputStreamReader != null) {
                            }
                            String sb222 = sb.toString();
                            return sb222;
                        } catch (Throwable th) {
                            th = th;
                            if (inputStreamReader != null) {
                                inputStreamReader.close();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        inputStreamReader = inputStreamReader2;
                        if (inputStreamReader != null) {
                        }
                        throw th;
                    }
                } catch (IOException e4) {
                    e = e4;
                    Log.e("CrashUtils", "Error running logcat", e);
                    if (inputStreamReader != null) {
                        inputStreamReader.close();
                    }
                    String sb2222 = sb.toString();
                    return sb2222;
                }
            }
            String sb22222 = sb.toString();
        }
        return sb22222;
    }

    @VisibleForTesting
    private static synchronized Throwable zza(Throwable th) {
        synchronized (CrashUtils.class) {
            LinkedList linkedList = new LinkedList();
            while (th != null) {
                linkedList.push(th);
                th = th.getCause();
            }
            Throwable th2 = null;
            boolean z = false;
            while (!linkedList.isEmpty()) {
                Throwable th3 = (Throwable) linkedList.pop();
                StackTraceElement[] stackTrace = th3.getStackTrace();
                ArrayList arrayList = new ArrayList();
                arrayList.add(new StackTraceElement(th3.getClass().getName(), "<filtered>", "<filtered>", 1));
                boolean z2 = z;
                for (StackTraceElement stackTraceElement : stackTrace) {
                    String className = stackTraceElement.getClassName();
                    String fileName = stackTraceElement.getFileName();
                    boolean z3 = !TextUtils.isEmpty(fileName) && fileName.startsWith(":com.google.android.gms");
                    z2 |= z3;
                    if (!z3 && !isSystemClassPrefixInternal(className)) {
                        stackTraceElement = new StackTraceElement("<filtered>", "<filtered>", "<filtered>", 1);
                    }
                    arrayList.add(stackTraceElement);
                }
                th2 = th2 == null ? new Throwable("<filtered>") : new Throwable("<filtered>", th2);
                th2.setStackTrace((StackTraceElement[]) arrayList.toArray(new StackTraceElement[0]));
                z = z2;
            }
            if (!z) {
                return null;
            }
            return th2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0056, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0058, code lost:
        return false;
     */
    private static synchronized boolean zza(Context context, String str, String str2, int i, Throwable th) {
        synchronized (CrashUtils.class) {
            Preconditions.checkNotNull(context);
            if (isPackageSide()) {
                if (!Strings.isEmptyOrWhitespace(str)) {
                    int hashCode = str.hashCode();
                    int hashCode2 = th == null ? zzzj : th.hashCode();
                    if (zzzi == hashCode && zzzj == hashCode2) {
                        return false;
                    }
                    zzzi = hashCode;
                    zzzj = hashCode2;
                    DropBoxManager dropBoxManager = zzzd != null ? zzzd : (DropBoxManager) context.getSystemService("dropbox");
                    if (dropBoxManager != null) {
                        if (dropBoxManager.isTagEnabled("system_app_crash")) {
                            dropBoxManager.addText("system_app_crash", zza(context, str, str2, i));
                            return true;
                        }
                    }
                }
            }
        }
    }

    private static boolean zzdb() {
        if (zzze) {
            return zzzg;
        }
        return false;
    }
}
