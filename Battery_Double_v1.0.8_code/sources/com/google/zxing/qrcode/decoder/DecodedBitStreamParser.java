package com.google.zxing.qrcode.decoder;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.common.BitSource;
import com.google.zxing.common.CharacterSetECI;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.StringUtils;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

final class DecodedBitStreamParser {
    private static final char[] ALPHANUMERIC_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();
    private static final int GB2312_SUBSET = 1;

    private DecodedBitStreamParser() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e6 A[LOOP:0: B:1:0x001e->B:59:0x00e6, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00c5 A[SYNTHETIC] */
    static DecoderResult decode(byte[] bArr, Version version, ErrorCorrectionLevel errorCorrectionLevel, Map<DecodeHintType, ?> map) throws FormatException {
        Mode forBits;
        Mode mode;
        String str;
        Version version2 = version;
        byte[] bArr2 = bArr;
        BitSource bitSource = new BitSource(bArr2);
        StringBuilder sb = new StringBuilder(50);
        int i = 1;
        ArrayList arrayList = new ArrayList(1);
        boolean z = false;
        int i2 = -1;
        int i3 = -1;
        CharacterSetECI characterSetECI = null;
        while (true) {
            try {
                if (bitSource.available() < 4) {
                    forBits = Mode.TERMINATOR;
                } else {
                    forBits = Mode.forBits(bitSource.readBits(4));
                }
                Mode mode2 = forBits;
                if (mode2 != Mode.TERMINATOR) {
                    if (mode2 != Mode.FNC1_FIRST_POSITION) {
                        if (mode2 != Mode.FNC1_SECOND_POSITION) {
                            if (mode2 == Mode.STRUCTURED_APPEND) {
                                if (bitSource.available() < 16) {
                                    throw FormatException.getFormatInstance();
                                }
                                int readBits = bitSource.readBits(8);
                                i3 = bitSource.readBits(8);
                                i2 = readBits;
                            } else if (mode2 == Mode.ECI) {
                                characterSetECI = CharacterSetECI.getCharacterSetECIByValue(parseECIValue(bitSource));
                                if (characterSetECI == null) {
                                    throw FormatException.getFormatInstance();
                                }
                            } else if (mode2 == Mode.HANZI) {
                                int readBits2 = bitSource.readBits(4);
                                int readBits3 = bitSource.readBits(mode2.getCharacterCountBits(version2));
                                if (readBits2 == i) {
                                    decodeHanziSegment(bitSource, sb, readBits3);
                                }
                            } else {
                                int readBits4 = bitSource.readBits(mode2.getCharacterCountBits(version2));
                                if (mode2 == Mode.NUMERIC) {
                                    decodeNumericSegment(bitSource, sb, readBits4);
                                } else if (mode2 == Mode.ALPHANUMERIC) {
                                    decodeAlphanumericSegment(bitSource, sb, readBits4, z);
                                } else {
                                    if (mode2 == Mode.BYTE) {
                                        mode = mode2;
                                        decodeByteSegment(bitSource, sb, readBits4, characterSetECI, arrayList, map);
                                    } else {
                                        mode = mode2;
                                        if (mode == Mode.KANJI) {
                                            decodeKanjiSegment(bitSource, sb, readBits4);
                                        } else {
                                            throw FormatException.getFormatInstance();
                                        }
                                    }
                                    if (mode == Mode.TERMINATOR) {
                                        String sb2 = sb.toString();
                                        ArrayList arrayList2 = arrayList.isEmpty() ? null : arrayList;
                                        if (errorCorrectionLevel == null) {
                                            str = null;
                                        } else {
                                            str = errorCorrectionLevel.toString();
                                        }
                                        DecoderResult decoderResult = new DecoderResult(bArr2, sb2, arrayList2, str, i2, i3);
                                        return decoderResult;
                                    }
                                    i = 1;
                                }
                            }
                        }
                    }
                    mode = mode2;
                    z = true;
                    if (mode == Mode.TERMINATOR) {
                    }
                }
                mode = mode2;
                if (mode == Mode.TERMINATOR) {
                }
            } catch (IllegalArgumentException unused) {
                throw FormatException.getFormatInstance();
            }
        }
    }

    private static void decodeHanziSegment(BitSource bitSource, StringBuilder sb, int i) throws FormatException {
        if (i * 13 > bitSource.available()) {
            throw FormatException.getFormatInstance();
        }
        byte[] bArr = new byte[(2 * i)];
        int i2 = 0;
        while (i > 0) {
            int readBits = bitSource.readBits(13);
            int i3 = (readBits % 96) | ((readBits / 96) << 8);
            int i4 = i3 + (i3 < 959 ? 41377 : 42657);
            bArr[i2] = (byte) (i4 >> 8);
            bArr[i2 + 1] = (byte) i4;
            i2 += 2;
            i--;
        }
        try {
            sb.append(new String(bArr, StringUtils.GB2312));
        } catch (UnsupportedEncodingException unused) {
            throw FormatException.getFormatInstance();
        }
    }

    private static void decodeKanjiSegment(BitSource bitSource, StringBuilder sb, int i) throws FormatException {
        if (i * 13 > bitSource.available()) {
            throw FormatException.getFormatInstance();
        }
        byte[] bArr = new byte[(2 * i)];
        int i2 = 0;
        while (i > 0) {
            int readBits = bitSource.readBits(13);
            int i3 = (readBits % PsExtractor.AUDIO_STREAM) | ((readBits / PsExtractor.AUDIO_STREAM) << 8);
            int i4 = i3 + (i3 < 7936 ? 33088 : 49472);
            bArr[i2] = (byte) (i4 >> 8);
            bArr[i2 + 1] = (byte) i4;
            i2 += 2;
            i--;
        }
        try {
            sb.append(new String(bArr, StringUtils.SHIFT_JIS));
        } catch (UnsupportedEncodingException unused) {
            throw FormatException.getFormatInstance();
        }
    }

    private static void decodeByteSegment(BitSource bitSource, StringBuilder sb, int i, CharacterSetECI characterSetECI, Collection<byte[]> collection, Map<DecodeHintType, ?> map) throws FormatException {
        String str;
        if ((i << 3) > bitSource.available()) {
            throw FormatException.getFormatInstance();
        }
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = (byte) bitSource.readBits(8);
        }
        if (characterSetECI == null) {
            str = StringUtils.guessEncoding(bArr, map);
        } else {
            str = characterSetECI.name();
        }
        try {
            sb.append(new String(bArr, str));
            collection.add(bArr);
        } catch (UnsupportedEncodingException unused) {
            throw FormatException.getFormatInstance();
        }
    }

    private static char toAlphaNumericChar(int i) throws FormatException {
        if (i < ALPHANUMERIC_CHARS.length) {
            return ALPHANUMERIC_CHARS[i];
        }
        throw FormatException.getFormatInstance();
    }

    private static void decodeAlphanumericSegment(BitSource bitSource, StringBuilder sb, int i, boolean z) throws FormatException {
        while (i > 1) {
            if (bitSource.available() < 11) {
                throw FormatException.getFormatInstance();
            }
            int readBits = bitSource.readBits(11);
            sb.append(toAlphaNumericChar(readBits / 45));
            sb.append(toAlphaNumericChar(readBits % 45));
            i -= 2;
        }
        if (i == 1) {
            if (bitSource.available() < 6) {
                throw FormatException.getFormatInstance();
            }
            sb.append(toAlphaNumericChar(bitSource.readBits(6)));
        }
        if (z) {
            for (int length = sb.length(); length < sb.length(); length++) {
                if (sb.charAt(length) == '%') {
                    if (length < sb.length() - 1) {
                        int i2 = length + 1;
                        if (sb.charAt(i2) == '%') {
                            sb.deleteCharAt(i2);
                        }
                    }
                    sb.setCharAt(length, 29);
                }
            }
        }
    }

    private static void decodeNumericSegment(BitSource bitSource, StringBuilder sb, int i) throws FormatException {
        while (i >= 3) {
            if (bitSource.available() < 10) {
                throw FormatException.getFormatInstance();
            }
            int readBits = bitSource.readBits(10);
            if (readBits >= 1000) {
                throw FormatException.getFormatInstance();
            }
            sb.append(toAlphaNumericChar(readBits / 100));
            sb.append(toAlphaNumericChar((readBits / 10) % 10));
            sb.append(toAlphaNumericChar(readBits % 10));
            i -= 3;
        }
        if (i != 2) {
            if (i == 1) {
                if (bitSource.available() < 4) {
                    throw FormatException.getFormatInstance();
                }
                int readBits2 = bitSource.readBits(4);
                if (readBits2 >= 10) {
                    throw FormatException.getFormatInstance();
                }
                sb.append(toAlphaNumericChar(readBits2));
            }
        } else if (bitSource.available() < 7) {
            throw FormatException.getFormatInstance();
        } else {
            int readBits3 = bitSource.readBits(7);
            if (readBits3 >= 100) {
                throw FormatException.getFormatInstance();
            }
            sb.append(toAlphaNumericChar(readBits3 / 10));
            sb.append(toAlphaNumericChar(readBits3 % 10));
        }
    }

    private static int parseECIValue(BitSource bitSource) throws FormatException {
        int readBits = bitSource.readBits(8);
        if ((readBits & 128) == 0) {
            return readBits & 127;
        }
        if ((readBits & PsExtractor.AUDIO_STREAM) == 128) {
            return bitSource.readBits(8) | ((readBits & 63) << 8);
        }
        if ((readBits & 224) == 192) {
            return bitSource.readBits(16) | ((readBits & 31) << 16);
        }
        throw FormatException.getFormatInstance();
    }
}
