package com.google.firebase.components;

import com.google.firebase.inject.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class zzg implements ComponentContainer {
    private final List<Component<?>> zzah;
    private final Map<Class<?>, zzi<?>> zzai = new HashMap();

    public zzg(Iterable<ComponentRegistrar> iterable, Component<?>... componentArr) {
        ArrayList<Component> arrayList = new ArrayList<>();
        for (ComponentRegistrar components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, componentArr);
        HashMap hashMap = new HashMap(arrayList.size());
        for (Component component : arrayList) {
            zzh zzh = new zzh(component);
            Iterator it = component.zze().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class cls = (Class) it.next();
                    if (hashMap.put(cls, zzh) != null) {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", new Object[]{cls}));
                    }
                }
            }
        }
        for (zzh zzh2 : hashMap.values()) {
            for (Dependency dependency : zzh2.zzk().zzf()) {
                if (dependency.zzp()) {
                    zzh zzh3 = (zzh) hashMap.get(dependency.zzn());
                    if (zzh3 != null) {
                        zzh2.zza(zzh3);
                        zzh3.zzb(zzh2);
                    }
                }
            }
        }
        HashSet<zzh> hashSet = new HashSet<>(hashMap.values());
        HashSet hashSet2 = new HashSet();
        for (zzh zzh4 : hashSet) {
            if (zzh4.zzl()) {
                hashSet2.add(zzh4);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        while (!hashSet2.isEmpty()) {
            zzh zzh5 = (zzh) hashSet2.iterator().next();
            hashSet2.remove(zzh5);
            arrayList2.add(zzh5.zzk());
            for (zzh zzh6 : zzh5.zzf()) {
                zzh6.zzc(zzh5);
                if (zzh6.zzl()) {
                    hashSet2.add(zzh6);
                }
            }
        }
        if (arrayList2.size() == arrayList.size()) {
            Collections.reverse(arrayList2);
            this.zzah = Collections.unmodifiableList(arrayList2);
            for (Component component2 : this.zzah) {
                zzi zzi = new zzi(component2.zzg(), new zzl(component2.zzf(), this));
                for (Class put : component2.zze()) {
                    this.zzai.put(put, zzi);
                }
            }
            for (Component component3 : this.zzah) {
                Iterator it2 = component3.zzf().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        Dependency dependency2 = (Dependency) it2.next();
                        if (dependency2.zzo() && !this.zzai.containsKey(dependency2.zzn())) {
                            throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", new Object[]{component3, dependency2.zzn()}));
                        }
                    }
                }
            }
            return;
        }
        ArrayList arrayList3 = new ArrayList();
        for (zzh zzh7 : hashSet) {
            if (!zzh7.zzl() && !zzh7.zzm()) {
                arrayList3.add(zzh7.zzk());
            }
        }
        throw new DependencyCycleException(arrayList3);
    }

    public final Object get(Class cls) {
        return ComponentContainer$$CC.get(this, cls);
    }

    public final <T> Provider<T> getProvider(Class<T> cls) {
        zzk.zza(cls, "Null interface requested.");
        return (Provider) this.zzai.get(cls);
    }

    public final void zzb(boolean z) {
        for (Component component : this.zzah) {
            if (component.zzh() || (component.zzi() && z)) {
                get((Class) component.zze().iterator().next());
            }
        }
    }
}
