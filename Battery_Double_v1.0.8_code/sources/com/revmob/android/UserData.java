package com.revmob.android;

import com.revmob.RevMobUserGender;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserData {
    private static String addr = null;
    private static final int ageLimit = 13;
    private static Calendar birth = null;
    private static int maxAge = -1;
    private static int minAge = -1;
    private static List<String> preferenc;
    private static RevMobUserGender sex = RevMobUserGender.UNDEFINED;
    private static String url;

    public static JSONObject addUserInfo(JSONObject jSONObject) throws JSONException {
        if (minAge != -1) {
            jSONObject.put("age_range_min", minAge);
        }
        if (maxAge != -1) {
            jSONObject.put("age_range_max", maxAge);
        }
        if (minAge > 0 && maxAge < 13) {
            return jSONObject;
        }
        if (birth != null) {
            jSONObject.put("birthday", new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(birth.getTime()));
            Calendar instance = Calendar.getInstance(Locale.US);
            instance.setTime(new Date());
            Calendar instance2 = Calendar.getInstance(Locale.US);
            instance2.setTime(birth.getTime());
            instance2.add(1, 13);
            if (instance.compareTo(instance2) == -1) {
                return jSONObject;
            }
        }
        if (addr != null) {
            jSONObject.put("email", addr);
        }
        if (sex != null) {
            jSONObject.put("gender", sex.getValue());
        }
        if (url != null) {
            jSONObject.put("user_page", url);
        }
        if (preferenc != null && preferenc.size() > 0) {
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < preferenc.size(); i++) {
                jSONArray.put(preferenc.get(i));
            }
            jSONObject.put("interests", jSONArray);
        }
        return jSONObject;
    }

    public static RevMobUserGender getUserGender() {
        return sex;
    }

    public static void setUserGender(RevMobUserGender revMobUserGender) {
        sex = revMobUserGender;
    }

    public static int getUserAgeRangeMin() {
        return minAge;
    }

    public static void setUserAgeRangeMin(int i) {
        minAge = i;
    }

    public static int getUserAgeRangeMax() {
        return maxAge;
    }

    public static void setUserAgeRangeMax(int i) {
        maxAge = i;
    }

    public static Calendar getUserBirthday() {
        return birth;
    }

    public static void setUserBirthday(Calendar calendar) {
        birth = calendar;
    }

    public static String getUserPage() {
        return url;
    }

    public static void setUserPage(String str) {
        url = str;
    }

    public static List<String> getUserInterests() {
        return preferenc;
    }

    public static void setUserInterests(List<String> list) {
        preferenc = list;
    }

    public static String getEmail() {
        return addr;
    }

    public static void setEmail(String str) {
        addr = str;
    }
}
