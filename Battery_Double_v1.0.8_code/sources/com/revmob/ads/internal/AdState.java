package com.revmob.ads.internal;

public enum AdState {
    CREATED,
    LOADING,
    LOADED,
    DISPLAYED,
    HIDDEN,
    CLOSED,
    CLICKED
}
