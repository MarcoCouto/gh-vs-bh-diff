package com.revmob.ads.link;

import android.app.Activity;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.link.client.LinkClientListener;
import com.revmob.ads.link.client.LinkData;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.MarketAsyncManager;
import com.revmob.internal.RMLog;

public class RevMobLink implements Ad {
    /* access modifiers changed from: private */
    public Activity activity;
    public boolean autoopen = false;
    /* access modifiers changed from: private */
    public LinkData data;
    /* access modifiers changed from: private */
    public RevMobAdsListener publisherListener;
    private AdState state;

    public RevMobLink(Activity activity2, RevMobAdsListener revMobAdsListener) {
        this.activity = activity2;
        this.publisherListener = revMobAdsListener;
        this.state = AdState.CREATED;
    }

    public void load() {
        load(null);
    }

    public void load(String str) {
        String str2;
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Loading Link ");
                sb.append(str);
                str2 = sb.toString();
            } else {
                str2 = "Loading Link";
            }
            RMLog.i(str2);
            RevMobContext.getRunningApps = true;
            RevMobClient.getInstance().fetchAdLink(str, RevMobContext.toPayload(this.activity), new LinkClientListener(this, this.publisherListener));
        }
    }

    public void updateWithData(AdData adData) {
        this.state = AdState.LOADED;
        this.data = (LinkData) adData;
        StringBuilder sb = new StringBuilder();
        sb.append("Link loaded - ");
        sb.append(this.data.getCampaignId());
        RMLog.i(sb.toString());
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdReceived();
        }
        if (this.autoopen) {
            open();
        }
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void cancel() {
        this.autoopen = false;
    }

    public void open() {
        this.autoopen = true;
        if (isLoaded() && this.state != AdState.DISPLAYED) {
            this.state = AdState.DISPLAYED;
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdDisplayed();
            }
            RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
            this.activity.runOnUiThread(new Runnable() {
                public void run() {
                    new MarketAsyncManager(RevMobLink.this.activity, RevMobLink.this.data, RevMobLink.this.publisherListener).execute(new Void[0]);
                }
            });
        } else if (this.state != AdState.CREATED && this.state != AdState.CLOSED) {
            RMLog.i(Ad.SHOWING_TOO_SOON_MSG);
        }
    }
}
