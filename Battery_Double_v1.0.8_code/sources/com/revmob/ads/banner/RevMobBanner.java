package com.revmob.ads.banner;

import android.app.Activity;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.banner.client.BannerClientListener;
import com.revmob.ads.banner.client.BannerData;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.internal.CloseAnimationConfiguration;
import com.revmob.ads.internal.ShowAnimationConfiguration;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.MarketAsyncManager;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobSoundPlayer;
import com.revmob.internal.RevMobWebView;
import com.revmob.internal.RevMobWebViewClient;
import com.revmob.internal.RevMobWebViewClient.RevMobWebViewClickListener;
import java.io.IOException;
import java.util.ArrayList;

public class RevMobBanner extends RelativeLayout implements Ad {
    public static final int DEFAULT_HEIGHT_IN_DIP = 50;
    public static int DEFAULT_WIDTH_IN_DIP = 320;
    public static final int MAXIMUM_HEIGHT_IN_DIP = 60;
    public static final int MINIMUM_HEIGHT_IN_DIP = 40;
    public static int bannerCount = 0;
    public static boolean isBannerImpression = false;
    public static ArrayList<String> usedCampaigns;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public View adView;
    /* access modifiers changed from: private */
    public View adView2;
    public boolean changedBannerParams;
    private boolean containerTooSmall = false;
    private int customHeight = 0;
    private int customWidth = 0;
    /* access modifiers changed from: private */
    public BannerData data;
    private int height = 50;
    private DisplayMetrics metrics = new DisplayMetrics();
    /* access modifiers changed from: private */
    public OnClickListener onClickListener;
    /* access modifiers changed from: private */
    public String placementId;
    /* access modifiers changed from: private */
    public RevMobAdsListener publisherListener;
    /* access modifiers changed from: private */
    public AdState state;
    private int width = DEFAULT_WIDTH_IN_DIP;

    public RevMobBanner(Activity activity2, RevMobAdsListener revMobAdsListener) {
        super(activity2);
        this.activity = activity2;
        this.publisherListener = revMobAdsListener;
        this.state = AdState.CREATED;
        isBannerImpression = false;
    }

    private OnClickListener onClickListener() {
        return new OnClickListener() {
            public void onClick(View view) {
                RevMobBanner.this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        RevMobBanner.this.state = AdState.CLICKED;
                        new MarketAsyncManager(RevMobBanner.this.activity, RevMobBanner.this.data, RevMobBanner.this.publisherListener).execute(new Void[0]);
                        RevMobBanner.this.hide();
                        RevMobBanner.this.load(RevMobBanner.this.placementId);
                    }
                });
            }
        };
    }

    public void setChangeBannerParams(boolean z) {
        this.changedBannerParams = z;
    }

    public static void setBannerImpression(boolean z) {
        isBannerImpression = z;
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void load() {
        load(null);
    }

    public void load(String str) {
        String str2;
        this.placementId = str;
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED || this.state == AdState.CLICKED) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Loading Banner ");
                sb.append(str);
                str2 = sb.toString();
            } else {
                str2 = "Loading Banner";
            }
            RMLog.i(str2);
            RevMobContext.getRunningApps = true;
            RevMobClient.getInstance().fetchBanner(str, RevMobContext.toPayload(this.activity), new BannerClientListener(this, this.publisherListener));
        }
    }

    public void updateWithData(AdData adData) {
        if (this.state == AdState.HIDDEN) {
            RMLog.d("state == hidden");
            return;
        }
        this.state = AdState.LOADED;
        this.data = (BannerData) adData;
        StringBuilder sb = new StringBuilder();
        sb.append("Banner loaded - ");
        sb.append(this.data.getCampaignId());
        RMLog.i(sb.toString());
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdReceived();
        }
        this.onClickListener = onClickListener();
        setOnClickListener(this.onClickListener);
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {
                if (RevMobBanner.this.adView == null && RevMobBanner.this.adView2 == null) {
                    RevMobBanner.bannerCount = 0;
                    RevMobBanner.usedCampaigns = null;
                    if (RevMobBanner.this.adView == null) {
                        RevMobBanner.this.addAdView(true);
                    } else {
                        RevMobBanner.this.addAdView(false);
                    }
                } else {
                    RevMobBanner.this.hide(true);
                }
            }
        });
        final ViewParent parent = getParent();
        if (parent != null && this.data.getRefreshTime() > 0) {
            new Handler(this.activity.getApplicationContext().getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (RevMobBanner.this.state != AdState.HIDDEN && parent != null) {
                        RevMobBanner.this.state = AdState.CREATED;
                        RevMobBanner.this.load(RevMobBanner.this.placementId);
                    }
                }
            }, (long) this.data.getRefreshTime());
        }
    }

    public void addAdView(boolean z) {
        if (this.data.isHtmlBanner()) {
            addHtmlAdView(z);
        } else if (this.data.isDspBanner()) {
            addDSPAdView(z);
        } else {
            addImageView(z);
        }
    }

    private void addHtmlAdView(final boolean z) {
        RMLog.d("html adview");
        RevMobWebViewClient revMobWebViewClient = new RevMobWebViewClient(this.publisherListener, new RevMobWebViewClickListener() {
            public void handlePageFinished(WebView webView, String str) {
            }

            public boolean handleClick(WebView webView, String str) {
                if (str.endsWith("#click")) {
                    if (z) {
                        RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView);
                    } else {
                        RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView2);
                    }
                }
                return true;
            }
        });
        if (z) {
            this.adView = new RevMobWebView(getContext(), this.data.getHtmlAdUrl(), null, revMobWebViewClient);
            this.adView.setBackgroundColor(0);
        } else {
            this.adView2 = new RevMobWebView(getContext(), this.data.getHtmlAdUrl(), null, revMobWebViewClient);
            this.adView2.setBackgroundColor(0);
        }
        configureAndAnimateView(z);
    }

    private void addDSPAdView(final boolean z) {
        RMLog.d("dspadview");
        RevMobWebViewClient revMobWebViewClient = new RevMobWebViewClient(this.publisherListener, new RevMobWebViewClickListener() {
            public void handlePageFinished(WebView webView, String str) {
            }

            public boolean handleClick(WebView webView, String str) {
                RevMobBanner.this.data.setDspUrl(str);
                if (z) {
                    RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView);
                } else {
                    RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView2);
                }
                return true;
            }
        });
        if (z) {
            this.adView = new RevMobWebView(getContext(), this.data.getDspUrl(), this.data.getDspHtml(), revMobWebViewClient);
            this.adView.setBackgroundColor(0);
        } else {
            this.adView2 = new RevMobWebView(getContext(), this.data.getDspUrl(), this.data.getDspHtml(), revMobWebViewClient);
            this.adView2.setBackgroundColor(0);
        }
        configureAndAnimateView(z);
    }

    private void addImageView(boolean z) {
        RMLog.d("banner imageview");
        if (z) {
            this.adView = new ImageView(getContext());
            ((ImageView) this.adView).setImageBitmap(this.data.getDrawable());
            if (getParent() != null) {
                ((View) getParent()).setVisibility(0);
            }
            ((ImageView) this.adView).setScaleType(ScaleType.FIT_XY);
            this.adView.setOnClickListener(this.onClickListener);
        } else {
            this.adView2 = new ImageView(getContext());
            ((ImageView) this.adView2).setImageBitmap(this.data.getDrawable());
            if (getParent() != null) {
                ((View) getParent()).setVisibility(0);
            }
            ((ImageView) this.adView2).setScaleType(ScaleType.FIT_XY);
            this.adView2.setOnClickListener(this.onClickListener);
        }
        configureAndAnimateView(z);
    }

    private void configureAndAnimateView(boolean z) {
        Animation animation;
        calculateDimensions();
        if (!this.containerTooSmall) {
            LayoutParams layoutParams = new LayoutParams(this.customWidth == 0 ? this.width : this.customWidth, this.customHeight == 0 ? this.height : this.customHeight);
            layoutParams.addRule(14);
            if (this.data.getRefreshTime() > 0) {
                ShowAnimationConfiguration showAnimationConfiguration = new ShowAnimationConfiguration();
                showAnimationConfiguration.addAnimation("slide_up");
                showAnimationConfiguration.setTime(500);
                animation = showAnimationConfiguration.getAnimation();
            } else {
                animation = this.data.getShowAnimation();
            }
            if (z) {
                this.adView.setLayoutParams(layoutParams);
                this.adView.setAnimation(animation);
                addView(this.adView);
            } else {
                this.adView2.setLayoutParams(layoutParams);
                this.adView2.setAnimation(animation);
                addView(this.adView2);
            }
            playSoundOnShow();
            reportShowOrHidden();
        }
    }

    public void setCustomSize(int i, int i2) {
        this.customWidth = i;
        this.customHeight = i2;
        LayoutParams layoutParams = new LayoutParams(this.customWidth, this.customHeight);
        if (this.adView != null) {
            this.adView.setLayoutParams(layoutParams);
        } else if (this.adView2 != null) {
            this.adView.setLayoutParams(layoutParams);
        }
    }

    public int dipToPixels(int i) {
        return (int) ((((float) i) * this.metrics.density) + 0.5f);
    }

    private void calculateDimensions() {
        View view = (View) getParent();
        int width2 = ((Activity) getContext()).getWindowManager().getDefaultDisplay().getWidth();
        int height2 = ((Activity) getContext()).getWindowManager().getDefaultDisplay().getHeight();
        this.activity.getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
        if (width2 < height2) {
            DEFAULT_WIDTH_IN_DIP = (int) (((float) width2) / this.metrics.density);
        } else {
            DEFAULT_WIDTH_IN_DIP = (int) (((float) height2) / this.metrics.density);
        }
        int dipToPixels = dipToPixels(50);
        if (!(view == null || view.getHeight() == 0 || view.getHeight() >= dipToPixels(40))) {
            this.containerTooSmall = true;
            RMLog.i("The container height must be at least 40dp");
        }
        if (!(view == null || view.getHeight() == 0)) {
            dipToPixels = view.getHeight();
        }
        this.height = dipToPixels;
        int i = (this.height * DEFAULT_WIDTH_IN_DIP) / 50;
        if (i >= width2 || i >= height2) {
            i = Math.min(height2, width2);
            int i2 = i * 50;
            this.height = i2 / DEFAULT_WIDTH_IN_DIP > dipToPixels(60) ? dipToPixels(60) : i2 / DEFAULT_WIDTH_IN_DIP;
        }
        if (!(view == null || view.getWidth() == 0)) {
            i = Math.min(view.getWidth(), i);
        }
        this.width = i;
        if (view != null && view.getWidth() != 0) {
            if (view.getWidth() < (DEFAULT_WIDTH_IN_DIP * 40) / 50) {
                this.containerTooSmall = true;
                StringBuilder sb = new StringBuilder();
                sb.append("The container width must be at least ");
                sb.append((40 * DEFAULT_WIDTH_IN_DIP) / 50);
                sb.append("dp");
                RMLog.i(sb.toString());
            } else if (this.width == view.getWidth()) {
                this.height = (this.width * 50) / DEFAULT_WIDTH_IN_DIP;
            }
        }
    }

    public void hide() {
        bannerCount = 0;
        usedCampaigns = null;
        hide(false);
    }

    public void hide(final boolean z) {
        Animation animation;
        final boolean z2 = this.adView != null;
        if (this.data == null || this.data.getRefreshTime() != 0) {
            CloseAnimationConfiguration closeAnimationConfiguration = new CloseAnimationConfiguration();
            closeAnimationConfiguration.addAnimation("slide_up");
            closeAnimationConfiguration.setTime(500);
            animation = closeAnimationConfiguration.getAnimation();
        } else {
            animation = this.data.getCloseAnimation();
        }
        animation.setAnimationListener(new AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (z2) {
                    RevMobBanner.this.removeAdViewBlock();
                } else {
                    RevMobBanner.this.removeAdView2Block();
                }
                if (!z) {
                    RevMobBanner.this.state = AdState.HIDDEN;
                }
            }
        });
        if (this.adView != null) {
            if (z) {
                addAdView(false);
            }
            this.adView.startAnimation(animation);
        } else if (z) {
            addAdView(true);
            this.adView2.startAnimation(animation);
        }
    }

    /* access modifiers changed from: private */
    public void removeAdViewBlock() {
        if (this.adView != null) {
            this.adView.setVisibility(4);
            new Handler(this.activity.getApplicationContext().getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (RevMobBanner.this.adView != null) {
                        ViewGroup viewGroup = (ViewGroup) RevMobBanner.this.adView.getParent();
                        if (viewGroup != null) {
                            viewGroup.removeView(RevMobBanner.this.adView);
                        }
                        RevMobBanner.this.adView = null;
                    }
                }
            }, 1000);
        }
    }

    /* access modifiers changed from: private */
    public void removeAdView2Block() {
        if (this.adView2 != null) {
            this.adView2.setVisibility(4);
            new Handler(this.activity.getApplicationContext().getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (RevMobBanner.this.adView2 != null) {
                        ViewGroup viewGroup = (ViewGroup) RevMobBanner.this.adView2.getParent();
                        if (viewGroup != null) {
                            viewGroup.removeView(RevMobBanner.this.adView2);
                        }
                        RevMobBanner.this.adView2 = null;
                    }
                }
            }, 1000);
        }
    }

    public void reportShowOrHidden() {
        if (!isShown()) {
            this.state = AdState.HIDDEN;
        } else if (isLoaded() && this.state != AdState.DISPLAYED) {
            this.state = AdState.DISPLAYED;
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdDisplayed();
            }
            if (usedCampaigns == null) {
                usedCampaigns = new ArrayList<>();
            }
            if (!usedCampaigns.contains(this.data.getCampaignId())) {
                usedCampaigns.add(this.data.getCampaignId());
            }
            bannerCount++;
            isBannerImpression = true;
            RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
        }
    }

    private void playSoundOnShow() {
        if (this.data.getShowSoundURL() != null && this.data.getShowSoundURL().length() != 0) {
            try {
                new RevMobSoundPlayer().playBannerSound(this.activity, this.data.getShowSoundURL());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
