package com.revmob;

import com.mansoon.BatteryDouble.Config;

public enum RevMobParallaxMode {
    DEFAULT("enabled"),
    DISABLED(Config.IMPORTANCE_DISABLED);
    
    private String value;

    private RevMobParallaxMode(String str) {
        this.value = str;
    }

    public String getValue() {
        return this.value;
    }
}
