package com.revmob.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.revmob.android.RevMobContext;
import com.revmob.android.RevMobContext.RUNNING_APPS_STATUS;
import org.json.JSONObject;

public class RevMobServicesManager extends BroadcastReceiver {
    public static JSONObject config;
    public static Context context;

    public static Context getMainContext() {
        return context;
    }

    public static void initialize(Context context2, JSONObject jSONObject) {
        context = context2;
        config = jSONObject;
        setServiceStatus();
        Editor edit = PreferenceManager.getDefaultSharedPreferences(context2).edit();
        edit.putString("runningAppsConfiguration", jSONObject.toString());
        edit.putString("deviceIdentifierJSON", RevMobContext.deviceIdentifierJSON().toString());
        edit.apply();
        context2.startService(new Intent(context, RevMobServices.class));
    }

    public void onReceive(Context context2, Intent intent) {
        context2.startService(new Intent(context2, RevMobServices.class));
    }

    private static void setServiceStatus() {
        int optInt = config.optInt("status");
        RevMobContext.runningAppsStatus = RUNNING_APPS_STATUS.NOTIFY_AND_FETCH;
        if (optInt >= 0 && optInt <= 2) {
            RevMobContext.runningAppsStatus = RUNNING_APPS_STATUS.values()[optInt];
        }
        if (optInt == 0) {
            context.stopService(new Intent(context, RevMobServices.class));
        }
    }
}
