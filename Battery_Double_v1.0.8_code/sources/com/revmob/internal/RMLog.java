package com.revmob.internal;

import android.util.Log;

public class RMLog {
    private static final String PREFIX = "[RevMob]";

    public static void d(String str) {
        Log.d(PREFIX, str);
    }

    public static void d(String str, Throwable th) {
        Log.d(PREFIX, str, th);
    }

    public static void i(String str) {
        Log.i(PREFIX, str);
    }

    public static void i(String str, Throwable th) {
        Log.d(PREFIX, str, th);
    }

    public static void w(String str) {
        Log.w(PREFIX, str);
    }

    public static void w(String str, Throwable th) {
        Log.d(PREFIX, str, th);
    }

    public static void e(String str) {
        Log.e(PREFIX, str);
    }

    public static void e(String str, Throwable th) {
        Log.d(PREFIX, str, th);
    }
}
