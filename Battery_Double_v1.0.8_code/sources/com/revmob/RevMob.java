package com.revmob;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.revmob.ads.banner.RevMobBanner;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.revmob.ads.link.RevMobLink;
import com.revmob.ads.popup.RevMobPopup;
import com.revmob.android.RevMobContext;
import com.revmob.android.RevMobScreen;
import com.revmob.android.StoredData;
import com.revmob.android.UserData;
import com.revmob.client.RevMobClient;
import com.revmob.client.SessionClientListener;
import com.revmob.internal.AndroidHelper;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobEula;
import java.util.Calendar;
import java.util.List;

public class RevMob {
    public static boolean adIsLoaded = false;
    /* access modifiers changed from: private */
    public static Activity bannerActivity = null;
    /* access modifiers changed from: private */
    public static RevMobBanner bannerAd = null;
    /* access modifiers changed from: private */
    public static RelativeLayout bannerLayout = null;
    /* access modifiers changed from: private */
    public static LayoutParams bannerParams = null;
    private static RevMobEula eula = null;
    private static SessionClientListener listener = null;
    private static RevMobAdsListener revmobListener = null;
    public static boolean rewardedVideoIsLoaded = false;
    protected static RevMob session = null;
    public static boolean videoIsLoaded = false;
    private boolean didSetBannerParams = false;

    private static void validateActivity(Activity activity) {
        if (activity == null) {
            throw new RuntimeException("RevMob: Activity must not be a null value.");
        }
    }

    private static void validatePermissions(Activity activity) {
        if (!AndroidHelper.isPermissionEnabled(activity, "INTERNET")) {
            RMLog.e(String.format("Permission %s is required. Add it to your AndroidManifest.xml file", new Object[]{"INTERNET"}));
        }
    }

    private static void validateFullscreenActivity(Activity activity) {
        if (!FullscreenActivity.isFullscreenActivityAvailable(activity).booleanValue()) {
            RMLog.e("You must declare the RevMob FullscreenActivity in the AndroidManifest.xml file");
        }
    }

    @Deprecated
    public static RevMob start(Activity activity, String str) {
        if (session == null) {
            validatePermissions(activity);
            validateActivity(activity);
            validateFullscreenActivity(activity);
            session = new RevMob(activity, str);
        }
        return session;
    }

    public static RevMob start(Activity activity) {
        if (session == null) {
            try {
                return start(activity, activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.getString("com.revmob.app.id"));
            } catch (NameNotFoundException unused) {
                throw new RuntimeException("You must put the revmob.app.id value in the AndroidManifest.xml file.");
            }
        } else {
            if (eula != null) {
                eula.loadAndShow();
            }
            return session;
        }
    }

    public static RevMob startWithListener(Activity activity, RevMobAdsListener revMobAdsListener) {
        if (session == null) {
            try {
                String string = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.getString("com.revmob.app.id");
                revmobListener = revMobAdsListener;
                return start(activity, string);
            } catch (NameNotFoundException unused) {
                throw new RuntimeException("You must the revmob.app.id value in the AndroidManifest.xml file.");
            }
        } else {
            if (eula != null) {
                eula.loadAndShow();
            }
            return session;
        }
    }

    public static RevMob startWithListener(Activity activity, RevMobAdsListener revMobAdsListener, String str, int i) {
        if (str != null) {
            RevMobClient.setProductionAdress(str, i);
        }
        return startWithListener(activity, revMobAdsListener);
    }

    public static RevMob startWithListenerForWrapper(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        if (session == null) {
            revmobListener = revMobAdsListener;
            return start(activity, str);
        }
        if (eula != null) {
            eula.loadAndShow();
        }
        return session;
    }

    public static RevMob session() {
        if (session == null) {
            RMLog.w(RevMobClient.SESSION_WARNING);
        }
        return session;
    }

    protected RevMob(Activity activity, String str) {
        validateActivity(activity);
        boolean z = !new StoredData(activity).isAlreadyTracked();
        HTTPHelper.setUserAgent(null, activity);
        HTTPHelper.setEulaUrl(null, activity);
        HTTPHelper.setEulaVersion(null, activity);
        HTTPHelper.setShouldShowEula(false, activity);
        listener = new SessionClientListener(activity, z, revmobListener);
        RevMobContext.loadAdvertisingInfo(str, listener, revmobListener, activity);
        RevMobScreen.load(activity);
    }

    public void acceptAndDismissEula() {
        eula = listener.getEulaPopup();
        eula.acceptAndDismiss();
    }

    public void rejectEula() {
        eula = listener.getEulaPopup();
        eula.reject();
    }

    public void showFullscreen(Activity activity) {
        returnFullScreen(activity, null, null, 0).setAutoShow(true);
    }

    public void showFullscreen(Activity activity, String str) {
        returnFullScreen(activity, str, null, 0).setAutoShow(true);
    }

    public void showFullscreen(Activity activity, RevMobAdsListener revMobAdsListener) {
        returnFullScreen(activity, null, revMobAdsListener, 0).setAutoShow(true);
    }

    public void showFullscreen(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        returnFullScreen(activity, str, revMobAdsListener, 0).setAutoShow(true);
    }

    public RevMobFullscreen createFullscreen(Activity activity, RevMobAdsListener revMobAdsListener) {
        return returnFullScreen(activity, null, revMobAdsListener, 2);
    }

    public boolean isAdLoaded() {
        return adIsLoaded;
    }

    public RevMobFullscreen createVideo(Activity activity, RevMobAdsListener revMobAdsListener) {
        return returnVideo(activity, null, revMobAdsListener);
    }

    public RevMobFullscreen createVideo(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        return returnVideo(activity, str, revMobAdsListener);
    }

    public boolean isVideoLoaded() {
        return videoIsLoaded;
    }

    public RevMobFullscreen createRewardedVideo(Activity activity, RevMobAdsListener revMobAdsListener) {
        return returnRewardedVideo(activity, null, revMobAdsListener);
    }

    public RevMobFullscreen createRewardedVideo(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        return returnRewardedVideo(activity, str, revMobAdsListener);
    }

    public boolean isRewardedVideoLoaded() {
        return rewardedVideoIsLoaded;
    }

    public RevMobFullscreen createFullscreen(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        return returnFullScreen(activity, str, revMobAdsListener, 2);
    }

    private RevMobFullscreen returnFullScreen(Activity activity, String str, RevMobAdsListener revMobAdsListener, int i) {
        validateActivity(activity);
        RevMobFullscreen revMobFullscreen = new RevMobFullscreen(activity, revMobAdsListener);
        revMobFullscreen.loadFullscreen(str, i);
        return revMobFullscreen;
    }

    private RevMobFullscreen returnVideo(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        validateActivity(activity);
        RevMobFullscreen revMobFullscreen = new RevMobFullscreen(activity, revMobAdsListener);
        revMobFullscreen.loadVideo(str);
        return revMobFullscreen;
    }

    private RevMobFullscreen returnRewardedVideo(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        validateActivity(activity);
        RevMobFullscreen revMobFullscreen = new RevMobFullscreen(activity, revMobAdsListener);
        revMobFullscreen.loadRewardedVideo(str);
        return revMobFullscreen;
    }

    public RevMobBanner createBanner(Activity activity) {
        return createBanner(activity, null, null);
    }

    public RevMobBanner createBanner(Activity activity, RevMobAdsListener revMobAdsListener) {
        return createBanner(activity, null, revMobAdsListener);
    }

    public RevMobBanner createBanner(Activity activity, String str) {
        return createBanner(activity, str, null);
    }

    public RevMobBanner createBanner(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        validateActivity(activity);
        RevMobBanner revMobBanner = new RevMobBanner(activity, revMobAdsListener);
        revMobBanner.setChangeBannerParams(false);
        revMobBanner.load(str);
        return revMobBanner;
    }

    public void releaseBanner(Activity activity) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    RevMob.bannerLayout.removeView(RevMob.bannerAd);
                    RevMob.bannerLayout.setVisibility(8);
                    RevMob.bannerLayout = null;
                    RevMob.bannerAd = null;
                    RevMob.bannerActivity = null;
                } catch (Exception e) {
                    RMLog.e(e.toString());
                }
            }
        });
    }

    public void hideBanner(Activity activity) {
        releaseBanner(activity);
    }

    public RelativeLayout showBanner(Activity activity) {
        return showBanner(activity, null, null);
    }

    public RelativeLayout showBanner(Activity activity, int i) {
        return showBanner(activity, i, null, null);
    }

    public RelativeLayout showBanner(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        return showBanner(activity, 80, str, revMobAdsListener);
    }

    public RelativeLayout showBanner(Activity activity, int i, String str, RevMobAdsListener revMobAdsListener) {
        return showBanner(activity, i, 0, 0, AndroidHelper.dipToPixels(activity, RevMobBanner.DEFAULT_WIDTH_IN_DIP), AndroidHelper.dipToPixels(activity, 50), str, revMobAdsListener);
    }

    public RelativeLayout showBanner(Activity activity, int i, int i2, int i3, int i4, int i5) {
        return showBanner(activity, i, i2, i3, i4, i5, null, null);
    }

    public RelativeLayout showBanner(Activity activity, int i, int i2, int i3, int i4, int i5, String str, RevMobAdsListener revMobAdsListener) {
        RelativeLayout preloadBanner = preloadBanner(activity, i, i2, i3, i4, i5, str, revMobAdsListener);
        showLoadedBanner();
        return preloadBanner;
    }

    public RelativeLayout preloadBanner(Activity activity, int i, int i2, int i3, int i4, int i5, String str, RevMobAdsListener revMobAdsListener) {
        final Activity activity2 = activity;
        final String str2 = str;
        final RevMobAdsListener revMobAdsListener2 = revMobAdsListener;
        final int i6 = i4;
        final int i7 = i5;
        final int i8 = i2;
        final int i9 = i3;
        final int i10 = i;
        AnonymousClass2 r0 = new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:17:0x008a A[Catch:{ Exception -> 0x00b0 }] */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0092 A[Catch:{ Exception -> 0x00b0 }] */
            public void run() {
                try {
                    if (RevMob.bannerAd != null) {
                        RevMob.this.releaseBanner(activity2);
                    }
                    RevMob.bannerAd = RevMob.this.createBanner(activity2, str2, revMobAdsListener2);
                    RevMob.bannerLayout = new RelativeLayout(activity2.getApplicationContext());
                    RevMob.bannerParams = new LayoutParams(i6, i7);
                    if (i8 == 0 && i9 == 0 && i6 == AndroidHelper.dipToPixels(activity2, RevMobBanner.DEFAULT_WIDTH_IN_DIP)) {
                        if (i7 == AndroidHelper.dipToPixels(activity2, 50)) {
                            RevMob.bannerParams.addRule(14);
                            if (i10 != 48) {
                                RevMob.bannerLayout.setGravity(48);
                            } else {
                                RevMob.bannerLayout.setGravity(80);
                            }
                            activity2.addContentView(RevMob.bannerLayout, new ViewGroup.LayoutParams(-1, -1));
                            RevMob.bannerActivity = activity2;
                        }
                    }
                    RevMob.bannerParams.leftMargin = i8;
                    RevMob.bannerParams.topMargin = i9;
                    RevMob.bannerAd.setChangeBannerParams(true);
                    RevMob.bannerAd.setCustomSize(i6, i7);
                    if (i10 != 48) {
                    }
                    activity2.addContentView(RevMob.bannerLayout, new ViewGroup.LayoutParams(-1, -1));
                    RevMob.bannerActivity = activity2;
                } catch (Exception e) {
                    RMLog.e(e.toString());
                }
            }
        };
        activity.runOnUiThread(r0);
        return bannerLayout;
    }

    public boolean showLoadedBanner() {
        if (bannerActivity == null) {
            return false;
        }
        hideLoadedBanner();
        if (bannerLayout == null || bannerAd == null || bannerParams == null) {
            return false;
        }
        bannerActivity.runOnUiThread(new Runnable() {
            public void run() {
                RevMob.bannerLayout.addView(RevMob.bannerAd, RevMob.bannerParams);
            }
        });
        return true;
    }

    public boolean hideLoadedBanner() {
        if (bannerActivity == null) {
            return false;
        }
        bannerActivity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    RevMob.bannerAd.hide();
                } catch (Exception unused) {
                }
                RevMob.bannerLayout.removeView(RevMob.bannerAd);
            }
        });
        return true;
    }

    public void releaseLoadedBanner() {
        if (bannerActivity != null) {
            hideLoadedBanner();
            releaseBanner(bannerActivity);
        }
    }

    public void openLink(Activity activity, RevMobAdsListener revMobAdsListener) {
        createLink(activity, null, revMobAdsListener).open();
    }

    public void openLink(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        createLink(activity, str, revMobAdsListener).open();
    }

    public RevMobLink createLink(Activity activity, RevMobAdsListener revMobAdsListener) {
        return createLink(activity, null, revMobAdsListener);
    }

    public RevMobLink createLink(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        validateActivity(activity);
        RevMobLink revMobLink = new RevMobLink(activity, revMobAdsListener);
        revMobLink.load(str);
        return revMobLink;
    }

    public void showPopup(Activity activity) {
        createPopup(activity, null, null).show();
    }

    public void showPopup(Activity activity, String str) {
        createPopup(activity, str, null).show();
    }

    public void showPopup(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        createPopup(activity, null, revMobAdsListener).show();
    }

    public RevMobPopup createPopup(Activity activity, RevMobAdsListener revMobAdsListener) {
        return createPopup(activity, null, revMobAdsListener);
    }

    public RevMobPopup createPopup(Activity activity, String str, RevMobAdsListener revMobAdsListener) {
        validateActivity(activity);
        RevMobPopup revMobPopup = new RevMobPopup(activity, revMobAdsListener);
        revMobPopup.load(str);
        return revMobPopup;
    }

    public void printEnvironmentInformation(Activity activity) {
        validateActivity(activity);
        RevMobContext.printEnvironmentInformation(RevMobClient.getInstance().getAppId(), activity);
    }

    public RevMobTestingMode getTestingMode() {
        return RevMobClient.getInstance().getTestingMode();
    }

    public void setTestingMode(RevMobTestingMode revMobTestingMode) {
        RevMobClient.getInstance().setTestingMode(revMobTestingMode);
    }

    public RevMobParallaxMode getParallaxMode() {
        return RevMobClient.getInstance().getParallaxMode();
    }

    public void setParallaxMode(RevMobParallaxMode revMobParallaxMode) {
        RevMobClient.getInstance().setParallaxMode(revMobParallaxMode);
    }

    public void setTimeoutInSeconds(int i) {
        RevMobClient.getInstance().setTimeoutInSeconds(i);
    }

    public void setUserEmail(String str) {
        UserData.setEmail(str);
    }

    public RevMobUserGender getUserGender() {
        return UserData.getUserGender();
    }

    public void setUserGender(RevMobUserGender revMobUserGender) {
        UserData.setUserGender(revMobUserGender);
    }

    public int getUserAgeRangeMin() {
        return UserData.getUserAgeRangeMin();
    }

    public void setUserAgeRangeMin(int i) {
        UserData.setUserAgeRangeMin(i);
    }

    public int getUserAgeRangeMax() {
        return UserData.getUserAgeRangeMax();
    }

    public void setUserAgeRangeMax(int i) {
        UserData.setUserAgeRangeMax(i);
    }

    public Calendar getUserBirthday() {
        return UserData.getUserBirthday();
    }

    public void setUserBirthday(Calendar calendar) {
        UserData.setUserBirthday(calendar);
    }

    public String getUserPage() {
        return UserData.getUserPage();
    }

    public void setUserPage(String str) {
        UserData.setUserPage(str);
    }

    public List<String> getUserInterests() {
        return UserData.getUserInterests();
    }

    public void setUserInterests(List<String> list) {
        UserData.setUserInterests(list);
    }
}
