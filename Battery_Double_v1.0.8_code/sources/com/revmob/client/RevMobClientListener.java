package com.revmob.client;

import org.json.JSONException;

public interface RevMobClientListener {
    void handleError(String str);

    void handleResponse(String str) throws JSONException;
}
