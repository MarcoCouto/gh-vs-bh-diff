package com.revmob.client;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.SparseArray;
import com.applovin.sdk.AppLovinErrorCodes;
import com.revmob.RevMobAdsListener;
import com.revmob.RevMobParallaxMode;
import com.revmob.RevMobTestingMode;
import com.revmob.android.RevMobContext;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobEncryption;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.json.JSONException;

public class RevMobClient {
    private static final String ADS_PATH = "/api/v4/mobile_apps/%s/%s/fetch_only.json";
    private static final String BAD_RESPONSE_MESSAGE = "Bad response from server.";
    private static final String DEFAULT_DOWNLOAD_ERROR_MESSAGE = "RevMob did not answered as expected.";
    private static final int DEFAULT_TIMEOUT = 30;
    private static final String INSTALL_URL = "/api/v4/mobile_apps/%s/install.json";
    private static final String INVALID_APP_ID = "Invalid App ID.";
    private static final String INVALID_TIMEOUT = "Invalid timeout.";
    private static final SparseArray<String> LOG_MESSAGES = new SparseArray<>();
    private static final String NO_MEMORY_MESSAGE = "It was not possible to load the RevMob banner because your device run out of RAM memory.";
    private static final String ONE_APP_ID_PER_APP = "You can use just one App Id per application.";
    private static final String PARSE_ERROR_MESSAGE = "Error on parse response from server.";
    private static final String PARSE_ERROR_MESSAGE2 = "Error on parse response from server. Unknown error.";
    private static final String PLACEMENT_ADS_PATH = "/api/v4/mobile_apps/%s/placements/%s/%s/fetch_only.json";
    private static String PRODUCTION_SERVER_ADDRESS = "https://android.revmob.com";
    public static String SDK_NAME = "android";
    public static String SDK_SOURCE_NAME = "android";
    public static String SDK_SOURCE_VERSION = "9.0.4";
    public static String SDK_VERSION = "9.0.4";
    private static final String SESSIONS_PATH = "/api/v4/mobile_apps/%s/sessions.json";
    public static final String SESSION_WARNING = "Call RevMobAds.start(activity, APP_ID) on application start/resume. It will help us to improve tracking and increase the eCPM.";
    private static RevMobClient instance = new RevMobClient();
    private static final Map<String, String> serverEndPoints = new HashMap();
    public static long t0;
    public static long t1;
    public static long t2;
    public static long t3;
    private String appId;
    private RevMobParallaxMode parallaxMode = RevMobParallaxMode.DISABLED;
    private RevMobAdsListener publisherListener;
    private boolean reportingImpression = false;
    private boolean requestInstalledApps = false;
    private boolean requestIsStartSession = false;
    private boolean sessionStarted = false;
    private RevMobTestingMode testingMode = RevMobTestingMode.DISABLED;

    static {
        LOG_MESSAGES.put(Callback.DEFAULT_DRAG_ANIMATION_DURATION, "OK.");
        LOG_MESSAGES.put(202, "OK.");
        LOG_MESSAGES.put(AppLovinErrorCodes.NO_FILL, "Ad retrieval failed: No ads for this device/country right now or your App ID is paused.");
        LOG_MESSAGES.put(404, "No ad retrieved: did you set a valid App ID? Get one at http://revmob.com.");
        LOG_MESSAGES.put(409, "No ad retrieved: did you set a valid Placement ID? Get one at http://revmob.com.");
        LOG_MESSAGES.put(422, "Request requirements did not met. Did you set required permissions?");
        LOG_MESSAGES.put(423, "Is your ad unit paused? Please, check it in the RevMob Console.");
        LOG_MESSAGES.put(500, "Unexpected server error.");
        LOG_MESSAGES.put(503, "Unexpected server error.");
    }

    public static RevMobClient getInstance() {
        return instance;
    }

    public static void setProductionAdress(String str, int i) {
        if (i == 714823364) {
            PRODUCTION_SERVER_ADDRESS = str;
        }
    }

    public boolean startSession(String str, String str2, RevMobClientListener revMobClientListener, RevMobAdsListener revMobAdsListener) {
        if (this.appId != null) {
            RMLog.w(ONE_APP_ID_PER_APP);
        } else if (isAppIdValid(str)) {
            this.requestIsStartSession = true;
            this.sessionStarted = true;
            this.appId = str;
            this.publisherListener = revMobAdsListener;
            StringBuilder sb = new StringBuilder();
            sb.append(PRODUCTION_SERVER_ADDRESS);
            sb.append(String.format(SESSIONS_PATH, new Object[]{str}));
            serverRequest(sb.toString(), str2, revMobClientListener);
            HTTPHelper.globalTimeoutInSeconds = 30;
            return true;
        } else {
            RMLog.w(INVALID_APP_ID);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void addServerEndPoint(String str, String str2) {
        if (str != null && str2 != null) {
            serverEndPoints.put(str, str2);
        }
    }

    public void registerInstall(String str, RevMobClientListener revMobClientListener) {
        String str2;
        String str3 = "install";
        if (serverEndPoints.containsKey(str3)) {
            str2 = (String) serverEndPoints.get(str3);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(PRODUCTION_SERVER_ADDRESS);
            sb.append(String.format(INSTALL_URL, new Object[]{this.appId}));
            str2 = sb.toString();
        }
        serverRequestWithSessionVerification(str2, str, revMobClientListener);
    }

    public void fetchFullscreen(String str, String str2, RevMobClientListener revMobClientListener) {
        fetch(createFetchUrl("fullscreen", "fullscreens", this.appId, str, false), str2, revMobClientListener);
    }

    public void fetchVideo(String str, String str2, RevMobClientListener revMobClientListener, int i) {
        String str3;
        if (i == 3) {
            str3 = createFetchUrl("video", "videos", this.appId, str, true);
        } else {
            str3 = createFetchUrl("video", "videos", this.appId, str, false);
        }
        fetch(str3, str2, revMobClientListener);
    }

    public void fetchVideoOrFullscreen(String str, String str2, RevMobClientListener revMobClientListener) {
        fetch(createFetchUrl("fullscreen", "fullscreens", this.appId, str, true), str2, revMobClientListener);
    }

    public void fetchBanner(String str, String str2, RevMobClientListener revMobClientListener) {
        fetch(createFetchUrl("banner", "banners", this.appId, str, false), str2, revMobClientListener);
    }

    public void fetchAdLink(String str, String str2, RevMobClientListener revMobClientListener) {
        fetch(createFetchUrl("link", "anchors", this.appId, str, false), str2, revMobClientListener);
    }

    public void fetchPopup(String str, String str2, RevMobClientListener revMobClientListener) {
        fetch(createFetchUrl("pop_up", "pop_ups", this.appId, str, false), str2, revMobClientListener);
    }

    public void fetch(String str, String str2, RevMobClientListener revMobClientListener) {
        if (this.testingMode != RevMobTestingMode.DISABLED) {
            StringBuilder sb = new StringBuilder();
            sb.append("Fetching ad using testing mode: ");
            sb.append(this.testingMode.getValue());
            RMLog.i(sb.toString());
        }
        serverRequestWithSessionVerification(str, str2, revMobClientListener);
    }

    public void registerUserInformation(String str, RevMobClientListener revMobClientListener) {
        if (serverEndPoints.containsKey("user_information")) {
            String str2 = (String) serverEndPoints.get("user_information");
            try {
                RevMobContext.adThread.join();
                serverRequest(str2, str, revMobClientListener);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void reportImpression(String str, String str2) {
        if (str != null) {
            if (this.testingMode != RevMobTestingMode.DISABLED) {
                StringBuilder sb = new StringBuilder();
                sb.append("Reporting impression using testing mode: ");
                sb.append(this.testingMode.getValue());
                RMLog.i(sb.toString());
            }
            this.reportingImpression = true;
            serverRequestWithSessionVerification(str, str2, null);
        }
    }

    public void serverRequestWithSessionVerification(String str, String str2, RevMobClientListener revMobClientListener) {
        if (!this.sessionStarted) {
            RMLog.w(SESSION_WARNING);
        } else {
            serverRequest(str, str2, revMobClientListener);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isPlacementIdValid(String str) {
        return str != null && str.length() == 24;
    }

    /* access modifiers changed from: 0000 */
    public String createFetchUrl(String str, String str2, String str3, String str4, boolean z) {
        String str5;
        if ((str == "videos" || str == "video") && z) {
            str = "rewardedVideos";
            str2 = "rewardedVideos";
        }
        if (isPlacementIdValid(str4)) {
            StringBuilder sb = new StringBuilder();
            sb.append("fetch_");
            sb.append(str);
            sb.append("_with_placement");
            String sb2 = sb.toString();
            if (serverEndPoints.containsKey(sb2)) {
                str5 = ((String) serverEndPoints.get(sb2)).replaceFirst("PLACEMENT_ID", str4);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(PRODUCTION_SERVER_ADDRESS);
                sb3.append(String.format(PLACEMENT_ADS_PATH, new Object[]{str3, str4, str2}));
                str5 = sb3.toString();
            }
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("fetch_");
            sb4.append(str);
            String sb5 = sb4.toString();
            if (serverEndPoints.containsKey(sb5)) {
                str5 = (String) serverEndPoints.get(sb5);
            } else {
                StringBuilder sb6 = new StringBuilder();
                sb6.append(PRODUCTION_SERVER_ADDRESS);
                sb6.append(String.format(ADS_PATH, new Object[]{str3, str2}));
                str5 = sb6.toString();
            }
        }
        if (!z) {
            return str5;
        }
        if (str != "fullscreens" && str != "fullscreen") {
            return str5;
        }
        StringBuilder sb7 = new StringBuilder();
        sb7.append(str5);
        sb7.append("?video=true");
        return sb7.toString();
    }

    public void serverRequest(final String str, final String str2, final RevMobClientListener revMobClientListener) {
        if (!this.requestIsStartSession && !this.reportingImpression && !this.requestInstalledApps) {
            t0 = System.currentTimeMillis();
        }
        new Thread() {
            public void run() {
                try {
                    RevMobClient.this.processResponse(new HTTPHelper().post(str, str2), revMobClientListener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: 0000 */
    public void processResponse(HttpResponse httpResponse, RevMobClientListener revMobClientListener) throws JSONException {
        if (httpResponse == null) {
            handleDownloadError(0, revMobClientListener);
            return;
        }
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode == 200 || statusCode == 202) {
            String encodedResponseBody = HTTPHelper.encodedResponseBody(httpResponse.getEntity());
            if (encodedResponseBody == null) {
                RMLog.w(BAD_RESPONSE_MESSAGE);
                if (revMobClientListener != null) {
                    revMobClientListener.handleError(BAD_RESPONSE_MESSAGE);
                }
                return;
            }
            handleSuccess(encodedResponseBody, revMobClientListener);
        } else {
            handleDownloadError(statusCode, revMobClientListener);
        }
    }

    /* access modifiers changed from: 0000 */
    public void handleSuccess(String str, RevMobClientListener revMobClientListener) {
        try {
            String decrypt = new RevMobEncryption().decrypt(str);
            if (this.requestIsStartSession) {
                if (this.publisherListener != null) {
                    this.publisherListener.onRevMobSessionIsStarted();
                }
                this.requestIsStartSession = false;
                this.requestInstalledApps = true;
            } else if (this.requestInstalledApps) {
                this.requestInstalledApps = false;
            } else if (!this.reportingImpression) {
                t1 = System.currentTimeMillis();
            }
            if (this.reportingImpression) {
                this.reportingImpression = false;
                if (!(t0 == 0 || t1 == 0 || t2 == 0 || t3 == 0)) {
                    t0 = 0;
                    t1 = 0;
                    t2 = 0;
                    t3 = 0;
                }
            }
            if (revMobClientListener != null) {
                revMobClientListener.handleResponse(decrypt);
            }
        } catch (OutOfMemoryError unused) {
            RMLog.w(NO_MEMORY_MESSAGE);
            if (revMobClientListener != null) {
                revMobClientListener.handleError(NO_MEMORY_MESSAGE);
            }
        } catch (JSONException e) {
            RMLog.w(PARSE_ERROR_MESSAGE);
            RMLog.d(str, e);
            if (revMobClientListener != null) {
                revMobClientListener.handleError(PARSE_ERROR_MESSAGE);
            }
        } catch (NullPointerException e2) {
            RMLog.d(str, e2);
            if (revMobClientListener != null) {
                revMobClientListener.handleError(PARSE_ERROR_MESSAGE2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void handleDownloadError(int i, RevMobClientListener revMobClientListener) {
        String str = (String) LOG_MESSAGES.get(i, DEFAULT_DOWNLOAD_ERROR_MESSAGE);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" (");
        sb.append(i);
        sb.append(")");
        String sb2 = sb.toString();
        RMLog.w(sb2);
        if (revMobClientListener != null) {
            revMobClientListener.handleError(sb2);
        }
        if (this.requestIsStartSession) {
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobSessionNotStarted(sb2);
            }
            this.requestIsStartSession = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isAppIdValid(String str) {
        return str != null && str.length() == 24;
    }

    public RevMobTestingMode getTestingMode() {
        return this.testingMode;
    }

    public void setTestingMode(RevMobTestingMode revMobTestingMode) {
        this.testingMode = revMobTestingMode;
        if (revMobTestingMode != RevMobTestingMode.DISABLED) {
            StringBuilder sb = new StringBuilder();
            sb.append("Testing mode enabled: ");
            sb.append(revMobTestingMode.getValue());
            RMLog.i(sb.toString());
            return;
        }
        RMLog.i("Testing mode disabled");
    }

    public RevMobParallaxMode getParallaxMode() {
        return this.parallaxMode;
    }

    public void setParallaxMode(RevMobParallaxMode revMobParallaxMode) {
        this.parallaxMode = revMobParallaxMode;
        if (revMobParallaxMode != RevMobParallaxMode.DISABLED) {
            RMLog.i("Parallax mode enabled");
        } else {
            RMLog.i("Parallax mode disabled");
        }
    }

    public void setTimeoutInSeconds(int i) {
        if (i <= 1 || i >= 300) {
            RMLog.w(INVALID_TIMEOUT);
            return;
        }
        HTTPHelper.globalTimeoutInSeconds = i;
        StringBuilder sb = new StringBuilder();
        sb.append("Timeout changed to ");
        sb.append(i);
        sb.append("s");
        RMLog.i(sb.toString());
    }

    public static void setSDKName(String str) {
        if (str != null) {
            SDK_NAME = str;
            SDK_SOURCE_NAME = "android";
        }
    }

    public static void setSDKVersion(String str) {
        if (str != null) {
            SDK_VERSION = str;
            SDK_SOURCE_VERSION = "9.0.4";
        }
    }

    public String getAppId() {
        return this.appId;
    }

    public static void sett0(long j) {
        t0 = j;
    }

    public static void sett1(long j) {
        t1 = j;
    }

    public static void sett2(long j) {
        t2 = j;
    }

    public static void sett3(long j) {
        t3 = j;
    }
}
