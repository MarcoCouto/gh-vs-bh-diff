package com.mansoon.BatteryDouble.util;

import android.content.Context;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.SparseArray;
import com.facebook.internal.AnalyticsEvents;
import com.mansoon.BatteryDouble.R;
import java.text.NumberFormat;
import java.util.List;

public class StringHelper {
    private static final SparseArray<String> importanceToString = new SparseArray<>();

    static {
        importanceToString.put(500, "Not running");
        importanceToString.put(400, "Background process");
        importanceToString.put(300, "Service");
        importanceToString.put(Callback.DEFAULT_DRAG_ANIMATION_DURATION, "Visible task");
        importanceToString.put(100, "Foreground app");
    }

    public static String importanceString(int i) {
        String str = (String) importanceToString.get(i);
        if (str != null && str.length() != 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(i);
        LogUtils.LOGE("Importance not found: ", sb.toString());
        return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
    }

    public static String translatedPriority(Context context, String str) {
        if (str == null) {
            return context.getString(R.string.priorityDefault);
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -1577917980:
                if (str.equals("Foreground app")) {
                    c = 4;
                    break;
                }
                break;
            case -1103780316:
                if (str.equals("Suggestion")) {
                    c = 6;
                    break;
                }
                break;
            case -786595502:
                if (str.equals("Not running")) {
                    c = 0;
                    break;
                }
                break;
            case -646160747:
                if (str.equals("Service")) {
                    c = 2;
                    break;
                }
                break;
            case -131006317:
                if (str.equals("Visible task")) {
                    c = 3;
                    break;
                }
                break;
            case 884577168:
                if (str.equals("Perceptible task")) {
                    c = 5;
                    break;
                }
                break;
            case 1295367581:
                if (str.equals("Background process")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return context.getString(R.string.prioritynotrunning);
            case 1:
                return context.getString(R.string.prioritybackground);
            case 2:
                return context.getString(R.string.priorityservice);
            case 3:
                return context.getString(R.string.priorityvisible);
            case 4:
                return context.getString(R.string.priorityforeground);
            case 5:
                return context.getString(R.string.priorityperceptible);
            case 6:
                return context.getString(R.string.prioritysuggestion);
            default:
                return context.getString(R.string.priorityDefault);
        }
    }

    public static String formatProcessName(String str) {
        int lastIndexOf = str.lastIndexOf(58);
        if (lastIndexOf <= 0) {
            lastIndexOf = str.length();
        }
        return str.substring(0, lastIndexOf);
    }

    public static String formatPercentageNumber(float f) {
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        percentInstance.setMinimumFractionDigits(0);
        return percentInstance.format((double) f);
    }

    public static String formatNumber(float f) {
        NumberFormat numberInstance = NumberFormat.getNumberInstance();
        numberInstance.setMinimumFractionDigits(1);
        return numberInstance.format((double) f);
    }

    public static String formatNumber(double d) {
        NumberFormat numberInstance = NumberFormat.getNumberInstance();
        numberInstance.setMaximumFractionDigits(2);
        return numberInstance.format(d);
    }

    public static String[] trimArray(String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = strArr[i].trim();
        }
        return strArr;
    }

    public static String convertToHex(byte[] bArr) {
        int i;
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            int i2 = (b >>> 4) & 15;
            int i3 = 0;
            while (true) {
                if (i2 < 0 || i2 > 9) {
                    i = 97;
                    i2 -= 10;
                } else {
                    i = 48;
                }
                sb.append((char) (i + i2));
                i2 = b & 15;
                int i4 = i3 + 1;
                if (i3 >= 1) {
                    break;
                }
                i3 = i4;
            }
        }
        return sb.toString();
    }

    public static String convertToString(Object obj) {
        if (!(obj instanceof List)) {
            return String.valueOf(obj);
        }
        String valueOf = String.valueOf(obj);
        return valueOf.substring(1, valueOf.length() - 1);
    }

    public static String truncate(String str, int i) {
        if (str.length() <= i) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str.substring(0, i));
        sb.append("...");
        return sb.toString();
    }
}
