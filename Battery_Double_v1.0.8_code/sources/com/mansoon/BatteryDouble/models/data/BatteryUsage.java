package com.mansoon.BatteryDouble.models.data;

import io.realm.BatteryUsageRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.internal.RealmObjectProxy;

public class BatteryUsage extends RealmObject implements BatteryUsageRealmProxyInterface {
    public BatteryDetails details;
    @PrimaryKey
    public int id;
    public float level;
    public int screenOn;
    public String state;
    @Index
    public long timestamp;
    public String triggeredBy;

    public BatteryDetails realmGet$details() {
        return this.details;
    }

    public int realmGet$id() {
        return this.id;
    }

    public float realmGet$level() {
        return this.level;
    }

    public int realmGet$screenOn() {
        return this.screenOn;
    }

    public String realmGet$state() {
        return this.state;
    }

    public long realmGet$timestamp() {
        return this.timestamp;
    }

    public String realmGet$triggeredBy() {
        return this.triggeredBy;
    }

    public void realmSet$details(BatteryDetails batteryDetails) {
        this.details = batteryDetails;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$level(float f) {
        this.level = f;
    }

    public void realmSet$screenOn(int i) {
        this.screenOn = i;
    }

    public void realmSet$state(String str) {
        this.state = str;
    }

    public void realmSet$timestamp(long j) {
        this.timestamp = j;
    }

    public void realmSet$triggeredBy(String str) {
        this.triggeredBy = str;
    }

    public BatteryUsage() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
