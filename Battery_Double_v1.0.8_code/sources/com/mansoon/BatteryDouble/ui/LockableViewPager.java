package com.mansoon.BatteryDouble.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class LockableViewPager extends ViewPager {
    private boolean swipeable;

    public LockableViewPager(Context context) {
        super(context);
    }

    public LockableViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.swipeable = true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.swipeable) {
            return super.onTouchEvent(motionEvent);
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.swipeable) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    public void setSwipeable(boolean z) {
        this.swipeable = z;
    }
}
