package com.mansoon.BatteryDouble;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.mansoon.BatteryDouble";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 15;
    public static final String VERSION_NAME = "1.0.8";
}
