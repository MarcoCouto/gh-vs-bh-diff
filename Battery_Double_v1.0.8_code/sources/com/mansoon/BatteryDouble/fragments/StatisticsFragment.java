package com.mansoon.BatteryDouble.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.RefreshChartEvent;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import com.mansoon.BatteryDouble.models.ui.ChartCard;
import com.mansoon.BatteryDouble.ui.MainActivity;
import com.mansoon.BatteryDouble.ui.adapters.ChartRVAdapter;
import com.mansoon.BatteryDouble.util.DateUtils;
import com.mansoon.BatteryDouble.util.LogUtils;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.Iterator;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class StatisticsFragment extends Fragment {
    private static final String TAG = LogUtils.makeLogTag(StatisticsFragment.class);
    private MainActivity mActivity;
    private ChartRVAdapter mAdapter;
    private ArrayList<ChartCard> mChartCards;
    private RecyclerView mRecyclerView;
    /* access modifiers changed from: private */
    public int mSelectedInterval;

    public static StatisticsFragment newInstance() {
        return new StatisticsFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_statistics, viewGroup, false);
        this.mActivity = (MainActivity) getActivity();
        this.mRecyclerView = (RecyclerView) inflate.findViewById(R.id.rv);
        this.mAdapter = null;
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(inflate.getContext()));
        this.mRecyclerView.setHasFixedSize(true);
        ((BottomNavigationView) inflate.findViewById(R.id.bottom_navigation)).setOnNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_24h /*2131296263*/:
                        StatisticsFragment.this.mSelectedInterval = 1;
                        StatisticsFragment.this.loadData(1);
                        return true;
                    case R.id.action_3days /*2131296264*/:
                        StatisticsFragment.this.mSelectedInterval = 2;
                        StatisticsFragment.this.loadData(2);
                        return true;
                    case R.id.action_5days /*2131296265*/:
                        StatisticsFragment.this.mSelectedInterval = 3;
                        StatisticsFragment.this.loadData(3);
                        return true;
                    default:
                        return false;
                }
            }
        });
        this.mSelectedInterval = 1;
        return inflate;
    }

    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onResume() {
        super.onResume();
        loadData(this.mSelectedInterval);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshChartsData(RefreshChartEvent refreshChartEvent) {
        loadData(this.mSelectedInterval);
    }

    /* access modifiers changed from: private */
    public void loadData(int i) {
        RealmResults realmResults;
        long currentTimeMillis = System.currentTimeMillis();
        this.mChartCards = new ArrayList<>();
        if (this.mActivity.database.isClosed()) {
            this.mActivity.database.getDefaultInstance();
        }
        if (i == 1) {
            realmResults = this.mActivity.database.betweenUsages(DateUtils.getMilliSecondsInterval(1), currentTimeMillis);
        } else if (i == 2) {
            realmResults = this.mActivity.database.betweenUsages(DateUtils.getMilliSecondsInterval(2), currentTimeMillis);
        } else if (i == 3) {
            realmResults = this.mActivity.database.betweenUsages(DateUtils.getMilliSecondsInterval(3), currentTimeMillis);
        } else {
            realmResults = this.mActivity.database.betweenUsages(DateUtils.getMilliSecondsInterval(1), currentTimeMillis);
        }
        fillData(realmResults);
        setAdapter(this.mSelectedInterval);
    }

    private void fillData(@NonNull RealmResults<BatteryUsage> realmResults) {
        double d;
        double d2;
        double d3;
        double d4;
        ChartCard chartCard = new ChartCard(1, getString(R.string.chart_battery_level), ColorTemplate.rgb("#E84813"));
        Iterator it = realmResults.iterator();
        while (it.hasNext()) {
            BatteryUsage batteryUsage = (BatteryUsage) it.next();
            chartCard.entries.add(new Entry((float) batteryUsage.realmGet$timestamp(), batteryUsage.realmGet$level()));
        }
        this.mChartCards.add(chartCard);
        if (!realmResults.isEmpty()) {
            d2 = Double.MIN_VALUE;
            d = Double.MAX_VALUE;
        } else {
            d2 = Utils.DOUBLE_EPSILON;
            d = Utils.DOUBLE_EPSILON;
        }
        ChartCard chartCard2 = new ChartCard(2, getString(R.string.chart_battery_temperature), ColorTemplate.rgb("#E81332"));
        Iterator it2 = realmResults.iterator();
        double d5 = d2;
        double d6 = Utils.DOUBLE_EPSILON;
        while (it2.hasNext()) {
            BatteryUsage batteryUsage2 = (BatteryUsage) it2.next();
            chartCard2.entries.add(new Entry((float) batteryUsage2.realmGet$timestamp(), (float) batteryUsage2.realmGet$details().realmGet$temperature()));
            if (batteryUsage2.realmGet$details().realmGet$temperature() < d) {
                d = batteryUsage2.realmGet$details().realmGet$temperature();
            }
            if (batteryUsage2.realmGet$details().realmGet$temperature() > d5) {
                d5 = batteryUsage2.realmGet$details().realmGet$temperature();
            }
            d6 += batteryUsage2.realmGet$details().realmGet$temperature();
        }
        double size = d6 / ((double) realmResults.size());
        chartCard2.extras = new double[]{d, size, d5};
        this.mChartCards.add(chartCard2);
        if (!realmResults.isEmpty()) {
            d5 = Double.MIN_VALUE;
            d4 = Double.MAX_VALUE;
            d3 = Utils.DOUBLE_EPSILON;
        } else {
            d3 = size;
            d4 = d;
        }
        ChartCard chartCard3 = new ChartCard(3, getString(R.string.chart_battery_voltage), ColorTemplate.rgb("#FF15AC"));
        Iterator it3 = realmResults.iterator();
        while (it3.hasNext()) {
            BatteryUsage batteryUsage3 = (BatteryUsage) it3.next();
            chartCard3.entries.add(new Entry((float) batteryUsage3.realmGet$timestamp(), (float) batteryUsage3.realmGet$details().realmGet$voltage()));
            if (batteryUsage3.realmGet$details().realmGet$voltage() < d4) {
                d4 = batteryUsage3.realmGet$details().realmGet$voltage();
            }
            if (batteryUsage3.realmGet$details().realmGet$voltage() > d5) {
                d5 = batteryUsage3.realmGet$details().realmGet$voltage();
            }
            d3 += batteryUsage3.realmGet$details().realmGet$voltage();
        }
        chartCard3.extras = new double[]{d4, d3 / ((double) realmResults.size()), d5};
        this.mChartCards.add(chartCard3);
    }

    private void setAdapter(int i) {
        if (this.mAdapter == null) {
            this.mAdapter = new ChartRVAdapter(this.mChartCards, i, getActivity().getApplicationContext());
            this.mRecyclerView.setAdapter(this.mAdapter);
        } else {
            this.mAdapter.setInterval(i);
            this.mAdapter.swap(this.mChartCards);
        }
        this.mRecyclerView.invalidate();
    }
}
