package com.mansoon.BatteryDouble.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.ui.WelcomeActivity.WelcomeActivityContent;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;

public class TosFragment extends WelcomeFragment implements WelcomeActivityContent {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag("TosFragment");

    public boolean shouldDisplay(Context context) {
        return !SettingsUtils.isTosAccepted(context);
    }

    /* access modifiers changed from: protected */
    public OnClickListener getPositiveListener() {
        return new WelcomeFragmentOnClickListener(this.mActivity) {
            public void onClick(View view) {
                LogUtils.LOGD(TosFragment.TAG, "Marking TOS flag.");
                SettingsUtils.markTosAccepted(this.mActivity, true);
                doNext();
            }
        };
    }

    /* access modifiers changed from: protected */
    public OnClickListener getNegativeListener() {
        return new WelcomeFragmentOnClickListener(this.mActivity) {
            public void onClick(View view) {
                LogUtils.LOGD(TosFragment.TAG, "Need to accept Tos.");
                doFinish();
            }
        };
    }

    /* access modifiers changed from: protected */
    public String getPositiveText() {
        LogUtils.LOGD(TAG, "Getting Accept string.");
        return getResourceString(R.string.accept);
    }

    /* access modifiers changed from: protected */
    public String getNegativeText() {
        LogUtils.LOGD(TAG, "Getting Decline string.");
        return getResourceString(R.string.decline);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        return layoutInflater.inflate(R.layout.welcome_tos_fragment, viewGroup, false);
    }
}
