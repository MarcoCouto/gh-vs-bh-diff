package com.mansoon.BatteryDouble.network.handlers;

import android.content.Context;
import com.mansoon.BatteryDouble.Config;
import com.mansoon.BatteryDouble.models.ServerStatus;
import com.mansoon.BatteryDouble.network.services.GreenHubStatusService;
import com.mansoon.BatteryDouble.tasks.RegisterDeviceTask;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerStatusHandler {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(ServerStatusHandler.class);
    private GreenHubStatusService mService = ((GreenHubStatusService) new Builder().baseUrl(Config.SERVER_STATUS_URL).addConverterFactory(GsonConverterFactory.create()).build().create(GreenHubStatusService.class));

    public void callGetStatus(final Context context) {
        LogUtils.LOGI(TAG, "callGetStatus()");
        this.mService.getStatus().enqueue(new Callback<ServerStatus>() {
            public void onResponse(Call<ServerStatus> call, Response<ServerStatus> response) {
                if (response != null && response.body() != null) {
                    String access$000 = ServerStatusHandler.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Server Status: { server: ");
                    sb.append(((ServerStatus) response.body()).server);
                    sb.append(", version: ");
                    sb.append(((ServerStatus) response.body()).version);
                    sb.append(" }");
                    LogUtils.LOGI(access$000, sb.toString());
                    if (!SettingsUtils.fetchServerUrl(context).equals(((ServerStatus) response.body()).server)) {
                        SettingsUtils.markDeviceAccepted(context, false);
                    }
                    SettingsUtils.saveServerUrl(context, ((ServerStatus) response.body()).server);
                    SettingsUtils.saveAppVersion(context, ((ServerStatus) response.body()).version);
                    if (!SettingsUtils.isDeviceRegistered(context)) {
                        new RegisterDeviceTask().execute(new Context[]{context});
                    }
                }
            }

            public void onFailure(Call<ServerStatus> call, Throwable th) {
                th.printStackTrace();
            }
        });
    }
}
