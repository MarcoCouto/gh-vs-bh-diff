package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.sdk.AppLovinLogger;

class j implements AppLovinLogger {
    private be a;
    private k b;

    j() {
    }

    /* access modifiers changed from: 0000 */
    public void a(be beVar) {
        this.a = beVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(k kVar) {
        this.b = kVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        if (this.a != null) {
            return ((Boolean) this.a.a(bb.j)).booleanValue();
        }
        return false;
    }

    public void d(String str, String str2) {
        if (a()) {
            String str3 = AppLovinLogger.SDK_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.d(str3, sb.toString());
        }
        if (this.b != null) {
            k kVar = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("DEBUG  [");
            sb2.append(str);
            sb2.append("] ");
            sb2.append(str2);
            kVar.a(sb2.toString());
        }
    }

    public void e(String str, String str2) {
        e(str, str2, null);
    }

    public void e(String str, String str2, Throwable th) {
        String str3;
        if (a()) {
            String str4 = AppLovinLogger.SDK_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.e(str4, sb.toString(), th);
        }
        if (this.b != null) {
            k kVar = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ERROR  [");
            sb2.append(str);
            sb2.append("] ");
            sb2.append(str2);
            if (th != null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(": ");
                sb3.append(th.getMessage());
                str3 = sb3.toString();
            } else {
                str3 = "";
            }
            sb2.append(str3);
            kVar.a(sb2.toString());
        }
    }

    public void i(String str, String str2) {
        if (a()) {
            String str3 = AppLovinLogger.SDK_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.i(str3, sb.toString());
        }
        if (this.b != null) {
            k kVar = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("INFO  [");
            sb2.append(str);
            sb2.append("] ");
            sb2.append(str2);
            kVar.a(sb2.toString());
        }
    }

    public void userError(String str, String str2) {
        userError(str, str2, null);
    }

    public void userError(String str, String str2, Throwable th) {
        String str3;
        String str4 = AppLovinLogger.SDK_TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.e(str4, sb.toString(), th);
        if (this.b != null) {
            k kVar = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("USER  [");
            sb2.append(str);
            sb2.append("] ");
            sb2.append(str2);
            if (th != null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(": ");
                sb3.append(th.getMessage());
                str3 = sb3.toString();
            } else {
                str3 = "";
            }
            sb2.append(str3);
            kVar.a(sb2.toString());
        }
    }

    public void w(String str, String str2) {
        w(str, str2, null);
    }

    public void w(String str, String str2, Throwable th) {
        if (a()) {
            String str3 = AppLovinLogger.SDK_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            Log.w(str3, sb.toString(), th);
        }
        if (this.b != null) {
            k kVar = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("WARN  [");
            sb2.append(str);
            sb2.append("] ");
            sb2.append(str2);
            kVar.a(sb2.toString());
        }
    }
}
