package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Map;

class i extends ba {
    final /* synthetic */ e a;
    private final AppLovinAdSize b;

    public i(e eVar, AppLovinAdSize appLovinAdSize) {
        this.a = eVar;
        super("UpdateAdTask", eVar.a);
        this.b = appLovinAdSize;
    }

    public void run() {
        h hVar = (h) ((Map) this.a.d.get(AppLovinAdType.REGULAR)).get(this.b);
        synchronized (hVar.b) {
            boolean a2 = this.a.a(this.b);
            boolean d = this.a.a();
            boolean z = !hVar.f.isEmpty();
            boolean z2 = System.currentTimeMillis() > hVar.d;
            if (a2 && z && z2 && d && !hVar.e) {
                hVar.e = true;
                this.a.b(this.b, AppLovinAdType.REGULAR, new g(this.a, hVar));
            }
        }
    }
}
