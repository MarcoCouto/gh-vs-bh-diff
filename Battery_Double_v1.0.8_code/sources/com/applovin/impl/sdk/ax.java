package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import java.util.LinkedList;
import java.util.Queue;

class ax {
    private final int a;
    private final Queue b;
    private final Object c;

    ax(int i) {
        if (i > 4) {
            i = 4;
        }
        this.a = i;
        this.b = new LinkedList();
        this.c = new Object();
    }

    /* access modifiers changed from: 0000 */
    public int a() {
        int size;
        synchronized (this.c) {
            size = this.b.size();
        }
        return size;
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAd appLovinAd) {
        synchronized (this.c) {
            if (!c()) {
                this.b.offer(appLovinAd);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        boolean z;
        synchronized (this.c) {
            z = a() >= this.a;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        boolean z;
        synchronized (this.c) {
            z = a() == 0;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public AppLovinAd e() {
        AppLovinAd appLovinAd;
        try {
            synchronized (this.c) {
                appLovinAd = !d() ? (AppLovinAd) this.b.poll() : null;
            }
            return appLovinAd;
        } catch (Exception unused) {
            return null;
        }
    }
}
