package com.applovin.impl.sdk;

import android.app.AlertDialog.Builder;

class ao implements Runnable {
    final /* synthetic */ an a;

    ao(an anVar) {
        this.a = anVar;
    }

    public void run() {
        Builder builder = new Builder(this.a.c);
        builder.setTitle((CharSequence) this.a.a.a(bb.X));
        builder.setMessage((CharSequence) this.a.a.a(bb.Y));
        builder.setCancelable(false);
        builder.setPositiveButton((CharSequence) this.a.a.a(bb.Z), new ap(this));
        builder.setNegativeButton((CharSequence) this.a.a.a(bb.aa), new as(this));
        builder.show();
    }
}
