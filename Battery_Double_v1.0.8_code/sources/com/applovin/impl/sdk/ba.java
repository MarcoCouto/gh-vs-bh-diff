package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.sdk.AppLovinLogger;

abstract class ba implements Runnable {
    final String e;
    protected final AppLovinSdkImpl f;
    final AppLovinLogger g;
    final Context h;

    ba(String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.f = appLovinSdkImpl;
        if (str == null) {
            str = getClass().getSimpleName();
        }
        this.e = str;
        this.g = appLovinSdkImpl.getLogger();
        this.h = appLovinSdkImpl.getApplicationContext();
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
    }

    /* access modifiers changed from: 0000 */
    public q c() {
        return new q(this.f);
    }
}
