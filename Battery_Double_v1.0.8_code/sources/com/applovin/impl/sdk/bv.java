package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.facebook.AccessToken;
import com.facebook.internal.NativeProtocol;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class bv extends ay {
    /* access modifiers changed from: private */
    public final AppLovinAdInternal a;

    public bv(AppLovinSdkImpl appLovinSdkImpl, AppLovinAd appLovinAd) {
        super("TaskReportReward", appLovinSdkImpl);
        this.a = (AppLovinAdInternal) appLovinAd;
    }

    public void run() {
        String b = w.b();
        String clCode = this.a.getClCode();
        HashMap hashMap = new HashMap(2);
        if (cd.c(clCode)) {
            hashMap.put("clcode", clCode);
        } else {
            hashMap.put("clcode", "NO_CLCODE");
        }
        if (b != null) {
            hashMap.put(AccessToken.USER_ID_KEY, b);
        }
        av a2 = av.a();
        String b2 = a2.b(this.a);
        if (b2 != null) {
            hashMap.put("result", b2);
            Map a3 = a2.a(this.a);
            if (a3 != null) {
                hashMap.put(NativeProtocol.WEB_DIALOG_PARAMS, a3);
            }
            a("cr", new JSONObject(hashMap), new bw(this));
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("No reward result was found for ad: ");
        sb.append(this.a);
        this.g.d("TaskReportReward", sb.toString());
    }
}
