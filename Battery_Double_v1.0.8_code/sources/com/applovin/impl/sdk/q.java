package com.applovin.impl.sdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import com.applovin.sdk.AppLovinLogger;
import com.facebook.places.model.PlaceFields;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

class q {
    private final AppLovinSdkImpl a;
    private final Context b;
    private final AppLovinLogger c;

    q(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.a = appLovinSdkImpl;
        this.c = appLovinSdkImpl.getLogger();
        this.b = appLovinSdkImpl.getApplicationContext();
    }

    static boolean a(String str, Context context) {
        if (str == null) {
            throw new IllegalArgumentException("No permission name specified");
        } else if (context != null) {
            return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    /* access modifiers changed from: 0000 */
    public t a() {
        t tVar = new t();
        tVar.h = Locale.getDefault();
        tVar.a = Build.MODEL;
        tVar.b = VERSION.RELEASE;
        tVar.c = Build.MANUFACTURER;
        tVar.e = VERSION.SDK_INT;
        tVar.d = Build.DEVICE;
        if (a("android.permission.READ_PHONE_STATE")) {
            TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                tVar.f = telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH);
                String networkOperatorName = telephonyManager.getNetworkOperatorName();
                try {
                    tVar.g = URLEncoder.encode(networkOperatorName, "UTF-8");
                    return tVar;
                } catch (UnsupportedEncodingException unused) {
                    tVar.g = networkOperatorName;
                }
            }
        }
        return tVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) {
        return a(str, this.b);
    }

    /* access modifiers changed from: 0000 */
    public s b() {
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo = this.b.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.b.getPackageManager();
        try {
            packageInfo = packageManager.getPackageInfo(this.b.getPackageName(), 0);
        } catch (NameNotFoundException unused) {
            packageInfo = null;
        }
        s sVar = new s();
        sVar.c = applicationInfo.packageName;
        sVar.d = lastModified;
        sVar.a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        sVar.b = packageInfo != null ? packageInfo.versionName : "";
        return sVar;
    }

    /* access modifiers changed from: 0000 */
    public r c() {
        try {
            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            if (cls != null) {
                Object invoke = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{this.b});
                if (invoke != null) {
                    Class cls2 = invoke.getClass();
                    Object invoke2 = cls2.getMethod("isLimitAdTrackingEnabled", null).invoke(invoke, null);
                    Object invoke3 = cls2.getMethod("getId", null).invoke(invoke, null);
                    r rVar = new r();
                    String str = (String) invoke3;
                    if (str == null) {
                        str = "";
                    }
                    rVar.a = ((Boolean) invoke2).booleanValue();
                    rVar.b = str;
                    return rVar;
                }
            }
        } catch (ClassNotFoundException e) {
            this.c.userError("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", e);
        } catch (Throwable th) {
            this.c.e("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
        }
        r rVar2 = new r();
        rVar2.b = "";
        rVar2.a = false;
        return rVar2;
    }
}
