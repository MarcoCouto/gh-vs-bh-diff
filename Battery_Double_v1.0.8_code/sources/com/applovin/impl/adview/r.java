package com.applovin.impl.adview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.ViewParent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;
import java.util.List;

class r extends WebViewClient {
    private final AppLovinLogger a;
    private final AdViewControllerImpl b;

    public r(AdViewControllerImpl adViewControllerImpl, AppLovinSdk appLovinSdk) {
        this.a = appLovinSdk.getLogger();
        this.b = adViewControllerImpl;
    }

    private void a(Uri uri, o oVar) {
        try {
            oVar.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to show \"");
            sb.append(uri);
            sb.append("\".");
            this.a.e("AdWebViewClient", sb.toString(), th);
        }
    }

    private void c(o oVar) {
        AppLovinAd a2 = oVar.a();
        if (a2 != null) {
            this.b.a(a2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(WebView webView, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Processing click on ad URL \"");
        sb.append(str);
        sb.append("\"");
        this.a.d("AdWebViewClient", sb.toString());
        if (str != null && (webView instanceof o)) {
            Uri parse = Uri.parse(str);
            o oVar = (o) webView;
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            if (!AppLovinSdk.URI_SCHEME.equals(scheme) || !AppLovinSdk.URI_HOST.equals(host)) {
                c(oVar);
                a(parse, oVar);
            } else if (AppLovinAdService.URI_NEXT_AD.equals(path)) {
                a(oVar);
            } else if (AppLovinAdService.URI_CLOSE_AD.equals(path)) {
                b(oVar);
            } else if (path == null || !path.startsWith("/launch/")) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unknown URL: ");
                sb2.append(str);
                this.a.w("AdWebViewClient", sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Path: ");
                sb3.append(path);
                this.a.w("AdWebViewClient", sb3.toString());
            } else {
                List pathSegments = parse.getPathSegments();
                if (pathSegments != null && pathSegments.size() > 1) {
                    String str2 = (String) pathSegments.get(pathSegments.size() - 1);
                    try {
                        Context context = webView.getContext();
                        context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str2));
                        c(oVar);
                    } catch (Exception e) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Threw Exception Trying to Launch App for Package: ");
                        sb4.append(str2);
                        this.a.e("AdWebViewClient", sb4.toString(), e);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar) {
        ViewParent parent = oVar.getParent();
        if (parent instanceof AppLovinAdView) {
            ((AppLovinAdView) parent).loadNextAd();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(o oVar) {
        this.b.a();
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.b.onAdHtmlLoaded(webView);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
