package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.applovin.adview.AppLovinInterstitialActivity;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.AppLovinAdInternal;
import com.applovin.impl.sdk.AppLovinAdInternal.AdTarget;
import com.applovin.impl.sdk.AppLovinSdkImpl;
import com.applovin.impl.sdk.cd;
import com.applovin.impl.sdk.m;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ad implements AppLovinInterstitialAdDialog {
    public static volatile boolean a = false;
    public static volatile boolean b = false;
    private static final Map c = Collections.synchronizedMap(new HashMap());
    private static volatile boolean n;
    private final String d;
    private final AppLovinSdkImpl e;
    private final Activity f;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener g;
    private volatile AppLovinAdDisplayListener h;
    private volatile AppLovinAdVideoPlaybackListener i;
    private volatile AppLovinAdClickListener j;
    private volatile AppLovinAdInternal k;
    private volatile AdTarget l;
    /* access modifiers changed from: private */
    public volatile u m;

    ad(AppLovinSdk appLovinSdk, Activity activity) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else {
            this.e = (AppLovinSdkImpl) appLovinSdk;
            this.d = UUID.randomUUID().toString();
            a = true;
            b = false;
            this.f = activity;
            c.put(this.d, this);
        }
    }

    public static ad a(String str) {
        return (ad) c.get(str);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.f.runOnUiThread(new ah(this, i2));
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        this.f.runOnUiThread(new ag(this, appLovinAd));
    }

    /* access modifiers changed from: private */
    public void h() {
        v vVar = new v(this.e, this.f);
        vVar.a(this);
        this.m = vVar;
        vVar.a((AppLovinAd) this.k);
    }

    /* access modifiers changed from: private */
    public void i() {
        Intent intent = new Intent(this.f, AppLovinInterstitialActivity.class);
        intent.putExtra(AppLovinInterstitialActivity.KEY_WRAPPER_ID, this.d);
        AppLovinInterstitialActivity.lastKnownWrapper = this;
        this.f.startActivity(intent);
        a(true);
    }

    public AppLovinSdk a() {
        return this.e;
    }

    public void a(u uVar) {
        this.m = uVar;
    }

    public void a(boolean z) {
        n = z;
    }

    public AppLovinAd b() {
        return this.k;
    }

    public AppLovinAdVideoPlaybackListener c() {
        return this.i;
    }

    public AppLovinAdDisplayListener d() {
        return this.h;
    }

    public void dismiss() {
        if (this.m != null) {
            this.f.runOnUiThread(new ai(this));
        }
    }

    public AppLovinAdClickListener e() {
        return this.j;
    }

    public AdTarget f() {
        return this.l;
    }

    public void g() {
        a = false;
        b = true;
        c.remove(this.d);
    }

    public boolean isAdReadyToDisplay() {
        return this.e.getAdService().hasPreloadedAd(AppLovinAdSize.INTERSTITIAL);
    }

    public boolean isShowing() {
        return n;
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.j = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.h = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.g = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.i = appLovinAdVideoPlaybackListener;
    }

    public void show() {
        this.e.getAdService().loadNextAd(AppLovinAdSize.INTERSTITIAL, new ae(this));
    }

    public void showAndRender(AppLovinAd appLovinAd) {
        if (isShowing()) {
            this.e.getLogger().userError("AppLovinInterstitialAdDialog", "Attempted to show an interstitial while one is already displayed; ignoring.");
            return;
        }
        this.k = (AppLovinAdInternal) appLovinAd;
        this.l = this.k != null ? this.k.getTarget() : AdTarget.DEFAULT;
        if (!cd.c(this.k.getVideoFilename()) || this.e.getFileManager().a(this.k.getVideoFilename(), (Context) this.f)) {
            this.f.runOnUiThread(new af(this, m.a(AppLovinInterstitialActivity.class, (Context) this.f), this.l == AdTarget.ACTIVITY_LANDSCAPE || this.l == AdTarget.ACTIVITY_PORTRAIT));
        }
    }
}
