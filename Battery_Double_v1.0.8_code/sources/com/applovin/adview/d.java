package com.applovin.adview;

class d implements Runnable {
    final /* synthetic */ int a;
    final /* synthetic */ int b;
    final /* synthetic */ c c;

    d(c cVar, int i, int i2) {
        this.c = cVar;
        this.a = i;
        this.b = i2;
    }

    public void run() {
        StringBuilder sb = new StringBuilder();
        sb.append("Media player error (");
        sb.append(this.a);
        sb.append(",");
        sb.append(this.b);
        sb.append(") - showing close button.");
        this.c.a.a.d.e("AppLovinInterstitialActivity", sb.toString());
        this.c.a.a.e();
    }
}
