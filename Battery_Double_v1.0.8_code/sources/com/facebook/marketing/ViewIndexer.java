package com.facebook.marketing;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.Callback;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.internal.Logger;
import com.facebook.internal.ServerProtocol;
import com.facebook.internal.Utility;
import com.facebook.marketing.internal.Constants;
import com.facebook.marketing.internal.MarketingLogger;
import com.facebook.marketing.internal.MarketingUtils;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ViewIndexer {
    private static final String APP_VERSION_PARAM = "app_version";
    private static final String PLATFORM_PARAM = "platform";
    private static final String SUCCESS = "success";
    /* access modifiers changed from: private */
    public static final String TAG = ViewIndexer.class.getCanonicalName();
    private static final String TREE_PARAM = "tree";
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public Timer indexingTimer;
    /* access modifiers changed from: private */
    public final MarketingLogger logger = new MarketingLogger(FacebookSdk.getApplicationContext(), FacebookSdk.getApplicationId());
    /* access modifiers changed from: private */
    public String previousDigest = null;
    /* access modifiers changed from: private */
    public final Handler uiThreadHandler = new Handler(Looper.getMainLooper());

    private static class ScreenshotTaker implements Callable<String> {
        private WeakReference<View> rootView;

        public ScreenshotTaker(View view) {
            this.rootView = new WeakReference<>(view);
        }

        public String call() throws Exception {
            View view = (View) this.rootView.get();
            if (view == null || view.getWidth() == 0 || view.getHeight() == 0) {
                return "";
            }
            Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.RGB_565);
            view.draw(new Canvas(createBitmap));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            createBitmap.compress(CompressFormat.JPEG, 10, byteArrayOutputStream);
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
        }
    }

    public ViewIndexer(Activity activity2) {
        this.activity = activity2;
    }

    public void schedule() {
        final String simpleName = this.activity.getClass().getSimpleName();
        FacebookSdk.getApplicationId();
        final AnonymousClass1 r1 = new TimerTask() {
            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0067 */
            public void run() {
                String str;
                try {
                    View rootView = ViewIndexer.this.activity.getWindow().getDecorView().getRootView();
                    if (CodelessActivityLifecycleTracker.getIsAppIndexingEnabled()) {
                        FutureTask futureTask = new FutureTask(new ScreenshotTaker(rootView));
                        ViewIndexer.this.uiThreadHandler.post(futureTask);
                        String str2 = "";
                        try {
                            str = (String) futureTask.get(1, TimeUnit.SECONDS);
                        } catch (Exception e) {
                            Log.e(ViewIndexer.TAG, "Failed to take screenshot.", e);
                            str = str2;
                        }
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("screenname", simpleName);
                        jSONObject.put("screenshot", str);
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(ViewHierarchy.getDictionaryOfView(rootView));
                        jSONObject.put("view", jSONArray);
                        Log.e(ViewIndexer.TAG, "Failed to create JSONObject");
                        ViewIndexer.this.sendToServer(jSONObject.toString());
                    }
                } catch (Exception e2) {
                    Log.e(ViewIndexer.TAG, "UI Component tree indexing failure!", e2);
                }
            }
        };
        FacebookSdk.getExecutor().execute(new Runnable() {
            public void run() {
                try {
                    if (ViewIndexer.this.indexingTimer != null) {
                        ViewIndexer.this.indexingTimer.cancel();
                    }
                    ViewIndexer.this.previousDigest = null;
                    ViewIndexer.this.indexingTimer = new Timer();
                    ViewIndexer.this.indexingTimer.scheduleAtFixedRate(r1, 0, 1000);
                } catch (Exception e) {
                    Log.e(ViewIndexer.TAG, "Error scheduling indexing job", e);
                }
            }
        });
    }

    public void unschedule() {
        if (this.indexingTimer != null) {
            try {
                this.indexingTimer.cancel();
                this.indexingTimer = null;
                if (CodelessActivityLifecycleTracker.getIsAppIndexingEnabled()) {
                    this.logger.logIndexingCancelled(this.activity.getClass().getCanonicalName());
                }
            } catch (Exception e) {
                Log.e(TAG, "Error unscheduling indexing job", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendToServer(final String str) {
        FacebookSdk.getExecutor().execute(new Runnable() {
            public void run() {
                String applicationId = FacebookSdk.getApplicationId();
                String canonicalName = ViewIndexer.this.activity.getClass().getCanonicalName();
                String md5hash = Utility.md5hash(str);
                AccessToken currentAccessToken = AccessToken.getCurrentAccessToken();
                if (md5hash == null || !md5hash.equals(ViewIndexer.this.previousDigest)) {
                    ViewIndexer.this.logger.logIndexingStart(canonicalName);
                    GraphRequest access$700 = ViewIndexer.buildAppIndexingRequest(str, currentAccessToken, applicationId);
                    if (access$700 != null) {
                        GraphResponse executeAndWait = access$700.executeAndWait();
                        try {
                            JSONObject jSONObject = executeAndWait.getJSONObject();
                            if (jSONObject != null) {
                                if (jSONObject.has("success") && jSONObject.getString("success") == ServerProtocol.DIALOG_RETURN_SCOPES_TRUE) {
                                    Logger.log(LoggingBehavior.APP_EVENTS, ViewIndexer.TAG, "Successfully send UI component tree to server");
                                    ViewIndexer.this.previousDigest = md5hash;
                                    ViewIndexer.this.logger.logIndexingComplete(canonicalName);
                                }
                                if (jSONObject.has(Constants.APP_INDEXING_ENABLED)) {
                                    CodelessActivityLifecycleTracker.updateAppIndexing(Boolean.valueOf(jSONObject.getBoolean(Constants.APP_INDEXING_ENABLED)));
                                }
                            } else {
                                String access$200 = ViewIndexer.TAG;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Error sending UI component tree to Facebook: ");
                                sb.append(executeAndWait.getError());
                                Log.e(access$200, sb.toString());
                            }
                        } catch (JSONException e) {
                            Log.e(ViewIndexer.TAG, "Error decoding server response.", e);
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    @Nullable
    public static GraphRequest buildAppIndexingRequest(String str, AccessToken accessToken, String str2) {
        if (str == null) {
            return null;
        }
        GraphRequest newPostRequest = GraphRequest.newPostRequest(accessToken, String.format(Locale.US, "%s/app_indexing", new Object[]{str2}), null, null);
        Bundle parameters = newPostRequest.getParameters();
        if (parameters == null) {
            parameters = new Bundle();
        }
        parameters.putString(TREE_PARAM, str);
        parameters.putString(APP_VERSION_PARAM, MarketingUtils.getAppVersion());
        parameters.putString(PLATFORM_PARAM, "android");
        parameters.putString(Constants.DEVICE_SESSION_ID, CodelessActivityLifecycleTracker.getCurrentDeviceSessionID());
        newPostRequest.setParameters(parameters);
        newPostRequest.setCallback(new Callback() {
            public void onCompleted(GraphResponse graphResponse) {
                Logger.log(LoggingBehavior.APP_EVENTS, ViewIndexer.TAG, "App index sent to FB!");
            }
        });
        return newPostRequest;
    }
}
