package com.facebook.marketing;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.AttributionIdentifiers;
import com.facebook.internal.FetchedAppSettings;
import com.facebook.internal.FetchedAppSettingsManager;
import com.facebook.internal.Utility;
import com.facebook.marketing.ViewIndexingTrigger.OnShakeListener;
import com.facebook.marketing.internal.Constants;
import com.facebook.marketing.internal.MarketingLogger;
import com.facebook.marketing.internal.MarketingUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Locale;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public class CodelessActivityLifecycleTracker {
    private static final String TAG = CodelessActivityLifecycleTracker.class.getCanonicalName();
    /* access modifiers changed from: private */
    public static String deviceSessionID;
    /* access modifiers changed from: private */
    public static Boolean isAppIndexingEnabled = Boolean.valueOf(false);
    /* access modifiers changed from: private */
    public static volatile Boolean isCheckingSession = Boolean.valueOf(false);
    /* access modifiers changed from: private */
    public static SensorManager sensorManager;
    /* access modifiers changed from: private */
    public static ViewIndexer viewIndexer;
    /* access modifiers changed from: private */
    public static final ViewIndexingTrigger viewIndexingTrigger = new ViewIndexingTrigger();

    public static void startTracking(Application application, String str) {
        application.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
            }

            public void onActivityDestroyed(Activity activity) {
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            public void onActivityStarted(Activity activity) {
            }

            public void onActivityStopped(Activity activity) {
            }

            public void onActivityResumed(Activity activity) {
                final Context applicationContext = activity.getApplicationContext();
                final String applicationId = FacebookSdk.getApplicationId();
                final FetchedAppSettings appSettingsWithoutQuery = FetchedAppSettingsManager.getAppSettingsWithoutQuery(applicationId);
                CodelessActivityLifecycleTracker.sensorManager = (SensorManager) applicationContext.getSystemService("sensor");
                Sensor defaultSensor = CodelessActivityLifecycleTracker.sensorManager.getDefaultSensor(1);
                CodelessActivityLifecycleTracker.viewIndexer = new ViewIndexer(activity);
                CodelessActivityLifecycleTracker.viewIndexingTrigger.setOnShakeListener(new OnShakeListener() {
                    public void onShake(int i) {
                        if (i >= 3) {
                            CodelessActivityLifecycleTracker.viewIndexingTrigger.resetCount();
                            MarketingLogger marketingLogger = new MarketingLogger(applicationContext, applicationId);
                            marketingLogger.logGestureTriggered();
                            if (appSettingsWithoutQuery != null && appSettingsWithoutQuery.getCodelessEventsEnabled()) {
                                CodelessActivityLifecycleTracker.checkCodelessSession(applicationId, marketingLogger);
                            }
                        }
                    }
                });
                CodelessActivityLifecycleTracker.sensorManager.registerListener(CodelessActivityLifecycleTracker.viewIndexingTrigger, defaultSensor, 2);
                if (appSettingsWithoutQuery != null && appSettingsWithoutQuery.getCodelessEventsEnabled()) {
                    CodelessActivityLifecycleTracker.viewIndexer.schedule();
                }
            }

            public void onActivityPaused(Activity activity) {
                CodelessActivityLifecycleTracker.viewIndexer.unschedule();
                CodelessActivityLifecycleTracker.sensorManager.unregisterListener(CodelessActivityLifecycleTracker.viewIndexingTrigger);
            }
        });
    }

    public static void checkCodelessSession(final String str, final MarketingLogger marketingLogger) {
        if (!isCheckingSession.booleanValue()) {
            isCheckingSession = Boolean.valueOf(true);
            FacebookSdk.getExecutor().execute(new Runnable() {
                public void run() {
                    boolean z = true;
                    GraphRequest newPostRequest = GraphRequest.newPostRequest(null, String.format(Locale.US, "%s/app_indexing_session", new Object[]{str}), null, null);
                    Bundle parameters = newPostRequest.getParameters();
                    if (parameters == null) {
                        parameters = new Bundle();
                    }
                    AttributionIdentifiers attributionIdentifiers = AttributionIdentifiers.getAttributionIdentifiers(FacebookSdk.getApplicationContext());
                    JSONArray jSONArray = new JSONArray();
                    jSONArray.put(Build.MODEL != null ? Build.MODEL : "");
                    if (attributionIdentifiers == null || attributionIdentifiers.getAndroidAdvertiserId() == null) {
                        jSONArray.put("");
                    } else {
                        jSONArray.put(attributionIdentifiers.getAndroidAdvertiserId());
                    }
                    jSONArray.put("0");
                    jSONArray.put(MarketingUtils.isEmulator() ? AppEventsConstants.EVENT_PARAM_VALUE_YES : "0");
                    Locale currentLocale = Utility.getCurrentLocale();
                    StringBuilder sb = new StringBuilder();
                    sb.append(currentLocale.getLanguage());
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(currentLocale.getCountry());
                    jSONArray.put(sb.toString());
                    String jSONArray2 = jSONArray.toString();
                    parameters.putString(Constants.DEVICE_SESSION_ID, CodelessActivityLifecycleTracker.getCurrentDeviceSessionID());
                    parameters.putString(Constants.EXTINFO, jSONArray2);
                    newPostRequest.setParameters(parameters);
                    if (newPostRequest != null) {
                        JSONObject jSONObject = newPostRequest.executeAndWait().getJSONObject();
                        if (jSONObject == null || !jSONObject.optBoolean(Constants.APP_INDEXING_ENABLED, false)) {
                            z = false;
                        }
                        CodelessActivityLifecycleTracker.isAppIndexingEnabled = Boolean.valueOf(z);
                        if (!CodelessActivityLifecycleTracker.isAppIndexingEnabled.booleanValue()) {
                            CodelessActivityLifecycleTracker.deviceSessionID = null;
                        } else {
                            marketingLogger.logSessionReady();
                            CodelessActivityLifecycleTracker.viewIndexer.schedule();
                        }
                    }
                    CodelessActivityLifecycleTracker.isCheckingSession = Boolean.valueOf(false);
                }
            });
        }
    }

    public static String getCurrentDeviceSessionID() {
        if (deviceSessionID == null) {
            deviceSessionID = UUID.randomUUID().toString();
        }
        return deviceSessionID;
    }

    public static boolean getIsAppIndexingEnabled() {
        return isAppIndexingEnabled.booleanValue();
    }

    public static void updateAppIndexing(Boolean bool) {
        isAppIndexingEnabled = bool;
    }
}
