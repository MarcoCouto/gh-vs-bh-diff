package com.facebook.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.q.c.d.a;
import com.facebook.ads.internal.view.e.b.b;
import com.facebook.ads.internal.view.e.b.c;
import com.facebook.ads.internal.view.e.b.d;
import com.facebook.ads.internal.view.e.b.e;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.i;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.k;
import com.facebook.ads.internal.view.e.b.l;
import com.facebook.ads.internal.view.e.b.m;
import com.facebook.ads.internal.view.e.b.p;
import com.facebook.ads.internal.view.e.b.q;
import com.facebook.ads.internal.view.e.b.v;
import com.facebook.ads.internal.view.e.b.w;
import com.facebook.ads.internal.view.n;
import com.facebook.ads.internal.view.o;

public abstract class MediaViewVideoRenderer extends FrameLayout {
    private static final String d = "MediaViewVideoRenderer";
    @Nullable
    protected NativeAd a;
    protected VideoAutoplayBehavior b;
    final n c;
    private final m e = new m() {
        public void a(l lVar) {
            MediaViewVideoRenderer.this.onPrepared();
        }
    };
    private final k f = new k() {
        public void a(j jVar) {
            MediaViewVideoRenderer.this.onPlayed();
        }
    };
    private final i g = new i() {
        public void a(h hVar) {
            MediaViewVideoRenderer.this.onPaused();
        }
    };
    private final q h = new q() {
        public void a(p pVar) {
            MediaViewVideoRenderer.this.onSeek();
        }
    };
    private final c i = new c() {
        public void a(b bVar) {
            MediaViewVideoRenderer.this.onCompleted();
        }
    };
    private final w j = new w() {
        public void a(v vVar) {
            MediaViewVideoRenderer.this.onVolumeChanged();
        }
    };
    private final e k = new e() {
        public void a(d dVar) {
            MediaViewVideoRenderer.this.onError();
        }
    };
    private boolean l;
    private boolean m;
    private boolean n = true;
    private boolean o = true;

    public MediaViewVideoRenderer(Context context) {
        super(context);
        this.c = new n(context);
        a();
    }

    public MediaViewVideoRenderer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = new n(context, attributeSet);
        a();
    }

    public MediaViewVideoRenderer(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new n(context, attributeSet, i2);
        a();
    }

    @TargetApi(21)
    public MediaViewVideoRenderer(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.c = new n(context, attributeSet, i2, i3);
        a();
    }

    private void a() {
        this.c.setEnableBackgroundVideo(shouldAllowBackgroundPlayback());
        this.c.setLayoutParams(new LayoutParams(-1, -1));
        addView(this.c);
        com.facebook.ads.internal.q.a.i.a(this.c, com.facebook.ads.internal.q.a.i.INTERNAL_AD_MEDIA);
        this.c.getEventBus().a((T[]) new f[]{this.e, this.f, this.g, this.h, this.i, this.j, this.k});
    }

    public void destroy() {
        this.c.k();
    }

    public final void disengageSeek(VideoStartReason videoStartReason) {
        if (!this.l) {
            Log.w(d, "disengageSeek called without engageSeek.");
            return;
        }
        this.l = false;
        if (this.m) {
            this.c.a(videoStartReason.a());
        }
        onSeekDisengaged();
    }

    public final void engageSeek() {
        if (this.l) {
            Log.w(d, "engageSeek called without disengageSeek.");
            return;
        }
        this.l = true;
        this.m = com.facebook.ads.internal.view.e.d.d.STARTED.equals(this.c.getState());
        this.c.a(false);
        onSeekEngaged();
    }

    @IntRange(from = 0)
    public final int getCurrentTimeMs() {
        return this.c.getCurrentPosition();
    }

    @IntRange(from = 0)
    public final int getDuration() {
        return this.c.getDuration();
    }

    @FloatRange(from = 0.0d, to = 0.0d)
    public final float getVolume() {
        return this.c.getVolume();
    }

    public void onCompleted() {
    }

    public void onError() {
    }

    public void onPaused() {
    }

    public void onPlayed() {
    }

    public void onPrepared() {
    }

    public void onSeek() {
    }

    public void onSeekDisengaged() {
    }

    public void onSeekEngaged() {
    }

    public void onVolumeChanged() {
    }

    public final void pause(boolean z) {
        this.c.a(z);
    }

    public final void play(VideoStartReason videoStartReason) {
        this.c.a(videoStartReason.a());
    }

    public final void seekTo(@IntRange(from = 0) int i2) {
        if (!this.l) {
            Log.w(d, "Seeking must be preceded by a call to engageSeek, and followed by a call to disengageSeek.");
        } else {
            this.c.a(i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void setAdEventManager(com.facebook.ads.internal.m.c cVar) {
        this.c.setAdEventManager(cVar);
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    public void setAutoplay(boolean z) {
        this.n = z;
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    public void setAutoplayOnMobile(boolean z) {
        this.o = z;
    }

    /* access modifiers changed from: 0000 */
    public final void setListener(o oVar) {
        this.c.setListener(oVar);
    }

    public void setNativeAd(NativeAd nativeAd) {
        this.a = nativeAd;
        this.c.a(nativeAd.d(), nativeAd.g());
        this.c.setVideoMPD(nativeAd.c());
        this.c.setVideoURI(nativeAd.b());
        this.c.setVideoCTA(nativeAd.getAdCallToAction());
        this.c.setNativeAd(nativeAd);
        this.b = nativeAd.e();
    }

    public final void setVolume(@FloatRange(from = 0.0d, to = 1.0d) float f2) {
        this.c.setVolume(f2);
    }

    public boolean shouldAllowBackgroundPlayback() {
        return false;
    }

    public boolean shouldAutoplay() {
        boolean z = false;
        if (this.c != null) {
            if (this.c.getState() == com.facebook.ads.internal.view.e.d.d.PLAYBACK_COMPLETED) {
                return false;
            }
            if (this.b == VideoAutoplayBehavior.DEFAULT) {
                if (this.n && (this.o || com.facebook.ads.internal.q.c.d.c(getContext()) == a.MOBILE_INTERNET)) {
                    z = true;
                }
                return z;
            } else if (this.b == VideoAutoplayBehavior.ON) {
                z = true;
            }
        }
        return z;
    }

    public void unsetNativeAd() {
        pause(false);
        this.c.a(null, null);
        this.c.setVideoMPD(null);
        this.c.setVideoURI(null);
        this.c.setVideoCTA(null);
        this.c.setNativeAd(null);
        this.b = VideoAutoplayBehavior.DEFAULT;
        this.a = null;
    }
}
