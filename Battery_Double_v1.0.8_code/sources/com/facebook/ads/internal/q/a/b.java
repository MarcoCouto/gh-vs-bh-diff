package com.facebook.ads.internal.q.a;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;

public class b {
    private static boolean a = false;
    private static boolean b = false;

    @Nullable
    public static synchronized String a(String str) {
        synchronized (b.class) {
            if (!a()) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("fb.e2e.");
            sb.append(str);
            String property = System.getProperty(sb.toString());
            return property;
        }
    }

    public static synchronized boolean a() {
        boolean z;
        synchronized (b.class) {
            if (!b) {
                a = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(System.getProperty("fb.running_e2e"));
                b = true;
            }
            z = a;
        }
        return z;
    }

    public static synchronized boolean b(String str) {
        boolean z;
        synchronized (b.class) {
            z = !TextUtils.isEmpty(a(str));
        }
        return z;
    }
}
