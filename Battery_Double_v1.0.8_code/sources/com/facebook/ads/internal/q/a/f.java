package com.facebook.ads.internal.q.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.Signature;
import android.os.Build;
import android.support.annotation.Nullable;
import com.google.android.gms.common.util.AndroidUtilsLight;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;

public class f {
    private static final String a = "f";

    public enum a {
        UNKNOWN(0),
        UNROOTED(1),
        ROOTED(2);
        
        public final int d;

        private a(int i) {
            this.d = i;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001a A[Catch:{ Throwable -> 0x0020 }] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001d A[Catch:{ Throwable -> 0x0020 }] */
    public static a a() {
        boolean z;
        try {
            if (!c() && !b()) {
                if (!a("su")) {
                    z = false;
                    return !z ? a.ROOTED : a.UNROOTED;
                }
            }
            z = true;
            if (!z) {
            }
        } catch (Throwable unused) {
            return a.UNKNOWN;
        }
    }

    @Nullable
    public static String a(Context context) {
        try {
            return b(context);
        } catch (Exception unused) {
            return null;
        }
    }

    private static PublicKey a(Signature signature) {
        return CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(signature.toByteArray())).getPublicKey();
    }

    private static boolean a(String str) {
        for (String file : System.getenv("PATH").split(":")) {
            File file2 = new File(file);
            if (file2.exists() && file2.isDirectory()) {
                File[] listFiles = file2.listFiles();
                if (listFiles == null) {
                    continue;
                } else {
                    for (File name : listFiles) {
                        if (name.getName().equals(str)) {
                            return true;
                        }
                    }
                    continue;
                }
            }
        }
        return false;
    }

    @SuppressLint({"PackageManagerGetSignatures"})
    private static String b(Context context) {
        StringBuilder sb = new StringBuilder();
        for (Signature a2 : context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures) {
            sb.append(h.a(MessageDigest.getInstance(AndroidUtilsLight.DIGEST_ALGORITHM_SHA1).digest(a(a2).getEncoded())));
            sb.append(";");
        }
        return sb.toString();
    }

    private static boolean b() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean c() {
        return new File("/system/app/Superuser.apk").exists();
    }
}
