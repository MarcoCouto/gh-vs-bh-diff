package com.facebook.ads.internal.q.a;

public class k {
    private final int a;
    private final int b;

    public k(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public int a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }
}
