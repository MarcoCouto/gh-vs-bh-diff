package com.facebook.ads.internal.q.a;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.util.Log;

public class g {
    public static boolean a(int i, int i2) {
        return i >= 640 && i2 >= 640;
    }

    public static boolean a(Context context) {
        boolean z;
        try {
            RunningTaskInfo runningTaskInfo = (RunningTaskInfo) ((ActivityManager) context.getSystemService("activity")).getRunningTasks(2).get(0);
            StringBuilder sb = new StringBuilder();
            sb.append(runningTaskInfo.topActivity.getPackageName());
            sb.append(".UnityPlayerActivity");
            String sb2 = sb.toString();
            if (runningTaskInfo.topActivity.getClassName() != sb2) {
                if (!a(sb2)) {
                    z = false;
                    Boolean valueOf = Boolean.valueOf(z);
                    Log.d("IS_UNITY", Boolean.toString(valueOf.booleanValue()));
                    return valueOf.booleanValue();
                }
            }
            z = true;
            Boolean valueOf2 = Boolean.valueOf(z);
            Log.d("IS_UNITY", Boolean.toString(valueOf2.booleanValue()));
            return valueOf2.booleanValue();
        } catch (Throwable unused) {
            return false;
        }
    }

    public static boolean a(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }
}
