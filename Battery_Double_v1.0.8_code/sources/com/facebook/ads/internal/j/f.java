package com.facebook.ads.internal.j;

import com.facebook.ads.internal.j.d;

public abstract class f<T extends d> {
    public abstract Class<T> a();

    public abstract void a(T t);

    public boolean b(T t) {
        return true;
    }
}
