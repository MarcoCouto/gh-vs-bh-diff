package com.facebook.ads.internal.m;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.facebook.ads.internal.o.d;
import com.facebook.ads.internal.p.a.n;
import com.facebook.ads.internal.p.a.p;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class b {
    private static final String a = "b";
    private static final String b = d.b();
    private final a c;
    /* access modifiers changed from: private */
    public final ThreadPoolExecutor d;
    private final ConnectivityManager e;
    private final com.facebook.ads.internal.p.a.a f;
    private final Handler g;
    private final long h;
    private final long i;
    private final Runnable j = new Runnable() {
        public void run() {
            b.this.k = false;
            if (b.this.d.getQueue().isEmpty()) {
                new AsyncTask<Void, Void, Void>() {
                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public Void doInBackground(Void... voidArr) {
                        b.b(b.this);
                        if (b.this.m > 0) {
                            try {
                                Thread.sleep(b.this.m);
                            } catch (InterruptedException unused) {
                            }
                        }
                        b.this.d();
                        return null;
                    }
                }.executeOnExecutor(b.this.d, new Void[0]);
            }
        }
    };
    /* access modifiers changed from: private */
    public volatile boolean k;
    private int l;
    /* access modifiers changed from: private */
    public long m;

    interface a {
        JSONObject a();

        boolean a(JSONArray jSONArray);

        void b();

        void b(JSONArray jSONArray);

        boolean c();
    }

    b(Context context, a aVar) {
        this.c = aVar;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue());
        this.d = threadPoolExecutor;
        this.e = (ConnectivityManager) context.getSystemService("connectivity");
        this.f = com.facebook.ads.internal.q.c.d.b(context);
        this.g = new Handler(Looper.getMainLooper());
        this.h = com.facebook.ads.internal.l.a.h(context);
        this.i = com.facebook.ads.internal.l.a.i(context);
    }

    private void a(long j2) {
        this.g.postDelayed(this.j, j2);
    }

    static /* synthetic */ int b(b bVar) {
        int i2 = bVar.l + 1;
        bVar.l = i2;
        return i2;
    }

    private void c() {
        if (this.l >= 5) {
            e();
            b();
            return;
        }
        this.m = this.l == 1 ? 2000 : this.m * 2;
        a();
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void d() {
        a aVar;
        JSONArray jSONArray;
        try {
            NetworkInfo activeNetworkInfo = this.e.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.isConnectedOrConnecting()) {
                    JSONObject a2 = this.c.a();
                    if (a2 == null) {
                        e();
                        return;
                    }
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("attempt", String.valueOf(this.l));
                    a2.put("data", jSONObject);
                    p pVar = new p();
                    pVar.put(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, a2.toString());
                    n b2 = this.f.b(b, pVar);
                    String e2 = b2 != null ? b2.e() : null;
                    if (TextUtils.isEmpty(e2)) {
                        if (a2.has("events")) {
                            aVar = this.c;
                            jSONArray = a2.getJSONArray("events");
                        }
                        c();
                        return;
                    }
                    if (b2.a() != 200) {
                        if (a2.has("events")) {
                            aVar = this.c;
                            jSONArray = a2.getJSONArray("events");
                        }
                    } else if (this.c.a(new JSONArray(e2))) {
                        if (!this.c.c()) {
                            e();
                            return;
                        }
                    }
                    c();
                    return;
                    aVar.b(jSONArray);
                    c();
                    return;
                }
            }
            a(this.i);
        } catch (Exception unused) {
            c();
        }
    }

    private void e() {
        this.l = 0;
        this.m = 0;
        if (this.d.getQueue().size() == 0) {
            this.c.b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.k = true;
        this.g.removeCallbacks(this.j);
        a(this.h);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (!this.k) {
            this.k = true;
            this.g.removeCallbacks(this.j);
            a(this.i);
        }
    }
}
