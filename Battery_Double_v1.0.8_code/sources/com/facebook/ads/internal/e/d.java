package com.facebook.ads.internal.e;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class d {
    private static final String a;
    private static final int b = Runtime.getRuntime().availableProcessors();
    private static final int c = Math.max(2, Math.min(b - 1, 4));
    private static final int d = ((b * 2) + 1);
    private static final ThreadFactory e = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder();
            sb.append("DatabaseTask #");
            sb.append(this.a.getAndIncrement());
            return new Thread(runnable, sb.toString());
        }
    };
    private static final BlockingQueue<Runnable> f = new LinkedBlockingQueue(128);
    private static final Executor g;
    private static final ReentrantReadWriteLock h = new ReentrantReadWriteLock();
    private static final Lock i = h.readLock();
    /* access modifiers changed from: private */
    public static final Lock j = h.writeLock();
    /* access modifiers changed from: private */
    public final Context k;
    /* access modifiers changed from: private */
    public final h l = new h(this);
    /* access modifiers changed from: private */
    public final c m = new c(this);
    private SQLiteOpenHelper n;

    private static class a<T> extends AsyncTask<Void, Void, T> {
        private final f<T> a;
        private final a<T> b;
        private com.facebook.ads.internal.e.f.a c;

        a(f<T> fVar, a<T> aVar) {
            this.a = fVar;
            this.b = aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public T doInBackground(Void... voidArr) {
            T t;
            try {
                t = this.a.b();
                try {
                    this.c = this.a.c();
                    return t;
                } catch (SQLiteException unused) {
                    this.c = com.facebook.ads.internal.e.f.a.UNKNOWN;
                    return t;
                }
            } catch (SQLiteException unused2) {
                t = null;
                this.c = com.facebook.ads.internal.e.f.a.UNKNOWN;
                return t;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(T t) {
            if (this.c == null) {
                this.b.a(t);
            } else {
                this.b.a(this.c.a(), this.c.b());
            }
            this.b.a();
        }
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT tokens.");
        sb.append(h.a.b);
        sb.append(", ");
        sb.append("tokens");
        sb.append(".");
        sb.append(h.b.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.a.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.c.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.d.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.e.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.f.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.g.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.h.b);
        sb.append(", ");
        sb.append("events");
        sb.append(".");
        sb.append(c.i.b);
        sb.append(" FROM ");
        sb.append("events");
        sb.append(" JOIN ");
        sb.append("tokens");
        sb.append(" ON ");
        sb.append("events");
        sb.append(".");
        sb.append(c.b.b);
        sb.append(" = ");
        sb.append("tokens");
        sb.append(".");
        sb.append(h.a.b);
        sb.append(" ORDER BY ");
        sb.append("events");
        sb.append(".");
        sb.append(c.e.b);
        sb.append(" ASC");
        a = sb.toString();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(c, d, 30, TimeUnit.SECONDS, f, e);
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        g = threadPoolExecutor;
    }

    public d(Context context) {
        this.k = context;
    }

    private synchronized SQLiteDatabase i() {
        if (this.n == null) {
            this.n = new e(this.k, this);
        }
        return this.n.getWritableDatabase();
    }

    @WorkerThread
    public Cursor a(int i2) {
        i.lock();
        try {
            SQLiteDatabase a2 = a();
            StringBuilder sb = new StringBuilder();
            sb.append(a);
            sb.append(" LIMIT ");
            sb.append(String.valueOf(i2));
            Cursor rawQuery = a2.rawQuery(sb.toString(), null);
            return rawQuery;
        } finally {
            i.unlock();
        }
    }

    public SQLiteDatabase a() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return i();
        }
        throw new IllegalStateException("Cannot call getDatabase from the UI thread!");
    }

    public <T> AsyncTask a(f<T> fVar, a<T> aVar) {
        return com.facebook.ads.internal.q.a.d.a(g, new a(fVar, aVar), new Void[0]);
    }

    public AsyncTask a(String str, int i2, String str2, double d2, double d3, String str3, Map<String, String> map, a<String> aVar) {
        final String str4 = str;
        final int i3 = i2;
        final String str5 = str2;
        final double d4 = d2;
        final double d5 = d3;
        final String str6 = str3;
        final Map<String, String> map2 = map;
        AnonymousClass2 r0 = new i<String>() {
            /* JADX WARNING: Removed duplicated region for block: B:25:0x0076 A[SYNTHETIC, Splitter:B:25:0x0076] */
            /* JADX WARNING: Removed duplicated region for block: B:35:0x0095 A[SYNTHETIC, Splitter:B:35:0x0095] */
            @Nullable
            /* renamed from: a */
            public String b() {
                SQLiteDatabase sQLiteDatabase;
                if (TextUtils.isEmpty(str4)) {
                    return null;
                }
                d.j.lock();
                try {
                    sQLiteDatabase = d.this.a();
                    try {
                        sQLiteDatabase.beginTransaction();
                        String a2 = d.this.m.a(d.this.l.a(str4), i3, str5, d4, d5, str6, map2);
                        sQLiteDatabase.setTransactionSuccessful();
                        if (sQLiteDatabase != null) {
                            try {
                                if (sQLiteDatabase.inTransaction()) {
                                    sQLiteDatabase.endTransaction();
                                }
                            } catch (Exception e2) {
                                com.facebook.ads.internal.q.d.a.a(e2, d.this.k);
                            }
                        }
                        d.j.unlock();
                        return a2;
                    } catch (Exception e3) {
                        e = e3;
                        try {
                            a(com.facebook.ads.internal.e.f.a.DATABASE_INSERT);
                            com.facebook.ads.internal.q.d.a.a(e, d.this.k);
                            if (sQLiteDatabase != null) {
                                try {
                                    if (sQLiteDatabase.inTransaction()) {
                                        sQLiteDatabase.endTransaction();
                                    }
                                } catch (Exception e4) {
                                    com.facebook.ads.internal.q.d.a.a(e4, d.this.k);
                                }
                            }
                            d.j.unlock();
                            return null;
                        } catch (Throwable th) {
                            th = th;
                            if (sQLiteDatabase != null) {
                                try {
                                    if (sQLiteDatabase.inTransaction()) {
                                        sQLiteDatabase.endTransaction();
                                    }
                                } catch (Exception e5) {
                                    com.facebook.ads.internal.q.d.a.a(e5, d.this.k);
                                }
                            }
                            d.j.unlock();
                            throw th;
                        }
                    }
                } catch (Exception e6) {
                    e = e6;
                    sQLiteDatabase = null;
                    a(com.facebook.ads.internal.e.f.a.DATABASE_INSERT);
                    com.facebook.ads.internal.q.d.a.a(e, d.this.k);
                    if (sQLiteDatabase != null) {
                    }
                    d.j.unlock();
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    sQLiteDatabase = null;
                    if (sQLiteDatabase != null) {
                    }
                    d.j.unlock();
                    throw th;
                }
            }
        };
        return a(r0, aVar);
    }

    @WorkerThread
    public boolean a(String str) {
        j.lock();
        boolean z = false;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append("events");
            sb.append(" SET ");
            sb.append(c.i.b);
            sb.append("=");
            sb.append(c.i.b);
            sb.append("+1");
            sb.append(" WHERE ");
            sb.append(c.a.b);
            sb.append("=?");
            a().execSQL(sb.toString(), new String[]{str});
            z = true;
        } catch (SQLiteException unused) {
        }
        j.unlock();
        return z;
    }

    public synchronized void b() {
        for (g e2 : c()) {
            e2.e();
        }
        if (this.n != null) {
            this.n.close();
            this.n = null;
        }
    }

    @WorkerThread
    public boolean b(String str) {
        j.lock();
        try {
            return this.m.a(str);
        } finally {
            j.unlock();
        }
    }

    public g[] c() {
        return new g[]{this.l, this.m};
    }

    public Cursor d() {
        i.lock();
        try {
            return this.m.c();
        } finally {
            i.unlock();
        }
    }

    @WorkerThread
    public Cursor e() {
        i.lock();
        try {
            return this.m.d();
        } finally {
            i.unlock();
        }
    }

    @WorkerThread
    public Cursor f() {
        i.lock();
        try {
            return this.l.c();
        } finally {
            i.unlock();
        }
    }

    @WorkerThread
    public void g() {
        j.lock();
        try {
            this.l.d();
        } finally {
            j.unlock();
        }
    }
}
