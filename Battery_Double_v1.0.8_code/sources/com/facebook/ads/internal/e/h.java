package com.facebook.ads.internal.e;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.UUID;

public class h extends g {
    public static final b a = new b(0, "token_id", "TEXT PRIMARY KEY");
    public static final b b = new b(1, "token", "TEXT");
    public static final b[] c = {a, b};
    private static final String d = "h";
    private static final String e = a("tokens", c);
    private static final String f = a("tokens", c, b);
    private static final String g;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM tokens WHERE NOT EXISTS (SELECT 1 FROM events WHERE tokens.");
        sb.append(a.b);
        sb.append(" = ");
        sb.append("events");
        sb.append(".");
        sb.append(c.b.b);
        sb.append(")");
        g = sb.toString();
    }

    public h(d dVar) {
        super(dVar);
    }

    public String a() {
        return "tokens";
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006c  */
    @WorkerThread
    public String a(String str) {
        Cursor cursor;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Invalid token.");
        }
        try {
            cursor = f().rawQuery(f, new String[]{str});
            try {
                String string = cursor.moveToNext() ? cursor.getString(a.a) : null;
                if (!TextUtils.isEmpty(string)) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return string;
                }
                String uuid = UUID.randomUUID().toString();
                ContentValues contentValues = new ContentValues(2);
                contentValues.put(a.b, uuid);
                contentValues.put(b.b, str);
                f().insertOrThrow("tokens", null, contentValues);
                if (cursor != null) {
                    cursor.close();
                }
                return uuid;
            } catch (Throwable th) {
                th = th;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public b[] b() {
        return c;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public Cursor c() {
        return f().rawQuery(e, null);
    }

    @WorkerThread
    public void d() {
        try {
            f().execSQL(g);
        } catch (SQLException unused) {
        }
    }
}
