package com.facebook.ads.internal.b;

import android.os.Bundle;
import com.facebook.ads.internal.q.a.o;
import com.github.mikephil.charting.utils.Utils;

public class d implements o<Bundle> {
    private c a;
    private final c b;
    private final b c;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;

    public d(b bVar) {
        this.c = bVar;
        this.b = new c(bVar.b);
        this.a = new c(bVar.b);
    }

    public d(b bVar, Bundle bundle) {
        this.c = bVar;
        this.b = (c) bundle.getSerializable("testStats");
        this.a = (c) bundle.getSerializable("viewableStats");
        this.d = bundle.getBoolean("ended");
        this.e = bundle.getBoolean("passed");
        this.f = bundle.getBoolean("complete");
    }

    private void a() {
        this.e = true;
        b();
    }

    private void b() {
        this.f = true;
        c();
    }

    private void c() {
        this.d = true;
        this.c.a(this.f, this.e, this.e ? this.a : this.b);
    }

    public void a(double d2, double d3) {
        if (!this.d) {
            this.b.a(d2, d3);
            this.a.a(d2, d3);
            double f2 = this.a.b().f();
            if (this.c.e && d3 < this.c.b) {
                this.a = new c(this.c.b);
            }
            if (this.c.c < Utils.DOUBLE_EPSILON || this.b.b().e() <= this.c.c || f2 != Utils.DOUBLE_EPSILON) {
                if (f2 >= this.c.d) {
                    a();
                }
                return;
            }
            b();
        }
    }

    public Bundle g() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("viewableStats", this.a);
        bundle.putSerializable("testStats", this.b);
        bundle.putBoolean("ended", this.d);
        bundle.putBoolean("passed", this.e);
        bundle.putBoolean("complete", this.f);
        return bundle;
    }
}
