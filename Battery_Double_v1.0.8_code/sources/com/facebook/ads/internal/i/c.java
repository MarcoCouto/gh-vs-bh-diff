package com.facebook.ads.internal.i;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Base64OutputStream;
import com.facebook.ads.internal.f.e;
import com.facebook.ads.internal.g.b;
import com.facebook.ads.internal.q.a.f;
import com.facebook.ads.internal.q.a.f.a;
import com.facebook.ads.internal.q.a.g;
import com.facebook.ads.internal.q.a.h;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.q.a.m;
import com.facebook.ads.internal.q.a.r;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.q.c.d;
import com.facebook.ads.internal.settings.AdInternalSettings;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.DeflaterOutputStream;
import org.json.JSONObject;

public class c {
    /* access modifiers changed from: private */
    public static final AtomicInteger a = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public static String b;
    private static final a c = f.a();
    private final Context d;
    private final b e;

    public c(Context context, boolean z) {
        this.d = context;
        this.e = new b(context);
        a(context, z);
    }

    private static void a(final Context context, boolean z) {
        if (a.compareAndSet(0, 1)) {
            try {
                m.a();
                final SharedPreferences sharedPreferences = context.getSharedPreferences("FBAdPrefs", 0);
                b bVar = new b(context);
                StringBuilder sb = new StringBuilder();
                sb.append("AFP;");
                sb.append(bVar.g());
                final String sb2 = sb.toString();
                b = sharedPreferences.getString(sb2, null);
                FutureTask futureTask = new FutureTask(new Callable<Boolean>() {
                    /* renamed from: a */
                    public Boolean call() {
                        c.b = c.b(context, context.getPackageName());
                        sharedPreferences.edit().putString(sb2, c.b).apply();
                        c.a.set(2);
                        return Boolean.valueOf(true);
                    }
                });
                Executors.newSingleThreadExecutor().submit(futureTask);
                if (z) {
                    futureTask.get();
                }
            } catch (Exception unused) {
                a.set(0);
            }
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public static String b(Context context, String str) {
        try {
            return h.a(context.getPackageManager().getApplicationInfo(str, 0).sourceDir);
        } catch (Exception e2) {
            e.a(e2, context, new c(context, false).b());
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x006e A[SYNTHETIC, Splitter:B:36:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0073 A[Catch:{ IOException -> 0x007b }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0078 A[Catch:{ IOException -> 0x007b }] */
    public String a() {
        DeflaterOutputStream deflaterOutputStream;
        Base64OutputStream base64OutputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        Throwable e2;
        a(this.d, true);
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                base64OutputStream = new Base64OutputStream(byteArrayOutputStream, 0);
            } catch (IOException e3) {
                deflaterOutputStream = null;
                e2 = e3;
                base64OutputStream = null;
                try {
                    throw new RuntimeException("Failed to build user token", e2);
                } catch (Throwable th) {
                    th = th;
                    if (deflaterOutputStream != null) {
                        try {
                            deflaterOutputStream.close();
                        } catch (IOException unused) {
                            throw th;
                        }
                    }
                    if (base64OutputStream != null) {
                        base64OutputStream.close();
                    }
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                deflaterOutputStream = null;
                th = th2;
                base64OutputStream = null;
                if (deflaterOutputStream != null) {
                }
                if (base64OutputStream != null) {
                }
                if (byteArrayOutputStream != null) {
                }
                throw th;
            }
            try {
                deflaterOutputStream = new DeflaterOutputStream(base64OutputStream);
                try {
                    deflaterOutputStream.write(new JSONObject(b()).toString().getBytes());
                    deflaterOutputStream.close();
                    String byteArrayOutputStream2 = byteArrayOutputStream.toString();
                    if (deflaterOutputStream != null) {
                        try {
                            deflaterOutputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                    if (base64OutputStream != null) {
                        base64OutputStream.close();
                    }
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.close();
                    }
                    return byteArrayOutputStream2;
                } catch (IOException e4) {
                    e2 = e4;
                    throw new RuntimeException("Failed to build user token", e2);
                }
            } catch (IOException e5) {
                Throwable th3 = e5;
                deflaterOutputStream = null;
                e2 = th3;
                throw new RuntimeException("Failed to build user token", e2);
            } catch (Throwable th4) {
                Throwable th5 = th4;
                deflaterOutputStream = null;
                th = th5;
                if (deflaterOutputStream != null) {
                }
                if (base64OutputStream != null) {
                }
                if (byteArrayOutputStream != null) {
                }
                throw th;
            }
        } catch (IOException e6) {
            base64OutputStream = null;
            deflaterOutputStream = null;
            e2 = e6;
            byteArrayOutputStream = null;
            throw new RuntimeException("Failed to build user token", e2);
        } catch (Throwable th6) {
            base64OutputStream = null;
            deflaterOutputStream = null;
            th = th6;
            byteArrayOutputStream = null;
            if (deflaterOutputStream != null) {
            }
            if (base64OutputStream != null) {
            }
            if (byteArrayOutputStream != null) {
            }
            throw th;
        }
    }

    public Map<String, String> b() {
        a(this.d, false);
        com.facebook.ads.internal.g.a.a(this.d);
        HashMap hashMap = new HashMap();
        hashMap.put("SDK", "android");
        hashMap.put("SDK_VERSION", "4.28.2");
        hashMap.put("LOCALE", Locale.getDefault().toString());
        float f = v.b;
        int i = this.d.getResources().getDisplayMetrics().widthPixels;
        int i2 = this.d.getResources().getDisplayMetrics().heightPixels;
        hashMap.put("DENSITY", String.valueOf(f));
        hashMap.put("SCREEN_WIDTH", String.valueOf((int) (((float) i) / f)));
        hashMap.put("SCREEN_HEIGHT", String.valueOf((int) (((float) i2) / f)));
        hashMap.put("ATTRIBUTION_ID", com.facebook.ads.internal.c.b.a);
        hashMap.put("ID_SOURCE", com.facebook.ads.internal.c.b.d);
        hashMap.put("OS", "Android");
        hashMap.put("OSVERS", b.a);
        hashMap.put("BUNDLE", this.e.f());
        hashMap.put("APPNAME", this.e.d());
        hashMap.put("APPVERS", this.e.g());
        hashMap.put("APPBUILD", String.valueOf(this.e.h()));
        hashMap.put("CARRIER", this.e.c());
        hashMap.put("MAKE", this.e.a());
        hashMap.put("MODEL", this.e.b());
        hashMap.put("ROOTED", String.valueOf(c.d));
        hashMap.put("INSTALLER", this.e.e());
        hashMap.put("SDK_CAPABILITY", com.facebook.ads.internal.q.a.c.b());
        hashMap.put("NETWORK_TYPE", String.valueOf(d.c(this.d).g));
        hashMap.put("SESSION_TIME", r.a(m.b()));
        hashMap.put("SESSION_ID", m.c());
        if (b != null) {
            hashMap.put("AFP", b);
        }
        String a2 = f.a(this.d);
        if (a2 != null) {
            hashMap.put("ASHAS", a2);
        }
        hashMap.put("UNITY", String.valueOf(g.a(this.d)));
        String mediationService = AdInternalSettings.getMediationService();
        if (mediationService != null) {
            hashMap.put("MEDIATION_SERVICE", mediationService);
        }
        hashMap.put("ACCESSIBILITY_ENABLED", String.valueOf(this.e.i()));
        if (this.e.j() != -1) {
            hashMap.put("APP_MIN_SDK_VERSION", String.valueOf(this.e.j()));
        }
        hashMap.put("VALPARAMS", b.a());
        hashMap.put("ANALOG", j.a(com.facebook.ads.internal.g.a.a()));
        return hashMap;
    }
}
