package com.facebook.ads.internal.n;

import android.support.annotation.Nullable;
import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import org.json.JSONObject;

public class g {
    private final double a;
    private final double b;

    public g(double d, double d2) {
        this.a = d;
        this.b = d2;
    }

    @Nullable
    public static g a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        double optDouble = jSONObject.optDouble(Param.VALUE, Utils.DOUBLE_EPSILON);
        double optDouble2 = jSONObject.optDouble("scale", Utils.DOUBLE_EPSILON);
        if (optDouble == Utils.DOUBLE_EPSILON || optDouble2 == Utils.DOUBLE_EPSILON) {
            return null;
        }
        return new g(optDouble, optDouble2);
    }

    public double a() {
        return this.a;
    }

    public double b() {
        return this.b;
    }
}
