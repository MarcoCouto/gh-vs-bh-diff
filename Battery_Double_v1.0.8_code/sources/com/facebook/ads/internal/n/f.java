package com.facebook.ads.internal.n;

import android.support.annotation.Nullable;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import org.json.JSONObject;

public class f {
    private final String a;
    private final int b;
    private final int c;

    public f(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    @Nullable
    public static f a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("url");
        if (optString == null) {
            return null;
        }
        return new f(optString, jSONObject.optInt(SettingsJsonConstants.ICON_WIDTH_KEY, 0), jSONObject.optInt(SettingsJsonConstants.ICON_HEIGHT_KEY, 0));
    }

    public String a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }
}
