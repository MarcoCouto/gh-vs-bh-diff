package com.facebook.ads.internal.n;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.ads.internal.DisplayAdController;
import com.facebook.ads.internal.adapters.AdAdapter;
import com.facebook.ads.internal.adapters.aa;
import com.facebook.ads.internal.adapters.ab;
import com.facebook.ads.internal.adapters.ac;
import com.facebook.ads.internal.protocol.AdPlacementType;
import com.facebook.ads.internal.protocol.f;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.w;
import com.facebook.ads.internal.view.x;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

public class e {
    private static final com.facebook.ads.internal.protocol.d b = com.facebook.ads.internal.protocol.d.ADS;
    private static final String c = "e";
    private static WeakHashMap<View, WeakReference<e>> d = new WeakHashMap<>();
    @Deprecated
    private boolean A;
    /* access modifiers changed from: private */
    public long B;
    /* access modifiers changed from: private */
    @Nullable
    public com.facebook.ads.internal.view.b.c C;
    private View D;
    /* access modifiers changed from: private */
    public String E;
    private boolean F;
    @Nullable
    protected ab a;
    /* access modifiers changed from: private */
    public final Context e;
    private final String f;
    /* access modifiers changed from: private */
    public final String g;
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.d.b h;
    /* access modifiers changed from: private */
    public b i;
    private final d j;
    /* access modifiers changed from: private */
    public DisplayAdController k;
    private volatile boolean l;
    private com.facebook.ads.internal.h.d m;
    /* access modifiers changed from: private */
    @Nullable
    public View n;
    private final List<View> o;
    /* access modifiers changed from: private */
    public OnTouchListener p;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.r.a q;
    private C0006a r;
    /* access modifiers changed from: private */
    public final s s;
    /* access modifiers changed from: private */
    @Nullable
    public aa t;
    private a u;
    private b v;
    private x w;
    /* access modifiers changed from: private */
    public i x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    private class a implements OnClickListener, OnLongClickListener, OnTouchListener {
        private a() {
        }

        public void onClick(View view) {
            String str;
            String str2;
            if (!e.this.s.d()) {
                Log.e("FBAudienceNetworkLog", "No touch data recorded, please ensure touch events reach the ad View by returning false if you intercept the event.");
            }
            int l = com.facebook.ads.internal.l.a.l(e.this.e);
            if (l < 0 || e.this.s.c() >= ((long) l)) {
                HashMap hashMap = new HashMap();
                hashMap.put("touch", j.a(e.this.s.e()));
                if (e.this.x != null) {
                    hashMap.put("nti", String.valueOf(e.this.x.c()));
                }
                if (e.this.y) {
                    hashMap.put("nhs", String.valueOf(e.this.y));
                }
                e.this.q.a((Map<String, String>) hashMap);
                if (e.this.a != null) {
                    e.this.a.b(hashMap);
                }
                return;
            }
            if (!e.this.s.b()) {
                str = "FBAudienceNetworkLog";
                str2 = "Ad cannot be clicked before it is viewed.";
            } else {
                str = "FBAudienceNetworkLog";
                str2 = "Clicks happened too fast.";
            }
            Log.e(str, str2);
        }

        public boolean onLongClick(View view) {
            if (e.this.n == null || e.this.C == null) {
                return false;
            }
            e.this.C.setBounds(0, 0, e.this.n.getWidth(), e.this.n.getHeight());
            e.this.C.a(!e.this.C.a());
            return true;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            e.this.s.a(motionEvent, e.this.n, view);
            return e.this.p != null && e.this.p.onTouch(view, motionEvent);
        }
    }

    private class b extends BroadcastReceiver {
        private boolean b;

        private b() {
        }

        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            StringBuilder sb = new StringBuilder();
            sb.append("com.facebook.ads.native.impression:");
            sb.append(e.this.g);
            intentFilter.addAction(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("com.facebook.ads.native.click:");
            sb2.append(e.this.g);
            intentFilter.addAction(sb2.toString());
            LocalBroadcastManager.getInstance(e.this.e).registerReceiver(this, intentFilter);
            this.b = true;
        }

        public void b() {
            if (this.b) {
                try {
                    LocalBroadcastManager.getInstance(e.this.e).unregisterReceiver(this);
                } catch (Exception unused) {
                }
            }
        }

        public void onReceive(Context context, Intent intent) {
            String str = intent.getAction().split(":")[0];
            if (!"com.facebook.ads.native.impression".equals(str) || e.this.t == null) {
                if ("com.facebook.ads.native.click".equals(str) && e.this.a != null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("mil", String.valueOf(true));
                    e.this.a.b(hashMap);
                }
                return;
            }
            e.this.t.a();
        }
    }

    private class c extends com.facebook.ads.internal.adapters.c {
        private c() {
        }

        public void a() {
            if (e.this.i != null) {
                e.this.i.c();
            }
        }

        public void b() {
        }

        public boolean c() {
            return false;
        }
    }

    public interface d {
        boolean a(View view);
    }

    public e(Context context, ab abVar, com.facebook.ads.internal.h.d dVar, d dVar2) {
        this(context, null, dVar2);
        this.a = abVar;
        this.m = dVar;
        this.l = true;
        this.D = new View(context);
    }

    public e(Context context, String str, d dVar) {
        this.g = UUID.randomUUID().toString();
        this.o = new ArrayList();
        this.s = new s();
        this.z = false;
        this.F = false;
        this.e = context;
        this.f = str;
        this.j = dVar;
        this.h = new com.facebook.ads.internal.d.b(context);
        this.D = new View(context);
    }

    public e(e eVar) {
        this(eVar.e, null, eVar.j);
        this.m = eVar.m;
        this.a = eVar.a;
        this.l = true;
        this.D = new View(this.e);
    }

    private int G() {
        com.facebook.ads.internal.h.d a2;
        if (this.m != null) {
            a2 = this.m;
        } else if (this.k == null || this.k.a() == null) {
            return 0;
        } else {
            a2 = this.k.a();
        }
        return a2.g();
    }

    private int H() {
        if (this.m != null) {
            return this.m.h();
        }
        if (this.a != null) {
            return this.a.j();
        }
        if (this.k == null || this.k.a() == null) {
            return 0;
        }
        return this.k.a().h();
    }

    private int I() {
        if (this.m != null) {
            return this.m.i();
        }
        if (this.a != null) {
            return this.a.k();
        }
        if (this.k == null || this.k.a() == null) {
            return 1000;
        }
        return this.k.a().i();
    }

    /* access modifiers changed from: private */
    public boolean J() {
        return z() == j.DEFAULT ? this.A : z() == j.ON;
    }

    private void K() {
        for (View view : this.o) {
            view.setOnClickListener(null);
            view.setOnTouchListener(null);
            view.setOnLongClickListener(null);
        }
        this.o.clear();
    }

    /* access modifiers changed from: private */
    public void L() {
        if (this.a != null && this.a.d()) {
            this.v = new b();
            this.v.a();
            this.t = new aa(this.e, new com.facebook.ads.internal.adapters.c() {
                public boolean c() {
                    return true;
                }
            }, this.q, this.a);
        }
    }

    public static void a(f fVar, ImageView imageView) {
        if (fVar != null && imageView != null) {
            new com.facebook.ads.internal.view.b.d(imageView).a(fVar.c(), fVar.b()).a(fVar.a());
        }
    }

    private void a(List<View> list, View view) {
        if (this.j == null || !this.j.a(view)) {
            list.add(view);
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    a(list, viewGroup.getChildAt(i2));
                }
            }
        }
    }

    private void b(View view) {
        this.o.add(view);
        view.setOnClickListener(this.u);
        view.setOnTouchListener(this.u);
        if (com.facebook.ads.internal.l.a.b(view.getContext())) {
            view.setOnLongClickListener(this.u);
        }
    }

    public List<e> A() {
        if (!f()) {
            return null;
        }
        return this.a.B();
    }

    @Nullable
    public String B() {
        if (!f()) {
            return null;
        }
        return this.a.c();
    }

    public void C() {
        this.D.performClick();
    }

    public void D() {
        if (this.n != null) {
            if (!d.containsKey(this.n) || ((WeakReference) d.get(this.n)).get() != this) {
                throw new IllegalStateException("View not registered with this NativeAd");
            }
            if ((this.n instanceof ViewGroup) && this.w != null) {
                ((ViewGroup) this.n).removeView(this.w);
                this.w = null;
            }
            if (this.a != null) {
                this.a.b_();
            }
            if (this.C != null && com.facebook.ads.internal.l.a.b(this.e)) {
                this.C.b();
                this.n.getOverlay().remove(this.C);
            }
            d.remove(this.n);
            K();
            this.n = null;
            if (this.q != null) {
                this.q.b();
                this.q = null;
            }
            this.t = null;
        }
    }

    public void E() {
        if (this.F) {
            this.t = new aa(this.e, new c() {
                public boolean d() {
                    return true;
                }

                public String e() {
                    return e.this.E;
                }
            }, this.q, this.a);
        }
    }

    public void F() {
        if (this.t != null) {
            this.t.a();
        }
    }

    public ab a() {
        return this.a;
    }

    public void a(OnTouchListener onTouchListener) {
        this.p = onTouchListener;
    }

    public void a(View view) {
        ArrayList arrayList = new ArrayList();
        a((List<View>) arrayList, view);
        a(view, (List<View>) arrayList);
    }

    public void a(View view, List<View> list) {
        com.facebook.ads.internal.view.b.c cVar;
        com.facebook.ads.internal.h.d a2;
        if (view == null) {
            throw new IllegalArgumentException("Must provide a View");
        } else if (list == null || list.size() == 0) {
            throw new IllegalArgumentException("Invalid set of clickable views");
        } else if (!f()) {
            Log.e(c, "Ad not loaded");
        } else {
            if (this.n != null) {
                Log.w(c, "Native Ad was already registered with a View. Auto unregistering and proceeding.");
                D();
            }
            if (d.containsKey(view) && ((WeakReference) d.get(view)).get() != null) {
                Log.w(c, "View already registered with a NativeAd. Auto unregistering and proceeding.");
                ((e) ((WeakReference) d.get(view)).get()).D();
            }
            this.u = new a();
            this.n = view;
            if (view instanceof ViewGroup) {
                this.w = new x(view.getContext(), new w() {
                    public void a(int i) {
                        if (e.this.a != null) {
                            e.this.a.a(i);
                        }
                    }
                });
                ((ViewGroup) view).addView(this.w);
            }
            ArrayList<View> arrayList = new ArrayList<>(list);
            if (this.D != null) {
                arrayList.add(this.D);
            }
            for (View b2 : arrayList) {
                b(b2);
            }
            this.a.a(view, arrayList);
            int d2 = d();
            this.r = new C0006a() {
                public void a() {
                    e.this.s.a();
                    e.this.q.b();
                    if (e.this.t == null) {
                        if (e.this.q != null) {
                            e.this.q.b();
                            e.this.q = null;
                        }
                        return;
                    }
                    e.this.t.a(e.this.n);
                    e.this.t.a(e.this.x);
                    e.this.t.a(e.this.y);
                    e.this.t.b(e.this.z);
                    e.this.t.c(e.this.J());
                    e.this.t.a();
                }
            };
            com.facebook.ads.internal.r.a aVar = new com.facebook.ads.internal.r.a(this.n, d2, G(), true, this.r);
            this.q = aVar;
            this.q.a(H());
            this.q.b(I());
            this.q.a();
            this.t = new aa(this.e, new c(), this.q, this.a);
            this.t.a((List<View>) arrayList);
            d.put(view, new WeakReference(this));
            if (com.facebook.ads.internal.l.a.b(this.e)) {
                this.C = new com.facebook.ads.internal.view.b.c();
                this.C.a(this.f);
                this.C.b(this.e.getPackageName());
                this.C.a(this.q);
                if (this.a.D() > 0) {
                    this.C.a(this.a.D(), this.a.C());
                }
                if (this.m != null) {
                    cVar = this.C;
                    a2 = this.m;
                } else {
                    if (!(this.k == null || this.k.a() == null)) {
                        cVar = this.C;
                        a2 = this.k.a();
                    }
                    this.n.getOverlay().add(this.C);
                }
                cVar.a(a2.a());
                this.n.getOverlay().add(this.C);
            }
        }
    }

    public void a(ac acVar) {
        if (this.a != null) {
            this.a.a(acVar);
        }
    }

    public void a(b bVar) {
        this.i = bVar;
    }

    public void a(i iVar) {
        this.x = iVar;
    }

    public void a(String str) {
        this.F = true;
        this.E = str;
    }

    public void a(final Set<d> set, String str) {
        if (this.l) {
            throw new IllegalStateException("loadAd cannot be called more than once");
        }
        this.B = System.currentTimeMillis();
        this.l = true;
        DisplayAdController displayAdController = new DisplayAdController(this.e, this.f, f.NATIVE_UNKNOWN, AdPlacementType.NATIVE, null, b, 1, true);
        this.k = displayAdController;
        this.k.a((com.facebook.ads.internal.adapters.a) new com.facebook.ads.internal.adapters.a() {
            public void a() {
                if (e.this.i != null) {
                    e.this.i.b();
                }
            }

            public void a(AdAdapter adAdapter) {
                if (e.this.k != null) {
                    e.this.k.b();
                }
            }

            public void a(final ab abVar) {
                com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(com.facebook.ads.internal.j.a.b.LOADING_AD, AdPlacementType.NATIVE.toString(), System.currentTimeMillis() - e.this.B, null));
                if (abVar != null) {
                    if (set.contains(d.ICON) && abVar.l() != null) {
                        e.this.h.a(abVar.l().a(), abVar.l().c(), abVar.l().b());
                    }
                    if (set.contains(d.IMAGE)) {
                        if (abVar.m() != null) {
                            e.this.h.a(abVar.m().a(), abVar.m().c(), abVar.m().b());
                        }
                        if (abVar.B() != null) {
                            for (e eVar : abVar.B()) {
                                if (eVar.j() != null) {
                                    e.this.h.a(eVar.j().a(), eVar.j().c(), eVar.j().b());
                                }
                            }
                        }
                    }
                    if (set.contains(d.VIDEO) && !TextUtils.isEmpty(abVar.x())) {
                        e.this.h.a(abVar.x());
                    }
                    e.this.h.a((com.facebook.ads.internal.d.a) new com.facebook.ads.internal.d.a() {
                        private void c() {
                            e.this.a = abVar;
                            e.this.L();
                            e.this.E();
                            if (e.this.i != null) {
                                e.this.i.a();
                            }
                        }

                        public void a() {
                            c();
                        }

                        public void b() {
                            c();
                        }
                    });
                    if (!(e.this.i == null || abVar.B() == null)) {
                        AnonymousClass2 r0 = new ac() {
                            public void a(ab abVar) {
                            }

                            public void a(ab abVar, com.facebook.ads.internal.protocol.a aVar) {
                            }

                            public void b(ab abVar) {
                            }

                            public void c(ab abVar) {
                                if (e.this.i != null) {
                                    e.this.i.b();
                                }
                            }
                        };
                        for (e a2 : abVar.B()) {
                            a2.a((ac) r0);
                        }
                    }
                }
            }

            public void a(com.facebook.ads.internal.protocol.a aVar) {
                if (e.this.i != null) {
                    e.this.i.a(aVar);
                }
            }

            public void b() {
                throw new IllegalStateException("Native ads manager their own impressions.");
            }
        });
        this.k.a(str);
    }

    public void a(boolean z2) {
        this.A = z2;
    }

    @Nullable
    public c b() {
        if (!f() || this.a == null) {
            return null;
        }
        return this.a.E();
    }

    public void b(String str) {
        if (this.a != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("eil", String.valueOf(true));
            hashMap.put("eil_source", str);
            this.a.b(hashMap);
        }
    }

    public void b(boolean z2) {
        this.y = z2;
    }

    public void c() {
        if (this.v != null) {
            this.v.b();
            this.v = null;
        }
        if (this.k != null) {
            this.k.b(true);
            this.k = null;
        }
    }

    public void c(boolean z2) {
        this.z = z2;
    }

    public int d() {
        com.facebook.ads.internal.h.d a2;
        if (this.m != null) {
            a2 = this.m;
        } else if (this.k == null || this.k.a() == null) {
            return 1;
        } else {
            a2 = this.k.a();
        }
        return a2.f();
    }

    public String e() {
        return this.f;
    }

    public boolean f() {
        return this.a != null && this.a.c_();
    }

    public boolean g() {
        return f() && this.a.g();
    }

    public boolean h() {
        return this.a != null && this.a.a_();
    }

    public f i() {
        if (!f()) {
            return null;
        }
        return this.a.l();
    }

    public f j() {
        if (!f()) {
            return null;
        }
        return this.a.m();
    }

    public h k() {
        if (!f()) {
            return null;
        }
        return this.a.n();
    }

    public String l() {
        if (!f()) {
            return null;
        }
        return this.a.o();
    }

    public String m() {
        if (!f()) {
            return null;
        }
        return this.a.p();
    }

    public String n() {
        if (!f()) {
            return null;
        }
        return this.a.q();
    }

    public String o() {
        if (!f()) {
            return null;
        }
        return this.a.G();
    }

    public String p() {
        if (!f()) {
            return null;
        }
        return this.a.r();
    }

    public String q() {
        if (!f()) {
            return null;
        }
        return this.a.s();
    }

    public g r() {
        if (!f()) {
            return null;
        }
        return this.a.t();
    }

    public String s() {
        if (!f()) {
            return null;
        }
        return this.g;
    }

    public f t() {
        if (!f()) {
            return null;
        }
        return this.a.u();
    }

    public String u() {
        if (!f()) {
            return null;
        }
        return this.a.v();
    }

    public String v() {
        if (!f()) {
            return null;
        }
        return this.a.w();
    }

    public String w() {
        if (!f() || TextUtils.isEmpty(this.a.x())) {
            return null;
        }
        return this.h.b(this.a.x());
    }

    public String x() {
        if (!f()) {
            return null;
        }
        return this.a.y();
    }

    public String y() {
        if (!f()) {
            return null;
        }
        return this.a.A();
    }

    public j z() {
        return !f() ? j.DEFAULT : this.a.z();
    }
}
