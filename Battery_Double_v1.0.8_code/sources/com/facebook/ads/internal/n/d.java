package com.facebook.ads.internal.n;

import java.util.EnumSet;

public enum d {
    NONE(0),
    ICON(1),
    IMAGE(2),
    VIDEO(3);
    
    public static final EnumSet<d> e = null;
    private final long f;

    static {
        e = EnumSet.allOf(d.class);
    }

    private d(long j) {
        this.f = j;
    }

    public long a() {
        return this.f;
    }
}
