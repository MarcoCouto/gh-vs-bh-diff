package com.facebook.ads.internal.a;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.c.g;

public class i extends a {
    private static final String d = "i";
    private final Uri e;

    public i(Context context, c cVar, String str, Uri uri) {
        super(context, cVar, str);
        this.e = uri;
    }

    public C0004a a() {
        return C0004a.OPEN_LINK;
    }

    public void b() {
        try {
            Log.w("REDIRECTACTION: ", this.e.toString());
            g.a(new g(), this.a, this.e, this.c);
        } catch (Exception e2) {
            String str = d;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to open link url: ");
            sb.append(this.e.toString());
            Log.d(str, sb.toString(), e2);
        }
    }
}
