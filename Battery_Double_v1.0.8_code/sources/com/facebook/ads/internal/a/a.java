package com.facebook.ads.internal.a;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.d;
import java.util.Map;

public abstract class a {
    protected final Context a;
    protected final c b;
    protected final String c;

    public a(Context context, c cVar, String str) {
        this.a = context;
        this.b = cVar;
        this.c = str;
    }

    public abstract C0004a a();

    /* access modifiers changed from: protected */
    public void a(Map<String, String> map) {
        if (!TextUtils.isEmpty(this.c)) {
            if (this instanceof e) {
                this.b.g(this.c, map);
            } else {
                this.b.c(this.c, map);
            }
        }
        d.a(this.a, "Click logged");
    }

    public abstract void b();
}
