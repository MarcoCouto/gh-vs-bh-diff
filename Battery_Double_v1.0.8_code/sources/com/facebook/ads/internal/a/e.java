package com.facebook.ads.internal.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.c.g;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e extends a {
    private static final String d = "e";
    private final Uri e;
    private final Map<String, String> f;

    public e(Context context, c cVar, String str, Uri uri, Map<String, String> map) {
        super(context, cVar, str);
        this.e = uri;
        this.f = map;
    }

    private Intent a(f fVar) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(ErrorDialogData.BINDER_CRASH);
        if (!TextUtils.isEmpty(fVar.a()) && !TextUtils.isEmpty(fVar.b())) {
            intent.setComponent(new ComponentName(fVar.a(), fVar.b()));
        }
        if (!TextUtils.isEmpty(fVar.c())) {
            intent.setData(Uri.parse(fVar.c()));
        }
        return intent;
    }

    private Intent b(f fVar) {
        if (TextUtils.isEmpty(fVar.a()) || !d.a(this.a, fVar.a())) {
            return null;
        }
        String c = fVar.c();
        if (!TextUtils.isEmpty(c) && (c.startsWith("tel:") || c.startsWith("telprompt:"))) {
            return new Intent("android.intent.action.CALL", Uri.parse(c));
        }
        PackageManager packageManager = this.a.getPackageManager();
        if (TextUtils.isEmpty(fVar.b()) && TextUtils.isEmpty(c)) {
            return packageManager.getLaunchIntentForPackage(fVar.a());
        }
        Intent a = a(fVar);
        List queryIntentActivities = packageManager.queryIntentActivities(a, 65536);
        if (a.getComponent() == null) {
            Iterator it = queryIntentActivities.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ResolveInfo resolveInfo = (ResolveInfo) it.next();
                if (resolveInfo.activityInfo.packageName.equals(fVar.a())) {
                    a.setComponent(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name));
                    break;
                }
            }
        }
        if (queryIntentActivities.isEmpty() || a.getComponent() == null) {
            return null;
        }
        return a;
    }

    private List<f> e() {
        String queryParameter = this.e.getQueryParameter("appsite_data");
        if (TextUtils.isEmpty(queryParameter) || "[]".equals(queryParameter)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray optJSONArray = new JSONObject(queryParameter).optJSONArray("android");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    f a = f.a(optJSONArray.optJSONObject(i));
                    if (a != null) {
                        arrayList.add(a);
                    }
                }
            }
        } catch (JSONException e2) {
            Log.w(d, "Error parsing appsite_data", e2);
        }
        return arrayList;
    }

    private boolean f() {
        List<Intent> d2 = d();
        if (d2 == null) {
            return false;
        }
        for (Intent startActivity : d2) {
            try {
                this.a.startActivity(startActivity);
                return true;
            } catch (Exception e2) {
                Log.d(d, "Failed to open app intent, falling back", e2);
            }
        }
        return false;
    }

    private boolean g() {
        g gVar = new g();
        try {
            g.a(gVar, this.a, c(), this.c);
            return true;
        } catch (Exception e2) {
            String str = d;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to open market url: ");
            sb.append(this.e.toString());
            Log.d(str, sb.toString(), e2);
            String queryParameter = this.e.getQueryParameter("store_url_web_fallback");
            if (queryParameter != null && queryParameter.length() > 0) {
                try {
                    g.a(gVar, this.a, Uri.parse(queryParameter), this.c);
                } catch (Exception e3) {
                    String str2 = d;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to open fallback url: ");
                    sb2.append(queryParameter);
                    Log.d(str2, sb2.toString(), e3);
                }
            }
            return false;
        }
    }

    public C0004a a() {
        return C0004a.OPEN_STORE;
    }

    public void b() {
        String str = "opened_deeplink";
        if (!f()) {
            str = g() ? "opened_store_url" : "opened_store_fallback_url";
        }
        this.f.put(str, String.valueOf(true));
        a(this.f);
    }

    /* access modifiers changed from: protected */
    public Uri c() {
        String queryParameter = this.e.getQueryParameter("store_url");
        if (!TextUtils.isEmpty(queryParameter)) {
            return Uri.parse(queryParameter);
        }
        return Uri.parse(String.format("market://details?id=%s", new Object[]{this.e.getQueryParameter("store_id")}));
    }

    /* access modifiers changed from: protected */
    public List<Intent> d() {
        List<f> e2 = e();
        ArrayList arrayList = new ArrayList();
        if (e2 != null) {
            for (f b : e2) {
                Intent b2 = b(b);
                if (b2 != null) {
                    arrayList.add(b2);
                }
            }
        }
        return arrayList;
    }
}
