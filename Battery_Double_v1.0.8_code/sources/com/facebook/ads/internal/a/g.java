package com.facebook.ads.internal.a;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import java.util.Map;

class g extends a {
    private static final String d = "g";
    private final Uri e;
    private final Map<String, String> f;

    g(Context context, c cVar, String str, Uri uri, Map<String, String> map) {
        super(context, cVar, str);
        this.e = uri;
        this.f = map;
    }

    public C0004a a() {
        return C0004a.OPEN_LINK;
    }

    public void b() {
        a(this.f);
        try {
            com.facebook.ads.internal.q.c.g.a(new com.facebook.ads.internal.q.c.g(), this.a, Uri.parse(this.e.getQueryParameter("link")), this.c);
        } catch (Exception e2) {
            String str = d;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to open link url: ");
            sb.append(this.e.toString());
            Log.d(str, sb.toString(), e2);
        }
    }
}
