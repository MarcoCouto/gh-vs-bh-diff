package com.facebook.ads.internal.a;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.internal.m.c;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class b {
    private static final String a = "b";

    @Nullable
    public static a a(Context context, c cVar, String str, Uri uri, Map<String, String> map) {
        if (uri == null) {
            return null;
        }
        String authority = uri.getAuthority();
        String queryParameter = uri.getQueryParameter("video_url");
        if (!TextUtils.isEmpty(uri.getQueryParameter("data"))) {
            try {
                JSONObject jSONObject = new JSONObject(uri.getQueryParameter("data"));
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str2 = (String) keys.next();
                    map.put(str2, jSONObject.getString(str2));
                }
            } catch (JSONException e) {
                Log.w(a, "Unable to parse json data in AdActionFactory.", e);
            }
        }
        char c = 65535;
        int hashCode = authority.hashCode();
        if (hashCode != -1458789996) {
            if (hashCode != 109770977) {
                if (hashCode == 1546100943 && authority.equals("open_link")) {
                    c = 1;
                }
            } else if (authority.equals("store")) {
                c = 0;
            }
        } else if (authority.equals("passthrough")) {
            c = 2;
        }
        switch (c) {
            case 0:
                if (queryParameter != null) {
                    return null;
                }
                e eVar = new e(context, cVar, str, uri, map);
                return eVar;
            case 1:
                g gVar = new g(context, cVar, str, uri, map);
                return gVar;
            case 2:
                h hVar = new h(context, cVar, str, uri, map);
                return hVar;
            default:
                return new i(context, cVar, str, uri);
        }
    }

    public static boolean a(String str) {
        return "store".equalsIgnoreCase(str) || "open_link".equalsIgnoreCase(str);
    }
}
