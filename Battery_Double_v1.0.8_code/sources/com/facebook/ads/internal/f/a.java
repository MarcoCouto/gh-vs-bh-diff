package com.facebook.ads.internal.f;

import com.facebook.ads.internal.q.a.m;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.HashMap;
import java.util.Map;

public class a extends d {
    public a(String str, String str2) {
        super(m.b(), m.c(), a(str, str2));
    }

    private static Map<String, String> a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("key", str);
        hashMap.put(Param.VALUE, str2);
        return hashMap;
    }

    public String a() {
        return "client_response";
    }
}
