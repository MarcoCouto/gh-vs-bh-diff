package com.facebook.ads.internal.f;

import android.content.Context;
import android.os.Process;
import android.support.annotation.Nullable;
import com.facebook.ads.BuildConfig;
import com.facebook.ads.internal.q.a.m;
import com.facebook.ads.internal.q.a.p;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Map;

public class c implements UncaughtExceptionHandler {
    private final UncaughtExceptionHandler a;
    private final Context b;
    private final Map<String, String> c;

    public c(@Nullable UncaughtExceptionHandler uncaughtExceptionHandler, Context context, Map<String, String> map) {
        this.a = uncaughtExceptionHandler;
        if (context == null) {
            throw new IllegalArgumentException("Missing Context");
        }
        this.b = context.getApplicationContext();
        this.c = map;
    }

    private void a(Thread thread, Throwable th) {
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
            return;
        }
        try {
            Process.killProcess(Process.myPid());
        } catch (Throwable unused) {
        }
        try {
            System.exit(10);
        } catch (Throwable unused2) {
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            String a2 = p.a(th);
            if (a2 != null && a2.contains(BuildConfig.APPLICATION_ID)) {
                e.a(new d(m.b(), m.c(), new b(a2, this.c).a()), this.b);
            }
        } catch (Exception unused) {
        }
        a(thread, th);
    }
}
