package com.facebook.ads.internal.r;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.facebook.ads.internal.q.a.r;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.q.a.w;
import com.facebook.ads.internal.q.a.x;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import org.json.JSONObject;

public class a {
    private static final String a = "a";
    /* access modifiers changed from: private */
    public final View b;
    /* access modifiers changed from: private */
    public final int c;
    /* access modifiers changed from: private */
    public final int d;
    /* access modifiers changed from: private */
    public final Handler e;
    /* access modifiers changed from: private */
    public final WeakReference<C0006a> f;
    /* access modifiers changed from: private */
    public final boolean g;
    /* access modifiers changed from: private */
    public Runnable h;
    private int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public b l;
    /* access modifiers changed from: private */
    public Map<String, Integer> m;
    /* access modifiers changed from: private */
    public long n;
    /* access modifiers changed from: private */
    public int o;

    /* renamed from: com.facebook.ads.internal.r.a$a reason: collision with other inner class name */
    public static abstract class C0006a {
        public abstract void a();

        public void b() {
        }
    }

    private static final class b extends w<a> {
        b(a aVar) {
            super(aVar);
        }

        public void run() {
            a aVar = (a) a();
            if (aVar != null) {
                View a = aVar.b;
                C0006a aVar2 = (C0006a) aVar.f.get();
                if (a != null && aVar2 != null) {
                    b a2 = a.a(a, aVar.c);
                    int i = 0;
                    if (a2.a()) {
                        aVar.o = aVar.o + 1;
                    } else {
                        aVar.o = 0;
                    }
                    boolean z = aVar.o > aVar.d;
                    boolean z2 = aVar.l != null && aVar.l.a();
                    if (z || !a2.a()) {
                        aVar.l = a2;
                    }
                    String valueOf = String.valueOf(a2.b());
                    synchronized (aVar) {
                        if (aVar.m.containsKey(valueOf)) {
                            i = ((Integer) aVar.m.get(valueOf)).intValue();
                        }
                        aVar.m.put(valueOf, Integer.valueOf(i + 1));
                    }
                    if (z && !z2) {
                        aVar.n = System.currentTimeMillis();
                        aVar2.a();
                        if (!aVar.g) {
                            return;
                        }
                    } else if (!z && z2) {
                        aVar2.b();
                    }
                    if (!aVar.k && aVar.h != null) {
                        aVar.e.postDelayed(aVar.h, (long) aVar.j);
                    }
                }
            }
        }
    }

    public a(View view, int i2, int i3, boolean z, C0006a aVar) {
        this.e = new Handler();
        this.i = 0;
        this.j = 1000;
        this.k = true;
        this.l = new b(c.UNKNOWN);
        this.m = new HashMap();
        this.n = 0;
        this.o = 0;
        this.b = view;
        if (view.getId() == -1) {
            v.a(view);
        }
        this.c = i2;
        this.f = new WeakReference<>(aVar);
        this.g = z;
        if (i3 < 0) {
            i3 = 0;
        }
        this.d = i3;
    }

    public a(View view, int i2, C0006a aVar) {
        this(view, i2, 0, false, aVar);
    }

    public a(View view, int i2, boolean z, C0006a aVar) {
        this(view, i2, 0, z, aVar);
    }

    private static int a(Vector<Rect> vector) {
        int size = vector.size();
        int i2 = size * 2;
        int[] iArr = new int[i2];
        int[] iArr2 = new int[i2];
        boolean[][] zArr = (boolean[][]) Array.newInstance(boolean.class, new int[]{i2, i2});
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < size) {
            Rect rect = (Rect) vector.elementAt(i3);
            int i6 = i4 + 1;
            iArr[i4] = rect.left;
            int i7 = i5 + 1;
            iArr2[i5] = rect.bottom;
            int i8 = i6 + 1;
            iArr[i6] = rect.right;
            int i9 = i7 + 1;
            iArr2[i7] = rect.top;
            i3++;
            i4 = i8;
            i5 = i9;
        }
        Arrays.sort(iArr);
        Arrays.sort(iArr2);
        for (int i10 = 0; i10 < size; i10++) {
            Rect rect2 = (Rect) vector.elementAt(i10);
            int a2 = a(iArr, rect2.left);
            int a3 = a(iArr, rect2.right);
            int a4 = a(iArr2, rect2.top);
            int a5 = a(iArr2, rect2.bottom);
            for (int i11 = a2 + 1; i11 <= a3; i11++) {
                for (int i12 = a4 + 1; i12 <= a5; i12++) {
                    zArr[i11][i12] = true;
                }
            }
        }
        int i13 = 0;
        int i14 = 0;
        while (i13 < i2) {
            int i15 = i14;
            for (int i16 = 0; i16 < i2; i16++) {
                i15 += zArr[i13][i16] ? (iArr[i13] - iArr[i13 - 1]) * (iArr2[i16] - iArr2[i16 - 1]) : 0;
            }
            i13++;
            i14 = i15;
        }
        return i14;
    }

    private static int a(int[] iArr, int i2) {
        int i3 = 0;
        int length = iArr.length;
        while (i3 < length) {
            int i4 = ((length - i3) / 2) + i3;
            if (iArr[i4] == i2) {
                return i4;
            }
            if (iArr[i4] > i2) {
                length = i4;
            } else {
                i3 = i4 + 1;
            }
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x019d  */
    public static b a(View view, int i2) {
        DisplayMetrics displayMetrics;
        float f2;
        boolean z;
        int i3;
        float f3;
        View view2 = view;
        if (view2 == null) {
            a(null, false, "adView is null.");
            return new b(c.AD_IS_NULL);
        } else if (view.getParent() == null) {
            a(view2, false, "adView has no parent.");
            return new b(c.INVALID_PARENT);
        } else if (!view.isShown()) {
            a(view2, false, "adView parent is not set to VISIBLE.");
            return new b(c.INVALID_PARENT);
        } else if (view.getWindowVisibility() != 0) {
            a(view2, false, "adView window is not set to VISIBLE.");
            return new b(c.INVALID_WINDOW);
        } else if (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("adView has invisible dimensions (w=");
            sb.append(view.getMeasuredWidth());
            sb.append(", h=");
            sb.append(view.getMeasuredHeight());
            a(view2, false, sb.toString());
            return new b(c.INVALID_DIMENSIONS);
        } else if (view.getAlpha() < 0.9f) {
            a(view2, false, "adView is too transparent.");
            return new b(c.AD_IS_TRANSPARENT);
        } else {
            int width = view.getWidth();
            int height = view.getHeight();
            int[] iArr = new int[2];
            try {
                view2.getLocationOnScreen(iArr);
                Rect rect = new Rect();
                if (!view2.getGlobalVisibleRect(rect)) {
                    return new b(c.AD_IS_NOT_VISIBLE);
                }
                Context context = view.getContext();
                if (VERSION.SDK_INT >= 17) {
                    Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
                    displayMetrics = new DisplayMetrics();
                    defaultDisplay.getRealMetrics(displayMetrics);
                } else {
                    displayMetrics = context.getResources().getDisplayMetrics();
                }
                if (com.facebook.ads.internal.l.a.p(context)) {
                    Rect rect2 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                    if (rect2.intersect(iArr[0], iArr[1], iArr[0] + width, iArr[1] + height)) {
                        f3 = ((float) (rect2.width() * rect2.height())) * 1.0f;
                        i3 = width * height;
                    } else {
                        f2 = 0.0f;
                        float o2 = ((float) com.facebook.ads.internal.l.a.o(context)) / 100.0f;
                        if (!com.facebook.ads.internal.l.a.n(context)) {
                            if (f2 < o2) {
                                a(view2, false, String.format(Locale.US, "adView visible area is too small [%.2f%% visible, current threshold %.2f%%]", new Object[]{Float.valueOf(f2), Float.valueOf(o2)}));
                                return new b(c.AD_INSUFFICIENT_VISIBLE_AREA, f2);
                            }
                            z = false;
                        } else if (iArr[0] < 0 || displayMetrics.widthPixels - iArr[0] < width) {
                            a(view2, false, "adView is not fully on screen horizontally.");
                            return new b(c.AD_OFFSCREEN_HORIZONTALLY, f2);
                        } else {
                            int i4 = (int) ((((double) height) * (100.0d - ((double) i2))) / 100.0d);
                            if (iArr[1] < 0 && Math.abs(iArr[1]) > i4) {
                                a(view2, false, "adView is not visible from the top.");
                                return new b(c.AD_OFFSCREEN_TOP, f2);
                            } else if ((iArr[1] + height) - displayMetrics.heightPixels > i4) {
                                a(view2, false, "adView is not visible from the bottom.");
                                return new b(c.AD_OFFSCREEN_BOTTOM, f2);
                            } else {
                                z = false;
                            }
                        }
                        if (com.facebook.ads.internal.q.e.a.b(context)) {
                            a(view2, z, "Screen is not interactive.");
                            return new b(c.SCREEN_NOT_INTERACTIVE, f2);
                        }
                        Map a2 = com.facebook.ads.internal.q.e.b.a(context);
                        if (x.b(a2)) {
                            a(view2, z, "Keyguard is obstructing view.");
                            return new b(c.AD_IS_OBSTRUCTED_BY_KEYGUARD, f2);
                        } else if (!com.facebook.ads.internal.l.a.c(context) || !x.a(a2)) {
                            Float a3 = com.facebook.ads.internal.l.a.q(context) ? d.a(view) : null;
                            if (a3 != null) {
                                if (a3.floatValue() == -1.0f) {
                                    a(view2, false, "adView is not in the top activity");
                                    return new b(c.AD_IS_NOT_IN_ACTIVITY);
                                } else if (a3.floatValue() == 0.0f) {
                                    a(view2, false, "adView is not visible");
                                    return new b(c.AD_IS_NOT_VISIBLE);
                                }
                            }
                            if (!com.facebook.ads.internal.l.a.r(context) || a3 == null || a3.floatValue() >= o2) {
                                a(view2, true, "adView is visible.");
                                return new b(c.IS_VIEWABLE, f2, a2);
                            }
                            a(view2, false, String.format(Locale.US, "adView visible area is too small [%.2f%% visible, current threshold %.2f%%]", new Object[]{a3, Float.valueOf(o2)}));
                            return new b(c.AD_INSUFFICIENT_VISIBLE_AREA, f2, a2);
                        } else {
                            a(view2, z, "Ad is on top of the Lockscreen.");
                            return new b(c.AD_IN_LOCKSCREEN, f2, a2);
                        }
                    }
                } else {
                    Vector a4 = a(view);
                    int a5 = a(a4);
                    a4.add(rect);
                    i3 = view.getMeasuredHeight() * view.getMeasuredWidth();
                    f3 = ((float) (a(a4) - a5)) * 1.0f;
                }
                f2 = f3 / ((float) i3);
                float o22 = ((float) com.facebook.ads.internal.l.a.o(context)) / 100.0f;
                if (!com.facebook.ads.internal.l.a.n(context)) {
                }
                if (com.facebook.ads.internal.q.e.a.b(context)) {
                }
            } catch (NullPointerException unused) {
                a(view2, false, "Cannot get location on screen.");
                return new b(c.INVALID_DIMENSIONS);
            }
        }
    }

    private static Vector<Rect> a(View view) {
        Vector<Rect> vector = new Vector<>();
        if (!(view.getParent() instanceof ViewGroup)) {
            return vector;
        }
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        int indexOfChild = viewGroup.indexOfChild(view);
        while (true) {
            indexOfChild++;
            if (indexOfChild < viewGroup.getChildCount()) {
                vector.addAll(b(viewGroup.getChildAt(indexOfChild)));
            } else {
                vector.addAll(a((View) viewGroup));
                return vector;
            }
        }
    }

    private static void a(View view, boolean z, String str) {
    }

    private static Vector<Rect> b(View view) {
        Vector<Rect> vector = new Vector<>();
        if (!view.isShown() || (VERSION.SDK_INT >= 11 && view.getAlpha() <= 0.0f)) {
            return vector;
        }
        if (!(view instanceof ViewGroup) || !c(view)) {
            Rect rect = new Rect();
            if (view.getGlobalVisibleRect(rect)) {
                vector.add(rect);
            }
            return vector;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
            vector.addAll(b(viewGroup.getChildAt(i2)));
        }
        return vector;
    }

    private static boolean c(View view) {
        return view.getBackground() == null || (VERSION.SDK_INT >= 19 && view.getBackground().getAlpha() <= 0);
    }

    public synchronized void a() {
        if (this.h != null) {
            b();
        }
        this.h = new b(this);
        this.e.postDelayed(this.h, (long) this.i);
        this.k = false;
        this.o = 0;
        this.l = new b(c.UNKNOWN);
        this.m = new HashMap();
    }

    public void a(int i2) {
        this.i = i2;
    }

    public synchronized void a(Map<String, String> map) {
        map.put("vrc", String.valueOf(this.l.b()));
        map.put("vp", String.valueOf(this.l.c()));
        map.put("vh", new JSONObject(this.m).toString());
        map.put("vt", r.a(this.n));
        map.putAll(this.l.d());
    }

    public synchronized void b() {
        this.e.removeCallbacks(this.h);
        this.h = null;
        this.k = true;
        this.o = 0;
    }

    public void b(int i2) {
        this.j = i2;
    }

    public synchronized String c() {
        StringBuilder sb;
        c cVar = c.values()[this.l.b()];
        sb = new StringBuilder();
        sb.append(cVar.toString());
        sb.append(String.format(" (%.1f%%)", new Object[]{Float.valueOf(this.l.c() * 100.0f)}));
        return sb.toString();
    }
}
