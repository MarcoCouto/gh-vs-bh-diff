package com.facebook.ads.internal.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.ads.internal.l.a;
import com.facebook.ads.internal.p.a.p;
import com.facebook.ads.internal.q.c.d;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class c {
    private static final String a = "c";
    private static c b;
    private final Context c;

    private c(Context context) {
        this.c = context;
    }

    private Bitmap a(String str) {
        byte[] d = d.a(this.c).a(str, (p) null).d();
        return BitmapFactory.decodeByteArray(d, 0, d.length);
    }

    public static c a(Context context) {
        if (b == null) {
            Context applicationContext = context.getApplicationContext();
            synchronized (c.class) {
                if (b == null) {
                    b = new c(applicationContext);
                }
            }
        }
        return b;
    }

    private static void a(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:34:0x0074=Splitter:B:34:0x0074, B:49:0x00ab=Splitter:B:49:0x00ab} */
    private void a(String str, Bitmap bitmap) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        File cacheDir = this.c.getCacheDir();
        StringBuilder sb = new StringBuilder();
        sb.append(str.hashCode());
        sb.append(".png");
        File file = new File(cacheDir, sb.toString());
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
            try {
                bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream2);
                if (byteArrayOutputStream2.size() >= 3145728) {
                    Log.d(a, "Bitmap size exceeds max size for storage");
                    a((Closeable) byteArrayOutputStream2);
                    a((Closeable) null);
                    return;
                }
                fileOutputStream = new FileOutputStream(file);
                try {
                    byteArrayOutputStream2.writeTo(fileOutputStream);
                    fileOutputStream.flush();
                    a((Closeable) byteArrayOutputStream2);
                } catch (FileNotFoundException e) {
                    e = e;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    String str2 = a;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Bad output destination (file=");
                    sb2.append(file.getAbsolutePath());
                    sb2.append(").");
                    Log.e(str2, sb2.toString(), e);
                    a((Closeable) byteArrayOutputStream);
                    a((Closeable) fileOutputStream);
                } catch (IOException e2) {
                    byteArrayOutputStream = byteArrayOutputStream2;
                    Throwable th = e2;
                    fileOutputStream2 = fileOutputStream;
                    e = th;
                    try {
                        String str3 = a;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Unable to write bitmap to file (url=");
                        sb3.append(str);
                        sb3.append(").");
                        Log.e(str3, sb3.toString(), e);
                        a((Closeable) byteArrayOutputStream);
                        a((Closeable) fileOutputStream2);
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = fileOutputStream2;
                        a((Closeable) byteArrayOutputStream);
                        a((Closeable) fileOutputStream);
                        throw th;
                    }
                } catch (OutOfMemoryError e3) {
                    e = e3;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    try {
                        Log.e(a, "Unable to write bitmap to output stream", e);
                        a((Closeable) byteArrayOutputStream);
                        a((Closeable) fileOutputStream);
                    } catch (Throwable th3) {
                        th = th3;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    a((Closeable) byteArrayOutputStream);
                    a((Closeable) fileOutputStream);
                    throw th;
                }
                a((Closeable) fileOutputStream);
            } catch (FileNotFoundException e4) {
                e = e4;
                fileOutputStream = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                String str22 = a;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("Bad output destination (file=");
                sb22.append(file.getAbsolutePath());
                sb22.append(").");
                Log.e(str22, sb22.toString(), e);
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream);
            } catch (IOException e5) {
                e = e5;
                fileOutputStream2 = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                String str32 = a;
                StringBuilder sb32 = new StringBuilder();
                sb32.append("Unable to write bitmap to file (url=");
                sb32.append(str);
                sb32.append(").");
                Log.e(str32, sb32.toString(), e);
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream2);
            } catch (OutOfMemoryError e6) {
                e = e6;
                fileOutputStream = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                Log.e(a, "Unable to write bitmap to output stream", e);
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream);
            } catch (Throwable th5) {
                th = th5;
                fileOutputStream = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream);
                throw th;
            }
        } catch (FileNotFoundException e7) {
            e = e7;
            fileOutputStream = null;
            String str222 = a;
            StringBuilder sb222 = new StringBuilder();
            sb222.append("Bad output destination (file=");
            sb222.append(file.getAbsolutePath());
            sb222.append(").");
            Log.e(str222, sb222.toString(), e);
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream);
        } catch (IOException e8) {
            e = e8;
            fileOutputStream2 = null;
            String str322 = a;
            StringBuilder sb322 = new StringBuilder();
            sb322.append("Unable to write bitmap to file (url=");
            sb322.append(str);
            sb322.append(").");
            Log.e(str322, sb322.toString(), e);
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream2);
        } catch (OutOfMemoryError e9) {
            e = e9;
            fileOutputStream = null;
            Log.e(a, "Unable to write bitmap to output stream", e);
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream);
        } catch (Throwable th6) {
            th = th6;
            fileOutputStream = null;
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream);
            throw th;
        }
    }

    private boolean a(int i, int i2) {
        return i > 0 && i2 > 0 && a.d(this.c);
    }

    @Nullable
    private Bitmap b(String str, int i, int i2) {
        try {
            Bitmap a2 = a(i, i2) ? com.facebook.ads.internal.q.b.c.a(str.substring("file://".length()), i, i2) : BitmapFactory.decodeStream(new FileInputStream(str.substring("file://".length())), null, null);
            a(str, a2);
            return a2;
        } catch (IOException e) {
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to copy local image into cache (url=");
            sb.append(str);
            sb.append(").");
            Log.e(str2, sb.toString(), e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f  */
    @Nullable
    private Bitmap c(String str, int i, int i2) {
        Bitmap bitmap;
        InputStream inputStream;
        if (str.startsWith("asset:///")) {
            InputStream inputStream2 = null;
            try {
                inputStream = this.c.getAssets().open(str.substring(9, str.length()));
                try {
                    bitmap = a(i, i2) ? com.facebook.ads.internal.q.b.c.a(inputStream, i, i2) : BitmapFactory.decodeStream(inputStream);
                    if (inputStream != null) {
                        a((Closeable) inputStream);
                    }
                } catch (IOException unused) {
                    if (inputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    inputStream2 = inputStream;
                    if (inputStream2 != null) {
                    }
                    throw th;
                }
            } catch (IOException unused2) {
                inputStream = null;
                if (inputStream != null) {
                    a((Closeable) inputStream);
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (inputStream2 != null) {
                    a((Closeable) inputStream2);
                }
                throw th;
            }
        } else {
            if (a(i, i2)) {
                try {
                    bitmap = d(str, i, i2);
                } catch (IOException unused3) {
                }
            }
            bitmap = a(str);
        }
        a(str, bitmap);
        return bitmap;
    }

    private Bitmap d(String str, int i, int i2) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoInput(true);
        httpURLConnection.connect();
        InputStream inputStream = httpURLConnection.getInputStream();
        Bitmap a2 = com.facebook.ads.internal.q.b.c.a(inputStream, i, i2);
        a((Closeable) inputStream);
        return a2;
    }

    @Nullable
    public Bitmap a(String str, int i, int i2) {
        File cacheDir = this.c.getCacheDir();
        StringBuilder sb = new StringBuilder();
        sb.append(str.hashCode());
        sb.append(".png");
        File file = new File(cacheDir, sb.toString());
        return !file.exists() ? str.startsWith("file://") ? b(str, i, i2) : c(str, i, i2) : a(i, i2) ? com.facebook.ads.internal.q.b.c.a(file.getAbsolutePath(), i, i2) : BitmapFactory.decodeFile(file.getAbsolutePath());
    }
}
