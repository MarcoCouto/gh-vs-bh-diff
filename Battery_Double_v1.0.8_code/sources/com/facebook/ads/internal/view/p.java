package com.facebook.ads.internal.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.h;

public abstract class p {
    public static LinearLayout a(Context context, e eVar, h hVar) {
        LinearLayout linearLayout = new LinearLayout(context);
        s sVar = new s(context);
        sVar.setText(eVar.q());
        b(sVar, hVar);
        linearLayout.addView(sVar);
        return linearLayout;
    }

    public static void a(TextView textView, h hVar) {
        textView.setTextColor(hVar.c());
        textView.setTextSize((float) hVar.h());
        textView.setTypeface(hVar.a(), 1);
    }

    public static void b(TextView textView, h hVar) {
        textView.setTextColor(hVar.d());
        textView.setTextSize((float) hVar.i());
        textView.setTypeface(hVar.a());
    }
}
