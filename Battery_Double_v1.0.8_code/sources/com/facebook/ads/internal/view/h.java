package com.facebook.ads.internal.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Build.VERSION;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.b.d;
import com.facebook.ads.internal.view.component.a;
import com.facebook.ads.internal.view.component.i;
import java.util.Map;

public class h extends RelativeLayout {
    private final Paint a;
    private final float b;
    private final float c;
    private final float d;
    private LinearLayout e;
    private i f;
    private a g;
    private u h;
    private c i;
    private C0008a j;
    private final String k;

    public h(Context context, j jVar, boolean z, c cVar, C0008a aVar, String str) {
        super(context);
        this.i = cVar;
        this.j = aVar;
        this.k = str;
        float f2 = getResources().getDisplayMetrics().density;
        this.b = 1.0f * f2;
        this.d = 4.0f * f2;
        this.c = 6.0f * f2;
        setGravity(17);
        setPadding((int) this.b, 0, (int) this.b, (int) this.b);
        v.a((View) this, 0);
        if (z) {
            b(context, f2, jVar);
        } else {
            a(context, f2, jVar);
        }
        this.a = new Paint();
        this.a.setColor(ViewCompat.MEASURED_STATE_MASK);
        this.a.setStyle(Style.FILL);
        this.a.setAlpha(16);
        this.a.setAntiAlias(true);
        if (VERSION.SDK_INT < 18) {
            setLayerType(1, null);
        }
    }

    private void a(Context context, float f2, j jVar) {
        Context context2 = context;
        this.h = new u(context2);
        this.h.setLayoutParams(new LayoutParams(-1, -2));
        v.a((View) this.h);
        i iVar = new i(context2, jVar, false, false, true);
        this.f = iVar;
        this.f.setAlignment(3);
        this.f.setLayoutParams(new LayoutParams(-1, -2));
        this.f.setPadding(0, 0, 0, (int) (20.0f * f2));
        a aVar = new a(context2, true, false, "com.facebook.ads.interstitial.clicked", jVar, this.i, this.j);
        this.g = aVar;
        this.g.setLayoutParams(new LayoutParams(-1, -2));
        this.e = new LinearLayout(context2);
        if (VERSION.SDK_INT >= 16) {
            this.e.setBackground(new ColorDrawable(-1));
        } else {
            this.e.setBackgroundDrawable(new ColorDrawable(-1));
        }
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(3, this.h.getId());
        this.e.setLayoutParams(layoutParams);
        this.e.setOrientation(1);
        int i2 = (int) (16.0f * f2);
        this.e.setPadding(i2, i2, i2, i2);
        this.e.addView(this.f);
        this.e.addView(this.g);
        addView(this.h);
        addView(this.e);
    }

    private void b(Context context, float f2, j jVar) {
        u uVar;
        int a2;
        this.h = new u(context);
        this.h.setLayoutParams(new LayoutParams(-1, -2));
        if (VERSION.SDK_INT >= 17) {
            uVar = this.h;
            a2 = View.generateViewId();
        } else {
            uVar = this.h;
            a2 = v.a();
        }
        uVar.setId(a2);
        i iVar = new i(context, jVar, true, true, true);
        this.f = iVar;
        this.f.setAlignment(3);
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(8, this.h.getId());
        int i2 = (int) (12.0f * f2);
        this.f.setLayoutParams(layoutParams);
        this.f.setPadding(i2, i2, i2, i2);
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, -15658735});
        gradientDrawable.setCornerRadius(0.0f);
        v.a((View) this.f, (Drawable) gradientDrawable);
        a aVar = new a(context, false, false, "com.facebook.ads.interstitial.clicked", jVar, this.i, this.j);
        this.g = aVar;
        LayoutParams layoutParams2 = new LayoutParams(-1, -2);
        layoutParams2.addRule(3, this.h.getId());
        this.g.setLayoutParams(layoutParams2);
        addView(this.h);
        addView(this.f);
        addView(this.g);
    }

    public void a(String str, String str2) {
        this.f.a(str, str2, true, false);
    }

    public void a(String str, String str2, Map<String, String> map) {
        this.g.a(str, str2, this.k, map);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Path path = new Path();
        path.addRoundRect(new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight()), this.c, this.c, Direction.CW);
        canvas.drawPath(path, this.a);
        Path path2 = new Path();
        path2.addRoundRect(new RectF(this.b, 0.0f, ((float) getWidth()) - this.b, ((float) getHeight()) - this.b), this.d, this.d, Direction.CW);
        canvas.clipPath(path2);
        super.onDraw(canvas);
    }

    public void setImageUrl(String str) {
        new d((ImageView) this.h).a(str);
    }
}
