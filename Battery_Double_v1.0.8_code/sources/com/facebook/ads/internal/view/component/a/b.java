package com.facebook.ads.internal.view.component.a;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.component.a;
import com.facebook.ads.internal.view.component.i;
import com.github.mikephil.charting.utils.Utils;
import java.util.HashMap;

public abstract class b extends RelativeLayout {
    static final int a = ((int) (16.0f * v.b));
    static final int b = ((int) (28.0f * v.b));
    private final i c;
    private final a d;

    b(Context context, c cVar, C0008a aVar, j jVar, boolean z) {
        super(context);
        a aVar2 = new a(context, true, c(), "com.facebook.ads.interstitial.clicked", jVar, cVar, aVar);
        this.d = aVar2;
        v.a((View) this.d);
        i iVar = new i(getContext(), jVar, z, true, b());
        this.c = iVar;
        v.a((View) this.c);
    }

    public void a(String str, String str2, String str3, String str4, String str5, double d2) {
        this.c.a(str, str2, false, !a() && d2 > Utils.DOUBLE_EPSILON && d2 < 1.0d);
        this.d.a(str3, str4, str5, new HashMap());
    }

    public abstract boolean a();

    /* access modifiers changed from: protected */
    public boolean b() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public a getCtaButton() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public i getTextContainer() {
        return this.c;
    }
}
