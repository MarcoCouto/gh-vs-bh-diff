package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.view.View;
import android.widget.ImageView;
import com.facebook.ads.internal.q.a.v;

public class e extends ImageView {
    private static final int a = ((int) (8.0f * v.b));
    private final Path b = new Path();
    private final RectF c = new RectF();
    private int d = a;

    public e(Context context) {
        super(context);
        v.a((View) this, 0);
        if (VERSION.SDK_INT < 18) {
            setLayerType(1, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.c.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        this.b.reset();
        this.b.addRoundRect(this.c, (float) this.d, (float) this.d, Direction.CW);
        canvas.clipPath(this.b);
        super.onDraw(canvas);
    }

    public void setRadius(int i) {
        this.d = (int) (((float) i) * v.b);
    }
}
