package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.view.p;
import com.facebook.ads.internal.view.r;

public class b extends LinearLayout {
    private r a = new r(getContext(), 2);
    private int b;

    public b(Context context, e eVar, h hVar) {
        super(context);
        setOrientation(1);
        setVerticalGravity(16);
        this.a.setMinTextSize((float) (hVar.h() - 2));
        this.a.setText(eVar.l());
        p.a(this.a, hVar);
        this.a.setLayoutParams(new LayoutParams(-2, -2));
        addView(this.a);
        int i = 21;
        if (eVar.l() != null) {
            i = Math.min(eVar.l().length(), 21);
        }
        this.b = i;
        addView(p.a(context, eVar, hVar));
    }

    public int getMinVisibleTitleCharacters() {
        return this.b;
    }

    public TextView getTitleTextView() {
        return this.a;
    }
}
