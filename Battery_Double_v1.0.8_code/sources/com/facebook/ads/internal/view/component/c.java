package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.view.p;
import com.facebook.ads.internal.view.s;

public class c extends LinearLayout {
    private ImageView a;
    private b b;
    private TextView c;
    private LinearLayout d = new LinearLayout(getContext());

    public c(Context context, e eVar, h hVar, boolean z, int i) {
        h hVar2 = hVar;
        boolean z2 = z;
        super(context);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        setVerticalGravity(16);
        setOrientation(1);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        linearLayout.setGravity(16);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.setMargins(Math.round(displayMetrics.density * 15.0f), Math.round(displayMetrics.density * 15.0f), Math.round(displayMetrics.density * 15.0f), Math.round(displayMetrics.density * 15.0f));
        linearLayout.setLayoutParams(layoutParams);
        addView(linearLayout);
        LayoutParams layoutParams2 = new LayoutParams(-1, 0);
        this.d.setOrientation(0);
        this.d.setGravity(16);
        layoutParams2.weight = 3.0f;
        this.d.setLayoutParams(layoutParams2);
        linearLayout.addView(this.d);
        this.a = new g(getContext());
        float a2 = (float) a(z2, i);
        LayoutParams layoutParams3 = new LayoutParams(Math.round(displayMetrics.density * a2), Math.round(a2 * displayMetrics.density));
        layoutParams3.setMargins(0, 0, Math.round(displayMetrics.density * 15.0f), 0);
        this.a.setLayoutParams(layoutParams3);
        e.a(eVar.i(), this.a);
        this.d.addView(this.a);
        LinearLayout linearLayout2 = new LinearLayout(getContext());
        linearLayout2.setLayoutParams(new LayoutParams(-1, -1));
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(16);
        this.d.addView(linearLayout2);
        this.b = new b(getContext(), eVar, hVar2);
        LayoutParams layoutParams4 = new LayoutParams(-2, -1);
        layoutParams4.setMargins(0, 0, Math.round(15.0f * displayMetrics.density), 0);
        layoutParams4.weight = 0.5f;
        this.b.setLayoutParams(layoutParams4);
        linearLayout2.addView(this.b);
        this.c = new TextView(getContext());
        this.c.setPadding(Math.round(displayMetrics.density * 6.0f), Math.round(displayMetrics.density * 6.0f), Math.round(displayMetrics.density * 6.0f), Math.round(6.0f * displayMetrics.density));
        this.c.setText(eVar.p());
        this.c.setTextColor(hVar.f());
        this.c.setTextSize(14.0f);
        this.c.setTypeface(hVar.a(), 1);
        this.c.setMaxLines(2);
        this.c.setEllipsize(TruncateAt.END);
        this.c.setGravity(17);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(hVar.e());
        gradientDrawable.setCornerRadius(5.0f * displayMetrics.density);
        gradientDrawable.setStroke(1, hVar.g());
        this.c.setBackgroundDrawable(gradientDrawable);
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        layoutParams5.weight = 0.25f;
        this.c.setLayoutParams(layoutParams5);
        if (!eVar.h()) {
            this.c.setVisibility(4);
        }
        linearLayout2.addView(this.c);
        if (z2) {
            s sVar = new s(getContext());
            sVar.setText(eVar.n());
            p.b(sVar, hVar2);
            sVar.setMinTextSize((float) (hVar.i() - 1));
            LayoutParams layoutParams6 = new LayoutParams(-1, 0);
            layoutParams6.weight = 1.0f;
            sVar.setLayoutParams(layoutParams6);
            sVar.setGravity(80);
            linearLayout.addView(sVar);
        }
    }

    private int a(boolean z, int i) {
        return (int) (((double) (i - 30)) * (3.0d / ((double) (true + (z ? 1 : 0)))));
    }

    public TextView getCallToActionView() {
        return this.c;
    }

    public ImageView getIconView() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        TextView titleTextView = this.b.getTitleTextView();
        if (titleTextView.getLayout().getLineEnd(titleTextView.getLineCount() - 1) < this.b.getMinVisibleTitleCharacters()) {
            this.d.removeView(this.a);
            super.onMeasure(i, i2);
        }
    }
}
