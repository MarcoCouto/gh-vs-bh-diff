package com.facebook.ads.internal.view.component.a;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.graphics.ColorUtils;
import android.view.View;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.component.a;
import com.facebook.ads.internal.view.e.c.o;

public class c {
    private static final int a = v.a.heightPixels;
    private static final int b = v.a.widthPixels;

    private static int a(int i) {
        return (a - i) - ((v.a(16) + (a.a * 2)) + (2 * d.a));
    }

    public static b a(Context context, com.facebook.ads.internal.m.c cVar, C0008a aVar, View view, j jVar, j jVar2, int i, int i2, int i3, int i4) {
        return a(context, cVar, aVar, view, jVar, jVar2, i, i2, i3, i4, null, null);
    }

    public static b a(Context context, com.facebook.ads.internal.m.c cVar, C0008a aVar, View view, j jVar, j jVar2, int i, int i2, int i3, int i4, @Nullable o oVar, @Nullable View view2) {
        int i5 = i2;
        o oVar2 = oVar;
        j jVar3 = i5 == 1 ? jVar : jVar2;
        int i6 = i;
        boolean a2 = a(i5, i3, i4, i6);
        if (oVar2 != null) {
            oVar2.setProgressBarColor(ColorUtils.setAlphaComponent(jVar3.a(a2), 128));
        }
        if (a2) {
            a aVar2 = new a(context, cVar, aVar, view, oVar2, view2, i6, jVar3, i5 == 2);
            return aVar2;
        }
        d dVar = new d(context, cVar, aVar, view, oVar2, view2, a(i3, i4), jVar3);
        return dVar;
    }

    private static boolean a(int i, int i2) {
        return ((double) c(i, i2)) < 0.9d;
    }

    private static boolean a(int i, int i2, int i3) {
        return a(i3) < b(i, i2);
    }

    private static boolean a(int i, int i2, int i3, int i4) {
        return i == 2 || a(i2, i3, i4);
    }

    private static int b(int i, int i2) {
        return (int) (((float) (b - (2 * d.a))) / c(i, i2));
    }

    private static float c(int i, int i2) {
        if (i2 > 0) {
            return ((float) i) / ((float) i2);
        }
        return -1.0f;
    }
}
