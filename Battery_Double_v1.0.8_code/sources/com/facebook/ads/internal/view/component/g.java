package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.widget.ImageView;

public class g extends ImageView {
    public g(Context context) {
        this(context, 30.0f);
    }

    public g(Context context, float f) {
        super(context);
    }
}
