package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.p;

public class h extends LinearLayout {
    public h(Context context, e eVar, com.facebook.ads.internal.n.h hVar) {
        super(context);
        float f = v.b;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        linearLayout.setVerticalGravity(16);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        float f2 = 15.0f * f;
        layoutParams.setMargins(Math.round(f2), Math.round(f2), Math.round(f2), Math.round(f2));
        linearLayout.setLayoutParams(layoutParams);
        addView(linearLayout);
        String m = eVar.m();
        TextView textView = new TextView(getContext());
        if (TextUtils.isEmpty(m)) {
            m = eVar.l();
        }
        textView.setText(m);
        p.a(textView, hVar);
        textView.setEllipsize(TruncateAt.END);
        textView.setSingleLine(true);
        linearLayout.addView(textView);
        TextView textView2 = new TextView(getContext());
        textView2.setText(eVar.n());
        p.b(textView2, hVar);
        textView2.setEllipsize(TruncateAt.END);
        textView2.setMaxLines(2);
        linearLayout.addView(textView2);
    }
}
