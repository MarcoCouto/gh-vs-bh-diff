package com.facebook.ads.internal.view.component.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;

public class a extends b {
    public a(Context context, c cVar, C0008a aVar, View view, @Nullable View view2, @Nullable View view3, int i, j jVar, boolean z) {
        Context context2 = context;
        View view4 = view2;
        View view5 = view3;
        super(context2, cVar, aVar, jVar, true);
        RelativeLayout relativeLayout = new RelativeLayout(context2);
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(12);
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.BOTTOM_TOP, new int[]{-1778384896, 0});
        gradientDrawable.setCornerRadius(0.0f);
        gradientDrawable.setGradientType(0);
        v.a((View) relativeLayout, (Drawable) gradientDrawable);
        LinearLayout linearLayout = new LinearLayout(context2);
        linearLayout.setOrientation(z ^ true ? 1 : 0);
        linearLayout.setGravity(80);
        v.a((View) linearLayout);
        LayoutParams layoutParams2 = new LayoutParams(-1, -2);
        layoutParams2.setMargins(a, 0, a, view4 == null ? a : a / 2);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(z ? -2 : -1, -2);
        layoutParams3.setMargins(z ? a : 0, z ? 0 : a, 0, 0);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(z ? 0 : -1, -2);
        layoutParams4.setMargins(0, 0, 0, 0);
        layoutParams4.weight = 1.0f;
        linearLayout.addView(getTextContainer(), layoutParams4);
        linearLayout.addView(getCtaButton(), layoutParams3);
        relativeLayout.addView(linearLayout, layoutParams2);
        if (view4 != null) {
            LayoutParams layoutParams5 = new LayoutParams(-1, -2);
            layoutParams5.setMargins(0, 0, 0, 0);
            layoutParams5.addRule(3, linearLayout.getId());
            relativeLayout.addView(view4, layoutParams5);
        }
        addView(view, new LayoutParams(-1, -1));
        addView(relativeLayout, layoutParams);
        if (view5 != null) {
            LayoutParams layoutParams6 = new LayoutParams(b, b);
            layoutParams6.addRule(10);
            layoutParams6.addRule(11);
            layoutParams6.setMargins(a, a + i, a, a);
            addView(view5, layoutParams6);
        }
    }

    public boolean a() {
        return true;
    }
}
