package com.facebook.ads.internal.view.component.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.view.a.C0008a;
import com.github.mikephil.charting.utils.Utils;

public class d extends b {
    private static final int c = Resources.getSystem().getDisplayMetrics().widthPixels;
    private final e d;

    public d(Context context, c cVar, C0008a aVar, View view, @Nullable View view2, @Nullable View view3, boolean z, j jVar) {
        super(context, cVar, aVar, jVar, z);
        this.d = new e(context, view);
        this.d.a(view2, view3, getTextContainer(), z);
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(12);
        layoutParams.setMargins(a, a, a, a);
        getCtaButton().setLayoutParams(layoutParams);
        FrameLayout frameLayout = new FrameLayout(context);
        LayoutParams layoutParams2 = new LayoutParams(-1, -1);
        layoutParams2.addRule(2, getCtaButton().getId());
        frameLayout.setLayoutParams(layoutParams2);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -2);
        layoutParams3.gravity = 17;
        layoutParams3.setMargins(a, 0, a, 0);
        frameLayout.addView(this.d, layoutParams3);
        addView(frameLayout);
        addView(getCtaButton());
    }

    public void a(String str, String str2, String str3, String str4, String str5, double d2) {
        super.a(str, str2, str3, str4, str5, d2);
        if (d2 > Utils.DOUBLE_EPSILON) {
            this.d.a((int) (d2 * ((double) (c - (a * 2)))));
        }
    }

    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return false;
    }
}
