package com.facebook.ads.internal.view.component;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.facebook.ads.internal.a.b;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;
import java.util.Locale;
import java.util.Map;

public class a extends Button {
    public static final int a = ((int) (16.0f * v.b));
    private static final int b = ((int) (4.0f * v.b));
    private final Paint c = new Paint();
    private final RectF d;
    private final boolean e;
    /* access modifiers changed from: private */
    public final String f;
    /* access modifiers changed from: private */
    @Nullable
    public final c g;
    /* access modifiers changed from: private */
    @Nullable
    public final C0008a h;

    public a(Context context, boolean z, boolean z2, String str, j jVar, c cVar, C0008a aVar) {
        super(context);
        this.g = cVar;
        this.h = aVar;
        this.e = z;
        this.f = str;
        setTextSize(2, 16.0f);
        setTypeface(Typeface.create("sans-serif-medium", 0));
        setGravity(17);
        setPadding(a, a, a, a);
        setTextColor(jVar.f(z2));
        int e2 = jVar.e(z2);
        int blendARGB = ColorUtils.blendARGB(e2, ViewCompat.MEASURED_STATE_MASK, 0.1f);
        this.c.setStyle(Style.FILL);
        this.c.setColor(e2);
        this.d = new RectF();
        if (!z) {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(blendARGB));
            stateListDrawable.addState(new int[0], new ColorDrawable(e2));
            setBackgroundDrawable(stateListDrawable);
        }
    }

    public void a(String str, final String str2, final String str3, final Map<String, String> map) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || this.g == null) {
            setVisibility(8);
            return;
        }
        setText(str.toUpperCase(Locale.US));
        setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                String str;
                String str2;
                try {
                    com.facebook.ads.internal.a.a a2 = b.a(a.this.getContext(), a.this.g, str3, Uri.parse(str2), map);
                    if (a2 != null) {
                        a2.b();
                    }
                    if (a.this.h != null) {
                        a.this.h.a(a.this.f);
                    }
                } catch (ActivityNotFoundException e) {
                    e = e;
                    str2 = String.valueOf(a.class);
                    StringBuilder sb = new StringBuilder();
                    sb.append("Error while opening ");
                    sb.append(str2);
                    str = sb.toString();
                    Log.e(str2, str, e);
                } catch (Exception e2) {
                    e = e2;
                    str2 = String.valueOf(a.class);
                    str = "Error executing action";
                    Log.e(str2, str, e);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.e) {
            this.d.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            canvas.drawRoundRect(this.d, (float) b, (float) b, this.c);
        }
        super.onDraw(canvas);
    }
}
