package com.facebook.ads.internal.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.MediaViewVideoRenderer;
import com.facebook.ads.NativeAd;
import com.facebook.ads.VideoStartReason;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.r.a;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.e.a.b;
import com.facebook.ads.internal.view.e.c.g;
import com.facebook.ads.internal.view.e.c.h;
import com.facebook.ads.internal.view.e.d.d;

public final class e extends MediaViewVideoRenderer {
    private static final String d = "e";
    private final g e;
    private final a f = b();
    private final C0006a g = c();
    /* access modifiers changed from: private */
    @Nullable
    public n h;
    private boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public boolean k;

    public e(Context context) {
        super(context);
        this.e = new g(context);
        a();
    }

    public e(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = new g(context);
        a();
    }

    public e(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.e = new g(context);
        a();
    }

    @TargetApi(21)
    public e(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.e = new g(context);
        a();
    }

    private void a() {
        setVolume(0.0f);
        float f2 = v.b;
        int i2 = (int) (2.0f * f2);
        int i3 = (int) (25.0f * f2);
        h hVar = new h(getContext());
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(12);
        hVar.setPadding(i2, i3, i3, i2);
        hVar.setLayoutParams(layoutParams);
        int i4 = 0;
        while (true) {
            if (i4 >= getChildCount()) {
                break;
            }
            View childAt = getChildAt(0);
            if (childAt instanceof n) {
                this.h = (n) childAt;
                break;
            }
            i4++;
        }
        if (this.h == null) {
            Log.e(d, "Unable to find MediaViewVideo child.");
        } else {
            this.h.a((b) this.e);
            this.h.a((b) hVar);
        }
        this.f.a(0);
        this.f.b((int) Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
    }

    private a b() {
        return new a(this, 50, true, this.g);
    }

    private C0006a c() {
        return new C0006a() {
            public void a() {
                if (e.this.h != null) {
                    if (!e.this.k && (e.this.j || e.this.shouldAutoplay())) {
                        e.this.play(VideoStartReason.AUTO_STARTED);
                    }
                    e.this.j = false;
                    e.this.k = false;
                }
            }

            public void b() {
                if (e.this.h != null) {
                    if (e.this.h.getState() == d.PAUSED) {
                        e.this.k = true;
                    } else if (e.this.h.getState() == d.STARTED) {
                        e.this.j = true;
                    }
                    e.this.pause(e.this.k);
                }
            }
        };
    }

    private void d() {
        if (getVisibility() != 0 || !this.i || !hasWindowFocus()) {
            if (this.h != null && this.h.getState() == d.PAUSED) {
                this.k = true;
            }
            this.f.b();
            return;
        }
        this.f.a();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.i = true;
        d();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.i = false;
        d();
        super.onDetachedFromWindow();
    }

    public void onPrepared() {
        super.onPrepared();
        setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (e.this.h != null && motionEvent.getAction() == 1) {
                    e.this.h.a();
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i2) {
        super.onVisibilityChanged(view, i2);
        d();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        d();
    }

    public void setNativeAd(NativeAd nativeAd) {
        super.setNativeAd(nativeAd);
        this.j = false;
        this.k = false;
        this.e.setImage((nativeAd == null || nativeAd.getAdCoverImage() == null) ? null : nativeAd.getAdCoverImage().getUrl());
        this.f.a();
    }
}
