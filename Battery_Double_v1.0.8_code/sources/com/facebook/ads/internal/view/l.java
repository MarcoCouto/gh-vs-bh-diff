package com.facebook.ads.internal.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.AudienceNetworkActivity.BackButtonInterceptor;
import com.facebook.ads.internal.adapters.v;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.r.a;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.e.b.b;
import com.facebook.ads.internal.view.e.b.c;
import com.facebook.ads.internal.view.e.b.d;
import com.facebook.ads.internal.view.e.b.e;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.i;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.k;
import com.facebook.ads.internal.view.e.b.m;
import com.facebook.ads.internal.view.e.c.f;
import com.facebook.ads.internal.view.e.c.g;
import com.facebook.ads.internal.view.e.c.o;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class l extends m {
    /* access modifiers changed from: private */
    public boolean A = false;
    private final BackButtonInterceptor f = new BackButtonInterceptor() {
        public boolean interceptBackButton() {
            return !l.this.c.a();
        }
    };
    private final e g = new e() {
        public void a(d dVar) {
            if (l.this.getAudienceNetworkListener() != null) {
                l.this.getAudienceNetworkListener().a("videoInterstitalEvent", dVar);
            }
            if (!l.this.A) {
                l.this.l.f();
                l.this.l.k();
                l.this.A = true;
            }
            if (l.this.x != null) {
                l.this.x.finish();
            }
        }
    };
    private final k h = new k() {
        public void a(j jVar) {
            if (l.this.getAudienceNetworkListener() != null) {
                l.this.getAudienceNetworkListener().a("videoInterstitalEvent", jVar);
            }
        }
    };
    private final i i = new i() {
        public void a(h hVar) {
            if (l.this.getAudienceNetworkListener() != null) {
                l.this.getAudienceNetworkListener().a("videoInterstitalEvent", hVar);
            }
        }
    };
    private final c j = new c() {
        public void a(b bVar) {
            l.this.u.set(true);
            if (l.this.getAudienceNetworkListener() != null) {
                l.this.getAudienceNetworkListener().a("videoInterstitalEvent", bVar);
            }
        }
    };
    private final m k = new m() {
        public void a(com.facebook.ads.internal.view.e.b.l lVar) {
            if (!l.this.A) {
                l.this.v.set(l.this.l.j());
                l.this.a();
            }
            if (l.this.getAudienceNetworkListener() != null) {
                l.this.getAudienceNetworkListener().a("videoInterstitalEvent", lVar);
            }
        }
    };
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.view.e.b l = new com.facebook.ads.internal.view.e.b(getContext());
    private final o m;
    private final f n;
    /* access modifiers changed from: private */
    public final v o;
    private final com.facebook.ads.internal.adapters.d p;
    /* access modifiers changed from: private */
    public final a q;
    private final C0006a r;
    /* access modifiers changed from: private */
    public final s s = new s();
    @Nullable
    private final com.facebook.ads.internal.d.b t;
    /* access modifiers changed from: private */
    public final AtomicBoolean u = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public final AtomicBoolean v = new AtomicBoolean(false);
    private final com.facebook.ads.internal.view.e.d w;
    /* access modifiers changed from: private */
    @Nullable
    public AudienceNetworkActivity x;
    @Nullable
    private com.facebook.ads.internal.view.e.a.a y;
    private long z;

    public l(Context context, com.facebook.ads.internal.m.c cVar, v vVar, @Nullable com.facebook.ads.internal.d.b bVar) {
        super(context, cVar);
        com.facebook.ads.internal.q.a.v.a((View) this.l);
        com.facebook.ads.internal.q.a.v.a((View) this.l, 0);
        this.o = vVar;
        this.p = (com.facebook.ads.internal.adapters.d) this.o.d().get(0);
        this.t = bVar;
        this.m = new o(getContext());
        this.n = new f(context);
        this.l.getEventBus().a((T[]) new com.facebook.ads.internal.j.f[]{this.h, this.i, this.j, this.g, this.k});
        setupPlugins(this.p);
        this.r = new C0006a() {
            public void a() {
                if (!l.this.s.b()) {
                    l.this.s.a();
                    HashMap hashMap = new HashMap();
                    if (!TextUtils.isEmpty(l.this.o.a())) {
                        l.this.q.a((Map<String, String>) hashMap);
                        hashMap.put("touch", com.facebook.ads.internal.q.a.j.a(l.this.s.e()));
                        l.this.b.a(l.this.o.a(), hashMap);
                        if (l.this.getAudienceNetworkListener() != null) {
                            l.this.getAudienceNetworkListener().a("com.facebook.ads.interstitial.impression.logged");
                        }
                    }
                }
            }
        };
        this.q = new a(this.l, 1, this.r);
        this.q.a(vVar.j());
        this.q.b(vVar.k());
        this.w = new com.facebook.ads.internal.view.e.c(getContext(), this.b, this.l, this.o.a());
        this.l.setVideoURI(a(this.p.i()));
    }

    private String a(String str) {
        String str2 = "";
        if (!(this.t == null || str == null)) {
            str2 = this.t.b(str);
        }
        return TextUtils.isEmpty(str2) ? str : str2;
    }

    /* access modifiers changed from: private */
    public void a() {
        this.n.setVisibility(this.v.get() ? 0 : 8);
    }

    private void setUpContent(int i2) {
        com.facebook.ads.internal.view.component.a.b a = com.facebook.ads.internal.view.component.a.c.a(getContext(), this.b, getAudienceNetworkListener(), this.l, this.d, this.e, a, i2, this.p.g(), this.p.h(), this.m, this.n);
        a();
        a.a(this.p.b(), this.p.c(), this.p.d(), this.p.e(), this.o.a(), ((double) this.p.h()) / ((double) this.p.g()));
        a(a, a.a(), i2);
    }

    private void setupPlugins(com.facebook.ads.internal.adapters.d dVar) {
        this.l.d();
        this.l.a((com.facebook.ads.internal.view.e.a.b) this.m);
        this.l.a((com.facebook.ads.internal.view.e.a.b) this.n);
        if (!TextUtils.isEmpty(dVar.f())) {
            g gVar = new g(getContext());
            this.l.a((com.facebook.ads.internal.view.e.a.b) gVar);
            gVar.setImage(dVar.f());
        }
        com.facebook.ads.internal.view.e.c.l lVar = new com.facebook.ads.internal.view.e.c.l(getContext(), true);
        this.l.a((com.facebook.ads.internal.view.e.a.b) lVar);
        this.l.a((com.facebook.ads.internal.view.e.a.b) new com.facebook.ads.internal.view.e.c.d(lVar, dVar.j() ? com.facebook.ads.internal.view.e.c.d.a.FADE_OUT_ON_PLAY : com.facebook.ads.internal.view.e.c.d.a.VISIBLE, true));
        this.l.a((com.facebook.ads.internal.view.e.a.b) new com.facebook.ads.internal.view.e.c.k(getContext()));
        this.l.a((com.facebook.ads.internal.view.e.a.b) this.c);
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        super.a(audienceNetworkActivity, this.o);
        this.x = audienceNetworkActivity;
        setUpContent(audienceNetworkActivity.getResources().getConfiguration().orientation);
        this.x.addBackButtonInterceptor(this.f);
        com.facebook.ads.internal.adapters.d dVar = (com.facebook.ads.internal.adapters.d) this.o.d().get(0);
        if (dVar.j()) {
            this.l.setVolume(dVar.k() ? 1.0f : 0.0f);
            this.l.a(com.facebook.ads.internal.view.e.a.a.AUTO_STARTED);
        }
        this.z = System.currentTimeMillis();
    }

    public void a(Bundle bundle) {
    }

    public void i() {
        if (!this.A && this.l.getState() == com.facebook.ads.internal.view.e.d.d.STARTED) {
            this.y = this.l.getVideoStartReason();
            this.l.a(false);
        }
    }

    public void j() {
        if (!this.A && this.y != null) {
            this.l.a(this.y);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        removeAllViews();
        com.facebook.ads.internal.q.a.v.b(this.l);
        com.facebook.ads.internal.q.a.v.b(this.m);
        com.facebook.ads.internal.q.a.v.b(this.n);
        setUpContent(configuration.orientation);
        super.onConfigurationChanged(configuration);
    }

    public void onDestroy() {
        if (!this.A) {
            if (!this.u.get()) {
                this.l.e();
            }
            if (this.o != null) {
                com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(this.z, C0004a.XOUT, this.o.f()));
                if (!TextUtils.isEmpty(this.o.a())) {
                    HashMap hashMap = new HashMap();
                    this.q.a((Map<String, String>) hashMap);
                    hashMap.put("touch", com.facebook.ads.internal.q.a.j.a(this.s.e()));
                    this.b.h(this.o.a(), hashMap);
                }
            }
            this.l.f();
            this.l.k();
            this.A = true;
        }
        this.x = null;
        super.onDestroy();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.s.a(motionEvent, this, this);
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (this.q != null) {
            if (i2 == 0) {
                this.q.a();
            } else if (i2 == 8) {
                this.q.b();
            }
        }
    }
}
