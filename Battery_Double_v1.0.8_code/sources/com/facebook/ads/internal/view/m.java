package com.facebook.ads.internal.view;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.d;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.q;
import com.facebook.ads.internal.q.a.q.a;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;

public abstract class m extends RelativeLayout implements a {
    protected static final int a = ((int) (56.0f * v.b));
    protected final c b;
    protected final g c = new g(getContext());
    protected j d;
    protected j e;
    @Nullable
    private C0008a f;
    private final q g = new q(this);

    m(Context context, c cVar) {
        super(context.getApplicationContext());
        this.b = cVar;
    }

    private void a() {
        removeAllViews();
        v.b(this);
    }

    /* access modifiers changed from: 0000 */
    public void a(View view, boolean z, int i) {
        int d2;
        g gVar;
        j jVar;
        this.g.a(a.DEFAULT);
        a();
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.setMargins(0, z ? 0 : a, 0, 0);
        addView(view, layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-1, a);
        layoutParams2.addRule(10);
        if (i == 1) {
            d2 = this.d.d(z);
            gVar = this.c;
            jVar = this.d;
        } else {
            d2 = this.e.d(z);
            gVar = this.c;
            jVar = this.e;
        }
        gVar.a(jVar, z);
        addView(this.c, layoutParams2);
        v.a((View) this, d2);
        if (this.f != null) {
            this.f.a((View) this);
            if (z && VERSION.SDK_INT >= 19) {
                this.g.a(a.FULL_SCREEN);
            }
        }
    }

    public void a(final AudienceNetworkActivity audienceNetworkActivity, com.facebook.ads.internal.adapters.v vVar) {
        this.g.a(audienceNetworkActivity.getWindow());
        this.d = vVar.h();
        this.e = vVar.i();
        this.c.a(vVar.b(), vVar.c(), vVar.g(), vVar.e(), vVar.a(), ((d) vVar.d().get(0)).l());
        this.c.setToolbarListener(new g.a() {
            public void a() {
                audienceNetworkActivity.finish();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public C0008a getAudienceNetworkListener() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        this.c.d();
        super.onConfigurationChanged(configuration);
        final ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @RequiresApi(api = 16)
            public void onGlobalLayout() {
                m.this.c.e();
                if (VERSION.SDK_INT >= 14) {
                    viewTreeObserver.removeOnGlobalLayoutListener(this);
                }
            }
        });
    }

    public void onDestroy() {
        this.g.a();
        this.c.setToolbarListener(null);
        a();
    }

    public void setListener(C0008a aVar) {
        this.f = aVar;
    }
}
