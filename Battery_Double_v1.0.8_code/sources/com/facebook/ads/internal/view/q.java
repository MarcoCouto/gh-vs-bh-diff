package com.facebook.ads.internal.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.AudienceNetworkActivity.BackButtonInterceptor;
import com.facebook.ads.internal.adapters.ad;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.d.a;
import com.facebook.ads.internal.view.e.b;
import com.facebook.ads.internal.view.e.b.c;
import com.facebook.ads.internal.view.e.b.e;
import com.facebook.ads.internal.view.e.b.m;
import com.facebook.ads.internal.view.e.b.n;
import com.facebook.ads.internal.view.e.b.o;
import com.facebook.ads.internal.view.e.b.t;
import com.facebook.ads.internal.view.e.b.u;
import com.facebook.ads.internal.view.e.b.z;
import com.facebook.ads.internal.view.e.c.d;
import com.facebook.ads.internal.view.e.c.f;
import com.facebook.ads.internal.view.e.c.k;
import com.facebook.ads.internal.view.e.c.l;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class q implements a {
    static final /* synthetic */ boolean a = true;
    private static final int b = ((int) (12.0f * v.b));
    private static final int c = ((int) (18.0f * v.b));
    private static final int d = ((int) (16.0f * v.b));
    private static final int e = ((int) (72.0f * v.b));
    private static final int f = ((int) (v.b * 56.0f));
    private static final int g = ((int) (56.0f * v.b));
    private static final int h = ((int) (28.0f * v.b));
    private static final int i = ((int) (20.0f * v.b));
    private static final LayoutParams j = new LayoutParams(-1, -1);
    private final j A;
    /* access modifiers changed from: private */
    public final AtomicBoolean B = new AtomicBoolean(false);
    @Nullable
    private Context C;
    /* access modifiers changed from: private */
    @Nullable
    public b D;
    /* access modifiers changed from: private */
    @Nullable
    public C0008a E;
    @Nullable
    private a F;
    /* access modifiers changed from: private */
    @Nullable
    public d G;
    @Nullable
    private l H;
    /* access modifiers changed from: private */
    @Nullable
    public com.facebook.ads.internal.view.e.c.j I;
    @Nullable
    private g J;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.view.d.b K;
    /* access modifiers changed from: private */
    public boolean L = false;
    private final BackButtonInterceptor k = new BackButtonInterceptor() {
        public boolean interceptBackButton() {
            return !q.this.L;
        }
    };
    private final c l = new c() {
        public void a(com.facebook.ads.internal.view.e.b.b bVar) {
            if (q.this.E != null) {
                q.this.K.d();
                q.this.e();
                q.this.E.a(z.REWARDED_VIDEO_COMPLETE.a(), bVar);
            }
        }
    };
    private final e m = new e() {
        public void a(com.facebook.ads.internal.view.e.b.d dVar) {
            if (q.this.E != null) {
                q.this.E.a(z.REWARDED_VIDEO_ERROR.a());
            }
            q.this.c();
        }
    };
    private final m n = new m() {
        public void a(com.facebook.ads.internal.view.e.b.l lVar) {
            if (q.this.D != null) {
                q.this.D.a(com.facebook.ads.internal.view.e.a.a.USER_STARTED);
                q.this.s.a();
                q.this.B.set(q.this.D.j());
                q.this.h();
            }
        }
    };
    private final u o = new u() {
        public void a(t tVar) {
            q.this.u.a(tVar.b(), q.this.D, tVar.a());
        }
    };
    private final o p = new o() {
        public void a(n nVar) {
            if (q.this.D != null && q.this.G != null && q.this.D.getDuration() - q.this.D.getCurrentPosition() <= 3000 && q.this.G.a()) {
                q.this.G.b();
            }
        }
    };
    /* access modifiers changed from: private */
    public final ad q;
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.m.c r;
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.r.a s;
    private final C0006a t;
    /* access modifiers changed from: private */
    public final s u = new s();
    private final com.facebook.ads.internal.view.e.c.o v;
    private final com.facebook.ads.internal.view.e.c w;
    private final RelativeLayout x;
    private final RelativeLayout y;
    private final f z;

    public q(Context context, com.facebook.ads.internal.m.c cVar, b bVar, C0008a aVar, ad adVar) {
        this.C = context;
        this.E = aVar;
        this.D = bVar;
        this.r = cVar;
        this.q = adVar;
        this.A = this.q.n();
        this.x = new RelativeLayout(context);
        this.y = new RelativeLayout(context);
        this.v = new com.facebook.ads.internal.view.e.c.o(this.C);
        this.z = new f(this.C);
        this.K = new com.facebook.ads.internal.view.d.b(this.C, this.r, this.q, this.E);
        new com.facebook.ads.internal.view.b.d(this.y, i).a().a(com.facebook.ads.internal.l.a.e(this.C)).a(this.q.j());
        this.t = new C0006a() {
            public void a() {
                if (!q.this.u.b()) {
                    q.this.u.a();
                    HashMap hashMap = new HashMap();
                    if (!TextUtils.isEmpty(q.this.q.b())) {
                        q.this.s.a((Map<String, String>) hashMap);
                        hashMap.put("touch", com.facebook.ads.internal.q.a.j.a(q.this.u.e()));
                        q.this.r.a(q.this.q.b(), hashMap);
                    }
                    if (q.this.E != null) {
                        q.this.E.a(z.REWARDED_VIDEO_IMPRESSION.a());
                    }
                }
            }
        };
        this.s = new com.facebook.ads.internal.r.a(this.D, 1, this.t);
        this.s.a((int) Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        this.w = new com.facebook.ads.internal.view.e.c(this.C, this.r, this.D, this.q.b());
        if (a || this.D != null) {
            this.D.h();
            this.D.setIsFullScreen(true);
            this.D.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            this.D.getEventBus().a((T[]) new com.facebook.ads.internal.j.f[]{this.l, this.m, this.n, this.o, this.p});
            return;
        }
        throw new AssertionError();
    }

    private void a(int i2) {
        this.y.removeAllViews();
        this.y.addView(this.D, j);
        if (this.F != null) {
            v.a((View) this.F);
            this.F.a(i2);
            LayoutParams layoutParams = new LayoutParams(-1, -2);
            layoutParams.addRule(12);
            this.F.setPadding(d, d, d, d);
            this.y.addView(this.F, layoutParams);
        }
        if (this.I != null) {
            LayoutParams layoutParams2 = new LayoutParams(f, f);
            layoutParams2.addRule(10);
            layoutParams2.addRule(11);
            this.I.setPadding(d, d, d, d);
            this.y.addView(this.I, layoutParams2);
        }
        LayoutParams layoutParams3 = new LayoutParams(h, h);
        layoutParams3.addRule(10);
        layoutParams3.addRule(11);
        layoutParams3.setMargins(b, b + g, b, c);
        this.y.addView(this.z, layoutParams3);
        h();
        LayoutParams layoutParams4 = new LayoutParams(-1, -2);
        layoutParams4.addRule(12);
        this.y.addView(this.v, layoutParams4);
    }

    private void d() {
        b bVar;
        com.facebook.ads.internal.view.e.a.b bVar2;
        if (this.D != null) {
            this.D.d();
            this.D.a((com.facebook.ads.internal.view.e.a.b) new k(this.C));
            this.D.a((com.facebook.ads.internal.view.e.a.b) this.z);
            this.D.a((com.facebook.ads.internal.view.e.a.b) this.v);
            this.H = new l(this.C, true);
            d dVar = new d(this.H, d.a.FADE_OUT_ON_PLAY, true);
            this.D.a((com.facebook.ads.internal.view.e.a.b) this.H);
            this.D.a((com.facebook.ads.internal.view.e.a.b) dVar);
            a aVar = new a(this.C, e, this.A, this.r, this.E, this.K.b() == com.facebook.ads.internal.view.d.b.a.INFO, this.K.b() == com.facebook.ads.internal.view.d.b.a.INFO);
            this.F = aVar;
            this.F.setInfo(this.q);
            this.G = new d(this.F, d.a.FADE_OUT_ON_PLAY, true);
            this.D.a((com.facebook.ads.internal.view.e.a.b) this.G);
            if (this.K.a() && this.q.k() > 0) {
                this.I = new com.facebook.ads.internal.view.e.c.j(this.C, this.q.k(), -12286980);
                this.I.setButtonMode(com.facebook.ads.internal.view.e.c.j.a.SKIP_BUTTON_MODE);
                this.I.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if (!(q.this.I == null || !q.this.I.a() || q.this.I.getSkipSeconds() == 0 || q.this.D == null)) {
                            q.this.D.e();
                        }
                    }
                });
                bVar = this.D;
                bVar2 = this.I;
            } else if (!this.K.a()) {
                this.J = new g(this.C);
                this.J.a(this.q.e(), this.q.i(), this.q.t(), this.q.r(), this.q.b(), this.q.k());
                if (this.q.k() <= 0) {
                    this.J.b();
                }
                if (this.K.b() != com.facebook.ads.internal.view.d.b.a.INFO) {
                    this.J.c();
                }
                this.J.setToolbarListener(new g.a() {
                    public void a() {
                        if (q.this.L || q.this.D == null) {
                            if (q.this.L && q.this.E != null) {
                                q.this.E.a(z.REWARDED_VIDEO_END_ACTIVITY.a());
                            }
                            return;
                        }
                        q.this.L = true;
                        q.this.D.e();
                    }
                });
                bVar = this.D;
                bVar2 = this.J;
            } else {
                return;
            }
            bVar.a(bVar2);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        LayoutParams layoutParams;
        this.L = true;
        g();
        f();
        if (this.D != null) {
            this.D.d();
            this.D.setVisibility(4);
        }
        if (this.J != null) {
            this.J.a(true);
            this.J.c();
        }
        v.a(this.D, this.I, this.z, this.v);
        Pair c2 = this.K.c();
        switch ((com.facebook.ads.internal.view.d.b.a) c2.first) {
            case MARKUP:
                v.a(this.F);
                this.y.addView((View) c2.second, j);
                return;
            case SCREENSHOTS:
                if (this.F != null) {
                    this.F.setVisibility(0);
                    this.F.a();
                }
                layoutParams = new LayoutParams(-1, -1);
                layoutParams.setMargins(0, g, 0, 0);
                layoutParams.addRule(2, this.F.getId());
                break;
            case INFO:
                v.a(this.F);
                layoutParams = new LayoutParams(-1, -2);
                layoutParams.addRule(15);
                layoutParams.setMargins(d, d, d, d);
                break;
            default:
                return;
        }
        this.y.addView((View) c2.second, layoutParams);
    }

    private void f() {
        if (VERSION.SDK_INT > 19) {
            AutoTransition autoTransition = new AutoTransition();
            autoTransition.setDuration(200);
            autoTransition.setInterpolator(new AccelerateDecelerateInterpolator());
            TransitionManager.beginDelayedTransition(this.y, autoTransition);
        }
    }

    private void g() {
        if (this.C != null) {
            FrameLayout frameLayout = new FrameLayout(this.C);
            frameLayout.setLayoutParams(j);
            v.a((View) frameLayout, -1509949440);
            this.y.addView(frameLayout, 0);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        this.z.setVisibility(this.B.get() ? 0 : 8);
    }

    public void a() {
        if (this.D != null) {
            this.D.a(true);
        }
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        if (this.D != null && this.E != null) {
            d();
            audienceNetworkActivity.addBackButtonInterceptor(this.k);
            if (!TextUtils.isEmpty(this.q.a())) {
                this.D.setVideoURI(!TextUtils.isEmpty(this.q.s()) ? this.q.s() : this.q.a());
            }
            a(audienceNetworkActivity.getResources().getConfiguration().orientation);
            this.x.addView(this.y, j);
            if (this.J != null) {
                v.a((View) this.J);
                this.J.a(this.A, true);
                this.x.addView(this.J, new LayoutParams(-1, g));
            }
            this.x.setLayoutParams(j);
            this.E.a((View) this.x);
            this.D.a(com.facebook.ads.internal.view.e.a.a.USER_STARTED);
        }
    }

    public void a(Configuration configuration) {
        if (this.F != null) {
            this.F.a(configuration.orientation);
        }
    }

    public void a(Bundle bundle) {
    }

    public boolean b() {
        return this.D == null || this.D.getState() == com.facebook.ads.internal.view.e.d.d.PAUSED;
    }

    public void c() {
        if (this.D != null) {
            this.D.f();
            this.D.k();
        }
        if (this.s != null) {
            this.s.b();
        }
    }

    public void i() {
        a();
    }

    public void j() {
        if (b() && this.D != null && this.E != null) {
            this.D.a(this.D.getCurrentPosition());
            this.D.a(com.facebook.ads.internal.view.e.a.a.USER_STARTED);
        }
    }

    public void onDestroy() {
        c();
        if (this.D != null) {
            this.D.getEventBus().b((T[]) new com.facebook.ads.internal.j.f[]{this.l, this.m, this.n, this.o, this.p});
        }
        if (!TextUtils.isEmpty(this.q.b())) {
            HashMap hashMap = new HashMap();
            this.s.a((Map<String, String>) hashMap);
            hashMap.put("touch", com.facebook.ads.internal.q.a.j.a(this.u.e()));
            this.r.h(this.q.b(), hashMap);
        }
        if (this.J != null) {
            this.J.setToolbarListener(null);
        }
        this.w.a();
        this.D = null;
        this.K.e();
        this.I = null;
        this.F = null;
        this.G = null;
        this.E = null;
        this.C = null;
        this.v.a();
    }

    public void setListener(C0008a aVar) {
    }
}
