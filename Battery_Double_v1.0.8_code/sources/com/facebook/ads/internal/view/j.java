package com.facebook.ads.internal.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.v;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.j.b;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.component.d;
import com.facebook.ads.internal.view.i.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class j extends m {
    /* access modifiers changed from: private */
    public final s f = new s();
    @Nullable
    private LinearLayout g;
    /* access modifiers changed from: private */
    public String h;
    private long i;
    private String j;
    private List<a> k;
    /* access modifiers changed from: private */
    @Nullable
    public d l;
    @Nullable
    private RecyclerView m;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.r.a n;
    private C0006a o;
    private int p;
    private int q;

    public j(Context context, c cVar) {
        super(context, cVar);
    }

    private void a(v vVar) {
        this.h = vVar.a();
        this.j = vVar.f();
        this.p = vVar.j();
        this.q = vVar.k();
        List d = vVar.d();
        this.k = new ArrayList(d.size());
        for (int i2 = 0; i2 < d.size(); i2++) {
            com.facebook.ads.internal.adapters.d dVar = (com.facebook.ads.internal.adapters.d) d.get(i2);
            List<a> list = this.k;
            a aVar = new a(i2, d.size(), dVar.f(), dVar.a(), dVar.c(), dVar.d(), dVar.e());
            list.add(aVar);
        }
    }

    public void a() {
        if (this.g != null) {
            this.g.removeAllViews();
            this.g = null;
        }
        if (this.m != null) {
            this.m.removeAllViews();
            this.m = null;
        }
        if (this.l != null) {
            this.l.removeAllViews();
            this.l = null;
        }
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        v vVar = (v) intent.getSerializableExtra("ad_data_bundle");
        super.a(audienceNetworkActivity, vVar);
        a(vVar);
        setUpLayout(audienceNetworkActivity.getResources().getConfiguration().orientation);
        this.i = System.currentTimeMillis();
    }

    public void a(Bundle bundle) {
    }

    public void i() {
    }

    public void j() {
    }

    public void onConfigurationChanged(Configuration configuration) {
        a();
        setUpLayout(configuration.orientation);
        super.onConfigurationChanged(configuration);
    }

    public void onDestroy() {
        super.onDestroy();
        b.a(com.facebook.ads.internal.j.a.a(this.i, C0004a.XOUT, this.j));
        if (!TextUtils.isEmpty(this.h)) {
            HashMap hashMap = new HashMap();
            this.n.a((Map<String, String>) hashMap);
            hashMap.put("touch", com.facebook.ads.internal.q.a.j.a(this.f.e()));
            this.b.h(this.h, hashMap);
        }
        a();
        this.n.b();
        this.n = null;
        this.k = null;
    }

    public void setUpLayout(int i2) {
        LinearLayout linearLayout;
        int i3;
        boolean z;
        int i4;
        int i5;
        int i6;
        int i7 = i2;
        this.g = new LinearLayout(getContext());
        if (i7 == 1) {
            linearLayout = this.g;
            i3 = 17;
        } else {
            linearLayout = this.g;
            i3 = 48;
        }
        linearLayout.setGravity(i3);
        this.g.setLayoutParams(new LayoutParams(-1, -1));
        this.g.setOrientation(1);
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i8 = displayMetrics.widthPixels;
        int i9 = displayMetrics.heightPixels;
        float f2 = displayMetrics.density;
        if (i7 == 1) {
            int min = Math.min(i8 - ((int) (32.0f * f2)), i9 / 2);
            int i10 = (i8 - min) / 8;
            i5 = i10;
            i6 = min;
            i4 = 4 * i10;
            z = false;
        } else {
            int i11 = (int) (8.0f * f2);
            z = true;
            i5 = i11;
            i6 = i9 - ((int) (120.0f * f2));
            i4 = 2 * i11;
        }
        this.m = new RecyclerView(getContext());
        this.m.setLayoutParams(new LayoutParams(-1, -2));
        RecyclerView recyclerView = this.m;
        i iVar = new i(this.k, this.b, this.f, getAudienceNetworkListener(), i7 == 1 ? this.d : this.e, this.h, i6, i5, i4, z);
        recyclerView.setAdapter(iVar);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 0, false);
        linearLayoutManager.setAutoMeasureEnabled(true);
        this.m.setLayoutManager(linearLayoutManager);
        this.o = new C0006a() {
            public void a() {
                HashMap hashMap = new HashMap();
                if (!j.this.f.b()) {
                    j.this.f.a();
                    if (j.this.getAudienceNetworkListener() != null) {
                        j.this.getAudienceNetworkListener().a("com.facebook.ads.interstitial.impression.logged");
                    }
                    if (!TextUtils.isEmpty(j.this.h)) {
                        j.this.n.a((Map<String, String>) hashMap);
                        hashMap.put("touch", com.facebook.ads.internal.q.a.j.a(j.this.f.e()));
                        j.this.b.a(j.this.h, hashMap);
                    }
                }
            }
        };
        this.n = new com.facebook.ads.internal.r.a(this.m, 1, this.o);
        this.n.a(this.p);
        this.n.b(this.q);
        if (i7 == 1) {
            new PagerSnapHelper().attachToRecyclerView(this.m);
            this.m.addOnScrollListener(new OnScrollListener() {
                public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                    super.onScrollStateChanged(recyclerView, i);
                }

                public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                    View findViewByPosition;
                    super.onScrolled(recyclerView, i, i2);
                    int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                    int findLastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                    int findFirstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (findFirstCompletelyVisibleItemPosition == -1) {
                        if (i > 0) {
                            if (j.this.l != null) {
                                j.this.l.a(findLastVisibleItemPosition);
                            }
                            findViewByPosition = linearLayoutManager.findViewByPosition(findLastVisibleItemPosition);
                        } else {
                            if (j.this.l != null) {
                                j.this.l.a(findFirstVisibleItemPosition);
                            }
                            findViewByPosition = linearLayoutManager.findViewByPosition(findFirstVisibleItemPosition);
                        }
                        findViewByPosition.setAlpha(1.0f);
                        return;
                    }
                    if (j.this.l != null) {
                        j.this.l.a(findFirstCompletelyVisibleItemPosition);
                    }
                    if (findFirstCompletelyVisibleItemPosition != findFirstVisibleItemPosition) {
                        linearLayoutManager.findViewByPosition(findFirstVisibleItemPosition).setAlpha(0.5f);
                    }
                    linearLayoutManager.findViewByPosition(findFirstCompletelyVisibleItemPosition).setAlpha(1.0f);
                    if (findFirstCompletelyVisibleItemPosition != findLastVisibleItemPosition) {
                        linearLayoutManager.findViewByPosition(findLastVisibleItemPosition).setAlpha(0.5f);
                    }
                }
            });
            this.l = new d(getContext(), i7 == 1 ? this.d : this.e, this.k.size());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, (int) (8.0f * f2));
            layoutParams.setMargins(0, (int) (12.0f * f2), 0, 0);
            this.l.setLayoutParams(layoutParams);
        }
        this.g.addView(this.m);
        if (this.l != null) {
            this.g.addView(this.l);
        }
        a(this.g, false, i7);
        this.n.a();
    }
}
