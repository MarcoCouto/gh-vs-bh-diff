package com.facebook.ads.internal.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.d;
import com.facebook.ads.internal.adapters.v;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.r.a;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.component.a.b;
import java.util.HashMap;
import java.util.Map;

public class k extends m {
    /* access modifiers changed from: private */
    public final v f;
    private final a g;
    /* access modifiers changed from: private */
    public final s h = new s();
    private final C0006a i;
    private long j;

    public k(Context context, v vVar, c cVar) {
        super(context, cVar);
        this.f = vVar;
        this.i = new C0006a() {
            public void a() {
                if (!k.this.h.b()) {
                    k.this.h.a();
                    k.this.b.a(k.this.f.a(), new HashMap());
                    if (k.this.getAudienceNetworkListener() != null) {
                        k.this.getAudienceNetworkListener().a("com.facebook.ads.interstitial.impression.logged");
                    }
                }
            }
        };
        this.g = new a(this, 100, this.i);
        this.g.a(vVar.j());
        this.g.b(vVar.k());
    }

    private void setUpContent(int i2) {
        d dVar = (d) this.f.d().get(0);
        ImageView imageView = new ImageView(getContext());
        imageView.setScaleType(ScaleType.CENTER);
        imageView.setAdjustViewBounds(true);
        new com.facebook.ads.internal.view.b.d(imageView).a(dVar.h(), dVar.g()).a(dVar.f());
        b a = com.facebook.ads.internal.view.component.a.c.a(getContext(), this.b, getAudienceNetworkListener(), imageView, this.d, this.e, a, i2, dVar.g(), dVar.h());
        a.a(dVar.b(), dVar.c(), dVar.d(), dVar.e(), this.f.a(), ((double) dVar.h()) / ((double) dVar.g()));
        a(a, a.a(), i2);
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        super.a(audienceNetworkActivity, this.f);
        setUpContent(audienceNetworkActivity.getResources().getConfiguration().orientation);
        this.j = System.currentTimeMillis();
    }

    public void a(Bundle bundle) {
    }

    public void i() {
    }

    public void j() {
    }

    public void onConfigurationChanged(Configuration configuration) {
        removeAllViews();
        setUpContent(configuration.orientation);
        super.onConfigurationChanged(configuration);
    }

    public void onDestroy() {
        if (this.f != null) {
            com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(this.j, C0004a.XOUT, this.f.f()));
            if (!TextUtils.isEmpty(this.f.a())) {
                HashMap hashMap = new HashMap();
                this.g.a((Map<String, String>) hashMap);
                hashMap.put("touch", j.a(this.h.e()));
                this.b.h(this.f.a(), hashMap);
            }
        }
        super.onDestroy();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.h.a(motionEvent, this, this);
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (this.g != null) {
            if (i2 == 0) {
                this.g.a();
            } else if (i2 == 8) {
                this.g.b();
            }
        }
    }
}
