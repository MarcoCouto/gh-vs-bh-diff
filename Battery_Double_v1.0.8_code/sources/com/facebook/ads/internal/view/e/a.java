package com.facebook.ads.internal.view.e;

import android.database.ContentObserver;
import android.os.Handler;

class a extends ContentObserver {
    private final d a;

    public a(Handler handler, d dVar) {
        super(handler);
        this.a = dVar;
    }

    public boolean deliverSelfNotifications() {
        return false;
    }

    public void onChange(boolean z) {
        this.a.e();
    }
}
