package com.facebook.ads.internal.view.e.b;

import android.view.MotionEvent;
import android.view.View;
import com.facebook.ads.internal.j.d;

public class t extends d {
    private final View a;
    private final MotionEvent b;

    public t(View view, MotionEvent motionEvent) {
        this.a = view;
        this.b = motionEvent;
    }

    public View a() {
        return this.a;
    }

    public MotionEvent b() {
        return this.b;
    }
}
