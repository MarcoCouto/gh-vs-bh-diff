package com.facebook.ads.internal.view.e;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.System;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.o;
import com.facebook.ads.internal.q.a.u;
import com.facebook.internal.NativeProtocol;
import com.github.mikephil.charting.utils.Utils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class d implements o<Bundle> {
    private final String a;
    private boolean b;
    private final Context c;
    private final c d;
    private final a e;
    private final com.facebook.ads.internal.b.a f;
    private int g;
    private int h;
    private final a i;

    public interface a {
        boolean g();

        int getCurrentPosition();

        boolean getGlobalVisibleRect(Rect rect);

        long getInitialBufferTime();

        int getMeasuredHeight();

        int getMeasuredWidth();

        com.facebook.ads.internal.view.e.a.a getVideoStartReason();

        float getVolume();

        boolean i();
    }

    protected enum b {
        PLAY(0),
        SKIP(1),
        TIME(2),
        MRC(3),
        PAUSE(4),
        RESUME(5),
        MUTE(6),
        UNMUTE(7),
        VIEWABLE_IMPRESSION(10);
        
        public final int j;

        private b(int i) {
            this.j = i;
        }
    }

    public d(Context context, c cVar, a aVar, List<com.facebook.ads.internal.b.b> list, String str) {
        this(context, cVar, aVar, list, str, null);
    }

    public d(Context context, c cVar, a aVar, List<com.facebook.ads.internal.b.b> list, String str, @Nullable Bundle bundle) {
        a aVar2 = aVar;
        List<com.facebook.ads.internal.b.b> list2 = list;
        Bundle bundle2 = bundle;
        this.b = true;
        this.g = 0;
        this.h = 0;
        this.c = context;
        c cVar2 = cVar;
        this.d = cVar2;
        this.e = aVar2;
        final String str2 = str;
        this.a = str2;
        AnonymousClass1 r16 = r0;
        final c cVar3 = cVar2;
        AnonymousClass1 r0 = new com.facebook.ads.internal.b.b(this, 0.5d, -1.0d, 2.0d, true) {
            final /* synthetic */ d g;

            {
                this.g = r10;
            }

            /* access modifiers changed from: protected */
            public void a(boolean z, boolean z2, com.facebook.ads.internal.b.c cVar) {
                cVar3.d(str2, this.g.a(b.MRC));
            }
        };
        list2.add(r16);
        AnonymousClass2 r15 = r0;
        final String str3 = str;
        AnonymousClass2 r02 = new com.facebook.ads.internal.b.b(this, 1.0E-7d, -1.0d, 0.001d, false) {
            final /* synthetic */ d g;

            {
                this.g = r10;
            }

            /* access modifiers changed from: protected */
            public void a(boolean z, boolean z2, com.facebook.ads.internal.b.c cVar) {
                cVar3.d(str3, this.g.a(b.VIEWABLE_IMPRESSION));
            }
        };
        list2.add(r15);
        if (bundle2 != null) {
            this.f = new com.facebook.ads.internal.b.a((View) aVar2, list2, bundle2.getBundle("adQualityManager"));
            this.g = bundle2.getInt("lastProgressTimeMS");
            this.h = bundle2.getInt("lastBoundaryTimeMS");
        } else {
            this.f = new com.facebook.ads.internal.b.a((View) aVar2, list2);
        }
        this.i = new a(new Handler(), this);
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(b bVar) {
        return a(bVar, this.e.getCurrentPosition());
    }

    private Map<String, String> a(b bVar, int i2) {
        Map<String, String> c2 = c(i2);
        c2.put(NativeProtocol.WEB_DIALOG_ACTION, String.valueOf(bVar.j));
        return c2;
    }

    private void a(int i2, boolean z) {
        if (((double) i2) > Utils.DOUBLE_EPSILON && i2 >= this.g) {
            if (i2 > this.g) {
                this.f.a((double) (((float) (i2 - this.g)) / 1000.0f), (double) d());
                this.g = i2;
                if (i2 - this.h >= 5000) {
                    this.d.d(this.a, a(b.TIME, i2));
                    this.h = this.g;
                    this.f.a();
                    return;
                }
            }
            if (z) {
                this.d.d(this.a, a(b.TIME, i2));
            }
        }
    }

    private void a(Map<String, String> map) {
        map.put("exoplayer", String.valueOf(this.e.g()));
        map.put("prep", Long.toString(this.e.getInitialBufferTime()));
    }

    private void a(Map<String, String> map, int i2) {
        map.put("ptime", String.valueOf(((float) this.h) / 1000.0f));
        map.put("time", String.valueOf(((float) i2) / 1000.0f));
    }

    private void b(Map<String, String> map) {
        com.facebook.ads.internal.b.c b2 = this.f.b();
        com.facebook.ads.internal.b.c.a b3 = b2.b();
        map.put("vwa", String.valueOf(b3.c()));
        map.put("vwm", String.valueOf(b3.b()));
        map.put("vwmax", String.valueOf(b3.d()));
        map.put("vtime_ms", String.valueOf(b3.f() * 1000.0d));
        map.put("mcvt_ms", String.valueOf(b3.g() * 1000.0d));
        com.facebook.ads.internal.b.c.a c2 = b2.c();
        map.put("vla", String.valueOf(c2.c()));
        map.put("vlm", String.valueOf(c2.b()));
        map.put("vlmax", String.valueOf(c2.d()));
        map.put("atime_ms", String.valueOf(c2.f() * 1000.0d));
        map.put("mcat_ms", String.valueOf(c2.g() * 1000.0d));
    }

    private Map<String, String> c(int i2) {
        HashMap hashMap = new HashMap();
        u.a(hashMap, this.e.getVideoStartReason() == com.facebook.ads.internal.view.e.a.a.AUTO_STARTED, !this.e.i());
        a((Map<String, String>) hashMap);
        b((Map<String, String>) hashMap);
        a((Map<String, String>) hashMap, i2);
        c((Map<String, String>) hashMap);
        return hashMap;
    }

    private void c(Map<String, String> map) {
        Rect rect = new Rect();
        this.e.getGlobalVisibleRect(rect);
        map.put("pt", String.valueOf(rect.top));
        map.put("pl", String.valueOf(rect.left));
        map.put("ph", String.valueOf(this.e.getMeasuredHeight()));
        map.put("pw", String.valueOf(this.e.getMeasuredWidth()));
        WindowManager windowManager = (WindowManager) this.c.getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        map.put("vph", String.valueOf(displayMetrics.heightPixels));
        map.put("vpw", String.valueOf(displayMetrics.widthPixels));
    }

    public void a(int i2) {
        a(i2, false);
    }

    public void a(int i2, int i3) {
        a(i2, true);
        this.h = i3;
        this.g = i3;
        this.f.a();
    }

    public void b() {
        this.c.getContentResolver().registerContentObserver(System.CONTENT_URI, true, this.i);
    }

    public void b(int i2) {
        a(i2, true);
        this.h = 0;
        this.g = 0;
        this.f.a();
    }

    public void c() {
        this.c.getContentResolver().unregisterContentObserver(this.i);
    }

    /* access modifiers changed from: protected */
    public float d() {
        return u.a(this.c) * this.e.getVolume();
    }

    public void e() {
        boolean z;
        if (((double) d()) < 0.05d) {
            if (this.b) {
                f();
                z = false;
            }
            return;
        }
        if (!this.b) {
            h();
            z = true;
        }
        return;
        this.b = z;
    }

    public void f() {
        this.d.d(this.a, a(b.MUTE));
    }

    public Bundle g() {
        a(l(), l());
        Bundle bundle = new Bundle();
        bundle.putInt("lastProgressTimeMS", this.g);
        bundle.putInt("lastBoundaryTimeMS", this.h);
        bundle.putBundle("adQualityManager", this.f.g());
        return bundle;
    }

    public void h() {
        this.d.d(this.a, a(b.UNMUTE));
    }

    public void i() {
        this.d.d(this.a, a(b.SKIP));
    }

    public void j() {
        this.d.d(this.a, a(b.PAUSE));
    }

    public void k() {
        this.d.d(this.a, a(b.RESUME));
    }

    public int l() {
        return this.g;
    }
}
