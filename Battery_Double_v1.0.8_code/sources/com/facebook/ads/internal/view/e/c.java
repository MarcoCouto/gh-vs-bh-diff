package com.facebook.ads.internal.view.e;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.view.e.b.b;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.l;
import com.facebook.ads.internal.view.e.b.m;
import com.facebook.ads.internal.view.e.b.n;
import com.facebook.ads.internal.view.e.b.p;
import com.facebook.ads.internal.view.e.b.r;
import com.facebook.ads.internal.view.e.b.s;
import com.facebook.ads.internal.view.e.b.v;
import com.facebook.ads.internal.view.e.b.w;
import com.facebook.ads.internal.view.e.b.x;
import com.facebook.ads.internal.view.e.b.y;
import java.util.ArrayList;
import java.util.List;

public class c extends d {
    public int a;
    /* access modifiers changed from: private */
    public final w b;
    /* access modifiers changed from: private */
    public final f<r> c;
    /* access modifiers changed from: private */
    public final f<h> d;
    /* access modifiers changed from: private */
    public final f<j> e;
    /* access modifiers changed from: private */
    public final f<n> f;
    /* access modifiers changed from: private */
    public final f<b> g;
    /* access modifiers changed from: private */
    public final f<p> h;
    /* access modifiers changed from: private */
    public final f<x> i;
    /* access modifiers changed from: private */
    public final f<y> j;
    /* access modifiers changed from: private */
    public final f<s> k;
    /* access modifiers changed from: private */
    public final m l;
    /* access modifiers changed from: private */
    public final b m;
    /* access modifiers changed from: private */
    public boolean n;

    public c(Context context, com.facebook.ads.internal.m.c cVar, b bVar, String str) {
        this(context, cVar, bVar, (List<com.facebook.ads.internal.b.b>) new ArrayList<com.facebook.ads.internal.b.b>(), str);
    }

    public c(Context context, com.facebook.ads.internal.m.c cVar, b bVar, String str, @Nullable Bundle bundle) {
        this(context, cVar, bVar, new ArrayList(), str, bundle);
    }

    public c(Context context, com.facebook.ads.internal.m.c cVar, b bVar, List<com.facebook.ads.internal.b.b> list, String str) {
        super(context, cVar, bVar, list, str);
        this.b = new w() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public void a(v vVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    c.this.e();
                }
            }
        };
        this.c = new f<r>() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public Class<r> a() {
                return r.class;
            }

            public void a(r rVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    c.this.i();
                }
            }
        };
        this.d = new f<h>() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public Class<h> a() {
                return h.class;
            }

            public void a(h hVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    c.this.j();
                }
            }
        };
        this.e = new f<j>() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public Class<j> a() {
                return j.class;
            }

            public void a(j jVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    if (!c.this.n) {
                        c.this.n = true;
                    } else {
                        c.this.k();
                    }
                }
            }
        };
        this.f = new f<n>() {
            public Class<n> a() {
                return n.class;
            }

            public void a(n nVar) {
                if (c.this.a <= 0 || c.this.m.getCurrentPosition() != c.this.m.getDuration() || c.this.m.getDuration() <= c.this.a) {
                    c.this.a(c.this.m.getCurrentPosition());
                }
            }
        };
        this.g = new f<b>() {
            public Class<b> a() {
                return b.class;
            }

            public void a(b bVar) {
                c cVar;
                int a2 = bVar.a();
                int b = bVar.b();
                if (c.this.a <= 0 || a2 != b || b <= c.this.a) {
                    if ((a2 != 0 || !c.this.m.g()) && b >= a2 + 500) {
                        cVar = c.this;
                    } else if (b == 0) {
                        cVar = c.this;
                        a2 = c.this.a;
                    } else {
                        c.this.b(b);
                        return;
                    }
                    cVar.b(a2);
                }
            }
        };
        this.h = new f<p>() {
            public Class<p> a() {
                return p.class;
            }

            public void a(p pVar) {
                c.this.a(pVar.a(), pVar.b());
            }
        };
        this.i = new f<x>() {
            public Class<x> a() {
                return x.class;
            }

            public void a(x xVar) {
                c.this.b();
            }
        };
        this.j = new f<y>() {
            public Class<y> a() {
                return y.class;
            }

            public void a(y yVar) {
                c.this.c();
            }
        };
        this.k = new f<s>() {
            public Class<s> a() {
                return s.class;
            }

            public void a(s sVar) {
                c.this.a(c.this.l(), c.this.l());
            }
        };
        this.l = new m() {
            public void a(l lVar) {
                c.this.a = c.this.m.getDuration();
            }
        };
        this.n = false;
        this.m = bVar;
        this.m.getEventBus().a((T[]) new f[]{this.b, this.f, this.c, this.e, this.d, this.g, this.h, this.i, this.j, this.l, this.k});
    }

    public c(Context context, com.facebook.ads.internal.m.c cVar, b bVar, List<com.facebook.ads.internal.b.b> list, String str, @Nullable Bundle bundle) {
        super(context, cVar, bVar, list, str, bundle);
        this.b = new w() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public void a(v vVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    c.this.e();
                }
            }
        };
        this.c = new f<r>() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public Class<r> a() {
                return r.class;
            }

            public void a(r rVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    c.this.i();
                }
            }
        };
        this.d = new f<h>() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public Class<h> a() {
                return h.class;
            }

            public void a(h hVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    c.this.j();
                }
            }
        };
        this.e = new f<j>() {
            static final /* synthetic */ boolean a = true;

            static {
                Class<c> cls = c.class;
            }

            public Class<j> a() {
                return j.class;
            }

            public void a(j jVar) {
                if (!a && c.this == null) {
                    throw new AssertionError();
                } else if (c.this != null) {
                    if (!c.this.n) {
                        c.this.n = true;
                    } else {
                        c.this.k();
                    }
                }
            }
        };
        this.f = new f<n>() {
            public Class<n> a() {
                return n.class;
            }

            public void a(n nVar) {
                if (c.this.a <= 0 || c.this.m.getCurrentPosition() != c.this.m.getDuration() || c.this.m.getDuration() <= c.this.a) {
                    c.this.a(c.this.m.getCurrentPosition());
                }
            }
        };
        this.g = new f<b>() {
            public Class<b> a() {
                return b.class;
            }

            public void a(b bVar) {
                c cVar;
                int a2 = bVar.a();
                int b = bVar.b();
                if (c.this.a <= 0 || a2 != b || b <= c.this.a) {
                    if ((a2 != 0 || !c.this.m.g()) && b >= a2 + 500) {
                        cVar = c.this;
                    } else if (b == 0) {
                        cVar = c.this;
                        a2 = c.this.a;
                    } else {
                        c.this.b(b);
                        return;
                    }
                    cVar.b(a2);
                }
            }
        };
        this.h = new f<p>() {
            public Class<p> a() {
                return p.class;
            }

            public void a(p pVar) {
                c.this.a(pVar.a(), pVar.b());
            }
        };
        this.i = new f<x>() {
            public Class<x> a() {
                return x.class;
            }

            public void a(x xVar) {
                c.this.b();
            }
        };
        this.j = new f<y>() {
            public Class<y> a() {
                return y.class;
            }

            public void a(y yVar) {
                c.this.c();
            }
        };
        this.k = new f<s>() {
            public Class<s> a() {
                return s.class;
            }

            public void a(s sVar) {
                c.this.a(c.this.l(), c.this.l());
            }
        };
        this.l = new m() {
            public void a(l lVar) {
                c.this.a = c.this.m.getDuration();
            }
        };
        this.n = false;
        this.m = bVar;
        this.m.getEventBus().a((T[]) new f[]{this.b, this.f, this.c, this.e, this.d, this.g, this.i, this.j, this.k});
    }

    public void a() {
        this.m.getStateHandler().post(new Runnable() {
            public void run() {
                c.this.m.getEventBus().b((T[]) new f[]{c.this.b, c.this.f, c.this.c, c.this.e, c.this.d, c.this.g, c.this.h, c.this.i, c.this.j, c.this.l, c.this.k});
            }
        });
    }
}
