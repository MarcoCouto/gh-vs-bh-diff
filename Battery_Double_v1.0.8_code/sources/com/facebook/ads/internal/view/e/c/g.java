package com.facebook.ads.internal.view.e.c;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.view.b.d;
import com.facebook.ads.internal.view.e.a.c;
import com.facebook.ads.internal.view.e.b.b;
import com.facebook.ads.internal.view.e.b.j;

public class g extends c implements OnLayoutChangeListener {
    private final ImageView a;
    private final f<j> b = new f<j>() {
        public Class<j> a() {
            return j.class;
        }

        public void a(j jVar) {
            g.this.setVisibility(8);
        }
    };
    private final f<b> c = new f<b>() {
        public Class<b> a() {
            return b.class;
        }

        public void a(b bVar) {
            g.this.setVisibility(0);
        }
    };

    public g(Context context) {
        super(context);
        this.a = new ImageView(context);
        this.a.setScaleType(ScaleType.FIT_CENTER);
        this.a.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.a.setLayoutParams(new LayoutParams(-1, -1));
        addView(this.a);
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        if (getVideoView() != null) {
            getVideoView().getEventBus().a((T[]) new f[]{this.b, this.c});
            getVideoView().addOnLayoutChangeListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (getVideoView() != null) {
            getVideoView().removeOnLayoutChangeListener(this);
            getVideoView().getEventBus().b((T[]) new f[]{this.c, this.b});
        }
        super.b();
    }

    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        int i9 = i4 - i2;
        int i10 = i3 - i;
        if (layoutParams.height != i9 || layoutParams.width != i10 || layoutParams.topMargin != i2 || layoutParams.leftMargin != i) {
            LayoutParams layoutParams2 = new LayoutParams(i10, i9);
            layoutParams2.topMargin = i2;
            layoutParams2.leftMargin = i;
            this.a.setLayoutParams(new LayoutParams(i10, i9));
            setLayoutParams(layoutParams2);
        }
    }

    public void setImage(@Nullable String str) {
        if (str == null) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        new d(this.a).a().a(str);
    }
}
