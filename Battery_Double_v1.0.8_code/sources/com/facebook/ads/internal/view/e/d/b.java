package com.facebook.ads.internal.view.e.d;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.media.MediaPlayer.TrackInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.view.e.a.a;
import java.io.IOException;

@TargetApi(14)
public class b extends TextureView implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, OnSeekCompleteListener, OnVideoSizeChangedListener, SurfaceTextureListener, c {
    private static final String s = "b";
    private Uri a;
    private e b;
    private Surface c;
    /* access modifiers changed from: private */
    @Nullable
    public MediaPlayer d;
    /* access modifiers changed from: private */
    public MediaController e;
    private d f = d.IDLE;
    private d g = d.IDLE;
    private d h = d.IDLE;
    private boolean i = false;
    private View j;
    private int k = 0;
    private long l;
    private int m = 0;
    private int n = 0;
    private float o = 1.0f;
    private boolean p = false;
    private int q = 3;
    private boolean r = false;
    private int t = 0;
    /* access modifiers changed from: private */
    public boolean u = false;
    private a v = a.NOT_STARTED;
    private final MediaPlayerControl w = new MediaPlayerControl() {
        public boolean canPause() {
            return true;
        }

        public boolean canSeekBackward() {
            return true;
        }

        public boolean canSeekForward() {
            return true;
        }

        public int getAudioSessionId() {
            if (b.this.d != null) {
                return b.this.d.getAudioSessionId();
            }
            return 0;
        }

        public int getBufferPercentage() {
            return 0;
        }

        public int getCurrentPosition() {
            return b.this.getCurrentPosition();
        }

        public int getDuration() {
            return b.this.getDuration();
        }

        public boolean isPlaying() {
            return b.this.d != null && b.this.d.isPlaying();
        }

        public void pause() {
            b.this.a(true);
        }

        public void seekTo(int i) {
            b.this.a(i);
        }

        public void start() {
            b.this.a(a.USER_STARTED);
        }
    };

    public b(Context context) {
        super(context);
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public b(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    @TargetApi(21)
    public b(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
    }

    private boolean a(@Nullable Surface surface) {
        if (this.d == null) {
            return false;
        }
        try {
            this.d.setSurface(surface);
            return true;
        } catch (IllegalStateException e2) {
            com.facebook.ads.internal.q.d.a.a(e2, getContext());
            Log.d(s, "The MediaPlayer failed", e2);
            return false;
        }
    }

    private boolean f() {
        return this.f == d.PREPARED || this.f == d.STARTED || this.f == d.PAUSED || this.f == d.PLAYBACK_COMPLETED;
    }

    private boolean g() {
        if (this.d == null) {
            return false;
        }
        try {
            this.d.reset();
            return true;
        } catch (IllegalStateException e2) {
            com.facebook.ads.internal.q.d.a.a(e2, getContext());
            Log.d(s, "The MediaPlayer failed", e2);
            return false;
        }
    }

    private boolean h() {
        return (this.f == d.PREPARING || this.f == d.PREPARED) ? false : true;
    }

    private boolean i() {
        return (this.f == d.PREPARING || this.f == d.PREPARED) ? false : true;
    }

    private void setVideoState(d dVar) {
        if (dVar != this.f) {
            this.f = dVar;
            if (this.b != null) {
                this.b.a(dVar);
            }
        }
    }

    public void a() {
        if (!this.r) {
            a(false);
        }
    }

    public void a(int i2) {
        if (this.d == null || !f()) {
            this.k = i2;
        } else if (i2 < getDuration() && i2 > 0) {
            this.t = getCurrentPosition();
            this.k = i2;
            this.d.seekTo(i2);
        }
    }

    public void a(a aVar) {
        this.g = d.STARTED;
        this.v = aVar;
        if (this.f == d.STARTED || this.f == d.PREPARED || this.f == d.IDLE || this.f == d.PAUSED || this.f == d.PLAYBACK_COMPLETED) {
            if (this.d == null) {
                setup(this.a);
            } else {
                if (this.k > 0) {
                    this.d.seekTo(this.k);
                }
                this.d.start();
                if (this.f != d.PREPARED) {
                    setVideoState(d.STARTED);
                }
            }
        }
        if (isAvailable()) {
            onSurfaceTextureAvailable(getSurfaceTexture(), 0, 0);
        }
    }

    public void a(boolean z) {
        d dVar;
        this.g = d.PAUSED;
        if (this.d == null) {
            dVar = d.IDLE;
        } else if (i()) {
            if (z) {
                this.h = d.PAUSED;
                this.i = true;
            }
            this.d.pause();
            if (this.f != d.PLAYBACK_COMPLETED) {
                dVar = d.PAUSED;
            }
        } else {
            return;
        }
        setVideoState(dVar);
    }

    public void b() {
        setVideoState(d.PLAYBACK_COMPLETED);
        c();
        this.k = 0;
    }

    public void b(boolean z) {
        if (this.e != null) {
            this.e.setVisibility(8);
        }
        this.u = true;
    }

    public void c() {
        this.g = d.IDLE;
        if (this.d != null) {
            int currentPosition = this.d.getCurrentPosition();
            if (currentPosition > 0) {
                this.k = currentPosition;
            }
            this.d.stop();
            g();
            this.d.release();
            this.d = null;
            if (this.e != null) {
                this.e.hide();
                this.e.setEnabled(false);
            }
        }
        setVideoState(d.IDLE);
    }

    @SuppressLint({"NewApi"})
    public boolean d() {
        if (this.d == null || VERSION.SDK_INT < 16) {
            return false;
        }
        try {
            for (TrackInfo trackType : this.d.getTrackInfo()) {
                if (trackType.getTrackType() == 2) {
                    return true;
                }
            }
            return false;
        } catch (IllegalStateException e2) {
            Log.e(s, "Couldn't retrieve video information", e2);
            return true;
        }
    }

    public void e() {
        if (this.d != null) {
            a((Surface) null);
            this.d.setOnBufferingUpdateListener(null);
            this.d.setOnCompletionListener(null);
            this.d.setOnErrorListener(null);
            this.d.setOnInfoListener(null);
            this.d.setOnPreparedListener(null);
            this.d.setOnVideoSizeChangedListener(null);
            this.d.setOnSeekCompleteListener(null);
            g();
            this.d = null;
        }
    }

    public int getCurrentPosition() {
        if (this.d == null || !f()) {
            return 0;
        }
        return this.d.getCurrentPosition();
    }

    public int getDuration() {
        if (this.d == null || !f()) {
            return 0;
        }
        return this.d.getDuration();
    }

    public long getInitialBufferTime() {
        return this.l;
    }

    public a getStartReason() {
        return this.v;
    }

    public d getState() {
        return this.f;
    }

    public d getTargetState() {
        return this.g;
    }

    public int getVideoHeight() {
        return this.n;
    }

    public int getVideoWidth() {
        return this.m;
    }

    public View getView() {
        return this;
    }

    public float getVolume() {
        return this.o;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.d != null) {
            this.d.pause();
        }
        setVideoState(d.PLAYBACK_COMPLETED);
        a(0);
        this.k = 0;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        if (this.q <= 0 || getState() != d.STARTED) {
            setVideoState(d.ERROR);
            c();
            return true;
        }
        this.q--;
        c();
        a(this.v);
        return true;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        d dVar;
        if (i2 != 3) {
            switch (i2) {
                case 701:
                    dVar = d.BUFFERING;
                case 702:
                    if (h()) {
                        dVar = d.STARTED;
                    }
                    break;
            }
            setVideoState(dVar);
            return false;
        }
        setVideoState(d.STARTED);
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        if ((r5.m * r7) > (r5.n * r6)) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0066, code lost:
        if (r1 > r6) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007e, code lost:
        if (r1 > r6) goto L_0x0043;
     */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int defaultSize = getDefaultSize(this.m, i2);
        int defaultSize2 = getDefaultSize(this.n, i3);
        if (this.m <= 0 || this.n <= 0) {
            i5 = defaultSize;
        } else {
            int mode = MeasureSpec.getMode(i2);
            i5 = MeasureSpec.getSize(i2);
            int mode2 = MeasureSpec.getMode(i3);
            i4 = MeasureSpec.getSize(i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                if (this.m * i4 < this.n * i5) {
                    i5 = (this.m * i4) / this.n;
                }
                setMeasuredDimension(i5, i4);
            }
            if (mode == 1073741824) {
                int i7 = (this.n * i5) / this.m;
                if (mode2 != Integer.MIN_VALUE || i7 <= i4) {
                    i4 = i7;
                }
            } else {
                if (mode2 == 1073741824) {
                    i6 = (this.m * i4) / this.n;
                    if (mode == Integer.MIN_VALUE) {
                    }
                } else {
                    int i8 = this.m;
                    int i9 = this.n;
                    if (mode2 != Integer.MIN_VALUE || i9 <= i4) {
                        i6 = i8;
                        i4 = i9;
                    } else {
                        i6 = (this.m * i4) / this.n;
                    }
                    if (mode == Integer.MIN_VALUE) {
                    }
                }
                i5 = i6;
            }
            setMeasuredDimension(i5, i4);
            defaultSize2 = (this.n * i5) / this.m;
        }
        i4 = defaultSize2;
        setMeasuredDimension(i5, i4);
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        setVideoState(d.PREPARED);
        if (this.p && !this.u) {
            this.e = new MediaController(getContext());
            this.e.setAnchorView(this.j == null ? this : this.j);
            this.e.setMediaPlayer(this.w);
            this.e.setEnabled(true);
        }
        setRequestedVolume(this.o);
        this.m = mediaPlayer.getVideoWidth();
        this.n = mediaPlayer.getVideoHeight();
        if (this.k > 0) {
            if (this.k >= this.d.getDuration()) {
                this.k = 0;
            }
            this.d.seekTo(this.k);
            this.k = 0;
        }
        if (this.g == d.STARTED) {
            a(this.v);
        }
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        if (this.b != null) {
            this.b.a(this.t, this.k);
            this.k = 0;
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (this.c == null) {
            this.c = new Surface(surfaceTexture);
        }
        if (!a(this.c)) {
            setVideoState(d.ERROR);
            e();
            return;
        }
        this.i = false;
        if (this.f == d.PAUSED && this.h != d.PAUSED) {
            a(this.v);
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        a((Surface) null);
        if (this.c != null) {
            this.c.release();
            this.c = null;
        }
        if (!this.i) {
            this.h = this.p ? d.STARTED : this.f;
            this.i = true;
        }
        if (this.f != d.PAUSED) {
            a(false);
        }
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.m = mediaPlayer.getVideoWidth();
        this.n = mediaPlayer.getVideoHeight();
        if (this.m != 0 && this.n != 0) {
            requestLayout();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.d != null) {
            if (this.e == null || !this.e.isShowing()) {
                if (!z) {
                    if (!this.i) {
                        this.h = this.p ? d.STARTED : this.f;
                        this.i = true;
                    }
                    if (this.f != d.PAUSED) {
                        a();
                    }
                } else {
                    this.i = false;
                    if (this.f == d.PAUSED && this.h != d.PAUSED) {
                        a(this.v);
                    }
                }
            }
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (VERSION.SDK_INT < 24) {
            super.setBackgroundDrawable(drawable);
            return;
        }
        if (AdInternalSettings.isDebugBuild()) {
            Log.w(s, "Google always throw an exception with setBackgroundDrawable on Nougat above. so we silently ignore it.");
        }
    }

    public void setBackgroundPlaybackEnabled(boolean z) {
        this.r = z;
    }

    public void setControlsAnchorView(View view) {
        this.j = view;
        view.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!b.this.u && b.this.e != null && motionEvent.getAction() == 1) {
                    if (b.this.e.isShowing()) {
                        b.this.e.hide();
                        return true;
                    }
                    b.this.e.show();
                }
                return true;
            }
        });
    }

    public void setForeground(Drawable drawable) {
        if (VERSION.SDK_INT < 24) {
            super.setForeground(drawable);
            return;
        }
        if (AdInternalSettings.isDebugBuild()) {
            Log.w(s, "Google always throw an exception with setForeground on Nougat above. so we silently ignore it.");
        }
    }

    public void setFullScreen(boolean z) {
        this.p = z;
        if (this.p && !this.u) {
            setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (!b.this.u && b.this.e != null && motionEvent.getAction() == 1) {
                        if (b.this.e.isShowing()) {
                            b.this.e.hide();
                            return true;
                        }
                        b.this.e.show();
                    }
                    return true;
                }
            });
        }
    }

    public void setRequestedVolume(float f2) {
        this.o = f2;
        if (this.d != null && this.f != d.PREPARING && this.f != d.IDLE) {
            this.d.setVolume(f2, f2);
        }
    }

    public void setVideoMPD(@Nullable String str) {
    }

    public void setVideoStateChangeListener(e eVar) {
        this.b = eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0091 A[SYNTHETIC, Splitter:B:28:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ac A[SYNTHETIC, Splitter:B:34:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    public void setup(Uri uri) {
        MediaPlayer mediaPlayer;
        String str;
        String sb;
        this.a = uri;
        AssetFileDescriptor assetFileDescriptor = null;
        if (this.d != null) {
            g();
            a((Surface) null);
            mediaPlayer = this.d;
        } else {
            mediaPlayer = new MediaPlayer();
        }
        try {
            if (uri.getScheme().equals("asset")) {
                try {
                    AssetFileDescriptor openFd = getContext().getAssets().openFd(uri.getPath().substring(1));
                    try {
                        MediaPlayer mediaPlayer2 = mediaPlayer;
                        mediaPlayer2.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
                        if (openFd != null) {
                            try {
                                openFd.close();
                            } catch (IOException e2) {
                                str = s;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Unable to close");
                                sb2.append(e2);
                                sb = sb2.toString();
                                Log.w(str, sb);
                                mediaPlayer.setLooping(false);
                                mediaPlayer.setOnBufferingUpdateListener(this);
                                mediaPlayer.setOnCompletionListener(this);
                                mediaPlayer.setOnErrorListener(this);
                                mediaPlayer.setOnInfoListener(this);
                                mediaPlayer.setOnPreparedListener(this);
                                mediaPlayer.setOnVideoSizeChangedListener(this);
                                mediaPlayer.setOnSeekCompleteListener(this);
                                mediaPlayer.prepareAsync();
                                this.d = mediaPlayer;
                                setVideoState(d.PREPARING);
                                setSurfaceTextureListener(this);
                                if (isAvailable()) {
                                }
                            }
                        }
                    } catch (IOException | SecurityException e3) {
                        Object obj = e3;
                        assetFileDescriptor = openFd;
                        e = obj;
                        try {
                            String str2 = s;
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Failed to open assets ");
                            sb3.append(e);
                            Log.w(str2, sb3.toString());
                            setVideoState(d.ERROR);
                            if (assetFileDescriptor != null) {
                            }
                            mediaPlayer.setLooping(false);
                            mediaPlayer.setOnBufferingUpdateListener(this);
                            mediaPlayer.setOnCompletionListener(this);
                            mediaPlayer.setOnErrorListener(this);
                            mediaPlayer.setOnInfoListener(this);
                            mediaPlayer.setOnPreparedListener(this);
                            mediaPlayer.setOnVideoSizeChangedListener(this);
                            mediaPlayer.setOnSeekCompleteListener(this);
                            mediaPlayer.prepareAsync();
                            this.d = mediaPlayer;
                            setVideoState(d.PREPARING);
                            setSurfaceTextureListener(this);
                            if (isAvailable()) {
                            }
                        } catch (Throwable th) {
                            th = th;
                            if (assetFileDescriptor != null) {
                                try {
                                    assetFileDescriptor.close();
                                } catch (IOException e4) {
                                    String str3 = s;
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append("Unable to close");
                                    sb4.append(e4);
                                    Log.w(str3, sb4.toString());
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        assetFileDescriptor = openFd;
                        th = th3;
                        if (assetFileDescriptor != null) {
                        }
                        throw th;
                    }
                } catch (IOException | SecurityException e5) {
                    e = e5;
                    String str22 = s;
                    StringBuilder sb32 = new StringBuilder();
                    sb32.append("Failed to open assets ");
                    sb32.append(e);
                    Log.w(str22, sb32.toString());
                    setVideoState(d.ERROR);
                    if (assetFileDescriptor != null) {
                        try {
                            assetFileDescriptor.close();
                        } catch (IOException e6) {
                            str = s;
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append("Unable to close");
                            sb5.append(e6);
                            sb = sb5.toString();
                            Log.w(str, sb);
                            mediaPlayer.setLooping(false);
                            mediaPlayer.setOnBufferingUpdateListener(this);
                            mediaPlayer.setOnCompletionListener(this);
                            mediaPlayer.setOnErrorListener(this);
                            mediaPlayer.setOnInfoListener(this);
                            mediaPlayer.setOnPreparedListener(this);
                            mediaPlayer.setOnVideoSizeChangedListener(this);
                            mediaPlayer.setOnSeekCompleteListener(this);
                            mediaPlayer.prepareAsync();
                            this.d = mediaPlayer;
                            setVideoState(d.PREPARING);
                            setSurfaceTextureListener(this);
                            if (isAvailable()) {
                            }
                        }
                    }
                    mediaPlayer.setLooping(false);
                    mediaPlayer.setOnBufferingUpdateListener(this);
                    mediaPlayer.setOnCompletionListener(this);
                    mediaPlayer.setOnErrorListener(this);
                    mediaPlayer.setOnInfoListener(this);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.setOnVideoSizeChangedListener(this);
                    mediaPlayer.setOnSeekCompleteListener(this);
                    mediaPlayer.prepareAsync();
                    this.d = mediaPlayer;
                    setVideoState(d.PREPARING);
                    setSurfaceTextureListener(this);
                    if (isAvailable()) {
                    }
                }
            } else {
                mediaPlayer.setDataSource(uri.toString());
            }
            mediaPlayer.setLooping(false);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnInfoListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnVideoSizeChangedListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.prepareAsync();
            this.d = mediaPlayer;
            setVideoState(d.PREPARING);
        } catch (Exception e7) {
            setVideoState(d.ERROR);
            mediaPlayer.release();
            String str4 = s;
            StringBuilder sb6 = new StringBuilder();
            sb6.append("Cannot prepare media player with SurfaceTexture: ");
            sb6.append(e7);
            Log.e(str4, sb6.toString());
        }
        setSurfaceTextureListener(this);
        if (isAvailable()) {
            onSurfaceTextureAvailable(getSurfaceTexture(), 0, 0);
        }
    }
}
