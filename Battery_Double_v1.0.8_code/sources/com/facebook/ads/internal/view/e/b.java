package com.facebook.ads.internal.view.e;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.view.e.b.d;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.l;
import com.facebook.ads.internal.view.e.b.n;
import com.facebook.ads.internal.view.e.b.p;
import com.facebook.ads.internal.view.e.b.r;
import com.facebook.ads.internal.view.e.b.s;
import com.facebook.ads.internal.view.e.b.t;
import com.facebook.ads.internal.view.e.b.v;
import com.facebook.ads.internal.view.e.b.x;
import com.facebook.ads.internal.view.e.b.y;
import com.facebook.ads.internal.view.e.d.a;
import com.facebook.ads.internal.view.e.d.c;
import com.facebook.ads.internal.view.e.d.e;
import java.util.ArrayList;
import java.util.List;

public class b extends RelativeLayout implements a, e {
    /* access modifiers changed from: private */
    public static final l b = new l();
    /* access modifiers changed from: private */
    public static final d c = new d();
    /* access modifiers changed from: private */
    public static final n d = new n();
    /* access modifiers changed from: private */
    public static final r e = new r();
    /* access modifiers changed from: private */
    public static final h f = new h();
    /* access modifiers changed from: private */
    public static final s g = new s();
    /* access modifiers changed from: private */
    public static final j h = new j();
    private static final v i = new v();
    private static final y j = new y();
    private static final x k = new x();
    protected final c a;
    private final List<com.facebook.ads.internal.view.e.a.b> l = new ArrayList();
    /* access modifiers changed from: private */
    public final Handler m = new Handler();
    private final Handler n = new Handler();
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.j.e<f, com.facebook.ads.internal.j.d> o = new com.facebook.ads.internal.j.e<>();
    /* access modifiers changed from: private */
    public boolean p;
    private boolean q;
    private final OnTouchListener r = new OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            b.this.o.a(new t(view, motionEvent));
            return false;
        }
    };

    public b(Context context) {
        super(context);
        this.a = com.facebook.ads.internal.l.a.a(context) ? new com.facebook.ads.internal.view.e.d.a(context) : new com.facebook.ads.internal.view.e.d.b(context);
        a();
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = com.facebook.ads.internal.l.a.a(context) ? new com.facebook.ads.internal.view.e.d.a(context, attributeSet) : new com.facebook.ads.internal.view.e.d.b(context, attributeSet);
        a();
    }

    public b(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = com.facebook.ads.internal.l.a.a(context) ? new com.facebook.ads.internal.view.e.d.a(context, attributeSet, i2) : new com.facebook.ads.internal.view.e.d.b(context, attributeSet, i2);
        a();
    }

    @TargetApi(21)
    public b(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.a = com.facebook.ads.internal.l.a.a(context) ? new com.facebook.ads.internal.view.e.d.a(context, attributeSet, i2, i3) : new com.facebook.ads.internal.view.e.d.b(context, attributeSet, i2, i3);
        a();
    }

    private void a() {
        if (g() && (this.a instanceof com.facebook.ads.internal.view.e.d.a)) {
            ((com.facebook.ads.internal.view.e.d.a) this.a).setTestMode(AdInternalSettings.isTestMode(getContext()));
        }
        this.a.setRequestedVolume(1.0f);
        this.a.setVideoStateChangeListener(this);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(13);
        addView((View) this.a, layoutParams);
        setOnTouchListener(this.r);
    }

    public void a(int i2) {
        this.a.a(i2);
    }

    public void a(final int i2, final int i3) {
        this.n.post(new Runnable() {
            public void run() {
                b.this.o.a(new p(i2, i3));
            }
        });
    }

    public void a(com.facebook.ads.internal.view.e.a.a aVar) {
        if (this.p && this.a.getState() == com.facebook.ads.internal.view.e.d.d.PLAYBACK_COMPLETED) {
            this.p = false;
        }
        this.a.a(aVar);
    }

    public void a(com.facebook.ads.internal.view.e.a.b bVar) {
        this.l.add(bVar);
    }

    public void a(final com.facebook.ads.internal.view.e.d.d dVar) {
        final int currentPosition = getCurrentPosition();
        final int duration = getDuration();
        this.n.post(new Runnable() {
            public void run() {
                com.facebook.ads.internal.j.e a2;
                com.facebook.ads.internal.j.d q;
                com.facebook.ads.internal.j.e a3;
                com.facebook.ads.internal.j.d bVar;
                if (dVar == com.facebook.ads.internal.view.e.d.d.PREPARED) {
                    a3 = b.this.o;
                    bVar = b.b;
                } else if (dVar == com.facebook.ads.internal.view.e.d.d.ERROR) {
                    b.this.p = true;
                    a3 = b.this.o;
                    bVar = b.c;
                } else if (dVar == com.facebook.ads.internal.view.e.d.d.PLAYBACK_COMPLETED) {
                    b.this.p = true;
                    b.this.m.removeCallbacksAndMessages(null);
                    a3 = b.this.o;
                    bVar = new com.facebook.ads.internal.view.e.b.b(currentPosition, duration);
                } else if (dVar == com.facebook.ads.internal.view.e.d.d.STARTED) {
                    b.this.o.a(b.h);
                    b.this.m.removeCallbacksAndMessages(null);
                    b.this.m.postDelayed(new Runnable() {
                        public void run() {
                            if (!b.this.p) {
                                b.this.o.a(b.d);
                                b.this.m.postDelayed(this, 250);
                            }
                        }
                    }, 250);
                    return;
                } else {
                    if (dVar == com.facebook.ads.internal.view.e.d.d.PAUSED) {
                        a2 = b.this.o;
                        q = b.f;
                    } else if (dVar == com.facebook.ads.internal.view.e.d.d.IDLE) {
                        a2 = b.this.o;
                        q = b.g;
                    } else {
                        return;
                    }
                    a2.a(q);
                    b.this.m.removeCallbacksAndMessages(null);
                    return;
                }
                a3.a(bVar);
            }
        });
    }

    public void a(boolean z) {
        this.a.a(z);
    }

    public void c() {
        for (com.facebook.ads.internal.view.e.a.b bVar : this.l) {
            if (bVar instanceof com.facebook.ads.internal.view.e.a.c) {
                com.facebook.ads.internal.view.e.a.c cVar = (com.facebook.ads.internal.view.e.a.c) bVar;
                if (cVar.getParent() == null) {
                    addView(cVar);
                    cVar.a(this);
                }
            } else {
                bVar.a(this);
            }
        }
    }

    public void d() {
        for (com.facebook.ads.internal.view.e.a.b bVar : this.l) {
            if (bVar instanceof com.facebook.ads.internal.view.e.a.c) {
                com.facebook.ads.internal.view.e.a.c cVar = (com.facebook.ads.internal.view.e.a.c) bVar;
                if (cVar.getParent() != null) {
                    cVar.b(this);
                    removeView(cVar);
                }
            } else {
                bVar.b(this);
            }
        }
    }

    public void e() {
        this.n.post(new Runnable() {
            public void run() {
                b.this.getEventBus().a(b.e);
            }
        });
        this.a.b();
    }

    public void f() {
        this.a.c();
    }

    public boolean g() {
        return com.facebook.ads.internal.l.a.a(getContext());
    }

    public int getCurrentPosition() {
        return this.a.getCurrentPosition();
    }

    public int getDuration() {
        return this.a.getDuration();
    }

    @NonNull
    public com.facebook.ads.internal.j.e<f, com.facebook.ads.internal.j.d> getEventBus() {
        return this.o;
    }

    public long getInitialBufferTime() {
        return this.a.getInitialBufferTime();
    }

    public com.facebook.ads.internal.view.e.d.d getState() {
        return this.a.getState();
    }

    /* access modifiers changed from: protected */
    public Handler getStateHandler() {
        return this.n;
    }

    public TextureView getTextureView() {
        return (TextureView) this.a;
    }

    public int getVideoHeight() {
        return this.a.getVideoHeight();
    }

    public com.facebook.ads.internal.view.e.a.a getVideoStartReason() {
        return this.a.getStartReason();
    }

    public View getVideoView() {
        return this.a.getView();
    }

    public int getVideoWidth() {
        return this.a.getVideoWidth();
    }

    public float getVolume() {
        return this.a.getVolume();
    }

    public void h() {
        this.a.b(true);
    }

    public boolean i() {
        return this.q;
    }

    public boolean j() {
        return this.a.d();
    }

    public void k() {
        this.a.setVideoStateChangeListener(null);
        this.a.e();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.o.a(k);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.o.a(j);
        super.onDetachedFromWindow();
    }

    public void setControlsAnchorView(View view) {
        if (this.a != null) {
            this.a.setControlsAnchorView(view);
        }
    }

    public void setIsFullScreen(boolean z) {
        this.q = z;
        this.a.setFullScreen(z);
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        super.setLayoutParams(layoutParams);
    }

    public void setVideoMPD(@Nullable String str) {
        this.a.setVideoMPD(str);
    }

    public void setVideoURI(@Nullable Uri uri) {
        if (uri == null) {
            d();
        } else {
            c();
            this.a.setup(uri);
        }
        this.p = false;
    }

    public void setVideoURI(@Nullable String str) {
        setVideoURI(str != null ? Uri.parse(str) : null);
    }

    public void setVolume(float f2) {
        this.a.setRequestedVolume(f2);
        getEventBus().a(i);
    }
}
