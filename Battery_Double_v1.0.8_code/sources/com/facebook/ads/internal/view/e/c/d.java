package com.facebook.ads.internal.view.e.c;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.view.e.a.b;
import com.facebook.ads.internal.view.e.b.c;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.i;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.k;
import com.facebook.ads.internal.view.e.b.t;
import com.facebook.ads.internal.view.e.b.u;

@TargetApi(12)
public class d implements b {
    private final i a;
    private final k b;
    private final c c;
    private final u d;
    /* access modifiers changed from: private */
    public final Handler e;
    /* access modifiers changed from: private */
    public final boolean f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    @Nullable
    public a h;
    /* access modifiers changed from: private */
    @Nullable
    public com.facebook.ads.internal.view.e.b i;
    /* access modifiers changed from: private */
    public boolean j;

    public enum a {
        VISIBLE,
        INVSIBLE,
        FADE_OUT_ON_PLAY
    }

    public d(View view, a aVar) {
        this(view, aVar, false);
    }

    public d(View view, a aVar, boolean z) {
        this.a = new i() {
            public void a(h hVar) {
                d.this.a(1, 0);
            }
        };
        this.b = new k() {
            public void a(j jVar) {
                if (d.this.j) {
                    if (d.this.h == a.FADE_OUT_ON_PLAY || d.this.f) {
                        d.this.h = null;
                        d.this.c();
                        return;
                    }
                    d.this.a(0, 8);
                }
            }
        };
        this.c = new c() {
            public void a(com.facebook.ads.internal.view.e.b.b bVar) {
                if (d.this.h != a.INVSIBLE) {
                    d.this.g.setAlpha(1.0f);
                    d.this.g.setVisibility(0);
                }
            }
        };
        this.d = new u() {
            public void a(t tVar) {
                if (d.this.i != null && tVar.b().getAction() == 0) {
                    d.this.e.removeCallbacksAndMessages(null);
                    d.this.a((AnimatorListenerAdapter) new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animator) {
                            d.this.e.postDelayed(new Runnable() {
                                public void run() {
                                    if (d.this.j) {
                                        d.this.c();
                                    }
                                }
                            }, 2000);
                        }
                    });
                }
            }
        };
        this.j = true;
        this.e = new Handler();
        this.f = z;
        a(view, aVar);
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        this.e.removeCallbacksAndMessages(null);
        this.g.clearAnimation();
        this.g.setAlpha((float) i2);
        this.g.setVisibility(i3);
    }

    /* access modifiers changed from: private */
    public void a(AnimatorListenerAdapter animatorListenerAdapter) {
        this.g.setVisibility(0);
        this.g.animate().alpha(1.0f).setDuration(500).setListener(animatorListenerAdapter);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.g.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                d.this.g.setVisibility(8);
            }
        });
    }

    public void a(View view, a aVar) {
        View view2;
        int i2;
        this.h = aVar;
        this.g = view;
        this.g.clearAnimation();
        if (aVar == a.INVSIBLE) {
            this.g.setAlpha(0.0f);
            view2 = this.g;
            i2 = 8;
        } else {
            this.g.setAlpha(1.0f);
            view2 = this.g;
            i2 = 0;
        }
        view2.setVisibility(i2);
    }

    public void a(com.facebook.ads.internal.view.e.b bVar) {
        this.i = bVar;
        bVar.getEventBus().a((T[]) new f[]{this.a, this.b, this.d, this.c});
    }

    public boolean a() {
        return this.j;
    }

    public void b() {
        this.j = false;
        a((AnimatorListenerAdapter) null);
    }

    public void b(com.facebook.ads.internal.view.e.b bVar) {
        a(1, 0);
        bVar.getEventBus().b((T[]) new f[]{this.c, this.d, this.b, this.a});
        this.i = null;
    }
}
