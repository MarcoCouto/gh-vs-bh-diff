package com.facebook.ads.internal.view.e.a;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.view.e.b;

public abstract class c extends RelativeLayout implements b {
    @Nullable
    private b a;

    public c(Context context) {
        super(context);
    }

    public c(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setLayoutParams(new LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public void a(b bVar) {
        this.a = bVar;
        a();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public void b(b bVar) {
        b();
        this.a = null;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public b getVideoView() {
        return this.a;
    }
}
