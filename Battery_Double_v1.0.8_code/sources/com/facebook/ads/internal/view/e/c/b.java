package com.facebook.ads.internal.view.e.c;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.view.e.a.c;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.i;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.k;
import com.google.android.exoplayer2.util.MimeTypes;
import java.lang.ref.WeakReference;

public class b extends c implements OnAudioFocusChangeListener {
    /* access modifiers changed from: private */
    public WeakReference<OnAudioFocusChangeListener> a = null;
    private final com.facebook.ads.internal.view.e.b.c b = new com.facebook.ads.internal.view.e.b.c() {
        public void a(com.facebook.ads.internal.view.e.b.b bVar) {
            ((AudioManager) b.this.getContext().getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO)).abandonAudioFocus(b.this.a == null ? null : (OnAudioFocusChangeListener) b.this.a.get());
        }
    };
    private final i c = new i() {
        public void a(h hVar) {
            ((AudioManager) b.this.getContext().getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO)).abandonAudioFocus(b.this.a == null ? null : (OnAudioFocusChangeListener) b.this.a.get());
        }
    };
    private final k d = new k() {
        public void a(j jVar) {
            if (b.this.a == null || b.this.a.get() == null) {
                b.this.a = new WeakReference(new OnAudioFocusChangeListener() {
                    public void onAudioFocusChange(int i) {
                        if (b.this.getVideoView() != null && i <= 0) {
                            b.this.getVideoView().a(false);
                        }
                    }
                });
            }
            ((AudioManager) b.this.getContext().getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO)).requestAudioFocus((OnAudioFocusChangeListener) b.this.a.get(), 3, 1);
        }
    };

    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        if (getVideoView() != null) {
            getVideoView().getEventBus().a((T[]) new f[]{this.d, this.b, this.c});
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (getVideoView() != null) {
            getVideoView().getEventBus().b((T[]) new f[]{this.c, this.b, this.d});
        }
        super.b();
    }

    public void onAudioFocusChange(int i) {
        if (getVideoView() != null && i <= 0) {
            getVideoView().a(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ((AudioManager) getContext().getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO)).abandonAudioFocus(this.a == null ? null : (OnAudioFocusChangeListener) this.a.get());
        super.onDetachedFromWindow();
    }
}
