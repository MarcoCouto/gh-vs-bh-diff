package com.facebook.ads.internal.view.d;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.adapters.ad;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.b.d;
import com.facebook.ads.internal.view.component.e;
import com.facebook.ads.internal.view.component.i;
import com.facebook.ads.internal.view.e.b.z;
import java.util.HashMap;

public class a extends LinearLayout {
    private static final int a = ((int) (12.0f * v.b));
    private static final int b = ((int) (16.0f * v.b));
    private final i c;
    private final ImageView d;
    private final RelativeLayout e;
    private final com.facebook.ads.internal.view.component.a f;
    private final int g;

    public a(Context context, int i, j jVar, c cVar, C0008a aVar, boolean z, boolean z2) {
        Context context2 = context;
        int i2 = i;
        super(context);
        this.g = i2;
        this.d = new e(context2);
        v.a((View) this.d);
        LayoutParams layoutParams = new LayoutParams(i2, i2);
        layoutParams.addRule(15);
        layoutParams.addRule(9);
        layoutParams.setMargins(0, 0, a, 0);
        if (z2) {
            this.d.setVisibility(8);
        }
        i iVar = new i(context2, jVar, true, z, true);
        this.c = iVar;
        this.c.setAlignment(GravityCompat.START);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(1, this.d.getId());
        layoutParams2.addRule(15);
        com.facebook.ads.internal.view.component.a aVar2 = new com.facebook.ads.internal.view.component.a(context2, true, false, z.REWARDED_VIDEO_AD_CLICK.a(), jVar, cVar, aVar);
        this.f = aVar2;
        this.f.setVisibility(8);
        this.e = new RelativeLayout(context2);
        v.a((View) this.e);
        this.e.addView(this.d, layoutParams);
        this.e.addView(this.c, layoutParams2);
        addView(this.e, new LinearLayout.LayoutParams(-2, -2));
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, -15658735});
        gradientDrawable.setCornerRadius(0.0f);
        v.a((View) this, (Drawable) gradientDrawable);
    }

    public void a() {
        this.f.setVisibility(0);
    }

    public void a(int i) {
        v.b(this.f);
        int i2 = 1;
        if (i != 1) {
            i2 = 0;
        }
        setOrientation(i2);
        int i3 = -1;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i2 != 0 ? -1 : 0, -2);
        layoutParams.weight = 1.0f;
        if (i2 == 0) {
            i3 = -2;
        }
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(i3, -2);
        layoutParams2.setMargins(i2 != 0 ? 0 : b, i2 != 0 ? b : 0, 0, 0);
        layoutParams2.gravity = 80;
        this.e.setLayoutParams(layoutParams);
        addView(this.f, layoutParams2);
    }

    public void setInfo(ad adVar) {
        this.c.a(adVar.f(), adVar.g(), false, false);
        this.f.a(adVar.q(), adVar.p(), adVar.b(), new HashMap());
        if (!TextUtils.isEmpty(adVar.i())) {
            new d(this.d).a(this.g, this.g).a(adVar.i());
        }
    }
}
