package com.facebook.ads.internal.view;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.v4.graphics.ColorUtils;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.view.component.CircularProgressView;
import com.facebook.ads.internal.view.e.a.b;
import com.facebook.ads.internal.view.e.b.c;
import com.facebook.ads.internal.view.e.b.n;
import com.facebook.ads.internal.view.e.b.o;

public class g extends LinearLayout implements b {
    private static final float c = Resources.getSystem().getDisplayMetrics().density;
    private static final int d = ((int) (40.0f * c));
    private static final int e = ((int) (44.0f * c));
    private static final int f = ((int) (10.0f * c));
    private static final int g = ((int) (16.0f * c));
    private static final int h = (g - f);
    private static final int i = ((2 * g) - f);
    /* access modifiers changed from: private */
    public final o a = new o() {
        public void a(n nVar) {
            if (g.this.q != null && g.this.r != 0 && g.this.m.isShown()) {
                float currentPosition = ((float) g.this.q.getCurrentPosition()) / Math.min(((float) g.this.r) * 1000.0f, (float) g.this.q.getDuration());
                g.this.m.setProgressWithAnimation(100.0f * currentPosition);
                if (currentPosition >= 1.0f) {
                    g.this.a(true);
                    g.this.q.getEventBus().b((T[]) new f[]{g.this.a, g.this.b});
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public final c b = new c() {
        public void a(com.facebook.ads.internal.view.e.b.b bVar) {
            if (g.this.q != null && g.this.r != 0 && g.this.m.isShown() && !g.this.t) {
                g.this.a(true);
                g.this.q.getEventBus().b((T[]) new f[]{g.this.a, g.this.b});
            }
        }
    };
    private final ImageView j;
    private final FrameLayout k;
    private final ImageView l;
    /* access modifiers changed from: private */
    public final CircularProgressView m;
    private final com.facebook.ads.internal.view.c.c n;
    /* access modifiers changed from: private */
    public final PopupMenu o;
    /* access modifiers changed from: private */
    @Nullable
    public a p;
    /* access modifiers changed from: private */
    @Nullable
    public com.facebook.ads.internal.view.e.b q;
    /* access modifiers changed from: private */
    public int r = 0;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public boolean t = false;
    private OnDismissListener u;

    public interface a {
        void a();
    }

    public g(Context context) {
        super(context);
        setGravity(16);
        if (VERSION.SDK_INT >= 14) {
            this.u = new OnDismissListener() {
                public void onDismiss(PopupMenu popupMenu) {
                    g.this.s = false;
                }
            };
        }
        this.l = new ImageView(context);
        this.l.setPadding(f, f, f, f);
        this.l.setScaleType(ScaleType.FIT_CENTER);
        this.l.setImageBitmap(com.facebook.ads.internal.q.b.c.a(com.facebook.ads.internal.q.b.b.INTERSTITIAL_CLOSE));
        this.l.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (g.this.p != null && g.this.t) {
                    g.this.p.a();
                }
            }
        });
        this.m = new CircularProgressView(context);
        this.m.setPadding(f, f, f, f);
        this.m.setProgress(0.0f);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.setMargins(h, h, i, h);
        LayoutParams layoutParams2 = new LayoutParams(e, e);
        this.k = new FrameLayout(context);
        this.k.setLayoutTransition(new LayoutTransition());
        this.k.addView(this.l, layoutParams2);
        this.k.addView(this.m, layoutParams2);
        addView(this.k, layoutParams);
        this.n = new com.facebook.ads.internal.view.c.c(context);
        LayoutParams layoutParams3 = new LayoutParams(0, -2);
        layoutParams3.gravity = 17;
        layoutParams3.weight = 1.0f;
        addView(this.n, layoutParams3);
        this.j = new ImageView(context);
        this.j.setPadding(f, f, f, f);
        this.j.setScaleType(ScaleType.FIT_CENTER);
        this.j.setImageBitmap(com.facebook.ads.internal.q.b.c.a(com.facebook.ads.internal.q.b.b.INTERSTITIAL_AD_CHOICES));
        this.j.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                g.this.o.show();
                g.this.s = true;
            }
        });
        this.o = new PopupMenu(context, this.j);
        this.o.getMenu().add("Ad Choices");
        LayoutParams layoutParams4 = new LayoutParams(d, d);
        layoutParams4.setMargins(0, g / 2, g / 2, g / 2);
        addView(this.j, layoutParams4);
    }

    public void a(j jVar, boolean z) {
        int a2 = jVar.a(z);
        this.n.a(jVar.g(z), a2);
        this.j.setColorFilter(a2);
        this.l.setColorFilter(a2);
        this.m.a(ColorUtils.setAlphaComponent(a2, 77), a2);
        if (z) {
            GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-1778384896, 0});
            gradientDrawable.setCornerRadius(0.0f);
            v.a((View) this, (Drawable) gradientDrawable);
            return;
        }
        v.a((View) this, 0);
    }

    public void a(com.facebook.ads.internal.view.e.b bVar) {
        this.q = bVar;
        this.q.getEventBus().a((T[]) new f[]{this.a, this.b});
    }

    public void a(String str, String str2, String str3, final String str4, final String str5, int i2) {
        this.r = i2;
        this.n.a(str, str2, str3);
        this.o.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                g.this.s = false;
                if (!TextUtils.isEmpty(str4)) {
                    com.facebook.ads.internal.q.c.g.a(new com.facebook.ads.internal.q.c.g(), g.this.getContext(), Uri.parse(str4), str5);
                }
                return true;
            }
        });
        if (VERSION.SDK_INT >= 14) {
            this.o.setOnDismissListener(this.u);
        }
        a(this.r <= 0);
    }

    public void a(boolean z) {
        this.t = z;
        int i2 = 0;
        this.k.setVisibility(0);
        this.m.setVisibility(z ? 4 : 0);
        ImageView imageView = this.l;
        if (!z) {
            i2 = 4;
        }
        imageView.setVisibility(i2);
    }

    public boolean a() {
        return this.t;
    }

    public void b() {
        this.t = false;
        this.k.setVisibility(4);
        this.m.setVisibility(4);
        this.l.setVisibility(4);
    }

    public void b(com.facebook.ads.internal.view.e.b bVar) {
        if (this.q != null) {
            this.q.getEventBus().b((T[]) new f[]{this.a, this.b});
            this.q = null;
        }
    }

    public void c() {
        this.n.setVisibility(4);
    }

    public void d() {
        if (VERSION.SDK_INT >= 14) {
            this.o.setOnDismissListener(null);
        }
        this.o.dismiss();
        if (VERSION.SDK_INT >= 14) {
            this.o.setOnDismissListener(this.u);
        }
    }

    public void e() {
        if (this.s && VERSION.SDK_INT >= 14) {
            this.o.show();
        }
    }

    public void setToolbarListener(a aVar) {
        this.p = aVar;
    }
}
