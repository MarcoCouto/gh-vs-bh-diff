package com.facebook.ads.internal.view;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import com.facebook.ads.internal.adapters.j;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.r.a.C0006a;
import com.facebook.ads.internal.view.a.C0008a;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class i extends Adapter<b> {
    private final c a;
    private final s b;
    private final j c;
    @Nullable
    private C0008a d;
    private int e;
    private int f;
    private String g;
    private boolean h;
    private int i;
    private List<a> j;

    public static class a {
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        private final int f;
        private final int g;
        /* access modifiers changed from: private */
        @Nullable
        public com.facebook.ads.internal.r.a h;
        /* access modifiers changed from: private */
        public boolean i = false;
        private C0006a j;

        public a(int i2, int i3, String str, String str2, String str3, String str4, String str5) {
            this.f = i2;
            this.g = i3;
            this.e = str;
            this.a = str2;
            this.b = str3;
            this.c = str4;
            this.d = str5;
        }

        /* access modifiers changed from: private */
        public void a(final c cVar, final s sVar, final String str, h hVar) {
            if (!this.i) {
                if (this.h != null) {
                    this.h.b();
                    this.h = null;
                }
                this.j = new C0006a() {
                    public void a() {
                        if (!TextUtils.isEmpty(str)) {
                            Map a2 = a.this.a();
                            if (a.this.h != null) {
                                a.this.h.a(a2);
                            }
                            a2.put("touch", com.facebook.ads.internal.q.a.j.a(sVar.e()));
                            cVar.a(str, a2);
                        }
                        a.this.i = true;
                    }
                };
                this.h = new com.facebook.ads.internal.r.a(hVar, 10, this.j);
                this.h.a(100);
                this.h.b(100);
                this.h.a();
            }
        }

        public Map<String, String> a() {
            HashMap hashMap = new HashMap();
            StringBuilder sb = new StringBuilder();
            sb.append(this.f);
            sb.append("");
            hashMap.put("cardind", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.g);
            sb2.append("");
            hashMap.put("cardcnt", sb2.toString());
            return hashMap;
        }
    }

    public static class b extends ViewHolder {
        public h a;

        public b(h hVar) {
            super(hVar);
            this.a = hVar;
        }
    }

    public i(List<a> list, c cVar, s sVar, C0008a aVar, j jVar, String str, int i2, int i3, int i4, boolean z) {
        this.a = cVar;
        this.b = sVar;
        this.d = aVar;
        this.j = list;
        this.f = i2;
        this.c = jVar;
        this.h = z;
        this.g = str;
        this.e = i4;
        this.i = i3;
    }

    /* renamed from: a */
    public b onCreateViewHolder(ViewGroup viewGroup, int i2) {
        h hVar = new h(viewGroup.getContext(), this.c, this.h, this.a, this.d, this.g);
        return new b(hVar);
    }

    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i2) {
        MarginLayoutParams marginLayoutParams = new MarginLayoutParams(this.f, -2);
        marginLayoutParams.setMargins(i2 == 0 ? this.e : this.i, 0, i2 >= this.j.size() + -1 ? this.e : this.i, 0);
        a aVar = (a) this.j.get(i2);
        bVar.a.setImageUrl(aVar.e);
        bVar.a.setLayoutParams(marginLayoutParams);
        bVar.a.a(aVar.a, aVar.b);
        bVar.a.a(aVar.c, aVar.d, aVar.a());
        aVar.a(this.a, this.b, this.g, bVar.a);
    }

    public int getItemCount() {
        return this.j.size();
    }
}
