package com.facebook.ads.internal.view;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.InterstitialAdActivity;
import com.facebook.ads.NativeAd;
import com.facebook.ads.internal.adapters.ah;
import com.facebook.ads.internal.j.a;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.settings.a.C0007a;
import com.facebook.ads.internal.view.e.b;
import com.facebook.ads.internal.view.e.b.c;
import com.facebook.ads.internal.view.e.b.h;
import com.facebook.ads.internal.view.e.b.i;
import com.facebook.ads.internal.view.e.b.j;
import com.facebook.ads.internal.view.e.b.k;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import java.util.UUID;

public class n extends b {
    private final String b = UUID.randomUUID().toString();
    private final k c = new k() {
        public void a(j jVar) {
            if (n.this.n != null) {
                n.this.n.c();
            }
        }
    };
    private final i d = new i() {
        public void a(h hVar) {
            if (n.this.n != null) {
                n.this.n.b();
            }
        }
    };
    private final c e = new c() {
        public void a(com.facebook.ads.internal.view.e.b.b bVar) {
            if (n.this.n != null) {
                n.this.n.h();
            }
        }
    };
    private final ah f;
    private com.facebook.ads.internal.m.c g;
    @Nullable
    private com.facebook.ads.internal.view.e.c h;
    @Nullable
    private String i;
    @Nullable
    private Uri j;
    @Nullable
    private String k;
    @Nullable
    private String l;
    @Nullable
    private String m;
    /* access modifiers changed from: private */
    @Nullable
    public o n;
    @Nullable
    private NativeAd o;

    public n(Context context) {
        super(context);
        this.f = new ah(this, context);
        s();
    }

    public n(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = new ah(this, context);
        s();
    }

    public n(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = new ah(this, context);
        s();
    }

    @TargetApi(21)
    public n(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.f = new ah(this, context);
        s();
    }

    private void a(Intent intent) {
        if (this.i == null || this.h == null) {
            throw new IllegalStateException("Must setVideoReportUri first.");
        } else if (this.j == null && this.l == null) {
            throw new IllegalStateException("Must setVideoURI or setVideoMPD first.");
        } else {
            intent.putExtra("useNativeCtaButton", this.m);
            intent.putExtra(AudienceNetworkActivity.VIEW_TYPE, C0007a.FULL_SCREEN_VIDEO);
            intent.putExtra(AudienceNetworkActivity.VIDEO_URL, this.j.toString());
            intent.putExtra(AudienceNetworkActivity.CLIENT_TOKEN, this.k == null ? "" : this.k);
            intent.putExtra(AudienceNetworkActivity.VIDEO_MPD, this.l);
            intent.putExtra(AudienceNetworkActivity.PREDEFINED_ORIENTATION_KEY, 13);
            intent.putExtra(AudienceNetworkActivity.VIDEO_SEEK_TIME, getCurrentPosition());
            intent.putExtra(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.b);
            intent.putExtra(AudienceNetworkActivity.VIDEO_LOGGER, this.h.g());
            intent.addFlags(ErrorDialogData.BINDER_CRASH);
        }
    }

    private void s() {
        getEventBus().a((T[]) new f[]{this.c, this.d, this.e});
    }

    public void a() {
        Context context = getContext();
        Intent intent = new Intent(context, AudienceNetworkActivity.class);
        a(intent);
        try {
            a(false);
            setVisibility(8);
            context.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            try {
                intent.setClass(context, InterstitialAdActivity.class);
                context.startActivity(intent);
            } catch (Exception e2) {
                com.facebook.ads.internal.j.b.a(a.a(e2, "Error occurred while loading fullscreen video activity."));
            }
        }
    }

    public void a(@Nullable String str, @Nullable String str2) {
        if (this.h != null) {
            this.h.a();
        }
        this.k = str2;
        this.i = str;
        this.h = (str == null || str2 == null) ? null : new com.facebook.ads.internal.view.e.c(getContext(), this.g, this, str2);
    }

    public void b() {
        if (this.o != null) {
            this.o.onCtaBroadcast();
        }
    }

    @Nullable
    public o getListener() {
        return this.n;
    }

    public String getUniqueId() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f.a();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.f.b();
        super.onDetachedFromWindow();
    }

    public void setAdEventManager(com.facebook.ads.internal.m.c cVar) {
        this.g = cVar;
    }

    public void setEnableBackgroundVideo(boolean z) {
        this.a.setBackgroundPlaybackEnabled(z);
    }

    public void setListener(@Nullable o oVar) {
        this.n = oVar;
    }

    public void setNativeAd(@Nullable NativeAd nativeAd) {
        this.o = nativeAd;
    }

    public void setVideoCTA(@Nullable String str) {
        this.m = str;
    }

    public void setVideoMPD(@Nullable String str) {
        if (str == null || this.h != null) {
            this.l = str;
            super.setVideoMPD(str);
            return;
        }
        throw new IllegalStateException("Must setVideoReportUri first.");
    }

    public void setVideoURI(@Nullable Uri uri) {
        if (uri == null || this.h != null) {
            this.j = uri;
            super.setVideoURI(uri);
            return;
        }
        throw new IllegalStateException("Must setVideoReportUri first.");
    }
}
