package com.facebook.ads.internal.view.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.ads.internal.d.c;
import com.facebook.ads.internal.j.a;
import com.facebook.ads.internal.j.b;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.q.b.e;
import java.lang.ref.WeakReference;

public class d extends AsyncTask<String, Void, Bitmap[]> {
    private static final String b = "d";
    public boolean a = false;
    private final WeakReference<Context> c;
    private final int d;
    @Nullable
    private final WeakReference<ImageView> e;
    @Nullable
    private final WeakReference<b> f;
    @Nullable
    private final WeakReference<ViewGroup> g;
    private e h;
    private int i = -1;
    private int j = -1;

    public d(ViewGroup viewGroup, int i2) {
        this.c = new WeakReference<>(viewGroup.getContext());
        this.f = null;
        this.e = null;
        this.g = new WeakReference<>(viewGroup);
        this.d = i2;
    }

    public d(ImageView imageView) {
        this.c = new WeakReference<>(imageView.getContext());
        this.f = null;
        this.e = new WeakReference<>(imageView);
        this.g = null;
        this.d = 0;
    }

    public d(b bVar) {
        this.c = new WeakReference<>(bVar.getContext());
        this.f = new WeakReference<>(bVar);
        this.e = null;
        this.g = null;
        this.d = 0;
    }

    public d a() {
        this.i = -1;
        this.j = -1;
        return this;
    }

    public d a(int i2, int i3) {
        this.i = i2;
        this.j = i3;
        return this;
    }

    public d a(e eVar) {
        this.h = eVar;
        return this;
    }

    public d a(boolean z) {
        this.a = z;
        return this;
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            executeOnExecutor(THREAD_POOL_EXECUTOR, new String[]{str});
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Bitmap[] bitmapArr) {
        if (this.e != null) {
            ImageView imageView = (ImageView) this.e.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmapArr[0]);
            }
        }
        if (this.f != null) {
            b bVar = (b) this.f.get();
            if (bVar != null) {
                bVar.a(bitmapArr[0], bitmapArr[1]);
            }
        }
        if (!(this.g == null || this.g.get() == null)) {
            v.a((View) this.g.get(), (Drawable) new BitmapDrawable(((Context) this.c.get()).getResources(), bitmapArr[1]));
        }
        if (this.h != null) {
            this.h.a();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap[] doInBackground(String... strArr) {
        Bitmap bitmap;
        Bitmap bitmap2;
        String str = strArr[0];
        Context context = (Context) this.c.get();
        Bitmap bitmap3 = null;
        if (context == null) {
            return new Bitmap[]{null, null};
        }
        try {
            bitmap = c.a(context).a(str, this.i, this.j);
            try {
                boolean z = (this.f == null || this.f.get() == null) ? false : true;
                boolean z2 = (this.g == null || this.g.get() == null) ? false : true;
                if ((z || z2) && bitmap != null) {
                    try {
                        if (!this.a) {
                            e eVar = new e(bitmap);
                            eVar.a(this.d != 0 ? this.d : Math.round(((float) bitmap.getWidth()) / 40.0f));
                            bitmap3 = eVar.a();
                        } else {
                            bitmap3 = bitmap;
                        }
                    } catch (Throwable th) {
                        th = th;
                        bitmap2 = bitmap;
                        String str2 = b;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Error downloading image: ");
                        sb.append(str);
                        Log.e(str2, sb.toString(), th);
                        b.a(a.a(th, null));
                        bitmap3 = bitmap2;
                        return new Bitmap[]{bitmap, bitmap3};
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                bitmap2 = null;
                String str22 = b;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Error downloading image: ");
                sb2.append(str);
                Log.e(str22, sb2.toString(), th);
                b.a(a.a(th, null));
                bitmap3 = bitmap2;
                return new Bitmap[]{bitmap, bitmap3};
            }
        } catch (Throwable th3) {
            th = th3;
            bitmap = null;
            bitmap2 = bitmap;
            String str222 = b;
            StringBuilder sb22 = new StringBuilder();
            sb22.append("Error downloading image: ");
            sb22.append(str);
            Log.e(str222, sb22.toString(), th);
            b.a(a.a(th, null));
            bitmap3 = bitmap2;
            return new Bitmap[]{bitmap, bitmap3};
        }
        return new Bitmap[]{bitmap, bitmap3};
    }
}
