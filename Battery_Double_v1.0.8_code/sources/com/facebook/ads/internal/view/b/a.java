package com.facebook.ads.internal.view.b;

import android.content.Context;
import android.net.http.SslError;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.q.a.s;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.r.a.C0006a;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class a extends com.facebook.ads.internal.q.c.a {
    private static final String a = "a";
    /* access modifiers changed from: private */
    public final WeakReference<b> b;
    /* access modifiers changed from: private */
    public s c = new s();
    private com.facebook.ads.internal.r.a d;
    private C0006a e;

    /* renamed from: com.facebook.ads.internal.view.b.a$a reason: collision with other inner class name */
    public static class C0010a {
        private final String a;
        private final WeakReference<a> b;
        private final WeakReference<b> c;
        private final WeakReference<com.facebook.ads.internal.r.a> d;

        private C0010a(a aVar, b bVar, com.facebook.ads.internal.r.a aVar2) {
            this.a = C0010a.class.getSimpleName();
            this.b = new WeakReference<>(aVar);
            this.c = new WeakReference<>(bVar);
            this.d = new WeakReference<>(aVar2);
        }

        @JavascriptInterface
        public void alert(String str) {
            Log.e(this.a, str);
        }

        @JavascriptInterface
        public String getAnalogInfo() {
            return j.a(com.facebook.ads.internal.g.a.a());
        }

        @JavascriptInterface
        public void onPageInitialized() {
            a aVar = (a) this.b.get();
            if (aVar != null && !aVar.c()) {
                b bVar = (b) this.c.get();
                if (bVar != null) {
                    bVar.a();
                }
                new Handler(Looper.getMainLooper()).post(new c(this.d));
            }
        }
    }

    public interface b {
        void a();

        void a(int i);

        void a(String str, Map<String, String> map);

        void b();
    }

    static class c implements Runnable {
        private final WeakReference<com.facebook.ads.internal.r.a> a;

        c(WeakReference<com.facebook.ads.internal.r.a> weakReference) {
            this.a = weakReference;
        }

        public void run() {
            com.facebook.ads.internal.r.a aVar = (com.facebook.ads.internal.r.a) this.a.get();
            if (aVar != null) {
                aVar.a();
            }
        }
    }

    static class d extends WebViewClient {
        private final WeakReference<b> a;
        private final WeakReference<com.facebook.ads.internal.r.a> b;
        private final WeakReference<s> c;

        d(WeakReference<b> weakReference, WeakReference<com.facebook.ads.internal.r.a> weakReference2, WeakReference<s> weakReference3) {
            this.a = weakReference;
            this.b = weakReference2;
            this.c = weakReference3;
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.cancel();
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            HashMap hashMap = new HashMap();
            if (this.b.get() != null) {
                ((com.facebook.ads.internal.r.a) this.b.get()).a((Map<String, String>) hashMap);
            }
            if (this.c.get() != null) {
                hashMap.put("touch", j.a(((s) this.c.get()).e()));
            }
            if (this.a.get() != null) {
                ((b) this.a.get()).a(str, hashMap);
            }
            return true;
        }
    }

    public a(Context context, WeakReference<b> weakReference, int i) {
        super(context);
        this.b = weakReference;
        this.e = new C0006a() {
            public void a() {
                a.this.c.a();
                if (a.this.b.get() != null) {
                    ((b) a.this.b.get()).b();
                }
            }
        };
        this.d = new com.facebook.ads.internal.r.a(this, i, this.e);
        setWebChromeClient(a());
        setWebViewClient(b());
        getSettings().setSupportZoom(false);
        getSettings().setCacheMode(1);
        addJavascriptInterface(new C0010a((b) weakReference.get(), this.d), "AdControl");
    }

    /* access modifiers changed from: protected */
    public WebChromeClient a() {
        return new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return true;
            }
        };
    }

    public void a(int i, int i2) {
        this.d.a(i);
        this.d.b(i2);
    }

    /* access modifiers changed from: protected */
    public WebViewClient b() {
        return new d(this.b, new WeakReference(this.d), new WeakReference(this.c));
    }

    public void destroy() {
        if (this.d != null) {
            this.d.b();
            this.d = null;
        }
        v.b(this);
        this.e = null;
        this.c = null;
        com.facebook.ads.internal.q.c.b.a(this);
        super.destroy();
    }

    public Map<String, String> getTouchData() {
        return this.c.e();
    }

    public com.facebook.ads.internal.r.a getViewabilityChecker() {
        return this.d;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.c.a(motionEvent, this, this);
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (this.b.get() != null) {
            ((b) this.b.get()).a(i);
        }
        if (this.d != null) {
            if (i == 0) {
                this.d.a();
            } else if (i == 8) {
                this.d.b();
            }
        }
    }
}
