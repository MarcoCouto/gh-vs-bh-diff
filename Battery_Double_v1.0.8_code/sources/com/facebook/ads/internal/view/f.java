package com.facebook.ads.internal.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.r;
import com.facebook.ads.internal.adapters.s;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.b.a;
import com.facebook.ads.internal.view.b.a.b;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class f implements a {
    /* access modifiers changed from: private */
    public static final String a = "f";
    /* access modifiers changed from: private */
    public final C0008a b;
    private final a c;
    private final b d;
    /* access modifiers changed from: private */
    public final s e;
    private final c f;
    /* access modifiers changed from: private */
    public r g;
    private long h = System.currentTimeMillis();
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public C0004a j;

    public f(final AudienceNetworkActivity audienceNetworkActivity, final c cVar, C0008a aVar) {
        this.b = aVar;
        this.f = cVar;
        this.d = new b() {
            private long d = 0;

            public void a() {
                f.this.e.b();
            }

            public void a(int i) {
            }

            public void a(String str, Map<String, String> map) {
                Uri parse = Uri.parse(str);
                if (!"fbad".equals(parse.getScheme()) || !"close".equals(parse.getAuthority())) {
                    long j = this.d;
                    this.d = System.currentTimeMillis();
                    if (this.d - j >= 1000) {
                        if ("fbad".equals(parse.getScheme()) && com.facebook.ads.internal.a.b.a(parse.getAuthority())) {
                            f.this.b.a("com.facebook.ads.interstitial.clicked");
                        }
                        com.facebook.ads.internal.a.a a2 = com.facebook.ads.internal.a.b.a(audienceNetworkActivity, cVar, f.this.g.c(), parse, map);
                        if (a2 != null) {
                            try {
                                f.this.j = a2.a();
                                f.this.i = System.currentTimeMillis();
                                a2.b();
                                return;
                            } catch (Exception e) {
                                Log.e(f.a, "Error executing action", e);
                            }
                        }
                        return;
                    }
                    return;
                }
                audienceNetworkActivity.finish();
            }

            public void b() {
                f.this.e.a();
            }
        };
        this.c = new a(audienceNetworkActivity, new WeakReference(this.d), 1);
        this.c.setLayoutParams(new LayoutParams(-1, -1));
        AudienceNetworkActivity audienceNetworkActivity2 = audienceNetworkActivity;
        c cVar2 = cVar;
        s sVar = new s(audienceNetworkActivity2, cVar2, this.c, this.c.getViewabilityChecker(), new com.facebook.ads.internal.adapters.c() {
            public void a() {
                f.this.b.a("com.facebook.ads.interstitial.impression.logged");
            }
        });
        this.e = sVar;
        aVar.a((View) this.c);
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        if (bundle == null || !bundle.containsKey("dataModel")) {
            this.g = r.b(intent);
            if (this.g != null) {
                this.e.a(this.g);
                this.c.loadDataWithBaseURL(com.facebook.ads.internal.q.c.b.a(), this.g.d(), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, AudienceNetworkActivity.WEBVIEW_ENCODING, null);
                this.c.a(this.g.h(), this.g.i());
            }
            return;
        }
        this.g = r.a(bundle.getBundle("dataModel"));
        if (this.g != null) {
            this.c.loadDataWithBaseURL(com.facebook.ads.internal.q.c.b.a(), this.g.d(), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, AudienceNetworkActivity.WEBVIEW_ENCODING, null);
            this.c.a(this.g.h(), this.g.i());
        }
    }

    public void a(Bundle bundle) {
        if (this.g != null) {
            bundle.putBundle("dataModel", this.g.j());
        }
    }

    public void i() {
        this.c.onPause();
    }

    public void j() {
        if (!(this.i <= 0 || this.j == null || this.g == null)) {
            com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(this.i, this.j, this.g.g()));
        }
        this.c.onResume();
    }

    public void onDestroy() {
        if (this.g != null) {
            com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(this.h, C0004a.XOUT, this.g.g()));
            if (!TextUtils.isEmpty(this.g.c())) {
                HashMap hashMap = new HashMap();
                this.c.getViewabilityChecker().a((Map<String, String>) hashMap);
                hashMap.put("touch", j.a(this.c.getTouchData()));
                this.f.h(this.g.c(), hashMap);
            }
        }
        com.facebook.ads.internal.q.c.b.a(this.c);
        this.c.destroy();
    }

    public void setListener(C0008a aVar) {
    }
}
