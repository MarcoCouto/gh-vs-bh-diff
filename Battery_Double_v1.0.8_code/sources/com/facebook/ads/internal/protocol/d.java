package com.facebook.ads.internal.protocol;

public enum d {
    ADS(0),
    APP_OF_THE_DAY(1);
    
    private final int c;

    private d(int i) {
        this.c = i;
    }

    public int a() {
        return this.c;
    }
}
