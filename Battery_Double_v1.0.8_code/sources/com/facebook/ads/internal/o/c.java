package com.facebook.ads.internal.o;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.ads.internal.f.d;
import com.facebook.ads.internal.f.e;
import com.facebook.ads.internal.p.a.b;
import com.facebook.ads.internal.p.a.m;
import com.facebook.ads.internal.p.a.n;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.f;
import com.facebook.ads.internal.protocol.i;
import com.facebook.ads.internal.q.a.h;
import com.facebook.ads.internal.q.a.l;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.security.MessageDigest;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONException;

public class c {
    private static final l i = new l();
    private static final ThreadPoolExecutor j = ((ThreadPoolExecutor) Executors.newCachedThreadPool(i));
    /* access modifiers changed from: private */
    public final Context a;
    /* access modifiers changed from: private */
    public final e b = e.a();
    private final com.facebook.ads.internal.l.a c = new com.facebook.ads.internal.l.a(this.a);
    /* access modifiers changed from: private */
    public Map<String, String> d;
    private a e;
    /* access modifiers changed from: private */
    public b f;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.p.a.a g;
    /* access modifiers changed from: private */
    public final String h = d.a();

    public interface a {
        void a(g gVar);

        void a(com.facebook.ads.internal.protocol.a aVar);
    }

    public c(Context context) {
        this.a = context.getApplicationContext();
    }

    private void a(g gVar) {
        if (this.e != null) {
            this.e.a(gVar);
        }
        a();
    }

    /* access modifiers changed from: private */
    public void a(com.facebook.ads.internal.protocol.a aVar) {
        if (this.e != null) {
            this.e.a(aVar);
        }
        a();
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        int i2;
        com.facebook.ads.internal.protocol.a aVar;
        try {
            f a2 = this.b.a(str);
            com.facebook.ads.internal.h.c a3 = a2.a();
            if (a3 != null) {
                this.c.a(a3.b());
                a.a(a3.a().d(), this.f);
            }
            switch (a2.b()) {
                case ADS:
                    g gVar = (g) a2;
                    if (a3 != null) {
                        if (a3.a().e()) {
                            a.a(str, this.f);
                        }
                        String str2 = this.d != null ? (String) this.d.get("CLIENT_REQUEST_ID") : null;
                        String c2 = a2.c();
                        if (!TextUtils.isEmpty(c2) && !TextUtils.isEmpty(str2)) {
                            StringBuilder sb = new StringBuilder();
                            for (int i3 = 0; i3 < "26n6n4pnp2p74100oqo8p5o336r8p510".length(); i3++) {
                                char charAt = "26n6n4pnp2p74100oqo8p5o336r8p510".charAt(i3);
                                if ((charAt < 'a' || charAt > 'm') && (charAt < 'A' || charAt > 'M')) {
                                    if ((charAt >= 'n' && charAt <= 'z') || (charAt >= 'N' && charAt <= 'Z')) {
                                        i2 = charAt - 13;
                                    }
                                    sb.append(charAt);
                                } else {
                                    i2 = charAt + 13;
                                }
                                charAt = (char) i2;
                                sb.append(charAt);
                            }
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str2);
                            sb2.append(c2);
                            sb2.append(sb.toString());
                            byte[] bytes = sb2.toString().getBytes("iso-8859-1");
                            MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
                            instance.update(bytes, 0, bytes.length);
                            if (!a2.d().equals(h.a(instance.digest()))) {
                                com.facebook.ads.internal.q.d.a.a(new i(), this.a);
                            }
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(c2);
                            sb3.append(str2);
                            sb3.append(sb.toString());
                            byte[] bytes2 = sb3.toString().getBytes("iso-8859-1");
                            MessageDigest instance2 = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
                            instance2.update(bytes2, 0, bytes2.length);
                            e.a((d) new com.facebook.ads.internal.f.a(c2, h.a(instance2.digest())), this.a);
                        }
                        if (!TextUtils.isEmpty(a2.e()) && !TextUtils.isEmpty(str2)) {
                            new com.facebook.ads.internal.k.a(this.a, str2, a2.e()).a();
                        }
                    }
                    a(gVar);
                    return;
                case ERROR:
                    h hVar = (h) a2;
                    String f2 = hVar.f();
                    AdErrorType adErrorTypeFromCode = AdErrorType.adErrorTypeFromCode(hVar.g(), AdErrorType.ERROR_MESSAGE);
                    if (f2 != null) {
                        str = f2;
                    }
                    aVar = com.facebook.ads.internal.protocol.a.a(adErrorTypeFromCode, str);
                    break;
                default:
                    aVar = com.facebook.ads.internal.protocol.a.a(AdErrorType.UNKNOWN_RESPONSE, str);
                    break;
            }
            a(aVar);
        } catch (Exception e2) {
            a(com.facebook.ads.internal.protocol.a.a(AdErrorType.PARSER_FAILURE, e2.getMessage()));
        }
    }

    /* access modifiers changed from: private */
    public b b() {
        return new b() {
            /* access modifiers changed from: 0000 */
            public void a(m mVar) {
                a.b(c.this.f);
                c.this.g = null;
                try {
                    n a2 = mVar.a();
                    if (a2 != null) {
                        String e = a2.e();
                        f a3 = c.this.b.a(e);
                        if (a3.b() == a.ERROR) {
                            h hVar = (h) a3;
                            String f = hVar.f();
                            AdErrorType adErrorTypeFromCode = AdErrorType.adErrorTypeFromCode(hVar.g(), AdErrorType.ERROR_MESSAGE);
                            c cVar = c.this;
                            if (f != null) {
                                e = f;
                            }
                            cVar.a(com.facebook.ads.internal.protocol.a.a(adErrorTypeFromCode, e));
                            return;
                        }
                    }
                } catch (JSONException unused) {
                }
                c.this.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.NETWORK_ERROR, mVar.getMessage()));
            }

            public void a(n nVar) {
                if (nVar != null) {
                    String e = nVar.e();
                    a.b(c.this.f);
                    c.this.g = null;
                    c.this.a(e);
                }
            }

            public void a(Exception exc) {
                if (m.class.equals(exc.getClass())) {
                    a((m) exc);
                } else {
                    c.this.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.NETWORK_ERROR, exc.getMessage()));
                }
            }
        };
    }

    public void a() {
        if (this.g != null) {
            this.g.c(1);
            this.g.b(1);
            this.g = null;
        }
    }

    public void a(final b bVar) {
        a();
        if (com.facebook.ads.internal.q.c.d.c(this.a) == com.facebook.ads.internal.q.c.d.a.NONE) {
            a(new com.facebook.ads.internal.protocol.a(AdErrorType.NETWORK_ERROR, "No network connection"));
            return;
        }
        this.f = bVar;
        com.facebook.ads.internal.g.a.a(this.a);
        if (a.a(bVar)) {
            String c2 = a.c(bVar);
            if (c2 != null) {
                a(c2);
            } else {
                a(com.facebook.ads.internal.protocol.a.a(AdErrorType.LOAD_TOO_FREQUENTLY, null));
            }
        } else {
            j.submit(new Runnable() {
                /* JADX WARNING: Can't wrap try/catch for region: R(9:8|9|10|11|12|(2:16|(1:18)(3:19|21|22))|20|21|22) */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x00e8, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e9, code lost:
                    com.facebook.ads.internal.o.c.a(r6.b, com.facebook.ads.internal.protocol.a.a(com.facebook.ads.internal.protocol.AdErrorType.AD_REQUEST_FAILED, r0.getMessage()));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f8, code lost:
                    return;
                 */
                /* JADX WARNING: Failed to process nested try/catch */
                /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0095 */
                /* JADX WARNING: Removed duplicated region for block: B:18:0x00ab A[Catch:{ Exception -> 0x00e8 }] */
                /* JADX WARNING: Removed duplicated region for block: B:19:0x00ac A[Catch:{ Exception -> 0x00e8 }] */
                public void run() {
                    boolean z;
                    com.facebook.ads.internal.c.b.a(c.this.a);
                    if (bVar.e().a()) {
                        try {
                            bVar.e().a(com.facebook.ads.internal.c.b.b);
                        } catch (com.facebook.ads.internal.protocol.b e) {
                            c.this.a(com.facebook.ads.internal.protocol.a.a(e));
                        }
                        c.this.a(bVar.e().b());
                        return;
                    }
                    c.this.d = bVar.f();
                    StringBuilder sb = new StringBuilder();
                    sb.append(c.this.a.getPackageName());
                    sb.append(" ");
                    sb.append(c.this.a.getPackageManager().getInstallerPackageName(c.this.a.getPackageName()));
                    c.this.d.put("M_BANNER_KEY", new String(Base64.encode(sb.toString().getBytes(), 2)));
                    if (!(bVar.c == f.NATIVE_250 || bVar.c == f.NATIVE_UNKNOWN)) {
                        if (bVar.c == null) {
                            z = false;
                            c.this.g = com.facebook.ads.internal.q.c.d.b(c.this.a, z);
                            c.this.g.a(c.this.h, c.this.g.b().a(c.this.d), c.this.b());
                        }
                    }
                    z = true;
                    c.this.g = com.facebook.ads.internal.q.c.d.b(c.this.a, z);
                    c.this.g.a(c.this.h, c.this.g.b().a(c.this.d), c.this.b());
                }
            });
        }
    }

    public void a(a aVar) {
        this.e = aVar;
    }
}
