package com.facebook.ads.internal.settings;

public class a {

    /* renamed from: com.facebook.ads.internal.settings.a$a reason: collision with other inner class name */
    public enum C0007a {
        INTERSTITIAL_WEB_VIEW,
        INTERSTITIAL_OLD_NATIVE_VIDEO,
        INTERSTITIAL_NATIVE_VIDEO,
        INTERSTITIAL_NATIVE_IMAGE,
        INTERSTITIAL_NATIVE_CAROUSEL,
        FULL_SCREEN_VIDEO,
        REWARDED_VIDEO,
        BROWSER
    }
}
