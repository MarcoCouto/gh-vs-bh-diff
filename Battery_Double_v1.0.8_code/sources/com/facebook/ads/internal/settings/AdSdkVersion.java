package com.facebook.ads.internal.settings;

public class AdSdkVersion {
    public static final String BUILD = "4.28.2";
}
