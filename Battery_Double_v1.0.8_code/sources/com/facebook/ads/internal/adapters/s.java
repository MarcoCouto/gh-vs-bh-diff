package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.c.a;
import java.util.Map;

public class s extends b {
    /* access modifiers changed from: private */
    public static final String c = "s";
    /* access modifiers changed from: private */
    public final a d;
    private final c e;
    /* access modifiers changed from: private */
    public r f;
    private boolean g;

    public s(Context context, c cVar, a aVar, com.facebook.ads.internal.r.a aVar2, c cVar2) {
        super(context, cVar2, aVar2);
        this.e = cVar;
        this.d = aVar;
    }

    public void a(r rVar) {
        this.f = rVar;
    }

    /* access modifiers changed from: protected */
    public void a(Map<String, String> map) {
        if (this.f != null && !TextUtils.isEmpty(this.f.c())) {
            this.e.a(this.f.c(), map);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        return;
     */
    public synchronized void b() {
        if (!this.g) {
            if (this.f != null) {
                this.g = true;
                if (this.d != null && !TextUtils.isEmpty(this.f.e())) {
                    this.d.post(new Runnable() {
                        public void run() {
                            if (s.this.d.c()) {
                                Log.w(s.c, "Webview already destroyed, cannot activate");
                                return;
                            }
                            a a2 = s.this.d;
                            StringBuilder sb = new StringBuilder();
                            sb.append("javascript:");
                            sb.append(s.this.f.e());
                            a2.loadUrl(sb.toString());
                        }
                    });
                }
            }
        }
    }
}
