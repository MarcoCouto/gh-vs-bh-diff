package com.facebook.ads.internal.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.InterstitialAdActivity;
import com.facebook.ads.internal.a.d;
import com.facebook.ads.internal.d.b;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.settings.a.C0007a;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.json.JSONObject;

public class m extends InterstitialAdapter {
    private static final ConcurrentMap<String, com.facebook.ads.internal.view.a> a = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final String b = UUID.randomUUID().toString();
    private String c;
    private long d;
    private Context e;
    private w f;
    /* access modifiers changed from: private */
    public InterstitialAdapterListener g;
    /* access modifiers changed from: private */
    public boolean h = false;
    private r i;
    /* access modifiers changed from: private */
    public a j = a.UNSPECIFIED;
    private v k;
    private C0007a l;
    /* access modifiers changed from: private */
    public boolean m;

    public enum a {
        UNSPECIFIED,
        VERTICAL,
        HORIZONTAL;

        public static a a(int i) {
            return i == 0 ? UNSPECIFIED : i == 2 ? HORIZONTAL : VERTICAL;
        }
    }

    private int a() {
        int rotation = ((WindowManager) this.e.getSystemService("window")).getDefaultDisplay().getRotation();
        if (this.j == a.UNSPECIFIED) {
            return -1;
        }
        if (this.j != a.HORIZONTAL) {
            return rotation != 2 ? 1 : 9;
        }
        switch (rotation) {
            case 2:
            case 3:
                return 8;
            default:
                return 0;
        }
    }

    public static com.facebook.ads.internal.view.a a(String str) {
        return (com.facebook.ads.internal.view.a) a.get(str);
    }

    public static void a(com.facebook.ads.internal.view.a aVar) {
        for (Entry entry : a.entrySet()) {
            if (entry.getValue() == aVar) {
                a.remove(entry.getKey());
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, com.facebook.ads.internal.view.a aVar) {
        a.put(str, aVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0079, code lost:
        if (r6.g != null) goto L_0x012a;
     */
    public void loadInterstitialAd(Context context, InterstitialAdapterListener interstitialAdapterListener, Map<String, Object> map, c cVar, final EnumSet<CacheFlag> enumSet) {
        b bVar;
        com.facebook.ads.internal.d.a aVar;
        this.e = context;
        this.g = interstitialAdapterListener;
        this.c = (String) map.get(AudienceNetworkActivity.PLACEMENT_ID);
        this.d = ((Long) map.get(AudienceNetworkActivity.REQUEST_TIME)).longValue();
        JSONObject jSONObject = (JSONObject) map.get("data");
        if (jSONObject.has("markup")) {
            this.l = C0007a.INTERSTITIAL_WEB_VIEW;
            this.i = r.a(jSONObject);
            if (d.a(context, this.i, cVar)) {
                interstitialAdapterListener.onInterstitialError(this, AdError.NO_FILL);
                return;
            }
            this.f = new w(context, this.b, this, this.g);
            this.f.a();
            Map f2 = this.i.f();
            if (f2.containsKey("orientation")) {
                this.j = a.a(Integer.parseInt((String) f2.get("orientation")));
            }
            this.h = true;
        } else if (jSONObject.has("video")) {
            this.l = C0007a.INTERSTITIAL_OLD_NATIVE_VIDEO;
            this.f = new w(context, this.b, this, this.g);
            this.f.a();
            final n nVar = new n();
            nVar.a(context, (com.facebook.ads.a.a) new com.facebook.ads.a.a() {
                public void a(u uVar) {
                    m.this.h = true;
                    if (m.this.g != null) {
                        m.this.g.onInterstitialAdLoaded(m.this);
                    }
                }

                public void a(u uVar, View view) {
                    m.this.j = nVar.k();
                    m.b(m.this.b, (com.facebook.ads.internal.view.a) nVar);
                }

                public void a(u uVar, AdError adError) {
                    nVar.l();
                    m.this.g.onInterstitialError(m.this, adError);
                }

                public void b(u uVar) {
                    m.this.g.onInterstitialAdClicked(m.this, "", true);
                }

                public void c(u uVar) {
                    m.this.g.onInterstitialLoggingImpression(m.this);
                }

                public void d(u uVar) {
                }
            }, map, cVar, enumSet);
            return;
        } else {
            this.k = v.a(jSONObject, context);
            if (this.k.d().size() == 0) {
                this.g.onInterstitialError(this, AdError.NO_FILL);
            }
            this.f = new w(context, this.b, this, this.g);
            this.f.a();
            if (jSONObject.has("carousel")) {
                this.l = C0007a.INTERSTITIAL_NATIVE_CAROUSEL;
                b bVar2 = new b(context);
                bVar2.a(this.k.c(), -1, -1);
                List d2 = this.k.d();
                for (int i2 = 0; i2 < d2.size(); i2++) {
                    bVar2.a(((d) d2.get(i2)).f(), ((d) d2.get(i2)).h(), ((d) d2.get(i2)).g());
                }
                bVar2.a((com.facebook.ads.internal.d.a) new com.facebook.ads.internal.d.a() {
                    private void c() {
                        m.this.h = true;
                        m.this.g.onInterstitialAdLoaded(m.this);
                    }

                    public void a() {
                        c();
                    }

                    public void b() {
                        c();
                    }
                });
                this.h = true;
            } else {
                if (jSONObject.has("video_url")) {
                    this.l = C0007a.INTERSTITIAL_NATIVE_VIDEO;
                    bVar = new b(context);
                    bVar.a(((d) this.k.d().get(0)).f(), ((d) this.k.d().get(0)).h(), ((d) this.k.d().get(0)).g());
                    bVar.a(this.k.c(), -1, -1);
                    if (enumSet.contains(CacheFlag.VIDEO)) {
                        bVar.a(((d) this.k.d().get(0)).i());
                    }
                    aVar = new com.facebook.ads.internal.d.a() {
                        private void a(boolean z) {
                            m.this.m = z;
                            m.this.h = true;
                            m.this.g.onInterstitialAdLoaded(m.this);
                        }

                        public void a() {
                            a(enumSet.contains(CacheFlag.VIDEO));
                        }

                        public void b() {
                            a(false);
                        }
                    };
                } else {
                    this.l = C0007a.INTERSTITIAL_NATIVE_IMAGE;
                    bVar = new b(context);
                    bVar.a(((d) this.k.d().get(0)).f(), ((d) this.k.d().get(0)).h(), ((d) this.k.d().get(0)).g());
                    bVar.a(this.k.c(), -1, -1);
                    aVar = new com.facebook.ads.internal.d.a() {
                        private void c() {
                            m.this.h = true;
                            m.this.g.onInterstitialAdLoaded(m.this);
                        }

                        public void a() {
                            c();
                        }

                        public void b() {
                            c();
                        }
                    };
                }
                bVar.a(aVar);
                return;
            }
        }
        this.g.onInterstitialAdLoaded(this);
    }

    public void onDestroy() {
        if (this.f != null) {
            this.f.b();
        }
    }

    public boolean show() {
        if (!this.h) {
            if (this.g != null) {
                this.g.onInterstitialError(this, AdError.INTERNAL_ERROR);
            }
            return false;
        }
        Intent intent = new Intent(this.e, AudienceNetworkActivity.class);
        intent.putExtra(AudienceNetworkActivity.PREDEFINED_ORIENTATION_KEY, a());
        intent.putExtra(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.b);
        intent.putExtra(AudienceNetworkActivity.PLACEMENT_ID, this.c);
        intent.putExtra(AudienceNetworkActivity.REQUEST_TIME, this.d);
        intent.putExtra(AudienceNetworkActivity.VIEW_TYPE, this.l);
        intent.putExtra(AudienceNetworkActivity.USE_CACHE, this.m);
        if (this.k != null) {
            intent.putExtra("ad_data_bundle", this.k);
        } else if (this.i != null) {
            this.i.a(intent);
        }
        intent.addFlags(ErrorDialogData.BINDER_CRASH);
        try {
            this.e.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            intent.setClass(this.e, InterstitialAdActivity.class);
            this.e.startActivity(intent);
        }
        return true;
    }
}
