package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.facebook.ads.internal.a.d.a;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.j.b;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.e.d;
import com.facebook.ads.internal.n.f;
import com.facebook.ads.internal.n.g;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.n.j;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.q.a.p;
import com.facebook.ads.internal.q.a.x;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.ads.mediation.facebook.FacebookAdapter;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class o extends ab implements a {
    private static final String a = "o";
    private f A;
    private String B;
    private String C;
    private h D;
    private List<e> E;
    private int F = -1;
    private int G;
    /* access modifiers changed from: private */
    public String H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    private boolean M;
    private long N = 0;
    private C0004a O = null;
    /* access modifiers changed from: private */
    @Nullable
    public c P;
    private d Q;
    private Context b;
    private ac c;
    private Uri d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private f j;
    private f k;
    private g l;
    private String m;
    private com.facebook.ads.internal.a.c n;
    private Collection<String> o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private int t;
    private int u;
    private int v;
    private String w;
    private String x;
    private j y;
    private String z;

    private boolean H() {
        return this.e != null && this.e.length() > 0 && (this.j != null || this.I) && this.k != null;
    }

    private void I() {
        if (!this.M) {
            if (this.P != null) {
                this.P.a(this.m);
            }
            this.M = true;
        }
    }

    private void a(Context context, JSONObject jSONObject, c cVar, String str, int i2, int i3) {
        this.I = true;
        this.b = context;
        this.P = cVar;
        this.F = i2;
        this.G = i3;
        a(jSONObject, str);
    }

    private void a(Map<String, String> map, final Map<String, String> map2) {
        try {
            final Map c2 = c(map);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (!TextUtils.isEmpty(o.this.H)) {
                        HashMap hashMap = new HashMap();
                        hashMap.putAll(map2);
                        hashMap.putAll(c2);
                        if (o.this.P != null) {
                            o.this.P.e(o.this.H, hashMap);
                        }
                    }
                }
            }, (long) (this.s * 1000));
        } catch (Exception unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x016f A[Catch:{ JSONException -> 0x0195 }, LOOP:0: B:46:0x016d->B:47:0x016f, LOOP_END] */
    private void a(JSONObject jSONObject, String str) {
        JSONArray jSONArray;
        JSONArray optJSONArray;
        int length;
        h hVar;
        if (this.J) {
            throw new IllegalStateException("Adapter already loaded data");
        } else if (jSONObject != null) {
            com.facebook.ads.internal.q.a.d.a(this.b, "Audience Network Loaded");
            this.H = str;
            String a2 = com.facebook.ads.internal.q.a.j.a(jSONObject, "fbad_command");
            this.d = TextUtils.isEmpty(a2) ? null : Uri.parse(a2);
            this.e = com.facebook.ads.internal.q.a.j.a(jSONObject, "title");
            this.f = com.facebook.ads.internal.q.a.j.a(jSONObject, "subtitle");
            this.g = com.facebook.ads.internal.q.a.j.a(jSONObject, TtmlNode.TAG_BODY);
            this.h = com.facebook.ads.internal.q.a.j.a(jSONObject, "call_to_action");
            if (TextUtils.isEmpty(this.h)) {
                this.h = null;
            }
            this.i = com.facebook.ads.internal.q.a.j.a(jSONObject, FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET);
            this.j = f.a(jSONObject.optJSONObject(SettingsJsonConstants.APP_ICON_KEY));
            this.k = f.a(jSONObject.optJSONObject(MessengerShareContentUtility.MEDIA_IMAGE));
            this.l = g.a(jSONObject.optJSONObject("star_rating"));
            this.m = com.facebook.ads.internal.q.a.j.a(jSONObject, "used_report_url");
            this.p = jSONObject.optBoolean("manual_imp");
            this.q = jSONObject.optBoolean("enable_view_log");
            this.r = jSONObject.optBoolean("enable_snapshot_log");
            this.s = jSONObject.optInt("snapshot_log_delay_second", 4);
            this.t = jSONObject.optInt("snapshot_compress_quality", 0);
            this.u = jSONObject.optInt("viewability_check_initial_delay", 0);
            this.v = jSONObject.optInt("viewability_check_interval", 1000);
            JSONObject optJSONObject = jSONObject.optJSONObject("ad_choices_icon");
            JSONObject optJSONObject2 = jSONObject.optJSONObject("native_ui_config");
            if (optJSONObject2 != null) {
                try {
                    if (optJSONObject2.length() != 0) {
                        hVar = new h(optJSONObject2);
                        this.D = hVar;
                        if (optJSONObject != null) {
                            this.A = f.a(optJSONObject);
                        }
                        this.B = com.facebook.ads.internal.q.a.j.a(jSONObject, "ad_choices_link_url");
                        this.C = com.facebook.ads.internal.q.a.j.a(jSONObject, "request_id");
                        this.n = com.facebook.ads.internal.a.c.a(jSONObject.optString("invalidation_behavior"));
                        jSONArray = new JSONArray(jSONObject.optString("detection_strings"));
                        this.o = com.facebook.ads.internal.a.d.a(jSONArray);
                        this.w = com.facebook.ads.internal.q.a.j.a(jSONObject, "video_url");
                        this.x = com.facebook.ads.internal.q.a.j.a(jSONObject, "video_mpd");
                        j jVar = jSONObject.has("video_autoplay_enabled") ? j.DEFAULT : jSONObject.optBoolean("video_autoplay_enabled") ? j.ON : j.OFF;
                        this.y = jVar;
                        this.z = com.facebook.ads.internal.q.a.j.a(jSONObject, "video_report_url");
                        optJSONArray = jSONObject.optJSONArray("carousel");
                        if (optJSONArray != null && optJSONArray.length() > 0) {
                            length = optJSONArray.length();
                            ArrayList arrayList = new ArrayList(length);
                            for (int i2 = 0; i2 < length; i2++) {
                                o oVar = new o();
                                oVar.a(this.b, optJSONArray.getJSONObject(i2), this.P, str, i2, length);
                                arrayList.add(new e(this.b, oVar, null, this.Q));
                            }
                            this.E = arrayList;
                        }
                        this.J = true;
                        this.K = H();
                    }
                } catch (JSONException unused) {
                    this.D = null;
                }
            }
            hVar = null;
            this.D = hVar;
            if (optJSONObject != null) {
            }
            this.B = com.facebook.ads.internal.q.a.j.a(jSONObject, "ad_choices_link_url");
            this.C = com.facebook.ads.internal.q.a.j.a(jSONObject, "request_id");
            this.n = com.facebook.ads.internal.a.c.a(jSONObject.optString("invalidation_behavior"));
            try {
                jSONArray = new JSONArray(jSONObject.optString("detection_strings"));
            } catch (JSONException e2) {
                e2.printStackTrace();
                jSONArray = null;
            }
            this.o = com.facebook.ads.internal.a.d.a(jSONArray);
            this.w = com.facebook.ads.internal.q.a.j.a(jSONObject, "video_url");
            this.x = com.facebook.ads.internal.q.a.j.a(jSONObject, "video_mpd");
            if (jSONObject.has("video_autoplay_enabled")) {
            }
            this.y = jVar;
            this.z = com.facebook.ads.internal.q.a.j.a(jSONObject, "video_report_url");
            try {
                optJSONArray = jSONObject.optJSONArray("carousel");
                length = optJSONArray.length();
                ArrayList arrayList2 = new ArrayList(length);
                while (i2 < length) {
                }
                this.E = arrayList2;
            } catch (JSONException e3) {
                Log.e(a, "Unable to parse carousel data.", e3);
            }
            this.J = true;
            this.K = H();
        }
    }

    private Map<String, String> c(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (map.containsKey("view")) {
            hashMap.put("view", map.get("view"));
        }
        if (map.containsKey("snapshot")) {
            hashMap.put("snapshot", map.get("snapshot"));
        }
        return hashMap;
    }

    public String A() {
        return this.z;
    }

    public List<e> B() {
        if (!c_()) {
            return null;
        }
        return this.E;
    }

    public int C() {
        return this.F;
    }

    public int D() {
        return this.G;
    }

    public com.facebook.ads.internal.n.c E() {
        return com.facebook.ads.internal.n.c.AN;
    }

    public String G() {
        if (!c_()) {
            return null;
        }
        I();
        return this.g;
    }

    public com.facebook.ads.internal.a.c a() {
        return this.n;
    }

    public void a(int i2) {
        if (c_() && i2 == 0 && this.N > 0 && this.O != null) {
            b.a(com.facebook.ads.internal.j.a.a(this.N, this.O, this.C));
            this.N = 0;
            this.O = null;
        }
    }

    public void a(Context context, ac acVar, c cVar, Map<String, Object> map, d dVar) {
        this.b = context;
        this.c = acVar;
        this.P = cVar;
        this.Q = dVar;
        JSONObject jSONObject = (JSONObject) map.get("data");
        a(jSONObject, com.facebook.ads.internal.q.a.j.a(jSONObject, "ct"));
        if (com.facebook.ads.internal.a.d.a(context, this, cVar)) {
            acVar.a(this, new com.facebook.ads.internal.protocol.a(AdErrorType.NO_FILL, "No Fill"));
            return;
        }
        if (acVar != null) {
            acVar.a(this);
        }
        com.facebook.ads.internal.j.a.a = this.C;
    }

    public void a(View view, List<View> list) {
    }

    public void a(ac acVar) {
        this.c = acVar;
    }

    public void a(Map<String, String> map) {
        if (c_() && !this.L) {
            if (this.c != null) {
                this.c.b(this);
            }
            HashMap hashMap = new HashMap();
            if (map != null) {
                hashMap.putAll(map);
            }
            if (this.I) {
                hashMap.put("cardind", String.valueOf(this.F));
                hashMap.put("cardcnt", String.valueOf(this.G));
            }
            if (!TextUtils.isEmpty(c()) && this.P != null) {
                this.P.a(c(), hashMap);
            }
            if (f() || e()) {
                a(map, (Map<String, String>) hashMap);
            }
            this.L = true;
        }
    }

    public boolean a_() {
        return c_() && this.d != null;
    }

    public Collection<String> b() {
        return this.o;
    }

    public void b(Map<String, String> map) {
        if (c_()) {
            if (!com.facebook.ads.internal.l.a.c(this.b) || !x.a(map)) {
                HashMap hashMap = new HashMap();
                if (map != null) {
                    hashMap.putAll(map);
                }
                com.facebook.ads.internal.q.a.d.a(this.b, "Click logged");
                if (this.c != null) {
                    this.c.c(this);
                }
                if (this.I) {
                    hashMap.put("cardind", String.valueOf(this.F));
                    hashMap.put("cardcnt", String.valueOf(this.G));
                }
                com.facebook.ads.internal.a.a a2 = com.facebook.ads.internal.a.b.a(this.b, this.P, this.H, this.d, hashMap);
                if (a2 != null) {
                    try {
                        this.N = System.currentTimeMillis();
                        this.O = a2.a();
                        a2.b();
                        return;
                    } catch (Exception e2) {
                        Log.e(a, "Error executing action", e2);
                    }
                }
                return;
            }
            Log.e(a, "Click happened on lockscreen ad");
        }
    }

    public void b_() {
        if (this.E != null && !this.E.isEmpty()) {
            for (e D2 : this.E) {
                D2.D();
            }
        }
    }

    public String c() {
        return this.H;
    }

    public boolean c_() {
        return this.J && this.K;
    }

    public boolean d() {
        return c_() && this.p;
    }

    public boolean e() {
        return c_() && this.r;
    }

    public boolean f() {
        return c_() && this.q;
    }

    public boolean g() {
        return c_() && this.D != null;
    }

    public boolean h() {
        return true;
    }

    public int i() {
        if (this.t < 0 || this.t > 100) {
            return 0;
        }
        return this.t;
    }

    public int j() {
        return this.u;
    }

    public int k() {
        return this.v;
    }

    public f l() {
        if (!c_()) {
            return null;
        }
        return this.j;
    }

    public f m() {
        if (!c_()) {
            return null;
        }
        return this.k;
    }

    public h n() {
        if (!c_()) {
            return null;
        }
        return this.D;
    }

    public String o() {
        if (!c_()) {
            return null;
        }
        I();
        return this.e;
    }

    public void onDestroy() {
    }

    public String p() {
        if (!c_()) {
            return null;
        }
        I();
        return this.f;
    }

    public String q() {
        if (!c_()) {
            return null;
        }
        I();
        return p.a(this.g);
    }

    public String r() {
        if (!c_()) {
            return null;
        }
        I();
        return this.h;
    }

    public String s() {
        if (!c_()) {
            return null;
        }
        I();
        return this.i;
    }

    public g t() {
        if (!c_()) {
            return null;
        }
        I();
        return this.l;
    }

    public f u() {
        if (!c_()) {
            return null;
        }
        return this.A;
    }

    public String v() {
        if (!c_()) {
            return null;
        }
        return this.B;
    }

    public String w() {
        if (!c_()) {
            return null;
        }
        return "AdChoices";
    }

    public String x() {
        if (!c_()) {
            return null;
        }
        return this.w;
    }

    public String y() {
        if (!c_()) {
            return null;
        }
        return this.x;
    }

    public j z() {
        return !c_() ? j.DEFAULT : this.y;
    }
}
