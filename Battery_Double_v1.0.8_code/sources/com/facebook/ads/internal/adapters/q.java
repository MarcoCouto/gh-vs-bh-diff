package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.view.View;
import com.facebook.ads.internal.n.c;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.e.d;
import com.facebook.ads.internal.n.f;
import com.facebook.ads.internal.n.g;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.n.j;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.a;
import com.flurry.android.FlurryAgent;
import com.flurry.android.ads.FlurryAdErrorType;
import com.flurry.android.ads.FlurryAdNative;
import com.flurry.android.ads.FlurryAdNativeAsset;
import com.flurry.android.ads.FlurryAdNativeListener;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class q extends ab implements x {
    private static volatile boolean a;
    /* access modifiers changed from: private */
    public ac b;
    private FlurryAdNative c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public f j;
    /* access modifiers changed from: private */
    public f k;
    /* access modifiers changed from: private */
    public f l;

    public String A() {
        return null;
    }

    public List<e> B() {
        return null;
    }

    public int C() {
        return 0;
    }

    public int D() {
        return 0;
    }

    public c E() {
        return c.FLURRY;
    }

    public g F() {
        return g.YAHOO;
    }

    public void a(int i2) {
    }

    public void a(final Context context, ac acVar, com.facebook.ads.internal.m.c cVar, Map<String, Object> map, d dVar) {
        JSONObject jSONObject = (JSONObject) map.get("data");
        String optString = jSONObject.optString("api_key");
        String optString2 = jSONObject.optString("placement_id");
        synchronized (q.class) {
            if (!a) {
                StringBuilder sb = new StringBuilder();
                sb.append(y.a(F()));
                sb.append(" Initializing");
                com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                FlurryAgent.setLogEnabled(true);
                FlurryAgent.init(context, optString);
                a = true;
            }
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(y.a(F()));
        sb2.append(" Loading");
        com.facebook.ads.internal.q.a.d.a(context, sb2.toString());
        this.b = acVar;
        this.c = new FlurryAdNative(context, optString2);
        this.c.setListener(new FlurryAdNativeListener() {
            public void onAppExit(FlurryAdNative flurryAdNative) {
            }

            public void onClicked(FlurryAdNative flurryAdNative) {
                if (q.this.b != null) {
                    q.this.b.c(q.this);
                }
            }

            public void onCloseFullscreen(FlurryAdNative flurryAdNative) {
            }

            public void onCollapsed(FlurryAdNative flurryAdNative) {
            }

            public void onError(FlurryAdNative flurryAdNative, FlurryAdErrorType flurryAdErrorType, int i) {
                Context context = context;
                StringBuilder sb = new StringBuilder();
                sb.append(y.a(q.this.F()));
                sb.append(" Failed with FlurryError: ");
                sb.append(flurryAdErrorType.toString());
                com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                if (q.this.b != null) {
                    q.this.b.a(q.this, a.a(AdErrorType.MEDIATION_ERROR, flurryAdErrorType.toString()));
                }
            }

            public void onExpanded(FlurryAdNative flurryAdNative) {
            }

            public void onFetched(FlurryAdNative flurryAdNative) {
                q qVar;
                String str;
                if (q.this.b != null) {
                    if (flurryAdNative.isVideoAd()) {
                        Context context = context;
                        StringBuilder sb = new StringBuilder();
                        sb.append(y.a(q.this.F()));
                        sb.append(" Failed. AN does not support Flurry video ads");
                        com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                        q.this.b.a(q.this, new a(AdErrorType.MEDIATION_ERROR, "video ad"));
                        return;
                    }
                    q.this.d = true;
                    FlurryAdNativeAsset asset = flurryAdNative.getAsset("headline");
                    if (asset != null) {
                        q.this.e = asset.getValue();
                    }
                    FlurryAdNativeAsset asset2 = flurryAdNative.getAsset("summary");
                    if (asset2 != null) {
                        q.this.f = asset2.getValue();
                    }
                    FlurryAdNativeAsset asset3 = flurryAdNative.getAsset("source");
                    if (asset3 != null) {
                        q.this.g = asset3.getValue();
                    }
                    FlurryAdNativeAsset asset4 = flurryAdNative.getAsset("appCategory");
                    if (asset4 != null) {
                        q.this.i = asset4.getValue();
                    }
                    FlurryAdNativeAsset asset5 = flurryAdNative.getAsset("callToAction");
                    if (asset5 != null) {
                        q.this.h = asset5.getValue();
                    } else {
                        if (flurryAdNative.getAsset("appRating") != null) {
                            qVar = q.this;
                            str = "Install Now";
                        } else {
                            qVar = q.this;
                            str = "Learn More";
                        }
                        qVar.h = str;
                    }
                    FlurryAdNativeAsset asset6 = flurryAdNative.getAsset("secImage");
                    if (asset6 != null) {
                        q.this.j = new f(asset6.getValue(), 82, 82);
                    }
                    FlurryAdNativeAsset asset7 = flurryAdNative.getAsset("secHqImage");
                    if (asset7 != null) {
                        q.this.k = new f(asset7.getValue(), 1200, 627);
                    }
                    FlurryAdNativeAsset asset8 = flurryAdNative.getAsset("secBrandingLogo");
                    if (asset8 != null) {
                        q.this.l = new f(asset8.getValue(), 20, 20);
                    }
                    Context context2 = context;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(y.a(q.this.F()));
                    sb2.append(" Loaded");
                    com.facebook.ads.internal.q.a.d.a(context2, sb2.toString());
                    q.this.b.a(q.this);
                }
            }

            public void onImpressionLogged(FlurryAdNative flurryAdNative) {
                if (q.this.b != null) {
                    q.this.b.b(q.this);
                }
            }

            public void onShowFullscreen(FlurryAdNative flurryAdNative) {
            }
        });
        this.c.fetchAd();
    }

    public void a(View view, List<View> list) {
        if (this.c != null) {
            this.c.setTrackingView(view);
        }
    }

    public void a(ac acVar) {
        this.b = acVar;
    }

    public void a(Map<String, String> map) {
    }

    public void b(Map<String, String> map) {
    }

    public void b_() {
        if (this.c != null) {
            this.c.removeTrackingView();
        }
    }

    public String c() {
        return null;
    }

    public boolean c_() {
        return this.d;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public boolean f() {
        return false;
    }

    public boolean g() {
        return false;
    }

    public boolean h() {
        return true;
    }

    public int i() {
        return 0;
    }

    public int j() {
        return 0;
    }

    public int k() {
        return 0;
    }

    public f l() {
        return this.j;
    }

    public f m() {
        return this.k;
    }

    public h n() {
        return null;
    }

    public String o() {
        return this.e;
    }

    public void onDestroy() {
        b_();
        this.b = null;
        if (this.c != null) {
            this.c.destroy();
            this.c = null;
        }
    }

    public String p() {
        return this.g;
    }

    public String q() {
        return this.f;
    }

    public String r() {
        return this.h;
    }

    public String s() {
        return this.i;
    }

    public g t() {
        return null;
    }

    public f u() {
        return this.l;
    }

    public String v() {
        return null;
    }

    public String w() {
        return "Ad";
    }

    public String x() {
        return null;
    }

    public String y() {
        return null;
    }

    public j z() {
        return j.DEFAULT;
    }
}
