package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.h.d;
import com.facebook.ads.internal.j.a.C0004a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.protocol.e;
import com.facebook.ads.internal.view.b.a;
import com.facebook.ads.internal.view.b.a.b;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

public class k extends BannerAdapter {
    /* access modifiers changed from: private */
    public static final String a = "k";
    @Nullable
    private b b;
    @Nullable
    private a c;
    /* access modifiers changed from: private */
    public s d;
    /* access modifiers changed from: private */
    public BannerAdapterListener e;
    private Map<String, Object> f;
    /* access modifiers changed from: private */
    @Nullable
    public c g;
    /* access modifiers changed from: private */
    public Context h;
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public C0004a j;

    private void a(d dVar) {
        this.i = 0;
        this.j = null;
        final r a2 = r.a((JSONObject) this.f.get("data"));
        if (com.facebook.ads.internal.a.d.a(this.h, a2, this.g)) {
            this.e.onBannerError(this, AdError.NO_FILL);
            return;
        }
        this.b = new b() {
            public void a() {
                k.this.d.b();
            }

            public void a(int i) {
                if (i == 0 && k.this.i > 0 && k.this.j != null) {
                    com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(k.this.i, k.this.j, a2.g()));
                    k.this.i = 0;
                    k.this.j = null;
                }
            }

            public void a(String str, Map<String, String> map) {
                Uri parse = Uri.parse(str);
                if ("fbad".equals(parse.getScheme()) && com.facebook.ads.internal.a.b.a(parse.getAuthority()) && k.this.e != null) {
                    k.this.e.onBannerAdClicked(k.this);
                }
                com.facebook.ads.internal.a.a a2 = com.facebook.ads.internal.a.b.a(k.this.h, k.this.g, a2.c(), parse, map);
                if (a2 != null) {
                    try {
                        k.this.j = a2.a();
                        k.this.i = System.currentTimeMillis();
                        a2.b();
                    } catch (Exception e) {
                        Log.e(k.a, "Error executing action", e);
                    }
                }
            }

            public void b() {
                if (k.this.d != null) {
                    k.this.d.a();
                }
            }
        };
        this.c = new a(this.h, new WeakReference(this.b), dVar.f());
        this.c.a(dVar.h(), dVar.i());
        s sVar = new s(this.h, this.g, this.c, this.c.getViewabilityChecker(), new c() {
            public void a() {
                if (k.this.e != null) {
                    k.this.e.onBannerLoggingImpression(k.this);
                }
            }
        });
        this.d = sVar;
        this.d.a(a2);
        this.c.loadDataWithBaseURL(com.facebook.ads.internal.q.c.b.a(), a2.d(), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, AudienceNetworkActivity.WEBVIEW_ENCODING, null);
        if (this.e != null) {
            this.e.onBannerAdLoaded(this, this.c);
        }
    }

    public void loadBannerAd(Context context, c cVar, e eVar, BannerAdapterListener bannerAdapterListener, Map<String, Object> map) {
        this.h = context;
        this.g = cVar;
        this.e = bannerAdapterListener;
        this.f = map;
        a((d) this.f.get("definition"));
    }

    public void onDestroy() {
        if (this.c != null) {
            this.c.destroy();
            this.c = null;
            this.b = null;
        }
    }
}
