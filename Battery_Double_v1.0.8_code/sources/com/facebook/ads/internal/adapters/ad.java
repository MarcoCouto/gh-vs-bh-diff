package com.facebook.ads.internal.adapters;

import android.text.TextUtils;
import com.facebook.ads.internal.j.c;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.github.mikephil.charting.utils.Utils;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ad implements Serializable {
    private static final long serialVersionUID = -5352540727250859603L;
    private final String a;
    private final String b;
    private final byte[] c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private final String j;
    private final int k;
    private final int l;
    private final j m;
    private final j n;
    private final double o;
    private final int p;
    private final List<String> q;
    private final String r;
    private final String s;
    private final String t;
    private final String u;
    private String v;
    private String w;

    private ad(String str, String str2, byte[] bArr, String str3, String str4, String str5, String str6, String str7, String str8, String str9, int i2, int i3, j jVar, j jVar2, double d2, int i4, List<String> list, String str10, String str11, String str12, String str13) {
        this.a = str;
        this.b = str2;
        this.c = bArr;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = str8;
        this.j = str9;
        this.k = i2;
        this.l = i3;
        this.m = jVar;
        this.n = jVar2;
        this.o = d2;
        this.p = i4;
        this.q = list;
        this.r = str10;
        this.s = str11;
        this.t = str12;
        this.u = str13;
    }

    public static ad a(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        JSONObject optJSONObject = jSONObject2.optJSONObject(TtmlNode.TAG_LAYOUT);
        ad adVar = new ad(jSONObject2.optString("video_url"), jSONObject2.optString("ct"), c.a(jSONObject2.optString("end_card_markup")), jSONObject2.optString("activation_command"), jSONObject2.optString("advertiser_name"), jSONObject2.optString("title"), jSONObject2.optString("subtitle"), jSONObject2.optString(TtmlNode.TAG_BODY), jSONObject2.optJSONObject(SettingsJsonConstants.APP_ICON_KEY) != null ? jSONObject2.optJSONObject(SettingsJsonConstants.APP_ICON_KEY).optString("url") : "", jSONObject2.optJSONObject(MessengerShareContentUtility.MEDIA_IMAGE) != null ? jSONObject2.optJSONObject(MessengerShareContentUtility.MEDIA_IMAGE).optString("url") : "", jSONObject2.optInt("skippable_seconds"), jSONObject2.optInt("video_duration_sec"), optJSONObject != null ? j.a(optJSONObject.optJSONObject("portrait")) : new j(), optJSONObject != null ? j.a(optJSONObject.optJSONObject("landscape")) : new j(), jSONObject2.optDouble("rating_value", Utils.DOUBLE_EPSILON), jSONObject2.optInt(PlaceFields.RATING_COUNT, 0), a(jSONObject2.optJSONArray("end_card_images")), jSONObject2.optString("fbad_command"), jSONObject2.optString("call_to_action"), jSONObject2.optString("ad_choices_link_url"), b(jSONObject));
        return adVar;
    }

    private static List<String> a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            String optString = jSONArray.optString(i2);
            if (!TextUtils.isEmpty(optString)) {
                arrayList.add(optString);
            }
        }
        return arrayList;
    }

    private static String b(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("generic_text");
        return optJSONObject == null ? "Sponsored" : optJSONObject.optString("sponsored", "Sponsored");
    }

    public String a() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.v = str;
    }

    public String b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.w = str;
    }

    public byte[] c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public String g() {
        return this.g;
    }

    public String h() {
        return this.h;
    }

    public String i() {
        return this.i;
    }

    public String j() {
        return this.j;
    }

    public int k() {
        return this.k;
    }

    public String l() {
        return this.v;
    }

    /* access modifiers changed from: 0000 */
    public int m() {
        return this.l;
    }

    public j n() {
        return this.m;
    }

    public List<String> o() {
        return Collections.unmodifiableList(this.q);
    }

    public String p() {
        return this.r;
    }

    public String q() {
        return this.s;
    }

    public String r() {
        return this.t;
    }

    public String s() {
        return this.w;
    }

    public String t() {
        return this.u;
    }
}
