package com.facebook.ads.internal.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.f;
import com.facebook.ads.internal.view.b.d;
import com.facebook.ads.internal.view.c;
import com.facebook.ads.internal.view.hscroll.b;
import com.facebook.ads.internal.view.u;
import java.util.List;

public class i extends Adapter<c> {
    /* access modifiers changed from: private */
    public static final int a = Color.argb(51, 0, 0, 0);
    private final List<e> b;
    private final int c;
    private final int d;

    public i(b bVar, List<e> list) {
        float f = bVar.getContext().getResources().getDisplayMetrics().density;
        this.b = list;
        this.c = Math.round(f * 1.0f);
        this.d = bVar.getChildSpacing();
    }

    /* renamed from: a */
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        u uVar = new u(viewGroup.getContext());
        uVar.setScaleType(ScaleType.CENTER_CROP);
        com.facebook.ads.internal.q.a.i.a(uVar, com.facebook.ads.internal.q.a.i.INTERNAL_AD_MEDIA);
        return new c(uVar);
    }

    /* renamed from: a */
    public void onBindViewHolder(final c cVar, int i) {
        MarginLayoutParams marginLayoutParams = new MarginLayoutParams(-2, -1);
        marginLayoutParams.setMargins(i == 0 ? this.d * 2 : this.d, 0, i >= this.b.size() + -1 ? this.d * 2 : this.d, 0);
        cVar.a.setBackgroundColor(0);
        cVar.a.setImageDrawable(null);
        cVar.a.setLayoutParams(marginLayoutParams);
        cVar.a.setPadding(this.c, this.c, this.c, this.c);
        e eVar = (e) this.b.get(i);
        eVar.a((View) cVar.a);
        f j = eVar.j();
        if (j != null) {
            d a2 = new d((ImageView) cVar.a).a();
            a2.a((com.facebook.ads.internal.view.b.e) new com.facebook.ads.internal.view.b.e() {
                public void a() {
                    cVar.a.setBackgroundColor(i.a);
                }
            });
            a2.a(j.a());
        }
    }

    public int getItemCount() {
        return this.b.size();
    }
}
