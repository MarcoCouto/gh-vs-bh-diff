package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.facebook.ads.internal.n.c;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.e.d;
import com.facebook.ads.internal.n.f;
import com.facebook.ads.internal.n.g;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.n.j;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.a;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiNative;
import com.inmobi.ads.InMobiNative.NativeAdListener;
import com.inmobi.sdk.InMobiSdk;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class t extends ab implements x {
    /* access modifiers changed from: private */
    public ac a;
    /* access modifiers changed from: private */
    public InMobiNative b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public g h;
    /* access modifiers changed from: private */
    public f i;
    /* access modifiers changed from: private */
    public f j;

    public String A() {
        return null;
    }

    public List<e> B() {
        return null;
    }

    public int C() {
        return 0;
    }

    public int D() {
        return 0;
    }

    public c E() {
        return c.INMOBI;
    }

    public g F() {
        return g.INMOBI;
    }

    public void a(int i2) {
    }

    public void a(final Context context, ac acVar, com.facebook.ads.internal.m.c cVar, Map<String, Object> map, d dVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(y.a(F()));
        sb.append(" Loading");
        com.facebook.ads.internal.q.a.d.a(context, sb.toString());
        JSONObject jSONObject = (JSONObject) map.get("data");
        String optString = jSONObject.optString("account_id");
        Long valueOf = Long.valueOf(jSONObject.optLong("placement_id"));
        if (TextUtils.isEmpty(optString) || valueOf == null) {
            acVar.a(this, new a(AdErrorType.MEDIATION_ERROR, "Mediation Error"));
            return;
        }
        this.a = acVar;
        InMobiSdk.init(context, optString);
        this.b = new InMobiNative(valueOf.longValue(), new NativeAdListener() {
            public void onAdDismissed(InMobiNative inMobiNative) {
            }

            public void onAdDisplayed(InMobiNative inMobiNative) {
            }

            public void onAdLoadFailed(InMobiNative inMobiNative, InMobiAdRequestStatus inMobiAdRequestStatus) {
                Context context = context;
                StringBuilder sb = new StringBuilder();
                sb.append(y.a(t.this.F()));
                sb.append(" Failed with InMobi error: ");
                sb.append(inMobiAdRequestStatus.getMessage());
                com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                if (t.this.a != null) {
                    t.this.a.a(t.this, new a(AdErrorType.MEDIATION_ERROR.getErrorCode(), inMobiAdRequestStatus.getMessage()));
                }
            }

            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x008a */
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0098 A[Catch:{ Exception -> 0x00da }] */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00ae A[Catch:{ Exception -> 0x00da }] */
            public void onAdLoadSucceeded(InMobiNative inMobiNative) {
                try {
                    JSONObject jSONObject = new JSONObject((String) inMobiNative.getAdContent());
                    t.this.e = jSONObject.optString("title");
                    t.this.f = jSONObject.optString("description");
                    t.this.g = jSONObject.optString("cta");
                    JSONObject optJSONObject = jSONObject.optJSONObject(SettingsJsonConstants.APP_ICON_KEY);
                    if (optJSONObject != null) {
                        int optInt = optJSONObject.optInt(SettingsJsonConstants.ICON_WIDTH_KEY);
                        int optInt2 = optJSONObject.optInt(SettingsJsonConstants.ICON_HEIGHT_KEY);
                        t.this.i = new f(optJSONObject.optString("url"), optInt, optInt2);
                    }
                    JSONObject optJSONObject2 = jSONObject.optJSONObject("screenshots");
                    if (optJSONObject2 != null) {
                        int optInt3 = optJSONObject2.optInt(SettingsJsonConstants.ICON_WIDTH_KEY);
                        int optInt4 = optJSONObject2.optInt(SettingsJsonConstants.ICON_HEIGHT_KEY);
                        t.this.j = new f(optJSONObject2.optString("url"), optInt3, optInt4);
                    }
                    t.this.h = new g(Double.parseDouble(jSONObject.optString("rating")), 5.0d);
                    t.this.c = true;
                    if (t.this.d != null) {
                        t.this.b;
                        InMobiNative.bind(t.this.d, inMobiNative);
                    }
                    if (t.this.a != null) {
                        Context context = context;
                        StringBuilder sb = new StringBuilder();
                        sb.append(y.a(t.this.F()));
                        sb.append(" Loaded");
                        com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                        t.this.a.a(t.this);
                    }
                } catch (Exception unused) {
                    if (t.this.a != null) {
                        Context context2 = context;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(y.a(t.this.F()));
                        sb2.append(" Failed. Internal AN SDK error");
                        com.facebook.ads.internal.q.a.d.a(context2, sb2.toString());
                        t.this.a.a(t.this, a.a(AdErrorType.INTERNAL_ERROR, "Internal Error"));
                    }
                }
            }

            public void onUserLeftApplication(InMobiNative inMobiNative) {
            }
        });
        this.b.load();
    }

    public void a(View view, List<View> list) {
        this.d = view;
        if (c_()) {
            InMobiNative inMobiNative = this.b;
            InMobiNative.bind(this.d, this.b);
        }
    }

    public void a(ac acVar) {
        this.a = acVar;
    }

    public void a(Map<String, String> map) {
        this.a.b(this);
    }

    public void b(Map<String, String> map) {
        if (c_()) {
            this.a.c(this);
            this.b.reportAdClickAndOpenLandingPage(null);
        }
    }

    public void b_() {
        if (c_()) {
            InMobiNative inMobiNative = this.b;
            InMobiNative.unbind(this.d);
        }
        this.d = null;
    }

    public String c() {
        return null;
    }

    public boolean c_() {
        return this.b != null && this.c;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public boolean f() {
        return false;
    }

    public boolean g() {
        return false;
    }

    public boolean h() {
        return true;
    }

    public int i() {
        return 0;
    }

    public int j() {
        return 0;
    }

    public int k() {
        return 0;
    }

    public f l() {
        return this.i;
    }

    public f m() {
        return this.j;
    }

    public h n() {
        return null;
    }

    public String o() {
        return this.e;
    }

    public void onDestroy() {
        b_();
        this.b = null;
        this.a = null;
    }

    public String p() {
        return null;
    }

    public String q() {
        return this.f;
    }

    public String r() {
        return this.g;
    }

    public String s() {
        return null;
    }

    public g t() {
        return null;
    }

    public f u() {
        return null;
    }

    public String v() {
        return null;
    }

    public String w() {
        return "Ad";
    }

    public String x() {
        return null;
    }

    public String y() {
        return null;
    }

    public j z() {
        return j.DEFAULT;
    }
}
