package com.facebook.ads.internal.adapters;

import android.content.Context;
import com.facebook.ads.internal.q.d.a;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class v implements Serializable {
    private static final long serialVersionUID = 8751287062553772011L;
    private final List<d> a;
    private final String b;
    private final String c;
    private final j d;
    private final String e;
    private final String f;
    private final String g;
    private final j h;
    private final String i;
    private final int j;
    private final int k;

    private v(List<d> list, String str, String str2, String str3, String str4, String str5, String str6, j jVar, j jVar2, int i2, int i3) {
        this.a = list;
        this.b = str;
        this.c = str2;
        this.d = jVar2;
        this.e = str3;
        this.f = str4;
        this.g = str6;
        this.h = jVar;
        this.i = str5;
        this.j = i2;
        this.k = i3;
    }

    public static v a(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        String optString = jSONObject.optString("ad_choices_link_url");
        String optString2 = jSONObject.optString("ct");
        String optString3 = jSONObject.optString("title");
        int optInt = jSONObject.optInt("viewability_check_initial_delay", 0);
        int optInt2 = jSONObject.optInt("viewability_check_interval", 1000);
        String str = "";
        JSONObject optJSONObject = jSONObject.optJSONObject(SettingsJsonConstants.APP_ICON_KEY);
        if (optJSONObject != null) {
            str = optJSONObject.optString("url");
        }
        String str2 = str;
        JSONObject optJSONObject2 = jSONObject.optJSONObject(TtmlNode.TAG_LAYOUT);
        JSONObject jSONObject3 = null;
        if (optJSONObject2 != null) {
            jSONObject3 = optJSONObject2.optJSONObject("portrait");
            jSONObject2 = optJSONObject2.optJSONObject("landscape");
        } else {
            jSONObject2 = null;
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("generic_text");
        String optString4 = optJSONObject3 != null ? optJSONObject3.optString("sponsored", "Sponsored") : "Sponsored";
        j a2 = j.a(jSONObject3);
        j a3 = j.a(jSONObject2);
        String optString5 = jSONObject.optString("request_id", "");
        JSONArray optJSONArray = jSONObject.optJSONArray("carousel");
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            arrayList.add(d.a(jSONObject));
        } else {
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                try {
                    arrayList.add(d.a(optJSONArray.getJSONObject(i2)));
                } catch (JSONException e2) {
                    a.a(e2, context);
                    e2.printStackTrace();
                }
            }
        }
        v vVar = new v(arrayList, optString, optString2, optString3, str2, optString5, optString4, a2, a3, optInt, optInt2);
        return vVar;
    }

    public String a() {
        return this.c;
    }

    public String b() {
        return this.e;
    }

    public String c() {
        return this.f;
    }

    public List<d> d() {
        return Collections.unmodifiableList(this.a);
    }

    public String e() {
        return this.b;
    }

    public String f() {
        return this.i;
    }

    public String g() {
        return this.g;
    }

    public j h() {
        return this.h;
    }

    public j i() {
        return this.d;
    }

    public int j() {
        return this.j;
    }

    public int k() {
        return this.k;
    }
}
