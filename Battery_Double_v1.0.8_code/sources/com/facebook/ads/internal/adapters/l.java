package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.o;
import com.facebook.ads.internal.view.e.b;
import com.facebook.ads.internal.view.e.b.a;
import com.facebook.ads.internal.view.e.b.d;
import com.facebook.ads.internal.view.e.c.e;
import com.facebook.ads.internal.view.e.c.i;
import com.facebook.ads.internal.view.e.c.k;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class l extends u implements o<Bundle> {
    static final /* synthetic */ boolean e = true;
    @Nullable
    protected c a;
    @Nullable
    protected b b;
    @Nullable
    protected JSONObject c;
    @Nullable
    protected Context d;
    private final f<com.facebook.ads.internal.view.e.b.b> f = new f<com.facebook.ads.internal.view.e.b.b>() {
        public Class<com.facebook.ads.internal.view.e.b.b> a() {
            return com.facebook.ads.internal.view.e.b.b.class;
        }

        public void a(com.facebook.ads.internal.view.e.b.b bVar) {
            if (l.this.j != null) {
                l.this.j.d(l.this);
            }
        }
    };
    private final f<com.facebook.ads.internal.view.e.b.l> g = new f<com.facebook.ads.internal.view.e.b.l>() {
        public Class<com.facebook.ads.internal.view.e.b.l> a() {
            return com.facebook.ads.internal.view.e.b.l.class;
        }

        public void a(com.facebook.ads.internal.view.e.b.l lVar) {
            l.this.l = true;
            if (l.this.j != null) {
                l.this.j.a(l.this);
            }
        }
    };
    private final f<d> h = new f<d>() {
        public Class<d> a() {
            return d.class;
        }

        public void a(d dVar) {
            if (l.this.j != null) {
                l.this.j.a((u) l.this, AdError.INTERNAL_ERROR);
            }
        }
    };
    private final f<a> i = new f<a>() {
        public Class<a> a() {
            return a.class;
        }

        public void a(a aVar) {
            if (l.this.j != null) {
                l.this.j.b(l.this);
            }
        }
    };
    /* access modifiers changed from: private */
    @Nullable
    public com.facebook.ads.a.a j;
    @Nullable
    private String k;
    /* access modifiers changed from: private */
    public boolean l = false;
    @Nullable
    private com.facebook.ads.internal.view.e.d m;
    @Nullable
    private String n;
    private boolean o = false;
    private com.facebook.ads.internal.d.b p;

    private void a(Context context, com.facebook.ads.a.a aVar, JSONObject jSONObject, c cVar, @Nullable Bundle bundle, EnumSet<CacheFlag> enumSet) {
        Context context2 = context;
        JSONObject jSONObject2 = jSONObject;
        Bundle bundle2 = bundle;
        this.d = context2;
        this.j = aVar;
        c cVar2 = cVar;
        this.a = cVar2;
        this.c = jSONObject2;
        this.l = false;
        JSONObject jSONObject3 = jSONObject2.getJSONObject("video");
        this.n = jSONObject2.optString("ct");
        this.b = new b(context2);
        a();
        this.b.getEventBus().a((T[]) new f[]{this.f, this.g, this.h, this.i});
        ArrayList arrayList = new ArrayList();
        AnonymousClass5 r0 = new com.facebook.ads.internal.b.b(this, 1.0E-7d, -1.0d, 0.001d, false) {
            final /* synthetic */ l a;

            {
                this.a = r10;
            }

            /* access modifiers changed from: protected */
            public void a(boolean z, boolean z2, com.facebook.ads.internal.b.c cVar) {
                this.a.f();
            }
        };
        arrayList.add(r0);
        if (bundle2 != null) {
            com.facebook.ads.internal.view.e.c cVar3 = new com.facebook.ads.internal.view.e.c(context2, cVar2, this.b, arrayList, this.n, bundle2.getBundle("logger"));
            this.m = cVar3;
        } else {
            com.facebook.ads.internal.view.e.c cVar4 = new com.facebook.ads.internal.view.e.c(context2, cVar2, this.b, (List<com.facebook.ads.internal.b.b>) arrayList, this.n);
            this.m = cVar4;
        }
        this.j.a((u) this, (View) this.b);
        this.k = jSONObject3.getString((com.facebook.ads.internal.q.c.d.c(context) != com.facebook.ads.internal.q.c.d.a.MOBILE_INTERNET || !jSONObject3.has("videoHDURL") || jSONObject3.isNull("videoHDURL")) ? AudienceNetworkActivity.VIDEO_URL : "videoHDURL");
        if (enumSet.contains(CacheFlag.VIDEO)) {
            this.p = new com.facebook.ads.internal.d.b(context2);
            this.p.a(this.k);
            this.p.a((com.facebook.ads.internal.d.a) new com.facebook.ads.internal.d.a() {
                public void a() {
                    l.this.b.setVideoURI(l.this.h());
                }

                public void b() {
                    l.this.b.setVideoURI(l.this.h());
                }
            });
            return;
        }
        this.b.setVideoURI(h());
    }

    /* access modifiers changed from: private */
    public String h() {
        String str = "";
        if (!(this.p == null || this.k == null)) {
            str = this.p.b(this.k);
        }
        return TextUtils.isEmpty(str) ? this.k : str;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!e && this.d == null) {
            throw new AssertionError();
        } else if (e || this.c != null) {
            JSONObject optJSONObject = this.c.optJSONObject(MimeTypes.BASE_TYPE_TEXT);
            if (optJSONObject == null) {
                optJSONObject = new JSONObject();
            }
            this.b.a((com.facebook.ads.internal.view.e.a.b) new k(this.d));
            com.facebook.ads.internal.view.e.c.l lVar = new com.facebook.ads.internal.view.e.c.l(this.d);
            this.b.a((com.facebook.ads.internal.view.e.a.b) lVar);
            this.b.a((com.facebook.ads.internal.view.e.a.b) new com.facebook.ads.internal.view.e.c.d(lVar, com.facebook.ads.internal.view.e.c.d.a.INVSIBLE));
            this.b.a((com.facebook.ads.internal.view.e.a.b) new com.facebook.ads.internal.view.e.c.b(this.d));
            String b2 = b();
            if (b2 != null) {
                com.facebook.ads.internal.view.e.c.c cVar = new com.facebook.ads.internal.view.e.c.c(this.d, b2);
                LayoutParams layoutParams = new LayoutParams(-2, -2);
                layoutParams.addRule(12);
                layoutParams.addRule(9);
                cVar.setLayoutParams(layoutParams);
                cVar.setCountdownTextColor(-1);
                this.b.a((com.facebook.ads.internal.view.e.a.b) cVar);
            }
            if (this.c.has("cta") && !this.c.isNull("cta")) {
                JSONObject jSONObject = this.c.getJSONObject("cta");
                e eVar = new e(this.d, jSONObject.getString("url"), this.a, this.n, jSONObject.getString(MimeTypes.BASE_TYPE_TEXT));
                LayoutParams layoutParams2 = new LayoutParams(-2, -2);
                layoutParams2.addRule(10);
                layoutParams2.addRule(11);
                eVar.setLayoutParams(layoutParams2);
                this.b.a((com.facebook.ads.internal.view.e.a.b) eVar);
            }
            String d2 = d();
            if (!TextUtils.isEmpty(d2)) {
                this.b.a((com.facebook.ads.internal.view.e.a.b) new com.facebook.ads.internal.view.e.c.a(this.d, d2, this.n, new float[]{0.0f, 0.0f, 8.0f, 0.0f}));
            }
            int c2 = c();
            if (c2 > 0) {
                i iVar = new i(this.d, c2, optJSONObject.optString("skipAdIn", "Skip Ad in"), optJSONObject.optString("skipAd", "Skip Ad"));
                LayoutParams layoutParams3 = new LayoutParams(-2, -2);
                layoutParams3.addRule(12);
                layoutParams3.addRule(11);
                iVar.setLayoutParams(layoutParams3);
                iVar.setPadding(0, 0, 0, 30);
                this.b.a((com.facebook.ads.internal.view.e.a.b) iVar);
            }
        } else {
            throw new AssertionError();
        }
    }

    public final void a(Context context, com.facebook.ads.a.a aVar, c cVar, Bundle bundle, EnumSet<CacheFlag> enumSet) {
        try {
            a(context, aVar, new JSONObject(bundle.getString("ad_response")), cVar, bundle, enumSet);
        } catch (JSONException unused) {
            aVar.a((u) this, AdError.INTERNAL_ERROR);
        }
    }

    public final void a(Context context, com.facebook.ads.a.a aVar, Map<String, Object> map, c cVar, EnumSet<CacheFlag> enumSet) {
        try {
            a(context, aVar, (JSONObject) map.get("data"), cVar, null, enumSet);
        } catch (JSONException unused) {
            aVar.a((u) this, AdError.INTERNAL_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        if (e || this.c != null) {
            try {
                JSONObject jSONObject = this.c.getJSONObject("capabilities");
                if (!jSONObject.has("countdown") || jSONObject.isNull("countdown")) {
                    return null;
                }
                JSONObject jSONObject2 = jSONObject.getJSONObject("countdown");
                if (jSONObject2.has("format")) {
                    return jSONObject2.optString("format");
                }
                return null;
            } catch (Exception e2) {
                Log.w(String.valueOf(l.class), "Invalid JSON", e2);
                return null;
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    public int c() {
        if (e || this.c != null) {
            try {
                JSONObject jSONObject = this.c.getJSONObject("capabilities");
                if (!jSONObject.has("skipButton") || jSONObject.isNull("skipButton")) {
                    return -1;
                }
                JSONObject jSONObject2 = jSONObject.getJSONObject("skipButton");
                if (jSONObject2.has("skippableSeconds")) {
                    return jSONObject2.getInt("skippableSeconds");
                }
                return -1;
            } catch (Exception e2) {
                Log.w(String.valueOf(l.class), "Invalid JSON", e2);
                return -1;
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    @Nullable
    public String d() {
        if (e || this.c != null) {
            try {
                JSONObject jSONObject = this.c.getJSONObject("capabilities");
                if (!jSONObject.has("adChoices") || jSONObject.isNull("adChoices")) {
                    return null;
                }
                JSONObject jSONObject2 = jSONObject.getJSONObject("adChoices");
                if (jSONObject2.has("url")) {
                    return jSONObject2.getString("url");
                }
                return null;
            } catch (Exception e2) {
                Log.w(String.valueOf(l.class), "Invalid JSON", e2);
                return null;
            }
        } else {
            throw new AssertionError();
        }
    }

    public boolean e() {
        if (!this.l || this.b == null) {
            return false;
        }
        if (this.m.l() > 0) {
            this.b.a(this.m.l());
        }
        this.b.a(com.facebook.ads.internal.view.e.a.a.AUTO_STARTED);
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (this.a != null && !this.o) {
            this.o = true;
            this.a.a(this.n, new HashMap());
            if (this.j != null) {
                this.j.c(this);
            }
        }
    }

    public Bundle g() {
        if (this.m == null || this.c == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putBundle("logger", this.m.g());
        bundle.putString("ad_response", this.c.toString());
        return bundle;
    }

    public void onDestroy() {
        if (this.b != null) {
            this.b.f();
            this.b.k();
        }
        this.j = null;
        this.a = null;
        this.k = null;
        this.l = false;
        this.n = null;
        this.b = null;
        this.m = null;
        this.c = null;
        this.d = null;
        this.o = false;
    }
}
