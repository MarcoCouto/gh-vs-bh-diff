package com.facebook.ads.internal.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.Settings.System;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.d.a;
import com.facebook.ads.internal.d.b;
import com.facebook.ads.internal.settings.a.C0007a;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

public class p extends ae {
    private final String c = UUID.randomUUID().toString();
    private Context d;
    /* access modifiers changed from: private */
    public af e;
    /* access modifiers changed from: private */
    public boolean f = false;
    private String g;
    private String h;
    private long i;
    /* access modifiers changed from: private */
    public ad j;
    private ag k;

    private void c() {
        LocalBroadcastManager.getInstance(this.d).registerReceiver(this.k, this.k.a());
    }

    private void d() {
        if (this.k != null) {
            try {
                LocalBroadcastManager.getInstance(this.d).unregisterReceiver(this.k);
            } catch (Exception unused) {
            }
        }
    }

    private String e() {
        String str;
        if (this.a == null) {
            return null;
        }
        String urlPrefix = AdSettings.getUrlPrefix();
        if (urlPrefix == null || urlPrefix.isEmpty()) {
            str = "https://www.facebook.com/audience_network/server_side_reward";
        } else {
            str = String.format("https://www.%s.facebook.com/audience_network/server_side_reward", new Object[]{urlPrefix});
        }
        Uri parse = Uri.parse(str);
        Builder builder = new Builder();
        builder.scheme(parse.getScheme());
        builder.authority(parse.getAuthority());
        builder.path(parse.getPath());
        builder.query(parse.getQuery());
        builder.fragment(parse.getFragment());
        builder.appendQueryParameter("puid", this.a.getUserID());
        builder.appendQueryParameter("pc", this.a.getCurrency());
        builder.appendQueryParameter("ptid", this.c);
        builder.appendQueryParameter("appid", this.g);
        return builder.build().toString();
    }

    public int a() {
        if (this.j == null) {
            return -1;
        }
        return this.j.m();
    }

    public void a(Context context, af afVar, Map<String, Object> map, final boolean z) {
        this.d = context;
        this.e = afVar;
        this.f = false;
        this.h = (String) map.get(AudienceNetworkActivity.PLACEMENT_ID);
        this.i = ((Long) map.get(AudienceNetworkActivity.REQUEST_TIME)).longValue();
        this.g = this.h != null ? this.h.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)[0] : "";
        this.j = ad.a((JSONObject) map.get("data"));
        if (TextUtils.isEmpty(this.j.a())) {
            this.e.a(this, AdError.INTERNAL_ERROR);
            return;
        }
        this.k = new ag(this.c, this, afVar);
        c();
        final b bVar = new b(context);
        bVar.a(this.j.a());
        bVar.a(this.j.i(), -1, -1);
        bVar.a(this.j.j(), -1, -1);
        bVar.a(this.j.i(), -1, -1);
        for (String a : this.j.o()) {
            bVar.a(a, -1, -1);
        }
        bVar.a((a) new a() {
            private void c() {
                p.this.f = true;
                p.this.e.a(p.this);
                p.this.j.b(bVar.b(p.this.j.a()));
            }

            public void a() {
                c();
            }

            public void b() {
                if (z) {
                    p.this.e.a(p.this, AdError.CACHE_ERROR);
                } else {
                    c();
                }
            }
        });
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x006e  */
    public boolean b() {
        String str;
        int i2;
        if (!this.f) {
            return false;
        }
        String e2 = e();
        this.j.a(e2);
        Intent intent = new Intent(this.d, AudienceNetworkActivity.class);
        intent.putExtra(AudienceNetworkActivity.VIEW_TYPE, C0007a.REWARDED_VIDEO);
        intent.putExtra("rewardedVideoAdDataBundle", this.j);
        intent.putExtra(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.c);
        intent.putExtra(AudienceNetworkActivity.REWARD_SERVER_URL, e2);
        intent.putExtra(AudienceNetworkActivity.PLACEMENT_ID, this.h);
        intent.putExtra(AudienceNetworkActivity.REQUEST_TIME, this.i);
        if (this.b == -1 || System.getInt(this.d.getContentResolver(), "accelerometer_rotation", 0) == 1) {
            if (!com.facebook.ads.internal.l.a.k(this.d)) {
                str = AudienceNetworkActivity.PREDEFINED_ORIENTATION_KEY;
                i2 = 6;
            }
            if (!(this.d instanceof Activity)) {
                intent.setFlags(intent.getFlags() | ErrorDialogData.BINDER_CRASH);
            }
            this.d.startActivity(intent);
            return true;
        }
        str = AudienceNetworkActivity.PREDEFINED_ORIENTATION_KEY;
        i2 = this.b;
        intent.putExtra(str, i2);
        if (!(this.d instanceof Activity)) {
        }
        this.d.startActivity(intent);
        return true;
    }

    public void onDestroy() {
        d();
    }
}
