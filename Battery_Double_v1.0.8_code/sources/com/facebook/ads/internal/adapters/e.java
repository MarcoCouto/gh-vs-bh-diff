package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.facebook.ads.internal.n.c;
import com.facebook.ads.internal.n.e.d;
import com.facebook.ads.internal.n.f;
import com.facebook.ads.internal.n.g;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.n.j;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.a;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader.Builder;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdView;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.google.android.gms.ads.formats.NativeContentAdView;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e extends ab implements x {
    private static final String a = "e";
    private View b;
    /* access modifiers changed from: private */
    public NativeAd c;
    /* access modifiers changed from: private */
    public ac d;
    private NativeAdView e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public Uri h;
    /* access modifiers changed from: private */
    public Uri i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m;

    private void a(View view) {
        if (view != null) {
            ViewGroup viewGroup = (ViewGroup) view.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(view);
            }
        }
    }

    public String A() {
        return null;
    }

    public List<com.facebook.ads.internal.n.e> B() {
        return null;
    }

    public int C() {
        return 0;
    }

    public int D() {
        return 0;
    }

    public c E() {
        return c.ADMOB;
    }

    public g F() {
        return g.ADMOB;
    }

    public void a(int i2) {
    }

    public void a(final Context context, ac acVar, com.facebook.ads.internal.m.c cVar, Map<String, Object> map, d dVar) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append(y.a(F()));
        sb.append(" Loading");
        com.facebook.ads.internal.q.a.d.a(context, sb.toString());
        JSONObject jSONObject = (JSONObject) map.get("data");
        String optString = jSONObject.optString("ad_unit_id");
        JSONArray optJSONArray = jSONObject.optJSONArray("creative_types");
        boolean z2 = false;
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            int i2 = 0;
            boolean z3 = false;
            z = false;
            while (i2 < length) {
                try {
                    String string = optJSONArray.getString(i2);
                    if (string != null) {
                        char c2 = 65535;
                        int hashCode = string.hashCode();
                        if (hashCode != 704091517) {
                            if (hashCode == 883765328) {
                                if (string.equals("page_post")) {
                                    c2 = 1;
                                }
                            }
                        } else if (string.equals("app_install")) {
                            c2 = 0;
                        }
                        switch (c2) {
                            case 0:
                                z3 = true;
                                break;
                            case 1:
                                z = true;
                                break;
                        }
                    }
                    i2++;
                } catch (JSONException unused) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(y.a(F()));
                    sb2.append(" AN server error");
                    com.facebook.ads.internal.q.a.d.a(context, sb2.toString());
                    acVar.a(this, a.a(AdErrorType.SERVER_ERROR, "Server Error"));
                    return;
                }
            }
            z2 = z3;
        } else {
            z = false;
        }
        if (TextUtils.isEmpty(optString) || (!z2 && !z)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(y.a(F()));
            sb3.append(" AN server error");
            com.facebook.ads.internal.q.a.d.a(context, sb3.toString());
            acVar.a(this, a.a(AdErrorType.SERVER_ERROR, "Server Error"));
            return;
        }
        this.d = acVar;
        Builder builder = new Builder(context, optString);
        if (z2) {
            builder.forAppInstallAd(new OnAppInstallAdLoadedListener() {
                public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                    e.this.c = nativeAppInstallAd;
                    e.this.g = true;
                    Uri uri = null;
                    e.this.j = nativeAppInstallAd.getHeadline() != null ? nativeAppInstallAd.getHeadline().toString() : null;
                    e.this.k = nativeAppInstallAd.getBody() != null ? nativeAppInstallAd.getBody().toString() : null;
                    e.this.m = nativeAppInstallAd.getStore() != null ? nativeAppInstallAd.getStore().toString() : null;
                    e.this.l = nativeAppInstallAd.getCallToAction() != null ? nativeAppInstallAd.getCallToAction().toString() : null;
                    List images = nativeAppInstallAd.getImages();
                    e.this.h = (images == null || images.size() <= 0) ? null : ((Image) images.get(0)).getUri();
                    e eVar = e.this;
                    if (nativeAppInstallAd.getIcon() != null) {
                        uri = nativeAppInstallAd.getIcon().getUri();
                    }
                    eVar.i = uri;
                    if (e.this.d != null) {
                        Context context = context;
                        StringBuilder sb = new StringBuilder();
                        sb.append(y.a(e.this.F()));
                        sb.append(" Loaded");
                        com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                        e.this.d.a(e.this);
                    }
                }
            });
        }
        if (z) {
            builder.forContentAd(new OnContentAdLoadedListener() {
                public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                    e.this.c = nativeContentAd;
                    e.this.g = true;
                    Uri uri = null;
                    e.this.j = nativeContentAd.getHeadline() != null ? nativeContentAd.getHeadline().toString() : null;
                    e.this.k = nativeContentAd.getBody() != null ? nativeContentAd.getBody().toString() : null;
                    e.this.m = nativeContentAd.getAdvertiser() != null ? nativeContentAd.getAdvertiser().toString() : null;
                    e.this.l = nativeContentAd.getCallToAction() != null ? nativeContentAd.getCallToAction().toString() : null;
                    List images = nativeContentAd.getImages();
                    e.this.h = (images == null || images.size() <= 0) ? null : ((Image) images.get(0)).getUri();
                    e eVar = e.this;
                    if (nativeContentAd.getLogo() != null) {
                        uri = nativeContentAd.getLogo().getUri();
                    }
                    eVar.i = uri;
                    if (e.this.d != null) {
                        Context context = context;
                        StringBuilder sb = new StringBuilder();
                        sb.append(y.a(e.this.F()));
                        sb.append(" Loaded");
                        com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                        e.this.d.a(e.this);
                    }
                }
            });
        }
        builder.withAdListener(new AdListener() {
            public void onAdFailedToLoad(int i) {
                Context context = context;
                StringBuilder sb = new StringBuilder();
                sb.append(y.a(e.this.F()));
                sb.append(" Failed with error code: ");
                sb.append(i);
                com.facebook.ads.internal.q.a.d.a(context, sb.toString());
                if (e.this.d != null) {
                    ac a2 = e.this.d;
                    e eVar = e.this;
                    int errorCode = AdErrorType.MEDIATION_ERROR.getErrorCode();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("AdMob error code: ");
                    sb2.append(i);
                    a2.a(eVar, new a(errorCode, sb2.toString()));
                }
            }

            public void onAdOpened() {
                if (e.this.d != null) {
                    e.this.d.c(e.this);
                }
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().setReturnUrlsForImageAssets(true).build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void a(View view, List<View> list) {
        this.b = view;
        if (c_() && view != null) {
            ViewGroup viewGroup = null;
            int i2 = -1;
            do {
                ViewGroup viewGroup2 = (ViewGroup) view.getParent();
                if (viewGroup2 != null) {
                    if (viewGroup2 instanceof NativeAdView) {
                        ViewGroup viewGroup3 = (ViewGroup) viewGroup2.getParent();
                        if (viewGroup3 != null) {
                            int indexOfChild = viewGroup3.indexOfChild(viewGroup2);
                            viewGroup2.removeView(view);
                            viewGroup3.removeView(viewGroup2);
                            viewGroup3.addView(view, indexOfChild);
                            continue;
                        }
                    } else {
                        i2 = viewGroup2.indexOfChild(view);
                        viewGroup = viewGroup2;
                        continue;
                    }
                }
                Log.e(a, "View must have valid parent for AdMob registration, skipping registration. Impressions and clicks will not be logged.");
                return;
            } while (viewGroup == null);
            NativeAdView nativeContentAdView = this.c instanceof NativeContentAd ? new NativeContentAdView(view.getContext()) : new NativeAppInstallAdView(view.getContext());
            if (view instanceof ViewGroup) {
                nativeContentAdView.setLayoutParams(view.getLayoutParams());
            }
            a(view);
            nativeContentAdView.addView(view);
            viewGroup.removeView(nativeContentAdView);
            viewGroup.addView(nativeContentAdView, i2);
            this.e = nativeContentAdView;
            this.e.setNativeAd(this.c);
            this.f = new View(view.getContext());
            this.e.addView(this.f);
            this.f.setVisibility(8);
            if (this.e instanceof NativeContentAdView) {
                ((NativeContentAdView) this.e).setCallToActionView(this.f);
            } else if (this.e instanceof NativeAppInstallAdView) {
                ((NativeAppInstallAdView) this.e).setCallToActionView(this.f);
            }
            AnonymousClass4 r6 = new OnClickListener() {
                public void onClick(View view) {
                    e.this.f.performClick();
                }
            };
            for (View onClickListener : list) {
                onClickListener.setOnClickListener(r6);
            }
        }
    }

    public void a(ac acVar) {
        this.d = acVar;
    }

    public void a(Map<String, String> map) {
        if (c_() && this.d != null) {
            this.d.b(this);
        }
    }

    public void b(Map<String, String> map) {
    }

    public void b_() {
        a(this.f);
        this.f = null;
        if (this.b != null) {
            ViewGroup viewGroup = (ViewGroup) this.b.getParent();
            if ((viewGroup instanceof NativeContentAdView) || (viewGroup instanceof NativeAppInstallAdView)) {
                ViewGroup viewGroup2 = (ViewGroup) viewGroup.getParent();
                if (viewGroup2 != null) {
                    int indexOfChild = viewGroup2.indexOfChild(viewGroup);
                    a(this.b);
                    a((View) viewGroup);
                    viewGroup2.addView(this.b, indexOfChild);
                }
            }
            this.b = null;
        }
        this.e = null;
    }

    public String c() {
        return null;
    }

    public boolean c_() {
        return this.g && this.c != null;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public boolean f() {
        return false;
    }

    public boolean g() {
        return false;
    }

    public boolean h() {
        return false;
    }

    public int i() {
        return 0;
    }

    public int j() {
        return 0;
    }

    public int k() {
        return 0;
    }

    public f l() {
        if (!c_() || this.i == null) {
            return null;
        }
        return new f(this.i.toString(), 50, 50);
    }

    public f m() {
        if (!c_() || this.h == null) {
            return null;
        }
        return new f(this.h.toString(), 1200, SettingsJsonConstants.ANALYTICS_FLUSH_INTERVAL_SECS_DEFAULT);
    }

    public h n() {
        return null;
    }

    public String o() {
        return this.j;
    }

    public void onDestroy() {
        b_();
        this.d = null;
        this.c = null;
        this.g = false;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
    }

    public String p() {
        return null;
    }

    public String q() {
        return this.k;
    }

    public String r() {
        return this.l;
    }

    public String s() {
        return this.m;
    }

    public g t() {
        return null;
    }

    public f u() {
        return null;
    }

    public String v() {
        return null;
    }

    public String w() {
        return null;
    }

    public String x() {
        return null;
    }

    public String y() {
        return null;
    }

    public j z() {
        return j.DEFAULT;
    }
}
