package com.facebook.ads.internal.g;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.StatFs;
import com.facebook.appevents.AppEventsConstants;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class a {
    private static SensorManager a;
    private static Sensor b;
    private static Sensor c;
    /* access modifiers changed from: private */
    public static volatile float[] d;
    /* access modifiers changed from: private */
    public static volatile float[] e;
    private static Map<String, String> f = new ConcurrentHashMap();
    private static String[] g = {"x", "y", "z"};
    private static SensorEventListener h;
    private static SensorEventListener i;

    /* renamed from: com.facebook.ads.internal.g.a$a reason: collision with other inner class name */
    private static class C0003a implements SensorEventListener {
        private C0003a() {
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            a.d = sensorEvent.values;
            a.d();
        }
    }

    private static class b implements SensorEventListener {
        private b() {
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            a.e = sensorEvent.values;
            a.e();
        }
    }

    public static Map<String, String> a() {
        HashMap hashMap = new HashMap();
        hashMap.putAll(f);
        a((Map<String, String>) hashMap);
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006d, code lost:
        return;
     */
    public static synchronized void a(Context context) {
        synchronized (a.class) {
            b(context);
            c(context);
            d(context);
            if (a == null) {
                a = (SensorManager) context.getSystemService("sensor");
                if (a == null) {
                    return;
                }
            }
            if (b == null) {
                b = a.getDefaultSensor(1);
            }
            if (c == null) {
                c = a.getDefaultSensor(4);
            }
            if (h == null) {
                h = new C0003a();
                if (b != null) {
                    a.registerListener(h, b, 3);
                }
            }
            if (i == null) {
                i = new b();
                if (c != null) {
                    a.registerListener(i, c, 3);
                }
            }
        }
    }

    private static void a(Map<String, String> map) {
        float[] fArr = d;
        float[] fArr2 = e;
        if (fArr != null) {
            int min = Math.min(g.length, fArr.length);
            for (int i2 = 0; i2 < min; i2++) {
                StringBuilder sb = new StringBuilder();
                sb.append("accelerometer_");
                sb.append(g[i2]);
                map.put(sb.toString(), String.valueOf(fArr[i2]));
            }
        }
        if (fArr2 != null) {
            int min2 = Math.min(g.length, fArr2.length);
            for (int i3 = 0; i3 < min2; i3++) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("rotation_");
                sb2.append(g[i3]);
                map.put(sb2.toString(), String.valueOf(fArr2[i3]));
            }
        }
    }

    private static void b(Context context) {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        f.put("available_memory", String.valueOf(memoryInfo.availMem));
    }

    private static void c(Context context) {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        long availableBlocks = (long) statFs.getAvailableBlocks();
        f.put("free_space", String.valueOf(availableBlocks * ((long) statFs.getBlockSize())));
    }

    /* access modifiers changed from: private */
    public static synchronized void d() {
        synchronized (a.class) {
            if (a != null) {
                a.unregisterListener(h);
            }
            h = null;
        }
    }

    private static void d(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra(Param.LEVEL, -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            int intExtra3 = registerReceiver.getIntExtra("status", -1);
            boolean z = intExtra3 == 2 || intExtra3 == 5;
            float f2 = 0.0f;
            if (intExtra2 > 0) {
                f2 = 100.0f * (((float) intExtra) / ((float) intExtra2));
            }
            f.put("battery", String.valueOf(f2));
            f.put("charging", z ? AppEventsConstants.EVENT_PARAM_VALUE_YES : "0");
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void e() {
        synchronized (a.class) {
            if (a != null) {
                a.unregisterListener(i);
            }
            i = null;
        }
    }
}
