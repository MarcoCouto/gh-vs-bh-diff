package com.facebook.ads.internal.p.b.a;

import android.util.Log;
import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

abstract class e implements a {
    private final ExecutorService a = Executors.newSingleThreadExecutor();

    private class a implements Callable<Void> {
        private final File b;

        public a(File file) {
            this.b = file;
        }

        /* renamed from: a */
        public Void call() {
            e.this.b(this.b);
            return null;
        }
    }

    e() {
    }

    private void a(List<File> list) {
        long b = b(list);
        int size = list.size();
        for (File file : list) {
            if (!a(file, b, size)) {
                long length = file.length();
                if (file.delete()) {
                    size--;
                    long j = b - length;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cache file ");
                    sb.append(file);
                    sb.append(" is deleted because it exceeds cache limit");
                    Log.i("ProxyCache", sb.toString());
                    b = j;
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Error deleting file ");
                    sb2.append(file);
                    sb2.append(" for trimming cache");
                    Log.e("ProxyCache", sb2.toString());
                }
            }
        }
    }

    private long b(List<File> list) {
        long j = 0;
        for (File length : list) {
            j += length.length();
        }
        return j;
    }

    /* access modifiers changed from: private */
    public void b(File file) {
        d.c(file);
        a(d.b(file.getParentFile()));
    }

    public void a(File file) {
        this.a.submit(new a(file));
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(File file, long j, int i);
}
