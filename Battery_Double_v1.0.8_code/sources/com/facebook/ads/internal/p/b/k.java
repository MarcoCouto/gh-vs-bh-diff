package com.facebook.ads.internal.p.b;

import android.util.Log;
import java.lang.Thread.State;
import java.util.concurrent.atomic.AtomicInteger;

class k {
    private final n a;
    private final a b;
    private final Object c = new Object();
    private final Object d = new Object();
    private final AtomicInteger e;
    private volatile Thread f;
    private volatile boolean g;
    private volatile int h = -1;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            k.this.e();
        }
    }

    public k(n nVar, a aVar) {
        this.a = (n) j.a(nVar);
        this.b = (a) j.a(aVar);
        this.e = new AtomicInteger();
    }

    private void b() {
        int i = this.e.get();
        if (i >= 1) {
            this.e.set(0);
            StringBuilder sb = new StringBuilder();
            sb.append("Error reading source ");
            sb.append(i);
            sb.append(" times");
            throw new l(sb.toString());
        }
    }

    private void b(long j, long j2) {
        a(j, j2);
        synchronized (this.c) {
            this.c.notifyAll();
        }
    }

    private synchronized void c() {
        boolean z = (this.f == null || this.f.getState() == State.TERMINATED) ? false : true;
        if (!this.g && !this.b.d() && !z) {
            a aVar = new a();
            StringBuilder sb = new StringBuilder();
            sb.append("Source reader for ");
            sb.append(this.a);
            this.f = new Thread(aVar, sb.toString());
            this.f.start();
        }
    }

    private void d() {
        synchronized (this.c) {
            try {
                this.c.wait(1000);
            } catch (InterruptedException e2) {
                throw new l("Waiting source data is interrupted!", e2);
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0038, code lost:
        r2 = r2 + r4;
     */
    public void e() {
        int i;
        int i2;
        int i3;
        Throwable th;
        int i4;
        int i5 = 0;
        try {
            i = this.b.a();
            try {
                this.a.a(i);
                i2 = this.a.a();
                try {
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int a2 = this.a.a(bArr);
                        if (a2 != -1) {
                            synchronized (this.d) {
                                if (g()) {
                                    h();
                                    b((long) i, (long) i2);
                                    return;
                                }
                                this.b.a(bArr, a2);
                            }
                        } else {
                            f();
                            h();
                            b((long) i, (long) i2);
                            return;
                        }
                        b((long) i, (long) i2);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    h();
                    b((long) i, (long) i2);
                    throw th;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                i2 = -1;
                th = th4;
                h();
                b((long) i, (long) i2);
                throw th;
            }
        } catch (Throwable th5) {
            i2 = -1;
            th = th5;
            i = 0;
            h();
            b((long) i, (long) i2);
            throw th;
        }
    }

    private void f() {
        synchronized (this.d) {
            if (!g() && this.b.a() == this.a.a()) {
                this.b.c();
            }
        }
    }

    private boolean g() {
        return Thread.currentThread().isInterrupted() || this.g;
    }

    private void h() {
        try {
            this.a.b();
        } catch (l e2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error closing source ");
            sb.append(this.a);
            a((Throwable) new l(sb.toString(), e2));
        }
    }

    public int a(byte[] bArr, long j, int i) {
        m.a(bArr, j, i);
        while (!this.b.d() && ((long) this.b.a()) < j + ((long) i) && !this.g) {
            c();
            d();
            b();
        }
        int a2 = this.b.a(bArr, j, i);
        if (this.b.d() && this.h != 100) {
            this.h = 100;
            a(100);
        }
        return a2;
    }

    public void a() {
        synchronized (this.d) {
            String str = "ProxyCache";
            StringBuilder sb = new StringBuilder();
            sb.append("Shutdown proxy for ");
            sb.append(this.a);
            Log.d(str, sb.toString());
            try {
                this.g = true;
                if (this.f != null) {
                    this.f.interrupt();
                }
                this.b.b();
            } catch (l e2) {
                a((Throwable) e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
    }

    /* access modifiers changed from: protected */
    public void a(long j, long j2) {
        boolean z = false;
        int i = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)) == 0 ? 100 : (int) ((j * 100) / j2);
        boolean z2 = i != this.h;
        if (j2 >= 0) {
            z = true;
        }
        if (z && z2) {
            a(i);
        }
        this.h = i;
    }

    /* access modifiers changed from: protected */
    public final void a(Throwable th) {
        if (th instanceof i) {
            Log.d("ProxyCache", "ProxyCache is interrupted");
        } else {
            Log.e("ProxyCache", "ProxyCache error", th);
        }
    }
}
