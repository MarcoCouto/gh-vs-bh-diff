package com.facebook.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.internal.adapters.ad;
import com.facebook.ads.internal.adapters.m;
import com.facebook.ads.internal.adapters.n;
import com.facebook.ads.internal.settings.a.C0007a;
import com.facebook.ads.internal.view.a.C0008a;
import com.facebook.ads.internal.view.e.b.z;
import com.facebook.ads.internal.view.f;
import com.facebook.ads.internal.view.j;
import com.facebook.ads.internal.view.k;
import com.facebook.ads.internal.view.l;
import com.facebook.ads.internal.view.q;
import com.facebook.ads.internal.view.v;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class AudienceNetworkActivity extends Activity {
    @Deprecated
    public static final String AD_ICON_URL = "adIconUrl";
    @Deprecated
    public static final String AD_SUBTITLE = "adSubtitle";
    @Deprecated
    public static final String AD_TITLE = "adTitle";
    public static final String AUDIENCE_NETWORK_UNIQUE_ID_EXTRA = "uniqueId";
    public static final String AUTOPLAY = "autoplay";
    public static final String BROWSER_URL = "browserURL";
    public static final String CLIENT_TOKEN = "clientToken";
    @Deprecated
    public static final String CONTEXT_SWITCH_BEHAVIOR = "contextSwitchBehavior";
    @Deprecated
    public static final String END_CARD_ACTIVATION_COMMAND = "facebookRewardedVideoEndCardActivationCommand";
    @Deprecated
    public static final String END_CARD_MARKUP = "facebookRewardedVideoEndCardMarkup";
    public static final String HANDLER_TIME = "handlerTime";
    public static final String PLACEMENT_ID = "placementId";
    public static final String PREDEFINED_ORIENTATION_KEY = "predefinedOrientationKey";
    public static final String REQUEST_TIME = "requestTime";
    public static final String REWARD_SERVER_URL = "rewardServerURL";
    public static final String SKIP_DELAY_SECONDS_KEY = "skipAfterSeconds";
    public static final String USE_CACHE = "useCache";
    public static final String VIDEO_LOGGER = "videoLogger";
    public static final String VIDEO_MPD = "videoMPD";
    @Deprecated
    public static final String VIDEO_REPORT_URL = "videoReportURL";
    public static final String VIDEO_SEEK_TIME = "videoSeekTime";
    public static final String VIDEO_URL = "videoURL";
    public static final String VIEW_TYPE = "viewType";
    @Deprecated
    public static final String WEBVIEW_ENCODING = "utf-8";
    @Deprecated
    public static final String WEBVIEW_MIME_TYPE = "text/html";
    private final List<BackButtonInterceptor> a = new ArrayList();
    /* access modifiers changed from: private */
    public RelativeLayout b;
    private int c = -1;
    private String d;
    private C0007a e;
    private long f;
    private long g;
    private int h;
    private com.facebook.ads.internal.view.a i;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.view.b.c j;

    public interface BackButtonInterceptor {
        boolean interceptBackButton();
    }

    public enum Type {
        INTERSTITIAL_WEB_VIEW(C0007a.INTERSTITIAL_WEB_VIEW),
        INTERSTITIAL_OLD_NATIVE_VIDEO(C0007a.INTERSTITIAL_OLD_NATIVE_VIDEO),
        INTERSTITIAL_NATIVE_VIDEO(C0007a.INTERSTITIAL_NATIVE_VIDEO),
        INTERSTITIAL_NATIVE_IMAGE(C0007a.INTERSTITIAL_NATIVE_IMAGE),
        INTERSTITIAL_NATIVE_CAROUSEL(C0007a.INTERSTITIAL_NATIVE_CAROUSEL),
        FULL_SCREEN_VIDEO(C0007a.FULL_SCREEN_VIDEO),
        REWARDED_VIDEO(C0007a.REWARDED_VIDEO),
        BROWSER(C0007a.BROWSER);
        
        C0007a a;

        private Type(C0007a aVar) {
            this.a = aVar;
        }
    }

    private static class a implements C0008a {
        final WeakReference<AudienceNetworkActivity> a;

        private a(AudienceNetworkActivity audienceNetworkActivity) {
            this.a = new WeakReference<>(audienceNetworkActivity);
        }

        public void a(View view) {
            if (this.a.get() != null) {
                ((AudienceNetworkActivity) this.a.get()).b.addView(view);
            }
        }

        public void a(String str) {
            if (this.a.get() != null) {
                ((AudienceNetworkActivity) this.a.get()).a(str);
            }
        }

        public void a(String str, com.facebook.ads.internal.j.d dVar) {
            if (this.a.get() != null) {
                ((AudienceNetworkActivity) this.a.get()).a(str, dVar);
            }
        }
    }

    private static class b {
        private final AudienceNetworkActivity a;
        private final Intent b;
        private final com.facebook.ads.internal.m.c c;

        private b(AudienceNetworkActivity audienceNetworkActivity, Intent intent, com.facebook.ads.internal.m.c cVar) {
            this.a = audienceNetworkActivity;
            this.b = intent;
            this.c = cVar;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a a() {
            l lVar = new l(this.a, this.c, i(), h() ? new com.facebook.ads.internal.d.b(this.a) : null);
            a((com.facebook.ads.internal.view.a) lVar);
            return lVar;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a a(RelativeLayout relativeLayout) {
            v vVar = new v(this.a, this.c, new a());
            vVar.a((View) relativeLayout);
            return vVar;
        }

        private void a(com.facebook.ads.internal.view.a aVar) {
            aVar.setListener(new a());
        }

        /* access modifiers changed from: private */
        @Nullable
        public com.facebook.ads.internal.view.a b() {
            com.facebook.ads.internal.view.a a2 = m.a(this.b.getStringExtra(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA));
            if (a2 == null) {
                return null;
            }
            a(a2);
            return a2;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a c() {
            return new com.facebook.ads.internal.view.b(this.a, this.c, new a());
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a d() {
            q qVar = new q(this.a, this.c, new com.facebook.ads.internal.view.e.b(this.a), new d(), (ad) this.b.getSerializableExtra("rewardedVideoAdDataBundle"));
            return qVar;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a e() {
            return new f(this.a, this.c, new a());
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a f() {
            j jVar = new j(this.a, this.c);
            a((com.facebook.ads.internal.view.a) jVar);
            return jVar;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a g() {
            k kVar = new k(this.a, i(), this.c);
            a((com.facebook.ads.internal.view.a) kVar);
            return kVar;
        }

        private boolean h() {
            return this.b.getBooleanExtra(AudienceNetworkActivity.USE_CACHE, false);
        }

        private com.facebook.ads.internal.adapters.v i() {
            return (com.facebook.ads.internal.adapters.v) this.b.getSerializableExtra("ad_data_bundle");
        }
    }

    private class c implements OnLongClickListener {
        private c() {
        }

        public boolean onLongClick(View view) {
            if (!(AudienceNetworkActivity.this.j == null || AudienceNetworkActivity.this.b == null)) {
                AudienceNetworkActivity.this.j.setBounds(0, 0, AudienceNetworkActivity.this.b.getWidth(), AudienceNetworkActivity.this.b.getHeight());
                AudienceNetworkActivity.this.j.a(!AudienceNetworkActivity.this.j.a());
            }
            return true;
        }
    }

    private static class d extends a {
        private d(AudienceNetworkActivity audienceNetworkActivity) {
            super();
        }

        public void a(String str) {
            if (this.a.get() != null) {
                ((AudienceNetworkActivity) this.a.get()).a(str);
                String a = z.REWARDED_VIDEO_END_ACTIVITY.a();
                String a2 = z.REWARDED_VIDEO_ERROR.a();
                if (str.equals(a) || str.equals(a2)) {
                    ((AudienceNetworkActivity) this.a.get()).finish();
                }
            }
        }
    }

    @Nullable
    private com.facebook.ads.internal.view.a a() {
        b bVar = new b(getIntent(), com.facebook.ads.internal.m.d.a((Context) this));
        if (this.e == null) {
            return null;
        }
        switch (this.e) {
            case FULL_SCREEN_VIDEO:
                return bVar.a(this.b);
            case REWARDED_VIDEO:
                return bVar.d();
            case INTERSTITIAL_WEB_VIEW:
                return bVar.e();
            case BROWSER:
                return bVar.c();
            case INTERSTITIAL_OLD_NATIVE_VIDEO:
                return bVar.b();
            case INTERSTITIAL_NATIVE_VIDEO:
                return bVar.a();
            case INTERSTITIAL_NATIVE_IMAGE:
                return bVar.g();
            case INTERSTITIAL_NATIVE_CAROUSEL:
                return bVar.f();
            default:
                return null;
        }
    }

    private void a(Intent intent, Bundle bundle) {
        if (bundle != null) {
            this.c = bundle.getInt(PREDEFINED_ORIENTATION_KEY, -1);
            this.d = bundle.getString(AUDIENCE_NETWORK_UNIQUE_ID_EXTRA);
            this.e = (C0007a) bundle.getSerializable(VIEW_TYPE);
            return;
        }
        this.c = intent.getIntExtra(PREDEFINED_ORIENTATION_KEY, -1);
        this.d = intent.getStringExtra(AUDIENCE_NETWORK_UNIQUE_ID_EXTRA);
        this.e = (C0007a) intent.getSerializableExtra(VIEW_TYPE);
        this.h = intent.getIntExtra(SKIP_DELAY_SECONDS_KEY, 0) * 1000;
    }

    private void a(Intent intent, boolean z) {
        if (com.facebook.ads.internal.l.a.b(this) && this.e != C0007a.BROWSER) {
            this.j = new com.facebook.ads.internal.view.b.c();
            this.j.a(intent.getStringExtra(PLACEMENT_ID));
            this.j.b(getPackageName());
            long longExtra = intent.getLongExtra(REQUEST_TIME, 0);
            if (longExtra != 0) {
                this.j.a(longExtra);
            }
            TextView textView = new TextView(this);
            textView.setText("Debug");
            textView.setTextColor(-1);
            textView.setBackgroundColor(Color.argb(160, 0, 0, 0));
            textView.setPadding(5, 5, 5, 5);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(12, -1);
            layoutParams.addRule(11, -1);
            textView.setLayoutParams(layoutParams);
            c cVar = new c();
            textView.setOnLongClickListener(cVar);
            if (z) {
                this.b.addView(textView);
            } else {
                this.b.setOnLongClickListener(cVar);
            }
            this.b.getOverlay().add(this.j);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(":");
        sb.append(this.d);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(sb.toString()));
    }

    /* access modifiers changed from: private */
    public void a(String str, com.facebook.ads.internal.j.d dVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(":");
        sb.append(this.d);
        Intent intent = new Intent(sb.toString());
        intent.putExtra(NotificationCompat.CATEGORY_EVENT, dVar);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void addBackButtonInterceptor(BackButtonInterceptor backButtonInterceptor) {
        this.a.add(backButtonInterceptor);
    }

    public void finish() {
        if (!isFinishing()) {
            a(this.e == C0007a.REWARDED_VIDEO ? z.REWARDED_VIDEO_CLOSED.a() : "com.facebook.ads.interstitial.dismissed");
            super.finish();
        }
    }

    public void onBackPressed() {
        long currentTimeMillis = System.currentTimeMillis();
        this.g += currentTimeMillis - this.f;
        this.f = currentTimeMillis;
        if (this.g > ((long) this.h)) {
            boolean z = false;
            for (BackButtonInterceptor interceptBackButton : this.a) {
                if (interceptBackButton.interceptBackButton()) {
                    z = true;
                }
            }
            if (!z) {
                super.onBackPressed();
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (this.i instanceof n) {
            ((n) this.i).a(configuration);
        } else if (this.i instanceof q) {
            ((q) this.i).a(configuration);
        }
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.facebook.ads.internal.q.a.d.a();
        boolean z = true;
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.b = new RelativeLayout(this);
        com.facebook.ads.internal.q.a.v.a((View) this.b, (int) ViewCompat.MEASURED_STATE_MASK);
        setContentView(this.b, new LayoutParams(-1, -1));
        Intent intent = getIntent();
        a(intent, bundle);
        this.i = a();
        if (this.i == null) {
            com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(null, "Unable to infer viewType from intent or savedInstanceState"));
            a("com.facebook.ads.interstitial.error");
            finish();
            return;
        }
        this.i.a(intent, bundle, this);
        a("com.facebook.ads.interstitial.displayed");
        this.f = System.currentTimeMillis();
        if (this.e != C0007a.INTERSTITIAL_WEB_VIEW) {
            z = false;
        }
        a(intent, z);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.b != null) {
            this.b.removeAllViews();
        }
        if (this.i != null) {
            m.a(this.i);
            this.i.onDestroy();
            this.i = null;
        }
        if (this.j != null && com.facebook.ads.internal.l.a.b(this)) {
            this.j.b();
        }
        super.onDestroy();
    }

    public void onPause() {
        this.g += System.currentTimeMillis() - this.f;
        if (this.i != null) {
            this.i.i();
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f = System.currentTimeMillis();
        if (this.i != null) {
            this.i.j();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.i != null) {
            this.i.a(bundle);
        }
        bundle.putInt(PREDEFINED_ORIENTATION_KEY, this.c);
        bundle.putString(AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.d);
        bundle.putSerializable(VIEW_TYPE, this.e);
    }

    public void onStart() {
        super.onStart();
        if (this.c != -1) {
            setRequestedOrientation(this.c);
        }
    }

    public void removeBackButtonInterceptor(BackButtonInterceptor backButtonInterceptor) {
        this.a.remove(backButtonInterceptor);
    }
}
