package com.facebook.ads;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.facebook.ads.NativeAd.MediaCacheFlag;
import com.facebook.ads.internal.a;
import com.facebook.ads.internal.a.C0000a;
import com.facebook.ads.internal.adapters.ab;
import com.facebook.ads.internal.d.b;
import com.facebook.ads.internal.protocol.d;
import com.facebook.ads.internal.protocol.f;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class NativeAdsManager {
    private static final String a = "NativeAdsManager";
    private static final d b = d.ADS;
    /* access modifiers changed from: private */
    public final Context c;
    private final String d;
    private final int e;
    /* access modifiers changed from: private */
    public final List<NativeAd> f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public Listener h;
    private a i;
    private boolean j;
    /* access modifiers changed from: private */
    public boolean k;

    public interface Listener {
        void onAdError(AdError adError);

        void onAdsLoaded();
    }

    public NativeAdsManager(Context context, String str, int i2) {
        if (context == null) {
            throw new IllegalArgumentException("context can not be null");
        }
        this.c = context;
        this.d = str;
        this.e = Math.max(i2, 0);
        this.f = new ArrayList(i2);
        this.g = -1;
        this.k = false;
        this.j = false;
        try {
            CookieManager.getInstance();
            if (VERSION.SDK_INT < 21) {
                CookieSyncManager.createInstance(context);
            }
        } catch (Exception e2) {
            Log.w(a, "Failed to initialize CookieManager.", e2);
        }
    }

    public void disableAutoRefresh() {
        this.j = true;
        if (this.i != null) {
            this.i.c();
        }
    }

    public int getUniqueNativeAdCount() {
        return this.f.size();
    }

    public boolean isLoaded() {
        return this.k;
    }

    public void loadAds() {
        loadAds(EnumSet.of(MediaCacheFlag.NONE));
    }

    public void loadAds(final EnumSet<MediaCacheFlag> enumSet) {
        f fVar = f.NATIVE_UNKNOWN;
        int i2 = this.e;
        if (this.i != null) {
            this.i.b();
        }
        a aVar = new a(this.c, this.d, fVar, null, b, i2, enumSet);
        this.i = aVar;
        if (this.j) {
            this.i.c();
        }
        this.i.a((C0000a) new C0000a() {
            public void a(com.facebook.ads.internal.protocol.a aVar) {
                if (NativeAdsManager.this.h != null) {
                    NativeAdsManager.this.h.onAdError(AdError.getAdErrorFromWrapper(aVar));
                }
            }

            public void a(final List<ab> list) {
                b bVar = new b(NativeAdsManager.this.c);
                for (ab abVar : list) {
                    if (enumSet.contains(MediaCacheFlag.ICON) && abVar.l() != null) {
                        bVar.a(abVar.l().a(), abVar.l().c(), abVar.l().b());
                    }
                    if (enumSet.contains(MediaCacheFlag.IMAGE) && abVar.m() != null) {
                        bVar.a(abVar.m().a(), abVar.m().c(), abVar.m().b());
                    }
                    if (enumSet.contains(MediaCacheFlag.VIDEO) && !TextUtils.isEmpty(abVar.x())) {
                        bVar.a(abVar.x());
                    }
                }
                bVar.a((com.facebook.ads.internal.d.a) new com.facebook.ads.internal.d.a() {
                    private void c() {
                        NativeAdsManager.this.k = true;
                        NativeAdsManager.this.f.clear();
                        NativeAdsManager.this.g = 0;
                        for (ab nativeAd : list) {
                            NativeAdsManager.this.f.add(new NativeAd(NativeAdsManager.this.c, nativeAd, null));
                        }
                        if (NativeAdsManager.this.h != null) {
                            NativeAdsManager.this.h.onAdsLoaded();
                        }
                    }

                    public void a() {
                        c();
                    }

                    public void b() {
                        c();
                    }
                });
            }
        });
        this.i.a();
    }

    public NativeAd nextNativeAd() {
        if (this.f.size() == 0) {
            return null;
        }
        int i2 = this.g;
        this.g = i2 + 1;
        NativeAd nativeAd = (NativeAd) this.f.get(i2 % this.f.size());
        return i2 >= this.f.size() ? new NativeAd(nativeAd) : nativeAd;
    }

    public void setListener(Listener listener) {
        this.h = listener;
    }
}
