package com.facebook.ads;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.NativeAd.Image;
import com.facebook.ads.internal.q.a.i;
import com.facebook.ads.internal.q.a.v;
import com.facebook.ads.internal.q.c.g;

public class AdChoicesView extends RelativeLayout {
    /* access modifiers changed from: private */
    public final Context a;
    /* access modifiers changed from: private */
    public final NativeAd b;
    private final float c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public TextView e;
    private String f;

    public AdChoicesView(Context context, NativeAd nativeAd) {
        this(context, nativeAd, false);
    }

    public AdChoicesView(Context context, final NativeAd nativeAd, boolean z) {
        super(context);
        this.d = false;
        this.a = context;
        this.b = nativeAd;
        this.c = v.b;
        if (!this.b.isAdLoaded() || this.b.a().h()) {
            this.f = this.b.getAdChoicesText();
            if (TextUtils.isEmpty(this.f)) {
                this.f = "AdChoices";
            }
            Image adChoicesIcon = this.b.getAdChoicesIcon();
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() != 0) {
                        return false;
                    }
                    if (!AdChoicesView.this.d) {
                        AdChoicesView.this.a();
                    } else if (!TextUtils.isEmpty(AdChoicesView.this.b.getAdChoicesLinkUrl())) {
                        g.a(new g(), AdChoicesView.this.a, Uri.parse(AdChoicesView.this.b.getAdChoicesLinkUrl()), nativeAd.g());
                    }
                    return true;
                }
            });
            this.e = new TextView(this.a);
            addView(this.e);
            LayoutParams layoutParams2 = new LayoutParams(-2, -2);
            if (!z || adChoicesIcon == null) {
                this.d = true;
            } else {
                layoutParams2.addRule(11, a(adChoicesIcon).getId());
                layoutParams2.width = 0;
                layoutParams.width = Math.round(((float) (adChoicesIcon.getWidth() + 4)) * this.c);
                layoutParams.height = Math.round(((float) (adChoicesIcon.getHeight() + 2)) * this.c);
                this.d = false;
            }
            setLayoutParams(layoutParams);
            layoutParams2.addRule(15, -1);
            this.e.setLayoutParams(layoutParams2);
            this.e.setSingleLine();
            this.e.setText(this.f);
            this.e.setTextSize(10.0f);
            this.e.setTextColor(-4341303);
            i.a(this, i.INTERNAL_AD_CHOICES_ICON);
            i.a(this.e, i.INTERNAL_AD_CHOICES_ICON);
            return;
        }
        setVisibility(8);
    }

    private ImageView a(Image image) {
        ImageView imageView = new ImageView(this.a);
        addView(imageView);
        LayoutParams layoutParams = new LayoutParams(Math.round(((float) image.getWidth()) * this.c), Math.round(((float) image.getHeight()) * this.c));
        layoutParams.addRule(9);
        layoutParams.addRule(15, -1);
        layoutParams.setMargins(Math.round(4.0f * this.c), Math.round(this.c * 2.0f), Math.round(this.c * 2.0f), Math.round(2.0f * this.c));
        imageView.setLayoutParams(layoutParams);
        NativeAd.downloadAndDisplayImage(image, imageView);
        return imageView;
    }

    /* access modifiers changed from: private */
    public void a() {
        Paint paint = new Paint();
        paint.setTextSize(this.e.getTextSize());
        int round = Math.round(paint.measureText(this.f) + (4.0f * this.c));
        final int width = getWidth();
        final int i = round + width;
        this.d = true;
        AnonymousClass2 r3 = new Animation() {
            /* access modifiers changed from: protected */
            public void applyTransformation(float f, Transformation transformation) {
                int i = (int) (((float) width) + (((float) (i - width)) * f));
                AdChoicesView.this.getLayoutParams().width = i;
                AdChoicesView.this.requestLayout();
                AdChoicesView.this.e.getLayoutParams().width = i - width;
                AdChoicesView.this.e.requestLayout();
            }

            public boolean willChangeBounds() {
                return true;
            }
        };
        r3.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (AdChoicesView.this.d) {
                            AdChoicesView.this.d = false;
                            AnonymousClass1 r0 = new Animation() {
                                /* access modifiers changed from: protected */
                                public void applyTransformation(float f, Transformation transformation) {
                                    int i = (int) (((float) i) + (((float) (width - i)) * f));
                                    AdChoicesView.this.getLayoutParams().width = i;
                                    AdChoicesView.this.requestLayout();
                                    AdChoicesView.this.e.getLayoutParams().width = i - width;
                                    AdChoicesView.this.e.requestLayout();
                                }

                                public boolean willChangeBounds() {
                                    return true;
                                }
                            };
                            r0.setDuration(300);
                            r0.setFillAfter(true);
                            AdChoicesView.this.startAnimation(r0);
                        }
                    }
                }, 3000);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        r3.setDuration(300);
        r3.setFillAfter(true);
        startAnimation(r3);
    }
}
