package com.facebook.ads;

import android.content.Context;
import android.widget.RelativeLayout;
import com.facebook.ads.NativeAdView.Type;
import com.facebook.ads.internal.n.a;
import com.facebook.ads.internal.n.h;

public class ANGenericTemplateView extends RelativeLayout {
    private final a a;

    public ANGenericTemplateView(Context context, NativeAd nativeAd, Type type, h hVar) {
        super(context);
        MediaView mediaView = new MediaView(getContext());
        mediaView.setNativeAd(nativeAd);
        a aVar = new a(context, nativeAd.getInternalNativeAd(), this, new AdChoicesView(getContext(), nativeAd, true), mediaView, type.a(), hVar);
        this.a = aVar;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.a.a();
    }
}
