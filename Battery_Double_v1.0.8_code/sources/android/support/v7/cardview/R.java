package android.support.v7.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130968655;
        public static final int cardCornerRadius = 2130968656;
        public static final int cardElevation = 2130968657;
        public static final int cardMaxElevation = 2130968658;
        public static final int cardPreventCornerOverlap = 2130968659;
        public static final int cardUseCompatPadding = 2130968660;
        public static final int contentPadding = 2130968702;
        public static final int contentPaddingBottom = 2130968703;
        public static final int contentPaddingLeft = 2130968704;
        public static final int contentPaddingRight = 2130968705;
        public static final int contentPaddingTop = 2130968706;
    }

    public static final class color {
        public static final int cardview_dark_background = 2131099695;
        public static final int cardview_light_background = 2131099696;
        public static final int cardview_shadow_end_color = 2131099697;
        public static final int cardview_shadow_start_color = 2131099698;
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131165263;
        public static final int cardview_default_elevation = 2131165264;
        public static final int cardview_default_radius = 2131165265;
    }

    public static final class style {
        public static final int Base_CardView = 2131689487;
        public static final int CardView = 2131689647;
        public static final int CardView_Dark = 2131689648;
        public static final int CardView_Light = 2131689649;
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.mansoon.BatteryDouble.R.attr.cardBackgroundColor, com.mansoon.BatteryDouble.R.attr.cardCornerRadius, com.mansoon.BatteryDouble.R.attr.cardElevation, com.mansoon.BatteryDouble.R.attr.cardMaxElevation, com.mansoon.BatteryDouble.R.attr.cardPreventCornerOverlap, com.mansoon.BatteryDouble.R.attr.cardUseCompatPadding, com.mansoon.BatteryDouble.R.attr.contentPadding, com.mansoon.BatteryDouble.R.attr.contentPaddingBottom, com.mansoon.BatteryDouble.R.attr.contentPaddingLeft, com.mansoon.BatteryDouble.R.attr.contentPaddingRight, com.mansoon.BatteryDouble.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
    }
}
