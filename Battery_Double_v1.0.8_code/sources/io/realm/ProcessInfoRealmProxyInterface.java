package io.realm;

import com.mansoon.BatteryDouble.models.data.AppPermission;
import com.mansoon.BatteryDouble.models.data.AppSignature;

public interface ProcessInfoRealmProxyInterface {
    RealmList<AppPermission> realmGet$appPermissions();

    RealmList<AppSignature> realmGet$appSignatures();

    String realmGet$applicationLabel();

    String realmGet$importance();

    String realmGet$installationPkg();

    boolean realmGet$isSystemApp();

    String realmGet$name();

    int realmGet$processId();

    int realmGet$versionCode();

    String realmGet$versionName();

    void realmSet$appPermissions(RealmList<AppPermission> realmList);

    void realmSet$appSignatures(RealmList<AppSignature> realmList);

    void realmSet$applicationLabel(String str);

    void realmSet$importance(String str);

    void realmSet$installationPkg(String str);

    void realmSet$isSystemApp(boolean z);

    void realmSet$name(String str);

    void realmSet$processId(int i);

    void realmSet$versionCode(int i);

    void realmSet$versionName(String str);
}
