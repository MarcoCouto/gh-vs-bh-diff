package io.realm;

public interface CellInfoRealmProxyInterface {
    int realmGet$cid();

    int realmGet$lac();

    int realmGet$mcc();

    int realmGet$mnc();

    String realmGet$radioType();

    void realmSet$cid(int i);

    void realmSet$lac(int i);

    void realmSet$mcc(int i);

    void realmSet$mnc(int i);

    void realmSet$radioType(String str);
}
