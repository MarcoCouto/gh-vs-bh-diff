package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.NetworkStatistics;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class NetworkStatisticsRealmProxy extends NetworkStatistics implements RealmObjectProxy, NetworkStatisticsRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private NetworkStatisticsColumnInfo columnInfo;
    private ProxyState<NetworkStatistics> proxyState;

    static final class NetworkStatisticsColumnInfo extends ColumnInfo {
        long mobileReceivedIndex;
        long mobileSentIndex;
        long wifiReceivedIndex;
        long wifiSentIndex;

        NetworkStatisticsColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("NetworkStatistics");
            this.wifiReceivedIndex = addColumnDetails("wifiReceived", objectSchemaInfo);
            this.wifiSentIndex = addColumnDetails("wifiSent", objectSchemaInfo);
            this.mobileReceivedIndex = addColumnDetails("mobileReceived", objectSchemaInfo);
            this.mobileSentIndex = addColumnDetails("mobileSent", objectSchemaInfo);
        }

        NetworkStatisticsColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new NetworkStatisticsColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            NetworkStatisticsColumnInfo networkStatisticsColumnInfo = (NetworkStatisticsColumnInfo) columnInfo;
            NetworkStatisticsColumnInfo networkStatisticsColumnInfo2 = (NetworkStatisticsColumnInfo) columnInfo2;
            networkStatisticsColumnInfo2.wifiReceivedIndex = networkStatisticsColumnInfo.wifiReceivedIndex;
            networkStatisticsColumnInfo2.wifiSentIndex = networkStatisticsColumnInfo.wifiSentIndex;
            networkStatisticsColumnInfo2.mobileReceivedIndex = networkStatisticsColumnInfo.mobileReceivedIndex;
            networkStatisticsColumnInfo2.mobileSentIndex = networkStatisticsColumnInfo.mobileSentIndex;
        }
    }

    public static String getTableName() {
        return "class_NetworkStatistics";
    }

    static {
        ArrayList arrayList = new ArrayList(4);
        arrayList.add("wifiReceived");
        arrayList.add("wifiSent");
        arrayList.add("mobileReceived");
        arrayList.add("mobileSent");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    NetworkStatisticsRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (NetworkStatisticsColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public double realmGet$wifiReceived() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.wifiReceivedIndex);
    }

    public void realmSet$wifiReceived(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.wifiReceivedIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.wifiReceivedIndex, row$realm.getIndex(), d, true);
        }
    }

    public double realmGet$wifiSent() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.wifiSentIndex);
    }

    public void realmSet$wifiSent(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.wifiSentIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.wifiSentIndex, row$realm.getIndex(), d, true);
        }
    }

    public double realmGet$mobileReceived() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.mobileReceivedIndex);
    }

    public void realmSet$mobileReceived(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.mobileReceivedIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.mobileReceivedIndex, row$realm.getIndex(), d, true);
        }
    }

    public double realmGet$mobileSent() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.mobileSentIndex);
    }

    public void realmSet$mobileSent(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.mobileSentIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.mobileSentIndex, row$realm.getIndex(), d, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("NetworkStatistics", 4, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("wifiReceived", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("wifiSent", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("mobileReceived", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("mobileSent", RealmFieldType.DOUBLE, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static NetworkStatisticsColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new NetworkStatisticsColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static NetworkStatistics createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        NetworkStatistics networkStatistics = (NetworkStatistics) realm.createObjectInternal(NetworkStatistics.class, true, Collections.emptyList());
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics;
        if (jSONObject.has("wifiReceived")) {
            if (jSONObject.isNull("wifiReceived")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'wifiReceived' to null.");
            }
            networkStatisticsRealmProxyInterface.realmSet$wifiReceived(jSONObject.getDouble("wifiReceived"));
        }
        if (jSONObject.has("wifiSent")) {
            if (jSONObject.isNull("wifiSent")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'wifiSent' to null.");
            }
            networkStatisticsRealmProxyInterface.realmSet$wifiSent(jSONObject.getDouble("wifiSent"));
        }
        if (jSONObject.has("mobileReceived")) {
            if (jSONObject.isNull("mobileReceived")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mobileReceived' to null.");
            }
            networkStatisticsRealmProxyInterface.realmSet$mobileReceived(jSONObject.getDouble("mobileReceived"));
        }
        if (jSONObject.has("mobileSent")) {
            if (jSONObject.isNull("mobileSent")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mobileSent' to null.");
            }
            networkStatisticsRealmProxyInterface.realmSet$mobileSent(jSONObject.getDouble("mobileSent"));
        }
        return networkStatistics;
    }

    @TargetApi(11)
    public static NetworkStatistics createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        NetworkStatistics networkStatistics = new NetworkStatistics();
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("wifiReceived")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkStatisticsRealmProxyInterface.realmSet$wifiReceived(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'wifiReceived' to null.");
                }
            } else if (nextName.equals("wifiSent")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkStatisticsRealmProxyInterface.realmSet$wifiSent(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'wifiSent' to null.");
                }
            } else if (nextName.equals("mobileReceived")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkStatisticsRealmProxyInterface.realmSet$mobileReceived(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mobileReceived' to null.");
                }
            } else if (!nextName.equals("mobileSent")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                networkStatisticsRealmProxyInterface.realmSet$mobileSent(jsonReader.nextDouble());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'mobileSent' to null.");
            }
        }
        jsonReader.endObject();
        return (NetworkStatistics) realm.copyToRealm(networkStatistics);
    }

    public static NetworkStatistics copyOrUpdate(Realm realm, NetworkStatistics networkStatistics, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (networkStatistics instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkStatistics;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return networkStatistics;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(networkStatistics);
        if (realmObjectProxy2 != null) {
            return (NetworkStatistics) realmObjectProxy2;
        }
        return copy(realm, networkStatistics, z, map);
    }

    public static NetworkStatistics copy(Realm realm, NetworkStatistics networkStatistics, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(networkStatistics);
        if (realmObjectProxy != null) {
            return (NetworkStatistics) realmObjectProxy;
        }
        NetworkStatistics networkStatistics2 = (NetworkStatistics) realm.createObjectInternal(NetworkStatistics.class, false, Collections.emptyList());
        map.put(networkStatistics, (RealmObjectProxy) networkStatistics2);
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics;
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface2 = networkStatistics2;
        networkStatisticsRealmProxyInterface2.realmSet$wifiReceived(networkStatisticsRealmProxyInterface.realmGet$wifiReceived());
        networkStatisticsRealmProxyInterface2.realmSet$wifiSent(networkStatisticsRealmProxyInterface.realmGet$wifiSent());
        networkStatisticsRealmProxyInterface2.realmSet$mobileReceived(networkStatisticsRealmProxyInterface.realmGet$mobileReceived());
        networkStatisticsRealmProxyInterface2.realmSet$mobileSent(networkStatisticsRealmProxyInterface.realmGet$mobileSent());
        return networkStatistics2;
    }

    public static long insert(Realm realm, NetworkStatistics networkStatistics, Map<RealmModel, Long> map) {
        NetworkStatistics networkStatistics2 = networkStatistics;
        if (networkStatistics2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkStatistics2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(NetworkStatistics.class);
        long nativePtr = table.getNativePtr();
        NetworkStatisticsColumnInfo networkStatisticsColumnInfo = (NetworkStatisticsColumnInfo) realm.getSchema().getColumnInfo(NetworkStatistics.class);
        long createRow = OsObject.createRow(table);
        map.put(networkStatistics2, Long.valueOf(createRow));
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiReceivedIndex, j2, networkStatisticsRealmProxyInterface.realmGet$wifiReceived(), false);
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiSentIndex, j2, networkStatisticsRealmProxyInterface.realmGet$wifiSent(), false);
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileReceivedIndex, j2, networkStatisticsRealmProxyInterface.realmGet$mobileReceived(), false);
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileSentIndex, j2, networkStatisticsRealmProxyInterface.realmGet$mobileSent(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(NetworkStatistics.class);
        long nativePtr = table.getNativePtr();
        NetworkStatisticsColumnInfo networkStatisticsColumnInfo = (NetworkStatisticsColumnInfo) realm.getSchema().getColumnInfo(NetworkStatistics.class);
        while (it.hasNext()) {
            NetworkStatistics networkStatistics = (NetworkStatistics) it.next();
            if (!map2.containsKey(networkStatistics)) {
                if (networkStatistics instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkStatistics;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(networkStatistics, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(networkStatistics, Long.valueOf(createRow));
                NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiReceivedIndex, createRow, networkStatisticsRealmProxyInterface.realmGet$wifiReceived(), false);
                long j3 = j2;
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiSentIndex, j3, networkStatisticsRealmProxyInterface.realmGet$wifiSent(), false);
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileReceivedIndex, j3, networkStatisticsRealmProxyInterface.realmGet$mobileReceived(), false);
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileSentIndex, j3, networkStatisticsRealmProxyInterface.realmGet$mobileSent(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, NetworkStatistics networkStatistics, Map<RealmModel, Long> map) {
        NetworkStatistics networkStatistics2 = networkStatistics;
        if (networkStatistics2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkStatistics2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(NetworkStatistics.class);
        long nativePtr = table.getNativePtr();
        NetworkStatisticsColumnInfo networkStatisticsColumnInfo = (NetworkStatisticsColumnInfo) realm.getSchema().getColumnInfo(NetworkStatistics.class);
        long createRow = OsObject.createRow(table);
        map.put(networkStatistics2, Long.valueOf(createRow));
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiReceivedIndex, j2, networkStatisticsRealmProxyInterface.realmGet$wifiReceived(), false);
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiSentIndex, j2, networkStatisticsRealmProxyInterface.realmGet$wifiSent(), false);
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileReceivedIndex, j2, networkStatisticsRealmProxyInterface.realmGet$mobileReceived(), false);
        Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileSentIndex, j2, networkStatisticsRealmProxyInterface.realmGet$mobileSent(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(NetworkStatistics.class);
        long nativePtr = table.getNativePtr();
        NetworkStatisticsColumnInfo networkStatisticsColumnInfo = (NetworkStatisticsColumnInfo) realm.getSchema().getColumnInfo(NetworkStatistics.class);
        while (it.hasNext()) {
            NetworkStatistics networkStatistics = (NetworkStatistics) it.next();
            if (!map2.containsKey(networkStatistics)) {
                if (networkStatistics instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkStatistics;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(networkStatistics, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(networkStatistics, Long.valueOf(createRow));
                NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiReceivedIndex, createRow, networkStatisticsRealmProxyInterface.realmGet$wifiReceived(), false);
                long j3 = j2;
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.wifiSentIndex, j3, networkStatisticsRealmProxyInterface.realmGet$wifiSent(), false);
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileReceivedIndex, j3, networkStatisticsRealmProxyInterface.realmGet$mobileReceived(), false);
                Table.nativeSetDouble(j, networkStatisticsColumnInfo.mobileSentIndex, j3, networkStatisticsRealmProxyInterface.realmGet$mobileSent(), false);
            }
        }
    }

    public static NetworkStatistics createDetachedCopy(NetworkStatistics networkStatistics, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        NetworkStatistics networkStatistics2;
        if (i > i2 || networkStatistics == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(networkStatistics);
        if (cacheData == null) {
            networkStatistics2 = new NetworkStatistics();
            map.put(networkStatistics, new CacheData(i, networkStatistics2));
        } else if (i >= cacheData.minDepth) {
            return (NetworkStatistics) cacheData.object;
        } else {
            NetworkStatistics networkStatistics3 = (NetworkStatistics) cacheData.object;
            cacheData.minDepth = i;
            networkStatistics2 = networkStatistics3;
        }
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface = networkStatistics2;
        NetworkStatisticsRealmProxyInterface networkStatisticsRealmProxyInterface2 = networkStatistics;
        networkStatisticsRealmProxyInterface.realmSet$wifiReceived(networkStatisticsRealmProxyInterface2.realmGet$wifiReceived());
        networkStatisticsRealmProxyInterface.realmSet$wifiSent(networkStatisticsRealmProxyInterface2.realmGet$wifiSent());
        networkStatisticsRealmProxyInterface.realmSet$mobileReceived(networkStatisticsRealmProxyInterface2.realmGet$mobileReceived());
        networkStatisticsRealmProxyInterface.realmSet$mobileSent(networkStatisticsRealmProxyInterface2.realmGet$mobileSent());
        return networkStatistics2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("NetworkStatistics = proxy[");
        sb.append("{wifiReceived:");
        sb.append(realmGet$wifiReceived());
        sb.append("}");
        sb.append(",");
        sb.append("{wifiSent:");
        sb.append(realmGet$wifiSent());
        sb.append("}");
        sb.append(",");
        sb.append("{mobileReceived:");
        sb.append(realmGet$mobileReceived());
        sb.append("}");
        sb.append(",");
        sb.append("{mobileSent:");
        sb.append(realmGet$mobileSent());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NetworkStatisticsRealmProxy networkStatisticsRealmProxy = (NetworkStatisticsRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = networkStatisticsRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = networkStatisticsRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == networkStatisticsRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
