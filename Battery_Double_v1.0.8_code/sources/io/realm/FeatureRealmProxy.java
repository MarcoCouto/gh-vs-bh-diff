package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.mansoon.BatteryDouble.models.data.Feature;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class FeatureRealmProxy extends Feature implements RealmObjectProxy, FeatureRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private FeatureColumnInfo columnInfo;
    private ProxyState<Feature> proxyState;

    static final class FeatureColumnInfo extends ColumnInfo {
        long keyIndex;
        long valueIndex;

        FeatureColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(2);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("Feature");
            this.keyIndex = addColumnDetails("key", objectSchemaInfo);
            this.valueIndex = addColumnDetails(Param.VALUE, objectSchemaInfo);
        }

        FeatureColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new FeatureColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            FeatureColumnInfo featureColumnInfo = (FeatureColumnInfo) columnInfo;
            FeatureColumnInfo featureColumnInfo2 = (FeatureColumnInfo) columnInfo2;
            featureColumnInfo2.keyIndex = featureColumnInfo.keyIndex;
            featureColumnInfo2.valueIndex = featureColumnInfo.valueIndex;
        }
    }

    public static String getTableName() {
        return "class_Feature";
    }

    static {
        ArrayList arrayList = new ArrayList(2);
        arrayList.add("key");
        arrayList.add(Param.VALUE);
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    FeatureRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (FeatureColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$key() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.keyIndex);
    }

    public void realmSet$key(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.keyIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.keyIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.keyIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.keyIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$value() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.valueIndex);
    }

    public void realmSet$value(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.valueIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.valueIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.valueIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.valueIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("Feature", 2, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("key", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty(Param.VALUE, RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static FeatureColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new FeatureColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Feature createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        Feature feature = (Feature) realm.createObjectInternal(Feature.class, true, Collections.emptyList());
        FeatureRealmProxyInterface featureRealmProxyInterface = feature;
        if (jSONObject.has("key")) {
            if (jSONObject.isNull("key")) {
                featureRealmProxyInterface.realmSet$key(null);
            } else {
                featureRealmProxyInterface.realmSet$key(jSONObject.getString("key"));
            }
        }
        if (jSONObject.has(Param.VALUE)) {
            if (jSONObject.isNull(Param.VALUE)) {
                featureRealmProxyInterface.realmSet$value(null);
            } else {
                featureRealmProxyInterface.realmSet$value(jSONObject.getString(Param.VALUE));
            }
        }
        return feature;
    }

    @TargetApi(11)
    public static Feature createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        Feature feature = new Feature();
        FeatureRealmProxyInterface featureRealmProxyInterface = feature;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("key")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    featureRealmProxyInterface.realmSet$key(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    featureRealmProxyInterface.realmSet$key(null);
                }
            } else if (!nextName.equals(Param.VALUE)) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                featureRealmProxyInterface.realmSet$value(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                featureRealmProxyInterface.realmSet$value(null);
            }
        }
        jsonReader.endObject();
        return (Feature) realm.copyToRealm(feature);
    }

    public static Feature copyOrUpdate(Realm realm, Feature feature, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (feature instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) feature;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return feature;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(feature);
        if (realmObjectProxy2 != null) {
            return (Feature) realmObjectProxy2;
        }
        return copy(realm, feature, z, map);
    }

    public static Feature copy(Realm realm, Feature feature, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(feature);
        if (realmObjectProxy != null) {
            return (Feature) realmObjectProxy;
        }
        Feature feature2 = (Feature) realm.createObjectInternal(Feature.class, false, Collections.emptyList());
        map.put(feature, (RealmObjectProxy) feature2);
        FeatureRealmProxyInterface featureRealmProxyInterface = feature;
        FeatureRealmProxyInterface featureRealmProxyInterface2 = feature2;
        featureRealmProxyInterface2.realmSet$key(featureRealmProxyInterface.realmGet$key());
        featureRealmProxyInterface2.realmSet$value(featureRealmProxyInterface.realmGet$value());
        return feature2;
    }

    public static long insert(Realm realm, Feature feature, Map<RealmModel, Long> map) {
        if (feature instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) feature;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Feature.class);
        long nativePtr = table.getNativePtr();
        FeatureColumnInfo featureColumnInfo = (FeatureColumnInfo) realm.getSchema().getColumnInfo(Feature.class);
        long createRow = OsObject.createRow(table);
        map.put(feature, Long.valueOf(createRow));
        FeatureRealmProxyInterface featureRealmProxyInterface = feature;
        String realmGet$key = featureRealmProxyInterface.realmGet$key();
        if (realmGet$key != null) {
            Table.nativeSetString(nativePtr, featureColumnInfo.keyIndex, createRow, realmGet$key, false);
        }
        String realmGet$value = featureRealmProxyInterface.realmGet$value();
        if (realmGet$value != null) {
            Table.nativeSetString(nativePtr, featureColumnInfo.valueIndex, createRow, realmGet$value, false);
        }
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        FeatureRealmProxyInterface featureRealmProxyInterface;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Feature.class);
        long nativePtr = table.getNativePtr();
        FeatureColumnInfo featureColumnInfo = (FeatureColumnInfo) realm.getSchema().getColumnInfo(Feature.class);
        while (it.hasNext()) {
            Feature feature = (Feature) it.next();
            if (!map2.containsKey(feature)) {
                if (feature instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) feature;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(feature, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(feature, Long.valueOf(createRow));
                FeatureRealmProxyInterface featureRealmProxyInterface2 = feature;
                String realmGet$key = featureRealmProxyInterface2.realmGet$key();
                if (realmGet$key != null) {
                    featureRealmProxyInterface = featureRealmProxyInterface2;
                    Table.nativeSetString(nativePtr, featureColumnInfo.keyIndex, createRow, realmGet$key, false);
                } else {
                    featureRealmProxyInterface = featureRealmProxyInterface2;
                }
                String realmGet$value = featureRealmProxyInterface.realmGet$value();
                if (realmGet$value != null) {
                    Table.nativeSetString(nativePtr, featureColumnInfo.valueIndex, createRow, realmGet$value, false);
                }
                map2 = map;
            }
        }
    }

    public static long insertOrUpdate(Realm realm, Feature feature, Map<RealmModel, Long> map) {
        if (feature instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) feature;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Feature.class);
        long nativePtr = table.getNativePtr();
        FeatureColumnInfo featureColumnInfo = (FeatureColumnInfo) realm.getSchema().getColumnInfo(Feature.class);
        long createRow = OsObject.createRow(table);
        map.put(feature, Long.valueOf(createRow));
        FeatureRealmProxyInterface featureRealmProxyInterface = feature;
        String realmGet$key = featureRealmProxyInterface.realmGet$key();
        if (realmGet$key != null) {
            Table.nativeSetString(nativePtr, featureColumnInfo.keyIndex, createRow, realmGet$key, false);
        } else {
            Table.nativeSetNull(nativePtr, featureColumnInfo.keyIndex, createRow, false);
        }
        String realmGet$value = featureRealmProxyInterface.realmGet$value();
        if (realmGet$value != null) {
            Table.nativeSetString(nativePtr, featureColumnInfo.valueIndex, createRow, realmGet$value, false);
        } else {
            Table.nativeSetNull(nativePtr, featureColumnInfo.valueIndex, createRow, false);
        }
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        FeatureRealmProxyInterface featureRealmProxyInterface;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Feature.class);
        long nativePtr = table.getNativePtr();
        FeatureColumnInfo featureColumnInfo = (FeatureColumnInfo) realm.getSchema().getColumnInfo(Feature.class);
        while (it.hasNext()) {
            Feature feature = (Feature) it.next();
            if (!map2.containsKey(feature)) {
                if (feature instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) feature;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(feature, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(feature, Long.valueOf(createRow));
                FeatureRealmProxyInterface featureRealmProxyInterface2 = feature;
                String realmGet$key = featureRealmProxyInterface2.realmGet$key();
                if (realmGet$key != null) {
                    featureRealmProxyInterface = featureRealmProxyInterface2;
                    Table.nativeSetString(nativePtr, featureColumnInfo.keyIndex, createRow, realmGet$key, false);
                } else {
                    featureRealmProxyInterface = featureRealmProxyInterface2;
                    Table.nativeSetNull(nativePtr, featureColumnInfo.keyIndex, createRow, false);
                }
                String realmGet$value = featureRealmProxyInterface.realmGet$value();
                if (realmGet$value != null) {
                    Table.nativeSetString(nativePtr, featureColumnInfo.valueIndex, createRow, realmGet$value, false);
                } else {
                    Table.nativeSetNull(nativePtr, featureColumnInfo.valueIndex, createRow, false);
                }
                map2 = map;
            }
        }
    }

    public static Feature createDetachedCopy(Feature feature, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        Feature feature2;
        if (i > i2 || feature == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(feature);
        if (cacheData == null) {
            feature2 = new Feature();
            map.put(feature, new CacheData(i, feature2));
        } else if (i >= cacheData.minDepth) {
            return (Feature) cacheData.object;
        } else {
            Feature feature3 = (Feature) cacheData.object;
            cacheData.minDepth = i;
            feature2 = feature3;
        }
        FeatureRealmProxyInterface featureRealmProxyInterface = feature2;
        FeatureRealmProxyInterface featureRealmProxyInterface2 = feature;
        featureRealmProxyInterface.realmSet$key(featureRealmProxyInterface2.realmGet$key());
        featureRealmProxyInterface.realmSet$value(featureRealmProxyInterface2.realmGet$value());
        return feature2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Feature = proxy[");
        sb.append("{key:");
        sb.append(realmGet$key() != null ? realmGet$key() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{value:");
        sb.append(realmGet$value() != null ? realmGet$value() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        FeatureRealmProxy featureRealmProxy = (FeatureRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = featureRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = featureRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == featureRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
