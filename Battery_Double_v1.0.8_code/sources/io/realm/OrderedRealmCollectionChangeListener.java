package io.realm;

import javax.annotation.Nullable;

public interface OrderedRealmCollectionChangeListener<T> {
    void onChange(T t, @Nullable OrderedCollectionChangeSet orderedCollectionChangeSet);
}
