package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mansoon.BatteryDouble.models.data.BatteryDetails;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.exceptions.RealmException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class BatteryUsageRealmProxy extends BatteryUsage implements RealmObjectProxy, BatteryUsageRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private BatteryUsageColumnInfo columnInfo;
    private ProxyState<BatteryUsage> proxyState;

    static final class BatteryUsageColumnInfo extends ColumnInfo {
        long detailsIndex;
        long idIndex;
        long levelIndex;
        long screenOnIndex;
        long stateIndex;
        long timestampIndex;
        long triggeredByIndex;

        BatteryUsageColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(7);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("BatteryUsage");
            this.idIndex = addColumnDetails("id", objectSchemaInfo);
            this.timestampIndex = addColumnDetails(Param.TIMESTAMP, objectSchemaInfo);
            this.stateIndex = addColumnDetails(ServerProtocol.DIALOG_PARAM_STATE, objectSchemaInfo);
            this.levelIndex = addColumnDetails(FirebaseAnalytics.Param.LEVEL, objectSchemaInfo);
            this.screenOnIndex = addColumnDetails("screenOn", objectSchemaInfo);
            this.triggeredByIndex = addColumnDetails("triggeredBy", objectSchemaInfo);
            this.detailsIndex = addColumnDetails("details", objectSchemaInfo);
        }

        BatteryUsageColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new BatteryUsageColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            BatteryUsageColumnInfo batteryUsageColumnInfo = (BatteryUsageColumnInfo) columnInfo;
            BatteryUsageColumnInfo batteryUsageColumnInfo2 = (BatteryUsageColumnInfo) columnInfo2;
            batteryUsageColumnInfo2.idIndex = batteryUsageColumnInfo.idIndex;
            batteryUsageColumnInfo2.timestampIndex = batteryUsageColumnInfo.timestampIndex;
            batteryUsageColumnInfo2.stateIndex = batteryUsageColumnInfo.stateIndex;
            batteryUsageColumnInfo2.levelIndex = batteryUsageColumnInfo.levelIndex;
            batteryUsageColumnInfo2.screenOnIndex = batteryUsageColumnInfo.screenOnIndex;
            batteryUsageColumnInfo2.triggeredByIndex = batteryUsageColumnInfo.triggeredByIndex;
            batteryUsageColumnInfo2.detailsIndex = batteryUsageColumnInfo.detailsIndex;
        }
    }

    public static String getTableName() {
        return "class_BatteryUsage";
    }

    static {
        ArrayList arrayList = new ArrayList(7);
        arrayList.add("id");
        arrayList.add(Param.TIMESTAMP);
        arrayList.add(ServerProtocol.DIALOG_PARAM_STATE);
        arrayList.add(FirebaseAnalytics.Param.LEVEL);
        arrayList.add("screenOn");
        arrayList.add("triggeredBy");
        arrayList.add("details");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    BatteryUsageRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (BatteryUsageColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$id() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.idIndex);
    }

    public void realmSet$id(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public long realmGet$timestamp() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.timestampIndex);
    }

    public void realmSet$timestamp(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.timestampIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.timestampIndex, row$realm.getIndex(), j, true);
        }
    }

    public String realmGet$state() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.stateIndex);
    }

    public void realmSet$state(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.stateIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.stateIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.stateIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.stateIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public float realmGet$level() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getFloat(this.columnInfo.levelIndex);
    }

    public void realmSet$level(float f) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setFloat(this.columnInfo.levelIndex, f);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setFloat(this.columnInfo.levelIndex, row$realm.getIndex(), f, true);
        }
    }

    public int realmGet$screenOn() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.screenOnIndex);
    }

    public void realmSet$screenOn(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.screenOnIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.screenOnIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$triggeredBy() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.triggeredByIndex);
    }

    public void realmSet$triggeredBy(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.triggeredByIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.triggeredByIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.triggeredByIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.triggeredByIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public BatteryDetails realmGet$details() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.detailsIndex)) {
            return null;
        }
        return (BatteryDetails) this.proxyState.getRealm$realm().get(BatteryDetails.class, this.proxyState.getRow$realm().getLink(this.columnInfo.detailsIndex), false, Collections.emptyList());
    }

    public void realmSet$details(BatteryDetails batteryDetails) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (batteryDetails == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.detailsIndex);
                return;
            }
            this.proxyState.checkValidObject(batteryDetails);
            this.proxyState.getRow$realm().setLink(this.columnInfo.detailsIndex, ((RealmObjectProxy) batteryDetails).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("details")) {
            if (batteryDetails != null && !RealmObject.isManaged(batteryDetails)) {
                batteryDetails = (BatteryDetails) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(batteryDetails);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (batteryDetails == null) {
                row$realm.nullifyLink(this.columnInfo.detailsIndex);
                return;
            }
            this.proxyState.checkValidObject(batteryDetails);
            row$realm.getTable().setLink(this.columnInfo.detailsIndex, row$realm.getIndex(), ((RealmObjectProxy) batteryDetails).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("BatteryUsage", 7, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("id", RealmFieldType.INTEGER, true, true, true);
        builder2.addPersistedProperty(Param.TIMESTAMP, RealmFieldType.INTEGER, false, true, true);
        builder2.addPersistedProperty(ServerProtocol.DIALOG_PARAM_STATE, RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty(FirebaseAnalytics.Param.LEVEL, RealmFieldType.FLOAT, false, false, true);
        builder2.addPersistedProperty("screenOn", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("triggeredBy", RealmFieldType.STRING, false, false, false);
        builder.addPersistedLinkProperty("details", RealmFieldType.OBJECT, "BatteryDetails");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static BatteryUsageColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new BatteryUsageColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0154  */
    public static BatteryUsage createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        BatteryUsageRealmProxy batteryUsageRealmProxy;
        ArrayList arrayList = new ArrayList(1);
        if (z) {
            Table table = realm.getTable(BatteryUsage.class);
            long findFirstLong = !jSONObject.isNull("id") ? table.findFirstLong(((BatteryUsageColumnInfo) realm.getSchema().getColumnInfo(BatteryUsage.class)).idIndex, jSONObject.getLong("id")) : -1;
            if (findFirstLong != -1) {
                RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
                try {
                    realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(BatteryUsage.class), false, Collections.emptyList());
                    batteryUsageRealmProxy = new BatteryUsageRealmProxy();
                    if (batteryUsageRealmProxy == null) {
                        if (jSONObject.has("details")) {
                            arrayList.add("details");
                        }
                        if (!jSONObject.has("id")) {
                            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
                        } else if (jSONObject.isNull("id")) {
                            batteryUsageRealmProxy = (BatteryUsageRealmProxy) realm.createObjectInternal(BatteryUsage.class, null, true, arrayList);
                        } else {
                            batteryUsageRealmProxy = (BatteryUsageRealmProxy) realm.createObjectInternal(BatteryUsage.class, Integer.valueOf(jSONObject.getInt("id")), true, arrayList);
                        }
                    }
                    BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsageRealmProxy;
                    if (jSONObject.has(Param.TIMESTAMP)) {
                        if (jSONObject.isNull(Param.TIMESTAMP)) {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                        }
                        batteryUsageRealmProxyInterface.realmSet$timestamp(jSONObject.getLong(Param.TIMESTAMP));
                    }
                    if (jSONObject.has(ServerProtocol.DIALOG_PARAM_STATE)) {
                        if (jSONObject.isNull(ServerProtocol.DIALOG_PARAM_STATE)) {
                            batteryUsageRealmProxyInterface.realmSet$state(null);
                        } else {
                            batteryUsageRealmProxyInterface.realmSet$state(jSONObject.getString(ServerProtocol.DIALOG_PARAM_STATE));
                        }
                    }
                    if (jSONObject.has(FirebaseAnalytics.Param.LEVEL)) {
                        if (jSONObject.isNull(FirebaseAnalytics.Param.LEVEL)) {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'level' to null.");
                        }
                        batteryUsageRealmProxyInterface.realmSet$level((float) jSONObject.getDouble(FirebaseAnalytics.Param.LEVEL));
                    }
                    if (jSONObject.has("screenOn")) {
                        if (jSONObject.isNull("screenOn")) {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'screenOn' to null.");
                        }
                        batteryUsageRealmProxyInterface.realmSet$screenOn(jSONObject.getInt("screenOn"));
                    }
                    if (jSONObject.has("triggeredBy")) {
                        if (jSONObject.isNull("triggeredBy")) {
                            batteryUsageRealmProxyInterface.realmSet$triggeredBy(null);
                        } else {
                            batteryUsageRealmProxyInterface.realmSet$triggeredBy(jSONObject.getString("triggeredBy"));
                        }
                    }
                    if (jSONObject.has("details")) {
                        if (jSONObject.isNull("details")) {
                            batteryUsageRealmProxyInterface.realmSet$details(null);
                        } else {
                            batteryUsageRealmProxyInterface.realmSet$details(BatteryDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("details"), z));
                        }
                    }
                    return batteryUsageRealmProxy;
                } finally {
                    realmObjectContext.clear();
                }
            }
        }
        batteryUsageRealmProxy = null;
        if (batteryUsageRealmProxy == null) {
        }
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface2 = batteryUsageRealmProxy;
        if (jSONObject.has(Param.TIMESTAMP)) {
        }
        if (jSONObject.has(ServerProtocol.DIALOG_PARAM_STATE)) {
        }
        if (jSONObject.has(FirebaseAnalytics.Param.LEVEL)) {
        }
        if (jSONObject.has("screenOn")) {
        }
        if (jSONObject.has("triggeredBy")) {
        }
        if (jSONObject.has("details")) {
        }
        return batteryUsageRealmProxy;
    }

    @TargetApi(11)
    public static BatteryUsage createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        BatteryUsage batteryUsage = new BatteryUsage();
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage;
        jsonReader.beginObject();
        boolean z = false;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("id")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryUsageRealmProxyInterface.realmSet$id(jsonReader.nextInt());
                    z = true;
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
            } else if (nextName.equals(Param.TIMESTAMP)) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryUsageRealmProxyInterface.realmSet$timestamp(jsonReader.nextLong());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                }
            } else if (nextName.equals(ServerProtocol.DIALOG_PARAM_STATE)) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryUsageRealmProxyInterface.realmSet$state(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    batteryUsageRealmProxyInterface.realmSet$state(null);
                }
            } else if (nextName.equals(FirebaseAnalytics.Param.LEVEL)) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryUsageRealmProxyInterface.realmSet$level((float) jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'level' to null.");
                }
            } else if (nextName.equals("screenOn")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryUsageRealmProxyInterface.realmSet$screenOn(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'screenOn' to null.");
                }
            } else if (nextName.equals("triggeredBy")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryUsageRealmProxyInterface.realmSet$triggeredBy(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    batteryUsageRealmProxyInterface.realmSet$triggeredBy(null);
                }
            } else if (!nextName.equals("details")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.skipValue();
                batteryUsageRealmProxyInterface.realmSet$details(null);
            } else {
                batteryUsageRealmProxyInterface.realmSet$details(BatteryDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
            }
        }
        jsonReader.endObject();
        if (z) {
            return (BatteryUsage) realm.copyToRealm(batteryUsage);
        }
        throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00aa  */
    public static BatteryUsage copyOrUpdate(Realm realm, BatteryUsage batteryUsage, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        boolean z2;
        if (batteryUsage instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryUsage;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return batteryUsage;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(batteryUsage);
        if (realmObjectProxy2 != null) {
            return (BatteryUsage) realmObjectProxy2;
        }
        BatteryUsageRealmProxy batteryUsageRealmProxy = null;
        if (z) {
            Table table = realm.getTable(BatteryUsage.class);
            long findFirstLong = table.findFirstLong(((BatteryUsageColumnInfo) realm.getSchema().getColumnInfo(BatteryUsage.class)).idIndex, (long) batteryUsage.realmGet$id());
            if (findFirstLong == -1) {
                z2 = false;
                return !z2 ? update(realm, batteryUsageRealmProxy, batteryUsage, map) : copy(realm, batteryUsage, z, map);
            }
            try {
                realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(BatteryUsage.class), false, Collections.emptyList());
                batteryUsageRealmProxy = new BatteryUsageRealmProxy();
                map.put(batteryUsage, batteryUsageRealmProxy);
            } finally {
                realmObjectContext.clear();
            }
        }
        z2 = z;
        return !z2 ? update(realm, batteryUsageRealmProxy, batteryUsage, map) : copy(realm, batteryUsage, z, map);
    }

    public static BatteryUsage copy(Realm realm, BatteryUsage batteryUsage, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(batteryUsage);
        if (realmObjectProxy != null) {
            return (BatteryUsage) realmObjectProxy;
        }
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage;
        BatteryUsage batteryUsage2 = (BatteryUsage) realm.createObjectInternal(BatteryUsage.class, Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id()), false, Collections.emptyList());
        map.put(batteryUsage, (RealmObjectProxy) batteryUsage2);
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface2 = batteryUsage2;
        batteryUsageRealmProxyInterface2.realmSet$timestamp(batteryUsageRealmProxyInterface.realmGet$timestamp());
        batteryUsageRealmProxyInterface2.realmSet$state(batteryUsageRealmProxyInterface.realmGet$state());
        batteryUsageRealmProxyInterface2.realmSet$level(batteryUsageRealmProxyInterface.realmGet$level());
        batteryUsageRealmProxyInterface2.realmSet$screenOn(batteryUsageRealmProxyInterface.realmGet$screenOn());
        batteryUsageRealmProxyInterface2.realmSet$triggeredBy(batteryUsageRealmProxyInterface.realmGet$triggeredBy());
        BatteryDetails realmGet$details = batteryUsageRealmProxyInterface.realmGet$details();
        if (realmGet$details == null) {
            batteryUsageRealmProxyInterface2.realmSet$details(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$details);
            if (batteryDetails != null) {
                batteryUsageRealmProxyInterface2.realmSet$details(batteryDetails);
            } else {
                batteryUsageRealmProxyInterface2.realmSet$details(BatteryDetailsRealmProxy.copyOrUpdate(realm, realmGet$details, z, map));
            }
        }
        return batteryUsage2;
    }

    public static long insert(Realm realm, BatteryUsage batteryUsage, Map<RealmModel, Long> map) {
        long j;
        Integer num;
        long j2;
        Realm realm2 = realm;
        BatteryUsage batteryUsage2 = batteryUsage;
        Map<RealmModel, Long> map2 = map;
        if (batteryUsage2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryUsage2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(BatteryUsage.class);
        long nativePtr = table.getNativePtr();
        BatteryUsageColumnInfo batteryUsageColumnInfo = (BatteryUsageColumnInfo) realm.getSchema().getColumnInfo(BatteryUsage.class);
        long j3 = batteryUsageColumnInfo.idIndex;
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage2;
        Integer valueOf = Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id());
        if (valueOf != null) {
            num = valueOf;
            j = Table.nativeFindFirstInt(nativePtr, j3, (long) batteryUsageRealmProxyInterface.realmGet$id());
        } else {
            num = valueOf;
            j = -1;
        }
        if (j == -1) {
            j2 = OsObject.createRowWithPrimaryKey(table, j3, Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id()));
        } else {
            Table.throwDuplicatePrimaryKeyException(num);
            j2 = j;
        }
        map2.put(batteryUsage2, Long.valueOf(j2));
        long j4 = j2;
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface2 = batteryUsageRealmProxyInterface;
        Table.nativeSetLong(nativePtr, batteryUsageColumnInfo.timestampIndex, j2, batteryUsageRealmProxyInterface.realmGet$timestamp(), false);
        String realmGet$state = batteryUsageRealmProxyInterface2.realmGet$state();
        if (realmGet$state != null) {
            Table.nativeSetString(nativePtr, batteryUsageColumnInfo.stateIndex, j4, realmGet$state, false);
        }
        long j5 = nativePtr;
        long j6 = j4;
        Table.nativeSetFloat(j5, batteryUsageColumnInfo.levelIndex, j6, batteryUsageRealmProxyInterface2.realmGet$level(), false);
        Table.nativeSetLong(j5, batteryUsageColumnInfo.screenOnIndex, j6, (long) batteryUsageRealmProxyInterface2.realmGet$screenOn(), false);
        String realmGet$triggeredBy = batteryUsageRealmProxyInterface2.realmGet$triggeredBy();
        if (realmGet$triggeredBy != null) {
            Table.nativeSetString(nativePtr, batteryUsageColumnInfo.triggeredByIndex, j4, realmGet$triggeredBy, false);
        }
        BatteryDetails realmGet$details = batteryUsageRealmProxyInterface2.realmGet$details();
        if (realmGet$details != null) {
            Long l = (Long) map2.get(realmGet$details);
            if (l == null) {
                l = Long.valueOf(BatteryDetailsRealmProxy.insert(realm, realmGet$details, map2));
            }
            Table.nativeSetLink(nativePtr, batteryUsageColumnInfo.detailsIndex, j4, l.longValue(), false);
        }
        return j4;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Integer num;
        Realm realm2;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(BatteryUsage.class);
        long nativePtr = table.getNativePtr();
        BatteryUsageColumnInfo batteryUsageColumnInfo = (BatteryUsageColumnInfo) realm.getSchema().getColumnInfo(BatteryUsage.class);
        long j2 = batteryUsageColumnInfo.idIndex;
        while (it.hasNext()) {
            BatteryUsage batteryUsage = (BatteryUsage) it.next();
            if (!map2.containsKey(batteryUsage)) {
                if (batteryUsage instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryUsage;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(batteryUsage, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage;
                Integer valueOf = Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id());
                if (valueOf != null) {
                    num = valueOf;
                    j = Table.nativeFindFirstInt(nativePtr, j2, (long) batteryUsageRealmProxyInterface.realmGet$id());
                } else {
                    num = valueOf;
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j2, Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id()));
                } else {
                    Table.throwDuplicatePrimaryKeyException(num);
                }
                long j3 = j;
                map2.put(batteryUsage, Long.valueOf(j3));
                long j4 = j3;
                long j5 = j2;
                Table.nativeSetLong(nativePtr, batteryUsageColumnInfo.timestampIndex, j3, batteryUsageRealmProxyInterface.realmGet$timestamp(), false);
                String realmGet$state = batteryUsageRealmProxyInterface.realmGet$state();
                if (realmGet$state != null) {
                    Table.nativeSetString(nativePtr, batteryUsageColumnInfo.stateIndex, j4, realmGet$state, false);
                }
                long j6 = nativePtr;
                long j7 = j4;
                Table.nativeSetFloat(j6, batteryUsageColumnInfo.levelIndex, j7, batteryUsageRealmProxyInterface.realmGet$level(), false);
                Table.nativeSetLong(j6, batteryUsageColumnInfo.screenOnIndex, j7, (long) batteryUsageRealmProxyInterface.realmGet$screenOn(), false);
                String realmGet$triggeredBy = batteryUsageRealmProxyInterface.realmGet$triggeredBy();
                if (realmGet$triggeredBy != null) {
                    Table.nativeSetString(nativePtr, batteryUsageColumnInfo.triggeredByIndex, j4, realmGet$triggeredBy, false);
                }
                BatteryDetails realmGet$details = batteryUsageRealmProxyInterface.realmGet$details();
                if (realmGet$details != null) {
                    Long l = (Long) map2.get(realmGet$details);
                    if (l == null) {
                        realm2 = realm;
                        l = Long.valueOf(BatteryDetailsRealmProxy.insert(realm2, realmGet$details, map2));
                    } else {
                        realm2 = realm;
                    }
                    table.setLink(batteryUsageColumnInfo.detailsIndex, j4, l.longValue(), false);
                } else {
                    realm2 = realm;
                }
                Realm realm3 = realm2;
                j2 = j5;
            }
        }
    }

    public static long insertOrUpdate(Realm realm, BatteryUsage batteryUsage, Map<RealmModel, Long> map) {
        Realm realm2 = realm;
        BatteryUsage batteryUsage2 = batteryUsage;
        Map<RealmModel, Long> map2 = map;
        if (batteryUsage2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryUsage2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(BatteryUsage.class);
        long nativePtr = table.getNativePtr();
        BatteryUsageColumnInfo batteryUsageColumnInfo = (BatteryUsageColumnInfo) realm.getSchema().getColumnInfo(BatteryUsage.class);
        long j = batteryUsageColumnInfo.idIndex;
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage2;
        long nativeFindFirstInt = Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id()) != null ? Table.nativeFindFirstInt(nativePtr, j, (long) batteryUsageRealmProxyInterface.realmGet$id()) : -1;
        long createRowWithPrimaryKey = nativeFindFirstInt == -1 ? OsObject.createRowWithPrimaryKey(table, j, Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id())) : nativeFindFirstInt;
        map2.put(batteryUsage2, Long.valueOf(createRowWithPrimaryKey));
        long j2 = createRowWithPrimaryKey;
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface2 = batteryUsageRealmProxyInterface;
        Table.nativeSetLong(nativePtr, batteryUsageColumnInfo.timestampIndex, createRowWithPrimaryKey, batteryUsageRealmProxyInterface.realmGet$timestamp(), false);
        String realmGet$state = batteryUsageRealmProxyInterface2.realmGet$state();
        if (realmGet$state != null) {
            Table.nativeSetString(nativePtr, batteryUsageColumnInfo.stateIndex, j2, realmGet$state, false);
        } else {
            Table.nativeSetNull(nativePtr, batteryUsageColumnInfo.stateIndex, j2, false);
        }
        long j3 = nativePtr;
        long j4 = j2;
        Table.nativeSetFloat(j3, batteryUsageColumnInfo.levelIndex, j4, batteryUsageRealmProxyInterface2.realmGet$level(), false);
        Table.nativeSetLong(j3, batteryUsageColumnInfo.screenOnIndex, j4, (long) batteryUsageRealmProxyInterface2.realmGet$screenOn(), false);
        String realmGet$triggeredBy = batteryUsageRealmProxyInterface2.realmGet$triggeredBy();
        if (realmGet$triggeredBy != null) {
            Table.nativeSetString(nativePtr, batteryUsageColumnInfo.triggeredByIndex, j2, realmGet$triggeredBy, false);
        } else {
            Table.nativeSetNull(nativePtr, batteryUsageColumnInfo.triggeredByIndex, j2, false);
        }
        BatteryDetails realmGet$details = batteryUsageRealmProxyInterface2.realmGet$details();
        if (realmGet$details != null) {
            Long l = (Long) map2.get(realmGet$details);
            if (l == null) {
                l = Long.valueOf(BatteryDetailsRealmProxy.insertOrUpdate(realm2, realmGet$details, map2));
            }
            Table.nativeSetLink(nativePtr, batteryUsageColumnInfo.detailsIndex, j2, l.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, batteryUsageColumnInfo.detailsIndex, j2);
        }
        return j2;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(BatteryUsage.class);
        long nativePtr = table.getNativePtr();
        BatteryUsageColumnInfo batteryUsageColumnInfo = (BatteryUsageColumnInfo) realm.getSchema().getColumnInfo(BatteryUsage.class);
        long j2 = batteryUsageColumnInfo.idIndex;
        while (it.hasNext()) {
            BatteryUsage batteryUsage = (BatteryUsage) it.next();
            if (!map2.containsKey(batteryUsage)) {
                if (batteryUsage instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryUsage;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(batteryUsage, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage;
                if (Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id()) != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j2, (long) batteryUsageRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j2, Integer.valueOf(batteryUsageRealmProxyInterface.realmGet$id()));
                }
                long j3 = j;
                map2.put(batteryUsage, Long.valueOf(j3));
                long j4 = j3;
                long j5 = j2;
                Table.nativeSetLong(nativePtr, batteryUsageColumnInfo.timestampIndex, j3, batteryUsageRealmProxyInterface.realmGet$timestamp(), false);
                String realmGet$state = batteryUsageRealmProxyInterface.realmGet$state();
                if (realmGet$state != null) {
                    Table.nativeSetString(nativePtr, batteryUsageColumnInfo.stateIndex, j4, realmGet$state, false);
                } else {
                    Table.nativeSetNull(nativePtr, batteryUsageColumnInfo.stateIndex, j4, false);
                }
                long j6 = j4;
                Table.nativeSetFloat(nativePtr, batteryUsageColumnInfo.levelIndex, j6, batteryUsageRealmProxyInterface.realmGet$level(), false);
                Table.nativeSetLong(nativePtr, batteryUsageColumnInfo.screenOnIndex, j6, (long) batteryUsageRealmProxyInterface.realmGet$screenOn(), false);
                String realmGet$triggeredBy = batteryUsageRealmProxyInterface.realmGet$triggeredBy();
                if (realmGet$triggeredBy != null) {
                    Table.nativeSetString(nativePtr, batteryUsageColumnInfo.triggeredByIndex, j4, realmGet$triggeredBy, false);
                } else {
                    Table.nativeSetNull(nativePtr, batteryUsageColumnInfo.triggeredByIndex, j4, false);
                }
                BatteryDetails realmGet$details = batteryUsageRealmProxyInterface.realmGet$details();
                if (realmGet$details != null) {
                    Long l = (Long) map2.get(realmGet$details);
                    if (l == null) {
                        l = Long.valueOf(BatteryDetailsRealmProxy.insertOrUpdate(realm2, realmGet$details, map2));
                    }
                    Table.nativeSetLink(nativePtr, batteryUsageColumnInfo.detailsIndex, j4, l.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, batteryUsageColumnInfo.detailsIndex, j4);
                }
                j2 = j5;
            }
        }
    }

    public static BatteryUsage createDetachedCopy(BatteryUsage batteryUsage, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        BatteryUsage batteryUsage2;
        if (i > i2 || batteryUsage == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(batteryUsage);
        if (cacheData == null) {
            batteryUsage2 = new BatteryUsage();
            map.put(batteryUsage, new CacheData(i, batteryUsage2));
        } else if (i >= cacheData.minDepth) {
            return (BatteryUsage) cacheData.object;
        } else {
            BatteryUsage batteryUsage3 = (BatteryUsage) cacheData.object;
            cacheData.minDepth = i;
            batteryUsage2 = batteryUsage3;
        }
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage2;
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface2 = batteryUsage;
        batteryUsageRealmProxyInterface.realmSet$id(batteryUsageRealmProxyInterface2.realmGet$id());
        batteryUsageRealmProxyInterface.realmSet$timestamp(batteryUsageRealmProxyInterface2.realmGet$timestamp());
        batteryUsageRealmProxyInterface.realmSet$state(batteryUsageRealmProxyInterface2.realmGet$state());
        batteryUsageRealmProxyInterface.realmSet$level(batteryUsageRealmProxyInterface2.realmGet$level());
        batteryUsageRealmProxyInterface.realmSet$screenOn(batteryUsageRealmProxyInterface2.realmGet$screenOn());
        batteryUsageRealmProxyInterface.realmSet$triggeredBy(batteryUsageRealmProxyInterface2.realmGet$triggeredBy());
        batteryUsageRealmProxyInterface.realmSet$details(BatteryDetailsRealmProxy.createDetachedCopy(batteryUsageRealmProxyInterface2.realmGet$details(), i + 1, i2, map));
        return batteryUsage2;
    }

    static BatteryUsage update(Realm realm, BatteryUsage batteryUsage, BatteryUsage batteryUsage2, Map<RealmModel, RealmObjectProxy> map) {
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface = batteryUsage;
        BatteryUsageRealmProxyInterface batteryUsageRealmProxyInterface2 = batteryUsage2;
        batteryUsageRealmProxyInterface.realmSet$timestamp(batteryUsageRealmProxyInterface2.realmGet$timestamp());
        batteryUsageRealmProxyInterface.realmSet$state(batteryUsageRealmProxyInterface2.realmGet$state());
        batteryUsageRealmProxyInterface.realmSet$level(batteryUsageRealmProxyInterface2.realmGet$level());
        batteryUsageRealmProxyInterface.realmSet$screenOn(batteryUsageRealmProxyInterface2.realmGet$screenOn());
        batteryUsageRealmProxyInterface.realmSet$triggeredBy(batteryUsageRealmProxyInterface2.realmGet$triggeredBy());
        BatteryDetails realmGet$details = batteryUsageRealmProxyInterface2.realmGet$details();
        if (realmGet$details == null) {
            batteryUsageRealmProxyInterface.realmSet$details(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$details);
            if (batteryDetails != null) {
                batteryUsageRealmProxyInterface.realmSet$details(batteryDetails);
            } else {
                batteryUsageRealmProxyInterface.realmSet$details(BatteryDetailsRealmProxy.copyOrUpdate(realm, realmGet$details, true, map));
            }
        }
        return batteryUsage;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("BatteryUsage = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{timestamp:");
        sb.append(realmGet$timestamp());
        sb.append("}");
        sb.append(",");
        sb.append("{state:");
        sb.append(realmGet$state() != null ? realmGet$state() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{level:");
        sb.append(realmGet$level());
        sb.append("}");
        sb.append(",");
        sb.append("{screenOn:");
        sb.append(realmGet$screenOn());
        sb.append("}");
        sb.append(",");
        sb.append("{triggeredBy:");
        sb.append(realmGet$triggeredBy() != null ? realmGet$triggeredBy() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{details:");
        sb.append(realmGet$details() != null ? "BatteryDetails" : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BatteryUsageRealmProxy batteryUsageRealmProxy = (BatteryUsageRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = batteryUsageRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = batteryUsageRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == batteryUsageRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
