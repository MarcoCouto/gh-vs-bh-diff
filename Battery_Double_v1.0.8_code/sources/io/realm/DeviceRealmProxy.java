package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.Device;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class DeviceRealmProxy extends Device implements RealmObjectProxy, DeviceRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private DeviceColumnInfo columnInfo;
    private ProxyState<Device> proxyState;

    static final class DeviceColumnInfo extends ColumnInfo {
        long brandIndex;
        long isRootIndex;
        long kernelVersionIndex;
        long manufacturerIndex;
        long modelIndex;
        long osVersionIndex;
        long productIndex;
        long uuIdIndex;

        DeviceColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(8);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("Device");
            this.uuIdIndex = addColumnDetails("uuId", objectSchemaInfo);
            this.modelIndex = addColumnDetails("model", objectSchemaInfo);
            this.manufacturerIndex = addColumnDetails("manufacturer", objectSchemaInfo);
            this.brandIndex = addColumnDetails("brand", objectSchemaInfo);
            this.productIndex = addColumnDetails("product", objectSchemaInfo);
            this.osVersionIndex = addColumnDetails("osVersion", objectSchemaInfo);
            this.kernelVersionIndex = addColumnDetails("kernelVersion", objectSchemaInfo);
            this.isRootIndex = addColumnDetails("isRoot", objectSchemaInfo);
        }

        DeviceColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new DeviceColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            DeviceColumnInfo deviceColumnInfo = (DeviceColumnInfo) columnInfo;
            DeviceColumnInfo deviceColumnInfo2 = (DeviceColumnInfo) columnInfo2;
            deviceColumnInfo2.uuIdIndex = deviceColumnInfo.uuIdIndex;
            deviceColumnInfo2.modelIndex = deviceColumnInfo.modelIndex;
            deviceColumnInfo2.manufacturerIndex = deviceColumnInfo.manufacturerIndex;
            deviceColumnInfo2.brandIndex = deviceColumnInfo.brandIndex;
            deviceColumnInfo2.productIndex = deviceColumnInfo.productIndex;
            deviceColumnInfo2.osVersionIndex = deviceColumnInfo.osVersionIndex;
            deviceColumnInfo2.kernelVersionIndex = deviceColumnInfo.kernelVersionIndex;
            deviceColumnInfo2.isRootIndex = deviceColumnInfo.isRootIndex;
        }
    }

    public static String getTableName() {
        return "class_Device";
    }

    static {
        ArrayList arrayList = new ArrayList(8);
        arrayList.add("uuId");
        arrayList.add("model");
        arrayList.add("manufacturer");
        arrayList.add("brand");
        arrayList.add("product");
        arrayList.add("osVersion");
        arrayList.add("kernelVersion");
        arrayList.add("isRoot");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    DeviceRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (DeviceColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$uuId() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.uuIdIndex);
    }

    public void realmSet$uuId(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.uuIdIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.uuIdIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.uuIdIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.uuIdIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$model() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.modelIndex);
    }

    public void realmSet$model(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.modelIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.modelIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.modelIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.modelIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$manufacturer() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.manufacturerIndex);
    }

    public void realmSet$manufacturer(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.manufacturerIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.manufacturerIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.manufacturerIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.manufacturerIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$brand() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.brandIndex);
    }

    public void realmSet$brand(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.brandIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.brandIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.brandIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.brandIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$product() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.productIndex);
    }

    public void realmSet$product(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.productIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.productIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.productIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.productIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$osVersion() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.osVersionIndex);
    }

    public void realmSet$osVersion(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.osVersionIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.osVersionIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.osVersionIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.osVersionIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$kernelVersion() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.kernelVersionIndex);
    }

    public void realmSet$kernelVersion(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.kernelVersionIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.kernelVersionIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.kernelVersionIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.kernelVersionIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public int realmGet$isRoot() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.isRootIndex);
    }

    public void realmSet$isRoot(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.isRootIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.isRootIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("Device", 8, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("uuId", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("model", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("manufacturer", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("brand", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("product", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("osVersion", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("kernelVersion", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("isRoot", RealmFieldType.INTEGER, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static DeviceColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new DeviceColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Device createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        Device device = (Device) realm.createObjectInternal(Device.class, true, Collections.emptyList());
        DeviceRealmProxyInterface deviceRealmProxyInterface = device;
        if (jSONObject.has("uuId")) {
            if (jSONObject.isNull("uuId")) {
                deviceRealmProxyInterface.realmSet$uuId(null);
            } else {
                deviceRealmProxyInterface.realmSet$uuId(jSONObject.getString("uuId"));
            }
        }
        if (jSONObject.has("model")) {
            if (jSONObject.isNull("model")) {
                deviceRealmProxyInterface.realmSet$model(null);
            } else {
                deviceRealmProxyInterface.realmSet$model(jSONObject.getString("model"));
            }
        }
        if (jSONObject.has("manufacturer")) {
            if (jSONObject.isNull("manufacturer")) {
                deviceRealmProxyInterface.realmSet$manufacturer(null);
            } else {
                deviceRealmProxyInterface.realmSet$manufacturer(jSONObject.getString("manufacturer"));
            }
        }
        if (jSONObject.has("brand")) {
            if (jSONObject.isNull("brand")) {
                deviceRealmProxyInterface.realmSet$brand(null);
            } else {
                deviceRealmProxyInterface.realmSet$brand(jSONObject.getString("brand"));
            }
        }
        if (jSONObject.has("product")) {
            if (jSONObject.isNull("product")) {
                deviceRealmProxyInterface.realmSet$product(null);
            } else {
                deviceRealmProxyInterface.realmSet$product(jSONObject.getString("product"));
            }
        }
        if (jSONObject.has("osVersion")) {
            if (jSONObject.isNull("osVersion")) {
                deviceRealmProxyInterface.realmSet$osVersion(null);
            } else {
                deviceRealmProxyInterface.realmSet$osVersion(jSONObject.getString("osVersion"));
            }
        }
        if (jSONObject.has("kernelVersion")) {
            if (jSONObject.isNull("kernelVersion")) {
                deviceRealmProxyInterface.realmSet$kernelVersion(null);
            } else {
                deviceRealmProxyInterface.realmSet$kernelVersion(jSONObject.getString("kernelVersion"));
            }
        }
        if (jSONObject.has("isRoot")) {
            if (jSONObject.isNull("isRoot")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isRoot' to null.");
            }
            deviceRealmProxyInterface.realmSet$isRoot(jSONObject.getInt("isRoot"));
        }
        return device;
    }

    @TargetApi(11)
    public static Device createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        Device device = new Device();
        DeviceRealmProxyInterface deviceRealmProxyInterface = device;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("uuId")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$uuId(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$uuId(null);
                }
            } else if (nextName.equals("model")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$model(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$model(null);
                }
            } else if (nextName.equals("manufacturer")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$manufacturer(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$manufacturer(null);
                }
            } else if (nextName.equals("brand")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$brand(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$brand(null);
                }
            } else if (nextName.equals("product")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$product(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$product(null);
                }
            } else if (nextName.equals("osVersion")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$osVersion(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$osVersion(null);
                }
            } else if (nextName.equals("kernelVersion")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    deviceRealmProxyInterface.realmSet$kernelVersion(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    deviceRealmProxyInterface.realmSet$kernelVersion(null);
                }
            } else if (!nextName.equals("isRoot")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                deviceRealmProxyInterface.realmSet$isRoot(jsonReader.nextInt());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'isRoot' to null.");
            }
        }
        jsonReader.endObject();
        return (Device) realm.copyToRealm(device);
    }

    public static Device copyOrUpdate(Realm realm, Device device, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (device instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) device;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return device;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(device);
        if (realmObjectProxy2 != null) {
            return (Device) realmObjectProxy2;
        }
        return copy(realm, device, z, map);
    }

    public static Device copy(Realm realm, Device device, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(device);
        if (realmObjectProxy != null) {
            return (Device) realmObjectProxy;
        }
        Device device2 = (Device) realm.createObjectInternal(Device.class, false, Collections.emptyList());
        map.put(device, (RealmObjectProxy) device2);
        DeviceRealmProxyInterface deviceRealmProxyInterface = device;
        DeviceRealmProxyInterface deviceRealmProxyInterface2 = device2;
        deviceRealmProxyInterface2.realmSet$uuId(deviceRealmProxyInterface.realmGet$uuId());
        deviceRealmProxyInterface2.realmSet$model(deviceRealmProxyInterface.realmGet$model());
        deviceRealmProxyInterface2.realmSet$manufacturer(deviceRealmProxyInterface.realmGet$manufacturer());
        deviceRealmProxyInterface2.realmSet$brand(deviceRealmProxyInterface.realmGet$brand());
        deviceRealmProxyInterface2.realmSet$product(deviceRealmProxyInterface.realmGet$product());
        deviceRealmProxyInterface2.realmSet$osVersion(deviceRealmProxyInterface.realmGet$osVersion());
        deviceRealmProxyInterface2.realmSet$kernelVersion(deviceRealmProxyInterface.realmGet$kernelVersion());
        deviceRealmProxyInterface2.realmSet$isRoot(deviceRealmProxyInterface.realmGet$isRoot());
        return device2;
    }

    public static long insert(Realm realm, Device device, Map<RealmModel, Long> map) {
        if (device instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) device;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Device.class);
        long nativePtr = table.getNativePtr();
        DeviceColumnInfo deviceColumnInfo = (DeviceColumnInfo) realm.getSchema().getColumnInfo(Device.class);
        long createRow = OsObject.createRow(table);
        map.put(device, Long.valueOf(createRow));
        DeviceRealmProxyInterface deviceRealmProxyInterface = device;
        String realmGet$uuId = deviceRealmProxyInterface.realmGet$uuId();
        if (realmGet$uuId != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.uuIdIndex, createRow, realmGet$uuId, false);
        }
        String realmGet$model = deviceRealmProxyInterface.realmGet$model();
        if (realmGet$model != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.modelIndex, createRow, realmGet$model, false);
        }
        String realmGet$manufacturer = deviceRealmProxyInterface.realmGet$manufacturer();
        if (realmGet$manufacturer != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.manufacturerIndex, createRow, realmGet$manufacturer, false);
        }
        String realmGet$brand = deviceRealmProxyInterface.realmGet$brand();
        if (realmGet$brand != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.brandIndex, createRow, realmGet$brand, false);
        }
        String realmGet$product = deviceRealmProxyInterface.realmGet$product();
        if (realmGet$product != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.productIndex, createRow, realmGet$product, false);
        }
        String realmGet$osVersion = deviceRealmProxyInterface.realmGet$osVersion();
        if (realmGet$osVersion != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.osVersionIndex, createRow, realmGet$osVersion, false);
        }
        String realmGet$kernelVersion = deviceRealmProxyInterface.realmGet$kernelVersion();
        if (realmGet$kernelVersion != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.kernelVersionIndex, createRow, realmGet$kernelVersion, false);
        }
        Table.nativeSetLong(nativePtr, deviceColumnInfo.isRootIndex, createRow, (long) deviceRealmProxyInterface.realmGet$isRoot(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Device.class);
        long nativePtr = table.getNativePtr();
        DeviceColumnInfo deviceColumnInfo = (DeviceColumnInfo) realm.getSchema().getColumnInfo(Device.class);
        while (it.hasNext()) {
            Device device = (Device) it.next();
            if (!map2.containsKey(device)) {
                if (device instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) device;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(device, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(device, Long.valueOf(createRow));
                DeviceRealmProxyInterface deviceRealmProxyInterface = device;
                String realmGet$uuId = deviceRealmProxyInterface.realmGet$uuId();
                if (realmGet$uuId != null) {
                    j = createRow;
                    Table.nativeSetString(nativePtr, deviceColumnInfo.uuIdIndex, createRow, realmGet$uuId, false);
                } else {
                    j = createRow;
                }
                String realmGet$model = deviceRealmProxyInterface.realmGet$model();
                if (realmGet$model != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.modelIndex, j, realmGet$model, false);
                }
                String realmGet$manufacturer = deviceRealmProxyInterface.realmGet$manufacturer();
                if (realmGet$manufacturer != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.manufacturerIndex, j, realmGet$manufacturer, false);
                }
                String realmGet$brand = deviceRealmProxyInterface.realmGet$brand();
                if (realmGet$brand != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.brandIndex, j, realmGet$brand, false);
                }
                String realmGet$product = deviceRealmProxyInterface.realmGet$product();
                if (realmGet$product != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.productIndex, j, realmGet$product, false);
                }
                String realmGet$osVersion = deviceRealmProxyInterface.realmGet$osVersion();
                if (realmGet$osVersion != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.osVersionIndex, j, realmGet$osVersion, false);
                }
                String realmGet$kernelVersion = deviceRealmProxyInterface.realmGet$kernelVersion();
                if (realmGet$kernelVersion != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.kernelVersionIndex, j, realmGet$kernelVersion, false);
                }
                Table.nativeSetLong(nativePtr, deviceColumnInfo.isRootIndex, j, (long) deviceRealmProxyInterface.realmGet$isRoot(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, Device device, Map<RealmModel, Long> map) {
        if (device instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) device;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Device.class);
        long nativePtr = table.getNativePtr();
        DeviceColumnInfo deviceColumnInfo = (DeviceColumnInfo) realm.getSchema().getColumnInfo(Device.class);
        long createRow = OsObject.createRow(table);
        map.put(device, Long.valueOf(createRow));
        DeviceRealmProxyInterface deviceRealmProxyInterface = device;
        String realmGet$uuId = deviceRealmProxyInterface.realmGet$uuId();
        if (realmGet$uuId != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.uuIdIndex, createRow, realmGet$uuId, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.uuIdIndex, createRow, false);
        }
        String realmGet$model = deviceRealmProxyInterface.realmGet$model();
        if (realmGet$model != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.modelIndex, createRow, realmGet$model, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.modelIndex, createRow, false);
        }
        String realmGet$manufacturer = deviceRealmProxyInterface.realmGet$manufacturer();
        if (realmGet$manufacturer != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.manufacturerIndex, createRow, realmGet$manufacturer, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.manufacturerIndex, createRow, false);
        }
        String realmGet$brand = deviceRealmProxyInterface.realmGet$brand();
        if (realmGet$brand != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.brandIndex, createRow, realmGet$brand, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.brandIndex, createRow, false);
        }
        String realmGet$product = deviceRealmProxyInterface.realmGet$product();
        if (realmGet$product != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.productIndex, createRow, realmGet$product, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.productIndex, createRow, false);
        }
        String realmGet$osVersion = deviceRealmProxyInterface.realmGet$osVersion();
        if (realmGet$osVersion != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.osVersionIndex, createRow, realmGet$osVersion, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.osVersionIndex, createRow, false);
        }
        String realmGet$kernelVersion = deviceRealmProxyInterface.realmGet$kernelVersion();
        if (realmGet$kernelVersion != null) {
            Table.nativeSetString(nativePtr, deviceColumnInfo.kernelVersionIndex, createRow, realmGet$kernelVersion, false);
        } else {
            Table.nativeSetNull(nativePtr, deviceColumnInfo.kernelVersionIndex, createRow, false);
        }
        Table.nativeSetLong(nativePtr, deviceColumnInfo.isRootIndex, createRow, (long) deviceRealmProxyInterface.realmGet$isRoot(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Device.class);
        long nativePtr = table.getNativePtr();
        DeviceColumnInfo deviceColumnInfo = (DeviceColumnInfo) realm.getSchema().getColumnInfo(Device.class);
        while (it.hasNext()) {
            Device device = (Device) it.next();
            if (!map2.containsKey(device)) {
                if (device instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) device;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(device, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(device, Long.valueOf(createRow));
                DeviceRealmProxyInterface deviceRealmProxyInterface = device;
                String realmGet$uuId = deviceRealmProxyInterface.realmGet$uuId();
                if (realmGet$uuId != null) {
                    j = createRow;
                    Table.nativeSetString(nativePtr, deviceColumnInfo.uuIdIndex, createRow, realmGet$uuId, false);
                } else {
                    j = createRow;
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.uuIdIndex, j, false);
                }
                String realmGet$model = deviceRealmProxyInterface.realmGet$model();
                if (realmGet$model != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.modelIndex, j, realmGet$model, false);
                } else {
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.modelIndex, j, false);
                }
                String realmGet$manufacturer = deviceRealmProxyInterface.realmGet$manufacturer();
                if (realmGet$manufacturer != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.manufacturerIndex, j, realmGet$manufacturer, false);
                } else {
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.manufacturerIndex, j, false);
                }
                String realmGet$brand = deviceRealmProxyInterface.realmGet$brand();
                if (realmGet$brand != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.brandIndex, j, realmGet$brand, false);
                } else {
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.brandIndex, j, false);
                }
                String realmGet$product = deviceRealmProxyInterface.realmGet$product();
                if (realmGet$product != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.productIndex, j, realmGet$product, false);
                } else {
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.productIndex, j, false);
                }
                String realmGet$osVersion = deviceRealmProxyInterface.realmGet$osVersion();
                if (realmGet$osVersion != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.osVersionIndex, j, realmGet$osVersion, false);
                } else {
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.osVersionIndex, j, false);
                }
                String realmGet$kernelVersion = deviceRealmProxyInterface.realmGet$kernelVersion();
                if (realmGet$kernelVersion != null) {
                    Table.nativeSetString(nativePtr, deviceColumnInfo.kernelVersionIndex, j, realmGet$kernelVersion, false);
                } else {
                    Table.nativeSetNull(nativePtr, deviceColumnInfo.kernelVersionIndex, j, false);
                }
                Table.nativeSetLong(nativePtr, deviceColumnInfo.isRootIndex, j, (long) deviceRealmProxyInterface.realmGet$isRoot(), false);
            }
        }
    }

    public static Device createDetachedCopy(Device device, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        Device device2;
        if (i > i2 || device == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(device);
        if (cacheData == null) {
            device2 = new Device();
            map.put(device, new CacheData(i, device2));
        } else if (i >= cacheData.minDepth) {
            return (Device) cacheData.object;
        } else {
            Device device3 = (Device) cacheData.object;
            cacheData.minDepth = i;
            device2 = device3;
        }
        DeviceRealmProxyInterface deviceRealmProxyInterface = device2;
        DeviceRealmProxyInterface deviceRealmProxyInterface2 = device;
        deviceRealmProxyInterface.realmSet$uuId(deviceRealmProxyInterface2.realmGet$uuId());
        deviceRealmProxyInterface.realmSet$model(deviceRealmProxyInterface2.realmGet$model());
        deviceRealmProxyInterface.realmSet$manufacturer(deviceRealmProxyInterface2.realmGet$manufacturer());
        deviceRealmProxyInterface.realmSet$brand(deviceRealmProxyInterface2.realmGet$brand());
        deviceRealmProxyInterface.realmSet$product(deviceRealmProxyInterface2.realmGet$product());
        deviceRealmProxyInterface.realmSet$osVersion(deviceRealmProxyInterface2.realmGet$osVersion());
        deviceRealmProxyInterface.realmSet$kernelVersion(deviceRealmProxyInterface2.realmGet$kernelVersion());
        deviceRealmProxyInterface.realmSet$isRoot(deviceRealmProxyInterface2.realmGet$isRoot());
        return device2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Device = proxy[");
        sb.append("{uuId:");
        sb.append(realmGet$uuId() != null ? realmGet$uuId() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{model:");
        sb.append(realmGet$model() != null ? realmGet$model() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{manufacturer:");
        sb.append(realmGet$manufacturer() != null ? realmGet$manufacturer() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{brand:");
        sb.append(realmGet$brand() != null ? realmGet$brand() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{product:");
        sb.append(realmGet$product() != null ? realmGet$product() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{osVersion:");
        sb.append(realmGet$osVersion() != null ? realmGet$osVersion() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{kernelVersion:");
        sb.append(realmGet$kernelVersion() != null ? realmGet$kernelVersion() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{isRoot:");
        sb.append(realmGet$isRoot());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DeviceRealmProxy deviceRealmProxy = (DeviceRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = deviceRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = deviceRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == deviceRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
