package io.realm;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.realm.internal.Collection;
import io.realm.internal.Collection.Aggregate;
import io.realm.internal.Collection.Iterator;
import io.realm.internal.Collection.ListIterator;
import io.realm.internal.InvalidRow;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.SortDescriptor;
import io.realm.internal.Table;
import io.realm.internal.UncheckedRow;
import io.realm.internal.fields.FieldDescriptor.SchemaProxy;
import java.util.AbstractList;
import java.util.Date;
import java.util.Locale;
import javax.annotation.Nullable;

abstract class OrderedRealmCollectionImpl<E> extends AbstractList<E> implements OrderedRealmCollection<E> {
    private static final String NOT_SUPPORTED_MESSAGE = "This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.";
    @Nullable
    final String className;
    @Nullable
    final Class<E> classSpec;
    final Collection collection;
    @SuppressFBWarnings({"SS_SHOULD_BE_STATIC"})
    final boolean forValues;
    final BaseRealm realm;

    private class RealmCollectionIterator extends Iterator<E> {
        RealmCollectionIterator() {
            super(OrderedRealmCollectionImpl.this.collection);
        }

        /* access modifiers changed from: protected */
        public E convertRowToObject(UncheckedRow uncheckedRow) {
            return OrderedRealmCollectionImpl.this.realm.get(OrderedRealmCollectionImpl.this.classSpec, OrderedRealmCollectionImpl.this.className, uncheckedRow);
        }
    }

    private class RealmCollectionListIterator extends ListIterator<E> {
        RealmCollectionListIterator(int i) {
            super(OrderedRealmCollectionImpl.this.collection, i);
        }

        /* access modifiers changed from: protected */
        public E convertRowToObject(UncheckedRow uncheckedRow) {
            return OrderedRealmCollectionImpl.this.realm.get(OrderedRealmCollectionImpl.this.classSpec, OrderedRealmCollectionImpl.this.className, uncheckedRow);
        }
    }

    public boolean isManaged() {
        return true;
    }

    OrderedRealmCollectionImpl(BaseRealm baseRealm, Collection collection2, Class<E> cls) {
        this(baseRealm, collection2, cls, null);
    }

    OrderedRealmCollectionImpl(BaseRealm baseRealm, Collection collection2, String str) {
        this(baseRealm, collection2, null, str);
    }

    private OrderedRealmCollectionImpl(BaseRealm baseRealm, Collection collection2, @Nullable Class<E> cls, @Nullable String str) {
        this.forValues = false;
        this.realm = baseRealm;
        this.collection = collection2;
        this.classSpec = cls;
        this.className = str;
    }

    /* access modifiers changed from: 0000 */
    public Table getTable() {
        return this.collection.getTable();
    }

    /* access modifiers changed from: 0000 */
    public Collection getCollection() {
        return this.collection;
    }

    public boolean isValid() {
        return this.collection.isValid();
    }

    public boolean contains(@Nullable Object obj) {
        if (!isLoaded() || ((obj instanceof RealmObjectProxy) && ((RealmObjectProxy) obj).realmGet$proxyState().getRow$realm() == InvalidRow.INSTANCE)) {
            return false;
        }
        java.util.Iterator it = iterator();
        while (it.hasNext()) {
            if (it.next().equals(obj)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    public E get(int i) {
        this.realm.checkIfValid();
        return this.realm.get(this.classSpec, this.className, this.collection.getUncheckedRow(i));
    }

    @Nullable
    public E first() {
        return firstImpl(true, null);
    }

    @Nullable
    public E first(@Nullable E e) {
        return firstImpl(false, e);
    }

    @Nullable
    private E firstImpl(boolean z, @Nullable E e) {
        UncheckedRow firstUncheckedRow = this.collection.firstUncheckedRow();
        if (firstUncheckedRow != null) {
            return this.realm.get(this.classSpec, this.className, firstUncheckedRow);
        }
        if (!z) {
            return e;
        }
        throw new IndexOutOfBoundsException("No results were found.");
    }

    @Nullable
    public E last() {
        return lastImpl(true, null);
    }

    @Nullable
    public E last(@Nullable E e) {
        return lastImpl(false, e);
    }

    @Nullable
    private E lastImpl(boolean z, @Nullable E e) {
        UncheckedRow lastUncheckedRow = this.collection.lastUncheckedRow();
        if (lastUncheckedRow != null) {
            return this.realm.get(this.classSpec, this.className, lastUncheckedRow);
        }
        if (!z) {
            return e;
        }
        throw new IndexOutOfBoundsException("No results were found.");
    }

    public void deleteFromRealm(int i) {
        this.realm.checkIfValidAndInTransaction();
        this.collection.delete((long) i);
    }

    public boolean deleteAllFromRealm() {
        this.realm.checkIfValid();
        if (size() <= 0) {
            return false;
        }
        this.collection.clear();
        return true;
    }

    public java.util.Iterator<E> iterator() {
        return new RealmCollectionIterator();
    }

    public java.util.ListIterator<E> listIterator() {
        return new RealmCollectionListIterator(0);
    }

    public java.util.ListIterator<E> listIterator(int i) {
        return new RealmCollectionListIterator(i);
    }

    private long getColumnIndexForSort(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Non-empty field name required.");
        } else if (str.contains(".")) {
            StringBuilder sb = new StringBuilder();
            sb.append("Aggregates on child object fields are not supported: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        } else {
            long columnIndex = this.collection.getTable().getColumnIndex(str);
            if (columnIndex >= 0) {
                return columnIndex;
            }
            throw new IllegalArgumentException(String.format(Locale.US, "Field '%s' does not exist.", new Object[]{str}));
        }
    }

    public RealmResults<E> sort(String str) {
        return createLoadedResults(this.collection.sort(SortDescriptor.getInstanceForSort((SchemaProxy) getSchemaConnector(), this.collection.getTable(), str, Sort.ASCENDING)));
    }

    public RealmResults<E> sort(String str, Sort sort) {
        return createLoadedResults(this.collection.sort(SortDescriptor.getInstanceForSort((SchemaProxy) getSchemaConnector(), this.collection.getTable(), str, sort)));
    }

    public RealmResults<E> sort(String[] strArr, Sort[] sortArr) {
        return createLoadedResults(this.collection.sort(SortDescriptor.getInstanceForSort((SchemaProxy) getSchemaConnector(), this.collection.getTable(), strArr, sortArr)));
    }

    public RealmResults<E> sort(String str, Sort sort, String str2, Sort sort2) {
        return sort(new String[]{str, str2}, new Sort[]{sort, sort2});
    }

    public int size() {
        if (!isLoaded()) {
            return 0;
        }
        long size = this.collection.size();
        return size > 2147483647L ? Integer.MAX_VALUE : (int) size;
    }

    public Number min(String str) {
        this.realm.checkIfValid();
        return this.collection.aggregateNumber(Aggregate.MINIMUM, getColumnIndexForSort(str));
    }

    public Date minDate(String str) {
        this.realm.checkIfValid();
        return this.collection.aggregateDate(Aggregate.MINIMUM, getColumnIndexForSort(str));
    }

    public Number max(String str) {
        this.realm.checkIfValid();
        return this.collection.aggregateNumber(Aggregate.MAXIMUM, getColumnIndexForSort(str));
    }

    @Nullable
    public Date maxDate(String str) {
        this.realm.checkIfValid();
        return this.collection.aggregateDate(Aggregate.MAXIMUM, getColumnIndexForSort(str));
    }

    public Number sum(String str) {
        this.realm.checkIfValid();
        return this.collection.aggregateNumber(Aggregate.SUM, getColumnIndexForSort(str));
    }

    public double average(String str) {
        this.realm.checkIfValid();
        return this.collection.aggregateNumber(Aggregate.AVERAGE, getColumnIndexForSort(str)).doubleValue();
    }

    @Deprecated
    public E remove(int i) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public boolean removeAll(java.util.Collection<?> collection2) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public E set(int i, E e) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public boolean retainAll(java.util.Collection<?> collection2) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    public boolean deleteLastFromRealm() {
        this.realm.checkIfValidAndInTransaction();
        return this.collection.deleteLast();
    }

    public boolean deleteFirstFromRealm() {
        this.realm.checkIfValidAndInTransaction();
        return this.collection.deleteFirst();
    }

    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public boolean add(E e) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public void add(int i, E e) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public boolean addAll(int i, java.util.Collection<? extends E> collection2) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    @Deprecated
    public boolean addAll(java.util.Collection<? extends E> collection2) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
    }

    public OrderedRealmCollectionSnapshot<E> createSnapshot() {
        if (this.className != null) {
            return new OrderedRealmCollectionSnapshot<>(this.realm, this.collection, this.className);
        }
        return new OrderedRealmCollectionSnapshot<>(this.realm, this.collection, this.classSpec);
    }

    /* access modifiers changed from: 0000 */
    public RealmResults<E> createLoadedResults(Collection collection2) {
        RealmResults<E> realmResults;
        if (this.className != null) {
            realmResults = new RealmResults<>(this.realm, collection2, this.className);
        } else {
            realmResults = new RealmResults<>(this.realm, collection2, this.classSpec);
        }
        realmResults.load();
        return realmResults;
    }

    private SchemaConnector getSchemaConnector() {
        return new SchemaConnector(this.realm.getSchema());
    }
}
