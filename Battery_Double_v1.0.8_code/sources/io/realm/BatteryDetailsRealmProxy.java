package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.BatteryDetails;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class BatteryDetailsRealmProxy extends BatteryDetails implements RealmObjectProxy, BatteryDetailsRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private BatteryDetailsColumnInfo columnInfo;
    private ProxyState<BatteryDetails> proxyState;

    static final class BatteryDetailsColumnInfo extends ColumnInfo {
        long capacityIndex;
        long chargeCounterIndex;
        long chargerIndex;
        long currentAverageIndex;
        long currentNowIndex;
        long energyCounterIndex;
        long healthIndex;
        long technologyIndex;
        long temperatureIndex;
        long voltageIndex;

        BatteryDetailsColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(10);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("BatteryDetails");
            this.chargerIndex = addColumnDetails("charger", objectSchemaInfo);
            this.healthIndex = addColumnDetails("health", objectSchemaInfo);
            this.voltageIndex = addColumnDetails("voltage", objectSchemaInfo);
            this.temperatureIndex = addColumnDetails("temperature", objectSchemaInfo);
            this.technologyIndex = addColumnDetails("technology", objectSchemaInfo);
            this.capacityIndex = addColumnDetails("capacity", objectSchemaInfo);
            this.chargeCounterIndex = addColumnDetails("chargeCounter", objectSchemaInfo);
            this.currentAverageIndex = addColumnDetails("currentAverage", objectSchemaInfo);
            this.currentNowIndex = addColumnDetails("currentNow", objectSchemaInfo);
            this.energyCounterIndex = addColumnDetails("energyCounter", objectSchemaInfo);
        }

        BatteryDetailsColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new BatteryDetailsColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            BatteryDetailsColumnInfo batteryDetailsColumnInfo = (BatteryDetailsColumnInfo) columnInfo;
            BatteryDetailsColumnInfo batteryDetailsColumnInfo2 = (BatteryDetailsColumnInfo) columnInfo2;
            batteryDetailsColumnInfo2.chargerIndex = batteryDetailsColumnInfo.chargerIndex;
            batteryDetailsColumnInfo2.healthIndex = batteryDetailsColumnInfo.healthIndex;
            batteryDetailsColumnInfo2.voltageIndex = batteryDetailsColumnInfo.voltageIndex;
            batteryDetailsColumnInfo2.temperatureIndex = batteryDetailsColumnInfo.temperatureIndex;
            batteryDetailsColumnInfo2.technologyIndex = batteryDetailsColumnInfo.technologyIndex;
            batteryDetailsColumnInfo2.capacityIndex = batteryDetailsColumnInfo.capacityIndex;
            batteryDetailsColumnInfo2.chargeCounterIndex = batteryDetailsColumnInfo.chargeCounterIndex;
            batteryDetailsColumnInfo2.currentAverageIndex = batteryDetailsColumnInfo.currentAverageIndex;
            batteryDetailsColumnInfo2.currentNowIndex = batteryDetailsColumnInfo.currentNowIndex;
            batteryDetailsColumnInfo2.energyCounterIndex = batteryDetailsColumnInfo.energyCounterIndex;
        }
    }

    public static String getTableName() {
        return "class_BatteryDetails";
    }

    static {
        ArrayList arrayList = new ArrayList(10);
        arrayList.add("charger");
        arrayList.add("health");
        arrayList.add("voltage");
        arrayList.add("temperature");
        arrayList.add("technology");
        arrayList.add("capacity");
        arrayList.add("chargeCounter");
        arrayList.add("currentAverage");
        arrayList.add("currentNow");
        arrayList.add("energyCounter");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    BatteryDetailsRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (BatteryDetailsColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$charger() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.chargerIndex);
    }

    public void realmSet$charger(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.chargerIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.chargerIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.chargerIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.chargerIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$health() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.healthIndex);
    }

    public void realmSet$health(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.healthIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.healthIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.healthIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.healthIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public double realmGet$voltage() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.voltageIndex);
    }

    public void realmSet$voltage(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.voltageIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.voltageIndex, row$realm.getIndex(), d, true);
        }
    }

    public double realmGet$temperature() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.temperatureIndex);
    }

    public void realmSet$temperature(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.temperatureIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.temperatureIndex, row$realm.getIndex(), d, true);
        }
    }

    public String realmGet$technology() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.technologyIndex);
    }

    public void realmSet$technology(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.technologyIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.technologyIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.technologyIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.technologyIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public int realmGet$capacity() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.capacityIndex);
    }

    public void realmSet$capacity(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.capacityIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.capacityIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$chargeCounter() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.chargeCounterIndex);
    }

    public void realmSet$chargeCounter(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.chargeCounterIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.chargeCounterIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$currentAverage() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.currentAverageIndex);
    }

    public void realmSet$currentAverage(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.currentAverageIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.currentAverageIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$currentNow() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.currentNowIndex);
    }

    public void realmSet$currentNow(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.currentNowIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.currentNowIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public long realmGet$energyCounter() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.energyCounterIndex);
    }

    public void realmSet$energyCounter(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.energyCounterIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.energyCounterIndex, row$realm.getIndex(), j, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("BatteryDetails", 10, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("charger", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("health", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("voltage", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("temperature", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("technology", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("capacity", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("chargeCounter", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("currentAverage", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("currentNow", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("energyCounter", RealmFieldType.INTEGER, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static BatteryDetailsColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new BatteryDetailsColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static BatteryDetails createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        BatteryDetails batteryDetails = (BatteryDetails) realm.createObjectInternal(BatteryDetails.class, true, Collections.emptyList());
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails;
        if (jSONObject.has("charger")) {
            if (jSONObject.isNull("charger")) {
                batteryDetailsRealmProxyInterface.realmSet$charger(null);
            } else {
                batteryDetailsRealmProxyInterface.realmSet$charger(jSONObject.getString("charger"));
            }
        }
        if (jSONObject.has("health")) {
            if (jSONObject.isNull("health")) {
                batteryDetailsRealmProxyInterface.realmSet$health(null);
            } else {
                batteryDetailsRealmProxyInterface.realmSet$health(jSONObject.getString("health"));
            }
        }
        if (jSONObject.has("voltage")) {
            if (jSONObject.isNull("voltage")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'voltage' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$voltage(jSONObject.getDouble("voltage"));
        }
        if (jSONObject.has("temperature")) {
            if (jSONObject.isNull("temperature")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'temperature' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$temperature(jSONObject.getDouble("temperature"));
        }
        if (jSONObject.has("technology")) {
            if (jSONObject.isNull("technology")) {
                batteryDetailsRealmProxyInterface.realmSet$technology(null);
            } else {
                batteryDetailsRealmProxyInterface.realmSet$technology(jSONObject.getString("technology"));
            }
        }
        if (jSONObject.has("capacity")) {
            if (jSONObject.isNull("capacity")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'capacity' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$capacity(jSONObject.getInt("capacity"));
        }
        if (jSONObject.has("chargeCounter")) {
            if (jSONObject.isNull("chargeCounter")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'chargeCounter' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$chargeCounter(jSONObject.getInt("chargeCounter"));
        }
        if (jSONObject.has("currentAverage")) {
            if (jSONObject.isNull("currentAverage")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'currentAverage' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$currentAverage(jSONObject.getInt("currentAverage"));
        }
        if (jSONObject.has("currentNow")) {
            if (jSONObject.isNull("currentNow")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'currentNow' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$currentNow(jSONObject.getInt("currentNow"));
        }
        if (jSONObject.has("energyCounter")) {
            if (jSONObject.isNull("energyCounter")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'energyCounter' to null.");
            }
            batteryDetailsRealmProxyInterface.realmSet$energyCounter(jSONObject.getLong("energyCounter"));
        }
        return batteryDetails;
    }

    @TargetApi(11)
    public static BatteryDetails createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        BatteryDetails batteryDetails = new BatteryDetails();
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("charger")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$charger(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    batteryDetailsRealmProxyInterface.realmSet$charger(null);
                }
            } else if (nextName.equals("health")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$health(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    batteryDetailsRealmProxyInterface.realmSet$health(null);
                }
            } else if (nextName.equals("voltage")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$voltage(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'voltage' to null.");
                }
            } else if (nextName.equals("temperature")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$temperature(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'temperature' to null.");
                }
            } else if (nextName.equals("technology")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$technology(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    batteryDetailsRealmProxyInterface.realmSet$technology(null);
                }
            } else if (nextName.equals("capacity")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$capacity(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'capacity' to null.");
                }
            } else if (nextName.equals("chargeCounter")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$chargeCounter(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'chargeCounter' to null.");
                }
            } else if (nextName.equals("currentAverage")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$currentAverage(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'currentAverage' to null.");
                }
            } else if (nextName.equals("currentNow")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batteryDetailsRealmProxyInterface.realmSet$currentNow(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'currentNow' to null.");
                }
            } else if (!nextName.equals("energyCounter")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                batteryDetailsRealmProxyInterface.realmSet$energyCounter(jsonReader.nextLong());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'energyCounter' to null.");
            }
        }
        jsonReader.endObject();
        return (BatteryDetails) realm.copyToRealm(batteryDetails);
    }

    public static BatteryDetails copyOrUpdate(Realm realm, BatteryDetails batteryDetails, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (batteryDetails instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryDetails;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return batteryDetails;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(batteryDetails);
        if (realmObjectProxy2 != null) {
            return (BatteryDetails) realmObjectProxy2;
        }
        return copy(realm, batteryDetails, z, map);
    }

    public static BatteryDetails copy(Realm realm, BatteryDetails batteryDetails, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(batteryDetails);
        if (realmObjectProxy != null) {
            return (BatteryDetails) realmObjectProxy;
        }
        BatteryDetails batteryDetails2 = (BatteryDetails) realm.createObjectInternal(BatteryDetails.class, false, Collections.emptyList());
        map.put(batteryDetails, (RealmObjectProxy) batteryDetails2);
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails;
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface2 = batteryDetails2;
        batteryDetailsRealmProxyInterface2.realmSet$charger(batteryDetailsRealmProxyInterface.realmGet$charger());
        batteryDetailsRealmProxyInterface2.realmSet$health(batteryDetailsRealmProxyInterface.realmGet$health());
        batteryDetailsRealmProxyInterface2.realmSet$voltage(batteryDetailsRealmProxyInterface.realmGet$voltage());
        batteryDetailsRealmProxyInterface2.realmSet$temperature(batteryDetailsRealmProxyInterface.realmGet$temperature());
        batteryDetailsRealmProxyInterface2.realmSet$technology(batteryDetailsRealmProxyInterface.realmGet$technology());
        batteryDetailsRealmProxyInterface2.realmSet$capacity(batteryDetailsRealmProxyInterface.realmGet$capacity());
        batteryDetailsRealmProxyInterface2.realmSet$chargeCounter(batteryDetailsRealmProxyInterface.realmGet$chargeCounter());
        batteryDetailsRealmProxyInterface2.realmSet$currentAverage(batteryDetailsRealmProxyInterface.realmGet$currentAverage());
        batteryDetailsRealmProxyInterface2.realmSet$currentNow(batteryDetailsRealmProxyInterface.realmGet$currentNow());
        batteryDetailsRealmProxyInterface2.realmSet$energyCounter(batteryDetailsRealmProxyInterface.realmGet$energyCounter());
        return batteryDetails2;
    }

    public static long insert(Realm realm, BatteryDetails batteryDetails, Map<RealmModel, Long> map) {
        BatteryDetails batteryDetails2 = batteryDetails;
        if (batteryDetails2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryDetails2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(BatteryDetails.class);
        long nativePtr = table.getNativePtr();
        BatteryDetailsColumnInfo batteryDetailsColumnInfo = (BatteryDetailsColumnInfo) realm.getSchema().getColumnInfo(BatteryDetails.class);
        long createRow = OsObject.createRow(table);
        map.put(batteryDetails2, Long.valueOf(createRow));
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails2;
        String realmGet$charger = batteryDetailsRealmProxyInterface.realmGet$charger();
        if (realmGet$charger != null) {
            Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.chargerIndex, createRow, realmGet$charger, false);
        }
        String realmGet$health = batteryDetailsRealmProxyInterface.realmGet$health();
        if (realmGet$health != null) {
            Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.healthIndex, createRow, realmGet$health, false);
        }
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, batteryDetailsColumnInfo.voltageIndex, j2, batteryDetailsRealmProxyInterface.realmGet$voltage(), false);
        Table.nativeSetDouble(j, batteryDetailsColumnInfo.temperatureIndex, j2, batteryDetailsRealmProxyInterface.realmGet$temperature(), false);
        String realmGet$technology = batteryDetailsRealmProxyInterface.realmGet$technology();
        if (realmGet$technology != null) {
            Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.technologyIndex, createRow, realmGet$technology, false);
        }
        long j3 = nativePtr;
        long j4 = createRow;
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.capacityIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$capacity(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.chargeCounterIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$chargeCounter(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.currentAverageIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentAverage(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.currentNowIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentNow(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.energyCounterIndex, j4, batteryDetailsRealmProxyInterface.realmGet$energyCounter(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(BatteryDetails.class);
        long nativePtr = table.getNativePtr();
        BatteryDetailsColumnInfo batteryDetailsColumnInfo = (BatteryDetailsColumnInfo) realm.getSchema().getColumnInfo(BatteryDetails.class);
        while (it.hasNext()) {
            BatteryDetails batteryDetails = (BatteryDetails) it.next();
            if (!map2.containsKey(batteryDetails)) {
                if (batteryDetails instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryDetails;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(batteryDetails, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(batteryDetails, Long.valueOf(createRow));
                BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails;
                String realmGet$charger = batteryDetailsRealmProxyInterface.realmGet$charger();
                if (realmGet$charger != null) {
                    j = createRow;
                    Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.chargerIndex, createRow, realmGet$charger, false);
                } else {
                    j = createRow;
                }
                String realmGet$health = batteryDetailsRealmProxyInterface.realmGet$health();
                if (realmGet$health != null) {
                    Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.healthIndex, j, realmGet$health, false);
                }
                long j2 = nativePtr;
                long j3 = j;
                Table.nativeSetDouble(j2, batteryDetailsColumnInfo.voltageIndex, j3, batteryDetailsRealmProxyInterface.realmGet$voltage(), false);
                Table.nativeSetDouble(j2, batteryDetailsColumnInfo.temperatureIndex, j3, batteryDetailsRealmProxyInterface.realmGet$temperature(), false);
                String realmGet$technology = batteryDetailsRealmProxyInterface.realmGet$technology();
                if (realmGet$technology != null) {
                    Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.technologyIndex, j, realmGet$technology, false);
                }
                long j4 = j;
                Table.nativeSetLong(nativePtr, batteryDetailsColumnInfo.capacityIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$capacity(), false);
                Table.nativeSetLong(nativePtr, batteryDetailsColumnInfo.chargeCounterIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$chargeCounter(), false);
                Table.nativeSetLong(nativePtr, batteryDetailsColumnInfo.currentAverageIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentAverage(), false);
                long j5 = nativePtr;
                Table.nativeSetLong(j5, batteryDetailsColumnInfo.currentNowIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentNow(), false);
                Table.nativeSetLong(j5, batteryDetailsColumnInfo.energyCounterIndex, j4, batteryDetailsRealmProxyInterface.realmGet$energyCounter(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, BatteryDetails batteryDetails, Map<RealmModel, Long> map) {
        BatteryDetails batteryDetails2 = batteryDetails;
        if (batteryDetails2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryDetails2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(BatteryDetails.class);
        long nativePtr = table.getNativePtr();
        BatteryDetailsColumnInfo batteryDetailsColumnInfo = (BatteryDetailsColumnInfo) realm.getSchema().getColumnInfo(BatteryDetails.class);
        long createRow = OsObject.createRow(table);
        map.put(batteryDetails2, Long.valueOf(createRow));
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails2;
        String realmGet$charger = batteryDetailsRealmProxyInterface.realmGet$charger();
        if (realmGet$charger != null) {
            Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.chargerIndex, createRow, realmGet$charger, false);
        } else {
            Table.nativeSetNull(nativePtr, batteryDetailsColumnInfo.chargerIndex, createRow, false);
        }
        String realmGet$health = batteryDetailsRealmProxyInterface.realmGet$health();
        if (realmGet$health != null) {
            Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.healthIndex, createRow, realmGet$health, false);
        } else {
            Table.nativeSetNull(nativePtr, batteryDetailsColumnInfo.healthIndex, createRow, false);
        }
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, batteryDetailsColumnInfo.voltageIndex, j2, batteryDetailsRealmProxyInterface.realmGet$voltage(), false);
        Table.nativeSetDouble(j, batteryDetailsColumnInfo.temperatureIndex, j2, batteryDetailsRealmProxyInterface.realmGet$temperature(), false);
        String realmGet$technology = batteryDetailsRealmProxyInterface.realmGet$technology();
        if (realmGet$technology != null) {
            Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.technologyIndex, createRow, realmGet$technology, false);
        } else {
            Table.nativeSetNull(nativePtr, batteryDetailsColumnInfo.technologyIndex, createRow, false);
        }
        long j3 = nativePtr;
        long j4 = createRow;
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.capacityIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$capacity(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.chargeCounterIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$chargeCounter(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.currentAverageIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentAverage(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.currentNowIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentNow(), false);
        Table.nativeSetLong(j3, batteryDetailsColumnInfo.energyCounterIndex, j4, batteryDetailsRealmProxyInterface.realmGet$energyCounter(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(BatteryDetails.class);
        long nativePtr = table.getNativePtr();
        BatteryDetailsColumnInfo batteryDetailsColumnInfo = (BatteryDetailsColumnInfo) realm.getSchema().getColumnInfo(BatteryDetails.class);
        while (it.hasNext()) {
            BatteryDetails batteryDetails = (BatteryDetails) it.next();
            if (!map2.containsKey(batteryDetails)) {
                if (batteryDetails instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batteryDetails;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(batteryDetails, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(batteryDetails, Long.valueOf(createRow));
                BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails;
                String realmGet$charger = batteryDetailsRealmProxyInterface.realmGet$charger();
                if (realmGet$charger != null) {
                    j = createRow;
                    Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.chargerIndex, createRow, realmGet$charger, false);
                } else {
                    j = createRow;
                    Table.nativeSetNull(nativePtr, batteryDetailsColumnInfo.chargerIndex, j, false);
                }
                String realmGet$health = batteryDetailsRealmProxyInterface.realmGet$health();
                if (realmGet$health != null) {
                    Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.healthIndex, j, realmGet$health, false);
                } else {
                    Table.nativeSetNull(nativePtr, batteryDetailsColumnInfo.healthIndex, j, false);
                }
                long j2 = nativePtr;
                long j3 = j;
                Table.nativeSetDouble(j2, batteryDetailsColumnInfo.voltageIndex, j3, batteryDetailsRealmProxyInterface.realmGet$voltage(), false);
                Table.nativeSetDouble(j2, batteryDetailsColumnInfo.temperatureIndex, j3, batteryDetailsRealmProxyInterface.realmGet$temperature(), false);
                String realmGet$technology = batteryDetailsRealmProxyInterface.realmGet$technology();
                if (realmGet$technology != null) {
                    Table.nativeSetString(nativePtr, batteryDetailsColumnInfo.technologyIndex, j, realmGet$technology, false);
                } else {
                    Table.nativeSetNull(nativePtr, batteryDetailsColumnInfo.technologyIndex, j, false);
                }
                long j4 = j;
                Table.nativeSetLong(nativePtr, batteryDetailsColumnInfo.capacityIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$capacity(), false);
                Table.nativeSetLong(nativePtr, batteryDetailsColumnInfo.chargeCounterIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$chargeCounter(), false);
                Table.nativeSetLong(nativePtr, batteryDetailsColumnInfo.currentAverageIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentAverage(), false);
                long j5 = nativePtr;
                Table.nativeSetLong(j5, batteryDetailsColumnInfo.currentNowIndex, j4, (long) batteryDetailsRealmProxyInterface.realmGet$currentNow(), false);
                Table.nativeSetLong(j5, batteryDetailsColumnInfo.energyCounterIndex, j4, batteryDetailsRealmProxyInterface.realmGet$energyCounter(), false);
            }
        }
    }

    public static BatteryDetails createDetachedCopy(BatteryDetails batteryDetails, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        BatteryDetails batteryDetails2;
        if (i > i2 || batteryDetails == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(batteryDetails);
        if (cacheData == null) {
            batteryDetails2 = new BatteryDetails();
            map.put(batteryDetails, new CacheData(i, batteryDetails2));
        } else if (i >= cacheData.minDepth) {
            return (BatteryDetails) cacheData.object;
        } else {
            BatteryDetails batteryDetails3 = (BatteryDetails) cacheData.object;
            cacheData.minDepth = i;
            batteryDetails2 = batteryDetails3;
        }
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface = batteryDetails2;
        BatteryDetailsRealmProxyInterface batteryDetailsRealmProxyInterface2 = batteryDetails;
        batteryDetailsRealmProxyInterface.realmSet$charger(batteryDetailsRealmProxyInterface2.realmGet$charger());
        batteryDetailsRealmProxyInterface.realmSet$health(batteryDetailsRealmProxyInterface2.realmGet$health());
        batteryDetailsRealmProxyInterface.realmSet$voltage(batteryDetailsRealmProxyInterface2.realmGet$voltage());
        batteryDetailsRealmProxyInterface.realmSet$temperature(batteryDetailsRealmProxyInterface2.realmGet$temperature());
        batteryDetailsRealmProxyInterface.realmSet$technology(batteryDetailsRealmProxyInterface2.realmGet$technology());
        batteryDetailsRealmProxyInterface.realmSet$capacity(batteryDetailsRealmProxyInterface2.realmGet$capacity());
        batteryDetailsRealmProxyInterface.realmSet$chargeCounter(batteryDetailsRealmProxyInterface2.realmGet$chargeCounter());
        batteryDetailsRealmProxyInterface.realmSet$currentAverage(batteryDetailsRealmProxyInterface2.realmGet$currentAverage());
        batteryDetailsRealmProxyInterface.realmSet$currentNow(batteryDetailsRealmProxyInterface2.realmGet$currentNow());
        batteryDetailsRealmProxyInterface.realmSet$energyCounter(batteryDetailsRealmProxyInterface2.realmGet$energyCounter());
        return batteryDetails2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("BatteryDetails = proxy[");
        sb.append("{charger:");
        sb.append(realmGet$charger() != null ? realmGet$charger() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{health:");
        sb.append(realmGet$health() != null ? realmGet$health() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{voltage:");
        sb.append(realmGet$voltage());
        sb.append("}");
        sb.append(",");
        sb.append("{temperature:");
        sb.append(realmGet$temperature());
        sb.append("}");
        sb.append(",");
        sb.append("{technology:");
        sb.append(realmGet$technology() != null ? realmGet$technology() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{capacity:");
        sb.append(realmGet$capacity());
        sb.append("}");
        sb.append(",");
        sb.append("{chargeCounter:");
        sb.append(realmGet$chargeCounter());
        sb.append("}");
        sb.append(",");
        sb.append("{currentAverage:");
        sb.append(realmGet$currentAverage());
        sb.append("}");
        sb.append(",");
        sb.append("{currentNow:");
        sb.append(realmGet$currentNow());
        sb.append("}");
        sb.append(",");
        sb.append("{energyCounter:");
        sb.append(realmGet$energyCounter());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BatteryDetailsRealmProxy batteryDetailsRealmProxy = (BatteryDetailsRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = batteryDetailsRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = batteryDetailsRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == batteryDetailsRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
