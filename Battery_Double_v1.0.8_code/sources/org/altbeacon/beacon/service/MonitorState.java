package org.altbeacon.beacon.service;

import java.util.Date;
import org.altbeacon.beacon.logging.LogManager;

public class MonitorState {
    public static long INSIDE_EXPIRATION_MILLIS = 10000;
    private static final String TAG = "MonitorState";
    private Callback callback;
    private boolean inside = false;
    private long lastSeenTime = 0;

    public MonitorState(Callback callback2) {
        this.callback = callback2;
    }

    public Callback getCallback() {
        return this.callback;
    }

    public boolean markInside() {
        this.lastSeenTime = System.currentTimeMillis();
        if (this.inside) {
            return false;
        }
        this.inside = true;
        return true;
    }

    public boolean isNewlyOutside() {
        if (!this.inside || this.lastSeenTime <= 0 || new Date().getTime() - this.lastSeenTime <= INSIDE_EXPIRATION_MILLIS) {
            return false;
        }
        this.inside = false;
        LogManager.d(TAG, "We are newly outside the region because the lastSeenTime of %s was %s seconds ago, and that is over the expiration duration of %s", Long.valueOf(this.lastSeenTime), Long.valueOf(System.currentTimeMillis() - this.lastSeenTime), Long.valueOf(INSIDE_EXPIRATION_MILLIS));
        this.lastSeenTime = 0;
        return true;
    }

    public boolean isInside() {
        return this.inside && !isNewlyOutside();
    }
}
