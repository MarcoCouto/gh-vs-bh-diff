package org.altbeacon.beacon.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.logging.LogManager;

public class RangedBeacon {
    public static long DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS = 20000;
    private static final String TAG = "RangedBeacon";
    private static long sampleExpirationMilliseconds = DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS;
    Beacon mBeacon;
    private ArrayList<Measurement> mMeasurements = new ArrayList<>();
    private boolean mTracked = true;

    private class Measurement implements Comparable<Measurement> {
        Integer rssi;
        long timestamp;

        private Measurement() {
        }

        public int compareTo(Measurement measurement) {
            return this.rssi.compareTo(measurement.rssi);
        }
    }

    public RangedBeacon(Beacon beacon) {
        updateBeacon(beacon);
    }

    public void updateBeacon(Beacon beacon) {
        this.mBeacon = beacon;
        addMeasurement(Integer.valueOf(this.mBeacon.getRssi()));
    }

    public boolean isTracked() {
        return this.mTracked;
    }

    public void setTracked(boolean z) {
        this.mTracked = z;
    }

    public Beacon getBeacon() {
        return this.mBeacon;
    }

    public void commitMeasurements() {
        if (this.mMeasurements.size() > 0) {
            double calculateRunningAverage = calculateRunningAverage();
            this.mBeacon.setRunningAverageRssi(calculateRunningAverage);
            LogManager.d(TAG, "calculated new runningAverageRssi: %s", Double.valueOf(calculateRunningAverage));
            return;
        }
        LogManager.d(TAG, "No measurements available to calculate running average", new Object[0]);
    }

    public static void setSampleExpirationMilliseconds(long j) {
        sampleExpirationMilliseconds = j;
    }

    public void addMeasurement(Integer num) {
        this.mTracked = true;
        Measurement measurement = new Measurement();
        measurement.rssi = num;
        measurement.timestamp = new Date().getTime();
        this.mMeasurements.add(measurement);
    }

    public boolean noMeasurementsAvailable() {
        return this.mMeasurements.size() == 0;
    }

    private synchronized void refreshMeasurements() {
        Date date = new Date();
        ArrayList<Measurement> arrayList = new ArrayList<>();
        Iterator it = this.mMeasurements.iterator();
        while (it.hasNext()) {
            Measurement measurement = (Measurement) it.next();
            if (date.getTime() - measurement.timestamp < sampleExpirationMilliseconds) {
                arrayList.add(measurement);
            }
        }
        this.mMeasurements = arrayList;
        Collections.sort(this.mMeasurements);
    }

    private double calculateRunningAverage() {
        int i;
        refreshMeasurements();
        int size = this.mMeasurements.size();
        int i2 = size - 1;
        if (size > 2) {
            int i3 = size / 10;
            i = i3 + 1;
            i2 = (size - i3) - 2;
        } else {
            i = 0;
        }
        double d = 0.0d;
        for (int i4 = i; i4 <= i2; i4++) {
            d += (double) ((Measurement) this.mMeasurements.get(i4)).rssi.intValue();
        }
        double d2 = d / ((double) ((i2 - i) + 1));
        LogManager.d(TAG, "Running average mRssi based on %s measurements: %s", Integer.valueOf(size), Double.valueOf(d2));
        return d2;
    }
}
