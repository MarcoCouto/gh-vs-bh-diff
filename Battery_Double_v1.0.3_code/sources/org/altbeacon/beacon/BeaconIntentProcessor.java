package org.altbeacon.beacon;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import java.util.Collection;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.service.MonitoringData;
import org.altbeacon.beacon.service.RangingData;

@TargetApi(3)
public class BeaconIntentProcessor extends IntentService {
    private static final String TAG = "BeaconIntentProcessor";

    public BeaconIntentProcessor() {
        super(TAG);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        RangingData rangingData;
        LogManager.d(TAG, "got an intent to process", new Object[0]);
        MonitoringData monitoringData = null;
        if (intent == null || intent.getExtras() == null) {
            rangingData = null;
        } else {
            monitoringData = (MonitoringData) intent.getExtras().get("monitoringData");
            rangingData = (RangingData) intent.getExtras().get("rangingData");
        }
        if (rangingData != null) {
            LogManager.d(TAG, "got ranging data", new Object[0]);
            if (rangingData.getBeacons() == null) {
                LogManager.w(TAG, "Ranging data has a null beacons collection", new Object[0]);
            }
            RangeNotifier rangingNotifier = BeaconManager.getInstanceForApplication(this).getRangingNotifier();
            Collection beacons = rangingData.getBeacons();
            if (rangingNotifier != null) {
                rangingNotifier.didRangeBeaconsInRegion(beacons, rangingData.getRegion());
            } else {
                LogManager.d(TAG, "but ranging notifier is null, so we're dropping it.", new Object[0]);
            }
            RangeNotifier dataRequestNotifier = BeaconManager.getInstanceForApplication(this).getDataRequestNotifier();
            if (dataRequestNotifier != null) {
                dataRequestNotifier.didRangeBeaconsInRegion(beacons, rangingData.getRegion());
            }
        }
        if (monitoringData != null) {
            LogManager.d(TAG, "got monitoring data", new Object[0]);
            MonitorNotifier monitoringNotifier = BeaconManager.getInstanceForApplication(this).getMonitoringNotifier();
            if (monitoringNotifier != null) {
                LogManager.d(TAG, "Calling monitoring notifier: %s", monitoringNotifier);
                boolean isInside = monitoringData.isInside();
                monitoringNotifier.didDetermineStateForRegion(isInside ? 1 : 0, monitoringData.getRegion());
                if (monitoringData.isInside()) {
                    monitoringNotifier.didEnterRegion(monitoringData.getRegion());
                } else {
                    monitoringNotifier.didExitRegion(monitoringData.getRegion());
                }
            }
        }
    }
}
