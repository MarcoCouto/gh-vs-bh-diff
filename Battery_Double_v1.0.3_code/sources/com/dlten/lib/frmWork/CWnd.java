package com.dlten.lib.frmWork;

import android.support.v4.view.MotionEventCompat;
import java.util.Timer;

public abstract class CWnd extends CEventWnd {
    public static final int ID_TIMER_0 = 0;
    public static final int ID_TIMER_1 = 1;
    public static final int ID_TIMER_2 = 2;
    public static final int ID_TIMER_3 = 3;
    public static final int ID_TIMER_4 = 4;
    private static final int ID_TIMER_COUNT = 5;
    static int fpscount;
    private CTimerTask[] m_tasks = new CTimerTask[5];
    private Timer[] m_timers = new Timer[5];

    public int OnNetEvent(int i, int i2, Object obj) {
        return -1;
    }

    public void OnPaint() {
    }

    public void OnTimer(int i) {
    }

    public CWnd() {
        for (int i = 0; i < this.m_tasks.length; i++) {
            this.m_tasks[i] = new CTimerTask(this);
            this.m_timers[i] = new Timer();
        }
    }

    private void DrawWindowName() {
        if (this.m_strName != null) {
            drawStr(0, 0, MotionEventCompat.ACTION_POINTER_INDEX_MASK, 17, this.m_strName);
        }
    }

    private void DrawFPS() {
        drawStr(CODE_WIDTH, 0, MotionEventCompat.ACTION_POINTER_INDEX_MASK, 65, String.format("%.1f ", new Object[]{Float.valueOf(getView().getFPS())}));
    }

    public void DrawPrevProc() {
        if (GetModalessWnd() != null) {
            GetModalessWnd().DrawPrevProc();
        }
    }

    public void DrawProcess() {
        OnPaint();
        int size = this.m_Anims.size();
        for (int i = 0; i < size; i++) {
            CAnimation cAnimation = (CAnimation) this.m_Anims.elementAt(i);
            if (cAnimation.IsDrawBylib()) {
                cAnimation.Draw();
            }
            cAnimation.UpdateFrame();
        }
        int size2 = this.m_Btns.size();
        for (int i2 = 0; i2 < size2; i2++) {
            ((CButton) this.m_Btns.elementAt(i2)).Draw();
        }
        if (GetModalessWnd() != null) {
            GetModalessWnd().DrawProcess();
        }
    }

    public void OnShowWindow() {
        super.OnShowWindow();
    }

    public void OnDestroy() {
        super.OnDestroy();
        for (int i = 0; i < this.m_tasks.length; i++) {
            this.m_tasks[i].cancel();
            this.m_timers[i].cancel();
            this.m_tasks[i] = null;
            this.m_timers[i] = null;
        }
        this.m_tasks = null;
        this.m_timers = null;
    }

    public int WindowProc(int i, int i2, int i3) {
        if (i != 4) {
            return super.WindowProc(i, i2, i3);
        }
        OnTimer(i2);
        return 0;
    }

    public boolean SetTimer(int i, int i2) {
        return SetTimer(i, i2, null);
    }

    public boolean SetTimer(int i, int i2, CTimerListner cTimerListner) {
        if (i < 0 || i >= 5) {
            return false;
        }
        this.m_tasks[i] = new CTimerTask(this);
        this.m_tasks[i].SetTimer(i, cTimerListner);
        this.m_timers[i].scheduleAtFixedRate(this.m_tasks[i], 100, (long) i2);
        return true;
    }

    public void KillTimer(int i) {
        this.m_tasks[i].KillTimer();
    }

    public final int SwitchWindow(int i) {
        return SwitchWindow(i, 0);
    }

    public final int SwitchWindow(int i, int i2) {
        return CWndMgr.getInstance().SwitchingWnd(i, i2);
    }

    public final void Exit(int i) {
        DestroyWindow(0);
    }
}
