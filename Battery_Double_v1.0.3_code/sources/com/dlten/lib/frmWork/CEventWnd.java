package com.dlten.lib.frmWork;

import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import com.dlten.lib.CBaseView;
import com.dlten.lib.Common;
import com.dlten.lib.graphics.CImgObj;
import com.dlten.lib.graphics.CPoint;
import java.util.Vector;

public abstract class CEventWnd {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_V_FILTER = 7;
    public static int CODE_HEIGHT = CBaseView.CODE_HEIGHT;
    public static int CODE_WIDTH = CBaseView.CODE_WIDTH;
    public static int HALF_HEIGHT = CBaseView.HALF_HEIGHT;
    public static int HALF_WIDTH = CBaseView.HALF_WIDTH;
    public static final int KEY_0 = 1;
    public static final int KEY_1 = 2;
    public static final int KEY_2 = 4;
    public static final int KEY_3 = 8;
    public static final int KEY_4 = 16;
    public static final int KEY_5 = 32;
    public static final int KEY_6 = 64;
    public static final int KEY_7 = 128;
    public static final int KEY_8 = 256;
    public static final int KEY_9 = 512;
    public static final int KEY_BACK = 4096;
    public static final int KEY_DIAL = 8192;
    public static final int KEY_DOWN = 32768;
    public static final int KEY_HOME = 2048;
    public static final int KEY_LEFT = 65536;
    public static final int KEY_MENU = 1024;
    public static final int KEY_NONE = 0;
    public static final int KEY_RIGHT = 131072;
    public static final int KEY_SELECT = 262144;
    public static final int KEY_UP = 16384;
    public static final int NET_EVT_CLOSED = 1;
    public static final int NET_EVT_CONNECTED = 0;
    public static final int NET_EVT_RECVDATA = 2;
    public static int REAL_HEIGHT = CBaseView.RES_HEIGHT;
    public static int REAL_WIDTH = CBaseView.RES_WIDTH;
    public static final int WM_ANIM_EVENT = 16;
    public static final int WM_APP_EXIT = 18;
    public static final int WM_COMMAND = 3;
    public static final int WM_KEY_DOWN = 12;
    public static final int WM_KEY_PRESS = 14;
    public static final int WM_KEY_UP = 13;
    public static final int WM_NET = 17;
    public static final int WM_PAINT = 2;
    public static final int WM_QUIT = 15;
    public static final int WM_RESIZE = 19;
    public static final int WM_RESUME = 6;
    public static final int WM_SUSPEND = 5;
    public static final int WM_TIMER = 4;
    public static final int WM_TOUCH_DOWN = 8;
    public static final int WM_TOUCH_MOVE = 10;
    public static final int WM_TOUCH_UP = 9;
    public static final int WM_USER = 1024;
    private static CBaseView m_baseView;
    private static Handler m_handleUpdateActivity = new Handler() {
        public void handleMessage(Message message) {
            CEventWnd.getView().getActivity().onRecvMessage(message.what, message.arg1, message.arg2);
        }
    };
    protected Vector<CAnimation> m_Anims = new Vector<>(10, 5);
    protected Vector<CButton> m_Btns = new Vector<>(10, 5);
    private boolean m_bEnable = true;
    private CButton m_btnFocus = null;
    protected CEventWnd m_pActiveWnd = null;
    protected CEventWnd m_pForegroundWnd = null;
    protected CEventWnd m_pModalessWnd = null;
    protected CEventWnd m_pParent = null;
    public String m_strName = null;

    public abstract void DrawPrevProc();

    public abstract void DrawProcess();

    public void OnAnimEvent(int i) {
    }

    public void OnCommand(int i) {
    }

    public void OnInitWindow() {
    }

    public void OnKeyDown(int i) {
    }

    public void OnKeyPress(int i) {
    }

    public void OnKeyUp(int i) {
    }

    public void OnLoadResource() {
    }

    public int OnNetEvent(int i, int i2, Object obj) {
        return 0;
    }

    public void OnResume() {
    }

    public void OnShowWindow() {
    }

    public void OnSuspend() {
    }

    public void OnTouchDown(int i, int i2) {
    }

    public void OnTouchMove(int i, int i2) {
    }

    public void OnTouchUp(int i, int i2) {
    }

    public CEventWnd() {
        m_baseView.clearMsgQueue();
    }

    public void OnDestroy() {
        RemoveAllButtons();
    }

    public final int RunProc() {
        return m_baseView.RunProc(this);
    }

    public int WindowProc(int i, int i2, int i3) {
        switch (i) {
            case 2:
                UpdateWindow();
                break;
            case 3:
                OnCommand(i2);
                break;
            case 8:
                if (!setFocusButton(i2, i3)) {
                    OnTouchDown(i2, i3);
                    break;
                }
                break;
            case 9:
                if (!clickFocusButton(i2, i3)) {
                    OnTouchUp(i2, i3);
                    break;
                }
                break;
            case 10:
                if (!changeFocusButton(i2, i3)) {
                    OnTouchMove(i2, i3);
                    break;
                }
                break;
            case 12:
                OnKeyDown(i2);
                break;
            case 13:
                OnKeyUp(i2);
                break;
            case 14:
                OnKeyPress(i2);
                break;
            case 16:
                OnAnimEvent((short) i2);
                break;
        }
        return 0;
    }

    private CButton findButton(int i, int i2) {
        CPoint cPoint = new CPoint(i, i2);
        CButton cButton = null;
        for (int i3 = 0; i3 < this.m_Btns.size(); i3++) {
            CButton cButton2 = (CButton) this.m_Btns.elementAt(i3);
            if (cButton2.isUseful() && cButton2.isInside(cPoint)) {
                cButton = cButton2;
            }
        }
        return cButton;
    }

    private boolean setFocusButton(int i, int i2) {
        CButton findButton = findButton(i, i2);
        if (findButton == null) {
            if (this.m_btnFocus != null) {
                this.m_btnFocus.setNormal();
                this.m_btnFocus = null;
            }
            return false;
        }
        if (!(findButton == this.m_btnFocus || this.m_btnFocus == null)) {
            this.m_btnFocus.setNormal();
            this.m_btnFocus = null;
        }
        findButton.setFocus();
        this.m_btnFocus = findButton;
        return true;
    }

    private boolean changeFocusButton(int i, int i2) {
        if (this.m_btnFocus == null || !this.m_btnFocus.isVisible()) {
            return false;
        }
        if (this.m_btnFocus.isEnable()) {
            if (this.m_btnFocus.isInside(new CPoint(i, i2))) {
                this.m_btnFocus.setFocus();
            } else {
                this.m_btnFocus.setNormal();
            }
        }
        return true;
    }

    private boolean clickFocusButton(int i, int i2) {
        if (this.m_btnFocus == null || !this.m_btnFocus.isUseful()) {
            return false;
        }
        CButton cButton = this.m_btnFocus;
        this.m_btnFocus = null;
        cButton.setNormal();
        if (cButton.isInside(new CPoint(i, i2))) {
            SendMessage(3, cButton.getCommand(), 0);
        }
        return true;
    }

    public static void setView(CBaseView cBaseView) {
        m_baseView = cBaseView;
    }

    public static CBaseView getView() {
        return m_baseView;
    }

    public boolean AddAnimation(CAnimation cAnimation) {
        if (this.m_Anims.indexOf(cAnimation) != -1) {
            return false;
        }
        this.m_Anims.addElement(cAnimation);
        return true;
    }

    public void RemoveAnimation(CAnimation cAnimation) {
        this.m_Anims.removeElement(cAnimation);
    }

    public CButton createButton(String str, String str2, String str3) {
        CButton cButton = new CButton();
        cButton.create(this, str, str2, str3);
        return cButton;
    }

    public boolean AddButton(CButton cButton) {
        if (this.m_Btns.indexOf(cButton) != -1) {
            return false;
        }
        this.m_Btns.addElement(cButton);
        return true;
    }

    public void RemoveButton(CButton cButton) {
        this.m_Btns.removeElement(cButton);
    }

    public void RemoveAllButtons() {
        while (this.m_Btns.size() > 0) {
            ((CButton) this.m_Btns.elementAt(0)).destroy();
        }
    }

    public boolean EnableWindow(boolean z) {
        boolean z2 = this.m_bEnable;
        this.m_bEnable = z;
        return z2;
    }

    public boolean IsEnable() {
        return this.m_bEnable;
    }

    public void NotifyToParentEndRun() {
        if (this.m_pParent != null) {
            this.m_pParent.InitKeyState();
        }
    }

    private void InitKeyState() {
        m_baseView.DeleteMsgs(new int[]{12, 13});
    }

    public void SetParent(CEventWnd cEventWnd) {
        this.m_pParent = cEventWnd;
    }

    public CEventWnd SetActiveWnd(CEventWnd cEventWnd) {
        CEventWnd cEventWnd2 = this.m_pActiveWnd;
        this.m_pActiveWnd = cEventWnd;
        return cEventWnd2;
    }

    public CEventWnd SetModalessWnd(CEventWnd cEventWnd) {
        CEventWnd cEventWnd2 = this.m_pModalessWnd;
        this.m_pModalessWnd = cEventWnd;
        return cEventWnd2;
    }

    public CEventWnd SetForegroundWnd(CEventWnd cEventWnd) {
        CEventWnd cEventWnd2 = this.m_pForegroundWnd;
        this.m_pForegroundWnd = cEventWnd;
        return cEventWnd2;
    }

    public void DestroyWindow(int i) {
        PostMessage(15, i);
    }

    public CEventWnd GetParent() {
        return this.m_pParent;
    }

    public CEventWnd GetActiveWnd() {
        return this.m_pActiveWnd;
    }

    public CEventWnd GetForegroundWnd() {
        return this.m_pForegroundWnd;
    }

    public CEventWnd GetModalessWnd() {
        return this.m_pModalessWnd;
    }

    public boolean SendMessage(int i) {
        SendMessage(i, 0);
        return true;
    }

    public boolean SendMessage(int i, int i2) {
        SendMessage(i, i2, 0);
        return true;
    }

    public boolean SendMessage(int i, int i2, int i3) {
        WindowProc(i, i2, i3);
        return true;
    }

    public boolean PostMessage(int i) {
        return PostMessage(i, 0);
    }

    public boolean PostMessage(int i, int i2) {
        return PostMessage(i, i2, 0);
    }

    public boolean PostMessage(int i, int i2, int i3) {
        if (!IsEnable() && i == 4) {
            return false;
        }
        m_baseView.PostMessage(i, i2, i3);
        return true;
    }

    public void postActivityMsg(int i, int i2, int i3) {
        Message message = new Message();
        message.what = i;
        message.arg1 = i2;
        message.arg2 = i3;
        m_handleUpdateActivity.sendMessage(message);
    }

    public void UpdateWindow() {
        m_baseView.update(this);
    }

    public void Invalidate() {
        PostMessage(2);
    }

    public void setString(String str) {
        this.m_strName = str;
    }

    public void drawStr(int i, int i2, int i3, int i4, String str) {
        CBaseView view = getView();
        view.setColor(i3);
        view.drawString(str, (float) ((int) Common.code2screen((float) i)), (float) ((int) Common.code2screen((float) i2)), i4);
    }

    public void drawStr(int i, int i2, String str) {
        drawStr(i, i2, ViewCompat.MEASURED_SIZE_MASK, 34, str);
    }

    public void drawRectStr(int i, int i2, int i3, int i4, int i5, int i6, String str) {
        int code2screen = (int) Common.code2screen((float) i);
        int code2screen2 = (int) Common.code2screen((float) i2);
        int code2screen3 = (int) Common.code2screen((float) i3);
        int code2screen4 = (int) Common.code2screen((float) i4);
        CBaseView view = getView();
        view.setColor(i5);
        view.drawRectString(str, code2screen, code2screen2, code2screen3, code2screen4, 0, -1, i6);
    }

    public void drawRectStr(int i, int i2, int i3, int i4, String str) {
        int code2screen = (int) Common.code2screen((float) i);
        int code2screen2 = (int) Common.code2screen((float) i2);
        CBaseView view = getView();
        view.setColor(i3);
        view.drawRectString(str, code2screen, code2screen2, REAL_WIDTH, REAL_HEIGHT, 0, -1, i4);
    }

    /* access modifiers changed from: protected */
    public CImgObj unload(CImgObj cImgObj) {
        if (cImgObj == null) {
            return cImgObj;
        }
        cImgObj.unload();
        return null;
    }

    public static void prepareDC() {
        CODE_WIDTH = CBaseView.CODE_WIDTH;
        CODE_HEIGHT = CBaseView.CODE_HEIGHT;
        REAL_WIDTH = CBaseView.RES_WIDTH;
        REAL_HEIGHT = CBaseView.RES_HEIGHT;
        HALF_WIDTH = CBaseView.HALF_WIDTH;
        HALF_HEIGHT = CBaseView.HALF_HEIGHT;
    }
}
