package com.dlten.lib.frmWork;

public abstract class CAnimation implements AnimListner {
    public static final int CALLBACK_EVER_FRM = 3;
    public static final int CALLBACK_LAST_FRM = 1;
    public static final int CALLBACK_NONE = 0;
    public static final int CALLBACK_SPEC_FRM = 2;
    public static final int CallbackType_MAX = 4;
    public static final short IDA_NONE = -1;
    private boolean m_bActive;
    private boolean m_bEnd;
    private boolean m_bLoop;
    private boolean m_bOwnerDraw;
    private boolean m_bParentRegistered;
    private boolean m_bPause;
    private boolean m_bVisible;
    private int m_callBackType;
    private short m_nAnimID;
    private int m_nCallBackFrameNum;
    private int m_nCurFrame;
    int m_nDirection;
    private int m_nEndFrame;
    private short m_nSpeed;
    private int m_nSpeedFrame;
    private int m_nStartFrame;
    private Object m_pParam;
    private CEventWnd m_pParent;
    private AnimListner m_pProc;

    /* access modifiers changed from: protected */
    public void BindResource() {
    }

    /* access modifiers changed from: protected */
    public abstract void DrawProc();

    public void SetFrame(int i) {
    }

    /* access modifiers changed from: protected */
    public void UnbindResource() {
    }

    public void onAnimAction(CAnimation cAnimation, Object obj) {
        CEventWnd cEventWnd = cAnimation.m_pParent;
        if (cEventWnd == CWndMgr.getInstance().GetCurWnd() && cEventWnd != null) {
            cEventWnd.PostMessage(16, cAnimation.GetID());
        }
    }

    public void ChangeCallBackType(int i, short s, int i2) {
        ChangeCallBackType(this, null, i, i2, s);
    }

    public void ChangeCallBackType(AnimListner animListner, Object obj, int i, int i2, short s) {
        this.m_nAnimID = s;
        this.m_pProc = animListner;
        this.m_pParam = obj;
        this.m_callBackType = i;
        this.m_nCallBackFrameNum = i2;
    }

    public CAnimation() {
        Reset();
    }

    public short GetID() {
        return this.m_nAnimID;
    }

    public void Create(CEventWnd cEventWnd, int i, int i2, boolean z, boolean z2) {
        Reset();
        this.m_pParent = cEventWnd;
        this.m_nStartFrame = i;
        this.m_nEndFrame = i2;
        this.m_bLoop = z;
        this.m_bOwnerDraw = z2;
    }

    private void Reset() {
        this.m_pParent = null;
        this.m_nAnimID = -1;
        this.m_nStartFrame = 0;
        this.m_nEndFrame = 0;
        this.m_nCurFrame = 0;
        this.m_bLoop = false;
        this.m_nSpeed = 8;
        this.m_nSpeed = 16;
        this.m_bVisible = true;
        this.m_bPause = false;
        this.m_bEnd = false;
        this.m_bOwnerDraw = true;
        this.m_nDirection = 1;
        this.m_bActive = false;
        this.m_bParentRegistered = false;
        this.m_callBackType = 0;
        this.m_nCallBackFrameNum = 0;
        this.m_pProc = null;
        this.m_pParam = null;
        Stop();
    }

    private void Register() {
        if (this.m_pParent != null && !this.m_bParentRegistered) {
            this.m_pParent.AddAnimation(this);
            this.m_bParentRegistered = true;
        }
    }

    private void Unregister() {
        if (this.m_pParent != null && this.m_bParentRegistered) {
            this.m_pParent.RemoveAnimation(this);
            this.m_bParentRegistered = false;
        }
    }

    public void Start() {
        Stop();
        this.m_nCurFrame = this.m_nStartFrame;
        this.m_bPause = false;
        this.m_bEnd = false;
        this.m_nSpeedFrame = 0;
        this.m_nDirection = 1;
        this.m_bActive = true;
        Register();
        BindResource();
        SetFrame(this.m_nCurFrame);
    }

    public void Stop() {
        this.m_bActive = false;
        this.m_nSpeedFrame = 0;
        Unregister();
        UnbindResource();
    }

    public void Pause() {
        this.m_bPause = true;
    }

    public void Resume() {
        this.m_bPause = false;
    }

    public boolean Animate(int i) {
        if (!IsActive()) {
            Start();
        }
        while (!IsEnd()) {
            if (this.m_pParent == null || !this.m_bParentRegistered) {
                Show();
            } else {
                this.m_pParent.UpdateWindow();
            }
        }
        return true;
    }

    public void Show() {
        Draw();
        UpdateFrame();
    }

    public void Draw() {
        if (GetVisible()) {
            DrawProc();
        }
    }

    public boolean UpdateFrame() {
        if (!IsActive() || IsPaused()) {
            return false;
        }
        this.m_nCurFrame = CalcNextFrame();
        EndFrameProc();
        SetFrame(this.m_nCurFrame);
        CallBackFrameProc();
        return true;
    }

    public void Reverse() {
        this.m_nDirection = -this.m_nDirection;
    }

    public int GetDirection() {
        return this.m_nDirection;
    }

    public void SetVisible(boolean z) {
        this.m_bVisible = z;
    }

    public final boolean GetVisible() {
        return this.m_bVisible;
    }

    public void SetSpeed(short s) {
        this.m_nSpeed = s;
    }

    public short GetSpeed() {
        return this.m_nSpeed;
    }

    public boolean IsPaused() {
        return this.m_bPause;
    }

    public boolean IsEnd() {
        return this.m_bEnd || this.m_nEndFrame == this.m_nStartFrame;
    }

    public boolean IsLoop() {
        return this.m_bLoop;
    }

    public boolean IsDrawBylib() {
        return this.m_bOwnerDraw;
    }

    public void SetDrawBylib(boolean z) {
        this.m_bOwnerDraw = z;
    }

    public boolean IsActive() {
        return this.m_bActive;
    }

    /* access modifiers changed from: protected */
    public int GetCurFrameNum() {
        return this.m_nCurFrame;
    }

    private int CalcNextFrame() {
        int i = this.m_nCurFrame;
        this.m_nSpeedFrame += GetSpeed();
        int i2 = this.m_nSpeedFrame / 8;
        this.m_nSpeedFrame %= 8;
        return i + (i2 * this.m_nDirection);
    }

    private void EndFrameProc() {
        if (this.m_nDirection > 0) {
            if (this.m_nCurFrame <= this.m_nEndFrame - 1) {
                return;
            }
            if (this.m_bLoop) {
                this.m_nCurFrame = this.m_nStartFrame;
                return;
            }
            this.m_nCurFrame = this.m_nEndFrame - 1;
            this.m_bActive = false;
            this.m_bEnd = true;
        } else if (this.m_nCurFrame >= this.m_nStartFrame) {
        } else {
            if (this.m_bLoop) {
                this.m_nCurFrame = this.m_nEndFrame - 1;
                return;
            }
            this.m_nCurFrame = this.m_nStartFrame;
            this.m_bActive = false;
            this.m_bEnd = true;
        }
    }

    private void CallBackFrameProc() {
        switch (this.m_callBackType) {
            case 1:
                if (IsEnd() && this.m_pProc != null) {
                    this.m_pProc.onAnimAction(this, this.m_pParam);
                    return;
                }
                return;
            case 2:
                if (this.m_nCurFrame == this.m_nCallBackFrameNum && this.m_pProc != null) {
                    this.m_pProc.onAnimAction(this, this.m_pParam);
                    return;
                }
                return;
            case 3:
                if (this.m_pProc != null) {
                    this.m_pProc.onAnimAction(this, this.m_pParam);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
