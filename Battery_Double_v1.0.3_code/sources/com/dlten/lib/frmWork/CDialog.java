package com.dlten.lib.frmWork;

import android.support.v4.view.MotionEventCompat;
import com.dlten.lib.STD;

public abstract class CDialog extends CEventWnd {
    private boolean m_bModal;

    public void OnKeyDown(int i) {
    }

    public int OnNetEvent(int i, int i2, Object obj) {
        return -1;
    }

    public void OnPaint() {
    }

    public void OnTouchDown(int i, int i2) {
    }

    public CDialog() {
        this.m_pParent = null;
        this.m_bModal = true;
    }

    public void DrawWindowName() {
        if (this.m_strName != null) {
            drawStr(0, 50, MotionEventCompat.ACTION_POINTER_INDEX_MASK, 17, this.m_strName);
        }
    }

    public void DrawProcess() {
        if (this.m_bModal) {
            GetParent().DrawProcess();
        }
        OnPaint();
        for (int i = 0; i < this.m_Anims.size(); i++) {
            CAnimation cAnimation = (CAnimation) this.m_Anims.elementAt(i);
            if (cAnimation.IsDrawBylib()) {
                cAnimation.Draw();
            }
            cAnimation.UpdateFrame();
        }
        for (int i2 = 0; i2 < this.m_Btns.size(); i2++) {
            ((CButton) this.m_Btns.elementAt(i2)).Draw();
        }
    }

    public void DrawPrevProc() {
        if (this.m_bModal) {
            GetParent().DrawPrevProc();
        }
    }

    public int DoModal(CEventWnd cEventWnd) {
        this.m_bModal = true;
        return CWndMgr.getInstance().DialogDoModal(this, cEventWnd);
    }

    public void Modaless(CEventWnd cEventWnd) {
        this.m_bModal = false;
        CWndMgr.getInstance().DialogModaless(this, cEventWnd);
    }

    public void Destroy() {
        STD.ASSERT(!this.m_bModal);
        this.m_bModal = true;
        CWndMgr.getInstance().DialogDestroy(this, GetParent());
    }
}
