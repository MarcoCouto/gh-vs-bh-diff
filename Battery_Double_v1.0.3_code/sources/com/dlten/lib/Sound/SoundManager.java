package com.dlten.lib.Sound;

import android.content.Context;

public class SoundManager {
    protected BgmManager mBgmManager;
    protected SeManager mSeManager;
    private boolean m_bEnable;

    /* access modifiers changed from: protected */
    public void loadSounds() {
    }

    protected SoundManager() {
        this.mBgmManager = null;
        this.mSeManager = null;
        this.m_bEnable = false;
        this.mBgmManager = BgmManager.getInstance();
        this.mSeManager = SeManager.getInstance();
    }

    public void init(Context context) {
        this.mBgmManager.init(context);
        this.mSeManager.init(context);
        loadSounds();
        setEnable(true);
    }

    public void destroy() {
        this.mBgmManager.destroy();
        this.mSeManager.destroy();
        this.mBgmManager = null;
        this.mSeManager = null;
    }

    public void playBGM(int i) {
        if (this.m_bEnable) {
            this.mBgmManager.playBGM(i);
        }
    }

    public void playBGM(int i, boolean z) {
        if (this.m_bEnable) {
            this.mBgmManager.playBGM(i, z);
        }
    }

    public void stopBGM() {
        if (this.m_bEnable) {
            this.mBgmManager.stopBGM();
        }
    }

    public void pauseBGM() {
        this.mBgmManager.pauseBGM();
    }

    public void resumeBGM() {
        this.mBgmManager.resumeBGM();
    }

    public void playSE(int i) {
        if (this.m_bEnable) {
            this.mSeManager.play(i);
        }
    }

    public void stopSE(int i) {
        this.mSeManager.stop(i);
    }

    public void stopSE() {
        this.mSeManager.stopAll();
    }

    public void setEnable(boolean z) {
        this.m_bEnable = z;
        this.mBgmManager.setEnable(this.m_bEnable);
        this.mSeManager.setEnable(this.m_bEnable);
    }
}
