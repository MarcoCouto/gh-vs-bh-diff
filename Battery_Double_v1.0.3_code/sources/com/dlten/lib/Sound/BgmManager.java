package com.dlten.lib.Sound;

import android.content.Context;
import android.media.MediaPlayer;
import com.dlten.lib.STD;

public class BgmManager {
    private static final int BGM_PLAYER = 0;
    public static final int PAUSE_SOUND = -2;
    private static final int SE_PLAYER = 1;
    public static final int STOP_SOUND = -1;
    private static BgmManager _instance;
    private static Context mContext;
    private boolean m_bEnable;
    private int m_nBgmNum = -1;
    private int m_nEffNum = -1;
    private MediaPlayer[] m_players = new MediaPlayer[2];

    public BgmManager() {
        _instance = this;
    }

    public static BgmManager getInstance() {
        if (_instance == null) {
            _instance = new BgmManager();
        }
        return _instance;
    }

    public void init(Context context) {
        mContext = context;
    }

    public void destroy() {
        playBGM(-1);
    }

    private MediaPlayer createPlayer(int i) {
        try {
            return MediaPlayer.create(mContext, i);
        } catch (Exception e) {
            STD.printStackTrace(e);
            return null;
        }
    }

    public void blockWhileSe() {
        if (this.m_nEffNum >= 0 && this.m_nEffNum < this.m_players.length) {
            while (this.m_players[1].isPlaying()) {
                STD.sleep(30);
            }
        }
    }

    public void stopBGM() {
        if (this.m_nBgmNum >= 0) {
            try {
                this.m_players[0].stop();
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
            this.m_nBgmNum = -1;
        }
    }

    public void pauseBGM() {
        if (this.m_nBgmNum >= 0) {
            try {
                this.m_players[0].pause();
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
    }

    public void resumeBGM() {
        if (this.m_nBgmNum >= 0) {
            try {
                this.m_players[0].start();
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
    }

    public void playBGM(int i) {
        playBGM(i, true);
    }

    public void playBGM(int i, boolean z) {
        if (this.m_bEnable && i >= 0 && this.m_nBgmNum != i) {
            try {
                stopBGM();
                stopSE();
                if (this.m_players[0] != null) {
                    this.m_players[0].release();
                    this.m_players[0] = null;
                }
                this.m_players[0] = createPlayer(i);
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
            playBGM_force(i, z);
        }
    }

    private void playBGM_force(int i, boolean z) {
        if (this.m_bEnable && i >= 0) {
            try {
                this.m_players[0].setLooping(z);
                this.m_players[0].start();
                this.m_nBgmNum = i;
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
    }

    public void playSE(int i) {
        if (this.m_bEnable && i >= 0) {
            stopSE();
            try {
                resetPlayer(i);
                this.m_players[1].start();
                this.m_nEffNum = i;
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
    }

    public void stopSE() {
        if (this.m_nEffNum >= 0) {
            do {
                try {
                } catch (Exception e) {
                    STD.printStackTrace(e);
                }
            } while (this.m_players[1].isPlaying());
            this.m_nEffNum = -1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setVolume(MediaPlayer mediaPlayer, int i) {
        float f = (float) i;
        mediaPlayer.setVolume(f, f);
    }

    private void resetPlayer(int i) {
        if (this.m_players[1] != null) {
            this.m_players[1].release();
            this.m_players[1] = null;
        }
        this.m_players[1] = createPlayer(i);
    }

    public void setEnable(boolean z) {
        this.m_bEnable = z;
        if (!this.m_bEnable) {
            stopBGM();
            stopSE();
        }
    }
}
