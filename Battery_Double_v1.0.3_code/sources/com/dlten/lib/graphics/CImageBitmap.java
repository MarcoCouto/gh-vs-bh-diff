package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.dlten.lib.STD;

public class CImageBitmap extends CImage {
    private Bitmap m_img = null;

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap bitmap) {
        Bitmap bitmap2;
        super.loadResource(bitmap);
        if (CDCView.m_fScaleRes != 1.0f) {
            try {
                bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), CImage.getDCView().m_matrix, true);
            } catch (Exception e) {
                STD.printStackTrace(e);
                bitmap2 = null;
            }
            this.m_img = bitmap2;
        } else {
            this.m_img = bitmap;
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public int changeWithPer(int i) {
        Bitmap bitmap;
        try {
            int height = (this.m_img.getHeight() * i) / 100;
            int height2 = this.m_img.getHeight() - height;
            bitmap = Bitmap.createBitmap(this.m_img, 0, height2, this.m_img.getWidth(), height);
        } catch (Exception e) {
            STD.printStackTrace(e);
            bitmap = null;
        }
        this.m_img = bitmap;
        return 1;
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
        if (this.m_img != null) {
            this.m_img.recycle();
            this.m_img = null;
        }
        super.unloadResource();
    }

    public void drawImage(float f, float f2, float f3, float f4, int i, Matrix matrix) {
        CDCView dCView = CImage.getDCView();
        if (i != 255) {
            dCView.setAlpha(i);
        }
        dCView.drawImage(this.m_img, matrix);
        if (i != 255) {
            dCView.setAlpha(255);
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap getImage() {
        return this.m_img;
    }

    public int[] getRGBData() {
        if (this.m_img == null) {
            return null;
        }
        return getRGBData(0, 0, this.m_img.getWidth(), this.m_img.getHeight());
    }

    public int[] getRGBData(int i, int i2, int i3, int i4) {
        return CBmpManager.getRGBData(this.m_img, i, i2, i3, i4);
    }
}
