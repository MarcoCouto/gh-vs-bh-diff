package com.dlten.lib.graphics;

public class CPoint {
    public float x;
    public float y;

    public CPoint() {
        this.x = 0.0f;
        this.y = 0.0f;
    }

    public CPoint(float f, float f2) {
        this.x = f;
        this.y = f2;
    }

    public CPoint(int i, int i2) {
        this.x = (float) i;
        this.y = (float) i2;
    }

    public CPoint(CPoint cPoint) {
        this.x = cPoint.x;
        this.y = cPoint.y;
    }

    public CPoint(CSize cSize) {
        this.x = cSize.w;
        this.y = cSize.h;
    }

    public void Offset(float f, float f2) {
        this.x += f;
        this.y += f2;
    }

    public void Offset(int i, int i2) {
        this.x += (float) i;
        this.y += (float) i2;
    }

    public void Offset(CPoint cPoint) {
        this.x += cPoint.x;
        this.y += cPoint.y;
    }

    public void Offset(CSize cSize) {
        this.x += cSize.w;
        this.y += cSize.h;
    }
}
