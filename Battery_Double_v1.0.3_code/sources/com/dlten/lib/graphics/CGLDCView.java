package com.dlten.lib.graphics;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import com.dlten.lib.STD;

public class CGLDCView extends GLSurfaceView {
    public static int HALF_HEIGHT = 0;
    public static int HALF_WIDTH = 0;
    public static int REAL_HEIGHT = 0;
    public static int REAL_WIDTH = 480;
    private int m_nDblOffsetX;
    private int m_nDblOffsetY;
    private int m_nWndOffsetX;
    private int m_nWndOffsetY;
    private CRenderer m_renderer = null;

    public CGLDCView(Context context) {
        super(context);
        initCBaseView();
    }

    public CGLDCView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initCBaseView();
    }

    private void initCBaseView() {
        this.m_renderer = new CRenderer();
        setRenderer(this.m_renderer);
    }

    public void prepareDC() {
        int width = getWidth();
        int height = getHeight();
        int i = (REAL_WIDTH * 854) / 480;
        REAL_HEIGHT = (REAL_WIDTH * height) / width;
        if (REAL_HEIGHT > i) {
            REAL_HEIGHT = i;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("init(");
        sb.append(width);
        sb.append(", ");
        sb.append(height);
        sb.append(")");
        STD.logout(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Drawing(");
        sb2.append(REAL_WIDTH);
        sb2.append(", ");
        sb2.append(REAL_HEIGHT);
        sb2.append(")");
        STD.logout(sb2.toString());
        HALF_WIDTH = REAL_WIDTH / 2;
        HALF_HEIGHT = REAL_HEIGHT / 2;
        this.m_nWndOffsetX = 0;
        this.m_nWndOffsetY = 0;
        this.m_nDblOffsetX = 0;
        this.m_nDblOffsetY = 0;
    }

    public static void setResWidth(int i) {
        REAL_WIDTH = i;
    }
}
