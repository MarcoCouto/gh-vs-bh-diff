package com.dlten.lib.graphics;

public class CSize {
    public float h;
    public float w;

    public CSize() {
    }

    public CSize(float f, float f2) {
        this.w = f;
        this.h = f2;
    }

    public CSize(CSize cSize) {
        this.w = cSize.w;
        this.h = cSize.h;
    }

    public CSize(CPoint cPoint) {
        this.w = cPoint.x;
        this.h = cPoint.y;
    }

    public float getWidth() {
        return this.w;
    }

    public float getHeight() {
        return this.h;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(this.w);
        sb.append(", ");
        sb.append(this.h);
        sb.append(">");
        return sb.toString();
    }

    public static CSize make(float f, float f2) {
        return new CSize(f, f2);
    }

    public static CSize zero() {
        return new CSize(0.0f, 0.0f);
    }

    public static boolean equalToSize(CSize cSize, CSize cSize2) {
        return cSize.w == cSize2.w && cSize.h == cSize2.h;
    }
}
