package com.dlten.lib.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.dlten.lib.Common;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;

public abstract class CDCView extends SurfaceView {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_HCENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_VCENTER = 2;
    public static final int ANCHOR_V_FILTER = 7;
    public static int CODE_HEIGHT = 0;
    public static int CODE_WIDTH = 0;
    public static int HALF_HEIGHT = 0;
    public static int HALF_WIDTH = 0;
    public static int RES_HEIGHT = 0;
    public static int RES_WIDTH = 0;
    private static final char RETURN_CHAR = '\n';
    public static int SC_HEIGHT = 0;
    public static int SC_WIDTH = 0;
    private static int m_bufHeight = 1024;
    private static int m_bufWidth = 1024;
    public static int m_drawHeight;
    public static int m_drawWidth;
    public static float m_fScaleCode;
    public static float m_fScaleRes;
    protected static int m_nDblOffsetX;
    protected static int m_nDblOffsetY;
    protected static int m_nWndOffsetX;
    protected static int m_nWndOffsetY;
    protected int FONT_BASELINE;
    protected int FONT_H;
    protected Bitmap m_bmpDblBuffer;
    protected Canvas m_canvas;
    protected SurfaceHolder m_holder;
    Matrix m_matrix = new Matrix();
    protected int m_nFontSize = 36;

    /* access modifiers changed from: protected */
    public final int MAKE_RGB(int i, int i2, int i3) {
        return ((i & 255) << 16) | ((i2 & 255) << 8) | ((i3 & 255) << 0);
    }

    public void drawImage(int i, float f, float f2, float f3, float f4, float f5, float f6, CRect cRect) {
    }

    public void drawImage(Bitmap bitmap, Matrix matrix) {
    }

    public void drawString(String str, float f, float f2, int i) {
    }

    /* access modifiers changed from: protected */
    public void fillRect(int i, int i2, int i3, int i4) {
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void releaseDC() {
    }

    public void setARGB(int i, int i2, int i3, int i4) {
    }

    public void setAlpha(int i) {
    }

    public void setEvent(Runnable runnable) {
    }

    /* access modifiers changed from: protected */
    public void setFont() {
    }

    public void setRotate(float f, int i, int i2) {
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    public static void setCodeWidth(int i) {
        CODE_WIDTH = i;
    }

    public static int getCodeWidth() {
        return CODE_WIDTH;
    }

    public static void setCodeHeight(int i) {
        CODE_HEIGHT = i;
    }

    public static int getCodeHeight() {
        return CODE_HEIGHT;
    }

    public static void setResWidth(int i) {
        RES_WIDTH = i;
    }

    public static int getResWidth() {
        return RES_WIDTH;
    }

    public static void setResHeight(int i) {
        RES_HEIGHT = i;
    }

    public static int getResHeight() {
        return RES_HEIGHT;
    }

    public static int getDrawWidth() {
        return m_drawWidth;
    }

    public static int getDrawHeight() {
        return m_drawHeight;
    }

    public static void setScaleCode(float f) {
        m_fScaleCode = f;
    }

    public static float getScaleCode() {
        return m_fScaleCode;
    }

    public static void setScaleRes(float f) {
        m_fScaleRes = f;
    }

    public static float getScaleRes() {
        return m_fScaleRes;
    }

    public static int getDblOffsetX() {
        return m_nDblOffsetX;
    }

    public static int getDblOffsetY() {
        return m_nDblOffsetY;
    }

    public int getCodePosX(int i) {
        return (int) Common.screen2code((float) (i - m_nDblOffsetX));
    }

    public int getCodePosY(int i) {
        return (int) Common.screen2code((float) (i - m_nDblOffsetY));
    }

    public static int getBufWidth() {
        return m_bufWidth;
    }

    public static int getBufHeight() {
        return m_bufHeight;
    }

    public CDCView(Context context) {
        super(context);
        init();
    }

    public CDCView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        this.m_holder = getHolder();
        this.m_holder.setType(2);
    }

    public synchronized void update(CEventWnd cEventWnd) {
    }

    public SurfaceHolder getSurfaceHolder() {
        return this.m_holder;
    }

    public void prepareDC() {
        int width = getWidth();
        int height = getHeight();
        SC_WIDTH = width;
        SC_HEIGHT = height;
        m_drawWidth = (int) Common.res2screen((float) RES_WIDTH);
        m_drawHeight = (int) Common.res2screen((float) RES_HEIGHT);
        HALF_WIDTH = RES_WIDTH / 2;
        HALF_HEIGHT = RES_HEIGHT / 2;
        m_nWndOffsetX = 0;
        m_nWndOffsetY = 0;
        m_nDblOffsetX = (width - m_drawWidth) / 2;
        m_nDblOffsetY = (height - m_drawHeight) / 2;
        StringBuilder sb = new StringBuilder();
        sb.append("init(");
        sb.append(width);
        sb.append(", ");
        sb.append(height);
        sb.append(")");
        STD.logout(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Drawing(");
        sb2.append(RES_WIDTH);
        sb2.append(", ");
        sb2.append(RES_HEIGHT);
        sb2.append(")");
        STD.logout(sb2.toString());
        this.m_matrix.postScale(m_fScaleRes, m_fScaleRes);
        createDoubleBuffer();
    }

    private void createDoubleBuffer() {
        m_bufWidth = 1024;
        m_bufHeight = 1024;
        this.m_bmpDblBuffer = Bitmap.createBitmap(m_bufWidth, m_bufHeight, Config.RGB_565);
        this.m_canvas = new Canvas(this.m_bmpDblBuffer);
    }

    public void clear() {
        clear(0);
    }

    public final void clear(int i) {
        setColor(i);
        fillRect(0, 0, m_bufWidth, m_bufHeight);
    }

    public final void setColor(int i) {
        setColor((i >>> 16) & 255, (i >>> 8) & 255, i & 255);
    }

    public final void setColor(int i, int i2, int i3) {
        setARGB(255, i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public final void fillRect(int[] iArr) {
        fillRect(iArr[0], iArr[1], iArr[2], iArr[3]);
    }

    public final void setRotate(float f) {
        setRotate(f, 0, 0);
    }

    public final void drawString(String str, int i, int i2) {
        drawString(str, (float) i, (float) i2, 17);
    }

    public int drawRectString(String str, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8;
        int i9;
        int i10;
        int i11;
        int i12 = i4;
        if (str == null) {
            return -1;
        }
        int length = str.length();
        if (length != 0) {
            int i13 = length - 1;
            int i14 = i5;
            if (i14 <= i13) {
                int i15 = i6 < 0 ? 50 : i6;
                int i16 = this.FONT_H + 6;
                char[] charArray = str.toCharArray();
                String[] strArr = new String[i15];
                int i17 = i14;
                int i18 = 0;
                int i19 = 0;
                while (true) {
                    char c = ' ';
                    if (i18 >= i15 || i17 >= length) {
                        break;
                    }
                    while (true) {
                        if (charArray[i17] != c && charArray[i17] != 10) {
                            break;
                        }
                        i17++;
                        length = length;
                        i15 = i15;
                        c = ' ';
                    }
                    if (this.FONT_H + i19 > i12) {
                        break;
                    }
                    int i20 = 0;
                    while (true) {
                        if (i17 + i20 >= length) {
                            i10 = length;
                            i11 = i15;
                            break;
                        }
                        i20++;
                        if (i3 < 0) {
                            int i21 = (i17 + i20) - 1;
                            i10 = length;
                            i11 = i15;
                            if (!(charArray[i21] == 12290 || charArray[i21] == 65289 || charArray[i21] == '?')) {
                                i20--;
                                break;
                            }
                        } else {
                            i10 = length;
                            i11 = i15;
                        }
                        if (charArray[(i17 + i20) - 1] == 10) {
                            i20--;
                            break;
                        }
                        length = i10;
                        i15 = i11;
                    }
                    strArr[i18] = new String(charArray, i17, i20);
                    if (i20 == 0) {
                        break;
                    }
                    i19 += i16;
                    i17 += i20;
                    i18++;
                    length = i10;
                    i15 = i11;
                }
                int i22 = i7 & 112;
                int i23 = 2;
                if (i22 == 64) {
                    i9 = i + i3;
                    i8 = 65;
                } else if (i22 == 32) {
                    i9 = i + (i3 / 2);
                    i8 = 33;
                } else {
                    i8 = 17;
                    i9 = i;
                }
                int i24 = i7 & 7;
                if (i24 != 1) {
                    i23 = i24 == 2 ? (i12 - i19) / 2 : i12 - i19;
                }
                for (int i25 = 0; i25 < i18; i25++) {
                    drawString(strArr[i25], (float) i9, (float) (i2 + i23), i8);
                    i23 += i16;
                }
                if (i17 > i13) {
                    return -1;
                }
                return i17;
            }
        }
        return -1;
    }

    public final int setFontSize(int i) {
        int code2screen = (int) Common.code2screen((float) i);
        int i2 = this.m_nFontSize;
        if (this.m_nFontSize != code2screen) {
            this.m_nFontSize = code2screen;
            setFont();
        }
        return (int) Common.screen2code((float) i2);
    }

    public final int getFontSize() {
        return (int) Common.screen2code((float) this.m_nFontSize);
    }
}
