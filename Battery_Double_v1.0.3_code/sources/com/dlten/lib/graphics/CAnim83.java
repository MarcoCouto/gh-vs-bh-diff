package com.dlten.lib.graphics;

import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CAnimation;
import com.dlten.lib.frmWork.CEventWnd;

public class CAnim83 extends CAnimation {
    private static final int ANIM_OBJ_COUNT = 5;
    private static final int ANIM_OBJ_GAME = 0;
    private static final int ANIM_OBJ_GTFDATA = 2;
    private static final int ANIM_OBJ_GTFPICTURE = 1;
    private static final int ANIM_OBJ_IMG_OBJ = 4;
    private static final int ANIM_OBJ_PANE = 3;
    public static final int CREATE_ANIM_COUNT = 2;
    public static final int CREATE_ANIM_LINE_ACCELER = 1;
    public static final int CREATE_ANIM_LINE_UNIFORM = 0;
    private int m_nFrameCount = 0;
    private ANIM_ELEMENT[] m_pAnimation = null;

    class ANIM_ELEMENT {
        int animObjType;
        int nParam1;
        int nParam2;
        Object pAnimObj;
        int x;
        int y;

        public ANIM_ELEMENT() {
            Reset();
        }

        public void Reset() {
            this.pAnimObj = null;
            this.animObjType = 0;
            this.x = 0;
            this.y = 0;
            this.nParam1 = 0;
            this.nParam2 = 0;
        }
    }

    class CGtfPicture {
        /* access modifiers changed from: 0000 */
        public void Draw() {
        }

        /* access modifiers changed from: 0000 */
        public void Draw(int i, int i2) {
        }

        /* access modifiers changed from: 0000 */
        public void SetPos(int i, int i2) {
        }

        CGtfPicture() {
        }
    }

    public void ResetAnimation() {
    }

    public CAnim83() {
        ResetAnimation();
    }

    public void SetFrame(int i) {
        int GetCurFrameNum = GetCurFrameNum();
        if (GetCurFrameNum >= 0 && GetCurFrameNum < this.m_nFrameCount) {
            Object obj = this.m_pAnimation[GetCurFrameNum].pAnimObj;
            int i2 = this.m_pAnimation[GetCurFrameNum].x;
            int i3 = this.m_pAnimation[GetCurFrameNum].y;
            if (obj != null) {
                switch (this.m_pAnimation[GetCurFrameNum].animObjType) {
                    case 1:
                        ((CGtfPicture) obj).SetPos(i2, i3);
                        break;
                    case 4:
                        ((CImgObj) obj).moveTo((float) i2, (float) i3);
                        break;
                }
            }
        }
    }

    public void DrawProc() {
        int GetCurFrameNum = GetCurFrameNum();
        if (GetCurFrameNum >= 0 && GetCurFrameNum < this.m_nFrameCount) {
            Object obj = this.m_pAnimation[GetCurFrameNum].pAnimObj;
            if (obj != null) {
                switch (this.m_pAnimation[GetCurFrameNum].animObjType) {
                    case 1:
                        ((CGtfPicture) obj).Draw();
                        break;
                    case 4:
                        ((CImgObj) obj).draw();
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int[] GetPos() {
        int GetCurFrameNum = GetCurFrameNum();
        return new int[]{this.m_pAnimation[GetCurFrameNum].x, this.m_pAnimation[GetCurFrameNum].y};
    }

    public void Create(CEventWnd cEventWnd, int i, CGtfPicture cGtfPicture, int[][] iArr, int i2, int i3, int i4, int i5) {
        Create(cEventWnd, i, cGtfPicture, 1, 0, 0, iArr, i2, i3, i4, i5);
    }

    public void Create(CEventWnd cEventWnd, int i, CImgObj cImgObj, int[][] iArr, int i2, int i3, int i4, int i5) {
        Create(cEventWnd, i, cImgObj, 4, 0, 0, iArr, i2, i3, i4, i5);
    }

    public void Create(CEventWnd cEventWnd, int i, Object obj, int i2, int i3, int i4, int[][] iArr, int i5, int i6, int i7, int i8) {
        boolean z = true;
        int i9 = i5;
        if (i9 <= 1) {
            z = false;
        }
        STD.ASSERT(z);
        ResetAnimation();
        CreateAnimElements(i, obj, i2, i3, i4, iArr, i9, i6, i7, i8);
        switch (i) {
            case 0:
                SetLineAnimProps_Uniform(this.m_pAnimation, i6, iArr, i9, i7);
                break;
            case 1:
                SetLineAnimProps_Accelerated(this.m_pAnimation, i6, iArr, i9, i7);
                break;
        }
        super.Create(cEventWnd, 0, this.m_nFrameCount, false, true);
    }

    public void CreateAnimElements(int i, Object obj, int i2, int i3, int i4, int[][] iArr, int i5, int i6, int i7, int i8) {
        int i9 = i7 + i6;
        this.m_nFrameCount = i8 + i9;
        this.m_pAnimation = null;
        this.m_pAnimation = new ANIM_ELEMENT[this.m_nFrameCount];
        for (int i10 = 0; i10 < this.m_nFrameCount; i10++) {
            this.m_pAnimation[i10] = new ANIM_ELEMENT();
        }
        for (int i11 = 0; i11 < i6; i11++) {
            this.m_pAnimation[i11].pAnimObj = obj;
            this.m_pAnimation[i11].animObjType = i2;
            this.m_pAnimation[i11].x = iArr[0][0];
            this.m_pAnimation[i11].y = iArr[0][1];
            this.m_pAnimation[i11].nParam1 = i3;
            this.m_pAnimation[i11].nParam2 = i4;
        }
        while (i6 < i9) {
            this.m_pAnimation[i6].pAnimObj = obj;
            this.m_pAnimation[i6].animObjType = i2;
            this.m_pAnimation[i6].x = iArr[0][0];
            this.m_pAnimation[i6].y = iArr[0][1];
            this.m_pAnimation[i6].nParam1 = i3;
            this.m_pAnimation[i6].nParam2 = i4;
            i6++;
        }
        while (i9 < this.m_nFrameCount) {
            this.m_pAnimation[i9].pAnimObj = obj;
            this.m_pAnimation[i9].animObjType = i2;
            int i12 = i5 - 1;
            this.m_pAnimation[i9].x = iArr[i12][0];
            this.m_pAnimation[i9].y = iArr[i12][1];
            this.m_pAnimation[i9].nParam1 = i3;
            this.m_pAnimation[i9].nParam2 = i4;
            i9++;
        }
    }

    /* access modifiers changed from: 0000 */
    public void SetLineAnimProps_Uniform(ANIM_ELEMENT[] anim_elementArr, int i, int[][] iArr, int i2, int i3) {
        int i4 = i3;
        int i5 = i2 - 1;
        int[] iArr2 = new int[i5];
        int[] iArr3 = new int[i5];
        int i6 = 0;
        int i7 = 0;
        while (i6 < i5) {
            int i8 = i6 + 1;
            int i9 = iArr[i8][0] - iArr[i6][0];
            int i10 = iArr[i8][1] - iArr[i6][1];
            iArr2[i6] = (i9 * i9) + (i10 * i10);
            i7 += iArr2[i6];
            i6 = i8;
        }
        int i11 = 0;
        for (int i12 = i2 - 2; i12 > 0; i12--) {
            if (i7 == 0) {
                iArr3[i12] = 0;
            } else {
                iArr3[i12] = (iArr2[i12] * i4) / i7;
            }
            i11 += iArr3[i12];
        }
        iArr3[0] = i4 - i11;
        int i13 = 0;
        int i14 = 0;
        while (i13 < i5) {
            int i15 = i14;
            for (int i16 = 0; i16 < iArr3[i13]; i16++) {
                if (i15 >= i4) {
                    STD.ASSERT(false);
                } else {
                    int i17 = i + i15;
                    int i18 = i13 + 1;
                    anim_elementArr[i17].x = iArr[i13][0] + (((iArr[i18][0] - iArr[i13][0]) * i16) / iArr3[i13]);
                    anim_elementArr[i17].y = iArr[i13][1] + (((iArr[i18][1] - iArr[i13][1]) * i16) / iArr3[i13]);
                    i15++;
                }
            }
            i13++;
            i14 = i15;
        }
    }

    /* access modifiers changed from: 0000 */
    public void SetLineAnimProps_Accelerated(ANIM_ELEMENT[] anim_elementArr, int i, int[][] iArr, int i2, int i3) {
        int i4 = i3;
        int i5 = i2 - 1;
        int[] iArr2 = new int[i5];
        int i6 = 0;
        for (int i7 = i5 - 1; i7 > 0; i7--) {
            iArr2[i7] = ((i4 * i7) / i5) - (((i7 - 1) * i4) / i5);
            i6 += iArr2[i7];
        }
        iArr2[0] = i4 - i6;
        int i8 = 0;
        int i9 = 0;
        while (i8 < i5) {
            int i10 = i9;
            for (int i11 = 0; i11 < iArr2[i8]; i11++) {
                if (i10 >= i4) {
                    STD.ASSERT(false);
                } else {
                    int i12 = i + i10;
                    int i13 = i8 + 1;
                    anim_elementArr[i12].x = iArr[i8][0] + (((iArr[i13][0] - iArr[i8][0]) * i11) / iArr2[i8]);
                    anim_elementArr[i12].y = iArr[i8][1] + (((iArr[i13][1] - iArr[i8][1]) * i11) / iArr2[i8]);
                    i10++;
                }
            }
            i8++;
            i9 = i10;
        }
    }
}
