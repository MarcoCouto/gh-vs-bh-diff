package com.dlten.lib.opengl;

public abstract class Renderable {
    public float height;
    public float width;
    public float x;
    public float y;
    public float z;
}
