package com.dlten.lib.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.opengl.GLES10;
import android.opengl.GLU;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.graphics.CBmpDCView;
import com.dlten.lib.graphics.CImageTexture;
import com.dlten.lib.graphics.CRect;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class CBeijingBmpGLDCView extends CBmpDCView {
    private static final int MAX_RECT_COUNT = 200;
    private static final Semaphore sEglSemaphore = new Semaphore(1);
    private GL10 GLThread_local_gl = null;
    private boolean GLThread_local_tellRendererSurfaceChanged;
    private boolean GLThread_local_tellRendererSurfaceCreated;
    private FloatBuffer mColorBuffer0;
    private boolean mContextLost;
    private volatile boolean mDone;
    private EglHelper mEglHelper;
    private FloatBuffer mFVertexBuffer;
    private FloatBuffer mFVertexBuffer0;
    private boolean mHasFocus;
    private boolean mHasSurface;
    private int mHeight;
    private ShortBuffer mIndexBuffer;
    private ShortBuffer mIndexBuffer0;
    private boolean mPaused;
    private boolean mSizeChanged = true;
    private FloatBuffer mTexBuffer;
    private int mWidth;
    private Bitmap m_CurTextureBitmap;
    private Canvas m_CurTextureCanvas;
    private ByteBuffer m_cbb0 = ByteBuffer.allocateDirect(64);
    private float m_fAlpha;
    private ByteBuffer m_ibb = ByteBuffer.allocateDirect(1600);
    private ByteBuffer m_ibb0 = ByteBuffer.allocateDirect(8);
    CImageTexture m_img = new CImageTexture();
    private ByteBuffer m_tbb = ByteBuffer.allocateDirect(6400);
    private ByteBuffer m_vbb = ByteBuffer.allocateDirect(9600);
    private ByteBuffer m_vbb0 = ByteBuffer.allocateDirect(48);

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start(int[] iArr) {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            this.mEgl.eglChooseConfig(this.mEglDisplay, iArr, eGLConfigArr, 1, new int[1]);
            this.mEglConfig = eGLConfigArr[0];
            this.mEglContext = this.mEgl.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL10.EGL_NO_CONTEXT, null);
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder surfaceHolder) {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = this.mEgl.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, surfaceHolder, null);
            this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
            return this.mEglContext.getGL();
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void finish() {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
            if (this.mEglContext != null) {
                this.mEgl.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }

    public CBeijingBmpGLDCView(Context context) {
        super(context);
        initCBaseView();
    }

    public CBeijingBmpGLDCView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initCBaseView();
    }

    private void initCBaseView() {
        GLThread_Constructor();
        this.m_vbb.order(ByteOrder.nativeOrder());
        this.m_tbb.order(ByteOrder.nativeOrder());
        this.m_ibb.order(ByteOrder.nativeOrder());
        this.m_vbb0.order(ByteOrder.nativeOrder());
        this.m_cbb0.order(ByteOrder.nativeOrder());
        this.m_ibb0.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer0 = this.m_vbb0.asFloatBuffer();
        this.mIndexBuffer0 = this.m_ibb0.asShortBuffer();
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.position(0);
        this.mIndexBuffer0.put(0);
        this.mIndexBuffer0.put(1);
        this.mIndexBuffer0.put(2);
        this.mIndexBuffer0.put(3);
        this.mIndexBuffer0.position(0);
    }

    public void prepareDC() {
        super.prepareDC();
        GLThread_run_start();
    }

    public void releaseDC() {
        GLThread_run_end();
    }

    public final synchronized void update(CEventWnd cEventWnd) {
        if (cEventWnd != null) {
            try {
                GLThread_run_loop();
                if (surfaceInitialized()) {
                    clear();
                    GLES10.glActiveTexture(33984);
                    cEventWnd.DrawPrevProc();
                    cEventWnd.DrawProcess();
                    this.m_img.load(this.m_bmpDblBuffer);
                    this.m_img.drawImage(0.0f, 0.0f, 1.0f, 1.0f, 255, (Matrix) null);
                }
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        } else {
            return;
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        GLThread_onWindowResize(i2, i3);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        GLThread_surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        GLThread_surfaceDestroyed();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        GLThread_onWindowFocusChanged(z);
    }

    public void GLThread_surfaceCreated() {
        synchronized (this) {
            this.mHasSurface = true;
            this.mContextLost = false;
            notify();
        }
    }

    public void GLThread_surfaceDestroyed() {
        synchronized (this) {
            this.mHasSurface = false;
            notify();
        }
    }

    public void GLThread_onWindowResize(int i, int i2) {
        synchronized (this) {
            this.mWidth = i;
            this.mHeight = i2;
            this.mSizeChanged = true;
        }
    }

    public void GLThread_onWindowFocusChanged(boolean z) {
        this.mHasFocus = true;
    }

    public void GLThread_Constructor() {
        this.mDone = false;
        this.mWidth = 0;
        this.mHeight = 0;
    }

    private boolean GLThread_needToWait() {
        return (this.mPaused || !this.mHasFocus || !this.mHasSurface || this.mContextLost) && !this.mDone;
    }

    public void GLThread_run_start() {
        try {
            sEglSemaphore.acquire();
            this.mEglHelper = new EglHelper();
            this.mEglHelper.start(null);
            this.GLThread_local_gl = null;
            this.GLThread_local_tellRendererSurfaceCreated = true;
            this.GLThread_local_tellRendererSurfaceChanged = true;
        } catch (InterruptedException unused) {
        } catch (Exception unused2) {
        }
    }

    public void GLThread_run_end() {
        this.mEglHelper.finish();
        sEglSemaphore.release();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        if (r0 == false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0035, code lost:
        r6.mEglHelper.start(null);
        r6.GLThread_local_tellRendererSurfaceCreated = true;
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003e, code lost:
        if (r3 == false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
        r6.GLThread_local_gl = (javax.microedition.khronos.opengles.GL10) r6.mEglHelper.createSurface(r6.m_holder);
        r6.GLThread_local_tellRendererSurfaceChanged = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0050, code lost:
        if (r6.GLThread_local_tellRendererSurfaceCreated == false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0052, code lost:
        setCamera();
        r6.GLThread_local_tellRendererSurfaceCreated = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0059, code lost:
        if (r6.GLThread_local_tellRendererSurfaceChanged == false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005b, code lost:
        r6.GLThread_local_tellRendererSurfaceChanged = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005d, code lost:
        if (r4 <= 0) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005f, code lost:
        if (r5 <= 0) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0061, code lost:
        r6.mEglHelper.swap();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0066, code lost:
        return;
     */
    public void GLThread_run_loop() throws InterruptedException {
        boolean z;
        if (!this.mDone) {
            synchronized (this) {
                if (this.mPaused) {
                    this.mEglHelper.finish();
                    z = true;
                } else {
                    z = false;
                }
                if (GLThread_needToWait()) {
                    while (GLThread_needToWait()) {
                        wait();
                    }
                }
                if (!this.mDone) {
                    boolean z2 = this.mSizeChanged;
                    int i = this.mWidth;
                    int i2 = this.mHeight;
                    this.mSizeChanged = false;
                }
            }
        }
    }

    public boolean surfaceInitialized() {
        return this.GLThread_local_gl != null;
    }

    public int createTexture(int i, int i2) {
        try {
            this.m_CurTextureBitmap = Bitmap.createBitmap(i, i2, Config.ARGB_4444);
        } catch (Exception unused) {
        }
        this.m_CurTextureCanvas = new Canvas(this.m_CurTextureBitmap);
        GLES10.glDisable(3024);
        GLES10.glHint(3152, 4353);
        GLES10.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES10.glShadeModel(7425);
        GLES10.glEnable(2929);
        GLES10.glEnable(3553);
        int[] iArr = new int[1];
        GLES10.glGenTextures(1, iArr, 0);
        int i3 = iArr[0];
        GLES10.glBindTexture(3553, i3);
        GLES10.glTexParameterf(3553, 10241, 9729.0f);
        GLES10.glTexParameterf(3553, 10240, 9729.0f);
        GLES10.glTexParameterf(3553, 10242, 33071.0f);
        GLES10.glTexParameterf(3553, 10243, 33071.0f);
        GLES10.glTexEnvf(8960, 8704, 7681.0f);
        return i3;
    }

    public void deleteTexture(int i) {
        GLES10.glDeleteTextures(1, new int[]{i}, 0);
    }

    private void setCamera() {
        GLES10.glViewport(m_nDblOffsetX, m_nDblOffsetY, SC_WIDTH - (m_nDblOffsetX * 2), SC_HEIGHT - (2 * m_nDblOffsetY));
        GLES10.glMatrixMode(5889);
        GLES10.glLoadIdentity();
        GLES10.glFrustumf(-0.25f, 0.25f, -0.25f, 0.25f, 2.5f, 5.5f);
    }

    public void setTexture(int i) {
        GLES10.glBindTexture(3553, i);
    }

    public void clear() {
        GLES10.glDisable(3024);
        GLES10.glTexEnvx(8960, 8704, 8448);
        GLES10.glClear(16640);
        GLES10.glMatrixMode(5888);
        GLES10.glLoadIdentity();
        GLU.gluLookAt(this.GLThread_local_gl, 0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        GLES10.glEnableClientState(32884);
        GLES10.glEnableClientState(32888);
        this.mFVertexBuffer = this.m_vbb.asFloatBuffer();
        this.mTexBuffer = this.m_tbb.asFloatBuffer();
        this.mIndexBuffer = this.m_ibb.asShortBuffer();
        GLES10.glEnable(3042);
        GLES10.glBlendFunc(770, 771);
        this.m_fAlpha = 1.0f;
    }

    public void drawImage(int i, float f, float f2, float f3, float f4, float f5, float f6, CRect cRect) {
        GLES10.glBindTexture(3553, i);
        float f7 = (f / ((float) RES_WIDTH)) - 0.5f;
        float f8 = 0.5f - (f2 / ((float) RES_HEIGHT));
        float f9 = ((f + (f3 * f5)) / ((float) RES_WIDTH)) - 0.5f;
        float f10 = 0.5f - ((f2 + (f4 * f6)) / ((float) RES_HEIGHT));
        this.mFVertexBuffer.put(f7);
        this.mFVertexBuffer.put(f8);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(f7);
        this.mFVertexBuffer.put(f10);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(f9);
        this.mFVertexBuffer.put(f10);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(f9);
        this.mFVertexBuffer.put(f8);
        this.mFVertexBuffer.put(0.0f);
        float f11 = cRect.left;
        float f12 = cRect.top;
        float Right = cRect.Right();
        float Bottom = cRect.Bottom();
        this.mTexBuffer.put(f11);
        this.mTexBuffer.put(f12);
        this.mTexBuffer.put(f11);
        this.mTexBuffer.put(Bottom);
        this.mTexBuffer.put(Right);
        this.mTexBuffer.put(Bottom);
        this.mTexBuffer.put(Right);
        this.mTexBuffer.put(f12);
        this.mIndexBuffer.put(0);
        this.mIndexBuffer.put(1);
        this.mIndexBuffer.put(2);
        this.mIndexBuffer.put(3);
        this.mFVertexBuffer.position(0);
        this.mTexBuffer.position(0);
        GLES10.glFrontFace(2304);
        GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer);
        GLES10.glEnable(3553);
        GLES10.glTexCoordPointer(2, 5126, 0, this.mTexBuffer);
        this.mIndexBuffer.position(0);
        GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
        if (this.m_fAlpha != 1.0f) {
            this.mColorBuffer0 = this.m_cbb0.asFloatBuffer();
            for (int i2 = 0; i2 < 4; i2++) {
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(1.0f - this.m_fAlpha);
            }
            this.mColorBuffer0.position(0);
            GLES10.glDisableClientState(32888);
            GLES10.glEnableClientState(32886);
            GLES10.glDisable(3553);
            GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer0);
            GLES10.glColorPointer(4, 5126, 0, this.mColorBuffer0);
            GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer0);
            GLES10.glEnableClientState(32888);
            GLES10.glDisableClientState(32886);
            GLES10.glEnable(3553);
        }
    }

    public void setAlpha(int i) {
        this.m_fAlpha = ((float) i) / 255.0f;
    }
}
