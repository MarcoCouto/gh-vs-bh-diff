package com.dlten.lib.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.opengl.GLUtils;
import android.util.Log;
import com.dlten.lib.graphics.CDCView;
import com.dlten.lib.opengl.CMyGLDCView.Renderer;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class SimpleGLRenderer implements Renderer {
    private static Options sBitmapOptions = new Options();
    private Context mContext;
    private int[] mCropWorkspace;
    private GLSprite[] mSprites;
    private int[] mTextureNameWorkspace;
    private boolean mUseHardwareBuffers;
    private boolean mUseVerts;
    private CMyGLDCView m_parent;

    public SimpleGLRenderer(Context context, CMyGLDCView cMyGLDCView) {
        this.m_parent = null;
        this.mTextureNameWorkspace = new int[1];
        this.mCropWorkspace = new int[4];
        sBitmapOptions.inPreferredConfig = Config.RGB_565;
        this.mContext = context;
        this.m_parent = cMyGLDCView;
    }

    public int[] getConfigSpec() {
        return new int[]{12325, 0, 12344};
    }

    public void setSprites(GLSprite[] gLSpriteArr) {
        this.mSprites = gLSpriteArr;
    }

    public void setVertMode(boolean z, boolean z2) {
        this.mUseVerts = z;
        if (!z) {
            z2 = false;
        }
        this.mUseHardwareBuffers = z2;
    }

    public void drawFrame(GL10 gl10) {
        if (this.mSprites != null) {
            deleteSprites(gl10);
            createSprites(gl10);
            gl10.glMatrixMode(5888);
            if (this.mUseVerts) {
                Grid.beginDrawing(gl10, true, false);
            }
            for (GLSprite draw : this.mSprites) {
                draw.draw(gl10);
            }
            if (this.mUseVerts) {
                Grid.endDrawing(gl10);
            }
        }
    }

    public void sizeChanged(GL10 gl10, int i, int i2) {
        int resWidth = CDCView.getResWidth();
        CDCView.getResHeight();
        CDCView.getBufWidth();
        int bufHeight = CDCView.getBufHeight();
        gl10.glViewport(0, (i2 - bufHeight) - CDCView.getDblOffsetY(), resWidth, bufHeight);
        gl10.glMatrixMode(5889);
        gl10.glLoadIdentity();
        gl10.glOrthof(0.0f, (float) resWidth, 0.0f, (float) bufHeight, 0.0f, 1.0f);
        gl10.glShadeModel(7424);
        gl10.glEnable(3042);
        gl10.glBlendFunc(770, 771);
        gl10.glColor4x(65536, 65536, 65536, 65536);
        gl10.glEnable(3553);
    }

    public void surfaceCreated(GL10 gl10) {
        gl10.glHint(3152, 4353);
        gl10.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl10.glShadeModel(7424);
        gl10.glDisable(2929);
        gl10.glEnable(3553);
        gl10.glDisable(3024);
        gl10.glDisable(2896);
        gl10.glClear(16640);
        createSprites(gl10);
    }

    public void shutdown(GL10 gl10) {
        deleteSprites(gl10);
    }

    /* access modifiers changed from: protected */
    public int loadBitmap(Context context, GL10 gl10, Bitmap bitmap) {
        if (context == null || gl10 == null || bitmap == null) {
            return -1;
        }
        gl10.glGenTextures(1, this.mTextureNameWorkspace, 0);
        int i = this.mTextureNameWorkspace[0];
        gl10.glBindTexture(3553, i);
        gl10.glTexParameterf(3553, 10241, 9729.0f);
        gl10.glTexParameterf(3553, 10240, 9729.0f);
        gl10.glTexEnvf(8960, 8704, 7681.0f);
        GLUtils.texImage2D(3553, 0, bitmap, 0);
        this.mCropWorkspace[0] = 0;
        this.mCropWorkspace[1] = bitmap.getHeight();
        this.mCropWorkspace[2] = bitmap.getWidth();
        this.mCropWorkspace[3] = -bitmap.getHeight();
        ((GL11) gl10).glTexParameteriv(3553, 35741, this.mCropWorkspace, 0);
        int glGetError = gl10.glGetError();
        if (glGetError == 0) {
            return i;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Texture Load GLError: ");
        sb.append(glGetError);
        Log.e("SpriteMethodTest", sb.toString());
        return i;
    }

    /* access modifiers changed from: protected */
    public int loadBitmap(Context context, GL10 gl10, int i) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(openRawResource, null, sBitmapOptions);
            int loadBitmap = loadBitmap(context, gl10, decodeStream);
            decodeStream.recycle();
            return loadBitmap;
        } finally {
            try {
                openRawResource.close();
            } catch (IOException unused) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void changeBitmap(GL10 gl10, int i, Bitmap bitmap) {
        if (gl10 != null && i != -1 && bitmap != null) {
            gl10.glBindTexture(3553, i);
            gl10.glCopyTexImage2D(3553, 0, 6408, 0, 0, bitmap.getWidth(), bitmap.getHeight(), 0);
        }
    }

    private void createSprites(GL10 gl10) {
        int loadBitmap;
        if (this.mSprites != null) {
            if (this.mUseHardwareBuffers) {
                for (GLSprite grid : this.mSprites) {
                    grid.getGrid().invalidateHardwareBuffers();
                }
            }
            for (int i = 0; i < this.mSprites.length; i++) {
                this.mSprites[i].getResourceId();
                Bitmap bmp = this.mSprites[i].getBmp();
                synchronized (bmp) {
                    loadBitmap = loadBitmap(this.mContext, gl10, bmp);
                }
                this.mSprites[i].setTextureName(loadBitmap);
                if (this.mUseHardwareBuffers) {
                    Grid grid2 = this.mSprites[i].getGrid();
                    if (!grid2.usingHardwareBuffers()) {
                        grid2.generateHardwareBuffers(gl10);
                    }
                }
            }
        }
    }

    private void changeSprites(GL10 gl10) {
        if (this.mSprites != null) {
            for (int i = 0; i < this.mSprites.length; i++) {
                int textureName = this.mSprites[i].getTextureName();
                if (textureName != -1) {
                    Bitmap bmp = this.mSprites[i].getBmp();
                    synchronized (bmp) {
                        changeBitmap(gl10, textureName, bmp);
                    }
                }
            }
        }
    }

    private void deleteSprites(GL10 gl10) {
        if (this.mSprites != null) {
            int[] iArr = new int[1];
            for (int i = 0; i < this.mSprites.length; i++) {
                this.mSprites[i].getResourceId();
                iArr[0] = this.mSprites[i].getTextureName();
                gl10.glDeleteTextures(1, iArr, 0);
                this.mSprites[i].setTextureName(0);
                if (this.mUseHardwareBuffers) {
                    this.mSprites[i].getGrid().releaseHardwareBuffers(gl10);
                }
            }
        }
    }
}
