package com.dlten.lib.file;

import android.content.Context;
import com.dlten.lib.STD;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

public class CConFile {
    private static Context m_context;

    public static void Initialize(Context context) {
        m_context = context;
        CResFile.Initialize(context);
    }

    public static Context getAppContext() {
        return m_context;
    }

    public static final boolean isFileExist(String str) {
        try {
            FileInputStream openFileInput = m_context.openFileInput(str);
            if (openFileInput == null) {
                return false;
            }
            openFileInput.close();
            return true;
        } catch (FileNotFoundException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("isFileExist : file not exist. ");
            sb.append(str);
            STD.logout(sb.toString());
            return false;
        } catch (IOException unused2) {
            return true;
        } catch (Exception unused3) {
            return false;
        }
    }

    public static final boolean write(String str, byte[] bArr) {
        byte[] bArr2 = new byte[(bArr.length + 8)];
        byte[] bArr3 = new byte[8];
        STD.Long2Bytes(bArr3, 0, System.currentTimeMillis());
        for (int i = 0; i < 8; i++) {
            bArr2[i] = bArr3[i];
        }
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr2[i2 + 8] = bArr[i2];
        }
        try {
            FileOutputStream openFileOutput = m_context.openFileOutput(str, 0);
            if (openFileOutput == null) {
                return false;
            }
            openFileOutput.write(bArr2);
            openFileOutput.close();
            return true;
        } catch (FileNotFoundException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("saveFile : file not found. ");
            sb.append(str);
            STD.logout(sb.toString());
            return false;
        } catch (Exception unused2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("saveFile : file writing failed. ");
            sb2.append(str);
            STD.logout(sb2.toString());
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[RETURN] */
    public static final byte[] read(String str) {
        byte[] bArr;
        try {
            FileInputStream openFileInput = m_context.openFileInput(str);
            if (openFileInput == null) {
                return null;
            }
            byte[] bArr2 = new byte[openFileInput.available()];
            openFileInput.read(bArr2);
            openFileInput.close();
            int i = 0;
            if (bArr2 != null) {
                if (bArr2.length > 8) {
                    bArr = new byte[(bArr2.length - 8)];
                    while (i < bArr.length) {
                        bArr[i] = bArr2[i + 8];
                        i++;
                    }
                    i = 1;
                    if (1 != i) {
                        return bArr;
                    }
                    return null;
                }
            }
            bArr = null;
            if (1 != i) {
            }
        } catch (FileNotFoundException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("readFile : file not found. ");
            sb.append(str);
            STD.logout(sb.toString());
            return null;
        } catch (Exception unused2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("readFile : file reading failed. ");
            sb2.append(str);
            STD.logout(sb2.toString());
            return null;
        }
    }

    public static final boolean delete(String str) {
        return m_context.deleteFile(str);
    }

    public static final Calendar getFileTime(String str) {
        Calendar calendar = null;
        try {
            FileInputStream openFileInput = m_context.openFileInput(str);
            if (openFileInput == null) {
                return null;
            }
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            Calendar instance = Calendar.getInstance();
            long Bytes2Long = STD.Bytes2Long(bArr, 0);
            if (Bytes2Long > 0) {
                instance.setTime(new Date(Bytes2Long));
                calendar = instance;
            }
            return calendar;
        } catch (FileNotFoundException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("readFile : file not found. ");
            sb.append(str);
            STD.logout(sb.toString());
            return null;
        } catch (Exception unused2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("readFile : file reading failed. ");
            sb2.append(str);
            STD.logout(sb2.toString());
            return null;
        }
    }

    public static byte[] loadBinaryRes(int i) {
        byte[] bArr = null;
        try {
            InputStream openRawResource = m_context.getResources().openRawResource(i);
            if (openRawResource == null) {
                return null;
            }
            byte[] bArr2 = new byte[openRawResource.available()];
            openRawResource.read(bArr2);
            openRawResource.close();
            bArr = bArr2;
            return bArr;
        } catch (Exception unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("reading binary error. nResID = ");
            sb.append(i);
            STD.logout(sb.toString());
        }
    }

    public static byte[] loadBinaryAssets(String str) {
        byte[] bArr = null;
        try {
            InputStream open = m_context.getAssets().open(str);
            if (open == null) {
                return null;
            }
            byte[] bArr2 = new byte[open.available()];
            open.read(bArr2);
            open.close();
            bArr = bArr2;
            return bArr;
        } catch (Exception unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("strFileName = ");
            sb.append(str);
            STD.logout(sb.toString());
        }
    }
}
