package com.revmob.ads.interstitial.client;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import com.revmob.ads.internal.CloseAnimationConfiguration;
import com.revmob.ads.internal.ShowAnimationConfiguration;
import com.revmob.ads.internal.StaticAssets;
import com.revmob.client.RevMobClient;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FullscreenClientListener extends AdRevMobClientListener {
    private static Activity activity;
    private static boolean success;
    private boolean shallUpdateData = true;

    public FullscreenClientListener(Ad ad, RevMobAdsListener revMobAdsListener, Activity activity2) {
        super(ad, revMobAdsListener);
        activity = activity2;
    }

    public void handleResponse(String str) throws JSONException {
        FullscreenData createData = createData(str, this.publisherListener);
        if (createData != null && this.shallUpdateData) {
            RevMobClient.sett3(System.currentTimeMillis());
            this.ad.updateWithData(createData);
        } else if (!this.shallUpdateData) {
            this.shallUpdateData = true;
        } else if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdNotReceived("An error occurred during the ad download.");
        }
    }

    /* JADX WARNING: type inference failed for: r3v8, types: [int] */
    /* JADX WARNING: type inference failed for: r68v0 */
    /* JADX WARNING: type inference failed for: r64v0, types: [boolean] */
    /* JADX WARNING: type inference failed for: r63v0, types: [int] */
    /* JADX WARNING: type inference failed for: r5v7 */
    /* JADX WARNING: type inference failed for: r64v1 */
    /* JADX WARNING: type inference failed for: r63v1 */
    /* JADX WARNING: type inference failed for: r5v9 */
    /* JADX WARNING: type inference failed for: r64v2 */
    /* JADX WARNING: type inference failed for: r63v2 */
    /* JADX WARNING: type inference failed for: r5v10 */
    /* JADX WARNING: type inference failed for: r5v11 */
    /* JADX WARNING: type inference failed for: r64v3 */
    /* JADX WARNING: type inference failed for: r63v3 */
    /* JADX WARNING: type inference failed for: r64v4 */
    /* JADX WARNING: type inference failed for: r63v4 */
    /* JADX WARNING: type inference failed for: r64v5 */
    /* JADX WARNING: type inference failed for: r63v5 */
    /* JADX WARNING: type inference failed for: r5v12 */
    /* JADX WARNING: type inference failed for: r64v6 */
    /* JADX WARNING: type inference failed for: r63v6 */
    /* JADX WARNING: type inference failed for: r5v13 */
    /* JADX WARNING: type inference failed for: r5v14 */
    /* JADX WARNING: type inference failed for: r64v7 */
    /* JADX WARNING: type inference failed for: r63v7 */
    /* JADX WARNING: type inference failed for: r5v15 */
    /* JADX WARNING: type inference failed for: r64v8 */
    /* JADX WARNING: type inference failed for: r63v8 */
    /* JADX WARNING: type inference failed for: r64v9 */
    /* JADX WARNING: type inference failed for: r63v9 */
    /* JADX WARNING: type inference failed for: r11v10 */
    /* JADX WARNING: type inference failed for: r4v31 */
    /* JADX WARNING: type inference failed for: r3v26 */
    /* JADX WARNING: type inference failed for: r64v10 */
    /* JADX WARNING: type inference failed for: r63v10 */
    /* JADX WARNING: type inference failed for: r5v17 */
    /* JADX WARNING: type inference failed for: r64v11 */
    /* JADX WARNING: type inference failed for: r63v11 */
    /* JADX WARNING: type inference failed for: r5v18 */
    /* JADX WARNING: type inference failed for: r87v0 */
    /* JADX WARNING: type inference failed for: r64v12 */
    /* JADX WARNING: type inference failed for: r5v19 */
    /* JADX WARNING: type inference failed for: r63v12 */
    /* JADX WARNING: type inference failed for: r87v1 */
    /* JADX WARNING: type inference failed for: r64v13 */
    /* JADX WARNING: type inference failed for: r5v20 */
    /* JADX WARNING: type inference failed for: r63v13 */
    /* JADX WARNING: type inference failed for: r87v2 */
    /* JADX WARNING: type inference failed for: r64v14 */
    /* JADX WARNING: type inference failed for: r5v21 */
    /* JADX WARNING: type inference failed for: r63v14 */
    /* JADX WARNING: type inference failed for: r11v16 */
    /* JADX WARNING: type inference failed for: r3v33 */
    /* JADX WARNING: type inference failed for: r4v32 */
    /* JADX WARNING: type inference failed for: r5v24 */
    /* JADX WARNING: type inference failed for: r4v34 */
    /* JADX WARNING: type inference failed for: r3v34 */
    /* JADX WARNING: type inference failed for: r1v53 */
    /* JADX WARNING: type inference failed for: r85v0 */
    /* JADX WARNING: type inference failed for: r11v18 */
    /* JADX WARNING: type inference failed for: r85v1 */
    /* JADX WARNING: type inference failed for: r85v2 */
    /* JADX WARNING: type inference failed for: r85v3 */
    /* JADX WARNING: type inference failed for: r85v4 */
    /* JADX WARNING: type inference failed for: r11v19 */
    /* JADX WARNING: type inference failed for: r85v5 */
    /* JADX WARNING: type inference failed for: r5v30 */
    /* JADX WARNING: type inference failed for: r4v35 */
    /* JADX WARNING: type inference failed for: r3v35 */
    /* JADX WARNING: type inference failed for: r1v66 */
    /* JADX WARNING: type inference failed for: r5v31 */
    /* JADX WARNING: type inference failed for: r1v67 */
    /* JADX WARNING: type inference failed for: r3v36 */
    /* JADX WARNING: type inference failed for: r4v37 */
    /* JADX WARNING: type inference failed for: r5v32 */
    /* JADX WARNING: type inference failed for: r5v33 */
    /* JADX WARNING: type inference failed for: r5v35 */
    /* JADX WARNING: type inference failed for: r3v37 */
    /* JADX WARNING: type inference failed for: r1v68 */
    /* JADX WARNING: type inference failed for: r4v40 */
    /* JADX WARNING: type inference failed for: r5v36 */
    /* JADX WARNING: type inference failed for: r64v15 */
    /* JADX WARNING: type inference failed for: r63v15 */
    /* JADX WARNING: type inference failed for: r5v43 */
    /* JADX WARNING: type inference failed for: r64v16 */
    /* JADX WARNING: type inference failed for: r63v16 */
    /* JADX WARNING: type inference failed for: r5v44 */
    /* JADX WARNING: type inference failed for: r5v45 */
    /* JADX WARNING: type inference failed for: r5v46 */
    /* JADX WARNING: type inference failed for: r64v17 */
    /* JADX WARNING: type inference failed for: r63v17 */
    /* JADX WARNING: type inference failed for: r5v47 */
    /* JADX WARNING: type inference failed for: r64v18 */
    /* JADX WARNING: type inference failed for: r63v18 */
    /* JADX WARNING: type inference failed for: r5v48 */
    /* JADX WARNING: type inference failed for: r5v49 */
    /* JADX WARNING: type inference failed for: r5v50 */
    /* JADX WARNING: type inference failed for: r5v51 */
    /* JADX WARNING: type inference failed for: r64v19 */
    /* JADX WARNING: type inference failed for: r63v19 */
    /* JADX WARNING: type inference failed for: r5v52 */
    /* JADX WARNING: type inference failed for: r64v20 */
    /* JADX WARNING: type inference failed for: r63v20 */
    /* JADX WARNING: type inference failed for: r5v53 */
    /* JADX WARNING: type inference failed for: r4v52 */
    /* JADX WARNING: type inference failed for: r4v53 */
    /* JADX WARNING: type inference failed for: r4v54 */
    /* JADX WARNING: type inference failed for: r3v60 */
    /* JADX WARNING: type inference failed for: r3v61 */
    /* JADX WARNING: type inference failed for: r85v6 */
    /* JADX WARNING: type inference failed for: r11v26 */
    /* JADX WARNING: type inference failed for: r85v7 */
    /* JADX WARNING: type inference failed for: r5v54 */
    /* JADX WARNING: type inference failed for: r1v73 */
    /* JADX WARNING: type inference failed for: r5v55 */
    /* JADX WARNING: type inference failed for: r1v74 */
    /* JADX WARNING: type inference failed for: r4v55 */
    /* JADX WARNING: type inference failed for: r5v56 */
    /* JADX WARNING: type inference failed for: r5v57 */
    /* JADX WARNING: type inference failed for: r1v75 */
    /* JADX WARNING: type inference failed for: r4v56 */
    /* JADX WARNING: type inference failed for: r5v58 */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x030d, code lost:
        if (r4 == 1) goto L_0x030f;
     */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r64v1
  assigns: []
  uses: []
  mth insns count: 654
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0434  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0458  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x045c  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0487  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0494  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x04a7  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x04b8  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x04bc  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x04cd  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x04d1  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x04e2  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x04eb  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x04fd  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0504  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x05a9  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x05bf  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x05c4  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x05cc  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x05f2  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0242 A[Catch:{ JSONException -> 0x026c }, LOOP:0: B:48:0x023c->B:50:0x0242, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0259 A[Catch:{ JSONException -> 0x026c }, LOOP:1: B:52:0x0253->B:54:0x0259, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0280 A[Catch:{ JSONException -> 0x02a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x02b6 A[ADDED_TO_REGION] */
    /* JADX WARNING: Unknown variable types count: 49 */
    public static FullscreenData createData(String str, RevMobAdsListener revMobAdsListener) throws JSONException {
        String str2;
        int[] iArr;
        String str3;
        ShowAnimationConfiguration showAnimationConfiguration;
        CloseAnimationConfiguration closeAnimationConfiguration;
        int i;
        ? r64;
        ? r63;
        int i2;
        int i3;
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        String str4;
        ? r5;
        Bitmap bitmap4;
        boolean z;
        Bitmap bitmap5;
        Bitmap bitmap6;
        boolean z2;
        Bitmap bitmap7;
        String str5;
        double d;
        double d2;
        double d3;
        double d4;
        Bitmap bitmap8;
        Bitmap bitmap9;
        Bitmap bitmap10;
        File file;
        Bitmap bitmap11;
        Bitmap bitmap12;
        double d5;
        Bitmap bitmap13;
        double d6;
        Bitmap bitmap14;
        ? r642;
        ? r632;
        int i4;
        int i5;
        ? r52;
        Bitmap bitmap15;
        boolean z3;
        Bitmap bitmap16;
        ? r643;
        ? r633;
        ? r53;
        String str6;
        String str7;
        ? r644;
        ? r634;
        ? r54;
        ? r645;
        ? r635;
        ? r55;
        int i6;
        Bitmap bitmap17;
        ? r11;
        Bitmap bitmap18;
        Bitmap bitmap19;
        int i7;
        ? r4;
        ? r3;
        ? r646;
        ? r636;
        ? r56;
        ? r647;
        ? r637;
        ? r57;
        ? r58;
        ? r42;
        ? r32;
        ? r1;
        ? r85;
        char c;
        ? r59;
        ? r33;
        int i8;
        JSONArray jSONArray;
        int i9;
        JSONArray jSONArray2;
        int i10;
        JSONArray jSONArray3;
        JSONArray jSONArray4;
        int i11;
        JSONObject jSONObject = new JSONObject(str).getJSONObject("fullscreen");
        String appOrSite = getAppOrSite(jSONObject);
        boolean openInside = getOpenInside(jSONObject);
        JSONArray jSONArray5 = jSONObject.getJSONArray("links");
        int i12 = activity.getResources().getConfiguration().orientation;
        String clickUrl = getClickUrl(jSONArray5);
        boolean followRedirect = getFollowRedirect(jSONObject);
        String impressionUrl = getImpressionUrl(jSONArray5);
        String linkByRel = getLinkByRel(jSONArray5, "html");
        String linkByRel2 = getLinkByRel(jSONArray5, "dsp_url");
        String linkByRel3 = getLinkByRel(jSONArray5, "dsp_html");
        String linkByRel4 = getLinkByRel(jSONArray5, "image_portrait");
        String linkByRel5 = getLinkByRel(jSONArray5, "image_landscape");
        String linkByRel6 = getLinkByRel(jSONArray5, "framePortrait");
        String linkByRel7 = getLinkByRel(jSONArray5, "frameLandscape");
        String stringKeyByRel = getStringKeyByRel(jSONArray5, "preRoll", "landscape");
        String str8 = appOrSite;
        String stringKeyByRel2 = getStringKeyByRel(jSONArray5, "preRoll", "portrait");
        boolean z4 = followRedirect;
        String stringKeyByRel3 = getStringKeyByRel(jSONArray5, "preRoll", "message");
        int intKeyByRel = getIntKeyByRel(jSONArray5, "preRoll", "red");
        int intKeyByRel2 = getIntKeyByRel(jSONArray5, "preRoll", "green");
        int intKeyByRel3 = getIntKeyByRel(jSONArray5, "preRoll", "blue");
        int intKeyByRel4 = getIntKeyByRel(jSONArray5, "preRoll", "alpha");
        String stringKeyByRel4 = getStringKeyByRel(jSONArray5, "postRoll", "href");
        String str9 = clickUrl;
        String stringKeyByRel5 = getStringKeyByRel(jSONArray5, "postRoll", "message");
        int intKeyByRel5 = getIntKeyByRel(jSONArray5, "preRoll", "red");
        int intKeyByRel6 = getIntKeyByRel(jSONArray5, "preRoll", "green");
        int intKeyByRel7 = getIntKeyByRel(jSONArray5, "preRoll", "blue");
        int intKeyByRel8 = getIntKeyByRel(jSONArray5, "preRoll", "alpha");
        int videoInt = getVideoInt(jSONObject, "orientation");
        ? videoInt2 = getVideoInt(jSONObject, "orientationLock");
        String linkByRel8 = getLinkByRel(jSONArray5, "image");
        ? r68 = videoInt2;
        String linkByRel9 = getLinkByRel(jSONArray5, "imageSize");
        String str10 = impressionUrl;
        double videoDouble = getVideoDouble(jSONObject, "videoSkipTime");
        int videoInt3 = getVideoInt(jSONObject, "timeLeftTime");
        int videoInt4 = getVideoInt(jSONObject, "videoEnd");
        double videoDouble2 = getVideoDouble(jSONObject, "fullscreenPercentage");
        String linkByRel10 = getLinkByRel(jSONArray5, "video");
        String str11 = stringKeyByRel4;
        String linkByRel11 = getLinkByRel(jSONArray5, "replay_button");
        String str12 = stringKeyByRel2;
        String linkByRel12 = getLinkByRel(jSONArray5, "revmob_logo");
        String str13 = stringKeyByRel;
        String linkByRel13 = getLinkByRel(jSONArray5, "volume_mute");
        String linkByRel14 = getLinkByRel(jSONArray5, "volume_up");
        String linkByRel15 = getLinkByRel(jSONArray5, "sound");
        String str14 = linkByRel12;
        String linkByRel16 = getLinkByRel(jSONArray5, "sound_off");
        String linkByRel17 = getLinkByRel(jSONArray5, "close_button");
        ArrayList arrayList = new ArrayList();
        String str15 = linkByRel15;
        ArrayList arrayList2 = new ArrayList();
        String str16 = linkByRel11;
        ArrayList arrayList3 = new ArrayList();
        String str17 = linkByRel17;
        ArrayList arrayList4 = new ArrayList();
        String str18 = linkByRel8;
        ArrayList arrayList5 = new ArrayList();
        String str19 = linkByRel7;
        ArrayList arrayList6 = new ArrayList();
        String str20 = linkByRel6;
        int i13 = i12;
        int[] iArr2 = new int[2];
        success = true;
        arrayList3.add("clicks");
        arrayList4.add("errors");
        arrayList5.add("impressions");
        try {
            String[] split = linkByRel9.split(",");
            str2 = linkByRel5;
            try {
                iArr2[0] = Integer.parseInt(split[0]);
                try {
                    iArr2[1] = Integer.parseInt(split[1]);
                } catch (Exception unused) {
                }
            } catch (Exception unused2) {
                i11 = 0;
                iArr2[i11] = i11;
                iArr2[1] = i11;
                if (linkByRel10 != null) {
                }
                iArr = iArr2;
                str3 = linkByRel10;
                showAnimationConfiguration = new ShowAnimationConfiguration();
                closeAnimationConfiguration = new CloseAnimationConfiguration();
                JSONObject jSONObject2 = jSONObject.getJSONObject("animation");
                long j = jSONObject2.getLong("duration");
                showAnimationConfiguration.setTime(j);
                closeAnimationConfiguration.setTime(j);
                jSONArray = jSONObject2.getJSONArray("show");
                while (i9 < jSONArray.length()) {
                }
                jSONArray2 = jSONObject2.getJSONArray("close");
                while (i10 < jSONArray2.length()) {
                }
                i = jSONObject2.getInt("parallax_delta");
                String str21 = new String();
                String str22 = new String();
                if (jSONObject.has("sound")) {
                }
                String str23 = str21;
                String str24 = str22;
                HTTPHelper hTTPHelper = new HTTPHelper();
                RevMobClient.sett2(System.currentTimeMillis());
                if (linkByRel2 == null) {
                }
                str6 = str19;
                str7 = str20;
                ? r510 = 0;
                if (str7 != null) {
                }
                if (str6 != null) {
                }
            }
        } catch (Exception unused3) {
            str2 = linkByRel5;
            i11 = 0;
            iArr2[i11] = i11;
            iArr2[1] = i11;
            if (linkByRel10 != null) {
            }
            iArr = iArr2;
            str3 = linkByRel10;
            showAnimationConfiguration = new ShowAnimationConfiguration();
            closeAnimationConfiguration = new CloseAnimationConfiguration();
            JSONObject jSONObject22 = jSONObject.getJSONObject("animation");
            long j2 = jSONObject22.getLong("duration");
            showAnimationConfiguration.setTime(j2);
            closeAnimationConfiguration.setTime(j2);
            jSONArray = jSONObject22.getJSONArray("show");
            while (i9 < jSONArray.length()) {
            }
            jSONArray2 = jSONObject22.getJSONArray("close");
            while (i10 < jSONArray2.length()) {
            }
            i = jSONObject22.getInt("parallax_delta");
            String str212 = new String();
            String str222 = new String();
            if (jSONObject.has("sound")) {
            }
            String str232 = str212;
            String str242 = str222;
            HTTPHelper hTTPHelper2 = new HTTPHelper();
            RevMobClient.sett2(System.currentTimeMillis());
            if (linkByRel2 == null) {
            }
            str6 = str19;
            str7 = str20;
            ? r5102 = 0;
            if (str7 != null) {
            }
            if (str6 != null) {
            }
        }
        if (linkByRel10 != null || !jSONObject.has("vast")) {
            iArr = iArr2;
            str3 = linkByRel10;
        } else {
            JSONObject jSONObject3 = jSONObject.getJSONObject("vast");
            JSONArray jSONArray6 = jSONObject3.has("trackingEvents") ? jSONObject3.getJSONArray("trackingEvents") : null;
            if (jSONObject3.has("clicks")) {
                jSONArray3 = jSONObject3.getJSONArray("clicks");
                str3 = linkByRel10;
            } else {
                str3 = linkByRel10;
                jSONArray3 = null;
            }
            if (jSONObject3.has("impressions")) {
                jSONArray4 = jSONObject3.getJSONArray("impressions");
                iArr = iArr2;
            } else {
                iArr = iArr2;
                jSONArray4 = null;
            }
            JSONArray jSONArray7 = jSONObject3.has("errors") ? jSONObject3.getJSONArray("errors") : null;
            if (jSONArray6 != null) {
                parseTrackingEvents(arrayList6, arrayList, jSONArray6);
            }
            if (jSONArray3 != null) {
                getAllStringsInJSONArray(jSONArray3, arrayList3);
            }
            if (jSONArray7 != null) {
                getAllStringsInJSONArray(jSONArray7, arrayList4);
            }
            if (jSONArray4 != null) {
                getAllStringsInJSONArray(jSONArray4, arrayList5);
            }
            arrayList2.add(arrayList3);
            arrayList2.add(arrayList5);
            arrayList2.add(arrayList4);
        }
        showAnimationConfiguration = new ShowAnimationConfiguration();
        closeAnimationConfiguration = new CloseAnimationConfiguration();
        try {
            JSONObject jSONObject222 = jSONObject.getJSONObject("animation");
            long j22 = jSONObject222.getLong("duration");
            showAnimationConfiguration.setTime(j22);
            closeAnimationConfiguration.setTime(j22);
            jSONArray = jSONObject222.getJSONArray("show");
            for (i9 = 0; i9 < jSONArray.length(); i9++) {
                showAnimationConfiguration.addAnimation(jSONArray.getString(i9));
            }
            jSONArray2 = jSONObject222.getJSONArray("close");
            for (i10 = 0; i10 < jSONArray2.length(); i10++) {
                closeAnimationConfiguration.addAnimation(jSONArray2.getString(i10));
            }
            i = jSONObject222.getInt("parallax_delta");
        } catch (JSONException unused4) {
            i = 0;
        }
        String str2122 = new String();
        String str2222 = new String();
        try {
            if (jSONObject.has("sound")) {
                JSONObject jSONObject4 = jSONObject.getJSONObject("sound");
                if (jSONObject4.has("on_show")) {
                    str2122 = jSONObject4.getString("on_show");
                }
                if (jSONObject4.has("on_click")) {
                    str2222 = jSONObject4.getString("on_click");
                }
            }
        } catch (JSONException unused5) {
        }
        String str2322 = str2122;
        String str2422 = str2222;
        HTTPHelper hTTPHelper22 = new HTTPHelper();
        RevMobClient.sett2(System.currentTimeMillis());
        if (linkByRel2 == null || linkByRel3 != null) {
            str6 = str19;
            str7 = str20;
            ? r51022 = 0;
            if (str7 != null) {
                z3 = true;
                bitmap16 = BitmapFactory.decodeFile(downloadFile(str7, true).getAbsolutePath());
                success = true;
                StringBuilder sb = new StringBuilder();
                sb.append("framePortrait ");
                sb.append(bitmap16);
                RMLog.e(sb.toString());
            } else {
                z3 = true;
                bitmap16 = null;
            }
            if (str6 != null) {
                bitmap15 = BitmapFactory.decodeFile(downloadFile(str6, z3).getAbsolutePath());
                success = z3;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("frameLandscape ");
                sb2.append(bitmap15);
                RMLog.e(sb2.toString());
                i5 = 0;
                i4 = 0;
                r642 = 0;
                r632 = r68;
                r52 = r51022;
                str4 = null;
                r645 = r642;
                r635 = r632;
                r55 = r52;
            } else {
                i5 = 0;
                i4 = 0;
                r643 = 0;
                r633 = r68;
                r53 = r51022;
                bitmap15 = null;
                r642 = r643;
                r632 = r633;
                r52 = r53;
                str4 = null;
                r645 = r642;
                r635 = r632;
                r55 = r52;
            }
        } else if (linkByRel != null) {
            str4 = hTTPHelper22.downloadHtml(linkByRel);
            i5 = 0;
            i4 = 0;
            r645 = 0;
            r635 = r68;
            bitmap16 = null;
            z3 = true;
            bitmap15 = null;
            r55 = 0;
        } else if (linkByRel4 == null || str2 == null) {
            z3 = true;
            ? r511 = 0;
            if (str18 != null) {
                Bitmap decodeFile = BitmapFactory.decodeFile(downloadFile(str18, true).getAbsolutePath());
                bitmap3 = decodeFile;
                i4 = decodeFile.getWidth();
                i5 = decodeFile.getHeight();
                r644 = 0;
                r634 = r68;
                bitmap16 = null;
                z3 = true;
                bitmap15 = null;
                str4 = null;
                r54 = r511;
                bitmap2 = null;
                bitmap = null;
                r64 = r644;
                r63 = r634;
                r5 = r54;
                if (str17 != null) {
                    StaticAssets.setCloseButton(Drawable.createFromPath(downloadFile(str17, z).getAbsolutePath()));
                }
                Bitmap decodeFile2 = str16 == null ? BitmapFactory.decodeFile(downloadFile(str16, z).getAbsolutePath()) : null;
                Bitmap decodeFile3 = str15 == null ? BitmapFactory.decodeFile(downloadFile(str15, z).getAbsolutePath()) : null;
                Bitmap decodeFile4 = linkByRel16 == null ? BitmapFactory.decodeFile(downloadFile(linkByRel16, z).getAbsolutePath()) : null;
                jSONObject.getBoolean("rewarded");
                if (str14 == null) {
                    z2 = true;
                    bitmap6 = BitmapFactory.decodeFile(downloadFile(str14, true).getAbsolutePath());
                } else {
                    z2 = true;
                    bitmap6 = null;
                }
                if (str3 == null) {
                    String str25 = str3;
                    File downloadFile = downloadFile(str25, z2);
                    if (str13 == null || str12 == null) {
                        str5 = str25;
                        bitmap7 = bitmap5;
                        d6 = 0.0d;
                        d5 = 0.0d;
                        bitmap14 = null;
                        bitmap13 = null;
                    } else {
                        str5 = str25;
                        bitmap13 = BitmapFactory.decodeFile(downloadFile(str13, z2).getAbsolutePath());
                        bitmap14 = BitmapFactory.decodeFile(downloadFile(str12, z2).getAbsolutePath());
                        if (bitmap14 != null) {
                            bitmap7 = bitmap5;
                            d5 = (double) bitmap14.getWidth();
                            d6 = (double) bitmap14.getHeight();
                        } else {
                            bitmap7 = bitmap5;
                            d6 = 0.0d;
                            d5 = 0.0d;
                        }
                    }
                    if (str11 != null) {
                        Bitmap bitmap20 = bitmap14;
                        double d7 = d6;
                        Bitmap decodeFile5 = BitmapFactory.decodeFile(downloadFile(str11, true).getAbsolutePath());
                        if (decodeFile5 != null) {
                            bitmap8 = decodeFile5;
                            d2 = (double) decodeFile5.getHeight();
                            file = downloadFile;
                            bitmap9 = bitmap13;
                            d3 = d5;
                            bitmap10 = bitmap20;
                            d4 = d7;
                            d = (double) decodeFile5.getWidth();
                        } else {
                            bitmap8 = decodeFile5;
                            file = downloadFile;
                            d2 = 0.0d;
                            d = 0.0d;
                            bitmap9 = bitmap13;
                            d3 = d5;
                            bitmap10 = bitmap20;
                            d4 = d7;
                        }
                        if (r5 != 0) {
                            bitmap12 = null;
                            bitmap11 = null;
                        } else {
                            bitmap11 = bitmap4;
                            bitmap12 = bitmap7;
                        }
                        if (!success) {
                            return null;
                        }
                        FullscreenData fullscreenData = new FullscreenData(str10, str9, z4, revMobAdsListener, linkByRel, str4, linkByRel2, linkByRel3, bitmap3, bitmap2, bitmap, showAnimationConfiguration, closeAnimationConfiguration, str8, openInside, str2322, str2422, i, videoDouble, videoInt3, videoInt4, str5, decodeFile2, bitmap6, linkByRel13, linkByRel14, file, decodeFile3, decodeFile4, arrayList, arrayList2, bitmap12, bitmap11, i3, i2, bitmap10, bitmap9, bitmap8, d4, d3, d2, d, stringKeyByRel3, stringKeyByRel5, intKeyByRel, intKeyByRel2, intKeyByRel3, intKeyByRel4, intKeyByRel5, intKeyByRel6, intKeyByRel7, intKeyByRel8, videoInt, iArr, videoDouble2, r63, r64);
                        return fullscreenData;
                    }
                    file = downloadFile;
                    d2 = 0.0d;
                    d = 0.0d;
                    bitmap9 = bitmap13;
                    d3 = d5;
                    bitmap10 = bitmap14;
                    d4 = d6;
                } else {
                    bitmap7 = bitmap5;
                    str5 = str3;
                    d4 = 0.0d;
                    d3 = 0.0d;
                    d2 = 0.0d;
                    d = 0.0d;
                    file = null;
                    bitmap10 = null;
                    bitmap9 = null;
                }
                bitmap8 = null;
                if (r5 != 0) {
                }
                if (!success) {
                }
            } else {
                i5 = 0;
                i4 = 0;
                r643 = 0;
                r633 = r68;
                bitmap16 = null;
                r53 = r511;
                bitmap15 = null;
                r642 = r643;
                r632 = r633;
                r52 = r53;
                str4 = null;
                r645 = r642;
                r635 = r632;
                r55 = r52;
            }
        } else {
            Bitmap decodeFile6 = BitmapFactory.decodeFile(downloadFile(linkByRel4, true).getAbsolutePath());
            Bitmap decodeFile7 = BitmapFactory.decodeFile(downloadFile(str2, true).getAbsolutePath());
            if (decodeFile6 == null || decodeFile7 == null) {
                i7 = 0;
                bitmap17 = decodeFile7;
                bitmap18 = decodeFile6;
                r11 = 0;
                i6 = 0;
                r3 = r68;
                bitmap19 = null;
                r4 = 1;
            } else {
                int width = decodeFile6.getWidth();
                int height = decodeFile6.getHeight();
                int width2 = decodeFile7.getWidth();
                if (decodeFile7.getHeight() == height || width == width2) {
                    if (height > width) {
                        i8 = i13;
                        if (i8 == 2) {
                            r59 = 1;
                            r42 = r59;
                            r33 = r68;
                            bitmap19 = null;
                            bitmap18 = null;
                            bitmap17 = null;
                            r58 = r59;
                            r1 = 0;
                        }
                    } else {
                        i8 = i13;
                    }
                    if (height < width) {
                        r59 = 1;
                    } else {
                        r59 = 1;
                    }
                    bitmap17 = decodeFile7;
                    bitmap19 = decodeFile6;
                    bitmap18 = bitmap19;
                    ? r12 = r59;
                    r33 = r12;
                    r58 = r59;
                    r1 = r12;
                    r42 = 0;
                } else {
                    bitmap17 = decodeFile7;
                    bitmap18 = decodeFile6;
                    r33 = r68;
                    bitmap19 = null;
                    r1 = 0;
                    r42 = 0;
                    r58 = 1;
                }
                if (iArr == null || iArr[0] == 0 || iArr[r58] == 0) {
                    r85 = r1;
                } else {
                    r85 = r1;
                    if (height == iArr[0] || height == iArr[r58] || height % iArr[0] == 0 || height % iArr[r58] == 0 || iArr[0] % height == 0 || iArr[r58] % height == 0) {
                        c = 0;
                    } else {
                        c = 0;
                        r85 = 1;
                    }
                    if (width != iArr[c]) {
                        i6 = height;
                        if (!(width == iArr[1] || width % iArr[c] == 0 || width % iArr[1] == 0 || iArr[c] % width == 0 || iArr[1] % width == 0)) {
                            i7 = width;
                            r4 = r42;
                            r3 = r32;
                            r11 = 1;
                        }
                        i7 = width;
                        r11 = r85;
                        r4 = r42;
                        r3 = r32;
                    }
                }
                i6 = height;
                r85 = r85;
                i7 = width;
                r11 = r85;
                r4 = r42;
                r3 = r32;
            }
            if (r11 == 0) {
                if (str20 != null) {
                    z = true;
                    bitmap5 = BitmapFactory.decodeFile(downloadFile(str20, true).getAbsolutePath());
                    success = true;
                } else {
                    z = true;
                    bitmap5 = null;
                }
                if (str19 != null) {
                    ? r87 = r3;
                    bitmap4 = BitmapFactory.decodeFile(downloadFile(str19, z).getAbsolutePath());
                    success = z;
                    r646 = r4;
                    i2 = i7;
                    r56 = r11;
                    bitmap2 = bitmap17;
                    i3 = i6;
                    r636 = r87;
                    bitmap = bitmap18;
                    bitmap3 = bitmap19;
                    str4 = null;
                    r64 = r646;
                    r63 = r636;
                    r5 = r56;
                    if (str17 != null) {
                    }
                    if (str16 == null) {
                    }
                    if (str15 == null) {
                    }
                    if (linkByRel16 == null) {
                    }
                    jSONObject.getBoolean("rewarded");
                    if (str14 == null) {
                    }
                    if (str3 == null) {
                    }
                    bitmap8 = null;
                    if (r5 != 0) {
                    }
                    if (!success) {
                    }
                } else {
                    r647 = r4;
                    i2 = i7;
                    r57 = r11;
                    bitmap2 = bitmap17;
                    i3 = i6;
                    r637 = r3;
                }
            } else {
                r647 = r4;
                i2 = i7;
                r57 = r11;
                bitmap2 = bitmap17;
                i3 = i6;
                r637 = r3;
                bitmap5 = null;
                z = true;
            }
            bitmap4 = null;
            r646 = r647;
            r636 = r637;
            r56 = r57;
            bitmap = bitmap18;
            bitmap3 = bitmap19;
            str4 = null;
            r64 = r646;
            r63 = r636;
            r5 = r56;
            if (str17 != null) {
            }
            if (str16 == null) {
            }
            if (str15 == null) {
            }
            if (linkByRel16 == null) {
            }
            jSONObject.getBoolean("rewarded");
            if (str14 == null) {
            }
            if (str3 == null) {
            }
            bitmap8 = null;
            if (r5 != 0) {
            }
            if (!success) {
            }
        }
        bitmap3 = null;
        r644 = r645;
        r634 = r635;
        r54 = r55;
        bitmap2 = null;
        bitmap = null;
        r64 = r644;
        r63 = r634;
        r5 = r54;
        if (str17 != null) {
        }
        if (str16 == null) {
        }
        if (str15 == null) {
        }
        if (linkByRel16 == null) {
        }
        try {
            jSONObject.getBoolean("rewarded");
        } catch (JSONException unused6) {
        }
        if (str14 == null) {
        }
        if (str3 == null) {
        }
        bitmap8 = null;
        if (r5 != 0) {
        }
        if (!success) {
        }
    }

    public void setShallUpdateData(boolean z) {
        this.shallUpdateData = z;
    }

    public boolean getShallUpdateData() {
        return this.shallUpdateData;
    }

    private static File downloadFile(String str, boolean z) {
        File file;
        String str2;
        String str3;
        AeSimpleSHA1 aeSimpleSHA1 = new AeSimpleSHA1();
        try {
            URL url = new URL(str);
            if (!z) {
                str2 = activity.getApplicationContext().getFilesDir().getPath();
            } else {
                str2 = activity.getApplicationContext().getCacheDir().getPath();
            }
            try {
                str3 = aeSimpleSHA1.SHA1(str);
            } catch (NoSuchAlgorithmException e) {
                str3 = Uri.parse(str).getLastPathSegment();
                e.printStackTrace();
            }
            file = new File(str2, str3);
            try {
                file.getAbsolutePath();
                if (!file.exists()) {
                    System.currentTimeMillis();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(url.openConnection().getInputStream());
                    ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(50);
                    while (true) {
                        int read = bufferedInputStream.read();
                        if (read == -1) {
                            break;
                        }
                        byteArrayBuffer.append((byte) read);
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(byteArrayBuffer.toByteArray());
                    fileOutputStream.close();
                }
            } catch (IOException e2) {
                e = e2;
                success = false;
                e.printStackTrace();
                return file;
            }
        } catch (IOException e3) {
            e = e3;
            file = null;
            success = false;
            e.printStackTrace();
            return file;
        }
        return file;
    }

    private static void parseTrackingEvents(List<String> list, ArrayList<ArrayList> arrayList, JSONArray jSONArray) throws JSONException {
        for (int i = 0; i < 13; i++) {
            String str = null;
            switch (i) {
                case 0:
                    str = "midpoint";
                    break;
                case 1:
                    str = "thirdQuartile";
                    break;
                case 2:
                    str = "complete";
                    break;
                case 3:
                    str = "creativeView";
                    break;
                case 4:
                    str = "start";
                    break;
                case 5:
                    str = "firstQuartile";
                    break;
                case 6:
                    str = "mute";
                    break;
                case 7:
                    str = "unmute";
                    break;
                case 8:
                    str = "close";
                    break;
                case 9:
                    str = "pause";
                    break;
                case 10:
                    str = "resume";
                    break;
                case 11:
                    str = "fullscreenPortraitView";
                    break;
                case 12:
                    str = "fullscreenLandscapeView";
                    break;
            }
            list.add(str);
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(list.get(i2));
            arrayList.add(arrayList2);
            getAllLinksByRel(jSONArray, (String) arrayList2.get(0), arrayList2);
            for (int i3 = 1; i3 < arrayList2.size(); i3++) {
            }
        }
    }
}
