package com.revmob.ads.interstitial.client;

import android.graphics.Bitmap;
import android.view.animation.Animation;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.AnimationConfiguration;
import com.revmob.client.AdData;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class FullscreenData extends AdData {
    public static final String KEY = "com.revmob.ads.fullscreen.adUrl";
    private static Map<String, FullscreenData> loadedFullscreens = new HashMap();
    private String clickSoundURL;
    private AnimationConfiguration closeAnimation;
    private boolean doNotShow = false;
    private String dspHtml;
    private String dspUrl;
    private Bitmap frameLandscape = null;
    private Bitmap framePortrait = null;
    private double fullscreenPercentage;
    private ArrayList<ArrayList> gettingEvents;
    private String htmlAdUrl;
    private String htmlCode;
    private int imageHeight;
    private Bitmap imageLandscape;
    private Bitmap imagePortrait;
    private int[] imageSize;
    private Bitmap imageSquare;
    private int imageWidth;
    private int orientationLock;
    private int parallaxDelta = 0;
    private Bitmap postRoll;
    private int postRollAlpha;
    private int postRollBlue;
    private int postRollGreen;
    private double postRollHeight;
    private int postRollRed;
    private String postRollText;
    private double postRollWidth;
    private int preRollAlpha;
    private int preRollBlue;
    private int preRollGreen;
    private double preRollHeight;
    private Bitmap preRollLandscape;
    private Bitmap preRollPortrait;
    private int preRollRed;
    private String preRollText;
    private double preRollWidth;
    private RevMobAdsListener publisherListener;
    private Bitmap replayButton;
    private Bitmap revmobLogo;
    private AnimationConfiguration showAnimation;
    private String showSoundURL;
    private Bitmap soundOff;
    private Bitmap soundOn;
    private int timeLeftTime;
    private ArrayList<ArrayList> trackingEvents;
    private int videoEnd;
    private File videoFile;
    private int videoOrientation;
    private double videoSkipTime;
    private String videoUrl;
    private String volumeMute;
    private String volumeUp;

    public static void addLoadedFullscreen(FullscreenData fullscreenData) {
        loadedFullscreens.put(fullscreenData.getClickUrl(), fullscreenData);
    }

    public static FullscreenData getLoadedFullscreen(String str) {
        return (FullscreenData) loadedFullscreens.get(str);
    }

    public static void cleanLoadedFullscreen(FullscreenData fullscreenData) {
        if (fullscreenData != null) {
            loadedFullscreens.remove(fullscreenData.getClickUrl());
        }
    }

    public FullscreenData(String str, String str2, boolean z, RevMobAdsListener revMobAdsListener, String str3, String str4, String str5, String str6, Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, AnimationConfiguration animationConfiguration, AnimationConfiguration animationConfiguration2, String str7, boolean z2, String str8, String str9, int i, double d, int i2, int i3, String str10, Bitmap bitmap4, Bitmap bitmap5, String str11, String str12, File file, Bitmap bitmap6, Bitmap bitmap7, ArrayList<ArrayList> arrayList, ArrayList<ArrayList> arrayList2, Bitmap bitmap8, Bitmap bitmap9, int i4, int i5, Bitmap bitmap10, Bitmap bitmap11, Bitmap bitmap12, double d2, double d3, double d4, double d5, String str13, String str14, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int[] iArr, double d6, int i15, boolean z3) {
        super(str, str2, z, str7, z2);
        this.publisherListener = revMobAdsListener;
        this.htmlCode = str4;
        this.htmlAdUrl = str3;
        this.dspUrl = str5;
        this.dspHtml = str6;
        this.imagePortrait = bitmap3;
        this.imageLandscape = bitmap2;
        this.framePortrait = bitmap8;
        this.frameLandscape = bitmap9;
        this.imageHeight = i4;
        this.imageWidth = i5;
        this.imageSquare = bitmap;
        this.showAnimation = animationConfiguration;
        this.closeAnimation = animationConfiguration2;
        this.showSoundURL = str8;
        this.clickSoundURL = str9;
        this.parallaxDelta = i;
        this.videoSkipTime = d;
        this.timeLeftTime = i2;
        this.soundOff = bitmap7;
        this.soundOn = bitmap6;
        this.videoEnd = i3;
        this.videoUrl = str10;
        this.replayButton = bitmap4;
        this.revmobLogo = bitmap5;
        this.volumeMute = str11;
        this.volumeUp = str12;
        this.videoFile = file;
        this.trackingEvents = arrayList;
        this.gettingEvents = arrayList2;
        this.preRollLandscape = bitmap10;
        this.preRollPortrait = bitmap11;
        this.preRollHeight = d2;
        this.preRollWidth = d3;
        this.postRoll = bitmap12;
        this.postRollHeight = d4;
        this.postRollWidth = d5;
        this.preRollText = str13;
        this.preRollRed = i6;
        this.preRollGreen = i7;
        this.preRollBlue = i8;
        this.preRollAlpha = i9;
        this.postRollText = str14;
        this.postRollRed = i10;
        this.postRollGreen = i11;
        this.postRollBlue = i12;
        this.postRollAlpha = i13;
        this.videoOrientation = i14;
        this.imageSize = iArr;
        this.fullscreenPercentage = d6;
        this.orientationLock = i15;
        this.doNotShow = z3;
    }

    public String getCampaignId() {
        String clickUrl = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parse = URLEncodedUtils.parse(new URI(clickUrl), "UTF-8");
            if (parse.size() > 0) {
                for (NameValuePair nameValuePair : parse) {
                    if (nameValuePair.getName().equals("campaign_id")) {
                        this.campaignId = nameValuePair.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }

    public Bitmap getAdImage(int i) {
        if (i == 2 && this.imageLandscape != null) {
            return this.imageLandscape;
        }
        if (i != 1 || this.imagePortrait == null) {
            return this.imageSquare;
        }
        return this.imagePortrait;
    }

    public boolean isHtmlCodeLoaded() {
        return (this.htmlCode == null || this.htmlCode == "") ? false : true;
    }

    public String getHtmlCode() {
        return this.htmlCode;
    }

    public String getHtmlAdUrl() {
        return this.htmlAdUrl;
    }

    public String getDspUrl() {
        return this.dspUrl;
    }

    public String getDspHtml() {
        return this.dspHtml;
    }

    public RevMobAdsListener getPublisherListener() {
        return this.publisherListener;
    }

    public boolean isHtmlFullscreen() {
        return (this.htmlAdUrl == null && this.htmlCode == null) ? false : true;
    }

    public boolean isDspFullscreen() {
        return (this.dspUrl == null && this.dspHtml == null) ? false : true;
    }

    public boolean isStaticMultiOrientationFullscreen() {
        return (this.imagePortrait == null || this.imageLandscape == null) ? false : true;
    }

    public Animation getShowAnimation() {
        return this.showAnimation.getAnimation();
    }

    public Animation getCloseAnimation() {
        return this.closeAnimation.getAnimation();
    }

    public int getParallaxDelta() {
        return this.parallaxDelta;
    }

    public String getShowSoundURL() {
        return this.showSoundURL;
    }

    public String getClickSoundURL() {
        return this.clickSoundURL;
    }

    public void setDspUrl(String str) {
        this.dspUrl = str;
    }

    public double getVideoSkipTime() {
        return this.videoSkipTime;
    }

    public int getTimeLeftTime() {
        return this.timeLeftTime;
    }

    public int getVideoEnd() {
        return this.videoEnd;
    }

    public String getVideoUrl() {
        return this.videoUrl;
    }

    public Bitmap getReplayButton() {
        return this.replayButton;
    }

    public Bitmap getRevmobLogo() {
        return this.revmobLogo;
    }

    public String getVolumeMute() {
        return this.volumeMute;
    }

    public String getVolumeUp() {
        return this.volumeUp;
    }

    public File getVideoFile() {
        return this.videoFile;
    }

    public Bitmap getSoundOn() {
        return this.soundOn;
    }

    public Bitmap getSoundOff() {
        return this.soundOff;
    }

    public ArrayList<ArrayList> getTrackingEvents() {
        return this.trackingEvents;
    }

    public ArrayList<ArrayList> getGettingEvents() {
        return this.gettingEvents;
    }

    public Bitmap getFrame(int i) {
        if (i == 2) {
            return this.frameLandscape;
        }
        return this.framePortrait;
    }

    public int getImageWidth() {
        return this.imageWidth;
    }

    public int getImageHeight() {
        return this.imageHeight;
    }

    public double getPreRollHeight() {
        return this.preRollHeight;
    }

    public double getPreRollWidth() {
        return this.preRollWidth;
    }

    public double getPostRollHeight() {
        return this.postRollHeight;
    }

    public double getPostRollWidth() {
        return this.postRollWidth;
    }

    public Bitmap getPreRollLandscape() {
        return this.preRollLandscape;
    }

    public Bitmap getPreRollPortrait() {
        return this.preRollPortrait;
    }

    public Bitmap getPostRoll() {
        return this.postRoll;
    }

    public String getPreRollText() {
        return this.preRollText;
    }

    public String getPostRollText() {
        return this.postRollText;
    }

    public int getPreRollRed() {
        return this.preRollRed;
    }

    public int getPreRollGreen() {
        return this.preRollGreen;
    }

    public int getPreRollBlue() {
        return this.preRollBlue;
    }

    public int getPreRollAlpha() {
        return this.preRollAlpha;
    }

    public int getPostRollRed() {
        return this.postRollRed;
    }

    public int getPostRollGreen() {
        return this.postRollGreen;
    }

    public int getPostRollBlue() {
        return this.postRollBlue;
    }

    public int getPostRollAlpha() {
        return this.postRollAlpha;
    }

    public int getVideoOrientation() {
        return this.videoOrientation;
    }

    public int[] getImageSize() {
        return this.imageSize;
    }

    public double getFullscreenPercentage() {
        return this.fullscreenPercentage;
    }

    public int getOrientationLock() {
        return this.orientationLock;
    }

    public boolean getDoNotShow() {
        return this.doNotShow;
    }
}
