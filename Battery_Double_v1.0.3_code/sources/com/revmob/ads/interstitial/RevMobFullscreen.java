package com.revmob.ads.interstitial;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Intent;
import com.revmob.FullscreenActivity;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.interstitial.client.FullscreenClientListener;
import com.revmob.ads.interstitial.client.FullscreenData;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.RMLog;
import java.util.List;

public class RevMobFullscreen implements Ad {
    private Activity activity;
    public boolean autoshow = false;
    private FullscreenData data;
    private FullscreenClientListener fullscreenClientListener;
    private boolean isRewarded = false;
    private boolean isVideo = false;
    private RevMobAdsListener publisherListener;
    private AdState state;
    private boolean videoFollowAppOrientation = false;

    public RevMobFullscreen(Activity activity2, RevMobAdsListener revMobAdsListener) {
        this.activity = activity2;
        this.publisherListener = revMobAdsListener;
        this.state = AdState.CREATED;
        this.fullscreenClientListener = new FullscreenClientListener(this, revMobAdsListener, activity2);
    }

    public void loadFullscreen(int i) {
        load((String) null, i);
    }

    public void loadFullscreen(String str, int i) {
        load(str, i);
    }

    public void loadVideo(String str) {
        load(str, 1);
    }

    public void loadRewardedVideo(String str) {
        load(str, 3);
    }

    public void load(String str, int i) {
        RevMobContext.getRunningApps = true;
        load(str, i, RevMobContext.toPayload(this.activity), null);
    }

    public void load(String str, String str2) {
        load(null, 2, str, str2);
    }

    public void load(String str, int i, String str2, String str3) {
        StringBuilder sb;
        FullscreenClientListener fullscreenClientListener2;
        String str4 = "Fullscreen ";
        if (i == 1) {
            RevMob.videoIsLoaded = false;
            this.isVideo = true;
            this.isRewarded = false;
            str4 = "Video ";
        } else if (i == 3) {
            RevMob.rewardedVideoIsLoaded = false;
            this.isVideo = true;
            this.isRewarded = true;
            str4 = "Rewarded Video ";
        } else {
            RevMob.adIsLoaded = false;
            this.isVideo = false;
        }
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED) {
            this.state = AdState.LOADING;
            if (str != null) {
                sb = new StringBuilder();
                sb.append("Loading ");
                sb.append(str4);
                sb.append(str);
            } else {
                sb = new StringBuilder();
                sb.append("Loading ");
                sb.append(str4);
            }
            RMLog.i(sb.toString());
            if (!this.fullscreenClientListener.getShallUpdateData()) {
                this.fullscreenClientListener = new FullscreenClientListener(this, this.publisherListener, this.activity);
                fullscreenClientListener2 = this.fullscreenClientListener;
            } else {
                fullscreenClientListener2 = this.fullscreenClientListener;
            }
            RevMobContext.getRunningApps = true;
            if (i == 0) {
                RevMobClient.getInstance().fetchFullscreen(str, RevMobContext.toPayload(this.activity), fullscreenClientListener2);
            } else if (i == 1 || i == 3) {
                RevMobClient.getInstance().fetchVideo(str, RevMobContext.toPayload(this.activity), fullscreenClientListener2, i);
            } else {
                RevMobClient.getInstance().fetchVideoOrFullscreen(str, str2, fullscreenClientListener2);
            }
        }
    }

    public void updateWithData(AdData adData) {
        this.state = AdState.LOADED;
        this.data = (FullscreenData) adData;
        FullscreenData.addLoadedFullscreen(this.data);
        if (this.publisherListener != null) {
            if (this.data.getVideoUrl() == null || !this.isVideo) {
                RevMob.adIsLoaded = true;
                this.publisherListener.onRevMobAdReceived();
            } else if (this.isRewarded) {
                RevMob.rewardedVideoIsLoaded = true;
                this.publisherListener.onRevMobRewardedVideoLoaded();
            } else {
                RevMob.videoIsLoaded = true;
                this.publisherListener.onRevMobVideoLoaded();
            }
        }
        if (this.autoshow && !this.isVideo) {
            show();
        }
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void show() {
        this.autoshow = true;
        if (isLoaded() && this.state != AdState.DISPLAYED) {
            List runningAppProcesses = ((ActivityManager) this.activity.getSystemService("activity")).getRunningAppProcesses();
            boolean z = false;
            for (int i = 0; i < runningAppProcesses.size(); i++) {
                if (((RunningAppProcessInfo) runningAppProcesses.get(i)).processName.equals(this.activity.getPackageName()) && ((RunningAppProcessInfo) runningAppProcesses.get(i)).importance == 100) {
                    z = true;
                }
            }
            if (FullscreenActivity.isFullscreenActivityAvailable(this.activity).booleanValue()) {
                this.state = AdState.DISPLAYED;
                if (!this.isVideo) {
                    this.videoFollowAppOrientation = true;
                }
                Intent intent = new Intent(this.activity, FullscreenActivity.class);
                intent.putExtra(FullscreenData.KEY, this.data.getClickUrl());
                intent.putExtra("followAppOrientation", this.videoFollowAppOrientation);
                intent.putExtra("isRewarded", this.isRewarded);
                if (z) {
                    this.activity.startActivityForResult(intent, 0);
                    if (this.publisherListener != null) {
                        if (!this.isVideo) {
                            RevMob.adIsLoaded = false;
                            this.publisherListener.onRevMobAdDisplayed();
                        } else if (!this.isRewarded) {
                            RevMob.videoIsLoaded = false;
                            this.publisherListener.onRevMobVideoStarted();
                        }
                    }
                    if (!this.isVideo) {
                        RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
                    }
                } else {
                    this.state = AdState.CLOSED;
                }
            } else {
                this.state = AdState.CLOSED;
                RMLog.e("You must declare the RevMob FullscreenActivity in the AndroidManifest.xml file");
            }
        } else if (this.state == AdState.LOADING) {
            this.state = AdState.CREATED;
            this.fullscreenClientListener.setShallUpdateData(false);
            load((String) null, 0);
        } else if (!(this.state == AdState.CREATED || this.state == AdState.CLOSED)) {
            RMLog.i(Ad.SHOWING_TOO_SOON_MSG);
        }
    }

    public void showVideo() {
        if (this.isVideo && isLoaded()) {
            this.videoFollowAppOrientation = false;
            show();
        } else if (this.publisherListener == null) {
        } else {
            if (this.isRewarded) {
                this.publisherListener.onRevMobRewardedVideoNotCompletelyLoaded();
            } else {
                this.publisherListener.onRevMobVideoNotCompletelyLoaded();
            }
        }
    }

    public void showVideo(boolean z) {
        if (this.isVideo && isLoaded()) {
            this.videoFollowAppOrientation = z;
            show();
        } else if (this.publisherListener == null) {
        } else {
            if (this.isRewarded) {
                this.publisherListener.onRevMobRewardedVideoNotCompletelyLoaded();
            } else {
                this.publisherListener.onRevMobVideoNotCompletelyLoaded();
            }
        }
    }

    public void showRewardedVideo() {
        this.isRewarded = true;
        showVideo();
    }

    public void showRewardedVideo(boolean z) {
        this.isRewarded = true;
        showVideo(z);
    }

    public void setAutoShow(boolean z) {
        this.autoshow = z;
    }

    public void hide() {
        this.autoshow = false;
        this.activity.finishActivity(0);
    }
}
