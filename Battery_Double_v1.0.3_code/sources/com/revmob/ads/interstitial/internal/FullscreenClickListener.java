package com.revmob.ads.interstitial.internal;

import com.revmob.FullscreenActivity;
import com.revmob.internal.MarketAsyncManager;
import com.revmob.internal.MarketAsyncManager.MarketAsyncManagerListener;
import com.revmob.internal.RMLog;

public class FullscreenClickListener {
    protected boolean clicked = false;
    protected final FullscreenActivity fullscreenActivity;
    protected String relativePosition = null;

    public FullscreenClickListener(FullscreenActivity fullscreenActivity2) {
        this.fullscreenActivity = fullscreenActivity2;
    }

    public boolean onClick() {
        if (!defaultClickHandler()) {
            openMarket();
        }
        return true;
    }

    public boolean onClick(String str) {
        if (!defaultClickHandler()) {
            openMarket(str);
        }
        return true;
    }

    public boolean onClose() {
        if (this.clicked) {
            return true;
        }
        this.clicked = true;
        if (this.fullscreenActivity.publisherListener != null) {
            this.fullscreenActivity.publisherListener.onRevMobAdDismissed();
        }
        this.fullscreenActivity.close();
        return true;
    }

    public boolean defaultClickHandler() {
        if (this.clicked) {
            return true;
        }
        this.clicked = true;
        this.fullscreenActivity.layout.setClickable(false);
        this.fullscreenActivity.addProgressBar();
        return false;
    }

    public void openMarket(String str) {
        this.relativePosition = str;
        openMarket();
    }

    public void openMarket() {
        this.fullscreenActivity.runOnUiThread(new Runnable() {
            public void run() {
                MarketAsyncManager marketAsyncManager = new MarketAsyncManager(FullscreenClickListener.this.fullscreenActivity, FullscreenClickListener.this.fullscreenActivity.data, FullscreenClickListener.this.fullscreenActivity.publisherListener, new MarketAsyncManagerListener() {
                    public void onPreExecute() {
                    }

                    public void onPostExecute() {
                        RMLog.d("Closing Fullscreen activity");
                        FullscreenClickListener.this.fullscreenActivity.finish();
                    }
                }, FullscreenClickListener.this.relativePosition);
                marketAsyncManager.execute(new Void[0]);
            }
        });
    }
}
