package com.revmob.ads.link.client;

import com.revmob.client.AdData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class LinkData extends AdData {
    public LinkData(String str, String str2, boolean z, String str3, boolean z2) {
        super(str, str2, z, str3, z2);
    }

    public String getCampaignId() {
        String clickUrl = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parse = URLEncodedUtils.parse(new URI(clickUrl), "UTF-8");
            if (parse.size() > 0) {
                for (NameValuePair nameValuePair : parse) {
                    if (nameValuePair.getName().equals("campaign_id")) {
                        this.campaignId = nameValuePair.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }
}
