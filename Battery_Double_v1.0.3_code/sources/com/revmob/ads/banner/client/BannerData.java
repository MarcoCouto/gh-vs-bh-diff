package com.revmob.ads.banner.client;

import android.graphics.Bitmap;
import android.view.animation.Animation;
import com.revmob.ads.internal.AnimationConfiguration;
import com.revmob.client.AdData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class BannerData extends AdData {
    private String clickSoundURL;
    private AnimationConfiguration closeAnimation;
    private Bitmap drawable;
    private String dspHtml;
    private String dspUrl;
    private String htmlAdUrl;
    private int[] imageSize;
    private int refreshTime;
    private AnimationConfiguration showAnimation;
    private String showSoundURL;

    public BannerData(String str, String str2, boolean z, Bitmap bitmap, String str3, String str4, String str5, AnimationConfiguration animationConfiguration, AnimationConfiguration animationConfiguration2, String str6, boolean z2, String str7, String str8, int[] iArr, int i) {
        super(str, str2, z, str6, z2);
        this.drawable = bitmap;
        this.htmlAdUrl = str3;
        this.dspUrl = str4;
        this.dspHtml = str5;
        this.showAnimation = animationConfiguration;
        this.closeAnimation = animationConfiguration2;
        this.showSoundURL = str7;
        this.clickSoundURL = str8;
        this.imageSize = iArr;
        this.refreshTime = i;
    }

    public String getCampaignId() {
        String clickUrl = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parse = URLEncodedUtils.parse(new URI(clickUrl), "UTF-8");
            if (parse.size() > 0) {
                for (NameValuePair nameValuePair : parse) {
                    if (nameValuePair.getName().equals("campaign_id")) {
                        this.campaignId = nameValuePair.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }

    public String getHtmlAdUrl() {
        return this.htmlAdUrl;
    }

    public Bitmap getDrawable() {
        return this.drawable;
    }

    public String getDspUrl() {
        return this.dspUrl;
    }

    public String getDspHtml() {
        return this.dspHtml;
    }

    public boolean isHtmlBanner() {
        return this.htmlAdUrl != null;
    }

    public boolean isDspBanner() {
        return (this.dspUrl == null && this.dspHtml == null) ? false : true;
    }

    public Animation getShowAnimation() {
        return this.showAnimation.getAnimation();
    }

    public Animation getCloseAnimation() {
        return this.closeAnimation.getAnimation();
    }

    public String getShowSoundURL() {
        return this.showSoundURL;
    }

    public String getClickSoundURL() {
        return this.clickSoundURL;
    }

    public void setDspUrl(String str) {
        this.dspUrl = str;
    }

    public int[] getImageSize() {
        return this.imageSize;
    }

    public int getRefreshTime() {
        return this.refreshTime;
    }
}
