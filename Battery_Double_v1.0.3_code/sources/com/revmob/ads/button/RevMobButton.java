package com.revmob.ads.button;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.link.RevMobLink;
import com.revmob.android.RevMobContext;

public class RevMobButton extends Button {
    /* access modifiers changed from: private */
    public Activity activity;
    private String placementId;
    private RevMobAdsListener publisherListener;
    private AdState state;

    private boolean isLoaded() {
        return true;
    }

    public RevMobButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.state = AdState.CREATED;
        setClickListener();
    }

    public RevMobButton(Activity activity2, RevMobAdsListener revMobAdsListener) {
        this(activity2.getApplicationContext(), (AttributeSet) null);
        this.publisherListener = revMobAdsListener;
    }

    public RevMobButton(Activity activity2, RevMobAdsListener revMobAdsListener, String str) {
        this(activity2, revMobAdsListener);
        this.placementId = str;
    }

    public void setListener(RevMobAdsListener revMobAdsListener) {
        this.publisherListener = revMobAdsListener;
    }

    public void setPlacementId(String str) {
        this.placementId = str;
    }

    private void setClickListener() {
        setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                RevMobButton.this.activity = (Activity) RevMobContext.activity;
                if (RevMobButton.this.activity == null) {
                    RevMobButton.this.performLinkClick();
                } else {
                    RevMobButton.this.activity.runOnUiThread(new Runnable() {
                        public void run() {
                            RevMobButton.this.performLinkClick();
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void performLinkClick() {
        this.state = AdState.CLICKED;
        createLink().open();
    }

    private RevMobLink createLink() {
        RevMobLink revMobLink = new RevMobLink(this.activity, getPublisherListener());
        revMobLink.load(this.placementId);
        return revMobLink;
    }

    private RevMobAdsListener getPublisherListener() {
        return this.publisherListener != null ? this.publisherListener : RevMobContext.publisherListener;
    }
}
