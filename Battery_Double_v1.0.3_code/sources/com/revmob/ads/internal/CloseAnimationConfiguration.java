package com.revmob.ads.internal;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class CloseAnimationConfiguration extends AnimationConfiguration {
    public Animation createFadeIn() {
        return new AlphaAnimation(1.0f, 2.0f);
    }

    public Animation createFadeOut() {
        return new AlphaAnimation(1.0f, 0.0f);
    }

    public Animation createSlideUp() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        return translateAnimation;
    }

    public Animation createSlideDown() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 2.0f);
        return translateAnimation;
    }

    public Animation createSlideRight() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 2.0f, 1, 0.0f, 1, 0.0f);
        return translateAnimation;
    }

    public Animation createSlideLeft() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, -2.0f, 1, 0.0f, 1, 0.0f);
        return translateAnimation;
    }

    public Animation createZoomIn() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f, 1, 0.5f, 1, 0.5f);
        return scaleAnimation;
    }

    public Animation createZoomOut() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, 1, 0.5f, 1, 0.5f);
        return scaleAnimation;
    }

    public Animation createClockwise() {
        return new RotateAnimation(0.0f, 90.0f);
    }

    public Animation createCounterClockwise() {
        return new RotateAnimation(0.0f, -90.0f);
    }
}
