package com.revmob.ads.internal;

import com.revmob.client.AdData;

public interface Ad {
    public static final int CLOSE_BUTTON_PADDING = 20;
    public static final int CLOSE_BUTTON_SIZE_IN_DIP = 40;
    public static final int REVMOB_LOGO_HEIGHT_IN_DIP = 50;
    public static final int REVMOB_LOGO_WIDTH_IN_DIP = 100;
    public static final String SHOWING_TOO_SOON_MSG = "The ad is not completely loaded yet. As soon as it is loaded, it is going to be displayed automatically.";
    public static final int TIMER_HEIGHT_IN_DIP = 30;
    public static final int TIMER_WIDTH_IN_DIP = 30;

    void updateWithData(AdData adData);
}
