package com.revmob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.revmob.ads.internal.StaticAssets;
import com.revmob.ads.interstitial.RevMobVideo;
import com.revmob.ads.interstitial.client.FullscreenData;
import com.revmob.ads.interstitial.internal.FullscreenClickListener;
import com.revmob.ads.interstitial.internal.FullscreenDSPClickListener;
import com.revmob.ads.interstitial.internal.FullscreenStatic;
import com.revmob.ads.interstitial.internal.FullscreenView;
import com.revmob.ads.interstitial.internal.FullscreenWebViewClickListener;
import com.revmob.ads.interstitial.internal.FullscreenWebview;
import com.revmob.client.RevMobClient;
import com.revmob.internal.AndroidHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobSoundPlayer;
import com.revmob.internal.RevMobWebViewClient;
import java.io.IOException;

@TargetApi(8)
public class FullscreenActivity extends Activity implements SensorEventListener {
    private static final int BACKGROUND_COLOR = -587202560;
    private static final String FORMAT = "%02d";
    private static Boolean fullscreenActivityAvailable;
    public RevMobVideo Video;
    private OnAudioFocusChangeListener afChangeListener;
    private AudioManager audioManager;
    private LayoutParams buttonParams;
    public int buttonSize;
    public ImageView closeButton;
    public LayoutParams closeLayoutParams;
    private int currentDpi;
    public int currentOrientation;
    public FullscreenData data;
    private double deviceDpiRatio;
    private double deviceHeightRatio = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double deviceRatio = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double deviceSizeRatio;
    private double deviceWidthRatio = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public boolean followAppOrientation;
    public ImageView frame;
    public double frameHeight;
    public LayoutParams frameLayoutParams;
    public double frameWidth;
    private double fullscreenPercentage = 0.65d;
    public FullscreenView fullscreenView;
    public Handler handler = new Handler();
    public double imageHeight;
    private double imageInverseRatio = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double imageRatio = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public double imageWidth;
    public boolean isParallaxEnabled = false;
    public Boolean isRewarded = Boolean.valueOf(false);
    private Boolean isTherePostRoll = Boolean.valueOf(false);
    public boolean isWebview = true;
    public RelativeLayout layout;
    public LayoutParams layoutP;
    private Sensor mOrientation;
    private SensorManager mSensorManager;
    private DisplayMetrics metrics = new DisplayMetrics();
    private Button okButton;
    private int orientationLock = 0;
    private int postRollHeight;
    private LayoutParams postRollLayoutParams;
    private ImageView postRollView;
    private int postRollWidth;
    private int preRollHeight;
    private LayoutParams preRollLayoutParams;
    private ImageView preRollView;
    private int preRollWidth;
    public ProgressBar progressBar;
    public RevMobAdsListener publisherListener;
    public double relativeFrameSideThickness;
    public double relativeFrameTopThickness;
    public RelativeLayout relativeLayout;
    public RelativeLayout relativeLayout2;
    private int screenHeight;
    private int screenWidth;
    public boolean shouldNotSkipVideo = false;
    private int stopPosition = 0;
    public boolean stopTimer1 = false;
    private TextView textPostRoll;
    private TextView textPreRoll;
    private int textTopMargin;
    private LayoutParams textViewParams;
    public VideoView videoView;

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public static Boolean isFullscreenActivityAvailable(Activity activity) {
        if (fullscreenActivityAvailable == null) {
            fullscreenActivityAvailable = Boolean.valueOf(AndroidHelper.isIntentAvailable((Context) activity, FullscreenActivity.class));
        }
        return fullscreenActivityAvailable;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            setStopPosition(bundle.getInt("videoPosition"));
        } else {
            setStopPosition(0);
        }
        if (Integer.valueOf(VERSION.SDK).intValue() <= 14) {
            requestWindowFeature(1);
            getWindow().addFlags(1024);
        } else {
            requestWindowFeature(1);
            getWindow().addFlags(1024);
        }
        this.screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        this.screenHeight = getWindowManager().getDefaultDisplay().getHeight();
        this.currentOrientation = getResources().getConfiguration().orientation;
        getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
        this.currentDpi = this.metrics.densityDpi;
        double d = 1.0d;
        this.deviceDpiRatio = 1.0d;
        if (this.currentDpi != 0) {
            this.deviceDpiRatio = 320.0d / ((double) this.currentDpi);
        }
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra(FullscreenData.KEY);
        this.followAppOrientation = intent.getBooleanExtra("followAppOrientation", false);
        this.isRewarded = Boolean.valueOf(intent.getBooleanExtra("isRewarded", false));
        try {
            FullscreenData fullscreenData = getLastNonConfigurationInstance() != null ? (FullscreenData) getLastNonConfigurationInstance() : stringExtra != null ? FullscreenData.getLoadedFullscreen(stringExtra) : null;
            if (fullscreenData != null) {
                this.data = fullscreenData;
                if (this.data.getDoNotShow()) {
                    RMLog.e("Unexpected error on create Fullscreen Ad.");
                    if (this.publisherListener != null) {
                        this.publisherListener.onRevMobAdNotReceived("Unexpected error on create Fullscreen Ad.");
                    }
                    finish();
                    return;
                }
                if (this.data.getFullscreenPercentage() != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                    d = this.data.getFullscreenPercentage();
                }
                this.fullscreenPercentage = d;
                this.publisherListener = this.data.getPublisherListener();
                this.orientationLock = this.data.getOrientationLock();
                if (this.orientationLock == 1) {
                    if (this.currentOrientation == 2) {
                        setRequestedOrientation(6);
                    } else {
                        setRequestedOrientation(7);
                    }
                }
                if (this.data.getVideoUrl() == null) {
                    createFullscreenView();
                    initFullscreen();
                    return;
                }
                this.afChangeListener = new OnAudioFocusChangeListener() {
                    public void onAudioFocusChange(int i) {
                    }
                };
                this.audioManager = (AudioManager) getSystemService("audio");
                int requestAudioFocus = this.audioManager.requestAudioFocus(this.afChangeListener, 3, 1);
                if (requestAudioFocus != 1) {
                    if (requestAudioFocus == 0) {
                        finish();
                        return;
                    }
                    return;
                }
                return;
            }
            createViewClickUrl(intent.getStringExtra("marketURL"));
            initFullscreen();
            addProgressBar();
        } catch (RuntimeException e) {
            RMLog.e("Unexpected error on create Fullscreen Ad.", e);
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdNotReceived("Unexpected error on create Fullscreen Ad.");
            }
            finish();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && this.Video != null && this.Video.video != null && getStopPosition() > 0 && !this.Video.video.isPlaying()) {
            this.Video.cancelCountDownTimer();
            this.Video.video.seekTo(getStopPosition());
            this.Video.video.resume();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.data != null && this.isParallaxEnabled) {
            this.mSensorManager = (SensorManager) getSystemService("sensor");
            this.mOrientation = this.mSensorManager.getDefaultSensor(3);
            if (this.mOrientation != null) {
                this.mSensorManager.registerListener(this, this.mOrientation, 3);
            }
        }
        if (this.Video != null) {
            if (this.Video.video != null && this.Video.video.isPlaying()) {
                this.Video.cancelCountDownTimer();
                this.Video.video.seekTo(getStopPosition());
                this.Video.video.start();
            } else if (this.Video.video != null && !this.Video.video.isPlaying()) {
                this.Video.cancelCountDownTimer();
                this.Video.video.seekTo(getStopPosition());
                this.Video.video.start();
            }
        } else if (this.data.getVideoUrl() != null && this.Video == null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    FullscreenActivity.this.initVideo();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mSensorManager != null) {
            this.mSensorManager.unregisterListener(this);
        }
        if (this.Video != null && this.Video.video != null && this.Video.video.isPlaying()) {
            setStopPosition(this.Video.video.getCurrentPosition());
        }
    }

    /* access modifiers changed from: 0000 */
    public void createFullscreenView() {
        StringBuilder sb = new StringBuilder();
        sb.append("Fullscreen loaded - ");
        sb.append(this.data.getCampaignId());
        RMLog.i(sb.toString());
        if (this.data.isHtmlFullscreen()) {
            this.fullscreenView = new FullscreenWebview(this, this.data.getHtmlAdUrl(), this.data.getHtmlCode(), new RevMobWebViewClient(this.publisherListener, new FullscreenWebViewClickListener(this)));
        } else if (this.data.isDspFullscreen()) {
            this.fullscreenView = new FullscreenWebview(this, this.data.getDspUrl(), this.data.getDspHtml(), new RevMobWebViewClient(this.publisherListener, new FullscreenDSPClickListener(this)));
        } else {
            if (RevMobClient.getInstance().getParallaxMode() == RevMobParallaxMode.DEFAULT) {
                this.isParallaxEnabled = true;
            }
            this.isWebview = false;
            FullscreenStatic fullscreenStatic = new FullscreenStatic(this, this.data, new FullscreenClickListener(this), this.isParallaxEnabled, this.data.getParallaxDelta());
            this.fullscreenView = fullscreenStatic;
        }
    }

    private void initFullscreen() {
        ((View) this.fullscreenView).setAnimation(this.data.getShowAnimation());
        this.layout = new RelativeLayout(this);
        this.layout.setBackgroundColor(BACKGROUND_COLOR);
        if (this.data.getFullscreenPercentage() != 1.0d) {
            if (this.data.getFrame(this.currentOrientation) != null) {
                this.layout.setGravity(48);
                this.frame = new ImageView(this);
                this.frame.setImageBitmap(this.data.getFrame(this.currentOrientation));
                this.frameLayoutParams = new LayoutParams(-1, -1);
                this.layout.addView(this.frame, this.frameLayoutParams);
                this.frame.setAnimation(this.data.getShowAnimation());
                setContentView(this.layout, new LayoutParams(-1, -1));
                ((View) this.fullscreenView).bringToFront();
            }
            this.imageHeight = (double) this.data.getImageHeight();
            this.imageWidth = (double) this.data.getImageWidth();
            this.imageRatio = this.imageHeight / this.imageWidth;
            this.imageInverseRatio = this.imageWidth / this.imageHeight;
            setContentView(this.layout, new LayoutParams(-1, -1));
            this.relativeLayout = new RelativeLayout(this);
            if (this.imageWidth == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE || this.imageHeight == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.fullscreenPercentage * ((double) this.screenHeight)));
            } else if (this.currentOrientation == 2) {
                if (this.imageWidth < this.imageHeight) {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageInverseRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
                } else {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
                }
            } else if (this.imageWidth < this.imageHeight) {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
            } else {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageInverseRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
            }
            this.layoutP.setMargins((this.screenWidth / 2) - (this.layoutP.width / 2), (this.screenHeight / 2) - (this.layoutP.height / 2), 0, 0);
            this.relativeLayout.setLayoutParams(this.layoutP);
            this.relativeLayout.addView((View) this.fullscreenView);
            if (this.isWebview) {
                ((View) this.fullscreenView).setLayoutParams(new LayoutParams(this.layoutP.width, this.layoutP.height));
            }
            this.layout.addView(this.relativeLayout);
        } else {
            this.layout.setGravity(48);
            this.layout.addView((View) this.fullscreenView, new LayoutParams(-1, -1));
            setContentView(this.layout, new LayoutParams(-1, -1));
            playSoundOnShow();
        }
        playSoundOnShow();
        addCloseButton();
    }

    public void initVideo() {
        boolean z = (this.data.getPreRollLandscape() == null && this.data.getPreRollPortrait() == null) ? false : true;
        if (this.data.getPostRoll() != null) {
            this.isTherePostRoll = Boolean.valueOf(true);
        }
        this.layout = new RelativeLayout(this);
        setContentView(this.layout, new LayoutParams(-1, -1));
        if (z) {
            this.layout.setBackgroundColor(BACKGROUND_COLOR);
            if (this.screenWidth < this.screenHeight) {
                this.preRollHeight = (int) (this.data.getPreRollHeight() / this.deviceDpiRatio);
                this.preRollWidth = (int) (this.data.getPreRollWidth() / this.deviceDpiRatio);
                this.textTopMargin = ((this.screenHeight - this.preRollHeight) / 2) + ((int) (0.17d * ((double) this.preRollHeight)));
            } else {
                this.preRollHeight = (int) (this.data.getPreRollWidth() / this.deviceDpiRatio);
                this.preRollWidth = (int) (this.data.getPreRollHeight() / this.deviceDpiRatio);
                this.textTopMargin = ((this.screenHeight - this.preRollHeight) / 2) + ((int) (0.05d * ((double) this.preRollHeight)));
            }
            this.preRollLayoutParams = new LayoutParams(this.preRollWidth, this.preRollHeight);
            this.preRollLayoutParams.addRule(13);
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.preRollWidth)), (int) (0.25d * ((double) this.preRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.layout.addView(createPreRollView(), this.preRollLayoutParams);
            this.layout.addView(createTextViewPreRollView(), this.textViewParams);
            addCloseButton();
            this.closeLayoutParams.rightMargin = ((this.screenWidth - this.preRollWidth) - this.buttonSize) / 2;
            this.closeLayoutParams.topMargin = ((this.screenHeight - this.preRollHeight) - this.buttonSize) / 2;
            this.closeButton.setLayoutParams(this.closeLayoutParams);
            this.publisherListener.onRevMobRewardedPreRollDisplayed();
            return;
        }
        createVideoView();
    }

    /* access modifiers changed from: private */
    public void createVideoView() {
        this.layout.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        if (this.preRollView != null) {
            this.layout.removeView(this.preRollView);
        }
        if (this.okButton != null) {
            this.layout.removeView(this.okButton);
        }
        if (this.textPreRoll != null) {
            this.layout.removeView(this.textPreRoll);
        }
        this.Video = new RevMobVideo(this, this.data, new FullscreenClickListener(this), this);
        this.layout.addView(this.Video, new LayoutParams(-1, -1));
        this.videoView = this.Video.video;
    }

    private View createPreRollView() {
        this.preRollView = new ImageView(this);
        this.currentOrientation = getResources().getConfiguration().orientation;
        if (this.currentOrientation == 2) {
            this.preRollView.setImageBitmap(this.data.getPreRollPortrait());
        } else {
            this.preRollView.setImageBitmap(this.data.getPreRollLandscape());
        }
        this.preRollView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FullscreenActivity.this.createVideoView();
            }
        });
        return this.preRollView;
    }

    public View createTextViewPreRollView() {
        String preRollText = this.data.getPreRollText();
        this.textPreRoll = new TextView(this);
        this.textPreRoll.setText(preRollText);
        int preRollRed = this.data.getPreRollRed();
        int preRollGreen = this.data.getPreRollGreen();
        int preRollBlue = this.data.getPreRollBlue();
        int preRollAlpha = this.data.getPreRollAlpha() * 255;
        this.textPreRoll.setTextSize((float) ((int) (((double) AndroidHelper.dipToPixels(this, 12)) * this.deviceDpiRatio)));
        this.textPreRoll.setTextColor(Color.argb(preRollAlpha, preRollRed, preRollGreen, preRollBlue));
        this.textPreRoll.setGravity(17);
        return this.textPreRoll;
    }

    public View createPostRollView() {
        this.postRollView = new ImageView(this);
        this.postRollView.setImageBitmap(this.data.getPostRoll());
        this.postRollView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FullscreenActivity.this.publisherListener.onRevMobRewardedVideoCompleted();
                FullscreenActivity.this.finish();
            }
        });
        return this.postRollView;
    }

    public View createTextViewPostRollView() {
        String postRollText = this.data.getPostRollText();
        this.textPostRoll = new TextView(this);
        this.textPostRoll.setText(postRollText);
        int postRollRed = this.data.getPostRollRed();
        int postRollGreen = this.data.getPostRollGreen();
        int postRollBlue = this.data.getPostRollBlue();
        int postRollAlpha = this.data.getPostRollAlpha() * 255;
        this.textPostRoll.setTextSize((float) ((int) (((double) AndroidHelper.dipToPixels(this, 8)) * this.deviceDpiRatio)));
        this.textPostRoll.setTextColor(Color.argb(postRollAlpha, postRollRed, postRollGreen, postRollBlue));
        this.textPostRoll.setGravity(17);
        return this.textPostRoll;
    }

    public void finishVideo() {
        int i;
        this.layout.removeAllViews();
        this.Video.releaseAllResources();
        if (this.isTherePostRoll.booleanValue()) {
            this.preRollView = null;
            setRequestedOrientation(-1);
            this.layout.setBackgroundColor(BACKGROUND_COLOR);
            this.postRollHeight = (int) (this.data.getPostRollHeight() / this.deviceDpiRatio);
            this.postRollWidth = (int) (this.data.getPostRollWidth() / this.deviceDpiRatio);
            if (this.screenWidth < this.screenHeight) {
                i = ((this.screenWidth - this.postRollHeight) / 2) + ((int) (0.2d * ((double) this.postRollHeight)));
            } else {
                i = ((this.screenHeight - this.postRollHeight) / 2) + ((int) (0.2d * ((double) this.postRollHeight)));
            }
            this.postRollLayoutParams = new LayoutParams(this.postRollWidth, this.postRollHeight);
            this.postRollLayoutParams.addRule(13);
            LayoutParams layoutParams = new LayoutParams((int) (0.8d * ((double) this.postRollWidth)), (int) (0.3d * ((double) this.postRollHeight)));
            layoutParams.topMargin = i;
            layoutParams.addRule(14);
            this.layout.addView(createPostRollView(), this.postRollLayoutParams);
            this.layout.addView(createTextViewPostRollView(), layoutParams);
        }
    }

    public void abandonAudioFocus() {
        this.audioManager.abandonAudioFocus(this.afChangeListener);
    }

    public void updatePreRoll() {
        if (this.preRollView == null) {
            return;
        }
        if (getResources().getConfiguration().orientation == 1) {
            this.preRollView.setImageBitmap(this.data.getPreRollLandscape());
        } else {
            this.preRollView.setImageBitmap(this.data.getPreRollPortrait());
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        int i = getResources().getConfiguration().orientation;
        if (this.data.getVideoUrl() == null) {
            if (!(this.data.getFrame(i) == null || this.frame == null)) {
                this.frame.setImageBitmap(this.data.getFrame(i));
                this.frame.setLayoutParams(new LayoutParams(-1, -1));
            }
            this.fullscreenView.update();
            if (this.data.getFullscreenPercentage() != 1.0d) {
                switch (i) {
                    case 1:
                        changeFullscreenImageOrientation(true);
                        return;
                    case 2:
                        changeFullscreenImageOrientation(false);
                        return;
                    default:
                        return;
                }
            }
        } else if (this.preRollView != null) {
            updatePreRoll();
            switch (getResources().getConfiguration().orientation) {
                case 1:
                    changePreRollOrientation(true);
                    return;
                case 2:
                    changePreRollOrientation(false);
                    return;
                default:
                    return;
            }
        } else if (this.postRollView != null) {
            switch (getResources().getConfiguration().orientation) {
                case 1:
                    changePostRollOrientation(true);
                    return;
                case 2:
                    changePostRollOrientation(false);
                    return;
                default:
                    return;
            }
        }
    }

    public void changeFullscreenImageOrientation(boolean z) {
        if (z) {
            this.closeLayoutParams = new LayoutParams(this.buttonSize, this.buttonSize);
            if (this.screenWidth > this.screenHeight) {
                if (this.imageWidth == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE || this.imageHeight == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.fullscreenPercentage * ((double) this.screenWidth)));
                } else if (this.imageWidth > this.imageHeight) {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.imageInverseRatio * this.fullscreenPercentage * ((double) this.screenHeight)));
                } else {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.imageRatio * this.fullscreenPercentage * ((double) this.screenHeight)));
                }
                this.layoutP.setMargins((this.screenHeight / 2) - (this.layoutP.width / 2), (this.screenWidth / 2) - (this.layoutP.height / 2), 0, 0);
                if (this.isWebview) {
                    ((View) this.fullscreenView).setLayoutParams(new LayoutParams(this.layoutP.width, this.layoutP.height));
                }
                this.closeLayoutParams.leftMargin = ((this.screenHeight + this.layoutP.width) - this.buttonSize) / 2;
                this.closeLayoutParams.topMargin = ((this.screenWidth - this.layoutP.height) - this.buttonSize) / 2;
                setContentView(this.layout, new LayoutParams(-1, -1));
            } else {
                if (this.imageWidth == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE || this.imageHeight == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.fullscreenPercentage * ((double) this.screenWidth)));
                } else if (this.imageWidth > this.imageHeight) {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageInverseRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
                } else {
                    this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
                }
                this.layoutP.setMargins((this.screenWidth / 2) - (this.layoutP.width / 2), (this.screenHeight / 2) - (this.layoutP.height / 2), 0, 0);
                if (this.isWebview) {
                    ((View) this.fullscreenView).setLayoutParams(new LayoutParams(this.layoutP.width, this.layoutP.height));
                }
                this.closeLayoutParams.leftMargin = ((this.screenWidth + this.layoutP.width) - this.buttonSize) / 2;
                this.closeLayoutParams.topMargin = ((this.screenHeight - this.layoutP.height) - this.buttonSize) / 2;
                setContentView(this.layout, new LayoutParams(-1, -1));
            }
            this.relativeLayout.setLayoutParams(this.layoutP);
            this.closeButton.setLayoutParams(this.closeLayoutParams);
            return;
        }
        this.closeLayoutParams = new LayoutParams(this.buttonSize, this.buttonSize);
        if (this.screenWidth < this.screenHeight) {
            if (this.imageWidth == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE || this.imageHeight == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.fullscreenPercentage * ((double) this.screenWidth)));
            } else if (this.imageWidth > this.imageHeight) {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.imageRatio * this.fullscreenPercentage * ((double) this.screenHeight)));
            } else {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.imageInverseRatio * this.fullscreenPercentage * ((double) this.screenHeight)));
            }
            this.layoutP.setMargins((this.screenHeight / 2) - (this.layoutP.width / 2), (this.screenWidth / 2) - (this.layoutP.height / 2), 0, 0);
            if (this.isWebview) {
                ((View) this.fullscreenView).setLayoutParams(new LayoutParams(this.layoutP.width, this.layoutP.height));
            }
            this.closeLayoutParams.leftMargin = ((this.screenHeight + this.layoutP.width) - this.buttonSize) / 2;
            this.closeLayoutParams.topMargin = ((this.screenWidth - this.layoutP.height) - this.buttonSize) / 2;
            setContentView(this.layout, new LayoutParams(-1, -1));
        } else {
            if (this.imageWidth == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE || this.imageHeight == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenHeight)), (int) (this.fullscreenPercentage * ((double) this.screenWidth)));
            } else if (this.imageWidth > this.imageHeight) {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
            } else {
                this.layoutP = new LayoutParams((int) (this.fullscreenPercentage * ((double) this.screenWidth)), (int) (this.imageInverseRatio * this.fullscreenPercentage * ((double) this.screenWidth)));
            }
            this.layoutP.setMargins((this.screenWidth / 2) - (this.layoutP.width / 2), (this.screenHeight / 2) - (this.layoutP.height / 2), 0, 0);
            if (this.isWebview) {
                ((View) this.fullscreenView).setLayoutParams(new LayoutParams(this.layoutP.width, this.layoutP.height));
            }
            this.closeLayoutParams.leftMargin = ((this.screenWidth + this.layoutP.width) - this.buttonSize) / 2;
            this.closeLayoutParams.topMargin = ((this.screenHeight - this.layoutP.height) - this.buttonSize) / 2;
            setContentView(this.layout, new LayoutParams(-1, -1));
        }
        this.relativeLayout.setLayoutParams(this.layoutP);
        this.closeButton.setLayoutParams(this.closeLayoutParams);
    }

    public void changePreRollOrientation(boolean z) {
        if (z) {
            if (this.screenWidth > this.screenHeight) {
                this.preRollWidth = (int) (this.data.getPreRollWidth() / this.deviceDpiRatio);
                this.preRollHeight = (int) (this.data.getPreRollHeight() / this.deviceDpiRatio);
                this.preRollLayoutParams = new LayoutParams(this.preRollWidth, this.preRollHeight);
                this.preRollLayoutParams.addRule(13);
                this.preRollView.setLayoutParams(this.preRollLayoutParams);
                this.closeLayoutParams.rightMargin = ((this.screenHeight - this.preRollWidth) - this.buttonSize) / 2;
                this.closeLayoutParams.topMargin = ((this.screenWidth - this.preRollHeight) - this.buttonSize) / 2;
                this.closeButton.setLayoutParams(this.closeLayoutParams);
                this.textTopMargin = ((this.screenWidth - this.preRollHeight) / 2) + ((int) (((double) this.preRollHeight) * 0.2d));
                this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.preRollWidth)), (int) (0.2d * ((double) this.preRollHeight)));
                this.textViewParams.topMargin = this.textTopMargin;
                this.textViewParams.addRule(14);
                this.textPreRoll.setLayoutParams(this.textViewParams);
                return;
            }
            this.preRollWidth = (int) (this.data.getPreRollWidth() / this.deviceDpiRatio);
            this.preRollHeight = (int) (this.data.getPreRollHeight() / this.deviceDpiRatio);
            this.preRollLayoutParams = new LayoutParams(this.preRollWidth, this.preRollHeight);
            this.preRollLayoutParams.addRule(13);
            this.preRollView.setLayoutParams(this.preRollLayoutParams);
            this.closeLayoutParams.rightMargin = ((this.screenWidth - this.preRollWidth) - this.buttonSize) / 2;
            this.closeLayoutParams.topMargin = ((this.screenHeight - this.preRollHeight) - this.buttonSize) / 2;
            this.closeButton.setLayoutParams(this.closeLayoutParams);
            this.textTopMargin = ((this.screenHeight - this.preRollHeight) / 2) + ((int) (((double) this.preRollHeight) * 0.2d));
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.preRollWidth)), (int) (0.2d * ((double) this.preRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.textPreRoll.setLayoutParams(this.textViewParams);
        } else if (this.screenWidth > this.screenHeight) {
            this.preRollHeight = (int) (this.data.getPreRollWidth() / this.deviceDpiRatio);
            this.preRollWidth = (int) (this.data.getPreRollHeight() / this.deviceDpiRatio);
            this.preRollLayoutParams = new LayoutParams(this.preRollWidth, this.preRollHeight);
            this.preRollLayoutParams.addRule(13);
            this.preRollView.setLayoutParams(this.preRollLayoutParams);
            this.closeLayoutParams.rightMargin = ((this.screenWidth - this.preRollWidth) - this.buttonSize) / 2;
            this.closeLayoutParams.topMargin = ((this.screenHeight - this.preRollHeight) - this.buttonSize) / 2;
            this.closeButton.setLayoutParams(this.closeLayoutParams);
            this.textTopMargin = ((this.screenHeight - this.preRollHeight) / 2) + ((int) (0.05d * ((double) this.preRollHeight)));
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.preRollWidth)), (int) (0.25d * ((double) this.preRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.textPreRoll.setLayoutParams(this.textViewParams);
        } else {
            this.preRollHeight = (int) (this.data.getPreRollWidth() / this.deviceDpiRatio);
            this.preRollWidth = (int) (this.data.getPreRollHeight() / this.deviceDpiRatio);
            this.preRollLayoutParams = new LayoutParams(this.preRollWidth, this.preRollHeight);
            this.preRollLayoutParams.addRule(13);
            this.preRollView.setLayoutParams(this.preRollLayoutParams);
            this.closeLayoutParams.rightMargin = ((this.screenHeight - this.preRollWidth) - this.buttonSize) / 2;
            this.closeLayoutParams.topMargin = ((this.screenWidth - this.preRollHeight) - this.buttonSize) / 2;
            this.closeButton.setLayoutParams(this.closeLayoutParams);
            this.textTopMargin = ((this.screenWidth - this.preRollHeight) / 2) + ((int) (0.05d * ((double) this.preRollHeight)));
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.preRollWidth)), (int) (0.25d * ((double) this.preRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.textPreRoll.setLayoutParams(this.textViewParams);
        }
    }

    public void changePostRollOrientation(boolean z) {
        if (z) {
            if (this.screenWidth > this.screenHeight) {
                this.textTopMargin = ((this.screenWidth - this.postRollHeight) / 2) + ((int) (0.2d * ((double) this.postRollHeight)));
                this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.postRollWidth)), (int) (0.3d * ((double) this.postRollHeight)));
                this.textViewParams.topMargin = this.textTopMargin;
                this.textViewParams.addRule(14);
                this.textPostRoll.setLayoutParams(this.textViewParams);
                return;
            }
            this.textTopMargin = ((this.screenHeight - this.postRollHeight) / 2) + ((int) (0.2d * ((double) this.postRollHeight)));
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.postRollWidth)), (int) (0.3d * ((double) this.postRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.textPostRoll.setLayoutParams(this.textViewParams);
        } else if (this.screenWidth > this.screenHeight) {
            this.textTopMargin = ((this.screenHeight - this.postRollHeight) / 2) + ((int) (0.2d * ((double) this.postRollHeight)));
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.postRollWidth)), (int) (0.3d * ((double) this.postRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.textPostRoll.setLayoutParams(this.textViewParams);
        } else {
            this.textTopMargin = ((this.screenWidth - this.postRollHeight) / 2) + ((int) (0.2d * ((double) this.postRollHeight)));
            this.textViewParams = new LayoutParams((int) (0.8d * ((double) this.postRollWidth)), (int) (0.3d * ((double) this.postRollHeight)));
            this.textViewParams.topMargin = this.textTopMargin;
            this.textViewParams.addRule(14);
            this.textPostRoll.setLayoutParams(this.textViewParams);
        }
    }

    public Object onRetainNonConfigurationInstance() {
        return this.data;
    }

    public void addProgressBar() {
        if (this.progressBar == null) {
            this.progressBar = new ProgressBar(this);
            this.progressBar.setIndeterminate(true);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.layout.addView(this.progressBar, layoutParams);
        }
    }

    public void removeProgressBar() {
        if (this.progressBar != null) {
            this.layout.removeView(this.progressBar);
        }
    }

    private void createViewClickUrl(String str) {
        this.fullscreenView = new FullscreenWebview(this, str, null, new RevMobWebViewClient(null, null) {
            public void onPageFinished(WebView webView, String str) {
                FullscreenActivity.this.removeProgressBar();
            }
        });
    }

    private void addCloseButton() {
        this.closeButton = new ImageView(this);
        this.closeButton.setImageDrawable(StaticAssets.getCloseButton());
        this.closeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (FullscreenActivity.this.publisherListener != null) {
                    FullscreenActivity.this.publisherListener.onRevMobAdDismissed();
                }
                FullscreenActivity.this.finish();
            }
        });
        this.buttonSize = AndroidHelper.dipToPixels(this, 40);
        this.closeLayoutParams = new LayoutParams(this.buttonSize, this.buttonSize);
        if (this.data.getFullscreenPercentage() != 1.0d) {
            this.currentOrientation = getResources().getConfiguration().orientation;
            if (this.layoutP != null) {
                this.closeLayoutParams.leftMargin = ((this.screenWidth + this.layoutP.width) - this.buttonSize) / 2;
                this.closeLayoutParams.topMargin = ((this.screenHeight - this.layoutP.height) - this.buttonSize) / 2;
            } else {
                int dipToPixels = AndroidHelper.dipToPixels(this, 20);
                this.closeLayoutParams.rightMargin = dipToPixels;
                this.closeLayoutParams.topMargin = dipToPixels;
                this.closeLayoutParams.addRule(11);
                this.closeLayoutParams.addRule(6);
            }
        } else {
            int dipToPixels2 = AndroidHelper.dipToPixels(this, 20);
            this.closeLayoutParams.rightMargin = dipToPixels2;
            this.closeLayoutParams.topMargin = dipToPixels2;
            this.closeLayoutParams.addRule(11);
            this.closeLayoutParams.addRule(6);
        }
        this.layout.addView(this.closeButton, this.closeLayoutParams);
    }

    public void close() {
        if (this.data.getVideoUrl() == null) {
            Animation closeAnimation = this.data.getCloseAnimation();
            ((View) this.fullscreenView).setAnimation(closeAnimation);
            closeAnimation.setAnimationListener(new AnimationListener() {
                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    FullscreenActivity.this.layout.removeView((View) FullscreenActivity.this.fullscreenView);
                    FullscreenActivity.this.layout.removeAllViews();
                    FullscreenActivity.this.finish();
                }
            });
            ((View) this.fullscreenView).startAnimation(closeAnimation);
            return;
        }
        this.layout.removeView(this.videoView);
        this.layout.removeAllViews();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.Video != null) {
            this.Video.cancelCountDownTimer();
            if (this.Video.video != null) {
                this.Video.video.stopPlayback();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        FullscreenData.cleanLoadedFullscreen(this.data);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && !this.isRewarded.booleanValue() && !this.shouldNotSkipVideo) {
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdDismissed();
            }
            finish();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onBackPressed() {
        if (!this.isRewarded.booleanValue() && !this.shouldNotSkipVideo) {
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdDismissed();
            }
            finish();
        }
    }

    public void finish() {
        if (this.Video != null) {
            abandonAudioFocus();
            this.Video = null;
        }
        super.finish();
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (this.data != null && this.isParallaxEnabled && sensorEvent.sensor.getType() == 3 && !this.data.isHtmlFullscreen() && !this.data.isDspFullscreen()) {
            ((FullscreenStatic) this.fullscreenView).updateAccordingToDevicePosition((int) sensorEvent.values[0], (int) sensorEvent.values[1]);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.Video != null) {
            bundle.putInt("videoPosition", getStopPosition());
        }
    }

    private void playSoundOnShow() {
        if (this.data.getShowSoundURL() != null && this.data.getShowSoundURL().length() != 0) {
            try {
                new RevMobSoundPlayer().playFullscreenSound(this, this.data.getShowSoundURL());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int getStopPosition() {
        return this.stopPosition;
    }

    public void setStopPosition(int i) {
        this.stopPosition = i;
    }
}
