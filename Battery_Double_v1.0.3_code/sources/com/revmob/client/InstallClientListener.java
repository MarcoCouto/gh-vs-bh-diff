package com.revmob.client;

import android.app.Activity;
import com.revmob.android.StoredData;
import com.revmob.internal.RMLog;
import org.json.JSONException;

public class InstallClientListener implements RevMobClientListener {
    private static final String ERROR_MESSAGE = "Install not registered on server. Did you set a valid App ID? If not, collect one at http://revmob.com.";
    private StoredData data;

    public InstallClientListener(Activity activity) {
        this.data = new StoredData(activity);
    }

    public void handleResponse(String str) throws JSONException {
        this.data.markAsTracked();
        RMLog.i("Install registered on server");
    }

    public void handleError(String str) {
        RMLog.w(ERROR_MESSAGE);
    }
}
