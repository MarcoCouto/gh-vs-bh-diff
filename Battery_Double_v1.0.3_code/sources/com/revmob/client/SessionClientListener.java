package com.revmob.client;

import android.app.Activity;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.AdRevMobClientListener;
import com.revmob.ads.link.RevMobLink;
import com.revmob.android.RevMobContext;
import com.revmob.android.RevMobContext.RUNNING_APPS_STATUS;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobBeacon;
import com.revmob.internal.RevMobEula;
import com.revmob.internal.RevMobServicesManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SessionClientListener implements RevMobClientListener {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public RevMobEula eula;
    private boolean needToRegisterInstall;
    /* access modifiers changed from: private */
    public RevMobAdsListener revmobListener;
    private boolean sentUserInformation = false;

    public SessionClientListener(Activity activity2, boolean z, RevMobAdsListener revMobAdsListener) {
        this.activity = activity2;
        this.needToRegisterInstall = z;
        this.revmobListener = revMobAdsListener;
    }

    public void handleResponse(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        JSONArray jSONArray = jSONObject.getJSONArray("links");
        HTTPHelper.setUserAgent(AdRevMobClientListener.getCustomUserAgent(jSONObject), null);
        HTTPHelper.setIpAddress(AdRevMobClientListener.getIpAddress(jSONObject));
        HTTPHelper.setEulaVersion(AdRevMobClientListener.getEulaVersion(jSONObject), null);
        HTTPHelper.setEulaUrl(AdRevMobClientListener.getEulaUrl(jSONObject), null);
        HTTPHelper.setShouldExtractSocial(AdRevMobClientListener.getShouldExtractSocial(jSONObject));
        HTTPHelper.setShouldExtractGeolocation(AdRevMobClientListener.getShouldExtractGeolocation(jSONObject));
        HTTPHelper.setShouldExtractOtherAppsData(AdRevMobClientListener.getShouldExtractOtherAppsData(jSONObject));
        HTTPHelper.setShouldContinueOnBackground(AdRevMobClientListener.getShouldContinueOnBackground(jSONObject));
        HTTPHelper.setShouldShowEula(AdRevMobClientListener.getShouldShowEula(jSONObject), null);
        StringBuilder sb = new StringBuilder();
        sb.append("Application startSession: ");
        sb.append(this.activity.getApplicationContext());
        RMLog.i(sb.toString());
        if (jSONObject.has("beaconConfig")) {
            RevMobBeacon.initialize(this.activity.getApplicationContext(), jSONObject.getJSONObject("beaconConfig"));
        }
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("url", "https://userinfo.revmob.com/api/v4/mobile_apps/5525ad9d76e44cd706879023/userInformation.json");
        jSONObject2.put("scanFrequency", 10);
        jSONObject2.put("notifyFrequency", 20);
        jSONObject2.put("status", RUNNING_APPS_STATUS.PAUSED.ordinal());
        jSONObject.put("runningAppsConfig", jSONObject2);
        if (jSONObject.has("runningAppsConfig") && jSONObject.getJSONObject("runningAppsConfig").getInt("status") != 0) {
            RevMobServicesManager.initialize(this.activity.getApplicationContext(), jSONObject.getJSONObject("runningAppsConfig"));
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                RevMobClient.getInstance().addServerEndPoint(jSONArray.getJSONObject(i).getString("rel"), jSONArray.getJSONObject(i).getString("href"));
            } catch (JSONException unused) {
            }
        }
        if (this.needToRegisterInstall) {
            RevMobClient.getInstance().registerInstall(RevMobContext.toPayload(this.activity), new InstallClientListener(this.activity));
        }
        if (!this.sentUserInformation) {
            RevMobContext.getInstalledApps = true;
            RevMobClient.getInstance().registerUserInformation(RevMobContext.toPayload(this.activity), new InstallClientListener(this.activity));
            this.sentUserInformation = true;
        }
        if (AdRevMobClientListener.getOpenAdLink(jSONObject)) {
            RevMobLink revMobLink = new RevMobLink(this.activity, null);
            revMobLink.load();
            revMobLink.open();
        }
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                SessionClientListener.this.eula = new RevMobEula(SessionClientListener.this.activity, SessionClientListener.this.revmobListener);
                SessionClientListener.this.eula.loadAndShow();
            }
        });
    }

    public void handleError(String str) {
        RMLog.d(str);
    }

    public RevMobEula getEulaPopup() {
        return this.eula;
    }
}
