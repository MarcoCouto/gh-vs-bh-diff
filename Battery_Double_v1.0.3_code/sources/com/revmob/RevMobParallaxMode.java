package com.revmob;

public enum RevMobParallaxMode {
    DEFAULT("enabled"),
    DISABLED("disabled");
    
    private String value;

    private RevMobParallaxMode(String str) {
        this.value = str;
    }

    public String getValue() {
        return this.value;
    }
}
