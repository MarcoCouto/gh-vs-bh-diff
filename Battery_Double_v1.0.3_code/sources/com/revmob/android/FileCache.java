package com.revmob.android;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileCache {
    private static final String CACHE_DIR = "RevMob";
    private File cacheDir;

    public FileCache(Context context) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            this.cacheDir = new File(Environment.getExternalStorageDirectory(), CACHE_DIR);
        } else {
            this.cacheDir = context.getCacheDir();
        }
        if (!this.cacheDir.exists()) {
            this.cacheDir.mkdirs();
        }
    }

    public File getFile(String str) {
        return new File(this.cacheDir, str);
    }

    public void write(File file, InputStream inputStream) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
        } catch (FileNotFoundException | IOException unused) {
        }
    }

    public static void copyStream(InputStream inputStream, OutputStream outputStream) {
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr, 0, 1024);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } catch (Exception unused) {
        }
    }
}
