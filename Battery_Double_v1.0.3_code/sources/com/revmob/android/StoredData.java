package com.revmob.android;

import android.content.Context;
import android.content.SharedPreferences.Editor;

public class StoredData {
    private static final String BASE_KEY = "RevMob";
    private Context context;

    public StoredData(Context context2) {
        this.context = context2;
    }

    public void markAsTracked() {
        Editor edit = this.context.getSharedPreferences(BASE_KEY, 0).edit();
        edit.putBoolean("Registered", true);
        edit.commit();
    }

    public boolean isAlreadyTracked() {
        return this.context.getSharedPreferences(BASE_KEY, 0).getBoolean("Registered", false);
    }
}
