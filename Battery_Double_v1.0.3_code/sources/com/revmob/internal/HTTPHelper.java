package com.revmob.internal;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable;
import android.webkit.WebView;
import com.revmob.android.RevMobScreen;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

public class HTTPHelper {
    private static String eulaUrl = null;
    private static String eulaVersion = null;
    public static int globalTimeoutInSeconds = 30;
    private static String ipAddress;
    private static boolean shouldContinueOnBackground;
    private static boolean shouldExtractGeolocation;
    private static boolean shouldExtractOtherAppsData;
    private static boolean shouldExtractSocial;
    private static boolean shouldShowEula;
    private static String userAgent;
    private AbstractHttpClient httpclient;

    public static void setUserAgent(String str, Activity activity) {
        if (activity != null) {
            userAgent = System.getProperty("http.agent");
            try {
                if (AndroidHelper.isUIThread()) {
                    userAgent = new WebView(activity).getSettings().getUserAgentString();
                }
            } catch (Exception e) {
                RMLog.e(e.getMessage());
            }
        } else {
            userAgent = str;
        }
    }

    public static String getUserAgent() {
        return userAgent;
    }

    public static void setIpAddress(String str) {
        ipAddress = str;
    }

    public static String getIpAddress() {
        return ipAddress;
    }

    public HTTPHelper() {
        this(new DefaultHttpClient());
    }

    public HTTPHelper(AbstractHttpClient abstractHttpClient) {
        this.httpclient = abstractHttpClient;
        setTimeout(globalTimeoutInSeconds);
        this.httpclient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    }

    public void setTimeout(int i) {
        HttpConnectionParams.setConnectionTimeout(this.httpclient.getParams(), i * 1000);
    }

    public HttpResponse get(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Sending GET to ");
        sb.append(str);
        RMLog.d(sb.toString());
        try {
            return sendRequestDealingWithSSLErrors(new HttpGet(str));
        } catch (IllegalArgumentException e) {
            RMLog.e("Unknown error", e);
            return null;
        }
    }

    public HttpResponse post(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Sending POST to: ");
            sb.append(str);
            RMLog.d(sb.toString());
            this.httpclient.getParams().setParameter("http.useragent", userAgent);
            HttpPost httpPost = new HttpPost(str);
            if (RevMobConstants.ENCRYPTION.booleanValue()) {
                httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
                httpPost.addHeader("x-revmob-crypt", RevMobConstants.REVMOB_ENCRYPTION_TYPE);
                str2 = new RevMobEncryption().encrypt(str2);
            } else {
                httpPost.setHeader("Content-type", "application/json");
            }
            httpPost.addHeader("User-Agent", userAgent);
            httpPost.setEntity(new StringEntity(str2, "UTF-8"));
            return sendRequestDealingWithSSLErrors(httpPost);
        } catch (UnsupportedEncodingException e) {
            RMLog.w("Encoding error.", e);
            return null;
        } catch (RuntimeException e2) {
            RMLog.e("Unknown error", e2);
            return null;
        }
    }

    public InputStream getAndReturnTheStream(String str) {
        try {
            HttpResponse httpResponse = get(str);
            if (!(httpResponse == null || httpResponse.getEntity() == null)) {
                return httpResponse.getEntity().getContent();
            }
        } catch (IllegalStateException unused) {
            RMLog.e("Read error.");
        } catch (IOException unused2) {
            RMLog.w("Read error.");
        }
        return null;
    }

    public static String encodedResponseBody(HttpEntity httpEntity) {
        String str;
        if (httpEntity == null) {
            return null;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer(1024);
            InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent(), "UTF-8");
            char[] cArr = new char[1024];
            while (true) {
                int read = inputStreamReader.read(cArr, 0, 1023);
                if (read <= 0) {
                    break;
                }
                stringBuffer.append(cArr, 0, read);
            }
            str = stringBuffer.toString();
            try {
                inputStreamReader.close();
            } catch (IOException unused) {
            }
        } catch (IOException unused2) {
            str = "";
            RMLog.w("Read error.");
            return str;
        }
        return str;
    }

    private HttpResponse sendRequest(HttpRequestBase httpRequestBase) throws SSLException {
        try {
            return this.httpclient.execute(httpRequestBase);
        } catch (UnknownHostException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error on requesting path ");
            sb.append(httpRequestBase.getRequestLine());
            sb.append(". Is the device connected to the internet?");
            RMLog.w(sb.toString(), e);
            return null;
        } catch (HttpHostConnectException e2) {
            throw new SSLException(e2);
        } catch (SSLPeerUnverifiedException e3) {
            throw new SSLException(e3);
        } catch (SocketException unused) {
            RMLog.w("Server took too long to respond.");
            return null;
        } catch (SSLException e4) {
            throw e4;
        } catch (IOException e5) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Error on requesting path ");
            sb2.append(httpRequestBase.getRequestLine());
            sb2.append(". Did the device lost its connection?");
            RMLog.w(sb2.toString(), e5);
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004f, code lost:
        com.revmob.internal.RMLog.i("Problem with SSL. What is the version of your Android?");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0055, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r5.getURI().toString().startsWith("https://") != false) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        r5.setURI(new java.net.URI(r5.getURI().toString().replace("https://", "http://")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0031, code lost:
        return sendRequest(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0032, code lost:
        r0 = new java.lang.StringBuilder();
        r0.append("Invalid url: ");
        r0.append(r5.getURI().toString());
        com.revmob.internal.RMLog.e(r0.toString());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0005 */
    private HttpResponse sendRequestDealingWithSSLErrors(HttpRequestBase httpRequestBase) {
        return sendRequest(httpRequestBase);
    }

    public Drawable downloadImage(String str) {
        return Drawable.createFromStream(getAndReturnTheStream(str), "src");
    }

    public int[] getImageSize(String str) {
        InputStream andReturnTheStream = getAndReturnTheStream(str);
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(andReturnTheStream, null, options);
        int i = options.outWidth;
        int i2 = options.outHeight;
        try {
            andReturnTheStream.close();
        } catch (IOException unused) {
        }
        return new int[]{i, i2};
    }

    public static int calculateInSampleSize(int[] iArr, int i, int i2) {
        int i3 = iArr[0];
        int i4 = 1;
        int i5 = iArr[1];
        if (i5 > i2 || i3 > i) {
            int round = Math.round(((float) i5) / ((float) i2));
            int round2 = Math.round(((float) i3) / ((float) i));
            i4 = round < round2 ? round : round2;
        }
        if (i4 < 4) {
            return i4;
        }
        return 5;
    }

    public Bitmap downloadBitmap(String str, String str2) {
        int[] iArr = new int[2];
        if (str2 == null) {
            iArr = getImageSize(str);
        } else {
            try {
                String[] split = str2.split(",");
                iArr[0] = Integer.parseInt(split[0]);
                iArr[1] = Integer.parseInt(split[1]);
            } catch (Exception unused) {
                iArr[0] = 320;
                iArr[1] = 50;
            }
        }
        int calculateInSampleSize = calculateInSampleSize(iArr, RevMobScreen.getScreenWidth(), RevMobScreen.getScreenHeight());
        InputStream andReturnTheStream = getAndReturnTheStream(str);
        Options options = new Options();
        options.inSampleSize = calculateInSampleSize;
        return BitmapFactory.decodeStream(andReturnTheStream, null, options);
    }

    public String downloadHtml(String str) {
        HttpResponse httpResponse = get(str);
        if (httpResponse == null || httpResponse.getEntity() == null) {
            return null;
        }
        return encodedResponseBody(httpResponse.getEntity());
    }

    public static void setShouldExtractSocial(boolean z) {
        shouldExtractSocial = z;
    }

    public static void setShouldExtractGeolocation(boolean z) {
        shouldExtractGeolocation = z;
    }

    public static void setShouldExtractOtherAppsData(boolean z) {
        shouldExtractOtherAppsData = z;
    }

    public static void setShouldContinueOnBackground(boolean z) {
        shouldContinueOnBackground = z;
    }

    public static void setShouldShowEula(boolean z, Activity activity) {
        if (activity != null) {
            shouldShowEula = false;
        } else {
            shouldShowEula = z;
        }
    }

    public static boolean getShouldExtractSocial() {
        return shouldExtractSocial;
    }

    public static boolean getShouldExtractGeolocation() {
        return shouldExtractGeolocation;
    }

    public static boolean getShouldExtractOtherAppsData() {
        return shouldExtractOtherAppsData;
    }

    public static boolean getShouldContinueOnBackground() {
        return shouldContinueOnBackground;
    }

    public static boolean getShouldShowEula() {
        return shouldShowEula;
    }

    public static void setEulaUrl(String str, Activity activity) {
        if (activity != null) {
            eulaUrl = "https://s3.amazonaws.com/www.revmob.com/Revmob_i_agree_terms.txt";
        } else {
            eulaUrl = str;
        }
    }

    public static void setEulaVersion(String str, Activity activity) {
        if (activity != null) {
            eulaVersion = "default";
        } else {
            eulaVersion = str;
        }
    }

    public static String getEulaUrl() {
        return eulaUrl;
    }

    public static String getEulaVersion() {
        return eulaVersion;
    }
}
