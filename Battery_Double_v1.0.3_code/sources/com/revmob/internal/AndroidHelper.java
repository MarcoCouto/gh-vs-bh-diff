package com.revmob.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;

public class AndroidHelper {
    public static boolean isPermissionEnabled(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("android.permission.");
        sb.append(str);
        return context.checkCallingOrSelfPermission(sb.toString()) == 0;
    }

    public static int dipToPixels(Context context, int i) {
        return (int) ((((float) i) * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static boolean isUIThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static boolean isIntentAvailable(Context context, String str) {
        boolean z = false;
        try {
            if (context.getPackageManager().queryIntentActivities(new Intent(str), 65536).size() > 0) {
                z = true;
            }
            return z;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean isIntentAvailable(Context context, Class<?> cls) {
        boolean z = false;
        try {
            if (context.getPackageManager().queryIntentActivities(new Intent(context, cls), 65536).size() > 0) {
                z = true;
            }
            return z;
        } catch (Exception unused) {
            return false;
        }
    }
}
