package com.revmob.internal;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.revmob.client.RevMobClient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RevMobBeaconManager extends Service implements BootstrapNotifier, RangeNotifier {
    private int BKGND_SCAN_PERIOD = 5000;
    private int BTW_BKGND_SCAN_PERIOD = 2000;
    private int BTW_FRGND_SCAN_PERIOD = 5000;
    private int FRGND_SCAN_PERIOD = 2000;
    Analytics analytics;
    /* access modifiers changed from: private */
    public ArrayList<Object> beaconData = new ArrayList<>();
    private String beaconLayout = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
    RevMobFullscreen fullscreen;
    private ArrayList<Identifier> identifiers = new ArrayList<>();
    private BeaconManager mBeaconManager;
    private Beacon mClosestBeacon = null;
    private HashMap<String, Object> mConfig;
    private RegionBootstrap mRegionBootstrap;
    private JSONObject mRevmobJSON;
    boolean monitoring;
    double now = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private HashMap<Region, HashMap<String, Beaconx>> rangedRegions;
    boolean ranging;
    RealTime realTime;
    private ArrayList<Region> regions = new ArrayList<>();
    private ArrayList<String> uuidArray;

    class Analytics {
        int distance;
        double lastRequest;
        double lastSample;
        int requestFreq;
        int sampleFreq;
        int timeout;
        String url;

        Analytics() {
        }
    }

    class Beaconx {
        Beacon b;
        double timeout;

        public Beaconx(Beacon beacon) {
            this.b = beacon;
        }
    }

    class RealTime {
        int distance;
        double lastDidRange;
        String url;

        RealTime() {
        }
    }

    public RevMobBeaconManager() {
        RMLog.i("RevMobBeaconManager constructor");
        this.rangedRegions = new HashMap<>();
        this.analytics = new Analytics();
        this.realTime = new RealTime();
    }

    public void startBeaconScan() {
        RMLog.i("startBeaconScan");
        Analytics analytics2 = this.analytics;
        Analytics analytics3 = this.analytics;
        RealTime realTime2 = this.realTime;
        double now2 = getNow();
        realTime2.lastDidRange = now2;
        analytics3.lastSample = now2;
        analytics2.lastRequest = now2;
        Iterator it = this.uuidArray.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            this.regions.add(new Region(str, Identifier.parse(str), null, null));
            this.identifiers.add(Identifier.parse(str));
        }
        if (getApplicationContext() != null) {
            this.mBeaconManager = BeaconManager.getInstanceForApplication(this);
            this.mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(this.beaconLayout));
        }
        this.mRegionBootstrap = new RegionBootstrap((BootstrapNotifier) this, (List<Region>) this.regions);
        startMonitoringBeacons();
    }

    private void startMonitoringBeacons() {
        if (this.monitoring) {
            this.mBeaconManager.setBackgroundScanPeriod((long) this.BKGND_SCAN_PERIOD);
            this.mBeaconManager.setBackgroundBetweenScanPeriod((long) this.BTW_BKGND_SCAN_PERIOD);
            this.mBeaconManager.setForegroundScanPeriod((long) this.FRGND_SCAN_PERIOD);
            this.mBeaconManager.setForegroundBetweenScanPeriod((long) this.BTW_FRGND_SCAN_PERIOD);
        }
    }

    public HashMap<String, Object> createRegionData(Region region) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("identifier", region.getUniqueId());
        return hashMap;
    }

    public HashMap<String, Object> createBeaconData(Beacon beacon) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("accuracy", Double.valueOf(((double) Math.round(beacon.getDistance() * 1000.0d)) / 1000.0d));
        hashMap.put("rssi", Integer.valueOf(beacon.getRssi()));
        hashMap.put("major", Integer.valueOf(beacon.getId2().toInt()));
        hashMap.put("minor", Integer.valueOf(beacon.getId3().toInt()));
        hashMap.put("mac", beacon.getBluetoothAddress());
        hashMap.put("txpower", Integer.valueOf(beacon.getTxPower()));
        hashMap.put("bltName", beacon.getBluetoothName());
        hashMap.put("manufacturer", Integer.valueOf(beacon.getManufacturer()));
        return hashMap;
    }

    public void didDetermineStateForRegion(int i, Region region) {
        StringBuilder sb = new StringBuilder();
        sb.append("didDetermineStateForRegion ");
        sb.append(i);
        sb.append(" ");
        sb.append(region.toString());
        RMLog.i(sb.toString());
        HashMap createRegionData = createRegionData(region);
        if (i == 1) {
            try {
                if (this.ranging) {
                    this.mBeaconManager.startRangingBeaconsInRegion(region);
                    this.mBeaconManager.setRangeNotifier(this);
                    createRegionData.put("event", "didDetermineState");
                    createRegionData.put("state", Integer.valueOf(i));
                    createRegionData.put("timestamp", Double.valueOf(getNow()));
                    notifyBeaconData(new JSONObject(createRegionData), this.realTime.url);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
        }
        this.mBeaconManager.stopRangingBeaconsInRegion(region);
        createRegionData.put("event", "didDetermineState");
        createRegionData.put("state", Integer.valueOf(i));
        createRegionData.put("timestamp", Double.valueOf(getNow()));
        notifyBeaconData(new JSONObject(createRegionData), this.realTime.url);
    }

    public void didEnterRegion(Region region) {
        StringBuilder sb = new StringBuilder();
        sb.append("didEnterRegion ");
        sb.append(region.toString());
        RMLog.i(sb.toString());
        HashMap createRegionData = createRegionData(region);
        createRegionData.put("event", "didEnterRegion");
        createRegionData.put("timestamp", Double.valueOf(getNow()));
        notifyBeaconData(new JSONObject(createRegionData), this.realTime.url);
    }

    public void didExitRegion(Region region) {
        StringBuilder sb = new StringBuilder();
        sb.append("didExitRegion ");
        sb.append(region.toString());
        RMLog.i(sb.toString());
        HashMap createRegionData = createRegionData(region);
        createRegionData.put("event", "didExitRegion");
        createRegionData.put("timestamp", Double.valueOf(getNow()));
        notifyBeaconData(new JSONObject(createRegionData), this.realTime.url);
    }

    public double getNow() {
        return (double) (System.currentTimeMillis() / 1000);
    }

    public void collectData() {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("timestamp", Double.valueOf(this.now));
        hashMap.put("event", "didRangeBeacons");
        for (Entry entry : this.rangedRegions.entrySet()) {
            HashMap createRegionData = createRegionData((Region) entry.getKey());
            ArrayList arrayList2 = new ArrayList();
            for (Entry value : ((HashMap) entry.getValue()).entrySet()) {
                Beaconx beaconx = (Beaconx) value.getValue();
                if (beaconx.b.getDistance() <= ((double) this.analytics.distance) && beaconx.b.getDistance() >= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                    arrayList2.add(createBeaconData(beaconx.b));
                }
            }
            createRegionData.put("beacons", arrayList2);
            if (arrayList2.size() > 0) {
                arrayList.add(createRegionData);
            }
        }
        hashMap.put("regions", arrayList);
        this.beaconData.add(hashMap);
        if (this.now - this.analytics.lastRequest > ((double) this.analytics.requestFreq) && this.beaconData.size() != 0) {
            this.analytics.lastRequest = this.now;
            notifyBeaconData(new JSONArray(this.beaconData), this.analytics.url, new Runnable() {
                public void run() {
                    RevMobBeaconManager.this.beaconData.clear();
                }
            });
        }
    }

    public void checkClosestBeacon() {
        Beacon beacon = null;
        for (Entry value : this.rangedRegions.entrySet()) {
            Iterator it = ((HashMap) value.getValue()).entrySet().iterator();
            while (it.hasNext()) {
                Beaconx beaconx = (Beaconx) ((Entry) it.next()).getValue();
                if (beaconx.b.getDistance() > FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE && beaconx.b.getDistance() < ((double) this.realTime.distance) && this.now < beaconx.timeout && (beacon == null || beacon.getRssi() < beaconx.b.getRssi())) {
                    beacon = beaconx.b;
                }
                if (this.now > beaconx.timeout) {
                    it.remove();
                }
            }
        }
        if (beacon == null) {
            return;
        }
        if (this.mClosestBeacon == null || !this.mClosestBeacon.equals(beacon)) {
            this.mClosestBeacon = beacon;
            HashMap createBeaconData = createBeaconData(this.mClosestBeacon);
            createBeaconData.put("event", "closestBeacon");
            createBeaconData.put("uuid", this.mClosestBeacon.getId1().toString());
            createBeaconData.put("timestamp", Double.valueOf(getNow()));
            notifyBeaconData(new JSONObject(createBeaconData), this.realTime.url);
        }
    }

    public void printBeacon(Beacon beacon) {
        StringBuilder sb = new StringBuilder();
        sb.append("UUID: ");
        sb.append(beacon.getId1());
        sb.append(" Major: ");
        sb.append(beacon.getId2());
        sb.append(" Minor: ");
        sb.append(beacon.getId3());
        sb.append(" RSSI: ");
        sb.append(beacon.getRssi());
        RMLog.d(sb.toString());
    }

    public void printRangedRegions() {
        for (Entry value : this.rangedRegions.entrySet()) {
            for (Entry value2 : ((HashMap) value.getValue()).entrySet()) {
                printBeacon(((Beaconx) value2.getValue()).b);
            }
        }
    }

    public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
        this.now = getNow();
        StringBuilder sb = new StringBuilder();
        sb.append("didRangeBeaconsInRegion ");
        sb.append(collection.size());
        sb.append(" ");
        sb.append(region.toString());
        RMLog.i(sb.toString());
        if (collection.size() != 0) {
            HashMap hashMap = (HashMap) this.rangedRegions.get(region);
            if (hashMap == null) {
                hashMap = new HashMap();
            }
            for (Beacon beacon : collection) {
                Beaconx beaconx = new Beaconx(beacon);
                beaconx.timeout = this.now + ((double) this.analytics.timeout);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(beacon.getId1());
                sb2.append("|");
                sb2.append(beacon.getId2());
                sb2.append("|");
                sb2.append(beacon.getId3());
                hashMap.put(sb2.toString(), beaconx);
            }
            this.rangedRegions.put(region, hashMap);
            if (this.now - this.realTime.lastDidRange > 0.6d) {
                this.realTime.lastDidRange = this.now;
                checkClosestBeacon();
                if (this.now - this.analytics.lastSample > ((double) this.analytics.sampleFreq)) {
                    this.analytics.lastSample = this.now;
                    collectData();
                }
            }
        }
    }

    public void notifyBeaconData(Object obj, String str) {
        notifyBeaconData(obj, str, null);
    }

    public void notifyBeaconData(Object obj, String str, Runnable runnable) {
        try {
            this.mRevmobJSON.put("beaconData", obj);
            RevMobClient.getInstance().serverRequest(str, this.mRevmobJSON.toString(), null);
            if (runnable != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Sending beaconData: ");
                sb.append(this.beaconData.size());
                RMLog.i(sb.toString());
                runnable.run();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseParameters(JSONObject jSONObject) {
        this.mConfig = new HashMap<>();
        try {
            this.uuidArray = parseJSONArrayToArrayList(jSONObject.getJSONArray("uuidArray"));
            this.monitoring = Boolean.parseBoolean(jSONObject.getString("monitoring"));
            this.ranging = Boolean.parseBoolean(jSONObject.getString("ranging"));
            this.beaconLayout = jSONObject.getString("beaconLayout");
            JSONObject jSONObject2 = jSONObject.getJSONObject("analytics");
            this.analytics.url = jSONObject2.getString("url");
            this.analytics.distance = Integer.parseInt(jSONObject2.getString("distance"));
            this.analytics.requestFreq = Integer.parseInt(jSONObject2.getString("refresh"));
            this.analytics.sampleFreq = Integer.parseInt(jSONObject2.getString("sample"));
            this.analytics.timeout = Integer.parseInt(jSONObject2.getString("timeout"));
            JSONObject jSONObject3 = jSONObject.getJSONObject("frequencies");
            this.BKGND_SCAN_PERIOD = Integer.parseInt(jSONObject3.getString("background"));
            this.BTW_BKGND_SCAN_PERIOD = Integer.parseInt(jSONObject3.getString("btwBackground"));
            this.FRGND_SCAN_PERIOD = Integer.parseInt(jSONObject3.getString("foreground"));
            this.BTW_FRGND_SCAN_PERIOD = Integer.parseInt(jSONObject3.getString("btwForeground"));
            JSONObject jSONObject4 = jSONObject.getJSONObject("realTime");
            this.realTime.url = jSONObject4.getString("url");
            this.realTime.distance = Integer.parseInt(jSONObject4.getString("distance"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> parseJSONArrayToArrayList(JSONArray jSONArray) throws JSONException {
        ArrayList<String> arrayList = new ArrayList<>();
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                arrayList.add(jSONArray.get(i).toString());
            }
        }
        return arrayList;
    }

    public IBinder onBind(Intent intent) {
        RMLog.i("Beacon onBind service");
        return null;
    }

    public void onCreate() {
        RMLog.i("Beacons monitoring service created");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            this.mRevmobJSON = new JSONObject(defaultSharedPreferences.getString("revmobJSON", ""));
            parseParameters(new JSONObject(defaultSharedPreferences.getString("beaconConfiguration", "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        RMLog.i("Beacons monitoring service destroyed");
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        RMLog.i("onStartCommand beacon");
        startBeaconScan();
        return 2;
    }
}
