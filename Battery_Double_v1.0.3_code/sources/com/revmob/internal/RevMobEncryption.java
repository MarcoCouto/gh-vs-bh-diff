package com.revmob.internal;

import android.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class RevMobEncryption {
    private int MINIMIUM_DATA_LENGTH = 24;

    public String encrypt(String str) {
        byte[] bArr;
        if (!RevMobConstants.ENCRYPTION.booleanValue() || str.length() <= 0) {
            return str;
        }
        byte[] hexStringToByteArray = hexStringToByteArray("f188c2f6176602368ab346d0b40f1098ed350c3c46595e9981a8db1db9d865b7");
        byte[] hexStringToByteArray2 = hexStringToByteArray("3066546c3043314e614c4b764f433338");
        SecretKeySpec secretKeySpec = new SecretKeySpec(hexStringToByteArray, "AES/CBC/PKCS5Padding");
        try {
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, new IvParameterSpec(hexStringToByteArray2));
            bArr = instance.doFinal(str.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            bArr = null;
        }
        return Base64.encodeToString(bArr, 2);
    }

    public String decrypt(String str) {
        byte[] bArr;
        if (!RevMobConstants.ENCRYPTION.booleanValue() || str.length() < this.MINIMIUM_DATA_LENGTH) {
            return str;
        }
        byte[] hexStringToByteArray = hexStringToByteArray("f188c2f6176602368ab346d0b40f1098ed350c3c46595e9981a8db1db9d865b7");
        byte[] hexStringToByteArray2 = hexStringToByteArray("3066546c3043314e614c4b764f433338");
        SecretKeySpec secretKeySpec = new SecretKeySpec(hexStringToByteArray, "AES/CBC/PKCS5Padding");
        try {
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(2, secretKeySpec, new IvParameterSpec(hexStringToByteArray2));
            bArr = instance.doFinal(Base64.decode(str, 2));
        } catch (Exception e) {
            e.printStackTrace();
            bArr = null;
        }
        return new String(bArr);
    }

    public static byte[] hexStringToByteArray(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }
}
