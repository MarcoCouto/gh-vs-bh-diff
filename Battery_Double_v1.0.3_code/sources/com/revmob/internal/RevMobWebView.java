package com.revmob.internal;

import android.content.Context;
import android.webkit.WebView;

public class RevMobWebView extends WebView {
    public RevMobWebView(Context context, RevMobWebViewClient revMobWebViewClient) {
        this(context, null, null, revMobWebViewClient);
    }

    public RevMobWebView(Context context, String str, String str2, RevMobWebViewClient revMobWebViewClient) {
        super(context);
        setScrollContainer(false);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        getSettings().setAppCacheEnabled(false);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setLoadsImagesAutomatically(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getSettings().setSaveFormData(false);
        getSettings().setSavePassword(false);
        if (revMobWebViewClient != null) {
            setWebViewClient(revMobWebViewClient);
        }
        if (str2 != null && !str2.equals("")) {
            loadData(str2, "text/html; charset=UTF-8", "utf-8");
        } else if (str != null) {
            loadUrl(str);
        }
    }

    public void loadData(String str, String str2, String str3) {
        super.loadData(solveAndroidBug(str), str2, str3);
    }

    private String solveAndroidBug(String str) {
        StringBuilder sb = new StringBuilder(str.length() + 100);
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '#') {
                sb.append("%23");
            } else if (charAt == '%') {
                sb.append("%25");
            } else if (charAt != '\'') {
                sb.append(charAt);
            } else {
                sb.append("%27");
            }
        }
        return sb.toString();
    }
}
