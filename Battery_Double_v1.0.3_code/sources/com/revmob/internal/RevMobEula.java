package com.revmob.internal;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.revmob.RevMobAdsListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class RevMobEula implements AsyncTaskCompleteListener {
    private String EULA_PREFIX = "RevMobEula_v";
    private DialogInterface dialog;
    private DownloadManager downloader;
    private String eulaKey;
    private Activity mActivity;
    private String message;
    private SharedPreferences prefs;
    private RevMobAdsListener revmobListener;
    private boolean wasDismissed;

    public RevMobEula(Activity activity, RevMobAdsListener revMobAdsListener) {
        this.mActivity = activity;
        this.revmobListener = revMobAdsListener;
    }

    public void loadAndShow() {
        if (HTTPHelper.getShouldShowEula()) {
            this.wasDismissed = false;
            StringBuilder sb = new StringBuilder();
            sb.append(this.EULA_PREFIX);
            sb.append(HTTPHelper.getEulaVersion());
            this.eulaKey = sb.toString();
            this.prefs = PreferenceManager.getDefaultSharedPreferences(this.mActivity);
            if (!this.prefs.getBoolean(this.eulaKey, false)) {
                load();
            }
        }
    }

    private void show() {
        this.dialog = new Builder(this.mActivity).setTitle("RevMob EULA").setMessage(this.message).setPositiveButton("I agree", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                RevMobEula.this.acceptAndDismiss();
            }
        }).setNegativeButton("I don't agree", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                RevMobEula.this.reject();
            }
        }).create();
        ((Dialog) this.dialog).show();
        if (this.revmobListener != null) {
            this.revmobListener.onRevMobEulaIsShown();
        }
    }

    private void load() {
        String eulaUrl = HTTPHelper.getEulaUrl();
        if (eulaUrl != null && eulaUrl.length() > 0) {
            this.downloader = new DownloadManager(this.mActivity, eulaUrl, eulaUrl.substring(eulaUrl.lastIndexOf(47) + 1), this);
            this.downloader.execute(new String[0]);
        }
    }

    public void onTaskComplete(String str) {
        if (this.downloader.getIsSuccessful()) {
            File file = this.downloader.getFile();
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                StringBuffer stringBuffer = new StringBuffer("");
                byte[] bArr = new byte[((int) file.length())];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read != -1) {
                        stringBuffer.append(new String(bArr, 0, read));
                    } else {
                        this.message = stringBuffer.toString();
                        fileInputStream.close();
                        show();
                        return;
                    }
                }
            } catch (IOException e) {
                RMLog.d(e.toString());
            }
        }
    }

    public void acceptAndDismiss() {
        if (!this.wasDismissed) {
            RMLog.i("Eula accepted and dismissed.");
            Editor edit = this.prefs.edit();
            edit.putBoolean(this.eulaKey, true);
            edit.commit();
            this.dialog.dismiss();
            this.wasDismissed = true;
            if (this.revmobListener != null) {
                this.revmobListener.onRevMobEulaWasAcceptedAndDismissed();
            }
        }
    }

    public void reject() {
        if (!this.wasDismissed) {
            RMLog.i("Eula rejected.");
            this.wasDismissed = true;
            if (this.revmobListener != null) {
                this.revmobListener.onRevMobEulaWasRejected();
            }
            this.mActivity.setResult(0);
            this.mActivity.finish();
            System.exit(0);
        }
    }
}
