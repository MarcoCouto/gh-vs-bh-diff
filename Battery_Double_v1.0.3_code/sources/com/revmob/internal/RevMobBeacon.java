package com.revmob.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.revmob.android.RevMobContext;
import org.json.JSONObject;

public class RevMobBeacon extends BroadcastReceiver {
    public static JSONObject config;
    public static Context context;

    public static Context getMainContext() {
        return context;
    }

    public static void initialize(Context context2, JSONObject jSONObject) {
        context = context2;
        config = jSONObject;
        Editor edit = PreferenceManager.getDefaultSharedPreferences(context2).edit();
        edit.putString("beaconConfiguration", jSONObject.toString());
        edit.putString("revmobJSON", RevMobContext.revmobJSON().toString());
        edit.apply();
        context2.startService(new Intent(context, RevMobBeaconManager.class));
    }

    public void onReceive(Context context2, Intent intent) {
        RMLog.i("BroadcastReceiver received!!");
        context2.startService(new Intent(context2, RevMobBeaconManager.class));
    }
}
