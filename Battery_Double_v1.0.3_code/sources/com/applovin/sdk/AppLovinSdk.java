package com.applovin.sdk;

import android.content.Context;
import android.util.Log;
import com.applovin.impl.sdk.AppLovinSdkImpl;

public abstract class AppLovinSdk {
    public static final String URI_HOST = "com.applovin.sdk";
    public static final String URI_SCHEME = "applovin";
    public static final String VERSION = "5.4.3";
    private static AppLovinSdk[] a = new AppLovinSdk[0];
    private static final Object b = new Object();

    public static AppLovinSdk getInstance(Context context) {
        if (context != null) {
            return getInstance(AppLovinSdkUtils.retrieveSdkKey(context), AppLovinSdkUtils.retrieveUserSettings(context), context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public static AppLovinSdk getInstance(AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (context != null) {
            return getInstance(AppLovinSdkUtils.retrieveSdkKey(context), appLovinSdkSettings, context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public static AppLovinSdk getInstance(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        AppLovinSdk[] appLovinSdkArr;
        synchronized (b) {
            if (a.length != 1 || !a[0].getSdkKey().equals(str)) {
                for (AppLovinSdk appLovinSdk : a) {
                    if (appLovinSdk.getSdkKey().equals(str)) {
                        return appLovinSdk;
                    }
                }
                try {
                    AppLovinSdkImpl appLovinSdkImpl = new AppLovinSdkImpl();
                    appLovinSdkImpl.initialize(str, appLovinSdkSettings, context.getApplicationContext());
                    AppLovinSdk[] appLovinSdkArr2 = new AppLovinSdk[(a.length + 1)];
                    System.arraycopy(a, 0, appLovinSdkArr2, 0, a.length);
                    appLovinSdkArr2[a.length] = appLovinSdkImpl;
                    a = appLovinSdkArr2;
                    return appLovinSdkImpl;
                } catch (Throwable th) {
                    Log.e(AppLovinLogger.SDK_TAG, "Failed to create AppLovin SDK. Try cleaning application data and starting the applion again.", th);
                    throw new RuntimeException("Unable to create AppLovin SDK");
                }
            } else {
                AppLovinSdk appLovinSdk2 = a[0];
                return appLovinSdk2;
            }
        }
    }

    public static void initializeSdk(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("No context specified");
        }
        AppLovinSdk instance = getInstance(context);
        if (instance != null) {
            instance.initializeSdk();
        } else {
            Log.e(AppLovinLogger.SDK_TAG, "Unable to initialize AppLovin SDK: SDK object not created");
        }
    }

    public abstract AppLovinAdService getAdService();

    public abstract Context getApplicationContext();

    public abstract AppLovinLogger getLogger();

    public abstract String getSdkKey();

    public abstract AppLovinSdkSettings getSettings();

    public abstract AppLovinTargetingData getTargetingData();

    public abstract boolean hasCriticalErrors();

    /* access modifiers changed from: protected */
    public abstract void initialize(String str, AppLovinSdkSettings appLovinSdkSettings, Context context);

    public abstract void initializeSdk();

    public abstract boolean isEnabled();

    public abstract void setPluginVersion(String str);
}
