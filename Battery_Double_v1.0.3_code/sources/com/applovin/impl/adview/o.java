package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.Rect;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.applovin.impl.sdk.AppLovinAdInternal;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;

class o extends WebView {
    private final AppLovinLogger a;
    private AppLovinAd b = null;
    private boolean c = false;

    o(r rVar, AppLovinSdk appLovinSdk, Context context) {
        super(context);
        this.a = appLovinSdk.getLogger();
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setSupportMultipleWindows(false);
        settings.setJavaScriptEnabled(true);
        setWebViewClient(rVar);
        setWebChromeClient(new n(appLovinSdk));
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setScrollBarStyle(33554432);
        setOnTouchListener(new p(this));
        setOnLongClickListener(new q(this));
    }

    /* access modifiers changed from: 0000 */
    public AppLovinAd a() {
        return this.b;
    }

    public void a(AppLovinAd appLovinAd) {
        if (!this.c) {
            this.b = appLovinAd;
            try {
                loadDataWithBaseURL("/", ((AppLovinAdInternal) appLovinAd).getHtmlSource(), "text/html", null, "");
                this.a.d("AdWebView", "AppLovinAd rendered");
            } catch (Exception unused) {
            }
        } else {
            this.a.userError("AdWebView", "Ad can not be loaded in a destroyed web view");
        }
    }

    public void destroy() {
        this.c = true;
        try {
            super.destroy();
            this.a.d("AdWebView", "Web view destroyed");
        } catch (Throwable th) {
            if (this.a != null) {
                this.a.e("AdWebView", "destroy() threw exception", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        try {
            super.onFocusChanged(z, i, rect);
        } catch (Exception e) {
            this.a.e("AdWebView", "onFocusChanged() threw exception", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
    }

    public void onWindowFocusChanged(boolean z) {
        try {
            super.onWindowFocusChanged(z);
        } catch (Exception e) {
            this.a.e("AdWebView", "onWindowFocusChanged() threw exception", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        try {
            super.onWindowVisibilityChanged(i);
        } catch (Exception e) {
            this.a.e("AdWebView", "onWindowVisibilityChanged() threw exception", e);
        }
    }

    public boolean requestFocus(int i, Rect rect) {
        try {
            return super.requestFocus(i, rect);
        } catch (Exception e) {
            this.a.e("AdWebView", "requestFocus() threw exception", e);
            return false;
        }
    }
}
