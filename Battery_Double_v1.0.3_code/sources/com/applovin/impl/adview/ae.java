package com.applovin.impl.adview;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;

class ae implements AppLovinAdLoadListener {
    final /* synthetic */ ad a;

    ae(ad adVar) {
        this.a = adVar;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        this.a.a(appLovinAd);
        this.a.showAndRender(appLovinAd);
    }

    public void failedToReceiveAd(int i) {
        this.a.a(i);
    }
}
