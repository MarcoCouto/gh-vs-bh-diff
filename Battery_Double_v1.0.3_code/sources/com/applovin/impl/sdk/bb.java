package com.applovin.impl.sdk;

import android.support.v4.os.EnvironmentCompat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.altbeacon.beacon.BeaconManager;

class bb {
    public static final bd A = a("ad_refresh_seconds", Long.valueOf(120));
    public static final bd B = a("mrec_ad_refresh_enabled", Boolean.valueOf(true));
    public static final bd C = a("mrec_ad_refresh_seconds", Long.valueOf(120));
    public static final bd D = a("leader_ad_refresh_enabled", Boolean.valueOf(true));
    public static final bd E = a("leader_ad_refresh_seconds", Long.valueOf(120));
    public static final bd F = a("plugin_version", "");
    public static final bd G = a("ad_preload_enabled", Boolean.valueOf(true));
    public static final bd H = a("ad_resource_caching_enabled", Boolean.valueOf(true));
    public static final bd I = a("resource_cache_prefix", "http://vid.applovin.com/,http://pdn.applovin.com/,http://img.applovin.com/,http://d.applovin.com/,http://assets.applovin.com/,http://cdnjs.cloudflare.com/");
    public static final bd J = a("ad_auto_preload_sizes", "BANNER,INTER");
    public static final bd K = a("ad_auto_preload_incent", Boolean.valueOf(true));
    public static final bd L = a("session_expiration_time", Long.valueOf(300));
    @Deprecated
    public static final bd M = a("track_installed_apps", Boolean.valueOf(true));
    public static final bd N = a("is_tracking_enabled", Boolean.valueOf(true));
    public static final bd O = a("force_back_button_enabled_always", Boolean.valueOf(false));
    public static final bd P = a("is_first_install", EnvironmentCompat.MEDIA_UNKNOWN);
    public static final bd Q = a("countdown_direction", "right_to_left");
    public static final bd R = a("countdown_color", "#C8FFFFFF");
    public static final bd S = a("countdown_height", Integer.valueOf(2));
    public static final bd T = a("close_fade_in_time", Integer.valueOf(400));
    public static final bd U = a("draw_countdown_text", Boolean.valueOf(true));
    public static final bd V = a("draw_countdown_bar", Boolean.valueOf(true));
    public static final bd W = a("show_close_on_exit", Boolean.valueOf(true));
    public static final bd X = a("text_incent_prompt_title", "Earn a Reward");
    public static final bd Y = a("text_incent_prompt_body", "Would you like to watch a video for a reward?");
    public static final bd Z = a("text_incent_prompt_yes_option", "Watch Now");
    public static final bd a = a("is_disabled", Boolean.valueOf(false));
    public static final bd aA = a("cache_cleanup_enabled", Boolean.valueOf(false));
    public static final bd aB = a("cache_file_ttl_seconds", Long.valueOf(86400));
    public static final bd aC = a("cache_max_size_mb", Integer.valueOf(-1));
    public static final bd aD = a("preload_merge_init_tasks_incent", Boolean.valueOf(true));
    public static final bd aE = a("preload_merge_init_tasks_inter", Boolean.valueOf(true));
    private static final List aF = Arrays.asList(new Class[]{Boolean.class, Float.class, Integer.class, Long.class, String.class});
    private static final List aG = new ArrayList();
    public static final bd aa = a("text_incent_prompt_no_option", "No Thanks");
    public static final bd ab = a("text_incent_completion_title", "Video Reward");
    public static final bd ac = a("text_incent_completion_body_success", "You have earned a reward!");
    public static final bd ad = a("text_incent_completion_body_quota_exceeded", "You have already earned the maximum reward for today.");
    public static final bd ae = a("text_incent_completion_body_reward_rejected", "Your reward was rejected.");
    public static final bd af = a("text_incent_completion_body_network_failure", "We were unable to contact the rewards server. Please try again later.");
    public static final bd ag = a("text_incent_completion_close_option", "Okay");
    public static final bd ah = a("show_incent_prepopup", Boolean.valueOf(true));
    public static final bd ai = a("show_incent_postpopup", Boolean.valueOf(true));
    public static final bd aj = a("preload_capacity_banner", Integer.valueOf(1));
    public static final bd ak = a("preload_capacity_mrec", Integer.valueOf(1));
    public static final bd al = a("preload_capacity_inter", Integer.valueOf(1));
    public static final bd am = a("preload_capacity_leader", Integer.valueOf(1));
    public static final bd an = a("preload_capacity_incent", Integer.valueOf(2));
    public static final bd ao = a("dismiss_video_on_error", Boolean.valueOf(true));
    public static final bd ap = a("precache_delimiters", ")]',");
    public static final bd aq = a("close_button_size_graphic", Integer.valueOf(27));
    public static final bd ar = a("close_button_size_video", Integer.valueOf(30));
    public static final bd as = a("close_button_top_margin_graphic", Integer.valueOf(10));
    public static final bd at = a("close_button_right_margin_graphic", Integer.valueOf(10));
    public static final bd au = a("close_button_top_margin_video", Integer.valueOf(8));
    public static final bd av = a("close_button_right_margin_video", Integer.valueOf(4));
    public static final bd aw = a("force_back_button_enabled_poststitial", Boolean.valueOf(false));
    public static final bd ax = a("force_back_button_enabled_close_button", Boolean.valueOf(false));
    public static final bd ay = a("close_button_touch_area", Integer.valueOf(0));
    public static final bd az = a("is_video_skippable", Boolean.valueOf(false));
    public static final bd b = a("honor_publisher_settings", Boolean.valueOf(true));
    public static final bd c = a("device_id", "");
    public static final bd d = a("publisher_id", "");
    public static final bd e = a("device_token", "");
    public static final bd f = a("init_retry_count", Integer.valueOf(1));
    public static final bd g = a("submit_data_retry_count", Integer.valueOf(1));
    public static final bd h = a("vr_retry_count", Integer.valueOf(1));
    public static final bd i = a("fetch_ad_retry_count", Integer.valueOf(1));
    public static final bd j = a("is_verbose_logging", Boolean.valueOf(false));
    public static final bd k = a("api_endpoint", "http://d.applovin.com/");
    public static final bd l = a("adserver_endpoint", "http://a.applovin.com/2.0/");
    public static final bd m = a("next_device_init", Long.valueOf(0));
    public static final bd n = a("get_retry_delay", Long.valueOf(BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD));
    @Deprecated
    public static final bd o = a("max_apps_to_send", Integer.valueOf(100));
    public static final bd p = a("is_app_list_shared", Boolean.valueOf(true));
    public static final bd q = a("next_app_list_update", Long.valueOf(0));
    public static final bd r = a("hash_algorithm", "SHA-1");
    public static final bd s = a("short_hash_size", Integer.valueOf(16));
    public static final bd t = a("http_connection_timeout", Integer.valueOf(30000));
    public static final bd u = a("fetch_ad_connection_timeout", Integer.valueOf(30000));
    public static final bd v = a("http_socket_timeout", Integer.valueOf(20000));
    public static final bd w = a("error_save_count", Integer.valueOf(15));
    public static final bd x = a("ad_session_minutes", Integer.valueOf(60));
    public static final bd y = a("ad_request_parameters", "");
    public static final bd z = a("ad_refresh_enabled", Boolean.valueOf(true));

    private static bd a(String str, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("No default value specified");
        } else if (!aF.contains(obj.getClass())) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unsupported value type: ");
            sb.append(obj.getClass());
            throw new IllegalArgumentException(sb.toString());
        } else {
            bd bdVar = new bd(str, obj);
            aG.add(bdVar);
            return bdVar;
        }
    }

    public static Collection a() {
        return Collections.unmodifiableList(aG);
    }

    public static int b() {
        return aG.size();
    }
}
