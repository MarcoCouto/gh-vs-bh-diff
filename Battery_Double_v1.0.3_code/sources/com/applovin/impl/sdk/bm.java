package com.applovin.impl.sdk;

import android.content.Context;
import android.support.v4.os.EnvironmentCompat;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinLogger;

class bm implements Runnable {
    private final AppLovinSdkImpl a;
    private final AppLovinLogger b;
    private final Context c;

    bm(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.c = appLovinSdkImpl.getApplicationContext();
        this.b = appLovinSdkImpl.getLogger();
    }

    private void c() {
        String str = (String) this.a.a(bb.J);
        e eVar = (e) this.a.getAdService();
        if (str.length() > 0) {
            for (String fromString : str.split(",")) {
                eVar.a(AppLovinAdSize.fromString(fromString), AppLovinAdType.REGULAR);
            }
        }
        if (((Boolean) this.a.a(bb.K)).booleanValue()) {
            eVar.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        if (q.a("android.permission.INTERNET", this.c)) {
            return true;
        }
        this.b.userError("TaskInitializeSdk", "Unable to enable AppLovin SDK: no android.permission.INTERNET");
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (cd.a(bb.m, this.a)) {
            this.a.a().a((ba) new bi(this.a), bo.BACKGROUND, 1500);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0077, code lost:
        if (r10.a.isEnabled() != false) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0079, code lost:
        r5 = "succeeded";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x007c, code lost:
        r5 = "failed";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x007e, code lost:
        r4.append(r5);
        r4.append(" in ");
        r4.append(java.lang.System.currentTimeMillis() - r0);
        r4.append("ms");
        r2.d(r3, r4.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x009b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00c1, code lost:
        if (r10.a.isEnabled() == false) goto L_0x007c;
     */
    public void run() {
        AppLovinLogger appLovinLogger;
        String str;
        StringBuilder sb;
        long currentTimeMillis = System.currentTimeMillis();
        this.b.d("TaskInitializeSdk", "Initializing AppLovin SDK 5.4.3...");
        try {
            if (a()) {
                bg b2 = this.a.b();
                b2.c();
                b2.c("ad_imp_session");
                a.b(this.a);
                this.a.getFileManager().d(this.c);
                b();
                c();
                if (((String) this.a.a(bb.P)).equals(EnvironmentCompat.MEDIA_UNKNOWN)) {
                    this.a.getSettingsManager().a(bb.P, "true");
                }
                this.a.a(true);
            } else {
                this.a.a(false);
            }
            appLovinLogger = this.b;
            str = "TaskInitializeSdk";
            sb = new StringBuilder();
            sb.append("AppLovin SDK 5.4.3 initialization ");
        } catch (Throwable th) {
            AppLovinLogger appLovinLogger2 = this.b;
            String str2 = "TaskInitializeSdk";
            StringBuilder sb2 = new StringBuilder();
            sb2.append("AppLovin SDK 5.4.3 initialization ");
            sb2.append(this.a.isEnabled() ? "succeeded" : "failed");
            sb2.append(" in ");
            sb2.append(System.currentTimeMillis() - currentTimeMillis);
            sb2.append("ms");
            appLovinLogger2.d(str2, sb2.toString());
            throw th;
        }
    }
}
