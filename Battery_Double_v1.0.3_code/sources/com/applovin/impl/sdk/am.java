package com.applovin.impl.sdk;

import android.content.Context;
import android.content.Intent;
import com.applovin.adview.AppLovinConfirmationActivity;

class am implements Runnable {
    final /* synthetic */ al a;

    am(al alVar) {
        this.a = alVar;
    }

    public void run() {
        String str = (String) this.a.a.a(bb.ab);
        String b = this.a.b();
        String str2 = (String) this.a.a.a(bb.ag);
        if (m.a(AppLovinConfirmationActivity.class, (Context) this.a.c)) {
            try {
                Intent intent = new Intent(this.a.c, AppLovinConfirmationActivity.class);
                intent.putExtra("dialog_title", str);
                intent.putExtra("dialog_body", b);
                intent.putExtra("dialog_button_text", str2);
                this.a.c.startActivity(intent);
            } catch (Throwable th) {
                this.a.a(b, th);
            }
        } else {
            this.a.a(b, null);
        }
    }
}
