package com.applovin.impl.sdk;

import org.json.JSONObject;

class bw implements o {
    final /* synthetic */ bv a;

    bw(bv bvVar) {
        this.a = bvVar;
    }

    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to report reward for ad: ");
        sb.append(this.a.a.getAdIdNumber());
        sb.append(" - error code: ");
        sb.append(i);
        this.a.g.d("TaskReportReward", sb.toString());
    }

    public void a(JSONObject jSONObject, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Reported reward successfully for ad: ");
        sb.append(this.a.a.getAdIdNumber());
        this.a.g.d("TaskReportReward", sb.toString());
    }
}
