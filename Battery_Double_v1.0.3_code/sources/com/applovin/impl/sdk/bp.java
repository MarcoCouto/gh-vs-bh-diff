package com.applovin.impl.sdk;

import java.util.concurrent.ThreadFactory;

class bp implements ThreadFactory {
    final /* synthetic */ bn a;
    private final String b;

    public bp(bn bnVar, String str) {
        this.a = bnVar;
        this.b = str;
    }

    public Thread newThread(Runnable runnable) {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinSdk:");
        sb.append(this.b);
        sb.append(":");
        sb.append(cd.a(this.a.a.getSdkKey()));
        Thread thread = new Thread(runnable, sb.toString());
        thread.setDaemon(true);
        thread.setPriority(10);
        thread.setUncaughtExceptionHandler(new bq(this));
        return thread;
    }
}
