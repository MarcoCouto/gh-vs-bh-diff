package com.google.firebase.remoteconfig;

public class FirebaseRemoteConfigFetchThrottledException extends FirebaseRemoteConfigFetchException {
    private final long BG;

    public FirebaseRemoteConfigFetchThrottledException(long j) {
        this.BG = j;
    }

    public long getThrottleEndTimeMillis() {
        return this.BG;
    }
}
