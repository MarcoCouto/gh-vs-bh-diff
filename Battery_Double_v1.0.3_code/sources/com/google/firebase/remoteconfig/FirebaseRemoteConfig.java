package com.google.firebase.remoteconfig;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.internal.zzals;
import com.google.android.gms.internal.zzalt;
import com.google.android.gms.internal.zzalu;
import com.google.android.gms.internal.zzalv;
import com.google.android.gms.internal.zzalw;
import com.google.android.gms.internal.zzalx;
import com.google.android.gms.internal.zzaly.zzb;
import com.google.android.gms.internal.zzaly.zzc;
import com.google.android.gms.internal.zzaly.zzd;
import com.google.android.gms.internal.zzaly.zze;
import com.google.android.gms.internal.zzaly.zzf;
import com.google.android.gms.internal.zzapn;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.internal.zzrr.zza.C0086zza;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings.Builder;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class FirebaseRemoteConfig {
    public static final boolean DEFAULT_VALUE_FOR_BOOLEAN = false;
    public static final byte[] DEFAULT_VALUE_FOR_BYTE_ARRAY = new byte[0];
    public static final double DEFAULT_VALUE_FOR_DOUBLE = 0.0d;
    public static final long DEFAULT_VALUE_FOR_LONG = 0;
    public static final String DEFAULT_VALUE_FOR_STRING = "";
    public static final int LAST_FETCH_STATUS_FAILURE = 1;
    public static final int LAST_FETCH_STATUS_NO_FETCH_YET = 0;
    public static final int LAST_FETCH_STATUS_SUCCESS = -1;
    public static final int LAST_FETCH_STATUS_THROTTLED = 2;
    public static final int VALUE_SOURCE_DEFAULT = 1;
    public static final int VALUE_SOURCE_REMOTE = 2;
    public static final int VALUE_SOURCE_STATIC = 0;
    private static FirebaseRemoteConfig baZ;
    private zzalu bba;
    private zzalu bbb;
    private zzalu bbc;
    private zzalx bbd;
    private final ReadWriteLock bbe;
    private final Context mContext;

    static class zza implements Executor {
        zza() {
        }

        public void execute(Runnable runnable) {
            new Thread(runnable).start();
        }
    }

    FirebaseRemoteConfig(Context context) {
        this(context, null, null, null, null);
    }

    private FirebaseRemoteConfig(Context context, zzalu zzalu, zzalu zzalu2, zzalu zzalu3, zzalx zzalx) {
        this.bbe = new ReentrantReadWriteLock(true);
        this.mContext = context;
        if (zzalx != null) {
            this.bbd = zzalx;
        } else {
            this.bbd = new zzalx();
        }
        this.bbd.zzcp(zzeu(this.mContext));
        if (zzalu != null) {
            this.bba = zzalu;
        }
        if (zzalu2 != null) {
            this.bbb = zzalu2;
        }
        if (zzalu3 != null) {
            this.bbc = zzalu3;
        }
    }

    public static FirebaseRemoteConfig getInstance() {
        if (baZ != null) {
            return baZ;
        }
        FirebaseApp instance = FirebaseApp.getInstance();
        if (instance != null) {
            return zzet(instance.getApplicationContext());
        }
        throw new IllegalStateException("FirebaseApp has not been initialized.");
    }

    private static zzalu zza(com.google.android.gms.internal.zzaly.zza zza2) {
        zzd[] zzdArr;
        zzb[] zzbArr;
        if (zza2 == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (zzd zzd : zza2.bbu) {
            String str = zzd.zx;
            HashMap hashMap2 = new HashMap();
            for (zzb zzb : zzd.bbA) {
                hashMap2.put(zzb.zzcb, zzb.bbw);
            }
            hashMap.put(str, hashMap2);
        }
        return new zzalu(hashMap, zza2.timestamp);
    }

    private static zzalx zza(zzc zzc) {
        if (zzc == null) {
            return null;
        }
        zzalx zzalx = new zzalx();
        zzalx.zzafe(zzc.bbx);
        zzalx.zzcw(zzc.bby);
        return zzalx;
    }

    private static Map<String, zzals> zza(zzf[] zzfArr) {
        HashMap hashMap = new HashMap();
        if (zzfArr == null) {
            return hashMap;
        }
        for (zzf zzf : zzfArr) {
            hashMap.put(zzf.zx, new zzals(zzf.resourceId, zzf.bbH));
        }
        return hashMap;
    }

    private static long zzb(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    private void zzc(Map<String, Object> map, String str, boolean z) {
        zzalu zzalu;
        long currentTimeMillis;
        String bool;
        byte[] bArr;
        if (str != null) {
            boolean z2 = map == null || map.isEmpty();
            HashMap hashMap = new HashMap();
            if (!z2) {
                for (String str2 : map.keySet()) {
                    Object obj = map.get(str2);
                    if (obj instanceof String) {
                        bool = (String) obj;
                    } else if (obj instanceof Long) {
                        bool = ((Long) obj).toString();
                    } else if (obj instanceof Integer) {
                        bool = ((Integer) obj).toString();
                    } else if (obj instanceof Double) {
                        bool = ((Double) obj).toString();
                    } else if (obj instanceof Float) {
                        bool = ((Float) obj).toString();
                    } else if (obj instanceof byte[]) {
                        bArr = (byte[]) obj;
                        hashMap.put(str2, bArr);
                    } else if (obj instanceof Boolean) {
                        bool = ((Boolean) obj).toString();
                    } else {
                        throw new IllegalArgumentException("The type of a default value needs to beone of String, Long, Double, Boolean, or byte[].");
                    }
                    bArr = bool.getBytes(zzalw.UTF_8);
                    hashMap.put(str2, bArr);
                }
            }
            this.bbe.writeLock().lock();
            if (z2) {
                try {
                    if (this.bbc != null) {
                        if (this.bbc.zztc(str)) {
                            this.bbc.zzk(null, str);
                            zzalu = this.bbc;
                            currentTimeMillis = System.currentTimeMillis();
                        }
                    }
                    return;
                } finally {
                    this.bbe.writeLock().unlock();
                }
            } else {
                if (this.bbc == null) {
                    this.bbc = new zzalu(new HashMap(), System.currentTimeMillis());
                }
                this.bbc.zzk(hashMap, str);
                zzalu = this.bbc;
                currentTimeMillis = System.currentTimeMillis();
            }
            zzalu.setTimestamp(currentTimeMillis);
            if (z) {
                this.bbd.zztd(str);
            }
            zzcxe();
            this.bbe.writeLock().unlock();
        }
    }

    private void zzcxe() {
        this.bbe.readLock().lock();
        try {
            zzalt zzalt = new zzalt(this.mContext, this.bba, this.bbb, this.bbc, this.bbd);
            if (VERSION.SDK_INT >= 11) {
                AsyncTask.SERIAL_EXECUTOR.execute(zzalt);
            } else {
                new zza().execute(zzalt);
            }
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public static FirebaseRemoteConfig zzet(Context context) {
        FirebaseRemoteConfig firebaseRemoteConfig;
        if (baZ == null) {
            zze zzev = zzev(context);
            if (zzev == null) {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "No persisted config was found. Initializing from scratch.");
                }
                firebaseRemoteConfig = new FirebaseRemoteConfig(context);
            } else {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "Initializing from persisted config.");
                }
                zzalu zza2 = zza(zzev.bbB);
                zzalu zza3 = zza(zzev.bbC);
                zzalu zza4 = zza(zzev.bbD);
                zzalx zza5 = zza(zzev.bbE);
                if (zza5 != null) {
                    zza5.zzcd(zza(zzev.bbF));
                }
                firebaseRemoteConfig = new FirebaseRemoteConfig(context, zza2, zza3, zza4, zza5);
            }
            baZ = firebaseRemoteConfig;
        }
        return baZ;
    }

    private long zzeu(Context context) {
        try {
            return this.mContext.getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
        } catch (NameNotFoundException unused) {
            String valueOf = String.valueOf(context.getPackageName());
            StringBuilder sb = new StringBuilder(25 + String.valueOf(valueOf).length());
            sb.append("Package [");
            sb.append(valueOf);
            sb.append("] was not found!");
            Log.e("FirebaseRemoteConfig", sb.toString());
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0040 A[SYNTHETIC, Splitter:B:25:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0058 A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0061 A[SYNTHETIC, Splitter:B:39:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0071 A[SYNTHETIC, Splitter:B:47:0x0071] */
    private static zze zzev(Context context) {
        FileInputStream fileInputStream;
        if (context == null) {
            return null;
        }
        try {
            fileInputStream = context.openFileInput("persisted_config");
            try {
                zzapn zzbd = zzapn.zzbd(zzl(fileInputStream));
                zze zze = new zze();
                zze zze2 = (zze) zze.zzb(zzbd);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                        return zze;
                    } catch (IOException e) {
                        Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e);
                    }
                }
                return zze;
            } catch (FileNotFoundException e2) {
                e = e2;
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                }
                if (fileInputStream != null) {
                }
                return null;
            } catch (IOException e3) {
                e = e3;
                try {
                    Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e);
                    if (fileInputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (FileNotFoundException e4) {
            e = e4;
            fileInputStream = null;
            if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                Log.d("FirebaseRemoteConfig", "Persisted config file was not found.", e);
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                    return null;
                } catch (IOException e5) {
                    Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e5);
                    return null;
                }
            }
            return null;
        } catch (IOException e6) {
            e = e6;
            fileInputStream = null;
            Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e);
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                    return null;
                } catch (IOException e7) {
                    Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e7);
                    return null;
                }
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e8) {
                    Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e8);
                }
            }
            throw th;
        }
    }

    private static byte[] zzl(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        zzb(inputStream, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public boolean activateFetched() {
        this.bbe.writeLock().lock();
        try {
            if (this.bba != null) {
                if (this.bbb == null || this.bbb.getTimestamp() < this.bba.getTimestamp()) {
                    long timestamp = this.bba.getTimestamp();
                    this.bbb = this.bba;
                    this.bbb.setTimestamp(System.currentTimeMillis());
                    this.bba = new zzalu(null, timestamp);
                    zzcxe();
                    this.bbe.writeLock().unlock();
                    return true;
                }
            }
            return false;
        } finally {
            this.bbe.writeLock().unlock();
        }
    }

    public Task<Void> fetch() {
        return fetch(43200);
    }

    /* JADX INFO: finally extract failed */
    public Task<Void> fetch(long j) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.bbe.writeLock().lock();
        try {
            C0086zza zza2 = new C0086zza();
            zza2.zzah(j);
            if (this.bbd.isDeveloperModeEnabled()) {
                zza2.zzah("_rcn_developer", "true");
            }
            new com.google.android.gms.config.internal.zzb(this.mContext).zza(zza2.zzawj()).setResultCallback(new ResultCallback<zzrr.zzb>() {
                /* renamed from: zza */
                public void onResult(@NonNull zzrr.zzb zzb) {
                    FirebaseRemoteConfig.this.zza(taskCompletionSource, zzb);
                }
            });
            this.bbe.writeLock().unlock();
            return taskCompletionSource.getTask();
        } catch (Throwable th) {
            this.bbe.writeLock().unlock();
            throw th;
        }
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, "configns:firebase");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        if (com.google.android.gms.internal.zzalw.BA.matcher(r1).matches() != false) goto L_0x0049;
     */
    public boolean getBoolean(String str, String str2) {
        if (str2 == null) {
            return false;
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                String str3 = new String(this.bbb.zzbu(str, str2), zzalw.UTF_8);
                if (!zzalw.Bz.matcher(str3).matches()) {
                }
                return true;
            }
            if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                String str4 = new String(this.bbc.zzbu(str, str2), zzalw.UTF_8);
                if (zzalw.Bz.matcher(str4).matches()) {
                    return true;
                }
                boolean matches = zzalw.BA.matcher(str4).matches();
            }
            this.bbe.readLock().unlock();
            return false;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public byte[] getByteArray(String str) {
        return getByteArray(str, "configns:firebase");
    }

    public byte[] getByteArray(String str, String str2) {
        byte[] bArr;
        zzalu zzalu;
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_BYTE_ARRAY;
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                zzalu = this.bbb;
            } else if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                bArr = DEFAULT_VALUE_FOR_BYTE_ARRAY;
                return bArr;
            } else {
                zzalu = this.bbc;
            }
            bArr = zzalu.zzbu(str, str2);
            return bArr;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public double getDouble(String str) {
        return getDouble(str, "configns:firebase");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:3|4|5|(5:9|10|11|12|13)|14|15|(5:19|20|21|22|23)|24|26) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0039 */
    public double getDouble(String str, String str2) {
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_DOUBLE;
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                double doubleValue = Double.valueOf(new String(this.bbb.zzbu(str, str2), zzalw.UTF_8)).doubleValue();
                return doubleValue;
            }
            if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                try {
                    double doubleValue2 = Double.valueOf(new String(this.bbc.zzbu(str, str2), zzalw.UTF_8)).doubleValue();
                    this.bbe.readLock().unlock();
                    return doubleValue2;
                } catch (NumberFormatException unused) {
                }
            }
            this.bbe.readLock().unlock();
            return DEFAULT_VALUE_FOR_DOUBLE;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public FirebaseRemoteConfigInfo getInfo() {
        zzalv zzalv = new zzalv();
        this.bbe.readLock().lock();
        try {
            zzalv.zzco(this.bba == null ? -1 : this.bba.getTimestamp());
            zzalv.zzafe(this.bbd.getLastFetchStatus());
            zzalv.setConfigSettings(new Builder().setDeveloperModeEnabled(this.bbd.isDeveloperModeEnabled()).build());
            return zzalv;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public Set<String> getKeysByPrefix(String str) {
        return getKeysByPrefix(str, "configns:firebase");
    }

    public Set<String> getKeysByPrefix(String str, String str2) {
        this.bbe.readLock().lock();
        try {
            Set<String> zzbv = this.bbb == null ? new TreeSet<>() : this.bbb.zzbv(str, str2);
            return zzbv;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public long getLong(String str) {
        return getLong(str, "configns:firebase");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:3|4|5|(5:9|10|11|12|13)|14|15|(5:19|20|21|22|23)|24|26) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0039 */
    public long getLong(String str, String str2) {
        if (str2 == null) {
            return 0;
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                long longValue = Long.valueOf(new String(this.bbb.zzbu(str, str2), zzalw.UTF_8)).longValue();
                return longValue;
            }
            if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                try {
                    long longValue2 = Long.valueOf(new String(this.bbc.zzbu(str, str2), zzalw.UTF_8)).longValue();
                    this.bbe.readLock().unlock();
                    return longValue2;
                } catch (NumberFormatException unused) {
                }
            }
            this.bbe.readLock().unlock();
            return 0;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public String getString(String str) {
        return getString(str, "configns:firebase");
    }

    public String getString(String str, String str2) {
        String str3;
        if (str2 == null) {
            return "";
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                str3 = new String(this.bbb.zzbu(str, str2), zzalw.UTF_8);
            } else if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                String str4 = "";
                this.bbe.readLock().unlock();
                return str4;
            } else {
                str3 = new String(this.bbc.zzbu(str, str2), zzalw.UTF_8);
            }
            return str3;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public FirebaseRemoteConfigValue getValue(String str) {
        return getValue(str, "configns:firebase");
    }

    public FirebaseRemoteConfigValue getValue(String str, String str2) {
        zzalw zzalw;
        if (str2 == null) {
            return new zzalw(DEFAULT_VALUE_FOR_BYTE_ARRAY, 0);
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                zzalw = new zzalw(this.bbb.zzbu(str, str2), 2);
            } else if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                zzalw zzalw2 = new zzalw(DEFAULT_VALUE_FOR_BYTE_ARRAY, 0);
                this.bbe.readLock().unlock();
                return zzalw2;
            } else {
                zzalw = new zzalw(this.bbc.zzbu(str, str2), 1);
            }
            return zzalw;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public void setConfigSettings(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.bbe.writeLock().lock();
        try {
            boolean isDeveloperModeEnabled = this.bbd.isDeveloperModeEnabled();
            boolean isDeveloperModeEnabled2 = firebaseRemoteConfigSettings == null ? false : firebaseRemoteConfigSettings.isDeveloperModeEnabled();
            this.bbd.zzcw(isDeveloperModeEnabled2);
            if (isDeveloperModeEnabled != isDeveloperModeEnabled2) {
                zzcxe();
            }
        } finally {
            this.bbe.writeLock().unlock();
        }
    }

    public void setDefaults(int i) {
        setDefaults(i, "configns:firebase");
    }

    public void setDefaults(int i, String str) {
        if (str == null) {
            if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                Log.d("FirebaseRemoteConfig", "namespace cannot be null for setDefaults.");
            }
            return;
        }
        this.bbe.readLock().lock();
        try {
            if (!(this.bbd == null || this.bbd.zzcxk() == null || this.bbd.zzcxk().get(str) == null)) {
                zzals zzals = (zzals) this.bbd.zzcxk().get(str);
                if (i == zzals.zzcxf() && this.bbd.zzcxl() == zzals.zzcxg()) {
                    if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                        Log.d("FirebaseRemoteConfig", "Skipped setting defaults from resource file as this resource file was already applied.");
                    }
                    return;
                }
            }
            this.bbe.readLock().unlock();
            HashMap hashMap = new HashMap();
            try {
                XmlResourceParser xml = this.mContext.getResources().getXml(i);
                Object obj = null;
                Object obj2 = null;
                Object obj3 = null;
                for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                    if (eventType == 2) {
                        obj = xml.getName();
                    } else if (eventType == 3) {
                        if (!(!"entry".equals(xml.getName()) || obj2 == null || obj3 == null)) {
                            hashMap.put(obj2, obj3);
                            obj2 = null;
                            obj3 = null;
                        }
                        obj = null;
                    } else if (eventType == 4) {
                        if ("key".equals(obj)) {
                            obj2 = xml.getText();
                        } else if (Param.VALUE.equals(obj)) {
                            obj3 = xml.getText();
                        }
                    }
                }
                this.bbd.zza(str, new zzals(i, this.bbd.zzcxl()));
                zzc(hashMap, str, false);
            } catch (Exception e) {
                Log.e("FirebaseRemoteConfig", "Caught exception while parsing XML resource. Skipping setDefaults.", e);
            }
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public void setDefaults(Map<String, Object> map) {
        setDefaults(map, "configns:firebase");
    }

    public void setDefaults(Map<String, Object> map, String str) {
        zzc(map, str, true);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005a, code lost:
        r11.setException(r12);
     */
    @VisibleForTesting
    public void zza(TaskCompletionSource<Void> taskCompletionSource, zzrr.zzb zzb) {
        FirebaseRemoteConfigFetchException firebaseRemoteConfigFetchException;
        if (zzb == null || zzb.getStatus() == null) {
            this.bbd.zzafe(1);
            taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
            zzcxe();
            return;
        }
        int statusCode = zzb.getStatus().getStatusCode();
        if (statusCode != -6508) {
            if (statusCode != 6507) {
                switch (statusCode) {
                    case -6506:
                        break;
                    case -6505:
                        Map zzawk = zzb.zzawk();
                        HashMap hashMap = new HashMap();
                        for (String str : zzawk.keySet()) {
                            HashMap hashMap2 = new HashMap();
                            for (String str2 : (Set) zzawk.get(str)) {
                                hashMap2.put(str2, zzb.zza(str2, null, str));
                            }
                            hashMap.put(str, hashMap2);
                        }
                        this.bba = new zzalu(hashMap, System.currentTimeMillis());
                        this.bbd.zzafe(-1);
                        break;
                    default:
                        switch (statusCode) {
                            case 6500:
                            case 6501:
                            case 6503:
                            case 6504:
                                this.bbd.zzafe(1);
                                firebaseRemoteConfigFetchException = new FirebaseRemoteConfigFetchException();
                                break;
                            case 6502:
                                break;
                            default:
                                if (zzb.getStatus().isSuccess()) {
                                    StringBuilder sb = new StringBuilder(45);
                                    sb.append("Unknown (successful) status code: ");
                                    sb.append(statusCode);
                                    Log.w("FirebaseRemoteConfig", sb.toString());
                                }
                                this.bbd.zzafe(1);
                                firebaseRemoteConfigFetchException = new FirebaseRemoteConfigFetchException();
                                break;
                        }
                }
            }
            this.bbd.zzafe(2);
            taskCompletionSource.setException(new FirebaseRemoteConfigFetchThrottledException(zzb.getThrottleEndTimeMillis()));
            zzcxe();
        }
        this.bbd.zzafe(-1);
        if (this.bba != null && !this.bba.zzcxi()) {
            Map zzawk2 = zzb.zzawk();
            HashMap hashMap3 = new HashMap();
            for (String str3 : zzawk2.keySet()) {
                HashMap hashMap4 = new HashMap();
                for (String str4 : (Set) zzawk2.get(str3)) {
                    hashMap4.put(str4, zzb.zza(str4, null, str3));
                }
                hashMap3.put(str3, hashMap4);
            }
            this.bba = new zzalu(hashMap3, this.bba.getTimestamp());
        }
        taskCompletionSource.setResult(null);
        zzcxe();
    }
}
