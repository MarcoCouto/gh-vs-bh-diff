package com.google.firebase.iid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import java.util.LinkedList;
import java.util.Queue;

public final class FirebaseInstanceIdInternalReceiver extends WakefulBroadcastReceiver {
    private static final Queue<Intent> bax = new LinkedList();
    private static final Queue<Intent> bay = new LinkedList();

    private static Intent zza(Context context, String str, Intent intent) {
        Intent intent2 = new Intent(context, FirebaseInstanceIdInternalReceiver.class);
        intent2.setAction(str);
        intent2.putExtra("wrapped_intent", intent);
        return intent2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d  */
    static int zzb(Context context, String str, Intent intent) {
        char c;
        Queue<Intent> queue;
        int hashCode = str.hashCode();
        if (hashCode != -842411455) {
            if (hashCode == 41532704 && str.equals("com.google.firebase.MESSAGING_EVENT")) {
                c = 1;
                switch (c) {
                    case 0:
                        queue = bax;
                        break;
                    case 1:
                        queue = bay;
                        break;
                    default:
                        String str2 = "FirebaseInstanceId";
                        String str3 = "Unknown service action: ";
                        String valueOf = String.valueOf(str);
                        Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                        return 500;
                }
                queue.offer(intent);
                Intent intent2 = new Intent(str);
                intent2.setPackage(context.getPackageName());
                return zzj(context, intent2);
            }
        } else if (str.equals("com.google.firebase.INSTANCE_ID_EVENT")) {
            c = 0;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
            }
            queue.offer(intent);
            Intent intent22 = new Intent(str);
            intent22.setPackage(context.getPackageName());
            return zzj(context, intent22);
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
        queue.offer(intent);
        Intent intent222 = new Intent(str);
        intent222.setPackage(context.getPackageName());
        return zzj(context, intent222);
    }

    public static Intent zzcww() {
        return (Intent) bax.poll();
    }

    public static Intent zzcwx() {
        return (Intent) bay.poll();
    }

    private static void zzg(Context context, Intent intent) {
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService == null || resolveService.serviceInfo == null) {
            Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
            return;
        }
        ServiceInfo serviceInfo = resolveService.serviceInfo;
        if (!context.getPackageName().equals(serviceInfo.packageName) || serviceInfo.name == null) {
            String valueOf = String.valueOf(serviceInfo.packageName);
            String valueOf2 = String.valueOf(serviceInfo.name);
            StringBuilder sb = new StringBuilder(94 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
            sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
            sb.append(valueOf);
            sb.append("/");
            sb.append(valueOf2);
            Log.e("FirebaseInstanceId", sb.toString());
            return;
        }
        String str = serviceInfo.name;
        if (str.startsWith(".")) {
            String valueOf3 = String.valueOf(context.getPackageName());
            String valueOf4 = String.valueOf(str);
            str = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String str2 = "FirebaseInstanceId";
            String str3 = "Restricting intent to a specific service: ";
            String valueOf5 = String.valueOf(str);
            Log.d(str2, valueOf5.length() != 0 ? str3.concat(valueOf5) : new String(str3));
        }
        intent.setClassName(context.getPackageName(), str);
    }

    public static Intent zzh(Context context, Intent intent) {
        return zza(context, "com.google.firebase.INSTANCE_ID_EVENT", intent);
    }

    public static Intent zzi(Context context, Intent intent) {
        return zza(context, "com.google.firebase.MESSAGING_EVENT", intent);
    }

    private static int zzj(Context context, Intent intent) {
        ComponentName componentName;
        zzg(context, intent);
        try {
            if (context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0) {
                componentName = startWakefulService(context, intent);
            } else {
                componentName = context.startService(intent);
                Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
            }
            if (componentName != null) {
                return -1;
            }
            Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
            return 404;
        } catch (SecurityException e) {
            Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e);
            return 401;
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Intent intent2 = (Intent) intent.getParcelableExtra("wrapped_intent");
            if (intent2 == null) {
                Log.w("FirebaseInstanceId", "Missing wrapped intent");
            } else {
                zzb(context, intent.getAction(), intent2);
            }
        }
    }
}
