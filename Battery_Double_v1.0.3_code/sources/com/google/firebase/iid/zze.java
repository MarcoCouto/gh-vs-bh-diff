package com.google.firebase.iid;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public class zze {
    private static final Object zzamr = new Object();
    private final zzg baH;

    zze(zzg zzg) {
        this.baH = zzg;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        return null;
     */
    @Nullable
    public String zzcxa() {
        synchronized (zzamr) {
            String string = this.baH.zzcxb().getString("topic_operaion_queue", null);
            if (string != null) {
                String[] split = string.split(",");
                if (split.length > 1 && !TextUtils.isEmpty(split[1])) {
                    String str = split[1];
                    return str;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzsv(String str) {
        synchronized (zzamr) {
            String string = this.baH.zzcxb().getString("topic_operaion_queue", "");
            String valueOf = String.valueOf(",");
            StringBuilder sb = new StringBuilder(0 + String.valueOf(string).length() + String.valueOf(valueOf).length() + String.valueOf(str).length());
            sb.append(string);
            sb.append(valueOf);
            sb.append(str);
            this.baH.zzcxb().edit().putString("topic_operaion_queue", sb.toString()).apply();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean zzsz(String str) {
        synchronized (zzamr) {
            String string = this.baH.zzcxb().getString("topic_operaion_queue", "");
            String valueOf = String.valueOf(",");
            String valueOf2 = String.valueOf(str);
            if (!string.startsWith(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf))) {
                return false;
            }
            String valueOf3 = String.valueOf(",");
            String valueOf4 = String.valueOf(str);
            this.baH.zzcxb().edit().putString("topic_operaion_queue", string.substring((valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3)).length())).apply();
            return true;
        }
    }
}
