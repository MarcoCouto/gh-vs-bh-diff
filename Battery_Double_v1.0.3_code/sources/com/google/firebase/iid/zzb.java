package com.google.firebase.iid;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import com.google.android.gms.iid.MessengerCompat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class zzb extends Service {
    private int YZ;
    private int Za = 0;
    MessengerCompat aaw = new MessengerCompat((Handler) new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            int zzc = MessengerCompat.zzc(message);
            zzf.zzdi(zzb.this);
            zzb.this.getPackageManager();
            if (zzc == zzf.aaI || zzc == zzf.aaH) {
                zzb.this.zzm((Intent) message.obj);
                return;
            }
            int i = zzf.aaH;
            int i2 = zzf.aaI;
            StringBuilder sb = new StringBuilder(77);
            sb.append("Message from unexpected caller ");
            sb.append(zzc);
            sb.append(" mine=");
            sb.append(i);
            sb.append(" appid=");
            sb.append(i2);
            Log.w("FirebaseInstanceId", sb.toString());
        }
    });
    @VisibleForTesting
    final ExecutorService axl = Executors.newSingleThreadExecutor();
    private final Object zzail = new Object();

    public final IBinder onBind(Intent intent) {
        if (intent == null || !"com.google.firebase.INSTANCE_ID_EVENT".equals(intent.getAction())) {
            return null;
        }
        return this.aaw.getBinder();
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.zzail) {
            this.YZ = i2;
            this.Za++;
        }
        Intent zzaa = zzaa(intent);
        if (zzaa == null) {
            zzble();
            return 2;
        }
        try {
            return zzab(zzaa);
        } finally {
            if (zzaa.getStringExtra("from") != null) {
                WakefulBroadcastReceiver.completeWakefulIntent(zzaa);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract Intent zzaa(Intent intent);

    /* access modifiers changed from: protected */
    public int zzab(final Intent intent) {
        this.axl.execute(new Runnable() {
            public void run() {
                zzb.this.zzm(intent);
                zzb.this.zzble();
            }
        });
        return 3;
    }

    /* access modifiers changed from: protected */
    public void zzble() {
        synchronized (this.zzail) {
            this.Za--;
            if (this.Za == 0) {
                zzsl(this.YZ);
            }
        }
    }

    public abstract void zzm(Intent intent);

    /* access modifiers changed from: 0000 */
    public boolean zzsl(int i) {
        return stopSelfResult(i);
    }
}
