package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import java.io.IOException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class FirebaseInstanceId {
    private static Map<String, FirebaseInstanceId> aap = new ArrayMap();
    private static zze bat;
    private final FirebaseApp bau;
    private final zzd bav;
    private final String baw = zzcws();

    private FirebaseInstanceId(FirebaseApp firebaseApp, zzd zzd) {
        this.bau = firebaseApp;
        this.bav = zzd;
        if (this.baw == null) {
            throw new IllegalStateException("IID failing to initialize, FirebaseApp is missing project ID");
        }
        FirebaseInstanceIdService.zza(this.bau.getApplicationContext(), this);
    }

    public static FirebaseInstanceId getInstance() {
        return getInstance(FirebaseApp.getInstance());
    }

    @Keep
    public static synchronized FirebaseInstanceId getInstance(@NonNull FirebaseApp firebaseApp) {
        FirebaseInstanceId firebaseInstanceId;
        synchronized (FirebaseInstanceId.class) {
            firebaseInstanceId = (FirebaseInstanceId) aap.get(firebaseApp.getOptions().getApplicationId());
            if (firebaseInstanceId == null) {
                zzd zzb = zzd.zzb(firebaseApp.getApplicationContext(), null);
                if (bat == null) {
                    bat = new zze(zzb.zzcwy());
                }
                FirebaseInstanceId firebaseInstanceId2 = new FirebaseInstanceId(firebaseApp, zzb);
                aap.put(firebaseApp.getOptions().getApplicationId(), firebaseInstanceId2);
                firebaseInstanceId = firebaseInstanceId2;
            }
        }
        return firebaseInstanceId;
    }

    static String zza(KeyPair keyPair) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(keyPair.getPublic().getEncoded());
            digest[0] = (byte) ((112 + (digest[0] & 15)) & 255);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException unused) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required alghorithms");
            return null;
        }
    }

    static void zza(Context context, zzg zzg) {
        zzg.zzbmd();
        Intent intent = new Intent();
        intent.putExtra("CMD", "RST");
        context.sendBroadcast(FirebaseInstanceIdInternalReceiver.zzh(context, intent));
    }

    static int zzdf(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(38 + String.valueOf(valueOf).length());
            sb.append("Never happens: can't find own package ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return 0;
        }
    }

    static String zzdg(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(38 + String.valueOf(valueOf).length());
            sb.append("Never happens: can't find own package ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    static void zzdh(Context context) {
        Intent intent = new Intent();
        intent.setPackage(context.getPackageName());
        intent.putExtra("CMD", "SYNC");
        context.sendBroadcast(FirebaseInstanceIdInternalReceiver.zzh(context, intent));
    }

    static String zzem(Context context) {
        return getInstance().bau.getOptions().getApplicationId();
    }

    static String zzu(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    public void deleteInstanceId() throws IOException {
        this.bav.zzb("*", "*", null);
        this.bav.zzblx();
    }

    @WorkerThread
    public void deleteToken(String str, String str2) throws IOException {
        this.bav.zzb(str, str2, null);
    }

    public long getCreationTime() {
        return this.bav.getCreationTime();
    }

    public String getId() {
        return zza(this.bav.zzblw());
    }

    @Nullable
    public String getToken() {
        String zzcwt = zzcwt();
        if (zzcwt == null) {
            FirebaseInstanceIdService.zzen(this.bau.getApplicationContext());
        }
        return zzcwt;
    }

    @WorkerThread
    public String getToken(String str, String str2) throws IOException {
        return this.bav.getToken(str, str2, null);
    }

    /* access modifiers changed from: 0000 */
    public String zzcws() {
        String gcmSenderId = this.bau.getOptions().getGcmSenderId();
        if (gcmSenderId != null) {
            return gcmSenderId;
        }
        String applicationId = this.bau.getOptions().getApplicationId();
        if (applicationId.startsWith("1:")) {
            String[] split = applicationId.split(":");
            if (split.length < 2) {
                return null;
            }
            applicationId = split[1];
            if (applicationId.isEmpty()) {
                return null;
            }
        }
        return applicationId;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String zzcwt() {
        return this.bav.zzcwy().zzi("", this.baw, "*");
    }

    /* access modifiers changed from: 0000 */
    public String zzcwu() throws IOException {
        return getToken(this.baw, "*");
    }

    /* access modifiers changed from: 0000 */
    public zze zzcwv() {
        return bat;
    }

    public void zzsv(String str) {
        bat.zzsv(str);
        FirebaseInstanceIdService.zzen(this.bau.getApplicationContext());
    }

    /* access modifiers changed from: 0000 */
    public void zzsw(String str) throws IOException {
        if (getToken() == null) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String str2 = "gcm.topic";
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString(str2, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        zzd zzd = this.bav;
        String token = getToken();
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        zzd.getToken(token, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* access modifiers changed from: 0000 */
    public void zzsx(String str) throws IOException {
        if (getToken() == null) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String str2 = "gcm.topic";
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString(str2, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        zzd zzd = this.bav;
        String token = getToken();
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        zzd.zzb(token, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }
}
