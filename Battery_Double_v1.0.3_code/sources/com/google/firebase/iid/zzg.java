package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.util.zzx;
import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class zzg {
    SharedPreferences aaT;
    Context zzagf;

    public zzg(Context context) {
        this(context, "com.google.android.gms.appid");
    }

    public zzg(Context context, String str) {
        this.zzagf = context;
        this.aaT = context.getSharedPreferences(str, 4);
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf("-no-backup");
        zzkf(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
    }

    private String zzh(String str, String str2, String str3) {
        String valueOf = String.valueOf("|T|");
        StringBuilder sb = new StringBuilder(1 + String.valueOf(str).length() + String.valueOf(valueOf).length() + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append(valueOf);
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    private void zzkf(String str) {
        File file = new File(zzx.getNoBackupFilesDir(this.zzagf), str);
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !isEmpty()) {
                    Log.i("InstanceID/Store", "App restored, clearing state");
                    FirebaseInstanceId.zza(this.zzagf, this);
                }
            } catch (IOException e) {
                if (Log.isLoggable("InstanceID/Store", 3)) {
                    String str2 = "InstanceID/Store";
                    String str3 = "Error creating file in no backup dir: ";
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized String get(String str) {
        return this.aaT.getString(str, null);
    }

    /* access modifiers changed from: 0000 */
    public synchronized String get(String str, String str2) {
        SharedPreferences sharedPreferences;
        StringBuilder sb;
        sharedPreferences = this.aaT;
        String valueOf = String.valueOf("|S|");
        sb = new StringBuilder(0 + String.valueOf(str).length() + String.valueOf(valueOf).length() + String.valueOf(str2).length());
        sb.append(str);
        sb.append(valueOf);
        sb.append(str2);
        return sharedPreferences.getString(sb.toString(), null);
    }

    public boolean isEmpty() {
        return this.aaT.getAll().isEmpty();
    }

    /* access modifiers changed from: 0000 */
    public synchronized void zza(Editor editor, String str, String str2, String str3) {
        String valueOf = String.valueOf("|S|");
        StringBuilder sb = new StringBuilder(0 + String.valueOf(str).length() + String.valueOf(valueOf).length() + String.valueOf(str2).length());
        sb.append(str);
        sb.append(valueOf);
        sb.append(str2);
        editor.putString(sb.toString(), str3);
    }

    public synchronized void zza(String str, String str2, String str3, String str4, String str5) {
        String zzh = zzh(str, str2, str3);
        Editor edit = this.aaT.edit();
        edit.putString(zzh, str4);
        edit.putString("appVersion", str5);
        edit.putString("lastToken", Long.toString(System.currentTimeMillis() / 1000));
        edit.commit();
    }

    public synchronized void zzbmd() {
        this.aaT.edit().clear().commit();
    }

    public SharedPreferences zzcxb() {
        return this.aaT;
    }

    /* access modifiers changed from: 0000 */
    public synchronized KeyPair zze(String str, long j) {
        KeyPair zzblv;
        zzblv = zza.zzblv();
        Editor edit = this.aaT.edit();
        zza(edit, str, "|P|", FirebaseInstanceId.zzu(zzblv.getPublic().getEncoded()));
        zza(edit, str, "|K|", FirebaseInstanceId.zzu(zzblv.getPrivate().getEncoded()));
        zza(edit, str, "cre", Long.toString(j));
        edit.commit();
        return zzblv;
    }

    public synchronized String zzi(String str, String str2, String str3) {
        return this.aaT.getString(zzh(str, str2, str3), null);
    }

    public synchronized void zzj(String str, String str2, String str3) {
        String zzh = zzh(str, str2, str3);
        Editor edit = this.aaT.edit();
        edit.remove(zzh);
        edit.commit();
    }

    public synchronized void zzkg(String str) {
        Editor edit = this.aaT.edit();
        for (String str2 : this.aaT.getAll().keySet()) {
            if (str2.startsWith(str)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }

    public KeyPair zzkh(String str) {
        return zzkk(str);
    }

    /* access modifiers changed from: 0000 */
    public void zzki(String str) {
        zzkg(String.valueOf(str).concat("|"));
    }

    public void zzkj(String str) {
        zzkg(String.valueOf(str).concat("|T|"));
    }

    /* access modifiers changed from: 0000 */
    public KeyPair zzkk(String str) {
        String str2 = get(str, "|P|");
        String str3 = get(str, "|K|");
        if (str2 == null || str3 == null) {
            return null;
        }
        try {
            byte[] decode = Base64.decode(str2, 8);
            byte[] decode2 = Base64.decode(str3, 8);
            KeyFactory instance = KeyFactory.getInstance("RSA");
            return new KeyPair(instance.generatePublic(new X509EncodedKeySpec(decode)), instance.generatePrivate(new PKCS8EncodedKeySpec(decode2)));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(19 + String.valueOf(valueOf).length());
            sb.append("Invalid key stored ");
            sb.append(valueOf);
            Log.w("InstanceID/Store", sb.toString());
            FirebaseInstanceId.zza(this.zzagf, this);
            return null;
        }
    }
}
