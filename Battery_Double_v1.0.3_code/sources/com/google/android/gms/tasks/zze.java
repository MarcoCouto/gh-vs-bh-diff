package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

class zze<TResult> implements zzf<TResult> {
    /* access modifiers changed from: private */
    public OnSuccessListener<? super TResult> aDp;
    private final Executor avv;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();

    public zze(@NonNull Executor executor, @NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        this.avv = executor;
        this.aDp = onSuccessListener;
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.aDp = null;
        }
    }

    public void onComplete(@NonNull final Task<TResult> task) {
        if (task.isSuccessful()) {
            synchronized (this.zzail) {
                if (this.aDp != null) {
                    this.avv.execute(new Runnable() {
                        public void run() {
                            synchronized (zze.this.zzail) {
                                if (zze.this.aDp != null) {
                                    zze.this.aDp.onSuccess(task.getResult());
                                }
                            }
                        }
                    });
                }
            }
        }
    }
}
