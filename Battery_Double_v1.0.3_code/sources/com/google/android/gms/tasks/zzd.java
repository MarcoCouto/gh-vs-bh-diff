package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

class zzd<TResult> implements zzf<TResult> {
    /* access modifiers changed from: private */
    public OnFailureListener aDn;
    private final Executor avv;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();

    public zzd(@NonNull Executor executor, @NonNull OnFailureListener onFailureListener) {
        this.avv = executor;
        this.aDn = onFailureListener;
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.aDn = null;
        }
    }

    public void onComplete(@NonNull final Task<TResult> task) {
        if (!task.isSuccessful()) {
            synchronized (this.zzail) {
                if (this.aDn != null) {
                    this.avv.execute(new Runnable() {
                        public void run() {
                            synchronized (zzd.this.zzail) {
                                if (zzd.this.aDn != null) {
                                    zzd.this.aDn.onFailure(task.getException());
                                }
                            }
                        }
                    });
                }
            }
        }
    }
}
