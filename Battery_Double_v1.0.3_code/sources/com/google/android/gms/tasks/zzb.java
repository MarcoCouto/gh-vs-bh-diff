package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

class zzb<TResult, TContinuationResult> implements OnFailureListener, OnSuccessListener<TContinuationResult>, zzf<TResult> {
    /* access modifiers changed from: private */
    public final Continuation<TResult, Task<TContinuationResult>> aDg;
    /* access modifiers changed from: private */
    public final zzh<TContinuationResult> aDh;
    private final Executor avv;

    public zzb(@NonNull Executor executor, @NonNull Continuation<TResult, Task<TContinuationResult>> continuation, @NonNull zzh<TContinuationResult> zzh) {
        this.avv = executor;
        this.aDg = continuation;
        this.aDh = zzh;
    }

    public void cancel() {
        throw new UnsupportedOperationException();
    }

    public void onComplete(@NonNull final Task<TResult> task) {
        this.avv.execute(new Runnable() {
            public void run() {
                zzh zzb;
                try {
                    Task task = (Task) zzb.this.aDg.then(task);
                    if (task == null) {
                        zzb.this.onFailure(new NullPointerException("Continuation returned null"));
                        return;
                    }
                    task.addOnSuccessListener(TaskExecutors.aDu, (OnSuccessListener<? super TResult>) zzb.this);
                    task.addOnFailureListener(TaskExecutors.aDu, (OnFailureListener) zzb.this);
                } catch (RuntimeExecutionException e) {
                    if (e.getCause() instanceof Exception) {
                        zzb = zzb.this.aDh;
                        r0 = (Exception) e.getCause();
                    } else {
                        zzb = zzb.this.aDh;
                        r0 = e;
                    }
                    zzb.setException(r0);
                } catch (Exception e2) {
                    zzb.this.aDh.setException(e2);
                }
            }
        });
    }

    public void onFailure(@NonNull Exception exc) {
        this.aDh.setException(exc);
    }

    public void onSuccess(TContinuationResult tcontinuationresult) {
        this.aDh.setResult(tcontinuationresult);
    }
}
