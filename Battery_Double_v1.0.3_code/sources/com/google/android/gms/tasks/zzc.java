package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

class zzc<TResult> implements zzf<TResult> {
    /* access modifiers changed from: private */
    public OnCompleteListener<TResult> aDl;
    private final Executor avv;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();

    public zzc(@NonNull Executor executor, @NonNull OnCompleteListener<TResult> onCompleteListener) {
        this.avv = executor;
        this.aDl = onCompleteListener;
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.aDl = null;
        }
    }

    public void onComplete(@NonNull final Task<TResult> task) {
        synchronized (this.zzail) {
            if (this.aDl != null) {
                this.avv.execute(new Runnable() {
                    public void run() {
                        synchronized (zzc.this.zzail) {
                            if (zzc.this.aDl != null) {
                                zzc.this.aDl.onComplete(task);
                            }
                        }
                    }
                });
            }
        }
    }
}
