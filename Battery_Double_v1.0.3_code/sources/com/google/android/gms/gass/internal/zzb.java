package com.google.android.gms.gass.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzd.zzc;
import com.google.android.gms.gass.internal.zze.zza;

public class zzb extends zzd<zze> {
    public zzb(Context context, Looper looper, com.google.android.gms.common.internal.zzd.zzb zzb, zzc zzc) {
        super(context, looper, 116, zzb, zzc, null);
    }

    public zze zzblb() throws DeadObjectException {
        return (zze) super.zzasa();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzgk */
    public zze zzbb(IBinder iBinder) {
        return zza.zzgl(iBinder);
    }

    /* access modifiers changed from: protected */
    public String zzqz() {
        return "com.google.android.gms.gass.START";
    }

    /* access modifiers changed from: protected */
    public String zzra() {
        return "com.google.android.gms.gass.internal.IGassService";
    }
}
