package com.google.android.gms.dynamic;

import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface zzc extends IInterface {

    public static abstract class zza extends Binder implements zzc {

        /* renamed from: com.google.android.gms.dynamic.zzc$zza$zza reason: collision with other inner class name */
        private static class C0043zza implements zzc {
            private IBinder zzahn;

            C0043zza(IBinder iBinder) {
                this.zzahn = iBinder;
            }

            public IBinder asBinder() {
                return this.zzahn;
            }

            public Bundle getArguments() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getId() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean getRetainInstance() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getTag() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getTargetRequestCode() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean getUserVisibleHint() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzd getView() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    return com.google.android.gms.dynamic.zzd.zza.zzfc(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isAdded() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isDetached() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isHidden() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isInLayout() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isRemoving() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isResumed() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isVisible() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    boolean z = false;
                    this.zzahn.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setHasOptionsMenu(boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    obtain.writeInt(z ? 1 : 0);
                    this.zzahn.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setMenuVisibility(boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    obtain.writeInt(z ? 1 : 0);
                    this.zzahn.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setRetainInstance(boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    obtain.writeInt(z ? 1 : 0);
                    this.zzahn.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setUserVisibleHint(boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    obtain.writeInt(z ? 1 : 0);
                    this.zzahn.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void startActivity(Intent intent) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.zzahn.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void startActivityForResult(Intent intent, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    this.zzahn.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zzab(zzd zzd) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    this.zzahn.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zzac(zzd zzd) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    this.zzahn.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzd zzbbu() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return com.google.android.gms.dynamic.zzd.zza.zzfc(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzc zzbbv() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return zza.zzfb(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzd zzbbw() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    return com.google.android.gms.dynamic.zzd.zza.zzfc(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzc zzbbx() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.dynamic.IFragmentWrapper");
                    this.zzahn.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return zza.zzfb(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public zza() {
            attachInterface(this, "com.google.android.gms.dynamic.IFragmentWrapper");
        }

        public static zzc zzfb(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IFragmentWrapper");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof zzc)) ? new C0043zza(iBinder) : (zzc) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* JADX WARNING: type inference failed for: r2v0 */
        /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v2, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v3, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v4, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v5, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v6, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v7, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v8, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v9, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v10, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v11, types: [android.content.Intent] */
        /* JADX WARNING: type inference failed for: r2v13, types: [android.content.Intent] */
        /* JADX WARNING: type inference failed for: r2v14, types: [android.content.Intent] */
        /* JADX WARNING: type inference failed for: r2v16, types: [android.content.Intent] */
        /* JADX WARNING: type inference failed for: r2v17 */
        /* JADX WARNING: type inference failed for: r2v18 */
        /* JADX WARNING: type inference failed for: r2v19 */
        /* JADX WARNING: type inference failed for: r2v20 */
        /* JADX WARNING: type inference failed for: r2v21 */
        /* JADX WARNING: type inference failed for: r2v22 */
        /* JADX WARNING: type inference failed for: r2v23 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], android.os.IBinder, android.content.Intent]
  uses: [android.os.IBinder, android.content.Intent]
  mth insns count: 207
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 8 */
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 1598968902) {
                boolean z = false;
                ? r2 = 0;
                switch (i) {
                    case 2:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzd zzbbu = zzbbu();
                        parcel2.writeNoException();
                        if (zzbbu != null) {
                            r2 = zzbbu.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 3:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        Bundle arguments = getArguments();
                        parcel2.writeNoException();
                        if (arguments != null) {
                            parcel2.writeInt(1);
                            arguments.writeToParcel(parcel2, 1);
                            return true;
                        }
                        parcel2.writeInt(0);
                        return true;
                    case 4:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        int id = getId();
                        parcel2.writeNoException();
                        parcel2.writeInt(id);
                        return true;
                    case 5:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzc zzbbv = zzbbv();
                        parcel2.writeNoException();
                        if (zzbbv != null) {
                            r2 = zzbbv.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 6:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzd zzbbw = zzbbw();
                        parcel2.writeNoException();
                        if (zzbbw != null) {
                            r2 = zzbbw.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 7:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean retainInstance = getRetainInstance();
                        parcel2.writeNoException();
                        parcel2.writeInt(retainInstance);
                        return true;
                    case 8:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        String tag = getTag();
                        parcel2.writeNoException();
                        parcel2.writeString(tag);
                        return true;
                    case 9:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzc zzbbx = zzbbx();
                        parcel2.writeNoException();
                        if (zzbbx != null) {
                            r2 = zzbbx.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 10:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        int targetRequestCode = getTargetRequestCode();
                        parcel2.writeNoException();
                        parcel2.writeInt(targetRequestCode);
                        return true;
                    case 11:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean userVisibleHint = getUserVisibleHint();
                        parcel2.writeNoException();
                        parcel2.writeInt(userVisibleHint);
                        return true;
                    case 12:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzd view = getView();
                        parcel2.writeNoException();
                        if (view != null) {
                            r2 = view.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 13:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isAdded = isAdded();
                        parcel2.writeNoException();
                        parcel2.writeInt(isAdded);
                        return true;
                    case 14:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isDetached = isDetached();
                        parcel2.writeNoException();
                        parcel2.writeInt(isDetached);
                        return true;
                    case 15:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isHidden = isHidden();
                        parcel2.writeNoException();
                        parcel2.writeInt(isHidden);
                        return true;
                    case 16:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isInLayout = isInLayout();
                        parcel2.writeNoException();
                        parcel2.writeInt(isInLayout);
                        return true;
                    case 17:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isRemoving = isRemoving();
                        parcel2.writeNoException();
                        parcel2.writeInt(isRemoving);
                        return true;
                    case 18:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isResumed = isResumed();
                        parcel2.writeNoException();
                        parcel2.writeInt(isResumed);
                        return true;
                    case 19:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        boolean isVisible = isVisible();
                        parcel2.writeNoException();
                        parcel2.writeInt(isVisible);
                        return true;
                    case 20:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzab(com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 21:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        setHasOptionsMenu(z);
                        parcel2.writeNoException();
                        return true;
                    case 22:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        setMenuVisibility(z);
                        parcel2.writeNoException();
                        return true;
                    case 23:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        setRetainInstance(z);
                        parcel2.writeNoException();
                        return true;
                    case 24:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        setUserVisibleHint(z);
                        parcel2.writeNoException();
                        return true;
                    case 25:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        if (parcel.readInt() != 0) {
                            r2 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                        }
                        startActivity(r2);
                        parcel2.writeNoException();
                        return true;
                    case 26:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        if (parcel.readInt() != 0) {
                            r2 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                        }
                        startActivityForResult(r2, parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 27:
                        parcel.enforceInterface("com.google.android.gms.dynamic.IFragmentWrapper");
                        zzac(com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("com.google.android.gms.dynamic.IFragmentWrapper");
                return true;
            }
        }
    }

    Bundle getArguments() throws RemoteException;

    int getId() throws RemoteException;

    boolean getRetainInstance() throws RemoteException;

    String getTag() throws RemoteException;

    int getTargetRequestCode() throws RemoteException;

    boolean getUserVisibleHint() throws RemoteException;

    zzd getView() throws RemoteException;

    boolean isAdded() throws RemoteException;

    boolean isDetached() throws RemoteException;

    boolean isHidden() throws RemoteException;

    boolean isInLayout() throws RemoteException;

    boolean isRemoving() throws RemoteException;

    boolean isResumed() throws RemoteException;

    boolean isVisible() throws RemoteException;

    void setHasOptionsMenu(boolean z) throws RemoteException;

    void setMenuVisibility(boolean z) throws RemoteException;

    void setRetainInstance(boolean z) throws RemoteException;

    void setUserVisibleHint(boolean z) throws RemoteException;

    void startActivity(Intent intent) throws RemoteException;

    void startActivityForResult(Intent intent, int i) throws RemoteException;

    void zzab(zzd zzd) throws RemoteException;

    void zzac(zzd zzd) throws RemoteException;

    zzd zzbbu() throws RemoteException;

    zzc zzbbv() throws RemoteException;

    zzd zzbbw() throws RemoteException;

    zzc zzbbx() throws RemoteException;
}
