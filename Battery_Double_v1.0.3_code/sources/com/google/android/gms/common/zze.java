package com.google.android.gms.common;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller.SessionInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.R;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.common.util.zzy;
import com.google.android.gms.internal.zzro;
import com.google.android.gms.internal.zzrp;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class zze {
    @Deprecated
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    @Deprecated
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE = zzann();
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";
    public static boolean rp = false;
    public static boolean rq = false;
    static boolean rr = false;
    private static String rs = null;
    private static int rt = 0;
    private static boolean ru = false;
    static final AtomicBoolean rv = new AtomicBoolean();
    private static final AtomicBoolean rw = new AtomicBoolean();

    zze() {
    }

    @Deprecated
    public static PendingIntent getErrorPendingIntent(int i, Context context, int i2) {
        return zzc.zzang().getErrorResolutionPendingIntent(context, i, i2);
    }

    @Deprecated
    public static String getErrorString(int i) {
        return ConnectionResult.getStatusString(i);
    }

    @Deprecated
    public static String getOpenSourceSoftwareLicenseInfo(Context context) {
        InputStream openInputStream;
        try {
            openInputStream = context.getContentResolver().openInputStream(new Builder().scheme("android.resource").authority("com.google.android.gms").appendPath("raw").appendPath("oss_notice").build());
            String next = new Scanner(openInputStream).useDelimiter("\\A").next();
            if (openInputStream != null) {
                openInputStream.close();
            }
            return next;
        } catch (NoSuchElementException unused) {
            if (openInputStream != null) {
                openInputStream.close();
            }
            return null;
        } catch (Exception unused2) {
            return null;
        } catch (Throwable th) {
            if (openInputStream != null) {
                openInputStream.close();
            }
            throw th;
        }
    }

    public static Context getRemoteContext(Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    public static Resources getRemoteResource(Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.common.zzf.zza(android.content.pm.PackageInfo, com.google.android.gms.common.zzd$zza[]):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r7.zza(r5, r1) == null) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0071, code lost:
        if (r7.zza(r5, com.google.android.gms.common.zzd.C0038zzd.ro) == null) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0080, code lost:
        if (com.google.android.gms.common.util.zzl.zzha(r5.versionCode) >= com.google.android.gms.common.util.zzl.zzha(GOOGLE_PLAY_SERVICES_VERSION_CODE)) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        r0 = GOOGLE_PLAY_SERVICES_VERSION_CODE;
        r1 = r5.versionCode;
        r2 = new java.lang.StringBuilder(77);
        r2.append("Google Play services out of date.  Requires ");
        r2.append(r0);
        r2.append(" but found ");
        r2.append(r1);
        android.util.Log.w("GooglePlayServicesUtil", r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a7, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a8, code lost:
        r7 = r5.applicationInfo;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00aa, code lost:
        if (r7 != null) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r7 = r0.getApplicationInfo("com.google.android.gms", 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b3, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b4, code lost:
        android.util.Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bb, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00be, code lost:
        if (r7.enabled != false) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c1, code lost:
        return 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c2, code lost:
        return 0;
     */
    @Deprecated
    public static int isGooglePlayServicesAvailable(Context context) {
        String str;
        String str2;
        PackageManager packageManager = context.getPackageManager();
        try {
            context.getResources().getString(R.string.common_google_play_services_unknown_issue);
        } catch (Throwable unused) {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            zzbs(context);
        }
        boolean z = !zzi.zzck(context);
        PackageInfo packageInfo = null;
        if (z) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            } catch (NameNotFoundException unused2) {
                str = "GooglePlayServicesUtil";
                str2 = "Google Play Store is missing.";
            }
        }
        try {
            PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            zzf zzbz = zzf.zzbz(context);
            if (z) {
                zza zza = zzbz.zza(packageInfo, C0038zzd.ro);
                if (zza == null) {
                    str = "GooglePlayServicesUtil";
                    str2 = "Google Play Store signature invalid.";
                    Log.w(str, str2);
                    return 9;
                }
            }
            str = "GooglePlayServicesUtil";
            str2 = "Google Play services signature invalid.";
            Log.w(str, str2);
            return 9;
        } catch (NameNotFoundException unused3) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 1;
        }
    }

    @Deprecated
    public static boolean isUserRecoverableError(int i) {
        if (i != 9) {
            switch (i) {
                case 1:
                case 2:
                case 3:
                    break;
                default:
                    return false;
            }
        }
        return true;
    }

    private static int zzann() {
        return com.google.android.gms.common.internal.zze.xM;
    }

    @Deprecated
    public static boolean zzano() {
        return "user".equals(Build.TYPE);
    }

    @TargetApi(19)
    @Deprecated
    public static boolean zzb(Context context, int i, String str) {
        return zzy.zzb(context, i, str);
    }

    @Deprecated
    public static void zzbb(Context context) throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException {
        int isGooglePlayServicesAvailable = zzc.zzang().isGooglePlayServicesAvailable(context);
        if (isGooglePlayServicesAvailable != 0) {
            Intent zza = zzc.zzang().zza(context, isGooglePlayServicesAvailable, "e");
            StringBuilder sb = new StringBuilder(57);
            sb.append("GooglePlayServices not available due to error ");
            sb.append(isGooglePlayServicesAvailable);
            Log.e("GooglePlayServicesUtil", sb.toString());
            if (zza == null) {
                throw new GooglePlayServicesNotAvailableException(isGooglePlayServicesAvailable);
            }
            throw new GooglePlayServicesRepairableException(isGooglePlayServicesAvailable, "Google Play Services not available", zza);
        }
    }

    @Deprecated
    public static int zzbn(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (NameNotFoundException unused) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }

    @Deprecated
    public static void zzbp(Context context) {
        if (!rv.getAndSet(true)) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    notificationManager.cancel(10436);
                }
            } catch (SecurityException unused) {
            }
        }
    }

    private static void zzbs(Context context) {
        if (!rw.get()) {
            zzbx(context);
            if (rt == 0) {
                throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            } else if (rt != GOOGLE_PLAY_SERVICES_VERSION_CODE) {
                int i = GOOGLE_PLAY_SERVICES_VERSION_CODE;
                int i2 = rt;
                String valueOf = String.valueOf("com.google.android.gms.version");
                StringBuilder sb = new StringBuilder(290 + String.valueOf(valueOf).length());
                sb.append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
                sb.append(i);
                sb.append(" but found ");
                sb.append(i2);
                sb.append(".  You must have the following declaration within the <application> element:     <meta-data android:name=\"");
                sb.append(valueOf);
                sb.append("\" android:value=\"@integer/google_play_services_version\" />");
                throw new IllegalStateException(sb.toString());
            }
        }
    }

    public static boolean zzbt(Context context) {
        zzbx(context);
        return rr;
    }

    public static boolean zzbu(Context context) {
        return zzbt(context) || !zzano();
    }

    public static String zzbv(Context context) {
        ApplicationInfo applicationInfo;
        String str = context.getApplicationInfo().name;
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        String packageName = context.getPackageName();
        PackageManager packageManager = context.getApplicationContext().getPackageManager();
        try {
            applicationInfo = zzrp.zzcq(context).getApplicationInfo(context.getPackageName(), 0);
        } catch (NameNotFoundException unused) {
            applicationInfo = null;
        }
        return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : packageName;
    }

    @TargetApi(18)
    public static boolean zzbw(Context context) {
        if (zzs.zzavt()) {
            Bundle applicationRestrictions = ((UserManager) context.getSystemService("user")).getApplicationRestrictions(context.getPackageName());
            if (applicationRestrictions != null && "true".equals(applicationRestrictions.getString("restricted_profile"))) {
                return true;
            }
        }
        return false;
    }

    private static void zzbx(Context context) {
        if (!ru) {
            zzby(context);
        }
    }

    private static void zzby(Context context) {
        try {
            rs = context.getPackageName();
            zzro zzcq = zzrp.zzcq(context);
            rt = zzz.zzcg(context);
            PackageInfo packageInfo = zzcq.getPackageInfo("com.google.android.gms", 64);
            if (packageInfo != null) {
                if (zzf.zzbz(context).zza(packageInfo, C0038zzd.ro[1]) != null) {
                    rr = true;
                    ru = true;
                }
            }
            rr = false;
        } catch (NameNotFoundException e) {
            Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", e);
        } catch (Throwable th) {
            ru = true;
            throw th;
        }
        ru = true;
    }

    @Deprecated
    public static boolean zzc(Context context, int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return zzl(context, "com.google.android.gms");
        }
        return false;
    }

    @Deprecated
    public static boolean zzd(Context context, int i) {
        if (i == 9) {
            return zzl(context, "com.android.vending");
        }
        return false;
    }

    @Deprecated
    public static boolean zze(Context context, int i) {
        return zzy.zze(context, i);
    }

    @Deprecated
    public static Intent zzfd(int i) {
        return zzc.zzang().zza(null, i, null);
    }

    static boolean zzfe(int i) {
        if (!(i == 18 || i == 42)) {
            switch (i) {
                case 1:
                case 2:
                case 3:
                    break;
                default:
                    return false;
            }
        }
        return true;
    }

    @TargetApi(21)
    static boolean zzl(Context context, String str) {
        boolean equals = str.equals("com.google.android.gms");
        boolean z = false;
        if (equals && zzd.zzabc()) {
            return false;
        }
        if (zzs.zzavx()) {
            for (SessionInfo appPackageName : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                if (str.equals(appPackageName.getAppPackageName())) {
                    return true;
                }
            }
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            if (applicationInfo.enabled && !zzbw(context)) {
                z = true;
            }
            return z;
        } catch (NameNotFoundException unused) {
        }
    }
}
