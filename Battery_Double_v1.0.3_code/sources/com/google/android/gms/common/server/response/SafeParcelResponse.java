package com.google.android.gms.common.server.response;

import android.os.Bundle;
import android.os.Parcel;
import android.util.SparseArray;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.server.response.FastJsonResponse.Field;
import com.google.android.gms.common.util.zzb;
import com.google.android.gms.common.util.zzc;
import com.google.android.gms.common.util.zzp;
import com.google.android.gms.common.util.zzq;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SafeParcelResponse extends FastSafeParcelableJsonResponse {
    public static final zze CREATOR = new zze();
    private final String mClassName;
    private final int mVersionCode;
    private final FieldMappingDictionary zN;
    private final Parcel zU;
    private final int zV = 2;
    private int zW;
    private int zX;

    SafeParcelResponse(int i, Parcel parcel, FieldMappingDictionary fieldMappingDictionary) {
        this.mVersionCode = i;
        this.zU = (Parcel) zzab.zzy(parcel);
        this.zN = fieldMappingDictionary;
        this.mClassName = this.zN == null ? null : this.zN.zzauj();
        this.zW = 2;
    }

    private void zza(StringBuilder sb, int i, Object obj) {
        String str;
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"");
                str = zzp.zzia(obj.toString());
                break;
            case 8:
                sb.append("\"");
                str = zzc.zzp((byte[]) obj);
                break;
            case 9:
                sb.append("\"");
                str = zzc.zzq((byte[]) obj);
                break;
            case 10:
                zzq.zza(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                StringBuilder sb2 = new StringBuilder(26);
                sb2.append("Unknown type = ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
        }
        sb.append(str);
        sb.append("\"");
    }

    private void zza(StringBuilder sb, Field<?, ?> field, Parcel parcel, int i) {
        Object obj;
        switch (field.zzatu()) {
            case 0:
                obj = Integer.valueOf(zza.zzg(parcel, i));
                break;
            case 1:
                obj = zza.zzk(parcel, i);
                break;
            case 2:
                obj = Long.valueOf(zza.zzi(parcel, i));
                break;
            case 3:
                obj = Float.valueOf(zza.zzl(parcel, i));
                break;
            case 4:
                obj = Double.valueOf(zza.zzn(parcel, i));
                break;
            case 5:
                obj = zza.zzp(parcel, i);
                break;
            case 6:
                obj = Boolean.valueOf(zza.zzc(parcel, i));
                break;
            case 7:
                obj = zza.zzq(parcel, i);
                break;
            case 8:
            case 9:
                obj = zza.zzt(parcel, i);
                break;
            case 10:
                obj = zzp(zza.zzs(parcel, i));
                break;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                int zzatu = field.zzatu();
                StringBuilder sb2 = new StringBuilder(36);
                sb2.append("Unknown field out type = ");
                sb2.append(zzatu);
                throw new IllegalArgumentException(sb2.toString());
        }
        zzb(sb, field, zza(field, obj));
    }

    private void zza(StringBuilder sb, String str, Field<?, ?> field, Parcel parcel, int i) {
        sb.append("\"");
        sb.append(str);
        sb.append("\":");
        if (field.zzaue()) {
            zza(sb, field, parcel, i);
        } else {
            zzb(sb, field, parcel, i);
        }
    }

    private void zza(StringBuilder sb, Map<String, Field<?, ?>> map, Parcel parcel) {
        SparseArray zzau = zzau(map);
        sb.append('{');
        int zzcm = zza.zzcm(parcel);
        boolean z = false;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            Entry entry = (Entry) zzau.get(zza.zzgm(zzcl));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                zza(sb, (String) entry.getKey(), (Field) entry.getValue(), parcel, zzcl);
                z = true;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb2 = new StringBuilder(37);
            sb2.append("Overread allowed size end=");
            sb2.append(zzcm);
            throw new C0028zza(sb2.toString(), parcel);
        }
        sb.append('}');
    }

    private static SparseArray<Entry<String, Field<?, ?>>> zzau(Map<String, Field<?, ?>> map) {
        SparseArray<Entry<String, Field<?, ?>>> sparseArray = new SparseArray<>();
        for (Entry entry : map.entrySet()) {
            sparseArray.put(((Field) entry.getValue()).zzaub(), entry);
        }
        return sparseArray;
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.common.util.zzb.zza(java.lang.StringBuilder, java.lang.Object[]):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0074, code lost:
        com.google.android.gms.common.util.zzb.zza(r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007f, code lost:
        r6 = "]";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x011e, code lost:
        r5.append(r6);
        r6 = "\"";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0150, code lost:
        r5.append(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0153, code lost:
        return;
     */
    private void zzb(StringBuilder sb, Field<?, ?> field, Parcel parcel, int i) {
        String str;
        Object obj;
        String str2;
        Object[] objArr;
        if (field.zzatz()) {
            sb.append("[");
            switch (field.zzatu()) {
                case 0:
                    zzb.zza(sb, zza.zzw(parcel, i));
                    break;
                case 1:
                    objArr = zza.zzy(parcel, i);
                    break;
                case 2:
                    zzb.zza(sb, zza.zzx(parcel, i));
                    break;
                case 3:
                    zzb.zza(sb, zza.zzz(parcel, i));
                    break;
                case 4:
                    zzb.zza(sb, zza.zzaa(parcel, i));
                    break;
                case 5:
                    objArr = zza.zzab(parcel, i);
                    break;
                case 6:
                    zzb.zza(sb, zza.zzv(parcel, i));
                    break;
                case 7:
                    zzb.zza(sb, zza.zzac(parcel, i));
                    break;
                case 8:
                case 9:
                case 10:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case 11:
                    Parcel[] zzag = zza.zzag(parcel, i);
                    int length = zzag.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            sb.append(",");
                        }
                        zzag[i2].setDataPosition(0);
                        zza(sb, field.zzaug(), zzag[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
        } else {
            switch (field.zzatu()) {
                case 0:
                    sb.append(zza.zzg(parcel, i));
                    return;
                case 1:
                    obj = zza.zzk(parcel, i);
                    break;
                case 2:
                    sb.append(zza.zzi(parcel, i));
                    return;
                case 3:
                    sb.append(zza.zzl(parcel, i));
                    return;
                case 4:
                    sb.append(zza.zzn(parcel, i));
                    return;
                case 5:
                    obj = zza.zzp(parcel, i);
                    break;
                case 6:
                    sb.append(zza.zzc(parcel, i));
                    return;
                case 7:
                    String zzq = zza.zzq(parcel, i);
                    sb.append("\"");
                    str2 = zzp.zzia(zzq);
                    break;
                case 8:
                    byte[] zzt = zza.zzt(parcel, i);
                    sb.append("\"");
                    str2 = zzc.zzp(zzt);
                    break;
                case 9:
                    byte[] zzt2 = zza.zzt(parcel, i);
                    sb.append("\"");
                    str2 = zzc.zzq(zzt2);
                    break;
                case 10:
                    Bundle zzs = zza.zzs(parcel, i);
                    Set<String> keySet = zzs.keySet();
                    keySet.size();
                    sb.append("{");
                    boolean z = true;
                    for (String str3 : keySet) {
                        if (!z) {
                            sb.append(",");
                        }
                        sb.append("\"");
                        sb.append(str3);
                        sb.append("\"");
                        sb.append(":");
                        sb.append("\"");
                        sb.append(zzp.zzia(zzs.getString(str3)));
                        sb.append("\"");
                        z = false;
                    }
                    str = "}";
                    break;
                case 11:
                    Parcel zzaf = zza.zzaf(parcel, i);
                    zzaf.setDataPosition(0);
                    zza(sb, field.zzaug(), zzaf);
                    return;
                default:
                    throw new IllegalStateException("Unknown field type out");
            }
        }
        sb.append(str);
    }

    private void zzb(StringBuilder sb, Field<?, ?> field, Object obj) {
        if (field.zzaty()) {
            zzb(sb, field, (ArrayList) obj);
        } else {
            zza(sb, field.zzatt(), obj);
        }
    }

    private void zzb(StringBuilder sb, Field<?, ?> field, ArrayList<?> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(",");
            }
            zza(sb, field.zzatt(), arrayList.get(i));
        }
        sb.append("]");
    }

    public static HashMap<String, String> zzp(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (String str : bundle.keySet()) {
            hashMap.put(str, bundle.getString(str));
        }
        return hashMap;
    }

    public int getVersionCode() {
        return this.mVersionCode;
    }

    public String toString() {
        zzab.zzb(this.zN, (Object) "Cannot convert to JSON on client side.");
        Parcel zzaul = zzaul();
        zzaul.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        zza(sb, this.zN.zzhw(this.mClassName), zzaul);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zze zze = CREATOR;
        zze.zza(this, parcel, i);
    }

    public Map<String, Field<?, ?>> zzatv() {
        if (this.zN == null) {
            return null;
        }
        return this.zN.zzhw(this.mClassName);
    }

    public Parcel zzaul() {
        switch (this.zW) {
            case 0:
                this.zX = com.google.android.gms.common.internal.safeparcel.zzb.zzcn(this.zU);
                break;
            case 1:
                break;
        }
        com.google.android.gms.common.internal.safeparcel.zzb.zzaj(this.zU, this.zX);
        this.zW = 2;
        return this.zU;
    }

    /* access modifiers changed from: 0000 */
    public FieldMappingDictionary zzaum() {
        switch (this.zV) {
            case 0:
                return null;
            case 1:
                return this.zN;
            case 2:
                return this.zN;
            default:
                int i = this.zV;
                StringBuilder sb = new StringBuilder(34);
                sb.append("Invalid creation type: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
        }
    }

    public Object zzhs(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public boolean zzht(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }
}
