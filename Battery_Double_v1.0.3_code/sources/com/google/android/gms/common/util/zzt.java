package com.google.android.gms.common.util;

import android.os.Binder;
import android.os.Process;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class zzt {
    private static String Bk;

    public static String zzavz() {
        return zzhc(Binder.getCallingPid());
    }

    public static String zzawa() {
        if (Bk == null) {
            Bk = zzhc(Process.myPid());
        }
        return Bk;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004d A[SYNTHETIC, Splitter:B:20:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0061 A[SYNTHETIC, Splitter:B:29:0x0061] */
    private static String zzhc(int i) {
        String str;
        BufferedReader bufferedReader;
        BufferedReader bufferedReader2 = null;
        try {
            StringBuilder sb = new StringBuilder(25);
            sb.append("/proc/");
            sb.append(i);
            sb.append("/cmdline");
            bufferedReader = new BufferedReader(new FileReader(sb.toString()));
            try {
                str = bufferedReader.readLine().trim();
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                        return str;
                    } catch (Exception e) {
                        Log.w("ProcessUtils", e.getMessage(), e);
                        return str;
                    }
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    Log.e("ProcessUtils", e.getMessage(), e);
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (Exception e3) {
                            Log.w("ProcessUtils", e3.getMessage(), e3);
                        }
                    }
                    str = null;
                    return str;
                } catch (Throwable th) {
                    th = th;
                    bufferedReader2 = bufferedReader;
                    if (bufferedReader2 != null) {
                    }
                    throw th;
                }
            }
        } catch (IOException e4) {
            e = e4;
            bufferedReader = null;
            Log.e("ProcessUtils", e.getMessage(), e);
            if (bufferedReader != null) {
            }
            str = null;
            return str;
        } catch (Throwable th2) {
            th = th2;
            if (bufferedReader2 != null) {
                try {
                    bufferedReader2.close();
                } catch (Exception e5) {
                    Log.w("ProcessUtils", e5.getMessage(), e5);
                }
            }
            throw th;
        }
        return str;
    }
}
