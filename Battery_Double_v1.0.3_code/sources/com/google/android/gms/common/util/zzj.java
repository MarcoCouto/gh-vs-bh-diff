package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.SystemClock;
import com.google.firebase.analytics.FirebaseAnalytics.Param;

public final class zzj {
    private static IntentFilter Bb = new IntentFilter("android.intent.action.BATTERY_CHANGED");
    private static long Bc = 0;
    private static float Bd = Float.NaN;

    @TargetApi(20)
    public static boolean zzb(PowerManager powerManager) {
        return zzs.zzavv() ? powerManager.isInteractive() : powerManager.isScreenOn();
    }

    @TargetApi(20)
    public static int zzcm(Context context) {
        if (context == null || context.getApplicationContext() == null) {
            return -1;
        }
        Intent registerReceiver = context.getApplicationContext().registerReceiver(null, Bb);
        int i = 0;
        if (((registerReceiver == null ? 0 : registerReceiver.getIntExtra("plugged", 0)) & 7) != 0) {
            i = 1;
        }
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return -1;
        }
        return ((zzb(powerManager) ? 1 : 0) << true) | i;
    }

    public static synchronized float zzcn(Context context) {
        synchronized (zzj.class) {
            if (SystemClock.elapsedRealtime() - Bc >= 60000 || Float.isNaN(Bd)) {
                Intent registerReceiver = context.getApplicationContext().registerReceiver(null, Bb);
                if (registerReceiver != null) {
                    Bd = ((float) registerReceiver.getIntExtra(Param.LEVEL, -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
                }
                Bc = SystemClock.elapsedRealtime();
                float f = Bd;
                return f;
            }
            float f2 = Bd;
            return f2;
        }
    }
}
