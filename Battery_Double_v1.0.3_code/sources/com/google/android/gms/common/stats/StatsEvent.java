package com.google.android.gms.common.stats;

import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class StatsEvent extends AbstractSafeParcelable implements ReflectedParcelable {
    public abstract int getEventType();

    public abstract long getTimeMillis();

    public String toString() {
        long timeMillis = getTimeMillis();
        String valueOf = String.valueOf("\t");
        int eventType = getEventType();
        String valueOf2 = String.valueOf("\t");
        long zzaut = zzaut();
        String valueOf3 = String.valueOf(zzauw());
        StringBuilder sb = new StringBuilder(51 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length());
        sb.append(timeMillis);
        sb.append(valueOf);
        sb.append(eventType);
        sb.append(valueOf2);
        sb.append(zzaut);
        sb.append(valueOf3);
        return sb.toString();
    }

    public abstract long zzaut();

    public abstract String zzauw();
}
