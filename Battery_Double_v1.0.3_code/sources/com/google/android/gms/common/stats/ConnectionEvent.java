package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class ConnectionEvent extends StatsEvent {
    public static final Creator<ConnectionEvent> CREATOR = new zza();
    private final String Aa;
    private final String Ab;
    private final String Ac;
    private final String Ad;
    private final String Ae;
    private final String Af;
    private final long Ag;
    private final long Ah;
    private long Ai;
    final int mVersionCode;
    private final long zY;
    private int zZ;

    ConnectionEvent(int i, long j, int i2, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3) {
        this.mVersionCode = i;
        this.zY = j;
        this.zZ = i2;
        this.Aa = str;
        this.Ab = str2;
        this.Ac = str3;
        this.Ad = str4;
        this.Ai = -1;
        this.Ae = str5;
        this.Af = str6;
        this.Ag = j2;
        this.Ah = j3;
    }

    public ConnectionEvent(long j, int i, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3) {
        this(1, j, i, str, str2, str3, str4, str5, str6, j2, j3);
    }

    public int getEventType() {
        return this.zZ;
    }

    public long getTimeMillis() {
        return this.zY;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zza.zza(this, parcel, i);
    }

    public String zzaun() {
        return this.Aa;
    }

    public String zzauo() {
        return this.Ab;
    }

    public String zzaup() {
        return this.Ac;
    }

    public String zzauq() {
        return this.Ad;
    }

    public String zzaur() {
        return this.Ae;
    }

    public String zzaus() {
        return this.Af;
    }

    public long zzaut() {
        return this.Ai;
    }

    public long zzauu() {
        return this.Ah;
    }

    public long zzauv() {
        return this.Ag;
    }

    public String zzauw() {
        String valueOf = String.valueOf("\t");
        String valueOf2 = String.valueOf(zzaun());
        String valueOf3 = String.valueOf(zzauo());
        String valueOf4 = String.valueOf("\t");
        String valueOf5 = String.valueOf(zzaup());
        String valueOf6 = String.valueOf(zzauq());
        String valueOf7 = String.valueOf("\t");
        String str = this.Ae == null ? "" : this.Ae;
        String valueOf8 = String.valueOf("\t");
        long zzauu = zzauu();
        StringBuilder sb = new StringBuilder(22 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length() + String.valueOf(valueOf5).length() + String.valueOf(valueOf6).length() + String.valueOf(valueOf7).length() + String.valueOf(str).length() + String.valueOf(valueOf8).length());
        sb.append(valueOf);
        sb.append(valueOf2);
        sb.append("/");
        sb.append(valueOf3);
        sb.append(valueOf4);
        sb.append(valueOf5);
        sb.append("/");
        sb.append(valueOf6);
        sb.append(valueOf7);
        sb.append(str);
        sb.append(valueOf8);
        sb.append(zzauu);
        return sb.toString();
    }
}
