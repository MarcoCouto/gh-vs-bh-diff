package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public abstract class DowngradeableSafeParcel extends AbstractSafeParcelable implements ReflectedParcelable {
    private static final Object yq = new Object();
    private static ClassLoader yr;
    private static Integer ys;
    private boolean yt = false;

    protected static ClassLoader zzass() {
        synchronized (yq) {
        }
        return null;
    }

    protected static Integer zzast() {
        synchronized (yq) {
        }
        return null;
    }

    private static boolean zzd(Class<?> cls) {
        try {
            return SafeParcelable.NULL.equals(cls.getField("NULL").get(null));
        } catch (IllegalAccessException | NoSuchFieldException unused) {
            return false;
        }
    }

    protected static boolean zzhk(String str) {
        ClassLoader zzass = zzass();
        if (zzass == null) {
            return true;
        }
        try {
            return zzd(zzass.loadClass(str));
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzasu() {
        return false;
    }
}
