package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.R;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.util.zzi;

public final class zzh {
    private static final SimpleArrayMap<String, String> yo = new SimpleArrayMap<>();

    public static String zzc(Context context, int i, String str) {
        Resources resources = context.getResources();
        if (i == 5) {
            return zze(context, "common_google_play_services_invalid_account_text", str);
        }
        if (i == 7) {
            return zze(context, "common_google_play_services_network_error_text", str);
        }
        if (i == 9) {
            return resources.getString(R.string.common_google_play_services_unsupported_text, new Object[]{str});
        } else if (i == 20) {
            return zze(context, "common_google_play_services_restricted_profile_text", str);
        } else {
            if (i == 42) {
                return resources.getString(R.string.common_google_play_services_wear_update_text);
            }
            switch (i) {
                case 1:
                    if (zzi.zzb(resources)) {
                        return resources.getString(R.string.common_google_play_services_install_text_tablet, new Object[]{str});
                    }
                    return resources.getString(R.string.common_google_play_services_install_text_phone, new Object[]{str});
                case 2:
                    return resources.getString(R.string.common_google_play_services_update_text, new Object[]{str});
                case 3:
                    return resources.getString(R.string.common_google_play_services_enable_text, new Object[]{str});
                default:
                    switch (i) {
                        case 16:
                            return zze(context, "common_google_play_services_api_unavailable_text", str);
                        case 17:
                            return zze(context, "common_google_play_services_sign_in_failed_text", str);
                        case 18:
                            return resources.getString(R.string.common_google_play_services_updating_text, new Object[]{str});
                        default:
                            return resources.getString(R.string.common_google_play_services_unknown_issue, new Object[]{str});
                    }
            }
        }
    }

    public static String zzd(Context context, int i, String str) {
        return i == 6 ? zze(context, "common_google_play_services_resolution_required_text", str) : zzc(context, i, str);
    }

    private static String zze(Context context, String str, String str2) {
        Resources resources = context.getResources();
        String zzn = zzn(context, str);
        if (zzn == null) {
            zzn = resources.getString(R.string.common_google_play_services_unknown_issue);
        }
        return String.format(resources.getConfiguration().locale, zzn, new Object[]{str2});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0028, code lost:
        android.util.Log.e(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        return null;
     */
    @Nullable
    public static String zzf(Context context, int i) {
        String str;
        String str2;
        Resources resources = context.getResources();
        if (i != 20) {
            if (i != 42) {
                switch (i) {
                    case 1:
                        return resources.getString(R.string.common_google_play_services_install_title);
                    case 2:
                        break;
                    case 3:
                        return resources.getString(R.string.common_google_play_services_enable_title);
                    case 4:
                    case 6:
                        return null;
                    case 5:
                        Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                        return zzn(context, "common_google_play_services_invalid_account_title");
                    case 7:
                        Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                        return zzn(context, "common_google_play_services_network_error_title");
                    case 8:
                        str = "GoogleApiAvailability";
                        str2 = "Internal error occurred. Please see logs for detailed information";
                        break;
                    case 9:
                        Log.e("GoogleApiAvailability", "Google Play services is invalid. Cannot recover.");
                        return resources.getString(R.string.common_google_play_services_unsupported_title);
                    case 10:
                        str = "GoogleApiAvailability";
                        str2 = "Developer error occurred. Please see logs for detailed information";
                        break;
                    case 11:
                        str = "GoogleApiAvailability";
                        str2 = "The application is not licensed to the user.";
                        break;
                    default:
                        switch (i) {
                            case 16:
                                str = "GoogleApiAvailability";
                                str2 = "One of the API components you attempted to connect to is not available.";
                                break;
                            case 17:
                                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                                return zzn(context, "common_google_play_services_sign_in_failed_title");
                            case 18:
                                return resources.getString(R.string.common_google_play_services_updating_title);
                            default:
                                str = "GoogleApiAvailability";
                                StringBuilder sb = new StringBuilder(33);
                                sb.append("Unexpected error code ");
                                sb.append(i);
                                str2 = sb.toString();
                                break;
                        }
                }
            }
            return resources.getString(R.string.common_google_play_services_update_title);
        }
        Log.e("GoogleApiAvailability", "The current user profile is restricted and could not use authenticated features.");
        return zzn(context, "common_google_play_services_restricted_profile_title");
    }

    @Nullable
    public static String zzg(Context context, int i) {
        return i == 6 ? zzn(context, "common_google_play_services_resolution_required_title") : zzf(context, i);
    }

    public static String zzh(Context context, int i) {
        int i2;
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                i2 = R.string.common_google_play_services_install_button;
                break;
            case 2:
                i2 = R.string.common_google_play_services_update_button;
                break;
            case 3:
                i2 = R.string.common_google_play_services_enable_button;
                break;
            default:
                i2 = 17039370;
                break;
        }
        return resources.getString(i2);
    }

    @Nullable
    private static String zzn(Context context, String str) {
        synchronized (yo) {
            String str2 = (String) yo.get(str);
            if (str2 != null) {
                return str2;
            }
            Resources remoteResource = GooglePlayServicesUtil.getRemoteResource(context);
            if (remoteResource == null) {
                return null;
            }
            int identifier = remoteResource.getIdentifier(str, "string", "com.google.android.gms");
            if (identifier == 0) {
                String str3 = "GoogleApiAvailability";
                String str4 = "Missing resource: ";
                String valueOf = String.valueOf(str);
                Log.w(str3, valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
                return null;
            }
            String string = remoteResource.getString(identifier);
            if (TextUtils.isEmpty(string)) {
                String str5 = "GoogleApiAvailability";
                String str6 = "Got empty resource: ";
                String valueOf2 = String.valueOf(str);
                Log.w(str5, valueOf2.length() != 0 ? str6.concat(valueOf2) : new String(str6));
                return null;
            }
            yo.put(str, string);
            return string;
        }
    }
}
