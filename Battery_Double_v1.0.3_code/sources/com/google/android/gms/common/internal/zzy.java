package com.google.android.gms.common.internal;

import java.util.Iterator;

public class zzy {
    private final String separator;

    private zzy(String str) {
        this.separator = str;
    }

    public static zzy zzhq(String str) {
        return new zzy(str);
    }

    public final String zza(Iterable<?> iterable) {
        return zza(new StringBuilder(), iterable).toString();
    }

    public final StringBuilder zza(StringBuilder sb, Iterable<?> iterable) {
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            while (true) {
                sb.append(zzw(it.next()));
                if (!it.hasNext()) {
                    break;
                }
                sb.append(this.separator);
            }
        }
        return sb;
    }

    /* access modifiers changed from: 0000 */
    public CharSequence zzw(Object obj) {
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }
}
