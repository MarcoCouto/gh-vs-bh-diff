package com.google.android.gms.common.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.Api.zzg;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

public class zzah<T extends IInterface> extends zzk<T> {
    private final zzg<T> zn;

    public zzah(Context context, Looper looper, int i, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, zzg zzg, zzg<T> zzg2) {
        super(context, looper, i, zzg, connectionCallbacks, onConnectionFailedListener);
        this.zn = zzg2;
    }

    public zzg<T> zzatn() {
        return this.zn;
    }

    /* access modifiers changed from: protected */
    public T zzbb(IBinder iBinder) {
        return this.zn.zzbb(iBinder);
    }

    /* access modifiers changed from: protected */
    public void zzc(int i, T t) {
        this.zn.zza(i, t);
    }

    /* access modifiers changed from: protected */
    public String zzqz() {
        return this.zn.zzqz();
    }

    /* access modifiers changed from: protected */
    public String zzra() {
        return this.zn.zzra();
    }
}
