package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzl implements Callback {
    private final Handler mHandler;
    private final zza yE;
    private final ArrayList<ConnectionCallbacks> yF = new ArrayList<>();
    final ArrayList<ConnectionCallbacks> yG = new ArrayList<>();
    private final ArrayList<OnConnectionFailedListener> yH = new ArrayList<>();
    private volatile boolean yI = false;
    private final AtomicInteger yJ = new AtomicInteger(0);
    private boolean yK = false;
    private final Object zzail = new Object();

    public interface zza {
        boolean isConnected();

        Bundle zzamh();
    }

    public zzl(Looper looper, zza zza2) {
        this.yE = zza2;
        this.mHandler = new Handler(looper, this);
    }

    public boolean handleMessage(Message message) {
        if (message.what == 1) {
            ConnectionCallbacks connectionCallbacks = (ConnectionCallbacks) message.obj;
            synchronized (this.zzail) {
                if (this.yI && this.yE.isConnected() && this.yF.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnected(this.yE.zzamh());
                }
            }
            return true;
        }
        int i = message.what;
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }

    public boolean isConnectionCallbacksRegistered(ConnectionCallbacks connectionCallbacks) {
        boolean contains;
        zzab.zzy(connectionCallbacks);
        synchronized (this.zzail) {
            contains = this.yF.contains(connectionCallbacks);
        }
        return contains;
    }

    public boolean isConnectionFailedListenerRegistered(OnConnectionFailedListener onConnectionFailedListener) {
        boolean contains;
        zzab.zzy(onConnectionFailedListener);
        synchronized (this.zzail) {
            contains = this.yH.contains(onConnectionFailedListener);
        }
        return contains;
    }

    public void registerConnectionCallbacks(ConnectionCallbacks connectionCallbacks) {
        zzab.zzy(connectionCallbacks);
        synchronized (this.zzail) {
            if (this.yF.contains(connectionCallbacks)) {
                String valueOf = String.valueOf(connectionCallbacks);
                StringBuilder sb = new StringBuilder(62 + String.valueOf(valueOf).length());
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.yF.add(connectionCallbacks);
            }
        }
        if (this.yE.isConnected()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, connectionCallbacks));
        }
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener) {
        zzab.zzy(onConnectionFailedListener);
        synchronized (this.zzail) {
            if (this.yH.contains(onConnectionFailedListener)) {
                String valueOf = String.valueOf(onConnectionFailedListener);
                StringBuilder sb = new StringBuilder(67 + String.valueOf(valueOf).length());
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.yH.add(onConnectionFailedListener);
            }
        }
    }

    public void unregisterConnectionCallbacks(ConnectionCallbacks connectionCallbacks) {
        zzab.zzy(connectionCallbacks);
        synchronized (this.zzail) {
            if (!this.yF.remove(connectionCallbacks)) {
                String valueOf = String.valueOf(connectionCallbacks);
                StringBuilder sb = new StringBuilder(52 + String.valueOf(valueOf).length());
                sb.append("unregisterConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            } else if (this.yK) {
                this.yG.add(connectionCallbacks);
            }
        }
    }

    public void unregisterConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener) {
        zzab.zzy(onConnectionFailedListener);
        synchronized (this.zzail) {
            if (!this.yH.remove(onConnectionFailedListener)) {
                String valueOf = String.valueOf(onConnectionFailedListener);
                StringBuilder sb = new StringBuilder(57 + String.valueOf(valueOf).length());
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }

    public void zzasw() {
        this.yI = false;
        this.yJ.incrementAndGet();
    }

    public void zzasx() {
        this.yI = true;
    }

    public void zzgf(int i) {
        zzab.zza(Looper.myLooper() == this.mHandler.getLooper(), (Object) "onUnintentionalDisconnection must only be called on the Handler thread");
        this.mHandler.removeMessages(1);
        synchronized (this.zzail) {
            this.yK = true;
            ArrayList arrayList = new ArrayList(this.yF);
            int i2 = this.yJ.get();
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ConnectionCallbacks connectionCallbacks = (ConnectionCallbacks) it.next();
                if (!this.yI) {
                    break;
                } else if (this.yJ.get() != i2) {
                    break;
                } else if (this.yF.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnectionSuspended(i);
                }
            }
            this.yG.clear();
            this.yK = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        return;
     */
    public void zzm(ConnectionResult connectionResult) {
        zzab.zza(Looper.myLooper() == this.mHandler.getLooper(), (Object) "onConnectionFailure must only be called on the Handler thread");
        this.mHandler.removeMessages(1);
        synchronized (this.zzail) {
            ArrayList arrayList = new ArrayList(this.yH);
            int i = this.yJ.get();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                OnConnectionFailedListener onConnectionFailedListener = (OnConnectionFailedListener) it.next();
                if (this.yI) {
                    if (this.yJ.get() == i) {
                        if (this.yH.contains(onConnectionFailedListener)) {
                            onConnectionFailedListener.onConnectionFailed(connectionResult);
                        }
                    }
                }
            }
        }
    }

    public void zzo(Bundle bundle) {
        boolean z = true;
        zzab.zza(Looper.myLooper() == this.mHandler.getLooper(), (Object) "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.zzail) {
            zzab.zzbn(!this.yK);
            this.mHandler.removeMessages(1);
            this.yK = true;
            if (this.yG.size() != 0) {
                z = false;
            }
            zzab.zzbn(z);
            ArrayList arrayList = new ArrayList(this.yF);
            int i = this.yJ.get();
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ConnectionCallbacks connectionCallbacks = (ConnectionCallbacks) it.next();
                if (!this.yI || !this.yE.isConnected()) {
                    break;
                } else if (this.yJ.get() != i) {
                    break;
                } else if (!this.yG.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnected(bundle);
                }
            }
            this.yG.clear();
            this.yK = false;
        }
    }
}
