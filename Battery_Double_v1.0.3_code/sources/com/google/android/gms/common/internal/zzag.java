package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import com.google.android.gms.R;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;

public final class zzag extends Button {
    public zzag(Context context) {
        this(context, null);
    }

    public zzag(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842824);
    }

    private void zza(Resources resources) {
        setTypeface(Typeface.DEFAULT_BOLD);
        setTextSize(14.0f);
        int i = (int) ((resources.getDisplayMetrics().density * 48.0f) + 0.5f);
        setMinHeight(i);
        setMinWidth(i);
    }

    private void zza(Resources resources, int i, int i2, boolean z) {
        int zzg;
        int i3;
        int i4;
        int i5;
        if (z) {
            zzg = zzg(i2, R.drawable.common_plus_signin_btn_icon_dark, R.drawable.common_plus_signin_btn_icon_light, R.drawable.common_plus_signin_btn_icon_dark);
            i3 = R.drawable.common_plus_signin_btn_text_dark;
            i4 = R.drawable.common_plus_signin_btn_text_light;
            i5 = R.drawable.common_plus_signin_btn_text_dark;
        } else {
            zzg = zzg(i2, R.drawable.common_google_signin_btn_icon_dark, R.drawable.common_google_signin_btn_icon_light, R.drawable.common_google_signin_btn_icon_light);
            i3 = R.drawable.common_google_signin_btn_text_dark;
            i4 = R.drawable.common_google_signin_btn_text_light;
            i5 = R.drawable.common_google_signin_btn_text_light;
        }
        setBackgroundDrawable(resources.getDrawable(zzd(i, zzg, zzg(i2, i3, i4, i5))));
    }

    private boolean zza(Scope[] scopeArr) {
        if (scopeArr == null) {
            return false;
        }
        for (Scope zzaok : scopeArr) {
            String zzaok2 = zzaok.zzaok();
            if ((zzaok2.contains("/plus.") && !zzaok2.equals(Scopes.PLUS_ME)) || zzaok2.equals(Scopes.GAMES)) {
                return true;
            }
        }
        return false;
    }

    private void zzb(Resources resources, int i, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        int i6;
        if (z) {
            i3 = R.color.common_plus_signin_btn_text_dark;
            i4 = R.color.common_plus_signin_btn_text_light;
            i5 = R.color.common_plus_signin_btn_text_dark;
        } else {
            i3 = R.color.common_google_signin_btn_text_dark;
            i4 = R.color.common_google_signin_btn_text_light;
            i5 = R.color.common_google_signin_btn_text_light;
        }
        setTextColor((ColorStateList) zzab.zzy(resources.getColorStateList(zzg(i2, i3, i4, i5))));
        switch (i) {
            case 0:
                i6 = R.string.common_signin_button_text;
                break;
            case 1:
                i6 = R.string.common_signin_button_text_long;
                break;
            case 2:
                setText(null);
                break;
            default:
                StringBuilder sb = new StringBuilder(32);
                sb.append("Unknown button size: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
        }
        setText(resources.getString(i6));
        setTransformationMethod(null);
    }

    private int zzd(int i, int i2, int i3) {
        switch (i) {
            case 0:
            case 1:
                return i3;
            case 2:
                return i2;
            default:
                StringBuilder sb = new StringBuilder(32);
                sb.append("Unknown button size: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
        }
    }

    private int zzg(int i, int i2, int i3, int i4) {
        switch (i) {
            case 0:
                return i2;
            case 1:
                return i3;
            case 2:
                return i4;
            default:
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unknown color scheme: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
        }
    }

    public void zza(Resources resources, int i, int i2, Scope[] scopeArr) {
        boolean zza = zza(scopeArr);
        zza(resources);
        zza(resources, i, i2, zza);
        zzb(resources, i, i2, zza);
    }
}
