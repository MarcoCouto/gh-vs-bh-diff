package com.google.android.gms.common.internal;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

final class zzn extends zzm implements Callback {
    private final Handler mHandler;
    /* access modifiers changed from: private */
    public final HashMap<zza, zzb> yN = new HashMap<>();
    /* access modifiers changed from: private */
    public final com.google.android.gms.common.stats.zzb yO;
    private final long yP;
    /* access modifiers changed from: private */
    public final Context zzaql;

    private static final class zza {
        private final String yQ;
        private final ComponentName yR;
        private final String zzcvc;

        public zza(ComponentName componentName) {
            this.zzcvc = null;
            this.yQ = null;
            this.yR = (ComponentName) zzab.zzy(componentName);
        }

        public zza(String str, String str2) {
            this.zzcvc = zzab.zzhr(str);
            this.yQ = zzab.zzhr(str2);
            this.yR = null;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            return zzaa.equal(this.zzcvc, zza.zzcvc) && zzaa.equal(this.yR, zza.yR);
        }

        public int hashCode() {
            return zzaa.hashCode(this.zzcvc, this.yR);
        }

        public String toString() {
            return this.zzcvc == null ? this.yR.flattenToString() : this.zzcvc;
        }

        public Intent zzasy() {
            return this.zzcvc != null ? new Intent(this.zzcvc).setPackage(this.yQ) : new Intent().setComponent(this.yR);
        }
    }

    private final class zzb {
        /* access modifiers changed from: private */
        public int mState = 2;
        /* access modifiers changed from: private */
        public IBinder xL;
        /* access modifiers changed from: private */
        public ComponentName yR;
        private final zza yS = new zza();
        /* access modifiers changed from: private */
        public final Set<ServiceConnection> yT = new HashSet();
        private boolean yU;
        /* access modifiers changed from: private */
        public final zza yV;

        public class zza implements ServiceConnection {
            public zza() {
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                synchronized (zzn.this.yN) {
                    zzb.this.xL = iBinder;
                    zzb.this.yR = componentName;
                    for (ServiceConnection onServiceConnected : zzb.this.yT) {
                        onServiceConnected.onServiceConnected(componentName, iBinder);
                    }
                    zzb.this.mState = 1;
                }
            }

            public void onServiceDisconnected(ComponentName componentName) {
                synchronized (zzn.this.yN) {
                    zzb.this.xL = null;
                    zzb.this.yR = componentName;
                    for (ServiceConnection onServiceDisconnected : zzb.this.yT) {
                        onServiceDisconnected.onServiceDisconnected(componentName);
                    }
                    zzb.this.mState = 2;
                }
            }
        }

        public zzb(zza zza2) {
            this.yV = zza2;
        }

        public IBinder getBinder() {
            return this.xL;
        }

        public ComponentName getComponentName() {
            return this.yR;
        }

        public int getState() {
            return this.mState;
        }

        public boolean isBound() {
            return this.yU;
        }

        public void zza(ServiceConnection serviceConnection, String str) {
            zzn.this.yO.zza(zzn.this.zzaql, serviceConnection, str, this.yV.zzasy());
            this.yT.add(serviceConnection);
        }

        public boolean zza(ServiceConnection serviceConnection) {
            return this.yT.contains(serviceConnection);
        }

        public boolean zzasz() {
            return this.yT.isEmpty();
        }

        public void zzb(ServiceConnection serviceConnection, String str) {
            zzn.this.yO.zzb(zzn.this.zzaql, serviceConnection);
            this.yT.remove(serviceConnection);
        }

        @TargetApi(14)
        public void zzhm(String str) {
            this.mState = 3;
            this.yU = zzn.this.yO.zza(zzn.this.zzaql, str, this.yV.zzasy(), (ServiceConnection) this.yS, 129);
            if (!this.yU) {
                this.mState = 2;
                try {
                    zzn.this.yO.zza(zzn.this.zzaql, (ServiceConnection) this.yS);
                } catch (IllegalArgumentException unused) {
                }
            }
        }

        public void zzhn(String str) {
            zzn.this.yO.zza(zzn.this.zzaql, (ServiceConnection) this.yS);
            this.yU = false;
            this.mState = 2;
        }
    }

    zzn(Context context) {
        this.zzaql = context.getApplicationContext();
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.yO = com.google.android.gms.common.stats.zzb.zzaux();
        this.yP = 5000;
    }

    private boolean zza(zza zza2, ServiceConnection serviceConnection, String str) {
        boolean isBound;
        zzab.zzb(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.yN) {
            zzb zzb2 = (zzb) this.yN.get(zza2);
            if (zzb2 != null) {
                this.mHandler.removeMessages(0, zzb2);
                if (!zzb2.zza(serviceConnection)) {
                    zzb2.zza(serviceConnection, str);
                    switch (zzb2.getState()) {
                        case 1:
                            serviceConnection.onServiceConnected(zzb2.getComponentName(), zzb2.getBinder());
                            break;
                        case 2:
                            zzb2.zzhm(str);
                            break;
                    }
                } else {
                    String valueOf = String.valueOf(zza2);
                    StringBuilder sb = new StringBuilder(81 + String.valueOf(valueOf).length());
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            } else {
                zzb2 = new zzb(zza2);
                zzb2.zza(serviceConnection, str);
                zzb2.zzhm(str);
                this.yN.put(zza2, zzb2);
            }
            isBound = zzb2.isBound();
        }
        return isBound;
    }

    private void zzb(zza zza2, ServiceConnection serviceConnection, String str) {
        zzab.zzb(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.yN) {
            zzb zzb2 = (zzb) this.yN.get(zza2);
            if (zzb2 == null) {
                String valueOf = String.valueOf(zza2);
                StringBuilder sb = new StringBuilder(50 + String.valueOf(valueOf).length());
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (!zzb2.zza(serviceConnection)) {
                String valueOf2 = String.valueOf(zza2);
                StringBuilder sb2 = new StringBuilder(76 + String.valueOf(valueOf2).length());
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            } else {
                zzb2.zzb(serviceConnection, str);
                if (zzb2.zzasz()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, zzb2), this.yP);
                }
            }
        }
    }

    public boolean handleMessage(Message message) {
        if (message.what != 0) {
            return false;
        }
        zzb zzb2 = (zzb) message.obj;
        synchronized (this.yN) {
            if (zzb2.zzasz()) {
                if (zzb2.isBound()) {
                    zzb2.zzhn("GmsClientSupervisor");
                }
                this.yN.remove(zzb2.yV);
            }
        }
        return true;
    }

    public boolean zza(ComponentName componentName, ServiceConnection serviceConnection, String str) {
        return zza(new zza(componentName), serviceConnection, str);
    }

    public boolean zza(String str, String str2, ServiceConnection serviceConnection, String str3) {
        return zza(new zza(str, str2), serviceConnection, str3);
    }

    public void zzb(ComponentName componentName, ServiceConnection serviceConnection, String str) {
        zzb(new zza(componentName), serviceConnection, str);
    }

    public void zzb(String str, String str2, ServiceConnection serviceConnection, String str3) {
        zzb(new zza(str, str2), serviceConnection, str3);
    }
}
