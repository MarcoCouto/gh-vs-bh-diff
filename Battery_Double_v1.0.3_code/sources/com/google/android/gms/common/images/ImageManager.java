package com.google.android.gms.common.images;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzrc;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ImageManager {
    /* access modifiers changed from: private */
    public static final Object wr = new Object();
    /* access modifiers changed from: private */
    public static HashSet<Uri> ws = new HashSet<>();
    private static ImageManager wt;
    private static ImageManager wu;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final Map<Uri, Long> wA;
    /* access modifiers changed from: private */
    public final ExecutorService wv = Executors.newFixedThreadPool(4);
    /* access modifiers changed from: private */
    public final zzb ww;
    /* access modifiers changed from: private */
    public final zzrc wx;
    /* access modifiers changed from: private */
    public final Map<zza, ImageReceiver> wy;
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> wz;

    @KeepName
    private final class ImageReceiver extends ResultReceiver {
        private final Uri mUri;
        /* access modifiers changed from: private */
        public final ArrayList<zza> wB = new ArrayList<>();

        ImageReceiver(Uri uri) {
            super(new Handler(Looper.getMainLooper()));
            this.mUri = uri;
        }

        public void onReceiveResult(int i, Bundle bundle) {
            ImageManager.this.wv.execute(new zzc(this.mUri, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }

        public void zzarp() {
            Intent intent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
            intent.putExtra("com.google.android.gms.extras.uri", this.mUri);
            intent.putExtra("com.google.android.gms.extras.resultReceiver", this);
            intent.putExtra("com.google.android.gms.extras.priority", 3);
            ImageManager.this.mContext.sendBroadcast(intent);
        }

        public void zzb(zza zza) {
            com.google.android.gms.common.internal.zzb.zzhi("ImageReceiver.addImageRequest() must be called in the main thread");
            this.wB.add(zza);
        }

        public void zzc(zza zza) {
            com.google.android.gms.common.internal.zzb.zzhi("ImageReceiver.removeImageRequest() must be called in the main thread");
            this.wB.remove(zza);
        }
    }

    public interface OnImageLoadedListener {
        void onImageLoaded(Uri uri, Drawable drawable, boolean z);
    }

    @TargetApi(11)
    private static final class zza {
        static int zza(ActivityManager activityManager) {
            return activityManager.getLargeMemoryClass();
        }
    }

    private static final class zzb extends LruCache<C0027zza, Bitmap> {
        public zzb(Context context) {
            super(zzcc(context));
        }

        @TargetApi(11)
        private static int zzcc(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            return (int) (0.33f * ((float) (1048576 * ((!((context.getApplicationInfo().flags & 1048576) != 0) || !zzs.zzavn()) ? activityManager.getMemoryClass() : zza.zza(activityManager)))));
        }

        /* access modifiers changed from: protected */
        /* renamed from: zza */
        public int sizeOf(C0027zza zza, Bitmap bitmap) {
            return bitmap.getHeight() * bitmap.getRowBytes();
        }

        /* access modifiers changed from: protected */
        /* renamed from: zza */
        public void entryRemoved(boolean z, C0027zza zza, Bitmap bitmap, Bitmap bitmap2) {
            super.entryRemoved(z, zza, bitmap, bitmap2);
        }
    }

    private final class zzc implements Runnable {
        private final Uri mUri;
        private final ParcelFileDescriptor wD;

        public zzc(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.mUri = uri;
            this.wD = parcelFileDescriptor;
        }

        public void run() {
            com.google.android.gms.common.internal.zzb.zzhj("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            boolean z = false;
            Bitmap bitmap = null;
            if (this.wD != null) {
                try {
                    bitmap = BitmapFactory.decodeFileDescriptor(this.wD.getFileDescriptor());
                } catch (OutOfMemoryError e) {
                    String valueOf = String.valueOf(this.mUri);
                    StringBuilder sb = new StringBuilder(34 + String.valueOf(valueOf).length());
                    sb.append("OOM while loading bitmap for uri: ");
                    sb.append(valueOf);
                    Log.e("ImageManager", sb.toString(), e);
                    z = true;
                }
                try {
                    this.wD.close();
                } catch (IOException e2) {
                    Log.e("ImageManager", "closed failed", e2);
                }
            }
            boolean z2 = z;
            Bitmap bitmap2 = bitmap;
            CountDownLatch countDownLatch = new CountDownLatch(1);
            Handler zzg = ImageManager.this.mHandler;
            zzf zzf = new zzf(this.mUri, bitmap2, z2, countDownLatch);
            zzg.post(zzf);
            try {
                countDownLatch.await();
            } catch (InterruptedException unused) {
                String valueOf2 = String.valueOf(this.mUri);
                StringBuilder sb2 = new StringBuilder(32 + String.valueOf(valueOf2).length());
                sb2.append("Latch interrupted while posting ");
                sb2.append(valueOf2);
                Log.w("ImageManager", sb2.toString());
            }
        }
    }

    private final class zzd implements Runnable {
        private final zza wE;

        public zzd(zza zza) {
            this.wE = zza;
        }

        public void run() {
            com.google.android.gms.common.internal.zzb.zzhi("LoadImageRunnable must be executed on the main thread");
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.wy.get(this.wE);
            if (imageReceiver != null) {
                ImageManager.this.wy.remove(this.wE);
                imageReceiver.zzc(this.wE);
            }
            C0027zza zza = this.wE.wG;
            if (zza.uri == null) {
                this.wE.zza(ImageManager.this.mContext, ImageManager.this.wx, true);
                return;
            }
            Bitmap zza2 = ImageManager.this.zza(zza);
            if (zza2 != null) {
                this.wE.zza(ImageManager.this.mContext, zza2, true);
                return;
            }
            Long l = (Long) ImageManager.this.wA.get(zza.uri);
            if (l != null) {
                if (SystemClock.elapsedRealtime() - l.longValue() < 3600000) {
                    this.wE.zza(ImageManager.this.mContext, ImageManager.this.wx, true);
                    return;
                }
                ImageManager.this.wA.remove(zza.uri);
            }
            this.wE.zza(ImageManager.this.mContext, ImageManager.this.wx);
            ImageReceiver imageReceiver2 = (ImageReceiver) ImageManager.this.wz.get(zza.uri);
            if (imageReceiver2 == null) {
                imageReceiver2 = new ImageReceiver(zza.uri);
                ImageManager.this.wz.put(zza.uri, imageReceiver2);
            }
            imageReceiver2.zzb(this.wE);
            if (!(this.wE instanceof com.google.android.gms.common.images.zza.zzc)) {
                ImageManager.this.wy.put(this.wE, imageReceiver2);
            }
            synchronized (ImageManager.wr) {
                if (!ImageManager.ws.contains(zza.uri)) {
                    ImageManager.ws.add(zza.uri);
                    imageReceiver2.zzarp();
                }
            }
        }
    }

    @TargetApi(14)
    private static final class zze implements ComponentCallbacks2 {
        private final zzb ww;

        public zze(zzb zzb) {
            this.ww = zzb;
        }

        public void onConfigurationChanged(Configuration configuration) {
        }

        public void onLowMemory() {
            this.ww.evictAll();
        }

        public void onTrimMemory(int i) {
            if (i >= 60) {
                this.ww.evictAll();
                return;
            }
            if (i >= 20) {
                this.ww.trimToSize(this.ww.size() / 2);
            }
        }
    }

    private final class zzf implements Runnable {
        private final Bitmap mBitmap;
        private final Uri mUri;
        private boolean wF;
        private final CountDownLatch zzale;

        public zzf(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.mUri = uri;
            this.mBitmap = bitmap;
            this.wF = z;
            this.zzale = countDownLatch;
        }

        private void zza(ImageReceiver imageReceiver, boolean z) {
            ArrayList zza = imageReceiver.wB;
            int size = zza.size();
            for (int i = 0; i < size; i++) {
                zza zza2 = (zza) zza.get(i);
                if (z) {
                    zza2.zza(ImageManager.this.mContext, this.mBitmap, false);
                } else {
                    ImageManager.this.wA.put(this.mUri, Long.valueOf(SystemClock.elapsedRealtime()));
                    zza2.zza(ImageManager.this.mContext, ImageManager.this.wx, false);
                }
                if (!(zza2 instanceof com.google.android.gms.common.images.zza.zzc)) {
                    ImageManager.this.wy.remove(zza2);
                }
            }
        }

        public void run() {
            com.google.android.gms.common.internal.zzb.zzhi("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.mBitmap != null;
            if (ImageManager.this.ww != null) {
                if (this.wF) {
                    ImageManager.this.ww.evictAll();
                    System.gc();
                    this.wF = false;
                    ImageManager.this.mHandler.post(this);
                    return;
                } else if (z) {
                    ImageManager.this.ww.put(new C0027zza(this.mUri), this.mBitmap);
                }
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.wz.remove(this.mUri);
            if (imageReceiver != null) {
                zza(imageReceiver, z);
            }
            this.zzale.countDown();
            synchronized (ImageManager.wr) {
                ImageManager.ws.remove(this.mUri);
            }
        }
    }

    private ImageManager(Context context, boolean z) {
        this.mContext = context.getApplicationContext();
        if (z) {
            this.ww = new zzb(this.mContext);
            if (zzs.zzavq()) {
                zzarn();
            }
        } else {
            this.ww = null;
        }
        this.wx = new zzrc();
        this.wy = new HashMap();
        this.wz = new HashMap();
        this.wA = new HashMap();
    }

    public static ImageManager create(Context context) {
        return zzg(context, false);
    }

    /* access modifiers changed from: private */
    public Bitmap zza(C0027zza zza2) {
        if (this.ww == null) {
            return null;
        }
        return (Bitmap) this.ww.get(zza2);
    }

    @TargetApi(14)
    private void zzarn() {
        this.mContext.registerComponentCallbacks(new zze(this.ww));
    }

    public static ImageManager zzg(Context context, boolean z) {
        if (z) {
            if (wu == null) {
                wu = new ImageManager(context, true);
            }
            return wu;
        }
        if (wt == null) {
            wt = new ImageManager(context, false);
        }
        return wt;
    }

    public void loadImage(ImageView imageView, int i) {
        zza((zza) new com.google.android.gms.common.images.zza.zzb(imageView, i));
    }

    public void loadImage(ImageView imageView, Uri uri) {
        zza((zza) new com.google.android.gms.common.images.zza.zzb(imageView, uri));
    }

    public void loadImage(ImageView imageView, Uri uri, int i) {
        com.google.android.gms.common.images.zza.zzb zzb2 = new com.google.android.gms.common.images.zza.zzb(imageView, uri);
        zzb2.zzfy(i);
        zza((zza) zzb2);
    }

    public void loadImage(OnImageLoadedListener onImageLoadedListener, Uri uri) {
        zza((zza) new com.google.android.gms.common.images.zza.zzc(onImageLoadedListener, uri));
    }

    public void loadImage(OnImageLoadedListener onImageLoadedListener, Uri uri, int i) {
        com.google.android.gms.common.images.zza.zzc zzc2 = new com.google.android.gms.common.images.zza.zzc(onImageLoadedListener, uri);
        zzc2.zzfy(i);
        zza((zza) zzc2);
    }

    public void zza(zza zza2) {
        com.google.android.gms.common.internal.zzb.zzhi("ImageManager.loadImage() must be called in the main thread");
        new zzd(zza2).run();
    }
}
