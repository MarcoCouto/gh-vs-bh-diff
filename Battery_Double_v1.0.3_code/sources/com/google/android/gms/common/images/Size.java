package com.google.android.gms.common.images;

public final class Size {
    private final int zzaie;
    private final int zzaif;

    public Size(int i, int i2) {
        this.zzaie = i;
        this.zzaif = i2;
    }

    public static Size parseSize(String str) throws NumberFormatException {
        if (str == null) {
            throw new IllegalArgumentException("string must not be null");
        }
        int indexOf = str.indexOf(42);
        if (indexOf < 0) {
            indexOf = str.indexOf(120);
        }
        if (indexOf < 0) {
            throw zzhh(str);
        }
        try {
            return new Size(Integer.parseInt(str.substring(0, indexOf)), Integer.parseInt(str.substring(indexOf + 1)));
        } catch (NumberFormatException unused) {
            throw zzhh(str);
        }
    }

    private static NumberFormatException zzhh(String str) {
        StringBuilder sb = new StringBuilder(16 + String.valueOf(str).length());
        sb.append("Invalid Size: \"");
        sb.append(str);
        sb.append("\"");
        throw new NumberFormatException(sb.toString());
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof Size) {
            Size size = (Size) obj;
            if (this.zzaie == size.zzaie && this.zzaif == size.zzaif) {
                z = true;
            }
        }
        return z;
    }

    public int getHeight() {
        return this.zzaif;
    }

    public int getWidth() {
        return this.zzaie;
    }

    public int hashCode() {
        return this.zzaif ^ ((this.zzaie << 16) | (this.zzaie >>> 16));
    }

    public String toString() {
        int i = this.zzaie;
        int i2 = this.zzaif;
        StringBuilder sb = new StringBuilder(23);
        sb.append(i);
        sb.append("x");
        sb.append(i2);
        return sb.toString();
    }
}
