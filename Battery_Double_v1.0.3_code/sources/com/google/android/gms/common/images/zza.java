package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.google.android.gms.common.images.ImageManager.OnImageLoadedListener;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzra;
import com.google.android.gms.internal.zzrb;
import com.google.android.gms.internal.zzrc;
import java.lang.ref.WeakReference;

public abstract class zza {
    final C0027zza wG;
    protected int wH = 0;
    protected int wI = 0;
    protected boolean wJ = false;
    private boolean wK = true;
    private boolean wL = false;
    private boolean wM = true;

    /* renamed from: com.google.android.gms.common.images.zza$zza reason: collision with other inner class name */
    static final class C0027zza {
        public final Uri uri;

        public C0027zza(Uri uri2) {
            this.uri = uri2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0027zza)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            return zzaa.equal(((C0027zza) obj).uri, this.uri);
        }

        public int hashCode() {
            return zzaa.hashCode(this.uri);
        }
    }

    public static final class zzb extends zza {
        private WeakReference<ImageView> wN;

        public zzb(ImageView imageView, int i) {
            super(null, i);
            com.google.android.gms.common.internal.zzb.zzu(imageView);
            this.wN = new WeakReference<>(imageView);
        }

        public zzb(ImageView imageView, Uri uri) {
            super(uri, 0);
            com.google.android.gms.common.internal.zzb.zzu(imageView);
            this.wN = new WeakReference<>(imageView);
        }

        private void zza(ImageView imageView, Drawable drawable, boolean z, boolean z2, boolean z3) {
            int i = 0;
            boolean z4 = !z2 && !z3;
            if (z4 && (imageView instanceof zzrb)) {
                int zzars = ((zzrb) imageView).zzars();
                if (this.wI != 0 && zzars == this.wI) {
                    return;
                }
            }
            boolean zzc = zzc(z, z2);
            if (zzc) {
                drawable = zza(imageView.getDrawable(), drawable);
            }
            imageView.setImageDrawable(drawable);
            if (imageView instanceof zzrb) {
                zzrb zzrb = (zzrb) imageView;
                zzrb.zzp(z3 ? this.wG.uri : null);
                if (z4) {
                    i = this.wI;
                }
                zzrb.zzga(i);
            }
            if (zzc) {
                ((zzra) drawable).startTransition(250);
            }
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof zzb)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            ImageView imageView = (ImageView) this.wN.get();
            ImageView imageView2 = (ImageView) ((zzb) obj).wN.get();
            return (imageView2 == null || imageView == null || !zzaa.equal(imageView2, imageView)) ? false : true;
        }

        public int hashCode() {
            return 0;
        }

        /* access modifiers changed from: protected */
        public void zza(Drawable drawable, boolean z, boolean z2, boolean z3) {
            ImageView imageView = (ImageView) this.wN.get();
            if (imageView != null) {
                zza(imageView, drawable, z, z2, z3);
            }
        }
    }

    public static final class zzc extends zza {
        private WeakReference<OnImageLoadedListener> wO;

        public zzc(OnImageLoadedListener onImageLoadedListener, Uri uri) {
            super(uri, 0);
            com.google.android.gms.common.internal.zzb.zzu(onImageLoadedListener);
            this.wO = new WeakReference<>(onImageLoadedListener);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof zzc)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            zzc zzc = (zzc) obj;
            OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.wO.get();
            OnImageLoadedListener onImageLoadedListener2 = (OnImageLoadedListener) zzc.wO.get();
            return onImageLoadedListener2 != null && onImageLoadedListener != null && zzaa.equal(onImageLoadedListener2, onImageLoadedListener) && zzaa.equal(zzc.wG, this.wG);
        }

        public int hashCode() {
            return zzaa.hashCode(this.wG);
        }

        /* access modifiers changed from: protected */
        public void zza(Drawable drawable, boolean z, boolean z2, boolean z3) {
            if (!z2) {
                OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.wO.get();
                if (onImageLoadedListener != null) {
                    onImageLoadedListener.onImageLoaded(this.wG.uri, drawable, z3);
                }
            }
        }
    }

    public zza(Uri uri, int i) {
        this.wG = new C0027zza(uri);
        this.wI = i;
    }

    private Drawable zza(Context context, zzrc zzrc, int i) {
        return context.getResources().getDrawable(i);
    }

    /* access modifiers changed from: protected */
    public zzra zza(Drawable drawable, Drawable drawable2) {
        if (drawable == null) {
            drawable = null;
        } else if (drawable instanceof zzra) {
            drawable = ((zzra) drawable).zzarq();
        }
        return new zzra(drawable, drawable2);
    }

    /* access modifiers changed from: 0000 */
    public void zza(Context context, Bitmap bitmap, boolean z) {
        com.google.android.gms.common.internal.zzb.zzu(bitmap);
        zza(new BitmapDrawable(context.getResources(), bitmap), z, false, true);
    }

    /* access modifiers changed from: 0000 */
    public void zza(Context context, zzrc zzrc) {
        if (this.wM) {
            zza(null, false, true, false);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(Context context, zzrc zzrc, boolean z) {
        zza(this.wI != 0 ? zza(context, zzrc, this.wI) : null, z, false, false);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(Drawable drawable, boolean z, boolean z2, boolean z3);

    /* access modifiers changed from: protected */
    public boolean zzc(boolean z, boolean z2) {
        return this.wK && !z2 && !z;
    }

    public void zzfy(int i) {
        this.wI = i;
    }
}
