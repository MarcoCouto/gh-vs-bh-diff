package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze implements Creator<DataHolder> {
    static void zza(DataHolder dataHolder, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zza(parcel, 1, dataHolder.zzari(), false);
        zzb.zza(parcel, 2, (T[]) dataHolder.zzarj(), i, false);
        zzb.zzc(parcel, 3, dataHolder.getStatusCode());
        zzb.zza(parcel, 4, dataHolder.zzarc(), false);
        zzb.zzc(parcel, 1000, dataHolder.getVersionCode());
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzcc */
    public DataHolder createFromParcel(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        int i2 = 0;
        String[] strArr = null;
        CursorWindow[] cursorWindowArr = null;
        Bundle bundle = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            int zzgm = zza.zzgm(zzcl);
            if (zzgm != 1000) {
                switch (zzgm) {
                    case 1:
                        strArr = zza.zzac(parcel, zzcl);
                        break;
                    case 2:
                        cursorWindowArr = (CursorWindow[]) zza.zzb(parcel, zzcl, CursorWindow.CREATOR);
                        break;
                    case 3:
                        i2 = zza.zzg(parcel, zzcl);
                        break;
                    case 4:
                        bundle = zza.zzs(parcel, zzcl);
                        break;
                    default:
                        zza.zzb(parcel, zzcl);
                        break;
                }
            } else {
                i = zza.zzg(parcel, zzcl);
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel);
        }
        DataHolder dataHolder = new DataHolder(i, strArr, cursorWindowArr, i2, bundle);
        dataHolder.zzarh();
        return dataHolder;
    }

    /* renamed from: zzfv */
    public DataHolder[] newArray(int i) {
        return new DataHolder[i];
    }
}
