package com.google.android.gms.common.data;

import java.util.ArrayList;

public abstract class zzf<T> extends AbstractDataBuffer<T> {
    private boolean wo = false;
    private ArrayList<Integer> wp;

    protected zzf(DataHolder dataHolder) {
        super(dataHolder);
    }

    private void zzarl() {
        synchronized (this) {
            if (!this.wo) {
                int count = this.tu.getCount();
                this.wp = new ArrayList<>();
                if (count > 0) {
                    this.wp.add(Integer.valueOf(0));
                    String zzark = zzark();
                    String zzd = this.tu.zzd(zzark, 0, this.tu.zzfs(0));
                    for (int i = 1; i < count; i++) {
                        int zzfs = this.tu.zzfs(i);
                        String zzd2 = this.tu.zzd(zzark, i, zzfs);
                        if (zzd2 == null) {
                            StringBuilder sb = new StringBuilder(78 + String.valueOf(zzark).length());
                            sb.append("Missing value for markerColumn: ");
                            sb.append(zzark);
                            sb.append(", at row: ");
                            sb.append(i);
                            sb.append(", for window: ");
                            sb.append(zzfs);
                            throw new NullPointerException(sb.toString());
                        }
                        if (!zzd2.equals(zzd)) {
                            this.wp.add(Integer.valueOf(i));
                            zzd = zzd2;
                        }
                    }
                }
                this.wo = true;
            }
        }
    }

    public final T get(int i) {
        zzarl();
        return zzl(zzfw(i), zzfx(i));
    }

    public int getCount() {
        zzarl();
        return this.wp.size();
    }

    /* access modifiers changed from: protected */
    public abstract String zzark();

    /* access modifiers changed from: protected */
    public String zzarm() {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public int zzfw(int i) {
        if (i >= 0 && i < this.wp.size()) {
            return ((Integer) this.wp.get(i)).intValue();
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(i);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }

    /* access modifiers changed from: protected */
    public int zzfx(int i) {
        if (i < 0 || i == this.wp.size()) {
            return 0;
        }
        int count = (i == this.wp.size() - 1 ? this.tu.getCount() : ((Integer) this.wp.get(i + 1)).intValue()) - ((Integer) this.wp.get(i)).intValue();
        if (count == 1) {
            int zzfw = zzfw(i);
            int zzfs = this.tu.zzfs(zzfw);
            String zzarm = zzarm();
            if (zzarm == null || this.tu.zzd(zzarm, zzfw, zzfs) != null) {
                return count;
            }
            return 0;
        }
        return count;
    }

    /* access modifiers changed from: protected */
    public abstract T zzl(int i, int i2);
}
