package com.google.android.gms.common.data;

import android.content.ContentValues;
import android.database.CharArrayBuffer;
import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzab;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@KeepName
public final class DataHolder extends AbstractSafeParcelable implements Closeable {
    public static final Creator<DataHolder> CREATOR = new zze();
    private static final zza wi = new zza(new String[0], null) {
        public zza zza(ContentValues contentValues) {
            throw new UnsupportedOperationException("Cannot add data to empty builder");
        }

        public zza zza(HashMap<String, Object> hashMap) {
            throw new UnsupportedOperationException("Cannot add data to empty builder");
        }
    };
    boolean mClosed;
    private final int mVersionCode;
    private final int ok;
    private final String[] wb;
    Bundle wc;
    private final CursorWindow[] wd;
    private final Bundle we;
    int[] wf;
    int wg;
    private boolean wh;

    public static class zza {
        /* access modifiers changed from: private */
        public final String[] wb;
        /* access modifiers changed from: private */
        public final ArrayList<HashMap<String, Object>> wj;
        private final String wk;
        private final HashMap<Object, Integer> wl;
        private boolean wm;
        private String wn;

        private zza(String[] strArr, String str) {
            this.wb = (String[]) zzab.zzy(strArr);
            this.wj = new ArrayList<>();
            this.wk = str;
            this.wl = new HashMap<>();
            this.wm = false;
            this.wn = null;
        }

        private int zzb(HashMap<String, Object> hashMap) {
            if (this.wk == null) {
                return -1;
            }
            Object obj = hashMap.get(this.wk);
            if (obj == null) {
                return -1;
            }
            Integer num = (Integer) this.wl.get(obj);
            if (num != null) {
                return num.intValue();
            }
            this.wl.put(obj, Integer.valueOf(this.wj.size()));
            return -1;
        }

        public zza zza(ContentValues contentValues) {
            com.google.android.gms.common.internal.zzb.zzu(contentValues);
            HashMap hashMap = new HashMap(contentValues.size());
            for (Entry entry : contentValues.valueSet()) {
                hashMap.put((String) entry.getKey(), entry.getValue());
            }
            return zza(hashMap);
        }

        public zza zza(HashMap<String, Object> hashMap) {
            com.google.android.gms.common.internal.zzb.zzu(hashMap);
            int zzb = zzb(hashMap);
            if (zzb == -1) {
                this.wj.add(hashMap);
            } else {
                this.wj.remove(zzb);
                this.wj.add(zzb, hashMap);
            }
            this.wm = false;
            return this;
        }

        public DataHolder zzfu(int i) {
            return new DataHolder(this, i, (Bundle) null);
        }
    }

    public static class zzb extends RuntimeException {
        public zzb(String str) {
            super(str);
        }
    }

    DataHolder(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.mClosed = false;
        this.wh = true;
        this.mVersionCode = i;
        this.wb = strArr;
        this.wd = cursorWindowArr;
        this.ok = i2;
        this.we = bundle;
    }

    private DataHolder(zza zza2, int i, Bundle bundle) {
        this(zza2.wb, zza(zza2, -1), i, bundle);
    }

    public DataHolder(String[] strArr, CursorWindow[] cursorWindowArr, int i, Bundle bundle) {
        this.mClosed = false;
        this.wh = true;
        this.mVersionCode = 1;
        this.wb = (String[]) zzab.zzy(strArr);
        this.wd = (CursorWindow[]) zzab.zzy(cursorWindowArr);
        this.ok = i;
        this.we = bundle;
        zzarh();
    }

    public static DataHolder zza(int i, Bundle bundle) {
        return new DataHolder(wi, i, bundle);
    }

    private static CursorWindow[] zza(zza zza2, int i) {
        long j;
        if (zza2.wb.length == 0) {
            return new CursorWindow[0];
        }
        List zzb2 = (i < 0 || i >= zza2.wj.size()) ? zza2.wj : zza2.wj.subList(0, i);
        int size = zzb2.size();
        CursorWindow cursorWindow = new CursorWindow(false);
        ArrayList arrayList = new ArrayList();
        arrayList.add(cursorWindow);
        cursorWindow.setNumColumns(zza2.wb.length);
        boolean z = false;
        CursorWindow cursorWindow2 = cursorWindow;
        int i2 = 0;
        while (i2 < size) {
            try {
                if (!cursorWindow2.allocRow()) {
                    StringBuilder sb = new StringBuilder(72);
                    sb.append("Allocating additional cursor window for large data set (row ");
                    sb.append(i2);
                    sb.append(")");
                    Log.d("DataHolder", sb.toString());
                    cursorWindow2 = new CursorWindow(false);
                    cursorWindow2.setStartPosition(i2);
                    cursorWindow2.setNumColumns(zza2.wb.length);
                    arrayList.add(cursorWindow2);
                    if (!cursorWindow2.allocRow()) {
                        Log.e("DataHolder", "Unable to allocate row to hold data.");
                        arrayList.remove(cursorWindow2);
                        return (CursorWindow[]) arrayList.toArray(new CursorWindow[arrayList.size()]);
                    }
                }
                Map map = (Map) zzb2.get(i2);
                boolean z2 = true;
                for (int i3 = 0; i3 < zza2.wb.length && z2; i3++) {
                    String str = zza2.wb[i3];
                    Object obj = map.get(str);
                    if (obj == null) {
                        z2 = cursorWindow2.putNull(i2, i3);
                    } else if (obj instanceof String) {
                        z2 = cursorWindow2.putString((String) obj, i2, i3);
                    } else {
                        if (obj instanceof Long) {
                            j = ((Long) obj).longValue();
                        } else if (obj instanceof Integer) {
                            z2 = cursorWindow2.putLong((long) ((Integer) obj).intValue(), i2, i3);
                        } else if (obj instanceof Boolean) {
                            j = ((Boolean) obj).booleanValue() ? 1 : 0;
                        } else if (obj instanceof byte[]) {
                            z2 = cursorWindow2.putBlob((byte[]) obj, i2, i3);
                        } else if (obj instanceof Double) {
                            z2 = cursorWindow2.putDouble(((Double) obj).doubleValue(), i2, i3);
                        } else if (obj instanceof Float) {
                            z2 = cursorWindow2.putDouble((double) ((Float) obj).floatValue(), i2, i3);
                        } else {
                            String valueOf = String.valueOf(obj);
                            StringBuilder sb2 = new StringBuilder(32 + String.valueOf(str).length() + String.valueOf(valueOf).length());
                            sb2.append("Unsupported object for column ");
                            sb2.append(str);
                            sb2.append(": ");
                            sb2.append(valueOf);
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        z2 = cursorWindow2.putLong(j, i2, i3);
                    }
                }
                if (z2) {
                    z = false;
                } else if (z) {
                    throw new zzb("Could not add the value to a new CursorWindow. The size of value may be larger than what a CursorWindow can handle.");
                } else {
                    StringBuilder sb3 = new StringBuilder(74);
                    sb3.append("Couldn't populate window data for row ");
                    sb3.append(i2);
                    sb3.append(" - allocating new window.");
                    Log.d("DataHolder", sb3.toString());
                    cursorWindow2.freeLastRow();
                    cursorWindow2 = new CursorWindow(false);
                    cursorWindow2.setStartPosition(i2);
                    cursorWindow2.setNumColumns(zza2.wb.length);
                    arrayList.add(cursorWindow2);
                    i2--;
                    z = true;
                }
                i2++;
            } catch (RuntimeException e) {
                int size2 = arrayList.size();
                for (int i4 = 0; i4 < size2; i4++) {
                    ((CursorWindow) arrayList.get(i4)).close();
                }
                throw e;
            }
        }
        return (CursorWindow[]) arrayList.toArray(new CursorWindow[arrayList.size()]);
    }

    public static zza zzb(String[] strArr) {
        return new zza(strArr, null);
    }

    public static DataHolder zzft(int i) {
        return zza(i, (Bundle) null);
    }

    private void zzh(String str, int i) {
        if (this.wc == null || !this.wc.containsKey(str)) {
            String str2 = "No such column: ";
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        } else if (isClosed()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i < 0 || i >= this.wg) {
            throw new CursorIndexOutOfBoundsException(i, this.wg);
        }
    }

    public void close() {
        synchronized (this) {
            if (!this.mClosed) {
                this.mClosed = true;
                for (CursorWindow close : this.wd) {
                    close.close();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            if (this.wh && this.wd.length > 0 && !isClosed()) {
                close();
                String valueOf = String.valueOf(toString());
                StringBuilder sb = new StringBuilder(178 + String.valueOf(valueOf).length());
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(valueOf);
                sb.append(")");
                Log.e("DataBuffer", sb.toString());
            }
        } finally {
            super.finalize();
        }
    }

    public int getCount() {
        return this.wg;
    }

    public int getStatusCode() {
        return this.ok;
    }

    /* access modifiers changed from: 0000 */
    public int getVersionCode() {
        return this.mVersionCode;
    }

    public boolean isClosed() {
        boolean z;
        synchronized (this) {
            z = this.mClosed;
        }
        return z;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zze.zza(this, parcel, i);
    }

    public void zza(String str, int i, int i2, CharArrayBuffer charArrayBuffer) {
        zzh(str, i);
        this.wd[i2].copyStringToBuffer(i, this.wc.getInt(str), charArrayBuffer);
    }

    public Bundle zzarc() {
        return this.we;
    }

    public void zzarh() {
        this.wc = new Bundle();
        for (int i = 0; i < this.wb.length; i++) {
            this.wc.putInt(this.wb[i], i);
        }
        this.wf = new int[this.wd.length];
        int i2 = 0;
        for (int i3 = 0; i3 < this.wd.length; i3++) {
            this.wf[i3] = i2;
            i2 += this.wd[i3].getNumRows() - (i2 - this.wd[i3].getStartPosition());
        }
        this.wg = i2;
    }

    /* access modifiers changed from: 0000 */
    public String[] zzari() {
        return this.wb;
    }

    /* access modifiers changed from: 0000 */
    public CursorWindow[] zzarj() {
        return this.wd;
    }

    public long zzb(String str, int i, int i2) {
        zzh(str, i);
        return this.wd[i2].getLong(i, this.wc.getInt(str));
    }

    public int zzc(String str, int i, int i2) {
        zzh(str, i);
        return this.wd[i2].getInt(i, this.wc.getInt(str));
    }

    public String zzd(String str, int i, int i2) {
        zzh(str, i);
        return this.wd[i2].getString(i, this.wc.getInt(str));
    }

    public boolean zze(String str, int i, int i2) {
        zzh(str, i);
        return Long.valueOf(this.wd[i2].getLong(i, this.wc.getInt(str))).longValue() == 1;
    }

    public float zzf(String str, int i, int i2) {
        zzh(str, i);
        return this.wd[i2].getFloat(i, this.wc.getInt(str));
    }

    public int zzfs(int i) {
        int i2 = 0;
        zzab.zzbn(i >= 0 && i < this.wg);
        while (true) {
            if (i2 >= this.wf.length) {
                break;
            } else if (i < this.wf[i2]) {
                i2--;
                break;
            } else {
                i2++;
            }
        }
        return i2 == this.wf.length ? i2 - 1 : i2;
    }

    public byte[] zzg(String str, int i, int i2) {
        zzh(str, i);
        return this.wd[i2].getBlob(i, this.wc.getInt(str));
    }

    public Uri zzh(String str, int i, int i2) {
        String zzd = zzd(str, i, i2);
        if (zzd == null) {
            return null;
        }
        return Uri.parse(zzd);
    }

    public boolean zzhe(String str) {
        return this.wc.containsKey(str);
    }

    public boolean zzi(String str, int i, int i2) {
        zzh(str, i);
        return this.wd[i2].isNull(i, this.wc.getInt(str));
    }
}
