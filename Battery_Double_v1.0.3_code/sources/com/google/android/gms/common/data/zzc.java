package com.google.android.gms.common.data;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzab;

public abstract class zzc {
    protected final DataHolder tu;
    protected int vX;
    private int vY;

    public zzc(DataHolder dataHolder, int i) {
        this.tu = (DataHolder) zzab.zzy(dataHolder);
        zzfq(i);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof zzc)) {
            return false;
        }
        zzc zzc = (zzc) obj;
        return zzaa.equal(Integer.valueOf(zzc.vX), Integer.valueOf(this.vX)) && zzaa.equal(Integer.valueOf(zzc.vY), Integer.valueOf(this.vY)) && zzc.tu == this.tu;
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(String str) {
        return this.tu.zze(str, this.vX, this.vY);
    }

    /* access modifiers changed from: protected */
    public byte[] getByteArray(String str) {
        return this.tu.zzg(str, this.vX, this.vY);
    }

    /* access modifiers changed from: protected */
    public float getFloat(String str) {
        return this.tu.zzf(str, this.vX, this.vY);
    }

    /* access modifiers changed from: protected */
    public int getInteger(String str) {
        return this.tu.zzc(str, this.vX, this.vY);
    }

    /* access modifiers changed from: protected */
    public long getLong(String str) {
        return this.tu.zzb(str, this.vX, this.vY);
    }

    /* access modifiers changed from: protected */
    public String getString(String str) {
        return this.tu.zzd(str, this.vX, this.vY);
    }

    public int hashCode() {
        return zzaa.hashCode(Integer.valueOf(this.vX), Integer.valueOf(this.vY), this.tu);
    }

    public boolean isDataValid() {
        return !this.tu.isClosed();
    }

    /* access modifiers changed from: protected */
    public void zza(String str, CharArrayBuffer charArrayBuffer) {
        this.tu.zza(str, this.vX, this.vY, charArrayBuffer);
    }

    /* access modifiers changed from: protected */
    public int zzarf() {
        return this.vX;
    }

    /* access modifiers changed from: protected */
    public void zzfq(int i) {
        zzab.zzbn(i >= 0 && i < this.tu.getCount());
        this.vX = i;
        this.vY = this.tu.zzfs(this.vX);
    }

    public boolean zzhe(String str) {
        return this.tu.zzhe(str);
    }

    /* access modifiers changed from: protected */
    public Uri zzhf(String str) {
        return this.tu.zzh(str, this.vX, this.vY);
    }

    /* access modifiers changed from: protected */
    public boolean zzhg(String str) {
        return this.tu.zzi(str, this.vX, this.vY);
    }
}
