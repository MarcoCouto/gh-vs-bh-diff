package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.zzab;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class zzb<T> implements Iterator<T> {
    protected final DataBuffer<T> vU;
    protected int vV = -1;

    public zzb(DataBuffer<T> dataBuffer) {
        this.vU = (DataBuffer) zzab.zzy(dataBuffer);
    }

    public boolean hasNext() {
        return this.vV < this.vU.getCount() - 1;
    }

    public T next() {
        if (!hasNext()) {
            int i = this.vV;
            StringBuilder sb = new StringBuilder(46);
            sb.append("Cannot advance the iterator beyond ");
            sb.append(i);
            throw new NoSuchElementException(sb.toString());
        }
        DataBuffer<T> dataBuffer = this.vU;
        int i2 = this.vV + 1;
        this.vV = i2;
        return dataBuffer.get(i2);
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
