package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf implements Creator<Status> {
    static void zza(Status status, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, status.getStatusCode());
        zzb.zza(parcel, 2, status.getStatusMessage(), false);
        zzb.zza(parcel, 3, (Parcelable) status.zzaol(), i, false);
        zzb.zzc(parcel, 1000, status.getVersionCode());
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzca */
    public Status createFromParcel(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        String str = null;
        PendingIntent pendingIntent = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            int zzgm = zza.zzgm(zzcl);
            if (zzgm != 1000) {
                switch (zzgm) {
                    case 1:
                        i2 = zza.zzg(parcel, zzcl);
                        break;
                    case 2:
                        str = zza.zzq(parcel, zzcl);
                        break;
                    case 3:
                        pendingIntent = (PendingIntent) zza.zza(parcel, zzcl, PendingIntent.CREATOR);
                        break;
                    default:
                        zza.zzb(parcel, zzcl);
                        break;
                }
            } else {
                i = zza.zzg(parcel, zzcl);
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new Status(i, i2, str, pendingIntent);
        }
        StringBuilder sb = new StringBuilder(37);
        sb.append("Overread allowed size end=");
        sb.append(zzcm);
        throw new C0028zza(sb.toString(), parcel);
    }

    /* renamed from: zzfg */
    public Status[] newArray(int i) {
        return new Status[i];
    }
}
