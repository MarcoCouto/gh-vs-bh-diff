package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.ApiOptions.NotRequiredOptions;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.api.Api.zzh;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzah;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.internal.zzg.zza;
import com.google.android.gms.internal.zzpk;
import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzpp;
import com.google.android.gms.internal.zzpy;
import com.google.android.gms.internal.zzqi;
import com.google.android.gms.internal.zzqn;
import com.google.android.gms.internal.zzqt;
import com.google.android.gms.internal.zzqx;
import com.google.android.gms.internal.zzvt;
import com.google.android.gms.internal.zzvu;
import com.google.android.gms.internal.zzvv;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GoogleApiClient {
    public static final int SIGN_IN_MODE_OPTIONAL = 2;
    public static final int SIGN_IN_MODE_REQUIRED = 1;
    /* access modifiers changed from: private */
    public static final Set<GoogleApiClient> rW = Collections.newSetFromMap(new WeakHashMap());

    public static final class Builder {
        private Account aL;
        private String bX;
        private final Context mContext;
        private final Set<Scope> rX;
        private final Set<Scope> rY;
        private int rZ;
        private View sa;
        private String sb;
        private final Map<Api<?>, zza> sc;
        private final Map<Api<?>, ApiOptions> sd;
        private zzqi se;
        private int sf;
        private OnConnectionFailedListener sg;
        private GoogleApiAvailability sh;
        private Api.zza<? extends zzvu, zzvv> si;
        private final ArrayList<ConnectionCallbacks> sj;
        private final ArrayList<OnConnectionFailedListener> sk;
        private Looper zzahv;

        public Builder(@NonNull Context context) {
            this.rX = new HashSet();
            this.rY = new HashSet();
            this.sc = new ArrayMap();
            this.sd = new ArrayMap();
            this.sf = -1;
            this.sh = GoogleApiAvailability.getInstance();
            this.si = zzvt.bK;
            this.sj = new ArrayList<>();
            this.sk = new ArrayList<>();
            this.mContext = context;
            this.zzahv = context.getMainLooper();
            this.bX = context.getPackageName();
            this.sb = context.getClass().getName();
        }

        public Builder(@NonNull Context context, @NonNull ConnectionCallbacks connectionCallbacks, @NonNull OnConnectionFailedListener onConnectionFailedListener) {
            this(context);
            zzab.zzb(connectionCallbacks, (Object) "Must provide a connected listener");
            this.sj.add(connectionCallbacks);
            zzab.zzb(onConnectionFailedListener, (Object) "Must provide a connection failed listener");
            this.sk.add(onConnectionFailedListener);
        }

        private static <C extends zze, O> C zza(Api.zza<C, O> zza, Object obj, Context context, Looper looper, zzg zzg, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            return zza.zza(context, looper, zzg, obj, connectionCallbacks, onConnectionFailedListener);
        }

        private Builder zza(@NonNull zzqi zzqi, int i, @Nullable OnConnectionFailedListener onConnectionFailedListener) {
            zzab.zzb(i >= 0, (Object) "clientId must be non-negative");
            this.sf = i;
            this.sg = onConnectionFailedListener;
            this.se = zzqi;
            return this;
        }

        private static <C extends Api.zzg, O> zzah zza(zzh<C, O> zzh, Object obj, Context context, Looper looper, zzg zzg, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            zzah zzah = new zzah(context, looper, zzh.zzanw(), connectionCallbacks, onConnectionFailedListener, zzg, zzh.zzr(obj));
            return zzah;
        }

        private <O extends ApiOptions> void zza(Api<O> api, O o, int i, Scope... scopeArr) {
            boolean z = true;
            if (i != 1) {
                if (i == 2) {
                    z = false;
                } else {
                    StringBuilder sb2 = new StringBuilder(90);
                    sb2.append("Invalid resolution mode: '");
                    sb2.append(i);
                    sb2.append("', use a constant from GoogleApiClient.ResolutionMode");
                    throw new IllegalArgumentException(sb2.toString());
                }
            }
            HashSet hashSet = new HashSet(api.zzanp().zzp(o));
            for (Scope add : scopeArr) {
                hashSet.add(add);
            }
            this.sc.put(api, new zza(hashSet, z));
        }

        private GoogleApiClient zzaoi() {
            Api api;
            Api api2;
            zze zze;
            zzg zzaoh = zzaoh();
            Map zzasl = zzaoh.zzasl();
            ArrayMap arrayMap = new ArrayMap();
            ArrayMap arrayMap2 = new ArrayMap();
            ArrayList arrayList = new ArrayList();
            Iterator it = this.sd.keySet().iterator();
            Api api3 = null;
            Api api4 = null;
            while (true) {
                int i = 0;
                if (it.hasNext()) {
                    Api api5 = (Api) it.next();
                    Object obj = this.sd.get(api5);
                    if (zzasl.get(api5) != null) {
                        i = ((zza) zzasl.get(api5)).yn ? 1 : 2;
                    }
                    arrayMap.put(api5, Integer.valueOf(i));
                    zzpp zzpp = new zzpp(api5, i);
                    arrayList.add(zzpp);
                    if (api5.zzant()) {
                        zzh zzanr = api5.zzanr();
                        api = zzanr.getPriority() == 1 ? api5 : api3;
                        api2 = api5;
                        zze = zza(zzanr, obj, this.mContext, this.zzahv, zzaoh, (ConnectionCallbacks) zzpp, (OnConnectionFailedListener) zzpp);
                    } else {
                        zzpp zzpp2 = zzpp;
                        api2 = api5;
                        Api.zza zzanq = api2.zzanq();
                        api = zzanq.getPriority() == 1 ? api2 : api3;
                        zze = zza(zzanq, obj, this.mContext, this.zzahv, zzaoh, (ConnectionCallbacks) zzpp2, (OnConnectionFailedListener) zzpp2);
                    }
                    arrayMap2.put(api2.zzans(), zze);
                    if (zze.zzafz()) {
                        if (api4 != null) {
                            String valueOf = String.valueOf(api2.getName());
                            String valueOf2 = String.valueOf(api4.getName());
                            StringBuilder sb2 = new StringBuilder(21 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
                            sb2.append(valueOf);
                            sb2.append(" cannot be used with ");
                            sb2.append(valueOf2);
                            throw new IllegalStateException(sb2.toString());
                        }
                        api4 = api2;
                    }
                    api3 = api;
                } else {
                    if (api4 != null) {
                        if (api3 != null) {
                            String valueOf3 = String.valueOf(api4.getName());
                            String valueOf4 = String.valueOf(api3.getName());
                            StringBuilder sb3 = new StringBuilder(21 + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length());
                            sb3.append(valueOf3);
                            sb3.append(" cannot be used with ");
                            sb3.append(valueOf4);
                            throw new IllegalStateException(sb3.toString());
                        }
                        zzab.zza(this.aL == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", api4.getName());
                        zzab.zza(this.rX.equals(this.rY), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", api4.getName());
                    }
                    zzpy zzpy = new zzpy(this.mContext, new ReentrantLock(), this.zzahv, zzaoh, this.sh, this.si, arrayMap, this.sj, this.sk, arrayMap2, this.sf, zzpy.zza(arrayMap2.values(), true), arrayList);
                    return zzpy;
                }
            }
        }

        private void zzf(GoogleApiClient googleApiClient) {
            zzpk.zza(this.se).zza(this.sf, googleApiClient, this.sg);
        }

        public Builder addApi(@NonNull Api<? extends NotRequiredOptions> api) {
            zzab.zzb(api, (Object) "Api must not be null");
            this.sd.put(api, null);
            List zzp = api.zzanp().zzp(null);
            this.rY.addAll(zzp);
            this.rX.addAll(zzp);
            return this;
        }

        public <O extends HasOptions> Builder addApi(@NonNull Api<O> api, @NonNull O o) {
            zzab.zzb(api, (Object) "Api must not be null");
            zzab.zzb(o, (Object) "Null options are not permitted for this Api");
            this.sd.put(api, o);
            List zzp = api.zzanp().zzp(o);
            this.rY.addAll(zzp);
            this.rX.addAll(zzp);
            return this;
        }

        public <O extends HasOptions> Builder addApiIfAvailable(@NonNull Api<O> api, @NonNull O o, Scope... scopeArr) {
            zzab.zzb(api, (Object) "Api must not be null");
            zzab.zzb(o, (Object) "Null options are not permitted for this Api");
            this.sd.put(api, o);
            zza(api, o, 1, scopeArr);
            return this;
        }

        public Builder addApiIfAvailable(@NonNull Api<? extends NotRequiredOptions> api, Scope... scopeArr) {
            zzab.zzb(api, (Object) "Api must not be null");
            this.sd.put(api, null);
            zza(api, null, 1, scopeArr);
            return this;
        }

        public Builder addConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks) {
            zzab.zzb(connectionCallbacks, (Object) "Listener must not be null");
            this.sj.add(connectionCallbacks);
            return this;
        }

        public Builder addOnConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener) {
            zzab.zzb(onConnectionFailedListener, (Object) "Listener must not be null");
            this.sk.add(onConnectionFailedListener);
            return this;
        }

        public Builder addScope(@NonNull Scope scope) {
            zzab.zzb(scope, (Object) "Scope must not be null");
            this.rX.add(scope);
            return this;
        }

        public GoogleApiClient build() {
            zzab.zzb(!this.sd.isEmpty(), (Object) "must call addApi() to add at least one API");
            GoogleApiClient zzaoi = zzaoi();
            synchronized (GoogleApiClient.rW) {
                GoogleApiClient.rW.add(zzaoi);
            }
            if (this.sf >= 0) {
                zzf(zzaoi);
            }
            return zzaoi;
        }

        public Builder enableAutoManage(@NonNull FragmentActivity fragmentActivity, int i, @Nullable OnConnectionFailedListener onConnectionFailedListener) {
            return zza(new zzqi(fragmentActivity), i, onConnectionFailedListener);
        }

        public Builder enableAutoManage(@NonNull FragmentActivity fragmentActivity, @Nullable OnConnectionFailedListener onConnectionFailedListener) {
            return enableAutoManage(fragmentActivity, 0, onConnectionFailedListener);
        }

        public Builder setAccountName(String str) {
            this.aL = str == null ? null : new Account(str, "com.google");
            return this;
        }

        public Builder setGravityForPopups(int i) {
            this.rZ = i;
            return this;
        }

        public Builder setHandler(@NonNull Handler handler) {
            zzab.zzb(handler, (Object) "Handler must not be null");
            this.zzahv = handler.getLooper();
            return this;
        }

        public Builder setViewForPopups(@NonNull View view) {
            zzab.zzb(view, (Object) "View must not be null");
            this.sa = view;
            return this;
        }

        public Builder useDefaultAccount() {
            return setAccountName("<<default account>>");
        }

        public zzg zzaoh() {
            zzvv zzvv = zzvv.atR;
            if (this.sd.containsKey(zzvt.API)) {
                zzvv = (zzvv) this.sd.get(zzvt.API);
            }
            zzg zzg = new zzg(this.aL, this.rX, this.sc, this.rZ, this.sa, this.bX, this.sb, zzvv);
            return zzg;
        }
    }

    public interface ConnectionCallbacks {
        public static final int CAUSE_NETWORK_LOST = 2;
        public static final int CAUSE_SERVICE_DISCONNECTED = 1;

        void onConnected(@Nullable Bundle bundle);

        void onConnectionSuspended(int i);
    }

    public interface OnConnectionFailedListener {
        void onConnectionFailed(@NonNull ConnectionResult connectionResult);
    }

    public static void dumpAll(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        synchronized (rW) {
            int i = 0;
            String concat = String.valueOf(str).concat("  ");
            for (GoogleApiClient googleApiClient : rW) {
                int i2 = i + 1;
                printWriter.append(str).append("GoogleApiClient#").println(i);
                googleApiClient.dump(concat, fileDescriptor, printWriter, strArr);
                i = i2;
            }
        }
    }

    public static Set<GoogleApiClient> zzaoe() {
        Set<GoogleApiClient> set;
        synchronized (rW) {
            set = rW;
        }
        return set;
    }

    public abstract ConnectionResult blockingConnect();

    public abstract ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit);

    public abstract PendingResult<Status> clearDefaultAccountAndReconnect();

    public abstract void connect();

    public void connect(int i) {
        throw new UnsupportedOperationException();
    }

    public abstract void disconnect();

    public abstract void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @NonNull
    public abstract ConnectionResult getConnectionResult(@NonNull Api<?> api);

    public Context getContext() {
        throw new UnsupportedOperationException();
    }

    public Looper getLooper() {
        throw new UnsupportedOperationException();
    }

    public abstract boolean hasConnectedApi(@NonNull Api<?> api);

    public abstract boolean isConnected();

    public abstract boolean isConnecting();

    public abstract boolean isConnectionCallbacksRegistered(@NonNull ConnectionCallbacks connectionCallbacks);

    public abstract boolean isConnectionFailedListenerRegistered(@NonNull OnConnectionFailedListener onConnectionFailedListener);

    public abstract void reconnect();

    public abstract void registerConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks);

    public abstract void registerConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener);

    public abstract void stopAutoManage(@NonNull FragmentActivity fragmentActivity);

    public abstract void unregisterConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks);

    public abstract void unregisterConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener);

    @NonNull
    public <C extends zze> C zza(@NonNull zzc<C> zzc) {
        throw new UnsupportedOperationException();
    }

    public void zza(zzqx zzqx) {
        throw new UnsupportedOperationException();
    }

    public boolean zza(@NonNull Api<?> api) {
        throw new UnsupportedOperationException();
    }

    public boolean zza(zzqt zzqt) {
        throw new UnsupportedOperationException();
    }

    public void zzaof() {
        throw new UnsupportedOperationException();
    }

    public void zzb(zzqx zzqx) {
        throw new UnsupportedOperationException();
    }

    public <A extends zzb, R extends Result, T extends zzpm.zza<R, A>> T zzc(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    public <A extends zzb, T extends zzpm.zza<? extends Result, A>> T zzd(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    public <L> zzqn<L> zzs(@NonNull L l) {
        throw new UnsupportedOperationException();
    }
}
