package com.google.android.gms.common.api;

import com.google.android.gms.common.api.PendingResult.zza;
import com.google.android.gms.internal.zzpo;
import java.util.ArrayList;
import java.util.List;

public final class Batch extends zzpo<BatchResult> {
    /* access modifiers changed from: private */
    public int rH;
    /* access modifiers changed from: private */
    public boolean rI;
    /* access modifiers changed from: private */
    public boolean rJ;
    /* access modifiers changed from: private */
    public final PendingResult<?>[] rK;
    /* access modifiers changed from: private */
    public final Object zzail;

    public static final class Builder {
        private GoogleApiClient gY;
        private List<PendingResult<?>> rM = new ArrayList();

        public Builder(GoogleApiClient googleApiClient) {
            this.gY = googleApiClient;
        }

        public <R extends Result> BatchResultToken<R> add(PendingResult<R> pendingResult) {
            BatchResultToken<R> batchResultToken = new BatchResultToken<>(this.rM.size());
            this.rM.add(pendingResult);
            return batchResultToken;
        }

        public Batch build() {
            return new Batch(this.rM, this.gY);
        }
    }

    private Batch(List<PendingResult<?>> list, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.zzail = new Object();
        this.rH = list.size();
        this.rK = new PendingResult[this.rH];
        if (list.isEmpty()) {
            zzc(new BatchResult(Status.sq, this.rK));
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            PendingResult<?> pendingResult = (PendingResult) list.get(i);
            this.rK[i] = pendingResult;
            pendingResult.zza(new zza() {
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
                    return;
                 */
                public void zzv(Status status) {
                    synchronized (Batch.this.zzail) {
                        if (!Batch.this.isCanceled()) {
                            if (status.isCanceled()) {
                                Batch.this.rJ = true;
                            } else if (!status.isSuccess()) {
                                Batch.this.rI = true;
                            }
                            Batch.this.rH = Batch.this.rH - 1;
                            if (Batch.this.rH == 0) {
                                if (Batch.this.rJ) {
                                    Batch.super.cancel();
                                } else {
                                    Batch.this.zzc(new BatchResult(Batch.this.rI ? new Status(13) : Status.sq, Batch.this.rK));
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    public void cancel() {
        super.cancel();
        for (PendingResult<?> cancel : this.rK) {
            cancel.cancel();
        }
    }

    /* renamed from: createFailedResult */
    public BatchResult zzc(Status status) {
        return new BatchResult(status, this.rK);
    }
}
