package com.google.android.gms.clearcut;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzpb;
import com.google.android.gms.internal.zzpc;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimeZone;

public final class zzb {
    public static final Api<NoOptions> API = new Api<>("ClearcutLogger.API", bK, bJ);
    public static final zzf<zzpc> bJ = new zzf<>();
    public static final com.google.android.gms.common.api.Api.zza<zzpc, NoOptions> bK = new com.google.android.gms.common.api.Api.zza<zzpc, NoOptions>() {
        /* renamed from: zze */
        public zzpc zza(Context context, Looper looper, zzg zzg, NoOptions noOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            zzpc zzpc = new zzpc(context, looper, zzg, connectionCallbacks, onConnectionFailedListener);
            return zzpc;
        }
    };
    public static final zzc pZ = new zzpb();
    /* access modifiers changed from: private */
    public final String aM;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final int qa;
    /* access modifiers changed from: private */
    public String qb;
    /* access modifiers changed from: private */
    public int qc;
    /* access modifiers changed from: private */
    public String qd;
    /* access modifiers changed from: private */
    public String qe;
    /* access modifiers changed from: private */
    public final boolean qf;
    private int qg;
    /* access modifiers changed from: private */
    public final zzc qh;
    /* access modifiers changed from: private */
    public final zza qi;
    /* access modifiers changed from: private */
    public zzd qj;
    /* access modifiers changed from: private */
    public final C0026zzb qk;
    /* access modifiers changed from: private */
    public final zze zzaoc;

    public class zza {
        private String qb;
        private int qc;
        private String qd;
        private String qe;
        private int qg;
        private final zzc ql;
        private ArrayList<Integer> qm;
        private ArrayList<String> qn;
        private ArrayList<Integer> qo;
        private ArrayList<byte[]> qp;
        private boolean qq;
        private final com.google.android.gms.internal.zzapz.zzd qr;
        private boolean qs;

        private zza(zzb zzb, byte[] bArr) {
            this(bArr, (zzc) null);
        }

        private zza(byte[] bArr, zzc zzc) {
            this.qc = zzb.this.qc;
            this.qb = zzb.this.qb;
            this.qd = zzb.this.qd;
            this.qe = zzb.this.qe;
            this.qg = zzb.zze(zzb.this);
            this.qm = null;
            this.qn = null;
            this.qo = null;
            this.qp = null;
            this.qq = true;
            this.qr = new com.google.android.gms.internal.zzapz.zzd();
            this.qs = false;
            this.qd = zzb.this.qd;
            this.qe = zzb.this.qe;
            this.qr.bka = zzb.this.zzaoc.currentTimeMillis();
            this.qr.bkb = zzb.this.zzaoc.elapsedRealtime();
            this.qr.bks = (long) zzb.this.qi.zzbk(zzb.this.mContext);
            this.qr.bkm = zzb.this.qj.zzae(this.qr.bka);
            if (bArr != null) {
                this.qr.bkh = bArr;
            }
            this.ql = zzc;
        }

        public LogEventParcelable zzana() {
            PlayLoggerContext playLoggerContext = new PlayLoggerContext(zzb.this.aM, zzb.this.qa, this.qc, this.qb, this.qd, this.qe, zzb.this.qf, this.qg);
            LogEventParcelable logEventParcelable = new LogEventParcelable(playLoggerContext, this.qr, this.ql, null, zzb.zzb(null), zzb.zzc(null), zzb.zzb(null), zzb.zzd(null), this.qq);
            return logEventParcelable;
        }

        public PendingResult<Status> zze(GoogleApiClient googleApiClient) {
            if (this.qs) {
                throw new IllegalStateException("do not reuse LogEventBuilder");
            }
            this.qs = true;
            PlayLoggerContext playLoggerContext = zzana().qu;
            return zzb.this.qk.zzg(playLoggerContext.arv, playLoggerContext.arr) ? zzb.this.qh.zza(googleApiClient, zzana()) : PendingResults.immediatePendingResult(Status.sq);
        }

        public zza zzey(int i) {
            this.qr.bkd = i;
            return this;
        }

        public zza zzez(int i) {
            this.qr.zzahl = i;
            return this;
        }
    }

    /* renamed from: com.google.android.gms.clearcut.zzb$zzb reason: collision with other inner class name */
    public interface C0026zzb {
        boolean zzg(String str, int i);
    }

    public interface zzc {
        byte[] zzanb();
    }

    public static class zzd {
        public long zzae(long j) {
            return (long) (TimeZone.getDefault().getOffset(j) / 1000);
        }
    }

    public zzb(Context context, int i, String str, String str2, String str3, boolean z, zzc zzc2, zze zze, zzd zzd2, zza zza2, C0026zzb zzb) {
        this.qc = -1;
        boolean z2 = false;
        this.qg = 0;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext == null) {
            applicationContext = context;
        }
        this.mContext = applicationContext;
        this.aM = context.getPackageName();
        this.qa = zzbl(context);
        this.qc = i;
        this.qb = str;
        this.qd = str2;
        this.qe = str3;
        this.qf = z;
        this.qh = zzc2;
        this.zzaoc = zze;
        if (zzd2 == null) {
            zzd2 = new zzd();
        }
        this.qj = zzd2;
        this.qi = zza2;
        this.qg = 0;
        this.qk = zzb;
        if (this.qf) {
            if (this.qd == null) {
                z2 = true;
            }
            zzab.zzb(z2, (Object) "can't be anonymous with an upload account");
        }
    }

    public zzb(Context context, String str, String str2) {
        this(context, -1, str, str2, null, false, pZ, zzh.zzavm(), null, zza.pY, new zzpg(context));
    }

    /* access modifiers changed from: private */
    public static int[] zzb(ArrayList<Integer> arrayList) {
        if (arrayList == null) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        int i = 0;
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            int i2 = i + 1;
            iArr[i] = ((Integer) it.next()).intValue();
            i = i2;
        }
        return iArr;
    }

    private int zzbl(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException unused) {
            Log.wtf("ClearcutLogger", "This can't happen.");
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public static String[] zzc(ArrayList<String> arrayList) {
        if (arrayList == null) {
            return null;
        }
        return (String[]) arrayList.toArray(new String[0]);
    }

    /* access modifiers changed from: private */
    public static byte[][] zzd(ArrayList<byte[]> arrayList) {
        if (arrayList == null) {
            return null;
        }
        return (byte[][]) arrayList.toArray(new byte[0][]);
    }

    static /* synthetic */ int zze(zzb zzb) {
        return 0;
    }

    public zza zzl(byte[] bArr) {
        return new zza(bArr);
    }
}
