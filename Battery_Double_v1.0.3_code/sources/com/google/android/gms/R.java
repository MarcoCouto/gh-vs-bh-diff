package com.google.android.gms;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837538;
        public static final int adSizes = 2130837539;
        public static final int adUnitId = 2130837540;
        public static final int buttonSize = 2130837561;
        public static final int circleCrop = 2130837568;
        public static final int colorScheme = 2130837581;
        public static final int imageAspectRatio = 2130837615;
        public static final int imageAspectRatioAdjust = 2130837616;
        public static final int scopeUris = 2130837656;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968604;
        public static final int common_google_signin_btn_text_dark_default = 2130968605;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968606;
        public static final int common_google_signin_btn_text_dark_focused = 2130968607;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968608;
        public static final int common_google_signin_btn_text_light = 2130968609;
        public static final int common_google_signin_btn_text_light_default = 2130968610;
        public static final int common_google_signin_btn_text_light_disabled = 2130968611;
        public static final int common_google_signin_btn_text_light_focused = 2130968612;
        public static final int common_google_signin_btn_text_light_pressed = 2130968613;
        public static final int common_plus_signin_btn_text_dark = 2130968614;
        public static final int common_plus_signin_btn_text_dark_default = 2130968615;
        public static final int common_plus_signin_btn_text_dark_disabled = 2130968616;
        public static final int common_plus_signin_btn_text_dark_focused = 2130968617;
        public static final int common_plus_signin_btn_text_dark_pressed = 2130968618;
        public static final int common_plus_signin_btn_text_light = 2130968619;
        public static final int common_plus_signin_btn_text_light_default = 2130968620;
        public static final int common_plus_signin_btn_text_light_disabled = 2130968621;
        public static final int common_plus_signin_btn_text_light_focused = 2130968622;
        public static final int common_plus_signin_btn_text_light_pressed = 2130968623;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099718;
        public static final int common_google_signin_btn_icon_dark = 2131099719;
        public static final int common_google_signin_btn_icon_dark_disabled = 2131099720;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099721;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099722;
        public static final int common_google_signin_btn_icon_dark_pressed = 2131099723;
        public static final int common_google_signin_btn_icon_light = 2131099724;
        public static final int common_google_signin_btn_icon_light_disabled = 2131099725;
        public static final int common_google_signin_btn_icon_light_focused = 2131099726;
        public static final int common_google_signin_btn_icon_light_normal = 2131099727;
        public static final int common_google_signin_btn_icon_light_pressed = 2131099728;
        public static final int common_google_signin_btn_text_dark = 2131099729;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099730;
        public static final int common_google_signin_btn_text_dark_focused = 2131099731;
        public static final int common_google_signin_btn_text_dark_normal = 2131099732;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099733;
        public static final int common_google_signin_btn_text_light = 2131099734;
        public static final int common_google_signin_btn_text_light_disabled = 2131099735;
        public static final int common_google_signin_btn_text_light_focused = 2131099736;
        public static final int common_google_signin_btn_text_light_normal = 2131099737;
        public static final int common_google_signin_btn_text_light_pressed = 2131099738;
        public static final int common_ic_googleplayservices = 2131099739;
        public static final int common_plus_signin_btn_icon_dark = 2131099740;
        public static final int common_plus_signin_btn_icon_dark_disabled = 2131099741;
        public static final int common_plus_signin_btn_icon_dark_focused = 2131099742;
        public static final int common_plus_signin_btn_icon_dark_normal = 2131099743;
        public static final int common_plus_signin_btn_icon_dark_pressed = 2131099744;
        public static final int common_plus_signin_btn_icon_light = 2131099745;
        public static final int common_plus_signin_btn_icon_light_disabled = 2131099746;
        public static final int common_plus_signin_btn_icon_light_focused = 2131099747;
        public static final int common_plus_signin_btn_icon_light_normal = 2131099748;
        public static final int common_plus_signin_btn_icon_light_pressed = 2131099749;
        public static final int common_plus_signin_btn_text_dark = 2131099750;
        public static final int common_plus_signin_btn_text_dark_disabled = 2131099751;
        public static final int common_plus_signin_btn_text_dark_focused = 2131099752;
        public static final int common_plus_signin_btn_text_dark_normal = 2131099753;
        public static final int common_plus_signin_btn_text_dark_pressed = 2131099754;
        public static final int common_plus_signin_btn_text_light = 2131099755;
        public static final int common_plus_signin_btn_text_light_disabled = 2131099756;
        public static final int common_plus_signin_btn_text_light_focused = 2131099757;
        public static final int common_plus_signin_btn_text_light_normal = 2131099758;
        public static final int common_plus_signin_btn_text_light_pressed = 2131099759;
    }

    public static final class id {
        public static final int adjust_height = 2131165201;
        public static final int adjust_width = 2131165202;
        public static final int auto = 2131165205;
        public static final int dark = 2131165222;
        public static final int icon_only = 2131165234;
        public static final int light = 2131165238;
        public static final int none = 2131165247;
        public static final int normal = 2131165248;
        public static final int radio = 2131165252;
        public static final int standard = 2131165278;
        public static final int text = 2131165282;
        public static final int text2 = 2131165283;
        public static final int wide = 2131165292;
        public static final int wrap_content = 2131165296;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131230724;
    }

    public static final class string {
        public static final int accept = 2131361809;
        public static final int common_google_play_services_enable_button = 2131361811;
        public static final int common_google_play_services_enable_text = 2131361812;
        public static final int common_google_play_services_enable_title = 2131361813;
        public static final int common_google_play_services_install_button = 2131361814;
        public static final int common_google_play_services_install_text_phone = 2131361815;
        public static final int common_google_play_services_install_text_tablet = 2131361816;
        public static final int common_google_play_services_install_title = 2131361817;
        public static final int common_google_play_services_notification_ticker = 2131361818;
        public static final int common_google_play_services_unknown_issue = 2131361819;
        public static final int common_google_play_services_unsupported_text = 2131361820;
        public static final int common_google_play_services_unsupported_title = 2131361821;
        public static final int common_google_play_services_update_button = 2131361822;
        public static final int common_google_play_services_update_text = 2131361823;
        public static final int common_google_play_services_update_title = 2131361824;
        public static final int common_google_play_services_updating_text = 2131361825;
        public static final int common_google_play_services_updating_title = 2131361826;
        public static final int common_google_play_services_wear_update_text = 2131361827;
        public static final int common_open_on_phone = 2131361828;
        public static final int common_signin_button_text = 2131361829;
        public static final int common_signin_button_text_long = 2131361830;
        public static final int create_calendar_message = 2131361831;
        public static final int create_calendar_title = 2131361832;
        public static final int decline = 2131361833;
        public static final int store_picture_message = 2131361843;
        public static final int store_picture_title = 2131361844;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131427559;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.mansoon.BatteryDouble.R.attr.adSize, com.mansoon.BatteryDouble.R.attr.adSizes, com.mansoon.BatteryDouble.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.mansoon.BatteryDouble.R.attr.circleCrop, com.mansoon.BatteryDouble.R.attr.imageAspectRatio, com.mansoon.BatteryDouble.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.mansoon.BatteryDouble.R.attr.buttonSize, com.mansoon.BatteryDouble.R.attr.colorScheme, com.mansoon.BatteryDouble.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
