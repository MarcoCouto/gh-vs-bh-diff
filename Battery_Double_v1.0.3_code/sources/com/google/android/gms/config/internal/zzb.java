package com.google.android.gms.config.internal;

import android.content.Context;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.zzc;
import com.google.android.gms.internal.zzrq;
import com.google.android.gms.internal.zzrr.zza;

public class zzb extends zzc<NoOptions> {
    public zzb(Context context) {
        super(context, zzrq.API, null);
    }

    public PendingResult<com.google.android.gms.internal.zzrr.zzb> zza(zza zza) {
        return zzrq.Bw.zza(zzaoc(), zza);
    }
}
