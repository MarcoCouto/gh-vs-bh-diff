package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class FetchConfigIpcRequest extends AbstractSafeParcelable {
    public static final Creator<FetchConfigIpcRequest> CREATOR = new zze();
    private final long BH;
    private final DataHolder BI;
    private final String BJ;
    private final String BK;
    private final String BL;
    private final List<String> BM;
    private final String aM;
    public final int mVersionCode;

    FetchConfigIpcRequest(int i, String str, long j, DataHolder dataHolder, String str2, String str3, String str4, List<String> list) {
        this.mVersionCode = i;
        this.aM = str;
        this.BH = j;
        this.BI = dataHolder;
        this.BJ = str2;
        this.BK = str3;
        this.BL = str4;
        this.BM = list;
    }

    public FetchConfigIpcRequest(String str, long j, DataHolder dataHolder, String str2, String str3, String str4) {
        this(1, str, j, dataHolder, str2, str3, str4, null);
    }

    public String getPackageName() {
        return this.aM;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zze.zza(this, parcel, i);
    }

    public long zzawl() {
        return this.BH;
    }

    public DataHolder zzawm() {
        return this.BI;
    }

    public String zzawn() {
        return this.BJ;
    }

    public String zzawo() {
        return this.BK;
    }

    public String zzawp() {
        return this.BL;
    }

    public List<String> zzawq() {
        return this.BM;
    }
}
