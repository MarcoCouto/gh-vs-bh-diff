package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;

public class zze implements Creator<FetchConfigIpcRequest> {
    static void zza(FetchConfigIpcRequest fetchConfigIpcRequest, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, fetchConfigIpcRequest.mVersionCode);
        zzb.zza(parcel, 2, fetchConfigIpcRequest.getPackageName(), false);
        zzb.zza(parcel, 3, fetchConfigIpcRequest.zzawl());
        zzb.zza(parcel, 4, (Parcelable) fetchConfigIpcRequest.zzawm(), i, false);
        zzb.zza(parcel, 5, fetchConfigIpcRequest.zzawn(), false);
        zzb.zza(parcel, 6, fetchConfigIpcRequest.zzawo(), false);
        zzb.zza(parcel, 7, fetchConfigIpcRequest.zzawp(), false);
        zzb.zzb(parcel, 8, fetchConfigIpcRequest.zzawq(), false);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzda */
    public FetchConfigIpcRequest createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int zzcm = zza.zzcm(parcel);
        String str = null;
        DataHolder dataHolder = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        List list = null;
        int i = 0;
        long j = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel2, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel2, zzcl);
                    break;
                case 3:
                    j = zza.zzi(parcel2, zzcl);
                    break;
                case 4:
                    dataHolder = (DataHolder) zza.zza(parcel2, zzcl, DataHolder.CREATOR);
                    break;
                case 5:
                    str2 = zza.zzq(parcel2, zzcl);
                    break;
                case 6:
                    str3 = zza.zzq(parcel2, zzcl);
                    break;
                case 7:
                    str4 = zza.zzq(parcel2, zzcl);
                    break;
                case 8:
                    list = zza.zzae(parcel2, zzcl);
                    break;
                default:
                    zza.zzb(parcel2, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel2);
        }
        FetchConfigIpcRequest fetchConfigIpcRequest = new FetchConfigIpcRequest(i, str, j, dataHolder, str2, str3, str4, list);
        return fetchConfigIpcRequest;
    }

    /* renamed from: zzhg */
    public FetchConfigIpcRequest[] newArray(int i) {
        return new FetchConfigIpcRequest[i];
    }
}
