package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class FetchConfigIpcResponse extends AbstractSafeParcelable {
    public static final Creator<FetchConfigIpcResponse> CREATOR = new zzf();
    private final long BG;
    private final DataHolder BN;
    private final int mVersionCode;
    private final int ok;

    FetchConfigIpcResponse(int i, int i2, DataHolder dataHolder, long j) {
        this.mVersionCode = i;
        this.ok = i2;
        this.BN = dataHolder;
        this.BG = j;
    }

    public int getStatusCode() {
        return this.ok;
    }

    public long getThrottleEndTimeMillis() {
        return this.BG;
    }

    public int getVersionCode() {
        return this.mVersionCode;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzf.zza(this, parcel, i);
    }

    public DataHolder zzawr() {
        return this.BN;
    }

    public void zzaws() {
        if (this.BN != null && !this.BN.isClosed()) {
            this.BN.close();
        }
    }
}
