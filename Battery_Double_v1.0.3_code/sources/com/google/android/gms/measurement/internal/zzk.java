package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk implements Creator<EventParcel> {
    static void zza(EventParcel eventParcel, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, eventParcel.versionCode);
        zzb.zza(parcel, 2, eventParcel.name, false);
        zzb.zza(parcel, 3, (Parcelable) eventParcel.aiI, i, false);
        zzb.zza(parcel, 4, eventParcel.aiJ, false);
        zzb.zza(parcel, 5, eventParcel.aiK);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzop */
    public EventParcel createFromParcel(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        String str = null;
        EventParams eventParams = null;
        String str2 = null;
        int i = 0;
        long j = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case 3:
                    eventParams = (EventParams) zza.zza(parcel, zzcl, (Creator<T>) EventParams.CREATOR);
                    break;
                case 4:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case 5:
                    j = zza.zzi(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel);
        }
        EventParcel eventParcel = new EventParcel(i, str, eventParams, str2, j);
        return eventParcel;
    }

    /* renamed from: zzvk */
    public EventParcel[] newArray(int i) {
        return new EventParcel[i];
    }
}
