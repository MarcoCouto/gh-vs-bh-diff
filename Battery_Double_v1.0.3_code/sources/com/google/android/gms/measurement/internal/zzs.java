package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzuf.zzd;

class zzs {
    final boolean ajN;
    final int ajO;
    long ajP;
    double ajQ;
    long ajR;
    double ajS;
    long ajT;
    double ajU;
    final boolean ajV;

    public zzs(zzd zzd) {
        zzab.zzy(zzd);
        boolean z = true;
        boolean z2 = (zzd.amN == null || zzd.amN.intValue() == 0 || (zzd.amN.intValue() == 4 ? zzd.amQ == null || zzd.amR == null : zzd.amP == null)) ? false : true;
        if (z2) {
            this.ajO = zzd.amN.intValue();
            if (zzd.amO == null || !zzd.amO.booleanValue()) {
                z = false;
            }
            this.ajN = z;
            if (zzd.amN.intValue() == 4) {
                if (this.ajN) {
                    this.ajS = Double.parseDouble(zzd.amQ);
                    this.ajU = Double.parseDouble(zzd.amR);
                } else {
                    this.ajR = Long.parseLong(zzd.amQ);
                    this.ajT = Long.parseLong(zzd.amR);
                }
            } else if (this.ajN) {
                this.ajQ = Double.parseDouble(zzd.amP);
            } else {
                this.ajP = Long.parseLong(zzd.amP);
            }
        } else {
            this.ajO = 0;
            this.ajN = false;
        }
        this.ajV = z2;
    }

    public Boolean zzbk(long j) {
        if (!this.ajV || this.ajN) {
            return null;
        }
        boolean z = false;
        switch (this.ajO) {
            case 1:
                if (j < this.ajP) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 2:
                if (j > this.ajP) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 3:
                if (j == this.ajP) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 4:
                if (j >= this.ajR && j <= this.ajT) {
                    z = true;
                }
                return Boolean.valueOf(z);
            default:
                return null;
        }
    }

    public Boolean zzj(double d) {
        if (!this.ajV || !this.ajN) {
            return null;
        }
        boolean z = false;
        switch (this.ajO) {
            case 1:
                if (d < this.ajQ) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 2:
                if (d > this.ajQ) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 3:
                if (d == this.ajQ || Math.abs(d - this.ajQ) < 2.0d * Math.max(Math.ulp(d), Math.ulp(this.ajQ))) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 4:
                if (d >= this.ajS && d <= this.ajU) {
                    z = true;
                }
                return Boolean.valueOf(z);
            default:
                return null;
        }
    }
}
