package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzuf.zzf;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

class zzag {
    final boolean ajV;
    final int amo;
    final boolean amp;
    final String amq;
    final List<String> amr;
    final String ams;

    public zzag(zzf zzf) {
        zzab.zzy(zzf);
        boolean z = false;
        boolean z2 = (zzf.amV == null || zzf.amV.intValue() == 0 || (zzf.amV.intValue() != 6 ? zzf.amW == null : zzf.amY == null || zzf.amY.length == 0)) ? false : true;
        if (z2) {
            this.amo = zzf.amV.intValue();
            if (zzf.amX != null && zzf.amX.booleanValue()) {
                z = true;
            }
            this.amp = z;
            this.amq = (this.amp || this.amo == 1 || this.amo == 6) ? zzf.amW : zzf.amW.toUpperCase(Locale.ENGLISH);
            this.amr = zzf.amY == null ? null : zza(zzf.amY, this.amp);
            if (this.amo == 1) {
                this.ams = this.amq;
                this.ajV = z2;
            }
        } else {
            this.amo = 0;
            this.amp = false;
            this.amq = null;
            this.amr = null;
        }
        this.ams = null;
        this.ajV = z2;
    }

    private List<String> zza(String[] strArr, boolean z) {
        if (z) {
            return Arrays.asList(strArr);
        }
        ArrayList arrayList = new ArrayList();
        for (String upperCase : strArr) {
            arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
        }
        return arrayList;
    }

    public Boolean zzmi(String str) {
        boolean matches;
        if (!this.ajV || str == null) {
            return null;
        }
        if (!this.amp && this.amo != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (this.amo) {
            case 1:
                matches = Pattern.compile(this.ams, this.amp ? 0 : 66).matcher(str).matches();
                break;
            case 2:
                matches = str.startsWith(this.amq);
                break;
            case 3:
                matches = str.endsWith(this.amq);
                break;
            case 4:
                matches = str.contains(this.amq);
                break;
            case 5:
                matches = str.equals(this.amq);
                break;
            case 6:
                matches = this.amr.contains(str);
                break;
            default:
                return null;
        }
        return Boolean.valueOf(matches);
    }
}
