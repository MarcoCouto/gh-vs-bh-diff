package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzab;
import java.util.Iterator;

public class zzh {
    final long aiA;
    final EventParams aiB;
    final String aiz;
    final String mName;
    final long pJ;
    final String zzcjf;

    zzh(zzx zzx, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        zzab.zzhr(str2);
        zzab.zzhr(str3);
        this.zzcjf = str2;
        this.mName = str3;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.aiz = str;
        this.pJ = j;
        this.aiA = j2;
        if (this.aiA != 0 && this.aiA > this.pJ) {
            zzx.zzbsd().zzbsx().log("Event created with reverse previous/current timestamps");
        }
        this.aiB = zza(zzx, bundle);
    }

    private zzh(zzx zzx, String str, String str2, String str3, long j, long j2, EventParams eventParams) {
        zzab.zzhr(str2);
        zzab.zzhr(str3);
        zzab.zzy(eventParams);
        this.zzcjf = str2;
        this.mName = str3;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.aiz = str;
        this.pJ = j;
        this.aiA = j2;
        if (this.aiA != 0 && this.aiA > this.pJ) {
            zzx.zzbsd().zzbsx().log("Event created with reverse previous/current timestamps");
        }
        this.aiB = eventParams;
    }

    public String toString() {
        String str = this.zzcjf;
        String str2 = this.mName;
        String valueOf = String.valueOf(this.aiB);
        StringBuilder sb = new StringBuilder(33 + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("'");
        sb.append(", name='");
        sb.append(str2);
        sb.append("'");
        sb.append(", params=");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public EventParams zza(zzx zzx, Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return new EventParams(new Bundle());
        }
        Bundle bundle2 = new Bundle(bundle);
        Iterator it = bundle2.keySet().iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str == null) {
                zzx.zzbsd().zzbsv().log("Param name can't be null");
            } else {
                Object zzl = zzx.zzbrz().zzl(str, bundle2.get(str));
                if (zzl == null) {
                    zzx.zzbsd().zzbsx().zzj("Param value can't be null", str);
                } else {
                    zzx.zzbrz().zza(bundle2, str, zzl);
                }
            }
            it.remove();
        }
        return new EventParams(bundle2);
    }

    /* access modifiers changed from: 0000 */
    public zzh zza(zzx zzx, long j) {
        zzh zzh = new zzh(zzx, this.aiz, this.zzcjf, this.mName, this.pJ, j, this.aiB);
        return zzh;
    }
}
