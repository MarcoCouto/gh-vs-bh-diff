package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzuf;
import com.google.android.gms.internal.zzuf.zzb;
import com.google.android.gms.internal.zzuf.zze;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzuh.zzf;
import com.google.android.gms.internal.zzuh.zzg;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurement.zzd;
import com.google.android.gms.measurement.internal.zzp.zza;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

class zzc extends zzaa {
    zzc(zzx zzx) {
        super(zzx);
    }

    private Boolean zza(zzb zzb, zzuh.zzb zzb2, long j) {
        com.google.android.gms.internal.zzuf.zzc[] zzcArr;
        com.google.android.gms.internal.zzuh.zzc[] zzcArr2;
        com.google.android.gms.internal.zzuf.zzc[] zzcArr3;
        String str;
        Object obj;
        if (zzb.amH != null) {
            Boolean zzbk = new zzs(zzb.amH).zzbk(j);
            if (zzbk == null) {
                return null;
            }
            if (!zzbk.booleanValue()) {
                return Boolean.valueOf(false);
            }
        }
        HashSet hashSet = new HashSet();
        for (com.google.android.gms.internal.zzuf.zzc zzc : zzb.amF) {
            if (TextUtils.isEmpty(zzc.amM)) {
                zzbsd().zzbsx().zzj("null or empty param name in filter. event", zzb2.name);
                return null;
            }
            hashSet.add(zzc.amM);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (com.google.android.gms.internal.zzuh.zzc zzc2 : zzb2.ann) {
            if (hashSet.contains(zzc2.name)) {
                if (zzc2.anr != null) {
                    str = zzc2.name;
                    obj = zzc2.anr;
                } else if (zzc2.amw != null) {
                    str = zzc2.name;
                    obj = zzc2.amw;
                } else if (zzc2.zD != null) {
                    str = zzc2.name;
                    obj = zzc2.zD;
                } else {
                    zzbsd().zzbsx().zze("Unknown value for param. event, param", zzb2.name, zzc2.name);
                    return null;
                }
                arrayMap.put(str, obj);
            }
        }
        for (com.google.android.gms.internal.zzuf.zzc zzc3 : zzb.amF) {
            boolean equals = Boolean.TRUE.equals(zzc3.amL);
            String str2 = zzc3.amM;
            if (TextUtils.isEmpty(str2)) {
                zzbsd().zzbsx().zzj("Event has empty param name. event", zzb2.name);
                return null;
            }
            Object obj2 = arrayMap.get(str2);
            if (obj2 instanceof Long) {
                if (zzc3.amK == null) {
                    zzbsd().zzbsx().zze("No number filter for long param. event, param", zzb2.name, str2);
                    return null;
                }
                Boolean zzbk2 = new zzs(zzc3.amK).zzbk(((Long) obj2).longValue());
                if (zzbk2 == null) {
                    return null;
                }
                if ((true ^ zzbk2.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj2 instanceof Double) {
                if (zzc3.amK == null) {
                    zzbsd().zzbsx().zze("No number filter for double param. event, param", zzb2.name, str2);
                    return null;
                }
                Boolean zzj = new zzs(zzc3.amK).zzj(((Double) obj2).doubleValue());
                if (zzj == null) {
                    return null;
                }
                if ((true ^ zzj.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj2 instanceof String) {
                if (zzc3.amJ == null) {
                    zzbsd().zzbsx().zze("No string filter for String param. event, param", zzb2.name, str2);
                    return null;
                }
                Boolean zzmi = new zzag(zzc3.amJ).zzmi((String) obj2);
                if (zzmi == null) {
                    return null;
                }
                if ((true ^ zzmi.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj2 == null) {
                zzbsd().zzbtc().zze("Missing param for filter. event, param", zzb2.name, str2);
                return Boolean.valueOf(false);
            } else {
                zzbsd().zzbsx().zze("Unknown param type. event, param", zzb2.name, str2);
                return null;
            }
        }
        return Boolean.valueOf(true);
    }

    private Boolean zza(zze zze, zzg zzg) {
        zza zzbsx;
        String str;
        Boolean zzmi;
        zza zza;
        String str2;
        com.google.android.gms.internal.zzuf.zzc zzc = zze.amU;
        if (zzc == null) {
            zzbsx = zzbsd().zzbsx();
            str = "Missing property filter. property";
        } else {
            boolean equals = Boolean.TRUE.equals(zzc.amL);
            if (zzg.anr != null) {
                if (zzc.amK == null) {
                    zzbsx = zzbsd().zzbsx();
                    str = "No number filter for long property. property";
                } else {
                    zzmi = new zzs(zzc.amK).zzbk(zzg.anr.longValue());
                }
            } else if (zzg.amw != null) {
                if (zzc.amK == null) {
                    zzbsx = zzbsd().zzbsx();
                    str = "No number filter for double property. property";
                } else {
                    zzmi = new zzs(zzc.amK).zzj(zzg.amw.doubleValue());
                }
            } else if (zzg.zD == null) {
                zzbsx = zzbsd().zzbsx();
                str = "User property has no value, property";
            } else if (zzc.amJ != null) {
                zzmi = new zzag(zzc.amJ).zzmi(zzg.zD);
            } else if (zzc.amK == null) {
                zzbsd().zzbsx().zzj("No string or number filter defined. property", zzg.name);
                return null;
            } else {
                zzs zzs = new zzs(zzc.amK);
                if (zzc.amK.amO == null || !zzc.amK.amO.booleanValue()) {
                    if (zzld(zzg.zD)) {
                        try {
                            return zza(zzs.zzbk(Long.parseLong(zzg.zD)), equals);
                        } catch (NumberFormatException unused) {
                            zza = zzbsd().zzbsx();
                            str2 = "User property value exceeded Long value range. property, value";
                        }
                    } else {
                        zza = zzbsd().zzbsx();
                        str2 = "Invalid user property value for Long number filter. property, value";
                    }
                } else if (zzle(zzg.zD)) {
                    try {
                        double parseDouble = Double.parseDouble(zzg.zD);
                        if (!Double.isInfinite(parseDouble)) {
                            return zza(zzs.zzj(parseDouble), equals);
                        }
                        zzbsd().zzbsx().zze("User property value exceeded Double value range. property, value", zzg.name, zzg.zD);
                        return null;
                    } catch (NumberFormatException unused2) {
                        zza = zzbsd().zzbsx();
                        str2 = "User property value exceeded Double value range. property, value";
                    }
                } else {
                    zza = zzbsd().zzbsx();
                    str2 = "Invalid user property value for Double number filter. property, value";
                }
                zza.zze(str2, zzg.name, zzg.zD);
                return null;
            }
            return zza(zzmi, equals);
        }
        zzbsx.zzj(str, zzg.name);
        return null;
    }

    static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zza(String str, zzuf.zza[] zzaArr) {
        zzb[] zzbArr;
        zze[] zzeArr;
        com.google.android.gms.internal.zzuf.zzc[] zzcArr;
        zzab.zzy(zzaArr);
        for (zzuf.zza zza : zzaArr) {
            for (zzb zzb : zza.amB) {
                String str2 = (String) AppMeasurement.zza.ahE.get(zzb.amE);
                if (str2 != null) {
                    zzb.amE = str2;
                }
                for (com.google.android.gms.internal.zzuf.zzc zzc : zzb.amF) {
                    String str3 = (String) zzd.ahF.get(zzc.amM);
                    if (str3 != null) {
                        zzc.amM = str3;
                    }
                }
            }
            for (zze zze : zza.amA) {
                String str4 = (String) AppMeasurement.zze.ahG.get(zze.amT);
                if (str4 != null) {
                    zze.amT = str4;
                }
            }
        }
        zzbry().zzb(str, zzaArr);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public zzuh.zza[] zza(String str, zzuh.zzb[] zzbArr, zzg[] zzgArr) {
        ArrayMap arrayMap;
        int i;
        Map map;
        Iterator it;
        char c;
        zze zze;
        Iterator it2;
        zzuh.zzb zzb;
        ArrayMap arrayMap2;
        int i2;
        int i3;
        int i4;
        char c2;
        ArrayMap arrayMap3;
        zzi zzi;
        ArrayMap arrayMap4;
        ArrayMap arrayMap5;
        Iterator it3;
        ArrayMap arrayMap6;
        Iterator it4;
        Map map2;
        String str2 = str;
        zzuh.zzb[] zzbArr2 = zzbArr;
        zzg[] zzgArr2 = zzgArr;
        zzab.zzhr(str);
        HashSet hashSet = new HashSet();
        ArrayMap arrayMap7 = new ArrayMap();
        ArrayMap arrayMap8 = new ArrayMap();
        ArrayMap arrayMap9 = new ArrayMap();
        Map zzlr = zzbry().zzlr(str2);
        if (zzlr != null) {
            Iterator it5 = zzlr.keySet().iterator();
            while (it5.hasNext()) {
                int intValue = ((Integer) it5.next()).intValue();
                zzf zzf = (zzf) zzlr.get(Integer.valueOf(intValue));
                BitSet bitSet = (BitSet) arrayMap8.get(Integer.valueOf(intValue));
                BitSet bitSet2 = (BitSet) arrayMap9.get(Integer.valueOf(intValue));
                if (bitSet == null) {
                    bitSet = new BitSet();
                    arrayMap8.put(Integer.valueOf(intValue), bitSet);
                    bitSet2 = new BitSet();
                    arrayMap9.put(Integer.valueOf(intValue), bitSet2);
                }
                int i5 = 0;
                while (i5 < zzf.anT.length * 64) {
                    if (zzal.zza(zzf.anT, i5)) {
                        map2 = zzlr;
                        it4 = it5;
                        arrayMap6 = arrayMap9;
                        zzbsd().zzbtc().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue), Integer.valueOf(i5));
                        bitSet2.set(i5);
                        if (zzal.zza(zzf.anU, i5)) {
                            bitSet.set(i5);
                        }
                    } else {
                        map2 = zzlr;
                        it4 = it5;
                        arrayMap6 = arrayMap9;
                    }
                    i5++;
                    zzlr = map2;
                    it5 = it4;
                    arrayMap9 = arrayMap6;
                }
                Map map3 = zzlr;
                Iterator it6 = it5;
                ArrayMap arrayMap10 = arrayMap9;
                zzuh.zza zza = new zzuh.zza();
                arrayMap7.put(Integer.valueOf(intValue), zza);
                zza.anl = Boolean.valueOf(false);
                zza.ank = zzf;
                zza.anj = new zzf();
                zza.anj.anU = zzal.zza(bitSet);
                zza.anj.anT = zzal.zza(bitSet2);
                zzlr = map3;
                it5 = it6;
                arrayMap9 = arrayMap10;
            }
        }
        ArrayMap arrayMap11 = arrayMap9;
        int i6 = 0;
        char c3 = 256;
        if (zzbArr2 != null) {
            ArrayMap arrayMap12 = new ArrayMap();
            int length = zzbArr2.length;
            int i7 = 0;
            while (i7 < length) {
                zzuh.zzb zzb2 = zzbArr2[i7];
                zzi zzaq = zzbry().zzaq(str2, zzb2.name);
                if (zzaq == null) {
                    zzbsd().zzbsx().zzj("Event aggregate wasn't created during raw event logging. event", zzb2.name);
                    i3 = i7;
                    i2 = length;
                    arrayMap2 = arrayMap12;
                    zzb = zzb2;
                    i4 = i6;
                    c2 = c3;
                    arrayMap3 = arrayMap11;
                    zzi = new zzi(str2, zzb2.name, 1, 1, zzb2.ano.longValue());
                } else {
                    zzb = zzb2;
                    i3 = i7;
                    arrayMap2 = arrayMap12;
                    i2 = length;
                    i4 = i6;
                    c2 = c3;
                    arrayMap3 = arrayMap11;
                    zzi = zzaq.zzbsr();
                }
                zzbry().zza(zzi);
                long j = zzi.aiC;
                zzuh.zzb zzb3 = zzb;
                ArrayMap arrayMap13 = arrayMap2;
                Map map4 = (Map) arrayMap13.get(zzb3.name);
                if (map4 == null) {
                    map4 = zzbry().zzat(str2, zzb3.name);
                    if (map4 == null) {
                        map4 = new ArrayMap();
                    }
                    arrayMap13.put(zzb3.name, map4);
                }
                zzbsd().zzbtc().zze("event, affected audience count", zzb3.name, Integer.valueOf(map4.size()));
                Iterator it7 = map4.keySet().iterator();
                while (it7.hasNext()) {
                    int intValue2 = ((Integer) it7.next()).intValue();
                    if (hashSet.contains(Integer.valueOf(intValue2))) {
                        zzbsd().zzbtc().zzj("Skipping failed audience ID", Integer.valueOf(intValue2));
                    } else {
                        BitSet bitSet3 = (BitSet) arrayMap8.get(Integer.valueOf(intValue2));
                        BitSet bitSet4 = (BitSet) arrayMap3.get(Integer.valueOf(intValue2));
                        if (((zzuh.zza) arrayMap7.get(Integer.valueOf(intValue2))) == null) {
                            zzuh.zza zza2 = new zzuh.zza();
                            arrayMap7.put(Integer.valueOf(intValue2), zza2);
                            zza2.anl = Boolean.valueOf(true);
                            BitSet bitSet5 = new BitSet();
                            arrayMap8.put(Integer.valueOf(intValue2), bitSet5);
                            bitSet4 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue2), bitSet4);
                            bitSet3 = bitSet5;
                        }
                        Iterator it8 = ((List) map4.get(Integer.valueOf(intValue2))).iterator();
                        while (it8.hasNext()) {
                            Map map5 = map4;
                            zzb zzb4 = (zzb) it8.next();
                            ArrayMap arrayMap14 = arrayMap13;
                            Iterator it9 = it7;
                            if (zzbsd().zzaz(2)) {
                                it3 = it8;
                                arrayMap5 = arrayMap3;
                                arrayMap4 = arrayMap8;
                                zzbsd().zzbtc().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(intValue2), zzb4.amD, zzb4.amE);
                                zzbsd().zzbtc().zzj("Filter definition", zzal.zza(zzb4));
                            } else {
                                it3 = it8;
                                arrayMap5 = arrayMap3;
                                arrayMap4 = arrayMap8;
                            }
                            if (zzb4.amD != null) {
                                if (zzb4.amD.intValue() <= 256) {
                                    if (bitSet3.get(zzb4.amD.intValue())) {
                                        zzbsd().zzbtc().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue2), zzb4.amD);
                                    } else {
                                        Boolean zza3 = zza(zzb4, zzb3, j);
                                        zzbsd().zzbtc().zzj("Event filter result", zza3 == 0 ? "null" : zza3);
                                        if (zza3 == 0) {
                                            hashSet.add(Integer.valueOf(intValue2));
                                        } else {
                                            bitSet4.set(zzb4.amD.intValue());
                                            if (zza3.booleanValue()) {
                                                bitSet3.set(zzb4.amD.intValue());
                                            }
                                        }
                                    }
                                    map4 = map5;
                                    arrayMap13 = arrayMap14;
                                    it7 = it9;
                                    it8 = it3;
                                    arrayMap3 = arrayMap5;
                                    arrayMap8 = arrayMap4;
                                }
                            }
                            zzbsd().zzbsx().zzj("Invalid event filter ID. id", String.valueOf(zzb4.amD));
                            map4 = map5;
                            arrayMap13 = arrayMap14;
                            it7 = it9;
                            it8 = it3;
                            arrayMap3 = arrayMap5;
                            arrayMap8 = arrayMap4;
                        }
                        Iterator it10 = it7;
                        c2 = 256;
                    }
                }
                ArrayMap arrayMap15 = arrayMap13;
                ArrayMap arrayMap16 = arrayMap8;
                i7 = i3 + 1;
                c3 = c2;
                i6 = i4;
                length = i2;
                arrayMap12 = arrayMap15;
                arrayMap11 = arrayMap3;
                zzbArr2 = zzbArr;
                zzgArr2 = zzgArr;
            }
        }
        int i8 = i6;
        ArrayMap arrayMap17 = arrayMap8;
        ArrayMap arrayMap18 = arrayMap11;
        char c4 = c3;
        zzg[] zzgArr3 = zzgArr2;
        if (zzgArr3 != null) {
            ArrayMap arrayMap19 = new ArrayMap();
            int length2 = zzgArr3.length;
            int i9 = i8;
            while (i9 < length2) {
                zzg zzg = zzgArr3[i9];
                Map map6 = (Map) arrayMap19.get(zzg.name);
                if (map6 == null) {
                    map6 = zzbry().zzau(str2, zzg.name);
                    if (map6 == null) {
                        map6 = new ArrayMap();
                    }
                    arrayMap19.put(zzg.name, map6);
                }
                zzbsd().zzbtc().zze("property, affected audience count", zzg.name, Integer.valueOf(map6.size()));
                Iterator it11 = map6.keySet().iterator();
                while (it11.hasNext()) {
                    int intValue3 = ((Integer) it11.next()).intValue();
                    if (hashSet.contains(Integer.valueOf(intValue3))) {
                        zzbsd().zzbtc().zzj("Skipping failed audience ID", Integer.valueOf(intValue3));
                    } else {
                        ArrayMap arrayMap20 = arrayMap17;
                        BitSet bitSet6 = (BitSet) arrayMap20.get(Integer.valueOf(intValue3));
                        ArrayMap arrayMap21 = arrayMap18;
                        BitSet bitSet7 = (BitSet) arrayMap21.get(Integer.valueOf(intValue3));
                        if (((zzuh.zza) arrayMap7.get(Integer.valueOf(intValue3))) == null) {
                            zzuh.zza zza4 = new zzuh.zza();
                            arrayMap7.put(Integer.valueOf(intValue3), zza4);
                            zza4.anl = Boolean.valueOf(true);
                            bitSet6 = new BitSet();
                            arrayMap20.put(Integer.valueOf(intValue3), bitSet6);
                            bitSet7 = new BitSet();
                            arrayMap21.put(Integer.valueOf(intValue3), bitSet7);
                        }
                        Iterator it12 = ((List) map6.get(Integer.valueOf(intValue3))).iterator();
                        while (true) {
                            if (!it12.hasNext()) {
                                arrayMap = arrayMap19;
                                i = length2;
                                map = map6;
                                it = it11;
                                c = 256;
                                break;
                            }
                            arrayMap = arrayMap19;
                            zze = (zze) it12.next();
                            i = length2;
                            map = map6;
                            if (zzbsd().zzaz(2)) {
                                it = it11;
                                it2 = it12;
                                zzbsd().zzbtc().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(intValue3), zze.amD, zze.amT);
                                zzbsd().zzbtc().zzj("Filter definition", zzal.zza(zze));
                            } else {
                                it = it11;
                                it2 = it12;
                            }
                            if (zze.amD == null) {
                                c = 256;
                                break;
                            }
                            c = 256;
                            if (zze.amD.intValue() > 256) {
                                break;
                            }
                            if (bitSet6.get(zze.amD.intValue())) {
                                zzbsd().zzbtc().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue3), zze.amD);
                            } else {
                                Boolean zza5 = zza(zze, zzg);
                                zzbsd().zzbtc().zzj("Property filter result", zza5 == 0 ? "null" : zza5);
                                if (zza5 == 0) {
                                    hashSet.add(Integer.valueOf(intValue3));
                                } else {
                                    bitSet7.set(zze.amD.intValue());
                                    if (zza5.booleanValue()) {
                                        bitSet6.set(zze.amD.intValue());
                                    }
                                }
                            }
                            arrayMap19 = arrayMap;
                            length2 = i;
                            map6 = map;
                            it11 = it;
                            it12 = it2;
                            String str3 = str;
                        }
                        zzbsd().zzbsx().zzj("Invalid property filter ID. id", String.valueOf(zze.amD));
                        hashSet.add(Integer.valueOf(intValue3));
                        arrayMap18 = arrayMap21;
                        c4 = c;
                        arrayMap17 = arrayMap20;
                        arrayMap19 = arrayMap;
                        length2 = i;
                        map6 = map;
                        it11 = it;
                        zzg[] zzgArr4 = zzgArr;
                        String str4 = str;
                    }
                }
                char c5 = c4;
                ArrayMap arrayMap22 = arrayMap19;
                int i10 = length2;
                ArrayMap arrayMap23 = arrayMap18;
                ArrayMap arrayMap24 = arrayMap17;
                i9++;
                zzgArr3 = zzgArr;
                str2 = str;
            }
        }
        ArrayMap arrayMap25 = arrayMap18;
        ArrayMap arrayMap26 = arrayMap17;
        zzuh.zza[] zzaArr = new zzuh.zza[arrayMap26.size()];
        int i11 = i8;
        for (Integer intValue4 : arrayMap26.keySet()) {
            int intValue5 = intValue4.intValue();
            if (!hashSet.contains(Integer.valueOf(intValue5))) {
                zzuh.zza zza6 = (zzuh.zza) arrayMap7.get(Integer.valueOf(intValue5));
                if (zza6 == null) {
                    zza6 = new zzuh.zza();
                }
                int i12 = i11 + 1;
                zzaArr[i11] = zza6;
                zza6.amz = Integer.valueOf(intValue5);
                zza6.anj = new zzf();
                zza6.anj.anU = zzal.zza((BitSet) arrayMap26.get(Integer.valueOf(intValue5)));
                zza6.anj.anT = zzal.zza((BitSet) arrayMap25.get(Integer.valueOf(intValue5)));
                zzbry().zza(str, intValue5, zza6.anj);
                i11 = i12;
            }
        }
        return (zzuh.zza[]) Arrays.copyOf(zzaArr, i11);
    }

    /* access modifiers changed from: 0000 */
    public boolean zzld(String str) {
        return Pattern.matches("[+-]?[0-9]+", str);
    }

    /* access modifiers changed from: 0000 */
    public boolean zzle(String str) {
        return Pattern.matches("[+-]?(([0-9]+\\.?)|([0-9]*\\.[0-9]+))", str);
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }
}
