package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzaj implements Creator<UserAttributeParcel> {
    static void zza(UserAttributeParcel userAttributeParcel, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, userAttributeParcel.versionCode);
        zzb.zza(parcel, 2, userAttributeParcel.name, false);
        zzb.zza(parcel, 3, userAttributeParcel.amt);
        zzb.zza(parcel, 4, userAttributeParcel.amu, false);
        zzb.zza(parcel, 5, userAttributeParcel.amv, false);
        zzb.zza(parcel, 6, userAttributeParcel.zD, false);
        zzb.zza(parcel, 7, userAttributeParcel.aiJ, false);
        zzb.zza(parcel, 8, userAttributeParcel.amw, false);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzoq */
    public UserAttributeParcel createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int zzcm = zza.zzcm(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        int i = 0;
        long j = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel2, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel2, zzcl);
                    break;
                case 3:
                    j = zza.zzi(parcel2, zzcl);
                    break;
                case 4:
                    l = zza.zzj(parcel2, zzcl);
                    break;
                case 5:
                    f = zza.zzm(parcel2, zzcl);
                    break;
                case 6:
                    str2 = zza.zzq(parcel2, zzcl);
                    break;
                case 7:
                    str3 = zza.zzq(parcel2, zzcl);
                    break;
                case 8:
                    d = zza.zzo(parcel2, zzcl);
                    break;
                default:
                    zza.zzb(parcel2, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel2);
        }
        UserAttributeParcel userAttributeParcel = new UserAttributeParcel(i, str, j, l, f, str2, str3, d);
        return userAttributeParcel;
    }

    /* renamed from: zzvl */
    public UserAttributeParcel[] newArray(int i) {
        return new UserAttributeParcel[i];
    }
}
