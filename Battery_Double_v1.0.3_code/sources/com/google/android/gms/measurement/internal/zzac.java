package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzf;
import com.google.android.gms.measurement.AppMeasurement.zzb;
import com.google.android.gms.measurement.AppMeasurement.zzc;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class zzac extends zzaa {
    private zzb alA;
    private final Set<zzc> alB = new HashSet();
    private boolean alC;
    private zza alz;

    @MainThread
    @TargetApi(14)
    private class zza implements ActivityLifecycleCallbacks {
        private zza() {
        }

        private boolean zzmh(String str) {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            zzac.this.zzd("auto", "_ldl", str);
            return true;
        }

        private boolean zzs(Uri uri) {
            String queryParameter = uri.getQueryParameter("utm_campaign");
            String queryParameter2 = uri.getQueryParameter("utm_source");
            String queryParameter3 = uri.getQueryParameter("utm_medium");
            String queryParameter4 = uri.getQueryParameter("gclid");
            if (TextUtils.isEmpty(queryParameter) && TextUtils.isEmpty(queryParameter2) && TextUtils.isEmpty(queryParameter3) && TextUtils.isEmpty(queryParameter4)) {
                return false;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("campaign", queryParameter);
            }
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("source", queryParameter2);
            }
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("medium", queryParameter3);
            }
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("gclid", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("term", queryParameter5);
            }
            String queryParameter6 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter6)) {
                bundle.putString("content", queryParameter6);
            }
            String queryParameter7 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter7)) {
                bundle.putString("aclid", queryParameter7);
            }
            String queryParameter8 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter8)) {
                bundle.putString("cp1", queryParameter8);
            }
            String queryParameter9 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter9)) {
                bundle.putString("anid", queryParameter9);
            }
            zzac.this.zze("auto", "_cmp", bundle);
            return true;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            try {
                zzac.this.zzbsd().zzbtc().log("onActivityCreated");
                Intent intent = activity.getIntent();
                if (intent != null) {
                    Uri data = intent.getData();
                    if (data != null && data.isHierarchical()) {
                        if (bundle == null) {
                            zzs(data);
                        }
                        String queryParameter = data.getQueryParameter("referrer");
                        if (!TextUtils.isEmpty(queryParameter)) {
                            if (!queryParameter.contains("gclid")) {
                                zzac.this.zzbsd().zzbtb().log("Activity created with data 'referrer' param without gclid");
                                return;
                            }
                            zzac.this.zzbsd().zzbtb().zzj("Activity created with referrer", queryParameter);
                            zzmh(queryParameter);
                        }
                    }
                }
            } catch (Throwable th) {
                zzac.this.zzbsd().zzbsv().zzj("Throwable caught in onActivityCreated", th);
            }
        }

        public void onActivityDestroyed(Activity activity) {
        }

        @MainThread
        public void onActivityPaused(Activity activity) {
            zzac.this.zzbsb().zzbuz();
        }

        @MainThread
        public void onActivityResumed(Activity activity) {
            zzac.this.zzbsb().zzbux();
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    protected zzac(zzx zzx) {
        super(zzx);
    }

    private void zza(String str, String str2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        zza(str, str2, zzyw().currentTimeMillis(), bundle, z, z2, z3, str3);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void zza(String str, String str2, Object obj, long j) {
        zzab.zzhr(str);
        zzab.zzhr(str2);
        zzwu();
        zzyv();
        zzzg();
        if (!this.ahD.isEnabled()) {
            zzbsd().zzbtb().log("User property not set since app measurement is disabled");
        } else if (this.ahD.zzbto()) {
            zzbsd().zzbtb().zze("Setting user property (FE)", str2, obj);
            UserAttributeParcel userAttributeParcel = new UserAttributeParcel(str2, j, obj, str);
            zzbrx().zza(userAttributeParcel);
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void zzb(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        zzab.zzhr(str);
        zzab.zzhr(str2);
        zzab.zzy(bundle);
        zzwu();
        zzzg();
        if (!this.ahD.isEnabled()) {
            zzbsd().zzbtb().log("Event not sent since app measurement is disabled");
            return;
        }
        if (!this.alC) {
            this.alC = true;
            zzbup();
        }
        boolean zzmt = zzal.zzmt(str2);
        if (z && this.alA != null && !zzmt) {
            zzbsd().zzbtb().zze("Passing event to registered event handler (FE)", str2, bundle);
            this.alA.zzb(str, str2, bundle, j);
        } else if (this.ahD.zzbto()) {
            int zzml = zzbrz().zzml(str2);
            if (zzml != 0) {
                this.ahD.zzbrz().zze(zzml, "_ev", zzbrz().zza(str2, zzbsf().zzbqn(), true));
                return;
            }
            bundle.putString("_o", str);
            Bundle zza2 = zzbrz().zza(str2, bundle, zzf.zzz("_o"), z3);
            if (z2) {
                zza2 = zzal(zza2);
            }
            zzbsd().zzbtb().zze("Logging event (FE)", str2, zza2);
            EventParcel eventParcel = new EventParcel(str2, new EventParams(zza2), str, j);
            zzbrx().zzc(eventParcel, str3);
            for (zzc zzc : this.alB) {
                zzc.zzc(str, str2, zza2, j);
            }
        }
    }

    @WorkerThread
    private void zzbup() {
        try {
            zzg(Class.forName(zzbuq()));
        } catch (ClassNotFoundException unused) {
            zzbsd().zzbta().log("Tag Manager is not found and thus will not be used");
        }
    }

    private String zzbuq() {
        return "com.google.android.gms.tagmanager.TagManagerService";
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void zzcd(boolean z) {
        zzwu();
        zzyv();
        zzzg();
        zzbsd().zzbtb().zzj("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        zzbse().setMeasurementEnabled(z);
        zzbrx().zzbur();
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public void setMeasurementEnabled(final boolean z) {
        zzzg();
        zzyv();
        zzbsc().zzm(new Runnable() {
            public void run() {
                zzac.this.zzcd(z);
            }
        });
    }

    public void setMinimumSessionDuration(final long j) {
        zzyv();
        zzbsc().zzm(new Runnable() {
            public void run() {
                zzac.this.zzbse().akh.set(j);
                zzac.this.zzbsd().zzbtb().zzj("Minimum session duration set", Long.valueOf(j));
            }
        });
    }

    public void setSessionTimeoutDuration(final long j) {
        zzyv();
        zzbsc().zzm(new Runnable() {
            public void run() {
                zzac.this.zzbse().aki.set(j);
                zzac.this.zzbsd().zzbtb().zzj("Session timeout duration set", Long.valueOf(j));
            }
        });
    }

    @WorkerThread
    public void zza(zzb zzb) {
        zzwu();
        zzyv();
        zzzg();
        if (!(zzb == null || zzb == this.alA)) {
            zzab.zza(this.alA == null, (Object) "EventInterceptor already set.");
        }
        this.alA = zzb;
    }

    @WorkerThread
    public void zza(zzc zzc) {
        zzwu();
        zzyv();
        zzzg();
        zzab.zzy(zzc);
        if (this.alB.contains(zzc)) {
            throw new IllegalStateException("OnEventListener already registered.");
        }
        this.alB.add(zzc);
    }

    /* access modifiers changed from: protected */
    public void zza(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        Bundle bundle2 = bundle;
        final Bundle bundle3 = bundle2 != null ? new Bundle(bundle2) : new Bundle();
        zzw zzbsc = zzbsc();
        final String str4 = str;
        final String str5 = str2;
        final long j2 = j;
        final boolean z4 = z;
        final boolean z5 = z2;
        final boolean z6 = z3;
        final String str6 = str3;
        AnonymousClass4 r2 = new Runnable() {
            public void run() {
                zzac.this.zzb(str4, str5, j2, bundle3, z4, z5, z6, str6);
            }
        };
        zzbsc.zzm(r2);
    }

    /* access modifiers changed from: 0000 */
    public void zza(String str, String str2, long j, Object obj) {
        zzw zzbsc = zzbsc();
        final String str3 = str;
        final String str4 = str2;
        final Object obj2 = obj;
        final long j2 = j;
        AnonymousClass5 r1 = new Runnable() {
            public void run() {
                zzac.this.zza(str3, str4, obj2, j2);
            }
        };
        zzbsc.zzm(r1);
    }

    public void zza(String str, String str2, Bundle bundle, boolean z) {
        zzyv();
        zza(str, str2, bundle, true, this.alA == null || zzal.zzmt(str2), z, null);
    }

    /* access modifiers changed from: 0000 */
    public Bundle zzal(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object zzl = zzbrz().zzl(str, bundle.get(str));
                if (zzl == null) {
                    zzbsd().zzbsx().zzj("Param value can't be null", str);
                } else if ((!(zzl instanceof String) && !(zzl instanceof Character) && !(zzl instanceof CharSequence)) || !TextUtils.isEmpty(String.valueOf(zzl))) {
                    zzbrz().zza(bundle2, str, zzl);
                }
            }
        }
        return bundle2;
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    @TargetApi(14)
    public void zzbun() {
        if (getContext().getApplicationContext() instanceof Application) {
            Application application = (Application) getContext().getApplicationContext();
            if (this.alz == null) {
                this.alz = new zza();
            }
            application.unregisterActivityLifecycleCallbacks(this.alz);
            application.registerActivityLifecycleCallbacks(this.alz);
            zzbsd().zzbtc().log("Registered activity lifecycle callback");
        }
    }

    @WorkerThread
    public void zzbuo() {
        zzwu();
        zzyv();
        zzzg();
        if (this.ahD.zzbto()) {
            zzbrx().zzbuo();
            String zzbtl = zzbse().zzbtl();
            if (!TextUtils.isEmpty(zzbtl) && !zzbtl.equals(zzbrw().zzbso())) {
                Bundle bundle = new Bundle();
                bundle.putString("_po", zzbtl);
                zze("auto", "_ou", bundle);
            }
        }
    }

    @Nullable
    @WorkerThread
    public List<UserAttributeParcel> zzce(final boolean z) {
        com.google.android.gms.measurement.internal.zzp.zza zzbsx;
        String str;
        zzyv();
        zzzg();
        zzbsd().zzbtb().log("Fetching user attributes (FE)");
        if (Looper.myLooper() == Looper.getMainLooper()) {
            zzbsx = zzbsd().zzbsx();
            str = "getUserProperties called from main thread.";
        } else {
            final AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.ahD.zzbsc().zzm(new Runnable() {
                    public void run() {
                        zzac.this.zzbrx().zza(atomicReference, z);
                    }
                });
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e) {
                    zzbsd().zzbsx().zzj("Interrupted waiting for get user properties", e);
                }
            }
            List<UserAttributeParcel> list = (List) atomicReference.get();
            if (list != null) {
                return list;
            }
            zzbsx = zzbsd().zzbsx();
            str = "Timed out waiting for get user properties";
        }
        zzbsx.log(str);
        return null;
    }

    public void zzd(String str, String str2, Bundle bundle, long j) {
        zzyv();
        zza(str, str2, j, bundle, false, true, true, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0047, code lost:
        if (r6 != null) goto L_0x004b;
     */
    public void zzd(String str, String str2, Object obj) {
        Object obj2;
        zzab.zzhr(str);
        long currentTimeMillis = zzyw().currentTimeMillis();
        int zzmn = zzbrz().zzmn(str2);
        if (zzmn == 0) {
            if (obj != null) {
                zzmn = zzbrz().zzm(str2, obj);
                if (zzmn == 0) {
                    obj2 = zzbrz().zzn(str2, obj);
                }
            } else {
                obj2 = null;
            }
            zza(str, str2, currentTimeMillis, obj2);
            return;
        }
        this.ahD.zzbrz().zze(zzmn, "_ev", zzbrz().zza(str2, zzbsf().zzbqo(), true));
    }

    public void zze(String str, String str2, Bundle bundle) {
        zzyv();
        zza(str, str2, bundle, true, this.alA == null || zzal.zzmt(str2), false, null);
    }

    @WorkerThread
    public void zzg(Class<?> cls) {
        try {
            cls.getDeclaredMethod("initialize", new Class[]{Context.class}).invoke(null, new Object[]{getContext()});
        } catch (Exception e) {
            zzbsd().zzbsx().zzj("Failed to invoke Tag Manager's initialize() method", e);
        }
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
