package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzd.zzb;
import com.google.android.gms.common.internal.zzd.zzc;
import com.google.android.gms.common.util.zze;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class zzad extends zzaa {
    /* access modifiers changed from: private */
    public final zza alO;
    /* access modifiers changed from: private */
    public zzm alP;
    private Boolean alQ;
    private final zzf alR;
    private final zzah alS;
    private final List<Runnable> alT = new ArrayList();
    private final zzf alU;

    protected class zza implements ServiceConnection, zzb, zzc {
        /* access modifiers changed from: private */
        public volatile boolean alX;
        private volatile zzo alY;

        protected zza() {
        }

        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0022 */
        @MainThread
        public void onConnected(@Nullable Bundle bundle) {
            zzab.zzhi("MeasurementServiceConnection.onConnected");
            synchronized (this) {
                final zzm zzm = (zzm) this.alY.zzasa();
                this.alY = null;
                zzad.this.zzbsc().zzm(new Runnable() {
                    public void run() {
                        synchronized (zza.this) {
                            zza.this.alX = false;
                            if (!zzad.this.isConnected()) {
                                zzad.this.zzbsd().zzbtb().log("Connected to remote service");
                                zzad.this.zza(zzm);
                            }
                        }
                    }
                });
                this.alY = null;
                this.alX = false;
            }
        }

        @MainThread
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            zzab.zzhi("MeasurementServiceConnection.onConnectionFailed");
            zzp zzbtp = zzad.this.ahD.zzbtp();
            if (zzbtp != null) {
                zzbtp.zzbsx().zzj("Service connection failed", connectionResult);
            }
            synchronized (this) {
                this.alX = false;
                this.alY = null;
            }
        }

        @MainThread
        public void onConnectionSuspended(int i) {
            zzab.zzhi("MeasurementServiceConnection.onConnectionSuspended");
            zzad.this.zzbsd().zzbtb().log("Service connection suspended");
            zzad.this.zzbsc().zzm(new Runnable() {
                public void run() {
                    zzad.this.onServiceDisconnected(new ComponentName(zzad.this.getContext(), "com.google.android.gms.measurement.AppMeasurementService"));
                }
            });
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(5:13|14|15|21|22) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0051 */
        @MainThread
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            final zzm zzm;
            zzab.zzhi("MeasurementServiceConnection.onServiceConnected");
            synchronized (this) {
                if (iBinder == null) {
                    this.alX = false;
                    zzad.this.zzbsd().zzbsv().log("Service connected with null binder");
                    return;
                }
                try {
                    String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                    if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                        zzm = com.google.android.gms.measurement.internal.zzm.zza.zzjf(iBinder);
                        zzad.this.zzbsd().zzbtc().log("Bound to IMeasurementService interface");
                        zzad.this.zzbsd().zzbsv().log("Service connect failed to get IMeasurementService");
                    } else {
                        zzad.this.zzbsd().zzbsv().zzj("Got binder with a wrong descriptor", interfaceDescriptor);
                        zzm = null;
                    }
                } catch (RemoteException unused) {
                    zzm = null;
                }
                if (zzm == null) {
                    this.alX = false;
                    try {
                        com.google.android.gms.common.stats.zzb.zzaux().zza(zzad.this.getContext(), (ServiceConnection) zzad.this.alO);
                    } catch (IllegalArgumentException unused2) {
                    }
                } else {
                    zzad.this.zzbsc().zzm(new Runnable() {
                        public void run() {
                            synchronized (zza.this) {
                                zza.this.alX = false;
                                if (!zzad.this.isConnected()) {
                                    zzad.this.zzbsd().zzbtc().log("Connected to service");
                                    zzad.this.zza(zzm);
                                }
                            }
                        }
                    });
                }
            }
        }

        @MainThread
        public void onServiceDisconnected(final ComponentName componentName) {
            zzab.zzhi("MeasurementServiceConnection.onServiceDisconnected");
            zzad.this.zzbsd().zzbtb().log("Service disconnected");
            zzad.this.zzbsc().zzm(new Runnable() {
                public void run() {
                    zzad.this.onServiceDisconnected(componentName);
                }
            });
        }

        @WorkerThread
        public void zzbuw() {
            zzad.this.zzwu();
            Context context = zzad.this.getContext();
            synchronized (this) {
                if (this.alX) {
                    zzad.this.zzbsd().zzbtc().log("Connection attempt already in progress");
                } else if (this.alY != null) {
                    zzad.this.zzbsd().zzbtc().log("Already awaiting connection attempt");
                } else {
                    this.alY = new zzo(context, Looper.getMainLooper(), this, this);
                    zzad.this.zzbsd().zzbtc().log("Connecting to remote service");
                    this.alX = true;
                    this.alY.zzarx();
                }
            }
        }

        @WorkerThread
        public void zzy(Intent intent) {
            zzad.this.zzwu();
            Context context = zzad.this.getContext();
            com.google.android.gms.common.stats.zzb zzaux = com.google.android.gms.common.stats.zzb.zzaux();
            synchronized (this) {
                if (this.alX) {
                    zzad.this.zzbsd().zzbtc().log("Connection attempt already in progress");
                    return;
                }
                this.alX = true;
                zzaux.zza(context, intent, (ServiceConnection) zzad.this.alO, 129);
            }
        }
    }

    protected zzad(zzx zzx) {
        super(zzx);
        this.alS = new zzah(zzx.zzyw());
        this.alO = new zza();
        this.alR = new zzf(zzx) {
            public void run() {
                zzad.this.zzzu();
            }
        };
        this.alU = new zzf(zzx) {
            public void run() {
                zzad.this.zzbsd().zzbsx().log("Tasks have been queued for a long time");
            }
        };
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void onServiceDisconnected(ComponentName componentName) {
        zzwu();
        if (this.alP != null) {
            this.alP = null;
            zzbsd().zzbtc().zzj("Disconnected from device MeasurementService", componentName);
            zzbuu();
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void zza(zzm zzm) {
        zzwu();
        zzab.zzy(zzm);
        this.alP = zzm;
        zzzt();
        zzbuv();
    }

    private boolean zzbus() {
        List queryIntentServices = getContext().getPackageManager().queryIntentServices(new Intent().setClassName(getContext(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
        return queryIntentServices != null && queryIntentServices.size() > 0;
    }

    @WorkerThread
    private void zzbuu() {
        zzwu();
        zzaai();
    }

    @WorkerThread
    private void zzbuv() {
        zzwu();
        zzbsd().zzbtc().zzj("Processing queued up service tasks", Integer.valueOf(this.alT.size()));
        for (Runnable zzm : this.alT) {
            zzbsc().zzm(zzm);
        }
        this.alT.clear();
        this.alU.cancel();
    }

    @WorkerThread
    private void zzo(Runnable runnable) throws IllegalStateException {
        zzwu();
        if (isConnected()) {
            runnable.run();
        } else if (((long) this.alT.size()) >= zzbsf().zzbrh()) {
            zzbsd().zzbsv().log("Discarding data. Max runnable queue size reached");
        } else {
            this.alT.add(runnable);
            if (!this.ahD.zzbty()) {
                this.alU.zzv(60000);
            }
            zzaai();
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void zzzt() {
        zzwu();
        this.alS.start();
        if (!this.ahD.zzbty()) {
            this.alR.zzv(zzbsf().zzabx());
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public void zzzu() {
        zzwu();
        if (isConnected()) {
            zzbsd().zzbtc().log("Inactivity, disconnecting from AppMeasurementService");
            disconnect();
        }
    }

    @WorkerThread
    public void disconnect() {
        zzwu();
        zzzg();
        try {
            com.google.android.gms.common.stats.zzb.zzaux().zza(getContext(), (ServiceConnection) this.alO);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.alP = null;
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    @WorkerThread
    public boolean isConnected() {
        zzwu();
        zzzg();
        return this.alP != null;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void zza(final UserAttributeParcel userAttributeParcel) {
        zzwu();
        zzzg();
        zzo(new Runnable() {
            public void run() {
                zzm zzc = zzad.this.alP;
                if (zzc == null) {
                    zzad.this.zzbsd().zzbsv().log("Discarding data. Failed to set user attribute");
                    return;
                }
                try {
                    zzc.zza(userAttributeParcel, zzad.this.zzbrv().zzlv(zzad.this.zzbsd().zzbtd()));
                    zzad.this.zzzt();
                } catch (RemoteException e) {
                    zzad.this.zzbsd().zzbsv().zzj("Failed to send attribute to AppMeasurementService", e);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void zza(final AtomicReference<List<UserAttributeParcel>> atomicReference, final boolean z) {
        zzwu();
        zzzg();
        zzo(new Runnable() {
            public void run() {
                AtomicReference atomicReference;
                synchronized (atomicReference) {
                    try {
                        zzm zzc = zzad.this.alP;
                        if (zzc == null) {
                            zzad.this.zzbsd().zzbsv().log("Failed to get user properties");
                            atomicReference.notify();
                            return;
                        }
                        atomicReference.set(zzc.zza(zzad.this.zzbrv().zzlv(null), z));
                        zzad.this.zzzt();
                        atomicReference = atomicReference;
                        atomicReference.notify();
                    } catch (RemoteException e) {
                        try {
                            zzad.this.zzbsd().zzbsv().zzj("Failed to get user properties", e);
                            atomicReference = atomicReference;
                        } catch (Throwable th) {
                            atomicReference.notify();
                            throw th;
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzaai() {
        zzwu();
        zzzg();
        if (!isConnected()) {
            if (this.alQ == null) {
                this.alQ = zzbse().zzbtj();
                if (this.alQ == null) {
                    zzbsd().zzbtc().log("State of service unknown");
                    this.alQ = Boolean.valueOf(zzbut());
                    zzbse().zzcb(this.alQ.booleanValue());
                }
            }
            if (this.alQ.booleanValue()) {
                zzbsd().zzbtc().log("Using measurement service");
                this.alO.zzbuw();
            } else if (!this.ahD.zzbty() && zzbus()) {
                zzbsd().zzbtc().log("Using local app measurement service");
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                intent.setComponent(new ComponentName(getContext(), "com.google.android.gms.measurement.AppMeasurementService"));
                this.alO.zzy(intent);
            } else if (zzbsf().zzabd()) {
                zzbsd().zzbtc().log("Using direct local measurement implementation");
                zza((zzm) new zzy(this.ahD, true));
            } else {
                zzbsd().zzbsv().log("Not in main process. Unable to use local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void zzbuo() {
        zzwu();
        zzzg();
        zzo(new Runnable() {
            public void run() {
                zzm zzc = zzad.this.alP;
                if (zzc == null) {
                    zzad.this.zzbsd().zzbsv().log("Discarding data. Failed to send app launch");
                    return;
                }
                try {
                    zzc.zza(zzad.this.zzbrv().zzlv(zzad.this.zzbsd().zzbtd()));
                    zzad.this.zzzt();
                } catch (RemoteException e) {
                    zzad.this.zzbsd().zzbsv().zzj("Failed to send app launch to AppMeasurementService", e);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void zzbur() {
        zzwu();
        zzzg();
        zzo(new Runnable() {
            public void run() {
                zzm zzc = zzad.this.alP;
                if (zzc == null) {
                    zzad.this.zzbsd().zzbsv().log("Failed to send measurementEnabled to service");
                    return;
                }
                try {
                    zzc.zzb(zzad.this.zzbrv().zzlv(zzad.this.zzbsd().zzbtd()));
                    zzad.this.zzzt();
                } catch (RemoteException e) {
                    zzad.this.zzbsd().zzbsv().zzj("Failed to send measurementEnabled to AppMeasurementService", e);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public boolean zzbut() {
        com.google.android.gms.measurement.internal.zzp.zza zzbsx;
        String str;
        com.google.android.gms.measurement.internal.zzp.zza zzbsx2;
        String str2;
        zzwu();
        zzzg();
        if (zzbsf().zzabc()) {
            return true;
        }
        zzbsd().zzbtc().log("Checking service availability");
        int isGooglePlayServicesAvailable = com.google.android.gms.common.zzc.zzang().isGooglePlayServicesAvailable(getContext());
        if (isGooglePlayServicesAvailable != 9) {
            if (isGooglePlayServicesAvailable != 18) {
                switch (isGooglePlayServicesAvailable) {
                    case 0:
                        zzbsx2 = zzbsd().zzbtc();
                        str2 = "Service available";
                        break;
                    case 1:
                        zzbsx = zzbsd().zzbtc();
                        str = "Service missing";
                        break;
                    case 2:
                        zzbsx2 = zzbsd().zzbtb();
                        str2 = "Service container out of date";
                        break;
                    case 3:
                        zzbsx = zzbsd().zzbsx();
                        str = "Service disabled";
                        break;
                    default:
                        return false;
                }
            } else {
                zzbsx2 = zzbsd().zzbsx();
                str2 = "Service updating";
            }
            zzbsx2.log(str2);
            return true;
        }
        zzbsx = zzbsd().zzbsx();
        str = "Service invalid";
        zzbsx.log(str);
        return false;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void zzc(final EventParcel eventParcel, final String str) {
        zzab.zzy(eventParcel);
        zzwu();
        zzzg();
        zzo(new Runnable() {
            public void run() {
                zzm zzc = zzad.this.alP;
                if (zzc == null) {
                    zzad.this.zzbsd().zzbsv().log("Discarding data. Failed to send event to service");
                    return;
                }
                try {
                    if (TextUtils.isEmpty(str)) {
                        zzc.zza(eventParcel, zzad.this.zzbrv().zzlv(zzad.this.zzbsd().zzbtd()));
                    } else {
                        zzc.zza(eventParcel, str, zzad.this.zzbsd().zzbtd());
                    }
                    zzad.this.zzzt();
                } catch (RemoteException e) {
                    zzad.this.zzbsd().zzbsv().zzj("Failed to send event to AppMeasurementService", e);
                }
            }
        });
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
