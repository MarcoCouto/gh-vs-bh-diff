package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface zzm extends IInterface {

    public static abstract class zza extends Binder implements zzm {

        /* renamed from: com.google.android.gms.measurement.internal.zzm$zza$zza reason: collision with other inner class name */
        private static class C0091zza implements zzm {
            private IBinder zzahn;

            C0091zza(IBinder iBinder) {
                this.zzahn = iBinder;
            }

            public IBinder asBinder() {
                return this.zzahn;
            }

            public List<UserAttributeParcel> zza(AppMetadata appMetadata, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(z ? 1 : 0);
                    this.zzahn.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(UserAttributeParcel.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(AppMetadata appMetadata) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.zzahn.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(EventParcel eventParcel, AppMetadata appMetadata) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (eventParcel != null) {
                        obtain.writeInt(1);
                        eventParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.zzahn.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(EventParcel eventParcel, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (eventParcel != null) {
                        obtain.writeInt(1);
                        eventParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.zzahn.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (userAttributeParcel != null) {
                        obtain.writeInt(1);
                        userAttributeParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.zzahn.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public byte[] zza(EventParcel eventParcel, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (eventParcel != null) {
                        obtain.writeInt(1);
                        eventParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.zzahn.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createByteArray();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zzb(AppMetadata appMetadata) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (appMetadata != null) {
                        obtain.writeInt(1);
                        appMetadata.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.zzahn.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public zza() {
            attachInterface(this, "com.google.android.gms.measurement.internal.IMeasurementService");
        }

        public static zzm zzjf(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof zzm)) ? new C0091zza(iBinder) : (zzm) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* JADX WARNING: type inference failed for: r2v0 */
        /* JADX WARNING: type inference failed for: r2v1, types: [com.google.android.gms.measurement.internal.EventParcel] */
        /* JADX WARNING: type inference failed for: r2v3, types: [com.google.android.gms.measurement.internal.EventParcel] */
        /* JADX WARNING: type inference failed for: r2v4, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v6, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v7, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v9, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v10, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v12, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v13, types: [com.google.android.gms.measurement.internal.EventParcel] */
        /* JADX WARNING: type inference failed for: r2v15, types: [com.google.android.gms.measurement.internal.EventParcel] */
        /* JADX WARNING: type inference failed for: r2v16, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v18, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v19, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v21, types: [com.google.android.gms.measurement.internal.AppMetadata] */
        /* JADX WARNING: type inference failed for: r2v22 */
        /* JADX WARNING: type inference failed for: r2v23 */
        /* JADX WARNING: type inference failed for: r2v24 */
        /* JADX WARNING: type inference failed for: r2v25 */
        /* JADX WARNING: type inference failed for: r2v26 */
        /* JADX WARNING: type inference failed for: r2v27 */
        /* JADX WARNING: type inference failed for: r2v28 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.measurement.internal.AppMetadata, com.google.android.gms.measurement.internal.EventParcel]
  uses: [com.google.android.gms.measurement.internal.EventParcel, com.google.android.gms.measurement.internal.AppMetadata]
  mth insns count: 115
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 8 */
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            ? r2 = 0;
            if (i == 9) {
                parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                if (parcel.readInt() != 0) {
                    r2 = (EventParcel) EventParcel.CREATOR.createFromParcel(parcel);
                }
                byte[] zza = zza((EventParcel) r2, parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(zza);
                return true;
            } else if (i != 1598968902) {
                switch (i) {
                    case 1:
                        parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                        EventParcel eventParcel = parcel.readInt() != 0 ? (EventParcel) EventParcel.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r2 = (AppMetadata) AppMetadata.CREATOR.createFromParcel(parcel);
                        }
                        zza(eventParcel, (AppMetadata) r2);
                        parcel2.writeNoException();
                        return true;
                    case 2:
                        parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                        UserAttributeParcel userAttributeParcel = parcel.readInt() != 0 ? (UserAttributeParcel) UserAttributeParcel.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r2 = (AppMetadata) AppMetadata.CREATOR.createFromParcel(parcel);
                        }
                        zza(userAttributeParcel, (AppMetadata) r2);
                        parcel2.writeNoException();
                        return true;
                    default:
                        switch (i) {
                            case 4:
                                parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                                if (parcel.readInt() != 0) {
                                    r2 = (AppMetadata) AppMetadata.CREATOR.createFromParcel(parcel);
                                }
                                zza(r2);
                                parcel2.writeNoException();
                                return true;
                            case 5:
                                parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                                if (parcel.readInt() != 0) {
                                    r2 = (EventParcel) EventParcel.CREATOR.createFromParcel(parcel);
                                }
                                zza(r2, parcel.readString(), parcel.readString());
                                parcel2.writeNoException();
                                return true;
                            case 6:
                                parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                                if (parcel.readInt() != 0) {
                                    r2 = (AppMetadata) AppMetadata.CREATOR.createFromParcel(parcel);
                                }
                                zzb(r2);
                                parcel2.writeNoException();
                                return true;
                            case 7:
                                parcel.enforceInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                                if (parcel.readInt() != 0) {
                                    r2 = (AppMetadata) AppMetadata.CREATOR.createFromParcel(parcel);
                                }
                                List zza2 = zza((AppMetadata) r2, parcel.readInt() != 0);
                                parcel2.writeNoException();
                                parcel2.writeTypedList(zza2);
                                return true;
                            default:
                                return super.onTransact(i, parcel, parcel2, i2);
                        }
                }
            } else {
                parcel2.writeString("com.google.android.gms.measurement.internal.IMeasurementService");
                return true;
            }
        }
    }

    List<UserAttributeParcel> zza(AppMetadata appMetadata, boolean z) throws RemoteException;

    void zza(AppMetadata appMetadata) throws RemoteException;

    void zza(EventParcel eventParcel, AppMetadata appMetadata) throws RemoteException;

    void zza(EventParcel eventParcel, String str, String str2) throws RemoteException;

    void zza(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) throws RemoteException;

    byte[] zza(EventParcel eventParcel, String str) throws RemoteException;

    void zzb(AppMetadata appMetadata) throws RemoteException;
}
