package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class zzq extends zzaa {

    @WorkerThread
    interface zza {
        void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
    }

    @WorkerThread
    private static class zzb implements Runnable {
        private final String aM;
        private final zza ajD;
        private final Throwable ajE;
        private final byte[] ajF;
        private final Map<String, List<String>> ajG;
        private final int zzblv;

        private zzb(String str, zza zza, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
            zzab.zzy(zza);
            this.ajD = zza;
            this.zzblv = i;
            this.ajE = th;
            this.ajF = bArr;
            this.aM = str;
            this.ajG = map;
        }

        public void run() {
            this.ajD.zza(this.aM, this.zzblv, this.ajE, this.ajF, this.ajG);
        }
    }

    @WorkerThread
    private class zzc implements Runnable {
        private final String aM;
        private final byte[] ajH;
        private final zza ajI;
        private final Map<String, String> ajJ;
        private final URL zzbij;

        public zzc(String str, URL url, byte[] bArr, Map<String, String> map, zza zza) {
            zzab.zzhr(str);
            zzab.zzy(url);
            zzab.zzy(zza);
            this.zzbij = url;
            this.ajH = bArr;
            this.ajI = zza;
            this.aM = str;
            this.ajJ = map;
        }

        /* JADX WARNING: Removed duplicated region for block: B:42:0x00cf A[SYNTHETIC, Splitter:B:42:0x00cf] */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x00e5  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x010b A[SYNTHETIC, Splitter:B:55:0x010b] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x0121  */
        public void run() {
            zzb zzb;
            zzw zzw;
            Map map;
            Throwable th;
            int i;
            HttpURLConnection httpURLConnection;
            Map map2;
            zzq.this.zzbrs();
            OutputStream outputStream = null;
            try {
                zzq.this.zzet(this.aM);
                httpURLConnection = zzq.this.zzc(this.zzbij);
                try {
                    if (this.ajJ != null) {
                        for (Entry entry : this.ajJ.entrySet()) {
                            httpURLConnection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
                        }
                    }
                    if (this.ajH != null) {
                        byte[] zzj = zzq.this.zzbrz().zzj(this.ajH);
                        zzq.this.zzbsd().zzbtc().zzj("Uploading data. size", Integer.valueOf(zzj.length));
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.addRequestProperty("Content-Encoding", "gzip");
                        httpURLConnection.setFixedLengthStreamingMode(zzj.length);
                        httpURLConnection.connect();
                        OutputStream outputStream2 = httpURLConnection.getOutputStream();
                        try {
                            outputStream2.write(zzj);
                            outputStream2.close();
                        } catch (IOException e) {
                            map = null;
                            i = 0;
                            th = e;
                            outputStream = outputStream2;
                        } catch (Throwable th2) {
                            th = th2;
                            map = null;
                            i = 0;
                            outputStream = outputStream2;
                            if (outputStream != null) {
                            }
                            if (httpURLConnection != null) {
                            }
                            zzq.this.zzrp();
                            zzw zzbsc = zzq.this.zzbsc();
                            zzb zzb2 = new zzb(this.aM, this.ajI, i, null, null, map);
                            zzbsc.zzm(zzb2);
                            throw th;
                        }
                    }
                    i = httpURLConnection.getResponseCode();
                } catch (IOException e2) {
                    e = e2;
                    map2 = null;
                    i = 0;
                    th = e;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzq.this.zzrp();
                    zzw = zzq.this.zzbsc();
                    zzb = new zzb(this.aM, this.ajI, i, th, null, map);
                    zzw.zzm(zzb);
                } catch (Throwable th3) {
                    th = th3;
                    map = null;
                    i = 0;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzq.this.zzrp();
                    zzw zzbsc2 = zzq.this.zzbsc();
                    zzb zzb22 = new zzb(this.aM, this.ajI, i, null, null, map);
                    zzbsc2.zzm(zzb22);
                    throw th;
                }
                try {
                    map = httpURLConnection.getHeaderFields();
                    try {
                        byte[] zza = zzq.this.zzc(httpURLConnection);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        zzq.this.zzrp();
                        zzw = zzq.this.zzbsc();
                        zzb = new zzb(this.aM, this.ajI, i, null, zza, map);
                    } catch (IOException e3) {
                        e = e3;
                        th = e;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        zzq.this.zzrp();
                        zzw = zzq.this.zzbsc();
                        zzb = new zzb(this.aM, this.ajI, i, th, null, map);
                        zzw.zzm(zzb);
                    } catch (Throwable th4) {
                        th = th4;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        zzq.this.zzrp();
                        zzw zzbsc22 = zzq.this.zzbsc();
                        zzb zzb222 = new zzb(this.aM, this.ajI, i, null, null, map);
                        zzbsc22.zzm(zzb222);
                        throw th;
                    }
                } catch (IOException e4) {
                    e = e4;
                    map = null;
                    th = e;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzq.this.zzrp();
                    zzw = zzq.this.zzbsc();
                    zzb = new zzb(this.aM, this.ajI, i, th, null, map);
                    zzw.zzm(zzb);
                } catch (Throwable th5) {
                    th = th5;
                    map = null;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzq.this.zzrp();
                    zzw zzbsc222 = zzq.this.zzbsc();
                    zzb zzb2222 = new zzb(this.aM, this.ajI, i, null, null, map);
                    zzbsc222.zzm(zzb2222);
                    throw th;
                }
            } catch (IOException e5) {
                e = e5;
                httpURLConnection = null;
                map2 = null;
                i = 0;
                th = e;
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e6) {
                        zzq.this.zzbsd().zzbsv().zzj("Error closing HTTP compressed POST connection output stream", e6);
                    }
                }
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                zzq.this.zzrp();
                zzw = zzq.this.zzbsc();
                zzb = new zzb(this.aM, this.ajI, i, th, null, map);
                zzw.zzm(zzb);
            } catch (Throwable th6) {
                th = th6;
                httpURLConnection = null;
                map = null;
                i = 0;
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e7) {
                        zzq.this.zzbsd().zzbsv().zzj("Error closing HTTP compressed POST connection output stream", e7);
                    }
                }
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                zzq.this.zzrp();
                zzw zzbsc2222 = zzq.this.zzbsc();
                zzb zzb22222 = new zzb(this.aM, this.ajI, i, null, null, map);
                zzbsc2222.zzm(zzb22222);
                throw th;
            }
            zzw.zzm(zzb);
        }
    }

    public zzq(zzx zzx) {
        super(zzx);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b  */
    @WorkerThread
    public byte[] zzc(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream;
        Throwable th;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            inputStream = httpURLConnection.getInputStream();
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                if (inputStream != null) {
                    inputStream.close();
                }
                return byteArray;
            } catch (Throwable th2) {
                th = th2;
                if (inputStream != null) {
                    inputStream.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            if (inputStream != null) {
            }
            throw th;
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    @WorkerThread
    public void zza(String str, URL url, Map<String, String> map, zza zza2) {
        zzwu();
        zzzg();
        zzab.zzy(url);
        zzab.zzy(zza2);
        zzw zzbsc = zzbsc();
        zzc zzc2 = new zzc(str, url, null, map, zza2);
        zzbsc.zzn(zzc2);
    }

    @WorkerThread
    public void zza(String str, URL url, byte[] bArr, Map<String, String> map, zza zza2) {
        zzwu();
        zzzg();
        zzab.zzy(url);
        zzab.zzy(bArr);
        zzab.zzy(zza2);
        zzw zzbsc = zzbsc();
        zzc zzc2 = new zzc(str, url, bArr, map, zza2);
        zzbsc.zzn(zzc2);
    }

    public boolean zzadj() {
        NetworkInfo networkInfo;
        zzzg();
        try {
            networkInfo = ((ConnectivityManager) getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException unused) {
            networkInfo = null;
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public HttpURLConnection zzc(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (!(openConnection instanceof HttpURLConnection)) {
            throw new IOException("Failed to obtain HTTP connection");
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setConnectTimeout((int) zzbsf().zzbrb());
        httpURLConnection.setReadTimeout((int) zzbsf().zzbrc());
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setDoInput(true);
        return httpURLConnection;
    }

    /* access modifiers changed from: protected */
    public void zzet(String str) {
    }

    /* access modifiers changed from: protected */
    public void zzrp() {
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
