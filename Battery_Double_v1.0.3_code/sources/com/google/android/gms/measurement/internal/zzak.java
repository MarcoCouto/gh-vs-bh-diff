package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzab;

class zzak {
    final long amx;
    final String mName;
    final String zzcjf;
    final Object zzcnn;

    zzak(String str, String str2, long j, Object obj) {
        zzab.zzhr(str);
        zzab.zzhr(str2);
        zzab.zzy(obj);
        this.zzcjf = str;
        this.mName = str2;
        this.amx = j;
        this.zzcnn = obj;
    }
}
