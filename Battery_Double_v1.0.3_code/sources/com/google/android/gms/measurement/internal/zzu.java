package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.MainThread;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzvw;
import com.google.android.gms.measurement.internal.zzp.zza;

public final class zzu {
    static final Object zzamr = new Object();
    static zzvw zzcrw;
    static Boolean zzcrx;

    public static boolean zzav(Context context) {
        zzab.zzy(context);
        if (zzcrx != null) {
            return zzcrx.booleanValue();
        }
        boolean zzb = zzal.zzb(context, "com.google.android.gms.measurement.AppMeasurementReceiver", false);
        zzcrx = Boolean.valueOf(zzb);
        return zzb;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r1.zzbsx().log("AppMeasurementService at risk of not starting. For more reliable app measurements, add the WAKE_LOCK permission to your manifest.");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0075 */
    @MainThread
    public void onReceive(Context context, Intent intent) {
        zza zzbtc;
        String str;
        zzx zzdo = zzx.zzdo(context);
        zzp zzbsd = zzdo.zzbsd();
        if (intent == null) {
            zzbsd.zzbsx().log("AppMeasurementReceiver called with null intent");
            return;
        }
        String action = intent.getAction();
        if (zzdo.zzbsf().zzabc()) {
            zzbtc = zzbsd.zzbtc();
            str = "Device AppMeasurementReceiver got";
        } else {
            zzbtc = zzbsd.zzbtc();
            str = "Local AppMeasurementReceiver got";
        }
        zzbtc.zzj(str, action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            boolean zzaw = zzae.zzaw(context);
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            synchronized (zzamr) {
                context.startService(className);
                if (zzaw) {
                    if (zzcrw == null) {
                        zzcrw = new zzvw(context, 1, "AppMeasurement WakeLock");
                        zzcrw.setReferenceCounted(false);
                    }
                    zzcrw.acquire(1000);
                }
            }
        }
    }
}
