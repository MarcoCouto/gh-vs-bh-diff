package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.measurement.AppMeasurement;

public class zzp extends zzaa {
    private final long ahR = zzbsf().zzbpz();
    private final char ajp;
    private final zza ajq;
    private final zza ajr;
    private final zza ajs;
    private final zza ajt;
    private final zza aju;
    private final zza ajv;
    private final zza ajw;
    private final zza ajx;
    private final zza ajy;
    private final String zc = zzbsf().zzbql();

    public class zza {
        private final boolean ajB;
        private final boolean ajC;
        private final int mPriority;

        zza(int i, boolean z, boolean z2) {
            this.mPriority = i;
            this.ajB = z;
            this.ajC = z2;
        }

        public void log(String str) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, null, null, null);
        }

        public void zzd(String str, Object obj, Object obj2, Object obj3) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, obj, obj2, obj3);
        }

        public void zze(String str, Object obj, Object obj2) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, obj, obj2, null);
        }

        public void zzj(String str, Object obj) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, obj, null, null);
        }
    }

    zzp(zzx zzx) {
        super(zzx);
        char c = zzbsf().zzabd() ? zzbsf().zzabc() ? 'P' : 'C' : zzbsf().zzabc() ? 'p' : 'c';
        this.ajp = c;
        this.ajq = new zza(6, false, false);
        this.ajr = new zza(6, true, false);
        this.ajs = new zza(6, false, true);
        this.ajt = new zza(5, false, false);
        this.aju = new zza(5, true, false);
        this.ajv = new zza(5, false, true);
        this.ajw = new zza(4, false, false);
        this.ajx = new zza(3, false, false);
        this.ajy = new zza(2, false, false);
    }

    static String zza(boolean z, String str, Object obj, Object obj2, Object obj3) {
        if (str == null) {
            str = "";
        }
        String zzc = zzc(z, obj);
        String zzc2 = zzc(z, obj2);
        String zzc3 = zzc(z, obj3);
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(zzc)) {
            sb.append(str2);
            sb.append(zzc);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(zzc2)) {
            sb.append(str2);
            sb.append(zzc2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(zzc3)) {
            sb.append(str2);
            sb.append(zzc3);
        }
        return sb.toString();
    }

    static String zzc(boolean z, Object obj) {
        StackTraceElement stackTraceElement;
        if (obj == null) {
            return "";
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf((long) ((Integer) obj).intValue());
        }
        int i = 0;
        if (obj instanceof Long) {
            if (!z) {
                return String.valueOf(obj);
            }
            Long l = (Long) obj;
            if (Math.abs(l.longValue()) < 100) {
                return String.valueOf(obj);
            }
            String str = String.valueOf(obj).charAt(0) == '-' ? "-" : "";
            String valueOf = String.valueOf(Math.abs(l.longValue()));
            long round = Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1)));
            long round2 = Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(43 + String.valueOf(str).length() + String.valueOf(str).length());
            sb.append(str);
            sb.append(round);
            sb.append("...");
            sb.append(str);
            sb.append(round2);
            return sb.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (!(obj instanceof Throwable)) {
                return z ? "-" : String.valueOf(obj);
            }
            Throwable th = (Throwable) obj;
            StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
            String zzlw = zzlw(AppMeasurement.class.getCanonicalName());
            String zzlw2 = zzlw(zzx.class.getCanonicalName());
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace.length;
            while (true) {
                if (i >= length) {
                    break;
                }
                stackTraceElement = stackTrace[i];
                if (!stackTraceElement.isNativeMethod()) {
                    String className = stackTraceElement.getClassName();
                    if (className == null) {
                        continue;
                    } else {
                        String zzlw3 = zzlw(className);
                        if (zzlw3.equals(zzlw) || zzlw3.equals(zzlw2)) {
                            sb2.append(": ");
                            sb2.append(stackTraceElement);
                        }
                    }
                }
                i++;
            }
            sb2.append(": ");
            sb2.append(stackTraceElement);
            return sb2.toString();
        }
    }

    private static String zzlw(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf == -1 ? str : str.substring(0, lastIndexOf);
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    /* access modifiers changed from: protected */
    public void zza(int i, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && zzaz(i)) {
            zzo(i, zza(false, str, obj, obj2, obj3));
        }
        if (!z2 && i >= 5) {
            zzb(i, str, obj, obj2, obj3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzaz(int i) {
        return Log.isLoggable(this.zc, i);
    }

    public void zzb(int i, String str, Object obj, Object obj2, Object obj3) {
        zzab.zzy(str);
        zzw zzbtq = this.ahD.zzbtq();
        if (zzbtq == null) {
            zzo(6, "Scheduler not set. Not logging error/warn.");
        } else if (!zzbtq.isInitialized()) {
            zzo(6, "Scheduler not initialized. Not logging error/warn.");
        } else if (zzbtq.zzbul()) {
            zzo(6, "Scheduler shutdown. Not logging error/warn.");
        } else {
            if (i < 0) {
                i = 0;
            }
            if (i >= "01VDIWEA?".length()) {
                i = "01VDIWEA?".length() - 1;
            }
            String valueOf = String.valueOf("1");
            char charAt = "01VDIWEA?".charAt(i);
            char c = this.ajp;
            long j = this.ahR;
            String valueOf2 = String.valueOf(zza(true, str, obj, obj2, obj3));
            StringBuilder sb = new StringBuilder(23 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
            sb.append(valueOf);
            sb.append(charAt);
            sb.append(c);
            sb.append(j);
            sb.append(":");
            sb.append(valueOf2);
            final String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = str.substring(0, 1024);
            }
            zzbtq.zzm(new Runnable() {
                public void run() {
                    zzt zzbse = zzp.this.ahD.zzbse();
                    if (!zzbse.isInitialized() || zzbse.zzbul()) {
                        zzp.this.zzo(6, "Persisted config not initialized . Not logging error/warn.");
                    } else {
                        zzbse.ajX.zzev(sb2);
                    }
                }
            });
        }
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    public zza zzbsv() {
        return this.ajq;
    }

    public zza zzbsw() {
        return this.ajr;
    }

    public zza zzbsx() {
        return this.ajt;
    }

    public zza zzbsy() {
        return this.aju;
    }

    public zza zzbsz() {
        return this.ajv;
    }

    public zza zzbta() {
        return this.ajw;
    }

    public zza zzbtb() {
        return this.ajx;
    }

    public zza zzbtc() {
        return this.ajy;
    }

    public String zzbtd() {
        Pair<String, Long> zzadv = zzbse().ajX.zzadv();
        if (zzadv == null || zzadv == zzt.ajW) {
            return null;
        }
        String valueOf = String.valueOf(String.valueOf(zzadv.second));
        String str = (String) zzadv.first;
        StringBuilder sb = new StringBuilder(1 + String.valueOf(valueOf).length() + String.valueOf(str).length());
        sb.append(valueOf);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void zzo(int i, String str) {
        Log.println(i, this.zc, str);
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
