package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzapo;
import com.google.android.gms.internal.zzug;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzuh.zzb;
import com.google.android.gms.internal.zzuh.zzc;
import com.google.android.gms.internal.zzuh.zzd;
import com.google.android.gms.internal.zzuh.zzg;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class zzx {
    private static volatile zzx akP;
    private final zzd akQ;
    private final zzt akR;
    private final zzp akS;
    private final zzw akT;
    private final zzaf akU;
    private final zzv akV;
    private final AppMeasurement akW;
    private final zzal akX;
    private final zze akY;
    private final zzq akZ;
    private final zzad ala;
    private final zzg alb;
    private final zzac alc;
    private final zzn ald;
    private final zzr ale;
    private final zzai alf;
    private final zzc alg;
    public final FirebaseAnalytics alh = new FirebaseAnalytics(this);
    private boolean ali;
    private Boolean alj;
    private FileLock alk;
    private FileChannel all;
    private List<Long> alm;
    private int aln;
    private int alo;
    private final Context mContext;
    private final zze zzaoc;
    private final boolean zzcwq;

    private class zza implements zzb {
        zzuh.zze alq;
        List<Long> alr;
        long als;
        List<zzb> zzalc;

        private zza() {
        }

        private long zza(zzb zzb) {
            return ((zzb.ano.longValue() / 1000) / 60) / 60;
        }

        /* access modifiers changed from: 0000 */
        public boolean isEmpty() {
            return this.zzalc == null || this.zzalc.isEmpty();
        }

        public boolean zza(long j, zzb zzb) {
            zzab.zzy(zzb);
            if (this.zzalc == null) {
                this.zzalc = new ArrayList();
            }
            if (this.alr == null) {
                this.alr = new ArrayList();
            }
            if (this.zzalc.size() > 0 && zza((zzb) this.zzalc.get(0)) != zza(zzb)) {
                return false;
            }
            long aM = this.als + ((long) zzb.aM());
            if (aM >= ((long) zzx.this.zzbsf().zzbri())) {
                return false;
            }
            this.als = aM;
            this.zzalc.add(zzb);
            this.alr.add(Long.valueOf(j));
            return this.zzalc.size() < zzx.this.zzbsf().zzbrj();
        }

        public void zzc(zzuh.zze zze) {
            zzab.zzy(zze);
            this.alq = zze;
        }
    }

    zzx(zzab zzab) {
        com.google.android.gms.measurement.internal.zzp.zza zza2;
        String str;
        zzab.zzy(zzab);
        this.mContext = zzab.mContext;
        this.zzaoc = zzab.zzl(this);
        this.akQ = zzab.zza(this);
        zzt zzb = zzab.zzb(this);
        zzb.initialize();
        this.akR = zzb;
        zzp zzc = zzab.zzc(this);
        zzc.initialize();
        this.akS = zzc;
        zzbsd().zzbta().zzj("App measurement is starting up, version", Long.valueOf(zzbsf().zzbpz()));
        zzbsd().zzbta().log("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        zzbsd().zzbtb().log("Debug logging enabled");
        zzbsd().zzbtb().zzj("AppMeasurement singleton hash", Integer.valueOf(System.identityHashCode(this)));
        this.akX = zzab.zzi(this);
        zzg zzn = zzab.zzn(this);
        zzn.initialize();
        this.alb = zzn;
        zzn zzo = zzab.zzo(this);
        zzo.initialize();
        this.ald = zzo;
        zze zzj = zzab.zzj(this);
        zzj.initialize();
        this.akY = zzj;
        zzc zzr = zzab.zzr(this);
        zzr.initialize();
        this.alg = zzr;
        zzq zzk = zzab.zzk(this);
        zzk.initialize();
        this.akZ = zzk;
        zzad zzm = zzab.zzm(this);
        zzm.initialize();
        this.ala = zzm;
        zzac zzh = zzab.zzh(this);
        zzh.initialize();
        this.alc = zzh;
        zzai zzq = zzab.zzq(this);
        zzq.initialize();
        this.alf = zzq;
        this.ale = zzab.zzp(this);
        this.akW = zzab.zzg(this);
        zzaf zze = zzab.zze(this);
        zze.initialize();
        this.akU = zze;
        zzv zzf = zzab.zzf(this);
        zzf.initialize();
        this.akV = zzf;
        zzw zzd = zzab.zzd(this);
        zzd.initialize();
        this.akT = zzd;
        if (this.aln != this.alo) {
            zzbsd().zzbsv().zze("Not all components initialized", Integer.valueOf(this.aln), Integer.valueOf(this.alo));
        }
        this.zzcwq = true;
        if (!this.akQ.zzabc() && !zzbty()) {
            if (!(this.mContext.getApplicationContext() instanceof Application)) {
                zza2 = zzbsd().zzbsx();
                str = "Application context is not an Application";
            } else if (VERSION.SDK_INT >= 14) {
                zzbru().zzbun();
            } else {
                zza2 = zzbsd().zzbtb();
                str = "Not tracking deep linking pre-ICS";
            }
            zza2.log(str);
        }
        this.akT.zzm(new Runnable() {
            public void run() {
                zzx.this.start();
            }
        });
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    @WorkerThread
    public void zza(int i, Throwable th, byte[] bArr) {
        zzwu();
        zzzg();
        boolean z = false;
        if (bArr == null) {
            bArr = new byte[0];
        }
        List<Long> list = this.alm;
        this.alm = null;
        if ((i == 200 || i == 204) && th == null) {
            zzbse().ajY.set(zzyw().currentTimeMillis());
            zzbse().ajZ.set(0);
            zzbue();
            zzbsd().zzbtc().zze("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
            zzbry().beginTransaction();
            try {
                for (Long longValue : list) {
                    zzbry().zzbh(longValue.longValue());
                }
                zzbry().setTransactionSuccessful();
                zzbry().endTransaction();
                if (zzbts().zzadj() && zzbud()) {
                    zzbuc();
                    return;
                }
            } catch (Throwable th2) {
                zzbry().endTransaction();
                throw th2;
            }
        } else {
            zzbsd().zzbtc().zze("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            zzbse().ajZ.set(zzyw().currentTimeMillis());
            if (i == 503 || i == 429) {
                z = true;
            }
            if (z) {
                zzbse().aka.set(zzyw().currentTimeMillis());
            }
        }
        zzbue();
    }

    private void zza(zzaa zzaa) {
        if (zzaa == null) {
            throw new IllegalStateException("Component not created");
        } else if (!zzaa.isInitialized()) {
            throw new IllegalStateException("Component not initialized");
        }
    }

    private void zza(zzz zzz) {
        if (zzz == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    private com.google.android.gms.internal.zzuh.zza[] zza(String str, zzg[] zzgArr, zzb[] zzbArr) {
        zzab.zzhr(str);
        return zzbrt().zza(str, zzbArr, zzgArr);
    }

    private void zzad(List<Long> list) {
        zzab.zzbo(!list.isEmpty());
        if (this.alm != null) {
            zzbsd().zzbsv().log("Set uploading progress before finishing the previous upload");
        } else {
            this.alm = new ArrayList(list);
        }
    }

    @WorkerThread
    private boolean zzbub() {
        zzwu();
        return this.alm != null;
    }

    private boolean zzbud() {
        zzwu();
        zzzg();
        return zzbry().zzbsl() || !TextUtils.isEmpty(zzbry().zzbsg());
    }

    @WorkerThread
    private void zzbue() {
        zzwu();
        zzzg();
        if (zzbui()) {
            if (zzbto() && zzbud()) {
                long zzbuf = zzbuf();
                if (zzbuf != 0) {
                    if (!zzbts().zzadj()) {
                        zzbtt().zzadg();
                        zzbtu().cancel();
                    }
                    long j = zzbse().aka.get();
                    long zzbrm = zzbsf().zzbrm();
                    if (!zzbrz().zzg(j, zzbrm)) {
                        zzbuf = Math.max(zzbuf, j + zzbrm);
                    }
                    zzbtt().unregister();
                    long currentTimeMillis = zzbuf - zzyw().currentTimeMillis();
                    if (currentTimeMillis <= 0) {
                        zzbtu().zzv(1);
                        return;
                    }
                    zzbsd().zzbtc().zzj("Upload scheduled in approximately ms", Long.valueOf(currentTimeMillis));
                    zzbtu().zzv(currentTimeMillis);
                    return;
                }
            }
            zzbtt().unregister();
            zzbtu().cancel();
        }
    }

    private long zzbuf() {
        long currentTimeMillis = zzyw().currentTimeMillis();
        long zzbrp = zzbsf().zzbrp();
        long zzbrn = zzbsf().zzbrn();
        long j = zzbse().ajY.get();
        long j2 = zzbse().ajZ.get();
        long max = Math.max(zzbry().zzbsj(), zzbry().zzbsk());
        if (max == 0) {
            return 0;
        }
        long abs = currentTimeMillis - Math.abs(max - currentTimeMillis);
        long abs2 = currentTimeMillis - Math.abs(j2 - currentTimeMillis);
        long max2 = Math.max(currentTimeMillis - Math.abs(j - currentTimeMillis), abs2);
        long j3 = abs + zzbrp;
        if (!zzbrz().zzg(max2, zzbrn)) {
            j3 = max2 + zzbrn;
        }
        if (abs2 == 0 || abs2 < abs) {
            return j3;
        }
        int i = 0;
        while (i < zzbsf().zzbrr()) {
            long zzbrq = j3 + (zzbsf().zzbrq() * ((long) (1 << i)));
            if (zzbrq > abs2) {
                return zzbrq;
            }
            i++;
            j3 = zzbrq;
        }
        return 0;
    }

    public static zzx zzdo(Context context) {
        zzab.zzy(context);
        zzab.zzy(context.getApplicationContext());
        if (akP == null) {
            synchronized (zzx.class) {
                if (akP == null) {
                    akP = new zzab(context).zzbum();
                }
            }
        }
        return akP;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    @WorkerThread
    private void zze(AppMetadata appMetadata) {
        boolean z;
        zzwu();
        zzzg();
        zzab.zzy(appMetadata);
        zzab.zzhr(appMetadata.packageName);
        zza zzln = zzbry().zzln(appMetadata.packageName);
        String zzly = zzbse().zzly(appMetadata.packageName);
        if (zzln == null) {
            zzln = new zza(this, appMetadata.packageName);
            zzln.zzky(zzbse().zzbtf());
            zzln.zzla(zzly);
        } else if (!zzly.equals(zzln.zzbpt())) {
            zzln.zzla(zzly);
            zzln.zzky(zzbse().zzbtf());
        } else {
            z = false;
            if (!TextUtils.isEmpty(appMetadata.aic) && !appMetadata.aic.equals(zzln.zzbps())) {
                zzln.zzkz(appMetadata.aic);
                z = true;
            }
            if (!TextUtils.isEmpty(appMetadata.aik) && !appMetadata.aik.equals(zzln.zzbpu())) {
                zzln.zzlb(appMetadata.aik);
                z = true;
            }
            if (!(appMetadata.aie == 0 || appMetadata.aie == zzln.zzbpz())) {
                zzln.zzax(appMetadata.aie);
                z = true;
            }
            if (!TextUtils.isEmpty(appMetadata.aav) && !appMetadata.aav.equals(zzln.zzxc())) {
                zzln.setAppVersion(appMetadata.aav);
                z = true;
            }
            if (appMetadata.aij != zzln.zzbpx()) {
                zzln.zzaw(appMetadata.aij);
                z = true;
            }
            if (!TextUtils.isEmpty(appMetadata.aid) && !appMetadata.aid.equals(zzln.zzbpy())) {
                zzln.zzlc(appMetadata.aid);
                z = true;
            }
            if (appMetadata.aif != zzln.zzbqa()) {
                zzln.zzay(appMetadata.aif);
                z = true;
            }
            if (appMetadata.aih != zzln.zzbqb()) {
                zzln.setMeasurementEnabled(appMetadata.aih);
                z = true;
            }
            if (!z) {
                zzbry().zza(zzln);
                return;
            }
            return;
        }
        z = true;
        zzln.zzkz(appMetadata.aic);
        z = true;
        zzln.zzlb(appMetadata.aik);
        z = true;
        zzln.zzax(appMetadata.aie);
        z = true;
        zzln.setAppVersion(appMetadata.aav);
        z = true;
        if (appMetadata.aij != zzln.zzbpx()) {
        }
        zzln.zzlc(appMetadata.aid);
        z = true;
        if (appMetadata.aif != zzln.zzbqa()) {
        }
        if (appMetadata.aih != zzln.zzbqb()) {
        }
        if (!z) {
        }
    }

    private boolean zzi(String str, long j) {
        boolean z;
        zzc[] zzcArr;
        zzc[] zzcArr2;
        zzbry().beginTransaction();
        try {
            zza zza2 = new zza();
            zzbry().zza(str, j, (zzb) zza2);
            int i = 0;
            if (!zza2.isEmpty()) {
                zzuh.zze zze = zza2.alq;
                zze.anv = new zzb[zza2.zzalc.size()];
                int i2 = 0;
                int i3 = 0;
                while (i2 < zza2.zzalc.size()) {
                    if (zzbsa().zzax(zza2.alq.zzck, ((zzb) zza2.zzalc.get(i2)).name)) {
                        zzbsd().zzbsx().zzj("Dropping blacklisted raw event", ((zzb) zza2.zzalc.get(i2)).name);
                        zzbrz().zze(11, "_ev", ((zzb) zza2.zzalc.get(i2)).name);
                    } else {
                        if (zzbsa().zzay(zza2.alq.zzck, ((zzb) zza2.zzalc.get(i2)).name)) {
                            if (((zzb) zza2.zzalc.get(i2)).ann == null) {
                                ((zzb) zza2.zzalc.get(i2)).ann = new zzc[i];
                            }
                            zzc[] zzcArr3 = ((zzb) zza2.zzalc.get(i2)).ann;
                            int length = zzcArr3.length;
                            int i4 = i;
                            while (true) {
                                if (i4 >= length) {
                                    z = false;
                                    break;
                                }
                                zzc zzc = zzcArr3[i4];
                                if ("_c".equals(zzc.name)) {
                                    zzc.anr = Long.valueOf(1);
                                    z = true;
                                    break;
                                }
                                i4++;
                            }
                            if (!z) {
                                zzbsd().zzbtc().zzj("Marking event as conversion", ((zzb) zza2.zzalc.get(i2)).name);
                                zzc[] zzcArr4 = (zzc[]) Arrays.copyOf(((zzb) zza2.zzalc.get(i2)).ann, ((zzb) zza2.zzalc.get(i2)).ann.length + 1);
                                zzc zzc2 = new zzc();
                                zzc2.name = "_c";
                                zzc2.anr = Long.valueOf(1);
                                zzcArr4[zzcArr4.length - 1] = zzc2;
                                ((zzb) zza2.zzalc.get(i2)).ann = zzcArr4;
                            }
                            boolean zzmj = zzal.zzmj(((zzb) zza2.zzalc.get(i2)).name);
                            if (zzmj && zzbry().zza(zzbtz(), zza2.alq.zzck, false, zzmj, false).air - ((long) zzbsf().zzlf(zza2.alq.zzck)) > 0) {
                                zzbsd().zzbsx().log("Too many conversions. Not logging as conversion.");
                                zzb zzb = (zzb) zza2.zzalc.get(i2);
                                boolean z2 = false;
                                zzc zzc3 = null;
                                for (zzc zzc4 : ((zzb) zza2.zzalc.get(i2)).ann) {
                                    if ("_c".equals(zzc4.name)) {
                                        zzc3 = zzc4;
                                    } else if ("_err".equals(zzc4.name)) {
                                        z2 = true;
                                    }
                                }
                                if (z2 && zzc3 != null) {
                                    zzc[] zzcArr5 = new zzc[(zzb.ann.length - 1)];
                                    int i5 = 0;
                                    for (zzc zzc5 : zzb.ann) {
                                        if (zzc5 != zzc3) {
                                            int i6 = i5 + 1;
                                            zzcArr5[i5] = zzc5;
                                            i5 = i6;
                                        }
                                    }
                                    ((zzb) zza2.zzalc.get(i2)).ann = zzcArr5;
                                } else if (zzc3 != null) {
                                    zzc3.name = "_err";
                                    zzc3.anr = Long.valueOf(10);
                                } else {
                                    zzbsd().zzbsv().log("Did not find conversion parameter. Error not tracked");
                                }
                            }
                        }
                        int i7 = i3 + 1;
                        zze.anv[i3] = (zzb) zza2.zzalc.get(i2);
                        i3 = i7;
                    }
                    i2++;
                    i = 0;
                }
                if (i3 < zza2.zzalc.size()) {
                    zze.anv = (zzb[]) Arrays.copyOf(zze.anv, i3);
                }
                zze.anO = zza(zza2.alq.zzck, zza2.alq.anw, zze.anv);
                zze.any = zze.anv[0].ano;
                zze.anz = zze.anv[0].ano;
                for (int i8 = 1; i8 < zze.anv.length; i8++) {
                    zzb zzb2 = zze.anv[i8];
                    if (zzb2.ano.longValue() < zze.any.longValue()) {
                        zze.any = zzb2.ano;
                    }
                    if (zzb2.ano.longValue() > zze.anz.longValue()) {
                        zze.anz = zzb2.ano;
                    }
                }
                String str2 = zza2.alq.zzck;
                zza zzln = zzbry().zzln(str2);
                if (zzln == null) {
                    zzbsd().zzbsv().log("Bundling raw events w/o app info");
                } else {
                    long zzbpw = zzln.zzbpw();
                    zze.anB = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                    long zzbpv = zzln.zzbpv();
                    if (zzbpv != 0) {
                        zzbpw = zzbpv;
                    }
                    zze.anA = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                    zzln.zzbqf();
                    zze.anM = Integer.valueOf((int) zzln.zzbqc());
                    zzln.zzau(zze.any.longValue());
                    zzln.zzav(zze.anz.longValue());
                    zzbry().zza(zzln);
                }
                zze.aig = zzbsd().zzbtd();
                zzbry().zza(zze);
                zzbry().zzac(zza2.alr);
                zzbry().zzlt(str2);
                zzbry().setTransactionSuccessful();
                zzbry().endTransaction();
                return true;
            }
            zzbry().setTransactionSuccessful();
            zzbry().endTransaction();
            return false;
        } catch (Throwable th) {
            Throwable th2 = th;
            zzbry().endTransaction();
            throw th2;
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    @WorkerThread
    public boolean isEnabled() {
        zzwu();
        zzzg();
        if (zzbsf().zzbrd()) {
            return false;
        }
        Boolean zzbre = zzbsf().zzbre();
        return zzbse().zzcc(zzbre != null ? zzbre.booleanValue() : !zzbsf().zzaqp());
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void start() {
        zzwu();
        if (!zzbty() || (this.akT.isInitialized() && !this.akT.zzbul())) {
            zzbry().zzbsh();
            if (zzbto()) {
                if (!zzbsf().zzabc() && !TextUtils.isEmpty(zzbrv().zzbps())) {
                    String zzbti = zzbse().zzbti();
                    if (zzbti != null) {
                        if (!zzbti.equals(zzbrv().zzbps())) {
                            zzbsd().zzbta().log("Rechecking which service to use due to a GMP App Id change");
                            zzbse().zzbtk();
                            this.ala.disconnect();
                            this.ala.zzaai();
                        }
                    }
                    zzbse().zzlz(zzbrv().zzbps());
                }
                if (!zzbsf().zzabc() && !zzbty() && !TextUtils.isEmpty(zzbrv().zzbps())) {
                    zzbru().zzbuo();
                }
            } else if (isEnabled()) {
                if (!zzbrz().zzeo("android.permission.INTERNET")) {
                    zzbsd().zzbsv().log("App is missing INTERNET permission");
                }
                if (!zzbrz().zzeo("android.permission.ACCESS_NETWORK_STATE")) {
                    zzbsd().zzbsv().log("App is missing ACCESS_NETWORK_STATE permission");
                }
                if (!zzu.zzav(getContext())) {
                    zzbsd().zzbsv().log("AppMeasurementReceiver not registered/enabled");
                }
                if (!zzae.zzaw(getContext())) {
                    zzbsd().zzbsv().log("AppMeasurementService not registered/enabled");
                }
                zzbsd().zzbsv().log("Uploading is not possible. App measurement disabled");
            }
            zzbue();
            return;
        }
        zzbsd().zzbsv().log("Scheduler shutting down before Scion.start() called");
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public int zza(FileChannel fileChannel) {
        zzwu();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzbsd().zzbsv().log("Bad chanel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                zzbsd().zzbsx().zzj("Unexpected data length or empty data in channel. Bytes read", Integer.valueOf(read));
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e) {
            zzbsd().zzbsv().zzj("Failed to read from channel", e);
            return 0;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zza(AppMetadata appMetadata, long j) {
        zza zzln = zzbry().zzln(appMetadata.packageName);
        if (!(zzln == null || zzln.zzbps() == null || zzln.zzbps().equals(appMetadata.aic))) {
            zzbsd().zzbsx().log("New GMP App Id passed in. Removing cached database data.");
            zzbry().zzls(zzln.zzsh());
            zzln = null;
        }
        if (zzln != null && zzln.zzxc() != null && !zzln.zzxc().equals(appMetadata.aav)) {
            Bundle bundle = new Bundle();
            bundle.putString("_pv", zzln.zzxc());
            EventParcel eventParcel = new EventParcel("_au", new EventParams(bundle), "auto", j);
            zzb(eventParcel, appMetadata);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzh zzh, AppMetadata appMetadata) {
        zzwu();
        zzzg();
        zzab.zzy(zzh);
        zzab.zzy(appMetadata);
        zzab.zzhr(zzh.zzcjf);
        zzab.zzbo(zzh.zzcjf.equals(appMetadata.packageName));
        zzuh.zze zze = new zzuh.zze();
        zze.anu = Integer.valueOf(1);
        zze.anC = "android";
        zze.zzck = appMetadata.packageName;
        zze.aid = appMetadata.aid;
        zze.aav = appMetadata.aav;
        zze.anP = Integer.valueOf((int) appMetadata.aij);
        zze.anG = Long.valueOf(appMetadata.aie);
        zze.aic = appMetadata.aic;
        zze.anL = appMetadata.aif == 0 ? null : Long.valueOf(appMetadata.aif);
        Pair zzlx = zzbse().zzlx(appMetadata.packageName);
        if (zzlx != null && !TextUtils.isEmpty((CharSequence) zzlx.first)) {
            zze.anI = (String) zzlx.first;
            zze.anJ = (Boolean) zzlx.second;
        } else if (!zzbrw().zzdn(this.mContext)) {
            String string = Secure.getString(this.mContext.getContentResolver(), "android_id");
            if (string == null) {
                zzbsd().zzbsx().log("null secure ID");
                string = "null";
            } else if (string.isEmpty()) {
                zzbsd().zzbsx().log("empty secure ID");
            }
            zze.anS = string;
        }
        zze.anD = zzbrw().zztg();
        zze.zzct = zzbrw().zzbso();
        zze.anF = Integer.valueOf((int) zzbrw().zzbsp());
        zze.anE = zzbrw().zzbsq();
        zze.anH = null;
        zze.anx = null;
        zze.any = null;
        zze.anz = null;
        zza zzln = zzbry().zzln(appMetadata.packageName);
        if (zzln == null) {
            zzln = new zza(this, appMetadata.packageName);
            zzln.zzky(zzbse().zzbtf());
            zzln.zzlb(appMetadata.aik);
            zzln.zzkz(appMetadata.aic);
            zzln.zzla(zzbse().zzly(appMetadata.packageName));
            zzln.zzaz(0);
            zzln.zzau(0);
            zzln.zzav(0);
            zzln.setAppVersion(appMetadata.aav);
            zzln.zzaw(appMetadata.aij);
            zzln.zzlc(appMetadata.aid);
            zzln.zzax(appMetadata.aie);
            zzln.zzay(appMetadata.aif);
            zzln.setMeasurementEnabled(appMetadata.aih);
            zzbry().zza(zzln);
        }
        zze.anK = zzln.zzawo();
        zze.aik = zzln.zzbpu();
        List zzlm = zzbry().zzlm(appMetadata.packageName);
        zze.anw = new zzg[zzlm.size()];
        for (int i = 0; i < zzlm.size(); i++) {
            zzg zzg = new zzg();
            zze.anw[i] = zzg;
            zzg.name = ((zzak) zzlm.get(i)).mName;
            zzg.anW = Long.valueOf(((zzak) zzlm.get(i)).amx);
            zzbrz().zza(zzg, ((zzak) zzlm.get(i)).zzcnn);
        }
        try {
            zzbry().zza(zzh, zzbry().zzb(zze));
        } catch (IOException e) {
            zzbsd().zzbsv().zzj("Data loss. Failed to insert raw event metadata", e);
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public boolean zza(int i, FileChannel fileChannel) {
        zzwu();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzbsd().zzbsv().log("Bad chanel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                zzbsd().zzbsv().zzj("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e) {
            zzbsd().zzbsv().zzj("Failed to write to channel", e);
            return false;
        }
    }

    @WorkerThread
    public byte[] zza(@NonNull EventParcel eventParcel, @Size(min = 1) String str) {
        zzd zzd;
        Bundle bundle;
        int i;
        long j;
        EventParcel eventParcel2 = eventParcel;
        String str2 = str;
        zzzg();
        zzwu();
        zzbua();
        zzab.zzy(eventParcel);
        zzab.zzhr(str);
        zzd zzd2 = new zzd();
        zzbry().beginTransaction();
        try {
            zza zzln = zzbry().zzln(str2);
            if (zzln == null) {
                zzbsd().zzbtb().zzj("Log and bundle not available. package_name", str2);
            } else if (!zzln.zzbqb()) {
                zzbsd().zzbtb().zzj("Log and bundle disabled. package_name", str2);
            } else {
                zzuh.zze zze = new zzuh.zze();
                zzd2.ans = new zzuh.zze[]{zze};
                zze.anu = Integer.valueOf(1);
                zze.anC = "android";
                zze.zzck = zzln.zzsh();
                zze.aid = zzln.zzbpy();
                zze.aav = zzln.zzxc();
                zze.anP = Integer.valueOf((int) zzln.zzbpx());
                zze.anG = Long.valueOf(zzln.zzbpz());
                zze.aic = zzln.zzbps();
                zze.anL = Long.valueOf(zzln.zzbqa());
                Pair zzlx = zzbse().zzlx(zzln.zzsh());
                if (zzlx != null && !TextUtils.isEmpty((CharSequence) zzlx.first)) {
                    zze.anI = (String) zzlx.first;
                    zze.anJ = (Boolean) zzlx.second;
                }
                zze.anD = zzbrw().zztg();
                zze.zzct = zzbrw().zzbso();
                zze.anF = Integer.valueOf((int) zzbrw().zzbsp());
                zze.anE = zzbrw().zzbsq();
                zze.anK = zzln.zzawo();
                zze.aik = zzln.zzbpu();
                List zzlm = zzbry().zzlm(zzln.zzsh());
                zze.anw = new zzg[zzlm.size()];
                for (int i2 = 0; i2 < zzlm.size(); i2++) {
                    zzg zzg = new zzg();
                    zze.anw[i2] = zzg;
                    zzg.name = ((zzak) zzlm.get(i2)).mName;
                    zzg.anW = Long.valueOf(((zzak) zzlm.get(i2)).amx);
                    zzbrz().zza(zzg, ((zzak) zzlm.get(i2)).zzcnn);
                }
                Bundle zzbss = eventParcel2.aiI.zzbss();
                if ("_iap".equals(eventParcel2.name)) {
                    zzbss.putLong("_c", 1);
                }
                zzbss.putString("_o", eventParcel2.aiJ);
                zzi zzaq = zzbry().zzaq(str2, eventParcel2.name);
                if (zzaq == null) {
                    zzi zzi = r2;
                    bundle = zzbss;
                    zzd = zzd2;
                    i = 1;
                    zzi zzi2 = new zzi(str2, eventParcel2.name, 1, 0, eventParcel2.aiK);
                    zzbry().zza(zzi);
                    j = 0;
                } else {
                    bundle = zzbss;
                    zzd = zzd2;
                    i = 1;
                    long j2 = zzaq.aiE;
                    zzbry().zza(zzaq.zzbj(eventParcel2.aiK).zzbsr());
                    j = j2;
                }
                zzh zzh = new zzh(this, eventParcel2.aiJ, str2, eventParcel2.name, eventParcel2.aiK, j, bundle);
                zzb zzb = new zzb();
                zzb[] zzbArr = new zzb[i];
                int i3 = 0;
                zzbArr[0] = zzb;
                zze.anv = zzbArr;
                zzb.ano = Long.valueOf(zzh.pJ);
                zzb.name = zzh.mName;
                zzb.anp = Long.valueOf(zzh.aiA);
                zzb.ann = new zzc[zzh.aiB.size()];
                Iterator it = zzh.aiB.iterator();
                while (it.hasNext()) {
                    String str3 = (String) it.next();
                    zzc zzc = new zzc();
                    int i4 = i3 + 1;
                    zzb.ann[i3] = zzc;
                    zzc.name = str3;
                    zzbrz().zza(zzc, zzh.aiB.get(str3));
                    i3 = i4;
                }
                try {
                    zze.anO = zza(zzln.zzsh(), zze.anw, zze.anv);
                    zze.any = zzb.ano;
                    zze.anz = zzb.ano;
                    long zzbpw = zzln.zzbpw();
                    zze.anB = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                    long zzbpv = zzln.zzbpv();
                    if (zzbpv != 0) {
                        zzbpw = zzbpv;
                    }
                    zze.anA = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                    zzln.zzbqf();
                    zze.anM = Integer.valueOf((int) zzln.zzbqc());
                    zze.anH = Long.valueOf(zzbsf().zzbpz());
                    zze.anx = Long.valueOf(zzyw().currentTimeMillis());
                    zze.anN = Boolean.TRUE;
                    zzln.zzau(zze.any.longValue());
                    zzln.zzav(zze.anz.longValue());
                    zzbry().zza(zzln);
                    zzbry().setTransactionSuccessful();
                    zzbry().endTransaction();
                    zzd zzd3 = zzd;
                    try {
                        byte[] bArr = new byte[zzd3.aM()];
                        zzapo zzbe = zzapo.zzbe(bArr);
                        zzd3.zza(zzbe);
                        zzbe.az();
                        return zzbrz().zzj(bArr);
                    } catch (IOException e) {
                        zzbsd().zzbsv().zzj("Data loss. Failed to bundle and serialize", e);
                        return null;
                    }
                } catch (Throwable th) {
                    th = th;
                    Throwable th2 = th;
                    zzbry().endTransaction();
                    throw th2;
                }
            }
            byte[] bArr2 = new byte[0];
            zzbry().endTransaction();
            return bArr2;
        } catch (Throwable th3) {
            th = th3;
            Throwable th22 = th;
            zzbry().endTransaction();
            throw th22;
        }
    }

    public void zzas(boolean z) {
        zzbue();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzb(AppMetadata appMetadata, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("_c", 1);
        EventParcel eventParcel = new EventParcel("_f", new EventParams(bundle), "auto", j);
        zzb(eventParcel, appMetadata);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x018c A[Catch:{ all -> 0x038f }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01ea A[Catch:{ all -> 0x038f }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x021d  */
    @WorkerThread
    public void zzb(EventParcel eventParcel, AppMetadata appMetadata) {
        Bundle bundle;
        String str;
        long zzbqv;
        zzi zzi;
        long j;
        zzak zzak;
        EventParcel eventParcel2 = eventParcel;
        AppMetadata appMetadata2 = appMetadata;
        long nanoTime = System.nanoTime();
        zzwu();
        zzzg();
        String str2 = appMetadata2.packageName;
        zzab.zzhr(str2);
        if (!TextUtils.isEmpty(appMetadata2.aic)) {
            if (!appMetadata2.aih) {
                zze(appMetadata2);
            } else if (zzbsa().zzax(str2, eventParcel2.name)) {
                zzbsd().zzbsx().zzj("Dropping blacklisted event", eventParcel2.name);
                zzbrz().zze(11, "_ev", eventParcel2.name);
            } else {
                if (zzbsd().zzaz(2)) {
                    zzbsd().zzbtc().zzj("Logging event", eventParcel2);
                }
                zzbry().beginTransaction();
                try {
                    Bundle zzbss = eventParcel2.aiI.zzbss();
                    zze(appMetadata2);
                    if (!"_iap".equals(eventParcel2.name)) {
                        if (Event.ECOMMERCE_PURCHASE.equals(eventParcel2.name)) {
                        }
                        bundle = zzbss;
                        str = null;
                        boolean zzmj = zzal.zzmj(eventParcel2.name);
                        Bundle bundle2 = bundle;
                        zzal.zzam(bundle2);
                        boolean equals = "_err".equals(eventParcel2.name);
                        Bundle bundle3 = bundle2;
                        long j2 = nanoTime;
                        String str3 = str;
                        com.google.android.gms.measurement.internal.zze.zza zza2 = zzbry().zza(zzbtz(), str2, zzmj, false, equals);
                        zzbqv = zza2.aiq - zzbsf().zzbqv();
                        if (zzbqv > 0) {
                            if (zzbqv % 1000 == 1) {
                                zzbsd().zzbsv().zzj("Data loss. Too many events logged. count", Long.valueOf(zza2.aiq));
                            }
                            zzbrz().zze(16, "_ev", eventParcel2.name);
                            zzbry().setTransactionSuccessful();
                            zzbry().endTransaction();
                            return;
                        }
                        if (zzmj) {
                            long zzbqw = zza2.aip - zzbsf().zzbqw();
                            if (zzbqw > 0) {
                                if (zzbqw % 1000 == 1) {
                                    zzbsd().zzbsv().zzj("Data loss. Too many public events logged. count", Long.valueOf(zza2.aip));
                                }
                                zzbrz().zze(16, "_ev", eventParcel2.name);
                                zzbry().setTransactionSuccessful();
                                zzbry().endTransaction();
                                return;
                            }
                        }
                        if (equals) {
                            long zzbqx = zza2.ais - zzbsf().zzbqx();
                            if (zzbqx > 0) {
                                if (zzbqx == 1) {
                                    zzbsd().zzbsv().zzj("Too many error events logged. count", Long.valueOf(zza2.ais));
                                }
                                zzbry().setTransactionSuccessful();
                                zzbry().endTransaction();
                                return;
                            }
                        }
                        zzbrz().zza(bundle3, "_o", (Object) eventParcel2.aiJ);
                        long zzlo = zzbry().zzlo(str2);
                        if (zzlo > 0) {
                            zzbsd().zzbsx().zzj("Data lost. Too many events stored on disk, deleted", Long.valueOf(zzlo));
                        }
                        zzh zzh = new zzh(this, eventParcel2.aiJ, str2, eventParcel2.name, eventParcel2.aiK, 0, bundle3);
                        zzi zzaq = zzbry().zzaq(str2, zzh.mName);
                        if (zzaq != null) {
                            zzh = zzh.zza(this, zzaq.aiE);
                            zzi = zzaq.zzbj(zzh.pJ);
                        } else if (zzbry().zzlu(str2) >= ((long) zzbsf().zzbqu())) {
                            zzbsd().zzbsv().zze("Too many event names used, ignoring event. name, supported count", zzh.mName, Integer.valueOf(zzbsf().zzbqu()));
                            zzbrz().zze(8, str3, str3);
                            zzbry().endTransaction();
                            return;
                        } else {
                            zzi = new zzi(str2, zzh.mName, 0, 0, zzh.pJ);
                        }
                        zzbry().zza(zzi);
                        zza(zzh, appMetadata2);
                        zzbry().setTransactionSuccessful();
                        if (zzbsd().zzaz(2)) {
                            zzbsd().zzbtc().zzj("Event recorded", zzh);
                        }
                        zzbry().endTransaction();
                        zzbue();
                        zzbsd().zzbtc().zzj("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / 1000000));
                        return;
                    }
                    String string = zzbss.getString(Param.CURRENCY);
                    if (Event.ECOMMERCE_PURCHASE.equals(eventParcel2.name)) {
                        double d = zzbss.getDouble(Param.VALUE) * 1000000.0d;
                        if (d == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                            d = ((double) zzbss.getLong(Param.VALUE)) * 1000000.0d;
                        }
                        if (d > 9.223372036854776E18d || d < -9.223372036854776E18d) {
                            zzbsd().zzbsx().zzj("Data lost. Currency value is too big", Double.valueOf(d));
                            zzbry().setTransactionSuccessful();
                            zzbry().endTransaction();
                            return;
                        }
                        j = Math.round(d);
                    } else {
                        j = zzbss.getLong(Param.VALUE);
                    }
                    if (!TextUtils.isEmpty(string)) {
                        String upperCase = string.toUpperCase(Locale.US);
                        if (upperCase.matches("[A-Z]{3}")) {
                            String valueOf = String.valueOf("_ltv_");
                            String valueOf2 = String.valueOf(upperCase);
                            String concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                            zzak zzas = zzbry().zzas(str2, concat);
                            if (zzas != null) {
                                if (zzas.zzcnn instanceof Long) {
                                    bundle = zzbss;
                                    zzak zzak2 = new zzak(str2, concat, zzyw().currentTimeMillis(), Long.valueOf(((Long) zzas.zzcnn).longValue() + j));
                                    zzak = zzak2;
                                    if (!zzbry().zza(zzak)) {
                                        zzbsd().zzbsv().zze("Too many unique user properties are set. Ignoring user property.", zzak.mName, zzak.zzcnn);
                                        str = null;
                                        zzbrz().zze(9, null, null);
                                        boolean zzmj2 = zzal.zzmj(eventParcel2.name);
                                        Bundle bundle22 = bundle;
                                        zzal.zzam(bundle22);
                                        boolean equals2 = "_err".equals(eventParcel2.name);
                                        Bundle bundle32 = bundle22;
                                        long j22 = nanoTime;
                                        String str32 = str;
                                        com.google.android.gms.measurement.internal.zze.zza zza22 = zzbry().zza(zzbtz(), str2, zzmj2, false, equals2);
                                        zzbqv = zza22.aiq - zzbsf().zzbqv();
                                        if (zzbqv > 0) {
                                        }
                                    }
                                    str = null;
                                    boolean zzmj22 = zzal.zzmj(eventParcel2.name);
                                    Bundle bundle222 = bundle;
                                    zzal.zzam(bundle222);
                                    boolean equals22 = "_err".equals(eventParcel2.name);
                                    Bundle bundle322 = bundle222;
                                    long j222 = nanoTime;
                                    String str322 = str;
                                    com.google.android.gms.measurement.internal.zze.zza zza222 = zzbry().zza(zzbtz(), str2, zzmj22, false, equals22);
                                    zzbqv = zza222.aiq - zzbsf().zzbqv();
                                    if (zzbqv > 0) {
                                    }
                                }
                            }
                            bundle = zzbss;
                            zzbry().zzy(str2, zzbsf().zzlh(str2) - 1);
                            zzak = new zzak(str2, concat, zzyw().currentTimeMillis(), Long.valueOf(j));
                            if (!zzbry().zza(zzak)) {
                            }
                            str = null;
                            boolean zzmj222 = zzal.zzmj(eventParcel2.name);
                            Bundle bundle2222 = bundle;
                            zzal.zzam(bundle2222);
                            boolean equals222 = "_err".equals(eventParcel2.name);
                            Bundle bundle3222 = bundle2222;
                            long j2222 = nanoTime;
                            String str3222 = str;
                            com.google.android.gms.measurement.internal.zze.zza zza2222 = zzbry().zza(zzbtz(), str2, zzmj222, false, equals222);
                            zzbqv = zza2222.aiq - zzbsf().zzbqv();
                            if (zzbqv > 0) {
                            }
                        }
                    }
                    bundle = zzbss;
                    str = null;
                    boolean zzmj2222 = zzal.zzmj(eventParcel2.name);
                    Bundle bundle22222 = bundle;
                    zzal.zzam(bundle22222);
                    boolean equals2222 = "_err".equals(eventParcel2.name);
                    Bundle bundle32222 = bundle22222;
                    long j22222 = nanoTime;
                    String str32222 = str;
                    com.google.android.gms.measurement.internal.zze.zza zza22222 = zzbry().zza(zzbtz(), str2, zzmj2222, false, equals2222);
                    zzbqv = zza22222.aiq - zzbsf().zzbqv();
                    if (zzbqv > 0) {
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    zzbry().endTransaction();
                    throw th2;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzb(EventParcel eventParcel, String str) {
        EventParcel eventParcel2 = eventParcel;
        String str2 = str;
        zza zzln = zzbry().zzln(str2);
        if (zzln == null || TextUtils.isEmpty(zzln.zzxc())) {
            zzbsd().zzbtb().zzj("No app data available; dropping event", str2);
            return;
        }
        try {
            String str3 = getContext().getPackageManager().getPackageInfo(str2, 0).versionName;
            if (zzln.zzxc() != null && !zzln.zzxc().equals(str3)) {
                zzbsd().zzbsx().zzj("App version does not match; dropping event", str2);
                return;
            }
        } catch (NameNotFoundException unused) {
            if (!"_ui".equals(eventParcel2.name)) {
                zzbsd().zzbsx().zzj("Could not find package", str2);
            }
        }
        AppMetadata appMetadata = r1;
        AppMetadata appMetadata2 = new AppMetadata(str2, zzln.zzbps(), zzln.zzxc(), zzln.zzbpx(), zzln.zzbpy(), zzln.zzbpz(), zzln.zzbqa(), null, zzln.zzbqb(), false, zzln.zzbpu());
        AppMetadata appMetadata3 = appMetadata;
        zzb(eventParcel, appMetadata3);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzb(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        zzwu();
        zzzg();
        if (!TextUtils.isEmpty(appMetadata.aic)) {
            if (!appMetadata.aih) {
                zze(appMetadata);
                return;
            }
            int zzmn = zzbrz().zzmn(userAttributeParcel.name);
            if (zzmn == 0) {
                zzmn = zzbrz().zzm(userAttributeParcel.name, userAttributeParcel.getValue());
                if (zzmn == 0) {
                    Object zzn = zzbrz().zzn(userAttributeParcel.name, userAttributeParcel.getValue());
                    if (zzn != null) {
                        zzak zzak = new zzak(appMetadata.packageName, userAttributeParcel.name, userAttributeParcel.amt, zzn);
                        zzbsd().zzbtb().zze("Setting user property", zzak.mName, zzn);
                        zzbry().beginTransaction();
                        try {
                            zze(appMetadata);
                            boolean zza2 = zzbry().zza(zzak);
                            zzbry().setTransactionSuccessful();
                            if (zza2) {
                                zzbsd().zzbtb().zze("User property set", zzak.mName, zzak.zzcnn);
                            } else {
                                zzbsd().zzbsv().zze("Too many unique user properties are set. Ignoring user property.", zzak.mName, zzak.zzcnn);
                                zzbrz().zze(9, null, null);
                            }
                            return;
                        } finally {
                            zzbry().endTransaction();
                        }
                    } else {
                        return;
                    }
                }
            }
            zzbrz().zze(zzmn, "_ev", zzbrz().zza(userAttributeParcel.name, zzbsf().zzbqo(), true));
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzb(zzaa zzaa) {
        this.aln++;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00cd, code lost:
        if (zzbsa().zzb(r7, r10, r11) == false) goto L_0x00cf;
     */
    @WorkerThread
    public void zzb(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        zzwu();
        zzzg();
        zzab.zzhr(str);
        boolean z = false;
        if (bArr == null) {
            bArr = new byte[0];
        }
        zzbry().beginTransaction();
        try {
            zza zzln = zzbry().zzln(str);
            boolean z2 = (i == 200 || i == 204 || i == 304) && th == null;
            if (zzln == null) {
                zzbsd().zzbsx().zzj("App does not exist in onConfigFetched", str);
            } else {
                if (!z2) {
                    if (i != 404) {
                        zzln.zzbb(zzyw().currentTimeMillis());
                        zzbry().zza(zzln);
                        zzbsd().zzbtc().zze("Fetching config failed. code, error", Integer.valueOf(i), th);
                        zzbsa().zzmd(str);
                        zzbse().ajZ.set(zzyw().currentTimeMillis());
                        if (i == 503 || i == 429) {
                            z = true;
                        }
                        if (z) {
                            zzbse().aka.set(zzyw().currentTimeMillis());
                        }
                        zzbue();
                    }
                }
                List list = map != null ? (List) map.get("Last-Modified") : null;
                String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
                if (i != 404) {
                    if (i == 304) {
                    }
                }
                if (zzbsa().zzmb(str) == null && !zzbsa().zzb(str, null, null)) {
                    zzbry().endTransaction();
                    return;
                }
                zzln.zzba(zzyw().currentTimeMillis());
                zzbry().zza(zzln);
                if (i == 404) {
                    zzbsd().zzbsx().log("Config not found. Using empty config");
                } else {
                    zzbsd().zzbtc().zze("Successfully fetched config. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                }
                if (zzbts().zzadj() && zzbud()) {
                    zzbuc();
                }
                zzbue();
            }
            zzbry().setTransactionSuccessful();
        } finally {
            zzbry().endTransaction();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean zzbl(long j) {
        return zzi(null, j);
    }

    public zzc zzbrt() {
        zza((zzaa) this.alg);
        return this.alg;
    }

    public zzac zzbru() {
        zza((zzaa) this.alc);
        return this.alc;
    }

    public zzn zzbrv() {
        zza((zzaa) this.ald);
        return this.ald;
    }

    public zzg zzbrw() {
        zza((zzaa) this.alb);
        return this.alb;
    }

    public zzad zzbrx() {
        zza((zzaa) this.ala);
        return this.ala;
    }

    public zze zzbry() {
        zza((zzaa) this.akY);
        return this.akY;
    }

    public zzal zzbrz() {
        zza((zzz) this.akX);
        return this.akX;
    }

    public zzv zzbsa() {
        zza((zzaa) this.akV);
        return this.akV;
    }

    public zzaf zzbsb() {
        zza((zzaa) this.akU);
        return this.akU;
    }

    public zzw zzbsc() {
        zza((zzaa) this.akT);
        return this.akT;
    }

    public zzp zzbsd() {
        zza((zzaa) this.akS);
        return this.akS;
    }

    public zzt zzbse() {
        zza((zzz) this.akR);
        return this.akR;
    }

    public zzd zzbsf() {
        return this.akQ;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public boolean zzbto() {
        zzzg();
        zzwu();
        if (this.alj == null) {
            this.alj = Boolean.valueOf(zzbrz().zzeo("android.permission.INTERNET") && zzbrz().zzeo("android.permission.ACCESS_NETWORK_STATE") && zzu.zzav(getContext()) && zzae.zzaw(getContext()));
            if (this.alj.booleanValue() && !zzbsf().zzabc()) {
                this.alj = Boolean.valueOf(zzbrz().zzmq(zzbrv().zzbps()));
            }
        }
        return this.alj.booleanValue();
    }

    public zzp zzbtp() {
        if (this.akS == null || !this.akS.isInitialized()) {
            return null;
        }
        return this.akS;
    }

    /* access modifiers changed from: 0000 */
    public zzw zzbtq() {
        return this.akT;
    }

    public AppMeasurement zzbtr() {
        return this.akW;
    }

    public zzq zzbts() {
        zza((zzaa) this.akZ);
        return this.akZ;
    }

    public zzr zzbtt() {
        if (this.ale != null) {
            return this.ale;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    public zzai zzbtu() {
        zza((zzaa) this.alf);
        return this.alf;
    }

    /* access modifiers changed from: 0000 */
    public FileChannel zzbtv() {
        return this.all;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzbtw() {
        zzwu();
        zzzg();
        if (zzbui() && zzbtx()) {
            zzu(zza(zzbtv()), zzbrv().zzbst());
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public boolean zzbtx() {
        String str;
        com.google.android.gms.measurement.internal.zzp.zza zza2;
        zzwu();
        try {
            this.all = new RandomAccessFile(new File(getContext().getFilesDir(), this.akY.zzaab()), "rw").getChannel();
            this.alk = this.all.tryLock();
            if (this.alk != null) {
                zzbsd().zzbtc().log("Storage concurrent access okay");
                return true;
            }
            zzbsd().zzbsv().log("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e) {
            e = e;
            zza2 = zzbsd().zzbsv();
            str = "Failed to acquire storage lock";
            zza2.zzj(str, e);
            return false;
        } catch (IOException e2) {
            e = e2;
            zza2 = zzbsd().zzbsv();
            str = "Failed to access storage lock file";
            zza2.zzj(str, e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzbty() {
        return false;
    }

    /* access modifiers changed from: 0000 */
    public long zzbtz() {
        return ((((zzyw().currentTimeMillis() + zzbse().zzbtg()) / 1000) / 60) / 60) / 24;
    }

    /* access modifiers changed from: 0000 */
    public void zzbua() {
        if (!zzbsf().zzabc()) {
            throw new IllegalStateException("Unexpected call on client side");
        }
    }

    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.util.Map] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.support.v4.util.ArrayMap, java.util.Map] */
    /* JADX WARNING: type inference failed for: r3v3, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v5, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r3v6 */
    /* JADX WARNING: type inference failed for: r3v7 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], java.lang.String, android.support.v4.util.ArrayMap]
  uses: [java.util.Map, java.lang.Object]
  mth insns count: 214
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 3 */
    @WorkerThread
    public void zzbuc() {
        String str;
        zzwu();
        zzzg();
        if (!zzbsf().zzabc()) {
            Boolean zzbtj = zzbse().zzbtj();
            if (zzbtj == null) {
                zzbsd().zzbsx().log("Upload data called on the client side before use of service was decided");
                return;
            } else if (zzbtj.booleanValue()) {
                zzbsd().zzbsv().log("Upload called in the client side when service should be used");
                return;
            }
        }
        if (zzbub()) {
            zzbsd().zzbsx().log("Uploading requested multiple times");
        } else if (!zzbts().zzadj()) {
            zzbsd().zzbsx().log("Network not connected, ignoring upload request");
            zzbue();
        } else {
            long currentTimeMillis = zzyw().currentTimeMillis();
            zzbl(currentTimeMillis - zzbsf().zzbrl());
            long j = zzbse().ajY.get();
            if (j != 0) {
                zzbsd().zzbtb().zzj("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(currentTimeMillis - j)));
            }
            String zzbsg = zzbry().zzbsg();
            ? r3 = 0;
            if (!TextUtils.isEmpty(zzbsg)) {
                List zzn = zzbry().zzn(zzbsg, zzbsf().zzlj(zzbsg), zzbsf().zzlk(zzbsg));
                if (!zzn.isEmpty()) {
                    Iterator it = zzn.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            str = null;
                            break;
                        }
                        zzuh.zze zze = (zzuh.zze) ((Pair) it.next()).first;
                        if (!TextUtils.isEmpty(zze.anI)) {
                            str = zze.anI;
                            break;
                        }
                    }
                    if (str != null) {
                        int i = 0;
                        while (true) {
                            if (i >= zzn.size()) {
                                break;
                            }
                            zzuh.zze zze2 = (zzuh.zze) ((Pair) zzn.get(i)).first;
                            if (!TextUtils.isEmpty(zze2.anI) && !zze2.anI.equals(str)) {
                                zzn = zzn.subList(0, i);
                                break;
                            }
                            i++;
                        }
                    }
                    zzd zzd = new zzd();
                    zzd.ans = new zzuh.zze[zzn.size()];
                    ArrayList arrayList = new ArrayList(zzn.size());
                    for (int i2 = 0; i2 < zzd.ans.length; i2++) {
                        zzd.ans[i2] = (zzuh.zze) ((Pair) zzn.get(i2)).first;
                        arrayList.add((Long) ((Pair) zzn.get(i2)).second);
                        zzd.ans[i2].anH = Long.valueOf(zzbsf().zzbpz());
                        zzd.ans[i2].anx = Long.valueOf(currentTimeMillis);
                        zzd.ans[i2].anN = Boolean.valueOf(zzbsf().zzabc());
                    }
                    if (zzbsd().zzaz(2)) {
                        r3 = zzal.zzb(zzd);
                    }
                    byte[] zza2 = zzbrz().zza(zzd);
                    String zzbrk = zzbsf().zzbrk();
                    try {
                        URL url = new URL(zzbrk);
                        zzad(arrayList);
                        zzbse().ajZ.set(currentTimeMillis);
                        String str2 = "?";
                        if (zzd.ans.length > 0) {
                            str2 = zzd.ans[0].zzck;
                        }
                        zzbsd().zzbtc().zzd("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(zza2.length), r3);
                        zzbts().zza(zzbsg, url, zza2, null, new zza() {
                            public void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
                                zzx.this.zza(i, th, bArr);
                            }
                        });
                    } catch (MalformedURLException unused) {
                        zzbsd().zzbsv().zzj("Failed to parse upload URL. Not uploading", zzbrk);
                    }
                }
            } else {
                String zzbi = zzbry().zzbi(currentTimeMillis - zzbsf().zzbrl());
                if (!TextUtils.isEmpty(zzbi)) {
                    zza zzln = zzbry().zzln(zzbi);
                    if (zzln != null) {
                        String zzap = zzbsf().zzap(zzln.zzbps(), zzln.zzawo());
                        try {
                            URL url2 = new URL(zzap);
                            zzbsd().zzbtc().zzj("Fetching remote configuration", zzln.zzsh());
                            zzug.zzb zzmb = zzbsa().zzmb(zzln.zzsh());
                            String zzmc = zzbsa().zzmc(zzln.zzsh());
                            if (zzmb != null && !TextUtils.isEmpty(zzmc)) {
                                ? arrayMap = new ArrayMap();
                                arrayMap.put("If-Modified-Since", zzmc);
                                r3 = arrayMap;
                            }
                            zzbts().zza(zzbi, url2, r3, new zza() {
                                public void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
                                    zzx.this.zzb(str, i, th, bArr, map);
                                }
                            });
                        } catch (MalformedURLException unused2) {
                            zzbsd().zzbsv().zzj("Failed to parse config URL. Not fetching", zzap);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzbug() {
        this.alo++;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzbuh() {
        zzwu();
        zzzg();
        if (!this.ali) {
            zzbsd().zzbta().log("This instance being marked as an uploader");
            zzbtw();
        }
        this.ali = true;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public boolean zzbui() {
        zzwu();
        zzzg();
        return this.ali || zzbty();
    }

    /* access modifiers changed from: 0000 */
    public void zzc(AppMetadata appMetadata) {
        zzwu();
        zzzg();
        zzab.zzhr(appMetadata.packageName);
        zze(appMetadata);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzc(AppMetadata appMetadata, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("_et", 1);
        EventParcel eventParcel = new EventParcel("_e", new EventParams(bundle), "auto", j);
        zzb(eventParcel, appMetadata);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzc(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        zzwu();
        zzzg();
        if (!TextUtils.isEmpty(appMetadata.aic)) {
            if (!appMetadata.aih) {
                zze(appMetadata);
                return;
            }
            zzbsd().zzbtb().zzj("Removing user property", userAttributeParcel.name);
            zzbry().beginTransaction();
            try {
                zze(appMetadata);
                zzbry().zzar(appMetadata.packageName, userAttributeParcel.name);
                zzbry().setTransactionSuccessful();
                zzbsd().zzbtb().zzj("User property removed", userAttributeParcel.name);
            } finally {
                zzbry().endTransaction();
            }
        }
    }

    @WorkerThread
    public void zzd(AppMetadata appMetadata) {
        zzwu();
        zzzg();
        zzab.zzy(appMetadata);
        zzab.zzhr(appMetadata.packageName);
        if (!TextUtils.isEmpty(appMetadata.aic)) {
            if (!appMetadata.aih) {
                zze(appMetadata);
                return;
            }
            long currentTimeMillis = zzyw().currentTimeMillis();
            zzbry().beginTransaction();
            try {
                zza(appMetadata, currentTimeMillis);
                zze(appMetadata);
                if (zzbry().zzaq(appMetadata.packageName, "_f") == null) {
                    UserAttributeParcel userAttributeParcel = new UserAttributeParcel("_fot", currentTimeMillis, Long.valueOf((1 + (currentTimeMillis / 3600000)) * 3600000), "auto");
                    zzb(userAttributeParcel, appMetadata);
                    zzb(appMetadata, currentTimeMillis);
                    zzc(appMetadata, currentTimeMillis);
                } else if (appMetadata.aii) {
                    zzd(appMetadata, currentTimeMillis);
                }
                zzbry().setTransactionSuccessful();
            } finally {
                zzbry().endTransaction();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzd(AppMetadata appMetadata, long j) {
        EventParcel eventParcel = new EventParcel("_cd", new EventParams(new Bundle()), "auto", j);
        zzb(eventParcel, appMetadata);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public boolean zzu(int i, int i2) {
        com.google.android.gms.measurement.internal.zzp.zza zzbsv;
        String str;
        zzwu();
        if (i > i2) {
            zzbsv = zzbsd().zzbsv();
            str = "Panic: can't downgrade version. Previous, current version";
        } else {
            if (i < i2) {
                if (zza(i2, zzbtv())) {
                    zzbsd().zzbtc().zze("Storage version upgraded. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
                } else {
                    zzbsv = zzbsd().zzbsv();
                    str = "Storage version upgrade failed. Previous, current version";
                }
            }
            return true;
        }
        zzbsv.zze(str, Integer.valueOf(i), Integer.valueOf(i2));
        return false;
    }

    @WorkerThread
    public void zzwu() {
        zzbsc().zzwu();
    }

    /* access modifiers changed from: 0000 */
    public void zzyv() {
        if (zzbsf().zzabc()) {
            throw new IllegalStateException("Unexpected call on package side");
        }
    }

    public zze zzyw() {
        return this.zzaoc;
    }

    /* access modifiers changed from: 0000 */
    public void zzzg() {
        if (!this.zzcwq) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }
}
