package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

public class zzw extends zzaa {
    /* access modifiers changed from: private */
    public static final AtomicLong akI = new AtomicLong(Long.MIN_VALUE);
    /* access modifiers changed from: private */
    public zzd akA;
    private final PriorityBlockingQueue<FutureTask<?>> akB = new PriorityBlockingQueue<>();
    private final BlockingQueue<FutureTask<?>> akC = new LinkedBlockingQueue();
    private final UncaughtExceptionHandler akD = new zzb("Thread death: Uncaught exception on worker thread");
    private final UncaughtExceptionHandler akE = new zzb("Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */
    public final Object akF = new Object();
    /* access modifiers changed from: private */
    public final Semaphore akG = new Semaphore(2);
    /* access modifiers changed from: private */
    public volatile boolean akH;
    /* access modifiers changed from: private */
    public zzd akz;

    static class zza extends RuntimeException {
    }

    private final class zzb implements UncaughtExceptionHandler {
        private final String akJ;

        public zzb(String str) {
            zzab.zzy(str);
            this.akJ = str;
        }

        public synchronized void uncaughtException(Thread thread, Throwable th) {
            zzw.this.zzbsd().zzbsv().zzj(this.akJ, th);
        }
    }

    private final class zzc<V> extends FutureTask<V> implements Comparable<zzc> {
        private final String akJ;
        private final long akL = zzw.akI.getAndIncrement();
        private final boolean akM;

        zzc(Runnable runnable, boolean z, String str) {
            super(runnable, null);
            zzab.zzy(str);
            this.akJ = str;
            this.akM = z;
            if (this.akL == Long.MAX_VALUE) {
                zzw.this.zzbsd().zzbsv().log("Tasks index overflow");
            }
        }

        zzc(Callable<V> callable, boolean z, String str) {
            super(callable);
            zzab.zzy(str);
            this.akJ = str;
            this.akM = z;
            if (this.akL == Long.MAX_VALUE) {
                zzw.this.zzbsd().zzbsv().log("Tasks index overflow");
            }
        }

        /* access modifiers changed from: protected */
        public void setException(Throwable th) {
            zzw.this.zzbsd().zzbsv().zzj(this.akJ, th);
            if (th instanceof zza) {
                Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
            }
            super.setException(th);
        }

        /* renamed from: zzb */
        public int compareTo(zzc zzc) {
            int i = 1;
            if (this.akM != zzc.akM) {
                if (this.akM) {
                    i = -1;
                }
                return i;
            } else if (this.akL < zzc.akL) {
                return -1;
            } else {
                if (this.akL > zzc.akL) {
                    return 1;
                }
                zzw.this.zzbsd().zzbsw().zzj("Two tasks share the same index. index", Long.valueOf(this.akL));
                return 0;
            }
        }
    }

    private final class zzd extends Thread {
        private final Object akN = new Object();
        private final BlockingQueue<FutureTask<?>> akO;

        public zzd(String str, BlockingQueue<FutureTask<?>> blockingQueue) {
            zzab.zzy(str);
            zzab.zzy(blockingQueue);
            this.akO = blockingQueue;
            setName(str);
        }

        private void zza(InterruptedException interruptedException) {
            zzw.this.zzbsd().zzbsx().zzj(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0052, code lost:
            r2 = com.google.android.gms.measurement.internal.zzw.zzc(r5.akK);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0058, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            com.google.android.gms.measurement.internal.zzw.zza(r5.akK).release();
            com.google.android.gms.measurement.internal.zzw.zzc(r5.akK).notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0071, code lost:
            if (r5 != com.google.android.gms.measurement.internal.zzw.zzd(r5.akK)) goto L_0x0079;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0073, code lost:
            com.google.android.gms.measurement.internal.zzw.zza(r5.akK, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x007f, code lost:
            if (r5 != com.google.android.gms.measurement.internal.zzw.zze(r5.akK)) goto L_0x0087;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0081, code lost:
            com.google.android.gms.measurement.internal.zzw.zzb(r5.akK, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0087, code lost:
            r5.akK.zzbsd().zzbsv().log("Current scheduler thread is neither worker nor network");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0096, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0097, code lost:
            return;
         */
        public void run() {
            boolean z = false;
            while (!z) {
                try {
                    zzw.this.akG.acquire();
                    z = true;
                } catch (InterruptedException e) {
                    zza(e);
                }
            }
            while (true) {
                try {
                    FutureTask futureTask = (FutureTask) this.akO.poll();
                    if (futureTask != null) {
                        futureTask.run();
                    } else {
                        synchronized (this.akN) {
                            if (this.akO.peek() == null && !zzw.this.akH) {
                                try {
                                    this.akN.wait(30000);
                                } catch (InterruptedException e2) {
                                    zza(e2);
                                }
                            }
                        }
                        synchronized (zzw.this.akF) {
                            if (this.akO.peek() == null) {
                            }
                        }
                    }
                } catch (Throwable th) {
                    synchronized (zzw.this.akF) {
                        zzw.this.akG.release();
                        zzw.this.akF.notifyAll();
                        if (this == zzw.this.akz) {
                            zzw.this.akz = null;
                        } else if (this == zzw.this.akA) {
                            zzw.this.akA = null;
                        } else {
                            zzw.this.zzbsd().zzbsv().log("Current scheduler thread is neither worker nor network");
                        }
                        throw th;
                    }
                }
            }
        }

        public void zznk() {
            synchronized (this.akN) {
                this.akN.notifyAll();
            }
        }
    }

    zzw(zzx zzx) {
        super(zzx);
    }

    private void zza(zzc<?> zzc2) {
        synchronized (this.akF) {
            this.akB.add(zzc2);
            if (this.akz == null) {
                this.akz = new zzd("Measurement Worker", this.akB);
                this.akz.setUncaughtExceptionHandler(this.akD);
                this.akz.start();
            } else {
                this.akz.zznk();
            }
        }
    }

    private void zza(FutureTask<?> futureTask) {
        synchronized (this.akF) {
            this.akC.add(futureTask);
            if (this.akA == null) {
                this.akA = new zzd("Measurement Network", this.akC);
                this.akA.setUncaughtExceptionHandler(this.akE);
                this.akA.start();
            } else {
                this.akA.zznk();
            }
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public void zzbrs() {
        if (Thread.currentThread() != this.akA) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    public <V> Future<V> zzd(Callable<V> callable) throws IllegalStateException {
        zzzg();
        zzab.zzy(callable);
        zzc zzc2 = new zzc(callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.akz) {
            zzc2.run();
            return zzc2;
        }
        zza(zzc2);
        return zzc2;
    }

    public <V> Future<V> zze(Callable<V> callable) throws IllegalStateException {
        zzzg();
        zzab.zzy(callable);
        zzc zzc2 = new zzc(callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.akz) {
            zzc2.run();
            return zzc2;
        }
        zza(zzc2);
        return zzc2;
    }

    public void zzm(Runnable runnable) throws IllegalStateException {
        zzzg();
        zzab.zzy(runnable);
        zza(new zzc<>(runnable, false, "Task exception on worker thread"));
    }

    public void zzn(Runnable runnable) throws IllegalStateException {
        zzzg();
        zzab.zzy(runnable);
        zza((FutureTask<?>) new zzc<Object>(runnable, false, "Task exception on network thread"));
    }

    public void zzwu() {
        if (Thread.currentThread() != this.akz) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
