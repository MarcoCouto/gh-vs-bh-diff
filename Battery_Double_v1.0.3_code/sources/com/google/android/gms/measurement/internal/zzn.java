package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzqf;
import com.google.android.gms.measurement.internal.zzp.zza;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;

public class zzn extends zzaa {
    private static final X500Principal ajm = new X500Principal("CN=Android Debug,O=Android,C=US");
    private String ahJ;
    private String ahQ;
    private int ajn;
    private long ajo;
    private String zzcjf;
    private String zzcum;
    private String zzcun;

    zzn(zzx zzx) {
        super(zzx);
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    /* access modifiers changed from: 0000 */
    public String zzbps() {
        zzzg();
        return this.ahJ;
    }

    /* access modifiers changed from: 0000 */
    public String zzbpy() {
        zzzg();
        return this.ahQ;
    }

    /* access modifiers changed from: 0000 */
    public long zzbpz() {
        return zzbsf().zzbpz();
    }

    /* access modifiers changed from: 0000 */
    public long zzbqa() {
        zzzg();
        return this.ajo;
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    /* access modifiers changed from: 0000 */
    public int zzbst() {
        zzzg();
        return this.ajn;
    }

    /* access modifiers changed from: 0000 */
    public boolean zzbsu() {
        String str;
        zza zza;
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 64);
            if (!(packageInfo == null || packageInfo.signatures == null || packageInfo.signatures.length <= 0)) {
                return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(packageInfo.signatures[0].toByteArray()))).getSubjectX500Principal().equals(ajm);
            }
        } catch (CertificateException e) {
            e = e;
            zza = zzbsd().zzbsv();
            str = "Error obtaining certificate";
            zza.zzj(str, e);
            return true;
        } catch (NameNotFoundException e2) {
            e = e2;
            zza = zzbsd().zzbsv();
            str = "Package name not found";
            zza.zzj(str, e);
            return true;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void zzdq(Status status) {
        if (status == null) {
            zzbsd().zzbsv().log("GoogleService failed to initialize (no status)");
        } else {
            zzbsd().zzbsv().zze("GoogleService failed to initialize, status", Integer.valueOf(status.getStatusCode()), status.getStatusMessage());
        }
    }

    /* access modifiers changed from: 0000 */
    public AppMetadata zzlv(String str) {
        AppMetadata appMetadata = new AppMetadata(zzsh(), zzbps(), zzxc(), (long) zzbst(), zzbpy(), zzbpz(), zzbqa(), str, this.ahD.isEnabled(), !zzbse().akm, zzbse().zzbpu());
        return appMetadata;
    }

    /* access modifiers changed from: 0000 */
    public String zzsh() {
        zzzg();
        return this.zzcjf;
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x015b A[SYNTHETIC, Splitter:B:63:0x015b] */
    public void zzwv() {
        MessageDigest zzfa;
        boolean z;
        zza zzbta;
        String str;
        String str2 = "Unknown";
        String str3 = "Unknown";
        PackageManager packageManager = getContext().getPackageManager();
        String packageName = getContext().getPackageName();
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        if (installerPackageName == null) {
            installerPackageName = "manual_install";
        } else if ("com.android.vending".equals(installerPackageName)) {
            installerPackageName = "";
        }
        int i = Integer.MIN_VALUE;
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), 0);
            if (packageInfo != null) {
                CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                if (!TextUtils.isEmpty(applicationLabel)) {
                    str3 = applicationLabel.toString();
                }
                String str4 = packageInfo.versionName;
                try {
                    i = packageInfo.versionCode;
                    str2 = str4;
                } catch (NameNotFoundException unused) {
                    str2 = str4;
                    zzbsd().zzbsv().zzj("Error retrieving package info: appName", str3);
                    this.zzcjf = packageName;
                    this.ahQ = installerPackageName;
                    this.zzcun = str2;
                    this.ajn = i;
                    this.zzcum = str3;
                    zzfa = zzal.zzfa("MD5");
                    if (zzfa == null) {
                    }
                    boolean z2 = true;
                    if (zzbsf().zzabc()) {
                    }
                    if (r0 == null) {
                    }
                    if (!z) {
                    }
                    if (z) {
                    }
                    z2 = false;
                    this.ahJ = "";
                    if (!zzbsf().zzabc()) {
                    }
                }
            }
        } catch (NameNotFoundException unused2) {
            zzbsd().zzbsv().zzj("Error retrieving package info: appName", str3);
            this.zzcjf = packageName;
            this.ahQ = installerPackageName;
            this.zzcun = str2;
            this.ajn = i;
            this.zzcum = str3;
            zzfa = zzal.zzfa("MD5");
            if (zzfa == null) {
            }
            boolean z22 = true;
            if (zzbsf().zzabc()) {
            }
            if (r0 == null) {
            }
            if (!z) {
            }
            if (z) {
            }
            z22 = false;
            this.ahJ = "";
            if (!zzbsf().zzabc()) {
            }
        }
        this.zzcjf = packageName;
        this.ahQ = installerPackageName;
        this.zzcun = str2;
        this.ajn = i;
        this.zzcum = str3;
        zzfa = zzal.zzfa("MD5");
        if (zzfa == null) {
            zzbsd().zzbsv().log("Could not get MD5 instance");
            this.ajo = -1;
        } else {
            this.ajo = 0;
            try {
                if (!zzbsu()) {
                    PackageInfo packageInfo2 = packageManager.getPackageInfo(getContext().getPackageName(), 64);
                    if (packageInfo2.signatures != null && packageInfo2.signatures.length > 0) {
                        this.ajo = zzal.zzx(zzfa.digest(packageInfo2.signatures[0].toByteArray()));
                    }
                }
            } catch (NameNotFoundException e) {
                zzbsd().zzbsv().zzj("Package name not found", e);
            }
        }
        boolean z222 = true;
        Status zzc = zzbsf().zzabc() ? zzqf.zzc(getContext(), "-", true) : zzqf.zzcb(getContext());
        z = zzc == null && zzc.isSuccess();
        if (!z) {
            zzdq(zzc);
        }
        if (z) {
            Boolean zzbre = zzbsf().zzbre();
            if (zzbsf().zzbrd()) {
                zzbta = zzbsd().zzbta();
                str = "Collection disabled with firebase_analytics_collection_deactivated=1";
            } else if (zzbre != null && !zzbre.booleanValue()) {
                zzbta = zzbsd().zzbta();
                str = "Collection disabled with firebase_analytics_collection_enabled=0";
            } else if (zzbre != null || !zzbsf().zzaqp()) {
                zzbsd().zzbtc().log("Collection enabled");
                this.ahJ = "";
                if (!zzbsf().zzabc()) {
                    try {
                        String zzaqo = zzqf.zzaqo();
                        if (TextUtils.isEmpty(zzaqo)) {
                            zzaqo = "";
                        }
                        this.ahJ = zzaqo;
                        if (z222) {
                            zzbsd().zzbtc().zze("App package, google app id", this.zzcjf, this.ahJ);
                            return;
                        }
                    } catch (IllegalStateException e2) {
                        zzbsd().zzbsv().zzj("getGoogleAppId or isMeasurementEnabled failed with exception", e2);
                    }
                }
            } else {
                zzbta = zzbsd().zzbta();
                str = "Collection disabled with google_app_measurement_enable=0";
            }
            zzbta.log(str);
        }
        z222 = false;
        this.ahJ = "";
        if (!zzbsf().zzabc()) {
        }
    }

    /* access modifiers changed from: 0000 */
    public String zzxc() {
        zzzg();
        return this.zzcun;
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
