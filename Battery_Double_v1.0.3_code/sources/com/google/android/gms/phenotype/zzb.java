package com.google.android.gms.phenotype;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;

public class zzb implements Creator<Flag> {
    static void zza(Flag flag, Parcel parcel, int i) {
        int zzcn = com.google.android.gms.common.internal.safeparcel.zzb.zzcn(parcel);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 1, flag.mVersionCode);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 2, flag.name, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 3, flag.arl);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 4, flag.ZV);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 5, flag.ZX);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 6, flag.zD, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zza(parcel, 7, flag.arm, false);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 8, flag.arn);
        com.google.android.gms.common.internal.safeparcel.zzb.zzc(parcel, 9, flag.aro);
        com.google.android.gms.common.internal.safeparcel.zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzqy */
    public Flag createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int zzcm = zza.zzcm(parcel);
        String str = null;
        String str2 = null;
        byte[] bArr = null;
        int i = 0;
        boolean z = false;
        int i2 = 0;
        int i3 = 0;
        long j = 0;
        double d = 0.0d;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel2, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel2, zzcl);
                    break;
                case 3:
                    j = zza.zzi(parcel2, zzcl);
                    break;
                case 4:
                    z = zza.zzc(parcel2, zzcl);
                    break;
                case 5:
                    d = zza.zzn(parcel2, zzcl);
                    break;
                case 6:
                    str2 = zza.zzq(parcel2, zzcl);
                    break;
                case 7:
                    bArr = zza.zzt(parcel2, zzcl);
                    break;
                case 8:
                    i2 = zza.zzg(parcel2, zzcl);
                    break;
                case 9:
                    i3 = zza.zzg(parcel2, zzcl);
                    break;
                default:
                    zza.zzb(parcel2, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel2);
        }
        Flag flag = new Flag(i, str, j, z, d, str2, bArr, i2, i3);
        return flag;
    }

    /* renamed from: zzyf */
    public Flag[] newArray(int i) {
        return new Flag[i];
    }
}
