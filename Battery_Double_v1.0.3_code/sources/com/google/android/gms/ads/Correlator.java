package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.internal.zzin;

@zzin
public final class Correlator {
    private zzn zzaii = new zzn();

    public void reset() {
        this.zzaii.zziy();
    }

    public zzn zzdd() {
        return this.zzaii;
    }
}
