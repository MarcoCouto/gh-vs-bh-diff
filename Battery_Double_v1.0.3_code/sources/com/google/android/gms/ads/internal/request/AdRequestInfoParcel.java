package com.google.android.gms.ads.internal.request;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Messenger;
import android.os.Parcel;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzin;
import java.util.Collections;
import java.util.List;

@zzin
public final class AdRequestInfoParcel extends AbstractSafeParcelable {
    public static final zzf CREATOR = new zzf();
    public final ApplicationInfo applicationInfo;
    public final int versionCode;
    public final String zzaot;
    public final String zzaou;
    public final VersionInfoParcel zzaow;
    public final AdSizeParcel zzapa;
    public final NativeAdOptionsParcel zzapo;
    public final List<String> zzaps;
    public final boolean zzbnq;
    @Nullable
    public final Bundle zzcaq;
    public final AdRequestParcel zzcar;
    @Nullable
    public final PackageInfo zzcas;
    public final String zzcat;
    public final String zzcau;
    public final String zzcav;
    public final Bundle zzcaw;
    public final int zzcax;
    public final Bundle zzcay;
    public final boolean zzcaz;
    public final Messenger zzcba;
    public final int zzcbb;
    public final int zzcbc;
    public final float zzcbd;
    public final String zzcbe;
    public final long zzcbf;
    public final String zzcbg;
    @Nullable
    public final List<String> zzcbh;
    public final List<String> zzcbi;
    public final long zzcbj;
    public final CapabilityParcel zzcbk;
    public final String zzcbl;
    public final float zzcbm;
    public final int zzcbn;
    public final int zzcbo;
    public final boolean zzcbp;
    public final boolean zzcbq;
    public final String zzcbr;
    public final boolean zzcbs;
    public final String zzcbt;
    public final int zzcbu;
    public final Bundle zzcbv;

    @zzin
    public static final class zza {
        public final ApplicationInfo applicationInfo;
        public final String zzaot;
        public final String zzaou;
        public final VersionInfoParcel zzaow;
        public final AdSizeParcel zzapa;
        public final NativeAdOptionsParcel zzapo;
        public final List<String> zzaps;
        public final boolean zzbnq;
        @Nullable
        public final Bundle zzcaq;
        public final AdRequestParcel zzcar;
        @Nullable
        public final PackageInfo zzcas;
        public final String zzcau;
        public final String zzcav;
        public final Bundle zzcaw;
        public final int zzcax;
        public final Bundle zzcay;
        public final boolean zzcaz;
        public final Messenger zzcba;
        public final int zzcbb;
        public final int zzcbc;
        public final float zzcbd;
        public final String zzcbe;
        public final long zzcbf;
        public final String zzcbg;
        @Nullable
        public final List<String> zzcbh;
        public final List<String> zzcbi;
        public final CapabilityParcel zzcbk;
        public final String zzcbl;
        public final float zzcbm;
        public final int zzcbn;
        public final int zzcbo;
        public final boolean zzcbp;
        public final boolean zzcbq;
        public final String zzcbr;
        public final boolean zzcbs;
        public final String zzcbt;
        public final int zzcbu;
        public final Bundle zzcbv;

        public zza(@Nullable Bundle bundle, AdRequestParcel adRequestParcel, AdSizeParcel adSizeParcel, String str, ApplicationInfo applicationInfo2, @Nullable PackageInfo packageInfo, String str2, String str3, VersionInfoParcel versionInfoParcel, Bundle bundle2, List<String> list, List<String> list2, Bundle bundle3, boolean z, Messenger messenger, int i, int i2, float f, String str4, long j, String str5, @Nullable List<String> list3, String str6, NativeAdOptionsParcel nativeAdOptionsParcel, CapabilityParcel capabilityParcel, String str7, float f2, boolean z2, int i3, int i4, boolean z3, boolean z4, String str8, String str9, boolean z5, int i5, Bundle bundle4) {
            List<String> list4;
            AdSizeParcel adSizeParcel2 = adSizeParcel;
            List<String> list5 = list;
            this.zzcaq = bundle;
            this.zzcar = adRequestParcel;
            this.zzapa = adSizeParcel2;
            this.zzaou = str;
            this.applicationInfo = applicationInfo2;
            this.zzcas = packageInfo;
            this.zzcau = str2;
            this.zzcav = str3;
            this.zzaow = versionInfoParcel;
            this.zzcaw = bundle2;
            this.zzcaz = z;
            this.zzcba = messenger;
            this.zzcbb = i;
            this.zzcbc = i2;
            this.zzcbd = f;
            if (list5 == null || list5.size() <= 0) {
                this.zzcax = adSizeParcel2.zzauw ? 4 : 0;
                list4 = null;
                this.zzaps = null;
            } else {
                this.zzcax = 3;
                this.zzaps = list5;
                list4 = list2;
            }
            this.zzcbi = list4;
            this.zzcay = bundle3;
            this.zzcbe = str4;
            this.zzcbf = j;
            this.zzcbg = str5;
            this.zzcbh = list3;
            this.zzaot = str6;
            this.zzapo = nativeAdOptionsParcel;
            this.zzcbk = capabilityParcel;
            this.zzcbl = str7;
            this.zzcbm = f2;
            this.zzcbs = z2;
            this.zzcbn = i3;
            this.zzcbo = i4;
            this.zzcbp = z3;
            this.zzcbq = z4;
            this.zzcbr = str8;
            this.zzcbt = str9;
            this.zzbnq = z5;
            this.zzcbu = i5;
            this.zzcbv = bundle4;
        }
    }

    AdRequestInfoParcel(int i, Bundle bundle, AdRequestParcel adRequestParcel, AdSizeParcel adSizeParcel, String str, ApplicationInfo applicationInfo2, PackageInfo packageInfo, String str2, String str3, String str4, VersionInfoParcel versionInfoParcel, Bundle bundle2, int i2, List<String> list, Bundle bundle3, boolean z, Messenger messenger, int i3, int i4, float f, String str5, long j, String str6, List<String> list2, String str7, NativeAdOptionsParcel nativeAdOptionsParcel, List<String> list3, long j2, CapabilityParcel capabilityParcel, String str8, float f2, boolean z2, int i5, int i6, boolean z3, boolean z4, String str9, String str10, boolean z5, int i7, Bundle bundle4) {
        this.versionCode = i;
        this.zzcaq = bundle;
        this.zzcar = adRequestParcel;
        this.zzapa = adSizeParcel;
        this.zzaou = str;
        this.applicationInfo = applicationInfo2;
        this.zzcas = packageInfo;
        this.zzcat = str2;
        this.zzcau = str3;
        this.zzcav = str4;
        this.zzaow = versionInfoParcel;
        this.zzcaw = bundle2;
        this.zzcax = i2;
        this.zzaps = list;
        this.zzcbi = list3 == null ? Collections.emptyList() : Collections.unmodifiableList(list3);
        this.zzcay = bundle3;
        this.zzcaz = z;
        this.zzcba = messenger;
        this.zzcbb = i3;
        this.zzcbc = i4;
        this.zzcbd = f;
        this.zzcbe = str5;
        this.zzcbf = j;
        this.zzcbg = str6;
        this.zzcbh = list2 == null ? Collections.emptyList() : Collections.unmodifiableList(list2);
        this.zzaot = str7;
        this.zzapo = nativeAdOptionsParcel;
        this.zzcbj = j2;
        this.zzcbk = capabilityParcel;
        this.zzcbl = str8;
        this.zzcbm = f2;
        this.zzcbs = z2;
        this.zzcbn = i5;
        this.zzcbo = i6;
        this.zzcbp = z3;
        this.zzcbq = z4;
        this.zzcbr = str9;
        this.zzcbt = str10;
        this.zzbnq = z5;
        this.zzcbu = i7;
        this.zzcbv = bundle4;
    }

    public AdRequestInfoParcel(@Nullable Bundle bundle, AdRequestParcel adRequestParcel, AdSizeParcel adSizeParcel, String str, ApplicationInfo applicationInfo2, @Nullable PackageInfo packageInfo, String str2, String str3, String str4, VersionInfoParcel versionInfoParcel, Bundle bundle2, int i, List<String> list, List<String> list2, Bundle bundle3, boolean z, Messenger messenger, int i2, int i3, float f, String str5, long j, String str6, @Nullable List<String> list3, String str7, NativeAdOptionsParcel nativeAdOptionsParcel, long j2, CapabilityParcel capabilityParcel, String str8, float f2, boolean z2, int i4, int i5, boolean z3, boolean z4, String str9, String str10, boolean z5, int i6, Bundle bundle4) {
        this(18, bundle, adRequestParcel, adSizeParcel, str, applicationInfo2, packageInfo, str2, str3, str4, versionInfoParcel, bundle2, i, list, bundle3, z, messenger, i2, i3, f, str5, j, str6, list3, str7, nativeAdOptionsParcel, list2, j2, capabilityParcel, str8, f2, z2, i4, i5, z3, z4, str9, str10, z5, i6, bundle4);
    }

    public AdRequestInfoParcel(zza zza2, String str, long j) {
        zza zza3 = zza2;
        Bundle bundle = zza3.zzcaq;
        AdRequestParcel adRequestParcel = zza3.zzcar;
        AdSizeParcel adSizeParcel = zza3.zzapa;
        String str2 = zza3.zzaou;
        ApplicationInfo applicationInfo2 = zza3.applicationInfo;
        PackageInfo packageInfo = zza3.zzcas;
        String str3 = zza3.zzcau;
        String str4 = zza3.zzcav;
        VersionInfoParcel versionInfoParcel = zza3.zzaow;
        Bundle bundle2 = zza3.zzcaw;
        int i = zza3.zzcax;
        List<String> list = zza3.zzaps;
        List<String> list2 = zza3.zzcbi;
        Bundle bundle3 = zza3.zzcay;
        boolean z = zza3.zzcaz;
        Bundle bundle4 = bundle3;
        Messenger messenger = zza3.zzcba;
        int i2 = zza3.zzcbb;
        int i3 = zza3.zzcbc;
        float f = zza3.zzcbd;
        List<String> list3 = list2;
        String str5 = zza3.zzcbe;
        long j2 = zza3.zzcbf;
        String str6 = zza3.zzcbg;
        List<String> list4 = zza3.zzcbh;
        String str7 = str6;
        String str8 = zza3.zzaot;
        NativeAdOptionsParcel nativeAdOptionsParcel = zza3.zzapo;
        CapabilityParcel capabilityParcel = zza3.zzcbk;
        String str9 = zza3.zzcbl;
        float f2 = zza3.zzcbm;
        boolean z2 = zza3.zzcbs;
        int i4 = zza3.zzcbn;
        int i5 = zza3.zzcbo;
        boolean z3 = zza3.zzcbp;
        boolean z4 = zza3.zzcbq;
        String str10 = zza3.zzcbr;
        String str11 = zza3.zzcbt;
        boolean z5 = zza3.zzbnq;
        int i6 = zza3.zzcbu;
        Bundle bundle5 = zza3.zzcbv;
        List<String> list5 = list4;
        List<String> list6 = list3;
        int i7 = i6;
        Bundle bundle6 = bundle4;
        this(bundle, adRequestParcel, adSizeParcel, str2, applicationInfo2, packageInfo, str, str3, str4, versionInfoParcel, bundle2, i, list, list6, bundle6, z, messenger, i2, i3, f, str5, j2, str7, list5, str8, nativeAdOptionsParcel, j, capabilityParcel, str9, f2, z2, i4, i5, z3, z4, str10, str11, z5, i7, bundle5);
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzf.zza(this, parcel, i);
    }
}
