package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzin;
import java.util.List;

@zzin
class zzb extends RelativeLayout {
    private static final float[] zzbfb = {5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f};
    private final RelativeLayout zzbfc;
    @Nullable
    private AnimationDrawable zzbfd;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
        r0.addRule(11);
     */
    public zzb(Context context, zza zza) {
        super(context);
        zzab.zzy(zza);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        int zzkr = zza.zzkr();
        if (zzkr != 0) {
            switch (zzkr) {
                case 2:
                    layoutParams.addRule(12);
                    break;
                case 3:
                    layoutParams.addRule(12);
                    break;
                default:
                    layoutParams.addRule(10);
                    break;
            }
        } else {
            layoutParams.addRule(10);
        }
        layoutParams.addRule(9);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(zzbfb, null, null));
        shapeDrawable.getPaint().setColor(zza.getBackgroundColor());
        this.zzbfc = new RelativeLayout(context);
        this.zzbfc.setLayoutParams(layoutParams);
        zzu.zzfs().zza((View) this.zzbfc, (Drawable) shapeDrawable);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        if (!TextUtils.isEmpty(zza.getText())) {
            LayoutParams layoutParams3 = new LayoutParams(-2, -2);
            TextView textView = new TextView(context);
            textView.setLayoutParams(layoutParams3);
            textView.setId(1195835393);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setText(zza.getText());
            textView.setTextColor(zza.getTextColor());
            textView.setTextSize((float) zza.getTextSize());
            textView.setPadding(zzm.zziw().zza(context, 4), 0, zzm.zziw().zza(context, 4), 0);
            this.zzbfc.addView(textView);
            layoutParams2.addRule(1, textView.getId());
        }
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(layoutParams2);
        imageView.setId(1195835394);
        List<Drawable> zzkp = zza.zzkp();
        if (zzkp.size() > 1) {
            this.zzbfd = new AnimationDrawable();
            for (Drawable addFrame : zzkp) {
                this.zzbfd.addFrame(addFrame, zza.zzkq());
            }
            zzu.zzfs().zza((View) imageView, (Drawable) this.zzbfd);
        } else if (zzkp.size() == 1) {
            imageView.setImageDrawable((Drawable) zzkp.get(0));
        }
        this.zzbfc.addView(imageView);
        addView(this.zzbfc);
    }

    public void onAttachedToWindow() {
        if (this.zzbfd != null) {
            this.zzbfd.start();
        }
        super.onAttachedToWindow();
    }

    public ViewGroup zzks() {
        return this.zzbfc;
    }
}
