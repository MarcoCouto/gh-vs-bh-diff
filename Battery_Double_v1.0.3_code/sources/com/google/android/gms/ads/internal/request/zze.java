package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.ads.internal.request.zzk.zza;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzd.zzb;
import com.google.android.gms.common.internal.zzd.zzc;
import com.google.android.gms.internal.zzin;

@zzin
public class zze extends zzd<zzk> {
    final int zzcap;

    public zze(Context context, Looper looper, zzb zzb, zzc zzc, int i) {
        super(context, looper, 8, zzb, zzc, null);
        this.zzcap = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzba */
    public zzk zzbb(IBinder iBinder) {
        return zza.zzbc(iBinder);
    }

    /* access modifiers changed from: protected */
    public String zzqz() {
        return "com.google.android.gms.ads.service.START";
    }

    /* access modifiers changed from: protected */
    public String zzra() {
        return "com.google.android.gms.ads.internal.request.IAdRequestService";
    }

    public zzk zzrb() throws DeadObjectException {
        return (zzk) super.zzasa();
    }
}
