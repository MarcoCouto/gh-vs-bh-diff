package com.google.android.gms.ads.internal.formats;

import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdz.zza;
import com.google.android.gms.internal.zzin;
import java.util.Arrays;
import java.util.List;

@zzin
public class zzf extends zza implements zzh.zza {
    private final Object zzail = new Object();
    private final zza zzbfo;
    private zzh zzbfp;
    private final String zzbfs;
    private final SimpleArrayMap<String, zzc> zzbft;
    private final SimpleArrayMap<String, String> zzbfu;

    public zzf(String str, SimpleArrayMap<String, zzc> simpleArrayMap, SimpleArrayMap<String, String> simpleArrayMap2, zza zza) {
        this.zzbfs = str;
        this.zzbft = simpleArrayMap;
        this.zzbfu = simpleArrayMap2;
        this.zzbfo = zza;
    }

    public List<String> getAvailableAssetNames() {
        String[] strArr = new String[(this.zzbft.size() + this.zzbfu.size())];
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.zzbft.size()) {
            strArr[i3] = (String) this.zzbft.keyAt(i2);
            i2++;
            i3++;
        }
        while (i < this.zzbfu.size()) {
            strArr[i3] = (String) this.zzbfu.keyAt(i);
            i++;
            i3++;
        }
        return Arrays.asList(strArr);
    }

    public String getCustomTemplateId() {
        return this.zzbfs;
    }

    public void performClick(String str) {
        synchronized (this.zzail) {
            if (this.zzbfp == null) {
                zzb.e("Attempt to call performClick before ad initialized.");
            } else {
                this.zzbfp.zza(str, null, null, null);
            }
        }
    }

    public void recordImpression() {
        synchronized (this.zzail) {
            if (this.zzbfp == null) {
                zzb.e("Attempt to perform recordImpression before ad initialized.");
            } else {
                this.zzbfp.recordImpression();
            }
        }
    }

    public String zzat(String str) {
        return (String) this.zzbfu.get(str);
    }

    public zzdr zzau(String str) {
        return (zzdr) this.zzbft.get(str);
    }

    public void zzb(zzh zzh) {
        synchronized (this.zzail) {
            this.zzbfp = zzh;
        }
    }

    public String zzkw() {
        return "3";
    }

    public zza zzkx() {
        return this.zzbfo;
    }
}
