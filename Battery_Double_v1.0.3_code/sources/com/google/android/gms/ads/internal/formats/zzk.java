package com.google.android.gms.ads.internal.formats;

import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdt.zza;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzlh;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzk extends zza implements OnClickListener, OnTouchListener, OnGlobalLayoutListener, OnScrollChangedListener {
    private final Object zzail = new Object();
    /* access modifiers changed from: private */
    @Nullable
    public FrameLayout zzaiz;
    @Nullable
    private zzh zzbfp;
    private final FrameLayout zzbgt;
    private Map<String, WeakReference<View>> zzbgu = new HashMap();
    @Nullable
    private zzb zzbgv;
    boolean zzbgw = false;
    int zzbgx;
    int zzbgy;

    public zzk(FrameLayout frameLayout, FrameLayout frameLayout2) {
        this.zzbgt = frameLayout;
        this.zzaiz = frameLayout2;
        zzu.zzgk().zza((View) this.zzbgt, (OnGlobalLayoutListener) this);
        zzu.zzgk().zza((View) this.zzbgt, (OnScrollChangedListener) this);
        this.zzbgt.setOnTouchListener(this);
        this.zzbgt.setOnClickListener(this);
    }

    public void destroy() {
        synchronized (this.zzail) {
            if (this.zzaiz != null) {
                this.zzaiz.removeAllViews();
            }
            this.zzaiz = null;
            this.zzbgu = null;
            this.zzbgv = null;
            this.zzbfp = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public int getMeasuredHeight() {
        return this.zzbgt.getMeasuredHeight();
    }

    /* access modifiers changed from: 0000 */
    public int getMeasuredWidth() {
        return this.zzbgt.getMeasuredWidth();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0117, code lost:
        return;
     */
    public void onClick(View view) {
        zzh zzh;
        String str;
        synchronized (this.zzail) {
            if (this.zzbfp != null) {
                JSONObject jSONObject = new JSONObject();
                for (Entry entry : this.zzbgu.entrySet()) {
                    View view2 = (View) ((WeakReference) entry.getValue()).get();
                    if (view2 != null) {
                        Point zzi = zzi(view2);
                        JSONObject jSONObject2 = new JSONObject();
                        try {
                            jSONObject2.put("width", zzx(view2.getWidth()));
                            jSONObject2.put("height", zzx(view2.getHeight()));
                            jSONObject2.put("x", zzx(zzi.x));
                            jSONObject2.put("y", zzx(zzi.y));
                            jSONObject.put((String) entry.getKey(), jSONObject2);
                        } catch (JSONException unused) {
                            String str2 = "Unable to get view rectangle for view ";
                            String valueOf = String.valueOf((String) entry.getKey());
                            zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                        }
                    }
                }
                JSONObject jSONObject3 = new JSONObject();
                try {
                    jSONObject3.put("x", zzx(this.zzbgx));
                    jSONObject3.put("y", zzx(this.zzbgy));
                } catch (JSONException unused2) {
                    zzkd.zzcx("Unable to get click location");
                }
                JSONObject jSONObject4 = new JSONObject();
                try {
                    jSONObject4.put("width", zzx(getMeasuredWidth()));
                    jSONObject4.put("height", zzx(getMeasuredHeight()));
                } catch (JSONException unused3) {
                    zzkd.zzcx("Unable to get native ad view bounding box");
                }
                if (this.zzbgv == null || !this.zzbgv.zzks().equals(view)) {
                    this.zzbfp.zza(view, this.zzbgu, jSONObject, jSONObject3, jSONObject4);
                } else {
                    if (!(this.zzbfp instanceof zzg) || ((zzg) this.zzbfp).zzla() == null) {
                        zzh = this.zzbfp;
                        str = "1007";
                    } else {
                        zzh = ((zzg) this.zzbfp).zzla();
                        str = "1007";
                    }
                    zzh.zza(str, jSONObject, jSONObject3, jSONObject4);
                }
            }
        }
    }

    public void onGlobalLayout() {
        synchronized (this.zzail) {
            if (this.zzbgw) {
                int measuredWidth = getMeasuredWidth();
                int measuredHeight = getMeasuredHeight();
                if (!(measuredWidth == 0 || measuredHeight == 0 || this.zzaiz == null)) {
                    this.zzaiz.setLayoutParams(new LayoutParams(measuredWidth, measuredHeight));
                    this.zzbgw = false;
                }
            }
            if (this.zzbfp != null) {
                this.zzbfp.zzg(this.zzbgt);
            }
        }
    }

    public void onScrollChanged() {
        synchronized (this.zzail) {
            if (this.zzbfp != null) {
                this.zzbfp.zzg(this.zzbgt);
            }
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.zzail) {
            if (this.zzbfp == null) {
                return false;
            }
            Point zzc = zzc(motionEvent);
            this.zzbgx = zzc.x;
            this.zzbgy = zzc.y;
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            obtain.setLocation((float) zzc.x, (float) zzc.y);
            this.zzbfp.zzb(obtain);
            obtain.recycle();
            return false;
        }
    }

    public zzd zzap(String str) {
        zzd zzac;
        synchronized (this.zzail) {
            WeakReference weakReference = (WeakReference) this.zzbgu.get(str);
            zzac = zze.zzac(weakReference == null ? null : (View) weakReference.get());
        }
        return zzac;
    }

    /* access modifiers changed from: 0000 */
    public Point zzc(MotionEvent motionEvent) {
        int[] iArr = new int[2];
        this.zzbgt.getLocationOnScreen(iArr);
        return new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public zzb zzc(zzi zzi) {
        return zzi.zza((OnClickListener) this);
    }

    public void zzc(String str, zzd zzd) {
        View view = (View) zze.zzad(zzd);
        synchronized (this.zzail) {
            if (view == null) {
                try {
                    this.zzbgu.remove(str);
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                this.zzbgu.put(str, new WeakReference(view));
                view.setOnTouchListener(this);
                view.setClickable(true);
                view.setOnClickListener(this);
            }
        }
    }

    public void zze(zzd zzd) {
        synchronized (this.zzail) {
            zzh(null);
            Object zzad = zze.zzad(zzd);
            if (!(zzad instanceof zzi)) {
                zzkd.zzcx("Not an instance of native engine. This is most likely a transient error");
                return;
            }
            if (this.zzaiz != null) {
                this.zzaiz.setLayoutParams(new LayoutParams(0, 0));
                this.zzbgt.requestLayout();
            }
            this.zzbgw = true;
            final zzi zzi = (zzi) zzad;
            if (this.zzbfp != null && ((Boolean) zzdc.zzbch.get()).booleanValue()) {
                this.zzbfp.zzb(this.zzbgt, this.zzbgu);
            }
            if (!(this.zzbfp instanceof zzg) || !((zzg) this.zzbfp).zzkz()) {
                this.zzbfp = zzi;
                if (zzi instanceof zzg) {
                    ((zzg) zzi).zzc(null);
                }
            } else {
                ((zzg) this.zzbfp).zzc(zzi);
            }
            if (((Boolean) zzdc.zzbch.get()).booleanValue()) {
                this.zzaiz.setClickable(false);
            }
            this.zzaiz.removeAllViews();
            this.zzbgv = zzc(zzi);
            if (this.zzbgv != null) {
                this.zzbgu.put("1007", new WeakReference(this.zzbgv.zzks()));
                this.zzaiz.addView(this.zzbgv);
            }
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzlh zzlb = zzi.zzlb();
                    if (zzlb != null && zzk.this.zzaiz != null) {
                        zzk.this.zzaiz.addView(zzlb.getView());
                    }
                }
            });
            zzi.zza((View) this.zzbgt, this.zzbgu, (OnTouchListener) this, (OnClickListener) this);
            zzh(this.zzbgt);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzh(@Nullable View view) {
        if (this.zzbfp != null) {
            zzh zzla = this.zzbfp instanceof zzg ? ((zzg) this.zzbfp).zzla() : this.zzbfp;
            if (zzla != null) {
                zzla.zzh(view);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public Point zzi(View view) {
        if (this.zzbgv == null || !this.zzbgv.zzks().equals(view)) {
            Point point = new Point();
            view.getGlobalVisibleRect(new Rect(), point);
            return point;
        }
        Point point2 = new Point();
        this.zzbgt.getGlobalVisibleRect(new Rect(), point2);
        Point point3 = new Point();
        view.getGlobalVisibleRect(new Rect(), point3);
        return new Point(point3.x - point2.x, point3.y - point2.y);
    }

    /* access modifiers changed from: 0000 */
    public int zzx(int i) {
        return zzm.zziw().zzb(this.zzbfp.getContext(), i);
    }
}
