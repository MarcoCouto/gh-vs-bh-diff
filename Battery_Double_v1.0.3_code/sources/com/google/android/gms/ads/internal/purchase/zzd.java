package com.google.android.gms.ads.internal.purchase;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzhn.zza;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@zzin
public class zzd extends zza {
    private Context mContext;
    private String zzarj;
    private String zzbwy;
    private ArrayList<String> zzbwz;

    public zzd(String str, ArrayList<String> arrayList, Context context, String str2) {
        this.zzbwy = str;
        this.zzbwz = arrayList;
        this.zzarj = str2;
        this.mContext = context;
    }

    public String getProductId() {
        return this.zzbwy;
    }

    public void recordPlayBillingResolution(int i) {
        if (i == 0) {
            zzps();
        }
        Map zzpr = zzpr();
        zzpr.put("google_play_status", String.valueOf(i));
        zzpr.put("sku", this.zzbwy);
        zzpr.put("status", String.valueOf(zzai(i)));
        LinkedList linkedList = new LinkedList();
        Iterator it = this.zzbwz.iterator();
        while (it.hasNext()) {
            linkedList.add(zzu.zzfq().zzb((String) it.next(), zzpr));
        }
        zzu.zzfq().zza(this.mContext, this.zzarj, (List<String>) linkedList);
    }

    public void recordResolution(int i) {
        if (i == 1) {
            zzps();
        }
        Map zzpr = zzpr();
        zzpr.put("status", String.valueOf(i));
        zzpr.put("sku", this.zzbwy);
        LinkedList linkedList = new LinkedList();
        Iterator it = this.zzbwz.iterator();
        while (it.hasNext()) {
            linkedList.add(zzu.zzfq().zzb((String) it.next(), zzpr));
        }
        zzu.zzfq().zza(this.mContext, this.zzarj, (List<String>) linkedList);
    }

    /* access modifiers changed from: protected */
    public int zzai(int i) {
        if (i == 0) {
            return 1;
        }
        if (i == 1) {
            return 2;
        }
        return i == 4 ? 3 : 0;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> zzpr() {
        String packageName = this.mContext.getPackageName();
        String str = "";
        try {
            str = this.mContext.getPackageManager().getPackageInfo(packageName, 0).versionName;
        } catch (NameNotFoundException e) {
            zzkd.zzd("Error to retrieve app version", e);
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - zzu.zzft().zzsk().zzsx();
        HashMap hashMap = new HashMap();
        hashMap.put("sessionid", zzu.zzft().getSessionId());
        hashMap.put("appid", packageName);
        hashMap.put("osversion", String.valueOf(VERSION.SDK_INT));
        hashMap.put("sdkversion", this.zzarj);
        hashMap.put("appversion", str);
        hashMap.put("timestamp", String.valueOf(elapsedRealtime));
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    public void zzps() {
        try {
            this.mContext.getClassLoader().loadClass("com.google.ads.conversiontracking.IAPConversionReporter").getDeclaredMethod("reportWithProductId", new Class[]{Context.class, String.class, String.class, Boolean.TYPE}).invoke(null, new Object[]{this.mContext, this.zzbwy, "", Boolean.valueOf(true)});
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            zzkd.zzcx("Google Conversion Tracking SDK 1.2.0 or above is required to report a conversion.");
        } catch (Exception e) {
            zzkd.zzd("Fail to report a conversion.", e);
        }
    }
}
