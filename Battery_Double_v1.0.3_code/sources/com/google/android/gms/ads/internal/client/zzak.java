package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzu.zza;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzdo;
import com.google.android.gms.internal.zzho;
import com.google.android.gms.internal.zzhs;

public class zzak extends zza {
    /* access modifiers changed from: private */
    public zzq zzalf;

    public void destroy() {
    }

    public String getMediationAdapterClassName() {
        return null;
    }

    public boolean isLoading() {
        return false;
    }

    public boolean isReady() {
        return false;
    }

    public void pause() {
    }

    public void resume() {
    }

    public void setManualImpressionsEnabled(boolean z) {
    }

    public void setUserId(String str) {
    }

    public void showInterstitial() {
    }

    public void stopLoading() {
    }

    public void zza(AdSizeParcel adSizeParcel) {
    }

    public void zza(VideoOptionsParcel videoOptionsParcel) {
    }

    public void zza(zzp zzp) {
    }

    public void zza(zzq zzq) {
        this.zzalf = zzq;
    }

    public void zza(zzw zzw) {
    }

    public void zza(zzy zzy) {
    }

    public void zza(zzd zzd) {
    }

    public void zza(zzdo zzdo) {
    }

    public void zza(zzho zzho) {
    }

    public void zza(zzhs zzhs, String str) {
    }

    public boolean zzb(AdRequestParcel adRequestParcel) {
        zzb.e("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                if (zzak.this.zzalf != null) {
                    try {
                        zzak.this.zzalf.onAdFailedToLoad(1);
                    } catch (RemoteException e) {
                        zzb.zzd("Could not notify onAdFailedToLoad event.", e);
                    }
                }
            }
        });
        return false;
    }

    public com.google.android.gms.dynamic.zzd zzdm() {
        return null;
    }

    public AdSizeParcel zzdn() {
        return null;
    }

    public void zzdp() {
    }

    public zzab zzdq() {
        return null;
    }
}
