package com.google.android.gms.ads.internal.request;

import android.content.Context;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkj;
import com.google.android.gms.internal.zzla;

@zzin
public final class zzc {

    public interface zza {
        void zzb(AdResponseParcel adResponseParcel);
    }

    interface zzb {
        boolean zza(VersionInfoParcel versionInfoParcel);
    }

    public static zzkj zza(final Context context, VersionInfoParcel versionInfoParcel, zzla<AdRequestInfoParcel> zzla, zza zza2) {
        return zza(context, versionInfoParcel, zzla, zza2, new zzb() {
            public boolean zza(VersionInfoParcel versionInfoParcel) {
                return versionInfoParcel.zzcnm || (zzi.zzcl(context) && !((Boolean) zzdc.zzayz.get()).booleanValue());
            }
        });
    }

    static zzkj zza(Context context, VersionInfoParcel versionInfoParcel, zzla<AdRequestInfoParcel> zzla, zza zza2, zzb zzb2) {
        return zzb2.zza(versionInfoParcel) ? zza(context, zzla, zza2) : zzb(context, versionInfoParcel, zzla, zza2);
    }

    private static zzkj zza(Context context, zzla<AdRequestInfoParcel> zzla, zza zza2) {
        zzkd.zzcv("Fetching ad response from local ad request service.");
        com.google.android.gms.ads.internal.request.zzd.zza zza3 = new com.google.android.gms.ads.internal.request.zzd.zza(context, zzla, zza2);
        Void voidR = (Void) zza3.zzpy();
        return zza3;
    }

    private static zzkj zzb(Context context, VersionInfoParcel versionInfoParcel, zzla<AdRequestInfoParcel> zzla, zza zza2) {
        zzkd.zzcv("Fetching ad response from remote ad request service.");
        if (zzm.zziw().zzar(context)) {
            return new com.google.android.gms.ads.internal.request.zzd.zzb(context, versionInfoParcel, zzla, zza2);
        }
        zzkd.zzcx("Failed to connect to remote ad request service.");
        return null;
    }
}
