package com.google.android.gms.ads.internal.overlay;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;

@zzin
public class zza {
    public boolean zza(Context context, Intent intent, zzp zzp) {
        String str = "Launching an intent: ";
        try {
            String valueOf = String.valueOf(intent.toURI());
            zzkd.v(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            zzu.zzfq().zzb(context, intent);
            if (zzp != null) {
                zzp.zzdo();
            }
            return true;
        } catch (ActivityNotFoundException e) {
            zzkd.zzcx(e.getMessage());
            return false;
        }
    }

    public boolean zza(Context context, AdLauncherIntentInfoParcel adLauncherIntentInfoParcel, zzp zzp) {
        int i;
        String str;
        if (adLauncherIntentInfoParcel == null) {
            str = "No intent data for launcher overlay.";
        } else if (adLauncherIntentInfoParcel.intent != null) {
            return zza(context, adLauncherIntentInfoParcel.intent, zzp);
        } else {
            Intent intent = new Intent();
            if (TextUtils.isEmpty(adLauncherIntentInfoParcel.url)) {
                str = "Open GMSG did not contain a URL.";
            } else {
                if (!TextUtils.isEmpty(adLauncherIntentInfoParcel.mimeType)) {
                    intent.setDataAndType(Uri.parse(adLauncherIntentInfoParcel.url), adLauncherIntentInfoParcel.mimeType);
                } else {
                    intent.setData(Uri.parse(adLauncherIntentInfoParcel.url));
                }
                intent.setAction("android.intent.action.VIEW");
                if (!TextUtils.isEmpty(adLauncherIntentInfoParcel.packageName)) {
                    intent.setPackage(adLauncherIntentInfoParcel.packageName);
                }
                if (!TextUtils.isEmpty(adLauncherIntentInfoParcel.zzbro)) {
                    String[] split = adLauncherIntentInfoParcel.zzbro.split("/", 2);
                    if (split.length < 2) {
                        String str2 = "Could not parse component name from open GMSG: ";
                        String valueOf = String.valueOf(adLauncherIntentInfoParcel.zzbro);
                        zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                        return false;
                    }
                    intent.setClassName(split[0], split[1]);
                }
                String str3 = adLauncherIntentInfoParcel.zzbrp;
                if (!TextUtils.isEmpty(str3)) {
                    try {
                        i = Integer.parseInt(str3);
                    } catch (NumberFormatException unused) {
                        zzkd.zzcx("Could not parse intent flags.");
                        i = 0;
                    }
                    intent.addFlags(i);
                }
                return zza(context, intent, zzp);
            }
        }
        zzkd.zzcx(str);
        return false;
    }
}
