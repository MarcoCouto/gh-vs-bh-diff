package com.google.android.gms.ads.internal;

import android.os.Bundle;
import android.os.Debug;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewParent;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.ThinAdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzf;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.request.zza.C0015zza;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzcg;
import com.google.android.gms.internal.zzcl;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdi;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzdo;
import com.google.android.gms.internal.zzel;
import com.google.android.gms.internal.zzho;
import com.google.android.gms.internal.zzhs;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjd;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzjv;
import com.google.android.gms.internal.zzjz;
import com.google.android.gms.internal.zzka;
import com.google.android.gms.internal.zzkd;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

@zzin
public abstract class zza extends com.google.android.gms.ads.internal.client.zzu.zza implements com.google.android.gms.ads.internal.client.zza, zzp, C0015zza, zzel, com.google.android.gms.internal.zzic.zza, zzjz {
    protected zzdk zzajn;
    protected zzdi zzajo;
    protected zzdi zzajp;
    protected boolean zzajq = false;
    protected final zzr zzajr;
    protected final zzv zzajs;
    @Nullable
    protected transient AdRequestParcel zzajt;
    protected final zzcg zzaju;
    protected final zzd zzajv;

    zza(zzv zzv, @Nullable zzr zzr, zzd zzd) {
        this.zzajs = zzv;
        if (zzr == null) {
            zzr = new zzr(this);
        }
        this.zzajr = zzr;
        this.zzajv = zzd;
        zzu.zzfq().zzad(this.zzajs.zzagf);
        zzu.zzft().zzb(this.zzajs.zzagf, this.zzajs.zzaow);
        this.zzaju = zzu.zzft().zzsu();
        zzdk();
    }

    private AdRequestParcel zza(AdRequestParcel adRequestParcel) {
        return (!zzi.zzcl(this.zzajs.zzagf) || adRequestParcel.zzatu == null) ? adRequestParcel : new zzf(adRequestParcel).zza(null).zzig();
    }

    private TimerTask zza(final Timer timer, final CountDownLatch countDownLatch) {
        return new TimerTask() {
            public void run() {
                if (((long) ((Integer) zzdc.zzbcl.get()).intValue()) != countDownLatch.getCount()) {
                    zzkd.zzcv("Stopping method tracing");
                    Debug.stopMethodTracing();
                    if (countDownLatch.getCount() == 0) {
                        timer.cancel();
                        return;
                    }
                }
                String concat = String.valueOf(zza.this.zzajs.zzagf.getPackageName()).concat("_adsTrace_");
                try {
                    zzkd.zzcv("Starting method tracing");
                    countDownLatch.countDown();
                    long currentTimeMillis = zzu.zzfu().currentTimeMillis();
                    StringBuilder sb = new StringBuilder(20 + String.valueOf(concat).length());
                    sb.append(concat);
                    sb.append(currentTimeMillis);
                    Debug.startMethodTracing(sb.toString(), ((Integer) zzdc.zzbcm.get()).intValue());
                } catch (Exception e) {
                    zzkd.zzd("Exception occurred while starting method tracing.", e);
                }
            }
        };
    }

    private void zzdk() {
        if (((Boolean) zzdc.zzbcj.get()).booleanValue()) {
            Timer timer = new Timer();
            timer.schedule(zza(timer, new CountDownLatch(((Integer) zzdc.zzbcl.get()).intValue())), 0, ((Long) zzdc.zzbck.get()).longValue());
        }
    }

    public void destroy() {
        zzab.zzhi("destroy must be called on the main UI thread.");
        this.zzajr.cancel();
        this.zzaju.zzj(this.zzajs.zzapb);
        this.zzajs.destroy();
    }

    public boolean isLoading() {
        return this.zzajq;
    }

    public boolean isReady() {
        zzab.zzhi("isLoaded must be called on the main UI thread.");
        return this.zzajs.zzaoy == null && this.zzajs.zzaoz == null && this.zzajs.zzapb != null;
    }

    public void onAdClicked() {
        if (this.zzajs.zzapb == null) {
            zzkd.zzcx("Ad state was null when trying to ping click URLs.");
            return;
        }
        zzkd.zzcv("Pinging click URLs.");
        this.zzajs.zzapd.zzrz();
        if (this.zzajs.zzapb.zzbnm != null) {
            zzu.zzfq().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb.zzbnm);
        }
        if (this.zzajs.zzape != null) {
            try {
                this.zzajs.zzape.onAdClicked();
            } catch (RemoteException e) {
                zzkd.zzd("Could not notify onAdClicked event.", e);
            }
        }
    }

    public void onAppEvent(String str, @Nullable String str2) {
        if (this.zzajs.zzapg != null) {
            try {
                this.zzajs.zzapg.onAppEvent(str, str2);
            } catch (RemoteException e) {
                zzkd.zzd("Could not call the AppEventListener.", e);
            }
        }
    }

    public void pause() {
        zzab.zzhi("pause must be called on the main UI thread.");
    }

    public void resume() {
        zzab.zzhi("resume must be called on the main UI thread.");
    }

    public void setManualImpressionsEnabled(boolean z) {
        throw new UnsupportedOperationException("Attempt to call setManualImpressionsEnabled for an unsupported ad type.");
    }

    public void setUserId(String str) {
        zzkd.zzcx("RewardedVideoAd.setUserId() is deprecated. Please do not call this method.");
    }

    public void stopLoading() {
        zzab.zzhi("stopLoading must be called on the main UI thread.");
        this.zzajq = false;
        this.zzajs.zzi(true);
    }

    /* access modifiers changed from: 0000 */
    public Bundle zza(@Nullable zzco zzco) {
        String str;
        String str2;
        if (zzco == null) {
            return null;
        }
        if (zzco.zzid()) {
            zzco.wakeup();
        }
        zzcl zzib = zzco.zzib();
        if (zzib != null) {
            str2 = zzib.zzhr();
            str = zzib.zzhs();
            String str3 = "In AdManager: loadAd, ";
            String valueOf = String.valueOf(zzib.toString());
            zzkd.zzcv(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            if (str2 != null) {
                zzu.zzft().zzcm(str2);
            }
        } else {
            str2 = zzu.zzft().zzsp();
            str = null;
        }
        if (str2 == null) {
            return null;
        }
        Bundle bundle = new Bundle(1);
        bundle.putString("fingerprint", str2);
        if (!str2.equals(str)) {
            bundle.putString("v_fp", str);
        }
        return bundle;
    }

    public void zza(AdSizeParcel adSizeParcel) {
        zzab.zzhi("setAdSize must be called on the main UI thread.");
        this.zzajs.zzapa = adSizeParcel;
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzbtm == null || this.zzajs.zzapw != 0)) {
            this.zzajs.zzapb.zzbtm.zza(adSizeParcel);
        }
        if (this.zzajs.zzaox != null) {
            if (this.zzajs.zzaox.getChildCount() > 1) {
                this.zzajs.zzaox.removeView(this.zzajs.zzaox.getNextView());
            }
            this.zzajs.zzaox.setMinimumWidth(adSizeParcel.widthPixels);
            this.zzajs.zzaox.setMinimumHeight(adSizeParcel.heightPixels);
            this.zzajs.zzaox.requestLayout();
        }
    }

    public void zza(@Nullable VideoOptionsParcel videoOptionsParcel) {
        zzab.zzhi("setVideoOptions must be called on the main UI thread.");
        this.zzajs.zzapp = videoOptionsParcel;
    }

    public void zza(com.google.android.gms.ads.internal.client.zzp zzp) {
        zzab.zzhi("setAdListener must be called on the main UI thread.");
        this.zzajs.zzape = zzp;
    }

    public void zza(zzq zzq) {
        zzab.zzhi("setAdListener must be called on the main UI thread.");
        this.zzajs.zzapf = zzq;
    }

    public void zza(zzw zzw) {
        zzab.zzhi("setAppEventListener must be called on the main UI thread.");
        this.zzajs.zzapg = zzw;
    }

    public void zza(zzy zzy) {
        zzab.zzhi("setCorrelationIdProvider must be called on the main UI thread");
        this.zzajs.zzaph = zzy;
    }

    public void zza(zzd zzd) {
        zzab.zzhi("setRewardedVideoAdListener can only be called from the UI thread.");
        this.zzajs.zzapr = zzd;
    }

    /* access modifiers changed from: protected */
    public void zza(@Nullable RewardItemParcel rewardItemParcel) {
        if (this.zzajs.zzapr != null) {
            String str = "";
            int i = 0;
            if (rewardItemParcel != null) {
                try {
                    str = rewardItemParcel.type;
                    i = rewardItemParcel.zzcid;
                } catch (RemoteException e) {
                    zzkd.zzd("Could not call RewardedVideoAdListener.onRewarded().", e);
                    return;
                }
            }
            this.zzajs.zzapr.zza(new zzjd(str, i));
        }
    }

    public void zza(zzdo zzdo) {
        throw new IllegalStateException("setOnCustomRenderedAdLoadedListener is not supported for current ad type");
    }

    public void zza(zzho zzho) {
        throw new IllegalStateException("setInAppPurchaseListener is not supported for current ad type");
    }

    public void zza(zzhs zzhs, String str) {
        throw new IllegalStateException("setPlayStorePurchaseParams is not supported for current ad type");
    }

    public void zza(com.google.android.gms.internal.zzju.zza zza) {
        if (zza.zzciq.zzccc != -1 && !TextUtils.isEmpty(zza.zzciq.zzccl)) {
            long zzs = zzs(zza.zzciq.zzccl);
            if (zzs != -1) {
                zzdi zzc = this.zzajn.zzc(zza.zzciq.zzccc + zzs);
                this.zzajn.zza(zzc, "stc");
            }
        }
        this.zzajn.zzas(zza.zzciq.zzccl);
        this.zzajn.zza(this.zzajo, "arf");
        this.zzajp = this.zzajn.zzkg();
        this.zzajn.zzh("gqi", zza.zzciq.zzccm);
        this.zzajs.zzaoy = null;
        this.zzajs.zzapc = zza;
        zza(zza, this.zzajn);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(com.google.android.gms.internal.zzju.zza zza, zzdk zzdk);

    public void zza(HashSet<zzjv> hashSet) {
        this.zzajs.zza(hashSet);
    }

    /* access modifiers changed from: protected */
    public abstract boolean zza(AdRequestParcel adRequestParcel, zzdk zzdk);

    /* access modifiers changed from: 0000 */
    public boolean zza(zzju zzju) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract boolean zza(@Nullable zzju zzju, zzju zzju2);

    /* access modifiers changed from: protected */
    public void zzb(View view) {
        com.google.android.gms.ads.internal.zzv.zza zza = this.zzajs.zzaox;
        if (zza != null) {
            zza.addView(view, zzu.zzfs().zztm());
        }
    }

    public void zzb(zzju zzju) {
        this.zzajn.zza(this.zzajp, "awr");
        this.zzajs.zzaoz = null;
        if (!(zzju.errorCode == -2 || zzju.errorCode == 3)) {
            zzu.zzft().zzb(this.zzajs.zzgl());
        }
        if (zzju.errorCode == -1) {
            this.zzajq = false;
            return;
        }
        if (zza(zzju)) {
            zzkd.zzcv("Ad refresh scheduled.");
        }
        if (zzju.errorCode != -2) {
            zzh(zzju.errorCode);
            return;
        }
        if (this.zzajs.zzapu == null) {
            this.zzajs.zzapu = new zzka(this.zzajs.zzaou);
        }
        this.zzaju.zzi(this.zzajs.zzapb);
        if (zza(this.zzajs.zzapb, zzju)) {
            this.zzajs.zzapb = zzju;
            this.zzajs.zzgu();
            this.zzajn.zzh("is_mraid", this.zzajs.zzapb.zzho() ? "1" : "0");
            this.zzajn.zzh("is_mediation", this.zzajs.zzapb.zzcby ? "1" : "0");
            if (!(this.zzajs.zzapb.zzbtm == null || this.zzajs.zzapb.zzbtm.zzuj() == null)) {
                this.zzajn.zzh("is_delay_pl", this.zzajs.zzapb.zzbtm.zzuj().zzuy() ? "1" : "0");
            }
            this.zzajn.zza(this.zzajo, "ttc");
            if (zzu.zzft().zzsl() != null) {
                zzu.zzft().zzsl().zza(this.zzajn);
            }
            if (this.zzajs.zzgp()) {
                zzdu();
            }
        }
        if (zzju.zzbnp != null) {
            zzu.zzfq().zza(this.zzajs.zzagf, zzju.zzbnp);
        }
    }

    public boolean zzb(AdRequestParcel adRequestParcel) {
        zzab.zzhi("loadAd must be called on the main UI thread.");
        AdRequestParcel zza = zza(adRequestParcel);
        if (this.zzajs.zzaoy == null && this.zzajs.zzaoz == null) {
            zzkd.zzcw("Starting ad request.");
            zzdl();
            this.zzajo = this.zzajn.zzkg();
            if (!zza.zzatp) {
                String valueOf = String.valueOf(zzm.zziw().zzaq(this.zzajs.zzagf));
                StringBuilder sb = new StringBuilder(71 + String.valueOf(valueOf).length());
                sb.append("Use AdRequest.Builder.addTestDevice(\"");
                sb.append(valueOf);
                sb.append("\") to get test ads on this device.");
                zzkd.zzcw(sb.toString());
            }
            this.zzajq = zza(zza, this.zzajn);
            return this.zzajq;
        }
        zzkd.zzcx(this.zzajt != null ? "Aborting last ad request since another ad request is already in progress. The current request object will still be cached for future refreshes." : "Loading already in progress, saving this object for future refreshes.");
        this.zzajt = zza;
        return false;
    }

    /* access modifiers changed from: protected */
    public void zzc(@Nullable zzju zzju) {
        if (zzju == null) {
            zzkd.zzcx("Ad state was null when trying to ping impression URLs.");
            return;
        }
        zzkd.zzcv("Pinging Impression URLs.");
        this.zzajs.zzapd.zzry();
        if (zzju.zzbnn != null && !zzju.zzcin) {
            zzu.zzfq().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, zzju.zzbnn);
            zzju.zzcin = true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzc(AdRequestParcel adRequestParcel) {
        if (this.zzajs.zzaox == null) {
            return false;
        }
        ViewParent parent = this.zzajs.zzaox.getParent();
        if (!(parent instanceof View)) {
            return false;
        }
        View view = (View) parent;
        return zzu.zzfq().zza(view, view.getContext());
    }

    public void zzd(AdRequestParcel adRequestParcel) {
        if (zzc(adRequestParcel)) {
            zzb(adRequestParcel);
            return;
        }
        zzkd.zzcw("Ad is not visible. Not refreshing ad.");
        this.zzajr.zzg(adRequestParcel);
    }

    public void zzdl() {
        this.zzajn = new zzdk(((Boolean) zzdc.zzaze.get()).booleanValue(), "load_ad", this.zzajs.zzapa.zzaur);
        this.zzajo = new zzdi(-1, null, null);
        this.zzajp = new zzdi(-1, null, null);
    }

    public com.google.android.gms.dynamic.zzd zzdm() {
        zzab.zzhi("getAdFrame must be called on the main UI thread.");
        return zze.zzac(this.zzajs.zzaox);
    }

    @Nullable
    public AdSizeParcel zzdn() {
        zzab.zzhi("getAdSize must be called on the main UI thread.");
        if (this.zzajs.zzapa == null) {
            return null;
        }
        return new ThinAdSizeParcel(this.zzajs.zzapa);
    }

    public void zzdo() {
        zzds();
    }

    public void zzdp() {
        zzab.zzhi("recordManualImpression must be called on the main UI thread.");
        if (this.zzajs.zzapb == null) {
            zzkd.zzcx("Ad state was null when trying to ping manual tracking URLs.");
            return;
        }
        zzkd.zzcv("Pinging manual tracking URLs.");
        if (this.zzajs.zzapb.zzcca != null && !this.zzajs.zzapb.zzcio) {
            zzu.zzfq().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb.zzcca);
            this.zzajs.zzapb.zzcio = true;
        }
    }

    public com.google.android.gms.ads.internal.client.zzab zzdq() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void zzdr() {
        zzkd.zzcw("Ad closing.");
        if (this.zzajs.zzapf != null) {
            try {
                this.zzajs.zzapf.onAdClosed();
            } catch (RemoteException e) {
                zzkd.zzd("Could not call AdListener.onAdClosed().", e);
            }
        }
        if (this.zzajs.zzapr != null) {
            try {
                this.zzajs.zzapr.onRewardedVideoAdClosed();
            } catch (RemoteException e2) {
                zzkd.zzd("Could not call RewardedVideoAdListener.onRewardedVideoAdClosed().", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzds() {
        zzkd.zzcw("Ad leaving application.");
        if (this.zzajs.zzapf != null) {
            try {
                this.zzajs.zzapf.onAdLeftApplication();
            } catch (RemoteException e) {
                zzkd.zzd("Could not call AdListener.onAdLeftApplication().", e);
            }
        }
        if (this.zzajs.zzapr != null) {
            try {
                this.zzajs.zzapr.onRewardedVideoAdLeftApplication();
            } catch (RemoteException e2) {
                zzkd.zzd("Could not call  RewardedVideoAdListener.onRewardedVideoAdLeftApplication().", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzdt() {
        zzkd.zzcw("Ad opening.");
        if (this.zzajs.zzapf != null) {
            try {
                this.zzajs.zzapf.onAdOpened();
            } catch (RemoteException e) {
                zzkd.zzd("Could not call AdListener.onAdOpened().", e);
            }
        }
        if (this.zzajs.zzapr != null) {
            try {
                this.zzajs.zzapr.onRewardedVideoAdOpened();
            } catch (RemoteException e2) {
                zzkd.zzd("Could not call RewardedVideoAdListener.onRewardedVideoAdOpened().", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzdu() {
        zzkd.zzcw("Ad finished loading.");
        this.zzajq = false;
        if (this.zzajs.zzapf != null) {
            try {
                this.zzajs.zzapf.onAdLoaded();
            } catch (RemoteException e) {
                zzkd.zzd("Could not call AdListener.onAdLoaded().", e);
            }
        }
        if (this.zzajs.zzapr != null) {
            try {
                this.zzajs.zzapr.onRewardedVideoAdLoaded();
            } catch (RemoteException e2) {
                zzkd.zzd("Could not call RewardedVideoAdListener.onRewardedVideoAdLoaded().", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzdv() {
        if (this.zzajs.zzapr != null) {
            try {
                this.zzajs.zzapr.onRewardedVideoStarted();
            } catch (RemoteException e) {
                zzkd.zzd("Could not call RewardedVideoAdListener.onVideoStarted().", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzh(int i) {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Failed to load ad: ");
        sb.append(i);
        zzkd.zzcx(sb.toString());
        this.zzajq = false;
        if (this.zzajs.zzapf != null) {
            try {
                this.zzajs.zzapf.onAdFailedToLoad(i);
            } catch (RemoteException e) {
                zzkd.zzd("Could not call AdListener.onAdFailedToLoad().", e);
            }
        }
        if (this.zzajs.zzapr != null) {
            try {
                this.zzajs.zzapr.onRewardedVideoAdFailedToLoad(i);
            } catch (RemoteException e2) {
                zzkd.zzd("Could not call RewardedVideoAdListener.onRewardedVideoAdFailedToLoad().", e2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public long zzs(String str) {
        String str2;
        int indexOf = str.indexOf("ufe");
        int indexOf2 = str.indexOf(44, indexOf);
        if (indexOf2 == -1) {
            indexOf2 = str.length();
        }
        try {
            return Long.parseLong(str.substring(indexOf + 4, indexOf2));
        } catch (IndexOutOfBoundsException unused) {
            str2 = "Invalid index for Url fetch time in CSI latency info.";
            zzkd.zzcx(str2);
            return -1;
        } catch (NumberFormatException unused2) {
            str2 = "Cannot find valid format of Url fetch time in CSI latency info.";
            zzkd.zzcx(str2);
            return -1;
        }
    }
}
