package com.google.android.gms.ads.internal.formats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj implements Creator<NativeAdOptionsParcel> {
    static void zza(NativeAdOptionsParcel nativeAdOptionsParcel, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, nativeAdOptionsParcel.versionCode);
        zzb.zza(parcel, 2, nativeAdOptionsParcel.zzbgp);
        zzb.zzc(parcel, 3, nativeAdOptionsParcel.zzbgq);
        zzb.zza(parcel, 4, nativeAdOptionsParcel.zzbgr);
        zzb.zzc(parcel, 5, nativeAdOptionsParcel.zzbgs);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzg */
    public NativeAdOptionsParcel createFromParcel(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    z = zza.zzc(parcel, zzcl);
                    break;
                case 3:
                    i2 = zza.zzg(parcel, zzcl);
                    break;
                case 4:
                    z2 = zza.zzc(parcel, zzcl);
                    break;
                case 5:
                    i3 = zza.zzg(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel);
        }
        NativeAdOptionsParcel nativeAdOptionsParcel = new NativeAdOptionsParcel(i, z, i2, z2, i3);
        return nativeAdOptionsParcel;
    }

    /* renamed from: zzw */
    public NativeAdOptionsParcel[] newArray(int i) {
        return new NativeAdOptionsParcel[i];
    }
}
