package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi implements Creator<AdSizeParcel> {
    static void zza(AdSizeParcel adSizeParcel, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, adSizeParcel.versionCode);
        zzb.zza(parcel, 2, adSizeParcel.zzaur, false);
        zzb.zzc(parcel, 3, adSizeParcel.height);
        zzb.zzc(parcel, 4, adSizeParcel.heightPixels);
        zzb.zza(parcel, 5, adSizeParcel.zzaus);
        zzb.zzc(parcel, 6, adSizeParcel.width);
        zzb.zzc(parcel, 7, adSizeParcel.widthPixels);
        zzb.zza(parcel, 8, (T[]) adSizeParcel.zzaut, i, false);
        zzb.zza(parcel, 9, adSizeParcel.zzauu);
        zzb.zza(parcel, 10, adSizeParcel.zzauv);
        zzb.zza(parcel, 11, adSizeParcel.zzauw);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzd */
    public AdSizeParcel createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int zzcm = zza.zzcm(parcel);
        String str = null;
        AdSizeParcel[] adSizeParcelArr = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        int i5 = 0;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel2, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel2, zzcl);
                    break;
                case 3:
                    i2 = zza.zzg(parcel2, zzcl);
                    break;
                case 4:
                    i3 = zza.zzg(parcel2, zzcl);
                    break;
                case 5:
                    z = zza.zzc(parcel2, zzcl);
                    break;
                case 6:
                    i4 = zza.zzg(parcel2, zzcl);
                    break;
                case 7:
                    i5 = zza.zzg(parcel2, zzcl);
                    break;
                case 8:
                    adSizeParcelArr = (AdSizeParcel[]) zza.zzb(parcel2, zzcl, AdSizeParcel.CREATOR);
                    break;
                case 9:
                    z2 = zza.zzc(parcel2, zzcl);
                    break;
                case 10:
                    z3 = zza.zzc(parcel2, zzcl);
                    break;
                case 11:
                    z4 = zza.zzc(parcel2, zzcl);
                    break;
                default:
                    zza.zzb(parcel2, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel2);
        }
        AdSizeParcel adSizeParcel = new AdSizeParcel(i, str, i2, i3, z, i4, i5, adSizeParcelArr, z2, z3, z4);
        return adSizeParcel;
    }

    /* renamed from: zzs */
    public AdSizeParcel[] newArray(int i) {
        return new AdSizeParcel[i];
    }
}
