package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.ads.internal.formats.zze;
import com.google.android.gms.ads.internal.zzf.zza;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzge;
import com.google.android.gms.internal.zzgn;
import com.google.android.gms.internal.zzgo;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zzli;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzn {
    private static zzd zza(zzgn zzgn) throws RemoteException {
        zzd zzd = new zzd(zzgn.getHeadline(), zzgn.getImages(), zzgn.getBody(), zzgn.zzku(), zzgn.getCallToAction(), zzgn.getStarRating(), zzgn.getStore(), zzgn.getPrice(), null, zzgn.getExtras());
        return zzd;
    }

    private static zze zza(zzgo zzgo) throws RemoteException {
        zze zze = new zze(zzgo.getHeadline(), zzgo.getImages(), zzgo.getBody(), zzgo.zzky(), zzgo.getCallToAction(), zzgo.getAdvertiser(), null, zzgo.getExtras());
        return zze;
    }

    static zzep zza(@Nullable final zzgn zzgn, @Nullable final zzgo zzgo, final zza zza) {
        return new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                zza zza;
                View view = zzlh.getView();
                if (view != null) {
                    try {
                        if (zzgn.this != null) {
                            if (!zzgn.this.getOverrideClickHandling()) {
                                zzgn.this.zzk(com.google.android.gms.dynamic.zze.zzac(view));
                                zza = zza;
                            }
                            zzn.zza(zzlh);
                            return;
                        }
                        if (zzgo != null) {
                            if (!zzgo.getOverrideClickHandling()) {
                                zzgo.zzk(com.google.android.gms.dynamic.zze.zzac(view));
                                zza = zza;
                            }
                            zzn.zza(zzlh);
                            return;
                        }
                        return;
                        zza.onClick();
                    } catch (RemoteException e) {
                        zzkd.zzd("Unable to call handleClick on mapper", e);
                    }
                }
            }
        };
    }

    static zzep zza(final CountDownLatch countDownLatch) {
        return new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                countDownLatch.countDown();
                zzlh.getView().setVisibility(0);
            }
        };
    }

    private static String zza(@Nullable Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap == null) {
            zzkd.zzcx("Bitmap is null. Returning empty string");
            return "";
        }
        bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        String valueOf = String.valueOf("data:image/png;base64,");
        String valueOf2 = String.valueOf(encodeToString);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    static String zza(@Nullable zzdr zzdr) {
        if (zzdr == null) {
            zzkd.zzcx("Image is null. Returning empty string");
            return "";
        }
        try {
            Uri uri = zzdr.getUri();
            if (uri != null) {
                return uri.toString();
            }
        } catch (RemoteException unused) {
            zzkd.zzcx("Unable to get image uri. Trying data uri next");
        }
        return zzb(zzdr);
    }

    /* access modifiers changed from: private */
    public static JSONObject zza(@Nullable Bundle bundle, String str) throws JSONException {
        String valueOf;
        String str2;
        JSONObject jSONObject = new JSONObject();
        if (bundle == null || TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str3 = (String) keys.next();
            if (bundle.containsKey(str3)) {
                if ("image".equals(jSONObject2.getString(str3))) {
                    Object obj = bundle.get(str3);
                    if (obj instanceof Bitmap) {
                        valueOf = zza((Bitmap) obj);
                    } else {
                        str2 = "Invalid type. An image type extra should return a bitmap";
                        zzkd.zzcx(str2);
                    }
                } else if (bundle.get(str3) instanceof Bitmap) {
                    str2 = "Invalid asset type. Bitmap should be returned only for image type";
                    zzkd.zzcx(str2);
                } else {
                    valueOf = String.valueOf(bundle.get(str3));
                }
                jSONObject.put(str3, valueOf);
            }
        }
        return jSONObject;
    }

    public static void zza(@Nullable zzju zzju, zza zza) {
        if (zzju != null && zzg(zzju)) {
            zzlh zzlh = zzju.zzbtm;
            Object view = zzlh != null ? zzlh.getView() : null;
            if (view == null) {
                zzkd.zzcx("AdWebView is null");
                return;
            }
            try {
                List list = zzju.zzbon != null ? zzju.zzbon.zzbni : null;
                if (list != null) {
                    if (!list.isEmpty()) {
                        zzgn zzmo = zzju.zzboo != null ? zzju.zzboo.zzmo() : null;
                        zzgo zzmp = zzju.zzboo != null ? zzju.zzboo.zzmp() : null;
                        if (list.contains("2") && zzmo != null) {
                            zzmo.zzl(com.google.android.gms.dynamic.zze.zzac(view));
                            if (!zzmo.getOverrideImpressionRecording()) {
                                zzmo.recordImpression();
                            }
                            zzlh.zzuj().zza("/nativeExpressViewClicked", zza(zzmo, (zzgo) null, zza));
                            return;
                        } else if (!list.contains("1") || zzmp == null) {
                            zzkd.zzcx("No matching template id and mapper");
                            return;
                        } else {
                            zzmp.zzl(com.google.android.gms.dynamic.zze.zzac(view));
                            if (!zzmp.getOverrideImpressionRecording()) {
                                zzmp.recordImpression();
                            }
                            zzlh.zzuj().zza("/nativeExpressViewClicked", zza((zzgn) null, zzmp, zza));
                            return;
                        }
                    }
                }
                zzkd.zzcx("No template ids present in mediation response");
            } catch (RemoteException e) {
                zzkd.zzd("Error occurred while recording impression and registering for clicks", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void zza(zzlh zzlh) {
        OnClickListener zzuw = zzlh.zzuw();
        if (zzuw != null) {
            zzuw.onClick(zzlh.getView());
        }
    }

    private static void zza(final zzlh zzlh, final zzd zzd, final String str) {
        zzlh.zzuj().zza((zzli.zza) new zzli.zza() {
            public void zza(zzlh zzlh, boolean z) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("headline", zzd.this.getHeadline());
                    jSONObject.put("body", zzd.this.getBody());
                    jSONObject.put("call_to_action", zzd.this.getCallToAction());
                    jSONObject.put(Param.PRICE, zzd.this.getPrice());
                    jSONObject.put("star_rating", String.valueOf(zzd.this.getStarRating()));
                    jSONObject.put("store", zzd.this.getStore());
                    jSONObject.put("icon", zzn.zza(zzd.this.zzku()));
                    JSONArray jSONArray = new JSONArray();
                    List<Object> images = zzd.this.getImages();
                    if (images != null) {
                        for (Object zzf : images) {
                            jSONArray.put(zzn.zza(zzn.zze(zzf)));
                        }
                    }
                    jSONObject.put("images", jSONArray);
                    jSONObject.put("extras", zzn.zza(zzd.this.getExtras(), str));
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("assets", jSONObject);
                    jSONObject2.put("template_id", "2");
                    zzlh.zza("google.afma.nativeExpressAds.loadAssets", jSONObject2);
                } catch (JSONException e) {
                    zzkd.zzd("Exception occurred when loading assets", e);
                }
            }
        });
    }

    private static void zza(final zzlh zzlh, final zze zze, final String str) {
        zzlh.zzuj().zza((zzli.zza) new zzli.zza() {
            public void zza(zzlh zzlh, boolean z) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("headline", zze.this.getHeadline());
                    jSONObject.put("body", zze.this.getBody());
                    jSONObject.put("call_to_action", zze.this.getCallToAction());
                    jSONObject.put("advertiser", zze.this.getAdvertiser());
                    jSONObject.put("logo", zzn.zza(zze.this.zzky()));
                    JSONArray jSONArray = new JSONArray();
                    List<Object> images = zze.this.getImages();
                    if (images != null) {
                        for (Object zzf : images) {
                            jSONArray.put(zzn.zza(zzn.zze(zzf)));
                        }
                    }
                    jSONObject.put("images", jSONArray);
                    jSONObject.put("extras", zzn.zza(zze.this.getExtras(), str));
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("assets", jSONObject);
                    jSONObject2.put("template_id", "1");
                    zzlh.zza("google.afma.nativeExpressAds.loadAssets", jSONObject2);
                } catch (JSONException e) {
                    zzkd.zzd("Exception occurred when loading assets", e);
                }
            }
        });
    }

    private static void zza(zzlh zzlh, CountDownLatch countDownLatch) {
        zzlh.zzuj().zza("/nativeExpressAssetsLoaded", zza(countDownLatch));
        zzlh.zzuj().zza("/nativeExpressAssetsLoadingFailed", zzb(countDownLatch));
    }

    public static boolean zza(zzlh zzlh, zzge zzge, CountDownLatch countDownLatch) {
        boolean z;
        try {
            z = zzb(zzlh, zzge, countDownLatch);
        } catch (RemoteException e) {
            zzkd.zzd("Unable to invoke load assets", e);
            z = false;
        } catch (RuntimeException e2) {
            countDownLatch.countDown();
            throw e2;
        }
        if (!z) {
            countDownLatch.countDown();
        }
        return z;
    }

    static zzep zzb(final CountDownLatch countDownLatch) {
        return new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                zzkd.zzcx("Adapter returned an ad, but assets substitution failed");
                countDownLatch.countDown();
                zzlh.destroy();
            }
        };
    }

    private static String zzb(zzdr zzdr) {
        try {
            com.google.android.gms.dynamic.zzd zzkt = zzdr.zzkt();
            if (zzkt == null) {
                zzkd.zzcx("Drawable is null. Returning empty string");
                return "";
            }
            Drawable drawable = (Drawable) com.google.android.gms.dynamic.zze.zzad(zzkt);
            if (drawable instanceof BitmapDrawable) {
                return zza(((BitmapDrawable) drawable).getBitmap());
            }
            zzkd.zzcx("Drawable is not an instance of BitmapDrawable. Returning empty string");
            return "";
        } catch (RemoteException unused) {
            zzkd.zzcx("Unable to get drawable. Returning empty string");
            return "";
        }
    }

    private static boolean zzb(zzlh zzlh, zzge zzge, CountDownLatch countDownLatch) throws RemoteException {
        String str;
        View view = zzlh.getView();
        if (view == null) {
            str = "AdWebView is null";
        } else {
            view.setVisibility(4);
            List<String> list = zzge.zzbon.zzbni;
            if (list == null || list.isEmpty()) {
                str = "No template ids present in mediation response";
            } else {
                zza(zzlh, countDownLatch);
                zzgn zzmo = zzge.zzboo.zzmo();
                zzgo zzmp = zzge.zzboo.zzmp();
                if (list.contains("2") && zzmo != null) {
                    zza(zzlh, zza(zzmo), zzge.zzbon.zzbnh);
                } else if (!list.contains("1") || zzmp == null) {
                    str = "No matching template id and mapper";
                } else {
                    zza(zzlh, zza(zzmp), zzge.zzbon.zzbnh);
                }
                String str2 = zzge.zzbon.zzbnf;
                String str3 = zzge.zzbon.zzbng;
                if (str3 != null) {
                    zzlh.loadDataWithBaseURL(str3, str2, "text/html", "UTF-8", null);
                } else {
                    zzlh.loadData(str2, "text/html", "UTF-8");
                }
                return true;
            }
        }
        zzkd.zzcx(str);
        return false;
    }

    /* access modifiers changed from: private */
    @Nullable
    public static zzdr zze(Object obj) {
        if (obj instanceof IBinder) {
            return zzdr.zza.zzy((IBinder) obj);
        }
        return null;
    }

    @Nullable
    public static View zzf(@Nullable zzju zzju) {
        if (zzju == null) {
            zzkd.e("AdState is null");
            return null;
        } else if (zzg(zzju) && zzju.zzbtm != null) {
            return zzju.zzbtm.getView();
        } else {
            try {
                com.google.android.gms.dynamic.zzd view = zzju.zzboo != null ? zzju.zzboo.getView() : null;
                if (view != null) {
                    return (View) com.google.android.gms.dynamic.zze.zzad(view);
                }
                zzkd.zzcx("View in mediation adapter is null.");
                return null;
            } catch (RemoteException e) {
                zzkd.zzd("Could not get View from mediation adapter.", e);
                return null;
            }
        }
    }

    public static boolean zzg(@Nullable zzju zzju) {
        return (zzju == null || !zzju.zzcby || zzju.zzbon == null || zzju.zzbon.zzbnf == null) ? false : true;
    }
}
