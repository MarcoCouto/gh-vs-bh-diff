package com.google.android.gms.ads.internal.request;

import android.content.Context;
import com.google.android.gms.internal.zzas;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkc;
import java.util.concurrent.Future;

@zzin
public class zza {

    /* renamed from: com.google.android.gms.ads.internal.request.zza$zza reason: collision with other inner class name */
    public interface C0015zza {
        void zza(com.google.android.gms.internal.zzju.zza zza);
    }

    public zzkc zza(Context context, com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zza, zzas zzas, C0015zza zza2) {
        zzkc zzn = zza.zzcar.extras.getBundle("sdk_less_server_data") != null ? new zzn(context, zza, zza2) : new zzb(context, zza, zzas, zza2);
        Future future = (Future) zzn.zzpy();
        return zzn;
    }
}
