package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.internal.zzin;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@zzin
public class zzh {
    public static final zzh zzauq = new zzh();

    protected zzh() {
    }

    public static zzh zzih() {
        return zzauq;
    }

    public AdRequestParcel zza(Context context, zzad zzad) {
        List list;
        Context context2;
        zzad zzad2 = zzad;
        Date birthday = zzad.getBirthday();
        long time = birthday != null ? birthday.getTime() : -1;
        String contentUrl = zzad.getContentUrl();
        int gender = zzad.getGender();
        Set keywords = zzad.getKeywords();
        if (!keywords.isEmpty()) {
            list = Collections.unmodifiableList(new ArrayList(keywords));
            context2 = context;
        } else {
            context2 = context;
            list = null;
        }
        boolean isTestDevice = zzad2.isTestDevice(context2);
        int zzji = zzad.zzji();
        Location location = zzad.getLocation();
        Bundle networkExtrasBundle = zzad2.getNetworkExtrasBundle(AdMobAdapter.class);
        boolean manualImpressionsEnabled = zzad.getManualImpressionsEnabled();
        String publisherProvidedId = zzad.getPublisherProvidedId();
        SearchAdRequest zzjf = zzad.zzjf();
        SearchAdRequestParcel searchAdRequestParcel = zzjf != null ? new SearchAdRequestParcel(zzjf) : null;
        Context applicationContext = context.getApplicationContext();
        AdRequestParcel adRequestParcel = new AdRequestParcel(7, time, networkExtrasBundle, gender, list, isTestDevice, zzji, manualImpressionsEnabled, publisherProvidedId, searchAdRequestParcel, location, contentUrl, zzad.zzjh(), zzad.getCustomTargeting(), Collections.unmodifiableList(new ArrayList(zzad.zzjj())), zzad.zzje(), applicationContext != null ? zzm.zziw().zza(Thread.currentThread().getStackTrace(), applicationContext.getPackageName()) : null, zzad.isDesignedForFamilies());
        return adRequestParcel;
    }

    public RewardedVideoAdRequestParcel zza(Context context, zzad zzad, String str) {
        return new RewardedVideoAdRequestParcel(zza(context, zzad), str);
    }
}
