package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzdi;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzlh;

@zzin
public class zzn extends zzj {
    @Nullable
    public zzi zza(Context context, zzlh zzlh, int i, boolean z, zzdk zzdk, zzdi zzdi) {
        if (!zzq(context)) {
            return null;
        }
        boolean zzg = zzg(zzlh);
        zzx zzx = new zzx(context, zzlh.zzum(), zzlh.getRequestId(), zzdk, zzdi);
        return new zzc(context, z, zzg, zzx);
    }
}
