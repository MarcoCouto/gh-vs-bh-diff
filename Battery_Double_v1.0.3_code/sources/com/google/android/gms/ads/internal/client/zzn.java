package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.client.zzy.zza;
import com.google.android.gms.internal.zzin;
import java.util.Random;

@zzin
public class zzn extends zza {
    private Object zzail = new Object();
    private final Random zzavp = new Random();
    private long zzavq;

    public zzn() {
        zziy();
    }

    public long getValue() {
        return this.zzavq;
    }

    public void zziy() {
        synchronized (this.zzail) {
            int i = 3;
            long j = 0;
            while (true) {
                i--;
                if (i <= 0) {
                    break;
                }
                try {
                    long nextInt = ((long) this.zzavp.nextInt()) + 2147483648L;
                    if (nextInt != this.zzavq && nextInt != 0) {
                        j = nextInt;
                        break;
                    }
                    j = nextInt;
                } finally {
                }
            }
            this.zzavq = j;
        }
    }
}
