package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.Parcel;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.zza;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzin;

@zzin
public class AdSizeParcel extends AbstractSafeParcelable {
    public static final zzi CREATOR = new zzi();
    public final int height;
    public final int heightPixels;
    public final int versionCode;
    public final int width;
    public final int widthPixels;
    public final String zzaur;
    public final boolean zzaus;
    public final AdSizeParcel[] zzaut;
    public final boolean zzauu;
    public final boolean zzauv;
    public boolean zzauw;

    public AdSizeParcel() {
        this(5, "interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    AdSizeParcel(int i, String str, int i2, int i3, boolean z, int i4, int i5, AdSizeParcel[] adSizeParcelArr, boolean z2, boolean z3, boolean z4) {
        this.versionCode = i;
        this.zzaur = str;
        this.height = i2;
        this.heightPixels = i3;
        this.zzaus = z;
        this.width = i4;
        this.widthPixels = i5;
        this.zzaut = adSizeParcelArr;
        this.zzauu = z2;
        this.zzauv = z3;
        this.zzauw = z4;
    }

    public AdSizeParcel(Context context, AdSize adSize) {
        this(context, new AdSize[]{adSize});
    }

    public AdSizeParcel(Context context, AdSize[] adSizeArr) {
        int height2;
        int i;
        String str;
        AdSize adSize = adSizeArr[0];
        this.versionCode = 5;
        this.zzaus = false;
        this.zzauv = adSize.isFluid();
        if (this.zzauv) {
            this.width = AdSize.BANNER.getWidth();
            height2 = AdSize.BANNER.getHeight();
        } else {
            this.width = adSize.getWidth();
            height2 = adSize.getHeight();
        }
        this.height = height2;
        boolean z = this.width == -1;
        boolean z2 = this.height == -2;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        if (z) {
            this.widthPixels = (!zzm.zziw().zzas(context) || !zzm.zziw().zzat(context)) ? zza(displayMetrics) : zza(displayMetrics) - zzm.zziw().zzau(context);
            double d = (double) (((float) this.widthPixels) / displayMetrics.density);
            i = (int) d;
            if (d - ((double) i) >= 0.01d) {
                i++;
            }
        } else {
            i = this.width;
            this.widthPixels = zzm.zziw().zza(displayMetrics, this.width);
        }
        int zzc = z2 ? zzc(displayMetrics) : this.height;
        this.heightPixels = zzm.zziw().zza(displayMetrics, zzc);
        if (z || z2) {
            StringBuilder sb = new StringBuilder(26);
            sb.append(i);
            sb.append("x");
            sb.append(zzc);
            sb.append("_as");
            str = sb.toString();
        } else {
            str = this.zzauv ? "320x50_mb" : adSize.toString();
        }
        this.zzaur = str;
        if (adSizeArr.length > 1) {
            this.zzaut = new AdSizeParcel[adSizeArr.length];
            for (int i2 = 0; i2 < adSizeArr.length; i2++) {
                this.zzaut[i2] = new AdSizeParcel(context, adSizeArr[i2]);
            }
        } else {
            this.zzaut = null;
        }
        this.zzauu = false;
        this.zzauw = false;
    }

    public AdSizeParcel(AdSizeParcel adSizeParcel, AdSizeParcel[] adSizeParcelArr) {
        this(5, adSizeParcel.zzaur, adSizeParcel.height, adSizeParcel.heightPixels, adSizeParcel.zzaus, adSizeParcel.width, adSizeParcel.widthPixels, adSizeParcelArr, adSizeParcel.zzauu, adSizeParcel.zzauv, adSizeParcel.zzauw);
    }

    public static int zza(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    public static int zzb(DisplayMetrics displayMetrics) {
        return (int) (((float) zzc(displayMetrics)) * displayMetrics.density);
    }

    private static int zzc(DisplayMetrics displayMetrics) {
        int i = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i <= 400) {
            return 32;
        }
        return i <= 720 ? 50 : 90;
    }

    public static AdSizeParcel zzii() {
        AdSizeParcel adSizeParcel = new AdSizeParcel(5, "reward_mb", 0, 0, true, 0, 0, null, false, false, false);
        return adSizeParcel;
    }

    public static AdSizeParcel zzk(Context context) {
        AdSizeParcel adSizeParcel = new AdSizeParcel(5, "320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
        return adSizeParcel;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzi.zza(this, parcel, i);
    }

    public AdSize zzij() {
        return zza.zza(this.width, this.height, this.zzaur);
    }

    public void zzk(boolean z) {
        this.zzauw = z;
    }
}
