package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

@zzin
public final class LargeParcelTeleporter extends AbstractSafeParcelable {
    public static final Creator<LargeParcelTeleporter> CREATOR = new zzm();
    final int mVersionCode;
    ParcelFileDescriptor zzccz;
    private Parcelable zzcda;
    private boolean zzcdb;

    LargeParcelTeleporter(int i, ParcelFileDescriptor parcelFileDescriptor) {
        this.mVersionCode = i;
        this.zzccz = parcelFileDescriptor;
        this.zzcda = null;
        this.zzcdb = true;
    }

    public LargeParcelTeleporter(SafeParcelable safeParcelable) {
        this.mVersionCode = 1;
        this.zzccz = null;
        this.zzcda = safeParcelable;
        this.zzcdb = false;
    }

    /* JADX INFO: finally extract failed */
    public void writeToParcel(Parcel parcel, int i) {
        if (this.zzccz == null) {
            Parcel obtain = Parcel.obtain();
            try {
                this.zzcda.writeToParcel(obtain, 0);
                byte[] marshall = obtain.marshall();
                obtain.recycle();
                this.zzccz = zzi(marshall);
            } catch (Throwable th) {
                obtain.recycle();
                throw th;
            }
        }
        zzm.zza(this, parcel, i);
    }

    /* JADX INFO: finally extract failed */
    public <T extends SafeParcelable> T zza(Creator<T> creator) {
        if (this.zzcdb) {
            if (this.zzccz == null) {
                zzkd.e("File descriptor is empty, returning null.");
                return null;
            }
            DataInputStream dataInputStream = new DataInputStream(new AutoCloseInputStream(this.zzccz));
            try {
                byte[] bArr = new byte[dataInputStream.readInt()];
                dataInputStream.readFully(bArr, 0, bArr.length);
                zzo.zzb(dataInputStream);
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.unmarshall(bArr, 0, bArr.length);
                    obtain.setDataPosition(0);
                    this.zzcda = (SafeParcelable) creator.createFromParcel(obtain);
                    obtain.recycle();
                    this.zzcdb = false;
                } catch (Throwable th) {
                    obtain.recycle();
                    throw th;
                }
            } catch (IOException e) {
                throw new IllegalStateException("Could not read from parcel file descriptor", e);
            } catch (Throwable th2) {
                zzo.zzb(dataInputStream);
                throw th2;
            }
        }
        return (SafeParcelable) this.zzcda;
    }

    /* access modifiers changed from: protected */
    public <T> ParcelFileDescriptor zzi(final byte[] bArr) {
        final AutoCloseOutputStream autoCloseOutputStream;
        try {
            ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
            autoCloseOutputStream = new AutoCloseOutputStream(createPipe[1]);
            try {
                new Thread(new Runnable() {
                    /* JADX WARNING: type inference failed for: r1v0 */
                    /* JADX WARNING: type inference failed for: r1v1, types: [java.io.Closeable] */
                    /* JADX WARNING: type inference failed for: r1v2, types: [java.io.OutputStream] */
                    /* JADX WARNING: type inference failed for: r1v3, types: [java.io.Closeable] */
                    /* JADX WARNING: type inference failed for: r1v4 */
                    /* JADX WARNING: type inference failed for: r1v6 */
                    /* JADX WARNING: type inference failed for: r1v8 */
                    /* JADX WARNING: type inference failed for: r1v10 */
                    /* JADX WARNING: type inference failed for: r1v11 */
                    /* JADX WARNING: type inference failed for: r1v12 */
                    /* JADX WARNING: type inference failed for: r1v13 */
                    /* JADX WARNING: Multi-variable type inference failed */
                    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b  */
                    /* JADX WARNING: Unknown variable types count: 3 */
                    public void run() {
                        ? r1;
                        ? r12;
                        Throwable e;
                        DataOutputStream dataOutputStream;
                        try {
                            dataOutputStream = new DataOutputStream(autoCloseOutputStream);
                            try {
                                dataOutputStream.writeInt(bArr.length);
                                dataOutputStream.write(bArr);
                                r12 = dataOutputStream;
                            } catch (IOException e2) {
                                e = e2;
                                dataOutputStream = dataOutputStream;
                            }
                        } catch (IOException e3) {
                            Throwable th = e3;
                            dataOutputStream = 0;
                            e = th;
                            try {
                                zzkd.zzb("Error transporting the ad response", e);
                                zzu.zzft().zzb(e, true);
                                if (r12 == 0) {
                                    zzo.zzb(autoCloseOutputStream);
                                    return;
                                }
                                zzo.zzb(r12);
                            } catch (Throwable th2) {
                                th = th2;
                                r1 = r12;
                                if (r1 == 0) {
                                    r1 = autoCloseOutputStream;
                                }
                                zzo.zzb(r1);
                                throw th;
                            }
                        } catch (Throwable th3) {
                            Throwable th4 = th3;
                            r1 = 0;
                            th = th4;
                            if (r1 == 0) {
                            }
                            zzo.zzb(r1);
                            throw th;
                        }
                        zzo.zzb(r12);
                    }
                }).start();
                return createPipe[0];
            } catch (IOException e) {
                e = e;
                zzkd.zzb("Error transporting the ad response", e);
                zzu.zzft().zzb((Throwable) e, true);
                zzo.zzb(autoCloseOutputStream);
                return null;
            }
        } catch (IOException e2) {
            e = e2;
            autoCloseOutputStream = null;
            zzkd.zzb("Error transporting the ad response", e);
            zzu.zzft().zzb((Throwable) e, true);
            zzo.zzb(autoCloseOutputStream);
            return null;
        }
    }
}
