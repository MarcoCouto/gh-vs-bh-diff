package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzef;
import com.google.android.gms.internal.zzhh;
import com.google.android.gms.internal.zzhu;
import com.google.android.gms.internal.zzin;

@zzin
public class zzm {
    private static final Object zzamr = new Object();
    private static zzm zzavm;
    private final zza zzavn = new zza();
    private final zzl zzavo;

    static {
        zza(new zzm());
    }

    protected zzm() {
        zzl zzl = new zzl(new zze(), new zzd(), new zzai(), new zzef(), new zzf(), new zzhu(), new zzhh());
        this.zzavo = zzl;
    }

    protected static void zza(zzm zzm) {
        synchronized (zzamr) {
            zzavm = zzm;
        }
    }

    private static zzm zziv() {
        zzm zzm;
        synchronized (zzamr) {
            zzm = zzavm;
        }
        return zzm;
    }

    public static zza zziw() {
        return zziv().zzavn;
    }

    public static zzl zzix() {
        return zziv().zzavo;
    }
}
