package com.google.android.gms.ads.internal.purchase;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza implements Creator<GInAppPurchaseManagerInfoParcel> {
    static void zza(GInAppPurchaseManagerInfoParcel gInAppPurchaseManagerInfoParcel, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, gInAppPurchaseManagerInfoParcel.versionCode);
        zzb.zza(parcel, 3, gInAppPurchaseManagerInfoParcel.zzpn(), false);
        zzb.zza(parcel, 4, gInAppPurchaseManagerInfoParcel.zzpo(), false);
        zzb.zza(parcel, 5, gInAppPurchaseManagerInfoParcel.zzpp(), false);
        zzb.zza(parcel, 6, gInAppPurchaseManagerInfoParcel.zzpm(), false);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzah */
    public GInAppPurchaseManagerInfoParcel[] newArray(int i) {
        return new GInAppPurchaseManagerInfoParcel[i];
    }

    /* renamed from: zzj */
    public GInAppPurchaseManagerInfoParcel createFromParcel(Parcel parcel) {
        int zzcm = com.google.android.gms.common.internal.safeparcel.zza.zzcm(parcel);
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        IBinder iBinder4 = null;
        int i = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = com.google.android.gms.common.internal.safeparcel.zza.zzcl(parcel);
            int zzgm = com.google.android.gms.common.internal.safeparcel.zza.zzgm(zzcl);
            if (zzgm != 1) {
                switch (zzgm) {
                    case 3:
                        iBinder = com.google.android.gms.common.internal.safeparcel.zza.zzr(parcel, zzcl);
                        break;
                    case 4:
                        iBinder2 = com.google.android.gms.common.internal.safeparcel.zza.zzr(parcel, zzcl);
                        break;
                    case 5:
                        iBinder3 = com.google.android.gms.common.internal.safeparcel.zza.zzr(parcel, zzcl);
                        break;
                    case 6:
                        iBinder4 = com.google.android.gms.common.internal.safeparcel.zza.zzr(parcel, zzcl);
                        break;
                    default:
                        com.google.android.gms.common.internal.safeparcel.zza.zzb(parcel, zzcl);
                        break;
                }
            } else {
                i = com.google.android.gms.common.internal.safeparcel.zza.zzg(parcel, zzcl);
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel);
        }
        GInAppPurchaseManagerInfoParcel gInAppPurchaseManagerInfoParcel = new GInAppPurchaseManagerInfoParcel(i, iBinder, iBinder2, iBinder3, iBinder4);
        return gInAppPurchaseManagerInfoParcel;
    }
}
