package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.ads.internal.formats.zze;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzdo;
import com.google.android.gms.internal.zzeb;
import com.google.android.gms.internal.zzec;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzee;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzgn;
import com.google.android.gms.internal.zzgo;
import com.google.android.gms.internal.zzho;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzju.zza;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import java.util.List;

@zzin
public class zzq extends zzb {
    public zzq(Context context, zzd zzd, AdSizeParcel adSizeParcel, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel) {
        super(context, adSizeParcel, str, zzgj, versionInfoParcel, zzd);
    }

    private static zzd zza(zzgn zzgn) throws RemoteException {
        zzd zzd = new zzd(zzgn.getHeadline(), zzgn.getImages(), zzgn.getBody(), zzgn.zzku() != null ? zzgn.zzku() : null, zzgn.getCallToAction(), zzgn.getStarRating(), zzgn.getStore(), zzgn.getPrice(), null, zzgn.getExtras());
        return zzd;
    }

    private static zze zza(zzgo zzgo) throws RemoteException {
        zze zze = new zze(zzgo.getHeadline(), zzgo.getImages(), zzgo.getBody(), zzgo.zzky() != null ? zzgo.zzky() : null, zzgo.getCallToAction(), zzgo.getAdvertiser(), null, zzgo.getExtras());
        return zze;
    }

    private void zza(final zzd zzd) {
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                try {
                    if (zzq.this.zzajs.zzapk != null) {
                        zzq.this.zzajs.zzapk.zza(zzd);
                    }
                } catch (RemoteException e) {
                    zzkd.zzd("Could not call OnAppInstallAdLoadedListener.onAppInstallAdLoaded().", e);
                }
            }
        });
    }

    private void zza(final zze zze) {
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                try {
                    if (zzq.this.zzajs.zzapl != null) {
                        zzq.this.zzajs.zzapl.zza(zze);
                    }
                } catch (RemoteException e) {
                    zzkd.zzd("Could not call OnContentAdLoadedListener.onContentAdLoaded().", e);
                }
            }
        });
    }

    private void zza(final zzju zzju, final String str) {
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                try {
                    ((zzee) zzq.this.zzajs.zzapn.get(str)).zza((zzf) zzju.zzcim);
                } catch (RemoteException e) {
                    zzkd.zzd("Could not call onCustomTemplateAdLoadedListener.onCustomTemplateAdLoaded().", e);
                }
            }
        });
    }

    public void pause() {
        throw new IllegalStateException("Native Ad DOES NOT support pause().");
    }

    public void resume() {
        throw new IllegalStateException("Native Ad DOES NOT support resume().");
    }

    public void showInterstitial() {
        throw new IllegalStateException("Interstitial is NOT supported by NativeAdManager.");
    }

    public void zza(SimpleArrayMap<String, zzee> simpleArrayMap) {
        zzab.zzhi("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        this.zzajs.zzapn = simpleArrayMap;
    }

    public void zza(zzh zzh) {
        if (this.zzajs.zzapb.zzcie != null) {
            zzu.zzft().zzsu().zza(this.zzajs.zzapa, this.zzajs.zzapb, zzh);
        }
    }

    public void zza(zzdo zzdo) {
        throw new IllegalStateException("CustomRendering is NOT supported by NativeAdManager.");
    }

    public void zza(zzho zzho) {
        throw new IllegalStateException("In App Purchase is NOT supported by NativeAdManager.");
    }

    public void zza(final zza zza, zzdk zzdk) {
        if (zza.zzapa != null) {
            this.zzajs.zzapa = zza.zzapa;
        }
        if (zza.errorCode != -2) {
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzq zzq = zzq.this;
                    zzju zzju = new zzju(zza, null, null, null, null, null, null, null);
                    zzq.zzb(zzju);
                }
            });
            return;
        }
        this.zzajs.zzapw = 0;
        this.zzajs.zzaoz = zzu.zzfp().zza(this.zzajs.zzagf, this, zza, this.zzajs.zzaov, null, this.zzajz, this, zzdk);
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(this.zzajs.zzaoz.getClass().getName());
        zzkd.zzcv(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    /* access modifiers changed from: protected */
    public boolean zza(AdRequestParcel adRequestParcel, zzju zzju, boolean z) {
        return this.zzajr.zzfc();
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzju zzju, zzju zzju2) {
        zzgo zzgo = null;
        zzb(null);
        if (!this.zzajs.zzgp()) {
            throw new IllegalStateException("Native ad DOES NOT have custom rendering mode.");
        }
        if (zzju2.zzcby) {
            try {
                zzgn zzmo = zzju2.zzboo != null ? zzju2.zzboo.zzmo() : null;
                if (zzju2.zzboo != null) {
                    zzgo = zzju2.zzboo.zzmp();
                }
                if (zzmo != null && this.zzajs.zzapk != null) {
                    zzd zza = zza(zzmo);
                    zza.zzb(new zzg(this.zzajs.zzagf, this, this.zzajs.zzaov, zzmo));
                    zza(zza);
                } else if (zzgo == null || this.zzajs.zzapl == null) {
                    zzkd.zzcx("No matching mapper/listener for retrieved native ad template.");
                    zzh(0);
                    return false;
                } else {
                    zze zza2 = zza(zzgo);
                    zza2.zzb(new zzg(this.zzajs.zzagf, this, this.zzajs.zzaov, zzgo));
                    zza(zza2);
                }
            } catch (RemoteException e) {
                zzkd.zzd("Failed to get native ad mapper", e);
            }
        } else {
            zzh.zza zza3 = zzju2.zzcim;
            if ((zza3 instanceof zze) && this.zzajs.zzapl != null) {
                zza((zze) zzju2.zzcim);
            } else if (!(zza3 instanceof zzd) || this.zzajs.zzapk == null) {
                if ((zza3 instanceof zzf) && this.zzajs.zzapn != null) {
                    zzf zzf = (zzf) zza3;
                    if (this.zzajs.zzapn.get(zzf.getCustomTemplateId()) != null) {
                        zza(zzju2, zzf.getCustomTemplateId());
                    }
                }
                zzkd.zzcx("No matching listener for retrieved native ad template.");
                zzh(0);
                return false;
            } else {
                zza((zzd) zzju2.zzcim);
            }
        }
        return super.zza(zzju, zzju2);
    }

    public void zzb(SimpleArrayMap<String, zzed> simpleArrayMap) {
        zzab.zzhi("setOnCustomClickListener must be called on the main UI thread.");
        this.zzajs.zzapm = simpleArrayMap;
    }

    public void zzb(NativeAdOptionsParcel nativeAdOptionsParcel) {
        zzab.zzhi("setNativeAdOptions must be called on the main UI thread.");
        this.zzajs.zzapo = nativeAdOptionsParcel;
    }

    public void zzb(zzeb zzeb) {
        zzab.zzhi("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
        this.zzajs.zzapk = zzeb;
    }

    public void zzb(zzec zzec) {
        zzab.zzhi("setOnContentAdLoadedListener must be called on the main UI thread.");
        this.zzajs.zzapl = zzec;
    }

    public void zzb(@Nullable List<String> list) {
        zzab.zzhi("setNativeTemplates must be called on the main UI thread.");
        this.zzajs.zzaps = list;
    }

    public SimpleArrayMap<String, zzee> zzfb() {
        zzab.zzhi("getOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        return this.zzajs.zzapn;
    }

    @Nullable
    public zzed zzv(String str) {
        zzab.zzhi("getOnCustomClickListener must be called on the main UI thread.");
        return (zzed) this.zzajs.zzapm.get(str);
    }
}
