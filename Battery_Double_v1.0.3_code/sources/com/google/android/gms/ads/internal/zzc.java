package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzdl;
import com.google.android.gms.internal.zzdo;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzhg;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjo;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzju.zza;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzlh;
import java.util.Map;

@zzin
public abstract class zzc extends zzb implements zzh, zzhg {
    public zzc(Context context, AdSizeParcel adSizeParcel, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel, zzd zzd) {
        super(context, adSizeParcel, str, zzgj, versionInfoParcel, zzd);
    }

    /* access modifiers changed from: protected */
    public zzlh zza(zza zza, @Nullable zze zze, @Nullable zzjo zzjo) {
        View nextView = this.zzajs.zzaox.getNextView();
        zzlh zzlh = null;
        if (nextView instanceof zzlh) {
            zzlh zzlh2 = (zzlh) nextView;
            if (((Boolean) zzdc.zzazz.get()).booleanValue()) {
                zzkd.zzcv("Reusing webview...");
                zzlh2.zza(this.zzajs.zzagf, this.zzajs.zzapa, this.zzajn);
                zzlh = zzlh2;
            } else {
                zzlh2.destroy();
            }
        }
        if (zzlh == null) {
            if (nextView != null) {
                this.zzajs.zzaox.removeView(nextView);
            }
            zzlh = zzu.zzfr().zza(this.zzajs.zzagf, this.zzajs.zzapa, false, false, this.zzajs.zzaov, this.zzajs.zzaow, this.zzajn, this, this.zzajv);
            if (this.zzajs.zzapa.zzaut == null) {
                zzb(zzlh.getView());
            }
        }
        zzlh zzlh3 = zzlh;
        zzlh3.zzuj().zza(this, this, this, this, false, this, null, zze, this, zzjo);
        zza((zzft) zzlh3);
        zzlh3.zzcz(zza.zzcip.zzcbg);
        return zzlh3;
    }

    public void zza(int i, int i2, int i3, int i4) {
        zzdt();
    }

    public void zza(zzdo zzdo) {
        zzab.zzhi("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
        this.zzajs.zzapq = zzdo;
    }

    /* access modifiers changed from: protected */
    public void zza(zzft zzft) {
        zzft.zza("/trackActiveViewUnit", (zzep) new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                if (zzc.this.zzajs.zzapb != null) {
                    zzc.this.zzaju.zza(zzc.this.zzajs.zzapa, zzc.this.zzajs.zzapb, zzlh.getView(), (zzft) zzlh);
                } else {
                    zzkd.zzcx("Request to enable ActiveView before adState is available.");
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zza(final zza zza, final zzdk zzdk) {
        if (zza.errorCode != -2) {
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzc zzc = zzc.this;
                    zzju zzju = new zzju(zza, null, null, null, null, null, null, null);
                    zzc.zzb(zzju);
                }
            });
            return;
        }
        if (zza.zzapa != null) {
            this.zzajs.zzapa = zza.zzapa;
        }
        if (!zza.zzciq.zzcby || zza.zzciq.zzauw) {
            final zzjo zza2 = ((Boolean) zzdc.zzbdf.get()).booleanValue() ? this.zzajv.zzakm.zza(this.zzajs.zzagf, zza.zzciq) : null;
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    if (zza.zzciq.zzcch && zzc.this.zzajs.zzapq != null) {
                        String str = null;
                        if (zza.zzciq.zzbto != null) {
                            str = zzu.zzfq().zzco(zza.zzciq.zzbto);
                        }
                        zzdl zzdl = new zzdl(zzc.this, str, zza.zzciq.body);
                        zzc.this.zzajs.zzapw = 1;
                        try {
                            zzc.this.zzajq = false;
                            zzc.this.zzajs.zzapq.zza(zzdl);
                            return;
                        } catch (RemoteException e) {
                            zzkd.zzd("Could not call the onCustomRenderedAdLoadedListener.", e);
                            zzc.this.zzajq = true;
                        }
                    }
                    final zze zze = new zze(zzc.this.zzajs.zzagf, zza);
                    zzlh zza = zzc.this.zza(zza, zze, zza2);
                    zza.setOnTouchListener(new OnTouchListener() {
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            zze.recordClick();
                            return false;
                        }
                    });
                    zza.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            zze.recordClick();
                        }
                    });
                    zzc.this.zzajs.zzapw = 0;
                    zzc.this.zzajs.zzaoz = zzu.zzfp().zza(zzc.this.zzajs.zzagf, zzc.this, zza, zzc.this.zzajs.zzaov, zza, zzc.this.zzajz, zzc.this, zzdk);
                }
            });
            return;
        }
        this.zzajs.zzapw = 0;
        this.zzajs.zzaoz = zzu.zzfp().zza(this.zzajs.zzagf, this, zza, this.zzajs.zzaov, null, this.zzajz, this, zzdk);
    }

    /* access modifiers changed from: protected */
    public boolean zza(@Nullable zzju zzju, zzju zzju2) {
        if (this.zzajs.zzgp() && this.zzajs.zzaox != null) {
            this.zzajs.zzaox.zzgv().zzcs(zzju2.zzccd);
        }
        return super.zza(zzju, zzju2);
    }

    public void zzc(View view) {
        this.zzajs.zzapv = view;
        zzju zzju = new zzju(this.zzajs.zzapc, null, null, null, null, null, null, null);
        zzb(zzju);
    }

    public void zzeh() {
        onAdClicked();
    }

    public void zzei() {
        recordImpression();
        zzdp();
    }

    public void zzej() {
        zzdr();
    }
}
