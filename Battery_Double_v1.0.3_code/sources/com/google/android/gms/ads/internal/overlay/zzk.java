package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.internal.zzdi;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzlh;
import java.util.HashMap;
import java.util.Map;

@zzin
public class zzk extends FrameLayout implements zzh {
    private final zzlh zzbgf;
    private String zzbjc;
    private final FrameLayout zzbtw;
    private final zzy zzbtx;
    @Nullable
    private zzi zzbty;
    private boolean zzbtz;
    private boolean zzbua;
    private TextView zzbub;
    private long zzbuc;
    private long zzbud;
    private String zzbue;

    public zzk(Context context, zzlh zzlh, int i, boolean z, zzdk zzdk, zzdi zzdi) {
        Context context2 = context;
        super(context2);
        zzlh zzlh2 = zzlh;
        this.zzbgf = zzlh2;
        this.zzbtw = new FrameLayout(context2);
        addView(this.zzbtw, new LayoutParams(-1, -1));
        zzb.zzu(zzlh2.zzug());
        this.zzbty = zzlh2.zzug().zzakk.zza(context2, zzlh2, i, z, zzdk, zzdi);
        if (this.zzbty != null) {
            this.zzbtw.addView(this.zzbty, new LayoutParams(-1, -1, 17));
        }
        this.zzbub = new TextView(context2);
        this.zzbub.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        zzop();
        this.zzbtx = new zzy(this);
        this.zzbtx.zzpk();
        if (this.zzbty != null) {
            this.zzbty.zza((zzh) this);
        }
        if (this.zzbty == null) {
            zzl("AdVideoUnderlay Error", "Allocating player failed.");
        }
    }

    /* access modifiers changed from: private */
    public void zza(String str, String... strArr) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", str);
        String str2 = null;
        for (String str3 : strArr) {
            if (str2 == null) {
                str2 = str3;
            } else {
                hashMap.put(str2, str3);
                str2 = null;
            }
        }
        this.zzbgf.zza("onVideoEvent", (Map<String, ?>) hashMap);
    }

    public static void zzh(zzlh zzlh) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "no_video_view");
        zzlh.zza("onVideoEvent", (Map<String, ?>) hashMap);
    }

    private void zzop() {
        if (!zzor()) {
            this.zzbtw.addView(this.zzbub, new LayoutParams(-1, -1));
            this.zzbtw.bringChildToFront(this.zzbub);
        }
    }

    private void zzoq() {
        if (zzor()) {
            this.zzbtw.removeView(this.zzbub);
        }
    }

    private boolean zzor() {
        return this.zzbub.getParent() != null;
    }

    private void zzos() {
        if (this.zzbgf.zzue() != null && !this.zzbtz) {
            this.zzbua = (this.zzbgf.zzue().getWindow().getAttributes().flags & 128) != 0;
            if (!this.zzbua) {
                this.zzbgf.zzue().getWindow().addFlags(128);
                this.zzbtz = true;
            }
        }
    }

    private void zzot() {
        if (this.zzbgf.zzue() != null && this.zzbtz && !this.zzbua) {
            this.zzbgf.zzue().getWindow().clearFlags(128);
            this.zzbtz = false;
        }
    }

    public void destroy() {
        this.zzbtx.cancel();
        if (this.zzbty != null) {
            this.zzbty.stop();
        }
        zzot();
    }

    public void onPaused() {
        zza("pause", new String[0]);
        zzot();
    }

    public void pause() {
        if (this.zzbty != null) {
            this.zzbty.pause();
        }
    }

    public void play() {
        if (this.zzbty != null) {
            this.zzbty.play();
        }
    }

    public void seekTo(int i) {
        if (this.zzbty != null) {
            this.zzbty.seekTo(i);
        }
    }

    public void setMimeType(String str) {
        this.zzbue = str;
    }

    public void zza(float f) {
        if (this.zzbty != null) {
            this.zzbty.zza(f);
        }
    }

    public void zza(float f, float f2) {
        if (this.zzbty != null) {
            this.zzbty.zza(f, f2);
        }
    }

    public void zzbw(String str) {
        this.zzbjc = str;
    }

    public void zzd(int i, int i2, int i3, int i4) {
        if (i3 != 0 && i4 != 0) {
            LayoutParams layoutParams = new LayoutParams(i3 + 2, i4 + 2);
            layoutParams.setMargins(i - 1, i2 - 1, 0, 0);
            this.zzbtw.setLayoutParams(layoutParams);
            requestLayout();
        }
    }

    public void zzd(MotionEvent motionEvent) {
        if (this.zzbty != null) {
            this.zzbty.dispatchTouchEvent(motionEvent);
        }
    }

    public void zzl(String str, String str2) {
        zza("error", "what", str, "extra", str2);
    }

    public void zzlv() {
        if (this.zzbty != null) {
            if (!TextUtils.isEmpty(this.zzbjc)) {
                this.zzbty.setMimeType(this.zzbue);
                this.zzbty.setVideoPath(this.zzbjc);
                return;
            }
            zza("no_src", new String[0]);
        }
    }

    public void zzno() {
        if (this.zzbty != null) {
            this.zzbty.zzno();
        }
    }

    public void zznp() {
        if (this.zzbty != null) {
            this.zzbty.zznp();
        }
    }

    public void zzoi() {
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzk.this.zza("surfaceCreated", new String[0]);
            }
        });
    }

    public void zzoj() {
        if (this.zzbty != null && this.zzbud == 0) {
            zza("canplaythrough", "duration", String.valueOf(((float) this.zzbty.getDuration()) / 1000.0f), "videoWidth", String.valueOf(this.zzbty.getVideoWidth()), "videoHeight", String.valueOf(this.zzbty.getVideoHeight()));
        }
    }

    public void zzok() {
        zzos();
    }

    public void zzol() {
        zza("ended", new String[0]);
        zzot();
    }

    public void zzom() {
        zzop();
        this.zzbud = this.zzbuc;
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzk.this.zza("surfaceDestroyed", new String[0]);
            }
        });
    }

    public void zzon() {
        if (this.zzbty != null) {
            TextView textView = new TextView(this.zzbty.getContext());
            String str = "AdMob - ";
            String valueOf = String.valueOf(this.zzbty.zzni());
            textView.setText(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            textView.setTextColor(SupportMenu.CATEGORY_MASK);
            textView.setBackgroundColor(InputDeviceCompat.SOURCE_ANY);
            this.zzbtw.addView(textView, new LayoutParams(-2, -2, 17));
            this.zzbtw.bringChildToFront(textView);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzoo() {
        if (this.zzbty != null) {
            long currentPosition = (long) this.zzbty.getCurrentPosition();
            if (this.zzbuc != currentPosition && currentPosition > 0) {
                zzoq();
                zza("timeupdate", "time", String.valueOf(((float) currentPosition) / 1000.0f));
                this.zzbuc = currentPosition;
            }
        }
    }
}
