package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.Nullable;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzdi;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzlh;

@zzin
public abstract class zzj {
    @Nullable
    public abstract zzi zza(Context context, zzlh zzlh, int i, boolean z, zzdk zzdk, zzdi zzdi);

    /* access modifiers changed from: protected */
    public boolean zzg(zzlh zzlh) {
        return zzlh.zzdn().zzaus;
    }

    /* access modifiers changed from: protected */
    public boolean zzq(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        return zzs.zzavq() && (applicationInfo == null || applicationInfo.targetSdkVersion >= 11);
    }
}
