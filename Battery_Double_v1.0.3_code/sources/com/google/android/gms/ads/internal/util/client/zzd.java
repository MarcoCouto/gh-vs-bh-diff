package com.google.android.gms.ads.internal.util.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd implements Creator<VersionInfoParcel> {
    static void zza(VersionInfoParcel versionInfoParcel, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, versionInfoParcel.versionCode);
        zzb.zza(parcel, 2, versionInfoParcel.zzcs, false);
        zzb.zzc(parcel, 3, versionInfoParcel.zzcnk);
        zzb.zzc(parcel, 4, versionInfoParcel.zzcnl);
        zzb.zza(parcel, 5, versionInfoParcel.zzcnm);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzba */
    public VersionInfoParcel[] newArray(int i) {
        return new VersionInfoParcel[i];
    }

    /* renamed from: zzs */
    public VersionInfoParcel createFromParcel(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        String str = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case 3:
                    i2 = zza.zzg(parcel, zzcl);
                    break;
                case 4:
                    i3 = zza.zzg(parcel, zzcl);
                    break;
                case 5:
                    z = zza.zzc(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() != zzcm) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(zzcm);
            throw new C0028zza(sb.toString(), parcel);
        }
        VersionInfoParcel versionInfoParcel = new VersionInfoParcel(i, str, i2, i3, z);
        return versionInfoParcel;
    }
}
