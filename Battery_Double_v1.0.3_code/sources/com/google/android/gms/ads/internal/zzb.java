package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.purchase.GInAppPurchaseManagerInfoParcel;
import com.google.android.gms.ads.internal.purchase.zzc;
import com.google.android.gms.ads.internal.purchase.zzd;
import com.google.android.gms.ads.internal.purchase.zzf;
import com.google.android.gms.ads.internal.purchase.zzj;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza;
import com.google.android.gms.ads.internal.request.CapabilityParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzer;
import com.google.android.gms.internal.zzgb;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzhl;
import com.google.android.gms.internal.zzho;
import com.google.android.gms.internal.zzhs;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzjv;
import com.google.android.gms.internal.zzjw;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzlh;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;

@zzin
public abstract class zzb extends zza implements zzg, zzj, zzs, zzer, zzgb {
    private final Messenger mMessenger;
    protected final zzgj zzajz;
    protected transient boolean zzaka;

    public zzb(Context context, AdSizeParcel adSizeParcel, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel, zzd zzd) {
        this(new zzv(context, adSizeParcel, str, versionInfoParcel), zzgj, null, zzd);
    }

    protected zzb(zzv zzv, zzgj zzgj, @Nullable zzr zzr, zzd zzd) {
        super(zzv, zzr, zzd);
        this.zzajz = zzgj;
        this.mMessenger = new Messenger(new zzhl(this.zzajs.zzagf));
        this.zzaka = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0106 A[LOOP:0: B:33:0x00fc->B:35:0x0106, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x011c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0169  */
    private zza zza(AdRequestParcel adRequestParcel, Bundle bundle, zzjw zzjw) {
        PackageInfo packageInfo;
        Bundle bundle2;
        long j;
        int i;
        ApplicationInfo applicationInfo = this.zzajs.zzagf.getApplicationInfo();
        String str = null;
        boolean z = false;
        try {
            packageInfo = this.zzajs.zzagf.getPackageManager().getPackageInfo(applicationInfo.packageName, 0);
        } catch (NameNotFoundException unused) {
            packageInfo = null;
        }
        DisplayMetrics displayMetrics = this.zzajs.zzagf.getResources().getDisplayMetrics();
        if (this.zzajs.zzaox == null || this.zzajs.zzaox.getParent() == null) {
            bundle2 = null;
        } else {
            int[] iArr = new int[2];
            this.zzajs.zzaox.getLocationOnScreen(iArr);
            int i2 = iArr[0];
            int i3 = iArr[1];
            int width = this.zzajs.zzaox.getWidth();
            int height = this.zzajs.zzaox.getHeight();
            int i4 = (!this.zzajs.zzaox.isShown() || i2 + width <= 0 || i3 + height <= 0 || i2 > displayMetrics.widthPixels || i3 > displayMetrics.heightPixels) ? 0 : 1;
            bundle2 = new Bundle(5);
            bundle2.putInt("x", i2);
            bundle2.putInt("y", i3);
            bundle2.putInt("width", width);
            bundle2.putInt("height", height);
            bundle2.putInt("visible", i4);
        }
        String zzsj = zzu.zzft().zzsj();
        this.zzajs.zzapd = new zzjv(zzsj, this.zzajs.zzaou);
        AdRequestParcel adRequestParcel2 = adRequestParcel;
        this.zzajs.zzapd.zzq(adRequestParcel2);
        String zza = zzu.zzfq().zza(this.zzajs.zzagf, (View) this.zzajs.zzaox, this.zzajs.zzapa);
        if (this.zzajs.zzaph != null) {
            try {
                j = this.zzajs.zzaph.getValue();
            } catch (RemoteException unused2) {
                zzkd.zzcx("Cannot get correlation id, default to 0.");
            }
            String uuid = UUID.randomUUID().toString();
            Bundle zza2 = zzu.zzft().zza(this.zzajs.zzagf, this, zzsj);
            ArrayList arrayList = new ArrayList();
            for (i = 0; i < this.zzajs.zzapn.size(); i++) {
                arrayList.add((String) this.zzajs.zzapn.keyAt(i));
            }
            boolean z2 = this.zzajs.zzapi == null;
            if (this.zzajs.zzapj != null && zzu.zzft().zzsv()) {
                z = true;
            }
            boolean zzr = this.zzajv.zzakl.zzr(this.zzajs.zzagf);
            String str2 = "";
            if (((Boolean) zzdc.zzbdn.get()).booleanValue()) {
                zzkd.zzcv("Getting webview cookie from CookieManager.");
                CookieManager zzao = zzu.zzfs().zzao(this.zzajs.zzagf);
                if (zzao != null) {
                    str2 = zzao.getCookie("googleads.g.doubleclick.net");
                }
            }
            String str3 = str2;
            if (zzjw != null) {
                str = zzjw.zzsg();
            }
            String str4 = str;
            AdSizeParcel adSizeParcel = this.zzajs.zzapa;
            String str5 = this.zzajs.zzaou;
            String sessionId = zzu.zzft().getSessionId();
            ArrayList arrayList2 = arrayList;
            VersionInfoParcel versionInfoParcel = this.zzajs.zzaow;
            Bundle bundle3 = zza2;
            List<String> list = this.zzajs.zzaps;
            boolean zzsn = zzu.zzft().zzsn();
            Messenger messenger = this.mMessenger;
            int i5 = displayMetrics.widthPixels;
            int i6 = displayMetrics.heightPixels;
            float f = displayMetrics.density;
            List zzjx = zzdc.zzjx();
            float f2 = f;
            String str6 = this.zzajs.zzaot;
            NativeAdOptionsParcel nativeAdOptionsParcel = this.zzajs.zzapo;
            int i7 = i6;
            CapabilityParcel capabilityParcel = new CapabilityParcel(z2, z, zzr);
            NativeAdOptionsParcel nativeAdOptionsParcel2 = nativeAdOptionsParcel;
            CapabilityParcel capabilityParcel2 = capabilityParcel;
            List<String> list2 = list;
            ArrayList arrayList3 = arrayList2;
            Bundle bundle4 = bundle;
            zza zza3 = new zza(bundle2, adRequestParcel2, adSizeParcel, str5, applicationInfo, packageInfo, zzsj, sessionId, versionInfoParcel, bundle3, list2, arrayList3, bundle4, zzsn, messenger, i5, i7, f2, zza, j, uuid, zzjx, str6, nativeAdOptionsParcel2, capabilityParcel2, this.zzajs.zzgt(), zzu.zzfq().zzey(), zzu.zzfq().zzfa(), zzu.zzfq().zzam(this.zzajs.zzagf), zzu.zzfq().zzn(this.zzajs.zzaox), this.zzajs.zzagf instanceof Activity, zzu.zzft().zzsr(), str3, str4, zzu.zzft().zzss(), zzu.zzgj().zzlk(), zzu.zzfq().zzti());
            return zza3;
        }
        j = 0;
        String uuid2 = UUID.randomUUID().toString();
        Bundle zza22 = zzu.zzft().zza(this.zzajs.zzagf, this, zzsj);
        ArrayList arrayList4 = new ArrayList();
        while (i < this.zzajs.zzapn.size()) {
        }
        if (this.zzajs.zzapi == null) {
        }
        z = true;
        boolean zzr2 = this.zzajv.zzakl.zzr(this.zzajs.zzagf);
        String str22 = "";
        if (((Boolean) zzdc.zzbdn.get()).booleanValue()) {
        }
        String str32 = str22;
        if (zzjw != null) {
        }
        String str42 = str;
        AdSizeParcel adSizeParcel2 = this.zzajs.zzapa;
        String str52 = this.zzajs.zzaou;
        String sessionId2 = zzu.zzft().getSessionId();
        ArrayList arrayList22 = arrayList4;
        VersionInfoParcel versionInfoParcel2 = this.zzajs.zzaow;
        Bundle bundle32 = zza22;
        List<String> list3 = this.zzajs.zzaps;
        boolean zzsn2 = zzu.zzft().zzsn();
        Messenger messenger2 = this.mMessenger;
        int i52 = displayMetrics.widthPixels;
        int i62 = displayMetrics.heightPixels;
        float f3 = displayMetrics.density;
        List zzjx2 = zzdc.zzjx();
        float f22 = f3;
        String str62 = this.zzajs.zzaot;
        NativeAdOptionsParcel nativeAdOptionsParcel3 = this.zzajs.zzapo;
        int i72 = i62;
        CapabilityParcel capabilityParcel3 = new CapabilityParcel(z2, z, zzr2);
        NativeAdOptionsParcel nativeAdOptionsParcel22 = nativeAdOptionsParcel3;
        CapabilityParcel capabilityParcel22 = capabilityParcel3;
        List<String> list22 = list3;
        ArrayList arrayList32 = arrayList22;
        Bundle bundle42 = bundle;
        zza zza32 = new zza(bundle2, adRequestParcel2, adSizeParcel2, str52, applicationInfo, packageInfo, zzsj, sessionId2, versionInfoParcel2, bundle32, list22, arrayList32, bundle42, zzsn2, messenger2, i52, i72, f22, zza, j, uuid2, zzjx2, str62, nativeAdOptionsParcel22, capabilityParcel22, this.zzajs.zzgt(), zzu.zzfq().zzey(), zzu.zzfq().zzfa(), zzu.zzfq().zzam(this.zzajs.zzagf), zzu.zzfq().zzn(this.zzajs.zzaox), this.zzajs.zzagf instanceof Activity, zzu.zzft().zzsr(), str32, str42, zzu.zzft().zzss(), zzu.zzgj().zzlk(), zzu.zzfq().zzti());
        return zza32;
    }

    public String getMediationAdapterClassName() {
        if (this.zzajs.zzapb == null) {
            return null;
        }
        return this.zzajs.zzapb.zzbop;
    }

    public void onAdClicked() {
        if (this.zzajs.zzapb == null) {
            zzkd.zzcx("Ad state was null when trying to ping click URLs.");
            return;
        }
        if (!(this.zzajs.zzapb.zzcig == null || this.zzajs.zzapb.zzcig.zzbnm == null)) {
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb, this.zzajs.zzaou, false, this.zzajs.zzapb.zzcig.zzbnm);
        }
        if (!(this.zzajs.zzapb.zzbon == null || this.zzajs.zzapb.zzbon.zzbmz == null)) {
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb, this.zzajs.zzaou, false, this.zzajs.zzapb.zzbon.zzbmz);
        }
        super.onAdClicked();
    }

    public void onPause() {
        this.zzaju.zzk(this.zzajs.zzapb);
    }

    public void onResume() {
        this.zzaju.zzl(this.zzajs.zzapb);
    }

    public void pause() {
        zzab.zzhi("pause must be called on the main UI thread.");
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzbtm == null || !this.zzajs.zzgp())) {
            zzu.zzfs().zzi(this.zzajs.zzapb.zzbtm);
        }
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzboo == null)) {
            try {
                this.zzajs.zzapb.zzboo.pause();
            } catch (RemoteException unused) {
                zzkd.zzcx("Could not pause mediation adapter.");
            }
        }
        this.zzaju.zzk(this.zzajs.zzapb);
        this.zzajr.pause();
    }

    public void recordImpression() {
        zza(this.zzajs.zzapb, false);
    }

    public void resume() {
        zzab.zzhi("resume must be called on the main UI thread.");
        zzlh zzlh = (this.zzajs.zzapb == null || this.zzajs.zzapb.zzbtm == null) ? null : this.zzajs.zzapb.zzbtm;
        if (zzlh != null && this.zzajs.zzgp()) {
            zzu.zzfs().zzj(this.zzajs.zzapb.zzbtm);
        }
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzboo == null)) {
            try {
                this.zzajs.zzapb.zzboo.resume();
            } catch (RemoteException unused) {
                zzkd.zzcx("Could not resume mediation adapter.");
            }
        }
        if (zzlh == null || !zzlh.zzup()) {
            this.zzajr.resume();
        }
        this.zzaju.zzl(this.zzajs.zzapb);
    }

    public void showInterstitial() {
        throw new IllegalStateException("showInterstitial is not supported for current ad type");
    }

    public void zza(zzho zzho) {
        zzab.zzhi("setInAppPurchaseListener must be called on the main UI thread.");
        this.zzajs.zzapi = zzho;
    }

    public void zza(zzhs zzhs, @Nullable String str) {
        zzab.zzhi("setPlayStorePurchaseParams must be called on the main UI thread.");
        this.zzajs.zzapt = new zzk(str);
        this.zzajs.zzapj = zzhs;
        if (!zzu.zzft().zzsm() && zzhs != null) {
            Future future = (Future) new zzc(this.zzajs.zzagf, this.zzajs.zzapj, this.zzajs.zzapt).zzpy();
        }
    }

    /* access modifiers changed from: protected */
    public void zza(@Nullable zzju zzju, boolean z) {
        if (zzju == null) {
            zzkd.zzcx("Ad state was null when trying to ping impression URLs.");
            return;
        }
        super.zzc(zzju);
        if (!(zzju.zzcig == null || zzju.zzcig.zzbnn == null)) {
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, zzju, this.zzajs.zzaou, z, zzju.zzcig.zzbnn);
        }
        if (!(zzju.zzbon == null || zzju.zzbon.zzbna == null)) {
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, zzju, this.zzajs.zzaou, z, zzju.zzbon.zzbna);
        }
    }

    public void zza(String str, ArrayList<String> arrayList) {
        zzd zzd = new zzd(str, arrayList, this.zzajs.zzagf, this.zzajs.zzaow.zzcs);
        if (this.zzajs.zzapi == null) {
            zzkd.zzcx("InAppPurchaseListener is not set. Try to launch default purchase flow.");
            if (!zzm.zziw().zzar(this.zzajs.zzagf)) {
                zzkd.zzcx("Google Play Service unavailable, cannot launch default purchase flow.");
            } else if (this.zzajs.zzapj == null) {
                zzkd.zzcx("PlayStorePurchaseListener is not set.");
            } else if (this.zzajs.zzapt == null) {
                zzkd.zzcx("PlayStorePurchaseVerifier is not initialized.");
            } else if (this.zzajs.zzapx) {
                zzkd.zzcx("An in-app purchase request is already in progress, abort");
            } else {
                this.zzajs.zzapx = true;
                try {
                    if (!this.zzajs.zzapj.isValidPurchase(str)) {
                        this.zzajs.zzapx = false;
                    } else {
                        zzu.zzga().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcnm, new GInAppPurchaseManagerInfoParcel(this.zzajs.zzagf, this.zzajs.zzapt, zzd, this));
                    }
                } catch (RemoteException unused) {
                    zzkd.zzcx("Could not start In-App purchase.");
                    this.zzajs.zzapx = false;
                }
            }
        } else {
            try {
                this.zzajs.zzapi.zza(zzd);
            } catch (RemoteException unused2) {
                zzkd.zzcx("Could not start In-App purchase.");
            }
        }
    }

    public void zza(String str, boolean z, int i, final Intent intent, zzf zzf) {
        try {
            if (this.zzajs.zzapj != null) {
                zzhs zzhs = this.zzajs.zzapj;
                com.google.android.gms.ads.internal.purchase.zzg zzg = new com.google.android.gms.ads.internal.purchase.zzg(this.zzajs.zzagf, str, z, i, intent, zzf);
                zzhs.zza(zzg);
            }
        } catch (RemoteException unused) {
            zzkd.zzcx("Fail to invoke PlayStorePurchaseListener.");
        }
        zzkh.zzclc.postDelayed(new Runnable() {
            public void run() {
                int zzd = zzu.zzga().zzd(intent);
                zzu.zzga();
                if (!(zzd != 0 || zzb.this.zzajs.zzapb == null || zzb.this.zzajs.zzapb.zzbtm == null || zzb.this.zzajs.zzapb.zzbtm.zzuh() == null)) {
                    zzb.this.zzajs.zzapb.zzbtm.zzuh().close();
                }
                zzb.this.zzajs.zzapx = false;
            }
        }, 500);
    }

    public boolean zza(AdRequestParcel adRequestParcel, zzdk zzdk) {
        zzjw zzjw;
        if (!zzdw()) {
            return false;
        }
        Bundle zza = zza(zzu.zzft().zzaa(this.zzajs.zzagf));
        this.zzajr.cancel();
        this.zzajs.zzapw = 0;
        String str = null;
        if (((Boolean) zzdc.zzbct.get()).booleanValue()) {
            zzjw = zzu.zzft().zzst();
            zzg zzgi = zzu.zzgi();
            Context context = this.zzajs.zzagf;
            VersionInfoParcel versionInfoParcel = this.zzajs.zzaow;
            if (zzjw != null) {
                str = zzjw.zzsh();
            }
            zzgi.zza(context, versionInfoParcel, false, zzjw, str, this.zzajs.zzaou);
        } else {
            zzjw = null;
        }
        zza zza2 = zza(adRequestParcel, zza, zzjw);
        zzdk.zzh("seq_num", zza2.zzcau);
        zzdk.zzh("request_id", zza2.zzcbg);
        zzdk.zzh("session_id", zza2.zzcav);
        if (zza2.zzcas != null) {
            zzdk.zzh("app_version", String.valueOf(zza2.zzcas.versionCode));
        }
        this.zzajs.zzaoy = zzu.zzfm().zza(this.zzajs.zzagf, zza2, this.zzajs.zzaov, this);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean zza(AdRequestParcel adRequestParcel, zzju zzju, boolean z) {
        zzr zzr;
        long j;
        if (!z && this.zzajs.zzgp()) {
            if (zzju.zzbns > 0) {
                zzr = this.zzajr;
                j = zzju.zzbns;
            } else if (zzju.zzcig != null && zzju.zzcig.zzbns > 0) {
                zzr = this.zzajr;
                j = zzju.zzcig.zzbns;
            } else if (!zzju.zzcby && zzju.errorCode == 2) {
                this.zzajr.zzg(adRequestParcel);
            }
            zzr.zza(adRequestParcel, j);
        }
        return this.zzajr.zzfc();
    }

    /* access modifiers changed from: 0000 */
    public boolean zza(zzju zzju) {
        AdRequestParcel adRequestParcel;
        boolean z = false;
        if (this.zzajt != null) {
            adRequestParcel = this.zzajt;
            this.zzajt = null;
        } else {
            adRequestParcel = zzju.zzcar;
            if (adRequestParcel.extras != null) {
                z = adRequestParcel.extras.getBoolean("_noRefresh", false);
            }
        }
        return zza(adRequestParcel, zzju, z);
    }

    /* access modifiers changed from: protected */
    public boolean zza(@Nullable zzju zzju, zzju zzju2) {
        int i;
        if (!(zzju == null || zzju.zzboq == null)) {
            zzju.zzboq.zza((zzgb) null);
        }
        if (zzju2.zzboq != null) {
            zzju2.zzboq.zza((zzgb) this);
        }
        int i2 = 0;
        if (zzju2.zzcig != null) {
            i2 = zzju2.zzcig.zzbny;
            i = zzju2.zzcig.zzbnz;
        } else {
            i = 0;
        }
        this.zzajs.zzapu.zzh(i2, i);
        return true;
    }

    public void zzb(zzju zzju) {
        super.zzb(zzju);
        if (zzju.zzbon != null) {
            zzkd.zzcv("Pinging network fill URLs.");
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, zzju, this.zzajs.zzaou, false, zzju.zzbon.zzbnb);
            if (zzju.zzcig.zzbnp != null && zzju.zzcig.zzbnp.size() > 0) {
                zzkd.zzcv("Pinging urls remotely");
                zzu.zzfq().zza(this.zzajs.zzagf, zzju.zzcig.zzbnp);
            }
        }
        if (zzju.errorCode == 3 && zzju.zzcig != null && zzju.zzcig.zzbno != null) {
            zzkd.zzcv("Pinging no fill URLs.");
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, zzju, this.zzajs.zzaou, false, zzju.zzcig.zzbno);
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzc(AdRequestParcel adRequestParcel) {
        return super.zzc(adRequestParcel) && !this.zzaka;
    }

    /* access modifiers changed from: protected */
    public boolean zzdw() {
        return zzu.zzfq().zza(this.zzajs.zzagf.getPackageManager(), this.zzajs.zzagf.getPackageName(), "android.permission.INTERNET") && zzu.zzfq().zzac(this.zzajs.zzagf);
    }

    public void zzdx() {
        this.zzaju.zzi(this.zzajs.zzapb);
        this.zzaka = false;
        zzdr();
        this.zzajs.zzapd.zzsa();
    }

    public void zzdy() {
        this.zzaka = true;
        zzdt();
    }

    public void zzdz() {
        onAdClicked();
    }

    public void zzea() {
        zzdx();
    }

    public void zzeb() {
        zzdo();
    }

    public void zzec() {
        zzdy();
    }

    public void zzed() {
        if (this.zzajs.zzapb != null) {
            String str = this.zzajs.zzapb.zzbop;
            StringBuilder sb = new StringBuilder(74 + String.valueOf(str).length());
            sb.append("Mediation adapter ");
            sb.append(str);
            sb.append(" refreshed, but mediation adapters should never refresh.");
            zzkd.zzcx(sb.toString());
        }
        zza(this.zzajs.zzapb, true);
        zzdu();
    }

    public void zzee() {
        recordImpression();
    }

    public void zzef() {
        zzu.zzfq().runOnUiThread(new Runnable() {
            public void run() {
                zzb.this.zzajr.pause();
            }
        });
    }

    public void zzeg() {
        zzu.zzfq().runOnUiThread(new Runnable() {
            public void run() {
                zzb.this.zzajr.resume();
            }
        });
    }
}
