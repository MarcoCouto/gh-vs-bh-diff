package com.google.android.gms.ads.internal.purchase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

@zzin
public class zzh {
    private static final Object zzail = new Object();
    /* access modifiers changed from: private */
    public static final String zzbxk = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s INTEGER)", new Object[]{"InAppPurchase", "purchase_id", Param.PRODUCT_ID, "developer_payload", "record_time"});
    private static zzh zzbxm;
    private final zza zzbxl;

    public class zza extends SQLiteOpenHelper {
        public zza(Context context, String str) {
            super(context, str, null, 4);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL(zzh.zzbxk);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Database updated from version ");
            sb.append(i);
            sb.append(" to version ");
            sb.append(i2);
            zzkd.zzcw(sb.toString());
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS InAppPurchase");
            onCreate(sQLiteDatabase);
        }
    }

    zzh(Context context) {
        this.zzbxl = new zza(context, "google_inapp_purchase.db");
    }

    public static zzh zzs(Context context) {
        zzh zzh;
        synchronized (zzail) {
            if (zzbxm == null) {
                zzbxm = new zzh(context);
            }
            zzh = zzbxm;
        }
        return zzh;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0023, code lost:
        return r3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0045 A[Catch:{ all -> 0x0032 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x004a A[Catch:{ all -> 0x0032 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0054 A[SYNTHETIC, Splitter:B:35:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x005b  */
    public int getRecordCount() {
        synchronized (zzail) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return 0;
            }
            Cursor cursor = null;
            try {
                Cursor rawQuery = writableDatabase.rawQuery("select count(*) from InAppPurchase", null);
                try {
                    if (rawQuery.moveToFirst()) {
                        int i = rawQuery.getInt(0);
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                    } else if (rawQuery != null) {
                        rawQuery.close();
                    }
                } catch (SQLiteException e) {
                    SQLiteException sQLiteException = e;
                    cursor = rawQuery;
                    e = sQLiteException;
                    String str = "Error getting record count";
                    try {
                        String valueOf = String.valueOf(e.getMessage());
                        zzkd.zzcx(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
                        if (cursor != null) {
                            cursor.close();
                        }
                        return 0;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = rawQuery;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                e = e2;
                String str2 = "Error getting record count";
                String valueOf2 = String.valueOf(e.getMessage());
                zzkd.zzcx(valueOf2.length() == 0 ? str2.concat(valueOf2) : new String(str2));
                if (cursor != null) {
                }
                return 0;
            }
        }
    }

    public SQLiteDatabase getWritableDatabase() {
        try {
            return this.zzbxl.getWritableDatabase();
        } catch (SQLiteException unused) {
            zzkd.zzcx("Error opening writable conversion tracking database");
            return null;
        }
    }

    public zzf zza(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        return new zzf(cursor.getLong(0), cursor.getString(1), cursor.getString(2));
    }

    public void zza(zzf zzf) {
        if (zzf != null) {
            synchronized (zzail) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    writableDatabase.delete("InAppPurchase", String.format(Locale.US, "%s = %d", new Object[]{"purchase_id", Long.valueOf(zzf.zzbxf)}), null);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        return;
     */
    public void zzb(zzf zzf) {
        if (zzf != null) {
            synchronized (zzail) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Param.PRODUCT_ID, zzf.zzbxh);
                    contentValues.put("developer_payload", zzf.zzbxg);
                    contentValues.put("record_time", Long.valueOf(SystemClock.elapsedRealtime()));
                    zzf.zzbxf = writableDatabase.insert("InAppPurchase", null, contentValues);
                    if (((long) getRecordCount()) > 20000) {
                        zzpt();
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x005c A[Catch:{ all -> 0x0049 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0061 A[Catch:{ all -> 0x0049 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006c A[SYNTHETIC, Splitter:B:36:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0073  */
    public List<zzf> zzg(long j) {
        synchronized (zzail) {
            LinkedList linkedList = new LinkedList();
            if (j <= 0) {
                return linkedList;
            }
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return linkedList;
            }
            Cursor cursor = null;
            try {
                Cursor query = writableDatabase.query("InAppPurchase", null, null, null, null, null, "record_time ASC", String.valueOf(j));
                try {
                    if (query.moveToFirst()) {
                        do {
                            linkedList.add(zza(query));
                        } while (query.moveToNext());
                    }
                    if (query != null) {
                        query.close();
                    }
                } catch (SQLiteException e) {
                    e = e;
                    cursor = query;
                    String str = "Error extracing purchase info: ";
                    try {
                        String valueOf = String.valueOf(e.getMessage());
                        zzkd.zzcx(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
                        if (cursor != null) {
                        }
                        return linkedList;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                e = e2;
                String str2 = "Error extracing purchase info: ";
                String valueOf2 = String.valueOf(e.getMessage());
                zzkd.zzcx(valueOf2.length() == 0 ? str2.concat(valueOf2) : new String(str2));
                if (cursor != null) {
                    cursor.close();
                }
                return linkedList;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x004a A[Catch:{ all -> 0x0037 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004f A[Catch:{ all -> 0x0037 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005a A[SYNTHETIC, Splitter:B:32:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0061  */
    public void zzpt() {
        synchronized (zzail) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                Cursor cursor = null;
                try {
                    Cursor query = writableDatabase.query("InAppPurchase", null, null, null, null, null, "record_time ASC", "1");
                    if (query != null) {
                        try {
                            if (query.moveToFirst()) {
                                zza(zza(query));
                            }
                        } catch (SQLiteException e) {
                            e = e;
                            cursor = query;
                            String str = "Error remove oldest record";
                            try {
                                String valueOf = String.valueOf(e.getMessage());
                                zzkd.zzcx(valueOf.length() == 0 ? str.concat(valueOf) : new String(str));
                                if (cursor != null) {
                                }
                            } catch (Throwable th) {
                                th = th;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            cursor = query;
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                    if (query != null) {
                        query.close();
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                    String str2 = "Error remove oldest record";
                    String valueOf2 = String.valueOf(e.getMessage());
                    zzkd.zzcx(valueOf2.length() == 0 ? str2.concat(valueOf2) : new String(str2));
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        }
    }
}
