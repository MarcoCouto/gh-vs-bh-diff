package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View.MeasureSpec;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@TargetApi(14)
@zzin
public class zzc extends zzi implements OnAudioFocusChangeListener, OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, OnVideoSizeChangedListener, SurfaceTextureListener {
    private static final Map<Integer, String> zzbrr = new HashMap();
    private final zzx zzbrs;
    private final boolean zzbrt;
    private int zzbru = 0;
    private int zzbrv = 0;
    private MediaPlayer zzbrw;
    private Uri zzbrx;
    private int zzbry;
    private int zzbrz;
    private int zzbsa;
    private int zzbsb;
    private int zzbsc;
    private float zzbsd = 1.0f;
    private boolean zzbse;
    private boolean zzbsf;
    private zzw zzbsg;
    private boolean zzbsh;
    private int zzbsi;
    /* access modifiers changed from: private */
    public zzh zzbsj;

    static {
        zzbrr.put(Integer.valueOf(-1004), "MEDIA_ERROR_IO");
        zzbrr.put(Integer.valueOf(-1007), "MEDIA_ERROR_MALFORMED");
        zzbrr.put(Integer.valueOf(-1010), "MEDIA_ERROR_UNSUPPORTED");
        zzbrr.put(Integer.valueOf(-110), "MEDIA_ERROR_TIMED_OUT");
        zzbrr.put(Integer.valueOf(100), "MEDIA_ERROR_SERVER_DIED");
        zzbrr.put(Integer.valueOf(1), "MEDIA_ERROR_UNKNOWN");
        zzbrr.put(Integer.valueOf(1), "MEDIA_INFO_UNKNOWN");
        zzbrr.put(Integer.valueOf(700), "MEDIA_INFO_VIDEO_TRACK_LAGGING");
        zzbrr.put(Integer.valueOf(3), "MEDIA_INFO_VIDEO_RENDERING_START");
        zzbrr.put(Integer.valueOf(701), "MEDIA_INFO_BUFFERING_START");
        zzbrr.put(Integer.valueOf(702), "MEDIA_INFO_BUFFERING_END");
        zzbrr.put(Integer.valueOf(800), "MEDIA_INFO_BAD_INTERLEAVING");
        zzbrr.put(Integer.valueOf(801), "MEDIA_INFO_NOT_SEEKABLE");
        zzbrr.put(Integer.valueOf(802), "MEDIA_INFO_METADATA_UPDATE");
        zzbrr.put(Integer.valueOf(901), "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
        zzbrr.put(Integer.valueOf(902), "MEDIA_INFO_SUBTITLE_TIMED_OUT");
    }

    public zzc(Context context, boolean z, boolean z2, zzx zzx) {
        super(context);
        setSurfaceTextureListener(this);
        this.zzbrs = zzx;
        this.zzbsh = z;
        this.zzbrt = z2;
        this.zzbrs.zza((zzi) this);
    }

    private void zzad(int i) {
        if (i == 3) {
            this.zzbrs.zzpi();
        } else if (this.zzbru == 3) {
            this.zzbrs.zzpj();
        }
        this.zzbru = i;
    }

    private void zzae(int i) {
        this.zzbrv = i;
    }

    private void zzb(float f) {
        if (this.zzbrw != null) {
            try {
                this.zzbrw.setVolume(f, f);
            } catch (IllegalStateException unused) {
            }
        } else {
            zzkd.zzcx("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
        }
    }

    private void zznj() {
        zzkd.v("AdMediaPlayerView init MediaPlayer");
        SurfaceTexture surfaceTexture = getSurfaceTexture();
        if (this.zzbrx != null && surfaceTexture != null) {
            zzy(false);
            try {
                this.zzbrw = zzu.zzgd().zzov();
                this.zzbrw.setOnBufferingUpdateListener(this);
                this.zzbrw.setOnCompletionListener(this);
                this.zzbrw.setOnErrorListener(this);
                this.zzbrw.setOnInfoListener(this);
                this.zzbrw.setOnPreparedListener(this);
                this.zzbrw.setOnVideoSizeChangedListener(this);
                this.zzbsa = 0;
                if (this.zzbsh) {
                    this.zzbsg = new zzw(getContext());
                    this.zzbsg.zza(surfaceTexture, getWidth(), getHeight());
                    this.zzbsg.start();
                    SurfaceTexture zzox = this.zzbsg.zzox();
                    if (zzox != null) {
                        surfaceTexture = zzox;
                    } else {
                        this.zzbsg.zzow();
                        this.zzbsg = null;
                    }
                }
                this.zzbrw.setDataSource(getContext(), this.zzbrx);
                this.zzbrw.setSurface(zzu.zzge().zza(surfaceTexture));
                this.zzbrw.setAudioStreamType(3);
                this.zzbrw.setScreenOnWhilePlaying(true);
                this.zzbrw.prepareAsync();
                zzad(1);
            } catch (IOException | IllegalArgumentException | IllegalStateException e) {
                String valueOf = String.valueOf(this.zzbrx);
                StringBuilder sb = new StringBuilder(36 + String.valueOf(valueOf).length());
                sb.append("Failed to initialize MediaPlayer at ");
                sb.append(valueOf);
                zzkd.zzd(sb.toString(), e);
                onError(this.zzbrw, 1, 0);
            }
        }
    }

    private void zznk() {
        if (this.zzbrt && zznn() && this.zzbrw.getCurrentPosition() > 0 && this.zzbrv != 3) {
            zzkd.v("AdMediaPlayerView nudging MediaPlayer");
            zzb(0.0f);
            this.zzbrw.start();
            int currentPosition = this.zzbrw.getCurrentPosition();
            long currentTimeMillis = zzu.zzfu().currentTimeMillis();
            while (zznn() && this.zzbrw.getCurrentPosition() == currentPosition) {
                if (zzu.zzfu().currentTimeMillis() - currentTimeMillis > 250) {
                    break;
                }
            }
            this.zzbrw.pause();
            zzns();
        }
    }

    private void zznl() {
        AudioManager zznt = zznt();
        if (zznt != null && !this.zzbsf) {
            if (zznt.requestAudioFocus(this, 3, 2) == 1) {
                zznq();
                return;
            }
            zzkd.zzcx("AdMediaPlayerView audio focus request failed");
        }
    }

    private void zznm() {
        zzkd.v("AdMediaPlayerView abandon audio focus");
        AudioManager zznt = zznt();
        if (zznt != null && this.zzbsf) {
            if (zznt.abandonAudioFocus(this) == 1) {
                this.zzbsf = false;
                return;
            }
            zzkd.zzcx("AdMediaPlayerView abandon audio focus failed");
        }
    }

    private boolean zznn() {
        return (this.zzbrw == null || this.zzbru == -1 || this.zzbru == 0 || this.zzbru == 1) ? false : true;
    }

    private void zznq() {
        zzkd.v("AdMediaPlayerView audio focus gained");
        this.zzbsf = true;
        zzns();
    }

    private void zznr() {
        zzkd.v("AdMediaPlayerView audio focus lost");
        this.zzbsf = false;
        zzns();
    }

    private void zzns() {
        zzb((this.zzbse || !this.zzbsf) ? 0.0f : this.zzbsd);
    }

    private AudioManager zznt() {
        return (AudioManager) getContext().getSystemService("audio");
    }

    private void zzy(boolean z) {
        zzkd.v("AdMediaPlayerView release");
        if (this.zzbsg != null) {
            this.zzbsg.zzow();
            this.zzbsg = null;
        }
        if (this.zzbrw != null) {
            this.zzbrw.reset();
            this.zzbrw.release();
            this.zzbrw = null;
            zzad(0);
            if (z) {
                this.zzbrv = 0;
                zzae(0);
            }
            zznm();
        }
    }

    public int getCurrentPosition() {
        if (zznn()) {
            return this.zzbrw.getCurrentPosition();
        }
        return 0;
    }

    public int getDuration() {
        if (zznn()) {
            return this.zzbrw.getDuration();
        }
        return -1;
    }

    public int getVideoHeight() {
        if (this.zzbrw != null) {
            return this.zzbrw.getVideoHeight();
        }
        return 0;
    }

    public int getVideoWidth() {
        if (this.zzbrw != null) {
            return this.zzbrw.getVideoWidth();
        }
        return 0;
    }

    public void onAudioFocusChange(int i) {
        if (i > 0) {
            zznq();
            return;
        }
        if (i < 0) {
            zznr();
        }
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        this.zzbsa = i;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        zzkd.v("AdMediaPlayerView completion");
        zzad(5);
        zzae(5);
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                if (zzc.this.zzbsj != null) {
                    zzc.this.zzbsj.zzol();
                }
            }
        });
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        final String str = (String) zzbrr.get(Integer.valueOf(i));
        final String str2 = (String) zzbrr.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(38 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer error: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzkd.zzcx(sb.toString());
        zzad(-1);
        zzae(-1);
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                if (zzc.this.zzbsj != null) {
                    zzc.this.zzbsj.zzl(str, str2);
                }
            }
        });
        return true;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        String str = (String) zzbrr.get(Integer.valueOf(i));
        String str2 = (String) zzbrr.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(37 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer info: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzkd.v(sb.toString());
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        if ((r5.zzbry * r7) > (r5.zzbrz * r6)) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
        if (r1 > r6) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        if (r1 > r6) goto L_0x0047;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int defaultSize = getDefaultSize(this.zzbry, i);
        int defaultSize2 = getDefaultSize(this.zzbrz, i2);
        if (this.zzbry <= 0 || this.zzbrz <= 0 || this.zzbsg != null) {
            i4 = defaultSize;
        } else {
            int mode = MeasureSpec.getMode(i);
            i4 = MeasureSpec.getSize(i);
            int mode2 = MeasureSpec.getMode(i2);
            i3 = MeasureSpec.getSize(i2);
            if (mode == 1073741824 && mode2 == 1073741824) {
                if (this.zzbry * i3 < this.zzbrz * i4) {
                    i4 = (this.zzbry * i3) / this.zzbrz;
                }
                setMeasuredDimension(i4, i3);
                if (this.zzbsg != null) {
                }
                if (VERSION.SDK_INT == 16) {
                }
            } else {
                if (mode == 1073741824) {
                    int i6 = (this.zzbrz * i4) / this.zzbry;
                    if (mode2 != Integer.MIN_VALUE || i6 <= i3) {
                        i3 = i6;
                    }
                } else {
                    if (mode2 == 1073741824) {
                        i5 = (this.zzbry * i3) / this.zzbrz;
                        if (mode == Integer.MIN_VALUE) {
                        }
                    } else {
                        int i7 = this.zzbry;
                        int i8 = this.zzbrz;
                        if (mode2 != Integer.MIN_VALUE || i8 <= i3) {
                            i5 = i7;
                            i3 = i8;
                        } else {
                            i5 = (this.zzbry * i3) / this.zzbrz;
                        }
                        if (mode == Integer.MIN_VALUE) {
                        }
                    }
                    i4 = i5;
                }
                setMeasuredDimension(i4, i3);
                if (this.zzbsg != null) {
                    this.zzbsg.zzg(i4, i3);
                }
                if (VERSION.SDK_INT == 16) {
                    if ((this.zzbsb > 0 && this.zzbsb != i4) || (this.zzbsc > 0 && this.zzbsc != i3)) {
                        zznk();
                    }
                    this.zzbsb = i4;
                    this.zzbsc = i3;
                    return;
                }
                return;
            }
            defaultSize2 = (this.zzbrz * i4) / this.zzbry;
        }
        i3 = defaultSize2;
        setMeasuredDimension(i4, i3);
        if (this.zzbsg != null) {
        }
        if (VERSION.SDK_INT == 16) {
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        zzkd.v("AdMediaPlayerView prepared");
        zzad(2);
        this.zzbrs.zzoj();
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                if (zzc.this.zzbsj != null) {
                    zzc.this.zzbsj.zzoj();
                }
            }
        });
        this.zzbry = mediaPlayer.getVideoWidth();
        this.zzbrz = mediaPlayer.getVideoHeight();
        if (this.zzbsi != 0) {
            seekTo(this.zzbsi);
        }
        zznk();
        int i = this.zzbry;
        int i2 = this.zzbrz;
        StringBuilder sb = new StringBuilder(62);
        sb.append("AdMediaPlayerView stream dimensions: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzkd.zzcw(sb.toString());
        if (this.zzbrv == 3) {
            play();
        }
        zznl();
        zzns();
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        zzkd.v("AdMediaPlayerView surface created");
        zznj();
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                if (zzc.this.zzbsj != null) {
                    zzc.this.zzbsj.zzoi();
                }
            }
        });
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        zzkd.v("AdMediaPlayerView surface destroyed");
        if (this.zzbrw != null && this.zzbsi == 0) {
            this.zzbsi = this.zzbrw.getCurrentPosition();
        }
        if (this.zzbsg != null) {
            this.zzbsg.zzow();
        }
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                if (zzc.this.zzbsj != null) {
                    zzc.this.zzbsj.onPaused();
                    zzc.this.zzbsj.zzom();
                }
            }
        });
        zzy(true);
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        zzkd.v("AdMediaPlayerView surface changed");
        boolean z = false;
        boolean z2 = this.zzbrv == 3;
        if (this.zzbry == i && this.zzbrz == i2) {
            z = true;
        }
        if (this.zzbrw != null && z2 && z) {
            if (this.zzbsi != 0) {
                seekTo(this.zzbsi);
            }
            play();
        }
        if (this.zzbsg != null) {
            this.zzbsg.zzg(i, i2);
        }
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.zzbrs.zzb(this);
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder sb = new StringBuilder(57);
        sb.append("AdMediaPlayerView size changed: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzkd.v(sb.toString());
        this.zzbry = mediaPlayer.getVideoWidth();
        this.zzbrz = mediaPlayer.getVideoHeight();
        if (this.zzbry != 0 && this.zzbrz != 0) {
            requestLayout();
        }
    }

    public void pause() {
        zzkd.v("AdMediaPlayerView pause");
        if (zznn() && this.zzbrw.isPlaying()) {
            this.zzbrw.pause();
            zzad(4);
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    if (zzc.this.zzbsj != null) {
                        zzc.this.zzbsj.onPaused();
                    }
                }
            });
        }
        zzae(4);
    }

    public void play() {
        zzkd.v("AdMediaPlayerView play");
        if (zznn()) {
            this.zzbrw.start();
            zzad(3);
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    if (zzc.this.zzbsj != null) {
                        zzc.this.zzbsj.zzok();
                    }
                }
            });
        }
        zzae(3);
    }

    public void seekTo(int i) {
        StringBuilder sb = new StringBuilder(34);
        sb.append("AdMediaPlayerView seek ");
        sb.append(i);
        zzkd.v(sb.toString());
        if (zznn()) {
            this.zzbrw.seekTo(i);
            i = 0;
        }
        this.zzbsi = i;
    }

    public void setMimeType(String str) {
    }

    public void setVideoPath(String str) {
        setVideoURI(Uri.parse(str));
    }

    public void setVideoURI(Uri uri) {
        this.zzbrx = uri;
        this.zzbsi = 0;
        zznj();
        requestLayout();
        invalidate();
    }

    public void stop() {
        zzkd.v("AdMediaPlayerView stop");
        if (this.zzbrw != null) {
            this.zzbrw.stop();
            this.zzbrw.release();
            this.zzbrw = null;
            zzad(0);
            zzae(0);
            zznm();
        }
        this.zzbrs.onStop();
    }

    public String toString() {
        String valueOf = String.valueOf(getClass().getName());
        String valueOf2 = String.valueOf(Integer.toHexString(hashCode()));
        StringBuilder sb = new StringBuilder(1 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append("@");
        sb.append(valueOf2);
        return sb.toString();
    }

    public void zza(float f) {
        this.zzbsd = f;
        zzns();
    }

    public void zza(float f, float f2) {
        if (this.zzbsg != null) {
            this.zzbsg.zzb(f, f2);
        }
    }

    public void zza(zzh zzh) {
        this.zzbsj = zzh;
    }

    public String zzni() {
        String str = "MediaPlayer";
        String valueOf = String.valueOf(this.zzbsh ? " spherical" : "");
        return valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
    }

    public void zzno() {
        this.zzbse = true;
        zzns();
    }

    public void zznp() {
        this.zzbse = false;
        zzns();
    }
}
