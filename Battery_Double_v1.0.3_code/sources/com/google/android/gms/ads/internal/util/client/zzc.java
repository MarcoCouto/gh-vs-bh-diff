package com.google.android.gms.ads.internal.util.client;

import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza.C0025zza;
import com.google.android.gms.internal.zzin;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@zzin
public class zzc implements C0025zza {
    @Nullable
    private final String zzbjf;

    public zzc() {
        this(null);
    }

    public zzc(@Nullable String str) {
        this.zzbjf = str;
    }

    @WorkerThread
    public void zzcr(String str) {
        String str2;
        StringBuilder sb;
        String str3;
        HttpURLConnection httpURLConnection;
        String str4 = "Pinging URL: ";
        try {
            String valueOf = String.valueOf(str);
            zzb.zzcv(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            zzm.zziw().zza(true, httpURLConnection, this.zzbjf);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                StringBuilder sb2 = new StringBuilder(65 + String.valueOf(str).length());
                sb2.append("Received non-success response code ");
                sb2.append(responseCode);
                sb2.append(" from pinging URL: ");
                sb2.append(str);
                zzb.zzcx(sb2.toString());
            }
            httpURLConnection.disconnect();
        } catch (IndexOutOfBoundsException e) {
            String valueOf2 = String.valueOf(e.getMessage());
            StringBuilder sb3 = new StringBuilder(32 + String.valueOf(str).length() + String.valueOf(valueOf2).length());
            sb3.append("Error while parsing ping URL: ");
            sb3.append(str);
            sb3.append(". ");
            sb3.append(valueOf2);
            str2 = sb3.toString();
            zzb.zzcx(str2);
        } catch (IOException e2) {
            str3 = String.valueOf(e2.getMessage());
            sb = new StringBuilder(27 + String.valueOf(str).length() + String.valueOf(str3).length());
            sb.append("Error while pinging URL: ");
            sb.append(str);
            sb.append(". ");
            sb.append(str3);
            str2 = sb.toString();
            zzb.zzcx(str2);
        } catch (RuntimeException e3) {
            str3 = String.valueOf(e3.getMessage());
            sb = new StringBuilder(27 + String.valueOf(str).length() + String.valueOf(str3).length());
            sb.append("Error while pinging URL: ");
            sb.append(str);
            sb.append(". ");
            sb.append(str3);
            str2 = sb.toString();
            zzb.zzcx(str2);
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
