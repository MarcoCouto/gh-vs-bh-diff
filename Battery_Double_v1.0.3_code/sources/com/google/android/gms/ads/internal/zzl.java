package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.Window;
import com.google.ads.mediation.AbstractAdViewAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzey;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzfz;
import com.google.android.gms.internal.zzga;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zziq;
import com.google.android.gms.internal.zzjo;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zzli;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzl extends zzc implements zzet, com.google.android.gms.internal.zzey.zza {
    protected transient boolean zzalw = false;
    private int zzalx = -1;
    /* access modifiers changed from: private */
    public boolean zzaly;
    /* access modifiers changed from: private */
    public float zzalz;

    @zzin
    private class zza extends zzkc {
        private final int zzama;

        public zza(int i) {
            this.zzama = i;
        }

        public void onStop() {
        }

        public void zzew() {
            InterstitialAdParameterParcel interstitialAdParameterParcel = new InterstitialAdParameterParcel(zzl.this.zzajs.zzame, zzl.this.zzet(), zzl.this.zzaly, zzl.this.zzalz, zzl.this.zzajs.zzame ? this.zzama : -1);
            int requestedOrientation = zzl.this.zzajs.zzapb.zzbtm.getRequestedOrientation();
            if (requestedOrientation == -1) {
                requestedOrientation = zzl.this.zzajs.zzapb.orientation;
            }
            final AdOverlayInfoParcel adOverlayInfoParcel = new AdOverlayInfoParcel(zzl.this, zzl.this, zzl.this, zzl.this.zzajs.zzapb.zzbtm, requestedOrientation, zzl.this.zzajs.zzaow, zzl.this.zzajs.zzapb.zzccd, interstitialAdParameterParcel);
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzu.zzfo().zza(zzl.this.zzajs.zzagf, adOverlayInfoParcel);
                }
            });
        }
    }

    public zzl(Context context, AdSizeParcel adSizeParcel, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel, zzd zzd) {
        super(context, adSizeParcel, str, zzgj, versionInfoParcel, zzd);
    }

    private void zzb(Bundle bundle) {
        zzu.zzfq().zzb(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, "gmob-apps", bundle, false);
    }

    private com.google.android.gms.internal.zzju.zza zzc(com.google.android.gms.internal.zzju.zza zza2) {
        com.google.android.gms.internal.zzju.zza zza3 = zza2;
        try {
            String jSONObject = zziq.zzc(zza3.zzciq).toString();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(AbstractAdViewAdapter.AD_UNIT_ID_PARAMETER, zza3.zzcip.zzaou);
            zzfz zzfz = new zzfz(jSONObject, null, Collections.singletonList("com.google.ads.mediation.admob.AdMobAdapter"), null, null, Collections.emptyList(), Collections.emptyList(), jSONObject2.toString(), null, Collections.emptyList(), Collections.emptyList(), null, null, null, null, null, Collections.emptyList());
            zzga zzga = new zzga(Collections.singletonList(zzfz), -1, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false, "", -1, 0, 1, null, 0, -1, -1, false);
            AdResponseParcel adResponseParcel = zza3.zzciq;
            AdRequestInfoParcel adRequestInfoParcel = zza3.zzcip;
            String str = adResponseParcel.zzbto;
            String str2 = adResponseParcel.body;
            List<String> list = adResponseParcel.zzbnm;
            List<String> list2 = adResponseParcel.zzbnn;
            long j = adResponseParcel.zzcbx;
            long j2 = adResponseParcel.zzcbz;
            List<String> list3 = adResponseParcel.zzcca;
            long j3 = j2;
            long j4 = adResponseParcel.zzbns;
            int i = adResponseParcel.orientation;
            String str3 = adResponseParcel.zzccb;
            long j5 = j4;
            long j6 = adResponseParcel.zzccc;
            String str4 = adResponseParcel.zzccd;
            boolean z = adResponseParcel.zzcce;
            String str5 = str4;
            String str6 = adResponseParcel.zzccf;
            String str7 = adResponseParcel.zzccg;
            boolean z2 = adResponseParcel.zzcch;
            boolean z3 = adResponseParcel.zzauu;
            boolean z4 = adResponseParcel.zzcaz;
            boolean z5 = adResponseParcel.zzcci;
            boolean z6 = adResponseParcel.zzccj;
            String str8 = adResponseParcel.zzccm;
            boolean z7 = adResponseParcel.zzauv;
            boolean z8 = adResponseParcel.zzauw;
            RewardItemParcel rewardItemParcel = adResponseParcel.zzccn;
            List<String> list4 = adResponseParcel.zzcco;
            List<String> list5 = adResponseParcel.zzccp;
            boolean z9 = adResponseParcel.zzccq;
            AutoClickProtectionConfigurationParcel autoClickProtectionConfigurationParcel = adResponseParcel.zzccr;
            boolean z10 = adResponseParcel.zzcbq;
            String str9 = adResponseParcel.zzcbr;
            List<String> list6 = adResponseParcel.zzbnp;
            String str10 = adResponseParcel.zzccs;
            long j7 = j5;
            long j8 = j6;
            String str11 = str5;
            String str12 = str6;
            String str13 = str7;
            boolean z11 = z2;
            boolean z12 = z3;
            boolean z13 = z4;
            boolean z14 = z5;
            boolean z15 = z6;
            String str14 = str8;
            boolean z16 = z7;
            boolean z17 = z8;
            RewardItemParcel rewardItemParcel2 = rewardItemParcel;
            List<String> list7 = list4;
            List<String> list8 = list5;
            boolean z18 = z9;
            AutoClickProtectionConfigurationParcel autoClickProtectionConfigurationParcel2 = autoClickProtectionConfigurationParcel;
            boolean z19 = z10;
            String str15 = str9;
            List<String> list9 = list6;
            String str16 = str10;
            boolean z20 = z;
            int i2 = i;
            long j9 = j3;
            AdResponseParcel adResponseParcel2 = new AdResponseParcel(adRequestInfoParcel, str, str2, list, list2, j, true, j9, list3, j7, i2, str3, j8, str11, z20, str12, str13, z11, z12, z13, z14, z15, str14, z16, z17, rewardItemParcel2, list7, list8, z18, autoClickProtectionConfigurationParcel2, z19, str15, list9, str16, adResponseParcel.zzbnq, adResponseParcel.zzcct);
            com.google.android.gms.internal.zzju.zza zza4 = new com.google.android.gms.internal.zzju.zza(zza3.zzcip, adResponseParcel2, zzga, zza3.zzapa, zza3.errorCode, zza3.zzcik, zza3.zzcil, zza3.zzcie);
            return zza4;
        } catch (JSONException e) {
            zzkd.zzb("Unable to generate ad state for an interstitial ad with pooling.", e);
            return zza3;
        }
    }

    public void showInterstitial() {
        String str;
        zzab.zzhi("showInterstitial must be called on the main UI thread.");
        if (this.zzajs.zzapb == null) {
            str = "The interstitial has not loaded.";
        } else {
            if (((Boolean) zzdc.zzbau.get()).booleanValue()) {
                String packageName = (this.zzajs.zzagf.getApplicationContext() != null ? this.zzajs.zzagf.getApplicationContext() : this.zzajs.zzagf).getPackageName();
                if (!this.zzalw) {
                    zzkd.zzcx("It is not recommended to show an interstitial before onAdLoaded completes.");
                    Bundle bundle = new Bundle();
                    bundle.putString("appid", packageName);
                    bundle.putString("action", "show_interstitial_before_load_finish");
                    zzb(bundle);
                }
                if (!zzu.zzfq().zzai(this.zzajs.zzagf)) {
                    zzkd.zzcx("It is not recommended to show an interstitial when app is not in foreground.");
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("appid", packageName);
                    bundle2.putString("action", "show_interstitial_app_not_in_foreground");
                    zzb(bundle2);
                }
            }
            if (!this.zzajs.zzgq()) {
                if (this.zzajs.zzapb.zzcby && this.zzajs.zzapb.zzboo != null) {
                    try {
                        this.zzajs.zzapb.zzboo.showInterstitial();
                        return;
                    } catch (RemoteException e) {
                        zzkd.zzd("Could not show interstitial.", e);
                        zzeu();
                        return;
                    }
                } else if (this.zzajs.zzapb.zzbtm == null) {
                    str = "The interstitial failed to load.";
                } else if (this.zzajs.zzapb.zzbtm.zzun()) {
                    str = "The interstitial is already showing.";
                } else {
                    this.zzajs.zzapb.zzbtm.zzah(true);
                    if (this.zzajs.zzapb.zzcie != null) {
                        this.zzaju.zza(this.zzajs.zzapa, this.zzajs.zzapb);
                    }
                    Bitmap zzaj = this.zzajs.zzame ? zzu.zzfq().zzaj(this.zzajs.zzagf) : null;
                    this.zzalx = zzu.zzgh().zzb(zzaj);
                    if (!((Boolean) zzdc.zzbca.get()).booleanValue() || zzaj == null) {
                        InterstitialAdParameterParcel interstitialAdParameterParcel = new InterstitialAdParameterParcel(this.zzajs.zzame, zzet(), false, 0.0f, -1);
                        int requestedOrientation = this.zzajs.zzapb.zzbtm.getRequestedOrientation();
                        if (requestedOrientation == -1) {
                            requestedOrientation = this.zzajs.zzapb.orientation;
                        }
                        AdOverlayInfoParcel adOverlayInfoParcel = new AdOverlayInfoParcel(this, this, this, this.zzajs.zzapb.zzbtm, requestedOrientation, this.zzajs.zzaow, this.zzajs.zzapb.zzccd, interstitialAdParameterParcel);
                        zzu.zzfo().zza(this.zzajs.zzagf, adOverlayInfoParcel);
                        return;
                    }
                    Future future = (Future) new zza(this.zzalx).zzpy();
                    return;
                }
            } else {
                return;
            }
        }
        zzkd.zzcx(str);
    }

    /* access modifiers changed from: protected */
    public zzlh zza(com.google.android.gms.internal.zzju.zza zza2, @Nullable zze zze, @Nullable zzjo zzjo) {
        zzlh zza3 = zzu.zzfr().zza(this.zzajs.zzagf, this.zzajs.zzapa, false, false, this.zzajs.zzaov, this.zzajs.zzaow, this.zzajn, this, this.zzajv);
        zza3.zzuj().zza(this, null, this, this, ((Boolean) zzdc.zzazt.get()).booleanValue(), this, this, zze, null, zzjo);
        zza((zzft) zza3);
        zza3.zzcz(zza2.zzcip.zzcbg);
        zzey.zza(zza3, (com.google.android.gms.internal.zzey.zza) this);
        return zza3;
    }

    public void zza(com.google.android.gms.internal.zzju.zza zza2, zzdk zzdk) {
        if (!((Boolean) zzdc.zzbae.get()).booleanValue()) {
            super.zza(zza2, zzdk);
        } else if (zza2.errorCode != -2) {
            super.zza(zza2, zzdk);
        } else {
            Bundle bundle = zza2.zzcip.zzcar.zzatw.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
            boolean z = true ^ zza2.zzciq.zzcby;
            if ((bundle == null || !bundle.containsKey("gw")) && z) {
                this.zzajs.zzapc = zzc(zza2);
            }
            super.zza(this.zzajs.zzapc, zzdk);
        }
    }

    public void zza(boolean z, float f) {
        this.zzaly = z;
        this.zzalz = f;
    }

    public boolean zza(AdRequestParcel adRequestParcel, zzdk zzdk) {
        if (this.zzajs.zzapb == null) {
            return super.zza(adRequestParcel, zzdk);
        }
        zzkd.zzcx("An interstitial is already loading. Aborting.");
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean zza(AdRequestParcel adRequestParcel, zzju zzju, boolean z) {
        if (this.zzajs.zzgp() && zzju.zzbtm != null) {
            zzu.zzfs().zzi(zzju.zzbtm);
        }
        return this.zzajr.zzfc();
    }

    public boolean zza(@Nullable zzju zzju, zzju zzju2) {
        if (!super.zza(zzju, zzju2)) {
            return false;
        }
        if (!(this.zzajs.zzgp() || this.zzajs.zzapv == null || zzju2.zzcie == null)) {
            this.zzaju.zza(this.zzajs.zzapa, zzju2, this.zzajs.zzapv);
        }
        return true;
    }

    public void zzb(RewardItemParcel rewardItemParcel) {
        if (this.zzajs.zzapb != null) {
            if (this.zzajs.zzapb.zzccp != null) {
                zzu.zzfq().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb.zzccp);
            }
            if (this.zzajs.zzapb.zzccn != null) {
                rewardItemParcel = this.zzajs.zzapb.zzccn;
            }
        }
        zza(rewardItemParcel);
    }

    /* access modifiers changed from: protected */
    public void zzdr() {
        zzeu();
        super.zzdr();
    }

    /* access modifiers changed from: protected */
    public void zzdu() {
        super.zzdu();
        this.zzalw = true;
    }

    public void zzdy() {
        recordImpression();
        super.zzdy();
        if (this.zzajs.zzapb != null && this.zzajs.zzapb.zzbtm != null) {
            zzli zzuj = this.zzajs.zzapb.zzbtm.zzuj();
            if (zzuj != null) {
                zzuj.zzva();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzet() {
        boolean z = false;
        if (!(this.zzajs.zzagf instanceof Activity)) {
            return false;
        }
        Window window = ((Activity) this.zzajs.zzagf).getWindow();
        if (window != null) {
            if (window.getDecorView() == null) {
                return false;
            }
            Rect rect = new Rect();
            Rect rect2 = new Rect();
            window.getDecorView().getGlobalVisibleRect(rect, null);
            window.getDecorView().getWindowVisibleDisplayFrame(rect2);
            if (!(rect.bottom == 0 || rect2.bottom == 0 || rect.top != rect2.top)) {
                z = true;
            }
        }
        return z;
    }

    public void zzeu() {
        zzu.zzgh().zzb(Integer.valueOf(this.zzalx));
        if (this.zzajs.zzgp()) {
            this.zzajs.zzgm();
            this.zzajs.zzapb = null;
            this.zzajs.zzame = false;
            this.zzalw = false;
        }
    }

    public void zzev() {
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzcij == null)) {
            zzu.zzfq().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb.zzcij);
        }
        zzdv();
    }

    public void zzg(boolean z) {
        this.zzajs.zzame = z;
    }
}
