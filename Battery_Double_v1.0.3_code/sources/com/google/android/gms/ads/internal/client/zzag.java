package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.reward.client.zzi;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzin;

@zzin
public class zzag {
    private static final Object zzamr = new Object();
    private static zzag zzawr;
    private zzz zzaws;
    private RewardedVideoAd zzawt;

    private zzag() {
    }

    public static zzag zzjo() {
        zzag zzag;
        synchronized (zzamr) {
            if (zzawr == null) {
                zzawr = new zzag();
            }
            zzag = zzawr;
        }
        return zzag;
    }

    public RewardedVideoAd getRewardedVideoAdInstance(Context context) {
        synchronized (zzamr) {
            if (this.zzawt != null) {
                RewardedVideoAd rewardedVideoAd = this.zzawt;
                return rewardedVideoAd;
            }
            this.zzawt = new zzi(context, zzm.zzix().zza(context, (zzgj) new zzgi()));
            RewardedVideoAd rewardedVideoAd2 = this.zzawt;
            return rewardedVideoAd2;
        }
    }

    public void setAppMuted(boolean z) {
        zzab.zza(this.zzaws != null, (Object) "MobileAds.initialize() must be called prior to setting the app volume.");
        try {
            this.zzaws.setAppMuted(z);
        } catch (RemoteException e) {
            zzb.zzb("Unable to set app mute state.", e);
        }
    }

    public void setAppVolume(float f) {
        boolean z = false;
        zzab.zzb(0.0f <= f && f <= 1.0f, (Object) "The app volume must be a value between 0 and 1 inclusive.");
        if (this.zzaws != null) {
            z = true;
        }
        zzab.zza(z, (Object) "MobileAds.initialize() must be called prior to setting the app volume.");
        try {
            this.zzaws.setAppVolume(f);
        } catch (RemoteException e) {
            zzb.zzb("Unable to set app volume.", e);
        }
    }

    public void zza(Context context, String str, zzah zzah) {
        synchronized (zzamr) {
            if (this.zzaws == null) {
                if (context == null) {
                    throw new IllegalArgumentException("Context cannot be null.");
                }
                try {
                    this.zzaws = zzm.zzix().zzl(context);
                    this.zzaws.initialize();
                    if (str != null) {
                        this.zzaws.zzu(str);
                    }
                } catch (RemoteException e) {
                    zzb.zzd("Fail to initialize or set applicationCode on mobile ads setting manager", e);
                }
            }
        }
    }
}
