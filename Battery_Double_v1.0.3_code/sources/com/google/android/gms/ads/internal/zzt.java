package com.google.android.gms.ads.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.actions.SearchIntents;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzbw;
import com.google.android.gms.internal.zzbx;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdo;
import com.google.android.gms.internal.zzho;
import com.google.android.gms.internal.zzhs;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkg;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzin
public class zzt extends com.google.android.gms.ads.internal.client.zzu.zza {
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    @Nullable
    public zzq zzalf;
    /* access modifiers changed from: private */
    public final VersionInfoParcel zzalo;
    private final AdSizeParcel zzani;
    /* access modifiers changed from: private */
    public final Future<zzbw> zzanj = zzfg();
    private final zzb zzank;
    /* access modifiers changed from: private */
    @Nullable
    public WebView zzanl = new WebView(this.mContext);
    /* access modifiers changed from: private */
    @Nullable
    public zzbw zzanm;
    private AsyncTask<Void, Void, Void> zzann;

    private class zza extends AsyncTask<Void, Void, Void> {
        private zza() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            try {
                zzt.this.zzanm = (zzbw) zzt.this.zzanj.get(((Long) zzdc.zzbdd.get()).longValue(), TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException e) {
                zzkd.zzd("Failed to load ad data", e);
            } catch (TimeoutException unused) {
                zzkd.zzcx("Timed out waiting for ad data");
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
            String zzfe = zzt.this.zzfe();
            if (zzt.this.zzanl != null) {
                zzt.this.zzanl.loadUrl(zzfe);
            }
        }
    }

    private static class zzb {
        private final String zzanp;
        private final Map<String, String> zzanq = new TreeMap();
        private String zzanr;
        private String zzans;

        public zzb(String str) {
            this.zzanp = str;
        }

        public String getQuery() {
            return this.zzanr;
        }

        public String zzfi() {
            return this.zzans;
        }

        public String zzfj() {
            return this.zzanp;
        }

        public Map<String, String> zzfk() {
            return this.zzanq;
        }

        public void zzh(AdRequestParcel adRequestParcel) {
            this.zzanr = adRequestParcel.zzatt.zzaxl;
            Bundle bundle = adRequestParcel.zzatw != null ? adRequestParcel.zzatw.getBundle(AdMobAdapter.class.getName()) : null;
            if (bundle != null) {
                String str = (String) zzdc.zzbdc.get();
                for (String str2 : bundle.keySet()) {
                    if (str.equals(str2)) {
                        this.zzans = bundle.getString(str2);
                    } else if (str2.startsWith("csa_")) {
                        this.zzanq.put(str2.substring("csa_".length()), bundle.getString(str2));
                    }
                }
            }
        }
    }

    public zzt(Context context, AdSizeParcel adSizeParcel, String str, VersionInfoParcel versionInfoParcel) {
        this.mContext = context;
        this.zzalo = versionInfoParcel;
        this.zzani = adSizeParcel;
        this.zzank = new zzb(str);
        zzfd();
    }

    private void zzfd() {
        zzj(0);
        this.zzanl.setVerticalScrollBarEnabled(false);
        this.zzanl.getSettings().setJavaScriptEnabled(true);
        this.zzanl.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                if (zzt.this.zzalf != null) {
                    try {
                        zzt.this.zzalf.onAdFailedToLoad(0);
                    } catch (RemoteException e) {
                        zzkd.zzd("Could not call AdListener.onAdFailedToLoad().", e);
                    }
                }
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (str.startsWith(zzt.this.zzff())) {
                    return false;
                }
                if (str.startsWith((String) zzdc.zzbcy.get())) {
                    if (zzt.this.zzalf != null) {
                        try {
                            zzt.this.zzalf.onAdFailedToLoad(3);
                        } catch (RemoteException e) {
                            zzkd.zzd("Could not call AdListener.onAdFailedToLoad().", e);
                        }
                    }
                    zzt.this.zzj(0);
                    return true;
                } else if (str.startsWith((String) zzdc.zzbcz.get())) {
                    if (zzt.this.zzalf != null) {
                        try {
                            zzt.this.zzalf.onAdFailedToLoad(0);
                        } catch (RemoteException e2) {
                            zzkd.zzd("Could not call AdListener.onAdFailedToLoad().", e2);
                        }
                    }
                    zzt.this.zzj(0);
                    return true;
                } else if (str.startsWith((String) zzdc.zzbda.get())) {
                    if (zzt.this.zzalf != null) {
                        try {
                            zzt.this.zzalf.onAdLoaded();
                        } catch (RemoteException e3) {
                            zzkd.zzd("Could not call AdListener.onAdLoaded().", e3);
                        }
                    }
                    zzt.this.zzj(zzt.this.zzw(str));
                    return true;
                } else if (str.startsWith("gmsg://")) {
                    return true;
                } else {
                    if (zzt.this.zzalf != null) {
                        try {
                            zzt.this.zzalf.onAdLeftApplication();
                        } catch (RemoteException e4) {
                            zzkd.zzd("Could not call AdListener.onAdLeftApplication().", e4);
                        }
                    }
                    zzt.this.zzy(zzt.this.zzx(str));
                    return true;
                }
            }
        });
        this.zzanl.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (zzt.this.zzanm != null) {
                    try {
                        zzt.this.zzanm.zza(motionEvent);
                    } catch (RemoteException e) {
                        zzkd.zzd("Unable to process ad data", e);
                    }
                }
                return false;
            }
        });
    }

    private Future<zzbw> zzfg() {
        return zzkg.zza((Callable<T>) new Callable<zzbw>() {
            /* renamed from: zzfh */
            public zzbw call() throws Exception {
                return new zzbw(zzt.this.zzalo.zzcs, zzt.this.mContext, false);
            }
        });
    }

    /* access modifiers changed from: private */
    public String zzx(String str) {
        String str2;
        if (this.zzanm == null) {
            return str;
        }
        Uri parse = Uri.parse(str);
        try {
            parse = this.zzanm.zzd(parse, this.mContext);
        } catch (RemoteException e) {
            e = e;
            str2 = "Unable to process ad data";
            zzkd.zzd(str2, e);
            return parse.toString();
        } catch (zzbx e2) {
            e = e2;
            str2 = "Unable to parse ad click url";
            zzkd.zzd(str2, e);
            return parse.toString();
        }
        return parse.toString();
    }

    /* access modifiers changed from: private */
    public void zzy(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        this.mContext.startActivity(intent);
    }

    public void destroy() throws RemoteException {
        zzab.zzhi("destroy must be called on the main UI thread.");
        this.zzann.cancel(true);
        this.zzanj.cancel(true);
        this.zzanl.destroy();
        this.zzanl = null;
    }

    @Nullable
    public String getMediationAdapterClassName() throws RemoteException {
        return null;
    }

    public boolean isLoading() throws RemoteException {
        return false;
    }

    public boolean isReady() throws RemoteException {
        return false;
    }

    public void pause() throws RemoteException {
        zzab.zzhi("pause must be called on the main UI thread.");
    }

    public void resume() throws RemoteException {
        zzab.zzhi("resume must be called on the main UI thread.");
    }

    public void setManualImpressionsEnabled(boolean z) throws RemoteException {
    }

    public void setUserId(String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void showInterstitial() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void stopLoading() throws RemoteException {
    }

    public void zza(AdSizeParcel adSizeParcel) throws RemoteException {
        throw new IllegalStateException("AdSize must be set before initialization");
    }

    public void zza(VideoOptionsParcel videoOptionsParcel) {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzp zzp) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzq zzq) throws RemoteException {
        this.zzalf = zzq;
    }

    public void zza(zzw zzw) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzy zzy) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzd zzd) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzdo zzdo) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzho zzho) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public void zza(zzhs zzhs, String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public boolean zzb(AdRequestParcel adRequestParcel) throws RemoteException {
        zzab.zzb(this.zzanl, (Object) "This Search Ad has already been torn down");
        this.zzank.zzh(adRequestParcel);
        this.zzann = new zza().execute(new Void[0]);
        return true;
    }

    public com.google.android.gms.dynamic.zzd zzdm() throws RemoteException {
        zzab.zzhi("getAdFrame must be called on the main UI thread.");
        return zze.zzac(this.zzanl);
    }

    public AdSizeParcel zzdn() throws RemoteException {
        return this.zzani;
    }

    public void zzdp() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    @Nullable
    public com.google.android.gms.ads.internal.client.zzab zzdq() {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public String zzfe() {
        Builder builder = new Builder();
        builder.scheme("https://").appendEncodedPath((String) zzdc.zzbdb.get());
        builder.appendQueryParameter(SearchIntents.EXTRA_QUERY, this.zzank.getQuery());
        builder.appendQueryParameter("pubId", this.zzank.zzfj());
        Map zzfk = this.zzank.zzfk();
        for (String str : zzfk.keySet()) {
            builder.appendQueryParameter(str, (String) zzfk.get(str));
        }
        Uri build = builder.build();
        if (this.zzanm != null) {
            try {
                build = this.zzanm.zzc(build, this.mContext);
            } catch (RemoteException | zzbx e) {
                zzkd.zzd("Unable to process ad data", e);
            }
        }
        String valueOf = String.valueOf(zzff());
        String valueOf2 = String.valueOf(build.getEncodedQuery());
        StringBuilder sb = new StringBuilder(1 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append("#");
        sb.append(valueOf2);
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public String zzff() {
        String zzfi = this.zzank.zzfi();
        if (TextUtils.isEmpty(zzfi)) {
            zzfi = "www.google.com";
        }
        String valueOf = String.valueOf("https://");
        String str = (String) zzdc.zzbdb.get();
        StringBuilder sb = new StringBuilder(0 + String.valueOf(valueOf).length() + String.valueOf(zzfi).length() + String.valueOf(str).length());
        sb.append(valueOf);
        sb.append(zzfi);
        sb.append(str);
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public void zzj(int i) {
        if (this.zzanl != null) {
            this.zzanl.setLayoutParams(new LayoutParams(-1, i));
        }
    }

    /* access modifiers changed from: 0000 */
    public int zzw(String str) {
        String queryParameter = Uri.parse(str).getQueryParameter("height");
        if (TextUtils.isEmpty(queryParameter)) {
            return 0;
        }
        try {
            return zzm.zziw().zza(this.mContext, Integer.parseInt(queryParameter));
        } catch (NumberFormatException unused) {
            return 0;
        }
    }
}
