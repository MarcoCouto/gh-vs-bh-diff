package com.google.android.gms.ads.internal.client;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzin;
import java.util.ArrayList;
import java.util.List;

@zzin
public final class zzf {
    private Bundle mExtras;
    private boolean zzakp;
    private long zzauc;
    private int zzaud;
    private List<String> zzaue;
    private boolean zzauf;
    private int zzaug;
    private String zzauh;
    private SearchAdRequestParcel zzaui;
    private String zzauj;
    private Bundle zzauk;
    private Bundle zzaul;
    private List<String> zzaum;
    private String zzaun;
    private String zzauo;
    private boolean zzaup;
    private Location zzft;

    public zzf() {
        this.zzauc = -1;
        this.mExtras = new Bundle();
        this.zzaud = -1;
        this.zzaue = new ArrayList();
        this.zzauf = false;
        this.zzaug = -1;
        this.zzakp = false;
        this.zzauh = null;
        this.zzaui = null;
        this.zzft = null;
        this.zzauj = null;
        this.zzauk = new Bundle();
        this.zzaul = new Bundle();
        this.zzaum = new ArrayList();
        this.zzaun = null;
        this.zzauo = null;
        this.zzaup = false;
    }

    public zzf(AdRequestParcel adRequestParcel) {
        this.zzauc = adRequestParcel.zzatm;
        this.mExtras = adRequestParcel.extras;
        this.zzaud = adRequestParcel.zzatn;
        this.zzaue = adRequestParcel.zzato;
        this.zzauf = adRequestParcel.zzatp;
        this.zzaug = adRequestParcel.zzatq;
        this.zzakp = adRequestParcel.zzatr;
        this.zzauh = adRequestParcel.zzats;
        this.zzaui = adRequestParcel.zzatt;
        this.zzft = adRequestParcel.zzatu;
        this.zzauj = adRequestParcel.zzatv;
        this.zzauk = adRequestParcel.zzatw;
        this.zzaul = adRequestParcel.zzatx;
        this.zzaum = adRequestParcel.zzaty;
        this.zzaun = adRequestParcel.zzatz;
        this.zzauo = adRequestParcel.zzaua;
    }

    public zzf zza(@Nullable Location location) {
        this.zzft = location;
        return this;
    }

    public zzf zzc(Bundle bundle) {
        this.zzauk = bundle;
        return this;
    }

    public AdRequestParcel zzig() {
        long j = this.zzauc;
        Bundle bundle = this.mExtras;
        int i = this.zzaud;
        List<String> list = this.zzaue;
        boolean z = this.zzauf;
        int i2 = this.zzaug;
        boolean z2 = this.zzakp;
        String str = this.zzauh;
        SearchAdRequestParcel searchAdRequestParcel = this.zzaui;
        Location location = this.zzft;
        String str2 = this.zzauj;
        Bundle bundle2 = this.zzauk;
        Bundle bundle3 = this.zzaul;
        Bundle bundle4 = bundle3;
        Bundle bundle5 = bundle4;
        AdRequestParcel adRequestParcel = new AdRequestParcel(7, j, bundle, i, list, z, i2, z2, str, searchAdRequestParcel, location, str2, bundle2, bundle5, this.zzaum, this.zzaun, this.zzauo, false);
        return adRequestParcel;
    }
}
