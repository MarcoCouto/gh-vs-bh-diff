package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.request.zza.C0015zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzfp;
import com.google.android.gms.internal.zzfs;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zziq;
import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkl;
import com.google.android.gms.internal.zzlh;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzn extends zzkc {
    private static final Object zzamr = new Object();
    /* access modifiers changed from: private */
    public static zzfs zzbyv = null;
    static final long zzcdf = TimeUnit.SECONDS.toMillis(10);
    static boolean zzcdg = false;
    private static zzeq zzcdh;
    /* access modifiers changed from: private */
    public static zzeu zzcdi;
    private static zzep zzcdj;
    private final Context mContext;
    private final Object zzbxu = new Object();
    /* access modifiers changed from: private */
    public final C0015zza zzcae;
    private final com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zzcaf;
    /* access modifiers changed from: private */
    public com.google.android.gms.internal.zzfs.zzc zzcdk;

    public static class zza implements zzkl<zzfp> {
        /* renamed from: zza */
        public void zzd(zzfp zzfp) {
            zzn.zzc(zzfp);
        }
    }

    public static class zzb implements zzkl<zzfp> {
        /* renamed from: zza */
        public void zzd(zzfp zzfp) {
            zzn.zzb(zzfp);
        }
    }

    public static class zzc implements zzep {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("request_id");
            String str2 = "Invalid request: ";
            String valueOf = String.valueOf((String) map.get("errors"));
            zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            zzn.zzcdi.zzax(str);
        }
    }

    public zzn(Context context, com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zza2, C0015zza zza3) {
        super(true);
        this.zzcae = zza3;
        this.mContext = context;
        this.zzcaf = zza2;
        synchronized (zzamr) {
            if (!zzcdg) {
                zzcdi = new zzeu();
                zzcdh = new zzeq(context.getApplicationContext(), zza2.zzaow);
                zzcdj = new zzc();
                zzfs zzfs = new zzfs(this.mContext.getApplicationContext(), this.zzcaf.zzaow, (String) zzdc.zzaxy.get(), new zzb(), new zza());
                zzbyv = zzfs;
                zzcdg = true;
            }
        }
    }

    private JSONObject zza(AdRequestInfoParcel adRequestInfoParcel, String str) {
        Info info;
        Bundle bundle = adRequestInfoParcel.zzcar.extras.getBundle("sdk_less_server_data");
        String string = adRequestInfoParcel.zzcar.extras.getString("sdk_less_network_id");
        if (bundle == null) {
            return null;
        }
        JSONObject zza2 = zziq.zza(this.mContext, adRequestInfoParcel, zzu.zzfw().zzy(this.mContext), null, null, new zzcv((String) zzdc.zzaxy.get()), null, new ArrayList(), null, null);
        if (zza2 == null) {
            return null;
        }
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(this.mContext);
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e) {
            zzkd.zzd("Cannot get advertising id info", e);
            info = null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("request_id", str);
        hashMap.put("network_id", string);
        hashMap.put("request_param", zza2);
        hashMap.put("data", bundle);
        if (info != null) {
            hashMap.put("adid", info.getId());
            hashMap.put("lat", Integer.valueOf(info.isLimitAdTrackingEnabled() ? 1 : 0));
        }
        try {
            return zzu.zzfq().zzam((Map<String, ?>) hashMap);
        } catch (JSONException unused) {
            return null;
        }
    }

    protected static void zzb(zzfp zzfp) {
        zzfp.zza("/loadAd", (zzep) zzcdi);
        zzfp.zza("/fetchHttpRequest", (zzep) zzcdh);
        zzfp.zza("/invalidRequest", zzcdj);
    }

    protected static void zzc(zzfp zzfp) {
        zzfp.zzb("/loadAd", (zzep) zzcdi);
        zzfp.zzb("/fetchHttpRequest", (zzep) zzcdh);
        zzfp.zzb("/invalidRequest", zzcdj);
    }

    private AdResponseParcel zze(AdRequestInfoParcel adRequestInfoParcel) {
        final String zzte = zzu.zzfq().zzte();
        final JSONObject zza2 = zza(adRequestInfoParcel, zzte);
        if (zza2 == null) {
            return new AdResponseParcel(0);
        }
        long elapsedRealtime = zzu.zzfu().elapsedRealtime();
        Future zzaw = zzcdi.zzaw(zzte);
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                zzn.this.zzcdk = zzn.zzbyv.zzma();
                zzn.this.zzcdk.zza(new com.google.android.gms.internal.zzla.zzc<zzft>() {
                    /* renamed from: zzb */
                    public void zzd(zzft zzft) {
                        try {
                            zzft.zza("AFMA_getAdapterLessMediationAd", zza2);
                        } catch (Exception e) {
                            zzkd.zzb("Error requesting an ad url", e);
                            zzn.zzcdi.zzax(zzte);
                        }
                    }
                }, new com.google.android.gms.internal.zzla.zza() {
                    public void run() {
                        zzn.zzcdi.zzax(zzte);
                    }
                });
            }
        });
        try {
            JSONObject jSONObject = (JSONObject) zzaw.get(zzcdf - (zzu.zzfu().elapsedRealtime() - elapsedRealtime), TimeUnit.MILLISECONDS);
            if (jSONObject == null) {
                return new AdResponseParcel(-1);
            }
            AdResponseParcel zza3 = zziq.zza(this.mContext, adRequestInfoParcel, jSONObject.toString());
            if (zza3.errorCode != -3 && TextUtils.isEmpty(zza3.body)) {
                zza3 = new AdResponseParcel(3);
            }
            return zza3;
        } catch (InterruptedException | CancellationException unused) {
            return new AdResponseParcel(-1);
        } catch (TimeoutException unused2) {
            return new AdResponseParcel(2);
        } catch (ExecutionException unused3) {
            return new AdResponseParcel(0);
        }
    }

    public void onStop() {
        synchronized (this.zzbxu) {
            com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
                public void run() {
                    if (zzn.this.zzcdk != null) {
                        zzn.this.zzcdk.release();
                        zzn.this.zzcdk = null;
                    }
                }
            });
        }
    }

    public void zzew() {
        zzkd.zzcv("SdkLessAdLoaderBackgroundTask started.");
        AdRequestInfoParcel adRequestInfoParcel = new AdRequestInfoParcel(this.zzcaf, null, -1);
        AdResponseParcel zze = zze(adRequestInfoParcel);
        final com.google.android.gms.internal.zzju.zza zza2 = new com.google.android.gms.internal.zzju.zza(adRequestInfoParcel, zze, null, null, zze.errorCode, zzu.zzfu().elapsedRealtime(), zze.zzccc, null);
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                zzn.this.zzcae.zza(zza2);
                if (zzn.this.zzcdk != null) {
                    zzn.this.zzcdk.release();
                    zzn.this.zzcdk = null;
                }
            }
        });
    }
}
