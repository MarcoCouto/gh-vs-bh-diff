package com.google.android.gms.internal;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.zzu;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zzis {
    private int mOrientation = -1;
    private boolean zzawn = false;
    private String zzbfi;
    private final AdRequestInfoParcel zzbot;
    private List<String> zzbzf;
    private String zzcet;
    private String zzceu;
    private List<String> zzcev;
    private String zzcew;
    private String zzcex;
    private List<String> zzcey;
    private long zzcez = -1;
    private boolean zzcfa = false;
    private final long zzcfb = -1;
    private long zzcfc = -1;
    private boolean zzcfd = false;
    private boolean zzcfe = false;
    private boolean zzcff = false;
    private boolean zzcfg = true;
    private String zzcfh = "";
    private boolean zzcfi = false;
    private RewardItemParcel zzcfj;
    private List<String> zzcfk;
    private List<String> zzcfl;
    private boolean zzcfm = false;
    private AutoClickProtectionConfigurationParcel zzcfn;
    private boolean zzcfo = false;
    private String zzcfp;
    private List<String> zzcfq;
    private String zzcfr;
    private boolean zzcfs;
    private String zzcft;

    public zzis(AdRequestInfoParcel adRequestInfoParcel) {
        this.zzbot = adRequestInfoParcel;
    }

    private void zzaa(Map<String, List<String>> map) {
        List list = (List) map.get("X-Afma-Gws-Query-Id");
        if (list != null && !list.isEmpty()) {
            this.zzcfh = (String) list.get(0);
        }
    }

    private void zzab(Map<String, List<String>> map) {
        String zzd = zzd(map, "X-Afma-Fluid");
        if (zzd != null && zzd.equals("height")) {
            this.zzcfi = true;
        }
    }

    private void zzac(Map<String, List<String>> map) {
        this.zzawn = "native_express".equals(zzd(map, "X-Afma-Ad-Format"));
    }

    private void zzad(Map<String, List<String>> map) {
        this.zzcfj = RewardItemParcel.zzch(zzd(map, "X-Afma-Rewards"));
    }

    private void zzae(Map<String, List<String>> map) {
        if (this.zzcfk == null) {
            this.zzcfk = zzf(map, "X-Afma-Reward-Video-Start-Urls");
        }
    }

    private void zzaf(Map<String, List<String>> map) {
        if (this.zzcfl == null) {
            this.zzcfl = zzf(map, "X-Afma-Reward-Video-Complete-Urls");
        }
    }

    private void zzag(Map<String, List<String>> map) {
        this.zzcfm = zzg(map, "X-Afma-Use-Displayed-Impression") | this.zzcfm;
    }

    private void zzah(Map<String, List<String>> map) {
        this.zzcfo = zzg(map, "X-Afma-Auto-Collect-Location") | this.zzcfo;
    }

    private void zzai(Map<String, List<String>> map) {
        List<String> zzf = zzf(map, "X-Afma-Remote-Ping-Urls");
        if (zzf != null) {
            this.zzcfq = zzf;
        }
    }

    private void zzaj(Map<String, List<String>> map) {
        String zzd = zzd(map, "X-Afma-Auto-Protection-Configuration");
        if (zzd == null || TextUtils.isEmpty(zzd)) {
            Builder buildUpon = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204").buildUpon();
            buildUpon.appendQueryParameter("id", "gmob-apps-blocked-navigation");
            if (!TextUtils.isEmpty(this.zzcew)) {
                buildUpon.appendQueryParameter("debugDialog", this.zzcew);
            }
            boolean booleanValue = ((Boolean) zzdc.zzayg.get()).booleanValue();
            String valueOf = String.valueOf(buildUpon.toString());
            String valueOf2 = String.valueOf("navigationURL");
            StringBuilder sb = new StringBuilder(18 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
            sb.append(valueOf);
            sb.append("&");
            sb.append(valueOf2);
            sb.append("={NAVIGATION_URL}");
            this.zzcfn = new AutoClickProtectionConfigurationParcel(booleanValue, Arrays.asList(new String[]{sb.toString()}));
            return;
        }
        try {
            this.zzcfn = AutoClickProtectionConfigurationParcel.zzh(new JSONObject(zzd));
        } catch (JSONException e) {
            zzkd.zzd("Error parsing configuration JSON", e);
            this.zzcfn = new AutoClickProtectionConfigurationParcel();
        }
    }

    private void zzak(Map<String, List<String>> map) {
        this.zzcfp = zzd(map, "Set-Cookie");
    }

    private void zzal(Map<String, List<String>> map) {
        this.zzcfr = zzd(map, "X-Afma-Safe-Browsing");
    }

    static String zzd(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (String) list.get(0);
    }

    static long zze(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list != null && !list.isEmpty()) {
            String str2 = (String) list.get(0);
            try {
                return (long) (Float.parseFloat(str2) * 1000.0f);
            } catch (NumberFormatException unused) {
                StringBuilder sb = new StringBuilder(36 + String.valueOf(str).length() + String.valueOf(str2).length());
                sb.append("Could not parse float from ");
                sb.append(str);
                sb.append(" header: ");
                sb.append(str2);
                zzkd.zzcx(sb.toString());
            }
        }
        return -1;
    }

    static List<String> zzf(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        if (list != null && !list.isEmpty()) {
            String str2 = (String) list.get(0);
            if (str2 != null) {
                return Arrays.asList(str2.trim().split("\\s+"));
            }
        }
        return null;
    }

    private boolean zzg(Map<String, List<String>> map, String str) {
        List list = (List) map.get(str);
        return list != null && !list.isEmpty() && Boolean.valueOf((String) list.get(0)).booleanValue();
    }

    private void zzk(Map<String, List<String>> map) {
        this.zzcet = zzd(map, "X-Afma-Ad-Size");
    }

    private void zzl(Map<String, List<String>> map) {
        this.zzcft = zzd(map, "X-Afma-Ad-Slot-Size");
    }

    private void zzm(Map<String, List<String>> map) {
        List<String> zzf = zzf(map, "X-Afma-Click-Tracking-Urls");
        if (zzf != null) {
            this.zzcev = zzf;
        }
    }

    private void zzn(Map<String, List<String>> map) {
        List list = (List) map.get("X-Afma-Debug-Dialog");
        if (list != null && !list.isEmpty()) {
            this.zzcew = (String) list.get(0);
        }
    }

    private void zzo(Map<String, List<String>> map) {
        List<String> zzf = zzf(map, "X-Afma-Tracking-Urls");
        if (zzf != null) {
            this.zzcey = zzf;
        }
    }

    private void zzp(Map<String, List<String>> map) {
        long zze = zze(map, "X-Afma-Interstitial-Timeout");
        if (zze != -1) {
            this.zzcez = zze;
        }
    }

    private void zzq(Map<String, List<String>> map) {
        this.zzcex = zzd(map, "X-Afma-ActiveView");
    }

    private void zzr(Map<String, List<String>> map) {
        this.zzcfe = "native".equals(zzd(map, "X-Afma-Ad-Format"));
    }

    private void zzs(Map<String, List<String>> map) {
        this.zzcfd = zzg(map, "X-Afma-Custom-Rendering-Allowed") | this.zzcfd;
    }

    private void zzt(Map<String, List<String>> map) {
        this.zzcfa = zzg(map, "X-Afma-Mediation") | this.zzcfa;
    }

    private void zzu(Map<String, List<String>> map) {
        this.zzcfs = zzg(map, "X-Afma-Render-In-Browser") | this.zzcfs;
    }

    private void zzv(Map<String, List<String>> map) {
        List<String> zzf = zzf(map, "X-Afma-Manual-Tracking-Urls");
        if (zzf != null) {
            this.zzbzf = zzf;
        }
    }

    private void zzw(Map<String, List<String>> map) {
        long zze = zze(map, "X-Afma-Refresh-Rate");
        if (zze != -1) {
            this.zzcfc = zze;
        }
    }

    private void zzx(Map<String, List<String>> map) {
        int zztj;
        List list = (List) map.get("X-Afma-Orientation");
        if (list != null && !list.isEmpty()) {
            String str = (String) list.get(0);
            if ("portrait".equalsIgnoreCase(str)) {
                zztj = zzu.zzfs().zztk();
            } else if ("landscape".equalsIgnoreCase(str)) {
                zztj = zzu.zzfs().zztj();
            }
            this.mOrientation = zztj;
        }
    }

    private void zzy(Map<String, List<String>> map) {
        List list = (List) map.get("X-Afma-Use-HTTPS");
        if (list != null && !list.isEmpty()) {
            this.zzcff = Boolean.valueOf((String) list.get(0)).booleanValue();
        }
    }

    private void zzz(Map<String, List<String>> map) {
        List list = (List) map.get("X-Afma-Content-Url-Opted-Out");
        if (list != null && !list.isEmpty()) {
            this.zzcfg = Boolean.valueOf((String) list.get(0)).booleanValue();
        }
    }

    public void zzb(String str, Map<String, List<String>> map, String str2) {
        this.zzceu = str;
        this.zzbfi = str2;
        zzj(map);
    }

    public AdResponseParcel zzj(long j) {
        AdRequestInfoParcel adRequestInfoParcel = this.zzbot;
        String str = this.zzceu;
        String str2 = this.zzbfi;
        List<String> list = this.zzcev;
        List<String> list2 = this.zzcey;
        long j2 = this.zzcez;
        boolean z = this.zzcfa;
        List<String> list3 = this.zzbzf;
        long j3 = this.zzcfc;
        int i = this.mOrientation;
        String str3 = this.zzcet;
        String str4 = this.zzcew;
        String str5 = this.zzcex;
        String str6 = str3;
        boolean z2 = this.zzcfd;
        boolean z3 = this.zzcfe;
        boolean z4 = this.zzcff;
        boolean z5 = this.zzcfg;
        String str7 = this.zzcfh;
        boolean z6 = this.zzcfi;
        boolean z7 = this.zzawn;
        RewardItemParcel rewardItemParcel = this.zzcfj;
        List<String> list4 = this.zzcfk;
        List<String> list5 = this.zzcfl;
        boolean z8 = this.zzcfm;
        AutoClickProtectionConfigurationParcel autoClickProtectionConfigurationParcel = this.zzcfn;
        boolean z9 = this.zzcfo;
        String str8 = this.zzcfp;
        List<String> list6 = this.zzcfq;
        String str9 = this.zzcfr;
        String str10 = str6;
        long j4 = j;
        AdResponseParcel adResponseParcel = new AdResponseParcel(adRequestInfoParcel, str, str2, list, list2, j2, z, -1, list3, j3, i, str10, j4, str4, str5, z2, z3, z4, z5, false, str7, z6, z7, rewardItemParcel, list4, list5, z8, autoClickProtectionConfigurationParcel, z9, str8, list6, str9, this.zzcfs, this.zzcft);
        return adResponseParcel;
    }

    public void zzj(Map<String, List<String>> map) {
        zzk(map);
        zzl(map);
        zzm(map);
        zzn(map);
        zzo(map);
        zzp(map);
        zzt(map);
        zzv(map);
        zzw(map);
        zzx(map);
        zzq(map);
        zzy(map);
        zzs(map);
        zzr(map);
        zzz(map);
        zzaa(map);
        zzab(map);
        zzac(map);
        zzad(map);
        zzae(map);
        zzaf(map);
        zzag(map);
        zzah(map);
        zzak(map);
        zzaj(map);
        zzai(map);
        zzal(map);
        zzu(map);
    }
}
