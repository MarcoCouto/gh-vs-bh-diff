package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzbl extends zzbp {
    private static final Object zzafc = new Object();
    private static volatile String zzct;

    public zzbl(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        this.zzaha.zzct = "E";
        if (zzct == null) {
            synchronized (zzafc) {
                if (zzct == null) {
                    zzct = (String) this.zzahh.invoke(null, new Object[0]);
                }
            }
        }
        synchronized (this.zzaha) {
            this.zzaha.zzct = zzct;
        }
    }
}
