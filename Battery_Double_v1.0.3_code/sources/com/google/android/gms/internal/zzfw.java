package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.location.Location;

public interface zzfw {
    zzky<Location> zza(ApplicationInfo applicationInfo);
}
