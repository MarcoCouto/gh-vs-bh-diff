package com.google.android.gms.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza.C0023zza;
import com.google.android.gms.dynamic.zzd;
import java.util.List;

public interface zzgk extends IInterface {

    public static abstract class zza extends Binder implements zzgk {

        /* renamed from: com.google.android.gms.internal.zzgk$zza$zza reason: collision with other inner class name */
        private static class C0067zza implements zzgk {
            private IBinder zzahn;

            C0067zza(IBinder iBinder) {
                this.zzahn = iBinder;
            }

            public IBinder asBinder() {
                return this.zzahn;
            }

            public void destroy() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Bundle getInterstitialAdapterInfo() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzd getView() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return com.google.android.gms.dynamic.zzd.zza.zzfc(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isInitialized() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    boolean z = false;
                    this.zzahn.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void pause() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void resume() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void showInterstitial() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void showVideo() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(AdRequestParcel adRequestParcel, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.zzahn.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, com.google.android.gms.ads.internal.reward.mediation.client.zza zza, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    IBinder iBinder = null;
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (zza != null) {
                        iBinder = zza.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str2);
                    this.zzahn.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, zzgl zzgl) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    IBinder iBinder = null;
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (zzgl != null) {
                        iBinder = zzgl.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, String str2, zzgl zzgl) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    IBinder iBinder = null;
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (zzgl != null) {
                        iBinder = zzgl.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, String str2, zzgl zzgl, NativeAdOptionsParcel nativeAdOptionsParcel, List<String> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    IBinder iBinder = null;
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (zzgl != null) {
                        iBinder = zzgl.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    if (nativeAdOptionsParcel != null) {
                        obtain.writeInt(1);
                        nativeAdOptionsParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStringList(list);
                    this.zzahn.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(zzd zzd, AdSizeParcel adSizeParcel, AdRequestParcel adRequestParcel, String str, zzgl zzgl) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    IBinder iBinder = null;
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    if (adSizeParcel != null) {
                        obtain.writeInt(1);
                        adSizeParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (zzgl != null) {
                        iBinder = zzgl.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zza(zzd zzd, AdSizeParcel adSizeParcel, AdRequestParcel adRequestParcel, String str, String str2, zzgl zzgl) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    IBinder iBinder = null;
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    if (adSizeParcel != null) {
                        obtain.writeInt(1);
                        adSizeParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (zzgl != null) {
                        iBinder = zzgl.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.zzahn.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zzc(AdRequestParcel adRequestParcel, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    if (adRequestParcel != null) {
                        obtain.writeInt(1);
                        adRequestParcel.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.zzahn.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void zzj(zzd zzd) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    obtain.writeStrongBinder(zzd != null ? zzd.asBinder() : null);
                    this.zzahn.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzgn zzmo() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                    return com.google.android.gms.internal.zzgn.zza.zzan(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public zzgo zzmp() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    return com.google.android.gms.internal.zzgo.zza.zzao(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Bundle zzmq() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Bundle zzmr() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                    this.zzahn.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public zza() {
            attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
        }

        public static zzgk zzak(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof zzgk)) ? new C0067zza(iBinder) : (zzgk) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* JADX WARNING: type inference failed for: r2v0 */
        /* JADX WARNING: type inference failed for: r2v1 */
        /* JADX WARNING: type inference failed for: r6v0, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v3, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v4, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v5, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v6, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v8, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v9 */
        /* JADX WARNING: type inference failed for: r6v1, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v11, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v12 */
        /* JADX WARNING: type inference failed for: r5v6, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v14, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v15 */
        /* JADX WARNING: type inference failed for: r5v7, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v17, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v18, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v20, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v21 */
        /* JADX WARNING: type inference failed for: r9v1, types: [com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel] */
        /* JADX WARNING: type inference failed for: r2v23, types: [com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel] */
        /* JADX WARNING: type inference failed for: r2v24, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v25, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v26, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v27, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r2v28, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v30, types: [com.google.android.gms.ads.internal.client.AdRequestParcel] */
        /* JADX WARNING: type inference failed for: r2v31 */
        /* JADX WARNING: type inference failed for: r2v32 */
        /* JADX WARNING: type inference failed for: r2v33 */
        /* JADX WARNING: type inference failed for: r2v34 */
        /* JADX WARNING: type inference failed for: r2v35 */
        /* JADX WARNING: type inference failed for: r2v36 */
        /* JADX WARNING: type inference failed for: r2v37 */
        /* JADX WARNING: type inference failed for: r2v38 */
        /* JADX WARNING: type inference failed for: r2v39 */
        /* JADX WARNING: type inference failed for: r2v40 */
        /* JADX WARNING: type inference failed for: r2v41 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], android.os.IBinder, com.google.android.gms.ads.internal.client.AdRequestParcel, com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel]
  uses: [?[OBJECT, ARRAY], android.os.IBinder, com.google.android.gms.ads.internal.client.AdRequestParcel]
  mth insns count: 268
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 17 */
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 1598968902) {
                ? r2 = 0;
                switch (i) {
                    case 1:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd zzfc = com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder());
                        AdSizeParcel adSizeParcel = parcel.readInt() != 0 ? (AdSizeParcel) AdSizeParcel.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(zzfc, adSizeParcel, (AdRequestParcel) r2, parcel.readString(), com.google.android.gms.internal.zzgl.zza.zzal(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 2:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd view = getView();
                        parcel2.writeNoException();
                        if (view != null) {
                            r2 = view.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 3:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd zzfc2 = com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(zzfc2, r2, parcel.readString(), com.google.android.gms.internal.zzgl.zza.zzal(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 4:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        showInterstitial();
                        parcel2.writeNoException();
                        return true;
                    case 5:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        destroy();
                        parcel2.writeNoException();
                        return true;
                    case 6:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd zzfc3 = com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder());
                        AdSizeParcel adSizeParcel2 = parcel.readInt() != 0 ? (AdSizeParcel) AdSizeParcel.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(zzfc3, adSizeParcel2, r2, parcel.readString(), parcel.readString(), com.google.android.gms.internal.zzgl.zza.zzal(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 7:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd zzfc4 = com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(zzfc4, (AdRequestParcel) r2, parcel.readString(), parcel.readString(), com.google.android.gms.internal.zzgl.zza.zzal(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    case 8:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        pause();
                        parcel2.writeNoException();
                        return true;
                    case 9:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        resume();
                        parcel2.writeNoException();
                        return true;
                    case 10:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd zzfc5 = com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(zzfc5, (AdRequestParcel) r2, parcel.readString(), C0023zza.zzbj(parcel.readStrongBinder()), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 11:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zzc(r2, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 12:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        showVideo();
                        parcel2.writeNoException();
                        return true;
                    case 13:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        boolean isInitialized = isInitialized();
                        parcel2.writeNoException();
                        parcel2.writeInt(isInitialized);
                        return true;
                    case 14:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzd zzfc6 = com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder());
                        AdRequestParcel adRequestParcel = parcel.readInt() != 0 ? (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel) : null;
                        String readString = parcel.readString();
                        String readString2 = parcel.readString();
                        zzgl zzal = com.google.android.gms.internal.zzgl.zza.zzal(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r2 = (NativeAdOptionsParcel) NativeAdOptionsParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(zzfc6, adRequestParcel, readString, readString2, zzal, r2, parcel.createStringArrayList());
                        parcel2.writeNoException();
                        return true;
                    case 15:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzgn zzmo = zzmo();
                        parcel2.writeNoException();
                        if (zzmo != null) {
                            r2 = zzmo.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 16:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzgo zzmp = zzmp();
                        parcel2.writeNoException();
                        if (zzmp != null) {
                            r2 = zzmp.asBinder();
                        }
                        parcel2.writeStrongBinder(r2);
                        return true;
                    case 17:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        Bundle zzmq = zzmq();
                        parcel2.writeNoException();
                        if (zzmq != null) {
                            parcel2.writeInt(1);
                            zzmq.writeToParcel(parcel2, 1);
                            return true;
                        }
                        parcel2.writeInt(0);
                        return true;
                    case 18:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        Bundle interstitialAdapterInfo = getInterstitialAdapterInfo();
                        parcel2.writeNoException();
                        if (interstitialAdapterInfo != null) {
                            parcel2.writeInt(1);
                            interstitialAdapterInfo.writeToParcel(parcel2, 1);
                            return true;
                        }
                        parcel2.writeInt(0);
                        return true;
                    case 19:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        Bundle zzmr = zzmr();
                        parcel2.writeNoException();
                        if (zzmr != null) {
                            parcel2.writeInt(1);
                            zzmr.writeToParcel(parcel2, 1);
                            return true;
                        }
                        parcel2.writeInt(0);
                        return true;
                    case 20:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        if (parcel.readInt() != 0) {
                            r2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(parcel);
                        }
                        zza(r2, parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 21:
                        parcel.enforceInterface("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                        zzj(com.google.android.gms.dynamic.zzd.zza.zzfc(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
                return true;
            }
        }
    }

    void destroy() throws RemoteException;

    Bundle getInterstitialAdapterInfo() throws RemoteException;

    zzd getView() throws RemoteException;

    boolean isInitialized() throws RemoteException;

    void pause() throws RemoteException;

    void resume() throws RemoteException;

    void showInterstitial() throws RemoteException;

    void showVideo() throws RemoteException;

    void zza(AdRequestParcel adRequestParcel, String str, String str2) throws RemoteException;

    void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, com.google.android.gms.ads.internal.reward.mediation.client.zza zza2, String str2) throws RemoteException;

    void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, zzgl zzgl) throws RemoteException;

    void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, String str2, zzgl zzgl) throws RemoteException;

    void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, String str2, zzgl zzgl, NativeAdOptionsParcel nativeAdOptionsParcel, List<String> list) throws RemoteException;

    void zza(zzd zzd, AdSizeParcel adSizeParcel, AdRequestParcel adRequestParcel, String str, zzgl zzgl) throws RemoteException;

    void zza(zzd zzd, AdSizeParcel adSizeParcel, AdRequestParcel adRequestParcel, String str, String str2, zzgl zzgl) throws RemoteException;

    void zzc(AdRequestParcel adRequestParcel, String str) throws RemoteException;

    void zzj(zzd zzd) throws RemoteException;

    zzgn zzmo() throws RemoteException;

    zzgo zzmp() throws RemoteException;

    Bundle zzmq() throws RemoteException;

    Bundle zzmr() throws RemoteException;
}
