package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomClickListener;
import com.google.android.gms.internal.zzed.zza;

@zzin
public class zzei extends zza {
    private final OnCustomClickListener zzbhk;

    public zzei(OnCustomClickListener onCustomClickListener) {
        this.zzbhk = onCustomClickListener;
    }

    public void zza(zzdz zzdz, String str) {
        this.zzbhk.onCustomClick(new zzea(zzdz), str);
    }
}
