package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Map;

@zzin
public abstract class zzdh {
    @zzin
    public static final zzdh zzbdy = new zzdh() {
        public String zzg(@Nullable String str, String str2) {
            return str2;
        }
    };
    @zzin
    public static final zzdh zzbdz = new zzdh() {
        public String zzg(@Nullable String str, String str2) {
            return str != null ? str : str2;
        }
    };
    @zzin
    public static final zzdh zzbea = new zzdh() {
        @Nullable
        private String zzar(@Nullable String str) {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            int i = 0;
            int length = str.length();
            while (i < str.length() && str.charAt(i) == ',') {
                i++;
            }
            while (length > 0 && str.charAt(length - 1) == ',') {
                length--;
            }
            return (i == 0 && length == str.length()) ? str : str.substring(i, length);
        }

        public String zzg(@Nullable String str, String str2) {
            String zzar = zzar(str);
            String zzar2 = zzar(str2);
            if (TextUtils.isEmpty(zzar)) {
                return zzar2;
            }
            if (TextUtils.isEmpty(zzar2)) {
                return zzar;
            }
            StringBuilder sb = new StringBuilder(1 + String.valueOf(zzar).length() + String.valueOf(zzar2).length());
            sb.append(zzar);
            sb.append(",");
            sb.append(zzar2);
            return sb.toString();
        }
    };

    public final void zza(Map<String, String> map, String str, String str2) {
        map.put(str, zzg((String) map.get(str), str2));
    }

    public abstract String zzg(@Nullable String str, String str2);
}
