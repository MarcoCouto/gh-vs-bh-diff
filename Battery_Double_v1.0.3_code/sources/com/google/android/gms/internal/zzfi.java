package com.google.android.gms.internal;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.zzl;
import com.google.android.gms.ads.internal.zzu;
import java.util.LinkedList;
import java.util.List;

@zzin
class zzfi {
    /* access modifiers changed from: private */
    public final List<zza> zzalc = new LinkedList();

    interface zza {
        void zzb(zzfj zzfj) throws RemoteException;
    }

    zzfi() {
    }

    /* access modifiers changed from: 0000 */
    public void zza(final zzfj zzfj) {
        Handler handler = zzkh.zzclc;
        for (final zza zza2 : this.zzalc) {
            handler.post(new Runnable() {
                public void run() {
                    try {
                        zza2.zzb(zzfj);
                    } catch (RemoteException e) {
                        zzkd.zzd("Could not propagate interstitial ad event.", e);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzc(zzl zzl) {
        zzl.zza((zzq) new com.google.android.gms.ads.internal.client.zzq.zza() {
            public void onAdClosed() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzalf != null) {
                            zzfj.zzalf.onAdClosed();
                        }
                        zzu.zzgb().zzlo();
                    }
                });
            }

            public void onAdFailedToLoad(final int i) throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzalf != null) {
                            zzfj.zzalf.onAdFailedToLoad(i);
                        }
                    }
                });
                zzkd.v("Pooled interstitial failed to load.");
            }

            public void onAdLeftApplication() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzalf != null) {
                            zzfj.zzalf.onAdLeftApplication();
                        }
                    }
                });
            }

            public void onAdLoaded() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzalf != null) {
                            zzfj.zzalf.onAdLoaded();
                        }
                    }
                });
                zzkd.v("Pooled interstitial loaded.");
            }

            public void onAdOpened() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzalf != null) {
                            zzfj.zzalf.onAdOpened();
                        }
                    }
                });
            }
        });
        zzl.zza((zzw) new com.google.android.gms.ads.internal.client.zzw.zza() {
            public void onAppEvent(final String str, final String str2) throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkh != null) {
                            zzfj.zzbkh.onAppEvent(str, str2);
                        }
                    }
                });
            }
        });
        zzl.zza((zzho) new com.google.android.gms.internal.zzho.zza() {
            public void zza(final zzhn zzhn) throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbki != null) {
                            zzfj.zzbki.zza(zzhn);
                        }
                    }
                });
            }
        });
        zzl.zza((zzdo) new com.google.android.gms.internal.zzdo.zza() {
            public void zza(final zzdn zzdn) throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkj != null) {
                            zzfj.zzbkj.zza(zzdn);
                        }
                    }
                });
            }
        });
        zzl.zza((zzp) new com.google.android.gms.ads.internal.client.zzp.zza() {
            public void onAdClicked() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkk != null) {
                            zzfj.zzbkk.onAdClicked();
                        }
                    }
                });
            }
        });
        zzl.zza((zzd) new com.google.android.gms.ads.internal.reward.client.zzd.zza() {
            public void onRewardedVideoAdClosed() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.onRewardedVideoAdClosed();
                        }
                    }
                });
            }

            public void onRewardedVideoAdFailedToLoad(final int i) throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.onRewardedVideoAdFailedToLoad(i);
                        }
                    }
                });
            }

            public void onRewardedVideoAdLeftApplication() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.onRewardedVideoAdLeftApplication();
                        }
                    }
                });
            }

            public void onRewardedVideoAdLoaded() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.onRewardedVideoAdLoaded();
                        }
                    }
                });
            }

            public void onRewardedVideoAdOpened() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.onRewardedVideoAdOpened();
                        }
                    }
                });
            }

            public void onRewardedVideoStarted() throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.onRewardedVideoStarted();
                        }
                    }
                });
            }

            public void zza(final com.google.android.gms.ads.internal.reward.client.zza zza) throws RemoteException {
                zzfi.this.zzalc.add(new zza() {
                    public void zzb(zzfj zzfj) throws RemoteException {
                        if (zzfj.zzbkl != null) {
                            zzfj.zzbkl.zza(zza);
                        }
                    }
                });
            }
        });
    }
}
