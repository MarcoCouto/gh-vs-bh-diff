package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.common.util.zzs;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(14)
@zzin
public class zzco extends Thread {
    private boolean mStarted = false;
    private final Object zzail;
    private final int zzarv;
    private final int zzarx;
    private boolean zzasj = false;
    private final zzcn zzask;
    private final zzcm zzasl;
    private final zzim zzasm;
    private final int zzasn;
    private final int zzaso;
    private final int zzasp;
    private boolean zzbl = false;

    @zzin
    class zza {
        final int zzasx;
        final int zzasy;

        zza(int i, int i2) {
            this.zzasx = i;
            this.zzasy = i2;
        }
    }

    public zzco(zzcn zzcn, zzcm zzcm, zzim zzim) {
        this.zzask = zzcn;
        this.zzasl = zzcm;
        this.zzasm = zzim;
        this.zzail = new Object();
        this.zzarv = ((Integer) zzdc.zzazi.get()).intValue();
        this.zzaso = ((Integer) zzdc.zzazj.get()).intValue();
        this.zzarx = ((Integer) zzdc.zzazk.get()).intValue();
        this.zzasp = ((Integer) zzdc.zzazl.get()).intValue();
        this.zzasn = ((Integer) zzdc.zzazm.get()).intValue();
        setName("ContentFetchTask");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0039 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0039 A[LOOP:1: B:14:0x0039->B:26:0x0039, LOOP_START, SYNTHETIC] */
    public void run() {
        while (true) {
            try {
                if (zzia()) {
                    Activity activity = this.zzask.getActivity();
                    if (activity == null) {
                        zzkd.zzcv("ContentFetchThread: no activity. Sleeping.");
                    } else {
                        zza(activity);
                        Thread.sleep((long) (this.zzasn * 1000));
                        synchronized (this.zzail) {
                            while (this.zzasj) {
                                zzkd.zzcv("ContentFetchTask: waiting");
                                this.zzail.wait();
                            }
                        }
                    }
                } else {
                    zzkd.zzcv("ContentFetchTask: sleeping");
                }
                zzic();
                Thread.sleep((long) (this.zzasn * 1000));
            } catch (Throwable th) {
                zzkd.zzb("Error in ContentFetchTask", th);
                this.zzasm.zza(th, true);
            }
            synchronized (this.zzail) {
            }
        }
    }

    public void wakeup() {
        synchronized (this.zzail) {
            this.zzasj = false;
            this.zzail.notifyAll();
            zzkd.zzcv("ContentFetchThread: wakeup");
        }
    }

    /* access modifiers changed from: 0000 */
    public zza zza(@Nullable View view, zzcl zzcl) {
        if (view == null) {
            return new zza(0, 0);
        }
        boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
        if ((view instanceof TextView) && !(view instanceof EditText)) {
            CharSequence text = ((TextView) view).getText();
            if (TextUtils.isEmpty(text)) {
                return new zza(0, 0);
            }
            zzcl.zze(text.toString(), globalVisibleRect);
            return new zza(1, 0);
        } else if ((view instanceof WebView) && !(view instanceof zzlh)) {
            zzcl.zzhv();
            return zza((WebView) view, zzcl, globalVisibleRect) ? new zza(0, 1) : new zza(0, 0);
        } else if (!(view instanceof ViewGroup)) {
            return new zza(0, 0);
        } else {
            ViewGroup viewGroup = (ViewGroup) view;
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                zza zza2 = zza(viewGroup.getChildAt(i3), zzcl);
                i += zza2.zzasx;
                i2 += zza2.zzasy;
            }
            return new zza(i, i2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(@Nullable Activity activity) {
        if (activity != null) {
            View view = null;
            try {
                if (!(activity.getWindow() == null || activity.getWindow().getDecorView() == null)) {
                    view = activity.getWindow().getDecorView().findViewById(16908290);
                }
            } catch (Throwable unused) {
                zzkd.zzcv("Failed getting root view of activity. Content not extracted.");
            }
            if (view != null) {
                zze(view);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzcl zzcl, WebView webView, String str, boolean z) {
        zzcl.zzhu();
        try {
            if (!TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString("text");
                if (!TextUtils.isEmpty(webView.getTitle())) {
                    String valueOf = String.valueOf(webView.getTitle());
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(optString).length());
                    sb.append(valueOf);
                    sb.append("\n");
                    sb.append(optString);
                    zzcl.zzd(sb.toString(), z);
                } else {
                    zzcl.zzd(optString, z);
                }
            }
            if (zzcl.zzhq()) {
                this.zzasl.zzb(zzcl);
            }
        } catch (JSONException unused) {
            zzkd.zzcv("Json string may be malformed.");
        } catch (Throwable th) {
            zzkd.zza("Failed to get webview content.", th);
            this.zzasm.zza(th, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean zza(RunningAppProcessInfo runningAppProcessInfo) {
        return runningAppProcessInfo.importance == 100;
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(19)
    public boolean zza(final WebView webView, final zzcl zzcl, final boolean z) {
        if (!zzs.zzavu()) {
            return false;
        }
        zzcl.zzhv();
        webView.post(new Runnable() {
            ValueCallback<String> zzass = new ValueCallback<String>() {
                /* renamed from: zzz */
                public void onReceiveValue(String str) {
                    zzco.this.zza(zzcl, webView, str, z);
                }
            };

            public void run() {
                if (webView.getSettings().getJavaScriptEnabled()) {
                    try {
                        webView.evaluateJavascript("(function() { return  {text:document.body.innerText}})();", this.zzass);
                    } catch (Throwable unused) {
                        this.zzass.onReceiveValue("");
                    }
                }
            }
        });
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean zze(@Nullable final View view) {
        if (view == null) {
            return false;
        }
        view.post(new Runnable() {
            public void run() {
                zzco.this.zzf(view);
            }
        });
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void zzf(View view) {
        try {
            zzcl zzcl = new zzcl(this.zzarv, this.zzaso, this.zzarx, this.zzasp);
            zza zza2 = zza(view, zzcl);
            zzcl.zzhw();
            if (zza2.zzasx != 0 || zza2.zzasy != 0) {
                if (zza2.zzasy != 0 || zzcl.zzhx() != 0) {
                    if (zza2.zzasy != 0 || !this.zzasl.zza(zzcl)) {
                        this.zzasl.zzc(zzcl);
                    }
                }
            }
        } catch (Exception e) {
            zzkd.zzb("Exception in fetchContentOnUIThread", e);
            this.zzasm.zza(e, true);
        }
    }

    public void zzhz() {
        synchronized (this.zzail) {
            if (this.mStarted) {
                zzkd.zzcv("Content hash thread already started, quiting...");
                return;
            }
            this.mStarted = true;
            start();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean zzia() {
        boolean z = false;
        try {
            Context context = this.zzask.getContext();
            if (context == null) {
                return false;
            }
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
            if (activityManager != null) {
                if (keyguardManager != null) {
                    List runningAppProcesses = activityManager.getRunningAppProcesses();
                    if (runningAppProcesses != null) {
                        Iterator it = runningAppProcesses.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            RunningAppProcessInfo runningAppProcessInfo = (RunningAppProcessInfo) it.next();
                            if (Process.myPid() == runningAppProcessInfo.pid) {
                                if (zza(runningAppProcessInfo) && !keyguardManager.inKeyguardRestrictedInputMode() && zzj(context)) {
                                    z = true;
                                }
                            }
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return z;
        } catch (Throwable unused) {
        }
    }

    public zzcl zzib() {
        return this.zzasl.zzhy();
    }

    public void zzic() {
        synchronized (this.zzail) {
            this.zzasj = true;
            boolean z = this.zzasj;
            StringBuilder sb = new StringBuilder(42);
            sb.append("ContentFetchThread: paused, mPause = ");
            sb.append(z);
            zzkd.zzcv(sb.toString());
        }
    }

    public boolean zzid() {
        return this.zzasj;
    }

    /* access modifiers changed from: 0000 */
    public boolean zzj(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return false;
        }
        return powerManager.isScreenOn();
    }
}
