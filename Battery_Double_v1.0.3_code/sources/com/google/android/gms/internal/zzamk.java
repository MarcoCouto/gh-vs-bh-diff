package com.google.android.gms.internal;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

final class zzamk implements zzamu<Date>, zzand<Date> {
    private final DateFormat bdE;
    private final DateFormat bdF;
    private final DateFormat bdG;

    zzamk() {
        this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
    }

    public zzamk(int i, int i2) {
        this(DateFormat.getDateTimeInstance(i, i2, Locale.US), DateFormat.getDateTimeInstance(i, i2));
    }

    zzamk(String str) {
        this((DateFormat) new SimpleDateFormat(str, Locale.US), (DateFormat) new SimpleDateFormat(str));
    }

    zzamk(DateFormat dateFormat, DateFormat dateFormat2) {
        this.bdE = dateFormat;
        this.bdF = dateFormat2;
        this.bdG = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        this.bdG.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:13|14|15|16|17) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:8|9|10|11|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1 = r3.bdG.parse(r4.zzczf());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0028, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0029, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        throw new com.google.android.gms.internal.zzane(r4.zzczf(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r1 = r3.bdE.parse(r4.zzczf());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0011 */
    private Date zza(zzamv zzamv) {
        synchronized (this.bdF) {
            Date parse = this.bdF.parse(zzamv.zzczf());
            return parse;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(zzamk.class.getSimpleName());
        sb.append('(');
        sb.append(this.bdF.getClass().getSimpleName());
        sb.append(')');
        return sb.toString();
    }

    public zzamv zza(Date date, Type type, zzanc zzanc) {
        zzanb zzanb;
        synchronized (this.bdF) {
            zzanb = new zzanb(this.bdE.format(date));
        }
        return zzanb;
    }

    /* renamed from: zza */
    public Date zzb(zzamv zzamv, Type type, zzamt zzamt) throws zzamz {
        if (!(zzamv instanceof zzanb)) {
            throw new zzamz("The date should be a string value");
        }
        Date zza = zza(zzamv);
        if (type == Date.class) {
            return zza;
        }
        if (type == Timestamp.class) {
            return new Timestamp(zza.getTime());
        }
        if (type == java.sql.Date.class) {
            return new java.sql.Date(zza.getTime());
        }
        String valueOf = String.valueOf(getClass());
        String valueOf2 = String.valueOf(type);
        StringBuilder sb = new StringBuilder(23 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(" cannot deserialize to ");
        sb.append(valueOf2);
        throw new IllegalArgumentException(sb.toString());
    }
}
