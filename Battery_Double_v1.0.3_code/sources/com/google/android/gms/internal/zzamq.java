package com.google.android.gms.internal;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzamq {
    private final List<zzani> bdQ = new ArrayList();
    private zzanq bea = zzanq.beK;
    private zzanf beb = zzanf.DEFAULT;
    private zzamo bec = zzamn.IDENTITY;
    private final Map<Type, zzamr<?>> bed = new HashMap();
    private final List<zzani> bee = new ArrayList();
    private int bef = 2;
    private int beg = 2;
    private boolean beh = true;

    private void zza(String str, int i, int i2, List<zzani> list) {
        zzamk zzamk;
        if (str != null && !"".equals(str.trim())) {
            zzamk = new zzamk(str);
        } else if (i != 2 && i2 != 2) {
            zzamk = new zzamk(i, i2);
        } else {
            return;
        }
        list.add(zzang.zza(zzaol.zzr(Date.class), (Object) zzamk));
        list.add(zzang.zza(zzaol.zzr(Timestamp.class), (Object) zzamk));
        list.add(zzang.zza(zzaol.zzr(java.sql.Date.class), (Object) zzamk));
    }

    public zzamq zza(Type type, Object obj) {
        boolean z = obj instanceof zzand;
        zzann.zzbo(z || (obj instanceof zzamu) || (obj instanceof zzamr) || (obj instanceof zzanh));
        if (obj instanceof zzamr) {
            this.bed.put(type, (zzamr) obj);
        }
        if (z || (obj instanceof zzamu)) {
            this.bdQ.add(zzang.zzb(zzaol.zzl(type), obj));
        }
        if (obj instanceof zzanh) {
            this.bdQ.add(zzaok.zza(zzaol.zzl(type), (zzanh) obj));
        }
        return this;
    }

    public zzamq zza(zzaml... zzamlArr) {
        for (zzaml zza : zzamlArr) {
            this.bea = this.bea.zza(zza, true, true);
        }
        return this;
    }

    public zzamq zzczc() {
        this.beh = false;
        return this;
    }

    public zzamp zzczd() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.bdQ);
        Collections.reverse(arrayList);
        arrayList.addAll(this.bee);
        zza(null, this.bef, this.beg, arrayList);
        zzamp zzamp = new zzamp(this.bea, this.bec, this.bed, false, false, false, this.beh, false, false, this.beb, arrayList);
        return zzamp;
    }

    public zzamq zzf(int... iArr) {
        this.bea = this.bea.zzg(iArr);
        return this;
    }
}
