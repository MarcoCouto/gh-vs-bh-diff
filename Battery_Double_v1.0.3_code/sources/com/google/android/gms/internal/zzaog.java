package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class zzaog implements zzani {
    private final zzanp bdR;
    private final zzanq bea;
    private final zzamo bec;

    public static final class zza<T> extends zzanh<T> {
        private final Map<String, zzb> bfR;
        private final zzanu<T> bfy;

        private zza(zzanu<T> zzanu, Map<String, zzb> map) {
            this.bfy = zzanu;
            this.bfR = map;
        }

        public void zza(zzaoo zzaoo, T t) throws IOException {
            if (t == null) {
                zzaoo.l();
                return;
            }
            zzaoo.j();
            try {
                for (zzb zzb : this.bfR.values()) {
                    if (zzb.zzco(t)) {
                        zzaoo.zztr(zzb.name);
                        zzb.zza(zzaoo, (Object) t);
                    }
                }
                zzaoo.k();
            } catch (IllegalAccessException unused) {
                throw new AssertionError();
            }
        }

        public T zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            T zzczu = this.bfy.zzczu();
            try {
                zzaom.beginObject();
                while (zzaom.hasNext()) {
                    zzb zzb = (zzb) this.bfR.get(zzaom.nextName());
                    if (zzb != null) {
                        if (zzb.bfT) {
                            zzb.zza(zzaom, (Object) zzczu);
                        }
                    }
                    zzaom.skipValue();
                }
                zzaom.endObject();
                return zzczu;
            } catch (IllegalStateException e) {
                throw new zzane((Throwable) e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    static abstract class zzb {
        final boolean bfS;
        final boolean bfT;
        final String name;

        protected zzb(String str, boolean z, boolean z2) {
            this.name = str;
            this.bfS = z;
            this.bfT = z2;
        }

        /* access modifiers changed from: 0000 */
        public abstract void zza(zzaom zzaom, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: 0000 */
        public abstract void zza(zzaoo zzaoo, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: 0000 */
        public abstract boolean zzco(Object obj) throws IOException, IllegalAccessException;
    }

    public zzaog(zzanp zzanp, zzamo zzamo, zzanq zzanq) {
        this.bdR = zzanp;
        this.bec = zzamo;
        this.bea = zzanq;
    }

    /* access modifiers changed from: private */
    public zzanh<?> zza(zzamp zzamp, Field field, zzaol<?> zzaol) {
        zzanj zzanj = (zzanj) field.getAnnotation(zzanj.class);
        if (zzanj != null) {
            zzanh<?> zza2 = zzaob.zza(this.bdR, zzamp, zzaol, zzanj);
            if (zza2 != null) {
                return zza2;
            }
        }
        return zzamp.zza(zzaol);
    }

    private zzb zza(zzamp zzamp, Field field, String str, zzaol<?> zzaol, boolean z, boolean z2) {
        final boolean zzk = zzanv.zzk(zzaol.m());
        final zzamp zzamp2 = zzamp;
        final Field field2 = field;
        final zzaol<?> zzaol2 = zzaol;
        AnonymousClass1 r1 = new zzb(str, z, z2) {
            final zzanh<?> bfL = zzaog.this.zza(zzamp2, field2, zzaol2);

            /* access modifiers changed from: 0000 */
            public void zza(zzaom zzaom, Object obj) throws IOException, IllegalAccessException {
                Object zzb = this.bfL.zzb(zzaom);
                if (zzb != null || !zzk) {
                    field2.set(obj, zzb);
                }
            }

            /* access modifiers changed from: 0000 */
            public void zza(zzaoo zzaoo, Object obj) throws IOException, IllegalAccessException {
                new zzaoj(zzamp2, this.bfL, zzaol2.n()).zza(zzaoo, field2.get(obj));
            }

            public boolean zzco(Object obj) throws IOException, IllegalAccessException {
                boolean z = false;
                if (!this.bfS) {
                    return false;
                }
                if (field2.get(obj) != obj) {
                    z = true;
                }
                return z;
            }
        };
        return r1;
    }

    static List<String> zza(zzamo zzamo, Field field) {
        zzank zzank = (zzank) field.getAnnotation(zzank.class);
        LinkedList linkedList = new LinkedList();
        if (zzank == null) {
            linkedList.add(zzamo.zzc(field));
            return linkedList;
        }
        linkedList.add(zzank.value());
        for (String add : zzank.zzczs()) {
            linkedList.add(add);
        }
        return linkedList;
    }

    /* JADX WARNING: type inference failed for: r1v1, types: [boolean] */
    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r13v3 */
    /* JADX WARNING: type inference failed for: r3v1, types: [int] */
    /* JADX WARNING: type inference failed for: r1v2 */
    /* JADX WARNING: type inference failed for: r17v0 */
    /* JADX WARNING: type inference failed for: r20v0, types: [int] */
    /* JADX WARNING: type inference failed for: r1v9 */
    /* JADX WARNING: type inference failed for: r13v6 */
    /* JADX WARNING: type inference failed for: r17v1 */
    /* JADX WARNING: type inference failed for: r17v2 */
    /* JADX WARNING: type inference failed for: r13v8 */
    /* JADX WARNING: type inference failed for: r1v10 */
    /* JADX WARNING: type inference failed for: r3v5 */
    /* JADX WARNING: type inference failed for: r13v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 7 */
    private Map<String, zzb> zza(zzamp zzamp, zzaol<?> zzaol, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type n = zzaol.n();
        zzaol<?> zzaol2 = zzaol;
        Class<?> cls2 = cls;
        while (cls2 != Object.class) {
            Field[] declaredFields = cls2.getDeclaredFields();
            int length = declaredFields.length;
            int i = 0;
            boolean z = 0;
            while (i < length) {
                Field field = declaredFields[i];
                ? zza2 = zza(field, true);
                boolean zza3 = zza(field, z);
                if (zza2 != 0 || zza3) {
                    field.setAccessible(true);
                    Type zza4 = zzano.zza(zzaol2.n(), cls2, field.getGenericType());
                    List zzd = zzd(field);
                    zzb zzb2 = null;
                    ? r3 = z;
                    ? r13 = z;
                    ? r1 = zza2;
                    while (r3 < zzd.size()) {
                        String str = (String) zzd.get(r3);
                        ? r17 = r3 != 0 ? r13 : r1;
                        String str2 = str;
                        zzb zzb3 = zzb2;
                        ? r20 = r3;
                        List list = zzd;
                        Type type = zza4;
                        Field field2 = field;
                        zzb2 = zzb3 == null ? (zzb) linkedHashMap.put(str2, zza(zzamp, field, str2, zzaol.zzl(zza4), r17, zza3)) : zzb3;
                        r1 = r17;
                        zza4 = type;
                        zzd = list;
                        field = field2;
                        r3 = r20 + 1;
                        r13 = 0;
                    }
                    zzb zzb4 = zzb2;
                    if (zzb4 != null) {
                        String valueOf = String.valueOf(n);
                        String str3 = zzb4.name;
                        StringBuilder sb = new StringBuilder(37 + String.valueOf(valueOf).length() + String.valueOf(str3).length());
                        sb.append(valueOf);
                        sb.append(" declares multiple JSON fields named ");
                        sb.append(str3);
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                i++;
                z = 0;
            }
            zzaol2 = zzaol.zzl(zzano.zza(zzaol2.n(), cls2, cls2.getGenericSuperclass()));
            cls2 = zzaol2.m();
        }
        return linkedHashMap;
    }

    static boolean zza(Field field, boolean z, zzanq zzanq) {
        return !zzanq.zza(field.getType(), z) && !zzanq.zza(field, z);
    }

    private List<String> zzd(Field field) {
        return zza(this.bec, field);
    }

    public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
        Class m = zzaol.m();
        if (!Object.class.isAssignableFrom(m)) {
            return null;
        }
        return new zza(this.bdR.zzb(zzaol), zza(zzamp, zzaol, m));
    }

    public boolean zza(Field field, boolean z) {
        return zza(field, z, this.bea);
    }
}
