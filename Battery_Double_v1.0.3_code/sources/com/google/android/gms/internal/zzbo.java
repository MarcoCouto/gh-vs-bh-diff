package com.google.android.gms.internal;

import com.google.android.gms.internal.zzau.zza;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class zzbo {
    protected static final String TAG = "zzbo";
    private final String className;
    private final zzax zzaey;
    private final String zzahf;
    private final int zzahg = 2;
    private volatile Method zzahh = null;
    private List<Class> zzahi;
    private CountDownLatch zzahj = new CountDownLatch(1);

    public zzbo(zzax zzax, String str, String str2, List<Class> list) {
        this.zzaey = zzax;
        this.className = str;
        this.zzahf = str2;
        this.zzahi = new ArrayList(list);
        this.zzaey.zzcd().submit(new Runnable() {
            public void run() {
                zzbo.this.zzcy();
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0042, code lost:
        if (r4.zzahh == null) goto L_0x0018;
     */
    public void zzcy() {
        try {
            Class loadClass = this.zzaey.zzce().loadClass(zzd(this.zzaey.zzcg(), this.className));
            if (loadClass != null) {
                this.zzahh = loadClass.getMethod(zzd(this.zzaey.zzcg(), this.zzahf), (Class[]) this.zzahi.toArray(new Class[this.zzahi.size()]));
            }
            this.zzahj.countDown();
            return;
        } catch (zza | UnsupportedEncodingException | ClassNotFoundException | NoSuchMethodException | NullPointerException unused) {
        } catch (Throwable th) {
            this.zzahj.countDown();
            throw th;
        }
        this.zzahj.countDown();
    }

    private String zzd(byte[] bArr, String str) throws zza, UnsupportedEncodingException {
        return new String(this.zzaey.zzcf().zzc(bArr, str), "UTF-8");
    }

    public Method zzcz() {
        if (this.zzahh != null) {
            return this.zzahh;
        }
        try {
            if (!this.zzahj.await(2, TimeUnit.SECONDS)) {
                return null;
            }
            return this.zzahh;
        } catch (InterruptedException unused) {
            return null;
        }
    }
}
