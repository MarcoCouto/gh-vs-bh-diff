package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzhd.zza;
import java.util.Map;

@zzin
public class zzhe extends zzhf implements zzep {
    private final Context mContext;
    private final WindowManager zzaqm;
    private final zzlh zzbgf;
    private final zzcu zzbrc;
    DisplayMetrics zzbrd;
    private float zzbre;
    int zzbrf = -1;
    int zzbrg = -1;
    private int zzbrh;
    int zzbri = -1;
    int zzbrj = -1;
    int zzbrk = -1;
    int zzbrl = -1;

    public zzhe(zzlh zzlh, Context context, zzcu zzcu) {
        super(zzlh);
        this.zzbgf = zzlh;
        this.mContext = context;
        this.zzbrc = zzcu;
        this.zzaqm = (WindowManager) context.getSystemService("window");
    }

    private void zzmz() {
        this.zzbrd = new DisplayMetrics();
        Display defaultDisplay = this.zzaqm.getDefaultDisplay();
        defaultDisplay.getMetrics(this.zzbrd);
        this.zzbre = this.zzbrd.density;
        this.zzbrh = defaultDisplay.getRotation();
    }

    private void zzne() {
        int[] iArr = new int[2];
        this.zzbgf.getLocationOnScreen(iArr);
        zze(zzm.zziw().zzb(this.mContext, iArr[0]), zzm.zziw().zzb(this.mContext, iArr[1]));
    }

    private zzhd zznh() {
        return new zza().zzu(this.zzbrc.zzjp()).zzt(this.zzbrc.zzjq()).zzv(this.zzbrc.zzju()).zzw(this.zzbrc.zzjr()).zzx(this.zzbrc.zzjs()).zzmy();
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        zznc();
    }

    public void zze(int i, int i2) {
        int i3 = 0;
        if (this.mContext instanceof Activity) {
            i3 = zzu.zzfq().zzk((Activity) this.mContext)[0];
        }
        zzc(i, i2 - i3, this.zzbrk, this.zzbrl);
        this.zzbgf.zzuj().zzd(i, i2);
    }

    /* access modifiers changed from: 0000 */
    public void zzna() {
        int i;
        this.zzbrf = zzm.zziw().zzb(this.zzbrd, this.zzbrd.widthPixels);
        this.zzbrg = zzm.zziw().zzb(this.zzbrd, this.zzbrd.heightPixels);
        Activity zzue = this.zzbgf.zzue();
        if (zzue == null || zzue.getWindow() == null) {
            this.zzbri = this.zzbrf;
            i = this.zzbrg;
        } else {
            int[] zzh = zzu.zzfq().zzh(zzue);
            this.zzbri = zzm.zziw().zzb(this.zzbrd, zzh[0]);
            i = zzm.zziw().zzb(this.zzbrd, zzh[1]);
        }
        this.zzbrj = i;
    }

    /* access modifiers changed from: 0000 */
    public void zznb() {
        int zzb;
        if (this.zzbgf.zzdn().zzaus) {
            this.zzbrk = this.zzbrf;
            zzb = this.zzbrg;
        } else {
            this.zzbgf.measure(0, 0);
            this.zzbrk = zzm.zziw().zzb(this.mContext, this.zzbgf.getMeasuredWidth());
            zzb = zzm.zziw().zzb(this.mContext, this.zzbgf.getMeasuredHeight());
        }
        this.zzbrl = zzb;
    }

    public void zznc() {
        zzmz();
        zzna();
        zznb();
        zznf();
        zzng();
        zzne();
        zznd();
    }

    /* access modifiers changed from: 0000 */
    public void zznd() {
        if (zzkd.zzaz(2)) {
            zzkd.zzcw("Dispatching Ready Event.");
        }
        zzbu(this.zzbgf.zzum().zzcs);
    }

    /* access modifiers changed from: 0000 */
    public void zznf() {
        zza(this.zzbrf, this.zzbrg, this.zzbri, this.zzbrj, this.zzbre, this.zzbrh);
    }

    /* access modifiers changed from: 0000 */
    public void zzng() {
        this.zzbgf.zzb("onDeviceFeaturesReceived", zznh().toJson());
    }
}
