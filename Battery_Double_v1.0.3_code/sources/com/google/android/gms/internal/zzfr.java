package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzu;
import org.json.JSONObject;

@zzin
public class zzfr implements zzfp {
    /* access modifiers changed from: private */
    public final zzlh zzbgf;

    public zzfr(Context context, VersionInfoParcel versionInfoParcel, @Nullable zzas zzas) {
        this.zzbgf = zzu.zzfr().zza(context, new AdSizeParcel(), false, false, zzas, versionInfoParcel);
        this.zzbgf.getWebView().setWillNotDraw(true);
    }

    private void runOnUiThread(Runnable runnable) {
        if (zzm.zziw().zztx()) {
            runnable.run();
        } else {
            zzkh.zzclc.post(runnable);
        }
    }

    public void destroy() {
        this.zzbgf.destroy();
    }

    public void zza(zza zza, zzg zzg, zzel zzel, zzp zzp, boolean z, zzer zzer, zzet zzet, zze zze, zzhg zzhg) {
        this.zzbgf.zzuj().zza(zza, zzg, zzel, zzp, z, zzer, zzet, new zze(this.zzbgf.getContext(), false), zzhg, null);
    }

    public void zza(final zzfp.zza zza) {
        this.zzbgf.zzuj().zza((zzli.zza) new zzli.zza() {
            public void zza(zzlh zzlh, boolean z) {
                zza.zzlz();
            }
        });
    }

    public void zza(String str, zzep zzep) {
        this.zzbgf.zzuj().zza(str, zzep);
    }

    public void zza(final String str, final JSONObject jSONObject) {
        runOnUiThread(new Runnable() {
            public void run() {
                zzfr.this.zzbgf.zza(str, jSONObject);
            }
        });
    }

    public void zzb(String str, zzep zzep) {
        this.zzbgf.zzuj().zzb(str, zzep);
    }

    public void zzb(String str, JSONObject jSONObject) {
        this.zzbgf.zzb(str, jSONObject);
    }

    public void zzbg(String str) {
        final String format = String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", new Object[]{str});
        runOnUiThread(new Runnable() {
            public void run() {
                zzfr.this.zzbgf.loadData(format, "text/html", "UTF-8");
            }
        });
    }

    public void zzbh(final String str) {
        runOnUiThread(new Runnable() {
            public void run() {
                zzfr.this.zzbgf.loadUrl(str);
            }
        });
    }

    public void zzbi(final String str) {
        runOnUiThread(new Runnable() {
            public void run() {
                zzfr.this.zzbgf.loadData(str, "text/html", "UTF-8");
            }
        });
    }

    public void zzj(final String str, final String str2) {
        runOnUiThread(new Runnable() {
            public void run() {
                zzfr.this.zzbgf.zzj(str, str2);
            }
        });
    }

    public zzfu zzly() {
        return new zzfv(this);
    }
}
