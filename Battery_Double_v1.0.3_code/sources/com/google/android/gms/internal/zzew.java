package com.google.android.gms.internal;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.AdLauncherIntentInfoParcel;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzu;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@zzin
public final class zzew implements zzep {
    private final zze zzbit;
    private final zzha zzbiu;
    private final zzer zzbiw;

    public static class zza {
        private final zzlh zzbgf;

        public zza(zzlh zzlh) {
            this.zzbgf = zzlh;
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0083  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0088  */
        public Intent zza(Context context, Map<String, String> map) {
            ResolveInfo zza;
            Builder buildUpon;
            String str;
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            String str2 = (String) map.get("u");
            Uri uri = null;
            if (TextUtils.isEmpty(str2)) {
                return null;
            }
            if (this.zzbgf != null) {
                str2 = zzu.zzfq().zza(this.zzbgf, str2);
            }
            Uri parse = Uri.parse(str2);
            boolean parseBoolean = Boolean.parseBoolean((String) map.get("use_first_package"));
            boolean parseBoolean2 = Boolean.parseBoolean((String) map.get("use_running_process"));
            if ("http".equalsIgnoreCase(parse.getScheme())) {
                buildUpon = parse.buildUpon();
                str = "https";
            } else {
                if ("https".equalsIgnoreCase(parse.getScheme())) {
                    buildUpon = parse.buildUpon();
                    str = "http";
                }
                ArrayList arrayList = new ArrayList();
                Intent zze = zze(parse);
                Intent zze2 = zze(uri);
                zza = zza(context, zze, arrayList);
                if (zza == null) {
                    return zza(zze, zza);
                }
                if (zze2 != null) {
                    ResolveInfo zza2 = zza(context, zze2);
                    if (zza2 != null) {
                        Intent zza3 = zza(zze, zza2);
                        if (zza(context, zza3) != null) {
                            return zza3;
                        }
                    }
                }
                if (arrayList.size() == 0) {
                    return zze;
                }
                if (parseBoolean2 && activityManager != null) {
                    List runningAppProcesses = activityManager.getRunningAppProcesses();
                    if (runningAppProcesses != null) {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            ResolveInfo resolveInfo = (ResolveInfo) it.next();
                            Iterator it2 = runningAppProcesses.iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (((RunningAppProcessInfo) it2.next()).processName.equals(resolveInfo.activityInfo.packageName)) {
                                        return zza(zze, resolveInfo);
                                    }
                                }
                            }
                        }
                    }
                }
                return parseBoolean ? zza(zze, (ResolveInfo) arrayList.get(0)) : zze;
            }
            uri = buildUpon.scheme(str).build();
            ArrayList arrayList2 = new ArrayList();
            Intent zze3 = zze(parse);
            Intent zze22 = zze(uri);
            zza = zza(context, zze3, arrayList2);
            if (zza == null) {
            }
        }

        public Intent zza(Intent intent, ResolveInfo resolveInfo) {
            Intent intent2 = new Intent(intent);
            intent2.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
            return intent2;
        }

        public ResolveInfo zza(Context context, Intent intent) {
            return zza(context, intent, new ArrayList());
        }

        public ResolveInfo zza(Context context, Intent intent, ArrayList<ResolveInfo> arrayList) {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return null;
            }
            List queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 65536);
            if (queryIntentActivities != null && resolveActivity != null) {
                int i = 0;
                while (true) {
                    if (i >= queryIntentActivities.size()) {
                        break;
                    }
                    ResolveInfo resolveInfo = (ResolveInfo) queryIntentActivities.get(i);
                    if (resolveActivity != null && resolveActivity.activityInfo.name.equals(resolveInfo.activityInfo.name)) {
                        break;
                    }
                    i++;
                }
            }
            resolveActivity = null;
            arrayList.addAll(queryIntentActivities);
            return resolveActivity;
        }

        public Intent zze(Uri uri) {
            if (uri == null) {
                return null;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setData(uri);
            intent.setAction("android.intent.action.VIEW");
            return intent;
        }
    }

    public zzew(zzer zzer, zze zze, zzha zzha) {
        this.zzbiw = zzer;
        this.zzbit = zze;
        this.zzbiu = zzha;
    }

    private static boolean zzc(Map<String, String> map) {
        return "1".equals(map.get("custom_close"));
    }

    private static int zzd(Map<String, String> map) {
        String str = (String) map.get("o");
        if (str != null) {
            if ("p".equalsIgnoreCase(str)) {
                return zzu.zzfs().zztk();
            }
            if ("l".equalsIgnoreCase(str)) {
                return zzu.zzfs().zztj();
            }
            if ("c".equalsIgnoreCase(str)) {
                return zzu.zzfs().zztl();
            }
        }
        return -1;
    }

    private static void zze(zzlh zzlh, Map<String, String> map) {
        Context context = zzlh.getContext();
        if (TextUtils.isEmpty((String) map.get("u"))) {
            zzkd.zzcx("Destination url cannot be empty.");
            return;
        }
        try {
            zzlh.zzuj().zza(new AdLauncherIntentInfoParcel(new zza(zzlh).zza(context, map)));
        } catch (ActivityNotFoundException e) {
            zzkd.zzcx(e.getMessage());
        }
    }

    private void zzr(boolean z) {
        if (this.zzbiu != null) {
            this.zzbiu.zzs(z);
        }
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        String str = (String) map.get("a");
        if (str == null) {
            zzkd.zzcx("Action missing from an open GMSG.");
        } else if (this.zzbit == null || this.zzbit.zzel()) {
            zzli zzuj = zzlh.zzuj();
            if ("expand".equalsIgnoreCase(str)) {
                if (zzlh.zzun()) {
                    zzkd.zzcx("Cannot expand WebView that is already expanded.");
                    return;
                }
                zzr(false);
                zzuj.zza(zzc(map), zzd(map));
            } else if ("webapp".equalsIgnoreCase(str)) {
                String str2 = (String) map.get("u");
                zzr(false);
                if (str2 != null) {
                    zzuj.zza(zzc(map), zzd(map), str2);
                } else {
                    zzuj.zza(zzc(map), zzd(map), (String) map.get("html"), (String) map.get("baseurl"));
                }
            } else if ("in_app_purchase".equalsIgnoreCase(str)) {
                String str3 = (String) map.get(Param.PRODUCT_ID);
                String str4 = (String) map.get("report_urls");
                if (this.zzbiw != null) {
                    if (str4 == null || str4.isEmpty()) {
                        this.zzbiw.zza(str3, new ArrayList());
                        return;
                    }
                    this.zzbiw.zza(str3, new ArrayList(Arrays.asList(str4.split(" "))));
                }
            } else if (!"app".equalsIgnoreCase(str) || !"true".equalsIgnoreCase((String) map.get("system_browser"))) {
                zzr(true);
                String str5 = (String) map.get("u");
                if (!TextUtils.isEmpty(str5)) {
                    str5 = zzu.zzfq().zza(zzlh, str5);
                }
                AdLauncherIntentInfoParcel adLauncherIntentInfoParcel = new AdLauncherIntentInfoParcel((String) map.get("i"), str5, (String) map.get("m"), (String) map.get("p"), (String) map.get("c"), (String) map.get("f"), (String) map.get("e"));
                zzuj.zza(adLauncherIntentInfoParcel);
            } else {
                zzr(true);
                zze(zzlh, map);
            }
        } else {
            this.zzbit.zzt((String) map.get("u"));
        }
    }
}
