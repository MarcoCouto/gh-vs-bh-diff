package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzu;
import java.util.Map;

@zzin
class zzfa implements zzep {
    zzfa() {
    }

    private int zzg(Map<String, String> map) throws NullPointerException, NumberFormatException {
        int parseInt = Integer.parseInt((String) map.get("playbackState"));
        if (parseInt < 0 || 3 < parseInt) {
            return 0;
        }
        return parseInt;
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        if (((Boolean) zzdc.zzbbb.get()).booleanValue()) {
            zzlm zzut = zzlh.zzut();
            if (zzut == null) {
                try {
                    zzlm zzlm = new zzlm(zzlh, Float.parseFloat((String) map.get("duration")));
                    zzlh.zza(zzlm);
                    zzut = zzlm;
                } catch (NullPointerException | NumberFormatException e) {
                    zzkd.zzb("Unable to parse videoMeta message.", e);
                    zzu.zzft().zzb(e, true);
                    return;
                }
            }
            zzut.zza(Float.parseFloat((String) map.get("currentTime")), zzg(map), "1".equals(map.get("muted")));
        }
    }
}
