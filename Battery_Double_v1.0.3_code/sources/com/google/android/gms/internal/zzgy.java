package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsIntent.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.AdLauncherIntentInfoParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.internal.zzdq.zza;

@zzin
public class zzgy implements MediationInterstitialAdapter {
    private Uri mUri;
    /* access modifiers changed from: private */
    public Activity zzbpu;
    /* access modifiers changed from: private */
    public zzdq zzbpv;
    /* access modifiers changed from: private */
    public MediationInterstitialListener zzbpw;

    public static boolean zzp(Context context) {
        return zzdq.zzo(context);
    }

    public void onDestroy() {
        zzb.zzcv("Destroying AdMobCustomTabsAdapter adapter.");
        try {
            this.zzbpv.zzd(this.zzbpu);
        } catch (Exception e) {
            zzb.zzb("Exception while unbinding from CustomTabsService.", e);
        }
    }

    public void onPause() {
        zzb.zzcv("Pausing AdMobCustomTabsAdapter adapter.");
    }

    public void onResume() {
        zzb.zzcv("Resuming AdMobCustomTabsAdapter adapter.");
    }

    public void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.zzbpw = mediationInterstitialListener;
        if (this.zzbpw == null) {
            zzb.zzcx("Listener not set for mediation. Returning.");
        } else if (!(context instanceof Activity)) {
            zzb.zzcx("AdMobCustomTabs can only work with Activity context. Bailing out.");
            this.zzbpw.onAdFailedToLoad(this, 0);
        } else if (!zzp(context)) {
            zzb.zzcx("Default browser does not support custom tabs. Bailing out.");
            this.zzbpw.onAdFailedToLoad(this, 0);
        } else {
            String string = bundle.getString("tab_url");
            if (TextUtils.isEmpty(string)) {
                zzb.zzcx("The tab_url retrieved from mediation metadata is empty. Bailing out.");
                this.zzbpw.onAdFailedToLoad(this, 0);
                return;
            }
            this.zzbpu = (Activity) context;
            this.mUri = Uri.parse(string);
            this.zzbpv = new zzdq();
            this.zzbpv.zza((zza) new zza() {
                public void zzkn() {
                    zzb.zzcv("Hinting CustomTabsService for the load of the new url.");
                }

                public void zzko() {
                    zzb.zzcv("Disconnecting from CustomTabs service.");
                }
            });
            this.zzbpv.zze(this.zzbpu);
            this.zzbpw.onAdLoaded(this);
        }
    }

    public void showInterstitial() {
        CustomTabsIntent build = new Builder(this.zzbpv.zzkl()).build();
        build.intent.setData(this.mUri);
        final AdOverlayInfoParcel adOverlayInfoParcel = new AdOverlayInfoParcel(new AdLauncherIntentInfoParcel(build.intent), null, new zzg() {
            public void onPause() {
                zzb.zzcv("AdMobCustomTabsAdapter overlay is paused.");
            }

            public void onResume() {
                zzb.zzcv("AdMobCustomTabsAdapter overlay is resumed.");
            }

            public void zzdx() {
                zzb.zzcv("AdMobCustomTabsAdapter overlay is closed.");
                zzgy.this.zzbpw.onAdClosed(zzgy.this);
                zzgy.this.zzbpv.zzd(zzgy.this.zzbpu);
            }

            public void zzdy() {
                zzb.zzcv("Opening AdMobCustomTabsAdapter overlay.");
                zzgy.this.zzbpw.onAdOpened(zzgy.this);
            }
        }, null, new VersionInfoParcel(0, 0, false));
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzu.zzfo().zza(zzgy.this.zzbpu, adOverlayInfoParcel);
            }
        });
        zzu.zzft().zzaf(false);
    }
}
