package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzab;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class zzpk extends zzpn {
    private final SparseArray<zza> sC = new SparseArray<>();

    private class zza implements OnConnectionFailedListener {
        public final int sD;
        public final GoogleApiClient sE;
        public final OnConnectionFailedListener sF;

        public zza(int i, GoogleApiClient googleApiClient, OnConnectionFailedListener onConnectionFailedListener) {
            this.sD = i;
            this.sE = googleApiClient;
            this.sF = onConnectionFailedListener;
            googleApiClient.registerConnectionFailedListener(this);
        }

        public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.append(str).append("GoogleApiClient #").print(this.sD);
            printWriter.println(":");
            this.sE.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        }

        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(27 + String.valueOf(valueOf).length());
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            zzpk.this.zzb(connectionResult, this.sD);
        }

        public void zzaop() {
            this.sE.unregisterConnectionFailedListener(this);
            this.sE.disconnect();
        }
    }

    private zzpk(zzqk zzqk) {
        super(zzqk);
        this.vm.zza("AutoManageHelper", (zzqj) this);
    }

    public static zzpk zza(zzqi zzqi) {
        zzqk zzc = zzc(zzqi);
        zzpk zzpk = (zzpk) zzc.zza("AutoManageHelper", zzpk.class);
        return zzpk != null ? zzpk : new zzpk(zzc);
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.sC.size(); i++) {
            ((zza) this.sC.valueAt(i)).dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onStart() {
        super.onStart();
        boolean z = this.mStarted;
        String valueOf = String.valueOf(this.sC);
        StringBuilder sb = new StringBuilder(14 + String.valueOf(valueOf).length());
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (!this.sL) {
            for (int i = 0; i < this.sC.size(); i++) {
                ((zza) this.sC.valueAt(i)).sE.connect();
            }
        }
    }

    public void onStop() {
        super.onStop();
        for (int i = 0; i < this.sC.size(); i++) {
            ((zza) this.sC.valueAt(i)).sE.disconnect();
        }
    }

    public void zza(int i, GoogleApiClient googleApiClient, OnConnectionFailedListener onConnectionFailedListener) {
        zzab.zzb(googleApiClient, (Object) "GoogleApiClient instance cannot be null");
        boolean z = this.sC.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        zzab.zza(z, (Object) sb.toString());
        boolean z2 = this.mStarted;
        boolean z3 = this.sL;
        StringBuilder sb2 = new StringBuilder(54);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(z3);
        Log.d("AutoManageHelper", sb2.toString());
        this.sC.put(i, new zza(i, googleApiClient, onConnectionFailedListener));
        if (this.mStarted && !this.sL) {
            String valueOf = String.valueOf(googleApiClient);
            StringBuilder sb3 = new StringBuilder(11 + String.valueOf(valueOf).length());
            sb3.append("connecting ");
            sb3.append(valueOf);
            Log.d("AutoManageHelper", sb3.toString());
            googleApiClient.connect();
        }
    }

    /* access modifiers changed from: protected */
    public void zza(ConnectionResult connectionResult, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        zza zza2 = (zza) this.sC.get(i);
        if (zza2 != null) {
            zzfh(i);
            OnConnectionFailedListener onConnectionFailedListener = zza2.sF;
            if (onConnectionFailedListener != null) {
                onConnectionFailedListener.onConnectionFailed(connectionResult);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzaoo() {
        for (int i = 0; i < this.sC.size(); i++) {
            ((zza) this.sC.valueAt(i)).sE.connect();
        }
    }

    public void zzfh(int i) {
        zza zza2 = (zza) this.sC.get(i);
        this.sC.remove(i);
        if (zza2 != null) {
            zza2.zzaop();
        }
    }
}
