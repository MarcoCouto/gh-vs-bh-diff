package com.google.android.gms.internal;

import java.io.IOException;

final class zzang<T> extends zzanh<T> {
    private zzanh<T> bdZ;
    private final zzand<T> beo;
    private final zzamu<T> bep;
    private final zzamp beq;
    private final zzaol<T> ber;
    private final zzani bes;

    private static class zza implements zzani {
        private final zzand<?> beo;
        private final zzamu<?> bep;
        private final zzaol<?> bet;
        private final boolean beu;
        private final Class<?> bev;

        private zza(Object obj, zzaol<?> zzaol, boolean z, Class<?> cls) {
            zzamu<?> zzamu = null;
            this.beo = obj instanceof zzand ? (zzand) obj : null;
            if (obj instanceof zzamu) {
                zzamu = (zzamu) obj;
            }
            this.bep = zzamu;
            zzann.zzbo((this.beo == null && this.bep == null) ? false : true);
            this.bet = zzaol;
            this.beu = z;
            this.bev = cls;
        }

        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            boolean z = this.bet != null ? this.bet.equals(zzaol) || (this.beu && this.bet.n() == zzaol.m()) : this.bev.isAssignableFrom(zzaol.m());
            if (!z) {
                return null;
            }
            zzang zzang = new zzang(this.beo, this.bep, zzamp, zzaol, this);
            return zzang;
        }
    }

    private zzang(zzand<T> zzand, zzamu<T> zzamu, zzamp zzamp, zzaol<T> zzaol, zzani zzani) {
        this.beo = zzand;
        this.bep = zzamu;
        this.beq = zzamp;
        this.ber = zzaol;
        this.bes = zzani;
    }

    public static zzani zza(zzaol<?> zzaol, Object obj) {
        zza zza2 = new zza(obj, zzaol, false, null);
        return zza2;
    }

    public static zzani zzb(zzaol<?> zzaol, Object obj) {
        zza zza2 = new zza(obj, zzaol, zzaol.n() == zzaol.m(), null);
        return zza2;
    }

    private zzanh<T> zzczr() {
        zzanh<T> zzanh = this.bdZ;
        if (zzanh != null) {
            return zzanh;
        }
        zzanh<T> zza2 = this.beq.zza(this.bes, this.ber);
        this.bdZ = zza2;
        return zza2;
    }

    public void zza(zzaoo zzaoo, T t) throws IOException {
        if (this.beo == null) {
            zzczr().zza(zzaoo, t);
        } else if (t == null) {
            zzaoo.l();
        } else {
            zzanw.zzb(this.beo.zza(t, this.ber.n(), this.beq.bdX), zzaoo);
        }
    }

    public T zzb(zzaom zzaom) throws IOException {
        if (this.bep == null) {
            return zzczr().zzb(zzaom);
        }
        zzamv zzh = zzanw.zzh(zzaom);
        if (zzh.zzczj()) {
            return null;
        }
        try {
            return this.bep.zzb(zzh, this.ber.n(), this.beq.bdW);
        } catch (zzamz e) {
            throw e;
        } catch (Exception e2) {
            throw new zzamz((Throwable) e2);
        }
    }
}
