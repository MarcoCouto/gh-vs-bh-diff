package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzs;
import com.google.android.gms.ads.internal.zzu;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
class zzll extends WebView implements OnGlobalLayoutListener, DownloadListener, zzlh {
    private final Object zzail = new Object();
    private final zzd zzajv;
    private final VersionInfoParcel zzalo;
    private AdSizeParcel zzani;
    private zzku zzaqg;
    private final WindowManager zzaqm;
    @Nullable
    private final zzas zzbgd;
    private int zzbrf = -1;
    private int zzbrg = -1;
    private int zzbri = -1;
    private int zzbrj = -1;
    private String zzbvq = "";
    private Boolean zzcjw;
    private final zza zzcpc;
    private final zzs zzcpd;
    private zzli zzcpe;
    private com.google.android.gms.ads.internal.overlay.zzd zzcpf;
    private boolean zzcpg;
    private boolean zzcph;
    private boolean zzcpi;
    private boolean zzcpj;
    private int zzcpk;
    private boolean zzcpl = true;
    boolean zzcpm = false;
    private zzlm zzcpn;
    private boolean zzcpo;
    private zzdi zzcpp;
    private zzdi zzcpq;
    private zzdi zzcpr;
    private zzdj zzcps;
    private WeakReference<OnClickListener> zzcpt;
    private com.google.android.gms.ads.internal.overlay.zzd zzcpu;
    private Map<String, zzfd> zzcpv;

    @zzin
    public static class zza extends MutableContextWrapper {
        private Context zzaql;
        private Activity zzcmv;
        private Context zzcpx;

        public zza(Context context) {
            super(context);
            setBaseContext(context);
        }

        public Object getSystemService(String str) {
            return this.zzcpx.getSystemService(str);
        }

        public void setBaseContext(Context context) {
            this.zzaql = context.getApplicationContext();
            this.zzcmv = context instanceof Activity ? (Activity) context : null;
            this.zzcpx = context;
            super.setBaseContext(this.zzaql);
        }

        public void startActivity(Intent intent) {
            if (this.zzcmv != null) {
                this.zzcmv.startActivity(intent);
                return;
            }
            intent.setFlags(268435456);
            this.zzaql.startActivity(intent);
        }

        public Activity zzue() {
            return this.zzcmv;
        }

        public Context zzuf() {
            return this.zzcpx;
        }
    }

    protected zzll(zza zza2, AdSizeParcel adSizeParcel, boolean z, boolean z2, @Nullable zzas zzas, VersionInfoParcel versionInfoParcel, zzdk zzdk, zzs zzs, zzd zzd) {
        super(zza2);
        this.zzcpc = zza2;
        this.zzani = adSizeParcel;
        this.zzcpi = z;
        this.zzcpk = -1;
        this.zzbgd = zzas;
        this.zzalo = versionInfoParcel;
        this.zzcpd = zzs;
        this.zzajv = zzd;
        this.zzaqm = (WindowManager) getContext().getSystemService("window");
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        settings.setJavaScriptEnabled(true);
        settings.setSavePassword(false);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(2);
        }
        zzu.zzfq().zza((Context) zza2, versionInfoParcel.zzcs, settings);
        zzu.zzfs().zza(getContext(), settings);
        setDownloadListener(this);
        zzvj();
        if (com.google.android.gms.common.util.zzs.zzavs()) {
            addJavascriptInterface(new zzln(this), "googleAdsJsInterface");
        }
        if (com.google.android.gms.common.util.zzs.zzavn()) {
            removeJavascriptInterface("accessibility");
            removeJavascriptInterface("accessibilityTraversal");
        }
        this.zzaqg = new zzku(this.zzcpc.zzue(), this, this, null);
        zzd(zzdk);
    }

    private void zzal(boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", z ? "1" : "0");
        zza("onAdVisibilityChanged", (Map<String, ?>) hashMap);
    }

    static zzll zzb(Context context, AdSizeParcel adSizeParcel, boolean z, boolean z2, @Nullable zzas zzas, VersionInfoParcel versionInfoParcel, zzdk zzdk, zzs zzs, zzd zzd) {
        zzll zzll = new zzll(new zza(context), adSizeParcel, z, z2, zzas, versionInfoParcel, zzdk, zzs, zzd);
        return zzll;
    }

    private void zzd(zzdk zzdk) {
        zzvn();
        this.zzcps = new zzdj(new zzdk(true, "make_wv", this.zzani.zzaur));
        this.zzcps.zzkf().zzc(zzdk);
        this.zzcpq = zzdg.zzb(this.zzcps.zzkf());
        this.zzcps.zza("native:view_create", this.zzcpq);
        this.zzcpr = null;
        this.zzcpp = null;
    }

    private void zzvh() {
        synchronized (this.zzail) {
            this.zzcjw = zzu.zzft().zzsq();
            if (this.zzcjw == null) {
                try {
                    evaluateJavascript("(function(){})()", null);
                    zzb(Boolean.valueOf(true));
                } catch (IllegalStateException unused) {
                    zzb(Boolean.valueOf(false));
                }
            }
        }
    }

    private void zzvi() {
        zzdg.zza(this.zzcps.zzkf(), this.zzcpq, "aeh2");
    }

    private void zzvj() {
        synchronized (this.zzail) {
            if (!this.zzcpi) {
                if (!this.zzani.zzaus) {
                    if (VERSION.SDK_INT < 18) {
                        zzkd.zzcv("Disabling hardware acceleration on an AdView.");
                        zzvk();
                    } else {
                        zzkd.zzcv("Enabling hardware acceleration on an AdView.");
                        zzvl();
                    }
                }
            }
            if (VERSION.SDK_INT < 14) {
                zzkd.zzcv("Disabling hardware acceleration on an overlay.");
                zzvk();
            } else {
                zzkd.zzcv("Enabling hardware acceleration on an overlay.");
                zzvl();
            }
        }
    }

    private void zzvk() {
        synchronized (this.zzail) {
            if (!this.zzcpj) {
                zzu.zzfs().zzp(this);
            }
            this.zzcpj = true;
        }
    }

    private void zzvl() {
        synchronized (this.zzail) {
            if (this.zzcpj) {
                zzu.zzfs().zzo(this);
            }
            this.zzcpj = false;
        }
    }

    private void zzvm() {
        synchronized (this.zzail) {
            this.zzcpv = null;
        }
    }

    private void zzvn() {
        if (this.zzcps != null) {
            zzdk zzkf = this.zzcps.zzkf();
            if (!(zzkf == null || zzu.zzft().zzsl() == null)) {
                zzu.zzft().zzsl().zza(zzkf);
            }
        }
    }

    public void destroy() {
        synchronized (this.zzail) {
            zzvn();
            this.zzaqg.zztt();
            if (this.zzcpf != null) {
                this.zzcpf.close();
                this.zzcpf.onDestroy();
                this.zzcpf = null;
            }
            this.zzcpe.reset();
            if (!this.zzcph) {
                zzu.zzgj().zzd(this);
                zzvm();
                this.zzcph = true;
                zzkd.v("Initiating WebView self destruct sequence in 3...");
                this.zzcpe.zzuz();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return;
     */
    @TargetApi(19)
    public void evaluateJavascript(String str, ValueCallback<String> valueCallback) {
        synchronized (this.zzail) {
            if (isDestroyed()) {
                zzkd.zzcx("The webview is destroyed. Ignoring action.");
                if (valueCallback != null) {
                    valueCallback.onReceiveValue(null);
                }
            } else {
                super.evaluateJavascript(str, valueCallback);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        synchronized (this.zzail) {
            if (!this.zzcph) {
                this.zzcpe.reset();
                zzu.zzgj().zzd(this);
                zzvm();
            }
        }
        super.finalize();
    }

    public String getRequestId() {
        String str;
        synchronized (this.zzail) {
            str = this.zzbvq;
        }
        return str;
    }

    public int getRequestedOrientation() {
        int i;
        synchronized (this.zzail) {
            i = this.zzcpk;
        }
        return i;
    }

    public View getView() {
        return this;
    }

    public WebView getWebView() {
        return this;
    }

    public boolean isDestroyed() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzcph;
        }
        return z;
    }

    public void loadData(String str, String str2, String str3) {
        synchronized (this.zzail) {
            if (!isDestroyed()) {
                super.loadData(str, str2, str3);
            } else {
                zzkd.zzcx("The webview is destroyed. Ignoring action.");
            }
        }
    }

    public void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        synchronized (this.zzail) {
            if (!isDestroyed()) {
                super.loadDataWithBaseURL(str, str2, str3, str4, str5);
            } else {
                zzkd.zzcx("The webview is destroyed. Ignoring action.");
            }
        }
    }

    public void loadUrl(String str) {
        String str2;
        synchronized (this.zzail) {
            if (!isDestroyed()) {
                try {
                    super.loadUrl(str);
                } catch (Throwable th) {
                    String valueOf = String.valueOf(th);
                    StringBuilder sb = new StringBuilder(24 + String.valueOf(valueOf).length());
                    sb.append("Could not call loadUrl. ");
                    sb.append(valueOf);
                    str2 = sb.toString();
                }
            } else {
                str2 = "The webview is destroyed. Ignoring action.";
                zzkd.zzcx(str2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        synchronized (this.zzail) {
            super.onAttachedToWindow();
            if (!isDestroyed()) {
                this.zzaqg.onAttachedToWindow();
            }
            zzal(this.zzcpo);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        synchronized (this.zzail) {
            if (!isDestroyed()) {
                this.zzaqg.onDetachedFromWindow();
            }
            super.onDetachedFromWindow();
        }
        zzal(false);
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            zzu.zzfq().zzb(getContext(), intent);
        } catch (ActivityNotFoundException unused) {
            StringBuilder sb = new StringBuilder(51 + String.valueOf(str).length() + String.valueOf(str4).length());
            sb.append("Couldn't find an Activity to view url/mimetype: ");
            sb.append(str);
            sb.append(" / ");
            sb.append(str4);
            zzkd.zzcv(sb.toString());
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public void onDraw(Canvas canvas) {
        if (!isDestroyed()) {
            if (VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
                super.onDraw(canvas);
                if (!(zzuj() == null || zzuj().zzvf() == null)) {
                    zzuj().zzvf().zzem();
                }
            }
        }
    }

    public void onGlobalLayout() {
        boolean zzvg = zzvg();
        com.google.android.gms.ads.internal.overlay.zzd zzuh = zzuh();
        if (zzuh != null && zzvg) {
            zzuh.zznz();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ed, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e5  */
    public void onMeasure(int i, int i2) {
        int i3;
        synchronized (this.zzail) {
            if (isDestroyed()) {
                setMeasuredDimension(0, 0);
                return;
            }
            if (!isInEditMode() && !this.zzcpi && !this.zzani.zzauu) {
                if (!this.zzani.zzauv) {
                    if (this.zzani.zzaus) {
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        this.zzaqm.getDefaultDisplay().getMetrics(displayMetrics);
                        setMeasuredDimension(displayMetrics.widthPixels, displayMetrics.heightPixels);
                        return;
                    }
                    int mode = MeasureSpec.getMode(i);
                    int size = MeasureSpec.getSize(i);
                    int mode2 = MeasureSpec.getMode(i2);
                    int size2 = MeasureSpec.getSize(i2);
                    int i4 = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
                    if (mode != Integer.MIN_VALUE) {
                        if (mode != 1073741824) {
                            i3 = Integer.MAX_VALUE;
                            if (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) {
                                i4 = size2;
                            }
                            if (this.zzani.widthPixels <= i3) {
                                if (this.zzani.heightPixels <= i4) {
                                    if (getVisibility() != 8) {
                                        setVisibility(0);
                                    }
                                    setMeasuredDimension(this.zzani.widthPixels, this.zzani.heightPixels);
                                }
                            }
                            float f = this.zzcpc.getResources().getDisplayMetrics().density;
                            int i5 = (int) (((float) this.zzani.widthPixels) / f);
                            int i6 = (int) (((float) this.zzani.heightPixels) / f);
                            int i7 = (int) (((float) size) / f);
                            int i8 = (int) (((float) size2) / f);
                            StringBuilder sb = new StringBuilder(103);
                            sb.append("Not enough space to show ad. Needs ");
                            sb.append(i5);
                            sb.append("x");
                            sb.append(i6);
                            sb.append(" dp, but only has ");
                            sb.append(i7);
                            sb.append("x");
                            sb.append(i8);
                            sb.append(" dp.");
                            zzkd.zzcx(sb.toString());
                            if (getVisibility() != 8) {
                                setVisibility(4);
                            }
                            setMeasuredDimension(0, 0);
                        }
                    }
                    i3 = size;
                    i4 = size2;
                    if (this.zzani.widthPixels <= i3) {
                    }
                    float f2 = this.zzcpc.getResources().getDisplayMetrics().density;
                    int i52 = (int) (((float) this.zzani.widthPixels) / f2);
                    int i62 = (int) (((float) this.zzani.heightPixels) / f2);
                    int i72 = (int) (((float) size) / f2);
                    int i82 = (int) (((float) size2) / f2);
                    StringBuilder sb2 = new StringBuilder(103);
                    sb2.append("Not enough space to show ad. Needs ");
                    sb2.append(i52);
                    sb2.append("x");
                    sb2.append(i62);
                    sb2.append(" dp, but only has ");
                    sb2.append(i72);
                    sb2.append("x");
                    sb2.append(i82);
                    sb2.append(" dp.");
                    zzkd.zzcx(sb2.toString());
                    if (getVisibility() != 8) {
                    }
                    setMeasuredDimension(0, 0);
                }
            }
            super.onMeasure(i, i2);
        }
    }

    public void onPause() {
        if (!isDestroyed()) {
            try {
                if (com.google.android.gms.common.util.zzs.zzavn()) {
                    super.onPause();
                }
            } catch (Exception e) {
                zzkd.zzb("Could not pause webview.", e);
            }
        }
    }

    public void onResume() {
        if (!isDestroyed()) {
            try {
                if (com.google.android.gms.common.util.zzs.zzavn()) {
                    super.onResume();
                }
            } catch (Exception e) {
                zzkd.zzb("Could not resume webview.", e);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.zzbgd != null) {
            this.zzbgd.zza(motionEvent);
        }
        if (isDestroyed()) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setContext(Context context) {
        this.zzcpc.setBaseContext(context);
        this.zzaqg.zzl(this.zzcpc.zzue());
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.zzcpt = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public void setRequestedOrientation(int i) {
        synchronized (this.zzail) {
            this.zzcpk = i;
            if (this.zzcpf != null) {
                this.zzcpf.setRequestedOrientation(this.zzcpk);
            }
        }
    }

    public void setWebViewClient(WebViewClient webViewClient) {
        super.setWebViewClient(webViewClient);
        if (webViewClient instanceof zzli) {
            this.zzcpe = (zzli) webViewClient;
        }
    }

    public void stopLoading() {
        if (!isDestroyed()) {
            try {
                super.stopLoading();
            } catch (Exception e) {
                zzkd.zzb("Could not stop loading webview.", e);
            }
        }
    }

    public void zza(Context context, AdSizeParcel adSizeParcel, zzdk zzdk) {
        synchronized (this.zzail) {
            this.zzaqg.zztt();
            setContext(context);
            this.zzcpf = null;
            this.zzani = adSizeParcel;
            this.zzcpi = false;
            this.zzcpg = false;
            this.zzbvq = "";
            this.zzcpk = -1;
            zzu.zzfs().zzj(this);
            loadUrl("about:blank");
            this.zzcpe.reset();
            setOnTouchListener(null);
            setOnClickListener(null);
            this.zzcpl = true;
            this.zzcpm = false;
            this.zzcpn = null;
            zzd(zzdk);
            this.zzcpo = false;
            zzu.zzgj().zzd(this);
            zzvm();
        }
    }

    public void zza(AdSizeParcel adSizeParcel) {
        synchronized (this.zzail) {
            this.zzani = adSizeParcel;
            requestLayout();
        }
    }

    public void zza(zzcd zzcd, boolean z) {
        synchronized (this.zzail) {
            this.zzcpo = z;
        }
        zzal(z);
    }

    public void zza(zzlm zzlm) {
        synchronized (this.zzail) {
            if (this.zzcpn != null) {
                zzkd.e("Attempt to create multiple AdWebViewVideoControllers.");
            } else {
                this.zzcpn = zzlm;
            }
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(19)
    public void zza(String str, ValueCallback<String> valueCallback) {
        synchronized (this.zzail) {
            if (!isDestroyed()) {
                evaluateJavascript(str, valueCallback);
            } else {
                zzkd.zzcx("The webview is destroyed. Ignoring action.");
                if (valueCallback != null) {
                    valueCallback.onReceiveValue(null);
                }
            }
        }
    }

    public void zza(String str, zzep zzep) {
        if (this.zzcpe != null) {
            this.zzcpe.zza(str, zzep);
        }
    }

    public void zza(String str, Map<String, ?> map) {
        try {
            zzb(str, zzu.zzfq().zzam(map));
        } catch (JSONException unused) {
            zzkd.zzcx("Could not convert parameters to JSON.");
        }
    }

    public void zza(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        zzj(str, jSONObject.toString());
    }

    public void zzaf(int i) {
        zzvi();
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i));
        hashMap.put("version", this.zzalo.zzcs);
        zza("onhide", (Map<String, ?>) hashMap);
    }

    public void zzah(boolean z) {
        synchronized (this.zzail) {
            this.zzcpi = z;
            zzvj();
        }
    }

    public void zzai(boolean z) {
        synchronized (this.zzail) {
            if (this.zzcpf != null) {
                this.zzcpf.zza(this.zzcpe.zzho(), z);
            } else {
                this.zzcpg = z;
            }
        }
    }

    public void zzaj(boolean z) {
        synchronized (this.zzail) {
            this.zzcpl = z;
        }
    }

    public void zzb(com.google.android.gms.ads.internal.overlay.zzd zzd) {
        synchronized (this.zzail) {
            this.zzcpf = zzd;
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzb(Boolean bool) {
        synchronized (this.zzail) {
            this.zzcjw = bool;
        }
        zzu.zzft().zzb(bool);
    }

    public void zzb(String str, zzep zzep) {
        if (this.zzcpe != null) {
            this.zzcpe.zzb(str, zzep);
        }
    }

    public void zzb(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("AFMA_ReceiveMessage('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String str2 = "Dispatching AFMA event: ";
        String valueOf = String.valueOf(sb.toString());
        zzkd.v(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        zzdc(sb.toString());
    }

    public void zzc(com.google.android.gms.ads.internal.overlay.zzd zzd) {
        synchronized (this.zzail) {
            this.zzcpu = zzd;
        }
    }

    public void zzcy(String str) {
        synchronized (this.zzail) {
            try {
                super.loadUrl(str);
            } catch (Throwable th) {
                String valueOf = String.valueOf(th);
                StringBuilder sb = new StringBuilder(24 + String.valueOf(valueOf).length());
                sb.append("Could not call loadUrl. ");
                sb.append(valueOf);
                zzkd.zzcx(sb.toString());
            }
        }
    }

    public void zzcz(String str) {
        synchronized (this.zzail) {
            if (str == null) {
                str = "";
            }
            this.zzbvq = str;
        }
    }

    /* access modifiers changed from: protected */
    public void zzdb(String str) {
        synchronized (this.zzail) {
            if (!isDestroyed()) {
                loadUrl(str);
            } else {
                zzkd.zzcx("The webview is destroyed. Ignoring action.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzdc(String str) {
        String str2;
        String str3;
        String str4;
        if (com.google.android.gms.common.util.zzs.zzavu()) {
            if (zzsq() == null) {
                zzvh();
            }
            if (zzsq().booleanValue()) {
                zza(str, null);
                return;
            }
            str4 = "javascript:";
            str3 = String.valueOf(str);
            if (str3.length() == 0) {
                str2 = new String(str4);
                zzdb(str2);
            }
        } else {
            str4 = "javascript:";
            str3 = String.valueOf(str);
            if (str3.length() == 0) {
                str2 = new String(str4);
                zzdb(str2);
            }
        }
        str2 = str4.concat(str3);
        zzdb(str2);
    }

    public AdSizeParcel zzdn() {
        AdSizeParcel adSizeParcel;
        synchronized (this.zzail) {
            adSizeParcel = this.zzani;
        }
        return adSizeParcel;
    }

    public void zzef() {
        synchronized (this.zzail) {
            this.zzcpm = true;
            if (this.zzcpd != null) {
                this.zzcpd.zzef();
            }
        }
    }

    public void zzeg() {
        synchronized (this.zzail) {
            this.zzcpm = false;
            if (this.zzcpd != null) {
                this.zzcpd.zzeg();
            }
        }
    }

    public void zzj(String str, String str2) {
        StringBuilder sb = new StringBuilder(3 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb.append(str);
        sb.append("(");
        sb.append(str2);
        sb.append(");");
        zzdc(sb.toString());
    }

    public void zzoa() {
        if (this.zzcpp == null) {
            zzdg.zza(this.zzcps.zzkf(), this.zzcpr, "aes");
            this.zzcpp = zzdg.zzb(this.zzcps.zzkf());
            this.zzcps.zza("native:view_show", this.zzcpp);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.zzalo.zzcs);
        zza("onshow", (Map<String, ?>) hashMap);
    }

    public boolean zzou() {
        boolean z;
        synchronized (this.zzail) {
            zzdg.zza(this.zzcps.zzkf(), this.zzcpq, "aebb2");
            z = this.zzcpl;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public Boolean zzsq() {
        Boolean bool;
        synchronized (this.zzail) {
            bool = this.zzcjw;
        }
        return bool;
    }

    public void zzuc() {
        zzvi();
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.zzalo.zzcs);
        zza("onhide", (Map<String, ?>) hashMap);
    }

    public void zzud() {
        HashMap hashMap = new HashMap(3);
        hashMap.put("app_muted", String.valueOf(zzu.zzfq().zzfa()));
        hashMap.put("app_volume", String.valueOf(zzu.zzfq().zzey()));
        hashMap.put("device_volume", String.valueOf(zzu.zzfq().zzal(getContext())));
        zza("volume", (Map<String, ?>) hashMap);
    }

    public Activity zzue() {
        return this.zzcpc.zzue();
    }

    public Context zzuf() {
        return this.zzcpc.zzuf();
    }

    public zzd zzug() {
        return this.zzajv;
    }

    public com.google.android.gms.ads.internal.overlay.zzd zzuh() {
        com.google.android.gms.ads.internal.overlay.zzd zzd;
        synchronized (this.zzail) {
            zzd = this.zzcpf;
        }
        return zzd;
    }

    public com.google.android.gms.ads.internal.overlay.zzd zzui() {
        com.google.android.gms.ads.internal.overlay.zzd zzd;
        synchronized (this.zzail) {
            zzd = this.zzcpu;
        }
        return zzd;
    }

    public zzli zzuj() {
        return this.zzcpe;
    }

    public boolean zzuk() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzcpg;
        }
        return z;
    }

    public zzas zzul() {
        return this.zzbgd;
    }

    public VersionInfoParcel zzum() {
        return this.zzalo;
    }

    public boolean zzun() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzcpi;
        }
        return z;
    }

    public void zzuo() {
        synchronized (this.zzail) {
            zzkd.v("Destroying WebView!");
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzll.super.destroy();
                }
            });
        }
    }

    public boolean zzup() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzcpm;
        }
        return z;
    }

    public zzlg zzuq() {
        return null;
    }

    public zzdi zzur() {
        return this.zzcpr;
    }

    public zzdj zzus() {
        return this.zzcps;
    }

    public zzlm zzut() {
        zzlm zzlm;
        synchronized (this.zzail) {
            zzlm = this.zzcpn;
        }
        return zzlm;
    }

    public void zzuu() {
        this.zzaqg.zzts();
    }

    public void zzuv() {
        if (this.zzcpr == null) {
            this.zzcpr = zzdg.zzb(this.zzcps.zzkf());
            this.zzcps.zza("native:view_load", this.zzcpr);
        }
    }

    public OnClickListener zzuw() {
        return (OnClickListener) this.zzcpt.get();
    }

    public boolean zzvg() {
        int i;
        int i2;
        boolean z = false;
        if (!zzuj().zzho()) {
            return false;
        }
        DisplayMetrics zza2 = zzu.zzfq().zza(this.zzaqm);
        int zzb = zzm.zziw().zzb(zza2, zza2.widthPixels);
        int zzb2 = zzm.zziw().zzb(zza2, zza2.heightPixels);
        Activity zzue = zzue();
        if (zzue == null || zzue.getWindow() == null) {
            i2 = zzb;
            i = zzb2;
        } else {
            int[] zzh = zzu.zzfq().zzh(zzue);
            int zzb3 = zzm.zziw().zzb(zza2, zzh[0]);
            i = zzm.zziw().zzb(zza2, zzh[1]);
            i2 = zzb3;
        }
        if (this.zzbrf == zzb && this.zzbrg == zzb2 && this.zzbri == i2 && this.zzbrj == i) {
            return false;
        }
        if (!(this.zzbrf == zzb && this.zzbrg == zzb2)) {
            z = true;
        }
        this.zzbrf = zzb;
        this.zzbrg = zzb2;
        this.zzbri = i2;
        this.zzbrj = i;
        new zzhf(this).zza(zzb, zzb2, i2, i, zza2.density, this.zzaqm.getDefaultDisplay().getRotation());
        return z;
    }
}
