package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.zzd;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class zzqy {
    private static final com.google.android.gms.internal.zzpm.zza<?, ?>[] vF = new com.google.android.gms.internal.zzpm.zza[0];
    private final Map<com.google.android.gms.common.api.Api.zzc<?>, zze> ui;
    final Set<com.google.android.gms.internal.zzpm.zza<?, ?>> vG;
    private final zzb vH;
    /* access modifiers changed from: private */
    public zzc vI;

    private static class zza implements DeathRecipient, zzb {
        private final WeakReference<com.google.android.gms.internal.zzpm.zza<?, ?>> vK;
        private final WeakReference<zzd> vL;
        private final WeakReference<IBinder> vM;

        private zza(com.google.android.gms.internal.zzpm.zza<?, ?> zza, zzd zzd, IBinder iBinder) {
            this.vL = new WeakReference<>(zzd);
            this.vK = new WeakReference<>(zza);
            this.vM = new WeakReference<>(iBinder);
        }

        private void zzaqg() {
            com.google.android.gms.internal.zzpm.zza zza = (com.google.android.gms.internal.zzpm.zza) this.vK.get();
            zzd zzd = (zzd) this.vL.get();
            if (!(zzd == null || zza == null)) {
                zzd.remove(zza.zzaoj().intValue());
            }
            IBinder iBinder = (IBinder) this.vM.get();
            if (this.vM != null) {
                iBinder.unlinkToDeath(this, 0);
            }
        }

        public void binderDied() {
            zzaqg();
        }

        public void zzh(com.google.android.gms.internal.zzpm.zza<?, ?> zza) {
            zzaqg();
        }
    }

    interface zzb {
        void zzh(com.google.android.gms.internal.zzpm.zza<?, ?> zza);
    }

    interface zzc {
        void zzaqn();
    }

    public zzqy(com.google.android.gms.common.api.Api.zzc<?> zzc2, zze zze) {
        this.vG = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
        this.vH = new zzb() {
            public void zzh(com.google.android.gms.internal.zzpm.zza<?, ?> zza) {
                zzqy.this.vG.remove(zza);
                if (!(zza.zzaoj() == null || zzqy.zza(zzqy.this) == null)) {
                    zzqy.zza(zzqy.this).remove(zza.zzaoj().intValue());
                }
                if (zzqy.this.vI != null && zzqy.this.vG.isEmpty()) {
                    zzqy.this.vI.zzaqn();
                }
            }
        };
        this.vI = null;
        this.ui = new ArrayMap();
        this.ui.put(zzc2, zze);
    }

    public zzqy(Map<com.google.android.gms.common.api.Api.zzc<?>, zze> map) {
        this.vG = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
        this.vH = new zzb() {
            public void zzh(com.google.android.gms.internal.zzpm.zza<?, ?> zza) {
                zzqy.this.vG.remove(zza);
                if (!(zza.zzaoj() == null || zzqy.zza(zzqy.this) == null)) {
                    zzqy.zza(zzqy.this).remove(zza.zzaoj().intValue());
                }
                if (zzqy.this.vI != null && zzqy.this.vG.isEmpty()) {
                    zzqy.this.vI.zzaqn();
                }
            }
        };
        this.vI = null;
        this.ui = map;
    }

    static /* synthetic */ zzd zza(zzqy zzqy) {
        return null;
    }

    private static void zza(com.google.android.gms.internal.zzpm.zza<?, ?> zza2, zzd zzd, IBinder iBinder) {
        if (zza2.isReady()) {
            zza2.zza((zzb) new zza(zza2, zzd, iBinder));
            return;
        }
        if (iBinder == null || !iBinder.isBinderAlive()) {
            zza2.zza((zzb) null);
        } else {
            zza zza3 = new zza(zza2, zzd, iBinder);
            zza2.zza((zzb) zza3);
            try {
                iBinder.linkToDeath(zza3, 0);
                return;
            } catch (RemoteException unused) {
            }
        }
        zza2.cancel();
        zzd.remove(zza2.zzaoj().intValue());
    }

    public void dump(PrintWriter printWriter) {
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.vG.size());
    }

    public void release() {
        com.google.android.gms.internal.zzpm.zza[] zzaArr;
        for (com.google.android.gms.internal.zzpm.zza zza2 : (com.google.android.gms.internal.zzpm.zza[]) this.vG.toArray(vF)) {
            zza2.zza((zzb) null);
            if (zza2.zzaoj() != null) {
                zza2.zzaor();
                zza(zza2, null, ((zze) this.ui.get(zza2.zzans())).zzanv());
            } else if (!zza2.zzaov()) {
            }
            this.vG.remove(zza2);
        }
    }

    public void zza(zzc zzc2) {
        if (this.vG.isEmpty()) {
            zzc2.zzaqn();
        }
        this.vI = zzc2;
    }

    public void zzaqz() {
        for (com.google.android.gms.internal.zzpm.zza zzaa : (com.google.android.gms.internal.zzpm.zza[]) this.vG.toArray(vF)) {
            zzaa.zzaa(new Status(8, "The connection to Google Play services was lost"));
        }
    }

    public boolean zzara() {
        for (com.google.android.gms.internal.zzpm.zza isReady : (com.google.android.gms.internal.zzpm.zza[]) this.vG.toArray(vF)) {
            if (!isReady.isReady()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public <A extends com.google.android.gms.common.api.Api.zzb> void zzg(com.google.android.gms.internal.zzpm.zza<? extends Result, A> zza2) {
        this.vG.add(zza2);
        zza2.zza(this.vH);
    }
}
