package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.SearchAdRequestParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zziz.zza;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zziq {
    private static final SimpleDateFormat zzcel = new SimpleDateFormat("yyyyMMdd", Locale.US);

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c8 A[Catch:{ JSONException -> 0x0217 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ce A[Catch:{ JSONException -> 0x0217 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0131 A[Catch:{ JSONException -> 0x0217 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x013a A[Catch:{ JSONException -> 0x0217 }] */
    public static AdResponseParcel zza(Context context, AdRequestInfoParcel adRequestInfoParcel, String str) {
        int i;
        long j;
        String str2;
        String str3;
        AdResponseParcel adResponseParcel;
        long j2;
        int zztj;
        AdRequestInfoParcel adRequestInfoParcel2 = adRequestInfoParcel;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("ad_base_url", null);
            String optString2 = jSONObject.optString("ad_url", null);
            String optString3 = jSONObject.optString("ad_size", null);
            String optString4 = jSONObject.optString("ad_slot_size", optString3);
            boolean z = (adRequestInfoParcel2 == null || adRequestInfoParcel2.zzcax == 0) ? false : true;
            String optString5 = jSONObject.optString("ad_json", null);
            if (optString5 == null) {
                optString5 = jSONObject.optString("ad_html", null);
            }
            if (optString5 == null) {
                optString5 = jSONObject.optString("body", null);
            }
            String optString6 = jSONObject.optString("debug_dialog", null);
            long j3 = jSONObject.has("interstitial_timeout") ? (long) (jSONObject.getDouble("interstitial_timeout") * 1000.0d) : -1;
            String optString7 = jSONObject.optString("orientation", null);
            if ("portrait".equals(optString7)) {
                zztj = zzu.zzfs().zztk();
            } else if ("landscape".equals(optString7)) {
                zztj = zzu.zzfs().zztj();
            } else {
                i = -1;
                if (TextUtils.isEmpty(optString5) || TextUtils.isEmpty(optString2)) {
                    str3 = optString;
                    str2 = optString5;
                    adResponseParcel = null;
                    j = -1;
                } else {
                    adResponseParcel = zzip.zza(adRequestInfoParcel2, context, adRequestInfoParcel2.zzaow.zzcs, optString2, null, null, null, null);
                    String str4 = adResponseParcel.zzbto;
                    String str5 = adResponseParcel.body;
                    j = adResponseParcel.zzccc;
                    str2 = str5;
                    str3 = str4;
                }
                if (str2 != null) {
                    return new AdResponseParcel(0);
                }
                JSONArray optJSONArray = jSONObject.optJSONArray("click_urls");
                List list = adResponseParcel == null ? null : adResponseParcel.zzbnm;
                if (optJSONArray != null) {
                    list = zza(optJSONArray, list);
                }
                JSONArray optJSONArray2 = jSONObject.optJSONArray("impression_urls");
                List list2 = adResponseParcel == null ? null : adResponseParcel.zzbnn;
                if (optJSONArray2 != null) {
                    list2 = zza(optJSONArray2, list2);
                }
                JSONArray optJSONArray3 = jSONObject.optJSONArray("manual_impression_urls");
                List list3 = adResponseParcel == null ? null : adResponseParcel.zzcca;
                List zza = optJSONArray3 != null ? zza(optJSONArray3, list3) : list3;
                if (adResponseParcel != null) {
                    if (adResponseParcel.orientation != -1) {
                        i = adResponseParcel.orientation;
                    }
                    if (adResponseParcel.zzcbx > 0) {
                        j2 = adResponseParcel.zzcbx;
                        String optString8 = jSONObject.optString("active_view");
                        boolean optBoolean = jSONObject.optBoolean("ad_is_javascript", false);
                        AdResponseParcel adResponseParcel2 = new AdResponseParcel(adRequestInfoParcel2, str3, str2, list, list2, j2, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), zza, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j, optString6, optBoolean, !optBoolean ? jSONObject.optString("ad_passback_url", null) : null, optString8, jSONObject.optBoolean("custom_render_allowed", false), z, adRequestInfoParcel2.zzcaz, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), "height".equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), RewardItemParcel.zza(jSONObject.optJSONArray("rewards")), zza(jSONObject.optJSONArray("video_start_urls"), null), zza(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), AutoClickProtectionConfigurationParcel.zzh(jSONObject.optJSONObject("auto_protection_configuration")), adRequestInfoParcel2.zzcbq, jSONObject.optString("set_cookie", ""), zza(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optString("safe_browsing"), jSONObject.optBoolean("render_in_browser", adRequestInfoParcel2.zzbnq), optString4);
                        return adResponseParcel2;
                    }
                }
                j2 = j3;
                String optString82 = jSONObject.optString("active_view");
                boolean optBoolean2 = jSONObject.optBoolean("ad_is_javascript", false);
                AdResponseParcel adResponseParcel22 = new AdResponseParcel(adRequestInfoParcel2, str3, str2, list, list2, j2, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), zza, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j, optString6, optBoolean2, !optBoolean2 ? jSONObject.optString("ad_passback_url", null) : null, optString82, jSONObject.optBoolean("custom_render_allowed", false), z, adRequestInfoParcel2.zzcaz, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), "height".equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), RewardItemParcel.zza(jSONObject.optJSONArray("rewards")), zza(jSONObject.optJSONArray("video_start_urls"), null), zza(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), AutoClickProtectionConfigurationParcel.zzh(jSONObject.optJSONObject("auto_protection_configuration")), adRequestInfoParcel2.zzcbq, jSONObject.optString("set_cookie", ""), zza(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optString("safe_browsing"), jSONObject.optBoolean("render_in_browser", adRequestInfoParcel2.zzbnq), optString4);
                return adResponseParcel22;
            }
            i = zztj;
            if (TextUtils.isEmpty(optString5)) {
            }
            str3 = optString;
            str2 = optString5;
            adResponseParcel = null;
            j = -1;
            if (str2 != null) {
            }
        } catch (JSONException e) {
            String str6 = "Could not parse the inline ad response: ";
            String valueOf = String.valueOf(e.getMessage());
            zzkd.zzcx(valueOf.length() != 0 ? str6.concat(valueOf) : new String(str6));
            return new AdResponseParcel(0);
        }
    }

    @Nullable
    private static List<String> zza(@Nullable JSONArray jSONArray, @Nullable List<String> list) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        if (list == null) {
            list = new LinkedList<>();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            list.add(jSONArray.getString(i));
        }
        return list;
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0249 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0280 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0291 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x02b7 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x02cf A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x02de A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0309 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x031a A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01ae A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01cb A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01d5 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01f7 A[Catch:{ JSONException -> 0x0348 }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x021d A[Catch:{ JSONException -> 0x0348 }] */
    @Nullable
    public static JSONObject zza(Context context, AdRequestInfoParcel adRequestInfoParcel, zziv zziv, zza zza, Location location, zzcv zzcv, String str, List<String> list, Bundle bundle, String str2) {
        AdRequestInfoParcel adRequestInfoParcel2 = adRequestInfoParcel;
        zziv zziv2 = zziv;
        Location location2 = location;
        Bundle bundle2 = bundle;
        try {
            HashMap hashMap = new HashMap();
            if (list.size() > 0) {
                hashMap.put("eid", TextUtils.join(",", list));
            }
            if (adRequestInfoParcel2.zzcaq != null) {
                hashMap.put("ad_pos", adRequestInfoParcel2.zzcaq);
            }
            zza(hashMap, adRequestInfoParcel2.zzcar);
            if (adRequestInfoParcel2.zzapa.zzaut != null) {
                AdSizeParcel[] adSizeParcelArr = adRequestInfoParcel2.zzapa.zzaut;
                int length = adSizeParcelArr.length;
                int i = 0;
                boolean z = false;
                boolean z2 = false;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    AdSizeParcel adSizeParcel = adSizeParcelArr[i];
                    if (!adSizeParcel.zzauv && !z) {
                        hashMap.put("format", adSizeParcel.zzaur);
                        z = true;
                    }
                    if (adSizeParcel.zzauv && !z2) {
                        hashMap.put("fluid", "height");
                        z2 = true;
                    }
                    if (z && z2) {
                        break;
                    }
                    i++;
                }
            } else {
                hashMap.put("format", adRequestInfoParcel2.zzapa.zzaur);
                if (adRequestInfoParcel2.zzapa.zzauv) {
                    hashMap.put("fluid", "height");
                }
            }
            int i2 = -1;
            if (adRequestInfoParcel2.zzapa.width == -1) {
                hashMap.put("smart_w", "full");
            }
            if (adRequestInfoParcel2.zzapa.height == -2) {
                hashMap.put("smart_h", "auto");
            }
            if (adRequestInfoParcel2.zzapa.zzaut != null) {
                StringBuilder sb = new StringBuilder();
                AdSizeParcel[] adSizeParcelArr2 = adRequestInfoParcel2.zzapa.zzaut;
                int length2 = adSizeParcelArr2.length;
                int i3 = 0;
                boolean z3 = false;
                while (i3 < length2) {
                    AdSizeParcel adSizeParcel2 = adSizeParcelArr2[i3];
                    if (adSizeParcel2.zzauv) {
                        z3 = true;
                    } else {
                        if (sb.length() != 0) {
                            sb.append("|");
                        }
                        sb.append(adSizeParcel2.width == i2 ? (int) (((float) adSizeParcel2.widthPixels) / zziv2.zzcbd) : adSizeParcel2.width);
                        sb.append("x");
                        sb.append(adSizeParcel2.height == -2 ? (int) (((float) adSizeParcel2.heightPixels) / zziv2.zzcbd) : adSizeParcel2.height);
                    }
                    i3++;
                    i2 = -1;
                }
                if (z3) {
                    if (sb.length() != 0) {
                        sb.insert(0, "|");
                    }
                    sb.insert(0, "320x50");
                }
                hashMap.put("sz", sb);
            }
            if (adRequestInfoParcel2.zzcax != 0) {
                hashMap.put("native_version", Integer.valueOf(adRequestInfoParcel2.zzcax));
                if (!adRequestInfoParcel2.zzapa.zzauw) {
                    hashMap.put("native_templates", adRequestInfoParcel2.zzaps);
                    hashMap.put("native_image_orientation", zzc(adRequestInfoParcel2.zzapo));
                    if (!adRequestInfoParcel2.zzcbi.isEmpty()) {
                        hashMap.put("native_custom_templates", adRequestInfoParcel2.zzcbi);
                    }
                }
            }
            hashMap.put("slotname", adRequestInfoParcel2.zzaou);
            hashMap.put("pn", adRequestInfoParcel2.applicationInfo.packageName);
            if (adRequestInfoParcel2.zzcas != null) {
                hashMap.put("vc", Integer.valueOf(adRequestInfoParcel2.zzcas.versionCode));
            }
            hashMap.put("ms", str);
            hashMap.put("seq_num", adRequestInfoParcel2.zzcau);
            hashMap.put("session_id", adRequestInfoParcel2.zzcav);
            hashMap.put("js", adRequestInfoParcel2.zzaow.zzcs);
            zza(hashMap, zziv2, zza, adRequestInfoParcel2.zzcbv);
            zza(hashMap, str2);
            hashMap.put("platform", Build.MANUFACTURER);
            hashMap.put("submodel", Build.MODEL);
            if (location2 == null) {
                if (adRequestInfoParcel2.zzcar.versionCode >= 2 && adRequestInfoParcel2.zzcar.zzatu != null) {
                    location2 = adRequestInfoParcel2.zzcar.zzatu;
                }
                if (adRequestInfoParcel2.versionCode >= 2) {
                    hashMap.put("quality_signals", adRequestInfoParcel2.zzcaw);
                }
                if (adRequestInfoParcel2.versionCode >= 4 && adRequestInfoParcel2.zzcaz) {
                    hashMap.put("forceHttps", Boolean.valueOf(adRequestInfoParcel2.zzcaz));
                }
                if (bundle2 != null) {
                    hashMap.put("content_info", bundle2);
                }
                if (adRequestInfoParcel2.versionCode < 5) {
                    hashMap.put("u_sd", Float.valueOf(adRequestInfoParcel2.zzcbd));
                    hashMap.put("sh", Integer.valueOf(adRequestInfoParcel2.zzcbc));
                    hashMap.put("sw", Integer.valueOf(adRequestInfoParcel2.zzcbb));
                } else {
                    hashMap.put("u_sd", Float.valueOf(zziv2.zzcbd));
                    hashMap.put("sh", Integer.valueOf(zziv2.zzcbc));
                    hashMap.put("sw", Integer.valueOf(zziv2.zzcbb));
                }
                if (adRequestInfoParcel2.versionCode >= 6) {
                    if (!TextUtils.isEmpty(adRequestInfoParcel2.zzcbe)) {
                        try {
                            hashMap.put("view_hierarchy", new JSONObject(adRequestInfoParcel2.zzcbe));
                        } catch (JSONException e) {
                            zzkd.zzd("Problem serializing view hierarchy to JSON", e);
                        }
                    }
                    hashMap.put("correlation_id", Long.valueOf(adRequestInfoParcel2.zzcbf));
                }
                if (adRequestInfoParcel2.versionCode >= 7) {
                    hashMap.put("request_id", adRequestInfoParcel2.zzcbg);
                }
                if (adRequestInfoParcel2.versionCode >= 11 && adRequestInfoParcel2.zzcbk != null) {
                    hashMap.put("capability", adRequestInfoParcel2.zzcbk.toBundle());
                }
                if (adRequestInfoParcel2.versionCode >= 12 && !TextUtils.isEmpty(adRequestInfoParcel2.zzcbl)) {
                    hashMap.put("anchor", adRequestInfoParcel2.zzcbl);
                }
                if (adRequestInfoParcel2.versionCode >= 13) {
                    hashMap.put("android_app_volume", Float.valueOf(adRequestInfoParcel2.zzcbm));
                }
                if (adRequestInfoParcel2.versionCode >= 18) {
                    hashMap.put("android_app_muted", Boolean.valueOf(adRequestInfoParcel2.zzcbs));
                }
                if (adRequestInfoParcel2.versionCode >= 14 && adRequestInfoParcel2.zzcbn > 0) {
                    hashMap.put("target_api", Integer.valueOf(adRequestInfoParcel2.zzcbn));
                }
                if (adRequestInfoParcel2.versionCode >= 15) {
                    String str3 = "scroll_index";
                    int i4 = -1;
                    if (adRequestInfoParcel2.zzcbo != -1) {
                        i4 = adRequestInfoParcel2.zzcbo;
                    }
                    hashMap.put(str3, Integer.valueOf(i4));
                }
                if (adRequestInfoParcel2.versionCode >= 16) {
                    hashMap.put("_activity_context", Boolean.valueOf(adRequestInfoParcel2.zzcbp));
                }
                if (adRequestInfoParcel2.versionCode >= 18) {
                    if (!TextUtils.isEmpty(adRequestInfoParcel2.zzcbt)) {
                        try {
                            hashMap.put("app_settings", new JSONObject(adRequestInfoParcel2.zzcbt));
                        } catch (JSONException e2) {
                            zzkd.zzd("Problem creating json from app settings", e2);
                        }
                    }
                    hashMap.put("render_in_browser", Boolean.valueOf(adRequestInfoParcel2.zzbnq));
                }
                if (adRequestInfoParcel2.versionCode >= 18) {
                    hashMap.put("android_num_video_cache_tasks", Integer.valueOf(adRequestInfoParcel2.zzcbu));
                }
                if (zzkd.zzaz(2)) {
                    String str4 = "Ad Request JSON: ";
                    String valueOf = String.valueOf(zzu.zzfq().zzam((Map<String, ?>) hashMap).toString(2));
                    zzkd.v(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
                }
                return zzu.zzfq().zzam((Map<String, ?>) hashMap);
            }
            zza(hashMap, location2);
            if (adRequestInfoParcel2.versionCode >= 2) {
            }
            hashMap.put("forceHttps", Boolean.valueOf(adRequestInfoParcel2.zzcaz));
            if (bundle2 != null) {
            }
            if (adRequestInfoParcel2.versionCode < 5) {
            }
            if (adRequestInfoParcel2.versionCode >= 6) {
            }
            if (adRequestInfoParcel2.versionCode >= 7) {
            }
            hashMap.put("capability", adRequestInfoParcel2.zzcbk.toBundle());
            hashMap.put("anchor", adRequestInfoParcel2.zzcbl);
            if (adRequestInfoParcel2.versionCode >= 13) {
            }
            if (adRequestInfoParcel2.versionCode >= 18) {
            }
            hashMap.put("target_api", Integer.valueOf(adRequestInfoParcel2.zzcbn));
            if (adRequestInfoParcel2.versionCode >= 15) {
            }
            if (adRequestInfoParcel2.versionCode >= 16) {
            }
            if (adRequestInfoParcel2.versionCode >= 18) {
            }
            if (adRequestInfoParcel2.versionCode >= 18) {
            }
            if (zzkd.zzaz(2)) {
            }
            return zzu.zzfq().zzam((Map<String, ?>) hashMap);
        } catch (JSONException e3) {
            String str5 = "Problem serializing ad request to JSON: ";
            String valueOf2 = String.valueOf(e3.getMessage());
            zzkd.zzcx(valueOf2.length() != 0 ? str5.concat(valueOf2) : new String(str5));
            return null;
        }
    }

    private static void zza(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put("lat", valueOf3);
        hashMap2.put("long", valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }

    private static void zza(HashMap<String, Object> hashMap, AdRequestParcel adRequestParcel) {
        String zzsy = zzkb.zzsy();
        if (zzsy != null) {
            hashMap.put("abf", zzsy);
        }
        if (adRequestParcel.zzatm != -1) {
            hashMap.put("cust_age", zzcel.format(new Date(adRequestParcel.zzatm)));
        }
        if (adRequestParcel.extras != null) {
            hashMap.put("extras", adRequestParcel.extras);
        }
        if (adRequestParcel.zzatn != -1) {
            hashMap.put("cust_gender", Integer.valueOf(adRequestParcel.zzatn));
        }
        if (adRequestParcel.zzato != null) {
            hashMap.put("kw", adRequestParcel.zzato);
        }
        if (adRequestParcel.zzatq != -1) {
            hashMap.put("tag_for_child_directed_treatment", Integer.valueOf(adRequestParcel.zzatq));
        }
        if (adRequestParcel.zzatp) {
            hashMap.put("adtest", "on");
        }
        if (adRequestParcel.versionCode >= 2) {
            if (adRequestParcel.zzatr) {
                hashMap.put("d_imp_hdr", Integer.valueOf(1));
            }
            if (!TextUtils.isEmpty(adRequestParcel.zzats)) {
                hashMap.put("ppid", adRequestParcel.zzats);
            }
            if (adRequestParcel.zzatt != null) {
                zza(hashMap, adRequestParcel.zzatt);
            }
        }
        if (adRequestParcel.versionCode >= 3 && adRequestParcel.zzatv != null) {
            hashMap.put("url", adRequestParcel.zzatv);
        }
        if (adRequestParcel.versionCode >= 5) {
            if (adRequestParcel.zzatx != null) {
                hashMap.put("custom_targeting", adRequestParcel.zzatx);
            }
            if (adRequestParcel.zzaty != null) {
                hashMap.put("category_exclusions", adRequestParcel.zzaty);
            }
            if (adRequestParcel.zzatz != null) {
                hashMap.put("request_agent", adRequestParcel.zzatz);
            }
        }
        if (adRequestParcel.versionCode >= 6 && adRequestParcel.zzaua != null) {
            hashMap.put("request_pkg", adRequestParcel.zzaua);
        }
        if (adRequestParcel.versionCode >= 7) {
            hashMap.put("is_designed_for_families", Boolean.valueOf(adRequestParcel.zzaub));
        }
    }

    private static void zza(HashMap<String, Object> hashMap, SearchAdRequestParcel searchAdRequestParcel) {
        Object obj;
        if (Color.alpha(searchAdRequestParcel.zzawz) != 0) {
            hashMap.put("acolor", zzau(searchAdRequestParcel.zzawz));
        }
        if (Color.alpha(searchAdRequestParcel.backgroundColor) != 0) {
            hashMap.put("bgcolor", zzau(searchAdRequestParcel.backgroundColor));
        }
        if (!(Color.alpha(searchAdRequestParcel.zzaxa) == 0 || Color.alpha(searchAdRequestParcel.zzaxb) == 0)) {
            hashMap.put("gradientto", zzau(searchAdRequestParcel.zzaxa));
            hashMap.put("gradientfrom", zzau(searchAdRequestParcel.zzaxb));
        }
        if (Color.alpha(searchAdRequestParcel.zzaxc) != 0) {
            hashMap.put("bcolor", zzau(searchAdRequestParcel.zzaxc));
        }
        hashMap.put("bthick", Integer.toString(searchAdRequestParcel.zzaxd));
        String str = null;
        switch (searchAdRequestParcel.zzaxe) {
            case 0:
                obj = "none";
                break;
            case 1:
                obj = "dashed";
                break;
            case 2:
                obj = "dotted";
                break;
            case 3:
                obj = "solid";
                break;
            default:
                obj = null;
                break;
        }
        if (obj != null) {
            hashMap.put("btype", obj);
        }
        switch (searchAdRequestParcel.zzaxf) {
            case 0:
                str = "light";
                break;
            case 1:
                str = "medium";
                break;
            case 2:
                str = "dark";
                break;
        }
        if (str != null) {
            hashMap.put("callbuttoncolor", str);
        }
        if (searchAdRequestParcel.zzaxg != null) {
            hashMap.put("channel", searchAdRequestParcel.zzaxg);
        }
        if (Color.alpha(searchAdRequestParcel.zzaxh) != 0) {
            hashMap.put("dcolor", zzau(searchAdRequestParcel.zzaxh));
        }
        if (searchAdRequestParcel.zzaxi != null) {
            hashMap.put("font", searchAdRequestParcel.zzaxi);
        }
        if (Color.alpha(searchAdRequestParcel.zzaxj) != 0) {
            hashMap.put("hcolor", zzau(searchAdRequestParcel.zzaxj));
        }
        hashMap.put("headersize", Integer.toString(searchAdRequestParcel.zzaxk));
        if (searchAdRequestParcel.zzaxl != null) {
            hashMap.put("q", searchAdRequestParcel.zzaxl);
        }
    }

    private static void zza(HashMap<String, Object> hashMap, zziv zziv, zza zza, Bundle bundle) {
        hashMap.put("am", Integer.valueOf(zziv.zzcgd));
        hashMap.put("cog", zzab(zziv.zzcge));
        hashMap.put("coh", zzab(zziv.zzcgf));
        if (!TextUtils.isEmpty(zziv.zzcgg)) {
            hashMap.put("carrier", zziv.zzcgg);
        }
        hashMap.put("gl", zziv.zzcgh);
        if (zziv.zzcgi) {
            hashMap.put("simulator", Integer.valueOf(1));
        }
        if (zziv.zzcgj) {
            hashMap.put("is_sidewinder", Integer.valueOf(1));
        }
        hashMap.put("ma", zzab(zziv.zzcgk));
        hashMap.put("sp", zzab(zziv.zzcgl));
        hashMap.put("hl", zziv.zzcgm);
        if (!TextUtils.isEmpty(zziv.zzcgn)) {
            hashMap.put("mv", zziv.zzcgn);
        }
        hashMap.put("muv", Integer.valueOf(zziv.zzcgo));
        if (zziv.zzcgp != -2) {
            hashMap.put("cnt", Integer.valueOf(zziv.zzcgp));
        }
        hashMap.put("gnt", Integer.valueOf(zziv.zzcgq));
        hashMap.put("pt", Integer.valueOf(zziv.zzcgr));
        hashMap.put("rm", Integer.valueOf(zziv.zzcgs));
        hashMap.put("riv", Integer.valueOf(zziv.zzcgt));
        Bundle bundle2 = new Bundle();
        bundle2.putString("build", zziv.zzcgy);
        Bundle bundle3 = new Bundle();
        bundle3.putBoolean("is_charging", zziv.zzcgv);
        bundle3.putDouble("battery_level", zziv.zzcgu);
        bundle2.putBundle("battery", bundle3);
        Bundle bundle4 = new Bundle();
        bundle4.putInt("active_network_state", zziv.zzcgx);
        bundle4.putBoolean("active_network_metered", zziv.zzcgw);
        if (zza != null) {
            Bundle bundle5 = new Bundle();
            bundle5.putInt("predicted_latency_micros", 0);
            bundle5.putLong("predicted_down_throughput_bps", 0);
            bundle5.putLong("predicted_up_throughput_bps", 0);
            bundle4.putBundle("predictions", bundle5);
        }
        bundle2.putBundle("network", bundle4);
        Bundle bundle6 = new Bundle();
        bundle6.putBoolean("is_browser_custom_tabs_capable", zziv.zzcgz);
        bundle2.putBundle("browser", bundle6);
        if (bundle != null) {
            bundle2.putBundle("android_mem_info", zzf(bundle));
        }
        hashMap.put("device", bundle2);
    }

    private static void zza(HashMap<String, Object> hashMap, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("doritos", str);
        hashMap.put("pii", bundle);
    }

    private static Integer zzab(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }

    private static String zzau(int i) {
        return String.format(Locale.US, "#%06x", new Object[]{Integer.valueOf(i & ViewCompat.MEASURED_SIZE_MASK)});
    }

    private static String zzc(NativeAdOptionsParcel nativeAdOptionsParcel) {
        switch (nativeAdOptionsParcel != null ? nativeAdOptionsParcel.zzbgq : 0) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return "any";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x013d  */
    public static JSONObject zzc(AdResponseParcel adResponseParcel) throws JSONException {
        String str;
        String str2;
        JSONObject jSONObject = new JSONObject();
        if (adResponseParcel.zzbto != null) {
            jSONObject.put("ad_base_url", adResponseParcel.zzbto);
        }
        if (adResponseParcel.zzccb != null) {
            jSONObject.put("ad_size", adResponseParcel.zzccb);
        }
        jSONObject.put("native", adResponseParcel.zzauu);
        jSONObject.put(adResponseParcel.zzauu ? "ad_json" : "ad_html", adResponseParcel.body);
        if (adResponseParcel.zzccd != null) {
            jSONObject.put("debug_dialog", adResponseParcel.zzccd);
        }
        if (adResponseParcel.zzcbx != -1) {
            jSONObject.put("interstitial_timeout", ((double) adResponseParcel.zzcbx) / 1000.0d);
        }
        if (adResponseParcel.orientation == zzu.zzfs().zztk()) {
            str = "orientation";
            str2 = "portrait";
        } else {
            if (adResponseParcel.orientation == zzu.zzfs().zztj()) {
                str = "orientation";
                str2 = "landscape";
            }
            if (adResponseParcel.zzbnm != null) {
                jSONObject.put("click_urls", zzk(adResponseParcel.zzbnm));
            }
            if (adResponseParcel.zzbnn != null) {
                jSONObject.put("impression_urls", zzk(adResponseParcel.zzbnn));
            }
            if (adResponseParcel.zzcca != null) {
                jSONObject.put("manual_impression_urls", zzk(adResponseParcel.zzcca));
            }
            if (adResponseParcel.zzccg != null) {
                jSONObject.put("active_view", adResponseParcel.zzccg);
            }
            jSONObject.put("ad_is_javascript", adResponseParcel.zzcce);
            if (adResponseParcel.zzccf != null) {
                jSONObject.put("ad_passback_url", adResponseParcel.zzccf);
            }
            jSONObject.put("mediation", adResponseParcel.zzcby);
            jSONObject.put("custom_render_allowed", adResponseParcel.zzcch);
            jSONObject.put("content_url_opted_out", adResponseParcel.zzcci);
            jSONObject.put("prefetch", adResponseParcel.zzccj);
            if (adResponseParcel.zzbns != -1) {
                jSONObject.put("refresh_interval_milliseconds", adResponseParcel.zzbns);
            }
            if (adResponseParcel.zzcbz != -1) {
                jSONObject.put("mediation_config_cache_time_milliseconds", adResponseParcel.zzcbz);
            }
            if (!TextUtils.isEmpty(adResponseParcel.zzccm)) {
                jSONObject.put("gws_query_id", adResponseParcel.zzccm);
            }
            jSONObject.put("fluid", !adResponseParcel.zzauv ? "height" : "");
            jSONObject.put("native_express", adResponseParcel.zzauw);
            if (adResponseParcel.zzcco != null) {
                jSONObject.put("video_start_urls", zzk(adResponseParcel.zzcco));
            }
            if (adResponseParcel.zzccp != null) {
                jSONObject.put("video_complete_urls", zzk(adResponseParcel.zzccp));
            }
            if (adResponseParcel.zzccn != null) {
                jSONObject.put("rewards", adResponseParcel.zzccn.zzrw());
            }
            jSONObject.put("use_displayed_impression", adResponseParcel.zzccq);
            jSONObject.put("auto_protection_configuration", adResponseParcel.zzccr);
            jSONObject.put("render_in_browser", adResponseParcel.zzbnq);
            return jSONObject;
        }
        jSONObject.put(str, str2);
        if (adResponseParcel.zzbnm != null) {
        }
        if (adResponseParcel.zzbnn != null) {
        }
        if (adResponseParcel.zzcca != null) {
        }
        if (adResponseParcel.zzccg != null) {
        }
        jSONObject.put("ad_is_javascript", adResponseParcel.zzcce);
        if (adResponseParcel.zzccf != null) {
        }
        jSONObject.put("mediation", adResponseParcel.zzcby);
        jSONObject.put("custom_render_allowed", adResponseParcel.zzcch);
        jSONObject.put("content_url_opted_out", adResponseParcel.zzcci);
        jSONObject.put("prefetch", adResponseParcel.zzccj);
        if (adResponseParcel.zzbns != -1) {
        }
        if (adResponseParcel.zzcbz != -1) {
        }
        if (!TextUtils.isEmpty(adResponseParcel.zzccm)) {
        }
        jSONObject.put("fluid", !adResponseParcel.zzauv ? "height" : "");
        jSONObject.put("native_express", adResponseParcel.zzauw);
        if (adResponseParcel.zzcco != null) {
        }
        if (adResponseParcel.zzccp != null) {
        }
        if (adResponseParcel.zzccn != null) {
        }
        jSONObject.put("use_displayed_impression", adResponseParcel.zzccq);
        jSONObject.put("auto_protection_configuration", adResponseParcel.zzccr);
        jSONObject.put("render_in_browser", adResponseParcel.zzbnq);
        return jSONObject;
    }

    private static Bundle zzf(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        bundle2.putString("runtime_free", Long.toString(bundle.getLong("runtime_free_memory", -1)));
        bundle2.putString("runtime_max", Long.toString(bundle.getLong("runtime_max_memory", -1)));
        bundle2.putString("runtime_total", Long.toString(bundle.getLong("runtime_total_memory", -1)));
        MemoryInfo memoryInfo = (MemoryInfo) bundle.getParcelable("debug_memory_info");
        if (memoryInfo != null) {
            bundle2.putString("debug_info_dalvik_private_dirty", Integer.toString(memoryInfo.dalvikPrivateDirty));
            bundle2.putString("debug_info_dalvik_pss", Integer.toString(memoryInfo.dalvikPss));
            bundle2.putString("debug_info_dalvik_shared_dirty", Integer.toString(memoryInfo.dalvikSharedDirty));
            bundle2.putString("debug_info_native_private_dirty", Integer.toString(memoryInfo.nativePrivateDirty));
            bundle2.putString("debug_info_native_pss", Integer.toString(memoryInfo.nativePss));
            bundle2.putString("debug_info_native_shared_dirty", Integer.toString(memoryInfo.nativeSharedDirty));
            bundle2.putString("debug_info_other_private_dirty", Integer.toString(memoryInfo.otherPrivateDirty));
            bundle2.putString("debug_info_other_pss", Integer.toString(memoryInfo.otherPss));
            bundle2.putString("debug_info_other_shared_dirty", Integer.toString(memoryInfo.otherSharedDirty));
        }
        return bundle2;
    }

    @Nullable
    static JSONArray zzk(List<String> list) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return jSONArray;
    }
}
