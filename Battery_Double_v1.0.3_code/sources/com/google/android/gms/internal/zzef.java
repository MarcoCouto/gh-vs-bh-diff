package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.internal.zzdu.zza;

@zzin
public class zzef extends zzg<zzdu> {
    public zzef() {
        super("com.google.android.gms.ads.NativeAdViewDelegateCreatorImpl");
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzai */
    public zzdu zzc(IBinder iBinder) {
        return zza.zzaa(iBinder);
    }

    public zzdt zzb(Context context, FrameLayout frameLayout, FrameLayout frameLayout2) {
        try {
            return zzdt.zza.zzz(((zzdu) zzcr(context)).zza(zze.zzac(context), zze.zzac(frameLayout), zze.zzac(frameLayout2), com.google.android.gms.common.internal.zze.xM));
        } catch (RemoteException | zzg.zza e) {
            zzb.zzd("Could not create remote NativeAdViewDelegate.", e);
            return null;
        }
    }
}
