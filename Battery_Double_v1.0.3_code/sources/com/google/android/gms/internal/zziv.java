package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzi;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.Locale;

@zzin
public final class zziv {
    public final int zzcbb;
    public final int zzcbc;
    public final float zzcbd;
    public final int zzcgd;
    public final boolean zzcge;
    public final boolean zzcgf;
    public final String zzcgg;
    public final String zzcgh;
    public final boolean zzcgi;
    public final boolean zzcgj;
    public final boolean zzcgk;
    public final boolean zzcgl;
    public final String zzcgm;
    public final String zzcgn;
    public final int zzcgo;
    public final int zzcgp;
    public final int zzcgq;
    public final int zzcgr;
    public final int zzcgs;
    public final int zzcgt;
    public final double zzcgu;
    public final boolean zzcgv;
    public final boolean zzcgw;
    public final int zzcgx;
    public final String zzcgy;
    public final boolean zzcgz;

    public static final class zza {
        private int zzcbb;
        private int zzcbc;
        private float zzcbd;
        private int zzcgd;
        private boolean zzcge;
        private boolean zzcgf;
        private String zzcgg;
        private String zzcgh;
        private boolean zzcgi;
        private boolean zzcgj;
        private boolean zzcgk;
        private boolean zzcgl;
        private String zzcgm;
        private String zzcgn;
        private int zzcgo;
        private int zzcgp;
        private int zzcgq;
        private int zzcgr;
        private int zzcgs;
        private int zzcgt;
        private double zzcgu;
        private boolean zzcgv;
        private boolean zzcgw;
        private int zzcgx;
        private String zzcgy;
        private boolean zzcgz;

        public zza(Context context) {
            PackageManager packageManager = context.getPackageManager();
            zzv(context);
            zza(context, packageManager);
            zzw(context);
            Locale locale = Locale.getDefault();
            boolean z = false;
            this.zzcge = zza(packageManager, "geo:0,0?q=donuts") != null;
            if (zza(packageManager, "http://www.google.com") != null) {
                z = true;
            }
            this.zzcgf = z;
            this.zzcgh = locale.getCountry();
            this.zzcgi = zzm.zziw().zztw();
            this.zzcgj = zzi.zzcl(context);
            this.zzcgm = locale.getLanguage();
            this.zzcgn = zza(packageManager);
            Resources resources = context.getResources();
            if (resources != null) {
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                if (displayMetrics != null) {
                    this.zzcbd = displayMetrics.density;
                    this.zzcbb = displayMetrics.widthPixels;
                    this.zzcbc = displayMetrics.heightPixels;
                }
            }
        }

        public zza(Context context, zziv zziv) {
            PackageManager packageManager = context.getPackageManager();
            zzv(context);
            zza(context, packageManager);
            zzw(context);
            zzx(context);
            this.zzcge = zziv.zzcge;
            this.zzcgf = zziv.zzcgf;
            this.zzcgh = zziv.zzcgh;
            this.zzcgi = zziv.zzcgi;
            this.zzcgj = zziv.zzcgj;
            this.zzcgm = zziv.zzcgm;
            this.zzcgn = zziv.zzcgn;
            this.zzcbd = zziv.zzcbd;
            this.zzcbb = zziv.zzcbb;
            this.zzcbc = zziv.zzcbc;
        }

        private static ResolveInfo zza(PackageManager packageManager, String str) {
            return packageManager.resolveActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)), 65536);
        }

        private static String zza(PackageManager packageManager) {
            ResolveInfo zza = zza(packageManager, "market://details?id=com.google.android.gms.ads");
            if (zza == null) {
                return null;
            }
            ActivityInfo activityInfo = zza.activityInfo;
            if (activityInfo == null) {
                return null;
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(activityInfo.packageName, 0);
                if (packageInfo == null) {
                    return null;
                }
                int i = packageInfo.versionCode;
                String valueOf = String.valueOf(activityInfo.packageName);
                StringBuilder sb = new StringBuilder(12 + String.valueOf(valueOf).length());
                sb.append(i);
                sb.append(".");
                sb.append(valueOf);
                return sb.toString();
            } catch (NameNotFoundException unused) {
                return null;
            }
        }

        @TargetApi(16)
        private void zza(Context context, PackageManager packageManager) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            this.zzcgg = telephonyManager.getNetworkOperator();
            this.zzcgq = telephonyManager.getNetworkType();
            this.zzcgr = telephonyManager.getPhoneType();
            this.zzcgp = -2;
            this.zzcgw = false;
            this.zzcgx = -1;
            if (zzu.zzfq().zza(packageManager, context.getPackageName(), "android.permission.ACCESS_NETWORK_STATE")) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null) {
                    this.zzcgp = activeNetworkInfo.getType();
                    this.zzcgx = activeNetworkInfo.getDetailedState().ordinal();
                } else {
                    this.zzcgp = -1;
                }
                if (VERSION.SDK_INT >= 16) {
                    this.zzcgw = connectivityManager.isActiveNetworkMetered();
                }
            }
        }

        private void zzv(Context context) {
            AudioManager zzak = zzu.zzfq().zzak(context);
            if (zzak != null) {
                try {
                    this.zzcgd = zzak.getMode();
                    this.zzcgk = zzak.isMusicActive();
                    this.zzcgl = zzak.isSpeakerphoneOn();
                    this.zzcgo = zzak.getStreamVolume(3);
                    this.zzcgs = zzak.getRingerMode();
                    this.zzcgt = zzak.getStreamVolume(2);
                    return;
                } catch (Throwable th) {
                    zzu.zzft().zzb(th, true);
                }
            }
            this.zzcgd = -2;
            this.zzcgk = false;
            this.zzcgl = false;
            this.zzcgo = 0;
            this.zzcgs = 0;
            this.zzcgt = 0;
        }

        private void zzw(Context context) {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            boolean z = false;
            if (registerReceiver != null) {
                int intExtra = registerReceiver.getIntExtra("status", -1);
                this.zzcgu = (double) (((float) registerReceiver.getIntExtra(Param.LEVEL, -1)) / ((float) registerReceiver.getIntExtra("scale", -1)));
                if (intExtra == 2 || intExtra == 5) {
                    z = true;
                }
            } else {
                this.zzcgu = -1.0d;
            }
            this.zzcgv = z;
        }

        private void zzx(Context context) {
            this.zzcgy = Build.FINGERPRINT;
            this.zzcgz = zzdq.zzo(context);
        }

        public zziv zzrn() {
            int i = this.zzcgd;
            boolean z = this.zzcge;
            boolean z2 = this.zzcgf;
            String str = this.zzcgg;
            String str2 = this.zzcgh;
            boolean z3 = this.zzcgi;
            boolean z4 = this.zzcgj;
            boolean z5 = this.zzcgk;
            boolean z6 = this.zzcgl;
            String str3 = this.zzcgm;
            String str4 = this.zzcgn;
            int i2 = this.zzcgo;
            int i3 = this.zzcgp;
            int i4 = this.zzcgq;
            int i5 = this.zzcgr;
            int i6 = i4;
            int i7 = this.zzcgs;
            int i8 = this.zzcgt;
            float f = this.zzcbd;
            int i9 = this.zzcbb;
            int i10 = i3;
            int i11 = this.zzcbc;
            double d = this.zzcgu;
            boolean z7 = this.zzcgv;
            boolean z8 = this.zzcgw;
            boolean z9 = z8;
            int i12 = i10;
            int i13 = i7;
            int i14 = i8;
            float f2 = f;
            int i15 = i9;
            int i16 = i11;
            int i17 = i6;
            zziv zziv = new zziv(i, z, z2, str, str2, z3, z4, z5, z6, str3, str4, i2, i12, i17, i5, i13, i14, f2, i15, i16, d, z7, z9, this.zzcgx, this.zzcgy, this.zzcgz);
            return zziv;
        }
    }

    zziv(int i, boolean z, boolean z2, String str, String str2, boolean z3, boolean z4, boolean z5, boolean z6, String str3, String str4, int i2, int i3, int i4, int i5, int i6, int i7, float f, int i8, int i9, double d, boolean z7, boolean z8, int i10, String str5, boolean z9) {
        this.zzcgd = i;
        this.zzcge = z;
        this.zzcgf = z2;
        this.zzcgg = str;
        this.zzcgh = str2;
        this.zzcgi = z3;
        this.zzcgj = z4;
        this.zzcgk = z5;
        this.zzcgl = z6;
        this.zzcgm = str3;
        this.zzcgn = str4;
        this.zzcgo = i2;
        this.zzcgp = i3;
        this.zzcgq = i4;
        this.zzcgr = i5;
        this.zzcgs = i6;
        this.zzcgt = i7;
        this.zzcbd = f;
        this.zzcbb = i8;
        this.zzcbc = i9;
        this.zzcgu = d;
        this.zzcgv = z7;
        this.zzcgw = z8;
        this.zzcgx = i10;
        this.zzcgy = str5;
        this.zzcgz = z9;
    }
}
