package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

@zzin
public class zzkm {
    private final String[] zzclr;
    private final double[] zzcls;
    private final double[] zzclt;
    private final int[] zzclu;
    private int zzclv;

    public static class zza {
        public final int count;
        public final String name;
        public final double zzclw;
        public final double zzclx;
        public final double zzcly;

        public zza(String str, double d, double d2, double d3, int i) {
            this.name = str;
            this.zzclx = d;
            this.zzclw = d2;
            this.zzcly = d3;
            this.count = i;
        }

        public boolean equals(Object obj) {
            boolean z = false;
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (zzaa.equal(this.name, zza.name) && this.zzclw == zza.zzclw && this.zzclx == zza.zzclx && this.count == zza.count && Double.compare(this.zzcly, zza.zzcly) == 0) {
                z = true;
            }
            return z;
        }

        public int hashCode() {
            return zzaa.hashCode(this.name, Double.valueOf(this.zzclw), Double.valueOf(this.zzclx), Double.valueOf(this.zzcly), Integer.valueOf(this.count));
        }

        public String toString() {
            return zzaa.zzx(this).zzg("name", this.name).zzg("minBound", Double.valueOf(this.zzclx)).zzg("maxBound", Double.valueOf(this.zzclw)).zzg("percent", Double.valueOf(this.zzcly)).zzg("count", Integer.valueOf(this.count)).toString();
        }
    }

    public static class zzb {
        /* access modifiers changed from: private */
        public final List<String> zzclz = new ArrayList();
        /* access modifiers changed from: private */
        public final List<Double> zzcma = new ArrayList();
        /* access modifiers changed from: private */
        public final List<Double> zzcmb = new ArrayList();

        public zzb zza(String str, double d, double d2) {
            int i = 0;
            while (i < this.zzclz.size()) {
                double doubleValue = ((Double) this.zzcmb.get(i)).doubleValue();
                double doubleValue2 = ((Double) this.zzcma.get(i)).doubleValue();
                if (d < doubleValue || (doubleValue == d && d2 < doubleValue2)) {
                    break;
                }
                i++;
            }
            this.zzclz.add(i, str);
            this.zzcmb.add(i, Double.valueOf(d));
            this.zzcma.add(i, Double.valueOf(d2));
            return this;
        }

        public zzkm zzto() {
            return new zzkm(this);
        }
    }

    private zzkm(zzb zzb2) {
        int size = zzb2.zzcma.size();
        this.zzclr = (String[]) zzb2.zzclz.toArray(new String[size]);
        this.zzcls = zzm(zzb2.zzcma);
        this.zzclt = zzm(zzb2.zzcmb);
        this.zzclu = new int[size];
        this.zzclv = 0;
    }

    private double[] zzm(List<Double> list) {
        double[] dArr = new double[list.size()];
        for (int i = 0; i < dArr.length; i++) {
            dArr[i] = ((Double) list.get(i)).doubleValue();
        }
        return dArr;
    }

    public List<zza> getBuckets() {
        ArrayList arrayList = new ArrayList(this.zzclr.length);
        for (int i = 0; i < this.zzclr.length; i++) {
            zza zza2 = new zza(this.zzclr[i], this.zzclt[i], this.zzcls[i], ((double) this.zzclu[i]) / ((double) this.zzclv), this.zzclu[i]);
            arrayList.add(zza2);
        }
        return arrayList;
    }

    public void zza(double d) {
        this.zzclv++;
        int i = 0;
        while (i < this.zzclt.length) {
            if (this.zzclt[i] <= d && d < this.zzcls[i]) {
                int[] iArr = this.zzclu;
                iArr[i] = iArr[i] + 1;
            }
            if (d >= this.zzclt[i]) {
                i++;
            } else {
                return;
            }
        }
    }
}
