package com.google.android.gms.internal;

import android.app.Activity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.CancellationException;

public class zzqp extends zzpn {
    private TaskCompletionSource<Void> sA = new TaskCompletionSource<>();

    private zzqp(zzqk zzqk) {
        super(zzqk);
        this.vm.zza("GmsAvailabilityHelper", (zzqj) this);
    }

    public static zzqp zzu(Activity activity) {
        zzqk zzs = zzs(activity);
        zzqp zzqp = (zzqp) zzs.zza("GmsAvailabilityHelper", zzqp.class);
        if (zzqp == null) {
            return new zzqp(zzs);
        }
        if (zzqp.sA.getTask().isComplete()) {
            zzqp.sA = new TaskCompletionSource<>();
        }
        return zzqp;
    }

    public Task<Void> getTask() {
        return this.sA.getTask();
    }

    public void onStop() {
        super.onStop();
        this.sA.setException(new CancellationException());
    }

    /* access modifiers changed from: protected */
    public void zza(ConnectionResult connectionResult, int i) {
        this.sA.setException(new Exception());
    }

    /* access modifiers changed from: protected */
    public void zzaoo() {
        int isGooglePlayServicesAvailable = this.sh.isGooglePlayServicesAvailable(this.vm.zzaqt());
        if (isGooglePlayServicesAvailable == 0) {
            this.sA.setResult(null);
        } else {
            zzk(new ConnectionResult(isGooglePlayServicesAvailable, null));
        }
    }

    public void zzk(ConnectionResult connectionResult) {
        zzb(connectionResult, 0);
    }
}
