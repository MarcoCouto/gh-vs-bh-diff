package com.google.android.gms.internal;

import java.io.IOException;

public interface zzad {

    public static final class zza extends zzapp<zza> {
        public String stackTrace;
        public String zzck;
        public Long zzcl;
        public String zzcm;
        public String zzcn;
        public Long zzco;
        public Long zzcp;
        public String zzcq;
        public Long zzcr;
        public String zzcs;

        public zza() {
            this.zzck = null;
            this.zzcl = null;
            this.stackTrace = null;
            this.zzcm = null;
            this.zzcn = null;
            this.zzco = null;
            this.zzcp = null;
            this.zzcq = null;
            this.zzcr = null;
            this.zzcs = null;
            this.bjG = -1;
        }

        /* renamed from: zza */
        public zza zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        return this;
                    case 10:
                        this.zzck = zzapn.readString();
                        break;
                    case 16:
                        this.zzcl = Long.valueOf(zzapn.ak());
                        break;
                    case 26:
                        this.stackTrace = zzapn.readString();
                        break;
                    case 34:
                        this.zzcm = zzapn.readString();
                        break;
                    case 42:
                        this.zzcn = zzapn.readString();
                        break;
                    case 48:
                        this.zzco = Long.valueOf(zzapn.ak());
                        break;
                    case 56:
                        this.zzcp = Long.valueOf(zzapn.ak());
                        break;
                    case 66:
                        this.zzcq = zzapn.readString();
                        break;
                    case 72:
                        this.zzcr = Long.valueOf(zzapn.ak());
                        break;
                    case 82:
                        this.zzcs = zzapn.readString();
                        break;
                    default:
                        if (super.zza(zzapn, ah)) {
                            break;
                        } else {
                            return this;
                        }
                }
            }
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzck != null) {
                zzapo.zzr(1, this.zzck);
            }
            if (this.zzcl != null) {
                zzapo.zzb(2, this.zzcl.longValue());
            }
            if (this.stackTrace != null) {
                zzapo.zzr(3, this.stackTrace);
            }
            if (this.zzcm != null) {
                zzapo.zzr(4, this.zzcm);
            }
            if (this.zzcn != null) {
                zzapo.zzr(5, this.zzcn);
            }
            if (this.zzco != null) {
                zzapo.zzb(6, this.zzco.longValue());
            }
            if (this.zzcp != null) {
                zzapo.zzb(7, this.zzcp.longValue());
            }
            if (this.zzcq != null) {
                zzapo.zzr(8, this.zzcq);
            }
            if (this.zzcr != null) {
                zzapo.zzb(9, this.zzcr.longValue());
            }
            if (this.zzcs != null) {
                zzapo.zzr(10, this.zzcs);
            }
            super.zza(zzapo);
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzck != null) {
                zzx += zzapo.zzs(1, this.zzck);
            }
            if (this.zzcl != null) {
                zzx += zzapo.zze(2, this.zzcl.longValue());
            }
            if (this.stackTrace != null) {
                zzx += zzapo.zzs(3, this.stackTrace);
            }
            if (this.zzcm != null) {
                zzx += zzapo.zzs(4, this.zzcm);
            }
            if (this.zzcn != null) {
                zzx += zzapo.zzs(5, this.zzcn);
            }
            if (this.zzco != null) {
                zzx += zzapo.zze(6, this.zzco.longValue());
            }
            if (this.zzcp != null) {
                zzx += zzapo.zze(7, this.zzcp.longValue());
            }
            if (this.zzcq != null) {
                zzx += zzapo.zzs(8, this.zzcq);
            }
            if (this.zzcr != null) {
                zzx += zzapo.zze(9, this.zzcr.longValue());
            }
            return this.zzcs != null ? zzx + zzapo.zzs(10, this.zzcs) : zzx;
        }
    }
}
