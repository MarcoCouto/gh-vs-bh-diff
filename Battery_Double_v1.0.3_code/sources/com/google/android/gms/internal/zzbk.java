package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzbk extends zzbp {
    private long zzahc = -1;

    public zzbk(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        this.zzaha.zzdd = Long.valueOf(-1);
        if (this.zzahc == -1) {
            this.zzahc = (long) ((Integer) this.zzahh.invoke(null, new Object[]{this.zzaey.getContext()})).intValue();
        }
        synchronized (this.zzaha) {
            this.zzaha.zzdd = Long.valueOf(this.zzahc);
        }
    }
}
