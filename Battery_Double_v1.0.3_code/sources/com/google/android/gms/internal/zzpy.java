package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.google.android.gms.auth.api.signin.internal.zzk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.internal.zzl;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;

public final class zzpy extends GoogleApiClient implements com.google.android.gms.internal.zzqh.zza {
    /* access modifiers changed from: private */
    public final Context mContext;
    private final int sf;
    private final GoogleApiAvailability sh;
    final com.google.android.gms.common.api.Api.zza<? extends zzvu, zzvv> si;
    final zzg tN;
    final Map<Api<?>, Integer> tO;
    private final Lock tr;
    private final zzl ua;
    private zzqh ub = null;
    final Queue<com.google.android.gms.internal.zzpm.zza<?, ?>> uc = new LinkedList();
    private volatile boolean ud;
    private long ue = 120000;
    private long uf = 5000;
    private final zza ug;
    zzqe uh;
    final Map<zzc<?>, zze> ui;
    Set<Scope> uj = new HashSet();
    private final zzqo uk = new zzqo();
    private final ArrayList<zzpp> ul;
    private Integer um = null;
    Set<zzqx> un = null;
    final zzqy uo;
    private final com.google.android.gms.common.internal.zzl.zza up = new com.google.android.gms.common.internal.zzl.zza() {
        public boolean isConnected() {
            return zzpy.this.isConnected();
        }

        public Bundle zzamh() {
            return null;
        }
    };
    private final Looper zzahv;

    final class zza extends Handler {
        zza(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    zzpy.this.zzapu();
                    return;
                case 2:
                    zzpy.this.resume();
                    return;
                default:
                    int i = message.what;
                    StringBuilder sb = new StringBuilder(31);
                    sb.append("Unknown message id: ");
                    sb.append(i);
                    Log.w("GoogleApiClientImpl", sb.toString());
                    return;
            }
        }
    }

    static class zzb extends com.google.android.gms.internal.zzqe.zza {
        private WeakReference<zzpy> uu;

        zzb(zzpy zzpy) {
            this.uu = new WeakReference<>(zzpy);
        }

        public void zzaou() {
            zzpy zzpy = (zzpy) this.uu.get();
            if (zzpy != null) {
                zzpy.resume();
            }
        }
    }

    public zzpy(Context context, Lock lock, Looper looper, zzg zzg, GoogleApiAvailability googleApiAvailability, com.google.android.gms.common.api.Api.zza<? extends zzvu, zzvv> zza2, Map<Api<?>, Integer> map, List<ConnectionCallbacks> list, List<OnConnectionFailedListener> list2, Map<zzc<?>, zze> map2, int i, int i2, ArrayList<zzpp> arrayList) {
        Looper looper2 = looper;
        this.mContext = context;
        this.tr = lock;
        this.ua = new zzl(looper2, this.up);
        this.zzahv = looper2;
        this.ug = new zza(looper2);
        this.sh = googleApiAvailability;
        this.sf = i;
        if (this.sf >= 0) {
            this.um = Integer.valueOf(i2);
        }
        this.tO = map;
        this.ui = map2;
        this.ul = arrayList;
        this.uo = new zzqy(this.ui);
        for (ConnectionCallbacks registerConnectionCallbacks : list) {
            this.ua.registerConnectionCallbacks(registerConnectionCallbacks);
        }
        for (OnConnectionFailedListener registerConnectionFailedListener : list2) {
            this.ua.registerConnectionFailedListener(registerConnectionFailedListener);
        }
        this.tN = zzg;
        this.si = zza2;
    }

    /* access modifiers changed from: private */
    public void resume() {
        this.tr.lock();
        try {
            if (isResuming()) {
                zzapt();
            }
        } finally {
            this.tr.unlock();
        }
    }

    public static int zza(Iterable<zze> iterable, boolean z) {
        boolean z2 = false;
        boolean z3 = false;
        for (zze zze : iterable) {
            if (zze.zzafk()) {
                z2 = true;
            }
            if (zze.zzafz()) {
                z3 = true;
            }
        }
        if (z2) {
            return (!z3 || !z) ? 1 : 2;
        }
        return 3;
    }

    /* access modifiers changed from: private */
    public void zza(final GoogleApiClient googleApiClient, final zzqu zzqu, final boolean z) {
        zzre.zt.zzg(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            /* renamed from: zzp */
            public void onResult(@NonNull Status status) {
                zzk.zzbc(zzpy.this.mContext).zzagl();
                if (status.isSuccess() && zzpy.this.isConnected()) {
                    zzpy.this.reconnect();
                }
                zzqu.zzc(status);
                if (z) {
                    googleApiClient.disconnect();
                }
            }
        });
    }

    private void zzapt() {
        this.ua.zzasx();
        this.ub.connect();
    }

    /* access modifiers changed from: private */
    public void zzapu() {
        this.tr.lock();
        try {
            if (zzapw()) {
                zzapt();
            }
        } finally {
            this.tr.unlock();
        }
    }

    private void zzb(@NonNull zzqi zzqi) {
        if (this.sf >= 0) {
            zzpk.zza(zzqi).zzfh(this.sf);
            return;
        }
        throw new IllegalStateException("Called stopAutoManage but automatic lifecycle management is not enabled.");
    }

    /* JADX WARNING: type inference failed for: r14v11, types: [com.google.android.gms.internal.zzpq] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    private void zzfk(int i) {
        zzqa zzqa;
        if (this.um == null) {
            this.um = Integer.valueOf(i);
        } else if (this.um.intValue() != i) {
            String valueOf = String.valueOf(zzfl(i));
            String valueOf2 = String.valueOf(zzfl(this.um.intValue()));
            StringBuilder sb = new StringBuilder(51 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(valueOf);
            sb.append(". Mode was already set to ");
            sb.append(valueOf2);
            throw new IllegalStateException(sb.toString());
        }
        if (this.ub == null) {
            boolean z = false;
            boolean z2 = false;
            for (zze zze : this.ui.values()) {
                if (zze.zzafk()) {
                    z = true;
                }
                if (zze.zzafz()) {
                    z2 = true;
                }
            }
            switch (this.um.intValue()) {
                case 1:
                    if (!z) {
                        throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
                    } else if (z2) {
                        throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
                    }
                case 2:
                    if (z) {
                        zzqa = zzpq.zza(this.mContext, this, this.tr, this.zzahv, this.sh, this.ui, this.tN, this.tO, this.si, this.ul);
                        break;
                    }
            }
            zzqa zzqa2 = new zzqa(this.mContext, this, this.tr, this.zzahv, this.sh, this.ui, this.tN, this.tO, this.si, this.ul, this);
            zzqa = zzqa2;
            this.ub = zzqa;
        }
    }

    static String zzfl(int i) {
        switch (i) {
            case 1:
                return "SIGN_IN_MODE_REQUIRED";
            case 2:
                return "SIGN_IN_MODE_OPTIONAL";
            case 3:
                return "SIGN_IN_MODE_NONE";
            default:
                return "UNKNOWN";
        }
    }

    public ConnectionResult blockingConnect() {
        boolean z = true;
        zzab.zza(Looper.myLooper() != Looper.getMainLooper(), (Object) "blockingConnect must not be called on the UI thread");
        this.tr.lock();
        try {
            if (this.sf >= 0) {
                if (this.um == null) {
                    z = false;
                }
                zzab.zza(z, (Object) "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.um == null) {
                this.um = Integer.valueOf(zza(this.ui.values(), false));
            } else if (this.um.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            zzfk(this.um.intValue());
            this.ua.zzasx();
            return this.ub.blockingConnect();
        } finally {
            this.tr.unlock();
        }
    }

    public ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit) {
        zzab.zza(Looper.myLooper() != Looper.getMainLooper(), (Object) "blockingConnect must not be called on the UI thread");
        zzab.zzb(timeUnit, (Object) "TimeUnit must not be null");
        this.tr.lock();
        try {
            if (this.um == null) {
                this.um = Integer.valueOf(zza(this.ui.values(), false));
            } else if (this.um.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            zzfk(this.um.intValue());
            this.ua.zzasx();
            return this.ub.blockingConnect(j, timeUnit);
        } finally {
            this.tr.unlock();
        }
    }

    public PendingResult<Status> clearDefaultAccountAndReconnect() {
        zzab.zza(isConnected(), (Object) "GoogleApiClient is not connected yet.");
        zzab.zza(this.um.intValue() != 2, (Object) "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        final zzqu zzqu = new zzqu((GoogleApiClient) this);
        if (this.ui.containsKey(zzre.bJ)) {
            zza(this, zzqu, false);
            return zzqu;
        }
        final AtomicReference atomicReference = new AtomicReference();
        GoogleApiClient build = new Builder(this.mContext).addApi(zzre.API).addConnectionCallbacks(new ConnectionCallbacks() {
            public void onConnected(Bundle bundle) {
                zzpy.this.zza((GoogleApiClient) atomicReference.get(), zzqu, true);
            }

            public void onConnectionSuspended(int i) {
            }
        }).addOnConnectionFailedListener(new OnConnectionFailedListener() {
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                zzqu.zzc(new Status(8));
            }
        }).setHandler(this.ug).build();
        atomicReference.set(build);
        build.connect();
        return zzqu;
    }

    public void connect() {
        this.tr.lock();
        try {
            boolean z = false;
            if (this.sf >= 0) {
                if (this.um != null) {
                    z = true;
                }
                zzab.zza(z, (Object) "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.um == null) {
                this.um = Integer.valueOf(zza(this.ui.values(), false));
            } else if (this.um.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            connect(this.um.intValue());
        } finally {
            this.tr.unlock();
        }
    }

    public void connect(int i) {
        this.tr.lock();
        boolean z = true;
        if (!(i == 3 || i == 1 || i == 2)) {
            z = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i);
            zzab.zzb(z, (Object) sb.toString());
            zzfk(i);
            zzapt();
        } finally {
            this.tr.unlock();
        }
    }

    public void disconnect() {
        this.tr.lock();
        try {
            this.uo.release();
            if (this.ub != null) {
                this.ub.disconnect();
            }
            this.uk.release();
            for (com.google.android.gms.internal.zzpm.zza zza2 : this.uc) {
                zza2.zza((zzb) null);
                zza2.cancel();
            }
            this.uc.clear();
            if (this.ub != null) {
                zzapw();
                this.ua.zzasw();
                this.tr.unlock();
            }
        } finally {
            this.tr.unlock();
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("mContext=").println(this.mContext);
        printWriter.append(str).append("mResuming=").print(this.ud);
        printWriter.append(" mWorkQueue.size()=").print(this.uc.size());
        this.uo.dump(printWriter);
        if (this.ub != null) {
            this.ub.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    @NonNull
    public ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        ConnectionResult connectionResult;
        this.tr.lock();
        try {
            if (!isConnected() && !isResuming()) {
                throw new IllegalStateException("Cannot invoke getConnectionResult unless GoogleApiClient is connected");
            } else if (this.ui.containsKey(api.zzans())) {
                ConnectionResult connectionResult2 = this.ub.getConnectionResult(api);
                if (connectionResult2 == null) {
                    if (isResuming()) {
                        connectionResult = ConnectionResult.rb;
                    } else {
                        Log.w("GoogleApiClientImpl", zzapy());
                        Log.wtf("GoogleApiClientImpl", String.valueOf(api.getName()).concat(" requested in getConnectionResult is not connected but is not present in the failed  connections map"), new Exception());
                        connectionResult = new ConnectionResult(8, null);
                    }
                    return connectionResult;
                }
                this.tr.unlock();
                return connectionResult2;
            } else {
                throw new IllegalArgumentException(String.valueOf(api.getName()).concat(" was never registered with GoogleApiClient"));
            }
        } finally {
            this.tr.unlock();
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    public Looper getLooper() {
        return this.zzahv;
    }

    public int getSessionId() {
        return System.identityHashCode(this);
    }

    public boolean hasConnectedApi(@NonNull Api<?> api) {
        zze zze = (zze) this.ui.get(api.zzans());
        return zze != null && zze.isConnected();
    }

    public boolean isConnected() {
        return this.ub != null && this.ub.isConnected();
    }

    public boolean isConnecting() {
        return this.ub != null && this.ub.isConnecting();
    }

    public boolean isConnectionCallbacksRegistered(@NonNull ConnectionCallbacks connectionCallbacks) {
        return this.ua.isConnectionCallbacksRegistered(connectionCallbacks);
    }

    public boolean isConnectionFailedListenerRegistered(@NonNull OnConnectionFailedListener onConnectionFailedListener) {
        return this.ua.isConnectionFailedListenerRegistered(onConnectionFailedListener);
    }

    /* access modifiers changed from: 0000 */
    public boolean isResuming() {
        return this.ud;
    }

    public void reconnect() {
        disconnect();
        connect();
    }

    public void registerConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks) {
        this.ua.registerConnectionCallbacks(connectionCallbacks);
    }

    public void registerConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener) {
        this.ua.registerConnectionFailedListener(onConnectionFailedListener);
    }

    public void stopAutoManage(@NonNull FragmentActivity fragmentActivity) {
        zzb(new zzqi(fragmentActivity));
    }

    public void unregisterConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks) {
        this.ua.unregisterConnectionCallbacks(connectionCallbacks);
    }

    public void unregisterConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener) {
        this.ua.unregisterConnectionFailedListener(onConnectionFailedListener);
    }

    @NonNull
    public <C extends zze> C zza(@NonNull zzc<C> zzc) {
        C c = (zze) this.ui.get(zzc);
        zzab.zzb(c, (Object) "Appropriate Api was not requested.");
        return c;
    }

    public void zza(zzqx zzqx) {
        this.tr.lock();
        try {
            if (this.un == null) {
                this.un = new HashSet();
            }
            this.un.add(zzqx);
        } finally {
            this.tr.unlock();
        }
    }

    public boolean zza(@NonNull Api<?> api) {
        return this.ui.containsKey(api.zzans());
    }

    public boolean zza(zzqt zzqt) {
        return this.ub != null && this.ub.zza(zzqt);
    }

    public void zzaof() {
        if (this.ub != null) {
            this.ub.zzaof();
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzapv() {
        if (!isResuming()) {
            this.ud = true;
            if (this.uh == null) {
                this.uh = this.sh.zza(this.mContext.getApplicationContext(), (com.google.android.gms.internal.zzqe.zza) new zzb(this));
            }
            this.ug.sendMessageDelayed(this.ug.obtainMessage(1), this.ue);
            this.ug.sendMessageDelayed(this.ug.obtainMessage(2), this.uf);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean zzapw() {
        if (!isResuming()) {
            return false;
        }
        this.ud = false;
        this.ug.removeMessages(2);
        this.ug.removeMessages(1);
        if (this.uh != null) {
            this.uh.unregister();
            this.uh = null;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: 0000 */
    public boolean zzapx() {
        this.tr.lock();
        try {
            if (this.un == null) {
                this.tr.unlock();
                return false;
            }
            boolean z = !this.un.isEmpty();
            this.tr.unlock();
            return z;
        } catch (Throwable th) {
            this.tr.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public String zzapy() {
        StringWriter stringWriter = new StringWriter();
        dump("", null, new PrintWriter(stringWriter), null);
        return stringWriter.toString();
    }

    /* access modifiers changed from: 0000 */
    public <C extends zze> C zzb(zzc<?> zzc) {
        C c = (zze) this.ui.get(zzc);
        zzab.zzb(c, (Object) "Appropriate Api was not requested.");
        return c;
    }

    public void zzb(zzqx zzqx) {
        String str;
        String str2;
        Exception exc;
        this.tr.lock();
        try {
            if (this.un == null) {
                str = "GoogleApiClientImpl";
                str2 = "Attempted to remove pending transform when no transforms are registered.";
                exc = new Exception();
            } else if (!this.un.remove(zzqx)) {
                str = "GoogleApiClientImpl";
                str2 = "Failed to remove pending transform - this may lead to memory leaks!";
                exc = new Exception();
            } else {
                if (!zzapx()) {
                    this.ub.zzapb();
                }
            }
            Log.wtf(str, str2, exc);
        } finally {
            this.tr.unlock();
        }
    }

    public <A extends com.google.android.gms.common.api.Api.zzb, R extends Result, T extends com.google.android.gms.internal.zzpm.zza<R, A>> T zzc(@NonNull T t) {
        zzab.zzb(t.zzans() != null, (Object) "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.ui.containsKey(t.zzans());
        String name = t.zzanz() != null ? t.zzanz().getName() : "the API";
        StringBuilder sb = new StringBuilder(65 + String.valueOf(name).length());
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(name);
        sb.append(" required for this call.");
        zzab.zzb(containsKey, (Object) sb.toString());
        this.tr.lock();
        try {
            if (this.ub == null) {
                this.uc.add(t);
            } else {
                t = this.ub.zzc(t);
            }
            return t;
        } finally {
            this.tr.unlock();
        }
    }

    public void zzc(int i, boolean z) {
        if (i == 1 && !z) {
            zzapv();
        }
        this.uo.zzaqz();
        this.ua.zzgf(i);
        this.ua.zzasw();
        if (i == 2) {
            zzapt();
        }
    }

    public <A extends com.google.android.gms.common.api.Api.zzb, T extends com.google.android.gms.internal.zzpm.zza<? extends Result, A>> T zzd(@NonNull T t) {
        zzab.zzb(t.zzans() != null, (Object) "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.ui.containsKey(t.zzans());
        String name = t.zzanz() != null ? t.zzanz().getName() : "the API";
        StringBuilder sb = new StringBuilder(65 + String.valueOf(name).length());
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(name);
        sb.append(" required for this call.");
        zzab.zzb(containsKey, (Object) sb.toString());
        this.tr.lock();
        try {
            if (this.ub == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            }
            if (isResuming()) {
                this.uc.add(t);
                while (!this.uc.isEmpty()) {
                    com.google.android.gms.internal.zzpm.zza zza2 = (com.google.android.gms.internal.zzpm.zza) this.uc.remove();
                    this.uo.zzg(zza2);
                    zza2.zzz(Status.ss);
                }
            } else {
                t = this.ub.zzd(t);
            }
            return t;
        } finally {
            this.tr.unlock();
        }
    }

    public void zzd(ConnectionResult connectionResult) {
        if (!this.sh.zzc(this.mContext, connectionResult.getErrorCode())) {
            zzapw();
        }
        if (!isResuming()) {
            this.ua.zzm(connectionResult);
            this.ua.zzasw();
        }
    }

    public void zzm(Bundle bundle) {
        while (!this.uc.isEmpty()) {
            zzd((T) (com.google.android.gms.internal.zzpm.zza) this.uc.remove());
        }
        this.ua.zzo(bundle);
    }

    public <L> zzqn<L> zzs(@NonNull L l) {
        this.tr.lock();
        try {
            return this.uk.zzb(l, this.zzahv);
        } finally {
            this.tr.unlock();
        }
    }
}
