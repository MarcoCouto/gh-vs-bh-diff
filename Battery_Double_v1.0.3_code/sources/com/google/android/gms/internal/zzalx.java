package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;

public class zzalx {
    private boolean bbh;
    private int bbp;
    private long bbs;
    private Map<String, zzals> bbt;

    public zzalx() {
        this(-1);
    }

    public zzalx(int i, long j, Map<String, zzals> map, boolean z) {
        this.bbp = i;
        this.bbs = j;
        if (map == null) {
            map = new HashMap<>();
        }
        this.bbt = map;
        this.bbh = z;
    }

    public zzalx(long j) {
        this(0, j, null, false);
    }

    public int getLastFetchStatus() {
        return this.bbp;
    }

    public boolean isDeveloperModeEnabled() {
        return this.bbh;
    }

    public void zza(String str, zzals zzals) {
        this.bbt.put(str, zzals);
    }

    public void zzafe(int i) {
        this.bbp = i;
    }

    public void zzcd(Map<String, zzals> map) {
        if (map == null) {
            map = new HashMap<>();
        }
        this.bbt = map;
    }

    public void zzcp(long j) {
        this.bbs = j;
    }

    public void zzcw(boolean z) {
        this.bbh = z;
    }

    public Map<String, zzals> zzcxk() {
        return this.bbt;
    }

    public long zzcxl() {
        return this.bbs;
    }

    public void zztd(String str) {
        if (this.bbt.get(str) != null) {
            this.bbt.remove(str);
        }
    }
}
