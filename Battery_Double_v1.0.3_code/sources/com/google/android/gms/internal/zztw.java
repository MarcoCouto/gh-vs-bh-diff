package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.internal.zztv.zzd;
import java.util.ArrayList;
import java.util.Collection;

public class zztw {
    private final Collection<zztv> zzaxr = new ArrayList();
    private final Collection<zzd> zzaxs = new ArrayList();
    private final Collection<zzd> zzaxt = new ArrayList();

    public static void initialize(Context context) {
        zztz.zzbeu().initialize(context);
    }

    public void zza(zztv zztv) {
        this.zzaxr.add(zztv);
    }
}
