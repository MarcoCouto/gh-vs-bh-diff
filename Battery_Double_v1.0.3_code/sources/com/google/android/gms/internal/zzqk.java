package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

public interface zzqk {
    void startActivityForResult(Intent intent, int i);

    <T extends zzqj> T zza(String str, Class<T> cls);

    void zza(String str, @NonNull zzqj zzqj);

    Activity zzaqt();
}
