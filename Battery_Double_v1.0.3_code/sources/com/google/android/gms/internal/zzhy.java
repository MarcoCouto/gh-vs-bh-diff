package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzli.zza;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

@zzin
public abstract class zzhy implements zzkj<Void>, zza {
    protected final Context mContext;
    protected final zzlh zzbgf;
    protected final zzic.zza zzbxq;
    protected final zzju.zza zzbxr;
    protected AdResponseParcel zzbxs;
    private Runnable zzbxt;
    protected final Object zzbxu = new Object();
    /* access modifiers changed from: private */
    public AtomicBoolean zzbxv = new AtomicBoolean(true);

    protected zzhy(Context context, zzju.zza zza, zzlh zzlh, zzic.zza zza2) {
        this.mContext = context;
        this.zzbxr = zza;
        this.zzbxs = this.zzbxr.zzciq;
        this.zzbgf = zzlh;
        this.zzbxq = zza2;
    }

    private zzju zzak(int i) {
        AdRequestInfoParcel adRequestInfoParcel = this.zzbxr.zzcip;
        AdRequestParcel adRequestParcel = adRequestInfoParcel.zzcar;
        zzlh zzlh = this.zzbgf;
        List<String> list = this.zzbxs.zzbnm;
        List<String> list2 = this.zzbxs.zzbnn;
        List<String> list3 = this.zzbxs.zzcca;
        int i2 = this.zzbxs.orientation;
        long j = this.zzbxs.zzbns;
        String str = adRequestInfoParcel.zzcau;
        boolean z = this.zzbxs.zzcby;
        long j2 = this.zzbxs.zzcbz;
        AdSizeParcel adSizeParcel = this.zzbxr.zzapa;
        long j3 = j2;
        long j4 = this.zzbxs.zzcbx;
        long j5 = this.zzbxr.zzcik;
        long j6 = this.zzbxs.zzccc;
        String str2 = this.zzbxs.zzccd;
        JSONObject jSONObject = this.zzbxr.zzcie;
        RewardItemParcel rewardItemParcel = this.zzbxs.zzccn;
        List<String> list4 = this.zzbxs.zzcco;
        List<String> list5 = this.zzbxs.zzccp;
        boolean z2 = this.zzbxs.zzccq;
        String str3 = str2;
        int i3 = i;
        long j7 = j3;
        AdSizeParcel adSizeParcel2 = adSizeParcel;
        long j8 = j4;
        long j9 = j5;
        long j10 = j6;
        zzju zzju = new zzju(adRequestParcel, zzlh, list, i3, list2, list3, i2, j, str, z, null, null, null, null, null, j7, adSizeParcel2, j8, j9, j10, str3, jSONObject, null, rewardItemParcel, list4, list5, z2, this.zzbxs.zzccr, null, this.zzbxs.zzbnp);
        return zzju;
    }

    public void cancel() {
        if (this.zzbxv.getAndSet(false)) {
            this.zzbgf.stopLoading();
            zzu.zzfs().zzi(this.zzbgf);
            zzaj(-1);
            zzkh.zzclc.removeCallbacks(this.zzbxt);
        }
    }

    public void zza(zzlh zzlh, boolean z) {
        zzkd.zzcv("WebView finished loading.");
        int i = 0;
        if (this.zzbxv.getAndSet(false)) {
            if (z) {
                i = zzpx();
            }
            zzaj(i);
            zzkh.zzclc.removeCallbacks(this.zzbxt);
        }
    }

    /* access modifiers changed from: protected */
    public void zzaj(int i) {
        if (i != -2) {
            this.zzbxs = new AdResponseParcel(i, this.zzbxs.zzbns);
        }
        this.zzbgf.zzud();
        this.zzbxq.zzb(zzak(i));
    }

    /* renamed from: zzpv */
    public final Void zzpy() {
        zzab.zzhi("Webview render task needs to be called on UI thread.");
        this.zzbxt = new Runnable() {
            public void run() {
                if (zzhy.this.zzbxv.get()) {
                    zzkd.e("Timed out waiting for WebView to finish loading.");
                    zzhy.this.cancel();
                }
            }
        };
        zzkh.zzclc.postDelayed(this.zzbxt, ((Long) zzdc.zzbbh.get()).longValue());
        zzpw();
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract void zzpw();

    /* access modifiers changed from: protected */
    public int zzpx() {
        return -2;
    }
}
