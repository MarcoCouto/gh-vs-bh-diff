package com.google.android.gms.internal;

public interface zzani {
    <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol);
}
