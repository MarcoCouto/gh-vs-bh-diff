package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import com.google.android.gms.internal.zzju.zza;

@TargetApi(19)
@zzin
public class zzie extends zzid {
    private Object zzbyj = new Object();
    private PopupWindow zzbyk;
    private boolean zzbyl = false;

    zzie(Context context, zza zza, zzlh zzlh, zzic.zza zza2) {
        super(context, zza, zzlh, zza2);
    }

    private void zzqe() {
        synchronized (this.zzbyj) {
            this.zzbyl = true;
            if ((this.mContext instanceof Activity) && ((Activity) this.mContext).isDestroyed()) {
                this.zzbyk = null;
            }
            if (this.zzbyk != null) {
                if (this.zzbyk.isShowing()) {
                    this.zzbyk.dismiss();
                }
                this.zzbyk = null;
            }
        }
    }

    public void cancel() {
        zzqe();
        super.cancel();
    }

    /* access modifiers changed from: protected */
    public void zzaj(int i) {
        zzqe();
        super.zzaj(i);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:18|19|20|21|22) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0069 */
    public void zzqd() {
        Window window = this.mContext instanceof Activity ? ((Activity) this.mContext).getWindow() : null;
        if (window != null && window.getDecorView() != null && !((Activity) this.mContext).isDestroyed()) {
            FrameLayout frameLayout = new FrameLayout(this.mContext);
            frameLayout.setLayoutParams(new LayoutParams(-1, -1));
            frameLayout.addView(this.zzbgf.getView(), -1, -1);
            synchronized (this.zzbyj) {
                if (!this.zzbyl) {
                    this.zzbyk = new PopupWindow(frameLayout, 1, 1, false);
                    this.zzbyk.setOutsideTouchable(true);
                    this.zzbyk.setClippingEnabled(false);
                    zzkd.zzcv("Displaying the 1x1 popup off the screen.");
                    this.zzbyk.showAtLocation(window.getDecorView(), 0, -1, -1);
                    this.zzbyk = null;
                }
            }
        }
    }
}
