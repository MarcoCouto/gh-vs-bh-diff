package com.google.android.gms.internal;

import com.google.android.gms.internal.zzapp;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class zzapq<M extends zzapp<M>, T> {
    protected final Class<T> baj;
    protected final boolean bjy;
    public final int tag;
    protected final int type;

    private zzapq(int i, Class<T> cls, int i2, boolean z) {
        this.type = i;
        this.baj = cls;
        this.tag = i2;
        this.bjy = z;
    }

    public static <M extends zzapp<M>, T extends zzapv> zzapq<M, T> zza(int i, Class<T> cls, long j) {
        return new zzapq<>(i, cls, (int) j, false);
    }

    private T zzaw(List<zzapx> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            zzapx zzapx = (zzapx) list.get(i);
            if (zzapx.apf.length != 0) {
                zza(zzapx, (List<Object>) arrayList);
            }
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        T cast = this.baj.cast(Array.newInstance(this.baj.getComponentType(), size));
        for (int i2 = 0; i2 < size; i2++) {
            Array.set(cast, i2, arrayList.get(i2));
        }
        return cast;
    }

    private T zzax(List<zzapx> list) {
        if (list.isEmpty()) {
            return null;
        }
        return this.baj.cast(zzcg(zzapn.zzbd(((zzapx) list.get(list.size() - 1)).apf)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzapq)) {
            return false;
        }
        zzapq zzapq = (zzapq) obj;
        return this.type == zzapq.type && this.baj == zzapq.baj && this.tag == zzapq.tag && this.bjy == zzapq.bjy;
    }

    public int hashCode() {
        return (31 * (((((1147 + this.type) * 31) + this.baj.hashCode()) * 31) + this.tag)) + (this.bjy ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public void zza(zzapx zzapx, List<Object> list) {
        list.add(zzcg(zzapn.zzbd(zzapx.apf)));
    }

    /* access modifiers changed from: 0000 */
    public void zza(Object obj, zzapo zzapo) throws IOException {
        if (this.bjy) {
            zzc(obj, zzapo);
        } else {
            zzb(obj, zzapo);
        }
    }

    /* access modifiers changed from: 0000 */
    public final T zzav(List<zzapx> list) {
        if (list == null) {
            return null;
        }
        return this.bjy ? zzaw(list) : zzax(list);
    }

    /* access modifiers changed from: protected */
    public void zzb(Object obj, zzapo zzapo) {
        try {
            zzapo.zzagb(this.tag);
            switch (this.type) {
                case 10:
                    zzapv zzapv = (zzapv) obj;
                    int zzagj = zzapy.zzagj(this.tag);
                    zzapo.zzb(zzapv);
                    zzapo.zzai(zzagj, 4);
                    return;
                case 11:
                    zzapo.zzc((zzapv) obj);
                    return;
                default:
                    int i = this.type;
                    StringBuilder sb = new StringBuilder(24);
                    sb.append("Unknown type ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void zzc(Object obj, zzapo zzapo) {
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            Object obj2 = Array.get(obj, i);
            if (obj2 != null) {
                zzb(obj2, zzapo);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Object zzcg(zzapn zzapn) {
        Class<T> componentType = this.bjy ? this.baj.getComponentType() : this.baj;
        try {
            switch (this.type) {
                case 10:
                    zzapv zzapv = (zzapv) componentType.newInstance();
                    zzapn.zza(zzapv, zzapy.zzagj(this.tag));
                    return zzapv;
                case 11:
                    zzapv zzapv2 = (zzapv) componentType.newInstance();
                    zzapn.zza(zzapv2);
                    return zzapv2;
                default:
                    int i = this.type;
                    StringBuilder sb = new StringBuilder(24);
                    sb.append("Unknown type ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
            }
        } catch (InstantiationException e) {
            String valueOf = String.valueOf(componentType);
            StringBuilder sb2 = new StringBuilder(33 + String.valueOf(valueOf).length());
            sb2.append("Error creating instance of class ");
            sb2.append(valueOf);
            throw new IllegalArgumentException(sb2.toString(), e);
        } catch (IllegalAccessException e2) {
            String valueOf2 = String.valueOf(componentType);
            StringBuilder sb3 = new StringBuilder(33 + String.valueOf(valueOf2).length());
            sb3.append("Error creating instance of class ");
            sb3.append(valueOf2);
            throw new IllegalArgumentException(sb3.toString(), e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }

    /* access modifiers changed from: 0000 */
    public int zzcp(Object obj) {
        return this.bjy ? zzcq(obj) : zzcr(obj);
    }

    /* access modifiers changed from: protected */
    public int zzcq(Object obj) {
        int length = Array.getLength(obj);
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (Array.get(obj, i2) != null) {
                i += zzcr(Array.get(obj, i2));
            }
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public int zzcr(Object obj) {
        int zzagj = zzapy.zzagj(this.tag);
        switch (this.type) {
            case 10:
                return zzapo.zzb(zzagj, (zzapv) obj);
            case 11:
                return zzapo.zzc(zzagj, (zzapv) obj);
            default:
                int i = this.type;
                StringBuilder sb = new StringBuilder(24);
                sb.append("Unknown type ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
        }
    }
}
