package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.internal.zzju.zza;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzjl extends zzkc implements zzjk {
    private final Context mContext;
    private final Object zzail = new Object();
    private final zza zzbxr;
    private final ArrayList<Future> zzchw = new ArrayList<>();
    private final ArrayList<String> zzchx = new ArrayList<>();
    private final HashSet<String> zzchy = new HashSet<>();
    /* access modifiers changed from: private */
    public final zzjf zzchz;

    public zzjl(Context context, zza zza, zzjf zzjf) {
        this.mContext = context;
        this.zzbxr = zza;
        this.zzchz = zzjf;
    }

    private zzju zza(int i, @Nullable String str, @Nullable zzfz zzfz) {
        AdRequestParcel adRequestParcel = this.zzbxr.zzcip.zzcar;
        List<String> list = this.zzbxr.zzciq.zzbnm;
        List<String> list2 = this.zzbxr.zzciq.zzbnn;
        List<String> list3 = this.zzbxr.zzciq.zzcca;
        int i2 = this.zzbxr.zzciq.orientation;
        long j = this.zzbxr.zzciq.zzbns;
        String str2 = this.zzbxr.zzcip.zzcau;
        boolean z = this.zzbxr.zzciq.zzcby;
        zzga zzga = this.zzbxr.zzcig;
        long j2 = this.zzbxr.zzciq.zzcbz;
        AdSizeParcel adSizeParcel = this.zzbxr.zzapa;
        long j3 = j2;
        zzga zzga2 = zzga;
        long j4 = this.zzbxr.zzciq.zzcbx;
        long j5 = this.zzbxr.zzcik;
        long j6 = this.zzbxr.zzciq.zzccc;
        String str3 = this.zzbxr.zzciq.zzccd;
        JSONObject jSONObject = this.zzbxr.zzcie;
        RewardItemParcel rewardItemParcel = this.zzbxr.zzciq.zzccn;
        List<String> list4 = this.zzbxr.zzciq.zzcco;
        List<String> list5 = this.zzbxr.zzciq.zzccp;
        boolean z2 = this.zzbxr.zzciq.zzccq;
        JSONObject jSONObject2 = jSONObject;
        AdSizeParcel adSizeParcel2 = adSizeParcel;
        int i3 = i;
        String str4 = str3;
        zzfz zzfz2 = zzfz;
        zzga zzga3 = zzga2;
        String str5 = str;
        long j7 = j3;
        long j8 = j4;
        long j9 = j5;
        long j10 = j6;
        zzju zzju = new zzju(adRequestParcel, null, list, i3, list2, list3, i2, j, str2, z, zzfz2, null, str5, zzga3, null, j7, adSizeParcel2, j8, j9, j10, str4, jSONObject2, null, rewardItemParcel, list4, list5, z2, this.zzbxr.zzciq.zzccr, null, this.zzbxr.zzciq.zzbnp);
        return zzju;
    }

    private zzju zza(String str, zzfz zzfz) {
        return zza(-2, str, zzfz);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        return;
     */
    private void zzd(String str, String str2, String str3) {
        synchronized (this.zzail) {
            zzjm zzcf = this.zzchz.zzcf(str);
            if (!(zzcf == null || zzcf.zzrv() == null)) {
                if (zzcf.zzru() != null) {
                    this.zzchw.add((Future) zza(str, str2, str3, zzcf).zzpy());
                    this.zzchx.add(str);
                }
            }
        }
    }

    private zzju zzrt() {
        return zza(3, null, null);
    }

    public void onStop() {
    }

    /* access modifiers changed from: protected */
    public zzjg zza(String str, String str2, String str3, zzjm zzjm) {
        zzjg zzjg = new zzjg(this.mContext, str, str2, str3, this.zzbxr, zzjm, this);
        return zzjg;
    }

    public void zza(String str, int i) {
    }

    public void zzcg(String str) {
        synchronized (this.zzail) {
            this.zzchy.add(str);
        }
    }

    public void zzew() {
        for (zzfz zzfz : this.zzbxr.zzcig.zzbnk) {
            String str = zzfz.zzbnc;
            for (String str2 : zzfz.zzbmw) {
                if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str2)) {
                    try {
                        str2 = new JSONObject(str).getString("class_name");
                    } catch (JSONException e) {
                        zzkd.zzb("Unable to determine custom event class name, skipping...", e);
                    }
                }
                zzd(str2, str, zzfz.zzbmu);
            }
        }
        int i = 0;
        while (i < this.zzchw.size()) {
            try {
                ((Future) this.zzchw.get(i)).get();
                synchronized (this.zzail) {
                    if (this.zzchy.contains(this.zzchx.get(i))) {
                        final zzju zza = zza((String) this.zzchx.get(i), (zzfz) this.zzbxr.zzcig.zzbnk.get(i));
                        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
                            public void run() {
                                zzjl.this.zzchz.zzb(zza);
                            }
                        });
                        return;
                    }
                }
            } catch (InterruptedException unused) {
            } catch (Exception unused2) {
            }
        }
        final zzju zzrt = zzrt();
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                zzjl.this.zzchz.zzb(zzrt);
            }
        });
        return;
        i++;
    }
}
