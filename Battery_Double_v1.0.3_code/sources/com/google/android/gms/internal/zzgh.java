package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzin
public class zzgh implements zzfy {
    private final Context mContext;
    private final Object zzail = new Object();
    private final zzdk zzajn;
    private final zzgj zzajz;
    private final boolean zzarl;
    private final boolean zzawn;
    private final zzga zzboe;
    private final AdRequestInfoParcel zzbot;
    private final long zzbou;
    private final long zzbov;
    private boolean zzbox = false;
    private List<zzge> zzboz = new ArrayList();
    private zzgd zzbpd;

    public zzgh(Context context, AdRequestInfoParcel adRequestInfoParcel, zzgj zzgj, zzga zzga, boolean z, boolean z2, long j, long j2, zzdk zzdk) {
        this.mContext = context;
        this.zzbot = adRequestInfoParcel;
        this.zzajz = zzgj;
        this.zzboe = zzga;
        this.zzarl = z;
        this.zzawn = z2;
        this.zzbou = j;
        this.zzbov = j2;
        this.zzajn = zzdk;
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.zzbox = true;
            if (this.zzbpd != null) {
                this.zzbpd.cancel();
            }
        }
    }

    public zzge zzd(List<zzfz> list) {
        Object obj;
        zzge zzge;
        zzgh zzgh = this;
        zzkd.zzcv("Starting mediation.");
        ArrayList arrayList = new ArrayList();
        zzdi zzkg = zzgh.zzajn.zzkg();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            zzfz zzfz = (zzfz) it.next();
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(zzfz.zzbmv);
            zzkd.zzcw(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            Iterator it2 = zzfz.zzbmw.iterator();
            while (true) {
                if (it2.hasNext()) {
                    String str2 = (String) it2.next();
                    zzdi zzkg2 = zzgh.zzajn.zzkg();
                    Object obj2 = zzgh.zzail;
                    synchronized (obj2) {
                        try {
                            if (zzgh.zzbox) {
                                zzge = new zzge(-1);
                            } else {
                                Context context = zzgh.mContext;
                                zzgj zzgj = zzgh.zzajz;
                                zzga zzga = zzgh.zzboe;
                                AdRequestParcel adRequestParcel = zzgh.zzbot.zzcar;
                                AdSizeParcel adSizeParcel = zzgh.zzbot.zzapa;
                                Iterator it3 = it;
                                VersionInfoParcel versionInfoParcel = zzgh.zzbot.zzaow;
                                zzdi zzdi = zzkg;
                                boolean z = zzgh.zzarl;
                                ArrayList arrayList2 = arrayList;
                                boolean z2 = zzgh.zzawn;
                                AdRequestParcel adRequestParcel2 = adRequestParcel;
                                List<String> list2 = zzgh.zzbot.zzaps;
                                AdRequestParcel adRequestParcel3 = adRequestParcel2;
                                NativeAdOptionsParcel nativeAdOptionsParcel = zzgh.zzbot.zzapo;
                                r7 = r7;
                                zzga zzga2 = zzga;
                                String str3 = str2;
                                obj = obj2;
                                zzfz zzfz2 = zzfz;
                                zzgd zzgd = r7;
                                zzdi zzdi2 = zzkg2;
                                zzfz zzfz3 = zzfz;
                                String str4 = str2;
                                AdSizeParcel adSizeParcel2 = adSizeParcel;
                                Iterator it4 = it2;
                                try {
                                    zzgd zzgd2 = new zzgd(context, str3, zzgj, zzga2, zzfz2, adRequestParcel3, adSizeParcel2, versionInfoParcel, z, z2, nativeAdOptionsParcel, list2);
                                } catch (Throwable th) {
                                    th = th;
                                    Throwable th2 = th;
                                    throw th2;
                                }
                                try {
                                    this.zzbpd = zzgd;
                                    final zzge zza = this.zzbpd.zza(this.zzbou, this.zzbov);
                                    this.zzboz.add(zza);
                                    if (zza.zzbom == 0) {
                                        zzkd.zzcv("Adapter succeeded.");
                                        this.zzajn.zzh("mediation_network_succeed", str4);
                                        ArrayList arrayList3 = arrayList2;
                                        if (!arrayList3.isEmpty()) {
                                            this.zzajn.zzh("mediation_networks_fail", TextUtils.join(",", arrayList3));
                                        }
                                        this.zzajn.zza(zzdi2, "mls");
                                        this.zzajn.zza(zzdi, "ttm");
                                        return zza;
                                    }
                                    zzdi zzdi3 = zzdi;
                                    ArrayList arrayList4 = arrayList2;
                                    arrayList4.add(str4);
                                    this.zzajn.zza(zzdi2, "mlf");
                                    if (zza.zzboo != null) {
                                        zzkh.zzclc.post(new Runnable() {
                                            public void run() {
                                                try {
                                                    zza.zzboo.destroy();
                                                } catch (RemoteException e) {
                                                    zzkd.zzd("Could not destroy mediation adapter.", e);
                                                }
                                            }
                                        });
                                    }
                                    zzgh = this;
                                    arrayList = arrayList4;
                                    it2 = it4;
                                    zzkg = zzdi3;
                                    it = it3;
                                    zzfz = zzfz3;
                                } catch (Throwable th3) {
                                    th = th3;
                                    Throwable th22 = th;
                                    throw th22;
                                }
                            }
                        } catch (Throwable th4) {
                            th = th4;
                            zzgh zzgh2 = zzgh;
                            obj = obj2;
                            Throwable th222 = th;
                            throw th222;
                        }
                    }
                    return zzge;
                }
            }
        }
        ArrayList arrayList5 = arrayList;
        zzgh zzgh3 = zzgh;
        if (!arrayList5.isEmpty()) {
            zzgh3.zzajn.zzh("mediation_networks_fail", TextUtils.join(",", arrayList5));
        }
        return new zzge(1);
    }

    public List<zzge> zzmg() {
        return this.zzboz;
    }
}
