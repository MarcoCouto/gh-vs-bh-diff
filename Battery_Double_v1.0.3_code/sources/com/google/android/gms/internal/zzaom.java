package com.google.android.gms.internal;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;

public class zzaom implements Closeable {
    private static final char[] bhc = ")]}'\n".toCharArray();
    private boolean bhd = false;
    private final char[] bhe = new char[1024];
    private int bhf = 0;
    private int bhg = 0;
    /* access modifiers changed from: private */
    public int bhh = 0;
    private long bhi;
    private int bhj;
    private String bhk;
    private int[] bhl = new int[32];
    private int bhm = 0;
    private String[] bhn;
    private int[] bho;
    private final Reader in;
    private int limit = 0;
    private int pos = 0;

    static {
        zzanr.beV = new zzanr() {
            public void zzi(zzaom zzaom) throws IOException {
                int i;
                if (zzaom instanceof zzaoc) {
                    ((zzaoc) zzaom).e();
                    return;
                }
                int zzag = zzaom.bhh;
                if (zzag == 0) {
                    zzag = zzaom.o();
                }
                if (zzag == 13) {
                    i = 9;
                } else if (zzag == 12) {
                    i = 8;
                } else if (zzag == 14) {
                    i = 10;
                } else {
                    String valueOf = String.valueOf(zzaom.b());
                    int zzai = zzaom.getLineNumber();
                    int zzaj = zzaom.getColumnNumber();
                    String path = zzaom.getPath();
                    StringBuilder sb = new StringBuilder(70 + String.valueOf(valueOf).length() + String.valueOf(path).length());
                    sb.append("Expected a name but was ");
                    sb.append(valueOf);
                    sb.append(" ");
                    sb.append(" at line ");
                    sb.append(zzai);
                    sb.append(" column ");
                    sb.append(zzaj);
                    sb.append(" path ");
                    sb.append(path);
                    throw new IllegalStateException(sb.toString());
                }
                zzaom.bhh = i;
            }
        };
    }

    public zzaom(Reader reader) {
        int[] iArr = this.bhl;
        int i = this.bhm;
        this.bhm = i + 1;
        iArr[i] = 6;
        this.bhn = new String[32];
        this.bho = new int[32];
        if (reader == null) {
            throw new NullPointerException("in == null");
        }
        this.in = reader;
    }

    /* access modifiers changed from: private */
    public int getColumnNumber() {
        return (this.pos - this.bhg) + 1;
    }

    /* access modifiers changed from: private */
    public int getLineNumber() {
        return this.bhf + 1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0123  */
    public int o() throws IOException {
        int zzdb;
        int i;
        int i2;
        int i3 = this.bhl[this.bhm - 1];
        if (i3 == 1) {
            this.bhl[this.bhm - 1] = 2;
            zzdb = zzdb(true);
            if (zzdb != 34) {
                if (this.bhm == 1) {
                    t();
                }
                i = 9;
            } else if (zzdb != 39) {
                if (!(zzdb == 44 || zzdb == 59)) {
                    if (zzdb == 91) {
                        this.bhh = 3;
                        return 3;
                    } else if (zzdb != 93) {
                        if (zzdb != 123) {
                            this.pos--;
                            if (this.bhm == 1) {
                                t();
                            }
                            int p = p();
                            if (p != 0) {
                                return p;
                            }
                            int q = q();
                            if (q != 0) {
                                return q;
                            }
                            if (!zze(this.bhe[this.pos])) {
                                throw zztu("Expected value");
                            }
                            t();
                            i = 10;
                        } else {
                            this.bhh = 1;
                            return 1;
                        }
                    } else if (i3 == 1) {
                        this.bhh = 4;
                        return 4;
                    }
                }
                if (i3 == 1 || i3 == 2) {
                    t();
                    this.pos--;
                    this.bhh = 7;
                    return 7;
                }
                throw zztu("Unexpected value");
            } else {
                t();
                this.bhh = 8;
                return 8;
            }
        } else {
            if (i3 == 2) {
                int zzdb2 = zzdb(true);
                if (zzdb2 != 44) {
                    if (zzdb2 == 59) {
                        t();
                    } else if (zzdb2 != 93) {
                        throw zztu("Unterminated array");
                    } else {
                        this.bhh = 4;
                        return 4;
                    }
                }
            } else if (i3 == 3 || i3 == 5) {
                this.bhl[this.bhm - 1] = 4;
                if (i3 == 5) {
                    int zzdb3 = zzdb(true);
                    if (zzdb3 != 44) {
                        if (zzdb3 == 59) {
                            t();
                        } else if (zzdb3 != 125) {
                            throw zztu("Unterminated object");
                        } else {
                            this.bhh = 2;
                            return 2;
                        }
                    }
                }
                int zzdb4 = zzdb(true);
                if (zzdb4 == 34) {
                    i = 13;
                } else if (zzdb4 == 39) {
                    t();
                    i = 12;
                } else if (zzdb4 != 125) {
                    t();
                    this.pos--;
                    if (zze((char) zzdb4)) {
                        i = 14;
                    } else {
                        throw zztu("Expected name");
                    }
                } else if (i3 != 5) {
                    this.bhh = 2;
                    return 2;
                } else {
                    throw zztu("Expected name");
                }
            } else {
                if (i3 == 4) {
                    this.bhl[this.bhm - 1] = 5;
                    int zzdb5 = zzdb(true);
                    if (zzdb5 != 58) {
                        if (zzdb5 != 61) {
                            throw zztu("Expected ':'");
                        }
                        t();
                        if ((this.pos < this.limit || zzafm(1)) && this.bhe[this.pos] == '>') {
                            i2 = this.pos + 1;
                        }
                    }
                } else if (i3 == 6) {
                    if (this.bhd) {
                        w();
                    }
                    this.bhl[this.bhm - 1] = 7;
                } else if (i3 == 7) {
                    if (zzdb(false) == -1) {
                        i = 17;
                    } else {
                        t();
                        i2 = this.pos - 1;
                    }
                } else if (i3 == 8) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                this.pos = i2;
            }
            zzdb = zzdb(true);
            if (zzdb != 34) {
            }
        }
        this.bhh = i;
        return i;
    }

    private int p() throws IOException {
        int i;
        String str;
        String str2;
        char c = this.bhe[this.pos];
        if (c == 't' || c == 'T') {
            str2 = "true";
            str = "TRUE";
            i = 5;
        } else if (c == 'f' || c == 'F') {
            str2 = "false";
            str = "FALSE";
            i = 6;
        } else if (c != 'n' && c != 'N') {
            return 0;
        } else {
            str2 = "null";
            str = "NULL";
            i = 7;
        }
        int length = str2.length();
        for (int i2 = 1; i2 < length; i2++) {
            if (this.pos + i2 >= this.limit && !zzafm(i2 + 1)) {
                return 0;
            }
            char c2 = this.bhe[this.pos + i2];
            if (c2 != str2.charAt(i2) && c2 != str.charAt(i2)) {
                return 0;
            }
        }
        if ((this.pos + length < this.limit || zzafm(length + 1)) && zze(this.bhe[this.pos + length])) {
            return 0;
        }
        this.pos += length;
        this.bhh = i;
        return i;
    }

    private int q() throws IOException {
        int i;
        char c;
        char[] cArr = this.bhe;
        int i2 = this.pos;
        int i3 = 0;
        int i4 = this.limit;
        boolean z = true;
        int i5 = 0;
        char c2 = 0;
        boolean z2 = false;
        long j = 0;
        while (true) {
            if (i2 + i5 == i4) {
                if (i5 == cArr.length) {
                    return i3;
                }
                if (zzafm(i5 + 1)) {
                    i2 = this.pos;
                    i4 = this.limit;
                }
            }
            c = cArr[i2 + i5];
            char c3 = 3;
            if (c != '+') {
                if (c != 'E' && c != 'e') {
                    switch (c) {
                        case '-':
                            c3 = 6;
                            i3 = 0;
                            if (c2 == 0) {
                                c2 = 1;
                                z2 = true;
                                break;
                            } else if (c2 != 5) {
                                return 0;
                            }
                        case '.':
                            i3 = 0;
                            if (c2 != 2) {
                                return 0;
                            }
                            c2 = c3;
                            break;
                        default:
                            if (c >= '0' && c <= '9') {
                                if (c2 != 1 && c2 != 0) {
                                    if (c2 != 2) {
                                        if (c2 != 3) {
                                            if (c2 == 5 || c2 == 6) {
                                                i3 = 0;
                                                c2 = 7;
                                                break;
                                            }
                                        } else {
                                            i3 = 0;
                                            c2 = 4;
                                            break;
                                        }
                                    } else if (j == 0) {
                                        return 0;
                                    } else {
                                        long j2 = (10 * j) - ((long) (c - '0'));
                                        z = (j > -922337203685477580L || (j == -922337203685477580L && j2 < j)) & z;
                                        j = j2;
                                    }
                                } else {
                                    j = (long) (-(c - '0'));
                                    c2 = 2;
                                }
                                i3 = 0;
                                break;
                            } else {
                                break;
                            }
                            break;
                    }
                } else {
                    i3 = 0;
                    if (c2 != 2 && c2 != 4) {
                        return 0;
                    }
                    c2 = 5;
                    i5++;
                }
            } else {
                c3 = 6;
                i3 = 0;
                if (c2 != 5) {
                    return 0;
                }
            }
            c2 = c3;
            i5++;
        }
        if (zze(c)) {
            return 0;
        }
        if (c2 == 2 && z && (j != Long.MIN_VALUE || z2)) {
            if (!z2) {
                j = -j;
            }
            this.bhi = j;
            this.pos += i5;
            i = 15;
        } else if (c2 != 2 && c2 != 4 && c2 != 7) {
            return 0;
        } else {
            this.bhj = i5;
            i = 16;
        }
        this.bhh = i;
        return i;
    }

    private String r() throws IOException {
        int i;
        String str;
        int i2 = 0;
        StringBuilder sb = null;
        while (true) {
            i = 0;
            while (true) {
                if (this.pos + i < this.limit) {
                    switch (this.bhe[this.pos + i]) {
                        case 9:
                        case 10:
                        case 12:
                        case 13:
                        case ' ':
                        case ',':
                        case ':':
                        case '[':
                        case ']':
                        case '{':
                        case '}':
                            break;
                        case '#':
                        case '/':
                        case ';':
                        case '=':
                        case '\\':
                            t();
                            break;
                        default:
                            i++;
                            break;
                    }
                } else if (i >= this.bhe.length) {
                    if (sb == null) {
                        sb = new StringBuilder();
                    }
                    sb.append(this.bhe, this.pos, i);
                    this.pos += i;
                    if (!zzafm(1)) {
                    }
                } else if (zzafm(i + 1)) {
                }
            }
        }
        i2 = i;
        if (sb == null) {
            str = new String(this.bhe, this.pos, i2);
        } else {
            sb.append(this.bhe, this.pos, i2);
            str = sb.toString();
        }
        this.pos += i2;
        return str;
    }

    private void s() throws IOException {
        do {
            int i = 0;
            while (this.pos + i < this.limit) {
                switch (this.bhe[this.pos + i]) {
                    case 9:
                    case 10:
                    case 12:
                    case 13:
                    case ' ':
                    case ',':
                    case ':':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                        break;
                    case '#':
                    case '/':
                    case ';':
                    case '=':
                    case '\\':
                        t();
                        break;
                    default:
                        i++;
                }
                this.pos += i;
                return;
            }
            this.pos += i;
        } while (zzafm(1));
    }

    private void t() throws IOException {
        if (!this.bhd) {
            throw zztu("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    private void u() throws IOException {
        char c;
        do {
            if (this.pos >= this.limit && !zzafm(1)) {
                break;
            }
            char[] cArr = this.bhe;
            int i = this.pos;
            this.pos = i + 1;
            c = cArr[i];
            if (c == 10) {
                this.bhf++;
                this.bhg = this.pos;
                return;
            }
        } while (c != 13);
    }

    private char v() throws IOException {
        int i;
        int i2;
        if (this.pos != this.limit || zzafm(1)) {
            char[] cArr = this.bhe;
            int i3 = this.pos;
            this.pos = i3 + 1;
            char c = cArr[i3];
            if (c == 10) {
                this.bhf++;
                this.bhg = this.pos;
                return c;
            } else if (c == 'b') {
                return 8;
            } else {
                if (c == 'f') {
                    return 12;
                }
                if (c == 'n') {
                    return 10;
                }
                if (c == 'r') {
                    return 13;
                }
                switch (c) {
                    case 't':
                        return 9;
                    case 'u':
                        if (this.pos + 4 <= this.limit || zzafm(4)) {
                            char c2 = 0;
                            int i4 = this.pos;
                            int i5 = i4 + 4;
                            while (i4 < i5) {
                                char c3 = this.bhe[i4];
                                char c4 = (char) (c2 << 4);
                                if (c3 < '0' || c3 > '9') {
                                    if (c3 >= 'a' && c3 <= 'f') {
                                        i = c3 - 'a';
                                    } else if (c3 < 'A' || c3 > 'F') {
                                        String str = "\\u";
                                        String valueOf = String.valueOf(new String(this.bhe, this.pos, 4));
                                        throw new NumberFormatException(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                                    } else {
                                        i = c3 - 'A';
                                    }
                                    i2 = i + 10;
                                } else {
                                    i2 = c3 - '0';
                                }
                                c2 = (char) (c4 + i2);
                                i4++;
                            }
                            this.pos += 4;
                            return c2;
                        }
                        throw zztu("Unterminated escape sequence");
                    default:
                        return c;
                }
            }
        } else {
            throw zztu("Unterminated escape sequence");
        }
    }

    private void w() throws IOException {
        zzdb(true);
        this.pos--;
        if (this.pos + bhc.length <= this.limit || zzafm(bhc.length)) {
            int i = 0;
            while (i < bhc.length) {
                if (this.bhe[this.pos + i] == bhc[i]) {
                    i++;
                } else {
                    return;
                }
            }
            this.pos += bhc.length;
        }
    }

    private void zzafl(int i) {
        if (this.bhm == this.bhl.length) {
            int[] iArr = new int[(this.bhm * 2)];
            int[] iArr2 = new int[(this.bhm * 2)];
            String[] strArr = new String[(this.bhm * 2)];
            System.arraycopy(this.bhl, 0, iArr, 0, this.bhm);
            System.arraycopy(this.bho, 0, iArr2, 0, this.bhm);
            System.arraycopy(this.bhn, 0, strArr, 0, this.bhm);
            this.bhl = iArr;
            this.bho = iArr2;
            this.bhn = strArr;
        }
        int[] iArr3 = this.bhl;
        int i2 = this.bhm;
        this.bhm = i2 + 1;
        iArr3[i2] = i;
    }

    private boolean zzafm(int i) throws IOException {
        char[] cArr = this.bhe;
        this.bhg -= this.pos;
        if (this.limit != this.pos) {
            this.limit -= this.pos;
            System.arraycopy(cArr, this.pos, cArr, 0, this.limit);
        } else {
            this.limit = 0;
        }
        this.pos = 0;
        do {
            int read = this.in.read(cArr, this.limit, cArr.length - this.limit);
            if (read == -1) {
                return false;
            }
            this.limit += read;
            if (this.bhf == 0 && this.bhg == 0 && this.limit > 0 && cArr[0] == 65279) {
                this.pos++;
                this.bhg++;
                i++;
            }
        } while (this.limit < i);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006e, code lost:
        if (r1 != '/') goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0070, code lost:
        r7.pos = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0073, code lost:
        if (r4 != r2) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0075, code lost:
        r7.pos--;
        r2 = zzafm(2);
        r7.pos++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0083, code lost:
        if (r2 != false) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0085, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0086, code lost:
        t();
        r2 = r0[r7.pos];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        if (r2 == '*') goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
        if (r2 == '/') goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0093, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0094, code lost:
        r7.pos++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x009e, code lost:
        r7.pos++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a9, code lost:
        if (zztt("*/") != false) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b1, code lost:
        throw zztu("Unterminated comment");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b9, code lost:
        if (r1 != '#') goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00bb, code lost:
        r7.pos = r4;
        t();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c1, code lost:
        r7.pos = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c3, code lost:
        return r1;
     */
    private int zzdb(boolean z) throws IOException {
        char[] cArr = this.bhe;
        while (true) {
            int i = this.pos;
            while (true) {
                int i2 = this.limit;
                while (true) {
                    if (i == i2) {
                        this.pos = i;
                        if (zzafm(1)) {
                            i = this.pos;
                            i2 = this.limit;
                        } else if (!z) {
                            return -1;
                        } else {
                            String valueOf = String.valueOf("End of input at line ");
                            int lineNumber = getLineNumber();
                            int columnNumber = getColumnNumber();
                            StringBuilder sb = new StringBuilder(30 + String.valueOf(valueOf).length());
                            sb.append(valueOf);
                            sb.append(lineNumber);
                            sb.append(" column ");
                            sb.append(columnNumber);
                            throw new EOFException(sb.toString());
                        }
                    }
                    int i3 = i + 1;
                    char c = cArr[i];
                    if (c != 10) {
                        if (!(c == ' ' || c == 13 || c == 9)) {
                            break;
                        }
                    } else {
                        this.bhf++;
                        this.bhg = i3;
                    }
                    i = i3;
                }
                i = this.pos + 2;
            }
            u();
        }
    }

    private boolean zze(char c) throws IOException {
        switch (c) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
            case ',':
            case ':':
            case '[':
            case ']':
            case '{':
            case '}':
                break;
            case '#':
            case '/':
            case ';':
            case '=':
            case '\\':
                t();
                break;
            default:
                return true;
        }
        return false;
    }

    private String zzf(char c) throws IOException {
        char[] cArr = this.bhe;
        StringBuilder sb = new StringBuilder();
        while (true) {
            int i = this.pos;
            int i2 = this.limit;
            int i3 = i;
            while (true) {
                if (i < i2) {
                    int i4 = i + 1;
                    char c2 = cArr[i];
                    if (c2 == c) {
                        this.pos = i4;
                        sb.append(cArr, i3, (i4 - i3) - 1);
                        return sb.toString();
                    } else if (c2 == '\\') {
                        this.pos = i4;
                        sb.append(cArr, i3, (i4 - i3) - 1);
                        sb.append(v());
                        break;
                    } else {
                        if (c2 == 10) {
                            this.bhf++;
                            this.bhg = i4;
                        }
                        i = i4;
                    }
                } else {
                    sb.append(cArr, i3, i - i3);
                    this.pos = i;
                    if (!zzafm(1)) {
                        throw zztu("Unterminated string");
                    }
                }
            }
        }
    }

    private void zzg(char c) throws IOException {
        char[] cArr = this.bhe;
        while (true) {
            int i = this.pos;
            int i2 = this.limit;
            while (true) {
                if (i < i2) {
                    int i3 = i + 1;
                    char c2 = cArr[i];
                    if (c2 == c) {
                        this.pos = i3;
                        return;
                    } else if (c2 == '\\') {
                        this.pos = i3;
                        v();
                        break;
                    } else {
                        if (c2 == 10) {
                            this.bhf++;
                            this.bhg = i3;
                        }
                        i = i3;
                    }
                } else {
                    this.pos = i;
                    if (!zzafm(1)) {
                        throw zztu("Unterminated string");
                    }
                }
            }
        }
    }

    private boolean zztt(String str) throws IOException {
        while (true) {
            int i = 0;
            if (this.pos + str.length() > this.limit && !zzafm(str.length())) {
                return false;
            }
            if (this.bhe[this.pos] == 10) {
                this.bhf++;
                this.bhg = this.pos + 1;
            } else {
                while (i < str.length()) {
                    if (this.bhe[this.pos + i] == str.charAt(i)) {
                        i++;
                    }
                }
                return true;
            }
            this.pos++;
        }
    }

    private IOException zztu(String str) throws IOException {
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        String path = getPath();
        StringBuilder sb = new StringBuilder(45 + String.valueOf(str).length() + String.valueOf(path).length());
        sb.append(str);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        sb.append(" path ");
        sb.append(path);
        throw new zzaop(sb.toString());
    }

    public zzaon b() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        switch (i) {
            case 1:
                return zzaon.BEGIN_OBJECT;
            case 2:
                return zzaon.END_OBJECT;
            case 3:
                return zzaon.BEGIN_ARRAY;
            case 4:
                return zzaon.END_ARRAY;
            case 5:
            case 6:
                return zzaon.BOOLEAN;
            case 7:
                return zzaon.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return zzaon.STRING;
            case 12:
            case 13:
            case 14:
                return zzaon.NAME;
            case 15:
            case 16:
                return zzaon.NUMBER;
            case 17:
                return zzaon.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    public void beginArray() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 3) {
            zzafl(1);
            this.bho[this.bhm - 1] = 0;
            this.bhh = 0;
            return;
        }
        String valueOf = String.valueOf(b());
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        String path = getPath();
        StringBuilder sb = new StringBuilder(74 + String.valueOf(valueOf).length() + String.valueOf(path).length());
        sb.append("Expected BEGIN_ARRAY but was ");
        sb.append(valueOf);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        sb.append(" path ");
        sb.append(path);
        throw new IllegalStateException(sb.toString());
    }

    public void beginObject() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 1) {
            zzafl(3);
            this.bhh = 0;
            return;
        }
        String valueOf = String.valueOf(b());
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        String path = getPath();
        StringBuilder sb = new StringBuilder(75 + String.valueOf(valueOf).length() + String.valueOf(path).length());
        sb.append("Expected BEGIN_OBJECT but was ");
        sb.append(valueOf);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        sb.append(" path ");
        sb.append(path);
        throw new IllegalStateException(sb.toString());
    }

    public void close() throws IOException {
        this.bhh = 0;
        this.bhl[0] = 8;
        this.bhm = 1;
        this.in.close();
    }

    public void endArray() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 4) {
            this.bhm--;
            int[] iArr = this.bho;
            int i2 = this.bhm - 1;
            iArr[i2] = iArr[i2] + 1;
            this.bhh = 0;
            return;
        }
        String valueOf = String.valueOf(b());
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        String path = getPath();
        StringBuilder sb = new StringBuilder(72 + String.valueOf(valueOf).length() + String.valueOf(path).length());
        sb.append("Expected END_ARRAY but was ");
        sb.append(valueOf);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        sb.append(" path ");
        sb.append(path);
        throw new IllegalStateException(sb.toString());
    }

    public void endObject() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 2) {
            this.bhm--;
            this.bhn[this.bhm] = null;
            int[] iArr = this.bho;
            int i2 = this.bhm - 1;
            iArr[i2] = iArr[i2] + 1;
            this.bhh = 0;
            return;
        }
        String valueOf = String.valueOf(b());
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        String path = getPath();
        StringBuilder sb = new StringBuilder(73 + String.valueOf(valueOf).length() + String.valueOf(path).length());
        sb.append("Expected END_OBJECT but was ");
        sb.append(valueOf);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        sb.append(" path ");
        sb.append(path);
        throw new IllegalStateException(sb.toString());
    }

    public String getPath() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = this.bhm;
        for (int i2 = 0; i2 < i; i2++) {
            switch (this.bhl[i2]) {
                case 1:
                case 2:
                    sb.append('[');
                    sb.append(this.bho[i2]);
                    sb.append(']');
                    break;
                case 3:
                case 4:
                case 5:
                    sb.append('.');
                    if (this.bhn[i2] == null) {
                        break;
                    } else {
                        sb.append(this.bhn[i2]);
                        break;
                    }
            }
        }
        return sb.toString();
    }

    public boolean hasNext() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        return (i == 2 || i == 4) ? false : true;
    }

    public final boolean isLenient() {
        return this.bhd;
    }

    public boolean nextBoolean() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 5) {
            this.bhh = 0;
            int[] iArr = this.bho;
            int i2 = this.bhm - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.bhh = 0;
            int[] iArr2 = this.bho;
            int i3 = this.bhm - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            String valueOf = String.valueOf(b());
            int lineNumber = getLineNumber();
            int columnNumber = getColumnNumber();
            String path = getPath();
            StringBuilder sb = new StringBuilder(72 + String.valueOf(valueOf).length() + String.valueOf(path).length());
            sb.append("Expected a boolean but was ");
            sb.append(valueOf);
            sb.append(" at line ");
            sb.append(lineNumber);
            sb.append(" column ");
            sb.append(columnNumber);
            sb.append(" path ");
            sb.append(path);
            throw new IllegalStateException(sb.toString());
        }
    }

    public double nextDouble() throws IOException {
        String zzf;
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 15) {
            this.bhh = 0;
            int[] iArr = this.bho;
            int i2 = this.bhm - 1;
            iArr[i2] = iArr[i2] + 1;
            return (double) this.bhi;
        }
        if (i == 16) {
            this.bhk = new String(this.bhe, this.pos, this.bhj);
            this.pos += this.bhj;
        } else {
            if (i == 8 || i == 9) {
                zzf = zzf(i == 8 ? '\'' : '\"');
            } else if (i == 10) {
                zzf = r();
            } else if (i != 11) {
                String valueOf = String.valueOf(b());
                int lineNumber = getLineNumber();
                int columnNumber = getColumnNumber();
                String path = getPath();
                StringBuilder sb = new StringBuilder(71 + String.valueOf(valueOf).length() + String.valueOf(path).length());
                sb.append("Expected a double but was ");
                sb.append(valueOf);
                sb.append(" at line ");
                sb.append(lineNumber);
                sb.append(" column ");
                sb.append(columnNumber);
                sb.append(" path ");
                sb.append(path);
                throw new IllegalStateException(sb.toString());
            }
            this.bhk = zzf;
        }
        this.bhh = 11;
        double parseDouble = Double.parseDouble(this.bhk);
        if (this.bhd || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
            this.bhk = null;
            this.bhh = 0;
            int[] iArr2 = this.bho;
            int i3 = this.bhm - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return parseDouble;
        }
        int lineNumber2 = getLineNumber();
        int columnNumber2 = getColumnNumber();
        String path2 = getPath();
        StringBuilder sb2 = new StringBuilder(102 + String.valueOf(path2).length());
        sb2.append("JSON forbids NaN and infinities: ");
        sb2.append(parseDouble);
        sb2.append(" at line ");
        sb2.append(lineNumber2);
        sb2.append(" column ");
        sb2.append(columnNumber2);
        sb2.append(" path ");
        sb2.append(path2);
        throw new zzaop(sb2.toString());
    }

    public int nextInt() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 15) {
            int i2 = (int) this.bhi;
            if (this.bhi != ((long) i2)) {
                long j = this.bhi;
                int lineNumber = getLineNumber();
                int columnNumber = getColumnNumber();
                String path = getPath();
                StringBuilder sb = new StringBuilder(89 + String.valueOf(path).length());
                sb.append("Expected an int but was ");
                sb.append(j);
                sb.append(" at line ");
                sb.append(lineNumber);
                sb.append(" column ");
                sb.append(columnNumber);
                sb.append(" path ");
                sb.append(path);
                throw new NumberFormatException(sb.toString());
            }
            this.bhh = 0;
            int[] iArr = this.bho;
            int i3 = this.bhm - 1;
            iArr[i3] = iArr[i3] + 1;
            return i2;
        }
        if (i == 16) {
            this.bhk = new String(this.bhe, this.pos, this.bhj);
            this.pos += this.bhj;
        } else if (i == 8 || i == 9) {
            this.bhk = zzf(i == 8 ? '\'' : '\"');
            try {
                int parseInt = Integer.parseInt(this.bhk);
                this.bhh = 0;
                int[] iArr2 = this.bho;
                int i4 = this.bhm - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        } else {
            String valueOf = String.valueOf(b());
            int lineNumber2 = getLineNumber();
            int columnNumber2 = getColumnNumber();
            String path2 = getPath();
            StringBuilder sb2 = new StringBuilder(69 + String.valueOf(valueOf).length() + String.valueOf(path2).length());
            sb2.append("Expected an int but was ");
            sb2.append(valueOf);
            sb2.append(" at line ");
            sb2.append(lineNumber2);
            sb2.append(" column ");
            sb2.append(columnNumber2);
            sb2.append(" path ");
            sb2.append(path2);
            throw new IllegalStateException(sb2.toString());
        }
        this.bhh = 11;
        double parseDouble = Double.parseDouble(this.bhk);
        int i5 = (int) parseDouble;
        if (((double) i5) != parseDouble) {
            String str = this.bhk;
            int lineNumber3 = getLineNumber();
            int columnNumber3 = getColumnNumber();
            String path3 = getPath();
            StringBuilder sb3 = new StringBuilder(69 + String.valueOf(str).length() + String.valueOf(path3).length());
            sb3.append("Expected an int but was ");
            sb3.append(str);
            sb3.append(" at line ");
            sb3.append(lineNumber3);
            sb3.append(" column ");
            sb3.append(columnNumber3);
            sb3.append(" path ");
            sb3.append(path3);
            throw new NumberFormatException(sb3.toString());
        }
        this.bhk = null;
        this.bhh = 0;
        int[] iArr3 = this.bho;
        int i6 = this.bhm - 1;
        iArr3[i6] = iArr3[i6] + 1;
        return i5;
    }

    public long nextLong() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 15) {
            this.bhh = 0;
            int[] iArr = this.bho;
            int i2 = this.bhm - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.bhi;
        }
        if (i == 16) {
            this.bhk = new String(this.bhe, this.pos, this.bhj);
            this.pos += this.bhj;
        } else if (i == 8 || i == 9) {
            this.bhk = zzf(i == 8 ? '\'' : '\"');
            try {
                long parseLong = Long.parseLong(this.bhk);
                this.bhh = 0;
                int[] iArr2 = this.bho;
                int i3 = this.bhm - 1;
                iArr2[i3] = iArr2[i3] + 1;
                return parseLong;
            } catch (NumberFormatException unused) {
            }
        } else {
            String valueOf = String.valueOf(b());
            int lineNumber = getLineNumber();
            int columnNumber = getColumnNumber();
            String path = getPath();
            StringBuilder sb = new StringBuilder(69 + String.valueOf(valueOf).length() + String.valueOf(path).length());
            sb.append("Expected a long but was ");
            sb.append(valueOf);
            sb.append(" at line ");
            sb.append(lineNumber);
            sb.append(" column ");
            sb.append(columnNumber);
            sb.append(" path ");
            sb.append(path);
            throw new IllegalStateException(sb.toString());
        }
        this.bhh = 11;
        double parseDouble = Double.parseDouble(this.bhk);
        long j = (long) parseDouble;
        if (((double) j) != parseDouble) {
            String str = this.bhk;
            int lineNumber2 = getLineNumber();
            int columnNumber2 = getColumnNumber();
            String path2 = getPath();
            StringBuilder sb2 = new StringBuilder(69 + String.valueOf(str).length() + String.valueOf(path2).length());
            sb2.append("Expected a long but was ");
            sb2.append(str);
            sb2.append(" at line ");
            sb2.append(lineNumber2);
            sb2.append(" column ");
            sb2.append(columnNumber2);
            sb2.append(" path ");
            sb2.append(path2);
            throw new NumberFormatException(sb2.toString());
        }
        this.bhk = null;
        this.bhh = 0;
        int[] iArr3 = this.bho;
        int i4 = this.bhm - 1;
        iArr3[i4] = iArr3[i4] + 1;
        return j;
    }

    public String nextName() throws IOException {
        String str;
        char c;
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 14) {
            str = r();
        } else {
            if (i == 12) {
                c = '\'';
            } else if (i == 13) {
                c = '\"';
            } else {
                String valueOf = String.valueOf(b());
                int lineNumber = getLineNumber();
                int columnNumber = getColumnNumber();
                String path = getPath();
                StringBuilder sb = new StringBuilder(69 + String.valueOf(valueOf).length() + String.valueOf(path).length());
                sb.append("Expected a name but was ");
                sb.append(valueOf);
                sb.append(" at line ");
                sb.append(lineNumber);
                sb.append(" column ");
                sb.append(columnNumber);
                sb.append(" path ");
                sb.append(path);
                throw new IllegalStateException(sb.toString());
            }
            str = zzf(c);
        }
        this.bhh = 0;
        this.bhn[this.bhm - 1] = str;
        return str;
    }

    public void nextNull() throws IOException {
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 7) {
            this.bhh = 0;
            int[] iArr = this.bho;
            int i2 = this.bhm - 1;
            iArr[i2] = iArr[i2] + 1;
            return;
        }
        String valueOf = String.valueOf(b());
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        String path = getPath();
        StringBuilder sb = new StringBuilder(67 + String.valueOf(valueOf).length() + String.valueOf(path).length());
        sb.append("Expected null but was ");
        sb.append(valueOf);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        sb.append(" path ");
        sb.append(path);
        throw new IllegalStateException(sb.toString());
    }

    public String nextString() throws IOException {
        String str;
        char c;
        int i = this.bhh;
        if (i == 0) {
            i = o();
        }
        if (i == 10) {
            str = r();
        } else {
            if (i == 8) {
                c = '\'';
            } else if (i == 9) {
                c = '\"';
            } else if (i == 11) {
                str = this.bhk;
                this.bhk = null;
            } else if (i == 15) {
                str = Long.toString(this.bhi);
            } else if (i == 16) {
                str = new String(this.bhe, this.pos, this.bhj);
                this.pos += this.bhj;
            } else {
                String valueOf = String.valueOf(b());
                int lineNumber = getLineNumber();
                int columnNumber = getColumnNumber();
                String path = getPath();
                StringBuilder sb = new StringBuilder(71 + String.valueOf(valueOf).length() + String.valueOf(path).length());
                sb.append("Expected a string but was ");
                sb.append(valueOf);
                sb.append(" at line ");
                sb.append(lineNumber);
                sb.append(" column ");
                sb.append(columnNumber);
                sb.append(" path ");
                sb.append(path);
                throw new IllegalStateException(sb.toString());
            }
            str = zzf(c);
        }
        this.bhh = 0;
        int[] iArr = this.bho;
        int i2 = this.bhm - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    public final void setLenient(boolean z) {
        this.bhd = z;
    }

    public void skipValue() throws IOException {
        char c;
        int i = 0;
        do {
            int i2 = this.bhh;
            if (i2 == 0) {
                i2 = o();
            }
            if (i2 == 3) {
                zzafl(1);
            } else if (i2 == 1) {
                zzafl(3);
            } else if (i2 == 4 || i2 == 2) {
                this.bhm--;
                i--;
                this.bhh = 0;
            } else if (i2 == 14 || i2 == 10) {
                s();
                this.bhh = 0;
            } else {
                if (i2 == 8 || i2 == 12) {
                    c = '\'';
                } else if (i2 == 9 || i2 == 13) {
                    c = '\"';
                } else {
                    if (i2 == 16) {
                        this.pos += this.bhj;
                    }
                    this.bhh = 0;
                }
                zzg(c);
                this.bhh = 0;
            }
            i++;
            this.bhh = 0;
        } while (i != 0);
        int[] iArr = this.bho;
        int i3 = this.bhm - 1;
        iArr[i3] = iArr[i3] + 1;
        this.bhn[this.bhm - 1] = "null";
    }

    public String toString() {
        String valueOf = String.valueOf(getClass().getSimpleName());
        int lineNumber = getLineNumber();
        int columnNumber = getColumnNumber();
        StringBuilder sb = new StringBuilder(39 + String.valueOf(valueOf).length());
        sb.append(valueOf);
        sb.append(" at line ");
        sb.append(lineNumber);
        sb.append(" column ");
        sb.append(columnNumber);
        return sb.toString();
    }
}
