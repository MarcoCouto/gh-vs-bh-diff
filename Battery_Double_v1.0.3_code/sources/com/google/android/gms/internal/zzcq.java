package com.google.android.gms.internal;

import android.util.Base64OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.PriorityQueue;

@zzin
public class zzcq {
    private final int zzata;
    private final int zzatb;
    private final int zzatc;
    private final zzcp zzatd = new zzcs();

    static class zza {
        ByteArrayOutputStream zzatf = new ByteArrayOutputStream(4096);
        Base64OutputStream zzatg = new Base64OutputStream(this.zzatf, 10);

        public String toString() {
            String str;
            try {
                this.zzatg.close();
            } catch (IOException e) {
                zzkd.zzb("HashManager: Unable to convert to Base64.", e);
            }
            try {
                this.zzatf.close();
                str = this.zzatf.toString();
            } catch (IOException e2) {
                zzkd.zzb("HashManager: Unable to convert to Base64.", e2);
                str = "";
            } catch (Throwable th) {
                this.zzatf = null;
                this.zzatg = null;
                throw th;
            }
            this.zzatf = null;
            this.zzatg = null;
            return str;
        }

        public void write(byte[] bArr) throws IOException {
            this.zzatg.write(bArr);
        }
    }

    public zzcq(int i) {
        this.zzatb = i;
        this.zzata = 6;
        this.zzatc = 0;
    }

    public String zza(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuffer.append(((String) it.next()).toLowerCase(Locale.US));
            stringBuffer.append(10);
        }
        return zzab(stringBuffer.toString());
    }

    /* access modifiers changed from: 0000 */
    public String zzab(String str) {
        String[] split = str.split("\n");
        if (split.length == 0) {
            return "";
        }
        zza zzif = zzif();
        PriorityQueue priorityQueue = new PriorityQueue(this.zzatb, new Comparator<com.google.android.gms.internal.zzct.zza>() {
            /* renamed from: zza */
            public int compare(com.google.android.gms.internal.zzct.zza zza, com.google.android.gms.internal.zzct.zza zza2) {
                int i = zza.zzatj - zza2.zzatj;
                return i != 0 ? i : (int) (zza.value - zza2.value);
            }
        });
        for (String zzad : split) {
            String[] zzad2 = zzcr.zzad(zzad);
            if (zzad2.length != 0) {
                zzct.zza(zzad2, this.zzatb, this.zzata, priorityQueue);
            }
        }
        Iterator it = priorityQueue.iterator();
        while (it.hasNext()) {
            try {
                zzif.write(this.zzatd.zzaa(((com.google.android.gms.internal.zzct.zza) it.next()).zzati));
            } catch (IOException e) {
                zzkd.zzb("Error while writing hash to byteStream", e);
            }
        }
        return zzif.toString();
    }

    /* access modifiers changed from: 0000 */
    public zza zzif() {
        return new zza();
    }
}
