package com.google.android.gms.internal;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;
import com.google.android.gms.clearcut.zzb.C0026zzb;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzab;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class zzpg implements C0026zzb {
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    static Boolean qU;
    final zza qV;

    static class zza {
        final ContentResolver mContentResolver;

        zza(Context context) {
            if (context == null || !zzbm(context)) {
                this.mContentResolver = null;
                return;
            }
            this.mContentResolver = context.getContentResolver();
            zzaeo.zzb(this.mContentResolver, "gms:playlog:service:sampling_");
        }

        private static boolean zzbm(Context context) {
            if (zzpg.qU == null) {
                zzpg.qU = Boolean.valueOf(context.checkCallingOrSelfPermission("com.google.android.providers.gsf.permission.READ_GSERVICES") == 0);
            }
            return zzpg.qU.booleanValue();
        }

        /* access modifiers changed from: 0000 */
        public long zzane() {
            if (this.mContentResolver == null) {
                return 0;
            }
            return zzaeo.getLong(this.mContentResolver, "android_id", 0);
        }

        /* access modifiers changed from: 0000 */
        public String zzgu(String str) {
            if (this.mContentResolver == null) {
                return null;
            }
            ContentResolver contentResolver = this.mContentResolver;
            String valueOf = String.valueOf("gms:playlog:service:sampling_");
            String valueOf2 = String.valueOf(str);
            return zzaeo.zza(contentResolver, valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf), null);
        }
    }

    static class zzb {
        public final String qW;
        public final long qX;
        public final long qY;

        public zzb(String str, long j, long j2) {
            this.qW = str;
            this.qX = j;
            this.qY = j2;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            return zzaa.equal(this.qW, zzb.qW) && zzaa.equal(Long.valueOf(this.qX), Long.valueOf(zzb.qX)) && zzaa.equal(Long.valueOf(this.qY), Long.valueOf(zzb.qY));
        }

        public int hashCode() {
            return zzaa.hashCode(this.qW, Long.valueOf(this.qX), Long.valueOf(this.qY));
        }
    }

    public zzpg() {
        this(new zza(null));
    }

    public zzpg(Context context) {
        this(new zza(context));
    }

    zzpg(zza zza2) {
        this.qV = (zza) zzab.zzy(zza2);
    }

    static boolean zza(long j, long j2, long j3) {
        if (j2 >= 0 && j3 >= 0) {
            return j3 > 0 && zzph.zzd(j, j3) < j2;
        }
        StringBuilder sb = new StringBuilder(72);
        sb.append("negative values not supported: ");
        sb.append(j2);
        sb.append("/");
        sb.append(j3);
        throw new IllegalArgumentException(sb.toString());
    }

    static long zzag(long j) {
        return zzpd.zzm(ByteBuffer.allocate(8).putLong(j).array());
    }

    static long zzd(String str, long j) {
        if (str == null || str.isEmpty()) {
            return zzag(j);
        }
        byte[] bytes = str.getBytes(UTF_8);
        ByteBuffer allocate = ByteBuffer.allocate(bytes.length + 8);
        allocate.put(bytes);
        allocate.putLong(j);
        return zzpd.zzm(allocate.array());
    }

    static zzb zzgt(String str) {
        if (str == null) {
            return null;
        }
        String str2 = "";
        int indexOf = str.indexOf(44);
        int i = 0;
        if (indexOf >= 0) {
            str2 = str.substring(0, indexOf);
            i = indexOf + 1;
        }
        String str3 = str2;
        int indexOf2 = str.indexOf(47, i);
        if (indexOf2 <= 0) {
            String str4 = "LogSamplerImpl";
            String str5 = "Failed to parse the rule: ";
            String valueOf = String.valueOf(str);
            Log.e(str4, valueOf.length() != 0 ? str5.concat(valueOf) : new String(str5));
            return null;
        }
        try {
            long parseLong = Long.parseLong(str.substring(i, indexOf2));
            long parseLong2 = Long.parseLong(str.substring(indexOf2 + 1));
            if (parseLong < 0 || parseLong2 < 0) {
                StringBuilder sb = new StringBuilder(72);
                sb.append("negative values not supported: ");
                sb.append(parseLong);
                sb.append("/");
                sb.append(parseLong2);
                Log.e("LogSamplerImpl", sb.toString());
                return null;
            }
            zzb zzb2 = new zzb(str3, parseLong, parseLong2);
            return zzb2;
        } catch (NumberFormatException e) {
            String str6 = "LogSamplerImpl";
            String str7 = "parseLong() failed while parsing: ";
            String valueOf2 = String.valueOf(str);
            Log.e(str6, valueOf2.length() != 0 ? str7.concat(valueOf2) : new String(str7), e);
            return null;
        }
    }

    public boolean zzg(String str, int i) {
        if (str == null || str.isEmpty()) {
            str = i >= 0 ? String.valueOf(i) : null;
        }
        if (str == null) {
            return true;
        }
        long zzane = this.qV.zzane();
        zzb zzgt = zzgt(this.qV.zzgu(str));
        if (zzgt == null) {
            return true;
        }
        return zza(zzd(zzgt.qW, zzane), zzgt.qX, zzgt.qY);
    }
}
