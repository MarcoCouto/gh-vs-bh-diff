package com.google.android.gms.internal;

import java.util.Map;

@zzin
public class zzes implements zzep {
    private final zzet zzbir;

    public zzes(zzet zzet) {
        this.zzbir = zzet;
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        boolean equals = "1".equals(map.get("transparentBackground"));
        boolean equals2 = "1".equals(map.get("blur"));
        float f = 0.0f;
        try {
            if (map.get("blurRadius") != null) {
                f = Float.parseFloat((String) map.get("blurRadius"));
            }
        } catch (NumberFormatException e) {
            zzkd.zzb("Fail to parse float", e);
        }
        this.zzbir.zzg(equals);
        this.zzbir.zza(equals2, f);
    }
}
