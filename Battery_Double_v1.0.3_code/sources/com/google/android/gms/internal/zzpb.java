package com.google.android.gms.internal;

import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.clearcut.LogEventParcelable;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.zzh;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class zzpb implements com.google.android.gms.clearcut.zzc {
    private static final Object qE = new Object();
    private static ScheduledExecutorService qF;
    /* access modifiers changed from: private */
    public static final zze qG = new zze();
    private static final long qH = TimeUnit.MILLISECONDS.convert(2, TimeUnit.MINUTES);
    /* access modifiers changed from: private */
    public GoogleApiClient gY;
    private final zza qI;
    /* access modifiers changed from: private */
    public final Object qJ;
    private long qK;
    private final long qL;
    private ScheduledFuture<?> qM;
    private final Runnable qN;
    /* access modifiers changed from: private */
    public final com.google.android.gms.common.util.zze zzaoc;

    public interface zza {
    }

    public static class zzb implements zza {
    }

    static abstract class zzc<R extends Result> extends com.google.android.gms.internal.zzpm.zza<R, zzpc> {
        public zzc(GoogleApiClient googleApiClient) {
            super(com.google.android.gms.clearcut.zzb.API, googleApiClient);
        }
    }

    static final class zzd extends zzc<Status> {
        private final LogEventParcelable qS;

        zzd(LogEventParcelable logEventParcelable, GoogleApiClient googleApiClient) {
            super(googleApiClient);
            this.qS = logEventParcelable;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof zzd)) {
                return false;
            }
            return this.qS.equals(((zzd) obj).qS);
        }

        public String toString() {
            String valueOf = String.valueOf(this.qS);
            StringBuilder sb = new StringBuilder(12 + String.valueOf(valueOf).length());
            sb.append("MethodImpl(");
            sb.append(valueOf);
            sb.append(")");
            return sb.toString();
        }

        /* access modifiers changed from: protected */
        public void zza(zzpc zzpc) throws RemoteException {
            AnonymousClass1 r0 = new com.google.android.gms.internal.zzpe.zza() {
                public void zzw(Status status) {
                    zzd.this.zzc(status);
                }
            };
            try {
                zzpb.zza(this.qS);
                zzpc.zza(r0, this.qS);
            } catch (RuntimeException e) {
                Log.e("ClearcutLoggerApiImpl", "derived ClearcutLogger.MessageProducer ", e);
                zzz(new Status(10, "MessageProducer"));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: zzb */
        public Status zzc(Status status) {
            return status;
        }
    }

    private static final class zze {
        private int mSize;

        private zze() {
            this.mSize = 0;
        }

        public synchronized void decrement() {
            if (this.mSize == 0) {
                throw new RuntimeException("too many decrements");
            }
            this.mSize--;
            if (this.mSize == 0) {
                notifyAll();
            }
        }

        public synchronized void increment() {
            this.mSize++;
        }
    }

    public zzpb() {
        this(new zzh(), qH, new zzb());
    }

    public zzpb(com.google.android.gms.common.util.zze zze2, long j, zza zza2) {
        this.qJ = new Object();
        this.qK = 0;
        this.qM = null;
        this.gY = null;
        this.qN = new Runnable() {
            public void run() {
                synchronized (zzpb.this.qJ) {
                    if (zzpb.zzb(zzpb.this) <= zzpb.this.zzaoc.elapsedRealtime() && zzpb.this.gY != null) {
                        Log.i("ClearcutLoggerApiImpl", "disconnect managed GoogleApiClient");
                        zzpb.this.gY.disconnect();
                        zzpb.this.gY = null;
                    }
                }
            }
        };
        this.zzaoc = zze2;
        this.qL = j;
        this.qI = zza2;
    }

    private PendingResult<Status> zza(final GoogleApiClient googleApiClient, final zzc<Status> zzc2) {
        zzanc().execute(new Runnable() {
            public void run() {
                googleApiClient.zzc(zzc2);
            }
        });
        return zzc2;
    }

    /* access modifiers changed from: private */
    public static void zza(LogEventParcelable logEventParcelable) {
        if (logEventParcelable.qC != null && logEventParcelable.qB.bkh.length == 0) {
            logEventParcelable.qB.bkh = logEventParcelable.qC.zzanb();
        }
        if (logEventParcelable.qD != null && logEventParcelable.qB.bko.length == 0) {
            logEventParcelable.qB.bko = logEventParcelable.qD.zzanb();
        }
        logEventParcelable.qv = zzapv.zzf(logEventParcelable.qB);
    }

    private ScheduledExecutorService zzanc() {
        synchronized (qE) {
            if (qF == null) {
                qF = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
                    public Thread newThread(final Runnable runnable) {
                        return new Thread(new Runnable() {
                            public void run() {
                                Process.setThreadPriority(10);
                                runnable.run();
                            }
                        }, "ClearcutLoggerApiImpl");
                    }
                });
            }
        }
        return qF;
    }

    static /* synthetic */ long zzb(zzpb zzpb) {
        return 0;
    }

    private zzd zzb(GoogleApiClient googleApiClient, LogEventParcelable logEventParcelable) {
        qG.increment();
        zzd zzd2 = new zzd(logEventParcelable, googleApiClient);
        zzd2.zza((com.google.android.gms.common.api.PendingResult.zza) new com.google.android.gms.common.api.PendingResult.zza() {
            public void zzv(Status status) {
                zzpb.qG.decrement();
            }
        });
        return zzd2;
    }

    public PendingResult<Status> zza(GoogleApiClient googleApiClient, LogEventParcelable logEventParcelable) {
        return zza(googleApiClient, (zzc<Status>) zzb(googleApiClient, logEventParcelable));
    }
}
