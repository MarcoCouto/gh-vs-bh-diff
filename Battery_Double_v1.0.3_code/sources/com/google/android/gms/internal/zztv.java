package com.google.android.gms.internal;

import android.os.RemoteException;

public abstract class zztv<T> {
    private final int zzaxo;
    private final String zzaxp;
    private final T zzaxq;

    public static class zza extends zztv<Boolean> {
        public zza(int i, String str, Boolean bool) {
            super(i, str, bool);
        }

        /* renamed from: zzb */
        public Boolean zza(zzty zzty) {
            try {
                return Boolean.valueOf(zzty.getBooleanFlagValue(getKey(), ((Boolean) zzjw()).booleanValue(), getSource()));
            } catch (RemoteException unused) {
                return (Boolean) zzjw();
            }
        }
    }

    public static class zzb extends zztv<Integer> {
        public zzb(int i, String str, Integer num) {
            super(i, str, num);
        }

        /* renamed from: zzc */
        public Integer zza(zzty zzty) {
            try {
                return Integer.valueOf(zzty.getIntFlagValue(getKey(), ((Integer) zzjw()).intValue(), getSource()));
            } catch (RemoteException unused) {
                return (Integer) zzjw();
            }
        }
    }

    public static class zzc extends zztv<Long> {
        public zzc(int i, String str, Long l) {
            super(i, str, l);
        }

        /* renamed from: zzd */
        public Long zza(zzty zzty) {
            try {
                return Long.valueOf(zzty.getLongFlagValue(getKey(), ((Long) zzjw()).longValue(), getSource()));
            } catch (RemoteException unused) {
                return (Long) zzjw();
            }
        }
    }

    public static class zzd extends zztv<String> {
        public zzd(int i, String str, String str2) {
            super(i, str, str2);
        }

        /* renamed from: zze */
        public String zza(zzty zzty) {
            try {
                return zzty.getStringFlagValue(getKey(), (String) zzjw(), getSource());
            } catch (RemoteException unused) {
                return (String) zzjw();
            }
        }
    }

    private zztv(int i, String str, T t) {
        this.zzaxo = i;
        this.zzaxp = str;
        this.zzaxq = t;
        zztz.zzbet().zza(this);
    }

    public static zza zzb(int i, String str, Boolean bool) {
        return new zza(i, str, bool);
    }

    public static zzb zzb(int i, String str, int i2) {
        return new zzb(i, str, Integer.valueOf(i2));
    }

    public static zzc zzb(int i, String str, long j) {
        return new zzc(i, str, Long.valueOf(j));
    }

    public static zzd zzc(int i, String str, String str2) {
        return new zzd(i, str, str2);
    }

    public T get() {
        return zztz.zzbeu().zzb(this);
    }

    public String getKey() {
        return this.zzaxp;
    }

    public int getSource() {
        return this.zzaxo;
    }

    /* access modifiers changed from: protected */
    public abstract T zza(zzty zzty);

    public T zzjw() {
        return this.zzaxq;
    }
}
