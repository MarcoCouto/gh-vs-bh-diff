package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzab;
import java.util.Map;

@zzin
public class zzfs {
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Object zzail;
    /* access modifiers changed from: private */
    public final VersionInfoParcel zzalo;
    /* access modifiers changed from: private */
    public final String zzblr;
    /* access modifiers changed from: private */
    public zzkl<zzfp> zzbls;
    private zzkl<zzfp> zzblt;
    /* access modifiers changed from: private */
    @Nullable
    public zzd zzblu;
    /* access modifiers changed from: private */
    public int zzblv;

    static class zza {
        static int zzbmg = 60000;
        static int zzbmh = 10000;
    }

    public static class zzb<T> implements zzkl<T> {
        public void zzd(T t) {
        }
    }

    public static class zzc extends zzlb<zzft> {
        private final Object zzail = new Object();
        /* access modifiers changed from: private */
        public final zzd zzbmi;
        private boolean zzbmj;

        public zzc(zzd zzd) {
            this.zzbmi = zzd;
        }

        public void release() {
            synchronized (this.zzail) {
                if (!this.zzbmj) {
                    this.zzbmj = true;
                    zza(new com.google.android.gms.internal.zzla.zzc<zzft>() {
                        /* renamed from: zzb */
                        public void zzd(zzft zzft) {
                            zzkd.v("Ending javascript session.");
                            ((zzfu) zzft).zzmf();
                        }
                    }, new com.google.android.gms.internal.zzla.zzb());
                    zza(new com.google.android.gms.internal.zzla.zzc<zzft>() {
                        /* renamed from: zzb */
                        public void zzd(zzft zzft) {
                            zzkd.v("Releasing engine reference.");
                            zzc.this.zzbmi.zzmc();
                        }
                    }, new com.google.android.gms.internal.zzla.zza() {
                        public void run() {
                            zzc.this.zzbmi.zzmc();
                        }
                    });
                }
            }
        }
    }

    public static class zzd extends zzlb<zzfp> {
        private final Object zzail = new Object();
        /* access modifiers changed from: private */
        public zzkl<zzfp> zzblt;
        private boolean zzbml;
        private int zzbmm;

        public zzd(zzkl<zzfp> zzkl) {
            this.zzblt = zzkl;
            this.zzbml = false;
            this.zzbmm = 0;
        }

        public zzc zzmb() {
            final zzc zzc = new zzc(this);
            synchronized (this.zzail) {
                zza(new com.google.android.gms.internal.zzla.zzc<zzfp>() {
                    /* renamed from: zza */
                    public void zzd(zzfp zzfp) {
                        zzkd.v("Getting a new session for JS Engine.");
                        zzc.zzg(zzfp.zzly());
                    }
                }, new com.google.android.gms.internal.zzla.zza() {
                    public void run() {
                        zzkd.v("Rejecting reference for JS Engine.");
                        zzc.reject();
                    }
                });
                zzab.zzbn(this.zzbmm >= 0);
                this.zzbmm++;
            }
            return zzc;
        }

        /* access modifiers changed from: protected */
        public void zzmc() {
            synchronized (this.zzail) {
                zzab.zzbn(this.zzbmm >= 1);
                zzkd.v("Releasing 1 reference for JS Engine");
                this.zzbmm--;
                zzme();
            }
        }

        public void zzmd() {
            synchronized (this.zzail) {
                zzab.zzbn(this.zzbmm >= 0);
                zzkd.v("Releasing root reference. JS Engine will be destroyed once other references are released.");
                this.zzbml = true;
                zzme();
            }
        }

        /* access modifiers changed from: protected */
        public void zzme() {
            synchronized (this.zzail) {
                zzab.zzbn(this.zzbmm >= 0);
                if (!this.zzbml || this.zzbmm != 0) {
                    zzkd.v("There are still references to the engine. Not destroying.");
                } else {
                    zzkd.v("No reference is left (including root). Cleaning up engine.");
                    zza(new com.google.android.gms.internal.zzla.zzc<zzfp>() {
                        /* renamed from: zza */
                        public void zzd(final zzfp zzfp) {
                            zzu.zzfq().runOnUiThread(new Runnable() {
                                public void run() {
                                    zzd.this.zzblt.zzd(zzfp);
                                    zzfp.destroy();
                                }
                            });
                        }
                    }, new com.google.android.gms.internal.zzla.zzb());
                }
            }
        }
    }

    public static class zze extends zzlb<zzft> {
        private zzc zzbmr;

        public zze(zzc zzc) {
            this.zzbmr = zzc;
        }

        public void finalize() {
            this.zzbmr.release();
            this.zzbmr = null;
        }

        public int getStatus() {
            return this.zzbmr.getStatus();
        }

        public void reject() {
            this.zzbmr.reject();
        }

        public void zza(com.google.android.gms.internal.zzla.zzc<zzft> zzc, com.google.android.gms.internal.zzla.zza zza) {
            this.zzbmr.zza(zzc, zza);
        }

        /* renamed from: zzf */
        public void zzg(zzft zzft) {
            this.zzbmr.zzg(zzft);
        }
    }

    public zzfs(Context context, VersionInfoParcel versionInfoParcel, String str) {
        this.zzail = new Object();
        this.zzblv = 1;
        this.zzblr = str;
        this.mContext = context.getApplicationContext();
        this.zzalo = versionInfoParcel;
        this.zzbls = new zzb();
        this.zzblt = new zzb();
    }

    public zzfs(Context context, VersionInfoParcel versionInfoParcel, String str, zzkl<zzfp> zzkl, zzkl<zzfp> zzkl2) {
        this(context, versionInfoParcel, str);
        this.zzbls = zzkl;
        this.zzblt = zzkl2;
    }

    private zzd zza(@Nullable final zzas zzas) {
        final zzd zzd2 = new zzd(this.zzblt);
        zzu.zzfq().runOnUiThread(new Runnable() {
            public void run() {
                final zzfp zza = zzfs.this.zza(zzfs.this.mContext, zzfs.this.zzalo, zzas);
                zza.zza(new com.google.android.gms.internal.zzfp.zza() {
                    public void zzlz() {
                        zzkh.zzclc.postDelayed(new Runnable() {
                            /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
                                return;
                             */
                            public void run() {
                                synchronized (zzfs.this.zzail) {
                                    if (zzd2.getStatus() != -1) {
                                        if (zzd2.getStatus() != 1) {
                                            zzd2.reject();
                                            zzu.zzfq().runOnUiThread(new Runnable() {
                                                public void run() {
                                                    zza.destroy();
                                                }
                                            });
                                            zzkd.v("Could not receive loaded message in a timely manner. Rejecting.");
                                        }
                                    }
                                }
                            }
                        }, (long) zza.zzbmh);
                    }
                });
                zza.zza("/jsLoaded", (zzep) new zzep() {
                    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0051, code lost:
                        return;
                     */
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        synchronized (zzfs.this.zzail) {
                            if (zzd2.getStatus() != -1) {
                                if (zzd2.getStatus() != 1) {
                                    zzfs.this.zzblv = 0;
                                    zzfs.this.zzbls.zzd(zza);
                                    zzd2.zzg(zza);
                                    zzfs.this.zzblu = zzd2;
                                    zzkd.v("Successfully loaded JS Engine.");
                                }
                            }
                        }
                    }
                });
                final zzks zzks = new zzks();
                AnonymousClass3 r2 = new zzep() {
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        synchronized (zzfs.this.zzail) {
                            zzkd.zzcw("JS Engine is requesting an update");
                            if (zzfs.this.zzblv == 0) {
                                zzkd.zzcw("Starting reload.");
                                zzfs.this.zzblv = 2;
                                zzfs.this.zzb(zzas);
                            }
                            zza.zzb("/requestReload", (zzep) zzks.get());
                        }
                    }
                };
                zzks.set(r2);
                zza.zza("/requestReload", (zzep) r2);
                if (zzfs.this.zzblr.endsWith(".js")) {
                    zza.zzbg(zzfs.this.zzblr);
                } else if (zzfs.this.zzblr.startsWith("<html>")) {
                    zza.zzbi(zzfs.this.zzblr);
                } else {
                    zza.zzbh(zzfs.this.zzblr);
                }
                zzkh.zzclc.postDelayed(new Runnable() {
                    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
                        return;
                     */
                    public void run() {
                        synchronized (zzfs.this.zzail) {
                            if (zzd2.getStatus() != -1) {
                                if (zzd2.getStatus() != 1) {
                                    zzd2.reject();
                                    zzu.zzfq().runOnUiThread(new Runnable() {
                                        public void run() {
                                            zza.destroy();
                                        }
                                    });
                                    zzkd.v("Could not receive loaded message in a timely manner. Rejecting.");
                                }
                            }
                        }
                    }
                }, (long) zza.zzbmg);
            }
        });
        return zzd2;
    }

    /* access modifiers changed from: protected */
    public zzfp zza(Context context, VersionInfoParcel versionInfoParcel, @Nullable zzas zzas) {
        return new zzfr(context, versionInfoParcel, zzas);
    }

    /* access modifiers changed from: protected */
    public zzd zzb(@Nullable zzas zzas) {
        final zzd zza2 = zza(zzas);
        zza2.zza(new com.google.android.gms.internal.zzla.zzc<zzfp>() {
            /* renamed from: zza */
            public void zzd(zzfp zzfp) {
                synchronized (zzfs.this.zzail) {
                    zzfs.this.zzblv = 0;
                    if (!(zzfs.this.zzblu == null || zza2 == zzfs.this.zzblu)) {
                        zzkd.v("New JS engine is loaded, marking previous one as destroyable.");
                        zzfs.this.zzblu.zzmd();
                    }
                    zzfs.this.zzblu = zza2;
                }
            }
        }, new com.google.android.gms.internal.zzla.zza() {
            public void run() {
                synchronized (zzfs.this.zzail) {
                    zzfs.this.zzblv = 1;
                    zzkd.v("Failed loading new engine. Marking new engine destroyable.");
                    zza2.zzmd();
                }
            }
        });
        return zza2;
    }

    public zzc zzc(@Nullable zzas zzas) {
        synchronized (this.zzail) {
            if (this.zzblu != null) {
                if (this.zzblu.getStatus() != -1) {
                    if (this.zzblv == 0) {
                        zzc zzmb = this.zzblu.zzmb();
                        return zzmb;
                    } else if (this.zzblv == 1) {
                        this.zzblv = 2;
                        zzb(zzas);
                        zzc zzmb2 = this.zzblu.zzmb();
                        return zzmb2;
                    } else if (this.zzblv == 2) {
                        zzc zzmb3 = this.zzblu.zzmb();
                        return zzmb3;
                    } else {
                        zzc zzmb4 = this.zzblu.zzmb();
                        return zzmb4;
                    }
                }
            }
            this.zzblv = 2;
            this.zzblu = zzb(zzas);
            zzc zzmb5 = this.zzblu.zzmb();
            return zzmb5;
        }
    }

    public zzc zzma() {
        return zzc((zzas) null);
    }
}
