package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzl;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzab;
import java.util.Iterator;
import java.util.LinkedList;

@zzin
class zzfm {
    /* access modifiers changed from: private */
    public final String zzaln;
    private final LinkedList<zza> zzbkr = new LinkedList<>();
    /* access modifiers changed from: private */
    public AdRequestParcel zzbks;
    private final int zzbkt;
    private boolean zzbku;

    class zza {
        zzl zzbkv;
        @Nullable
        AdRequestParcel zzbkw;
        zzfi zzbkx;
        long zzbky;
        boolean zzbkz;
        boolean zzbla;

        zza(zzfh zzfh) {
            this.zzbkv = zzfh.zzbd(zzfm.this.zzaln);
            this.zzbkx = new zzfi();
            this.zzbkx.zzc(this.zzbkv);
        }

        zza(zzfm zzfm, zzfh zzfh, AdRequestParcel adRequestParcel) {
            this(zzfh);
            this.zzbkw = adRequestParcel;
        }

        /* access modifiers changed from: 0000 */
        public void zzlv() {
            if (!this.zzbkz) {
                this.zzbla = this.zzbkv.zzb(zzfk.zzj(this.zzbkw != null ? this.zzbkw : zzfm.this.zzbks));
                this.zzbkz = true;
                this.zzbky = zzu.zzfu().currentTimeMillis();
            }
        }
    }

    zzfm(AdRequestParcel adRequestParcel, String str, int i) {
        zzab.zzy(adRequestParcel);
        zzab.zzy(str);
        this.zzbks = adRequestParcel;
        this.zzaln = str;
        this.zzbkt = i;
    }

    /* access modifiers changed from: 0000 */
    public String getAdUnitId() {
        return this.zzaln;
    }

    /* access modifiers changed from: 0000 */
    public int getNetworkType() {
        return this.zzbkt;
    }

    /* access modifiers changed from: 0000 */
    public int size() {
        return this.zzbkr.size();
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzfh zzfh, AdRequestParcel adRequestParcel) {
        this.zzbkr.add(new zza(this, zzfh, adRequestParcel));
    }

    /* access modifiers changed from: 0000 */
    public void zzb(zzfh zzfh) {
        zza zza2 = new zza(zzfh);
        this.zzbkr.add(zza2);
        zza2.zzlv();
    }

    /* access modifiers changed from: 0000 */
    public AdRequestParcel zzlq() {
        return this.zzbks;
    }

    /* access modifiers changed from: 0000 */
    public int zzlr() {
        Iterator it = this.zzbkr.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (((zza) it.next()).zzbkz) {
                i++;
            }
        }
        return i;
    }

    /* access modifiers changed from: 0000 */
    public void zzls() {
        Iterator it = this.zzbkr.iterator();
        while (it.hasNext()) {
            ((zza) it.next()).zzlv();
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzlt() {
        this.zzbku = true;
    }

    /* access modifiers changed from: 0000 */
    public boolean zzlu() {
        return this.zzbku;
    }

    /* access modifiers changed from: 0000 */
    public zza zzm(@Nullable AdRequestParcel adRequestParcel) {
        if (adRequestParcel != null) {
            this.zzbks = adRequestParcel;
        }
        return (zza) this.zzbkr.remove();
    }
}
