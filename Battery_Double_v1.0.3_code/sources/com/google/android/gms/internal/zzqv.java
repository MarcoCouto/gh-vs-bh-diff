package com.google.android.gms.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public final class zzqv extends Fragment implements zzqk {
    private static WeakHashMap<FragmentActivity, WeakReference<zzqv>> vn = new WeakHashMap<>();
    private Map<String, zzqj> vo = new ArrayMap();
    /* access modifiers changed from: private */
    public Bundle vp;
    /* access modifiers changed from: private */
    public int zzblv = 0;

    public static zzqv zza(FragmentActivity fragmentActivity) {
        WeakReference weakReference = (WeakReference) vn.get(fragmentActivity);
        if (weakReference != null) {
            zzqv zzqv = (zzqv) weakReference.get();
            if (zzqv != null) {
                return zzqv;
            }
        }
        try {
            zzqv zzqv2 = (zzqv) fragmentActivity.getSupportFragmentManager().findFragmentByTag("SupportLifecycleFragmentImpl");
            if (zzqv2 == null || zzqv2.isRemoving()) {
                zzqv2 = new zzqv();
                fragmentActivity.getSupportFragmentManager().beginTransaction().add((Fragment) zzqv2, "SupportLifecycleFragmentImpl").commitAllowingStateLoss();
            }
            vn.put(fragmentActivity, new WeakReference(zzqv2));
            return zzqv2;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e);
        }
    }

    private void zzb(final String str, @NonNull final zzqj zzqj) {
        if (this.zzblv > 0) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (zzqv.this.zzblv >= 1) {
                        zzqj.onCreate(zzqv.this.vp != null ? zzqv.this.vp.getBundle(str) : null);
                    }
                    if (zzqv.this.zzblv >= 2) {
                        zzqj.onStart();
                    }
                    if (zzqv.this.zzblv >= 3) {
                        zzqj.onStop();
                    }
                }
            });
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (zzqj dump : this.vo.values()) {
            dump.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (zzqj onActivityResult : this.vo.values()) {
            onActivityResult.onActivityResult(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzblv = 1;
        this.vp = bundle;
        for (Entry entry : this.vo.entrySet()) {
            ((zzqj) entry.getValue()).onCreate(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Entry entry : this.vo.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((zzqj) entry.getValue()).onSaveInstanceState(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public void onStart() {
        super.onStop();
        this.zzblv = 2;
        for (zzqj onStart : this.vo.values()) {
            onStart.onStart();
        }
    }

    public void onStop() {
        super.onStop();
        this.zzblv = 3;
        for (zzqj onStop : this.vo.values()) {
            onStop.onStop();
        }
    }

    public <T extends zzqj> T zza(String str, Class<T> cls) {
        return (zzqj) cls.cast(this.vo.get(str));
    }

    public void zza(String str, @NonNull zzqj zzqj) {
        if (!this.vo.containsKey(str)) {
            this.vo.put(str, zzqj);
            zzb(str, zzqj);
            return;
        }
        StringBuilder sb = new StringBuilder(59 + String.valueOf(str).length());
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    /* renamed from: zzaqv */
    public FragmentActivity zzaqt() {
        return getActivity();
    }
}
