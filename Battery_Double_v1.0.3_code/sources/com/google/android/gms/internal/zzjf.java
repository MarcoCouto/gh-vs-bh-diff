package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.ads.mediation.AbstractAdViewAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzb;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzju.zza;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzjf extends zzb implements zzji {
    private static final zzgi zzchg = new zzgi();
    private final Map<String, zzjm> zzchh = new HashMap();
    private boolean zzchi;

    public zzjf(Context context, zzd zzd, AdSizeParcel adSizeParcel, zzgj zzgj, VersionInfoParcel versionInfoParcel) {
        super(context, adSizeParcel, null, zzgj, versionInfoParcel, zzd);
    }

    private zza zze(zza zza) {
        zza zza2 = zza;
        zzkd.v("Creating mediation ad response for non-mediated rewarded ad.");
        try {
            String jSONObject = zziq.zzc(zza2.zzciq).toString();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(AbstractAdViewAdapter.AD_UNIT_ID_PARAMETER, zza2.zzcip.zzaou);
            zzfz zzfz = new zzfz(jSONObject, null, Arrays.asList(new String[]{"com.google.ads.mediation.admob.AdMobAdapter"}), null, null, Collections.emptyList(), Collections.emptyList(), jSONObject2.toString(), null, Collections.emptyList(), Collections.emptyList(), null, null, null, null, null, Collections.emptyList());
            zzga zzga = new zzga(Arrays.asList(new zzfz[]{zzfz}), -1, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false, "", -1, 0, 1, null, 0, -1, -1, false);
            zza zza3 = new zza(zza2.zzcip, zza2.zzciq, zzga, zza2.zzapa, zza2.errorCode, zza2.zzcik, zza2.zzcil, zza2.zzcie);
            return zza3;
        } catch (JSONException e) {
            zzkd.zzb("Unable to generate ad state for non-mediated rewarded video.", e);
            return zzf(zza);
        }
    }

    private zza zzf(zza zza) {
        zza zza2 = new zza(zza.zzcip, zza.zzciq, null, zza.zzapa, 0, zza.zzcik, zza.zzcil, zza.zzcie);
        return zza2;
    }

    public void destroy() {
        zzab.zzhi("destroy must be called on the main UI thread.");
        for (String str : this.zzchh.keySet()) {
            try {
                zzjm zzjm = (zzjm) this.zzchh.get(str);
                if (!(zzjm == null || zzjm.zzru() == null)) {
                    zzjm.zzru().destroy();
                }
            } catch (RemoteException unused) {
                String str2 = "Fail to destroy adapter: ";
                String valueOf = String.valueOf(str);
                zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
        }
    }

    public boolean isLoaded() {
        zzab.zzhi("isLoaded must be called on the main UI thread.");
        return this.zzajs.zzaoy == null && this.zzajs.zzaoz == null && this.zzajs.zzapb != null && !this.zzchi;
    }

    public void onContextChanged(@NonNull Context context) {
        for (zzjm zzru : this.zzchh.values()) {
            try {
                zzru.zzru().zzj(zze.zzac(context));
            } catch (RemoteException e) {
                zzkd.zzb("Unable to call Adapter.onContextChanged.", e);
            }
        }
    }

    public void onRewardedVideoAdClosed() {
        zzdr();
    }

    public void onRewardedVideoAdLeftApplication() {
        zzds();
    }

    public void onRewardedVideoAdOpened() {
        zza(this.zzajs.zzapb, false);
        zzdt();
    }

    public void onRewardedVideoStarted() {
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzbon == null)) {
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb, this.zzajs.zzaou, false, this.zzajs.zzapb.zzbon.zzbnd);
        }
        zzdv();
    }

    public void pause() {
        zzab.zzhi("pause must be called on the main UI thread.");
        for (String str : this.zzchh.keySet()) {
            try {
                zzjm zzjm = (zzjm) this.zzchh.get(str);
                if (!(zzjm == null || zzjm.zzru() == null)) {
                    zzjm.zzru().pause();
                }
            } catch (RemoteException unused) {
                String str2 = "Fail to pause adapter: ";
                String valueOf = String.valueOf(str);
                zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
        }
    }

    public void resume() {
        zzab.zzhi("resume must be called on the main UI thread.");
        for (String str : this.zzchh.keySet()) {
            try {
                zzjm zzjm = (zzjm) this.zzchh.get(str);
                if (!(zzjm == null || zzjm.zzru() == null)) {
                    zzjm.zzru().resume();
                }
            } catch (RemoteException unused) {
                String str2 = "Fail to resume adapter: ";
                String valueOf = String.valueOf(str);
                zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
        }
    }

    public void zza(RewardedVideoAdRequestParcel rewardedVideoAdRequestParcel) {
        zzab.zzhi("loadAd must be called on the main UI thread.");
        if (TextUtils.isEmpty(rewardedVideoAdRequestParcel.zzaou)) {
            zzkd.zzcx("Invalid ad unit id. Aborting.");
            return;
        }
        this.zzchi = false;
        this.zzajs.zzaou = rewardedVideoAdRequestParcel.zzaou;
        super.zzb(rewardedVideoAdRequestParcel.zzcar);
    }

    public void zza(final zza zza, zzdk zzdk) {
        if (zza.errorCode != -2) {
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzjf zzjf = zzjf.this;
                    zzju zzju = new zzju(zza, null, null, null, null, null, null, null);
                    zzjf.zzb(zzju);
                }
            });
            return;
        }
        this.zzajs.zzapc = zza;
        if (zza.zzcig == null) {
            this.zzajs.zzapc = zze(zza);
        }
        this.zzajs.zzapw = 0;
        this.zzajs.zzaoz = zzu.zzfp().zza(this.zzajs.zzagf, this.zzajs.zzapc, this);
    }

    /* access modifiers changed from: protected */
    public boolean zza(AdRequestParcel adRequestParcel, zzju zzju, boolean z) {
        return false;
    }

    public boolean zza(zzju zzju, zzju zzju2) {
        return true;
    }

    public void zzc(@Nullable RewardItemParcel rewardItemParcel) {
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzbon == null)) {
            zzu.zzgf().zza(this.zzajs.zzagf, this.zzajs.zzaow.zzcs, this.zzajs.zzapb, this.zzajs.zzaou, false, this.zzajs.zzapb.zzbon.zzbne);
        }
        if (!(this.zzajs.zzapb == null || this.zzajs.zzapb.zzcig == null || TextUtils.isEmpty(this.zzajs.zzapb.zzcig.zzbnt))) {
            rewardItemParcel = new RewardItemParcel(this.zzajs.zzapb.zzcig.zzbnt, this.zzajs.zzapb.zzcig.zzbnu);
        }
        zza(rewardItemParcel);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b  */
    @Nullable
    public zzjm zzcf(String str) {
        zzjm zzjm = (zzjm) this.zzchh.get(str);
        if (zzjm == null) {
            try {
                zzgj zzgj = this.zzajz;
                if ("com.google.ads.mediation.admob.AdMobAdapter".equals(str)) {
                    zzgj = zzchg;
                }
                zzjm zzjm2 = new zzjm(zzgj.zzbm(str), this);
                try {
                    this.zzchh.put(str, zzjm2);
                    return zzjm2;
                } catch (Exception e) {
                    e = e;
                    zzjm = zzjm2;
                    String str2 = "Fail to instantiate adapter ";
                    String valueOf = String.valueOf(str);
                    zzkd.zzd(valueOf.length() == 0 ? str2.concat(valueOf) : new String(str2), e);
                    return zzjm;
                }
            } catch (Exception e2) {
                e = e2;
                String str22 = "Fail to instantiate adapter ";
                String valueOf2 = String.valueOf(str);
                zzkd.zzd(valueOf2.length() == 0 ? str22.concat(valueOf2) : new String(str22), e);
                return zzjm;
            }
        }
        return zzjm;
    }

    public void zzrq() {
        zzab.zzhi("showAd must be called on the main UI thread.");
        if (!isLoaded()) {
            zzkd.zzcx("The reward video has not loaded.");
            return;
        }
        this.zzchi = true;
        zzjm zzcf = zzcf(this.zzajs.zzapb.zzbop);
        if (!(zzcf == null || zzcf.zzru() == null)) {
            try {
                zzcf.zzru().showVideo();
            } catch (RemoteException e) {
                zzkd.zzd("Could not call showVideo.", e);
            }
        }
    }

    public void zzrr() {
        onAdClicked();
    }
}
