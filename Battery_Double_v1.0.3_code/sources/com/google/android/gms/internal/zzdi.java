package com.google.android.gms.internal;

import android.support.annotation.Nullable;

@zzin
public class zzdi {
    private final long zzbeb;
    @Nullable
    private final String zzbec;
    @Nullable
    private final zzdi zzbed;

    public zzdi(long j, @Nullable String str, @Nullable zzdi zzdi) {
        this.zzbeb = j;
        this.zzbec = str;
        this.zzbed = zzdi;
    }

    /* access modifiers changed from: 0000 */
    public long getTime() {
        return this.zzbeb;
    }

    /* access modifiers changed from: 0000 */
    public String zzkd() {
        return this.zzbec;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public zzdi zzke() {
        return this.zzbed;
    }
}
