package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzju.zza;

@zzin
public class zzjg extends zzkc implements zzjh, zzjk {
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Object zzail;
    /* access modifiers changed from: private */
    public final String zzboc;
    private final zza zzbxr;
    private int zzbyi = 3;
    private final zzjm zzchm;
    private final zzjk zzchn;
    /* access modifiers changed from: private */
    public final String zzcho;
    private final String zzchp;
    private int zzchq = 0;

    public zzjg(Context context, String str, String str2, String str3, zza zza, zzjm zzjm, zzjk zzjk) {
        this.mContext = context;
        this.zzboc = str;
        this.zzcho = str2;
        this.zzchp = str3;
        this.zzbxr = zza;
        this.zzchm = zzjm;
        this.zzail = new Object();
        this.zzchn = zzjk;
    }

    /* access modifiers changed from: private */
    public void zza(AdRequestParcel adRequestParcel, zzgk zzgk) {
        try {
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzboc)) {
                zzgk.zza(adRequestParcel, this.zzcho, this.zzchp);
            } else {
                zzgk.zzc(adRequestParcel, this.zzcho);
            }
        } catch (RemoteException e) {
            zzkd.zzd("Fail to load ad from adapter.", e);
            zza(this.zzboc, 0);
        }
    }

    private void zzk(long j) {
        while (true) {
            synchronized (this.zzail) {
                if (this.zzchq == 0) {
                    if (!zzf(j)) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }

    public void onStop() {
    }

    public void zza(String str, int i) {
        synchronized (this.zzail) {
            this.zzchq = 2;
            this.zzbyi = i;
            this.zzail.notify();
        }
    }

    public void zzaw(int i) {
        zza(this.zzboc, 0);
    }

    public void zzcg(String str) {
        synchronized (this.zzail) {
            this.zzchq = 1;
            this.zzail.notify();
        }
    }

    public void zzew() {
        Handler handler;
        Runnable r4;
        if (this.zzchm != null && this.zzchm.zzrv() != null && this.zzchm.zzru() != null) {
            final zzjj zzrv = this.zzchm.zzrv();
            zzrv.zza((zzjk) this);
            zzrv.zza((zzjh) this);
            final AdRequestParcel adRequestParcel = this.zzbxr.zzcip.zzcar;
            final zzgk zzru = this.zzchm.zzru();
            try {
                if (zzru.isInitialized()) {
                    handler = com.google.android.gms.ads.internal.util.client.zza.zzcnb;
                    r4 = new Runnable() {
                        public void run() {
                            zzjg.this.zza(adRequestParcel, zzru);
                        }
                    };
                } else {
                    handler = com.google.android.gms.ads.internal.util.client.zza.zzcnb;
                    r4 = new Runnable() {
                        public void run() {
                            try {
                                zzru.zza(zze.zzac(zzjg.this.mContext), adRequestParcel, (String) null, (com.google.android.gms.ads.internal.reward.mediation.client.zza) zzrv, zzjg.this.zzcho);
                            } catch (RemoteException e) {
                                String str = "Fail to initialize adapter ";
                                String valueOf = String.valueOf(zzjg.this.zzboc);
                                zzkd.zzd(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), e);
                                zzjg.this.zza(zzjg.this.zzboc, 0);
                            }
                        }
                    };
                }
                handler.post(r4);
            } catch (RemoteException e) {
                zzkd.zzd("Fail to check if adapter is initialized.", e);
                zza(this.zzboc, 0);
            }
            zzk(zzu.zzfu().elapsedRealtime());
            zzrv.zza((zzjk) null);
            zzrv.zza((zzjh) null);
            if (this.zzchq == 1) {
                this.zzchn.zzcg(this.zzboc);
            } else {
                this.zzchn.zza(this.zzboc, this.zzbyi);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzf(long j) {
        long elapsedRealtime = 20000 - (zzu.zzfu().elapsedRealtime() - j);
        boolean z = false;
        if (elapsedRealtime <= 0) {
            return false;
        }
        try {
            this.zzail.wait(elapsedRealtime);
            z = true;
        } catch (InterruptedException unused) {
        }
        return z;
    }

    public void zzrs() {
        zza(this.zzbxr.zzcip.zzcar, this.zzchm.zzru());
    }
}
