package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;

@zzin
public abstract class zzix {
    public abstract void zza(Context context, zzir zzir, VersionInfoParcel versionInfoParcel);

    /* access modifiers changed from: protected */
    public void zze(zzir zzir) {
        zzir.zzri();
        if (zzir.zzrg() != null) {
            zzir.zzrg().release();
        }
    }
}
