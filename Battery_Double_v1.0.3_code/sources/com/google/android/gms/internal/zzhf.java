package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzhf {
    private final zzlh zzbgf;
    private final String zzbrm;

    public zzhf(zzlh zzlh) {
        this(zzlh, "");
    }

    public zzhf(zzlh zzlh, String str) {
        this.zzbgf = zzlh;
        this.zzbrm = str;
    }

    public void zza(int i, int i2, int i3, int i4, float f, int i5) {
        try {
            this.zzbgf.zzb("onScreenInfoChanged", new JSONObject().put("width", i).put("height", i2).put("maxSizeWidth", i3).put("maxSizeHeight", i4).put("density", (double) f).put("rotation", i5));
        } catch (JSONException e) {
            zzkd.zzb("Error occured while obtaining screen information.", e);
        }
    }

    public void zzb(int i, int i2, int i3, int i4) {
        try {
            this.zzbgf.zzb("onSizeChanged", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            zzkd.zzb("Error occured while dispatching size change.", e);
        }
    }

    public void zzbt(String str) {
        try {
            this.zzbgf.zzb("onError", new JSONObject().put("message", str).put("action", this.zzbrm));
        } catch (JSONException e) {
            zzkd.zzb("Error occurred while dispatching error event.", e);
        }
    }

    public void zzbu(String str) {
        try {
            this.zzbgf.zzb("onReadyEventReceived", new JSONObject().put("js", str));
        } catch (JSONException e) {
            zzkd.zzb("Error occured while dispatching ready Event.", e);
        }
    }

    public void zzbv(String str) {
        try {
            this.zzbgf.zzb("onStateChanged", new JSONObject().put("state", str));
        } catch (JSONException e) {
            zzkd.zzb("Error occured while dispatching state change.", e);
        }
    }

    public void zzc(int i, int i2, int i3, int i4) {
        try {
            this.zzbgf.zzb("onDefaultPositionReceived", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            zzkd.zzb("Error occured while dispatching default position.", e);
        }
    }
}
