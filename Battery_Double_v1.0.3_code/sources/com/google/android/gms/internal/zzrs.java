package com.google.android.gms.internal;

import com.google.android.gms.common.api.CommonStatusCodes;

public final class zzrs extends CommonStatusCodes {
    public static String getStatusCodeString(int i) {
        if (i == -6508) {
            return "SUCCESS_CACHE_STALE";
        }
        if (i == 6507) {
            return "FETCH_THROTTLED_STALE";
        }
        switch (i) {
            case -6506:
                return "SUCCESS_CACHE";
            case -6505:
                return "SUCCESS_FRESH";
            default:
                switch (i) {
                    case 6500:
                        return "NOT_AUTHORIZED_TO_FETCH";
                    case 6501:
                        return "ANOTHER_FETCH_INFLIGHT";
                    case 6502:
                        return "FETCH_THROTTLED";
                    case 6503:
                        return "NOT_AVAILABLE";
                    case 6504:
                        return "FAILURE_CACHE";
                    default:
                        return CommonStatusCodes.getStatusCodeString(i);
                }
        }
    }
}
