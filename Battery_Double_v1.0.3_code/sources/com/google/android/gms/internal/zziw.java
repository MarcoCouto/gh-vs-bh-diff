package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzu;
import java.util.WeakHashMap;

@zzin
public final class zziw {
    private WeakHashMap<Context, zza> zzcha = new WeakHashMap<>();

    private class zza {
        public final long zzchb = zzu.zzfu().currentTimeMillis();
        public final zziv zzchc;

        public zza(zziv zziv) {
            this.zzchc = zziv;
        }

        public boolean hasExpired() {
            return this.zzchb + ((Long) zzdc.zzbat.get()).longValue() < zzu.zzfu().currentTimeMillis();
        }
    }

    public zziv zzy(Context context) {
        zza zza2 = (zza) this.zzcha.get(context);
        zziv zzrn = (zza2 == null || zza2.hasExpired() || !((Boolean) zzdc.zzbas.get()).booleanValue()) ? new com.google.android.gms.internal.zziv.zza(context).zzrn() : new com.google.android.gms.internal.zziv.zza(context, zza2.zzchc).zzrn();
        this.zzcha.put(context, new zza(zzrn));
        return zzrn;
    }
}
