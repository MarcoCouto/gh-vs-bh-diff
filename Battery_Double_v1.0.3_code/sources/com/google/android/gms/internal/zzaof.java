package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;

public final class zzaof extends zzanh<Object> {
    public static final zzani bfu = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() == Object.class) {
                return new zzaof(zzamp);
            }
            return null;
        }
    };
    private final zzamp beq;

    private zzaof(zzamp zzamp) {
        this.beq = zzamp;
    }

    public void zza(zzaoo zzaoo, Object obj) throws IOException {
        if (obj == null) {
            zzaoo.l();
            return;
        }
        zzanh zzk = this.beq.zzk(obj.getClass());
        if (zzk instanceof zzaof) {
            zzaoo.j();
            zzaoo.k();
            return;
        }
        zzk.zza(zzaoo, obj);
    }

    public Object zzb(zzaom zzaom) throws IOException {
        switch (zzaom.b()) {
            case BEGIN_ARRAY:
                ArrayList arrayList = new ArrayList();
                zzaom.beginArray();
                while (zzaom.hasNext()) {
                    arrayList.add(zzb(zzaom));
                }
                zzaom.endArray();
                return arrayList;
            case BEGIN_OBJECT:
                zzant zzant = new zzant();
                zzaom.beginObject();
                while (zzaom.hasNext()) {
                    zzant.put(zzaom.nextName(), zzb(zzaom));
                }
                zzaom.endObject();
                return zzant;
            case STRING:
                return zzaom.nextString();
            case NUMBER:
                return Double.valueOf(zzaom.nextDouble());
            case BOOLEAN:
                return Boolean.valueOf(zzaom.nextBoolean());
            case NULL:
                zzaom.nextNull();
                return null;
            default:
                throw new IllegalStateException();
        }
    }
}
