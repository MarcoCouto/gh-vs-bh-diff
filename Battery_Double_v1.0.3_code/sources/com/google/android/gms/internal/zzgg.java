package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@zzin
public class zzgg implements zzfy {
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();
    private final zzgj zzajz;
    private final boolean zzarl;
    private final boolean zzawn;
    private final zzga zzboe;
    private final AdRequestInfoParcel zzbot;
    /* access modifiers changed from: private */
    public final long zzbou;
    /* access modifiers changed from: private */
    public final long zzbov;
    private final int zzbow;
    /* access modifiers changed from: private */
    public boolean zzbox = false;
    /* access modifiers changed from: private */
    public final Map<zzky<zzge>, zzgd> zzboy = new HashMap();
    private List<zzge> zzboz = new ArrayList();

    public zzgg(Context context, AdRequestInfoParcel adRequestInfoParcel, zzgj zzgj, zzga zzga, boolean z, boolean z2, long j, long j2, int i) {
        this.mContext = context;
        this.zzbot = adRequestInfoParcel;
        this.zzajz = zzgj;
        this.zzboe = zzga;
        this.zzarl = z;
        this.zzawn = z2;
        this.zzbou = j;
        this.zzbov = j2;
        this.zzbow = i;
    }

    private void zza(final zzky<zzge> zzky) {
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                for (zzky zzky : zzgg.this.zzboy.keySet()) {
                    if (zzky != zzky) {
                        ((zzgd) zzgg.this.zzboy.get(zzky)).cancel();
                    }
                }
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r4.hasNext() == false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        r0 = (com.google.android.gms.internal.zzky) r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1 = (com.google.android.gms.internal.zzge) r0.get();
        r3.zzboz.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r1 == null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        if (r1.zzbom != 0) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0031, code lost:
        zza(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        com.google.android.gms.internal.zzkd.zzd("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003c, code lost:
        zza(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0046, code lost:
        return new com.google.android.gms.internal.zzge(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r4 = r4.iterator();
     */
    private zzge zze(List<zzky<zzge>> list) {
        synchronized (this.zzail) {
            if (this.zzbox) {
                zzge zzge = new zzge(-1);
                return zzge;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r14.zzboe.zzbnw == -1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r0 = r14.zzboe.zzbnw;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r0 = org.altbeacon.beacon.BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        r15 = r15.iterator();
        r3 = null;
        r4 = -1;
        r1 = r0;
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        if (r15.hasNext() == false) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        r5 = (com.google.android.gms.internal.zzky) r15.next();
        r6 = com.google.android.gms.ads.internal.zzu.zzfu().currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        if (r1 != 0) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        if (r5.isDone() == false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r10 = r5.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004d, code lost:
        r10 = (com.google.android.gms.internal.zzge) r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0050, code lost:
        r15 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0054, code lost:
        r10 = r5.get(r1, java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005b, code lost:
        r14.zzboz.add(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0060, code lost:
        if (r10 == null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0064, code lost:
        if (r10.zzbom != 0) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0066, code lost:
        r11 = r10.zzbor;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0068, code lost:
        if (r11 == null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006e, code lost:
        if (r11.zzmm() <= r4) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0074, code lost:
        r3 = r5;
        r0 = r10;
        r4 = r11.zzmm();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        com.google.android.gms.internal.zzkd.zzd("Exception while processing an adapter; continuing with other adapters", r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008e, code lost:
        java.lang.Math.max(r1 - (com.google.android.gms.ads.internal.zzu.zzfu().currentTimeMillis() - r6), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009d, code lost:
        throw r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009e, code lost:
        zza(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a1, code lost:
        if (r0 != null) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a9, code lost:
        return new com.google.android.gms.internal.zzge(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        return r0;
     */
    private zzge zzf(List<zzky<zzge>> list) {
        long currentTimeMillis;
        synchronized (this.zzail) {
            if (this.zzbox) {
                zzge zzge = new zzge(-1);
                return zzge;
            }
        }
        long j = Math.max(j - (zzu.zzfu().currentTimeMillis() - currentTimeMillis), 0);
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.zzbox = true;
            for (zzgd cancel : this.zzboy.values()) {
                cancel.cancel();
            }
        }
    }

    public zzge zzd(List<zzfz> list) {
        zzkd.zzcv("Starting mediation.");
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            zzfz zzfz = (zzfz) it.next();
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(zzfz.zzbmv);
            zzkd.zzcw(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            Iterator it2 = zzfz.zzbmw.iterator();
            while (it2.hasNext()) {
                String str2 = (String) it2.next();
                Context context = this.mContext;
                zzgj zzgj = this.zzajz;
                zzga zzga = this.zzboe;
                AdRequestParcel adRequestParcel = this.zzbot.zzcar;
                AdSizeParcel adSizeParcel = this.zzbot.zzapa;
                VersionInfoParcel versionInfoParcel = this.zzbot.zzaow;
                Iterator it3 = it;
                boolean z = this.zzarl;
                zzfz zzfz2 = zzfz;
                zzfz zzfz3 = zzfz;
                final zzgd zzgd = r5;
                boolean z2 = z;
                Iterator it4 = it2;
                zzgd zzgd2 = new zzgd(context, str2, zzgj, zzga, zzfz2, adRequestParcel, adSizeParcel, versionInfoParcel, z2, this.zzawn, this.zzbot.zzapo, this.zzbot.zzaps);
                zzky zza = zzkg.zza(newCachedThreadPool, (Callable<T>) new Callable<zzge>() {
                    /* renamed from: zzmn */
                    public zzge call() throws Exception {
                        synchronized (zzgg.this.zzail) {
                            if (zzgg.this.zzbox) {
                                return null;
                            }
                            return zzgd.zza(zzgg.this.zzbou, zzgg.this.zzbov);
                        }
                    }
                });
                this.zzboy.put(zza, zzgd);
                arrayList.add(zza);
                it = it3;
                it2 = it4;
                zzfz = zzfz3;
            }
        }
        return this.zzbow != 2 ? zze((List<zzky<zzge>>) arrayList) : zzf(arrayList);
    }

    public List<zzge> zzmg() {
        return this.zzboz;
    }
}
