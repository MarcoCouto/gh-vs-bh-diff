package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@zzin
public class zzdk {
    private final Object zzail = new Object();
    boolean zzbdo;
    private final List<zzdi> zzbef = new LinkedList();
    private final Map<String, String> zzbeg = new LinkedHashMap();
    private String zzbeh;
    private zzdi zzbei;
    @Nullable
    private zzdk zzbej;

    public zzdk(boolean z, String str, String str2) {
        this.zzbdo = z;
        this.zzbeg.put("action", str);
        this.zzbeg.put("ad_format", str2);
    }

    public boolean zza(zzdi zzdi, long j, String... strArr) {
        synchronized (this.zzail) {
            for (String zzdi2 : strArr) {
                this.zzbef.add(new zzdi(j, zzdi2, zzdi));
            }
        }
        return true;
    }

    public boolean zza(@Nullable zzdi zzdi, String... strArr) {
        if (!this.zzbdo || zzdi == null) {
            return false;
        }
        return zza(zzdi, zzu.zzfu().elapsedRealtime(), strArr);
    }

    public void zzas(String str) {
        if (this.zzbdo) {
            synchronized (this.zzail) {
                this.zzbeh = str;
            }
        }
    }

    @Nullable
    public zzdi zzc(long j) {
        if (!this.zzbdo) {
            return null;
        }
        return new zzdi(j, null, null);
    }

    public void zzc(@Nullable zzdk zzdk) {
        synchronized (this.zzail) {
            this.zzbej = zzdk;
        }
    }

    public void zzh(String str, String str2) {
        if (this.zzbdo && !TextUtils.isEmpty(str2)) {
            zzde zzsl = zzu.zzft().zzsl();
            if (zzsl != null) {
                synchronized (this.zzail) {
                    zzsl.zzaq(str).zza(this.zzbeg, str, str2);
                }
            }
        }
    }

    public zzdi zzkg() {
        return zzc(zzu.zzfu().elapsedRealtime());
    }

    public void zzkh() {
        synchronized (this.zzail) {
            this.zzbei = zzkg();
        }
    }

    public String zzki() {
        String sb;
        StringBuilder sb2 = new StringBuilder();
        synchronized (this.zzail) {
            for (zzdi zzdi : this.zzbef) {
                long time = zzdi.getTime();
                String zzkd = zzdi.zzkd();
                zzdi zzke = zzdi.zzke();
                if (zzke != null && time > 0) {
                    long time2 = time - zzke.getTime();
                    sb2.append(zzkd);
                    sb2.append('.');
                    sb2.append(time2);
                    sb2.append(',');
                }
            }
            this.zzbef.clear();
            if (!TextUtils.isEmpty(this.zzbeh)) {
                sb2.append(this.zzbeh);
            } else if (sb2.length() > 0) {
                sb2.setLength(sb2.length() - 1);
            }
            sb = sb2.toString();
        }
        return sb;
    }

    public zzdi zzkj() {
        zzdi zzdi;
        synchronized (this.zzail) {
            zzdi = this.zzbei;
        }
        return zzdi;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> zzm() {
        synchronized (this.zzail) {
            zzde zzsl = zzu.zzft().zzsl();
            if (zzsl != null) {
                if (this.zzbej != null) {
                    Map<String, String> zza = zzsl.zza(this.zzbeg, this.zzbej.zzm());
                    return zza;
                }
            }
            Map<String, String> map = this.zzbeg;
            return map;
        }
    }
}
