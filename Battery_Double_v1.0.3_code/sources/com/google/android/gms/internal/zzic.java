package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.zzs;

@zzin
public class zzic {

    public interface zza {
        void zzb(zzju zzju);
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [com.google.android.gms.internal.zzkj, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.google.android.gms.internal.zzid] */
    /* JADX WARNING: type inference failed for: r7v2, types: [com.google.android.gms.internal.zzie] */
    /* JADX WARNING: type inference failed for: r7v3, types: [com.google.android.gms.internal.zzia] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 3 */
    public zzkj zza(Context context, com.google.android.gms.ads.internal.zza zza2, com.google.android.gms.internal.zzju.zza zza3, zzas zzas, @Nullable zzlh zzlh, zzgj zzgj, zza zza4, zzdk zzdk) {
        zzif zzif;
        Context context2 = context;
        com.google.android.gms.ads.internal.zza zza5 = zza2;
        com.google.android.gms.internal.zzju.zza zza6 = zza3;
        zzlh zzlh2 = zzlh;
        zza zza7 = zza4;
        AdResponseParcel adResponseParcel = zza6.zzciq;
        if (adResponseParcel.zzcby) {
            zzif zzif2 = new zzif(context2, zza6, zzgj, zza7, zzdk, zzlh2);
            zzif = zzif2;
        } else if (!adResponseParcel.zzauu) {
            zzif = adResponseParcel.zzcce ? new zzia(context2, zza6, zzlh2, zza7) : (!((Boolean) zzdc.zzazs.get()).booleanValue() || !zzs.zzavu() || zzs.zzavw() || zzlh2 == null || !zzlh2.zzdn().zzaus) ? new zzid(context2, zza6, zzlh2, zza7) : new zzie(context2, zza6, zzlh2, zza7);
        } else if (zza5 instanceof zzq) {
            zzig zzig = new zzig(context2, (zzq) zza5, zza6, zzas, zza7);
            zzif = zzig;
        } else {
            String valueOf = String.valueOf(zza5 != null ? zza5.getClass().getName() : "null");
            StringBuilder sb = new StringBuilder(65 + String.valueOf(valueOf).length());
            sb.append("Invalid NativeAdManager type. Found: ");
            sb.append(valueOf);
            sb.append("; Required: NativeAdManager.");
            throw new IllegalArgumentException(sb.toString());
        }
        String str = "AdRenderer: ";
        String valueOf2 = String.valueOf(zzif.getClass().getName());
        zzkd.zzcv(valueOf2.length() != 0 ? str.concat(valueOf2) : new String(str));
        zzif.zzpy();
        return zzif;
    }

    public zzkj zza(Context context, com.google.android.gms.internal.zzju.zza zza2, zzjf zzjf) {
        zzjl zzjl = new zzjl(context, zza2, zzjf);
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(zzjl.getClass().getName());
        zzkd.zzcv(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        zzjl.zzpy();
        return zzjl;
    }
}
