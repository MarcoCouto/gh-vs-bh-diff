package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import java.util.Map;

@zzin
public class zzey implements zzep {
    private final zza zzbiy;

    public interface zza {
        void zzb(RewardItemParcel rewardItemParcel);

        void zzev();
    }

    public zzey(zza zza2) {
        this.zzbiy = zza2;
    }

    public static void zza(zzlh zzlh, zza zza2) {
        zzlh.zzuj().zza("/reward", (zzep) new zzey(zza2));
    }

    private void zze(Map<String, String> map) {
        RewardItemParcel rewardItemParcel = null;
        try {
            int parseInt = Integer.parseInt((String) map.get("amount"));
            String str = (String) map.get("type");
            if (!TextUtils.isEmpty(str)) {
                rewardItemParcel = new RewardItemParcel(str, parseInt);
            }
        } catch (NumberFormatException e) {
            zzkd.zzd("Unable to parse reward amount.", e);
        }
        this.zzbiy.zzb(rewardItemParcel);
    }

    private void zzf(Map<String, String> map) {
        this.zzbiy.zzev();
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        String str = (String) map.get("action");
        if ("grant".equals(str)) {
            zze(map);
            return;
        }
        if ("video_start".equals(str)) {
            zzf(map);
        }
    }
}
