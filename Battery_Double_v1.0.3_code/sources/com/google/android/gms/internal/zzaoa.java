package com.google.android.gms.internal;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class zzaoa extends zzanh<Date> {
    public static final zzani bfu = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() == Date.class) {
                return new zzaoa();
            }
            return null;
        }
    };
    private final DateFormat bdE = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat bdF = DateFormat.getDateTimeInstance(2, 2);
    private final DateFormat bdG = a();

    private static DateFormat a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:6|7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        return r2.bdG.parse(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0021, code lost:
        throw new com.google.android.gms.internal.zzane(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        return r2.bdE.parse(r3);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0013 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x000b */
    private synchronized Date zztq(String str) {
        return this.bdF.parse(str);
    }

    public synchronized void zza(zzaoo zzaoo, Date date) throws IOException {
        if (date == null) {
            zzaoo.l();
        } else {
            zzaoo.zzts(this.bdE.format(date));
        }
    }

    /* renamed from: zzk */
    public Date zzb(zzaom zzaom) throws IOException {
        if (zzaom.b() != zzaon.NULL) {
            return zztq(zzaom.nextString());
        }
        zzaom.nextNull();
        return null;
    }
}
