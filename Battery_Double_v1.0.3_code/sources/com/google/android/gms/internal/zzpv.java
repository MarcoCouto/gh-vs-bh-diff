package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzah;
import com.google.android.gms.internal.zzpm.zza;

public class zzpv implements zzpz {
    /* access modifiers changed from: private */
    public final zzqa tw;
    private boolean tx = false;

    public zzpv(zzqa zzqa) {
        this.tw = zzqa;
    }

    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.common.api.Api$zzg] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    private <A extends zzb> void zzf(zza<? extends Result, A> zza) throws DeadObjectException {
        this.tw.th.uo.zzg(zza);
        zze zzb = this.tw.th.zzb(zza.zzans());
        if (zzb.isConnected() || !this.tw.ux.containsKey(zza.zzans())) {
            if (zzb instanceof zzah) {
                zzb = ((zzah) zzb).zzatn();
            }
            zza.zzb(zzb);
            return;
        }
        zza.zzz(new Status(17));
    }

    public void begin() {
    }

    public void connect() {
        if (this.tx) {
            this.tx = false;
            this.tw.zza((zza) new zza(this) {
                public void zzapl() {
                    zzpv.this.tw.uB.zzm(null);
                }
            });
        }
    }

    public boolean disconnect() {
        if (this.tx) {
            return false;
        }
        if (this.tw.th.zzapx()) {
            this.tx = true;
            for (zzqx zzaqx : this.tw.th.un) {
                zzaqx.zzaqx();
            }
            return false;
        }
        this.tw.zzi(null);
        return true;
    }

    public void onConnected(Bundle bundle) {
    }

    public void onConnectionSuspended(int i) {
        this.tw.zzi(null);
        this.tw.uB.zzc(i, this.tx);
    }

    public void zza(ConnectionResult connectionResult, Api<?> api, int i) {
    }

    /* access modifiers changed from: 0000 */
    public void zzapk() {
        if (this.tx) {
            this.tx = false;
            this.tw.th.uo.release();
            disconnect();
        }
    }

    public <A extends zzb, R extends Result, T extends zza<R, A>> T zzc(T t) {
        return zzd(t);
    }

    public <A extends zzb, T extends zza<? extends Result, A>> T zzd(T t) {
        try {
            zzf(t);
            return t;
        } catch (DeadObjectException unused) {
            this.tw.zza((zza) new zza(this) {
                public void zzapl() {
                    zzpv.this.onConnectionSuspended(1);
                }
            });
            return t;
        }
    }
}
