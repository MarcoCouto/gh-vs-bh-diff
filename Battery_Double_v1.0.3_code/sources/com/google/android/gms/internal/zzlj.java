package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzs;
import com.google.android.gms.ads.internal.zzu;

@zzin
public class zzlj {
    public zzlh zza(Context context, AdSizeParcel adSizeParcel, boolean z, boolean z2, @Nullable zzas zzas, VersionInfoParcel versionInfoParcel) {
        return zza(context, adSizeParcel, z, z2, zzas, versionInfoParcel, null, null, null);
    }

    public zzlh zza(Context context, AdSizeParcel adSizeParcel, boolean z, boolean z2, @Nullable zzas zzas, VersionInfoParcel versionInfoParcel, zzdk zzdk, zzs zzs, zzd zzd) {
        zzlk zzlk = new zzlk(zzll.zzb(context, adSizeParcel, z, z2, zzas, versionInfoParcel, zzdk, zzs, zzd));
        zzlk.setWebViewClient(zzu.zzfs().zzb((zzlh) zzlk, z2));
        zzlk.setWebChromeClient(zzu.zzfs().zzk(zzlk));
        return zzlk;
    }
}
