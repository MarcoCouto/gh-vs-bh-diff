package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zziv.zza;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

@zzin
public class zzfk {
    private final Map<zzfl, zzfm> zzbko = new HashMap();
    private final LinkedList<zzfl> zzbkp = new LinkedList<>();
    @Nullable
    private zzfh zzbkq;

    private static void zza(String str, zzfl zzfl) {
        if (zzkd.zzaz(2)) {
            zzkd.v(String.format(str, new Object[]{zzfl}));
        }
    }

    private String[] zzbe(String str) {
        try {
            String[] split = str.split("\u0000");
            for (int i = 0; i < split.length; i++) {
                split[i] = new String(Base64.decode(split[i], 0), "UTF-8");
            }
            return split;
        } catch (UnsupportedEncodingException unused) {
            return new String[0];
        }
    }

    private boolean zzbf(String str) {
        try {
            return Pattern.matches((String) zzdc.zzbal.get(), str);
        } catch (RuntimeException e) {
            zzu.zzft().zzb((Throwable) e, true);
            return false;
        }
    }

    private static void zzc(Bundle bundle, String str) {
        String[] split = str.split("/", 2);
        if (split.length != 0) {
            String str2 = split[0];
            if (split.length == 1) {
                bundle.remove(str2);
                return;
            }
            Bundle bundle2 = bundle.getBundle(str2);
            if (bundle2 != null) {
                zzc(bundle2, split[1]);
            }
        }
    }

    @Nullable
    static Bundle zzi(AdRequestParcel adRequestParcel) {
        Bundle bundle = adRequestParcel.zzatw;
        if (bundle == null) {
            return null;
        }
        return bundle.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
    }

    static AdRequestParcel zzj(AdRequestParcel adRequestParcel) {
        Parcel obtain = Parcel.obtain();
        adRequestParcel.writeToParcel(obtain, 0);
        obtain.setDataPosition(0);
        AdRequestParcel adRequestParcel2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(obtain);
        obtain.recycle();
        Bundle zzi = zzi(adRequestParcel2);
        if (zzi == null) {
            zzi = new Bundle();
            adRequestParcel2.zzatw.putBundle("com.google.ads.mediation.admob.AdMobAdapter", zzi);
        }
        zzi.putBoolean("_skipMediation", true);
        return adRequestParcel2;
    }

    static boolean zzk(AdRequestParcel adRequestParcel) {
        Bundle bundle = adRequestParcel.zzatw;
        boolean z = false;
        if (bundle == null) {
            return false;
        }
        Bundle bundle2 = bundle.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle2 != null && bundle2.containsKey("_skipMediation")) {
            z = true;
        }
        return z;
    }

    private static AdRequestParcel zzl(AdRequestParcel adRequestParcel) {
        Parcel obtain = Parcel.obtain();
        adRequestParcel.writeToParcel(obtain, 0);
        obtain.setDataPosition(0);
        AdRequestParcel adRequestParcel2 = (AdRequestParcel) AdRequestParcel.CREATOR.createFromParcel(obtain);
        obtain.recycle();
        for (String zzc : ((String) zzdc.zzbah.get()).split(",")) {
            zzc(adRequestParcel2.zzatw, zzc);
        }
        return adRequestParcel2;
    }

    private String zzlp() {
        try {
            StringBuilder sb = new StringBuilder();
            Iterator it = this.zzbkp.iterator();
            while (it.hasNext()) {
                sb.append(Base64.encodeToString(((zzfl) it.next()).toString().getBytes("UTF-8"), 0));
                if (it.hasNext()) {
                    sb.append("\u0000");
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    /* access modifiers changed from: 0000 */
    public void flush() {
        while (this.zzbkp.size() > 0) {
            zzfl zzfl = (zzfl) this.zzbkp.remove();
            zzfm zzfm = (zzfm) this.zzbko.get(zzfl);
            zza("Flushing interstitial queue for %s.", zzfl);
            while (zzfm.size() > 0) {
                zzfm.zzm(null).zzbkv.zzeu();
            }
            this.zzbko.remove(zzfl);
        }
    }

    /* access modifiers changed from: 0000 */
    public void restore() {
        if (this.zzbkq != null) {
            SharedPreferences sharedPreferences = this.zzbkq.getApplicationContext().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
            flush();
            HashMap hashMap = new HashMap();
            for (Entry entry : sharedPreferences.getAll().entrySet()) {
                try {
                    if (!((String) entry.getKey()).equals("PoolKeys")) {
                        zzfo zzfo = new zzfo((String) entry.getValue());
                        zzfl zzfl = new zzfl(zzfo.zzanc, zzfo.zzaln, zzfo.zzbkt);
                        if (!this.zzbko.containsKey(zzfl)) {
                            this.zzbko.put(zzfl, new zzfm(zzfo.zzanc, zzfo.zzaln, zzfo.zzbkt));
                            hashMap.put(zzfl.toString(), zzfl);
                            zza("Restored interstitial queue for %s.", zzfl);
                        }
                    }
                } catch (IOException | ClassCastException e) {
                    zzkd.zzd("Malformed preferences value for InterstitialAdPool.", e);
                }
            }
            for (String str : zzbe(sharedPreferences.getString("PoolKeys", ""))) {
                zzfl zzfl2 = (zzfl) hashMap.get(str);
                if (this.zzbko.containsKey(zzfl2)) {
                    this.zzbkp.add(zzfl2);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void save() {
        if (this.zzbkq != null) {
            Editor edit = this.zzbkq.getApplicationContext().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0).edit();
            edit.clear();
            for (Entry entry : this.zzbko.entrySet()) {
                zzfl zzfl = (zzfl) entry.getKey();
                zzfm zzfm = (zzfm) entry.getValue();
                if (zzfm.zzlu()) {
                    edit.putString(zzfl.toString(), new zzfo(zzfm).zzlx());
                    zza("Saved interstitial queue for %s.", zzfl);
                }
            }
            edit.putString("PoolKeys", zzlp());
            edit.apply();
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public zza zza(AdRequestParcel adRequestParcel, String str) {
        if (zzbf(str)) {
            return null;
        }
        int i = new zza(this.zzbkq.getApplicationContext()).zzrn().zzcgp;
        AdRequestParcel zzl = zzl(adRequestParcel);
        zzfl zzfl = new zzfl(zzl, str, i);
        zzfm zzfm = (zzfm) this.zzbko.get(zzfl);
        if (zzfm == null) {
            zza("Interstitial pool created at %s.", zzfl);
            zzfm = new zzfm(zzl, str, i);
            this.zzbko.put(zzfl, zzfm);
        }
        this.zzbkp.remove(zzfl);
        this.zzbkp.add(zzfl);
        zzfm.zzlt();
        while (this.zzbkp.size() > ((Integer) zzdc.zzbai.get()).intValue()) {
            zzfl zzfl2 = (zzfl) this.zzbkp.remove();
            zzfm zzfm2 = (zzfm) this.zzbko.get(zzfl2);
            zza("Evicting interstitial queue for %s.", zzfl2);
            while (zzfm2.size() > 0) {
                zzfm2.zzm(null).zzbkv.zzeu();
            }
            this.zzbko.remove(zzfl2);
        }
        while (zzfm.size() > 0) {
            zza zzm = zzfm.zzm(zzl);
            if (!zzm.zzbkz || zzu.zzfu().currentTimeMillis() - zzm.zzbky <= 1000 * ((long) ((Integer) zzdc.zzbak.get()).intValue())) {
                String str2 = zzm.zzbkw != null ? " (inline) " : " ";
                StringBuilder sb = new StringBuilder(34 + String.valueOf(str2).length());
                sb.append("Pooled interstitial");
                sb.append(str2);
                sb.append("returned at %s.");
                zza(sb.toString(), zzfl);
                return zzm;
            }
            zza("Expired interstitial at %s.", zzfl);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzfh zzfh) {
        if (this.zzbkq == null) {
            this.zzbkq = zzfh.zzln();
            restore();
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzb(AdRequestParcel adRequestParcel, String str) {
        if (this.zzbkq != null) {
            int i = new zza(this.zzbkq.getApplicationContext()).zzrn().zzcgp;
            AdRequestParcel zzl = zzl(adRequestParcel);
            zzfl zzfl = new zzfl(zzl, str, i);
            zzfm zzfm = (zzfm) this.zzbko.get(zzfl);
            if (zzfm == null) {
                zza("Interstitial pool created at %s.", zzfl);
                zzfm = new zzfm(zzl, str, i);
                this.zzbko.put(zzfl, zzfm);
            }
            zzfm.zza(this.zzbkq, adRequestParcel);
            zzfm.zzlt();
            zza("Inline entry added to the queue at %s.", zzfl);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzlo() {
        if (this.zzbkq != null) {
            for (Entry entry : this.zzbko.entrySet()) {
                zzfl zzfl = (zzfl) entry.getKey();
                zzfm zzfm = (zzfm) entry.getValue();
                if (zzkd.zzaz(2)) {
                    int size = zzfm.size();
                    int zzlr = zzfm.zzlr();
                    if (zzlr < size) {
                        zzkd.v(String.format("Loading %s/%s pooled interstitials for %s.", new Object[]{Integer.valueOf(size - zzlr), Integer.valueOf(size), zzfl}));
                    }
                }
                zzfm.zzls();
                while (zzfm.size() < ((Integer) zzdc.zzbaj.get()).intValue()) {
                    zza("Pooling and loading one new interstitial for %s.", zzfl);
                    zzfm.zzb(this.zzbkq);
                }
            }
            save();
        }
    }
}
