package com.google.android.gms.internal;

import java.io.IOException;

public final class zzapn {
    private int bjn;
    private int bjo;
    private int bjp;
    private int bjq;
    private int bjr;
    private int bjs = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
    private int bjt;
    private int bju = 64;
    private int bjv = 67108864;
    private final byte[] buffer;

    private zzapn(byte[] bArr, int i, int i2) {
        this.buffer = bArr;
        this.bjn = i;
        this.bjo = i2 + i;
        this.bjq = i;
    }

    private void au() {
        this.bjo += this.bjp;
        int i = this.bjo;
        if (i > this.bjs) {
            this.bjp = i - this.bjs;
            this.bjo -= this.bjp;
            return;
        }
        this.bjp = 0;
    }

    public static int zzafq(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public static zzapn zzb(byte[] bArr, int i, int i2) {
        return new zzapn(bArr, i, i2);
    }

    public static zzapn zzbd(byte[] bArr) {
        return zzb(bArr, 0, bArr.length);
    }

    public static long zzcs(long j) {
        return (j >>> 1) ^ (-(j & 1));
    }

    public int ah() throws IOException {
        if (aw()) {
            this.bjr = 0;
            return 0;
        }
        this.bjr = aq();
        if (this.bjr != 0) {
            return this.bjr;
        }
        throw zzapu.aH();
    }

    public void ai() throws IOException {
        int ah;
        do {
            ah = ah();
            if (ah == 0) {
                return;
            }
        } while (zzafp(ah));
    }

    public long aj() throws IOException {
        return ar();
    }

    public long ak() throws IOException {
        return ar();
    }

    public int al() throws IOException {
        return aq();
    }

    public long am() throws IOException {
        return at();
    }

    public boolean an() throws IOException {
        return aq() != 0;
    }

    public int ao() throws IOException {
        return zzafq(aq());
    }

    public long ap() throws IOException {
        return zzcs(ar());
    }

    public int aq() throws IOException {
        int i;
        byte ax = ax();
        if (ax >= 0) {
            return ax;
        }
        byte b = ax & Byte.MAX_VALUE;
        byte ax2 = ax();
        if (ax2 >= 0) {
            i = ax2 << 7;
        } else {
            b |= (ax2 & Byte.MAX_VALUE) << 7;
            byte ax3 = ax();
            if (ax3 >= 0) {
                i = ax3 << 14;
            } else {
                b |= (ax3 & Byte.MAX_VALUE) << 14;
                byte ax4 = ax();
                if (ax4 >= 0) {
                    i = ax4 << 21;
                } else {
                    byte b2 = b | ((ax4 & Byte.MAX_VALUE) << 21);
                    byte ax5 = ax();
                    byte b3 = b2 | (ax5 << 28);
                    if (ax5 >= 0) {
                        return b3;
                    }
                    for (int i2 = 0; i2 < 5; i2++) {
                        if (ax() >= 0) {
                            return b3;
                        }
                    }
                    throw zzapu.aG();
                }
            }
        }
        return b | i;
    }

    public long ar() throws IOException {
        int i = 0;
        long j = 0;
        while (i < 64) {
            byte ax = ax();
            long j2 = j | (((long) (ax & Byte.MAX_VALUE)) << i);
            if ((ax & 128) == 0) {
                return j2;
            }
            i += 7;
            j = j2;
        }
        throw zzapu.aG();
    }

    public int as() throws IOException {
        return (ax() & 255) | ((ax() & 255) << 8) | ((ax() & 255) << 16) | ((ax() & 255) << 24);
    }

    public long at() throws IOException {
        return (((long) ax()) & 255) | ((((long) ax()) & 255) << 8) | ((((long) ax()) & 255) << 16) | ((((long) ax()) & 255) << 24) | ((((long) ax()) & 255) << 32) | ((((long) ax()) & 255) << 40) | ((((long) ax()) & 255) << 48) | ((((long) ax()) & 255) << 56);
    }

    public int av() {
        if (this.bjs == Integer.MAX_VALUE) {
            return -1;
        }
        return this.bjs - this.bjq;
    }

    public boolean aw() {
        return this.bjq == this.bjo;
    }

    public byte ax() throws IOException {
        if (this.bjq == this.bjo) {
            throw zzapu.aE();
        }
        byte[] bArr = this.buffer;
        int i = this.bjq;
        this.bjq = i + 1;
        return bArr[i];
    }

    public int getPosition() {
        return this.bjq - this.bjn;
    }

    public byte[] readBytes() throws IOException {
        int aq = aq();
        if (aq < 0) {
            throw zzapu.aF();
        } else if (aq == 0) {
            return zzapy.bjO;
        } else {
            if (aq > this.bjo - this.bjq) {
                throw zzapu.aE();
            }
            byte[] bArr = new byte[aq];
            System.arraycopy(this.buffer, this.bjq, bArr, 0, aq);
            this.bjq += aq;
            return bArr;
        }
    }

    public double readDouble() throws IOException {
        return Double.longBitsToDouble(at());
    }

    public float readFloat() throws IOException {
        return Float.intBitsToFloat(as());
    }

    public String readString() throws IOException {
        int aq = aq();
        if (aq < 0) {
            throw zzapu.aF();
        } else if (aq > this.bjo - this.bjq) {
            throw zzapu.aE();
        } else {
            String str = new String(this.buffer, this.bjq, aq, zzapt.UTF_8);
            this.bjq += aq;
            return str;
        }
    }

    public void zza(zzapv zzapv) throws IOException {
        int aq = aq();
        if (this.bjt >= this.bju) {
            throw zzapu.aK();
        }
        int zzafr = zzafr(aq);
        this.bjt++;
        zzapv.zzb(this);
        zzafo(0);
        this.bjt--;
        zzafs(zzafr);
    }

    public void zza(zzapv zzapv, int i) throws IOException {
        if (this.bjt >= this.bju) {
            throw zzapu.aK();
        }
        this.bjt++;
        zzapv.zzb(this);
        zzafo(zzapy.zzaj(i, 4));
        this.bjt--;
    }

    public byte[] zzad(int i, int i2) {
        if (i2 == 0) {
            return zzapy.bjO;
        }
        byte[] bArr = new byte[i2];
        System.arraycopy(this.buffer, this.bjn + i, bArr, 0, i2);
        return bArr;
    }

    public void zzafo(int i) throws zzapu {
        if (this.bjr != i) {
            throw zzapu.aI();
        }
    }

    public boolean zzafp(int i) throws IOException {
        switch (zzapy.zzagi(i)) {
            case 0:
                al();
                return true;
            case 1:
                at();
                return true;
            case 2:
                zzafu(aq());
                return true;
            case 3:
                ai();
                zzafo(zzapy.zzaj(zzapy.zzagj(i), 4));
                return true;
            case 4:
                return false;
            case 5:
                as();
                return true;
            default:
                throw zzapu.aJ();
        }
    }

    public int zzafr(int i) throws zzapu {
        if (i < 0) {
            throw zzapu.aF();
        }
        int i2 = i + this.bjq;
        int i3 = this.bjs;
        if (i2 > i3) {
            throw zzapu.aE();
        }
        this.bjs = i2;
        au();
        return i3;
    }

    public void zzafs(int i) {
        this.bjs = i;
        au();
    }

    public void zzaft(int i) {
        if (i > this.bjq - this.bjn) {
            int i2 = this.bjq - this.bjn;
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i);
            sb.append(" is beyond current ");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        } else if (i < 0) {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i);
            throw new IllegalArgumentException(sb2.toString());
        } else {
            this.bjq = this.bjn + i;
        }
    }

    public void zzafu(int i) throws IOException {
        if (i < 0) {
            throw zzapu.aF();
        } else if (this.bjq + i > this.bjs) {
            zzafu(this.bjs - this.bjq);
            throw zzapu.aE();
        } else if (i <= this.bjo - this.bjq) {
            this.bjq += i;
        } else {
            throw zzapu.aE();
        }
    }
}
