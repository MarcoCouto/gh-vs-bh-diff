package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.overlay.zzm;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zzeo {
    public static final zzep zzbhn = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
        }
    };
    public static final zzep zzbho = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("urls");
            if (TextUtils.isEmpty(str)) {
                zzkd.zzcx("URLs missing in canOpenURLs GMSG.");
                return;
            }
            String[] split = str.split(",");
            HashMap hashMap = new HashMap();
            PackageManager packageManager = zzlh.getContext().getPackageManager();
            for (String str2 : split) {
                String[] split2 = str2.split(";", 2);
                boolean z = true;
                if (packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), 65536) == null) {
                    z = false;
                }
                hashMap.put(str2, Boolean.valueOf(z));
            }
            zzlh.zza("openableURLs", (Map<String, ?>) hashMap);
        }
    };
    public static final zzep zzbhp = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str;
            PackageManager packageManager = zzlh.getContext().getPackageManager();
            try {
                try {
                    JSONArray jSONArray = new JSONObject((String) map.get("data")).getJSONArray("intents");
                    JSONObject jSONObject = new JSONObject();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                            String optString = jSONObject2.optString("id");
                            String optString2 = jSONObject2.optString("u");
                            String optString3 = jSONObject2.optString("i");
                            String optString4 = jSONObject2.optString("m");
                            String optString5 = jSONObject2.optString("p");
                            String optString6 = jSONObject2.optString("c");
                            jSONObject2.optString("f");
                            jSONObject2.optString("e");
                            Intent intent = new Intent();
                            if (!TextUtils.isEmpty(optString2)) {
                                intent.setData(Uri.parse(optString2));
                            }
                            if (!TextUtils.isEmpty(optString3)) {
                                intent.setAction(optString3);
                            }
                            if (!TextUtils.isEmpty(optString4)) {
                                intent.setType(optString4);
                            }
                            if (!TextUtils.isEmpty(optString5)) {
                                intent.setPackage(optString5);
                            }
                            boolean z = true;
                            if (!TextUtils.isEmpty(optString6)) {
                                String[] split = optString6.split("/", 2);
                                if (split.length == 2) {
                                    intent.setComponent(new ComponentName(split[0], split[1]));
                                }
                            }
                            if (packageManager.resolveActivity(intent, 65536) == null) {
                                z = false;
                            }
                            try {
                                jSONObject.put(optString, z);
                            } catch (JSONException e) {
                                e = e;
                                str = "Error constructing openable urls response.";
                            }
                        } catch (JSONException e2) {
                            e = e2;
                            str = "Error parsing the intent data.";
                            zzkd.zzb(str, e);
                        }
                    }
                    zzlh.zzb("openableIntents", jSONObject);
                } catch (JSONException unused) {
                    zzlh.zzb("openableIntents", new JSONObject());
                }
            } catch (JSONException unused2) {
                zzlh.zzb("openableIntents", new JSONObject());
            }
        }
    };
    public static final zzep zzbhq = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("u");
            if (str == null) {
                zzkd.zzcx("URL missing from click GMSG.");
                return;
            }
            Uri parse = Uri.parse(str);
            try {
                zzas zzul = zzlh.zzul();
                if (zzul != null && zzul.zzc(parse)) {
                    parse = zzul.zzb(parse, zzlh.getContext());
                }
            } catch (zzat unused) {
                String str2 = "Unable to append parameter to URL: ";
                String valueOf = String.valueOf(str);
                zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
            Future future = (Future) new zzkq(zzlh.getContext(), zzlh.zzum().zzcs, parse.toString()).zzpy();
        }
    };
    public static final zzep zzbhr = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            zzd zzuh = zzlh.zzuh();
            if (zzuh != null) {
                zzuh.close();
                return;
            }
            zzd zzui = zzlh.zzui();
            if (zzui != null) {
                zzui.close();
            } else {
                zzkd.zzcx("A GMSG tried to close something that wasn't an overlay.");
            }
        }
    };
    public static final zzep zzbhs = new zzep() {
        private void zzc(zzlh zzlh) {
            boolean z;
            zzkd.zzcw("Received support message, responding.");
            com.google.android.gms.ads.internal.zzd zzug = zzlh.zzug();
            if (zzug != null) {
                zzm zzm = zzug.zzakl;
                if (zzm != null) {
                    z = zzm.zzr(zzlh.getContext());
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("event", "checkSupport");
                    jSONObject.put("supports", z);
                    zzlh.zzb("appStreaming", jSONObject);
                }
            }
            z = false;
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("event", "checkSupport");
                jSONObject2.put("supports", z);
                zzlh.zzb("appStreaming", jSONObject2);
            } catch (Throwable unused) {
            }
        }

        public void zza(zzlh zzlh, Map<String, String> map) {
            if ("checkSupport".equals(map.get("action"))) {
                zzc(zzlh);
                return;
            }
            zzd zzuh = zzlh.zzuh();
            if (zzuh != null) {
                zzuh.zzf(zzlh, map);
            }
        }
    };
    public static final zzep zzbht = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            zzlh.zzai("1".equals(map.get("custom_close")));
        }
    };
    public static final zzep zzbhu = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("u");
            if (str == null) {
                zzkd.zzcx("URL missing from httpTrack GMSG.");
            } else {
                Future future = (Future) new zzkq(zzlh.getContext(), zzlh.zzum().zzcs, str).zzpy();
            }
        }
    };
    public static final zzep zzbhv = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = "Received log message: ";
            String valueOf = String.valueOf((String) map.get("string"));
            zzkd.zzcw(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
    };
    public static final zzep zzbhw = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("tx");
            String str2 = (String) map.get("ty");
            String str3 = (String) map.get("td");
            try {
                int parseInt = Integer.parseInt(str);
                int parseInt2 = Integer.parseInt(str2);
                int parseInt3 = Integer.parseInt(str3);
                zzas zzul = zzlh.zzul();
                if (zzul != null) {
                    zzul.zzaw().zza(parseInt, parseInt2, parseInt3);
                }
            } catch (NumberFormatException unused) {
                zzkd.zzcx("Could not parse touch parameters from gmsg.");
            }
        }
    };
    public static final zzep zzbhx = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            if (((Boolean) zzdc.zzbba.get()).booleanValue()) {
                zzlh.zzaj(!Boolean.parseBoolean((String) map.get("disabled")));
            }
        }
    };
    public static final zzep zzbhy = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("action");
            if ("pause".equals(str)) {
                zzlh.zzef();
                return;
            }
            if ("resume".equals(str)) {
                zzlh.zzeg();
            }
        }
    };
    public static final zzep zzbhz = new zzez();
    public static final zzep zzbia = new zzfa();
    public static final zzep zzbib = new zzfe();
    public static final zzep zzbic = new zzen();
    public static final zzex zzbid = new zzex();
}
