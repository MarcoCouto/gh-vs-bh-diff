package com.google.android.gms.internal;

import java.util.List;

public interface zzfy {
    void cancel();

    zzge zzd(List<zzfz> list);

    List<zzge> zzmg();
}
