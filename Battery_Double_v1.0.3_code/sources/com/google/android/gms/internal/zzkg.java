package com.google.android.gms.internal;

import android.os.Process;
import com.google.android.gms.ads.internal.zzu;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

@zzin
public final class zzkg {
    private static final ExecutorService zzcku = Executors.newFixedThreadPool(10, zzcn("Default"));
    private static final ExecutorService zzckv = Executors.newFixedThreadPool(5, zzcn("Loader"));

    public static zzky<Void> zza(int i, final Runnable runnable) {
        ExecutorService executorService;
        Callable r0;
        if (i == 1) {
            executorService = zzckv;
            r0 = new Callable<Void>() {
                /* renamed from: zzcx */
                public Void call() {
                    runnable.run();
                    return null;
                }
            };
        } else {
            executorService = zzcku;
            r0 = new Callable<Void>() {
                /* renamed from: zzcx */
                public Void call() {
                    runnable.run();
                    return null;
                }
            };
        }
        return zza(executorService, r0);
    }

    public static zzky<Void> zza(Runnable runnable) {
        return zza(0, runnable);
    }

    public static <T> zzky<T> zza(Callable<T> callable) {
        return zza(zzcku, callable);
    }

    public static <T> zzky<T> zza(ExecutorService executorService, final Callable<T> callable) {
        final zzkv zzkv = new zzkv();
        try {
            final Future submit = executorService.submit(new Runnable() {
                public void run() {
                    try {
                        Process.setThreadPriority(10);
                        zzkv.this.zzh(callable.call());
                    } catch (Exception e) {
                        zzu.zzft().zzb((Throwable) e, true);
                        zzkv.this.cancel(true);
                    }
                }
            });
            zzkv.zzd(new Runnable() {
                public void run() {
                    if (zzkv.this.isCancelled()) {
                        submit.cancel(true);
                    }
                }
            });
            return zzkv;
        } catch (RejectedExecutionException e) {
            zzkd.zzd("Thread execution is rejected.", e);
            zzkv.cancel(true);
            return zzkv;
        }
    }

    private static ThreadFactory zzcn(final String str) {
        return new ThreadFactory() {
            private final AtomicInteger zzcla = new AtomicInteger(1);

            public Thread newThread(Runnable runnable) {
                String str = str;
                int andIncrement = this.zzcla.getAndIncrement();
                StringBuilder sb = new StringBuilder(23 + String.valueOf(str).length());
                sb.append("AdWorker(");
                sb.append(str);
                sb.append(") #");
                sb.append(andIncrement);
                return new Thread(runnable, sb.toString());
            }
        };
    }
}
