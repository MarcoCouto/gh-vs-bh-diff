package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.internal.zzii.zza;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzij implements zza<zzd> {
    private final boolean zzcaa;
    private final boolean zzcab;

    public zzij(boolean z, boolean z2) {
        this.zzcaa = z;
        this.zzcab = z2;
    }

    /* renamed from: zzb */
    public zzd zza(zzii zzii, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException {
        JSONObject jSONObject2 = jSONObject;
        List<zzky> zza = zzii.zza(jSONObject2, "images", true, this.zzcaa, this.zzcab);
        zzky zza2 = zzii.zza(jSONObject2, "app_icon", true, this.zzcaa);
        zzky zzg = zzii.zzg(jSONObject);
        ArrayList arrayList = new ArrayList();
        for (zzky zzky : zza) {
            arrayList.add((zzc) zzky.get());
        }
        zzd zzd = new zzd(jSONObject2.getString("headline"), arrayList, jSONObject2.getString("body"), (zzdr) zza2.get(), jSONObject2.getString("call_to_action"), jSONObject2.optDouble("rating", -1.0d), jSONObject2.optString("store"), jSONObject2.optString(Param.PRICE), (com.google.android.gms.ads.internal.formats.zza) zzg.get(), new Bundle());
        return zzd;
    }
}
