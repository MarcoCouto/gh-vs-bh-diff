package com.google.android.gms.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzin
public class zzfg extends zzfd {
    private static final Set<String> zzbjp = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzbjq = new DecimalFormat("#,###");
    private File zzbjr;
    private boolean zzbjs;

    public zzfg(zzlh zzlh) {
        super(zzlh);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzkd.zzcx("Context.getCacheDir() returned null");
            return;
        }
        this.zzbjr = new File(cacheDir, "admobVideoStreams");
        if (!this.zzbjr.isDirectory() && !this.zzbjr.mkdirs()) {
            String str = "Could not create preload cache directory at ";
            String valueOf = String.valueOf(this.zzbjr.getAbsolutePath());
            zzkd.zzcx(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.zzbjr = null;
        } else if (!this.zzbjr.setReadable(true, false) || !this.zzbjr.setExecutable(true, false)) {
            String str2 = "Could not set cache file permissions at ";
            String valueOf2 = String.valueOf(this.zzbjr.getAbsolutePath());
            zzkd.zzcx(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            this.zzbjr = null;
        }
    }

    private File zzb(File file) {
        return new File(this.zzbjr, String.valueOf(file.getName()).concat(".done"));
    }

    private static void zzc(File file) {
        if (file.isFile()) {
            file.setLastModified(System.currentTimeMillis());
        } else {
            try {
                file.createNewFile();
            } catch (IOException unused) {
            }
        }
    }

    public void abort() {
        this.zzbjs = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0271, code lost:
        if (r3.length() == 0) goto L_0x0279;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0273, code lost:
        r10 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x027e, code lost:
        r10 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0286, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0287, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0288, code lost:
        r15 = r1;
        r3 = r10;
        r2 = r18;
        r10 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0290, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0291, code lost:
        r15 = r1;
        r2 = r18;
        r10 = r19;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        r2.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x02a8, code lost:
        if (r3.write(r2) <= 0) goto L_0x02ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02ab, code lost:
        r2.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02ba, code lost:
        if ((r1.currentTimeMillis() - r16) <= (1000 * r13)) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02bc, code lost:
        r1 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:?, code lost:
        r2 = java.lang.String.valueOf(java.lang.Long.toString(r13));
        r3 = new java.lang.StringBuilder(29 + java.lang.String.valueOf(r2).length());
        r3.append("Timeout exceeded. Limit: ");
        r3.append(r2);
        r3.append(" sec");
        r10 = r3.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02ee, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x02ef, code lost:
        r25 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02f3, code lost:
        if (r7.zzbjs == false) goto L_0x02ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02f5, code lost:
        r1 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02fe, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0303, code lost:
        if (r9.tryAcquire() == false) goto L_0x0326;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0309, code lost:
        r22 = r25;
        r23 = r2;
        r24 = r3;
        r26 = r9;
        r9 = r19;
        r19 = r4;
        r20 = r5;
        r25 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        zza(r8, r11.getAbsolutePath(), r4, r6, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0326, code lost:
        r23 = r2;
        r24 = r3;
        r20 = r5;
        r26 = r9;
        r9 = r19;
        r22 = r25;
        r19 = r4;
        r25 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0336, code lost:
        r4 = r19;
        r5 = r20;
        r1 = r22;
        r2 = r23;
        r3 = r24;
        r6 = r25;
        r19 = r9;
        r9 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0348, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0349, code lost:
        r9 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x034b, code lost:
        r1 = r0;
        r10 = r9;
        r2 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0351, code lost:
        r9 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x035b, code lost:
        if (com.google.android.gms.internal.zzkd.zzaz(3) == false) goto L_0x0397;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:?, code lost:
        r1 = zzbjq.format((long) r4);
        r2 = new java.lang.StringBuilder((22 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r28).length());
        r2.append("Preloaded ");
        r2.append(r1);
        r2.append(" bytes from ");
        r2.append(r8);
        com.google.android.gms.internal.zzkd.zzcv(r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0395, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:?, code lost:
        r11.setReadable(true, false);
        zzc(r12);
        zza(r8, r11.getAbsolutePath(), r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x03a8, code lost:
        r2 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        zzbjp.remove(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x03ae, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03af, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x03b1, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x03b3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x03b4, code lost:
        r2 = r18;
        r9 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03b9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x03ba, code lost:
        r9 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03bb, code lost:
        r2 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03be, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03bf, code lost:
        r9 = r4;
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03c1, code lost:
        r1 = r0;
        r10 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03c3, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03c5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03c6, code lost:
        r2 = r14;
        r1 = r0;
        r3 = null;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x03cc, code lost:
        if ((r1 instanceof java.lang.RuntimeException) != false) goto L_0x03ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03ce, code lost:
        com.google.android.gms.ads.internal.zzu.zzft().zzb(r1, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x03db, code lost:
        if (r7.zzbjs == false) goto L_0x0402;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x03dd, code lost:
        r1 = new java.lang.StringBuilder(26 + java.lang.String.valueOf(r28).length());
        r1.append("Preload aborted for URL \"");
        r1.append(r8);
        r1.append("\"");
        com.google.android.gms.internal.zzkd.zzcw(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0402, code lost:
        r4 = new java.lang.StringBuilder(25 + java.lang.String.valueOf(r28).length());
        r4.append("Preload failed for URL \"");
        r4.append(r8);
        r4.append("\"");
        com.google.android.gms.internal.zzkd.zzd(r4.toString(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0432, code lost:
        r1 = "Could not delete partial cache file at ";
        r4 = java.lang.String.valueOf(r11.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0440, code lost:
        if (r4.length() == 0) goto L_0x0447;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0442, code lost:
        r1 = r1.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x0447, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x044d, code lost:
        com.google.android.gms.internal.zzkd.zzcx(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0450, code lost:
        zza(r8, r11.getAbsolutePath(), r15, r3);
        zzbjp.remove(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x045d, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c9, code lost:
        r15 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r1 = new java.net.URL(r8).openConnection();
        r2 = ((java.lang.Integer) com.google.android.gms.internal.zzdc.zzayr.get()).intValue();
        r1.setConnectTimeout(r2);
        r1.setReadTimeout(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e8, code lost:
        if ((r1 instanceof java.net.HttpURLConnection) == false) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r2 = ((java.net.HttpURLConnection) r1).getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f3, code lost:
        if (r2 < 400) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f5, code lost:
        r1 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f7, code lost:
        r3 = "HTTP request failed. Code: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r4 = java.lang.String.valueOf(java.lang.Integer.toString(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0105, code lost:
        if (r4.length() == 0) goto L_0x010c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0107, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0111, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r5 = new java.lang.StringBuilder(32 + java.lang.String.valueOf(r28).length());
        r5.append("HTTP status code ");
        r5.append(r2);
        r5.append(" at ");
        r5.append(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x013b, code lost:
        throw new java.io.IOException(r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x013c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x013d, code lost:
        r15 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x013f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0140, code lost:
        r15 = r1;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0142, code lost:
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0143, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0146, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0147, code lost:
        r1 = r0;
        r3 = null;
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        r6 = r1.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0150, code lost:
        if (r6 >= 0) goto L_0x017b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0152, code lost:
        r1 = "Stream cache aborted, missing content-length header at ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r2 = java.lang.String.valueOf(r28);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x015c, code lost:
        if (r2.length() == 0) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x015e, code lost:
        r1 = r1.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0163, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0169, code lost:
        com.google.android.gms.internal.zzkd.zzcx(r1);
        zza(r8, r11.getAbsolutePath(), "contentLengthMissing", null);
        zzbjp.remove(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x017a, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r2 = zzbjq.format((long) r6);
        r5 = ((java.lang.Integer) com.google.android.gms.internal.zzdc.zzayn.get()).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x018e, code lost:
        if (r6 <= r5) goto L_0x01e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r1 = new java.lang.StringBuilder((33 + java.lang.String.valueOf(r2).length()) + java.lang.String.valueOf(r28).length());
        r1.append("Content length ");
        r1.append(r2);
        r1.append(" exceeds limit at ");
        r1.append(r8);
        com.google.android.gms.internal.zzkd.zzcx(r1.toString());
        r1 = "File too big for full file cache. Size: ";
        r2 = java.lang.String.valueOf(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01ca, code lost:
        if (r2.length() == 0) goto L_0x01d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01cc, code lost:
        r1 = r1.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01d1, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01d7, code lost:
        zza(r8, r11.getAbsolutePath(), "sizeExceeded", r1);
        zzbjp.remove(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01e5, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r3 = new java.lang.StringBuilder((20 + java.lang.String.valueOf(r2).length()) + java.lang.String.valueOf(r28).length());
        r3.append("Caching ");
        r3.append(r2);
        r3.append(" bytes from ");
        r3.append(r8);
        com.google.android.gms.internal.zzkd.zzcv(r3.toString());
        r10 = java.nio.channels.Channels.newChannel(r1.getInputStream());
        r4 = new java.io.FileOutputStream(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r3 = r4.getChannel();
        r2 = java.nio.ByteBuffer.allocate(1048576);
        r1 = com.google.android.gms.ads.internal.zzu.zzfu();
        r16 = r1.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x023d, code lost:
        r18 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        r9 = new com.google.android.gms.internal.zzkr(((java.lang.Long) com.google.android.gms.internal.zzdc.zzayq.get()).longValue());
        r13 = ((java.lang.Long) com.google.android.gms.internal.zzdc.zzayp.get()).longValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0254, code lost:
        r19 = r4;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        r20 = r10.read(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x025b, code lost:
        if (r20 < 0) goto L_0x0351;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x025d, code lost:
        r4 = r4 + r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x025f, code lost:
        if (r4 <= r5) goto L_0x02a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0261, code lost:
        r1 = "sizeExceeded";
        r2 = "File too big for full file cache. Size: ";
     */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x03ce  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x03dd  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0402  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0442  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0447  */
    public boolean zzaz(String str) {
        String str2;
        String str3;
        String str4;
        String str5 = str;
        FileOutputStream fileOutputStream = null;
        if (this.zzbjr == null) {
            str4 = "noCacheDir";
        } else {
            while (zzll() > ((Integer) zzdc.zzaym.get()).intValue()) {
                if (!zzlm()) {
                    zzkd.zzcx("Unable to expire stream cache");
                    str4 = "expireFailed";
                }
            }
            File file = new File(this.zzbjr, zzba(str));
            File zzb = zzb(file);
            if (!file.isFile() || !zzb.isFile()) {
                String valueOf = String.valueOf(this.zzbjr.getAbsolutePath());
                String valueOf2 = String.valueOf(str);
                Object concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                synchronized (zzbjp) {
                    try {
                        if (zzbjp.contains(concat)) {
                            String str6 = "Stream cache already in progress at ";
                            String valueOf3 = String.valueOf(str);
                            if (valueOf3.length() != 0) {
                                str3 = str6.concat(valueOf3);
                            } else {
                                th = new String(str6);
                            }
                            zzkd.zzcx(str3);
                            zza(str5, file.getAbsolutePath(), "inProgress", null);
                            return false;
                        }
                        zzbjp.add(concat);
                    } finally {
                        while (true) {
                            str2 = th;
                        }
                    }
                }
            } else {
                int length = (int) file.length();
                String str7 = "Stream cache hit at ";
                String valueOf4 = String.valueOf(str);
                zzkd.zzcv(valueOf4.length() != 0 ? str7.concat(valueOf4) : new String(str7));
                zza(str5, file.getAbsolutePath(), length);
                return true;
            }
        }
        zza(str5, null, str4, null);
        return false;
    }

    public int zzll() {
        if (this.zzbjr == null) {
            return 0;
        }
        int i = 0;
        for (File name : this.zzbjr.listFiles()) {
            if (!name.getName().endsWith(".done")) {
                i++;
            }
        }
        return i;
    }

    public boolean zzlm() {
        File[] listFiles;
        boolean z = false;
        if (this.zzbjr == null) {
            return false;
        }
        long j = Long.MAX_VALUE;
        File file = null;
        for (File file2 : this.zzbjr.listFiles()) {
            if (!file2.getName().endsWith(".done")) {
                long lastModified = file2.lastModified();
                if (lastModified < j) {
                    file = file2;
                    j = lastModified;
                }
            }
        }
        if (file != null) {
            z = file.delete();
            File zzb = zzb(file);
            if (zzb.isFile()) {
                z &= zzb.delete();
            }
        }
        return z;
    }
}
