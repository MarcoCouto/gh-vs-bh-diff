package com.google.android.gms.internal;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.internal.zzae.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class zzbh extends zzbp {
    public zzbh(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    private void zzcv() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.zzaha) {
            this.zzaha.zzeg = (String) this.zzahh.invoke(null, new Object[]{this.zzaey.getContext()});
        }
    }

    private void zzcw() {
        String str;
        AdvertisingIdClient zzcr = this.zzaey.zzcr();
        if (zzcr == null) {
            str = "E1";
        } else {
            try {
                Info info = zzcr.getInfo();
                String zzo = zzay.zzo(info.getId());
                if (zzo != null) {
                    synchronized (this.zzaha) {
                        this.zzaha.zzeg = zzo;
                        this.zzaha.zzei = Boolean.valueOf(info.isLimitAdTrackingEnabled());
                        this.zzaha.zzeh = Integer.valueOf(5);
                    }
                    return;
                }
                zzp("E");
                return;
            } catch (IOException unused) {
                str = "E";
            }
        }
        zzp(str);
    }

    private void zzp(String str) {
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        if (this.zzaey.zzci()) {
            zzcw();
        } else {
            zzcv();
        }
    }
}
