package com.google.android.gms.internal;

import java.util.Map;

@zzin
public final class zzek implements zzep {
    private final zzel zzbhm;

    public zzek(zzel zzel) {
        this.zzbhm = zzel;
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        String str = (String) map.get("name");
        if (str == null) {
            zzkd.zzcx("App event with no name parameter.");
        } else {
            this.zzbhm.onAppEvent(str, (String) map.get("info"));
        }
    }
}
