package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import com.google.android.gms.internal.zzae.zza;
import com.google.android.gms.internal.zzae.zza.C0049zza;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class zzaq extends zzao {
    private static final String TAG = "zzaq";
    private static long startTime = 0;
    protected static volatile zzax zzaey = null;
    private static Method zzafo = null;
    static boolean zzafq = false;
    protected static final Object zzaft = new Object();
    protected boolean zzafn = false;
    protected String zzafp;
    protected boolean zzafr = false;
    protected boolean zzafs = false;

    protected zzaq(Context context, String str) {
        super(context);
        this.zzafp = str;
        this.zzafn = false;
    }

    protected zzaq(Context context, String str, boolean z) {
        super(context);
        this.zzafp = str;
        this.zzafn = z;
    }

    static List<Long> zza(zzax zzax, MotionEvent motionEvent, DisplayMetrics displayMetrics) throws zzaw {
        zzafo = zzax.zzc(zzav.zzcb(), zzav.zzcc());
        if (zzafo == null || motionEvent == null) {
            throw new zzaw();
        }
        try {
            return (ArrayList) zzafo.invoke(null, new Object[]{motionEvent, displayMetrics});
        } catch (IllegalAccessException e) {
            throw new zzaw(e);
        } catch (InvocationTargetException e2) {
            throw new zzaw(e2);
        }
    }

    protected static synchronized void zza(Context context, boolean z) {
        synchronized (zzaq.class) {
            if (!zzafq) {
                startTime = Calendar.getInstance().getTime().getTime() / 1000;
                zzaey = zzb(context, z);
                zzafq = true;
            }
        }
    }

    private static void zza(zzax zzax) {
        List singletonList = Collections.singletonList(Context.class);
        zzax.zza(zzav.zzbn(), zzav.zzbo(), singletonList);
        zzax.zza(zzav.zzbl(), zzav.zzbm(), singletonList);
        zzax.zza(zzav.zzbx(), zzav.zzby(), singletonList);
        zzax.zza(zzav.zzbv(), zzav.zzbw(), singletonList);
        zzax.zza(zzav.zzbf(), zzav.zzbg(), singletonList);
        zzax.zza(zzav.zzbd(), zzav.zzbe(), singletonList);
        zzax.zza(zzav.zzbb(), zzav.zzbc(), singletonList);
        zzax.zza(zzav.zzbr(), zzav.zzbs(), singletonList);
        zzax.zza(zzav.zzaz(), zzav.zzba(), singletonList);
        zzax.zza(zzav.zzcb(), zzav.zzcc(), Arrays.asList(new Class[]{MotionEvent.class, DisplayMetrics.class}));
        zzax.zza(zzav.zzbj(), zzav.zzbk(), Collections.emptyList());
        zzax.zza(zzav.zzbz(), zzav.zzca(), Collections.emptyList());
        zzax.zza(zzav.zzbt(), zzav.zzbu(), Collections.emptyList());
        zzax.zza(zzav.zzbh(), zzav.zzbi(), Collections.emptyList());
        zzax.zza(zzav.zzbp(), zzav.zzbq(), Collections.emptyList());
    }

    protected static zzax zzb(Context context, boolean z) {
        if (zzaey == null) {
            synchronized (zzaft) {
                if (zzaey == null) {
                    zzax zza = zzax.zza(context, zzav.getKey(), zzav.zzay(), z);
                    zza(zza);
                    zzaey = zza;
                }
            }
        }
        return zzaey;
    }

    /* access modifiers changed from: protected */
    public void zza(zzax zzax, zza zza) {
        if (zzax.zzcd() != null) {
            zza(zzb(zzax, zza));
        }
    }

    /* access modifiers changed from: protected */
    public void zza(List<Callable<Void>> list) {
        if (zzaey != null) {
            ExecutorService zzcd = zzaey.zzcd();
            if (zzcd != null && !list.isEmpty()) {
                try {
                    zzcd.invokeAll(list, ((Long) zzdc.zzbbj.get()).longValue(), TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    Log.d(TAG, String.format("class methods got exception: %s", new Object[]{zzay.zza(e)}));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public List<Callable<Void>> zzb(zzax zzax, zza zza) {
        int zzat = zzax.zzat();
        ArrayList arrayList = new ArrayList();
        zzax zzax2 = zzax;
        zza zza2 = zza;
        zzbb zzbb = new zzbb(zzax2, zzav.zzbn(), zzav.zzbo(), zza2, zzat, 27);
        arrayList.add(zzbb);
        zzbg zzbg = new zzbg(zzax2, zzav.zzbj(), zzav.zzbk(), zza2, startTime, zzat, 25);
        arrayList.add(zzbg);
        int i = zzat;
        zzbl zzbl = new zzbl(zzax2, zzav.zzbt(), zzav.zzbu(), zza2, i, 1);
        arrayList.add(zzbl);
        zzbm zzbm = new zzbm(zzax2, zzav.zzbv(), zzav.zzbw(), zza2, i, 31);
        arrayList.add(zzbm);
        zzbn zzbn = new zzbn(zzax2, zzav.zzbz(), zzav.zzca(), zza2, i, 33);
        arrayList.add(zzbn);
        zzba zzba = new zzba(zzax2, zzav.zzbx(), zzav.zzby(), zza2, i, 29);
        arrayList.add(zzba);
        zzbe zzbe = new zzbe(zzax2, zzav.zzbf(), zzav.zzbg(), zza2, i, 5);
        arrayList.add(zzbe);
        zzbk zzbk = new zzbk(zzax2, zzav.zzbr(), zzav.zzbs(), zza2, i, 12);
        arrayList.add(zzbk);
        zzaz zzaz = new zzaz(zzax2, zzav.zzaz(), zzav.zzba(), zza2, i, 3);
        arrayList.add(zzaz);
        zzbd zzbd = new zzbd(zzax2, zzav.zzbd(), zzav.zzbe(), zza2, i, 34);
        arrayList.add(zzbd);
        zzbc zzbc = new zzbc(zzax2, zzav.zzbb(), zzav.zzbc(), zza2, i, 35);
        arrayList.add(zzbc);
        if (((Boolean) zzdc.zzbbn.get()).booleanValue()) {
            zzbf zzbf = new zzbf(zzax, zzav.zzbh(), zzav.zzbi(), zza, zzat, 44);
            arrayList.add(zzbf);
        }
        if (((Boolean) zzdc.zzbbq.get()).booleanValue()) {
            zzbj zzbj = new zzbj(zzax, zzav.zzbp(), zzav.zzbq(), zza, zzat, 22);
            arrayList.add(zzbj);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public zza zzc(Context context) {
        zza zza = new zza();
        if (!TextUtils.isEmpty(this.zzafp)) {
            zza.zzcs = this.zzafp;
        }
        zzax zzb = zzb(context, this.zzafn);
        zzb.zzcs();
        zza(zzb, zza);
        zzb.zzct();
        return zza;
    }

    /* access modifiers changed from: protected */
    public List<Callable<Void>> zzc(zzax zzax, zza zza) {
        ArrayList arrayList = new ArrayList();
        if (zzax.zzcd() == null) {
            return arrayList;
        }
        int zzat = zzax.zzat();
        arrayList.add(new zzbi(zzax, zza));
        zzax zzax2 = zzax;
        zza zza2 = zza;
        zzbl zzbl = new zzbl(zzax2, zzav.zzbt(), zzav.zzbu(), zza2, zzat, 1);
        arrayList.add(zzbl);
        zzbg zzbg = new zzbg(zzax2, zzav.zzbj(), zzav.zzbk(), zza2, startTime, zzat, 25);
        arrayList.add(zzbg);
        if (((Boolean) zzdc.zzbbo.get()).booleanValue()) {
            zzbf zzbf = new zzbf(zzax, zzav.zzbh(), zzav.zzbi(), zza, zzat, 44);
            arrayList.add(zzbf);
        }
        zzaz zzaz = new zzaz(zzax, zzav.zzaz(), zzav.zzba(), zza, zzat, 3);
        arrayList.add(zzaz);
        if (((Boolean) zzdc.zzbbr.get()).booleanValue()) {
            zzbj zzbj = new zzbj(zzax, zzav.zzbp(), zzav.zzbq(), zza, zzat, 22);
            arrayList.add(zzbj);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public zza zzd(Context context) {
        zza zza = new zza();
        if (!TextUtils.isEmpty(this.zzafp)) {
            zza.zzcs = this.zzafp;
        }
        zzax zzb = zzb(context, this.zzafn);
        zzb.zzcs();
        zzd(zzb, zza);
        zzb.zzct();
        return zza;
    }

    /* access modifiers changed from: protected */
    public void zzd(zzax zzax, zza zza) {
        try {
            List zza2 = zza(zzax, this.zzafd, this.zzafl);
            zza.zzdf = (Long) zza2.get(0);
            zza.zzdg = (Long) zza2.get(1);
            if (((Long) zza2.get(2)).longValue() >= 0) {
                zza.zzdh = (Long) zza2.get(2);
            }
            zza.zzdv = (Long) zza2.get(3);
            zza.zzdw = (Long) zza2.get(4);
        } catch (zzaw unused) {
        }
        if (this.zzaff > 0) {
            zza.zzea = Long.valueOf(this.zzaff);
        }
        if (this.zzafg > 0) {
            zza.zzdz = Long.valueOf(this.zzafg);
        }
        if (this.zzafh > 0) {
            zza.zzdy = Long.valueOf(this.zzafh);
        }
        if (this.zzafi > 0) {
            zza.zzeb = Long.valueOf(this.zzafi);
        }
        if (this.zzafj > 0) {
            zza.zzed = Long.valueOf(this.zzafj);
        }
        try {
            int size = this.zzafe.size() - 1;
            if (size > 0) {
                zza.zzee = new C0049zza[size];
                for (int i = 0; i < size; i++) {
                    List zza3 = zza(zzax, (MotionEvent) this.zzafe.get(i), this.zzafl);
                    C0049zza zza4 = new C0049zza();
                    zza4.zzdf = (Long) zza3.get(0);
                    zza4.zzdg = (Long) zza3.get(1);
                    zza.zzee[i] = zza4;
                }
            }
        } catch (zzaw unused2) {
            zza.zzee = null;
        }
        zza(zzc(zzax, zza));
    }
}
