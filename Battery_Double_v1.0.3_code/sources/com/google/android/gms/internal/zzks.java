package com.google.android.gms.internal;

@zzin
public class zzks<T> {
    private T zzcmu;

    public T get() {
        return this.zzcmu;
    }

    public void set(T t) {
        this.zzcmu = t;
    }
}
