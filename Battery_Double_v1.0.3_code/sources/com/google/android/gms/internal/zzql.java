package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

@TargetApi(11)
public final class zzql extends Fragment implements zzqk {
    private static WeakHashMap<Activity, WeakReference<zzql>> vn = new WeakHashMap<>();
    private Map<String, zzqj> vo = new ArrayMap();
    /* access modifiers changed from: private */
    public Bundle vp;
    /* access modifiers changed from: private */
    public int zzblv = 0;

    private void zzb(final String str, @NonNull final zzqj zzqj) {
        if (this.zzblv > 0) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (zzql.this.zzblv >= 1) {
                        zzqj.onCreate(zzql.this.vp != null ? zzql.this.vp.getBundle(str) : null);
                    }
                    if (zzql.this.zzblv >= 2) {
                        zzqj.onStart();
                    }
                    if (zzql.this.zzblv >= 3) {
                        zzqj.onStop();
                    }
                }
            });
        }
    }

    public static zzql zzt(Activity activity) {
        WeakReference weakReference = (WeakReference) vn.get(activity);
        if (weakReference != null) {
            zzql zzql = (zzql) weakReference.get();
            if (zzql != null) {
                return zzql;
            }
        }
        try {
            zzql zzql2 = (zzql) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
            if (zzql2 == null || zzql2.isRemoving()) {
                zzql2 = new zzql();
                activity.getFragmentManager().beginTransaction().add(zzql2, "LifecycleFragmentImpl").commitAllowingStateLoss();
            }
            vn.put(activity, new WeakReference(zzql2));
            return zzql2;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (zzqj dump : this.vo.values()) {
            dump.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (zzqj onActivityResult : this.vo.values()) {
            onActivityResult.onActivityResult(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzblv = 1;
        this.vp = bundle;
        for (Entry entry : this.vo.entrySet()) {
            ((zzqj) entry.getValue()).onCreate(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Entry entry : this.vo.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((zzqj) entry.getValue()).onSaveInstanceState(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public void onStart() {
        super.onStop();
        this.zzblv = 2;
        for (zzqj onStart : this.vo.values()) {
            onStart.onStart();
        }
    }

    public void onStop() {
        super.onStop();
        this.zzblv = 3;
        for (zzqj onStop : this.vo.values()) {
            onStop.onStop();
        }
    }

    public <T extends zzqj> T zza(String str, Class<T> cls) {
        return (zzqj) cls.cast(this.vo.get(str));
    }

    public void zza(String str, @NonNull zzqj zzqj) {
        if (!this.vo.containsKey(str)) {
            this.vo.put(str, zzqj);
            zzb(str, zzqj);
            return;
        }
        StringBuilder sb = new StringBuilder(59 + String.valueOf(str).length());
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    public Activity zzaqt() {
        return getActivity();
    }
}
