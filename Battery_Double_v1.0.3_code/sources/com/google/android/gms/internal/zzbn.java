package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzbn extends zzbp {
    private static final Object zzafc = new Object();
    private static volatile Long zzahe;

    public zzbn(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        if (zzahe == null) {
            synchronized (zzafc) {
                if (zzahe == null) {
                    zzahe = (Long) this.zzahh.invoke(null, new Object[0]);
                }
            }
        }
        synchronized (this.zzaha) {
            this.zzaha.zzds = zzahe;
        }
    }
}
