package com.google.android.gms.internal;

import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class zzanw {

    private static final class zza extends Writer {
        private final Appendable bfn;
        private final C0050zza bfo;

        /* renamed from: com.google.android.gms.internal.zzanw$zza$zza reason: collision with other inner class name */
        static class C0050zza implements CharSequence {
            char[] bfp;

            C0050zza() {
            }

            public char charAt(int i) {
                return this.bfp[i];
            }

            public int length() {
                return this.bfp.length;
            }

            public CharSequence subSequence(int i, int i2) {
                return new String(this.bfp, i, i2 - i);
            }
        }

        private zza(Appendable appendable) {
            this.bfo = new C0050zza();
            this.bfn = appendable;
        }

        public void close() {
        }

        public void flush() {
        }

        public void write(int i) throws IOException {
            this.bfn.append((char) i);
        }

        public void write(char[] cArr, int i, int i2) throws IOException {
            this.bfo.bfp = cArr;
            this.bfn.append(this.bfo, i, i2 + i);
        }
    }

    public static Writer zza(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new zza(appendable);
    }

    public static void zzb(zzamv zzamv, zzaoo zzaoo) throws IOException {
        zzaok.bgM.zza(zzaoo, zzamv);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        throw new com.google.android.gms.internal.zzamw((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        throw new com.google.android.gms.internal.zzane((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002a, code lost:
        return com.google.android.gms.internal.zzamx.bei;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0030, code lost:
        throw new com.google.android.gms.internal.zzane((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        throw new com.google.android.gms.internal.zzane((java.lang.Throwable) r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0016 A[ExcHandler: IOException (r2v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001d A[ExcHandler: zzaop (r2v4 'e' com.google.android.gms.internal.zzaop A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x000f A[ExcHandler: NumberFormatException (r2v6 'e' java.lang.NumberFormatException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    public static zzamv zzh(zzaom zzaom) throws zzamz {
        boolean z;
        try {
            zzaom.b();
            z = false;
            return (zzamv) zzaok.bgM.zzb(zzaom);
        } catch (EOFException e) {
            e = e;
            if (!z) {
            }
        } catch (zzaop e2) {
        } catch (IOException e3) {
        } catch (NumberFormatException e4) {
        }
    }
}
