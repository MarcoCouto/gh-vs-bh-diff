package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfs.zzc;
import java.util.Map;
import java.util.concurrent.Future;

@zzin
public final class zzir {
    /* access modifiers changed from: private */
    public final Object zzail = new Object();
    /* access modifiers changed from: private */
    public String zzbvq;
    /* access modifiers changed from: private */
    public String zzcem;
    /* access modifiers changed from: private */
    public zzkv<zziu> zzcen = new zzkv<>();
    zzc zzceo;
    public final zzep zzcep = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            synchronized (zzir.this.zzail) {
                if (!zzir.this.zzcen.isDone()) {
                    if (zzir.this.zzbvq.equals(map.get("request_id"))) {
                        zziu zziu = new zziu(1, map);
                        String valueOf = String.valueOf(zziu.getType());
                        String valueOf2 = String.valueOf(zziu.zzrj());
                        StringBuilder sb = new StringBuilder(24 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
                        sb.append("Invalid ");
                        sb.append(valueOf);
                        sb.append(" request error: ");
                        sb.append(valueOf2);
                        zzkd.zzcx(sb.toString());
                        zzir.this.zzcen.zzh(zziu);
                    }
                }
            }
        }
    };
    public final zzep zzceq = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            synchronized (zzir.this.zzail) {
                if (!zzir.this.zzcen.isDone()) {
                    zziu zziu = new zziu(-2, map);
                    if (zzir.this.zzbvq.equals(zziu.getRequestId())) {
                        String url = zziu.getUrl();
                        if (url == null) {
                            zzkd.zzcx("URL missing in loadAdUrl GMSG.");
                            return;
                        }
                        if (url.contains("%40mediation_adapters%40")) {
                            String replaceAll = url.replaceAll("%40mediation_adapters%40", zzkb.zza(zzlh.getContext(), (String) map.get("check_adapters"), zzir.this.zzcem));
                            zziu.setUrl(replaceAll);
                            String str = "Ad request URL modified to ";
                            String valueOf = String.valueOf(replaceAll);
                            zzkd.v(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        }
                        zzir.this.zzcen.zzh(zziu);
                    }
                }
            }
        }
    };
    public final zzep zzcer = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            synchronized (zzir.this.zzail) {
                if (!zzir.this.zzcen.isDone()) {
                    zziu zziu = new zziu(-2, map);
                    if (zzir.this.zzbvq.equals(zziu.getRequestId())) {
                        zziu.zzrm();
                        zzir.this.zzcen.zzh(zziu);
                    }
                }
            }
        }
    };

    public zzir(String str, String str2) {
        this.zzcem = str2;
        this.zzbvq = str;
    }

    public void zzb(zzc zzc) {
        this.zzceo = zzc;
    }

    public zzc zzrg() {
        return this.zzceo;
    }

    public Future<zziu> zzrh() {
        return this.zzcen;
    }

    public void zzri() {
    }
}
