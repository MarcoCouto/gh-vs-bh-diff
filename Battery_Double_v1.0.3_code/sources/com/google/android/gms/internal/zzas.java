package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.view.MotionEvent;

public class zzas {
    private static final String[] zzafy = {"/aclk", "/pcs/click"};
    private String zzafu = "googleads.g.doubleclick.net";
    private String zzafv = "/pagead/ads";
    private String zzafw = "ad.doubleclick.net";
    private String[] zzafx = {".doubleclick.net", ".googleadservices.com", ".googlesyndication.com"};
    private zzan zzafz;

    public zzas(zzan zzan) {
        this.zzafz = zzan;
    }

    private Uri zza(Uri uri, Context context, String str, boolean z) throws zzat {
        try {
            boolean zzb = zzb(uri);
            if (zzb) {
                if (uri.toString().contains("dc_ms=")) {
                    throw new zzat("Parameter already exists: dc_ms");
                }
            } else if (uri.getQueryParameter("ms") != null) {
                throw new zzat("Query parameter already exists: ms");
            }
            String zzb2 = z ? this.zzafz.zzb(context, str) : this.zzafz.zzb(context);
            return zzb ? zzb(uri, "dc_ms", zzb2) : zza(uri, "ms", zzb2);
        } catch (UnsupportedOperationException unused) {
            throw new zzat("Provided Uri is not in a valid state");
        }
    }

    private Uri zza(Uri uri, String str, String str2) throws UnsupportedOperationException {
        String uri2 = uri.toString();
        int indexOf = uri2.indexOf("&adurl");
        if (indexOf == -1) {
            indexOf = uri2.indexOf("?adurl");
        }
        if (indexOf == -1) {
            return uri.buildUpon().appendQueryParameter(str, str2).build();
        }
        int i = indexOf + 1;
        StringBuilder sb = new StringBuilder(uri2.substring(0, i));
        sb.append(str);
        sb.append("=");
        sb.append(str2);
        sb.append("&");
        sb.append(uri2.substring(i));
        return Uri.parse(sb.toString());
    }

    private Uri zzb(Uri uri, String str, String str2) {
        String sb;
        String uri2 = uri.toString();
        int indexOf = uri2.indexOf(";adurl");
        if (indexOf != -1) {
            int i = indexOf + 1;
            StringBuilder sb2 = new StringBuilder(uri2.substring(0, i));
            sb2.append(str);
            sb2.append("=");
            sb2.append(str2);
            sb2.append(";");
            sb2.append(uri2.substring(i));
            sb = sb2.toString();
        } else {
            String encodedPath = uri.getEncodedPath();
            int indexOf2 = uri2.indexOf(encodedPath);
            StringBuilder sb3 = new StringBuilder(uri2.substring(0, encodedPath.length() + indexOf2));
            sb3.append(";");
            sb3.append(str);
            sb3.append("=");
            sb3.append(str2);
            sb3.append(";");
            sb3.append(uri2.substring(indexOf2 + encodedPath.length()));
            sb = sb3.toString();
        }
        return Uri.parse(sb);
    }

    public Uri zza(Uri uri, Context context) throws zzat {
        return zza(uri, context, null, false);
    }

    public void zza(MotionEvent motionEvent) {
        this.zzafz.zza(motionEvent);
    }

    public boolean zza(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }
        try {
            return uri.getHost().equals(this.zzafu) && uri.getPath().equals(this.zzafv);
        } catch (NullPointerException unused) {
            return false;
        }
    }

    public zzan zzaw() {
        return this.zzafz;
    }

    public Uri zzb(Uri uri, Context context) throws zzat {
        try {
            return zza(uri, context, uri.getQueryParameter("ai"), true);
        } catch (UnsupportedOperationException unused) {
            throw new zzat("Provided Uri is not in a valid state");
        }
    }

    public void zzb(String str, String str2) {
        this.zzafu = str;
        this.zzafv = str2;
    }

    public boolean zzb(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }
        try {
            return uri.getHost().equals(this.zzafw);
        } catch (NullPointerException unused) {
            return false;
        }
    }

    public boolean zzc(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }
        try {
            String host = uri.getHost();
            for (String endsWith : this.zzafx) {
                if (host.endsWith(endsWith)) {
                    return true;
                }
            }
        } catch (NullPointerException unused) {
        }
        return false;
    }

    public boolean zzd(Uri uri) {
        if (zzc(uri)) {
            for (String endsWith : zzafy) {
                if (uri.getPath().endsWith(endsWith)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void zzk(String str) {
        this.zzafx = str.split(",");
    }
}
