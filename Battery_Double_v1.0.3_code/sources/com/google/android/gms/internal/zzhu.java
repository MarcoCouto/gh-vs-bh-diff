package com.google.android.gms.internal;

import android.app.Activity;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.internal.zzhq.zza;

@zzin
public final class zzhu extends zzg<zzhq> {
    public zzhu() {
        super("com.google.android.gms.ads.InAppPurchaseManagerCreatorImpl");
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzaz */
    public zzhq zzc(IBinder iBinder) {
        return zza.zzaw(iBinder);
    }

    public zzhp zzg(Activity activity) {
        try {
            return zzhp.zza.zzav(((zzhq) zzcr(activity)).zzo(zze.zzac(activity)));
        } catch (RemoteException | zzg.zza e) {
            zzb.zzd("Could not create remote InAppPurchaseManager.", e);
            return null;
        }
    }
}
