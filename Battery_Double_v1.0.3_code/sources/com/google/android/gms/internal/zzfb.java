package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzu;

@zzin
public class zzfb extends zzkc {
    final zzlh zzbgf;
    final zzfd zzbjb;
    private final String zzbjc;

    zzfb(zzlh zzlh, zzfd zzfd, String str) {
        this.zzbgf = zzlh;
        this.zzbjb = zzfd;
        this.zzbjc = str;
        zzu.zzgj().zza(this);
    }

    public void onStop() {
        this.zzbjb.abort();
    }

    public void zzew() {
        try {
            this.zzbjb.zzaz(this.zzbjc);
        } finally {
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzu.zzgj().zzb(zzfb.this);
                }
            });
        }
    }
}
