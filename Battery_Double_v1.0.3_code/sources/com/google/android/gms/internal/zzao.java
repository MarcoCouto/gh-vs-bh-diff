package com.google.android.gms.internal;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import com.google.android.gms.internal.zzae.zza;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class zzao implements zzan {
    protected MotionEvent zzafd;
    protected LinkedList<MotionEvent> zzafe = new LinkedList<>();
    protected long zzaff = 0;
    protected long zzafg = 0;
    protected long zzafh = 0;
    protected long zzafi = 0;
    protected long zzafj = 0;
    private boolean zzafk = false;
    protected DisplayMetrics zzafl;

    protected zzao(Context context) {
        zzak.zzar();
        try {
            this.zzafl = context.getResources().getDisplayMetrics();
        } catch (UnsupportedOperationException unused) {
            this.zzafl = new DisplayMetrics();
            this.zzafl.density = 1.0f;
        }
    }

    private String zza(Context context, String str, boolean z) {
        zza zza;
        if (z) {
            try {
                zza = zzd(context);
                this.zzafk = true;
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException unused) {
                return Integer.toString(7);
            } catch (Throwable unused2) {
                return Integer.toString(3);
            }
        } else {
            zza = zzc(context);
        }
        if (zza != null) {
            if (zza.aM() != 0) {
                return zzak.zza(zza, str, !zzb(z));
            }
        }
        return Integer.toString(5);
    }

    private void zzav() {
        if (((Boolean) zzdc.zzbbt.get()).booleanValue()) {
            StackTraceElement[] stackTrace = new Throwable().getStackTrace();
            int i = 0;
            for (int length = stackTrace.length - 1; length >= 0; length--) {
                i++;
                if (stackTrace[length].toString().startsWith("com.google.android.ads.") || stackTrace[length].toString().startsWith("com.google.android.gms.")) {
                    break;
                }
            }
            this.zzafj = (long) i;
        }
    }

    private static boolean zzb(boolean z) {
        if (!((Boolean) zzdc.zzbbl.get()).booleanValue()) {
            return true;
        }
        return ((Boolean) zzdc.zzbbu.get()).booleanValue() && z;
    }

    public void zza(int i, int i2, int i3) {
        if (this.zzafd != null) {
            this.zzafd.recycle();
        }
        this.zzafd = MotionEvent.obtain(0, (long) i3, 1, this.zzafl.density * ((float) i), this.zzafl.density * ((float) i2), 0.0f, 0.0f, 0, 0.0f, 0.0f, 0, 0);
    }

    public void zza(MotionEvent motionEvent) {
        if (this.zzafk) {
            this.zzafi = 0;
            this.zzafh = 0;
            this.zzafg = 0;
            this.zzaff = 0;
            this.zzafj = 0;
            Iterator it = this.zzafe.iterator();
            while (it.hasNext()) {
                ((MotionEvent) it.next()).recycle();
            }
            this.zzafe.clear();
            this.zzafd = null;
            this.zzafk = false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.zzaff++;
                return;
            case 1:
                this.zzafd = MotionEvent.obtain(motionEvent);
                this.zzafe.add(this.zzafd);
                if (this.zzafe.size() > 6) {
                    ((MotionEvent) this.zzafe.remove()).recycle();
                }
                this.zzafh++;
                zzav();
                return;
            case 2:
                this.zzafg += (long) (motionEvent.getHistorySize() + 1);
                return;
            case 3:
                this.zzafi++;
                return;
            default:
                return;
        }
    }

    public String zzb(Context context) {
        return zza(context, (String) null, false);
    }

    public String zzb(Context context, String str) {
        return zza(context, str, true);
    }

    /* access modifiers changed from: protected */
    public abstract zza zzc(Context context);

    /* access modifiers changed from: protected */
    public abstract zza zzd(Context context);
}
