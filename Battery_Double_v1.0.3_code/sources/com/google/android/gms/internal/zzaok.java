package com.google.android.gms.internal;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.UUID;

public final class zzaok {
    public static final zzanh<Class> bfX = new zzanh<Class>() {
        public void zza(zzaoo zzaoo, Class cls) throws IOException {
            if (cls == null) {
                zzaoo.l();
                return;
            }
            String valueOf = String.valueOf(cls.getName());
            StringBuilder sb = new StringBuilder(76 + String.valueOf(valueOf).length());
            sb.append("Attempted to serialize java.lang.Class: ");
            sb.append(valueOf);
            sb.append(". Forgot to register a type adapter?");
            throw new UnsupportedOperationException(sb.toString());
        }

        /* renamed from: zzo */
        public Class zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }
    };
    public static final zzani bfY = zza(Class.class, bfX);
    public static final zzanh<BitSet> bfZ = new zzanh<BitSet>() {
        public void zza(zzaoo zzaoo, BitSet bitSet) throws IOException {
            if (bitSet == null) {
                zzaoo.l();
                return;
            }
            zzaoo.h();
            for (int i = 0; i < bitSet.length(); i++) {
                zzaoo.zzcr(bitSet.get(i) ? 1 : 0);
            }
            zzaoo.i();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0059, code lost:
            if (java.lang.Integer.parseInt(r1) != 0) goto L_0x0086;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x005c, code lost:
            r5 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0084, code lost:
            if (r7.nextInt() != 0) goto L_0x0086;
         */
        /* renamed from: zzx */
        public BitSet zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            BitSet bitSet = new BitSet();
            zzaom.beginArray();
            zzaon b = zzaom.b();
            int i = 0;
            while (b != zzaon.END_ARRAY) {
                boolean z = true;
                switch (AnonymousClass26.bfK[b.ordinal()]) {
                    case 1:
                        break;
                    case 2:
                        z = zzaom.nextBoolean();
                        break;
                    case 3:
                        String nextString = zzaom.nextString();
                        try {
                            break;
                        } catch (NumberFormatException unused) {
                            String str = "Error: Expecting: bitset number value (1, 0), Found: ";
                            String valueOf = String.valueOf(nextString);
                            throw new zzane(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        }
                    default:
                        String valueOf2 = String.valueOf(b);
                        StringBuilder sb = new StringBuilder(27 + String.valueOf(valueOf2).length());
                        sb.append("Invalid bitset value type: ");
                        sb.append(valueOf2);
                        throw new zzane(sb.toString());
                }
                if (z) {
                    bitSet.set(i);
                }
                i++;
                b = zzaom.b();
            }
            zzaom.endArray();
            return bitSet;
        }
    };
    public static final zzani bgA = zza(URL.class, bgz);
    public static final zzanh<URI> bgB = new zzanh<URI>() {
        public void zza(zzaoo zzaoo, URI uri) throws IOException {
            zzaoo.zzts(uri == null ? null : uri.toASCIIString());
        }

        /* renamed from: zzw */
        public URI zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                String nextString = zzaom.nextString();
                if ("null".equals(nextString)) {
                    return null;
                }
                return new URI(nextString);
            } catch (URISyntaxException e) {
                throw new zzamw((Throwable) e);
            }
        }
    };
    public static final zzani bgC = zza(URI.class, bgB);
    public static final zzanh<InetAddress> bgD = new zzanh<InetAddress>() {
        public void zza(zzaoo zzaoo, InetAddress inetAddress) throws IOException {
            zzaoo.zzts(inetAddress == null ? null : inetAddress.getHostAddress());
        }

        /* renamed from: zzy */
        public InetAddress zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return InetAddress.getByName(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgE = zzb(InetAddress.class, bgD);
    public static final zzanh<UUID> bgF = new zzanh<UUID>() {
        public void zza(zzaoo zzaoo, UUID uuid) throws IOException {
            zzaoo.zzts(uuid == null ? null : uuid.toString());
        }

        /* renamed from: zzz */
        public UUID zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return UUID.fromString(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgG = zza(UUID.class, bgF);
    public static final zzani bgH = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() != Timestamp.class) {
                return null;
            }
            final zzanh zzk = zzamp.zzk(Date.class);
            return new zzanh<Timestamp>() {
                public void zza(zzaoo zzaoo, Timestamp timestamp) throws IOException {
                    zzk.zza(zzaoo, timestamp);
                }

                /* renamed from: zzaa */
                public Timestamp zzb(zzaom zzaom) throws IOException {
                    Date date = (Date) zzk.zzb(zzaom);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }
            };
        }
    };
    public static final zzanh<Calendar> bgI = new zzanh<Calendar>() {
        public void zza(zzaoo zzaoo, Calendar calendar) throws IOException {
            if (calendar == null) {
                zzaoo.l();
                return;
            }
            zzaoo.j();
            zzaoo.zztr("year");
            zzaoo.zzcr((long) calendar.get(1));
            zzaoo.zztr("month");
            zzaoo.zzcr((long) calendar.get(2));
            zzaoo.zztr("dayOfMonth");
            zzaoo.zzcr((long) calendar.get(5));
            zzaoo.zztr("hourOfDay");
            zzaoo.zzcr((long) calendar.get(11));
            zzaoo.zztr("minute");
            zzaoo.zzcr((long) calendar.get(12));
            zzaoo.zztr("second");
            zzaoo.zzcr((long) calendar.get(13));
            zzaoo.k();
        }

        /* renamed from: zzab */
        public Calendar zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            zzaom.beginObject();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (zzaom.b() != zzaon.END_OBJECT) {
                String nextName = zzaom.nextName();
                int nextInt = zzaom.nextInt();
                if ("year".equals(nextName)) {
                    i = nextInt;
                } else if ("month".equals(nextName)) {
                    i2 = nextInt;
                } else if ("dayOfMonth".equals(nextName)) {
                    i3 = nextInt;
                } else if ("hourOfDay".equals(nextName)) {
                    i4 = nextInt;
                } else if ("minute".equals(nextName)) {
                    i5 = nextInt;
                } else if ("second".equals(nextName)) {
                    i6 = nextInt;
                }
            }
            zzaom.endObject();
            GregorianCalendar gregorianCalendar = new GregorianCalendar(i, i2, i3, i4, i5, i6);
            return gregorianCalendar;
        }
    };
    public static final zzani bgJ = zzb(Calendar.class, GregorianCalendar.class, bgI);
    public static final zzanh<Locale> bgK = new zzanh<Locale>() {
        public void zza(zzaoo zzaoo, Locale locale) throws IOException {
            zzaoo.zzts(locale == null ? null : locale.toString());
        }

        /* renamed from: zzac */
        public Locale zzb(zzaom zzaom) throws IOException {
            String str = null;
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(zzaom.nextString(), "_");
            String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            if (stringTokenizer.hasMoreElements()) {
                str = stringTokenizer.nextToken();
            }
            return (nextToken2 == null && str == null) ? new Locale(nextToken) : str == null ? new Locale(nextToken, nextToken2) : new Locale(nextToken, nextToken2, str);
        }
    };
    public static final zzani bgL = zza(Locale.class, bgK);
    public static final zzanh<zzamv> bgM = new zzanh<zzamv>() {
        public void zza(zzaoo zzaoo, zzamv zzamv) throws IOException {
            if (zzamv == null || zzamv.zzczj()) {
                zzaoo.l();
            } else if (zzamv.zzczi()) {
                zzanb zzczm = zzamv.zzczm();
                if (zzczm.zzczp()) {
                    zzaoo.zza(zzczm.zzcze());
                } else if (zzczm.zzczo()) {
                    zzaoo.zzda(zzczm.getAsBoolean());
                } else {
                    zzaoo.zzts(zzczm.zzczf());
                }
            } else if (zzamv.zzczg()) {
                zzaoo.h();
                Iterator it = zzamv.zzczl().iterator();
                while (it.hasNext()) {
                    zza(zzaoo, (zzamv) it.next());
                }
                zzaoo.i();
            } else if (zzamv.zzczh()) {
                zzaoo.j();
                for (Entry entry : zzamv.zzczk().entrySet()) {
                    zzaoo.zztr((String) entry.getKey());
                    zza(zzaoo, (zzamv) entry.getValue());
                }
                zzaoo.k();
            } else {
                String valueOf = String.valueOf(zzamv.getClass());
                StringBuilder sb = new StringBuilder(15 + String.valueOf(valueOf).length());
                sb.append("Couldn't write ");
                sb.append(valueOf);
                throw new IllegalArgumentException(sb.toString());
            }
        }

        /* renamed from: zzad */
        public zzamv zzb(zzaom zzaom) throws IOException {
            switch (AnonymousClass26.bfK[zzaom.b().ordinal()]) {
                case 1:
                    return new zzanb((Number) new zzans(zzaom.nextString()));
                case 2:
                    return new zzanb(Boolean.valueOf(zzaom.nextBoolean()));
                case 3:
                    return new zzanb(zzaom.nextString());
                case 4:
                    zzaom.nextNull();
                    return zzamx.bei;
                case 5:
                    zzams zzams = new zzams();
                    zzaom.beginArray();
                    while (zzaom.hasNext()) {
                        zzams.zzc((zzamv) zzb(zzaom));
                    }
                    zzaom.endArray();
                    return zzams;
                case 6:
                    zzamy zzamy = new zzamy();
                    zzaom.beginObject();
                    while (zzaom.hasNext()) {
                        zzamy.zza(zzaom.nextName(), (zzamv) zzb(zzaom));
                    }
                    zzaom.endObject();
                    return zzamy;
                default:
                    throw new IllegalArgumentException();
            }
        }
    };
    public static final zzani bgN = zzb(zzamv.class, bgM);
    public static final zzani bgO = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            Class<Enum> m = zzaol.m();
            if (!Enum.class.isAssignableFrom(m) || m == Enum.class) {
                return null;
            }
            if (!m.isEnum()) {
                m = m.getSuperclass();
            }
            return new zza(m);
        }
    };
    public static final zzani bga = zza(BitSet.class, bfZ);
    public static final zzanh<Boolean> bgb = new zzanh<Boolean>() {
        public void zza(zzaoo zzaoo, Boolean bool) throws IOException {
            if (bool == null) {
                zzaoo.l();
            } else {
                zzaoo.zzda(bool.booleanValue());
            }
        }

        /* renamed from: zzae */
        public Boolean zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return zzaom.b() == zzaon.STRING ? Boolean.valueOf(Boolean.parseBoolean(zzaom.nextString())) : Boolean.valueOf(zzaom.nextBoolean());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<Boolean> bgc = new zzanh<Boolean>() {
        public void zza(zzaoo zzaoo, Boolean bool) throws IOException {
            zzaoo.zzts(bool == null ? "null" : bool.toString());
        }

        /* renamed from: zzae */
        public Boolean zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return Boolean.valueOf(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgd = zza(Boolean.TYPE, Boolean.class, bgb);
    public static final zzanh<Number> bge = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Byte.valueOf((byte) zzaom.nextInt());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgf = zza(Byte.TYPE, Byte.class, bge);
    public static final zzanh<Number> bgg = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Short.valueOf((short) zzaom.nextInt());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgh = zza(Short.TYPE, Short.class, bgg);
    public static final zzanh<Number> bgi = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Integer.valueOf(zzaom.nextInt());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgj = zza(Integer.TYPE, Integer.class, bgi);
    public static final zzanh<Number> bgk = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Long.valueOf(zzaom.nextLong());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzanh<Number> bgl = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return Float.valueOf((float) zzaom.nextDouble());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<Number> bgm = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return Double.valueOf(zzaom.nextDouble());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<Number> bgn = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            zzaon b = zzaom.b();
            int i = AnonymousClass26.bfK[b.ordinal()];
            if (i == 1) {
                return new zzans(zzaom.nextString());
            }
            if (i != 4) {
                String valueOf = String.valueOf(b);
                StringBuilder sb = new StringBuilder(23 + String.valueOf(valueOf).length());
                sb.append("Expecting number, got: ");
                sb.append(valueOf);
                throw new zzane(sb.toString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgo = zza(Number.class, bgn);
    public static final zzanh<Character> bgp = new zzanh<Character>() {
        public void zza(zzaoo zzaoo, Character ch) throws IOException {
            zzaoo.zzts(ch == null ? null : String.valueOf(ch));
        }

        /* renamed from: zzp */
        public Character zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            String nextString = zzaom.nextString();
            if (nextString.length() == 1) {
                return Character.valueOf(nextString.charAt(0));
            }
            String str = "Expecting character, got: ";
            String valueOf = String.valueOf(nextString);
            throw new zzane(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
    };
    public static final zzani bgq = zza(Character.TYPE, Character.class, bgp);
    public static final zzanh<String> bgr = new zzanh<String>() {
        public void zza(zzaoo zzaoo, String str) throws IOException {
            zzaoo.zzts(str);
        }

        /* renamed from: zzq */
        public String zzb(zzaom zzaom) throws IOException {
            zzaon b = zzaom.b();
            if (b != zzaon.NULL) {
                return b == zzaon.BOOLEAN ? Boolean.toString(zzaom.nextBoolean()) : zzaom.nextString();
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<BigDecimal> bgs = new zzanh<BigDecimal>() {
        public void zza(zzaoo zzaoo, BigDecimal bigDecimal) throws IOException {
            zzaoo.zza(bigDecimal);
        }

        /* renamed from: zzr */
        public BigDecimal zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return new BigDecimal(zzaom.nextString());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzanh<BigInteger> bgt = new zzanh<BigInteger>() {
        public void zza(zzaoo zzaoo, BigInteger bigInteger) throws IOException {
            zzaoo.zza(bigInteger);
        }

        /* renamed from: zzs */
        public BigInteger zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return new BigInteger(zzaom.nextString());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgu = zza(String.class, bgr);
    public static final zzanh<StringBuilder> bgv = new zzanh<StringBuilder>() {
        public void zza(zzaoo zzaoo, StringBuilder sb) throws IOException {
            zzaoo.zzts(sb == null ? null : sb.toString());
        }

        /* renamed from: zzt */
        public StringBuilder zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return new StringBuilder(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgw = zza(StringBuilder.class, bgv);
    public static final zzanh<StringBuffer> bgx = new zzanh<StringBuffer>() {
        public void zza(zzaoo zzaoo, StringBuffer stringBuffer) throws IOException {
            zzaoo.zzts(stringBuffer == null ? null : stringBuffer.toString());
        }

        /* renamed from: zzu */
        public StringBuffer zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return new StringBuffer(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgy = zza(StringBuffer.class, bgx);
    public static final zzanh<URL> bgz = new zzanh<URL>() {
        public void zza(zzaoo zzaoo, URL url) throws IOException {
            zzaoo.zzts(url == null ? null : url.toExternalForm());
        }

        /* renamed from: zzv */
        public URL zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            String nextString = zzaom.nextString();
            if ("null".equals(nextString)) {
                return null;
            }
            return new URL(nextString);
        }
    };

    /* renamed from: com.google.android.gms.internal.zzaok$26 reason: invalid class name */
    static /* synthetic */ class AnonymousClass26 {
        static final /* synthetic */ int[] bfK = new int[zzaon.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(3:19|20|22)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|22) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            bfK[zzaon.NUMBER.ordinal()] = 1;
            bfK[zzaon.BOOLEAN.ordinal()] = 2;
            bfK[zzaon.STRING.ordinal()] = 3;
            bfK[zzaon.NULL.ordinal()] = 4;
            bfK[zzaon.BEGIN_ARRAY.ordinal()] = 5;
            bfK[zzaon.BEGIN_OBJECT.ordinal()] = 6;
            bfK[zzaon.END_DOCUMENT.ordinal()] = 7;
            bfK[zzaon.NAME.ordinal()] = 8;
            bfK[zzaon.END_OBJECT.ordinal()] = 9;
            try {
                bfK[zzaon.END_ARRAY.ordinal()] = 10;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    private static final class zza<T extends Enum<T>> extends zzanh<T> {
        private final Map<String, T> bgY = new HashMap();
        private final Map<T, String> bgZ = new HashMap();

        public zza(Class<T> cls) {
            Enum[] enumArr;
            try {
                for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                    String name = enumR.name();
                    zzank zzank = (zzank) cls.getField(name).getAnnotation(zzank.class);
                    if (zzank != null) {
                        name = zzank.value();
                        for (String put : zzank.zzczs()) {
                            this.bgY.put(put, enumR);
                        }
                    }
                    this.bgY.put(name, enumR);
                    this.bgZ.put(enumR, name);
                }
            } catch (NoSuchFieldException unused) {
                throw new AssertionError();
            }
        }

        public void zza(zzaoo zzaoo, T t) throws IOException {
            zzaoo.zzts(t == null ? null : (String) this.bgZ.get(t));
        }

        /* renamed from: zzaf */
        public T zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return (Enum) this.bgY.get(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    }

    public static <TT> zzani zza(final zzaol<TT> zzaol, final zzanh<TT> zzanh) {
        return new zzani() {
            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                if (zzaol.equals(zzaol)) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zza(final Class<TT> cls, final zzanh<TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(zzanh);
                StringBuilder sb = new StringBuilder(23 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
                sb.append("Factory[type=");
                sb.append(valueOf);
                sb.append(",adapter=");
                sb.append(valueOf2);
                sb.append("]");
                return sb.toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                if (zzaol.m() == cls) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zza(final Class<TT> cls, final Class<TT> cls2, final zzanh<? super TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls2.getName());
                String valueOf2 = String.valueOf(cls.getName());
                String valueOf3 = String.valueOf(zzanh);
                StringBuilder sb = new StringBuilder(24 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length());
                sb.append("Factory[type=");
                sb.append(valueOf);
                sb.append("+");
                sb.append(valueOf2);
                sb.append(",adapter=");
                sb.append(valueOf3);
                sb.append("]");
                return sb.toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                Class m = zzaol.m();
                if (m == cls || m == cls2) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zzb(final Class<TT> cls, final zzanh<TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(zzanh);
                StringBuilder sb = new StringBuilder(32 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
                sb.append("Factory[typeHierarchy=");
                sb.append(valueOf);
                sb.append(",adapter=");
                sb.append(valueOf2);
                sb.append("]");
                return sb.toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                if (cls.isAssignableFrom(zzaol.m())) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zzb(final Class<TT> cls, final Class<? extends TT> cls2, final zzanh<? super TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(cls2.getName());
                String valueOf3 = String.valueOf(zzanh);
                StringBuilder sb = new StringBuilder(24 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length());
                sb.append("Factory[type=");
                sb.append(valueOf);
                sb.append("+");
                sb.append(valueOf2);
                sb.append(",adapter=");
                sb.append(valueOf3);
                sb.append("]");
                return sb.toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                Class m = zzaol.m();
                if (m == cls || m == cls2) {
                    return zzanh;
                }
                return null;
            }
        };
    }
}
