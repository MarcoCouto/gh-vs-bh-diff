package com.google.android.gms.internal;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public abstract class zzcd implements OnGlobalLayoutListener, OnScrollChangedListener {
    protected final Object zzail = new Object();
    private boolean zzane = false;
    private zzkr zzaqb;
    private final WeakReference<zzju> zzaqh;
    private WeakReference<ViewTreeObserver> zzaqi;
    private final zzck zzaqj;
    protected final zzcf zzaqk;
    private final Context zzaql;
    private final WindowManager zzaqm;
    private final PowerManager zzaqn;
    private final KeyguardManager zzaqo;
    @Nullable
    private zzch zzaqp;
    private boolean zzaqq;
    private boolean zzaqr = false;
    private boolean zzaqs;
    private boolean zzaqt;
    private boolean zzaqu;
    @Nullable
    BroadcastReceiver zzaqv;
    private final HashSet<zzce> zzaqw = new HashSet<>();
    private final zzep zzaqx = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            if (zzcd.this.zzb(map)) {
                zzcd.this.zza(zzlh.getView(), map);
            }
        }
    };
    private final zzep zzaqy = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            if (zzcd.this.zzb(map)) {
                String str = "Received request to untrack: ";
                String valueOf = String.valueOf(zzcd.this.zzaqk.zzhn());
                zzkd.zzcv(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                zzcd.this.destroy();
            }
        }
    };
    private final zzep zzaqz = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            if (zzcd.this.zzb(map) && map.containsKey("isVisible")) {
                zzcd.this.zzj(Boolean.valueOf("1".equals(map.get("isVisible")) || "true".equals(map.get("isVisible"))).booleanValue());
            }
        }
    };

    public static class zza implements zzck {
        private WeakReference<zzh> zzarb;

        public zza(zzh zzh) {
            this.zzarb = new WeakReference<>(zzh);
        }

        @Nullable
        public View zzhh() {
            zzh zzh = (zzh) this.zzarb.get();
            if (zzh != null) {
                return zzh.zzlc();
            }
            return null;
        }

        public boolean zzhi() {
            return this.zzarb.get() == null;
        }

        public zzck zzhj() {
            return new zzb((zzh) this.zzarb.get());
        }
    }

    public static class zzb implements zzck {
        private zzh zzarc;

        public zzb(zzh zzh) {
            this.zzarc = zzh;
        }

        public View zzhh() {
            return this.zzarc.zzlc();
        }

        public boolean zzhi() {
            return this.zzarc == null;
        }

        public zzck zzhj() {
            return this;
        }
    }

    public static class zzc implements zzck {
        @Nullable
        private final View mView;
        @Nullable
        private final zzju zzard;

        public zzc(View view, zzju zzju) {
            this.mView = view;
            this.zzard = zzju;
        }

        public View zzhh() {
            return this.mView;
        }

        public boolean zzhi() {
            return this.zzard == null || this.mView == null;
        }

        public zzck zzhj() {
            return this;
        }
    }

    public static class zzd implements zzck {
        private final WeakReference<View> zzare;
        private final WeakReference<zzju> zzarf;

        public zzd(View view, zzju zzju) {
            this.zzare = new WeakReference<>(view);
            this.zzarf = new WeakReference<>(zzju);
        }

        public View zzhh() {
            return (View) this.zzare.get();
        }

        public boolean zzhi() {
            return this.zzare.get() == null || this.zzarf.get() == null;
        }

        public zzck zzhj() {
            return new zzc((View) this.zzare.get(), (zzju) this.zzarf.get());
        }
    }

    public zzcd(Context context, AdSizeParcel adSizeParcel, zzju zzju, VersionInfoParcel versionInfoParcel, zzck zzck) {
        this.zzaqh = new WeakReference<>(zzju);
        this.zzaqj = zzck;
        this.zzaqi = new WeakReference<>(null);
        this.zzaqs = true;
        this.zzaqu = false;
        this.zzaqb = new zzkr(200);
        zzcf zzcf = new zzcf(UUID.randomUUID().toString(), versionInfoParcel, adSizeParcel.zzaur, zzju.zzcie, zzju.zzho(), adSizeParcel.zzauu);
        this.zzaqk = zzcf;
        this.zzaqm = (WindowManager) context.getSystemService("window");
        this.zzaqn = (PowerManager) context.getApplicationContext().getSystemService("power");
        this.zzaqo = (KeyguardManager) context.getSystemService("keyguard");
        this.zzaql = context;
    }

    /* access modifiers changed from: protected */
    public void destroy() {
        synchronized (this.zzail) {
            zzhc();
            zzgx();
            this.zzaqs = false;
            zzgz();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isScreenOn() {
        return this.zzaqn.isScreenOn();
    }

    public void onGlobalLayout() {
        zzk(2);
    }

    public void onScrollChanged() {
        zzk(1);
    }

    public void pause() {
        synchronized (this.zzail) {
            this.zzane = true;
            zzk(3);
        }
    }

    public void resume() {
        synchronized (this.zzail) {
            this.zzane = false;
            zzk(3);
        }
    }

    public void stop() {
        synchronized (this.zzail) {
            this.zzaqr = true;
            zzk(3);
        }
    }

    /* access modifiers changed from: protected */
    public int zza(int i, DisplayMetrics displayMetrics) {
        return (int) (((float) i) / displayMetrics.density);
    }

    /* access modifiers changed from: protected */
    public void zza(View view, Map<String, String> map) {
        zzk(3);
    }

    public void zza(zzce zzce) {
        this.zzaqw.add(zzce);
    }

    public void zza(zzch zzch) {
        synchronized (this.zzail) {
            this.zzaqp = zzch;
        }
    }

    /* access modifiers changed from: protected */
    public void zza(JSONObject jSONObject) {
        try {
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONArray.put(jSONObject);
            jSONObject2.put("units", jSONArray);
            zzb(jSONObject2);
        } catch (Throwable th) {
            zzkd.zzb("Skipping active view message.", th);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzb(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public boolean zzb(@Nullable Map<String, String> map) {
        boolean z = false;
        if (map == null) {
            return false;
        }
        String str = (String) map.get("hashCode");
        if (!TextUtils.isEmpty(str) && str.equals(this.zzaqk.zzhn())) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void zzc(zzft zzft) {
        zzft.zza("/updateActiveView", this.zzaqx);
        zzft.zza("/untrackActiveViewUnit", this.zzaqy);
        zzft.zza("/visibilityChanged", this.zzaqz);
    }

    /* access modifiers changed from: protected */
    public JSONObject zzd(@Nullable View view) throws JSONException {
        if (view == null) {
            return zzhf();
        }
        boolean isAttachedToWindow = zzu.zzfs().isAttachedToWindow(view);
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        try {
            view.getLocationOnScreen(iArr);
            view.getLocationInWindow(iArr2);
        } catch (Exception e) {
            zzkd.zzb("Failure getting view location.", e);
        }
        DisplayMetrics displayMetrics = view.getContext().getResources().getDisplayMetrics();
        Rect rect = new Rect();
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        Rect rect2 = new Rect();
        rect2.right = this.zzaqm.getDefaultDisplay().getWidth();
        rect2.bottom = this.zzaqm.getDefaultDisplay().getHeight();
        Rect rect3 = new Rect();
        boolean globalVisibleRect = view.getGlobalVisibleRect(rect3, null);
        Rect rect4 = new Rect();
        boolean localVisibleRect = view.getLocalVisibleRect(rect4);
        Rect rect5 = new Rect();
        view.getHitRect(rect5);
        JSONObject zzhd = zzhd();
        zzhd.put("windowVisibility", view.getWindowVisibility()).put("isAttachedToWindow", isAttachedToWindow).put("viewBox", new JSONObject().put("top", zza(rect2.top, displayMetrics)).put("bottom", zza(rect2.bottom, displayMetrics)).put("left", zza(rect2.left, displayMetrics)).put("right", zza(rect2.right, displayMetrics))).put("adBox", new JSONObject().put("top", zza(rect.top, displayMetrics)).put("bottom", zza(rect.bottom, displayMetrics)).put("left", zza(rect.left, displayMetrics)).put("right", zza(rect.right, displayMetrics))).put("globalVisibleBox", new JSONObject().put("top", zza(rect3.top, displayMetrics)).put("bottom", zza(rect3.bottom, displayMetrics)).put("left", zza(rect3.left, displayMetrics)).put("right", zza(rect3.right, displayMetrics))).put("globalVisibleBoxVisible", globalVisibleRect).put("localVisibleBox", new JSONObject().put("top", zza(rect4.top, displayMetrics)).put("bottom", zza(rect4.bottom, displayMetrics)).put("left", zza(rect4.left, displayMetrics)).put("right", zza(rect4.right, displayMetrics))).put("localVisibleBoxVisible", localVisibleRect).put("hitBox", new JSONObject().put("top", zza(rect5.top, displayMetrics)).put("bottom", zza(rect5.bottom, displayMetrics)).put("left", zza(rect5.left, displayMetrics)).put("right", zza(rect5.right, displayMetrics))).put("screenDensity", (double) displayMetrics.density).put("isVisible", zzu.zzfq().zza(view, this.zzaqn, this.zzaqo));
        return zzhd;
    }

    /* access modifiers changed from: protected */
    public void zzd(zzft zzft) {
        zzft.zzb("/visibilityChanged", this.zzaqz);
        zzft.zzb("/untrackActiveViewUnit", this.zzaqy);
        zzft.zzb("/updateActiveView", this.zzaqx);
    }

    /* access modifiers changed from: protected */
    public void zzgw() {
        synchronized (this.zzail) {
            if (this.zzaqv == null) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.SCREEN_ON");
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                this.zzaqv = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        zzcd.this.zzk(3);
                    }
                };
                this.zzaql.registerReceiver(this.zzaqv, intentFilter);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzgx() {
        synchronized (this.zzail) {
            if (this.zzaqv != null) {
                try {
                    this.zzaql.unregisterReceiver(this.zzaqv);
                } catch (IllegalStateException e) {
                    zzkd.zzb("Failed trying to unregister the receiver", e);
                } catch (Exception e2) {
                    zzu.zzft().zzb((Throwable) e2, true);
                }
                this.zzaqv = null;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002f A[Catch:{ JSONException -> 0x0019, RuntimeException -> 0x0012 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0034 A[Catch:{ JSONException -> 0x0019, RuntimeException -> 0x0012 }] */
    public void zzgy() {
        String str;
        synchronized (this.zzail) {
            if (this.zzaqs) {
                this.zzaqt = true;
                try {
                    zza(zzhg());
                } catch (JSONException e) {
                    e = e;
                    str = "JSON failure while processing active view data.";
                } catch (RuntimeException e2) {
                    e = e2;
                    str = "Failure while processing active view data.";
                }
                String str2 = "Untracking ad unit: ";
                String valueOf = String.valueOf(this.zzaqk.zzhn());
                zzkd.zzcv(valueOf.length() == 0 ? str2.concat(valueOf) : new String(str2));
            }
        }
        zzkd.zzb(str, e);
        String str22 = "Untracking ad unit: ";
        String valueOf2 = String.valueOf(this.zzaqk.zzhn());
        zzkd.zzcv(valueOf2.length() == 0 ? str22.concat(valueOf2) : new String(str22));
    }

    /* access modifiers changed from: protected */
    public void zzgz() {
        if (this.zzaqp != null) {
            this.zzaqp.zza(this);
        }
    }

    public boolean zzha() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzaqs;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void zzhb() {
        View zzhh = this.zzaqj.zzhj().zzhh();
        if (zzhh != null) {
            ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.zzaqi.get();
            ViewTreeObserver viewTreeObserver2 = zzhh.getViewTreeObserver();
            if (viewTreeObserver2 != viewTreeObserver) {
                zzhc();
                if (!this.zzaqq || (viewTreeObserver != null && viewTreeObserver.isAlive())) {
                    this.zzaqq = true;
                    viewTreeObserver2.addOnScrollChangedListener(this);
                    viewTreeObserver2.addOnGlobalLayoutListener(this);
                }
                this.zzaqi = new WeakReference<>(viewTreeObserver2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzhc() {
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.zzaqi.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnScrollChangedListener(this);
            viewTreeObserver.removeGlobalOnLayoutListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public JSONObject zzhd() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("afmaVersion", this.zzaqk.zzhl()).put("activeViewJSON", this.zzaqk.zzhm()).put("timestamp", zzu.zzfu().elapsedRealtime()).put("adFormat", this.zzaqk.zzhk()).put("hashCode", this.zzaqk.zzhn()).put("isMraid", this.zzaqk.zzho()).put("isStopped", this.zzaqr).put("isPaused", this.zzane).put("isScreenOn", isScreenOn()).put("isNative", this.zzaqk.zzhp());
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public abstract boolean zzhe();

    /* access modifiers changed from: protected */
    public JSONObject zzhf() throws JSONException {
        return zzhd().put("isAttachedToWindow", false).put("isScreenOn", isScreenOn()).put("isVisible", false);
    }

    /* access modifiers changed from: protected */
    public JSONObject zzhg() throws JSONException {
        JSONObject zzhd = zzhd();
        zzhd.put("doneReasonCode", "u");
        return zzhd;
    }

    /* access modifiers changed from: protected */
    public void zzj(boolean z) {
        Iterator it = this.zzaqw.iterator();
        while (it.hasNext()) {
            ((zzce) it.next()).zza(this, z);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0079, code lost:
        return;
     */
    public void zzk(int i) {
        synchronized (this.zzail) {
            if (zzhe()) {
                if (this.zzaqs) {
                    View zzhh = this.zzaqj.zzhh();
                    boolean z = false;
                    boolean z2 = zzhh != null && zzu.zzfq().zza(zzhh, this.zzaqn, this.zzaqo) && zzhh.getGlobalVisibleRect(new Rect(), null);
                    this.zzaqu = z2;
                    if (this.zzaqj.zzhi()) {
                        zzgy();
                        return;
                    }
                    if (i == 1) {
                        z = true;
                    }
                    if (z && !this.zzaqb.tryAcquire() && z2 == this.zzaqu) {
                        return;
                    }
                    if (z2 || this.zzaqu || i != 1) {
                        try {
                            zza(zzd(zzhh));
                        } catch (RuntimeException | JSONException e) {
                            zzkd.zza("Active view update failed.", e);
                        }
                        zzhb();
                        zzgz();
                    }
                }
            }
        }
    }
}
