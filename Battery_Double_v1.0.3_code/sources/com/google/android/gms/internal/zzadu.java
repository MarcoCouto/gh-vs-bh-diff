package com.google.android.gms.internal;

import com.google.android.gms.internal.zzah.zzf;
import com.google.android.gms.internal.zzah.zzj;
import java.io.IOException;

public interface zzadu {

    public static final class zza extends zzapp<zza> {
        public long aCV;
        public zzj aCW;
        public zzf zzwr;

        public zza() {
            zzcgx();
        }

        public static zza zzao(byte[] bArr) throws zzapu {
            return (zza) zzapv.zza(new zza(), bArr);
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.aCV != zza.aCV) {
                return false;
            }
            if (this.zzwr == null) {
                if (zza.zzwr != null) {
                    return false;
                }
            } else if (!this.zzwr.equals(zza.zzwr)) {
                return false;
            }
            if (this.aCW == null) {
                if (zza.aCW != null) {
                    return false;
                }
            } else if (!this.aCW.equals(zza.aCW)) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zza.bjx);
            }
            if (zza.bjx != null) {
                if (zza.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((((((527 + getClass().getName().hashCode()) * 31) + ((int) (this.aCV ^ (this.aCV >>> 32)))) * 31) + (this.zzwr == null ? 0 : this.zzwr.hashCode())) * 31) + (this.aCW == null ? 0 : this.aCW.hashCode()));
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            zzapo.zzb(1, this.aCV);
            if (this.zzwr != null) {
                zzapo.zza(2, (zzapv) this.zzwr);
            }
            if (this.aCW != null) {
                zzapo.zza(3, (zzapv) this.aCW);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzas */
        public zza zzb(zzapn zzapn) throws IOException {
            zzapv zzapv;
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah != 8) {
                    if (ah == 18) {
                        if (this.zzwr == null) {
                            this.zzwr = new zzf();
                        }
                        zzapv = this.zzwr;
                    } else if (ah == 26) {
                        if (this.aCW == null) {
                            this.aCW = new zzj();
                        }
                        zzapv = this.aCW;
                    } else if (!super.zza(zzapn, ah)) {
                        return this;
                    }
                    zzapn.zza(zzapv);
                } else {
                    this.aCV = zzapn.ak();
                }
            }
        }

        public zza zzcgx() {
            this.aCV = 0;
            this.zzwr = null;
            this.aCW = null;
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx() + zzapo.zze(1, this.aCV);
            if (this.zzwr != null) {
                zzx += zzapo.zzc(2, (zzapv) this.zzwr);
            }
            return this.aCW != null ? zzx + zzapo.zzc(3, (zzapv) this.aCW) : zzx;
        }
    }
}
