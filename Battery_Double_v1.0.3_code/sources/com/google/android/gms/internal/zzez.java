package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.overlay.zzk;
import java.util.Map;
import java.util.WeakHashMap;
import org.json.JSONObject;

@zzin
public final class zzez implements zzep {
    private final Map<zzlh, Integer> zzbiz = new WeakHashMap();
    private boolean zzbja;

    private static int zza(Context context, Map<String, String> map, String str, int i) {
        String str2 = (String) map.get(str);
        if (str2 != null) {
            try {
                return zzm.zziw().zza(context, Integer.parseInt(str2));
            } catch (NumberFormatException unused) {
                StringBuilder sb = new StringBuilder(34 + String.valueOf(str).length() + String.valueOf(str2).length());
                sb.append("Could not parse ");
                sb.append(str);
                sb.append(" in a video GMSG: ");
                sb.append(str2);
                zzkd.zzcx(sb.toString());
            }
        }
        return i;
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        int i;
        String str;
        String str2;
        String valueOf;
        String str3;
        String str4 = (String) map.get("action");
        if (str4 == null) {
            str3 = "Action missing from video GMSG.";
        } else {
            if (zzkd.zzaz(3)) {
                JSONObject jSONObject = new JSONObject(map);
                jSONObject.remove("google.afma.Notify_dt");
                String valueOf2 = String.valueOf(jSONObject.toString());
                StringBuilder sb = new StringBuilder(13 + String.valueOf(str4).length() + String.valueOf(valueOf2).length());
                sb.append("Video GMSG: ");
                sb.append(str4);
                sb.append(" ");
                sb.append(valueOf2);
                zzkd.zzcv(sb.toString());
            }
            if ("background".equals(str4)) {
                String str5 = (String) map.get("color");
                if (TextUtils.isEmpty(str5)) {
                    str3 = "Color parameter missing from color video GMSG.";
                } else {
                    try {
                        int parseColor = Color.parseColor(str5);
                        zzlg zzuq = zzlh.zzuq();
                        if (zzuq != null) {
                            zzk zzub = zzuq.zzub();
                            if (zzub != null) {
                                zzub.setBackgroundColor(parseColor);
                                return;
                            }
                        }
                        this.zzbiz.put(zzlh, Integer.valueOf(parseColor));
                        return;
                    } catch (IllegalArgumentException unused) {
                        zzkd.zzcx("Invalid color parameter in video GMSG.");
                        return;
                    }
                }
            } else {
                zzlg zzuq2 = zzlh.zzuq();
                if (zzuq2 == null) {
                    str3 = "Could not get underlay container for a video GMSG.";
                } else {
                    boolean equals = "new".equals(str4);
                    boolean equals2 = "position".equals(str4);
                    if (equals || equals2) {
                        Context context = zzlh.getContext();
                        int zza = zza(context, map, "x", 0);
                        int zza2 = zza(context, map, "y", 0);
                        int zza3 = zza(context, map, "w", -1);
                        int zza4 = zza(context, map, "h", -1);
                        try {
                            i = Integer.parseInt((String) map.get("player"));
                        } catch (NumberFormatException unused2) {
                            i = 0;
                        }
                        boolean parseBoolean = Boolean.parseBoolean((String) map.get("spherical"));
                        if (!equals || zzuq2.zzub() != null) {
                            zzuq2.zze(zza, zza2, zza3, zza4);
                        } else {
                            zzuq2.zza(zza, zza2, zza3, zza4, i, parseBoolean);
                            if (this.zzbiz.containsKey(zzlh)) {
                                zzuq2.zzub().setBackgroundColor(((Integer) this.zzbiz.get(zzlh)).intValue());
                                return;
                            }
                        }
                    } else {
                        zzk zzub2 = zzuq2.zzub();
                        if (zzub2 == null) {
                            zzk.zzh(zzlh);
                            return;
                        } else if ("click".equals(str4)) {
                            Context context2 = zzlh.getContext();
                            int zza5 = zza(context2, map, "x", 0);
                            int zza6 = zza(context2, map, "y", 0);
                            long uptimeMillis = SystemClock.uptimeMillis();
                            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, (float) zza5, (float) zza6, 0);
                            zzub2.zzd(obtain);
                            obtain.recycle();
                            return;
                        } else if ("currentTime".equals(str4)) {
                            String str6 = (String) map.get("time");
                            if (str6 == null) {
                                str3 = "Time parameter missing from currentTime video GMSG.";
                            } else {
                                try {
                                    zzub2.seekTo((int) (Float.parseFloat(str6) * 1000.0f));
                                    return;
                                } catch (NumberFormatException unused3) {
                                    str2 = "Could not parse time parameter from currentTime video GMSG: ";
                                    valueOf = String.valueOf(str6);
                                    if (valueOf.length() == 0) {
                                        str = new String(str2);
                                    }
                                    str = str2.concat(valueOf);
                                }
                            }
                        } else if ("hide".equals(str4)) {
                            zzub2.setVisibility(4);
                            return;
                        } else if ("load".equals(str4)) {
                            zzub2.zzlv();
                            return;
                        } else if ("mimetype".equals(str4)) {
                            zzub2.setMimeType((String) map.get("mimetype"));
                            return;
                        } else if ("muted".equals(str4)) {
                            if (Boolean.parseBoolean((String) map.get("muted"))) {
                                zzub2.zzno();
                                return;
                            } else {
                                zzub2.zznp();
                                return;
                            }
                        } else if ("pause".equals(str4)) {
                            zzub2.pause();
                            return;
                        } else if ("play".equals(str4)) {
                            zzub2.play();
                            return;
                        } else if ("show".equals(str4)) {
                            zzub2.setVisibility(0);
                            return;
                        } else if ("src".equals(str4)) {
                            zzub2.zzbw((String) map.get("src"));
                            return;
                        } else if ("touchMove".equals(str4)) {
                            Context context3 = zzlh.getContext();
                            zzub2.zza((float) zza(context3, map, "dx", 0), (float) zza(context3, map, "dy", 0));
                            if (!this.zzbja) {
                                zzlh.zzuh().zzob();
                                this.zzbja = true;
                                return;
                            }
                        } else if ("volume".equals(str4)) {
                            String str7 = (String) map.get("volume");
                            if (str7 == null) {
                                str3 = "Level parameter missing from volume video GMSG.";
                            } else {
                                try {
                                    zzub2.zza(Float.parseFloat(str7));
                                    return;
                                } catch (NumberFormatException unused4) {
                                    str2 = "Could not parse volume parameter from volume video GMSG: ";
                                    valueOf = String.valueOf(str7);
                                    if (valueOf.length() == 0) {
                                        str = new String(str2);
                                    }
                                    str = str2.concat(valueOf);
                                }
                            }
                        } else if ("watermark".equals(str4)) {
                            zzub2.zzon();
                            return;
                        } else {
                            String str8 = "Unknown video action: ";
                            String valueOf3 = String.valueOf(str4);
                            str = valueOf3.length() != 0 ? str8.concat(valueOf3) : new String(str8);
                            zzkd.zzcx(str);
                            return;
                        }
                    }
                    return;
                }
            }
        }
        zzkd.zzcx(str3);
    }
}
