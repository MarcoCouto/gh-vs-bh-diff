package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import org.json.JSONObject;

@zzin
public class zzhl extends Handler {
    private final zzhk zzbwg;

    public zzhl(Context context) {
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this((zzhk) new zzhm(context));
    }

    public zzhl(zzhk zzhk) {
        this.zzbwg = zzhk;
    }

    private void zze(JSONObject jSONObject) {
        try {
            this.zzbwg.zza(jSONObject.getString("request_id"), jSONObject.getString("base_url"), jSONObject.getString("html"));
        } catch (Exception unused) {
        }
    }

    public void handleMessage(Message message) {
        try {
            Bundle data = message.getData();
            if (data != null) {
                JSONObject jSONObject = new JSONObject(data.getString("data"));
                if ("fetch_html".equals(jSONObject.getString("message_name"))) {
                    zze(jSONObject);
                }
            }
        } catch (Exception unused) {
        }
    }
}
