package com.google.android.gms.internal;

import android.support.annotation.Nullable;

@zzin
public class zzdg {
    @Nullable
    public static zzdi zza(@Nullable zzdk zzdk, long j) {
        if (zzdk == null) {
            return null;
        }
        return zzdk.zzc(j);
    }

    public static boolean zza(@Nullable zzdk zzdk, @Nullable zzdi zzdi, long j, String... strArr) {
        if (zzdk == null || zzdi == null) {
            return false;
        }
        return zzdk.zza(zzdi, j, strArr);
    }

    public static boolean zza(@Nullable zzdk zzdk, @Nullable zzdi zzdi, String... strArr) {
        if (zzdk == null || zzdi == null) {
            return false;
        }
        return zzdk.zza(zzdi, strArr);
    }

    @Nullable
    public static zzdi zzb(@Nullable zzdk zzdk) {
        if (zzdk == null) {
            return null;
        }
        return zzdk.zzkg();
    }
}
