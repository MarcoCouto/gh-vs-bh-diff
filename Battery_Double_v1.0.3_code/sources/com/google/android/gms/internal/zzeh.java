package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.google.android.gms.internal.zzec.zza;

@zzin
public class zzeh extends zza {
    private final OnContentAdLoadedListener zzbhj;

    public zzeh(OnContentAdLoadedListener onContentAdLoadedListener) {
        this.zzbhj = onContentAdLoadedListener;
    }

    public void zza(zzdx zzdx) {
        this.zzbhj.onContentAdLoaded(zzb(zzdx));
    }

    /* access modifiers changed from: 0000 */
    public zzdy zzb(zzdx zzdx) {
        return new zzdy(zzdx);
    }
}
