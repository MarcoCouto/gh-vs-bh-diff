package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzin
public class zzcm {
    private final Object zzail = new Object();
    private int zzash;
    private List<zzcl> zzasi = new LinkedList();

    public boolean zza(zzcl zzcl) {
        synchronized (this.zzail) {
            return this.zzasi.contains(zzcl);
        }
    }

    public boolean zzb(zzcl zzcl) {
        synchronized (this.zzail) {
            Iterator it = this.zzasi.iterator();
            while (it.hasNext()) {
                zzcl zzcl2 = (zzcl) it.next();
                if (zzcl != zzcl2 && zzcl2.zzhr().equals(zzcl.zzhr())) {
                    it.remove();
                    return true;
                }
            }
            return false;
        }
    }

    public void zzc(zzcl zzcl) {
        synchronized (this.zzail) {
            if (this.zzasi.size() >= 10) {
                int size = this.zzasi.size();
                StringBuilder sb = new StringBuilder(41);
                sb.append("Queue is full, current size = ");
                sb.append(size);
                zzkd.zzcv(sb.toString());
                this.zzasi.remove(0);
            }
            int i = this.zzash;
            this.zzash = i + 1;
            zzcl.zzl(i);
            this.zzasi.add(zzcl);
        }
    }

    @Nullable
    public zzcl zzhy() {
        synchronized (this.zzail) {
            zzcl zzcl = null;
            if (this.zzasi.size() == 0) {
                zzkd.zzcv("Queue empty");
                return null;
            } else if (this.zzasi.size() >= 2) {
                int i = Integer.MIN_VALUE;
                for (zzcl zzcl2 : this.zzasi) {
                    int score = zzcl2.getScore();
                    if (score > i) {
                        zzcl = zzcl2;
                        i = score;
                    }
                }
                this.zzasi.remove(zzcl);
                return zzcl;
            } else {
                zzcl zzcl3 = (zzcl) this.zzasi.get(0);
                zzcl3.zzht();
                return zzcl3;
            }
        }
    }
}
