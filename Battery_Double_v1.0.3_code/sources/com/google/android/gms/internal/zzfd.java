package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.api.Releasable;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

@zzin
public abstract class zzfd implements Releasable {
    protected Context mContext;
    protected String zzbjf;
    protected WeakReference<zzlh> zzbjg;

    public zzfd(zzlh zzlh) {
        this.mContext = zzlh.getContext();
        this.zzbjf = zzu.zzfq().zzg(this.mContext, zzlh.zzum().zzcs);
        this.zzbjg = new WeakReference<>(zzlh);
    }

    /* access modifiers changed from: private */
    public void zza(String str, Map<String, String> map) {
        zzlh zzlh = (zzlh) this.zzbjg.get();
        if (zzlh != null) {
            zzlh.zza(str, map);
        }
    }

    /* access modifiers changed from: private */
    public String zzbb(String str) {
        char c;
        String str2 = "internal";
        switch (str.hashCode()) {
            case -1396664534:
                if (str.equals("badUrl")) {
                    c = 6;
                    break;
                }
            case -1347010958:
                if (str.equals("inProgress")) {
                    c = 2;
                    break;
                }
            case -918817863:
                if (str.equals("downloadTimeout")) {
                    c = 7;
                    break;
                }
            case -659376217:
                if (str.equals("contentLengthMissing")) {
                    c = 3;
                    break;
                }
            case -642208130:
                if (str.equals("playerFailed")) {
                    c = 1;
                    break;
                }
            case -354048396:
                if (str.equals("sizeExceeded")) {
                    c = 8;
                    break;
                }
            case -32082395:
                if (str.equals("externalAbort")) {
                    c = 9;
                    break;
                }
            case 96784904:
                if (str.equals("error")) {
                    c = 0;
                    break;
                }
            case 580119100:
                if (str.equals("expireFailed")) {
                    c = 5;
                    break;
                }
            case 725497484:
                if (str.equals("noCacheDir")) {
                    c = 4;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
                return "internal";
            case 4:
            case 5:
                return "io";
            case 6:
            case 7:
                return "network";
            case 8:
            case 9:
                return "policy";
            default:
                return str2;
        }
    }

    public abstract void abort();

    public void release() {
    }

    /* access modifiers changed from: protected */
    public void zza(final String str, final String str2, final int i) {
        zza.zzcnb.post(new Runnable() {
            public void run() {
                HashMap hashMap = new HashMap();
                hashMap.put("event", "precacheComplete");
                hashMap.put("src", str);
                hashMap.put("cachedSrc", str2);
                hashMap.put("totalBytes", Integer.toString(i));
                zzfd.this.zza("onPrecacheEvent", (Map<String, String>) hashMap);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zza(String str, String str2, int i, int i2, boolean z) {
        Handler handler = zza.zzcnb;
        final String str3 = str;
        final String str4 = str2;
        final int i3 = i;
        final int i4 = i2;
        final boolean z2 = z;
        AnonymousClass1 r1 = new Runnable() {
            public void run() {
                HashMap hashMap = new HashMap();
                hashMap.put("event", "precacheProgress");
                hashMap.put("src", str3);
                hashMap.put("cachedSrc", str4);
                hashMap.put("bytesLoaded", Integer.toString(i3));
                hashMap.put("totalBytes", Integer.toString(i4));
                hashMap.put("cacheReady", z2 ? "1" : "0");
                zzfd.this.zza("onPrecacheEvent", (Map<String, String>) hashMap);
            }
        };
        handler.post(r1);
    }

    /* access modifiers changed from: protected */
    public void zza(String str, String str2, String str3, String str4) {
        Handler handler = zza.zzcnb;
        final String str5 = str;
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        AnonymousClass3 r1 = new Runnable() {
            public void run() {
                HashMap hashMap = new HashMap();
                hashMap.put("event", "precacheCanceled");
                hashMap.put("src", str5);
                if (!TextUtils.isEmpty(str6)) {
                    hashMap.put("cachedSrc", str6);
                }
                hashMap.put("type", zzfd.this.zzbb(str7));
                hashMap.put("reason", str7);
                if (!TextUtils.isEmpty(str8)) {
                    hashMap.put("message", str8);
                }
                zzfd.this.zza("onPrecacheEvent", (Map<String, String>) hashMap);
            }
        };
        handler.post(r1);
    }

    public abstract boolean zzaz(String str);

    /* access modifiers changed from: protected */
    public String zzba(String str) {
        return zzm.zziw().zzcu(str);
    }
}
