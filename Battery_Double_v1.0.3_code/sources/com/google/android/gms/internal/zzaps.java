package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class zzaps implements Cloneable {
    private zzapq<?, ?> bjD;
    private List<zzapx> bjE = new ArrayList();
    private Object value;

    zzaps() {
    }

    private byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzx()];
        zza(zzapo.zzbe(bArr));
        return bArr;
    }

    /* renamed from: aD */
    public final zzaps clone() {
        Object clone;
        zzaps zzaps = new zzaps();
        try {
            zzaps.bjD = this.bjD;
            if (this.bjE == null) {
                zzaps.bjE = null;
            } else {
                zzaps.bjE.addAll(this.bjE);
            }
            if (this.value == null) {
                return zzaps;
            }
            if (this.value instanceof zzapv) {
                clone = (zzapv) ((zzapv) this.value).clone();
            } else if (this.value instanceof byte[]) {
                clone = ((byte[]) this.value).clone();
            } else {
                int i = 0;
                if (this.value instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.value;
                    byte[][] bArr2 = new byte[bArr.length][];
                    zzaps.value = bArr2;
                    while (i < bArr.length) {
                        bArr2[i] = (byte[]) bArr[i].clone();
                        i++;
                    }
                } else if (this.value instanceof boolean[]) {
                    clone = ((boolean[]) this.value).clone();
                } else if (this.value instanceof int[]) {
                    clone = ((int[]) this.value).clone();
                } else if (this.value instanceof long[]) {
                    clone = ((long[]) this.value).clone();
                } else if (this.value instanceof float[]) {
                    clone = ((float[]) this.value).clone();
                } else if (this.value instanceof double[]) {
                    clone = ((double[]) this.value).clone();
                } else if (this.value instanceof zzapv[]) {
                    zzapv[] zzapvArr = (zzapv[]) this.value;
                    zzapv[] zzapvArr2 = new zzapv[zzapvArr.length];
                    zzaps.value = zzapvArr2;
                    while (i < zzapvArr.length) {
                        zzapvArr2[i] = (zzapv) zzapvArr[i].clone();
                        i++;
                    }
                }
                return zzaps;
            }
            zzaps.value = clone;
            return zzaps;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzaps)) {
            return false;
        }
        zzaps zzaps = (zzaps) obj;
        if (this.value == null || zzaps.value == null) {
            if (this.bjE != null && zzaps.bjE != null) {
                return this.bjE.equals(zzaps.bjE);
            }
            try {
                return Arrays.equals(toByteArray(), zzaps.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.bjD != zzaps.bjD) {
            return false;
        } else {
            return !this.bjD.baj.isArray() ? this.value.equals(zzaps.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zzaps.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zzaps.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zzaps.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zzaps.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zzaps.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zzaps.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zzaps.value);
        }
    }

    public int hashCode() {
        try {
            return 527 + Arrays.hashCode(toByteArray());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzapo zzapo) throws IOException {
        if (this.value != null) {
            this.bjD.zza(this.value, zzapo);
            return;
        }
        for (zzapx zza : this.bjE) {
            zza.zza(zzapo);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzapx zzapx) {
        this.bjE.add(zzapx);
    }

    /* access modifiers changed from: 0000 */
    public <T> T zzb(zzapq<?, T> zzapq) {
        if (this.value == null) {
            this.bjD = zzapq;
            this.value = zzapq.zzav(this.bjE);
            this.bjE = null;
        } else if (!this.bjD.equals(zzapq)) {
            throw new IllegalStateException("Tried to getExtension with a different Extension.");
        }
        return this.value;
    }

    /* access modifiers changed from: 0000 */
    public int zzx() {
        if (this.value != null) {
            return this.bjD.zzcp(this.value);
        }
        int i = 0;
        for (zzapx zzx : this.bjE) {
            i += zzx.zzx();
        }
        return i;
    }
}
