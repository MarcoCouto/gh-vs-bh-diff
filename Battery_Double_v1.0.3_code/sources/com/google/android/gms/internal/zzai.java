package com.google.android.gms.internal;

import java.io.IOException;

public interface zzai {

    public static final class zza extends zzapp<zza> {
        private static volatile zza[] zzwt;
        public String string;
        public int type;
        public zza[] zzwu;
        public zza[] zzwv;
        public zza[] zzww;
        public String zzwx;
        public String zzwy;
        public long zzwz;
        public boolean zzxa;
        public zza[] zzxb;
        public int[] zzxc;
        public boolean zzxd;

        public zza() {
            zzaq();
        }

        public static zza[] zzap() {
            if (zzwt == null) {
                synchronized (zzapt.bjF) {
                    if (zzwt == null) {
                        zzwt = new zza[0];
                    }
                }
            }
            return zzwt;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.type != zza.type) {
                return false;
            }
            if (this.string == null) {
                if (zza.string != null) {
                    return false;
                }
            } else if (!this.string.equals(zza.string)) {
                return false;
            }
            if (!zzapt.equals((Object[]) this.zzwu, (Object[]) zza.zzwu) || !zzapt.equals((Object[]) this.zzwv, (Object[]) zza.zzwv) || !zzapt.equals((Object[]) this.zzww, (Object[]) zza.zzww)) {
                return false;
            }
            if (this.zzwx == null) {
                if (zza.zzwx != null) {
                    return false;
                }
            } else if (!this.zzwx.equals(zza.zzwx)) {
                return false;
            }
            if (this.zzwy == null) {
                if (zza.zzwy != null) {
                    return false;
                }
            } else if (!this.zzwy.equals(zza.zzwy)) {
                return false;
            }
            if (this.zzwz != zza.zzwz || this.zzxa != zza.zzxa || !zzapt.equals((Object[]) this.zzxb, (Object[]) zza.zzxb) || !zzapt.equals(this.zzxc, zza.zzxc) || this.zzxd != zza.zzxd) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zza.bjx);
            }
            if (zza.bjx != null) {
                if (zza.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int i2 = 1237;
            int hashCode = (((((((((((((((((((((((527 + getClass().getName().hashCode()) * 31) + this.type) * 31) + (this.string == null ? 0 : this.string.hashCode())) * 31) + zzapt.hashCode((Object[]) this.zzwu)) * 31) + zzapt.hashCode((Object[]) this.zzwv)) * 31) + zzapt.hashCode((Object[]) this.zzww)) * 31) + (this.zzwx == null ? 0 : this.zzwx.hashCode())) * 31) + (this.zzwy == null ? 0 : this.zzwy.hashCode())) * 31) + ((int) (this.zzwz ^ (this.zzwz >>> 32)))) * 31) + (this.zzxa ? 1231 : 1237)) * 31) + zzapt.hashCode((Object[]) this.zzxb)) * 31) + zzapt.hashCode(this.zzxc)) * 31;
            if (this.zzxd) {
                i2 = 1231;
            }
            int i3 = 31 * (hashCode + i2);
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return i3 + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            zzapo.zzae(1, this.type);
            if (!this.string.equals("")) {
                zzapo.zzr(2, this.string);
            }
            if (this.zzwu != null && this.zzwu.length > 0) {
                for (zza zza : this.zzwu) {
                    if (zza != null) {
                        zzapo.zza(3, (zzapv) zza);
                    }
                }
            }
            if (this.zzwv != null && this.zzwv.length > 0) {
                for (zza zza2 : this.zzwv) {
                    if (zza2 != null) {
                        zzapo.zza(4, (zzapv) zza2);
                    }
                }
            }
            if (this.zzww != null && this.zzww.length > 0) {
                for (zza zza3 : this.zzww) {
                    if (zza3 != null) {
                        zzapo.zza(5, (zzapv) zza3);
                    }
                }
            }
            if (!this.zzwx.equals("")) {
                zzapo.zzr(6, this.zzwx);
            }
            if (!this.zzwy.equals("")) {
                zzapo.zzr(7, this.zzwy);
            }
            if (this.zzwz != 0) {
                zzapo.zzb(8, this.zzwz);
            }
            if (this.zzxd) {
                zzapo.zzj(9, this.zzxd);
            }
            if (this.zzxc != null && this.zzxc.length > 0) {
                for (int zzae : this.zzxc) {
                    zzapo.zzae(10, zzae);
                }
            }
            if (this.zzxb != null && this.zzxb.length > 0) {
                for (zza zza4 : this.zzxb) {
                    if (zza4 != null) {
                        zzapo.zza(11, (zzapv) zza4);
                    }
                }
            }
            if (this.zzxa) {
                zzapo.zzj(12, this.zzxa);
            }
            super.zza(zzapo);
        }

        public zza zzaq() {
            this.type = 1;
            this.string = "";
            this.zzwu = zzap();
            this.zzwv = zzap();
            this.zzww = zzap();
            this.zzwx = "";
            this.zzwy = "";
            this.zzwz = 0;
            this.zzxa = false;
            this.zzxb = zzap();
            this.zzxc = zzapy.bjH;
            this.zzxd = false;
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* renamed from: zzt */
        public zza zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        return this;
                    case 8:
                        int al = zzapn.al();
                        switch (al) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                this.type = al;
                                break;
                        }
                    case 18:
                        this.string = zzapn.readString();
                        break;
                    case 26:
                        int zzc = zzapy.zzc(zzapn, 26);
                        int length = this.zzwu == null ? 0 : this.zzwu.length;
                        zza[] zzaArr = new zza[(zzc + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzwu, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new zza();
                            zzapn.zza(zzaArr[length]);
                            zzapn.ah();
                            length++;
                        }
                        zzaArr[length] = new zza();
                        zzapn.zza(zzaArr[length]);
                        this.zzwu = zzaArr;
                        break;
                    case 34:
                        int zzc2 = zzapy.zzc(zzapn, 34);
                        int length2 = this.zzwv == null ? 0 : this.zzwv.length;
                        zza[] zzaArr2 = new zza[(zzc2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzwv, 0, zzaArr2, 0, length2);
                        }
                        while (length2 < zzaArr2.length - 1) {
                            zzaArr2[length2] = new zza();
                            zzapn.zza(zzaArr2[length2]);
                            zzapn.ah();
                            length2++;
                        }
                        zzaArr2[length2] = new zza();
                        zzapn.zza(zzaArr2[length2]);
                        this.zzwv = zzaArr2;
                        break;
                    case 42:
                        int zzc3 = zzapy.zzc(zzapn, 42);
                        int length3 = this.zzww == null ? 0 : this.zzww.length;
                        zza[] zzaArr3 = new zza[(zzc3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzww, 0, zzaArr3, 0, length3);
                        }
                        while (length3 < zzaArr3.length - 1) {
                            zzaArr3[length3] = new zza();
                            zzapn.zza(zzaArr3[length3]);
                            zzapn.ah();
                            length3++;
                        }
                        zzaArr3[length3] = new zza();
                        zzapn.zza(zzaArr3[length3]);
                        this.zzww = zzaArr3;
                        break;
                    case 50:
                        this.zzwx = zzapn.readString();
                        break;
                    case 58:
                        this.zzwy = zzapn.readString();
                        break;
                    case 64:
                        this.zzwz = zzapn.ak();
                        break;
                    case 72:
                        this.zzxd = zzapn.an();
                        break;
                    case 80:
                        int zzc4 = zzapy.zzc(zzapn, 80);
                        int[] iArr = new int[zzc4];
                        int i = 0;
                        for (int i2 = 0; i2 < zzc4; i2++) {
                            if (i2 != 0) {
                                zzapn.ah();
                            }
                            int al2 = zzapn.al();
                            switch (al2) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    int i3 = i + 1;
                                    iArr[i] = al2;
                                    i = i3;
                                    break;
                            }
                        }
                        if (i != 0) {
                            int length4 = this.zzxc == null ? 0 : this.zzxc.length;
                            if (length4 != 0 || i != zzc4) {
                                int[] iArr2 = new int[(length4 + i)];
                                if (length4 != 0) {
                                    System.arraycopy(this.zzxc, 0, iArr2, 0, length4);
                                }
                                System.arraycopy(iArr, 0, iArr2, length4, i);
                                this.zzxc = iArr2;
                                break;
                            } else {
                                this.zzxc = iArr;
                                break;
                            }
                        } else {
                            break;
                        }
                    case 82:
                        int zzafr = zzapn.zzafr(zzapn.aq());
                        int position = zzapn.getPosition();
                        int i4 = 0;
                        while (zzapn.av() > 0) {
                            switch (zzapn.al()) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            zzapn.zzaft(position);
                            int length5 = this.zzxc == null ? 0 : this.zzxc.length;
                            int[] iArr3 = new int[(i4 + length5)];
                            if (length5 != 0) {
                                System.arraycopy(this.zzxc, 0, iArr3, 0, length5);
                            }
                            while (zzapn.av() > 0) {
                                int al3 = zzapn.al();
                                switch (al3) {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                    case 14:
                                    case 15:
                                    case 16:
                                    case 17:
                                        int i5 = length5 + 1;
                                        iArr3[length5] = al3;
                                        length5 = i5;
                                        break;
                                }
                            }
                            this.zzxc = iArr3;
                        }
                        zzapn.zzafs(zzafr);
                        break;
                    case 90:
                        int zzc5 = zzapy.zzc(zzapn, 90);
                        int length6 = this.zzxb == null ? 0 : this.zzxb.length;
                        zza[] zzaArr4 = new zza[(zzc5 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.zzxb, 0, zzaArr4, 0, length6);
                        }
                        while (length6 < zzaArr4.length - 1) {
                            zzaArr4[length6] = new zza();
                            zzapn.zza(zzaArr4[length6]);
                            zzapn.ah();
                            length6++;
                        }
                        zzaArr4[length6] = new zza();
                        zzapn.zza(zzaArr4[length6]);
                        this.zzxb = zzaArr4;
                        break;
                    case 96:
                        this.zzxa = zzapn.an();
                        break;
                    default:
                        if (super.zza(zzapn, ah)) {
                            break;
                        } else {
                            return this;
                        }
                }
            }
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx() + zzapo.zzag(1, this.type);
            if (!this.string.equals("")) {
                zzx += zzapo.zzs(2, this.string);
            }
            if (this.zzwu != null && this.zzwu.length > 0) {
                int i = zzx;
                for (zza zza : this.zzwu) {
                    if (zza != null) {
                        i += zzapo.zzc(3, (zzapv) zza);
                    }
                }
                zzx = i;
            }
            if (this.zzwv != null && this.zzwv.length > 0) {
                int i2 = zzx;
                for (zza zza2 : this.zzwv) {
                    if (zza2 != null) {
                        i2 += zzapo.zzc(4, (zzapv) zza2);
                    }
                }
                zzx = i2;
            }
            if (this.zzww != null && this.zzww.length > 0) {
                int i3 = zzx;
                for (zza zza3 : this.zzww) {
                    if (zza3 != null) {
                        i3 += zzapo.zzc(5, (zzapv) zza3);
                    }
                }
                zzx = i3;
            }
            if (!this.zzwx.equals("")) {
                zzx += zzapo.zzs(6, this.zzwx);
            }
            if (!this.zzwy.equals("")) {
                zzx += zzapo.zzs(7, this.zzwy);
            }
            if (this.zzwz != 0) {
                zzx += zzapo.zze(8, this.zzwz);
            }
            if (this.zzxd) {
                zzx += zzapo.zzk(9, this.zzxd);
            }
            if (this.zzxc != null && this.zzxc.length > 0) {
                int i4 = 0;
                for (int zzafx : this.zzxc) {
                    i4 += zzapo.zzafx(zzafx);
                }
                zzx = zzx + i4 + (1 * this.zzxc.length);
            }
            if (this.zzxb != null && this.zzxb.length > 0) {
                for (zza zza4 : this.zzxb) {
                    if (zza4 != null) {
                        zzx += zzapo.zzc(11, (zzapv) zza4);
                    }
                }
            }
            return this.zzxa ? zzx + zzapo.zzk(12, this.zzxa) : zzx;
        }
    }
}
