package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zzk.zza;
import com.google.android.gms.ads.internal.request.zzl;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzfs.zzb;
import com.google.android.gms.internal.zzfs.zzc;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zzip extends zza {
    private static final Object zzamr = new Object();
    private static zzip zzcdx;
    private final Context mContext;
    private final zzio zzcdy;
    private final zzcv zzcdz;
    private final zzfs zzcea;

    zzip(Context context, zzcv zzcv, zzio zzio) {
        this.mContext = context;
        this.zzcdy = zzio;
        this.zzcdz = zzcv;
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        zzfs zzfs = new zzfs(context, new VersionInfoParcel(9452208, 9452208, true), zzcv.zzjv(), new zzkl<zzfp>() {
            /* renamed from: zza */
            public void zzd(zzfp zzfp) {
                zzfp.zza("/log", zzeo.zzbhv);
            }
        }, new zzb());
        this.zzcea = zzfs;
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x0246 A[Catch:{ Exception -> 0x0271, all -> 0x026c, all -> 0x0283 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x024c A[Catch:{ Exception -> 0x0271, all -> 0x026c, all -> 0x0283 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d0  */
    private static AdResponseParcel zza(Context context, zzfs zzfs, zzcv zzcv, zzio zzio, AdRequestInfoParcel adRequestInfoParcel) {
        Bundle bundle;
        Future future;
        zzky zzkw;
        zziv zzy;
        final Context context2;
        Throwable th;
        final Context context3 = context;
        final zzio zzio2 = zzio;
        final AdRequestInfoParcel adRequestInfoParcel2 = adRequestInfoParcel;
        zzkd.zzcv("Starting ad request from service using: AFMA_getAd");
        zzdc.initialize(context);
        zzdk zzdk = new zzdk(((Boolean) zzdc.zzaze.get()).booleanValue(), "load_ad", adRequestInfoParcel2.zzapa.zzaur);
        if (adRequestInfoParcel2.versionCode > 10 && adRequestInfoParcel2.zzcbj != -1) {
            zzdk.zza(zzdk.zzc(adRequestInfoParcel2.zzcbj), "cts");
        }
        zzdi zzkg = zzdk.zzkg();
        final Bundle bundle2 = (adRequestInfoParcel2.versionCode < 4 || adRequestInfoParcel2.zzcay == null) ? null : adRequestInfoParcel2.zzcay;
        if (((Boolean) zzdc.zzazn.get()).booleanValue() && zzio2.zzcdw != null) {
            if (bundle2 == null && ((Boolean) zzdc.zzazo.get()).booleanValue()) {
                zzkd.v("contentInfo is not present, but we'll still launch the app index task");
                bundle2 = new Bundle();
            }
            if (bundle2 != null) {
                future = zzkg.zza((Callable<T>) new Callable<Void>() {
                    /* renamed from: zzcx */
                    public Void call() throws Exception {
                        zzio.this.zzcdw.zza(context3, adRequestInfoParcel2.zzcas.packageName, bundle2);
                        return null;
                    }
                });
                bundle = bundle2;
                zzkw = new zzkw(null);
                Bundle bundle3 = adRequestInfoParcel2.zzcar.extras;
                boolean z = bundle3 == null && bundle3.getString("_ad") != null;
                if (adRequestInfoParcel2.zzcbq && !z) {
                    zzkw = zzio2.zzcds.zza(adRequestInfoParcel2.applicationInfo);
                }
                zzy = zzu.zzfw().zzy(context3);
                if (zzy.zzcgp != -1) {
                    zzkd.zzcv("Device is offline.");
                    return new AdResponseParcel(2);
                }
                String uuid = adRequestInfoParcel2.versionCode >= 7 ? adRequestInfoParcel2.zzcbg : UUID.randomUUID().toString();
                zzir zzir = new zzir(uuid, adRequestInfoParcel2.applicationInfo.packageName);
                if (adRequestInfoParcel2.zzcar.extras != null) {
                    String string = adRequestInfoParcel2.zzcar.extras.getString("_ad");
                    if (string != null) {
                        return zziq.zza(context3, adRequestInfoParcel2, string);
                    }
                }
                List zza = zzio2.zzcdq.zza(adRequestInfoParcel2);
                String zzf = zzio2.zzcdt.zzf(adRequestInfoParcel2);
                zziz.zza zzz = zzio2.zzcdu.zzz(context3);
                if (future != null) {
                    try {
                        zzkd.v("Waiting for app index fetching task.");
                        future.get(((Long) zzdc.zzazp.get()).longValue(), TimeUnit.MILLISECONDS);
                        zzkd.v("App index fetching task completed.");
                    } catch (InterruptedException | ExecutionException e) {
                        zzkd.zzd("Failed to fetch app index signal", e);
                    } catch (TimeoutException unused) {
                        zzkd.zzcv("Timed out waiting for app index fetching task");
                    }
                }
                String zzck = zzio2.zzcdp.zzck(adRequestInfoParcel2.zzcas.packageName);
                zzir zzir2 = zzir;
                String str = uuid;
                AdResponseParcel adResponseParcel = null;
                zzdi zzdi = zzkg;
                JSONObject zza2 = zziq.zza(context, adRequestInfoParcel2, zzy, zzz, zzb(zzkw), zzcv, zzf, zza, bundle, zzck);
                if (zza2 == null) {
                    return new AdResponseParcel(0);
                }
                if (adRequestInfoParcel2.versionCode < 7) {
                    try {
                        zza2.put("request_id", str);
                    } catch (JSONException unused2) {
                    }
                }
                try {
                    zza2.put("prefetch_mode", "url");
                } catch (JSONException e2) {
                    zzkd.zzd("Failed putting prefetch parameters to ad request.", e2);
                }
                final String jSONObject = zza2.toString();
                zzdi zzdi2 = zzdi;
                zzdk.zza(zzdi2, "arc");
                final zzdi zzkg2 = zzdk.zzkg();
                Handler handler = zzkh.zzclc;
                final zzfs zzfs2 = zzfs;
                final zzir zzir3 = zzir2;
                final zzdk zzdk2 = zzdk;
                AnonymousClass2 r2 = new Runnable() {
                    public void run() {
                        zzc zzma = zzfs.this.zzma();
                        zzir3.zzb(zzma);
                        zzdk2.zza(zzkg2, "rwc");
                        final zzdi zzkg = zzdk2.zzkg();
                        zzma.zza(new zzla.zzc<zzft>() {
                            /* renamed from: zzb */
                            public void zzd(zzft zzft) {
                                zzdk2.zza(zzkg, "jsf");
                                zzdk2.zzkh();
                                zzft.zza("/invalidRequest", zzir3.zzcep);
                                zzft.zza("/loadAdURL", zzir3.zzceq);
                                zzft.zza("/loadAd", zzir3.zzcer);
                                try {
                                    zzft.zzj("AFMA_getAd", jSONObject);
                                } catch (Exception e) {
                                    zzkd.zzb("Error requesting an ad url", e);
                                }
                            }
                        }, new zzla.zza() {
                            public void run() {
                            }
                        });
                    }
                };
                handler.post(r2);
                final zzir zzir4 = zzir2;
                try {
                    zziu zziu = (zziu) zzir4.zzrh().get(10, TimeUnit.SECONDS);
                    if (zziu == null) {
                        try {
                            AdResponseParcel adResponseParcel2 = new AdResponseParcel(0);
                            final Context context4 = context;
                            zzkh.zzclc.post(new Runnable() {
                                public void run() {
                                    zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                }
                            });
                            return adResponseParcel2;
                        } catch (Throwable th2) {
                            th = th2;
                            context2 = context;
                            zzkh.zzclc.post(new Runnable() {
                                public void run() {
                                    zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                }
                            });
                            throw th;
                        }
                    } else {
                        final Context context5 = context;
                        try {
                            if (zziu.getErrorCode() != -2) {
                                try {
                                    AdResponseParcel adResponseParcel3 = new AdResponseParcel(zziu.getErrorCode());
                                    zzkh.zzclc.post(new Runnable() {
                                        public void run() {
                                            zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                        }
                                    });
                                    return adResponseParcel3;
                                } catch (Throwable th3) {
                                    th = th3;
                                    context2 = context5;
                                    zzkh.zzclc.post(new Runnable() {
                                        public void run() {
                                            zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                        }
                                    });
                                    throw th;
                                }
                            } else {
                                if (zzdk.zzkj() != null) {
                                    zzdk.zza(zzdk.zzkj(), "rur");
                                }
                                if (!TextUtils.isEmpty(zziu.zzrm())) {
                                    adResponseParcel = zziq.zza(context5, adRequestInfoParcel2, zziu.zzrm());
                                }
                                if (adResponseParcel == null) {
                                    if (!TextUtils.isEmpty(zziu.getUrl())) {
                                        String str2 = zzck;
                                        context2 = context5;
                                        adResponseParcel = zza(adRequestInfoParcel2, context5, adRequestInfoParcel2.zzaow.zzcs, zziu.getUrl(), str2, zziu, zzdk, zzio2);
                                        AdResponseParcel adResponseParcel4 = adResponseParcel != null ? new AdResponseParcel(0) : adResponseParcel;
                                        zzdk.zza(zzdi2, "tts");
                                        adResponseParcel4.zzccl = zzdk.zzki();
                                        zzkh.zzclc.post(new Runnable() {
                                            public void run() {
                                                zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                            }
                                        });
                                        return adResponseParcel4;
                                    }
                                }
                                context2 = context5;
                                if (adResponseParcel != null) {
                                }
                                zzdk.zza(zzdi2, "tts");
                                adResponseParcel4.zzccl = zzdk.zzki();
                                zzkh.zzclc.post(new Runnable() {
                                    public void run() {
                                        zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                    }
                                });
                                return adResponseParcel4;
                            }
                        } catch (Throwable th4) {
                            th = th4;
                            context2 = context5;
                            th = th;
                            zzkh.zzclc.post(new Runnable() {
                                public void run() {
                                    zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                                }
                            });
                            throw th;
                        }
                    }
                } catch (Exception unused3) {
                    context2 = context;
                    AdResponseParcel adResponseParcel5 = new AdResponseParcel(0);
                    zzkh.zzclc.post(new Runnable() {
                        public void run() {
                            zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                        }
                    });
                    return adResponseParcel5;
                } catch (Throwable th5) {
                    th = th5;
                    th = th;
                    zzkh.zzclc.post(new Runnable() {
                        public void run() {
                            zzio.this.zzcdr.zza(context2, zzir4, adRequestInfoParcel2.zzaow);
                        }
                    });
                    throw th;
                }
            }
        }
        bundle = bundle2;
        future = null;
        zzkw = new zzkw(null);
        Bundle bundle32 = adRequestInfoParcel2.zzcar.extras;
        if (bundle32 == null) {
        }
        zzkw = zzio2.zzcds.zza(adRequestInfoParcel2.applicationInfo);
        zzy = zzu.zzfw().zzy(context3);
        if (zzy.zzcgp != -1) {
        }
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.zzis.zzj(long):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c5, code lost:
        r7 = r7.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r10 = new java.io.InputStreamReader(r12.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r11 = com.google.android.gms.ads.internal.zzu.zzfq().zza(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        com.google.android.gms.common.util.zzo.zzb(r10);
        zza(r7, r4, r11, r1);
        r6.zzb(r7, r4, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e3, code lost:
        if (r2 == null) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e5, code lost:
        r2.zza(r5, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f0, code lost:
        r1 = r6.zzj(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f7, code lost:
        if (r3 == null) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f9, code lost:
        r3.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00fe, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00ff, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0100, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0102, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0103, code lost:
        r1 = r0;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        com.google.android.gms.common.util.zzo.zzb(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0108, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0123, code lost:
        com.google.android.gms.internal.zzkd.zzcx("No location header to follow redirect.");
        r1 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0131, code lost:
        if (r3 == null) goto L_0x0138;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0133, code lost:
        r3.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0138, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0143, code lost:
        com.google.android.gms.internal.zzkd.zzcx("Too many redirects.");
        r1 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0151, code lost:
        if (r3 == null) goto L_0x0158;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0153, code lost:
        r3.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0158, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r2 = new java.lang.StringBuilder(46);
        r2.append("Received error HTTP response code: ");
        r2.append(r1);
        com.google.android.gms.internal.zzkd.zzcx(r2.toString());
        r1 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x018a, code lost:
        if (r3 == null) goto L_0x0191;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x018c, code lost:
        r3.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0191, code lost:
        return r1;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:61:0x0105=Splitter:B:61:0x0105, B:92:0x016b=Splitter:B:92:0x016b} */
    public static AdResponseParcel zza(AdRequestInfoParcel adRequestInfoParcel, Context context, String str, String str2, String str3, zziu zziu, zzdk zzdk, zzio zzio) {
        BufferedOutputStream bufferedOutputStream;
        Throwable th;
        BufferedOutputStream bufferedOutputStream2;
        AdRequestInfoParcel adRequestInfoParcel2 = adRequestInfoParcel;
        zzdk zzdk2 = zzdk;
        zzio zzio2 = zzio;
        zzdi zzkg = zzdk2 != null ? zzdk.zzkg() : null;
        try {
            zzis zzis = new zzis(adRequestInfoParcel2);
            String str4 = "AdRequestServiceImpl: Sending request: ";
            String valueOf = String.valueOf(str2);
            zzkd.zzcv(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
            URL url = new URL(str2);
            long elapsedRealtime = zzu.zzfu().elapsedRealtime();
            boolean z = false;
            int i = 0;
            while (true) {
                if (zzio2 != null) {
                    zzio2.zzcdv.zzro();
                }
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                try {
                    zzu.zzfq().zza(context, str, z, httpURLConnection);
                    if (TextUtils.isEmpty(str3) || !zziu.zzrl()) {
                        String str5 = str3;
                    } else {
                        httpURLConnection.addRequestProperty("x-afma-drt-cookie", str3);
                    }
                    String str6 = adRequestInfoParcel2.zzcbr;
                    if (!TextUtils.isEmpty(str6)) {
                        zzkd.zzcv("Sending webview cookie in ad request header.");
                        httpURLConnection.addRequestProperty("Cookie", str6);
                    }
                    if (zziu != null && !TextUtils.isEmpty(zziu.zzrk())) {
                        httpURLConnection.setDoOutput(true);
                        byte[] bytes = zziu.zzrk().getBytes();
                        httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                        try {
                            bufferedOutputStream2 = new BufferedOutputStream(httpURLConnection.getOutputStream());
                            bufferedOutputStream2.write(bytes);
                            zzo.zzb(bufferedOutputStream2);
                        } catch (Throwable th2) {
                            th = th2;
                            bufferedOutputStream = null;
                            zzo.zzb(bufferedOutputStream);
                            throw th;
                        }
                    }
                    int responseCode = httpURLConnection.getResponseCode();
                    Map headerFields = httpURLConnection.getHeaderFields();
                    if (responseCode >= 200 && responseCode < 300) {
                        break;
                    }
                    zza(url.toString(), headerFields, null, responseCode);
                    if (responseCode < 300 || responseCode >= 400) {
                        break;
                    }
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (TextUtils.isEmpty(headerField)) {
                        break;
                    }
                    url = new URL(headerField);
                    i++;
                    if (i > 5) {
                        break;
                    }
                    zzis.zzj(headerFields);
                    httpURLConnection.disconnect();
                    if (zzio2 != null) {
                        zzio2.zzcdv.zzrp();
                    }
                    adRequestInfoParcel2 = adRequestInfoParcel;
                    z = false;
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    httpURLConnection.disconnect();
                    if (zzio2 != null) {
                        zzio2.zzcdv.zzrp();
                    }
                    throw th4;
                }
            }
        } catch (IOException e) {
            String str7 = "Error while connecting to ad server: ";
            String valueOf2 = String.valueOf(e.getMessage());
            zzkd.zzcx(valueOf2.length() != 0 ? str7.concat(valueOf2) : new String(str7));
            return new AdResponseParcel(2);
        }
    }

    public static zzip zza(Context context, zzcv zzcv, zzio zzio) {
        zzip zzip;
        synchronized (zzamr) {
            if (zzcdx == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zzcdx = new zzip(context, zzcv, zzio);
            }
            zzip = zzcdx;
        }
        return zzip;
    }

    private static void zza(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzkd.zzaz(2)) {
            StringBuilder sb = new StringBuilder(39 + String.valueOf(str).length());
            sb.append("Http Response: {\n  URL:\n    ");
            sb.append(str);
            sb.append("\n  Headers:");
            zzkd.v(sb.toString());
            if (map != null) {
                for (String str3 : map.keySet()) {
                    StringBuilder sb2 = new StringBuilder(5 + String.valueOf(str3).length());
                    sb2.append("    ");
                    sb2.append(str3);
                    sb2.append(":");
                    zzkd.v(sb2.toString());
                    for (String valueOf : (List) map.get(str3)) {
                        String str4 = "      ";
                        String valueOf2 = String.valueOf(valueOf);
                        zzkd.v(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                    }
                }
            }
            zzkd.v("  Body:");
            if (str2 != null) {
                int i2 = 0;
                while (i2 < Math.min(str2.length(), 100000)) {
                    int i3 = i2 + 1000;
                    zzkd.v(str2.substring(i2, Math.min(str2.length(), i3)));
                    i2 = i3;
                }
            } else {
                zzkd.v("    null");
            }
            StringBuilder sb3 = new StringBuilder(34);
            sb3.append("  Response Code:\n    ");
            sb3.append(i);
            sb3.append("\n}");
            zzkd.v(sb3.toString());
        }
    }

    private static Location zzb(zzky<Location> zzky) {
        try {
            return (Location) zzky.get(((Long) zzdc.zzbcp.get()).longValue(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            zzkd.zzd("Exception caught while getting location", e);
            return null;
        }
    }

    public void zza(final AdRequestInfoParcel adRequestInfoParcel, final zzl zzl) {
        zzu.zzft().zzb(this.mContext, adRequestInfoParcel.zzaow);
        zzkg.zza((Runnable) new Runnable() {
            public void run() {
                AdResponseParcel adResponseParcel;
                try {
                    adResponseParcel = zzip.this.zzd(adRequestInfoParcel);
                } catch (Exception e) {
                    zzu.zzft().zzb((Throwable) e, true);
                    zzkd.zzd("Could not fetch ad response due to an Exception.", e);
                    adResponseParcel = null;
                }
                if (adResponseParcel == null) {
                    adResponseParcel = new AdResponseParcel(0);
                }
                try {
                    zzl.zzb(adResponseParcel);
                } catch (RemoteException e2) {
                    zzkd.zzd("Fail to forward ad response.", e2);
                }
            }
        });
    }

    public AdResponseParcel zzd(AdRequestInfoParcel adRequestInfoParcel) {
        return zza(this.mContext, this.zzcea, this.zzcdz, this.zzcdy, adRequestInfoParcel);
    }
}
