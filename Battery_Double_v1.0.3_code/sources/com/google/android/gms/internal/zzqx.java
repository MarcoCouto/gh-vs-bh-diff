package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.internal.zzab;
import java.lang.ref.WeakReference;

public class zzqx<R extends Result> extends TransformedResult<R> implements ResultCallback<R> {
    /* access modifiers changed from: private */
    public final Object sT = new Object();
    /* access modifiers changed from: private */
    public final WeakReference<GoogleApiClient> sV;
    private Status vA = null;
    /* access modifiers changed from: private */
    public final zza vB;
    private boolean vC = false;
    /* access modifiers changed from: private */
    public ResultTransform<? super R, ? extends Result> vw = null;
    /* access modifiers changed from: private */
    public zzqx<? extends Result> vx = null;
    private volatile ResultCallbacks<? super R> vy = null;
    private PendingResult<R> vz = null;

    private final class zza extends Handler {
        public zza(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    PendingResult pendingResult = (PendingResult) message.obj;
                    synchronized (zzqx.this.sT) {
                        if (pendingResult == null) {
                            try {
                                zzqx.this.vx.zzac(new Status(13, "Transform returned null"));
                            } catch (Throwable th) {
                                throw th;
                            }
                        } else if (pendingResult instanceof zzqs) {
                            zzqx.this.vx.zzac(((zzqs) pendingResult).getStatus());
                        } else {
                            zzqx.this.vx.zza(pendingResult);
                        }
                    }
                    return;
                case 1:
                    RuntimeException runtimeException = (RuntimeException) message.obj;
                    String str = "TransformedResultImpl";
                    String str2 = "Runtime exception on the transformation worker thread: ";
                    String valueOf = String.valueOf(runtimeException.getMessage());
                    Log.e(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                    throw runtimeException;
                default:
                    int i = message.what;
                    StringBuilder sb = new StringBuilder(70);
                    sb.append("TransformationResultHandler received unknown message type: ");
                    sb.append(i);
                    Log.e("TransformedResultImpl", sb.toString());
                    return;
            }
        }
    }

    public zzqx(WeakReference<GoogleApiClient> weakReference) {
        zzab.zzb(weakReference, (Object) "GoogleApiClient reference must not be null");
        this.sV = weakReference;
        GoogleApiClient googleApiClient = (GoogleApiClient) this.sV.get();
        this.vB = new zza(googleApiClient != null ? googleApiClient.getLooper() : Looper.getMainLooper());
    }

    /* access modifiers changed from: private */
    public void zzac(Status status) {
        synchronized (this.sT) {
            this.vA = status;
            zzad(this.vA);
        }
    }

    private void zzad(Status status) {
        synchronized (this.sT) {
            if (this.vw != null) {
                Status onFailure = this.vw.onFailure(status);
                zzab.zzb(onFailure, (Object) "onFailure must not return null");
                this.vx.zzac(onFailure);
            } else if (zzaqy()) {
                this.vy.onFailure(status);
            }
        }
    }

    private void zzaqw() {
        if (this.vw != null || this.vy != null) {
            GoogleApiClient googleApiClient = (GoogleApiClient) this.sV.get();
            if (!(this.vC || this.vw == null || googleApiClient == null)) {
                googleApiClient.zza(this);
                this.vC = true;
            }
            if (this.vA != null) {
                zzad(this.vA);
                return;
            }
            if (this.vz != null) {
                this.vz.setResultCallback(this);
            }
        }
    }

    private boolean zzaqy() {
        return (this.vy == null || ((GoogleApiClient) this.sV.get()) == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public void zze(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(result);
                StringBuilder sb = new StringBuilder(18 + String.valueOf(valueOf).length());
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e);
            }
        }
    }

    public void andFinally(@NonNull ResultCallbacks<? super R> resultCallbacks) {
        synchronized (this.sT) {
            boolean z = false;
            zzab.zza(this.vy == null, (Object) "Cannot call andFinally() twice.");
            if (this.vw == null) {
                z = true;
            }
            zzab.zza(z, (Object) "Cannot call then() and andFinally() on the same TransformedResult.");
            this.vy = resultCallbacks;
            zzaqw();
        }
    }

    public void onResult(final R r) {
        synchronized (this.sT) {
            if (!r.getStatus().isSuccess()) {
                zzac(r.getStatus());
                zze((Result) r);
            } else if (this.vw != null) {
                zzqr.zzaqc().submit(new Runnable() {
                    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0080, code lost:
                        if (r0 == null) goto L_0x0083;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0083, code lost:
                        return;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0046, code lost:
                        if (r0 != null) goto L_0x0048;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0048, code lost:
                        r0.zzb(r5.vE);
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:6:0x004d, code lost:
                        return;
                     */
                    @WorkerThread
                    public void run() {
                        GoogleApiClient googleApiClient;
                        try {
                            zzpo.sS.set(Boolean.valueOf(true));
                            zzqx.this.vB.sendMessage(zzqx.this.vB.obtainMessage(0, zzqx.this.vw.onSuccess(r)));
                            zzpo.sS.set(Boolean.valueOf(false));
                            zzqx.this.zze(r);
                            googleApiClient = (GoogleApiClient) zzqx.this.sV.get();
                        } catch (RuntimeException e) {
                            zzqx.this.vB.sendMessage(zzqx.this.vB.obtainMessage(1, e));
                            zzpo.sS.set(Boolean.valueOf(false));
                            zzqx.this.zze(r);
                            googleApiClient = (GoogleApiClient) zzqx.this.sV.get();
                        } catch (Throwable th) {
                            zzpo.sS.set(Boolean.valueOf(false));
                            zzqx.this.zze(r);
                            GoogleApiClient googleApiClient2 = (GoogleApiClient) zzqx.this.sV.get();
                            if (googleApiClient2 != null) {
                                googleApiClient2.zzb(zzqx.this);
                            }
                            throw th;
                        }
                    }
                });
            } else if (zzaqy()) {
                this.vy.onSuccess(r);
            }
        }
    }

    @NonNull
    public <S extends Result> TransformedResult<S> then(@NonNull ResultTransform<? super R, ? extends S> resultTransform) {
        zzqx<? extends Result> zzqx;
        synchronized (this.sT) {
            boolean z = false;
            zzab.zza(this.vw == null, (Object) "Cannot call then() twice.");
            if (this.vy == null) {
                z = true;
            }
            zzab.zza(z, (Object) "Cannot call then() and andFinally() on the same TransformedResult.");
            this.vw = resultTransform;
            zzqx = new zzqx<>(this.sV);
            this.vx = zzqx;
            zzaqw();
        }
        return zzqx;
    }

    public void zza(PendingResult<?> pendingResult) {
        synchronized (this.sT) {
            this.vz = pendingResult;
            zzaqw();
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzaqx() {
        this.vy = null;
    }
}
