package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public interface zzaly {

    public static final class zza extends zzapp<zza> {
        public zzd[] bbu;
        public long timestamp;

        public zza() {
            zzcxm();
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (!zzapt.equals((Object[]) this.bbu, (Object[]) zza.bbu) || this.timestamp != zza.timestamp) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zza.bjx);
            }
            if (zza.bjx != null) {
                if (zza.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return (31 * (((((527 + getClass().getName().hashCode()) * 31) + zzapt.hashCode((Object[]) this.bbu)) * 31) + ((int) (this.timestamp ^ (this.timestamp >>> 32))))) + ((this.bjx == null || this.bjx.isEmpty()) ? 0 : this.bjx.hashCode());
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.bbu != null && this.bbu.length > 0) {
                for (zzd zzd : this.bbu) {
                    if (zzd != null) {
                        zzapo.zza(1, (zzapv) zzd);
                    }
                }
            }
            if (this.timestamp != 0) {
                zzapo.zzc(2, this.timestamp);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzay */
        public zza zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 10) {
                    int zzc = zzapy.zzc(zzapn, 10);
                    int length = this.bbu == null ? 0 : this.bbu.length;
                    zzd[] zzdArr = new zzd[(zzc + length)];
                    if (length != 0) {
                        System.arraycopy(this.bbu, 0, zzdArr, 0, length);
                    }
                    while (length < zzdArr.length - 1) {
                        zzdArr[length] = new zzd();
                        zzapn.zza(zzdArr[length]);
                        zzapn.ah();
                        length++;
                    }
                    zzdArr[length] = new zzd();
                    zzapn.zza(zzdArr[length]);
                    this.bbu = zzdArr;
                } else if (ah == 17) {
                    this.timestamp = zzapn.am();
                } else if (!super.zza(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zza zzcxm() {
            this.bbu = zzd.zzcxq();
            this.timestamp = 0;
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.bbu != null && this.bbu.length > 0) {
                for (zzd zzd : this.bbu) {
                    if (zzd != null) {
                        zzx += zzapo.zzc(1, (zzapv) zzd);
                    }
                }
            }
            return this.timestamp != 0 ? zzx + zzapo.zzf(2, this.timestamp) : zzx;
        }
    }

    public static final class zzb extends zzapp<zzb> {
        private static volatile zzb[] bbv;
        public byte[] bbw;
        public String zzcb;

        public zzb() {
            zzcxo();
        }

        public static zzb[] zzcxn() {
            if (bbv == null) {
                synchronized (zzapt.bjF) {
                    if (bbv == null) {
                        bbv = new zzb[0];
                    }
                }
            }
            return bbv;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            if (this.zzcb == null) {
                if (zzb.zzcb != null) {
                    return false;
                }
            } else if (!this.zzcb.equals(zzb.zzcb)) {
                return false;
            }
            if (!Arrays.equals(this.bbw, zzb.bbw)) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zzb.bjx);
            }
            if (zzb.bjx != null) {
                if (zzb.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((((527 + getClass().getName().hashCode()) * 31) + (this.zzcb == null ? 0 : this.zzcb.hashCode())) * 31) + Arrays.hashCode(this.bbw));
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (!this.zzcb.equals("")) {
                zzapo.zzr(1, this.zzcb);
            }
            if (!Arrays.equals(this.bbw, zzapy.bjO)) {
                zzapo.zza(2, this.bbw);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzaz */
        public zzb zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 10) {
                    this.zzcb = zzapn.readString();
                } else if (ah == 18) {
                    this.bbw = zzapn.readBytes();
                } else if (!super.zza(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zzb zzcxo() {
            this.zzcb = "";
            this.bbw = zzapy.bjO;
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (!this.zzcb.equals("")) {
                zzx += zzapo.zzs(1, this.zzcb);
            }
            return !Arrays.equals(this.bbw, zzapy.bjO) ? zzx + zzapo.zzb(2, this.bbw) : zzx;
        }
    }

    public static final class zzc extends zzapp<zzc> {
        public int bbx;
        public boolean bby;

        public zzc() {
            zzcxp();
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzc)) {
                return false;
            }
            zzc zzc = (zzc) obj;
            if (this.bbx != zzc.bbx || this.bby != zzc.bby) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zzc.bjx);
            }
            if (zzc.bjx != null) {
                if (zzc.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return (31 * (((((527 + getClass().getName().hashCode()) * 31) + this.bbx) * 31) + (this.bby ? 1231 : 1237))) + ((this.bjx == null || this.bjx.isEmpty()) ? 0 : this.bjx.hashCode());
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.bbx != 0) {
                zzapo.zzae(1, this.bbx);
            }
            if (this.bby) {
                zzapo.zzj(2, this.bby);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzba */
        public zzc zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 8) {
                    this.bbx = zzapn.al();
                } else if (ah == 16) {
                    this.bby = zzapn.an();
                } else if (!super.zza(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zzc zzcxp() {
            this.bbx = 0;
            this.bby = false;
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.bbx != 0) {
                zzx += zzapo.zzag(1, this.bbx);
            }
            return this.bby ? zzx + zzapo.zzk(2, this.bby) : zzx;
        }
    }

    public static final class zzd extends zzapp<zzd> {
        private static volatile zzd[] bbz;
        public zzb[] bbA;
        public String zx;

        public zzd() {
            zzcxr();
        }

        public static zzd[] zzcxq() {
            if (bbz == null) {
                synchronized (zzapt.bjF) {
                    if (bbz == null) {
                        bbz = new zzd[0];
                    }
                }
            }
            return bbz;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzd)) {
                return false;
            }
            zzd zzd = (zzd) obj;
            if (this.zx == null) {
                if (zzd.zx != null) {
                    return false;
                }
            } else if (!this.zx.equals(zzd.zx)) {
                return false;
            }
            if (!zzapt.equals((Object[]) this.bbA, (Object[]) zzd.bbA)) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zzd.bjx);
            }
            if (zzd.bjx != null) {
                if (zzd.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((((527 + getClass().getName().hashCode()) * 31) + (this.zx == null ? 0 : this.zx.hashCode())) * 31) + zzapt.hashCode((Object[]) this.bbA));
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (!this.zx.equals("")) {
                zzapo.zzr(1, this.zx);
            }
            if (this.bbA != null && this.bbA.length > 0) {
                for (zzb zzb : this.bbA) {
                    if (zzb != null) {
                        zzapo.zza(2, (zzapv) zzb);
                    }
                }
            }
            super.zza(zzapo);
        }

        /* renamed from: zzbb */
        public zzd zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 10) {
                    this.zx = zzapn.readString();
                } else if (ah == 18) {
                    int zzc = zzapy.zzc(zzapn, 18);
                    int length = this.bbA == null ? 0 : this.bbA.length;
                    zzb[] zzbArr = new zzb[(zzc + length)];
                    if (length != 0) {
                        System.arraycopy(this.bbA, 0, zzbArr, 0, length);
                    }
                    while (length < zzbArr.length - 1) {
                        zzbArr[length] = new zzb();
                        zzapn.zza(zzbArr[length]);
                        zzapn.ah();
                        length++;
                    }
                    zzbArr[length] = new zzb();
                    zzapn.zza(zzbArr[length]);
                    this.bbA = zzbArr;
                } else if (!super.zza(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zzd zzcxr() {
            this.zx = "";
            this.bbA = zzb.zzcxn();
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (!this.zx.equals("")) {
                zzx += zzapo.zzs(1, this.zx);
            }
            if (this.bbA != null && this.bbA.length > 0) {
                for (zzb zzb : this.bbA) {
                    if (zzb != null) {
                        zzx += zzapo.zzc(2, (zzapv) zzb);
                    }
                }
            }
            return zzx;
        }
    }

    public static final class zze extends zzapp<zze> {
        public zza bbB;
        public zza bbC;
        public zza bbD;
        public zzc bbE;
        public zzf[] bbF;

        public zze() {
            zzcxs();
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zze)) {
                return false;
            }
            zze zze = (zze) obj;
            if (this.bbB == null) {
                if (zze.bbB != null) {
                    return false;
                }
            } else if (!this.bbB.equals(zze.bbB)) {
                return false;
            }
            if (this.bbC == null) {
                if (zze.bbC != null) {
                    return false;
                }
            } else if (!this.bbC.equals(zze.bbC)) {
                return false;
            }
            if (this.bbD == null) {
                if (zze.bbD != null) {
                    return false;
                }
            } else if (!this.bbD.equals(zze.bbD)) {
                return false;
            }
            if (this.bbE == null) {
                if (zze.bbE != null) {
                    return false;
                }
            } else if (!this.bbE.equals(zze.bbE)) {
                return false;
            }
            if (!zzapt.equals((Object[]) this.bbF, (Object[]) zze.bbF)) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zze.bjx);
            }
            if (zze.bjx != null) {
                if (zze.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((((((((((527 + getClass().getName().hashCode()) * 31) + (this.bbB == null ? 0 : this.bbB.hashCode())) * 31) + (this.bbC == null ? 0 : this.bbC.hashCode())) * 31) + (this.bbD == null ? 0 : this.bbD.hashCode())) * 31) + (this.bbE == null ? 0 : this.bbE.hashCode())) * 31) + zzapt.hashCode((Object[]) this.bbF));
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.bbB != null) {
                zzapo.zza(1, (zzapv) this.bbB);
            }
            if (this.bbC != null) {
                zzapo.zza(2, (zzapv) this.bbC);
            }
            if (this.bbD != null) {
                zzapo.zza(3, (zzapv) this.bbD);
            }
            if (this.bbE != null) {
                zzapo.zza(4, (zzapv) this.bbE);
            }
            if (this.bbF != null && this.bbF.length > 0) {
                for (zzf zzf : this.bbF) {
                    if (zzf != null) {
                        zzapo.zza(5, (zzapv) zzf);
                    }
                }
            }
            super.zza(zzapo);
        }

        /* renamed from: zzbc */
        public zze zzb(zzapn zzapn) throws IOException {
            zzapv zzapv;
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 10) {
                    if (this.bbB == null) {
                        this.bbB = new zza();
                    }
                    zzapv = this.bbB;
                } else if (ah == 18) {
                    if (this.bbC == null) {
                        this.bbC = new zza();
                    }
                    zzapv = this.bbC;
                } else if (ah == 26) {
                    if (this.bbD == null) {
                        this.bbD = new zza();
                    }
                    zzapv = this.bbD;
                } else if (ah == 34) {
                    if (this.bbE == null) {
                        this.bbE = new zzc();
                    }
                    zzapv = this.bbE;
                } else if (ah == 42) {
                    int zzc = zzapy.zzc(zzapn, 42);
                    int length = this.bbF == null ? 0 : this.bbF.length;
                    zzf[] zzfArr = new zzf[(zzc + length)];
                    if (length != 0) {
                        System.arraycopy(this.bbF, 0, zzfArr, 0, length);
                    }
                    while (length < zzfArr.length - 1) {
                        zzfArr[length] = new zzf();
                        zzapn.zza(zzfArr[length]);
                        zzapn.ah();
                        length++;
                    }
                    zzfArr[length] = new zzf();
                    zzapn.zza(zzfArr[length]);
                    this.bbF = zzfArr;
                } else if (!super.zza(zzapn, ah)) {
                    return this;
                }
                zzapn.zza(zzapv);
            }
        }

        public zze zzcxs() {
            this.bbB = null;
            this.bbC = null;
            this.bbD = null;
            this.bbE = null;
            this.bbF = zzf.zzcxt();
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.bbB != null) {
                zzx += zzapo.zzc(1, (zzapv) this.bbB);
            }
            if (this.bbC != null) {
                zzx += zzapo.zzc(2, (zzapv) this.bbC);
            }
            if (this.bbD != null) {
                zzx += zzapo.zzc(3, (zzapv) this.bbD);
            }
            if (this.bbE != null) {
                zzx += zzapo.zzc(4, (zzapv) this.bbE);
            }
            if (this.bbF != null && this.bbF.length > 0) {
                for (zzf zzf : this.bbF) {
                    if (zzf != null) {
                        zzx += zzapo.zzc(5, (zzapv) zzf);
                    }
                }
            }
            return zzx;
        }
    }

    public static final class zzf extends zzapp<zzf> {
        private static volatile zzf[] bbG;
        public long bbH;
        public int resourceId;
        public String zx;

        public zzf() {
            zzcxu();
        }

        public static zzf[] zzcxt() {
            if (bbG == null) {
                synchronized (zzapt.bjF) {
                    if (bbG == null) {
                        bbG = new zzf[0];
                    }
                }
            }
            return bbG;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzf)) {
                return false;
            }
            zzf zzf = (zzf) obj;
            if (this.resourceId != zzf.resourceId || this.bbH != zzf.bbH) {
                return false;
            }
            if (this.zx == null) {
                if (zzf.zx != null) {
                    return false;
                }
            } else if (!this.zx.equals(zzf.zx)) {
                return false;
            }
            if (this.bjx != null && !this.bjx.isEmpty()) {
                return this.bjx.equals(zzf.bjx);
            }
            if (zzf.bjx != null) {
                if (zzf.bjx.isEmpty()) {
                    return true;
                }
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((((((527 + getClass().getName().hashCode()) * 31) + this.resourceId) * 31) + ((int) (this.bbH ^ (this.bbH >>> 32)))) * 31) + (this.zx == null ? 0 : this.zx.hashCode()));
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.resourceId != 0) {
                zzapo.zzae(1, this.resourceId);
            }
            if (this.bbH != 0) {
                zzapo.zzc(2, this.bbH);
            }
            if (!this.zx.equals("")) {
                zzapo.zzr(3, this.zx);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzbd */
        public zzf zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 8) {
                    this.resourceId = zzapn.al();
                } else if (ah == 17) {
                    this.bbH = zzapn.am();
                } else if (ah == 26) {
                    this.zx = zzapn.readString();
                } else if (!super.zza(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zzf zzcxu() {
            this.resourceId = 0;
            this.bbH = 0;
            this.zx = "";
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.resourceId != 0) {
                zzx += zzapo.zzag(1, this.resourceId);
            }
            if (this.bbH != 0) {
                zzx += zzapo.zzf(2, this.bbH);
            }
            return !this.zx.equals("") ? zzx + zzapo.zzs(3, this.zx) : zzx;
        }
    }
}
