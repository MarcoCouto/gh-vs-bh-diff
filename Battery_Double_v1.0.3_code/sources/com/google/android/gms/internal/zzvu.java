package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.signin.internal.zzd;

public interface zzvu extends zze {
    void connect();

    void zza(zzq zzq, boolean z);

    void zza(zzd zzd);

    void zzbzo();
}
