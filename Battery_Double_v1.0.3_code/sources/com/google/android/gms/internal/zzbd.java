package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzbd extends zzbp {
    private static final Object zzafc = new Object();
    private static volatile String zzcq;

    public zzbd(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        this.zzaha.zzdt = "E";
        if (zzcq == null) {
            synchronized (zzafc) {
                if (zzcq == null) {
                    zzcq = (String) this.zzahh.invoke(null, new Object[]{this.zzaey.getContext()});
                }
            }
        }
        synchronized (this.zzaha) {
            this.zzaha.zzdt = zzcq;
        }
    }
}
