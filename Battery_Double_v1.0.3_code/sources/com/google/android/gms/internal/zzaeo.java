package com.google.android.gms.internal;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class zzaeo {
    public static final Uri CONTENT_URI = Uri.parse("content://com.google.android.gsf.gservices");
    public static final Uri aLH = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    public static final Pattern aLI = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
    public static final Pattern aLJ = Pattern.compile("^(0|false|f|off|no|n)$", 2);
    static HashMap<String, String> aLK;
    /* access modifiers changed from: private */
    public static Object aLL;
    static HashSet<String> aLM = new HashSet<>();

    public static long getLong(ContentResolver contentResolver, String str, long j) {
        String string = getString(contentResolver, str);
        if (string == null) {
            return j;
        }
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException unused) {
            return j;
        }
    }

    public static String getString(ContentResolver contentResolver, String str) {
        return zza(contentResolver, str, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        return r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r0 = aLM.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
        if (r0.hasNext() == false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r10.startsWith((java.lang.String) r0.next()) == false) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        return r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0038, code lost:
        r9 = r9.query(CONTENT_URI, null, null, new java.lang.String[]{r10}, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0048, code lost:
        if (r9 == null) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004e, code lost:
        if (r9.moveToFirst() != false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0051, code lost:
        r0 = r9.getString(1);
        r2 = com.google.android.gms.internal.zzaeo.class;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005a, code lost:
        if (r1 != aLL) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        aLK.put(r10, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0061, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0062, code lost:
        if (r0 == null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0064, code lost:
        r11 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0065, code lost:
        if (r9 == null) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0067, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006a, code lost:
        return r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x006e, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0070, code lost:
        aLK.put(r10, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0076, code lost:
        if (r9 == null) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0078, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x007b, code lost:
        return r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x007c, code lost:
        if (r9 != null) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x007e, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0081, code lost:
        throw r10;
     */
    public static String zza(ContentResolver contentResolver, String str, String str2) {
        synchronized (zzaeo.class) {
            zza(contentResolver);
            Object obj = aLL;
            if (aLK.containsKey(str)) {
                String str3 = (String) aLK.get(str);
                if (str3 == null) {
                    str3 = str2;
                }
            }
        }
    }

    public static Map<String, String> zza(ContentResolver contentResolver, String... strArr) {
        Cursor query = contentResolver.query(aLH, null, null, strArr, null);
        TreeMap treeMap = new TreeMap();
        if (query == null) {
            return treeMap;
        }
        while (query.moveToNext()) {
            try {
                treeMap.put(query.getString(0), query.getString(1));
            } finally {
                query.close();
            }
        }
        return treeMap;
    }

    private static void zza(final ContentResolver contentResolver) {
        if (aLK == null) {
            aLK = new HashMap<>();
            aLL = new Object();
            new Thread("Gservices") {
                public void run() {
                    Looper.prepare();
                    contentResolver.registerContentObserver(zzaeo.CONTENT_URI, true, new ContentObserver(new Handler(Looper.myLooper())) {
                        public void onChange(boolean z) {
                            synchronized (zzaeo.class) {
                                zzaeo.aLK.clear();
                                zzaeo.aLL = new Object();
                                if (!zzaeo.aLM.isEmpty()) {
                                    zzaeo.zzb(contentResolver, (String[]) zzaeo.aLM.toArray(new String[zzaeo.aLM.size()]));
                                }
                            }
                        }
                    });
                    Looper.loop();
                }
            }.start();
        }
    }

    public static void zzb(ContentResolver contentResolver, String... strArr) {
        Map zza = zza(contentResolver, strArr);
        synchronized (zzaeo.class) {
            zza(contentResolver);
            aLM.addAll(Arrays.asList(strArr));
            for (Entry entry : zza.entrySet()) {
                aLK.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }
}
