package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class zzanq implements zzani, Cloneable {
    public static final zzanq beK = new zzanq();
    private double beL = -1.0d;
    private int beM = 136;
    private boolean beN = true;
    private List<zzaml> beO = Collections.emptyList();
    private List<zzaml> beP = Collections.emptyList();

    private boolean zza(zzanl zzanl) {
        return zzanl == null || zzanl.zzczt() <= this.beL;
    }

    private boolean zza(zzanl zzanl, zzanm zzanm) {
        return zza(zzanl) && zza(zzanm);
    }

    private boolean zza(zzanm zzanm) {
        return zzanm == null || zzanm.zzczt() > this.beL;
    }

    private boolean zzm(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean zzn(Class<?> cls) {
        return cls.isMemberClass() && !zzo(cls);
    }

    private boolean zzo(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
        Class m = zzaol.m();
        final boolean zza = zza(m, true);
        final boolean zza2 = zza(m, false);
        if (!zza && !zza2) {
            return null;
        }
        final zzamp zzamp2 = zzamp;
        final zzaol<T> zzaol2 = zzaol;
        AnonymousClass1 r2 = new zzanh<T>() {
            private zzanh<T> bdZ;

            private zzanh<T> zzczr() {
                zzanh<T> zzanh = this.bdZ;
                if (zzanh != null) {
                    return zzanh;
                }
                zzanh<T> zza = zzamp2.zza((zzani) zzanq.this, zzaol2);
                this.bdZ = zza;
                return zza;
            }

            public void zza(zzaoo zzaoo, T t) throws IOException {
                if (zza) {
                    zzaoo.l();
                } else {
                    zzczr().zza(zzaoo, t);
                }
            }

            public T zzb(zzaom zzaom) throws IOException {
                if (!zza2) {
                    return zzczr().zzb(zzaom);
                }
                zzaom.skipValue();
                return null;
            }
        };
        return r2;
    }

    public zzanq zza(zzaml zzaml, boolean z, boolean z2) {
        zzanq zzczv = clone();
        if (z) {
            zzczv.beO = new ArrayList(this.beO);
            zzczv.beO.add(zzaml);
        }
        if (z2) {
            zzczv.beP = new ArrayList(this.beP);
            zzczv.beP.add(zzaml);
        }
        return zzczv;
    }

    public boolean zza(Class<?> cls, boolean z) {
        if (this.beL != -1.0d && !zza((zzanl) cls.getAnnotation(zzanl.class), (zzanm) cls.getAnnotation(zzanm.class))) {
            return true;
        }
        if ((!this.beN && zzn(cls)) || zzm(cls)) {
            return true;
        }
        for (zzaml zzh : z ? this.beO : this.beP) {
            if (zzh.zzh(cls)) {
                return true;
            }
        }
        return false;
    }

    public boolean zza(Field field, boolean z) {
        if ((this.beM & field.getModifiers()) != 0) {
            return true;
        }
        if ((this.beL != -1.0d && !zza((zzanl) field.getAnnotation(zzanl.class), (zzanm) field.getAnnotation(zzanm.class))) || field.isSynthetic()) {
            return true;
        }
        if ((!this.beN && zzn(field.getType())) || zzm(field.getType())) {
            return true;
        }
        List<zzaml> list = z ? this.beO : this.beP;
        if (!list.isEmpty()) {
            zzamm zzamm = new zzamm(field);
            for (zzaml zza : list) {
                if (zza.zza(zzamm)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzczv */
    public zzanq clone() {
        try {
            return (zzanq) super.clone();
        } catch (CloneNotSupportedException unused) {
            throw new AssertionError();
        }
    }

    public zzanq zzg(int... iArr) {
        zzanq zzczv = clone();
        zzczv.beM = 0;
        for (int i : iArr) {
            zzczv.beM = i | zzczv.beM;
        }
        return zzczv;
    }
}
