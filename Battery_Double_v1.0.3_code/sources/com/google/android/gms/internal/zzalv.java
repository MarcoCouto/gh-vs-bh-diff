package com.google.android.gms.internal;

import com.google.firebase.remoteconfig.FirebaseRemoteConfigInfo;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public class zzalv implements FirebaseRemoteConfigInfo {
    private long bbo;
    private int bbp;
    private FirebaseRemoteConfigSettings bbq;

    public FirebaseRemoteConfigSettings getConfigSettings() {
        return this.bbq;
    }

    public long getFetchTimeMillis() {
        return this.bbo;
    }

    public int getLastFetchStatus() {
        return this.bbp;
    }

    public void setConfigSettings(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.bbq = firebaseRemoteConfigSettings;
    }

    public void zzafe(int i) {
        this.bbp = i;
    }

    public void zzco(long j) {
        this.bbo = j;
    }
}
