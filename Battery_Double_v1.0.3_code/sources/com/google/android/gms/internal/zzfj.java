package com.google.android.gms.internal;

import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.zzl;
import com.google.android.gms.ads.internal.zzu;

@zzin
class zzfj {
    @Nullable
    zzq zzalf;
    @Nullable
    zzw zzbkh;
    @Nullable
    zzho zzbki;
    @Nullable
    zzdo zzbkj;
    @Nullable
    zzp zzbkk;
    @Nullable
    zzd zzbkl;

    private class zza extends com.google.android.gms.ads.internal.client.zzq.zza {
        zzq zzbkm;

        zza(zzq zzq) {
            this.zzbkm = zzq;
        }

        public void onAdClosed() throws RemoteException {
            this.zzbkm.onAdClosed();
            zzu.zzgb().zzlo();
        }

        public void onAdFailedToLoad(int i) throws RemoteException {
            this.zzbkm.onAdFailedToLoad(i);
        }

        public void onAdLeftApplication() throws RemoteException {
            this.zzbkm.onAdLeftApplication();
        }

        public void onAdLoaded() throws RemoteException {
            this.zzbkm.onAdLoaded();
        }

        public void onAdOpened() throws RemoteException {
            this.zzbkm.onAdOpened();
        }
    }

    zzfj() {
    }

    /* access modifiers changed from: 0000 */
    public void zzc(zzl zzl) {
        if (this.zzalf != null) {
            zzl.zza((zzq) new zza(this.zzalf));
        }
        if (this.zzbkh != null) {
            zzl.zza(this.zzbkh);
        }
        if (this.zzbki != null) {
            zzl.zza(this.zzbki);
        }
        if (this.zzbkj != null) {
            zzl.zza(this.zzbkj);
        }
        if (this.zzbkk != null) {
            zzl.zza(this.zzbkk);
        }
        if (this.zzbkl != null) {
            zzl.zza(this.zzbkl);
        }
    }
}
