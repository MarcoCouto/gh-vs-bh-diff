package com.google.android.gms.internal;

import java.io.IOException;

public interface zzug {

    public static final class zza extends zzapv {
        private static volatile zza[] amZ;
        public Boolean ana;
        public Boolean anb;
        public String name;

        public zza() {
            zzbvo();
        }

        public static zza[] zzbvn() {
            if (amZ == null) {
                synchronized (zzapt.bjF) {
                    if (amZ == null) {
                        amZ = new zza[0];
                    }
                }
            }
            return amZ;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.name == null) {
                if (zza.name != null) {
                    return false;
                }
            } else if (!this.name.equals(zza.name)) {
                return false;
            }
            if (this.ana == null) {
                if (zza.ana != null) {
                    return false;
                }
            } else if (!this.ana.equals(zza.ana)) {
                return false;
            }
            if (this.anb == null) {
                if (zza.anb != null) {
                    return false;
                }
            } else if (!this.anb.equals(zza.anb)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((((527 + getClass().getName().hashCode()) * 31) + (this.name == null ? 0 : this.name.hashCode())) * 31) + (this.ana == null ? 0 : this.ana.hashCode()));
            if (this.anb != null) {
                i = this.anb.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.name != null) {
                zzapo.zzr(1, this.name);
            }
            if (this.ana != null) {
                zzapo.zzj(2, this.ana.booleanValue());
            }
            if (this.anb != null) {
                zzapo.zzj(3, this.anb.booleanValue());
            }
            super.zza(zzapo);
        }

        /* renamed from: zzai */
        public zza zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 10) {
                    this.name = zzapn.readString();
                } else if (ah == 16) {
                    this.ana = Boolean.valueOf(zzapn.an());
                } else if (ah == 24) {
                    this.anb = Boolean.valueOf(zzapn.an());
                } else if (!zzapy.zzb(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zza zzbvo() {
            this.name = null;
            this.ana = null;
            this.anb = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.name != null) {
                zzx += zzapo.zzs(1, this.name);
            }
            if (this.ana != null) {
                zzx += zzapo.zzk(2, this.ana.booleanValue());
            }
            return this.anb != null ? zzx + zzapo.zzk(3, this.anb.booleanValue()) : zzx;
        }
    }

    public static final class zzb extends zzapv {
        public String aic;
        public Long anc;
        public Integer and;
        public zzc[] ane;
        public zza[] anf;
        public com.google.android.gms.internal.zzuf.zza[] ang;

        public zzb() {
            zzbvp();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            if (this.anc == null) {
                if (zzb.anc != null) {
                    return false;
                }
            } else if (!this.anc.equals(zzb.anc)) {
                return false;
            }
            if (this.aic == null) {
                if (zzb.aic != null) {
                    return false;
                }
            } else if (!this.aic.equals(zzb.aic)) {
                return false;
            }
            if (this.and == null) {
                if (zzb.and != null) {
                    return false;
                }
            } else if (!this.and.equals(zzb.and)) {
                return false;
            }
            return zzapt.equals((Object[]) this.ane, (Object[]) zzb.ane) && zzapt.equals((Object[]) this.anf, (Object[]) zzb.anf) && zzapt.equals((Object[]) this.ang, (Object[]) zzb.ang);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = (((((527 + getClass().getName().hashCode()) * 31) + (this.anc == null ? 0 : this.anc.hashCode())) * 31) + (this.aic == null ? 0 : this.aic.hashCode())) * 31;
            if (this.and != null) {
                i = this.and.hashCode();
            }
            return (31 * (((((hashCode + i) * 31) + zzapt.hashCode((Object[]) this.ane)) * 31) + zzapt.hashCode((Object[]) this.anf))) + zzapt.hashCode((Object[]) this.ang);
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.anc != null) {
                zzapo.zzb(1, this.anc.longValue());
            }
            if (this.aic != null) {
                zzapo.zzr(2, this.aic);
            }
            if (this.and != null) {
                zzapo.zzae(3, this.and.intValue());
            }
            if (this.ane != null && this.ane.length > 0) {
                for (zzc zzc : this.ane) {
                    if (zzc != null) {
                        zzapo.zza(4, (zzapv) zzc);
                    }
                }
            }
            if (this.anf != null && this.anf.length > 0) {
                for (zza zza : this.anf) {
                    if (zza != null) {
                        zzapo.zza(5, (zzapv) zza);
                    }
                }
            }
            if (this.ang != null && this.ang.length > 0) {
                for (com.google.android.gms.internal.zzuf.zza zza2 : this.ang) {
                    if (zza2 != null) {
                        zzapo.zza(6, (zzapv) zza2);
                    }
                }
            }
            super.zza(zzapo);
        }

        /* renamed from: zzaj */
        public zzb zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 8) {
                    this.anc = Long.valueOf(zzapn.ak());
                } else if (ah == 18) {
                    this.aic = zzapn.readString();
                } else if (ah == 24) {
                    this.and = Integer.valueOf(zzapn.al());
                } else if (ah == 34) {
                    int zzc = zzapy.zzc(zzapn, 34);
                    int length = this.ane == null ? 0 : this.ane.length;
                    zzc[] zzcArr = new zzc[(zzc + length)];
                    if (length != 0) {
                        System.arraycopy(this.ane, 0, zzcArr, 0, length);
                    }
                    while (length < zzcArr.length - 1) {
                        zzcArr[length] = new zzc();
                        zzapn.zza(zzcArr[length]);
                        zzapn.ah();
                        length++;
                    }
                    zzcArr[length] = new zzc();
                    zzapn.zza(zzcArr[length]);
                    this.ane = zzcArr;
                } else if (ah == 42) {
                    int zzc2 = zzapy.zzc(zzapn, 42);
                    int length2 = this.anf == null ? 0 : this.anf.length;
                    zza[] zzaArr = new zza[(zzc2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.anf, 0, zzaArr, 0, length2);
                    }
                    while (length2 < zzaArr.length - 1) {
                        zzaArr[length2] = new zza();
                        zzapn.zza(zzaArr[length2]);
                        zzapn.ah();
                        length2++;
                    }
                    zzaArr[length2] = new zza();
                    zzapn.zza(zzaArr[length2]);
                    this.anf = zzaArr;
                } else if (ah == 50) {
                    int zzc3 = zzapy.zzc(zzapn, 50);
                    int length3 = this.ang == null ? 0 : this.ang.length;
                    com.google.android.gms.internal.zzuf.zza[] zzaArr2 = new com.google.android.gms.internal.zzuf.zza[(zzc3 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.ang, 0, zzaArr2, 0, length3);
                    }
                    while (length3 < zzaArr2.length - 1) {
                        zzaArr2[length3] = new com.google.android.gms.internal.zzuf.zza();
                        zzapn.zza(zzaArr2[length3]);
                        zzapn.ah();
                        length3++;
                    }
                    zzaArr2[length3] = new com.google.android.gms.internal.zzuf.zza();
                    zzapn.zza(zzaArr2[length3]);
                    this.ang = zzaArr2;
                } else if (!zzapy.zzb(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zzb zzbvp() {
            this.anc = null;
            this.aic = null;
            this.and = null;
            this.ane = zzc.zzbvq();
            this.anf = zza.zzbvn();
            this.ang = com.google.android.gms.internal.zzuf.zza.zzbvd();
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.anc != null) {
                zzx += zzapo.zze(1, this.anc.longValue());
            }
            if (this.aic != null) {
                zzx += zzapo.zzs(2, this.aic);
            }
            if (this.and != null) {
                zzx += zzapo.zzag(3, this.and.intValue());
            }
            if (this.ane != null && this.ane.length > 0) {
                int i = zzx;
                for (zzc zzc : this.ane) {
                    if (zzc != null) {
                        i += zzapo.zzc(4, (zzapv) zzc);
                    }
                }
                zzx = i;
            }
            if (this.anf != null && this.anf.length > 0) {
                int i2 = zzx;
                for (zza zza : this.anf) {
                    if (zza != null) {
                        i2 += zzapo.zzc(5, (zzapv) zza);
                    }
                }
                zzx = i2;
            }
            if (this.ang != null && this.ang.length > 0) {
                for (com.google.android.gms.internal.zzuf.zza zza2 : this.ang) {
                    if (zza2 != null) {
                        zzx += zzapo.zzc(6, (zzapv) zza2);
                    }
                }
            }
            return zzx;
        }
    }

    public static final class zzc extends zzapv {
        private static volatile zzc[] anh;
        public String value;
        public String zzcb;

        public zzc() {
            zzbvr();
        }

        public static zzc[] zzbvq() {
            if (anh == null) {
                synchronized (zzapt.bjF) {
                    if (anh == null) {
                        anh = new zzc[0];
                    }
                }
            }
            return anh;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzc)) {
                return false;
            }
            zzc zzc = (zzc) obj;
            if (this.zzcb == null) {
                if (zzc.zzcb != null) {
                    return false;
                }
            } else if (!this.zzcb.equals(zzc.zzcb)) {
                return false;
            }
            if (this.value == null) {
                if (zzc.value != null) {
                    return false;
                }
            } else if (!this.value.equals(zzc.value)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = 31 * (((527 + getClass().getName().hashCode()) * 31) + (this.zzcb == null ? 0 : this.zzcb.hashCode()));
            if (this.value != null) {
                i = this.value.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzcb != null) {
                zzapo.zzr(1, this.zzcb);
            }
            if (this.value != null) {
                zzapo.zzr(2, this.value);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzak */
        public zzc zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                if (ah == 0) {
                    return this;
                }
                if (ah == 10) {
                    this.zzcb = zzapn.readString();
                } else if (ah == 18) {
                    this.value = zzapn.readString();
                } else if (!zzapy.zzb(zzapn, ah)) {
                    return this;
                }
            }
        }

        public zzc zzbvr() {
            this.zzcb = null;
            this.value = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzcb != null) {
                zzx += zzapo.zzs(1, this.zzcb);
            }
            return this.value != null ? zzx + zzapo.zzs(2, this.value) : zzx;
        }
    }
}
