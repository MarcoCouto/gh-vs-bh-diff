package com.google.android.gms.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.FirebaseException;

public abstract class zzpi {
    public final int iq;
    public final int sx;

    public static final class zza extends zzpi {
        public final com.google.android.gms.internal.zzpm.zza<? extends Result, com.google.android.gms.common.api.Api.zzb> sy;

        public zza(int i, int i2, com.google.android.gms.internal.zzpm.zza<? extends Result, com.google.android.gms.common.api.Api.zzb> zza) {
            super(i, i2);
            this.sy = zza;
        }

        public boolean cancel() {
            return this.sy.zzaov();
        }

        public void zza(SparseArray<zzqy> sparseArray) {
            zzqy zzqy = (zzqy) sparseArray.get(this.sx);
            if (zzqy != null) {
                zzqy.zzg(this.sy);
            }
        }

        public void zzb(com.google.android.gms.common.api.Api.zzb zzb) throws DeadObjectException {
            this.sy.zzb(zzb);
        }

        public void zzx(@NonNull Status status) {
            this.sy.zzz(status);
        }
    }

    public static final class zzb<TResult> extends zzpi {
        private static final Status sB = new Status(8, "Connection to Google Play services was lost while executing the API call.");
        private final TaskCompletionSource<TResult> sA;
        private final zzqw<com.google.android.gms.common.api.Api.zzb, TResult> sz;

        public zzb(int i, int i2, zzqw<com.google.android.gms.common.api.Api.zzb, TResult> zzqw, TaskCompletionSource<TResult> taskCompletionSource) {
            super(i, i2);
            this.sA = taskCompletionSource;
            this.sz = zzqw;
        }

        public void zzb(com.google.android.gms.common.api.Api.zzb zzb) throws DeadObjectException {
            try {
                this.sz.zza(zzb, this.sA);
            } catch (DeadObjectException e) {
                zzx(sB);
                throw e;
            } catch (RemoteException unused) {
                zzx(sB);
            }
        }

        public void zzx(@NonNull Status status) {
            TaskCompletionSource<TResult> taskCompletionSource;
            Exception firebaseApiNotAvailableException;
            if (status.getStatusCode() == 8) {
                taskCompletionSource = this.sA;
                firebaseApiNotAvailableException = new FirebaseException(status.getStatusMessage());
            } else {
                taskCompletionSource = this.sA;
                firebaseApiNotAvailableException = new FirebaseApiNotAvailableException(status.getStatusMessage());
            }
            taskCompletionSource.setException(firebaseApiNotAvailableException);
        }
    }

    public zzpi(int i, int i2) {
        this.sx = i;
        this.iq = i2;
    }

    public boolean cancel() {
        return true;
    }

    public void zza(SparseArray<zzqy> sparseArray) {
    }

    public abstract void zzb(com.google.android.gms.common.api.Api.zzb zzb2) throws DeadObjectException;

    public abstract void zzx(@NonNull Status status);
}
