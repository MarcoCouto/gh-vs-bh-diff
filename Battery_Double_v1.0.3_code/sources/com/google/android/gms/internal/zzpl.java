package com.google.android.gms.internal;

import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.zza;
import com.google.android.gms.common.api.zzb;
import java.util.Set;

public final class zzpl extends zzpo<zzb> {
    private int sH;
    private boolean sI;

    private void zza(ConnectionResult connectionResult) {
        int i = 0;
        while (true) {
            ArrayMap arrayMap = null;
            if (i < arrayMap.size()) {
                zza((zzpj) arrayMap.keyAt(i), connectionResult);
                i++;
            } else {
                return;
            }
        }
    }

    public void zza(zzpj<?> zzpj, ConnectionResult connectionResult) {
        ArrayMap arrayMap = null;
        synchronized (arrayMap) {
            arrayMap.put(zzpj, connectionResult);
            this.sH--;
            if (!connectionResult.isSuccess()) {
                this.sI = true;
            }
            if (this.sH == 0) {
                Status status = this.sI ? new Status(13) : Status.sq;
                zzc(arrayMap.size() == 1 ? new zza(status, arrayMap) : new zzb(status, arrayMap));
            }
        }
    }

    public Set<zzpj<?>> zzaoq() {
        ArrayMap arrayMap = null;
        return arrayMap.keySet();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzy */
    public zzb zzc(Status status) {
        zzb zza;
        ArrayMap arrayMap = null;
        synchronized (arrayMap) {
            zza(new ConnectionResult(8));
            zza = arrayMap.size() == 1 ? new zza(status, arrayMap) : new zzb(status, arrayMap);
        }
        return zza;
    }
}
