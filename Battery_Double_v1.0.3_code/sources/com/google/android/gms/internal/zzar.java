package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.internal.zzae.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class zzar extends zzaq {
    private static final String TAG = "zzar";

    protected zzar(Context context, String str, boolean z) {
        super(context, str, z);
    }

    public static zzar zza(String str, Context context, boolean z) {
        zza(context, z);
        return new zzar(context, str, z);
    }

    /* access modifiers changed from: protected */
    public List<Callable<Void>> zzb(zzax zzax, zza zza) {
        if (zzax.zzcd() == null || !this.zzafn) {
            return super.zzb(zzax, zza);
        }
        int zzat = zzax.zzat();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(super.zzb(zzax, zza));
        zzbh zzbh = new zzbh(zzax, zzav.zzbl(), zzav.zzbm(), zza, zzat, 24);
        arrayList.add(zzbh);
        return arrayList;
    }
}
