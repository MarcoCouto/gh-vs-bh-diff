package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.signin.internal.zzg;

public final class zzvt {
    public static final Api<zzvv> API = new Api<>("SignIn.API", bK, bJ);
    public static final Api<zza> Dz = new Api<>("SignIn.INTERNAL_API", atQ, atP);
    public static final zzf<zzg> atP = new zzf<>();
    static final com.google.android.gms.common.api.Api.zza<zzg, zza> atQ = new com.google.android.gms.common.api.Api.zza<zzg, zza>() {
        public zzg zza(Context context, Looper looper, com.google.android.gms.common.internal.zzg zzg, zza zza, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            zzg zzg2 = new zzg(context, looper, false, zzg, zza.zzbzn(), connectionCallbacks, onConnectionFailedListener);
            return zzg2;
        }
    };
    public static final zzf<zzg> bJ = new zzf<>();
    public static final com.google.android.gms.common.api.Api.zza<zzg, zzvv> bK = new com.google.android.gms.common.api.Api.zza<zzg, zzvv>() {
        public zzg zza(Context context, Looper looper, com.google.android.gms.common.internal.zzg zzg, zzvv zzvv, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            if (zzvv == null) {
                zzvv = zzvv.atR;
            }
            zzg zzg2 = new zzg(context, looper, true, zzg, zzvv, connectionCallbacks, onConnectionFailedListener);
            return zzg2;
        }
    };
    public static final Scope dK = new Scope(Scopes.PROFILE);
    public static final Scope dL = new Scope("email");

    public static class zza implements HasOptions {
        public Bundle zzbzn() {
            return null;
        }
    }
}
