package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.api.Api.zzh;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzah;
import com.google.android.gms.common.internal.zzd.zzf;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.altbeacon.beacon.BeaconManager;

public class zzqc implements Callback {
    private static zzqc uG;
    /* access modifiers changed from: private */
    public static final Object zzamr = new Object();
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    /* access modifiers changed from: private */
    public final GoogleApiAvailability sh;
    /* access modifiers changed from: private */
    public long uF;
    /* access modifiers changed from: private */
    public int uH;
    private final AtomicInteger uI;
    private final SparseArray<zzc<?>> uJ;
    /* access modifiers changed from: private */
    public final Map<zzpj<?>, zzc<?>> uK;
    private zzpr uL;
    /* access modifiers changed from: private */
    public final Set<zzpj<?>> uM;
    private final ReferenceQueue<com.google.android.gms.common.api.zzc<?>> uN;
    /* access modifiers changed from: private */
    public final SparseArray<zza> uO;
    private zzb uP;
    /* access modifiers changed from: private */
    public long ue;
    /* access modifiers changed from: private */
    public long uf;

    private final class zza extends PhantomReference<com.google.android.gms.common.api.zzc<?>> {
        /* access modifiers changed from: private */
        public final int sx;

        public zza(com.google.android.gms.common.api.zzc zzc, int i, ReferenceQueue<com.google.android.gms.common.api.zzc<?>> referenceQueue) {
            super(zzc, referenceQueue);
            this.sx = i;
        }

        public void zzaqg() {
            zzqc.this.mHandler.sendMessage(zzqc.this.mHandler.obtainMessage(2, this.sx, 2));
        }
    }

    private static final class zzb extends Thread {
        private final ReferenceQueue<com.google.android.gms.common.api.zzc<?>> uN;
        private final SparseArray<zza> uO;
        /* access modifiers changed from: private */
        public final AtomicBoolean uR = new AtomicBoolean();

        public zzb(ReferenceQueue<com.google.android.gms.common.api.zzc<?>> referenceQueue, SparseArray<zza> sparseArray) {
            super("GoogleApiCleanup");
            this.uN = referenceQueue;
            this.uO = sparseArray;
        }

        public void run() {
            this.uR.set(true);
            Process.setThreadPriority(10);
            while (this.uR.get()) {
                try {
                    zza zza = (zza) this.uN.remove();
                    this.uO.remove(zza.sx);
                    zza.zzaqg();
                } catch (InterruptedException unused) {
                } catch (Throwable th) {
                    this.uR.set(false);
                    throw th;
                }
            }
            this.uR.set(false);
        }
    }

    private class zzc<O extends ApiOptions> implements ConnectionCallbacks, OnConnectionFailedListener {
        private final zzpj<O> rQ;
        /* access modifiers changed from: private */
        public final Queue<zzpi> uS = new LinkedList();
        private final zze uT;
        private final com.google.android.gms.common.api.Api.zzb uU;
        private final SparseArray<zzqy> uV = new SparseArray<>();
        private final Set<zzpl> uW = new HashSet();
        private final SparseArray<Map<Object, com.google.android.gms.internal.zzpm.zza>> uX = new SparseArray<>();
        private ConnectionResult uY = null;
        private boolean ud;

        @WorkerThread
        public zzc(com.google.android.gms.common.api.zzc<O> zzc) {
            this.uT = zzb((com.google.android.gms.common.api.zzc) zzc);
            this.uU = this.uT instanceof zzah ? ((zzah) this.uT).zzatn() : this.uT;
            this.rQ = zzc.zzaob();
        }

        /* access modifiers changed from: private */
        @WorkerThread
        public void connect() {
            if (!this.uT.isConnected() && !this.uT.isConnecting()) {
                if (this.uT.zzanu() && zzqc.this.uH != 0) {
                    zzqc.this.uH = zzqc.this.sh.isGooglePlayServicesAvailable(zzqc.this.mContext);
                    if (zzqc.this.uH != 0) {
                        onConnectionFailed(new ConnectionResult(zzqc.this.uH, null));
                        return;
                    }
                }
                this.uT.zza(new zzd(this.uT, this.rQ));
            }
        }

        /* access modifiers changed from: private */
        @WorkerThread
        public void resume() {
            if (this.ud) {
                connect();
            }
        }

        /* access modifiers changed from: private */
        @WorkerThread
        public void zzab(Status status) {
            for (zzpi zzx : this.uS) {
                zzx.zzx(status);
            }
            this.uS.clear();
        }

        /* access modifiers changed from: private */
        @WorkerThread
        public void zzapu() {
            if (this.ud) {
                zzaqk();
                zzab(zzqc.this.sh.isGooglePlayServicesAvailable(zzqc.this.mContext) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
                this.uT.disconnect();
            }
        }

        @WorkerThread
        private void zzaqk() {
            if (this.ud) {
                zzqc.this.mHandler.removeMessages(9, this.rQ);
                zzqc.this.mHandler.removeMessages(8, this.rQ);
                this.ud = false;
            }
        }

        private void zzaql() {
            zzqc.this.mHandler.removeMessages(10, this.rQ);
            zzqc.this.mHandler.sendMessageDelayed(zzqc.this.mHandler.obtainMessage(10, this.rQ), zzqc.this.uF);
        }

        /* access modifiers changed from: private */
        public void zzaqm() {
            if (this.uT.isConnected() && this.uX.size() == 0) {
                for (int i = 0; i < this.uV.size(); i++) {
                    if (((zzqy) this.uV.get(this.uV.keyAt(i))).zzara()) {
                        zzaql();
                        return;
                    }
                }
                this.uT.disconnect();
            }
        }

        @WorkerThread
        private zze zzb(com.google.android.gms.common.api.zzc zzc) {
            Api zzanz = zzc.zzanz();
            if (!zzanz.zzant()) {
                return zzc.zzanz().zzanq().zza(zzc.getApplicationContext(), zzqc.this.mHandler.getLooper(), zzg.zzcd(zzc.getApplicationContext()), zzc.zzaoa(), this, this);
            }
            zzh zzanr = zzanz.zzanr();
            zzah zzah = new zzah(zzc.getApplicationContext(), zzqc.this.mHandler.getLooper(), zzanr.zzanw(), this, this, zzg.zzcd(zzc.getApplicationContext()), zzanr.zzr(zzc.zzaoa()));
            return zzah;
        }

        @WorkerThread
        private void zzc(zzpi zzpi) {
            zzpi.zza(this.uV);
            if (zzpi.iq == 3) {
                try {
                    Map map = (Map) this.uX.get(zzpi.sx);
                    if (map == null) {
                        map = new ArrayMap(1);
                        this.uX.put(zzpi.sx, map);
                    }
                    com.google.android.gms.internal.zzpm.zza<? extends Result, com.google.android.gms.common.api.Api.zzb> zza = ((com.google.android.gms.internal.zzpi.zza) zzpi).sy;
                    map.put(((zzqm) zza).zzaqu(), zza);
                } catch (ClassCastException unused) {
                    throw new IllegalStateException("Listener registration methods must implement ListenerApiMethod");
                }
            } else if (zzpi.iq == 4) {
                try {
                    Map map2 = (Map) this.uX.get(zzpi.sx);
                    zzqm zzqm = (zzqm) ((com.google.android.gms.internal.zzpi.zza) zzpi).sy;
                    if (map2 != null) {
                        map2.remove(zzqm.zzaqu());
                    } else {
                        Log.w("GoogleApiManager", "Received call to unregister a listener without a matching registration call.");
                    }
                } catch (ClassCastException unused2) {
                    throw new IllegalStateException("Listener unregistration methods must implement ListenerApiMethod");
                }
            }
            try {
                zzpi.zzb(this.uU);
            } catch (DeadObjectException unused3) {
                this.uT.disconnect();
                onConnectionSuspended(1);
            }
        }

        @WorkerThread
        private void zzj(ConnectionResult connectionResult) {
            for (zzpl zza : this.uW) {
                zza.zza(this.rQ, connectionResult);
            }
            this.uW.clear();
        }

        /* access modifiers changed from: 0000 */
        public boolean isConnected() {
            return this.uT.isConnected();
        }

        @WorkerThread
        public void onConnected(@Nullable Bundle bundle) {
            zzaqi();
            zzj(ConnectionResult.rb);
            zzaqk();
            for (int i = 0; i < this.uX.size(); i++) {
                for (com.google.android.gms.internal.zzpm.zza zzb : ((Map) this.uX.get(this.uX.keyAt(i))).values()) {
                    try {
                        zzb.zzb(this.uU);
                    } catch (DeadObjectException unused) {
                        this.uT.disconnect();
                        onConnectionSuspended(1);
                    }
                }
            }
            zzaqh();
            zzaql();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
            if (r5.uQ.zzc(r6, r0) != false) goto L_0x00af;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
            if (r6.getErrorCode() != 18) goto L_0x0058;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
            r5.ud = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
            if (r5.ud == false) goto L_0x007a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x005c, code lost:
            com.google.android.gms.internal.zzqc.zza(r5.uQ).sendMessageDelayed(android.os.Message.obtain(com.google.android.gms.internal.zzqc.zza(r5.uQ), 8, r5.rQ), com.google.android.gms.internal.zzqc.zzb(r5.uQ));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0079, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x007a, code lost:
            r1 = java.lang.String.valueOf(r5.rQ.zzaon());
            r2 = new java.lang.StringBuilder(38 + java.lang.String.valueOf(r1).length());
            r2.append("API: ");
            r2.append(r1);
            r2.append(" is not available on this device.");
            zzab(new com.google.android.gms.common.api.Status(17, r2.toString()));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00af, code lost:
            return;
         */
        @WorkerThread
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            zzaqi();
            zzqc.this.uH = -1;
            zzj(connectionResult);
            int keyAt = this.uV.keyAt(0);
            if (this.uS.isEmpty()) {
                this.uY = connectionResult;
                return;
            }
            synchronized (zzqc.zzamr) {
                if (zzqc.zzd(zzqc.this) != null && zzqc.this.uM.contains(this.rQ)) {
                    zzqc.zzd(zzqc.this).zzb(connectionResult, keyAt);
                }
            }
        }

        @WorkerThread
        public void onConnectionSuspended(int i) {
            zzaqi();
            this.ud = true;
            zzqc.this.mHandler.sendMessageDelayed(Message.obtain(zzqc.this.mHandler, 8, this.rQ), zzqc.this.uf);
            zzqc.this.mHandler.sendMessageDelayed(Message.obtain(zzqc.this.mHandler, 9, this.rQ), zzqc.this.ue);
            zzqc.this.uH = -1;
        }

        @WorkerThread
        public void zzaqh() {
            while (this.uT.isConnected() && !this.uS.isEmpty()) {
                zzc((zzpi) this.uS.remove());
            }
        }

        @WorkerThread
        public void zzaqi() {
            this.uY = null;
        }

        /* access modifiers changed from: 0000 */
        public ConnectionResult zzaqj() {
            return this.uY;
        }

        @WorkerThread
        public void zzb(zzpi zzpi) {
            if (this.uT.isConnected()) {
                zzc(zzpi);
                zzaql();
                return;
            }
            this.uS.add(zzpi);
            if (this.uY == null || !this.uY.hasResolution()) {
                connect();
            } else {
                onConnectionFailed(this.uY);
            }
        }

        @WorkerThread
        public void zzb(zzpl zzpl) {
            this.uW.add(zzpl);
        }

        @WorkerThread
        public void zzf(int i, boolean z) {
            Iterator it = this.uS.iterator();
            while (it.hasNext()) {
                zzpi zzpi = (zzpi) it.next();
                if (zzpi.sx == i && zzpi.iq != 1 && zzpi.cancel()) {
                    it.remove();
                }
            }
            ((zzqy) this.uV.get(i)).release();
            this.uX.delete(i);
            if (!z) {
                this.uV.remove(i);
                zzqc.this.uO.remove(i);
                if (this.uV.size() == 0 && this.uS.isEmpty()) {
                    zzaqk();
                    this.uT.disconnect();
                    zzqc.this.uK.remove(this.rQ);
                    synchronized (zzqc.zzamr) {
                        zzqc.this.uM.remove(this.rQ);
                    }
                }
            }
        }

        @WorkerThread
        public void zzfn(int i) {
            this.uV.put(i, new zzqy(this.rQ.zzans(), this.uT));
        }

        @WorkerThread
        public void zzfo(final int i) {
            ((zzqy) this.uV.get(i)).zza((zzc) new zzc() {
                public void zzaqn() {
                    if (zzc.this.uS.isEmpty()) {
                        zzc.this.zzf(i, false);
                    }
                }
            });
        }
    }

    private class zzd implements zzf {
        private final zzpj<?> rQ;
        private final zze uT;

        public zzd(zze zze, zzpj<?> zzpj) {
            this.uT = zze;
            this.rQ = zzpj;
        }

        @WorkerThread
        public void zzh(@NonNull ConnectionResult connectionResult) {
            if (connectionResult.isSuccess()) {
                this.uT.zza(null, Collections.emptySet());
            } else {
                ((zzc) zzqc.this.uK.get(this.rQ)).onConnectionFailed(connectionResult);
            }
        }
    }

    private zzqc(Context context) {
        this(context, GoogleApiAvailability.getInstance());
    }

    private zzqc(Context context, GoogleApiAvailability googleApiAvailability) {
        this.uf = 5000;
        this.ue = 120000;
        this.uF = BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD;
        this.uH = -1;
        this.uI = new AtomicInteger(1);
        this.uJ = new SparseArray<>();
        this.uK = new ConcurrentHashMap(5, 0.75f, 1);
        this.uL = null;
        this.uM = new com.google.android.gms.common.util.zza();
        this.uN = new ReferenceQueue<>();
        this.uO = new SparseArray<>();
        this.mContext = context;
        HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
        handlerThread.start();
        this.mHandler = new Handler(handlerThread.getLooper(), this);
        this.sh = googleApiAvailability;
    }

    private int zza(com.google.android.gms.common.api.zzc<?> zzc2) {
        int andIncrement = this.uI.getAndIncrement();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(6, andIncrement, 0, zzc2));
        return andIncrement;
    }

    public static Pair<zzqc, Integer> zza(Context context, com.google.android.gms.common.api.zzc<?> zzc2) {
        Pair<zzqc, Integer> create;
        synchronized (zzamr) {
            if (uG == null) {
                uG = new zzqc(context.getApplicationContext());
            }
            create = Pair.create(uG, Integer.valueOf(uG.zza(zzc2)));
        }
        return create;
    }

    @WorkerThread
    private void zza(com.google.android.gms.common.api.zzc<?> zzc2, int i) {
        zzpj zzaob = zzc2.zzaob();
        if (!this.uK.containsKey(zzaob)) {
            this.uK.put(zzaob, new zzc(zzc2));
        }
        zzc zzc3 = (zzc) this.uK.get(zzaob);
        zzc3.zzfn(i);
        this.uJ.put(i, zzc3);
        zzc3.connect();
        this.uO.put(i, new zza(zzc2, i, this.uN));
        if (this.uP == null || !this.uP.uR.get()) {
            this.uP = new zzb(this.uN, this.uO);
            this.uP.start();
        }
    }

    @WorkerThread
    private void zza(zzpi zzpi) {
        ((zzc) this.uJ.get(zzpi.sx)).zzb(zzpi);
    }

    public static zzqc zzaqd() {
        zzqc zzqc;
        synchronized (zzamr) {
            zzqc = uG;
        }
        return zzqc;
    }

    @WorkerThread
    private void zzaqe() {
        for (zzc zzc2 : this.uK.values()) {
            zzc2.zzaqi();
            zzc2.connect();
        }
    }

    static /* synthetic */ zzpr zzd(zzqc zzqc) {
        return null;
    }

    @WorkerThread
    private void zze(int i, boolean z) {
        zzc zzc2 = (zzc) this.uJ.get(i);
        if (zzc2 != null) {
            if (!z) {
                this.uJ.delete(i);
            }
            zzc2.zzf(i, z);
            return;
        }
        StringBuilder sb = new StringBuilder(52);
        sb.append("onRelease received for unknown instance: ");
        sb.append(i);
        Log.wtf("GoogleApiManager", sb.toString(), new Exception());
    }

    @WorkerThread
    private void zzfm(int i) {
        zzc zzc2 = (zzc) this.uJ.get(i);
        if (zzc2 != null) {
            this.uJ.delete(i);
            zzc2.zzfo(i);
            return;
        }
        StringBuilder sb = new StringBuilder(64);
        sb.append("onCleanupLeakInternal received for unknown instance: ");
        sb.append(i);
        Log.wtf("GoogleApiManager", sb.toString(), new Exception());
    }

    @WorkerThread
    public boolean handleMessage(Message message) {
        boolean z = false;
        switch (message.what) {
            case 1:
                zza((zzpl) message.obj);
                break;
            case 2:
                zzfm(message.arg1);
                return true;
            case 3:
                zzaqe();
                return true;
            case 4:
                zza((zzpi) message.obj);
                return true;
            case 5:
                if (this.uJ.get(message.arg1) != null) {
                    ((zzc) this.uJ.get(message.arg1)).zzab(new Status(17, "Error resolution was canceled by the user."));
                    return true;
                }
                break;
            case 6:
                zza((com.google.android.gms.common.api.zzc) message.obj, message.arg1);
                return true;
            case 7:
                int i = message.arg1;
                if (message.arg2 == 1) {
                    z = true;
                }
                zze(i, z);
                return true;
            case 8:
                if (this.uK.containsKey(message.obj)) {
                    ((zzc) this.uK.get(message.obj)).resume();
                    return true;
                }
                break;
            case 9:
                if (this.uK.containsKey(message.obj)) {
                    ((zzc) this.uK.get(message.obj)).zzapu();
                    return true;
                }
                break;
            case 10:
                if (this.uK.containsKey(message.obj)) {
                    ((zzc) this.uK.get(message.obj)).zzaqm();
                    return true;
                }
                break;
            default:
                int i2 = message.what;
                StringBuilder sb = new StringBuilder(31);
                sb.append("Unknown message id: ");
                sb.append(i2);
                Log.w("GoogleApiManager", sb.toString());
                return false;
        }
        return true;
    }

    public void zza(ConnectionResult connectionResult, int i) {
        if (!zzc(connectionResult, i)) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(5, i, 0));
        }
    }

    public <O extends ApiOptions> void zza(com.google.android.gms.common.api.zzc<O> zzc2, int i, com.google.android.gms.internal.zzpm.zza<? extends Result, com.google.android.gms.common.api.Api.zzb> zza2) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, new com.google.android.gms.internal.zzpi.zza(zzc2.getInstanceId(), i, zza2)));
    }

    public <O extends ApiOptions, TResult> void zza(com.google.android.gms.common.api.zzc<O> zzc2, int i, zzqw<com.google.android.gms.common.api.Api.zzb, TResult> zzqw, TaskCompletionSource<TResult> taskCompletionSource) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, new com.google.android.gms.internal.zzpi.zzb(zzc2.getInstanceId(), i, zzqw, taskCompletionSource)));
    }

    @WorkerThread
    public void zza(zzpl zzpl) {
        ConnectionResult zzaqj;
        for (zzpj zzpj : zzpl.zzaoq()) {
            zzc zzc2 = (zzc) this.uK.get(zzpj);
            if (zzc2 == null) {
                zzpl.cancel();
                return;
            }
            if (zzc2.isConnected()) {
                zzaqj = ConnectionResult.rb;
            } else if (zzc2.zzaqj() != null) {
                zzaqj = zzc2.zzaqj();
            } else {
                zzc2.zzb(zzpl);
            }
            zzpl.zza(zzpj, zzaqj);
        }
    }

    public void zza(zzpr zzpr) {
        synchronized (zzamr) {
            if (zzpr == null) {
                try {
                    this.uL = null;
                    this.uM.clear();
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    public void zzaoo() {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(3));
    }

    /* access modifiers changed from: 0000 */
    public boolean zzc(ConnectionResult connectionResult, int i) {
        if (!connectionResult.hasResolution() && !this.sh.isUserResolvableError(connectionResult.getErrorCode())) {
            return false;
        }
        this.sh.zza(this.mContext, connectionResult, i);
        return true;
    }

    public void zzd(int i, boolean z) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(7, i, z ? 1 : 2));
    }
}
