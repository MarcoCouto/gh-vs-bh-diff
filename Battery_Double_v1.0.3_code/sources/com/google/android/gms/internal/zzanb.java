package com.google.android.gms.internal;

import java.math.BigInteger;

public final class zzanb extends zzamv {
    private static final Class<?>[] bek = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};
    private Object value;

    public zzanb(Boolean bool) {
        setValue(bool);
    }

    public zzanb(Number number) {
        setValue(number);
    }

    public zzanb(String str) {
        setValue(str);
    }

    private static boolean zza(zzanb zzanb) {
        if (!(zzanb.value instanceof Number)) {
            return false;
        }
        Number number = (Number) zzanb.value;
        return (number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte);
    }

    private static boolean zzci(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class cls = obj.getClass();
        for (Class<?> isAssignableFrom : bek) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        zzanb zzanb = (zzanb) obj;
        if (this.value == null) {
            return zzanb.value == null;
        }
        if (zza(this) && zza(zzanb)) {
            return zzcze().longValue() == zzanb.zzcze().longValue();
        }
        if (!(this.value instanceof Number) || !(zzanb.value instanceof Number)) {
            return this.value.equals(zzanb.value);
        }
        double doubleValue = zzcze().doubleValue();
        double doubleValue2 = zzanb.zzcze().doubleValue();
        if (doubleValue != doubleValue2) {
            if (Double.isNaN(doubleValue) && Double.isNaN(doubleValue2)) {
                return true;
            }
            z = false;
        }
        return z;
    }

    public boolean getAsBoolean() {
        return zzczo() ? zzczn().booleanValue() : Boolean.parseBoolean(zzczf());
    }

    public double getAsDouble() {
        return zzczp() ? zzcze().doubleValue() : Double.parseDouble(zzczf());
    }

    public int getAsInt() {
        return zzczp() ? zzcze().intValue() : Integer.parseInt(zzczf());
    }

    public long getAsLong() {
        return zzczp() ? zzcze().longValue() : Long.parseLong(zzczf());
    }

    public int hashCode() {
        if (this.value == null) {
            return 31;
        }
        if (zza(this)) {
            long longValue = zzcze().longValue();
            return (int) (longValue ^ (longValue >>> 32));
        } else if (!(this.value instanceof Number)) {
            return this.value.hashCode();
        } else {
            long doubleToLongBits = Double.doubleToLongBits(zzcze().doubleValue());
            return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        }
    }

    /* access modifiers changed from: 0000 */
    public void setValue(Object obj) {
        if (obj instanceof Character) {
            obj = String.valueOf(((Character) obj).charValue());
        } else {
            zzann.zzbo((obj instanceof Number) || zzci(obj));
        }
        this.value = obj;
    }

    public Number zzcze() {
        return this.value instanceof String ? new zzans((String) this.value) : (Number) this.value;
    }

    public String zzczf() {
        return zzczp() ? zzcze().toString() : zzczo() ? zzczn().toString() : (String) this.value;
    }

    /* access modifiers changed from: 0000 */
    public Boolean zzczn() {
        return (Boolean) this.value;
    }

    public boolean zzczo() {
        return this.value instanceof Boolean;
    }

    public boolean zzczp() {
        return this.value instanceof Number;
    }

    public boolean zzczq() {
        return this.value instanceof String;
    }
}
