package com.google.android.gms.internal;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzex implements zzep {
    private final Object zzail = new Object();
    private final Map<String, zza> zzbix = new HashMap();

    public interface zza {
        void zzay(String str);

        void zzd(JSONObject jSONObject);
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        String str = (String) map.get("id");
        String str2 = (String) map.get("fail");
        String str3 = (String) map.get("fail_reason");
        String str4 = (String) map.get("result");
        synchronized (this.zzail) {
            zza zza2 = (zza) this.zzbix.remove(str);
            if (zza2 == null) {
                String str5 = "Received result for unexpected method invocation: ";
                String valueOf = String.valueOf(str);
                zzkd.zzcx(valueOf.length() != 0 ? str5.concat(valueOf) : new String(str5));
            } else if (!TextUtils.isEmpty(str2)) {
                zza2.zzay(str3);
            } else if (str4 == null) {
                zza2.zzay("No result.");
            } else {
                try {
                    zza2.zzd(new JSONObject(str4));
                } catch (JSONException e) {
                    zza2.zzay(e.getMessage());
                }
            }
        }
    }
}
