package com.google.android.gms.internal;

import android.os.SystemClock;
import com.google.android.gms.internal.zzb.zza;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.cookie.DateUtils;

public class zzt implements zzf {
    protected static final boolean DEBUG = zzs.DEBUG;
    private static int zzbn = 3000;
    private static int zzbo = 4096;
    protected final zzy zzbp;
    protected final zzu zzbq;

    public zzt(zzy zzy) {
        this(zzy, new zzu(zzbo));
    }

    public zzt(zzy zzy, zzu zzu) {
        this.zzbp = zzy;
        this.zzbq = zzu;
    }

    protected static Map<String, String> zza(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headerArr.length; i++) {
            treeMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return treeMap;
    }

    private void zza(long j, zzk<?> zzk, byte[] bArr, StatusLine statusLine) {
        if (DEBUG || j > ((long) zzbn)) {
            String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
            Object[] objArr = new Object[5];
            objArr[0] = zzk;
            objArr[1] = Long.valueOf(j);
            objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
            objArr[3] = Integer.valueOf(statusLine.getStatusCode());
            objArr[4] = Integer.valueOf(zzk.zzt().zzd());
            zzs.zzb(str, objArr);
        }
    }

    private static void zza(String str, zzk<?> zzk, zzr zzr) throws zzr {
        zzo zzt = zzk.zzt();
        int zzs = zzk.zzs();
        try {
            zzt.zza(zzr);
            zzk.zzc(String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(zzs)}));
        } catch (zzr e) {
            zzk.zzc(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(zzs)}));
            throw e;
        }
    }

    private void zza(Map<String, String> map, zza zza) {
        if (zza != null) {
            if (zza.zza != null) {
                map.put("If-None-Match", zza.zza);
            }
            if (zza.zzc > 0) {
                map.put("If-Modified-Since", DateUtils.formatDate(new Date(zza.zzc)));
            }
        }
    }

    private byte[] zza(HttpEntity httpEntity) throws IOException, zzp {
        zzaa zzaa = new zzaa(this.zzbq, (int) httpEntity.getContentLength());
        byte[] bArr = null;
        try {
            InputStream content = httpEntity.getContent();
            if (content == null) {
                throw new zzp();
            }
            byte[] zzb = this.zzbq.zzb(1024);
            while (true) {
                try {
                    int read = content.read(zzb);
                    if (read == -1) {
                        break;
                    }
                    zzaa.write(zzb, 0, read);
                } catch (Throwable th) {
                    th = th;
                    bArr = zzb;
                    try {
                        httpEntity.consumeContent();
                    } catch (IOException unused) {
                        zzs.zza("Error occured when calling consumingContent", new Object[0]);
                    }
                    this.zzbq.zza(bArr);
                    zzaa.close();
                    throw th;
                }
            }
            byte[] byteArray = zzaa.toByteArray();
            try {
                httpEntity.consumeContent();
            } catch (IOException unused2) {
                zzs.zza("Error occured when calling consumingContent", new Object[0]);
            }
            this.zzbq.zza(zzb);
            zzaa.close();
            return byteArray;
        } catch (Throwable th2) {
            th = th2;
            httpEntity.consumeContent();
            this.zzbq.zza(bArr);
            zzaa.close();
            throw th;
        }
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.zzh.<init>(com.google.android.gms.internal.zzi):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006f, code lost:
        r1 = r0;
        r16 = null;
        r17 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00bb, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00bd, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00be, code lost:
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bf, code lost:
        r1 = r0;
        r17 = r2;
        r16 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c6, code lost:
        r1 = r0;
        r17 = r14;
        r16 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00cd, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ce, code lost:
        r17 = r1;
        r16 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00dc, code lost:
        r1 = r13.getStatusLine().getStatusCode();
        com.google.android.gms.internal.zzs.zzc("Unexpected response code %d for %s", java.lang.Integer.valueOf(r1), r24.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f9, code lost:
        if (r16 != null) goto L_0x00fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00fb, code lost:
        r14 = new com.google.android.gms.internal.zzi(r1, r16, r17, false, android.os.SystemClock.elapsedRealtime() - r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x010c, code lost:
        if (r1 == 401) goto L_0x0119;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0118, code lost:
        throw new com.google.android.gms.internal.zzp(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0119, code lost:
        zza("auth", r8, new com.google.android.gms.internal.zza(r14));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x012a, code lost:
        throw new com.google.android.gms.internal.zzh((com.google.android.gms.internal.zzi) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0130, code lost:
        throw new com.google.android.gms.internal.zzj(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0131, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0132, code lost:
        r1 = r0;
        r3 = "Bad URL ";
        r4 = java.lang.String.valueOf(r24.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0143, code lost:
        if (r4.length() != 0) goto L_0x0145;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0145, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x014a, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0153, code lost:
        throw new java.lang.RuntimeException(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0154, code lost:
        r1 = "connection";
        r2 = new com.google.android.gms.internal.zzq();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x015c, code lost:
        r1 = "socket";
        r2 = new com.google.android.gms.internal.zzq();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0163, code lost:
        zza(r1, r8, r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0131 A[ExcHandler: MalformedURLException (r0v0 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[ExcHandler: ConnectTimeoutException (unused org.apache.http.conn.ConnectTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:73:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x012b A[SYNTHETIC] */
    public zzi zza(zzk<?> zzk) throws zzr {
        HttpResponse httpResponse;
        Map zza;
        zzk<?> zzk2 = zzk;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            Map emptyMap = Collections.emptyMap();
            try {
                HashMap hashMap = new HashMap();
                zza(hashMap, zzk.zzh());
                httpResponse = this.zzbp.zza(zzk2, hashMap);
                StatusLine statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                zza = zza(httpResponse.getAllHeaders());
                if (statusCode == 304) {
                    zza zzh = zzk.zzh();
                    if (zzh == null) {
                        zzi zzi = new zzi(304, null, zza, true, SystemClock.elapsedRealtime() - elapsedRealtime);
                        return zzi;
                    }
                    zzh.zzf.putAll(zza);
                    zzi zzi2 = new zzi(304, zzh.data, zzh.zzf, true, SystemClock.elapsedRealtime() - elapsedRealtime);
                    return zzi2;
                }
                byte[] zza2 = httpResponse.getEntity() != null ? zza(httpResponse.getEntity()) : new byte[0];
                zza(SystemClock.elapsedRealtime() - elapsedRealtime, zzk2, zza2, statusLine);
                if (statusCode < 200) {
                    break;
                } else if (statusCode > 299) {
                    break;
                } else {
                    Map map = zza;
                    r14 = r14;
                    zzi zzi3 = new zzi(statusCode, zza2, map, false, SystemClock.elapsedRealtime() - elapsedRealtime);
                    return zzi3;
                }
            } catch (SocketTimeoutException unused) {
            } catch (ConnectTimeoutException unused2) {
            } catch (MalformedURLException e) {
            } catch (IOException e2) {
                e = e2;
                Map map2 = emptyMap;
                httpResponse = null;
                byte[] bArr = null;
                Throwable th = e;
                if (httpResponse == null) {
                }
            }
        }
        Map map3 = zza;
        throw new IOException();
    }
}
