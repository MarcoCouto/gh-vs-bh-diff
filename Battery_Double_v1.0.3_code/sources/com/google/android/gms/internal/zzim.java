package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri.Builder;
import android.os.Build.VERSION;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@zzin
public class zzim implements UncaughtExceptionHandler {
    private Context mContext;
    private VersionInfoParcel zzamw;
    @Nullable
    private UncaughtExceptionHandler zzcac;
    @Nullable
    private UncaughtExceptionHandler zzcad;

    public zzim(Context context, VersionInfoParcel versionInfoParcel, @Nullable UncaughtExceptionHandler uncaughtExceptionHandler, @Nullable UncaughtExceptionHandler uncaughtExceptionHandler2) {
        this.zzcac = uncaughtExceptionHandler;
        this.zzcad = uncaughtExceptionHandler2;
        this.mContext = context;
        this.zzamw = versionInfoParcel;
    }

    public static zzim zza(Context context, Thread thread, VersionInfoParcel versionInfoParcel) {
        if (context == null || thread == null || versionInfoParcel == null || !zzu(context)) {
            return null;
        }
        UncaughtExceptionHandler uncaughtExceptionHandler = thread.getUncaughtExceptionHandler();
        zzim zzim = new zzim(context, versionInfoParcel, uncaughtExceptionHandler, Thread.getDefaultUncaughtExceptionHandler());
        if (uncaughtExceptionHandler != null && (uncaughtExceptionHandler instanceof zzim)) {
            return (zzim) uncaughtExceptionHandler;
        }
        try {
            thread.setUncaughtExceptionHandler(zzim);
            return zzim;
        } catch (SecurityException e) {
            zzkd.zzc("Fail to set UncaughtExceptionHandler.", e);
            return null;
        }
    }

    private Throwable zzc(Throwable th) {
        if (((Boolean) zzdc.zzaye.get()).booleanValue()) {
            return th;
        }
        LinkedList linkedList = new LinkedList();
        while (th != null) {
            linkedList.push(th);
            th = th.getCause();
        }
        Throwable th2 = null;
        while (!linkedList.isEmpty()) {
            Throwable th3 = (Throwable) linkedList.pop();
            StackTraceElement[] stackTrace = th3.getStackTrace();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new StackTraceElement(th3.getClass().getName(), "<filtered>", "<filtered>", 1));
            boolean z = false;
            for (StackTraceElement stackTraceElement : stackTrace) {
                if (zzcc(stackTraceElement.getClassName())) {
                    arrayList.add(stackTraceElement);
                    z = true;
                } else {
                    if (!zzcd(stackTraceElement.getClassName())) {
                        stackTraceElement = new StackTraceElement("<filtered>", "<filtered>", "<filtered>", 1);
                    }
                    arrayList.add(stackTraceElement);
                }
            }
            if (z) {
                th2 = th2 == null ? new Throwable(th3.getMessage()) : new Throwable(th3.getMessage(), th2);
                th2.setStackTrace((StackTraceElement[]) arrayList.toArray(new StackTraceElement[0]));
            }
        }
        return th2;
    }

    private static boolean zzu(Context context) {
        return ((Boolean) zzdc.zzayd.get()).booleanValue();
    }

    public void uncaughtException(Thread thread, Throwable th) {
        UncaughtExceptionHandler uncaughtExceptionHandler;
        if (zzb(th)) {
            if (Looper.getMainLooper().getThread() != thread) {
                zza(th, true);
                return;
            }
            zza(th, false);
        }
        if (this.zzcac != null) {
            uncaughtExceptionHandler = this.zzcac;
        } else if (this.zzcad != null) {
            uncaughtExceptionHandler = this.zzcad;
        } else {
            return;
        }
        uncaughtExceptionHandler.uncaughtException(thread, th);
    }

    /* access modifiers changed from: 0000 */
    public String zza(Class cls, Throwable th, boolean z) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return new Builder().scheme("https").path("//pagead2.googlesyndication.com/pagead/gen_204").appendQueryParameter("id", "gmob-apps-report-exception").appendQueryParameter("os", VERSION.RELEASE).appendQueryParameter("api", String.valueOf(VERSION.SDK_INT)).appendQueryParameter("device", zzu.zzfq().zztg()).appendQueryParameter("js", this.zzamw.zzcs).appendQueryParameter("appid", this.mContext.getApplicationContext().getPackageName()).appendQueryParameter("exceptiontype", cls.getName()).appendQueryParameter("stacktrace", stringWriter.toString()).appendQueryParameter("eids", TextUtils.join(",", zzdc.zzjx())).appendQueryParameter("trapped", String.valueOf(z)).toString();
    }

    public void zza(Throwable th, boolean z) {
        if (zzu(this.mContext)) {
            Throwable zzc = zzc(th);
            if (zzc != null) {
                Class cls = th.getClass();
                ArrayList arrayList = new ArrayList();
                arrayList.add(zza(cls, zzc, z));
                zzu.zzfq().zza((List<String>) arrayList, zzu.zzft().zzso());
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzb(Throwable th) {
        StackTraceElement[] stackTrace;
        boolean z = false;
        if (th == null) {
            return false;
        }
        boolean z2 = false;
        boolean z3 = false;
        while (th != null) {
            boolean z4 = z3;
            boolean z5 = z2;
            for (StackTraceElement stackTraceElement : th.getStackTrace()) {
                if (zzcc(stackTraceElement.getClassName())) {
                    z5 = true;
                }
                if (getClass().getName().equals(stackTraceElement.getClassName())) {
                    z4 = true;
                }
            }
            th = th.getCause();
            z2 = z5;
            z3 = z4;
        }
        if (z2 && !z3) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public boolean zzcc(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (str.startsWith((String) zzdc.zzayf.get())) {
            return true;
        }
        try {
            return Class.forName(str).isAnnotationPresent(zzin.class);
        } catch (Exception e) {
            String str2 = "Fail to check class type for class ";
            String valueOf = String.valueOf(str);
            zzkd.zza(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2), e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzcd(String str) {
        boolean z = false;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (str.startsWith("android.") || str.startsWith("java.")) {
            z = true;
        }
        return z;
    }
}
