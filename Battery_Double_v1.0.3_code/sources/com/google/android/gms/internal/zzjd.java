package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.client.zza.C0018zza;

@zzin
public class zzjd extends C0018zza {
    private final String zzcfz;
    private final int zzche;

    public zzjd(String str, int i) {
        this.zzcfz = str;
        this.zzche = i;
    }

    public int getAmount() {
        return this.zzche;
    }

    public String getType() {
        return this.zzcfz;
    }
}
