package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public final class zzany<E> extends zzanh<Object> {
    public static final zzani bfu = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            Type n = zzaol.n();
            if (!(n instanceof GenericArrayType) && (!(n instanceof Class) || !((Class) n).isArray())) {
                return null;
            }
            Type zzh = zzano.zzh(n);
            return new zzany(zzamp, zzamp.zza(zzaol.zzl(zzh)), zzano.zzf(zzh));
        }
    };
    private final Class<E> bfv;
    private final zzanh<E> bfw;

    public zzany(zzamp zzamp, zzanh<E> zzanh, Class<E> cls) {
        this.bfw = new zzaoj(zzamp, zzanh, cls);
        this.bfv = cls;
    }

    public void zza(zzaoo zzaoo, Object obj) throws IOException {
        if (obj == null) {
            zzaoo.l();
            return;
        }
        zzaoo.h();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.bfw.zza(zzaoo, Array.get(obj, i));
        }
        zzaoo.i();
    }

    public Object zzb(zzaom zzaom) throws IOException {
        if (zzaom.b() == zzaon.NULL) {
            zzaom.nextNull();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        zzaom.beginArray();
        while (zzaom.hasNext()) {
            arrayList.add(this.bfw.zzb(zzaom));
        }
        zzaom.endArray();
        Object newInstance = Array.newInstance(this.bfv, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }
}
