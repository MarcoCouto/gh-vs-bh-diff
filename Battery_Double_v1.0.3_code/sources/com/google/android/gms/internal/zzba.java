package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzba extends zzbp {
    private static final Object zzafc = new Object();
    private static volatile String zzagy;

    public zzba(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        this.zzaha.zzdp = "E";
        if (zzagy == null) {
            synchronized (zzafc) {
                if (zzagy == null) {
                    zzagy = (String) this.zzahh.invoke(null, new Object[]{this.zzaey.getContext()});
                }
            }
        }
        synchronized (this.zzaha) {
            this.zzaha.zzdp = zzaj.zza(zzagy.getBytes(), true);
        }
    }
}
