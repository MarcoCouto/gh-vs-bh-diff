package com.google.android.gms.internal;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzi;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.dynamic.zze;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzii implements Callable<zzju> {
    private static final long zzbyt = TimeUnit.SECONDS.toMillis(60);
    private final Context mContext;
    private final Object zzail = new Object();
    private final zzih zzbgb;
    private final zzas zzbgd;
    /* access modifiers changed from: private */
    public final com.google.android.gms.internal.zzju.zza zzbxr;
    private int zzbyi;
    private final zzkn zzbzc;
    /* access modifiers changed from: private */
    public final zzq zzbzd;
    private boolean zzbze;
    private List<String> zzbzf;
    private JSONObject zzbzg;

    public interface zza<T extends com.google.android.gms.ads.internal.formats.zzh.zza> {
        T zza(zzii zzii, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException;
    }

    class zzb {
        public zzep zzbzz;

        zzb() {
        }
    }

    public zzii(Context context, zzq zzq, zzkn zzkn, zzas zzas, com.google.android.gms.internal.zzju.zza zza2) {
        this.mContext = context;
        this.zzbzd = zzq;
        this.zzbzc = zzkn;
        this.zzbxr = zza2;
        this.zzbgd = zzas;
        this.zzbgb = zza(context, zza2, zzq, zzas);
        this.zzbgb.zzqg();
        this.zzbze = false;
        this.zzbyi = -2;
        this.zzbzf = null;
    }

    private com.google.android.gms.ads.internal.formats.zzh.zza zza(zza zza2, JSONObject jSONObject, String str) throws ExecutionException, InterruptedException, JSONException {
        if (zzqs()) {
            return null;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject("tracking_urls_and_actions");
        String[] zzc = zzc(jSONObject2, "impression_tracking_urls");
        this.zzbzf = zzc == null ? null : Arrays.asList(zzc);
        this.zzbzg = jSONObject2.optJSONObject("active_view");
        com.google.android.gms.ads.internal.formats.zzh.zza zza3 = zza2.zza(this, jSONObject);
        if (zza3 == null) {
            zzkd.e("Failed to retrieve ad assets.");
            return null;
        }
        zzi zzi = new zzi(this.mContext, this.zzbzd, this.zzbgb, this.zzbgd, jSONObject, zza3, this.zzbxr.zzcip.zzaow, str);
        zza3.zzb(zzi);
        return zza3;
    }

    private zzky<zzc> zza(JSONObject jSONObject, boolean z, boolean z2) throws JSONException {
        String string = z ? jSONObject.getString("url") : jSONObject.optString("url");
        final double optDouble = jSONObject.optDouble("scale", 1.0d);
        if (TextUtils.isEmpty(string)) {
            zza(0, z);
            return new zzkw(null);
        } else if (z2) {
            return new zzkw(new zzc(null, Uri.parse(string), optDouble));
        } else {
            zzkn zzkn = this.zzbzc;
            final boolean z3 = z;
            final String str = string;
            AnonymousClass6 r1 = new com.google.android.gms.internal.zzkn.zza<zzc>() {
                /* renamed from: zzg */
                public zzc zzh(InputStream inputStream) {
                    byte[] bArr;
                    try {
                        bArr = zzo.zzk(inputStream);
                    } catch (IOException unused) {
                        bArr = null;
                    }
                    if (bArr == null) {
                        zzii.this.zza(2, z3);
                        return null;
                    }
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                    if (decodeByteArray == null) {
                        zzii.this.zza(2, z3);
                        return null;
                    }
                    decodeByteArray.setDensity((int) (160.0d * optDouble));
                    return new zzc(new BitmapDrawable(Resources.getSystem(), decodeByteArray), Uri.parse(str), optDouble);
                }

                /* renamed from: zzqt */
                public zzc zzqu() {
                    zzii.this.zza(2, z3);
                    return null;
                }
            };
            return zzkn.zza(string, r1);
        }
    }

    private void zza(com.google.android.gms.ads.internal.formats.zzh.zza zza2) {
        if (zza2 instanceof zzf) {
            final zzf zzf = (zzf) zza2;
            zzb zzb2 = new zzb();
            final AnonymousClass3 r1 = new zzep() {
                public void zza(zzlh zzlh, Map<String, String> map) {
                    zzii.this.zzb((zzdz) zzf, (String) map.get("asset"));
                }
            };
            zzb2.zzbzz = r1;
            this.zzbgb.zza((com.google.android.gms.internal.zzih.zza) new com.google.android.gms.internal.zzih.zza() {
                public void zze(zzft zzft) {
                    zzft.zza("/nativeAdCustomClick", r1);
                }
            });
        }
    }

    private zzju zzb(com.google.android.gms.ads.internal.formats.zzh.zza zza2) {
        int i;
        synchronized (this.zzail) {
            try {
                int i2 = this.zzbyi;
                if (zza2 == null && this.zzbyi == -2) {
                    i2 = 0;
                }
            } finally {
                while (true) {
                    i = th;
                }
            }
        }
        com.google.android.gms.ads.internal.formats.zzh.zza zza3 = i != -2 ? null : zza2;
        AdRequestParcel adRequestParcel = this.zzbxr.zzcip.zzcar;
        List<String> list = this.zzbxr.zzciq.zzbnm;
        List<String> list2 = this.zzbxr.zzciq.zzbnn;
        List<String> list3 = this.zzbzf;
        int i3 = this.zzbxr.zzciq.orientation;
        long j = this.zzbxr.zzciq.zzbns;
        String str = this.zzbxr.zzcip.zzcau;
        AdSizeParcel adSizeParcel = this.zzbxr.zzapa;
        long j2 = this.zzbxr.zzciq.zzcbx;
        List<String> list4 = list;
        long j3 = this.zzbxr.zzcik;
        long j4 = j2;
        long j5 = this.zzbxr.zzcil;
        String str2 = this.zzbxr.zzciq.zzccd;
        long j6 = j3;
        List<String> list5 = list4;
        AdSizeParcel adSizeParcel2 = adSizeParcel;
        String str3 = str2;
        zzju zzju = new zzju(adRequestParcel, null, list5, i, list2, list3, i3, j, str, false, null, null, null, null, null, 0, adSizeParcel2, j4, j6, j5, str3, this.zzbzg, zza3, null, null, null, this.zzbxr.zzciq.zzccq, this.zzbxr.zzciq.zzccr, null, this.zzbxr.zzciq.zzbnp);
        return zzju;
    }

    private Integer zzb(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt("r"), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void zzb(zzdz zzdz, String str) {
        try {
            zzed zzv = this.zzbzd.zzv(zzdz.getCustomTemplateId());
            if (zzv != null) {
                zzv.zza(zzdz, str);
            }
        } catch (RemoteException e) {
            StringBuilder sb = new StringBuilder(40 + String.valueOf(str).length());
            sb.append("Failed to call onCustomClick for asset ");
            sb.append(str);
            sb.append(".");
            zzkd.zzd(sb.toString(), e);
        }
    }

    private String[] zzc(JSONObject jSONObject, String str) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        String[] strArr = new String[optJSONArray.length()];
        for (int i = 0; i < optJSONArray.length(); i++) {
            strArr[i] = optJSONArray.getString(i);
        }
        return strArr;
    }

    private JSONObject zzcb(final String str) throws ExecutionException, InterruptedException, TimeoutException, JSONException {
        if (zzqs()) {
            return null;
        }
        final zzkv zzkv = new zzkv();
        final zzb zzb2 = new zzb();
        this.zzbgb.zza((com.google.android.gms.internal.zzih.zza) new com.google.android.gms.internal.zzih.zza() {
            public void zze(final zzft zzft) {
                AnonymousClass1 r0 = new zzep() {
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        zzft.zzb("/nativeAdPreProcess", zzb2.zzbzz);
                        try {
                            String str = (String) map.get("success");
                            if (!TextUtils.isEmpty(str)) {
                                zzkv.zzh(new JSONObject(str).getJSONArray("ads").getJSONObject(0));
                                return;
                            }
                        } catch (JSONException e) {
                            zzkd.zzb("Malformed native JSON response.", e);
                        }
                        zzii.this.zzan(0);
                        zzab.zza(zzii.this.zzqs(), (Object) "Unable to set the ad state error!");
                        zzkv.zzh(null);
                    }
                };
                zzb2.zzbzz = r0;
                zzft.zza("/nativeAdPreProcess", (zzep) r0);
                try {
                    JSONObject jSONObject = new JSONObject(zzii.this.zzbxr.zzciq.body);
                    jSONObject.put("ads_id", str);
                    zzft.zza("google.afma.nativeAds.preProcessJsonGmsg", jSONObject);
                } catch (JSONException e) {
                    zzkd.zzd("Exception occurred while invoking javascript", e);
                    zzkv.zzh(null);
                }
            }

            public void zzqq() {
                zzkv.zzh(null);
            }
        });
        return (JSONObject) zzkv.get(zzbyt, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: private */
    public static List<Drawable> zzh(List<zzc> list) throws RemoteException {
        ArrayList arrayList = new ArrayList();
        for (zzc zzkt : list) {
            arrayList.add((Drawable) zze.zzad(zzkt.zzkt()));
        }
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public zzih zza(Context context, com.google.android.gms.internal.zzju.zza zza2, zzq zzq, zzas zzas) {
        return new zzih(context, zza2, zzq, zzas);
    }

    public zzky<zzc> zza(JSONObject jSONObject, String str, boolean z, boolean z2) throws JSONException {
        JSONObject jSONObject2 = z ? jSONObject.getJSONObject(str) : jSONObject.optJSONObject(str);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, z, z2);
    }

    public List<zzky<zzc>> zza(JSONObject jSONObject, String str, boolean z, boolean z2, boolean z3) throws JSONException {
        JSONArray jSONArray = z ? jSONObject.getJSONArray(str) : jSONObject.optJSONArray(str);
        ArrayList arrayList = new ArrayList();
        if (jSONArray == null || jSONArray.length() == 0) {
            zza(0, z);
            return arrayList;
        }
        int length = z3 ? jSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            if (jSONObject2 == null) {
                jSONObject2 = new JSONObject();
            }
            arrayList.add(zza(jSONObject2, z, z2));
        }
        return arrayList;
    }

    public Future<zzc> zza(JSONObject jSONObject, String str, boolean z) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        boolean optBoolean = jSONObject2.optBoolean("require", true);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, optBoolean, z);
    }

    public void zza(int i, boolean z) {
        if (z) {
            zzan(i);
        }
    }

    public void zzan(int i) {
        synchronized (this.zzail) {
            this.zzbze = true;
            this.zzbyi = i;
        }
    }

    /* access modifiers changed from: protected */
    public zza zzf(JSONObject jSONObject) throws ExecutionException, InterruptedException, JSONException, TimeoutException {
        if (zzqs()) {
            return null;
        }
        String string = jSONObject.getString("template_id");
        boolean z = this.zzbxr.zzcip.zzapo != null ? this.zzbxr.zzcip.zzapo.zzbgp : false;
        boolean z2 = this.zzbxr.zzcip.zzapo != null ? this.zzbxr.zzcip.zzapo.zzbgr : false;
        if ("2".equals(string)) {
            return new zzij(z, z2);
        }
        if ("1".equals(string)) {
            return new zzik(z, z2);
        }
        if ("3".equals(string)) {
            final String string2 = jSONObject.getString("custom_template_id");
            final zzkv zzkv = new zzkv();
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzkv.zzh((zzee) zzii.this.zzbzd.zzfb().get(string2));
                }
            });
            if (zzkv.get(zzbyt, TimeUnit.MILLISECONDS) != null) {
                return new zzil(z);
            }
            String str = "No handler for custom template: ";
            String valueOf = String.valueOf(jSONObject.getString("custom_template_id"));
            zzkd.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            return null;
        }
        zzan(0);
        return null;
    }

    public zzky<com.google.android.gms.ads.internal.formats.zza> zzg(JSONObject jSONObject) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject("attribution");
        if (optJSONObject == null) {
            return new zzkw(null);
        }
        String optString = optJSONObject.optString("text");
        final int optInt = optJSONObject.optInt("text_size", -1);
        final Integer zzb2 = zzb(optJSONObject, "text_color");
        Integer zzb3 = zzb(optJSONObject, "bg_color");
        final int optInt2 = optJSONObject.optInt("animation_ms", 1000);
        final int optInt3 = optJSONObject.optInt("presentation_ms", 4000);
        final int i = (this.zzbxr.zzcip.zzapo == null || this.zzbxr.zzcip.zzapo.versionCode < 2) ? 1 : this.zzbxr.zzcip.zzapo.zzbgs;
        List arrayList = new ArrayList();
        if (optJSONObject.optJSONArray("images") != null) {
            arrayList = zza(optJSONObject, "images", false, false, true);
        } else {
            arrayList.add(zza(optJSONObject, "image", false, false));
        }
        zzky zzn = zzkx.zzn(arrayList);
        final String str = optString;
        final Integer num = zzb3;
        AnonymousClass5 r3 = new com.google.android.gms.internal.zzkx.zza<List<zzc>, com.google.android.gms.ads.internal.formats.zza>() {
            /* renamed from: zzj */
            public com.google.android.gms.ads.internal.formats.zza apply(List<zzc> list) {
                if (list != null) {
                    try {
                        if (list.isEmpty()) {
                            return null;
                        }
                        com.google.android.gms.ads.internal.formats.zza zza = new com.google.android.gms.ads.internal.formats.zza(str, zzii.zzh(list), num, zzb2, optInt > 0 ? Integer.valueOf(optInt) : null, optInt3 + optInt2, i);
                        return zza;
                    } catch (RemoteException e) {
                        zzkd.zzb("Could not get attribution icon", e);
                    }
                }
                return null;
            }
        };
        return zzkx.zza(zzn, r3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* renamed from: zzqr */
    public zzju call() {
        String str;
        try {
            this.zzbgb.zzqh();
            String uuid = UUID.randomUUID().toString();
            JSONObject zzcb = zzcb(uuid);
            com.google.android.gms.ads.internal.formats.zzh.zza zza2 = zza(zzf(zzcb), zzcb, uuid);
            zza(zza2);
            return zzb(zza2);
        } catch (InterruptedException | CancellationException | ExecutionException unused) {
            if (!this.zzbze) {
                zzan(0);
            }
            return zzb((com.google.android.gms.ads.internal.formats.zzh.zza) null);
        } catch (JSONException e) {
            e = e;
            str = "Malformed native JSON response.";
            zzkd.zzd(str, e);
            if (!this.zzbze) {
            }
            return zzb((com.google.android.gms.ads.internal.formats.zzh.zza) null);
        } catch (TimeoutException e2) {
            e = e2;
            str = "Timeout when loading native ad.";
            zzkd.zzd(str, e);
            if (!this.zzbze) {
            }
            return zzb((com.google.android.gms.ads.internal.formats.zzh.zza) null);
        }
    }

    public boolean zzqs() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzbze;
        }
        return z;
    }
}
