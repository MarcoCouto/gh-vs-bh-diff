package com.google.android.gms.internal;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public final class zzana {
    public zzamv zza(Reader reader) throws zzamw, zzane {
        try {
            zzaom zzaom = new zzaom(reader);
            zzamv zzh = zzh(zzaom);
            if (zzh.zzczj() || zzaom.b() == zzaon.END_DOCUMENT) {
                return zzh;
            }
            throw new zzane("Did not consume the entire document.");
        } catch (zzaop e) {
            throw new zzane((Throwable) e);
        } catch (IOException e2) {
            throw new zzamw((Throwable) e2);
        } catch (NumberFormatException e3) {
            throw new zzane((Throwable) e3);
        }
    }

    public zzamv zzh(zzaom zzaom) throws zzamw, zzane {
        boolean isLenient = zzaom.isLenient();
        zzaom.setLenient(true);
        try {
            zzamv zzh = zzanw.zzh(zzaom);
            zzaom.setLenient(isLenient);
            return zzh;
        } catch (StackOverflowError e) {
            String valueOf = String.valueOf(zzaom);
            StringBuilder sb = new StringBuilder(36 + String.valueOf(valueOf).length());
            sb.append("Failed parsing JSON source: ");
            sb.append(valueOf);
            sb.append(" to Json");
            throw new zzamz(sb.toString(), e);
        } catch (OutOfMemoryError e2) {
            String valueOf2 = String.valueOf(zzaom);
            StringBuilder sb2 = new StringBuilder(36 + String.valueOf(valueOf2).length());
            sb2.append("Failed parsing JSON source: ");
            sb2.append(valueOf2);
            sb2.append(" to Json");
            throw new zzamz(sb2.toString(), e2);
        } catch (Throwable th) {
            zzaom.setLenient(isLenient);
            throw th;
        }
    }

    public zzamv zztp(String str) throws zzane {
        return zza(new StringReader(str));
    }
}
