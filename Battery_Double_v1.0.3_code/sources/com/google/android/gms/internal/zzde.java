package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@zzin
public class zzde {
    final Context mContext;
    final String zzarj;
    String zzbdp;
    BlockingQueue<zzdk> zzbdr;
    ExecutorService zzbds;
    LinkedHashMap<String, String> zzbdt = new LinkedHashMap<>();
    Map<String, zzdh> zzbdu = new HashMap();
    private AtomicBoolean zzbdv;
    private File zzbdw;

    public zzde(Context context, String str, String str2, Map<String, String> map) {
        this.mContext = context;
        this.zzarj = str;
        this.zzbdp = str2;
        this.zzbdv = new AtomicBoolean(false);
        this.zzbdv.set(((Boolean) zzdc.zzazg.get()).booleanValue());
        if (this.zzbdv.get()) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null) {
                this.zzbdw = new File(externalStorageDirectory, "sdk_csi_data.txt");
            }
        }
        for (Entry entry : map.entrySet()) {
            this.zzbdt.put((String) entry.getKey(), (String) entry.getValue());
        }
        this.zzbdr = new ArrayBlockingQueue(30);
        this.zzbds = Executors.newSingleThreadExecutor();
        this.zzbds.execute(new Runnable() {
            public void run() {
                zzde.this.zzkc();
            }
        });
        this.zzbdu.put("action", zzdh.zzbdz);
        this.zzbdu.put("ad_format", zzdh.zzbdz);
        this.zzbdu.put("e", zzdh.zzbea);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x002b A[SYNTHETIC, Splitter:B:20:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0038 A[SYNTHETIC, Splitter:B:27:0x0038] */
    private void zzc(@Nullable File file, String str) {
        if (file != null) {
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file, true);
                try {
                    fileOutputStream2.write(str.getBytes());
                    fileOutputStream2.write(10);
                    if (fileOutputStream2 != null) {
                        try {
                            fileOutputStream2.close();
                        } catch (IOException e) {
                            zzkd.zzd("CsiReporter: Cannot close file: sdk_csi_data.txt.", e);
                        }
                    }
                } catch (IOException e2) {
                    e = e2;
                    fileOutputStream = fileOutputStream2;
                    try {
                        zzkd.zzd("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                        if (fileOutputStream != null) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e3) {
                                zzkd.zzd("CsiReporter: Cannot close file: sdk_csi_data.txt.", e3);
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    if (fileOutputStream != null) {
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                zzkd.zzd("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            }
        } else {
            zzkd.zzcx("CsiReporter: File doesn't exists. Cannot write CSI data to file.");
        }
    }

    private void zzc(Map<String, String> map, String str) {
        String zza = zza(this.zzbdp, map, str);
        if (this.zzbdv.get()) {
            zzc(this.zzbdw, zza);
        } else {
            zzu.zzfq().zzc(this.mContext, this.zzarj, zza);
        }
    }

    /* access modifiers changed from: private */
    public void zzkc() {
        while (true) {
            try {
                zzdk zzdk = (zzdk) this.zzbdr.take();
                String zzki = zzdk.zzki();
                if (!TextUtils.isEmpty(zzki)) {
                    zzc(zza(this.zzbdt, zzdk.zzm()), zzki);
                }
            } catch (InterruptedException e) {
                zzkd.zzd("CsiReporter:reporter interrupted", e);
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public String zza(String str, Map<String, String> map, @NonNull String str2) {
        Builder buildUpon = Uri.parse(str).buildUpon();
        for (Entry entry : map.entrySet()) {
            buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
        }
        StringBuilder sb = new StringBuilder(buildUpon.build().toString());
        sb.append("&");
        sb.append("it");
        sb.append("=");
        sb.append(str2);
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> zza(Map<String, String> map, @Nullable Map<String, String> map2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        if (map2 == null) {
            return linkedHashMap;
        }
        for (Entry entry : map2.entrySet()) {
            String str = (String) entry.getKey();
            String str2 = (String) linkedHashMap.get(str);
            linkedHashMap.put(str, zzaq(str).zzg(str2, (String) entry.getValue()));
        }
        return linkedHashMap;
    }

    public boolean zza(zzdk zzdk) {
        return this.zzbdr.offer(zzdk);
    }

    public zzdh zzaq(String str) {
        zzdh zzdh = (zzdh) this.zzbdu.get(str);
        return zzdh != null ? zzdh : zzdh.zzbdy;
    }

    public void zzc(@Nullable List<String> list) {
        if (list != null && !list.isEmpty()) {
            this.zzbdt.put("e", TextUtils.join(",", list));
        }
    }
}
