package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.BinderThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.signin.internal.SignInResponse;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public class zzpw implements zzpz {
    /* access modifiers changed from: private */
    public final Context mContext;
    private final com.google.android.gms.common.api.Api.zza<? extends zzvu, zzvv> si;
    private ConnectionResult tA;
    private int tB;
    private int tC = 0;
    private int tD;
    private final Bundle tE = new Bundle();
    private final Set<com.google.android.gms.common.api.Api.zzc> tF = new HashSet();
    /* access modifiers changed from: private */
    public zzvu tG;
    private int tH;
    /* access modifiers changed from: private */
    public boolean tI;
    private boolean tJ;
    /* access modifiers changed from: private */
    public zzq tK;
    private boolean tL;
    private boolean tM;
    private final zzg tN;
    private final Map<Api<?>, Integer> tO;
    private ArrayList<Future<?>> tP = new ArrayList<>();
    /* access modifiers changed from: private */
    public final Lock tr;
    /* access modifiers changed from: private */
    public final zzqa tw;
    /* access modifiers changed from: private */
    public final com.google.android.gms.common.zzc tz;

    private static class zza implements com.google.android.gms.common.internal.zzd.zzf {
        private final Api<?> pN;
        private final WeakReference<zzpw> tR;
        /* access modifiers changed from: private */
        public final int tf;

        public zza(zzpw zzpw, Api<?> api, int i) {
            this.tR = new WeakReference<>(zzpw);
            this.pN = api;
            this.tf = i;
        }

        public void zzh(@NonNull ConnectionResult connectionResult) {
            zzpw zzpw = (zzpw) this.tR.get();
            if (zzpw != null) {
                zzab.zza(Looper.myLooper() == zzpw.tw.th.getLooper(), (Object) "onReportServiceBinding must be called on the GoogleApiClient handler thread");
                zzpw.tr.lock();
                try {
                    if (zzpw.zzfi(0)) {
                        if (!connectionResult.isSuccess()) {
                            zzpw.zzb(connectionResult, this.pN, this.tf);
                        }
                        if (zzpw.zzapm()) {
                            zzpw.zzapn();
                        }
                        zzpw.tr.unlock();
                    }
                } finally {
                    zzpw.tr.unlock();
                }
            }
        }
    }

    private class zzb extends zzf {
        private final Map<com.google.android.gms.common.api.Api.zze, zza> tS;

        public zzb(Map<com.google.android.gms.common.api.Api.zze, zza> map) {
            super();
            this.tS = map;
        }

        @WorkerThread
        public void zzapl() {
            boolean z;
            Iterator it = this.tS.keySet().iterator();
            boolean z2 = true;
            int i = 0;
            boolean z3 = true;
            boolean z4 = false;
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    z2 = z4;
                    break;
                }
                com.google.android.gms.common.api.Api.zze zze = (com.google.android.gms.common.api.Api.zze) it.next();
                if (!zze.zzanu()) {
                    z3 = false;
                } else if (((zza) this.tS.get(zze)).tf == 0) {
                    z = true;
                    break;
                } else {
                    z4 = true;
                }
            }
            if (z2) {
                i = zzpw.this.tz.isGooglePlayServicesAvailable(zzpw.this.mContext);
            }
            if (i == 0 || (!z && !z3)) {
                if (zzpw.this.tI) {
                    zzpw.this.tG.connect();
                }
                for (com.google.android.gms.common.api.Api.zze zze2 : this.tS.keySet()) {
                    final com.google.android.gms.common.internal.zzd.zzf zzf = (com.google.android.gms.common.internal.zzd.zzf) this.tS.get(zze2);
                    if (!zze2.zzanu() || i == 0) {
                        zze2.zza(zzf);
                    } else {
                        zzpw.this.tw.zza((zza) new zza(zzpw.this) {
                            public void zzapl() {
                                zzf.zzh(new ConnectionResult(16, null));
                            }
                        });
                    }
                }
                return;
            }
            final ConnectionResult connectionResult = new ConnectionResult(i, null);
            zzpw.this.tw.zza((zza) new zza(zzpw.this) {
                public void zzapl() {
                    zzpw.this.zzg(connectionResult);
                }
            });
        }
    }

    private class zzc extends zzf {
        private final ArrayList<com.google.android.gms.common.api.Api.zze> tW;

        public zzc(ArrayList<com.google.android.gms.common.api.Api.zze> arrayList) {
            super();
            this.tW = arrayList;
        }

        @WorkerThread
        public void zzapl() {
            zzpw.this.tw.th.uj = zzpw.this.zzaps();
            Iterator it = this.tW.iterator();
            while (it.hasNext()) {
                ((com.google.android.gms.common.api.Api.zze) it.next()).zza(zzpw.this.tK, zzpw.this.tw.th.uj);
            }
        }
    }

    private static class zzd extends com.google.android.gms.signin.internal.zzb {
        private final WeakReference<zzpw> tR;

        zzd(zzpw zzpw) {
            this.tR = new WeakReference<>(zzpw);
        }

        @BinderThread
        public void zzb(final SignInResponse signInResponse) {
            final zzpw zzpw = (zzpw) this.tR.get();
            if (zzpw != null) {
                zzpw.tw.zza((zza) new zza(zzpw) {
                    public void zzapl() {
                        zzpw.zza(signInResponse);
                    }
                });
            }
        }
    }

    private class zze implements ConnectionCallbacks, OnConnectionFailedListener {
        private zze() {
        }

        public void onConnected(Bundle bundle) {
            zzpw.this.tG.zza(new zzd(zzpw.this));
        }

        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            zzpw.this.tr.lock();
            try {
                if (zzpw.this.zzf(connectionResult)) {
                    zzpw.this.zzapq();
                    zzpw.this.zzapn();
                } else {
                    zzpw.this.zzg(connectionResult);
                }
            } finally {
                zzpw.this.tr.unlock();
            }
        }

        public void onConnectionSuspended(int i) {
        }
    }

    private abstract class zzf implements Runnable {
        private zzf() {
        }

        @WorkerThread
        public void run() {
            zzpw.this.tr.lock();
            try {
                if (Thread.interrupted()) {
                    zzpw.this.tr.unlock();
                    return;
                }
                zzapl();
                zzpw.this.tr.unlock();
            } catch (RuntimeException e) {
                zzpw.this.tw.zza(e);
            } catch (Throwable th) {
                zzpw.this.tr.unlock();
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        @WorkerThread
        public abstract void zzapl();
    }

    public zzpw(zzqa zzqa, zzg zzg, Map<Api<?>, Integer> map, com.google.android.gms.common.zzc zzc2, com.google.android.gms.common.api.Api.zza<? extends zzvu, zzvv> zza2, Lock lock, Context context) {
        this.tw = zzqa;
        this.tN = zzg;
        this.tO = map;
        this.tz = zzc2;
        this.si = zza2;
        this.tr = lock;
        this.mContext = context;
    }

    /* access modifiers changed from: private */
    public void zza(SignInResponse signInResponse) {
        if (zzfi(0)) {
            ConnectionResult zzath = signInResponse.zzath();
            if (zzath.isSuccess()) {
                ResolveAccountResponse zzbzz = signInResponse.zzbzz();
                ConnectionResult zzath2 = zzbzz.zzath();
                if (!zzath2.isSuccess()) {
                    String valueOf = String.valueOf(zzath2);
                    StringBuilder sb = new StringBuilder(48 + String.valueOf(valueOf).length());
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    zzg(zzath2);
                    return;
                }
                this.tJ = true;
                this.tK = zzbzz.zzatg();
                this.tL = zzbzz.zzati();
                this.tM = zzbzz.zzatj();
            } else if (zzf(zzath)) {
                zzapq();
            } else {
                zzg(zzath);
                return;
            }
            zzapn();
        }
    }

    private boolean zza(int i, int i2, ConnectionResult connectionResult) {
        boolean z = false;
        if (i2 == 1 && !zze(connectionResult)) {
            return false;
        }
        if (this.tA == null || i < this.tB) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public boolean zzapm() {
        ConnectionResult connectionResult;
        this.tD--;
        if (this.tD > 0) {
            return false;
        }
        if (this.tD < 0) {
            Log.w("GoogleApiClientConnecting", this.tw.th.zzapy());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            connectionResult = new ConnectionResult(8, null);
        } else if (this.tA == null) {
            return true;
        } else {
            this.tw.uA = this.tB;
            connectionResult = this.tA;
        }
        zzg(connectionResult);
        return false;
    }

    /* access modifiers changed from: private */
    public void zzapn() {
        if (this.tD == 0) {
            if (!this.tI || this.tJ) {
                zzapo();
            }
        }
    }

    private void zzapo() {
        ArrayList arrayList = new ArrayList();
        this.tC = 1;
        this.tD = this.tw.ui.size();
        for (com.google.android.gms.common.api.Api.zzc zzc2 : this.tw.ui.keySet()) {
            if (!this.tw.ux.containsKey(zzc2)) {
                arrayList.add((com.google.android.gms.common.api.Api.zze) this.tw.ui.get(zzc2));
            } else if (zzapm()) {
                zzapp();
            }
        }
        if (!arrayList.isEmpty()) {
            this.tP.add(zzqb.zzaqc().submit(new zzc(arrayList)));
        }
    }

    private void zzapp() {
        this.tw.zzaqa();
        zzqb.zzaqc().execute(new Runnable() {
            public void run() {
                zzpw.this.tz.zzbp(zzpw.this.mContext);
            }
        });
        if (this.tG != null) {
            if (this.tL) {
                this.tG.zza(this.tK, this.tM);
            }
            zzbm(false);
        }
        for (com.google.android.gms.common.api.Api.zzc zzc2 : this.tw.ux.keySet()) {
            ((com.google.android.gms.common.api.Api.zze) this.tw.ui.get(zzc2)).disconnect();
        }
        this.tw.uB.zzm(this.tE.isEmpty() ? null : this.tE);
    }

    /* access modifiers changed from: private */
    public void zzapq() {
        this.tI = false;
        this.tw.th.uj = Collections.emptySet();
        for (com.google.android.gms.common.api.Api.zzc zzc2 : this.tF) {
            if (!this.tw.ux.containsKey(zzc2)) {
                this.tw.ux.put(zzc2, new ConnectionResult(17, null));
            }
        }
    }

    private void zzapr() {
        Iterator it = this.tP.iterator();
        while (it.hasNext()) {
            ((Future) it.next()).cancel(true);
        }
        this.tP.clear();
    }

    /* access modifiers changed from: private */
    public Set<Scope> zzaps() {
        if (this.tN == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.tN.zzasj());
        Map zzasl = this.tN.zzasl();
        for (Api api : zzasl.keySet()) {
            if (!this.tw.ux.containsKey(api.zzans())) {
                hashSet.addAll(((com.google.android.gms.common.internal.zzg.zza) zzasl.get(api)).dT);
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    public void zzb(ConnectionResult connectionResult, Api<?> api, int i) {
        if (i != 2) {
            int priority = api.zzanp().getPriority();
            if (zza(priority, i, connectionResult)) {
                this.tA = connectionResult;
                this.tB = priority;
            }
        }
        this.tw.ux.put(api.zzans(), connectionResult);
    }

    private void zzbm(boolean z) {
        if (this.tG != null) {
            if (this.tG.isConnected() && z) {
                this.tG.zzbzo();
            }
            this.tG.disconnect();
            this.tK = null;
        }
    }

    private boolean zze(ConnectionResult connectionResult) {
        return connectionResult.hasResolution() || this.tz.zzfc(connectionResult.getErrorCode()) != null;
    }

    /* access modifiers changed from: private */
    public boolean zzf(ConnectionResult connectionResult) {
        boolean z = true;
        if (this.tH != 2) {
            if (this.tH == 1 && !connectionResult.hasResolution()) {
                return true;
            }
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public boolean zzfi(int i) {
        if (this.tC == i) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.tw.th.zzapy());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(23 + String.valueOf(valueOf).length());
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GoogleApiClientConnecting", sb.toString());
        int i2 = this.tD;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i2);
        Log.w("GoogleApiClientConnecting", sb2.toString());
        String valueOf2 = String.valueOf(zzfj(this.tC));
        String valueOf3 = String.valueOf(zzfj(i));
        StringBuilder sb3 = new StringBuilder(70 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(valueOf2);
        sb3.append(" but received callback for step ");
        sb3.append(valueOf3);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        zzg(new ConnectionResult(8, null));
        return false;
    }

    private String zzfj(int i) {
        switch (i) {
            case 0:
                return "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
            case 1:
                return "STEP_GETTING_REMOTE_SERVICE";
            default:
                return "UNKNOWN";
        }
    }

    /* access modifiers changed from: private */
    public void zzg(ConnectionResult connectionResult) {
        zzapr();
        zzbm(!connectionResult.hasResolution());
        this.tw.zzi(connectionResult);
        this.tw.uB.zzd(connectionResult);
    }

    public void begin() {
        this.tw.ux.clear();
        this.tI = false;
        this.tA = null;
        this.tC = 0;
        this.tH = 2;
        this.tJ = false;
        this.tL = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (Api api : this.tO.keySet()) {
            com.google.android.gms.common.api.Api.zze zze2 = (com.google.android.gms.common.api.Api.zze) this.tw.ui.get(api.zzans());
            int intValue = ((Integer) this.tO.get(api)).intValue();
            z |= api.zzanp().getPriority() == 1;
            if (zze2.zzafk()) {
                this.tI = true;
                if (intValue < this.tH) {
                    this.tH = intValue;
                }
                if (intValue != 0) {
                    this.tF.add(api.zzans());
                }
            }
            hashMap.put(zze2, new zza(this, api, intValue));
        }
        if (z) {
            this.tI = false;
        }
        if (this.tI) {
            this.tN.zzc(Integer.valueOf(this.tw.th.getSessionId()));
            zze zze3 = new zze();
            this.tG = (zzvu) this.si.zza(this.mContext, this.tw.th.getLooper(), this.tN, this.tN.zzasp(), zze3, zze3);
        }
        this.tD = this.tw.ui.size();
        this.tP.add(zzqb.zzaqc().submit(new zzb(hashMap)));
    }

    public void connect() {
    }

    public boolean disconnect() {
        zzapr();
        zzbm(true);
        this.tw.zzi(null);
        return true;
    }

    public void onConnected(Bundle bundle) {
        if (zzfi(1)) {
            if (bundle != null) {
                this.tE.putAll(bundle);
            }
            if (zzapm()) {
                zzapp();
            }
        }
    }

    public void onConnectionSuspended(int i) {
        zzg(new ConnectionResult(8, null));
    }

    public void zza(ConnectionResult connectionResult, Api<?> api, int i) {
        if (zzfi(1)) {
            zzb(connectionResult, api, i);
            if (zzapm()) {
                zzapp();
            }
        }
    }

    public <A extends com.google.android.gms.common.api.Api.zzb, R extends Result, T extends com.google.android.gms.internal.zzpm.zza<R, A>> T zzc(T t) {
        this.tw.th.uc.add(t);
        return t;
    }

    public <A extends com.google.android.gms.common.api.Api.zzb, T extends com.google.android.gms.internal.zzpm.zza<? extends Result, A>> T zzd(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }
}
