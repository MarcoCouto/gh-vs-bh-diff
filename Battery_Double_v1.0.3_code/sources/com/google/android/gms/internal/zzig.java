package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.zzic.zza;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;

@zzin
public class zzig extends zzkc {
    private final Object zzail;
    /* access modifiers changed from: private */
    public final zza zzbxq;
    private final zzju.zza zzbxr;
    private final AdResponseParcel zzbxs;
    private final zzii zzbyq;
    private Future<zzju> zzbyr;

    public zzig(Context context, zzq zzq, zzju.zza zza, zzas zzas, zza zza2) {
        zzii zzii = new zzii(context, zzq, new zzkn(context), zzas, zza);
        this(zza, zza2, zzii);
    }

    zzig(zzju.zza zza, zza zza2, zzii zzii) {
        this.zzail = new Object();
        this.zzbxr = zza;
        this.zzbxs = zza.zzciq;
        this.zzbxq = zza2;
        this.zzbyq = zzii;
    }

    private zzju zzam(int i) {
        AdRequestParcel adRequestParcel = this.zzbxr.zzcip.zzcar;
        int i2 = this.zzbxs.orientation;
        long j = this.zzbxs.zzbns;
        String str = this.zzbxr.zzcip.zzcau;
        long j2 = this.zzbxs.zzcbz;
        AdSizeParcel adSizeParcel = this.zzbxr.zzapa;
        long j3 = this.zzbxs.zzcbx;
        long j4 = this.zzbxr.zzcik;
        long j5 = j2;
        long j6 = this.zzbxs.zzccc;
        String str2 = this.zzbxs.zzccd;
        JSONObject jSONObject = this.zzbxr.zzcie;
        JSONObject jSONObject2 = jSONObject;
        int i3 = i;
        AdSizeParcel adSizeParcel2 = adSizeParcel;
        long j7 = j5;
        long j8 = j3;
        long j9 = j4;
        long j10 = j6;
        String str3 = str2;
        zzju zzju = new zzju(adRequestParcel, null, null, i3, null, null, i2, j, str, false, null, null, null, null, null, j7, adSizeParcel2, j8, j9, j10, str3, jSONObject2, null, null, null, null, this.zzbxr.zzciq.zzccq, this.zzbxr.zzciq.zzccr, null, null);
        return zzju;
    }

    public void onStop() {
        synchronized (this.zzail) {
            if (this.zzbyr != null) {
                this.zzbyr.cancel(true);
            }
        }
    }

    public void zzew() {
        int i = 0;
        final zzju zzju = null;
        try {
            synchronized (this.zzail) {
                this.zzbyr = zzkg.zza((Callable<T>) this.zzbyq);
            }
            i = -2;
            zzju = (zzju) this.zzbyr.get(60000, TimeUnit.MILLISECONDS);
        } catch (TimeoutException unused) {
            zzkd.zzcx("Timed out waiting for native ad.");
            i = 2;
            this.zzbyr.cancel(true);
        } catch (InterruptedException | CancellationException | ExecutionException unused2) {
        }
        if (zzju == null) {
            zzju = zzam(i);
        }
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzig.this.zzbxq.zzb(zzju);
            }
        });
    }
}
