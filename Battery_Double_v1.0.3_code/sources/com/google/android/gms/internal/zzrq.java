package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.config.internal.zza;
import com.google.android.gms.config.internal.zzc;

public final class zzrq {
    public static final Api<NoOptions> API = new Api<>("Config.API", bK, bJ);
    public static final zzrr Bw = new zza();
    public static final zzf<zzc> bJ = new zzf<>();
    static final Api.zza<zzc, NoOptions> bK = new Api.zza<zzc, NoOptions>() {
        /* renamed from: zzg */
        public zzc zza(Context context, Looper looper, zzg zzg, NoOptions noOptions, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
            zzc zzc = new zzc(context, looper, zzg, connectionCallbacks, onConnectionFailedListener);
            return zzc;
        }
    };
}
