package com.google.android.gms.internal;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

public final class zzano {
    static final Type[] bew = new Type[0];

    private static final class zza implements Serializable, GenericArrayType {
        private final Type bex;

        public zza(Type type) {
            this.bex = zzano.zze(type);
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && zzano.zza((Type) this, (Type) (GenericArrayType) obj);
        }

        public Type getGenericComponentType() {
            return this.bex;
        }

        public int hashCode() {
            return this.bex.hashCode();
        }

        public String toString() {
            return String.valueOf(zzano.zzg(this.bex)).concat("[]");
        }
    }

    private static final class zzb implements Serializable, ParameterizedType {
        private final Type[] beA;
        private final Type bey;
        private final Type bez;

        public zzb(Type type, Type type2, Type... typeArr) {
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                boolean z = true;
                boolean z2 = Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null;
                if (type == null && !z2) {
                    z = false;
                }
                zzann.zzbo(z);
            }
            this.bey = type == null ? null : zzano.zze(type);
            this.bez = zzano.zze(type2);
            this.beA = (Type[]) typeArr.clone();
            for (int i = 0; i < this.beA.length; i++) {
                zzann.zzy(this.beA[i]);
                zzano.zzi(this.beA[i]);
                this.beA[i] = zzano.zze(this.beA[i]);
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && zzano.zza((Type) this, (Type) (ParameterizedType) obj);
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.beA.clone();
        }

        public Type getOwnerType() {
            return this.bey;
        }

        public Type getRawType() {
            return this.bez;
        }

        public int hashCode() {
            return (Arrays.hashCode(this.beA) ^ this.bez.hashCode()) ^ zzano.zzck(this.bey);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(30 * (this.beA.length + 1));
            sb.append(zzano.zzg(this.bez));
            if (this.beA.length == 0) {
                return sb.toString();
            }
            sb.append("<");
            sb.append(zzano.zzg(this.beA[0]));
            for (int i = 1; i < this.beA.length; i++) {
                sb.append(", ");
                sb.append(zzano.zzg(this.beA[i]));
            }
            sb.append(">");
            return sb.toString();
        }
    }

    private static final class zzc implements Serializable, WildcardType {
        private final Type beB;
        private final Type beC;

        public zzc(Type[] typeArr, Type[] typeArr2) {
            Type type;
            boolean z = true;
            zzann.zzbo(typeArr2.length <= 1);
            zzann.zzbo(typeArr.length == 1);
            if (typeArr2.length == 1) {
                zzann.zzy(typeArr2[0]);
                zzano.zzi(typeArr2[0]);
                if (typeArr[0] != Object.class) {
                    z = false;
                }
                zzann.zzbo(z);
                this.beC = zzano.zze(typeArr2[0]);
                type = Object.class;
            } else {
                zzann.zzy(typeArr[0]);
                zzano.zzi(typeArr[0]);
                this.beC = null;
                type = zzano.zze(typeArr[0]);
            }
            this.beB = type;
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && zzano.zza((Type) this, (Type) (WildcardType) obj);
        }

        public Type[] getLowerBounds() {
            if (this.beC == null) {
                return zzano.bew;
            }
            return new Type[]{this.beC};
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.beB};
        }

        public int hashCode() {
            return (this.beC != null ? this.beC.hashCode() + 31 : 1) ^ (31 + this.beB.hashCode());
        }

        public String toString() {
            if (this.beC != null) {
                String str = "? super ";
                String valueOf = String.valueOf(zzano.zzg(this.beC));
                return valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
            } else if (this.beB == Object.class) {
                return "?";
            } else {
                String str2 = "? extends ";
                String valueOf2 = String.valueOf(zzano.zzg(this.beB));
                return valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2);
            }
        }
    }

    static boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    private static int zza(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    private static Class<?> zza(TypeVariable<?> typeVariable) {
        GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    public static ParameterizedType zza(Type type, Type type2, Type... typeArr) {
        return new zzb(type, type2, typeArr);
    }

    public static Type zza(Type type, Class<?> cls) {
        Type zzb2 = zzb(type, cls, Collection.class);
        if (zzb2 instanceof WildcardType) {
            zzb2 = ((WildcardType) zzb2).getUpperBounds()[0];
        }
        return zzb2 instanceof ParameterizedType ? ((ParameterizedType) zzb2).getActualTypeArguments()[0] : Object.class;
    }

    static Type zza(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return zza(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<?> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return zza(cls.getGenericSuperclass(), superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    public static Type zza(Type type, Class<?> cls, Type type2) {
        while (true) {
            boolean z = r10 instanceof TypeVariable;
            r10 = type2;
            if (z) {
                TypeVariable typeVariable = (TypeVariable) r10;
                Type zza2 = zza(type, cls, typeVariable);
                if (zza2 == typeVariable) {
                    return zza2;
                }
                r10 = zza2;
            } else {
                if (r10 instanceof Class) {
                    Class cls2 = (Class) r10;
                    if (cls2.isArray()) {
                        Class componentType = cls2.getComponentType();
                        Type zza3 = zza(type, cls, (Type) componentType);
                        return componentType == zza3 ? cls2 : zzb(zza3);
                    }
                }
                if (r10 instanceof GenericArrayType) {
                    GenericArrayType genericArrayType = (GenericArrayType) r10;
                    Type genericComponentType = genericArrayType.getGenericComponentType();
                    Type zza4 = zza(type, cls, genericComponentType);
                    return genericComponentType == zza4 ? genericArrayType : zzb(zza4);
                }
                if (r10 instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) r10;
                    Type ownerType = parameterizedType.getOwnerType();
                    Type zza5 = zza(type, cls, ownerType);
                    boolean z2 = zza5 != ownerType;
                    Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                    int length = actualTypeArguments.length;
                    for (int i = 0; i < length; i++) {
                        Type zza6 = zza(type, cls, actualTypeArguments[i]);
                        if (zza6 != actualTypeArguments[i]) {
                            if (!z2) {
                                actualTypeArguments = (Type[]) actualTypeArguments.clone();
                                z2 = true;
                            }
                            actualTypeArguments[i] = zza6;
                        }
                    }
                    if (z2) {
                        parameterizedType = zza(zza5, parameterizedType.getRawType(), actualTypeArguments);
                    }
                    return parameterizedType;
                }
                if (r10 instanceof WildcardType) {
                    r10 = r10;
                    Type[] lowerBounds = r10.getLowerBounds();
                    Type[] upperBounds = r10.getUpperBounds();
                    if (lowerBounds.length == 1) {
                        Type zza7 = zza(type, cls, lowerBounds[0]);
                        if (zza7 != lowerBounds[0]) {
                            return zzd(zza7);
                        }
                    } else if (upperBounds.length == 1) {
                        Type zza8 = zza(type, cls, upperBounds[0]);
                        if (zza8 != upperBounds[0]) {
                            return zzc(zza8);
                        }
                    }
                }
                return r10;
            }
        }
    }

    static Type zza(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class zza2 = zza(typeVariable);
        if (zza2 == null) {
            return typeVariable;
        }
        Type zza3 = zza(type, cls, zza2);
        if (!(zza3 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) zza3).getActualTypeArguments()[zza((Object[]) zza2.getTypeParameters(), (Object) typeVariable)];
    }

    public static boolean zza(Type type, Type type2) {
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            return equal(parameterizedType.getOwnerType(), parameterizedType2.getOwnerType()) && parameterizedType.getRawType().equals(parameterizedType2.getRawType()) && Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            if (!(type2 instanceof GenericArrayType)) {
                return false;
            }
            return zza(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            return Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) && Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds());
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            return typeVariable.getGenericDeclaration() == typeVariable2.getGenericDeclaration() && typeVariable.getName().equals(typeVariable2.getName());
        }
    }

    public static GenericArrayType zzb(Type type) {
        return new zza(type);
    }

    static Type zzb(Type type, Class<?> cls, Class<?> cls2) {
        zzann.zzbo(cls2.isAssignableFrom(cls));
        return zza(type, cls, zza(type, cls, cls2));
    }

    public static Type[] zzb(Type type, Class<?> cls) {
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type zzb2 = zzb(type, cls, Map.class);
        if (zzb2 instanceof ParameterizedType) {
            return ((ParameterizedType) zzb2).getActualTypeArguments();
        }
        return new Type[]{Object.class, Object.class};
    }

    public static WildcardType zzc(Type type) {
        return new zzc(new Type[]{type}, bew);
    }

    /* access modifiers changed from: private */
    public static int zzck(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public static WildcardType zzd(Type type) {
        return new zzc(new Type[]{Object.class}, new Type[]{type});
    }

    /* JADX WARNING: type inference failed for: r3v8 */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.google.android.gms.internal.zzano$zza] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static Type zze(Type type) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (cls.isArray()) {
                cls = new zza(zze(cls.getComponentType()));
            }
            return (Type) cls;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new zzb(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new zza(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new zzc(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    public static Class<?> zzf(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            zzann.zzbo(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(zzf(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return zzf(((WildcardType) type).getUpperBounds()[0]);
            }
            String name = type == null ? "null" : type.getClass().getName();
            String valueOf = String.valueOf("Expected a Class, ParameterizedType, or GenericArrayType, but <");
            String valueOf2 = String.valueOf(type);
            StringBuilder sb = new StringBuilder(13 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(name).length());
            sb.append(valueOf);
            sb.append(valueOf2);
            sb.append("> is of type ");
            sb.append(name);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public static String zzg(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    public static Type zzh(Type type) {
        return type instanceof GenericArrayType ? ((GenericArrayType) type).getGenericComponentType() : ((Class) type).getComponentType();
    }

    /* access modifiers changed from: private */
    public static void zzi(Type type) {
        zzann.zzbo(!(type instanceof Class) || !((Class) type).isPrimitive());
    }
}
