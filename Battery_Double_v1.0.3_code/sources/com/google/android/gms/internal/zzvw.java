package com.google.android.gms.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.stats.zzf;
import com.google.android.gms.common.stats.zzh;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.common.util.zzw;
import com.google.android.gms.common.util.zzz;

public class zzvw {
    private static boolean DEBUG = false;
    private static String TAG = "WakeLock";
    private static String auf = "*gcore*:";
    private final String AK;
    private final String AM;
    private WorkSource aaW;
    private final WakeLock aug;
    private final int auh;
    private final String aui;
    private boolean auj;
    private int auk;
    private int aul;
    private final Context mContext;

    public zzvw(Context context, int i, String str) {
        this(context, i, str, null, context == null ? null : context.getPackageName());
    }

    @SuppressLint({"UnwrappedWakeLock"})
    public zzvw(Context context, int i, String str, String str2, String str3) {
        this(context, i, str, str2, str3, null);
    }

    @SuppressLint({"UnwrappedWakeLock"})
    public zzvw(Context context, int i, String str, String str2, String str3, String str4) {
        this.auj = true;
        zzab.zzh(str, "Wake lock name can NOT be empty");
        this.auh = i;
        this.aui = str2;
        this.AM = str4;
        this.mContext = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(auf);
            String valueOf2 = String.valueOf(str);
            this.AK = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        } else {
            this.AK = str;
        }
        this.aug = ((PowerManager) context.getSystemService("power")).newWakeLock(i, str);
        if (zzz.zzco(this.mContext)) {
            if (zzw.zzib(str3)) {
                str3 = context.getPackageName();
            }
            this.aaW = zzz.zzr(context, str3);
            zzc(this.aaW);
        }
    }

    private void zzd(WorkSource workSource) {
        try {
            this.aug.setWorkSource(workSource);
        } catch (IllegalArgumentException e) {
            Log.wtf(TAG, e.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r12.aul == 0) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r0 == false) goto L_0x0017;
     */
    private void zzl(String str, long j) {
        boolean zznz = zznz(str);
        String zzp = zzp(str, zznz);
        synchronized (this) {
            if (this.auj) {
                int i = this.auk;
                this.auk = i + 1;
                if (i != 0) {
                }
                zzh.zzavi().zza(this.mContext, zzf.zza(this.aug, zzp), 7, this.AK, zzp, this.AM, this.auh, zzz.zzb(this.aaW), j);
                this.aul++;
            }
            if (!this.auj) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r11.aul == 1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r0 == false) goto L_0x0017;
     */
    private void zzny(String str) {
        boolean zznz = zznz(str);
        String zzp = zzp(str, zznz);
        synchronized (this) {
            if (this.auj) {
                int i = this.auk - 1;
                this.auk = i;
                if (i != 0) {
                }
                zzh.zzavi().zza(this.mContext, zzf.zza(this.aug, zzp), 8, this.AK, zzp, this.AM, this.auh, zzz.zzb(this.aaW));
                this.aul--;
            }
            if (!this.auj) {
            }
        }
    }

    private boolean zznz(String str) {
        return !TextUtils.isEmpty(str) && !str.equals(this.aui);
    }

    private String zzp(String str, boolean z) {
        return (!this.auj || !z) ? this.aui : str;
    }

    public void acquire(long j) {
        if (!zzs.zzavq() && this.auj) {
            String str = TAG;
            String str2 = "Do not acquire with timeout on reference counted WakeLocks before ICS. wakelock: ";
            String valueOf = String.valueOf(this.AK);
            Log.wtf(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
        zzl(null, j);
        this.aug.acquire(j);
    }

    public boolean isHeld() {
        return this.aug.isHeld();
    }

    public void release() {
        zzny(null);
        this.aug.release();
    }

    public void setReferenceCounted(boolean z) {
        this.aug.setReferenceCounted(z);
        this.auj = z;
    }

    public void zzc(WorkSource workSource) {
        if (workSource != null && zzz.zzco(this.mContext)) {
            if (this.aaW != null) {
                this.aaW.add(workSource);
            } else {
                this.aaW = workSource;
            }
            zzd(this.aaW);
        }
    }
}
