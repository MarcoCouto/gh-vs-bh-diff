package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzaz extends zzbp {
    public zzaz(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.zzaha) {
            this.zzaha.zzcu = Long.valueOf(-1);
            this.zzaha.zzcu = (Long) this.zzahh.invoke(null, new Object[]{this.zzaey.getContext()});
        }
    }
}
