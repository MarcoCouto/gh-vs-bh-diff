package com.google.android.gms.internal;

public final class zztz {
    private static zztz OT;
    private final zztw OU = new zztw();
    private final zztx OV = new zztx();

    static {
        zza(new zztz());
    }

    private zztz() {
    }

    protected static void zza(zztz zztz) {
        synchronized (zztz.class) {
            OT = zztz;
        }
    }

    private static zztz zzbes() {
        zztz zztz;
        synchronized (zztz.class) {
            zztz = OT;
        }
        return zztz;
    }

    public static zztw zzbet() {
        return zzbes().OU;
    }

    public static zztx zzbeu() {
        return zzbes().OV;
    }
}
