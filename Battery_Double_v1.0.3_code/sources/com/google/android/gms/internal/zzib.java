package com.google.android.gms.internal;

import android.content.Context;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.request.AdResponseParcel;

@zzin
public abstract class zzib extends zzkc {
    protected final Context mContext;
    protected final Object zzail = new Object();
    protected final com.google.android.gms.internal.zzic.zza zzbxq;
    protected final com.google.android.gms.internal.zzju.zza zzbxr;
    protected AdResponseParcel zzbxs;
    protected final Object zzbxu = new Object();

    protected static final class zza extends Exception {
        private final int zzbyi;

        public zza(String str, int i) {
            super(str);
            this.zzbyi = i;
        }

        public int getErrorCode() {
            return this.zzbyi;
        }
    }

    protected zzib(Context context, com.google.android.gms.internal.zzju.zza zza2, com.google.android.gms.internal.zzic.zza zza3) {
        super(true);
        this.mContext = context;
        this.zzbxr = zza2;
        this.zzbxs = zza2.zzciq;
        this.zzbxq = zza3;
    }

    public void onStop() {
    }

    /* access modifiers changed from: protected */
    public abstract zzju zzak(int i);

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[Catch:{ zza -> 0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[Catch:{ zza -> 0x0014 }] */
    public void zzew() {
        synchronized (this.zzail) {
            zzkd.zzcv("AdRendererBackgroundTask started.");
            int i = this.zzbxr.errorCode;
            try {
                zzh(SystemClock.elapsedRealtime());
            } catch (zza e) {
                int errorCode = e.getErrorCode();
                if (errorCode != 3) {
                    if (errorCode != -1) {
                        zzkd.zzcx(e.getMessage());
                        this.zzbxs = this.zzbxs != null ? new AdResponseParcel(errorCode) : new AdResponseParcel(errorCode, this.zzbxs.zzbns);
                        zzkh.zzclc.post(new Runnable() {
                            public void run() {
                                zzib.this.onStop();
                            }
                        });
                        i = errorCode;
                    }
                }
                zzkd.zzcw(e.getMessage());
                this.zzbxs = this.zzbxs != null ? new AdResponseParcel(errorCode) : new AdResponseParcel(errorCode, this.zzbxs.zzbns);
                zzkh.zzclc.post(new Runnable() {
                    public void run() {
                        zzib.this.onStop();
                    }
                });
                i = errorCode;
            }
            final zzju zzak = zzak(i);
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    synchronized (zzib.this.zzail) {
                        zzib.this.zzm(zzak);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzh(long j) throws zza;

    /* access modifiers changed from: protected */
    public void zzm(zzju zzju) {
        this.zzbxq.zzb(zzju);
    }
}
