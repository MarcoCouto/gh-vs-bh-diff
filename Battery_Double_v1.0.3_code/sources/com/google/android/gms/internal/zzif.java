package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.zzn;
import com.google.android.gms.internal.zzju.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@zzin
public class zzif extends zzib {
    private final zzdk zzajn;
    private zzgj zzajz;
    /* access modifiers changed from: private */
    public final zzlh zzbgf;
    private zzga zzboe;
    zzfy zzbym;
    protected zzge zzbyn;
    /* access modifiers changed from: private */
    public boolean zzbyo;

    zzif(Context context, zza zza, zzgj zzgj, zzic.zza zza2, zzdk zzdk, zzlh zzlh) {
        super(context, zza, zza2);
        this.zzajz = zzgj;
        this.zzboe = zza.zzcig;
        this.zzajn = zzdk;
        this.zzbgf = zzlh;
    }

    private static String zza(zzge zzge) {
        String str = zzge.zzbon.zzbmx;
        int zzal = zzal(zzge.zzbom);
        long j = zzge.zzbos;
        StringBuilder sb = new StringBuilder(33 + String.valueOf(str).length());
        sb.append(str);
        sb.append(".");
        sb.append(zzal);
        sb.append(".");
        sb.append(j);
        return sb.toString();
    }

    private static int zzal(int i) {
        switch (i) {
            case -1:
                return 4;
            case 0:
                return 0;
            case 1:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 5;
            default:
                return 6;
        }
    }

    private static String zzg(List<zzge> list) {
        String str = "";
        if (list == null) {
            return str.toString();
        }
        for (zzge zzge : list) {
            if (!(zzge == null || zzge.zzbon == null || TextUtils.isEmpty(zzge.zzbon.zzbmx))) {
                String valueOf = String.valueOf(str);
                String valueOf2 = String.valueOf(zza(zzge));
                StringBuilder sb = new StringBuilder(1 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
                sb.append(valueOf);
                sb.append(valueOf2);
                sb.append("_");
                str = sb.toString();
            }
        }
        return str.substring(0, Math.max(0, str.length() - 1));
    }

    private void zzqf() throws zza {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                synchronized (zzif.this.zzbxu) {
                    zzif.this.zzbyo = zzn.zza(zzif.this.zzbgf, zzif.this.zzbyn, countDownLatch);
                }
            }
        });
        try {
            countDownLatch.await(10, TimeUnit.SECONDS);
            synchronized (this.zzbxu) {
                if (!this.zzbyo) {
                    throw new zza("View could not be prepared", 0);
                } else if (this.zzbgf.isDestroyed()) {
                    throw new zza("Assets not loaded, web view is destroyed", 0);
                }
            }
        } catch (InterruptedException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(38 + String.valueOf(valueOf).length());
            sb.append("Interrupted while waiting for latch : ");
            sb.append(valueOf);
            throw new zza(sb.toString(), 0);
        }
    }

    public void onStop() {
        synchronized (this.zzbxu) {
            super.onStop();
            if (this.zzbym != null) {
                this.zzbym.cancel();
            }
        }
    }

    /* access modifiers changed from: protected */
    public zzju zzak(int i) {
        AdRequestInfoParcel adRequestInfoParcel = this.zzbxr.zzcip;
        AdRequestParcel adRequestParcel = adRequestInfoParcel.zzcar;
        zzlh zzlh = this.zzbgf;
        List<String> list = this.zzbxs.zzbnm;
        List<String> list2 = this.zzbxs.zzbnn;
        List<String> list3 = this.zzbxs.zzcca;
        int i2 = this.zzbxs.orientation;
        long j = this.zzbxs.zzbns;
        String str = adRequestInfoParcel.zzcau;
        boolean z = this.zzbxs.zzcby;
        zzfz zzfz = this.zzbyn != null ? this.zzbyn.zzbon : null;
        zzgk zzgk = this.zzbyn != null ? this.zzbyn.zzboo : null;
        zzju zzju = new zzju(adRequestParcel, zzlh, list, i, list2, list3, i2, j, str, z, zzfz, zzgk, this.zzbyn != null ? this.zzbyn.zzbop : AdMobAdapter.class.getName(), this.zzboe, this.zzbyn != null ? this.zzbyn.zzboq : null, this.zzbxs.zzcbz, this.zzbxr.zzapa, this.zzbxs.zzcbx, this.zzbxr.zzcik, this.zzbxs.zzccc, this.zzbxs.zzccd, this.zzbxr.zzcie, null, this.zzbxs.zzccn, this.zzbxs.zzcco, this.zzbxs.zzccp, this.zzboe != null ? this.zzboe.zzbnx : false, this.zzbxs.zzccr, this.zzbym != null ? zzg(this.zzbym.zzmg()) : null, this.zzbxs.zzbnp);
        return zzju;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0081  */
    public void zzh(long j) throws zza {
        boolean z;
        synchronized (this.zzbxu) {
            this.zzbym = zzi(j);
        }
        ArrayList arrayList = new ArrayList(this.zzboe.zzbnk);
        Bundle bundle = this.zzbxr.zzcip.zzcar.zzatw;
        String str = "com.google.ads.mediation.admob.AdMobAdapter";
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle(str);
            if (bundle2 != null) {
                z = bundle2.getBoolean("_skipMediation");
                if (z) {
                    ListIterator listIterator = arrayList.listIterator();
                    while (listIterator.hasNext()) {
                        if (!((zzfz) listIterator.next()).zzbmw.contains(str)) {
                            listIterator.remove();
                        }
                    }
                }
                this.zzbyn = this.zzbym.zzd(arrayList);
                switch (this.zzbyn.zzbom) {
                    case 0:
                        if (this.zzbyn.zzbon != null && this.zzbyn.zzbon.zzbnf != null) {
                            zzqf();
                            return;
                        }
                        return;
                    case 1:
                        throw new zza("No fill from any mediation ad networks.", 3);
                    default:
                        int i = this.zzbyn.zzbom;
                        StringBuilder sb = new StringBuilder(40);
                        sb.append("Unexpected mediation result: ");
                        sb.append(i);
                        throw new zza(sb.toString(), 0);
                }
            }
        }
        z = false;
        if (z) {
        }
        this.zzbyn = this.zzbym.zzd(arrayList);
        switch (this.zzbyn.zzbom) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    public zzfy zzi(long j) {
        if (this.zzboe.zzbnv != -1) {
            zzgg zzgg = new zzgg(this.mContext, this.zzbxr.zzcip, this.zzajz, this.zzboe, this.zzbxs.zzauu, this.zzbxs.zzauw, j, ((Long) zzdc.zzbbh.get()).longValue(), 2);
            return zzgg;
        }
        Context context = this.mContext;
        AdRequestInfoParcel adRequestInfoParcel = this.zzbxr.zzcip;
        zzgj zzgj = this.zzajz;
        zzga zzga = this.zzboe;
        boolean z = this.zzbxs.zzauu;
        zzgh zzgh = new zzgh(context, adRequestInfoParcel, zzgj, zzga, z, this.zzbxs.zzauw, j, ((Long) zzdc.zzbbh.get()).longValue(), this.zzajn);
        return zzgh;
    }
}
