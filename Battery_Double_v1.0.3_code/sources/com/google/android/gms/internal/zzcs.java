package com.google.android.gms.internal;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import java.security.MessageDigest;

@zzin
public class zzcs extends zzcp {
    private MessageDigest zzath;

    /* access modifiers changed from: 0000 */
    public byte[] zza(String[] strArr) {
        if (strArr.length == 1) {
            return zzcr.zzn(zzcr.zzac(strArr[0]));
        }
        if (strArr.length < 5) {
            byte[] bArr = new byte[(strArr.length * 2)];
            for (int i = 0; i < strArr.length; i++) {
                byte[] zzq = zzq(zzcr.zzac(strArr[i]));
                int i2 = i * 2;
                bArr[i2] = zzq[0];
                bArr[i2 + 1] = zzq[1];
            }
            return bArr;
        }
        byte[] bArr2 = new byte[strArr.length];
        for (int i3 = 0; i3 < strArr.length; i3++) {
            bArr2[i3] = zzp(zzcr.zzac(strArr[i3]));
        }
        return bArr2;
    }

    public byte[] zzaa(String str) {
        byte[] zza = zza(str.split(" "));
        this.zzath = zzie();
        synchronized (this.zzail) {
            if (this.zzath == null) {
                byte[] bArr = new byte[0];
                return bArr;
            }
            this.zzath.reset();
            this.zzath.update(zza);
            byte[] digest = this.zzath.digest();
            int i = 4;
            if (digest.length <= 4) {
                i = digest.length;
            }
            byte[] bArr2 = new byte[i];
            System.arraycopy(digest, 0, bArr2, 0, bArr2.length);
            return bArr2;
        }
    }

    /* access modifiers changed from: 0000 */
    public byte zzp(int i) {
        return (byte) (((i & ViewCompat.MEASURED_STATE_MASK) >> 24) ^ (((i & 255) ^ ((65280 & i) >> 8)) ^ ((16711680 & i) >> 16)));
    }

    /* access modifiers changed from: 0000 */
    public byte[] zzq(int i) {
        int i2 = ((i & SupportMenu.CATEGORY_MASK) >> 16) ^ (65535 & i);
        return new byte[]{(byte) i2, (byte) (i2 >> 8)};
    }
}
