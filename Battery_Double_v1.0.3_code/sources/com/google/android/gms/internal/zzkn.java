package com.google.android.gms.internal;

import android.content.Context;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

@zzin
public class zzkn {
    private static zzl zzcmc;
    private static final Object zzcmd = new Object();
    public static final zza<Void> zzcme = new zza<Void>() {
        /* renamed from: zzi */
        public Void zzh(InputStream inputStream) {
            return null;
        }

        /* renamed from: zztp */
        public Void zzqu() {
            return null;
        }
    };

    public interface zza<T> {
        T zzh(InputStream inputStream);

        T zzqu();
    }

    private static class zzb<T> extends zzk<InputStream> {
        private final com.google.android.gms.internal.zzm.zzb<T> zzcg;
        private final zza<T> zzcmj;

        public zzb(String str, final zza<T> zza, final com.google.android.gms.internal.zzm.zzb<T> zzb) {
            super(0, str, new com.google.android.gms.internal.zzm.zza() {
                public void zze(zzr zzr) {
                    com.google.android.gms.internal.zzm.zzb.this.zzb(zza.zzqu());
                }
            });
            this.zzcmj = zza;
            this.zzcg = zzb;
        }

        /* access modifiers changed from: protected */
        public zzm<InputStream> zza(zzi zzi) {
            return zzm.zza(new ByteArrayInputStream(zzi.data), zzx.zzb(zzi));
        }

        /* access modifiers changed from: protected */
        /* renamed from: zzj */
        public void zza(InputStream inputStream) {
            this.zzcg.zzb(this.zzcmj.zzh(inputStream));
        }
    }

    private class zzc<T> extends zzkv<T> implements com.google.android.gms.internal.zzm.zzb<T> {
        private zzc() {
        }

        public void zzb(T t) {
            super.zzh(t);
        }
    }

    public zzkn(Context context) {
        zzap(context);
    }

    private static zzl zzap(Context context) {
        zzl zzl;
        synchronized (zzcmd) {
            if (zzcmc == null) {
                zzcmc = zzac.zza(context.getApplicationContext());
            }
            zzl = zzcmc;
        }
        return zzl;
    }

    public zzky<String> zza(int i, final String str, Map<String, String> map, byte[] bArr) {
        final zzc zzc2 = new zzc();
        final byte[] bArr2 = bArr;
        final Map<String, String> map2 = map;
        AnonymousClass3 r0 = new zzab(i, str, zzc2, new com.google.android.gms.internal.zzm.zza() {
            public void zze(zzr zzr) {
                String str = str;
                String valueOf = String.valueOf(zzr.toString());
                StringBuilder sb = new StringBuilder(21 + String.valueOf(str).length() + String.valueOf(valueOf).length());
                sb.append("Failed to load URL: ");
                sb.append(str);
                sb.append("\n");
                sb.append(valueOf);
                zzkd.zzcx(sb.toString());
                zzc2.zzb(null);
            }
        }) {
            public Map<String, String> getHeaders() throws zza {
                return map2 == null ? super.getHeaders() : map2;
            }

            public byte[] zzp() throws zza {
                return bArr2 == null ? super.zzp() : bArr2;
            }
        };
        zzcmc.zze(r0);
        return zzc2;
    }

    public <T> zzky<T> zza(String str, zza<T> zza2) {
        zzc zzc2 = new zzc();
        zzcmc.zze(new zzb(str, zza2, zzc2));
        return zzc2;
    }

    public zzky<String> zzc(String str, Map<String, String> map) {
        return zza(0, str, map, null);
    }
}
