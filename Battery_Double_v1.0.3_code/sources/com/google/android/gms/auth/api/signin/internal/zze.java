package com.google.android.gms.auth.api.signin.internal;

public class zze {
    static int ef = 31;
    private int eg = 1;

    public int zzagc() {
        return this.eg;
    }

    public zze zzba(boolean z) {
        this.eg = (ef * this.eg) + (z ? 1 : 0);
        return this;
    }

    public zze zzq(Object obj) {
        this.eg = (ef * this.eg) + (obj == null ? 0 : obj.hashCode());
        return this;
    }
}
