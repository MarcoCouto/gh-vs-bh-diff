package android.support.v7.internal.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompatBase.Action;
import android.support.v7.appcompat.R;
import android.widget.RemoteViews;
import java.text.NumberFormat;
import java.util.List;

public class NotificationCompatImplBase {
    static final int MAX_MEDIA_BUTTONS = 5;
    static final int MAX_MEDIA_BUTTONS_IN_COMPACT = 3;

    public static <T extends Action> void overrideContentView(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, int[] iArr, boolean z2, PendingIntent pendingIntent) {
        notificationBuilderWithBuilderAccessor.getBuilder().setContent(generateContentView(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, list, iArr, z2, pendingIntent));
        if (z2) {
            notificationBuilderWithBuilderAccessor.getBuilder().setOngoing(true);
        }
    }

    private static <T extends Action> RemoteViews generateContentView(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, int[] iArr, boolean z2, PendingIntent pendingIntent) {
        int i2;
        int[] iArr2 = iArr;
        RemoteViews applyStandardTemplate = applyStandardTemplate(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, R.layout.notification_template_media, true);
        int size = list.size();
        if (iArr2 == null) {
            i2 = 0;
        } else {
            i2 = Math.min(iArr2.length, 3);
        }
        applyStandardTemplate.removeAllViews(R.id.media_actions);
        if (i2 > 0) {
            for (int i3 = 0; i3 < i2; i3++) {
                if (i3 >= size) {
                    throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", new Object[]{Integer.valueOf(i3), Integer.valueOf(size - 1)}));
                }
                Context context2 = context;
                applyStandardTemplate.addView(R.id.media_actions, generateMediaActionButton(context2, (Action) list.get(iArr2[i3])));
            }
        }
        Context context3 = context;
        if (z2) {
            applyStandardTemplate.setViewVisibility(R.id.end_padder, 8);
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 0);
            applyStandardTemplate.setOnClickPendingIntent(R.id.cancel_action, pendingIntent);
            applyStandardTemplate.setInt(R.id.cancel_action, "setAlpha", context3.getResources().getInteger(R.integer.cancel_button_image_alpha));
        } else {
            applyStandardTemplate.setViewVisibility(R.id.end_padder, 0);
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
        }
        return applyStandardTemplate;
    }

    public static <T extends Action> void overrideBigContentView(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, boolean z2, PendingIntent pendingIntent) {
        notification.bigContentView = generateBigContentView(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, list, z2, pendingIntent);
        if (z2) {
            notification.flags |= 2;
        }
    }

    private static <T extends Action> RemoteViews generateBigContentView(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, boolean z2, PendingIntent pendingIntent) {
        int min = Math.min(list.size(), 5);
        RemoteViews applyStandardTemplate = applyStandardTemplate(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, getBigLayoutResource(min), false);
        applyStandardTemplate.removeAllViews(R.id.media_actions);
        if (min > 0) {
            for (int i2 = 0; i2 < min; i2++) {
                applyStandardTemplate.addView(R.id.media_actions, generateMediaActionButton(context, (Action) list.get(i2)));
            }
        }
        Context context2 = context;
        if (z2) {
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 0);
            applyStandardTemplate.setInt(R.id.cancel_action, "setAlpha", context2.getResources().getInteger(R.integer.cancel_button_image_alpha));
            applyStandardTemplate.setOnClickPendingIntent(R.id.cancel_action, pendingIntent);
        } else {
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
        }
        return applyStandardTemplate;
    }

    private static RemoteViews generateMediaActionButton(Context context, Action action) {
        boolean z = action.getActionIntent() == null;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_media_action);
        remoteViews.setImageViewResource(R.id.action0, action.getIcon());
        if (!z) {
            remoteViews.setOnClickPendingIntent(R.id.action0, action.getActionIntent());
        }
        if (VERSION.SDK_INT >= 15) {
            remoteViews.setContentDescription(R.id.action0, action.getTitle());
        }
        return remoteViews;
    }

    private static int getBigLayoutResource(int i) {
        if (i <= 3) {
            return R.layout.notification_template_big_media_narrow;
        }
        return R.layout.notification_template_big_media;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0100  */
    private static RemoteViews applyStandardTemplate(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        CharSequence charSequence5 = charSequence;
        CharSequence charSequence6 = charSequence2;
        CharSequence charSequence7 = charSequence3;
        int i3 = i;
        Bitmap bitmap2 = bitmap;
        CharSequence charSequence8 = charSequence4;
        long j2 = j;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), i2);
        int i4 = 8;
        if (bitmap2 == null || VERSION.SDK_INT < 16) {
            remoteViews.setViewVisibility(R.id.icon, 8);
        } else {
            remoteViews.setImageViewBitmap(R.id.icon, bitmap2);
        }
        if (charSequence5 != null) {
            remoteViews.setTextViewText(R.id.title, charSequence5);
        }
        if (charSequence6 != null) {
            remoteViews.setTextViewText(R.id.text, charSequence6);
            z3 = true;
        } else {
            z3 = false;
        }
        if (charSequence7 != null) {
            remoteViews.setTextViewText(R.id.info, charSequence7);
            remoteViews.setViewVisibility(R.id.info, 0);
        } else if (i3 > 0) {
            if (i3 > context.getResources().getInteger(R.integer.status_bar_notification_info_maxnum)) {
                remoteViews.setTextViewText(R.id.info, context.getResources().getString(R.string.status_bar_notification_info_overflow));
            } else {
                remoteViews.setTextViewText(R.id.info, NumberFormat.getIntegerInstance().format((long) i3));
            }
            remoteViews.setViewVisibility(R.id.info, 0);
        } else {
            remoteViews.setViewVisibility(R.id.info, 8);
            z4 = z3;
            if (charSequence8 != null && VERSION.SDK_INT >= 16) {
                remoteViews.setTextViewText(R.id.text, charSequence8);
                if (charSequence6 == null) {
                    remoteViews.setTextViewText(R.id.text2, charSequence6);
                    remoteViews.setViewVisibility(R.id.text2, 0);
                    z5 = true;
                    if (z5 && VERSION.SDK_INT >= 16) {
                        if (z2) {
                            remoteViews.setTextViewTextSize(R.id.text, 0, (float) context.getResources().getDimensionPixelSize(R.dimen.notification_subtext_size));
                        }
                        remoteViews.setViewPadding(R.id.line1, 0, 0, 0, 0);
                    }
                    if (j2 != 0) {
                        if (z) {
                            remoteViews.setViewVisibility(R.id.chronometer, 0);
                            remoteViews.setLong(R.id.chronometer, "setBase", j2 + (SystemClock.elapsedRealtime() - System.currentTimeMillis()));
                            remoteViews.setBoolean(R.id.chronometer, "setStarted", true);
                        } else {
                            remoteViews.setViewVisibility(R.id.time, 0);
                            remoteViews.setLong(R.id.time, "setTime", j2);
                        }
                    }
                    int i5 = R.id.line3;
                    if (z4) {
                        i4 = 0;
                    }
                    remoteViews.setViewVisibility(i5, i4);
                    return remoteViews;
                }
                remoteViews.setViewVisibility(R.id.text2, 8);
            }
            z5 = false;
            if (z2) {
            }
            remoteViews.setViewPadding(R.id.line1, 0, 0, 0, 0);
            if (j2 != 0) {
            }
            int i52 = R.id.line3;
            if (z4) {
            }
            remoteViews.setViewVisibility(i52, i4);
            return remoteViews;
        }
        z4 = true;
        remoteViews.setTextViewText(R.id.text, charSequence8);
        if (charSequence6 == null) {
        }
    }
}
