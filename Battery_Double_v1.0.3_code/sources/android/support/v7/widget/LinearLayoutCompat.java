package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.internal.widget.TintTypedArray;
import android.support.v7.internal.widget.ViewUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LinearLayoutCompat extends ViewGroup {
    public static final int HORIZONTAL = 0;
    private static final int INDEX_BOTTOM = 2;
    private static final int INDEX_CENTER_VERTICAL = 0;
    private static final int INDEX_FILL = 3;
    private static final int INDEX_TOP = 1;
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    public static final int VERTICAL = 1;
    private static final int VERTICAL_GRAVITY_COUNT = 4;
    private boolean mBaselineAligned;
    private int mBaselineAlignedChildIndex;
    private int mBaselineChildTop;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;
    private int mGravity;
    private int[] mMaxAscent;
    private int[] mMaxDescent;
    private int mOrientation;
    private int mShowDividers;
    private int mTotalLength;
    private boolean mUseLargestChild;
    private float mWeightSum;

    @Retention(RetentionPolicy.SOURCE)
    public @interface DividerMode {
    }

    public static class LayoutParams extends MarginLayoutParams {
        public int gravity;
        public float weight;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.gravity = -1;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.LinearLayoutCompat_Layout);
            this.weight = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0f);
            this.gravity = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.gravity = -1;
            this.weight = 0.0f;
        }

        public LayoutParams(int i, int i2, float f) {
            super(i, i2);
            this.gravity = -1;
            this.weight = f;
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = -1;
        }

        public LayoutParams(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.gravity = -1;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = -1;
            this.weight = layoutParams.weight;
            this.gravity = layoutParams.gravity;
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface OrientationMode {
    }

    /* access modifiers changed from: 0000 */
    public int getChildrenSkipCount(View view, int i) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int getLocationOffset(View view) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int getNextLocationOffset(View view) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int measureNullChild(int i) {
        return 0;
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public LinearLayoutCompat(Context context) {
        this(context, null);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mBaselineAligned = true;
        this.mBaselineAlignedChildIndex = -1;
        this.mBaselineChildTop = 0;
        this.mGravity = 8388659;
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, attributeSet, R.styleable.LinearLayoutCompat, i, 0);
        int i2 = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_orientation, -1);
        if (i2 >= 0) {
            setOrientation(i2);
        }
        int i3 = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_gravity, -1);
        if (i3 >= 0) {
            setGravity(i3);
        }
        boolean z = obtainStyledAttributes.getBoolean(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
        if (!z) {
            setBaselineAligned(z);
        }
        this.mWeightSum = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_android_weightSum, -1.0f);
        this.mBaselineAlignedChildIndex = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.mUseLargestChild = obtainStyledAttributes.getBoolean(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
        setDividerDrawable(obtainStyledAttributes.getDrawable(R.styleable.LinearLayoutCompat_divider));
        this.mShowDividers = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_showDividers, 0);
        this.mDividerPadding = obtainStyledAttributes.getDimensionPixelSize(R.styleable.LinearLayoutCompat_dividerPadding, 0);
        obtainStyledAttributes.recycle();
    }

    public void setShowDividers(int i) {
        if (i != this.mShowDividers) {
            requestLayout();
        }
        this.mShowDividers = i;
    }

    public int getShowDividers() {
        return this.mShowDividers;
    }

    public Drawable getDividerDrawable() {
        return this.mDivider;
    }

    public void setDividerDrawable(Drawable drawable) {
        if (drawable != this.mDivider) {
            this.mDivider = drawable;
            boolean z = false;
            if (drawable != null) {
                this.mDividerWidth = drawable.getIntrinsicWidth();
                this.mDividerHeight = drawable.getIntrinsicHeight();
            } else {
                this.mDividerWidth = 0;
                this.mDividerHeight = 0;
            }
            if (drawable == null) {
                z = true;
            }
            setWillNotDraw(z);
            requestLayout();
        }
    }

    public void setDividerPadding(int i) {
        this.mDividerPadding = i;
    }

    public int getDividerPadding() {
        return this.mDividerPadding;
    }

    public int getDividerWidth() {
        return this.mDividerWidth;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.mDivider != null) {
            if (this.mOrientation == 1) {
                drawDividersVertical(canvas);
            } else {
                drawDividersHorizontal(canvas);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void drawDividersVertical(Canvas canvas) {
        int i;
        int virtualChildCount = getVirtualChildCount();
        for (int i2 = 0; i2 < virtualChildCount; i2++) {
            View virtualChildAt = getVirtualChildAt(i2);
            if (!(virtualChildAt == null || virtualChildAt.getVisibility() == 8 || !hasDividerBeforeChildAt(i2))) {
                drawHorizontalDivider(canvas, (virtualChildAt.getTop() - ((LayoutParams) virtualChildAt.getLayoutParams()).topMargin) - this.mDividerHeight);
            }
        }
        if (hasDividerBeforeChildAt(virtualChildCount)) {
            View virtualChildAt2 = getVirtualChildAt(virtualChildCount - 1);
            if (virtualChildAt2 == null) {
                i = (getHeight() - getPaddingBottom()) - this.mDividerHeight;
            } else {
                i = virtualChildAt2.getBottom() + ((LayoutParams) virtualChildAt2.getLayoutParams()).bottomMargin;
            }
            drawHorizontalDivider(canvas, i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void drawDividersHorizontal(Canvas canvas) {
        int i;
        int i2;
        int virtualChildCount = getVirtualChildCount();
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        for (int i3 = 0; i3 < virtualChildCount; i3++) {
            View virtualChildAt = getVirtualChildAt(i3);
            if (!(virtualChildAt == null || virtualChildAt.getVisibility() == 8 || !hasDividerBeforeChildAt(i3))) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (isLayoutRtl) {
                    i2 = virtualChildAt.getRight() + layoutParams.rightMargin;
                } else {
                    i2 = (virtualChildAt.getLeft() - layoutParams.leftMargin) - this.mDividerWidth;
                }
                drawVerticalDivider(canvas, i2);
            }
        }
        if (hasDividerBeforeChildAt(virtualChildCount)) {
            View virtualChildAt2 = getVirtualChildAt(virtualChildCount - 1);
            if (virtualChildAt2 != null) {
                LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                if (isLayoutRtl) {
                    i = (virtualChildAt2.getLeft() - layoutParams2.leftMargin) - this.mDividerWidth;
                } else {
                    i = virtualChildAt2.getRight() + layoutParams2.rightMargin;
                }
            } else if (isLayoutRtl) {
                i = getPaddingLeft();
            } else {
                i = (getWidth() - getPaddingRight()) - this.mDividerWidth;
            }
            drawVerticalDivider(canvas, i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void drawHorizontalDivider(Canvas canvas, int i) {
        this.mDivider.setBounds(getPaddingLeft() + this.mDividerPadding, i, (getWidth() - getPaddingRight()) - this.mDividerPadding, this.mDividerHeight + i);
        this.mDivider.draw(canvas);
    }

    /* access modifiers changed from: 0000 */
    public void drawVerticalDivider(Canvas canvas, int i) {
        this.mDivider.setBounds(i, getPaddingTop() + this.mDividerPadding, this.mDividerWidth + i, (getHeight() - getPaddingBottom()) - this.mDividerPadding);
        this.mDivider.draw(canvas);
    }

    public boolean isBaselineAligned() {
        return this.mBaselineAligned;
    }

    public void setBaselineAligned(boolean z) {
        this.mBaselineAligned = z;
    }

    public boolean isMeasureWithLargestChildEnabled() {
        return this.mUseLargestChild;
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.mUseLargestChild = z;
    }

    public int getBaseline() {
        if (this.mBaselineAlignedChildIndex < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.mBaselineAlignedChildIndex) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        View childAt = getChildAt(this.mBaselineAlignedChildIndex);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i = this.mBaselineChildTop;
            if (this.mOrientation == 1) {
                int i2 = this.mGravity & 112;
                if (i2 != 48) {
                    if (i2 == 16) {
                        i += ((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.mTotalLength) / 2;
                    } else if (i2 == 80) {
                        i = ((getBottom() - getTop()) - getPaddingBottom()) - this.mTotalLength;
                    }
                }
            }
            return i + ((LayoutParams) childAt.getLayoutParams()).topMargin + baseline;
        } else if (this.mBaselineAlignedChildIndex == 0) {
            return -1;
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.mBaselineAlignedChildIndex;
    }

    public void setBaselineAlignedChildIndex(int i) {
        if (i < 0 || i >= getChildCount()) {
            StringBuilder sb = new StringBuilder();
            sb.append("base aligned child index out of range (0, ");
            sb.append(getChildCount());
            sb.append(")");
            throw new IllegalArgumentException(sb.toString());
        }
        this.mBaselineAlignedChildIndex = i;
    }

    /* access modifiers changed from: 0000 */
    public View getVirtualChildAt(int i) {
        return getChildAt(i);
    }

    /* access modifiers changed from: 0000 */
    public int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.mWeightSum;
    }

    public void setWeightSum(float f) {
        this.mWeightSum = Math.max(0.0f, f);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.mOrientation == 1) {
            measureVertical(i, i2);
        } else {
            measureHorizontal(i, i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasDividerBeforeChildAt(int i) {
        boolean z = false;
        if (i == 0) {
            if ((this.mShowDividers & 1) != 0) {
                z = true;
            }
            return z;
        } else if (i == getChildCount()) {
            if ((this.mShowDividers & 4) != 0) {
                z = true;
            }
            return z;
        } else if ((this.mShowDividers & 2) == 0) {
            return false;
        } else {
            int i2 = i - 1;
            while (true) {
                if (i2 < 0) {
                    break;
                } else if (getChildAt(i2).getVisibility() != 8) {
                    z = true;
                    break;
                } else {
                    i2--;
                }
            }
            return z;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0337  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x017b  */
    public void measureVertical(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        float f;
        int i7;
        int i8;
        boolean z;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        int i19;
        LayoutParams layoutParams;
        View view;
        int i20;
        boolean z2;
        int i21;
        int i22;
        int i23;
        int i24 = i;
        int i25 = i2;
        int i26 = 0;
        this.mTotalLength = 0;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        int i27 = this.mBaselineAlignedChildIndex;
        boolean z3 = this.mUseLargestChild;
        int i28 = 0;
        int i29 = 0;
        int i30 = 0;
        boolean z4 = false;
        boolean z5 = false;
        int i31 = 0;
        float f2 = 0.0f;
        boolean z6 = true;
        int i32 = Integer.MIN_VALUE;
        while (i30 < virtualChildCount) {
            View virtualChildAt = getVirtualChildAt(i30);
            if (virtualChildAt == null) {
                this.mTotalLength += measureNullChild(i30);
                i13 = i26;
                i11 = virtualChildCount;
                i12 = mode2;
            } else {
                int i33 = i28;
                if (virtualChildAt.getVisibility() == 8) {
                    i30 += getChildrenSkipCount(virtualChildAt, i30);
                    i13 = i26;
                    i11 = virtualChildCount;
                    i12 = mode2;
                    i28 = i33;
                } else {
                    if (hasDividerBeforeChildAt(i30)) {
                        this.mTotalLength += this.mDividerHeight;
                    }
                    LayoutParams layoutParams2 = (LayoutParams) virtualChildAt.getLayoutParams();
                    float f3 = f2 + layoutParams2.weight;
                    if (mode2 == 1073741824 && layoutParams2.height == 0 && layoutParams2.weight > 0.0f) {
                        int i34 = this.mTotalLength;
                        i17 = i32;
                        this.mTotalLength = Math.max(i34, layoutParams2.topMargin + i34 + layoutParams2.bottomMargin);
                        i15 = i29;
                        view = virtualChildAt;
                        layoutParams = layoutParams2;
                        i14 = i26;
                        i23 = virtualChildCount;
                        i22 = mode2;
                        z4 = true;
                        i16 = i31;
                        i18 = i33;
                        i19 = i30;
                    } else {
                        int i35 = i32;
                        if (layoutParams2.height != 0 || layoutParams2.weight <= 0.0f) {
                            i21 = Integer.MIN_VALUE;
                        } else {
                            layoutParams2.height = -2;
                            i21 = 0;
                        }
                        i22 = mode2;
                        i18 = i33;
                        int i36 = i21;
                        int i37 = i35;
                        int i38 = i30;
                        i23 = virtualChildCount;
                        int i39 = i29;
                        int i40 = i24;
                        view = virtualChildAt;
                        i15 = i39;
                        i16 = i31;
                        i19 = i30;
                        int i41 = i25;
                        layoutParams = layoutParams2;
                        i14 = i26;
                        measureChildBeforeLayout(virtualChildAt, i38, i40, 0, i41, f3 == 0.0f ? this.mTotalLength : 0);
                        int i42 = i36;
                        if (i42 != Integer.MIN_VALUE) {
                            layoutParams.height = i42;
                        }
                        int measuredHeight = view.getMeasuredHeight();
                        int i43 = this.mTotalLength;
                        this.mTotalLength = Math.max(i43, i43 + measuredHeight + layoutParams.topMargin + layoutParams.bottomMargin + getNextLocationOffset(view));
                        i17 = z3 ? Math.max(measuredHeight, i37) : i37;
                    }
                    if (i27 >= 0 && i27 == i19 + 1) {
                        this.mBaselineChildTop = this.mTotalLength;
                    }
                    if (i19 >= i27 || layoutParams.weight <= 0.0f) {
                        if (mode != 1073741824) {
                            i20 = -1;
                            if (layoutParams.width == -1) {
                                z2 = true;
                                z5 = true;
                                int i44 = layoutParams.leftMargin + layoutParams.rightMargin;
                                int measuredWidth = view.getMeasuredWidth() + i44;
                                int max = Math.max(i18, measuredWidth);
                                int combineMeasuredStates = ViewUtils.combineMeasuredStates(i14, ViewCompat.getMeasuredState(view));
                                boolean z7 = !z6 && layoutParams.width == i20;
                                if (layoutParams.weight <= 0.0f) {
                                    if (!z2) {
                                        i44 = measuredWidth;
                                    }
                                    i29 = Math.max(i15, i44);
                                } else {
                                    int i45 = i15;
                                    if (z2) {
                                        measuredWidth = i44;
                                    }
                                    int max2 = Math.max(i16, measuredWidth);
                                    i29 = i45;
                                    i16 = max2;
                                }
                                z6 = z7;
                                i28 = max;
                                i13 = combineMeasuredStates;
                                i32 = i17;
                                i31 = i16;
                                i30 = getChildrenSkipCount(view, i19) + i19;
                                f2 = f3;
                                i30++;
                                i26 = i13;
                                mode2 = i12;
                                virtualChildCount = i11;
                                i24 = i;
                                i25 = i2;
                            }
                        } else {
                            i20 = -1;
                        }
                        z2 = false;
                        int i442 = layoutParams.leftMargin + layoutParams.rightMargin;
                        int measuredWidth2 = view.getMeasuredWidth() + i442;
                        int max3 = Math.max(i18, measuredWidth2);
                        int combineMeasuredStates2 = ViewUtils.combineMeasuredStates(i14, ViewCompat.getMeasuredState(view));
                        if (!z6) {
                        }
                        if (layoutParams.weight <= 0.0f) {
                        }
                        z6 = z7;
                        i28 = max3;
                        i13 = combineMeasuredStates2;
                        i32 = i17;
                        i31 = i16;
                        i30 = getChildrenSkipCount(view, i19) + i19;
                        f2 = f3;
                        i30++;
                        i26 = i13;
                        mode2 = i12;
                        virtualChildCount = i11;
                        i24 = i;
                        i25 = i2;
                    } else {
                        throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                    }
                }
            }
            i30++;
            i26 = i13;
            mode2 = i12;
            virtualChildCount = i11;
            i24 = i;
            i25 = i2;
        }
        int i46 = i32;
        int i47 = i29;
        int i48 = i26;
        int i49 = virtualChildCount;
        int i50 = mode2;
        int i51 = i31;
        int i52 = i28;
        if (this.mTotalLength > 0) {
            i3 = i49;
            if (hasDividerBeforeChildAt(i3)) {
                this.mTotalLength += this.mDividerHeight;
            }
        } else {
            i3 = i49;
        }
        if (z3) {
            i4 = i50;
            if (i4 == Integer.MIN_VALUE || i4 == 0) {
                this.mTotalLength = 0;
                int i53 = 0;
                while (i53 < i3) {
                    View virtualChildAt2 = getVirtualChildAt(i53);
                    if (virtualChildAt2 == null) {
                        this.mTotalLength += measureNullChild(i53);
                    } else if (virtualChildAt2.getVisibility() == 8) {
                        i53 += getChildrenSkipCount(virtualChildAt2, i53);
                    } else {
                        LayoutParams layoutParams3 = (LayoutParams) virtualChildAt2.getLayoutParams();
                        int i54 = this.mTotalLength;
                        this.mTotalLength = Math.max(i54, i54 + i46 + layoutParams3.topMargin + layoutParams3.bottomMargin + getNextLocationOffset(virtualChildAt2));
                    }
                    i53++;
                }
            }
        } else {
            i4 = i50;
        }
        this.mTotalLength += getPaddingTop() + getPaddingBottom();
        int i55 = i2;
        int resolveSizeAndState = ViewCompat.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumHeight()), i55, 0);
        int i56 = (16777215 & resolveSizeAndState) - this.mTotalLength;
        if (z4 || (i56 != 0 && f2 > 0.0f)) {
            if (this.mWeightSum > 0.0f) {
                f2 = this.mWeightSum;
            }
            this.mTotalLength = 0;
            float f4 = f2;
            int i57 = 0;
            int i58 = i56;
            int i59 = i48;
            int i60 = i58;
            while (i57 < i3) {
                View virtualChildAt3 = getVirtualChildAt(i57);
                if (virtualChildAt3.getVisibility() == 8) {
                    f = f4;
                    int i61 = i;
                } else {
                    LayoutParams layoutParams4 = (LayoutParams) virtualChildAt3.getLayoutParams();
                    float f5 = layoutParams4.weight;
                    if (f5 > 0.0f) {
                        int i62 = (int) ((((float) i60) * f5) / f4);
                        i7 = i60 - i62;
                        f = f4 - f5;
                        int childMeasureSpec = getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + layoutParams4.leftMargin + layoutParams4.rightMargin, layoutParams4.width);
                        if (layoutParams4.height == 0) {
                            i10 = 1073741824;
                            if (i4 == 1073741824) {
                                if (i62 <= 0) {
                                    i62 = 0;
                                }
                                virtualChildAt3.measure(childMeasureSpec, MeasureSpec.makeMeasureSpec(i62, 1073741824));
                                i59 = ViewUtils.combineMeasuredStates(i59, ViewCompat.getMeasuredState(virtualChildAt3) & InputDeviceCompat.SOURCE_ANY);
                            }
                        } else {
                            i10 = 1073741824;
                        }
                        int measuredHeight2 = virtualChildAt3.getMeasuredHeight() + i62;
                        if (measuredHeight2 < 0) {
                            measuredHeight2 = 0;
                        }
                        virtualChildAt3.measure(childMeasureSpec, MeasureSpec.makeMeasureSpec(measuredHeight2, i10));
                        i59 = ViewUtils.combineMeasuredStates(i59, ViewCompat.getMeasuredState(virtualChildAt3) & InputDeviceCompat.SOURCE_ANY);
                    } else {
                        float f6 = f4;
                        int i63 = i;
                        i7 = i60;
                        f = f6;
                    }
                    int i64 = layoutParams4.leftMargin + layoutParams4.rightMargin;
                    int measuredWidth3 = virtualChildAt3.getMeasuredWidth() + i64;
                    i52 = Math.max(i52, measuredWidth3);
                    if (mode != 1073741824) {
                        i8 = i64;
                        i9 = -1;
                        if (layoutParams4.width == -1) {
                            z = true;
                            if (z) {
                                measuredWidth3 = i8;
                            }
                            i51 = Math.max(i51, measuredWidth3);
                            boolean z8 = !z6 && layoutParams4.width == i9;
                            int i65 = this.mTotalLength;
                            this.mTotalLength = Math.max(i65, i65 + virtualChildAt3.getMeasuredHeight() + layoutParams4.topMargin + layoutParams4.bottomMargin + getNextLocationOffset(virtualChildAt3));
                            z6 = z8;
                            i60 = i7;
                        }
                    } else {
                        i8 = i64;
                        i9 = -1;
                    }
                    z = false;
                    if (z) {
                    }
                    i51 = Math.max(i51, measuredWidth3);
                    if (!z6) {
                    }
                    int i652 = this.mTotalLength;
                    this.mTotalLength = Math.max(i652, i652 + virtualChildAt3.getMeasuredHeight() + layoutParams4.topMargin + layoutParams4.bottomMargin + getNextLocationOffset(virtualChildAt3));
                    z6 = z8;
                    i60 = i7;
                }
                i57++;
                f4 = f;
            }
            i5 = i;
            this.mTotalLength += getPaddingTop() + getPaddingBottom();
            i6 = i51;
            i48 = i59;
        } else {
            i6 = Math.max(i51, i47);
            if (z3 && i4 != 1073741824) {
                for (int i66 = 0; i66 < i3; i66++) {
                    View virtualChildAt4 = getVirtualChildAt(i66);
                    if (!(virtualChildAt4 == null || virtualChildAt4.getVisibility() == 8 || ((LayoutParams) virtualChildAt4.getLayoutParams()).weight <= 0.0f)) {
                        virtualChildAt4.measure(MeasureSpec.makeMeasureSpec(virtualChildAt4.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(i46, 1073741824));
                    }
                }
            }
            i5 = i;
        }
        if (!z6 && mode != 1073741824) {
            i52 = i6;
        }
        setMeasuredDimension(ViewCompat.resolveSizeAndState(Math.max(i52 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i5, i48), resolveSizeAndState);
        if (z5) {
            forceUniformWidth(i3, i55);
        }
    }

    private void forceUniformWidth(int i, int i2) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        for (int i3 = 0; i3 < i; i3++) {
            View virtualChildAt = getVirtualChildAt(i3);
            if (virtualChildAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (layoutParams.width == -1) {
                    int i4 = layoutParams.height;
                    layoutParams.height = virtualChildAt.getMeasuredHeight();
                    measureChildWithMargins(virtualChildAt, makeMeasureSpec, 0, i2, 0);
                    layoutParams.height = i4;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x043f  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01e2  */
    public void measureHorizontal(int i, int i2) {
        int[] iArr;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z;
        int i10;
        int i11;
        int i12;
        boolean z2;
        boolean z3;
        int i13;
        int i14;
        int i15;
        View view;
        LayoutParams layoutParams;
        boolean z4;
        int i16;
        int i17;
        int i18 = i;
        int i19 = i2;
        this.mTotalLength = 0;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        if (this.mMaxAscent == null || this.mMaxDescent == null) {
            this.mMaxAscent = new int[4];
            this.mMaxDescent = new int[4];
        }
        int[] iArr2 = this.mMaxAscent;
        int[] iArr3 = this.mMaxDescent;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        iArr3[3] = -1;
        iArr3[2] = -1;
        iArr3[1] = -1;
        iArr3[0] = -1;
        boolean z5 = this.mBaselineAligned;
        boolean z6 = this.mUseLargestChild;
        int i20 = 1073741824;
        boolean z7 = mode == 1073741824;
        int i21 = 0;
        int i22 = 0;
        boolean z8 = false;
        int i23 = 0;
        int i24 = 0;
        int i25 = 0;
        boolean z9 = false;
        boolean z10 = true;
        float f = 0.0f;
        int i26 = Integer.MIN_VALUE;
        while (true) {
            iArr = iArr3;
            i3 = 8;
            if (i21 >= virtualChildCount) {
                break;
            }
            View virtualChildAt = getVirtualChildAt(i21);
            if (virtualChildAt == null) {
                this.mTotalLength += measureNullChild(i21);
            } else if (virtualChildAt.getVisibility() == 8) {
                i21 += getChildrenSkipCount(virtualChildAt, i21);
            } else {
                if (hasDividerBeforeChildAt(i21)) {
                    this.mTotalLength += this.mDividerWidth;
                }
                LayoutParams layoutParams2 = (LayoutParams) virtualChildAt.getLayoutParams();
                f += layoutParams2.weight;
                if (mode == i20 && layoutParams2.width == 0 && layoutParams2.weight > 0.0f) {
                    if (z7) {
                        this.mTotalLength += layoutParams2.leftMargin + layoutParams2.rightMargin;
                    } else {
                        int i27 = this.mTotalLength;
                        this.mTotalLength = Math.max(i27, layoutParams2.leftMargin + i27 + layoutParams2.rightMargin);
                    }
                    if (z5) {
                        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
                        virtualChildAt.measure(makeMeasureSpec, makeMeasureSpec);
                        i15 = i21;
                        z3 = z6;
                        z2 = z5;
                        layoutParams = layoutParams2;
                        i12 = mode;
                        view = virtualChildAt;
                    } else {
                        i15 = i21;
                        z3 = z6;
                        z2 = z5;
                        layoutParams = layoutParams2;
                        i12 = mode;
                        z8 = true;
                        i14 = 1073741824;
                        view = virtualChildAt;
                        if (mode2 == i14 && layoutParams.height == -1) {
                            z4 = true;
                            z9 = true;
                        } else {
                            z4 = false;
                        }
                        int i28 = layoutParams.topMargin + layoutParams.bottomMargin;
                        int measuredHeight = view.getMeasuredHeight() + i28;
                        int combineMeasuredStates = ViewUtils.combineMeasuredStates(i25, ViewCompat.getMeasuredState(view));
                        if (z2) {
                            int baseline = view.getBaseline();
                            if (baseline != -1) {
                                int i29 = ((((layoutParams.gravity < 0 ? this.mGravity : layoutParams.gravity) & 112) >> 4) & -2) >> 1;
                                iArr2[i29] = Math.max(iArr2[i29], baseline);
                                iArr[i29] = Math.max(iArr[i29], measuredHeight - baseline);
                            }
                        }
                        int max = Math.max(i22, measuredHeight);
                        boolean z11 = !z10 && layoutParams.height == -1;
                        if (layoutParams.weight <= 0.0f) {
                            if (!z4) {
                                i28 = measuredHeight;
                            }
                            i16 = Math.max(i24, i28);
                        } else {
                            i16 = i24;
                            if (z4) {
                                measuredHeight = i28;
                            }
                            i23 = Math.max(i23, measuredHeight);
                        }
                        int i30 = i15;
                        i13 = getChildrenSkipCount(view, i30) + i30;
                        i25 = combineMeasuredStates;
                        i22 = max;
                        z10 = z11;
                        i24 = i16;
                        i20 = i14;
                        i21 = i13 + 1;
                        iArr3 = iArr;
                        z6 = z3;
                        z5 = z2;
                        mode = i12;
                        i18 = i;
                    }
                } else {
                    if (layoutParams2.width != 0 || layoutParams2.weight <= 0.0f) {
                        i17 = Integer.MIN_VALUE;
                    } else {
                        layoutParams2.width = -2;
                        i17 = 0;
                    }
                    i15 = i21;
                    int i31 = i17;
                    z3 = z6;
                    z2 = z5;
                    layoutParams = layoutParams2;
                    i12 = mode;
                    view = virtualChildAt;
                    measureChildBeforeLayout(virtualChildAt, i15, i18, f == 0.0f ? this.mTotalLength : 0, i19, 0);
                    if (i31 != Integer.MIN_VALUE) {
                        layoutParams.width = i31;
                    }
                    int measuredWidth = view.getMeasuredWidth();
                    if (z7) {
                        this.mTotalLength += layoutParams.leftMargin + measuredWidth + layoutParams.rightMargin + getNextLocationOffset(view);
                    } else {
                        int i32 = this.mTotalLength;
                        this.mTotalLength = Math.max(i32, i32 + measuredWidth + layoutParams.leftMargin + layoutParams.rightMargin + getNextLocationOffset(view));
                    }
                    if (z3) {
                        i26 = Math.max(measuredWidth, i26);
                    }
                }
                i14 = 1073741824;
                if (mode2 == i14) {
                }
                z4 = false;
                int i282 = layoutParams.topMargin + layoutParams.bottomMargin;
                int measuredHeight2 = view.getMeasuredHeight() + i282;
                int combineMeasuredStates2 = ViewUtils.combineMeasuredStates(i25, ViewCompat.getMeasuredState(view));
                if (z2) {
                }
                int max2 = Math.max(i22, measuredHeight2);
                if (!z10) {
                }
                if (layoutParams.weight <= 0.0f) {
                }
                int i302 = i15;
                i13 = getChildrenSkipCount(view, i302) + i302;
                i25 = combineMeasuredStates2;
                i22 = max2;
                z10 = z11;
                i24 = i16;
                i20 = i14;
                i21 = i13 + 1;
                iArr3 = iArr;
                z6 = z3;
                z5 = z2;
                mode = i12;
                i18 = i;
            }
            i13 = i21;
            i14 = i20;
            z3 = z6;
            z2 = z5;
            i12 = mode;
            i20 = i14;
            i21 = i13 + 1;
            iArr3 = iArr;
            z6 = z3;
            z5 = z2;
            mode = i12;
            i18 = i;
        }
        int i33 = i20;
        boolean z12 = z6;
        boolean z13 = z5;
        int i34 = mode;
        int i35 = i22;
        int i36 = i23;
        int i37 = i24;
        int i38 = i25;
        if (this.mTotalLength > 0 && hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerWidth;
        }
        if (!(iArr2[1] == -1 && iArr2[0] == -1 && iArr2[2] == -1 && iArr2[3] == -1)) {
            i35 = Math.max(i35, Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))) + Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))));
        }
        if (z12) {
            i4 = i34;
            if (i4 == Integer.MIN_VALUE || i4 == 0) {
                this.mTotalLength = 0;
                int i39 = 0;
                while (i39 < virtualChildCount) {
                    View virtualChildAt2 = getVirtualChildAt(i39);
                    if (virtualChildAt2 == null) {
                        this.mTotalLength += measureNullChild(i39);
                    } else if (virtualChildAt2.getVisibility() == i3) {
                        i39 += getChildrenSkipCount(virtualChildAt2, i39);
                    } else {
                        LayoutParams layoutParams3 = (LayoutParams) virtualChildAt2.getLayoutParams();
                        if (z7) {
                            this.mTotalLength += layoutParams3.leftMargin + i26 + layoutParams3.rightMargin + getNextLocationOffset(virtualChildAt2);
                        } else {
                            int i40 = this.mTotalLength;
                            i11 = i39;
                            this.mTotalLength = Math.max(i40, i40 + i26 + layoutParams3.leftMargin + layoutParams3.rightMargin + getNextLocationOffset(virtualChildAt2));
                            i39 = i11 + 1;
                            i3 = 8;
                        }
                    }
                    i11 = i39;
                    i39 = i11 + 1;
                    i3 = 8;
                }
            }
        } else {
            i4 = i34;
        }
        this.mTotalLength += getPaddingLeft() + getPaddingRight();
        int resolveSizeAndState = ViewCompat.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumWidth()), i, 0);
        int i41 = (16777215 & resolveSizeAndState) - this.mTotalLength;
        if (z8 || (i41 != 0 && f > 0.0f)) {
            float f2 = this.mWeightSum > 0.0f ? this.mWeightSum : f;
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            this.mTotalLength = 0;
            i6 = i36;
            int i42 = 0;
            int i43 = -1;
            while (i42 < virtualChildCount) {
                View virtualChildAt3 = getVirtualChildAt(i42);
                if (virtualChildAt3 == null || virtualChildAt3.getVisibility() == 8) {
                    i7 = virtualChildCount;
                } else {
                    LayoutParams layoutParams4 = (LayoutParams) virtualChildAt3.getLayoutParams();
                    float f3 = layoutParams4.weight;
                    if (f3 > 0.0f) {
                        i7 = virtualChildCount;
                        int i44 = (int) ((((float) i41) * f3) / f2);
                        f2 -= f3;
                        i8 = i41 - i44;
                        int childMeasureSpec = getChildMeasureSpec(i19, getPaddingTop() + getPaddingBottom() + layoutParams4.topMargin + layoutParams4.bottomMargin, layoutParams4.height);
                        if (layoutParams4.width == 0) {
                            i10 = 1073741824;
                            if (i4 == 1073741824) {
                                if (i44 <= 0) {
                                    i44 = 0;
                                }
                                virtualChildAt3.measure(MeasureSpec.makeMeasureSpec(i44, 1073741824), childMeasureSpec);
                                i38 = ViewUtils.combineMeasuredStates(i38, ViewCompat.getMeasuredState(virtualChildAt3) & ViewCompat.MEASURED_STATE_MASK);
                            }
                        } else {
                            i10 = 1073741824;
                        }
                        int measuredWidth2 = virtualChildAt3.getMeasuredWidth() + i44;
                        if (measuredWidth2 < 0) {
                            measuredWidth2 = 0;
                        }
                        virtualChildAt3.measure(MeasureSpec.makeMeasureSpec(measuredWidth2, i10), childMeasureSpec);
                        i38 = ViewUtils.combineMeasuredStates(i38, ViewCompat.getMeasuredState(virtualChildAt3) & ViewCompat.MEASURED_STATE_MASK);
                    } else {
                        i7 = virtualChildCount;
                        i8 = i41;
                    }
                    if (z7) {
                        this.mTotalLength += virtualChildAt3.getMeasuredWidth() + layoutParams4.leftMargin + layoutParams4.rightMargin + getNextLocationOffset(virtualChildAt3);
                    } else {
                        int i45 = this.mTotalLength;
                        this.mTotalLength = Math.max(i45, virtualChildAt3.getMeasuredWidth() + i45 + layoutParams4.leftMargin + layoutParams4.rightMargin + getNextLocationOffset(virtualChildAt3));
                    }
                    boolean z14 = mode2 != 1073741824 && layoutParams4.height == -1;
                    int i46 = layoutParams4.topMargin + layoutParams4.bottomMargin;
                    int measuredHeight3 = virtualChildAt3.getMeasuredHeight() + i46;
                    i43 = Math.max(i43, measuredHeight3);
                    if (!z14) {
                        i46 = measuredHeight3;
                    }
                    int max3 = Math.max(i6, i46);
                    if (z10) {
                        i9 = -1;
                        if (layoutParams4.height == -1) {
                            z = true;
                            if (z13) {
                                int baseline2 = virtualChildAt3.getBaseline();
                                if (baseline2 != i9) {
                                    int i47 = ((((layoutParams4.gravity < 0 ? this.mGravity : layoutParams4.gravity) & 112) >> 4) & -2) >> 1;
                                    iArr2[i47] = Math.max(iArr2[i47], baseline2);
                                    iArr[i47] = Math.max(iArr[i47], measuredHeight3 - baseline2);
                                    i6 = max3;
                                    z10 = z;
                                    i41 = i8;
                                }
                            }
                            i6 = max3;
                            z10 = z;
                            i41 = i8;
                        }
                    } else {
                        i9 = -1;
                    }
                    z = false;
                    if (z13) {
                    }
                    i6 = max3;
                    z10 = z;
                    i41 = i8;
                }
                i42++;
                virtualChildCount = i7;
                int i48 = i;
            }
            i5 = virtualChildCount;
            this.mTotalLength += getPaddingLeft() + getPaddingRight();
            if (iArr2[1] == -1 && iArr2[0] == -1 && iArr2[2] == -1 && iArr2[3] == -1) {
                i35 = i43;
            } else {
                i35 = Math.max(i43, Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))) + Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))));
            }
        } else {
            int max4 = Math.max(i36, i37);
            if (z12 && i4 != 1073741824) {
                for (int i49 = 0; i49 < virtualChildCount; i49++) {
                    View virtualChildAt4 = getVirtualChildAt(i49);
                    if (!(virtualChildAt4 == null || virtualChildAt4.getVisibility() == 8 || ((LayoutParams) virtualChildAt4.getLayoutParams()).weight <= 0.0f)) {
                        virtualChildAt4.measure(MeasureSpec.makeMeasureSpec(i26, 1073741824), MeasureSpec.makeMeasureSpec(virtualChildAt4.getMeasuredHeight(), 1073741824));
                    }
                }
            }
            i6 = max4;
            i5 = virtualChildCount;
        }
        if (z10 || mode2 == 1073741824) {
            i6 = i35;
        }
        setMeasuredDimension(resolveSizeAndState | (-16777216 & i38), ViewCompat.resolveSizeAndState(Math.max(i6 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i19, i38 << 16));
        if (z9) {
            forceUniformHeight(i5, i);
        }
    }

    private void forceUniformHeight(int i, int i2) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
        for (int i3 = 0; i3 < i; i3++) {
            View virtualChildAt = getVirtualChildAt(i3);
            if (virtualChildAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (layoutParams.height == -1) {
                    int i4 = layoutParams.width;
                    layoutParams.width = virtualChildAt.getMeasuredWidth();
                    measureChildWithMargins(virtualChildAt, i2, 0, makeMeasureSpec, 0);
                    layoutParams.width = i4;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void measureChildBeforeLayout(View view, int i, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.mOrientation == 1) {
            layoutVertical(i, i2, i3, i4);
        } else {
            layoutHorizontal(i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: 0000 */
    public void layoutVertical(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int paddingLeft = getPaddingLeft();
        int i8 = i3 - i;
        int paddingRight = i8 - getPaddingRight();
        int paddingRight2 = (i8 - paddingLeft) - getPaddingRight();
        int virtualChildCount = getVirtualChildCount();
        int i9 = this.mGravity & 112;
        int i10 = this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        if (i9 == 16) {
            i5 = (((i4 - i2) - this.mTotalLength) / 2) + getPaddingTop();
        } else if (i9 != 80) {
            i5 = getPaddingTop();
        } else {
            i5 = ((getPaddingTop() + i4) - i2) - this.mTotalLength;
        }
        int i11 = 0;
        while (i11 < virtualChildCount) {
            View virtualChildAt = getVirtualChildAt(i11);
            if (virtualChildAt == null) {
                i5 += measureNullChild(i11);
            } else if (virtualChildAt.getVisibility() != 8) {
                int measuredWidth = virtualChildAt.getMeasuredWidth();
                int measuredHeight = virtualChildAt.getMeasuredHeight();
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                int i12 = layoutParams.gravity;
                if (i12 < 0) {
                    i12 = i10;
                }
                int absoluteGravity = GravityCompat.getAbsoluteGravity(i12, ViewCompat.getLayoutDirection(this)) & 7;
                if (absoluteGravity == 1) {
                    i7 = ((((paddingRight2 - measuredWidth) / 2) + paddingLeft) + layoutParams.leftMargin) - layoutParams.rightMargin;
                } else if (absoluteGravity != 5) {
                    i7 = layoutParams.leftMargin + paddingLeft;
                } else {
                    i7 = (paddingRight - measuredWidth) - layoutParams.rightMargin;
                }
                int i13 = i7;
                if (hasDividerBeforeChildAt(i11)) {
                    i5 += this.mDividerHeight;
                }
                int i14 = i5 + layoutParams.topMargin;
                LayoutParams layoutParams2 = layoutParams;
                setChildFrame(virtualChildAt, i13, i14 + getLocationOffset(virtualChildAt), measuredWidth, measuredHeight);
                i11 += getChildrenSkipCount(virtualChildAt, i11);
                i5 = i14 + measuredHeight + layoutParams2.bottomMargin + getNextLocationOffset(virtualChildAt);
                i6 = 1;
                i11 += i6;
            }
            i6 = 1;
            i11 += i6;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010a  */
    public void layoutHorizontal(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z;
        int i11;
        boolean z2;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        int paddingTop = getPaddingTop();
        int i17 = i4 - i2;
        int paddingBottom = i17 - getPaddingBottom();
        int paddingBottom2 = (i17 - paddingTop) - getPaddingBottom();
        int virtualChildCount = getVirtualChildCount();
        int i18 = this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        int i19 = this.mGravity & 112;
        boolean z3 = this.mBaselineAligned;
        int[] iArr = this.mMaxAscent;
        int[] iArr2 = this.mMaxDescent;
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i18, ViewCompat.getLayoutDirection(this));
        boolean z4 = true;
        if (absoluteGravity == 1) {
            i5 = (((i3 - i) - this.mTotalLength) / 2) + getPaddingLeft();
        } else if (absoluteGravity != 5) {
            i5 = getPaddingLeft();
        } else {
            i5 = ((getPaddingLeft() + i3) - i) - this.mTotalLength;
        }
        if (isLayoutRtl) {
            i7 = virtualChildCount - 1;
            i6 = -1;
        } else {
            i7 = 0;
            i6 = 1;
        }
        int i20 = 0;
        while (i20 < virtualChildCount) {
            int i21 = i7 + (i6 * i20);
            View virtualChildAt = getVirtualChildAt(i21);
            if (virtualChildAt == null) {
                i5 += measureNullChild(i21);
                z2 = z4;
                i8 = paddingTop;
                i11 = virtualChildCount;
                i9 = i19;
            } else if (virtualChildAt.getVisibility() != 8) {
                int measuredWidth = virtualChildAt.getMeasuredWidth();
                int measuredHeight = virtualChildAt.getMeasuredHeight();
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (z3) {
                    i12 = i20;
                    i10 = virtualChildCount;
                    if (layoutParams.height != -1) {
                        i13 = virtualChildAt.getBaseline();
                        i14 = layoutParams.gravity;
                        if (i14 < 0) {
                            i14 = i19;
                        }
                        i15 = i14 & 112;
                        i9 = i19;
                        if (i15 != 16) {
                            z = true;
                            i16 = ((((paddingBottom2 - measuredHeight) / 2) + paddingTop) + layoutParams.topMargin) - layoutParams.bottomMargin;
                        } else if (i15 != 48) {
                            if (i15 != 80) {
                                i16 = paddingTop;
                            } else {
                                int i22 = (paddingBottom - measuredHeight) - layoutParams.bottomMargin;
                                if (i13 != -1) {
                                    i22 -= iArr2[2] - (virtualChildAt.getMeasuredHeight() - i13);
                                }
                                i16 = i22;
                            }
                            z = true;
                        } else {
                            int i23 = layoutParams.topMargin + paddingTop;
                            if (i13 != -1) {
                                z = true;
                                i23 += iArr[1] - i13;
                            } else {
                                z = true;
                            }
                            i16 = i23;
                        }
                        if (hasDividerBeforeChildAt(i21)) {
                            i5 += this.mDividerWidth;
                        }
                        int i24 = layoutParams.leftMargin + i5;
                        View view = virtualChildAt;
                        int i25 = i21;
                        int locationOffset = i24 + getLocationOffset(virtualChildAt);
                        int i26 = i12;
                        i8 = paddingTop;
                        LayoutParams layoutParams2 = layoutParams;
                        setChildFrame(virtualChildAt, locationOffset, i16, measuredWidth, measuredHeight);
                        View view2 = view;
                        i20 = i26 + getChildrenSkipCount(view2, i25);
                        i5 = i24 + measuredWidth + layoutParams2.rightMargin + getNextLocationOffset(view2);
                        i20++;
                        z4 = z;
                        virtualChildCount = i10;
                        i19 = i9;
                        paddingTop = i8;
                    }
                } else {
                    i12 = i20;
                    i10 = virtualChildCount;
                }
                i13 = -1;
                i14 = layoutParams.gravity;
                if (i14 < 0) {
                }
                i15 = i14 & 112;
                i9 = i19;
                if (i15 != 16) {
                }
                if (hasDividerBeforeChildAt(i21)) {
                }
                int i242 = layoutParams.leftMargin + i5;
                View view3 = virtualChildAt;
                int i252 = i21;
                int locationOffset2 = i242 + getLocationOffset(virtualChildAt);
                int i262 = i12;
                i8 = paddingTop;
                LayoutParams layoutParams22 = layoutParams;
                setChildFrame(virtualChildAt, locationOffset2, i16, measuredWidth, measuredHeight);
                View view22 = view3;
                i20 = i262 + getChildrenSkipCount(view22, i252);
                i5 = i242 + measuredWidth + layoutParams22.rightMargin + getNextLocationOffset(view22);
                i20++;
                z4 = z;
                virtualChildCount = i10;
                i19 = i9;
                paddingTop = i8;
            } else {
                int i27 = i20;
                i8 = paddingTop;
                i11 = virtualChildCount;
                i9 = i19;
                z2 = true;
            }
            i20++;
            z4 = z;
            virtualChildCount = i10;
            i19 = i9;
            paddingTop = i8;
        }
    }

    private void setChildFrame(View view, int i, int i2, int i3, int i4) {
        view.layout(i, i2, i3 + i, i4 + i2);
    }

    public void setOrientation(int i) {
        if (this.mOrientation != i) {
            this.mOrientation = i;
            requestLayout();
        }
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public void setGravity(int i) {
        if (this.mGravity != i) {
            if ((8388615 & i) == 0) {
                i |= 8388611;
            }
            if ((i & 112) == 0) {
                i |= 48;
            }
            this.mGravity = i;
            requestLayout();
        }
    }

    public void setHorizontalGravity(int i) {
        int i2 = i & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        if ((8388615 & this.mGravity) != i2) {
            this.mGravity = i2 | (this.mGravity & -8388616);
            requestLayout();
        }
    }

    public void setVerticalGravity(int i) {
        int i2 = i & 112;
        if ((this.mGravity & 112) != i2) {
            this.mGravity = i2 | (this.mGravity & -113);
            requestLayout();
        }
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        if (this.mOrientation == 0) {
            return new LayoutParams(-2, -2);
        }
        if (this.mOrientation == 1) {
            return new LayoutParams(-1, -2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(LinearLayoutCompat.class.getName());
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        if (VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(LinearLayoutCompat.class.getName());
        }
    }
}
