package com.google.android.gms.common.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGmsServiceBroker extends IInterface {

    public static abstract class Stub extends Binder implements IGmsServiceBroker {

        private static class zza implements IGmsServiceBroker {
            private final IBinder zza;

            zza(IBinder iBinder) {
                this.zza = iBinder;
            }

            public final IBinder asBinder() {
                return this.zza;
            }

            public final void getService(IGmsCallbacks iGmsCallbacks, GetServiceRequest getServiceRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(iGmsCallbacks != null ? iGmsCallbacks.asBinder() : null);
                    if (getServiceRequest != null) {
                        obtain.writeInt(1);
                        getServiceRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.zza.transact(46, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.google.android.gms.common.internal.IGmsServiceBroker");
        }

        public static IGmsServiceBroker asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof IGmsServiceBroker)) ? new zza(iBinder) : (IGmsServiceBroker) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* access modifiers changed from: protected */
        public void getLegacyService(int i, IGmsCallbacks iGmsCallbacks, int i2, String str, String str2, String[] strArr, Bundle bundle, IBinder iBinder, String str3, String str4) throws RemoteException {
            throw new UnsupportedOperationException();
        }

        /* JADX WARNING: type inference failed for: r0v1 */
        /* JADX WARNING: type inference failed for: r10v0, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r9v0, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r8v0, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r7v0, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r6v0, types: [java.lang.String[]] */
        /* JADX WARNING: type inference failed for: r9v1 */
        /* JADX WARNING: type inference failed for: r8v1 */
        /* JADX WARNING: type inference failed for: r7v1 */
        /* JADX WARNING: type inference failed for: r6v1 */
        /* JADX WARNING: type inference failed for: r10v1 */
        /* JADX WARNING: type inference failed for: r8v2 */
        /* JADX WARNING: type inference failed for: r7v2 */
        /* JADX WARNING: type inference failed for: r6v2 */
        /* JADX WARNING: type inference failed for: r9v2 */
        /* JADX WARNING: type inference failed for: r7v3 */
        /* JADX WARNING: type inference failed for: r6v3 */
        /* JADX WARNING: type inference failed for: r8v3 */
        /* JADX WARNING: type inference failed for: r6v4 */
        /* JADX WARNING: type inference failed for: r7v4 */
        /* JADX WARNING: type inference failed for: r6v5 */
        /* JADX WARNING: type inference failed for: r13v2, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r7v5 */
        /* JADX WARNING: type inference failed for: r6v6 */
        /* JADX WARNING: type inference failed for: r8v4 */
        /* JADX WARNING: type inference failed for: r15v12, types: [java.lang.String[]] */
        /* JADX WARNING: type inference failed for: r8v5 */
        /* JADX WARNING: type inference failed for: r7v6 */
        /* JADX WARNING: type inference failed for: r6v7 */
        /* JADX WARNING: type inference failed for: r9v3 */
        /* JADX WARNING: type inference failed for: r10v2 */
        /* JADX WARNING: type inference failed for: r6v8 */
        /* JADX WARNING: type inference failed for: r7v7 */
        /* JADX WARNING: type inference failed for: r8v6 */
        /* JADX WARNING: type inference failed for: r13v4, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r7v8 */
        /* JADX WARNING: type inference failed for: r6v9 */
        /* JADX WARNING: type inference failed for: r8v7 */
        /* JADX WARNING: type inference failed for: r6v10 */
        /* JADX WARNING: type inference failed for: r10v3 */
        /* JADX WARNING: type inference failed for: r9v4 */
        /* JADX WARNING: type inference failed for: r8v8 */
        /* JADX WARNING: type inference failed for: r7v9 */
        /* JADX WARNING: type inference failed for: r1v2 */
        /* JADX WARNING: type inference failed for: r6v11 */
        /* JADX WARNING: type inference failed for: r15v16, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r1v3, types: [java.lang.String[]] */
        /* JADX WARNING: type inference failed for: r9v5 */
        /* JADX WARNING: type inference failed for: r8v9 */
        /* JADX WARNING: type inference failed for: r7v10 */
        /* JADX WARNING: type inference failed for: r10v4 */
        /* JADX WARNING: type inference failed for: r9v6 */
        /* JADX WARNING: type inference failed for: r7v11 */
        /* JADX WARNING: type inference failed for: r8v10 */
        /* JADX WARNING: type inference failed for: r13v7, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r7v12 */
        /* JADX WARNING: type inference failed for: r9v7 */
        /* JADX WARNING: type inference failed for: r8v11 */
        /* JADX WARNING: type inference failed for: r1v4, types: [java.lang.String[]] */
        /* JADX WARNING: type inference failed for: r5v13, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r6v14, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r7v13, types: [java.lang.String] */
        /* JADX WARNING: type inference failed for: r9v8 */
        /* JADX WARNING: type inference failed for: r8v13 */
        /* JADX WARNING: type inference failed for: r10v5 */
        /* JADX WARNING: type inference failed for: r7v14 */
        /* JADX WARNING: type inference failed for: r13v9, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r9v9 */
        /* JADX WARNING: type inference failed for: r8v14 */
        /* JADX WARNING: type inference failed for: r10v6 */
        /* JADX WARNING: type inference failed for: r7v15 */
        /* JADX WARNING: type inference failed for: r13v10, types: [java.lang.String[]] */
        /* JADX WARNING: type inference failed for: r6v15 */
        /* JADX WARNING: type inference failed for: r7v16 */
        /* JADX WARNING: type inference failed for: r15v19, types: [android.os.IBinder] */
        /* JADX WARNING: type inference failed for: r8v15 */
        /* JADX WARNING: type inference failed for: r6v16 */
        /* JADX WARNING: type inference failed for: r7v17 */
        /* JADX WARNING: type inference failed for: r9v10 */
        /* JADX WARNING: type inference failed for: r13v12, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r7v18 */
        /* JADX WARNING: type inference failed for: r8v16 */
        /* JADX WARNING: type inference failed for: r6v17 */
        /* JADX WARNING: type inference failed for: r9v11 */
        /* JADX WARNING: type inference failed for: r0v4, types: [com.google.android.gms.common.internal.ValidateAccountRequest] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.common.internal.ValidateAccountRequest] */
        /* JADX WARNING: type inference failed for: r0v7, types: [com.google.android.gms.common.internal.GetServiceRequest] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.google.android.gms.common.internal.GetServiceRequest] */
        /* JADX WARNING: type inference failed for: r9v12 */
        /* JADX WARNING: type inference failed for: r8v17 */
        /* JADX WARNING: type inference failed for: r7v19 */
        /* JADX WARNING: type inference failed for: r6v18 */
        /* JADX WARNING: type inference failed for: r8v18 */
        /* JADX WARNING: type inference failed for: r7v20 */
        /* JADX WARNING: type inference failed for: r6v19 */
        /* JADX WARNING: type inference failed for: r7v21 */
        /* JADX WARNING: type inference failed for: r6v20 */
        /* JADX WARNING: type inference failed for: r6v21 */
        /* JADX WARNING: type inference failed for: r8v19 */
        /* JADX WARNING: type inference failed for: r7v22 */
        /* JADX WARNING: type inference failed for: r6v22 */
        /* JADX WARNING: type inference failed for: r9v13 */
        /* JADX WARNING: type inference failed for: r10v7 */
        /* JADX WARNING: type inference failed for: r9v14 */
        /* JADX WARNING: type inference failed for: r8v20 */
        /* JADX WARNING: type inference failed for: r7v23 */
        /* JADX WARNING: type inference failed for: r1v7 */
        /* JADX WARNING: type inference failed for: r9v15 */
        /* JADX WARNING: type inference failed for: r8v21 */
        /* JADX WARNING: type inference failed for: r7v24 */
        /* JADX WARNING: type inference failed for: r1v8 */
        /* JADX WARNING: type inference failed for: r1v9 */
        /* JADX WARNING: type inference failed for: r0v10 */
        /* JADX WARNING: type inference failed for: r0v11 */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x00fa, code lost:
            r6 = r1;
            r10 = r10;
            r9 = r9;
            r8 = r8;
            r7 = r7;
         */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v1
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.common.internal.GetServiceRequest, com.google.android.gms.common.internal.ValidateAccountRequest]
  uses: [com.google.android.gms.common.internal.ValidateAccountRequest, com.google.android.gms.common.internal.GetServiceRequest]
  mth insns count: 161
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 47 */
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            ? r10;
            ? r9;
            ? r8;
            ? r7;
            ? r6;
            String str;
            ? r92;
            ? r82;
            ? r72;
            ? r62;
            ? r83;
            ? r73;
            ? r63;
            String str2;
            ? r93;
            ? r74;
            ? r64;
            ? r65;
            ? r84;
            ? r75;
            ? r66;
            ? r67;
            ? r76;
            ? r85;
            ? r102;
            ? r94;
            ? r86;
            ? r77;
            ? r1;
            ? r87;
            ? r78;
            if (i > 16777215) {
                return super.onTransact(i, parcel, parcel2, i2);
            }
            parcel.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            IGmsCallbacks asInterface = com.google.android.gms.common.internal.IGmsCallbacks.Stub.asInterface(parcel.readStrongBinder());
            ? r0 = 0;
            if (i == 46) {
                if (parcel.readInt() != 0) {
                    r0 = (GetServiceRequest) GetServiceRequest.CREATOR.createFromParcel(parcel);
                }
                getService(asInterface, r0);
            } else if (i == 47) {
                if (parcel.readInt() != 0) {
                    r0 = (ValidateAccountRequest) ValidateAccountRequest.CREATOR.createFromParcel(parcel);
                }
                validateAccount(asInterface, r0);
            } else {
                int readInt = parcel.readInt();
                String readString = i != 4 ? parcel.readString() : null;
                if (!(i == 23 || i == 25 || i == 27)) {
                    if (i != 30) {
                        if (i != 34) {
                            if (!(i == 41 || i == 43)) {
                                switch (i) {
                                    case 1:
                                        ? readString2 = parcel.readString();
                                        ? createStringArray = parcel.createStringArray();
                                        str = parcel.readString();
                                        if (parcel.readInt() != 0) {
                                            r78 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                                            r92 = readString2;
                                            r87 = 0;
                                        } else {
                                            r92 = readString2;
                                            r78 = 0;
                                            r87 = 0;
                                        }
                                        r102 = r87;
                                        r1 = createStringArray;
                                        r94 = r92;
                                        r86 = r87;
                                        r77 = r78;
                                        break;
                                    case 2:
                                        break;
                                    default:
                                        switch (i) {
                                            case 5:
                                            case 6:
                                            case 7:
                                            case 8:
                                            case 11:
                                            case 12:
                                            case 13:
                                            case 14:
                                            case 15:
                                            case 16:
                                            case 17:
                                            case 18:
                                                break;
                                            case 9:
                                                String readString3 = parcel.readString();
                                                ? createStringArray2 = parcel.createStringArray();
                                                ? readString4 = parcel.readString();
                                                ? readStrongBinder = parcel.readStrongBinder();
                                                ? readString5 = parcel.readString();
                                                if (parcel.readInt() == 0) {
                                                    r94 = readString4;
                                                    r86 = readStrongBinder;
                                                    r102 = readString5;
                                                    str = readString3;
                                                    r77 = 0;
                                                    r1 = createStringArray2;
                                                    break;
                                                } else {
                                                    r94 = readString4;
                                                    r86 = readStrongBinder;
                                                    r102 = readString5;
                                                    r77 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                                                    str = readString3;
                                                    r1 = createStringArray2;
                                                    break;
                                                }
                                            case 10:
                                                String readString6 = parcel.readString();
                                                r64 = parcel.createStringArray();
                                                str2 = readString6;
                                                r74 = 0;
                                                break;
                                            case 19:
                                                ? readStrongBinder2 = parcel.readStrongBinder();
                                                if (parcel.readInt() == 0) {
                                                    r82 = readStrongBinder2;
                                                    str2 = null;
                                                    r62 = 0;
                                                    r72 = 0;
                                                    r93 = 0;
                                                    break;
                                                } else {
                                                    r72 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                                                    r82 = readStrongBinder2;
                                                    str2 = null;
                                                    r62 = 0;
                                                    r93 = 0;
                                                    break;
                                                }
                                            case 20:
                                                break;
                                            default:
                                                switch (i) {
                                                    case 37:
                                                    case 38:
                                                        break;
                                                }
                                        }
                                }
                            }
                        } else {
                            str2 = parcel.readString();
                            r65 = 0;
                            r74 = r65;
                            r64 = r65;
                            r83 = r74;
                            r73 = r74;
                            r63 = r64;
                            r93 = r83;
                            r82 = r83;
                            r72 = r73;
                            r62 = r63;
                            r10 = r92;
                            r9 = r92;
                            r8 = r82;
                            r7 = r72;
                            r6 = r62;
                            getLegacyService(i, asInterface, readInt, readString, str, r6, r7, r8, r9, r10);
                        }
                    }
                    ? createStringArray3 = parcel.createStringArray();
                    String readString7 = parcel.readString();
                    if (parcel.readInt() != 0) {
                        r76 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        r67 = createStringArray3;
                        r85 = 0;
                    } else {
                        r67 = createStringArray3;
                        r76 = 0;
                        r85 = 0;
                    }
                    ? r95 = r84;
                    r10 = r95;
                    str = readString7;
                    r8 = r84;
                    r7 = r75;
                    r6 = r66;
                    r9 = r95;
                    getLegacyService(i, asInterface, readInt, readString, str, r6, r7, r8, r9, r10);
                }
                if (parcel.readInt() != 0) {
                    r73 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    str2 = null;
                    r63 = 0;
                    r83 = 0;
                    r93 = r83;
                    r82 = r83;
                    r72 = r73;
                    r62 = r63;
                    r10 = r92;
                    r9 = r92;
                    r8 = r82;
                    r7 = r72;
                    r6 = r62;
                    getLegacyService(i, asInterface, readInt, readString, str, r6, r7, r8, r9, r10);
                }
                str2 = null;
                r65 = 0;
                r74 = r65;
                r64 = r65;
                r83 = r74;
                r73 = r74;
                r63 = r64;
                r93 = r83;
                r82 = r83;
                r72 = r73;
                r62 = r63;
                r10 = r92;
                r9 = r92;
                r8 = r82;
                r7 = r72;
                r6 = r62;
                getLegacyService(i, asInterface, readInt, readString, str, r6, r7, r8, r9, r10);
            }
            parcel2.writeNoException();
            return true;
        }

        /* access modifiers changed from: protected */
        public void validateAccount(IGmsCallbacks iGmsCallbacks, ValidateAccountRequest validateAccountRequest) throws RemoteException {
            throw new UnsupportedOperationException();
        }
    }

    void getService(IGmsCallbacks iGmsCallbacks, GetServiceRequest getServiceRequest) throws RemoteException;
}
