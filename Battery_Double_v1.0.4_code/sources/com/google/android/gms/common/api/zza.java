package com.google.android.gms.common.api;

import com.google.android.gms.common.api.PendingResult.StatusListener;

final class zza implements StatusListener {
    private final /* synthetic */ Batch zzch;

    zza(Batch batch) {
        this.zzch = batch;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
        return;
     */
    public final void onComplete(Status status) {
        synchronized (this.zzch.mLock) {
            if (!this.zzch.isCanceled()) {
                if (status.isCanceled()) {
                    this.zzch.zzcf = true;
                } else if (!status.isSuccess()) {
                    this.zzch.zzce = true;
                }
                this.zzch.zzcd = this.zzch.zzcd - 1;
                if (this.zzch.zzcd == 0) {
                    if (this.zzch.zzcf) {
                        zza.super.cancel();
                    } else {
                        this.zzch.setResult(new BatchResult(this.zzch.zzce ? new Status(13) : Status.RESULT_SUCCESS, this.zzch.zzcg));
                    }
                }
            }
        }
    }
}
