package com.google.android.gms.ads;

import com.mansoon.BatteryDouble.C0002R;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837538;
        public static final int adSizes = 2130837539;
        public static final int adUnitId = 2130837540;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131493129;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {C0002R.attr.adSize, C0002R.attr.adSizes, C0002R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
    }
}
