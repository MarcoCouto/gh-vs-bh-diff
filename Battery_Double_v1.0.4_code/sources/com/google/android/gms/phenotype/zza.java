package com.google.android.gms.phenotype;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class zza {
    private static final ConcurrentHashMap<Uri, zza> zzg = new ConcurrentHashMap<>();
    private static final String[] zzl = {"key", Param.VALUE};
    private final Uri uri;
    private final ContentResolver zzh;
    private final ContentObserver zzi;
    private final Object zzj = new Object();
    private volatile Map<String, String> zzk;

    private zza(ContentResolver contentResolver, Uri uri2) {
        this.zzh = contentResolver;
        this.uri = uri2;
        this.zzi = new zzb(this, null);
    }

    public static zza zza(ContentResolver contentResolver, Uri uri2) {
        zza zza = (zza) zzg.get(uri2);
        if (zza == null) {
            zza zza2 = new zza(contentResolver, uri2);
            zza zza3 = (zza) zzg.putIfAbsent(uri2, zza2);
            if (zza3 == null) {
                zza2.zzh.registerContentObserver(zza2.uri, false, zza2.zzi);
                return zza2;
            }
            zza = zza3;
        }
        return zza;
    }

    private final Map<String, String> zzc() {
        HashMap hashMap = new HashMap();
        Cursor query = this.zzh.query(this.uri, zzl, null, null, null);
        if (query == null) {
            return hashMap;
        }
        while (query.moveToNext()) {
            try {
                hashMap.put(query.getString(0), query.getString(1));
            } finally {
                query.close();
            }
        }
        return hashMap;
    }

    public final Map<String, String> zza() {
        Map<String, String> map;
        Map<String, String> zzc = PhenotypeFlag.zza("gms:phenotype:phenotype_flag:debug_disable_caching", false) ? zzc() : this.zzk;
        if (zzc != null) {
            return zzc;
        }
        synchronized (this.zzj) {
            map = this.zzk;
            if (map == null) {
                map = zzc();
                this.zzk = map;
            }
        }
        return map;
    }

    public final void zzb() {
        synchronized (this.zzj) {
            this.zzk = null;
        }
    }
}
