package com.google.android.gms.base;

import com.mansoon.BatteryDouble.C0002R;

public final class R {

    public static final class attr {
        public static final int buttonSize = 2130837573;
        public static final int circleCrop = 2130837581;
        public static final int colorScheme = 2130837596;
        public static final int imageAspectRatio = 2130837650;
        public static final int imageAspectRatioAdjust = 2130837651;
        public static final int scopeUris = 2130837755;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968617;
        public static final int common_google_signin_btn_text_dark_default = 2130968618;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968619;
        public static final int common_google_signin_btn_text_dark_focused = 2130968620;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968621;
        public static final int common_google_signin_btn_text_light = 2130968622;
        public static final int common_google_signin_btn_text_light_default = 2130968623;
        public static final int common_google_signin_btn_text_light_disabled = 2130968624;
        public static final int common_google_signin_btn_text_light_focused = 2130968625;
        public static final int common_google_signin_btn_text_light_pressed = 2130968626;
        public static final int common_google_signin_btn_tint = 2130968627;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099739;
        public static final int common_google_signin_btn_icon_dark = 2131099740;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099741;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099742;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131099743;
        public static final int common_google_signin_btn_icon_disabled = 2131099744;
        public static final int common_google_signin_btn_icon_light = 2131099745;
        public static final int common_google_signin_btn_icon_light_focused = 2131099746;
        public static final int common_google_signin_btn_icon_light_normal = 2131099747;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131099748;
        public static final int common_google_signin_btn_text_dark = 2131099749;
        public static final int common_google_signin_btn_text_dark_focused = 2131099750;
        public static final int common_google_signin_btn_text_dark_normal = 2131099751;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131099752;
        public static final int common_google_signin_btn_text_disabled = 2131099753;
        public static final int common_google_signin_btn_text_light = 2131099754;
        public static final int common_google_signin_btn_text_light_focused = 2131099755;
        public static final int common_google_signin_btn_text_light_normal = 2131099756;
        public static final int common_google_signin_btn_text_light_normal_background = 2131099757;
        public static final int googleg_disabled_color_18 = 2131099758;
        public static final int googleg_standard_color_18 = 2131099759;
    }

    public static final class id {
        public static final int adjust_height = 2131165212;
        public static final int adjust_width = 2131165213;
        public static final int auto = 2131165218;
        public static final int dark = 2131165244;
        public static final int icon_only = 2131165264;
        public static final int light = 2131165271;
        public static final int none = 2131165281;
        public static final int standard = 2131165326;
        public static final int wide = 2131165347;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131427358;
        public static final int common_google_play_services_enable_text = 2131427359;
        public static final int common_google_play_services_enable_title = 2131427360;
        public static final int common_google_play_services_install_button = 2131427361;
        public static final int common_google_play_services_install_text = 2131427362;
        public static final int common_google_play_services_install_title = 2131427363;
        public static final int common_google_play_services_notification_channel_name = 2131427364;
        public static final int common_google_play_services_notification_ticker = 2131427365;
        public static final int common_google_play_services_unsupported_text = 2131427367;
        public static final int common_google_play_services_update_button = 2131427368;
        public static final int common_google_play_services_update_text = 2131427369;
        public static final int common_google_play_services_update_title = 2131427370;
        public static final int common_google_play_services_updating_text = 2131427371;
        public static final int common_google_play_services_wear_update_text = 2131427372;
        public static final int common_open_on_phone = 2131427373;
        public static final int common_signin_button_text = 2131427374;
        public static final int common_signin_button_text_long = 2131427375;
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {C0002R.attr.circleCrop, C0002R.attr.imageAspectRatio, C0002R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {C0002R.attr.buttonSize, C0002R.attr.colorScheme, C0002R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
