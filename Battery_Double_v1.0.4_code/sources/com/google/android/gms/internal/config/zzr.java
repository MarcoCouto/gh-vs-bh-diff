package com.google.android.gms.internal.config;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl;

abstract class zzr<R extends Result> extends ApiMethodImpl<R, zzw> {
    public zzr(GoogleApiClient googleApiClient) {
        super(zze.API, googleApiClient);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ void doExecute(AnyClient anyClient) throws RemoteException {
        zzw zzw = (zzw) anyClient;
        zza(zzw.getContext(), (zzah) zzw.getService());
    }

    /* access modifiers changed from: protected */
    public abstract void zza(Context context, zzah zzah) throws RemoteException;
}
