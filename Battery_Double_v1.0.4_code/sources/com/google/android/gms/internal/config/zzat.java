package com.google.android.gms.internal.config;

import java.io.IOException;
import java.util.Arrays;

public final class zzat extends zzbb<zzat> {
    private static volatile zzat[] zzbi;
    public String zzbj;
    public byte[] zzbk;

    public zzat() {
        this.zzbj = "";
        this.zzbk = zzbk.zzde;
        this.zzci = null;
        this.zzcr = -1;
    }

    public static zzat[] zzv() {
        if (zzbi == null) {
            synchronized (zzbf.zzcq) {
                if (zzbi == null) {
                    zzbi = new zzat[0];
                }
            }
        }
        return zzbi;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzat)) {
            return false;
        }
        zzat zzat = (zzat) obj;
        if (this.zzbj == null) {
            if (zzat.zzbj != null) {
                return false;
            }
        } else if (!this.zzbj.equals(zzat.zzbj)) {
            return false;
        }
        if (!Arrays.equals(this.zzbk, zzat.zzbk)) {
            return false;
        }
        return (this.zzci == null || this.zzci.isEmpty()) ? zzat.zzci == null || zzat.zzci.isEmpty() : this.zzci.equals(zzat.zzci);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((527 + getClass().getName().hashCode()) * 31) + (this.zzbj == null ? 0 : this.zzbj.hashCode())) * 31) + Arrays.hashCode(this.zzbk)) * 31;
        if (this.zzci != null && !this.zzci.isEmpty()) {
            i = this.zzci.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzbh zza(zzay zzay) throws IOException {
        while (true) {
            int zzy = zzay.zzy();
            if (zzy == 0) {
                return this;
            }
            if (zzy == 10) {
                this.zzbj = zzay.readString();
            } else if (zzy == 18) {
                this.zzbk = zzay.readBytes();
            } else if (!super.zza(zzay, zzy)) {
                return this;
            }
        }
    }

    public final void zza(zzaz zzaz) throws IOException {
        if (this.zzbj != null && !this.zzbj.equals("")) {
            zzaz.zza(1, this.zzbj);
        }
        if (!Arrays.equals(this.zzbk, zzbk.zzde)) {
            zzaz.zza(2, this.zzbk);
        }
        super.zza(zzaz);
    }

    /* access modifiers changed from: protected */
    public final int zzu() {
        int zzu = super.zzu();
        if (this.zzbj != null && !this.zzbj.equals("")) {
            zzu += zzaz.zzb(1, this.zzbj);
        }
        if (Arrays.equals(this.zzbk, zzbk.zzde)) {
            return zzu;
        }
        return zzu + zzaz.zzl(2) + zzaz.zzb(this.zzbk);
    }
}
