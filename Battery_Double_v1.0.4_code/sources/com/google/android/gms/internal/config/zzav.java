package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzav extends zzbb<zzav> {
    private static volatile zzav[] zzbo;
    public String namespace;
    public zzat[] zzbp;

    public zzav() {
        this.namespace = "";
        this.zzbp = zzat.zzv();
        this.zzci = null;
        this.zzcr = -1;
    }

    public static zzav[] zzw() {
        if (zzbo == null) {
            synchronized (zzbf.zzcq) {
                if (zzbo == null) {
                    zzbo = new zzav[0];
                }
            }
        }
        return zzbo;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzav)) {
            return false;
        }
        zzav zzav = (zzav) obj;
        if (this.namespace == null) {
            if (zzav.namespace != null) {
                return false;
            }
        } else if (!this.namespace.equals(zzav.namespace)) {
            return false;
        }
        if (!zzbf.equals(this.zzbp, zzav.zzbp)) {
            return false;
        }
        return (this.zzci == null || this.zzci.isEmpty()) ? zzav.zzci == null || zzav.zzci.isEmpty() : this.zzci.equals(zzav.zzci);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((527 + getClass().getName().hashCode()) * 31) + (this.namespace == null ? 0 : this.namespace.hashCode())) * 31) + zzbf.hashCode(this.zzbp)) * 31;
        if (this.zzci != null && !this.zzci.isEmpty()) {
            i = this.zzci.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzbh zza(zzay zzay) throws IOException {
        while (true) {
            int zzy = zzay.zzy();
            if (zzy == 0) {
                return this;
            }
            if (zzy == 10) {
                this.namespace = zzay.readString();
            } else if (zzy == 18) {
                int zzb = zzbk.zzb(zzay, 18);
                int length = this.zzbp == null ? 0 : this.zzbp.length;
                zzat[] zzatArr = new zzat[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzbp, 0, zzatArr, 0, length);
                }
                while (length < zzatArr.length - 1) {
                    zzatArr[length] = new zzat();
                    zzay.zza(zzatArr[length]);
                    zzay.zzy();
                    length++;
                }
                zzatArr[length] = new zzat();
                zzay.zza(zzatArr[length]);
                this.zzbp = zzatArr;
            } else if (!super.zza(zzay, zzy)) {
                return this;
            }
        }
    }

    public final void zza(zzaz zzaz) throws IOException {
        if (this.namespace != null && !this.namespace.equals("")) {
            zzaz.zza(1, this.namespace);
        }
        if (this.zzbp != null && this.zzbp.length > 0) {
            for (zzat zzat : this.zzbp) {
                if (zzat != null) {
                    zzaz.zza(2, (zzbh) zzat);
                }
            }
        }
        super.zza(zzaz);
    }

    /* access modifiers changed from: protected */
    public final int zzu() {
        int zzu = super.zzu();
        if (this.namespace != null && !this.namespace.equals("")) {
            zzu += zzaz.zzb(1, this.namespace);
        }
        if (this.zzbp != null && this.zzbp.length > 0) {
            for (zzat zzat : this.zzbp) {
                if (zzat != null) {
                    zzu += zzaz.zzb(2, (zzbh) zzat);
                }
            }
        }
        return zzu;
    }
}
