package com.google.android.gms.internal.config;

import android.content.Context;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class zzan implements Runnable {
    private final Context mContext;
    private final zzar zzaj;
    private final zzao zzat;
    private final zzao zzau;
    private final zzao zzav;

    public zzan(Context context, zzao zzao, zzao zzao2, zzao zzao3, zzar zzar) {
        this.mContext = context;
        this.zzat = zzao;
        this.zzau = zzao2;
        this.zzav = zzao3;
        this.zzaj = zzar;
    }

    private static zzas zza(zzao zzao) {
        zzas zzas = new zzas();
        if (zzao.zzp() != null) {
            Map zzp = zzao.zzp();
            ArrayList arrayList = new ArrayList();
            if (zzp != null) {
                for (String str : zzp.keySet()) {
                    ArrayList arrayList2 = new ArrayList();
                    Map map = (Map) zzp.get(str);
                    if (map != null) {
                        for (String str2 : map.keySet()) {
                            zzat zzat2 = new zzat();
                            zzat2.zzbj = str2;
                            zzat2.zzbk = (byte[]) map.get(str2);
                            arrayList2.add(zzat2);
                        }
                    }
                    zzav zzav2 = new zzav();
                    zzav2.namespace = str;
                    zzav2.zzbp = (zzat[]) arrayList2.toArray(new zzat[arrayList2.size()]);
                    arrayList.add(zzav2);
                }
            }
            zzas.zzbg = (zzav[]) arrayList.toArray(new zzav[arrayList.size()]);
        }
        if (zzao.zzg() != null) {
            List zzg = zzao.zzg();
            zzas.zzbh = (byte[][]) zzg.toArray(new byte[zzg.size()][]);
        }
        zzas.timestamp = zzao.getTimestamp();
        return zzas;
    }

    public final void run() {
        zzaw zzaw = new zzaw();
        if (this.zzat != null) {
            zzaw.zzbq = zza(this.zzat);
        }
        if (this.zzau != null) {
            zzaw.zzbr = zza(this.zzau);
        }
        if (this.zzav != null) {
            zzaw.zzbs = zza(this.zzav);
        }
        if (this.zzaj != null) {
            zzau zzau2 = new zzau();
            zzau2.zzbl = this.zzaj.getLastFetchStatus();
            zzau2.zzbm = this.zzaj.isDeveloperModeEnabled();
            zzau2.zzbn = this.zzaj.zzt();
            zzaw.zzbt = zzau2;
        }
        if (!(this.zzaj == null || this.zzaj.zzr() == null)) {
            ArrayList arrayList = new ArrayList();
            Map zzr = this.zzaj.zzr();
            for (String str : zzr.keySet()) {
                if (zzr.get(str) != null) {
                    zzax zzax = new zzax();
                    zzax.namespace = str;
                    zzax.zzbw = ((zzal) zzr.get(str)).zzo();
                    zzax.resourceId = ((zzal) zzr.get(str)).getResourceId();
                    arrayList.add(zzax);
                }
            }
            zzaw.zzbu = (zzax[]) arrayList.toArray(new zzax[arrayList.size()]);
        }
        byte[] bArr = new byte[zzaw.zzai()];
        try {
            zzaz zzb = zzaz.zzb(bArr, 0, bArr.length);
            zzaw.zza(zzb);
            zzb.zzad();
            try {
                FileOutputStream openFileOutput = this.mContext.openFileOutput("persisted_config", 0);
                openFileOutput.write(bArr);
                openFileOutput.close();
            } catch (IOException e) {
                Log.e("AsyncPersisterTask", "Could not persist config.", e);
            }
        } catch (IOException e2) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e2);
        }
    }
}
