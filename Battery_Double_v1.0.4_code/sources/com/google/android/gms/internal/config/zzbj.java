package com.google.android.gms.internal.config;

import java.util.Arrays;

final class zzbj {
    final int tag;
    final byte[] zzcs;

    zzbj(int i, byte[] bArr) {
        this.tag = i;
        this.zzcs = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbj)) {
            return false;
        }
        zzbj zzbj = (zzbj) obj;
        return this.tag == zzbj.tag && Arrays.equals(this.zzcs, zzbj.zzcs);
    }

    public final int hashCode() {
        return ((527 + this.tag) * 31) + Arrays.hashCode(this.zzcs);
    }
}
