package com.google.android.gms.internal.config;

import com.google.android.gms.common.api.Status;
import java.util.Map;
import java.util.TreeMap;

final class zzt extends zzq {
    private final /* synthetic */ zzs zzp;

    zzt(zzs zzs) {
        this.zzp = zzs;
    }

    public final void zza(Status status, zzad zzad) {
        if (zzad.getStatusCode() == 6502 || zzad.getStatusCode() == 6507) {
            zzs zzs = this.zzp;
            zzu zzu = new zzu(zzo.zzd(zzad.getStatusCode()), zzo.zza(zzad), zzad.getThrottleEndTimeMillis(), zzo.zzb(zzad));
            zzs.setResult(zzu);
            return;
        }
        this.zzp.setResult(new zzu(zzo.zzd(zzad.getStatusCode()), (Map<String, TreeMap<String, byte[]>>) zzo.zza(zzad), zzo.zzb(zzad)));
    }
}
