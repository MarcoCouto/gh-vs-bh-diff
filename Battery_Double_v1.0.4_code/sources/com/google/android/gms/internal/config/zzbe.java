package com.google.android.gms.internal.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class zzbe implements Cloneable {
    private Object value;
    private zzbc<?, ?> zzco;
    private List<zzbj> zzcp = new ArrayList();

    zzbe() {
    }

    private final byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzu()];
        zza(zzaz.zza(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzaf */
    public final zzbe clone() {
        Object clone;
        zzbe zzbe = new zzbe();
        try {
            zzbe.zzco = this.zzco;
            if (this.zzcp == null) {
                zzbe.zzcp = null;
            } else {
                zzbe.zzcp.addAll(this.zzcp);
            }
            if (this.value != null) {
                if (this.value instanceof zzbh) {
                    clone = (zzbh) ((zzbh) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    clone = ((byte[]) this.value).clone();
                } else {
                    int i = 0;
                    if (this.value instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.value;
                        byte[][] bArr2 = new byte[bArr.length][];
                        zzbe.value = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.value instanceof boolean[]) {
                        clone = ((boolean[]) this.value).clone();
                    } else if (this.value instanceof int[]) {
                        clone = ((int[]) this.value).clone();
                    } else if (this.value instanceof long[]) {
                        clone = ((long[]) this.value).clone();
                    } else if (this.value instanceof float[]) {
                        clone = ((float[]) this.value).clone();
                    } else if (this.value instanceof double[]) {
                        clone = ((double[]) this.value).clone();
                    } else if (this.value instanceof zzbh[]) {
                        zzbh[] zzbhArr = (zzbh[]) this.value;
                        zzbh[] zzbhArr2 = new zzbh[zzbhArr.length];
                        zzbe.value = zzbhArr2;
                        while (i < zzbhArr.length) {
                            zzbhArr2[i] = (zzbh) zzbhArr[i].clone();
                            i++;
                        }
                    }
                }
                zzbe.value = clone;
                return zzbe;
            }
            return zzbe;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbe)) {
            return false;
        }
        zzbe zzbe = (zzbe) obj;
        if (this.value == null || zzbe.value == null) {
            if (this.zzcp != null && zzbe.zzcp != null) {
                return this.zzcp.equals(zzbe.zzcp);
            }
            try {
                return Arrays.equals(toByteArray(), zzbe.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.zzco != zzbe.zzco) {
            return false;
        } else {
            return !this.zzco.zzcj.isArray() ? this.value.equals(zzbe.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zzbe.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zzbe.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zzbe.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zzbe.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zzbe.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zzbe.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zzbe.value);
        }
    }

    public final int hashCode() {
        try {
            return 527 + Arrays.hashCode(toByteArray());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zza(zzaz zzaz) throws IOException {
        if (this.value != null) {
            throw new NoSuchMethodError();
        }
        for (zzbj zzbj : this.zzcp) {
            zzaz.zzm(zzbj.tag);
            zzaz.zzc(zzbj.zzcs);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zza(zzbj zzbj) throws IOException {
        if (this.zzcp != null) {
            this.zzcp.add(zzbj);
        } else if (this.value instanceof zzbh) {
            byte[] bArr = zzbj.zzcs;
            zzay zza = zzay.zza(bArr, 0, bArr.length);
            int zzz = zza.zzz();
            if (zzz != bArr.length - zzaz.zzj(zzz)) {
                throw zzbg.zzag();
            }
            zzbh zza2 = ((zzbh) this.value).zza(zza);
            this.zzco = this.zzco;
            this.value = zza2;
            this.zzcp = null;
        } else if (this.value instanceof zzbh[]) {
            Collections.singletonList(zzbj);
            throw new NoSuchMethodError();
        } else {
            Collections.singletonList(zzbj);
            throw new NoSuchMethodError();
        }
    }

    /* access modifiers changed from: 0000 */
    public final int zzu() {
        if (this.value != null) {
            throw new NoSuchMethodError();
        }
        int i = 0;
        for (zzbj zzbj : this.zzcp) {
            i += zzaz.zzn(zzbj.tag) + 0 + zzbj.zzcs.length;
        }
        return i;
    }
}
