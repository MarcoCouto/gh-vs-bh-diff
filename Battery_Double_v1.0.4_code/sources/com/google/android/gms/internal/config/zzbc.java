package com.google.android.gms.internal.config;

import com.google.android.gms.internal.config.zzbb;

public final class zzbc<M extends zzbb<M>, T> {
    protected final Class<T> zzcj;

    public final boolean equals(Object obj) {
        throw new NoSuchMethodError();
    }

    public final int hashCode() {
        throw new NoSuchMethodError();
    }
}
