package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzay {
    private final byte[] buffer;
    private final int zzbx;
    private final int zzby;
    private int zzbz;
    private int zzca;
    private int zzcb;
    private int zzcc;
    private int zzcd = Integer.MAX_VALUE;
    private int zzce;
    private int zzcf = 64;
    private int zzcg = 67108864;

    private zzay(byte[] bArr, int i, int i2) {
        this.buffer = bArr;
        this.zzbx = 0;
        int i3 = i2 + 0;
        this.zzbz = i3;
        this.zzby = i3;
        this.zzcb = 0;
    }

    public static zzay zza(byte[] bArr, int i, int i2) {
        return new zzay(bArr, 0, i2);
    }

    private final void zzab() {
        this.zzbz += this.zzca;
        int i = this.zzbz;
        if (i > this.zzcd) {
            this.zzca = i - this.zzcd;
            this.zzbz -= this.zzca;
            return;
        }
        this.zzca = 0;
    }

    private final byte zzac() throws IOException {
        if (this.zzcb == this.zzbz) {
            throw zzbg.zzag();
        }
        byte[] bArr = this.buffer;
        int i = this.zzcb;
        this.zzcb = i + 1;
        return bArr[i];
    }

    private final void zzg(int i) throws zzbg {
        if (this.zzcc != i) {
            throw new zzbg("Protocol message end-group tag did not match expected tag.");
        }
    }

    private final void zzi(int i) throws IOException {
        if (i < 0) {
            throw zzbg.zzah();
        } else if (this.zzcb + i > this.zzcd) {
            zzi(this.zzcd - this.zzcb);
            throw zzbg.zzag();
        } else if (i <= this.zzbz - this.zzcb) {
            this.zzcb += i;
        } else {
            throw zzbg.zzag();
        }
    }

    public final int getPosition() {
        return this.zzcb - this.zzbx;
    }

    public final byte[] readBytes() throws IOException {
        int zzz = zzz();
        if (zzz < 0) {
            throw zzbg.zzah();
        } else if (zzz == 0) {
            return zzbk.zzde;
        } else {
            if (zzz > this.zzbz - this.zzcb) {
                throw zzbg.zzag();
            }
            byte[] bArr = new byte[zzz];
            System.arraycopy(this.buffer, this.zzcb, bArr, 0, zzz);
            this.zzcb += zzz;
            return bArr;
        }
    }

    public final String readString() throws IOException {
        int zzz = zzz();
        if (zzz < 0) {
            throw zzbg.zzah();
        } else if (zzz > this.zzbz - this.zzcb) {
            throw zzbg.zzag();
        } else {
            String str = new String(this.buffer, this.zzcb, zzz, zzbf.UTF_8);
            this.zzcb += zzz;
            return str;
        }
    }

    public final void zza(zzbh zzbh) throws IOException {
        int zzz = zzz();
        if (this.zzce >= this.zzcf) {
            throw new zzbg("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
        } else if (zzz < 0) {
            throw zzbg.zzah();
        } else {
            int i = zzz + this.zzcb;
            int i2 = this.zzcd;
            if (i > i2) {
                throw zzbg.zzag();
            }
            this.zzcd = i;
            zzab();
            this.zzce++;
            zzbh.zza(this);
            zzg(0);
            this.zzce--;
            this.zzcd = i2;
            zzab();
        }
    }

    public final byte[] zza(int i, int i2) {
        if (i2 == 0) {
            return zzbk.zzde;
        }
        byte[] bArr = new byte[i2];
        System.arraycopy(this.buffer, this.zzbx + i, bArr, 0, i2);
        return bArr;
    }

    public final long zzaa() throws IOException {
        return (((long) zzac()) & 255) | ((((long) zzac()) & 255) << 8) | ((((long) zzac()) & 255) << 16) | ((((long) zzac()) & 255) << 24) | ((((long) zzac()) & 255) << 32) | ((((long) zzac()) & 255) << 40) | ((((long) zzac()) & 255) << 48) | ((((long) zzac()) & 255) << 56);
    }

    /* access modifiers changed from: 0000 */
    public final void zzb(int i, int i2) {
        if (i > this.zzcb - this.zzbx) {
            int i3 = this.zzcb - this.zzbx;
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i);
            sb.append(" is beyond current ");
            sb.append(i3);
            throw new IllegalArgumentException(sb.toString());
        } else if (i < 0) {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i);
            throw new IllegalArgumentException(sb2.toString());
        } else {
            this.zzcb = this.zzbx + i;
            this.zzcc = i2;
        }
    }

    public final boolean zzh(int i) throws IOException {
        int zzy;
        switch (i & 7) {
            case 0:
                zzz();
                return true;
            case 1:
                zzaa();
                return true;
            case 2:
                zzi(zzz());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                zzac();
                zzac();
                zzac();
                zzac();
                return true;
            default:
                throw new zzbg("Protocol message tag had invalid wire type.");
        }
        do {
            zzy = zzy();
            if (zzy != 0) {
            }
            zzg(((i >>> 3) << 3) | 4);
            return true;
        } while (zzh(zzy));
        zzg(((i >>> 3) << 3) | 4);
        return true;
    }

    public final int zzy() throws IOException {
        if (this.zzcb == this.zzbz) {
            this.zzcc = 0;
            return 0;
        }
        this.zzcc = zzz();
        if (this.zzcc != 0) {
            return this.zzcc;
        }
        throw new zzbg("Protocol message contained an invalid tag (zero).");
    }

    public final int zzz() throws IOException {
        int i;
        byte zzac = zzac();
        if (zzac >= 0) {
            return zzac;
        }
        byte b = zzac & Byte.MAX_VALUE;
        byte zzac2 = zzac();
        if (zzac2 >= 0) {
            i = zzac2 << 7;
        } else {
            b |= (zzac2 & Byte.MAX_VALUE) << 7;
            byte zzac3 = zzac();
            if (zzac3 >= 0) {
                i = zzac3 << 14;
            } else {
                b |= (zzac3 & Byte.MAX_VALUE) << 14;
                byte zzac4 = zzac();
                if (zzac4 >= 0) {
                    i = zzac4 << 21;
                } else {
                    byte b2 = b | ((zzac4 & Byte.MAX_VALUE) << 21);
                    byte zzac5 = zzac();
                    byte b3 = b2 | (zzac5 << 28);
                    if (zzac5 >= 0) {
                        return b3;
                    }
                    for (int i2 = 0; i2 < 5; i2++) {
                        if (zzac() >= 0) {
                            return b3;
                        }
                    }
                    throw new zzbg("CodedInputStream encountered a malformed varint.");
                }
            }
        }
        return b | i;
    }
}
