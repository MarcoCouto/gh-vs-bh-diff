package com.google.android.gms.internal.config;

import java.io.IOException;

public abstract class zzbh {
    protected volatile int zzcr = -1;

    public String toString() {
        return zzbi.zzb(this);
    }

    public abstract zzbh zza(zzay zzay) throws IOException;

    public void zza(zzaz zzaz) throws IOException {
    }

    /* renamed from: zzae */
    public zzbh clone() throws CloneNotSupportedException {
        return (zzbh) super.clone();
    }

    public final int zzai() {
        int zzu = zzu();
        this.zzcr = zzu;
        return zzu;
    }

    /* access modifiers changed from: protected */
    public int zzu() {
        return 0;
    }
}
