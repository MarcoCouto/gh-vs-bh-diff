package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzau extends zzbb<zzau> {
    public int zzbl;
    public boolean zzbm;
    public long zzbn;

    public zzau() {
        this.zzbl = 0;
        this.zzbm = false;
        this.zzbn = 0;
        this.zzci = null;
        this.zzcr = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzau)) {
            return false;
        }
        zzau zzau = (zzau) obj;
        if (this.zzbl == zzau.zzbl && this.zzbm == zzau.zzbm && this.zzbn == zzau.zzbn) {
            return (this.zzci == null || this.zzci.isEmpty()) ? zzau.zzci == null || zzau.zzci.isEmpty() : this.zzci.equals(zzau.zzci);
        }
        return false;
    }

    public final int hashCode() {
        return ((((((((527 + getClass().getName().hashCode()) * 31) + this.zzbl) * 31) + (this.zzbm ? 1231 : 1237)) * 31) + ((int) (this.zzbn ^ (this.zzbn >>> 32)))) * 31) + ((this.zzci == null || this.zzci.isEmpty()) ? 0 : this.zzci.hashCode());
    }

    public final /* synthetic */ zzbh zza(zzay zzay) throws IOException {
        while (true) {
            int zzy = zzay.zzy();
            if (zzy == 0) {
                return this;
            }
            if (zzy == 8) {
                this.zzbl = zzay.zzz();
            } else if (zzy == 16) {
                this.zzbm = zzay.zzz() != 0;
            } else if (zzy == 25) {
                this.zzbn = zzay.zzaa();
            } else if (!super.zza(zzay, zzy)) {
                return this;
            }
        }
    }

    public final void zza(zzaz zzaz) throws IOException {
        if (this.zzbl != 0) {
            zzaz.zzc(1, this.zzbl);
        }
        if (this.zzbm) {
            boolean z = this.zzbm;
            zzaz.zze(2, 0);
            zzaz.zza(z ? (byte) 1 : 0);
        }
        if (this.zzbn != 0) {
            zzaz.zza(3, this.zzbn);
        }
        super.zza(zzaz);
    }

    /* access modifiers changed from: protected */
    public final int zzu() {
        int zzu = super.zzu();
        if (this.zzbl != 0) {
            zzu += zzaz.zzd(1, this.zzbl);
        }
        if (this.zzbm) {
            zzu += zzaz.zzl(2) + 1;
        }
        return this.zzbn != 0 ? zzu + zzaz.zzl(3) + 8 : zzu;
    }
}
