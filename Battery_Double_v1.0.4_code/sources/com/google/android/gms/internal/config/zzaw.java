package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzaw extends zzbb<zzaw> {
    public zzas zzbq;
    public zzas zzbr;
    public zzas zzbs;
    public zzau zzbt;
    public zzax[] zzbu;

    public zzaw() {
        this.zzbq = null;
        this.zzbr = null;
        this.zzbs = null;
        this.zzbt = null;
        this.zzbu = zzax.zzx();
        this.zzci = null;
        this.zzcr = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzaw)) {
            return false;
        }
        zzaw zzaw = (zzaw) obj;
        if (this.zzbq == null) {
            if (zzaw.zzbq != null) {
                return false;
            }
        } else if (!this.zzbq.equals(zzaw.zzbq)) {
            return false;
        }
        if (this.zzbr == null) {
            if (zzaw.zzbr != null) {
                return false;
            }
        } else if (!this.zzbr.equals(zzaw.zzbr)) {
            return false;
        }
        if (this.zzbs == null) {
            if (zzaw.zzbs != null) {
                return false;
            }
        } else if (!this.zzbs.equals(zzaw.zzbs)) {
            return false;
        }
        if (this.zzbt == null) {
            if (zzaw.zzbt != null) {
                return false;
            }
        } else if (!this.zzbt.equals(zzaw.zzbt)) {
            return false;
        }
        if (!zzbf.equals(this.zzbu, zzaw.zzbu)) {
            return false;
        }
        return (this.zzci == null || this.zzci.isEmpty()) ? zzaw.zzci == null || zzaw.zzci.isEmpty() : this.zzci.equals(zzaw.zzci);
    }

    public final int hashCode() {
        int hashCode = 527 + getClass().getName().hashCode();
        zzas zzas = this.zzbq;
        int i = 0;
        int hashCode2 = (hashCode * 31) + (zzas == null ? 0 : zzas.hashCode());
        zzas zzas2 = this.zzbr;
        int hashCode3 = (hashCode2 * 31) + (zzas2 == null ? 0 : zzas2.hashCode());
        zzas zzas3 = this.zzbs;
        int hashCode4 = (hashCode3 * 31) + (zzas3 == null ? 0 : zzas3.hashCode());
        zzau zzau = this.zzbt;
        int hashCode5 = ((((hashCode4 * 31) + (zzau == null ? 0 : zzau.hashCode())) * 31) + zzbf.hashCode(this.zzbu)) * 31;
        if (this.zzci != null && !this.zzci.isEmpty()) {
            i = this.zzci.hashCode();
        }
        return hashCode5 + i;
    }

    public final /* synthetic */ zzbh zza(zzay zzay) throws IOException {
        zzbh zzbh;
        while (true) {
            int zzy = zzay.zzy();
            if (zzy == 0) {
                return this;
            }
            if (zzy == 10) {
                if (this.zzbq == null) {
                    this.zzbq = new zzas();
                }
                zzbh = this.zzbq;
            } else if (zzy == 18) {
                if (this.zzbr == null) {
                    this.zzbr = new zzas();
                }
                zzbh = this.zzbr;
            } else if (zzy == 26) {
                if (this.zzbs == null) {
                    this.zzbs = new zzas();
                }
                zzbh = this.zzbs;
            } else if (zzy == 34) {
                if (this.zzbt == null) {
                    this.zzbt = new zzau();
                }
                zzbh = this.zzbt;
            } else if (zzy == 42) {
                int zzb = zzbk.zzb(zzay, 42);
                int length = this.zzbu == null ? 0 : this.zzbu.length;
                zzax[] zzaxArr = new zzax[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzbu, 0, zzaxArr, 0, length);
                }
                while (length < zzaxArr.length - 1) {
                    zzaxArr[length] = new zzax();
                    zzay.zza(zzaxArr[length]);
                    zzay.zzy();
                    length++;
                }
                zzaxArr[length] = new zzax();
                zzay.zza(zzaxArr[length]);
                this.zzbu = zzaxArr;
            } else if (!super.zza(zzay, zzy)) {
                return this;
            }
            zzay.zza(zzbh);
        }
    }

    public final void zza(zzaz zzaz) throws IOException {
        if (this.zzbq != null) {
            zzaz.zza(1, (zzbh) this.zzbq);
        }
        if (this.zzbr != null) {
            zzaz.zza(2, (zzbh) this.zzbr);
        }
        if (this.zzbs != null) {
            zzaz.zza(3, (zzbh) this.zzbs);
        }
        if (this.zzbt != null) {
            zzaz.zza(4, (zzbh) this.zzbt);
        }
        if (this.zzbu != null && this.zzbu.length > 0) {
            for (zzax zzax : this.zzbu) {
                if (zzax != null) {
                    zzaz.zza(5, (zzbh) zzax);
                }
            }
        }
        super.zza(zzaz);
    }

    /* access modifiers changed from: protected */
    public final int zzu() {
        int zzu = super.zzu();
        if (this.zzbq != null) {
            zzu += zzaz.zzb(1, (zzbh) this.zzbq);
        }
        if (this.zzbr != null) {
            zzu += zzaz.zzb(2, (zzbh) this.zzbr);
        }
        if (this.zzbs != null) {
            zzu += zzaz.zzb(3, (zzbh) this.zzbs);
        }
        if (this.zzbt != null) {
            zzu += zzaz.zzb(4, (zzbh) this.zzbt);
        }
        if (this.zzbu != null && this.zzbu.length > 0) {
            for (zzax zzax : this.zzbu) {
                if (zzax != null) {
                    zzu += zzaz.zzb(5, (zzbh) zzax);
                }
            }
        }
        return zzu;
    }
}
