package com.google.android.gms.internal.config;

import com.google.android.gms.internal.config.zzbb;
import java.io.IOException;

public abstract class zzbb<M extends zzbb<M>> extends zzbh {
    protected zzbd zzci;

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzbb zzbb = (zzbb) super.clone();
        zzbf.zza(this, zzbb);
        return zzbb;
    }

    public void zza(zzaz zzaz) throws IOException {
        if (this.zzci != null) {
            for (int i = 0; i < this.zzci.size(); i++) {
                this.zzci.zzp(i).zza(zzaz);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzay zzay, int i) throws IOException {
        int position = zzay.getPosition();
        if (!zzay.zzh(i)) {
            return false;
        }
        int i2 = i >>> 3;
        zzbj zzbj = new zzbj(i, zzay.zza(position, zzay.getPosition() - position));
        zzbe zzbe = null;
        if (this.zzci == null) {
            this.zzci = new zzbd();
        } else {
            zzbe = this.zzci.zzo(i2);
        }
        if (zzbe == null) {
            zzbe = new zzbe();
            this.zzci.zza(i2, zzbe);
        }
        zzbe.zza(zzbj);
        return true;
    }

    public final /* synthetic */ zzbh zzae() throws CloneNotSupportedException {
        return (zzbb) clone();
    }

    /* access modifiers changed from: protected */
    public int zzu() {
        if (this.zzci == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.zzci.size(); i2++) {
            i += this.zzci.zzp(i2).zzu();
        }
        return i;
    }
}
