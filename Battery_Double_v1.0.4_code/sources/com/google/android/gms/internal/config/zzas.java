package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzas extends zzbb<zzas> {
    public long timestamp;
    public zzav[] zzbg;
    public byte[][] zzbh;

    public zzas() {
        this.zzbg = zzav.zzw();
        this.timestamp = 0;
        this.zzbh = zzbk.zzdd;
        this.zzci = null;
        this.zzcr = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzas)) {
            return false;
        }
        zzas zzas = (zzas) obj;
        if (zzbf.equals(this.zzbg, zzas.zzbg) && this.timestamp == zzas.timestamp && zzbf.zza(this.zzbh, zzas.zzbh)) {
            return (this.zzci == null || this.zzci.isEmpty()) ? zzas.zzci == null || zzas.zzci.isEmpty() : this.zzci.equals(zzas.zzci);
        }
        return false;
    }

    public final int hashCode() {
        return ((((((((527 + getClass().getName().hashCode()) * 31) + zzbf.hashCode(this.zzbg)) * 31) + ((int) (this.timestamp ^ (this.timestamp >>> 32)))) * 31) + zzbf.zza(this.zzbh)) * 31) + ((this.zzci == null || this.zzci.isEmpty()) ? 0 : this.zzci.hashCode());
    }

    public final /* synthetic */ zzbh zza(zzay zzay) throws IOException {
        while (true) {
            int zzy = zzay.zzy();
            if (zzy == 0) {
                return this;
            }
            if (zzy == 10) {
                int zzb = zzbk.zzb(zzay, 10);
                int length = this.zzbg == null ? 0 : this.zzbg.length;
                zzav[] zzavArr = new zzav[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzbg, 0, zzavArr, 0, length);
                }
                while (length < zzavArr.length - 1) {
                    zzavArr[length] = new zzav();
                    zzay.zza(zzavArr[length]);
                    zzay.zzy();
                    length++;
                }
                zzavArr[length] = new zzav();
                zzay.zza(zzavArr[length]);
                this.zzbg = zzavArr;
            } else if (zzy == 17) {
                this.timestamp = zzay.zzaa();
            } else if (zzy == 26) {
                int zzb2 = zzbk.zzb(zzay, 26);
                int length2 = this.zzbh == null ? 0 : this.zzbh.length;
                byte[][] bArr = new byte[(zzb2 + length2)][];
                if (length2 != 0) {
                    System.arraycopy(this.zzbh, 0, bArr, 0, length2);
                }
                while (length2 < bArr.length - 1) {
                    bArr[length2] = zzay.readBytes();
                    zzay.zzy();
                    length2++;
                }
                bArr[length2] = zzay.readBytes();
                this.zzbh = bArr;
            } else if (!super.zza(zzay, zzy)) {
                return this;
            }
        }
    }

    public final void zza(zzaz zzaz) throws IOException {
        if (this.zzbg != null && this.zzbg.length > 0) {
            for (zzav zzav : this.zzbg) {
                if (zzav != null) {
                    zzaz.zza(1, (zzbh) zzav);
                }
            }
        }
        if (this.timestamp != 0) {
            zzaz.zza(2, this.timestamp);
        }
        if (this.zzbh != null && this.zzbh.length > 0) {
            for (byte[] bArr : this.zzbh) {
                if (bArr != null) {
                    zzaz.zza(3, bArr);
                }
            }
        }
        super.zza(zzaz);
    }

    /* access modifiers changed from: protected */
    public final int zzu() {
        int zzu = super.zzu();
        if (this.zzbg != null && this.zzbg.length > 0) {
            int i = zzu;
            for (zzav zzav : this.zzbg) {
                if (zzav != null) {
                    i += zzaz.zzb(1, (zzbh) zzav);
                }
            }
            zzu = i;
        }
        if (this.timestamp != 0) {
            zzu += zzaz.zzl(2) + 8;
        }
        if (this.zzbh == null || this.zzbh.length <= 0) {
            return zzu;
        }
        int i2 = 0;
        int i3 = 0;
        for (byte[] bArr : this.zzbh) {
            if (bArr != null) {
                i3++;
                i2 += zzaz.zzb(bArr);
            }
        }
        return zzu + i2 + (1 * i3);
    }
}
