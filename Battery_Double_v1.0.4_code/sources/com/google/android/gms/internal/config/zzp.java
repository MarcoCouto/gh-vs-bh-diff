package com.google.android.gms.internal.config;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.DataHolder.Builder;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.HashMap;
import java.util.Map.Entry;

final class zzp extends zzs {
    private final /* synthetic */ zzi zzn;

    zzp(zzo zzo, GoogleApiClient googleApiClient, zzi zzi) {
        this.zzn = zzi;
        super(googleApiClient);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return new zzu(status, new HashMap());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
    public final void zza(Context context, zzah zzah) throws RemoteException {
        String str;
        String str2;
        String str3;
        Throwable th;
        Builder buildDataHolder = DataBufferSafeParcelable.buildDataHolder();
        for (Entry entry : this.zzn.zzb().entrySet()) {
            DataBufferSafeParcelable.addValue(buildDataHolder, new zzz((String) entry.getKey(), (String) entry.getValue()));
        }
        DataHolder build = buildDataHolder.build(0);
        try {
            String id = FirebaseInstanceId.getInstance().getId();
            try {
                str2 = id;
                str = FirebaseInstanceId.getInstance().getToken();
            } catch (IllegalStateException e) {
                str3 = id;
                th = e;
                if (Log.isLoggable("ConfigApiImpl", 3)) {
                }
                str = null;
                str2 = str3;
                DataHolder dataHolder = build;
                zzab zzab = new zzab(context.getPackageName(), this.zzn.zza(), dataHolder, this.zzn.getGmpAppId(), str2, str, null, this.zzn.zzc(), zzn.zzb(context), this.zzn.zzd(), this.zzn.zze());
                zzah.zza(this.zzo, zzab);
                build.close();
            }
        } catch (IllegalStateException e2) {
            th = e2;
            str3 = null;
            if (Log.isLoggable("ConfigApiImpl", 3)) {
                Log.d("ConfigApiImpl", "Cannot retrieve instanceId or instanceIdToken.", th);
            }
            str = null;
            str2 = str3;
            DataHolder dataHolder2 = build;
            zzab zzab2 = new zzab(context.getPackageName(), this.zzn.zza(), dataHolder2, this.zzn.getGmpAppId(), str2, str, null, this.zzn.zzc(), zzn.zzb(context), this.zzn.zzd(), this.zzn.zze());
            zzah.zza(this.zzo, zzab2);
            build.close();
        }
        DataHolder dataHolder22 = build;
        zzab zzab22 = new zzab(context.getPackageName(), this.zzn.zza(), dataHolder22, this.zzn.getGmpAppId(), str2, str, null, this.zzn.zzc(), zzn.zzb(context), this.zzn.zzd(), this.zzn.zze());
        try {
            zzah.zza(this.zzo, zzab22);
            build.close();
        } catch (Throwable th2) {
            Throwable th3 = th2;
            build.close();
            throw th3;
        }
    }
}
