package com.google.android.gms.internal.config;

public final class zzbd implements Cloneable {
    private static final zzbe zzck = new zzbe();
    private int mSize;
    private boolean zzcl;
    private int[] zzcm;
    private zzbe[] zzcn;

    zzbd() {
        this(10);
    }

    private zzbd(int i) {
        this.zzcl = false;
        int idealIntArraySize = idealIntArraySize(i);
        this.zzcm = new int[idealIntArraySize];
        this.zzcn = new zzbe[idealIntArraySize];
        this.mSize = 0;
    }

    private static int idealIntArraySize(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        return i2 / 4;
    }

    private final int zzq(int i) {
        int i2 = this.mSize - 1;
        int i3 = 0;
        while (i3 <= i2) {
            int i4 = (i3 + i2) >>> 1;
            int i5 = this.zzcm[i4];
            if (i5 < i) {
                i3 = i4 + 1;
            } else if (i5 <= i) {
                return i4;
            } else {
                i2 = i4 - 1;
            }
        }
        return i3 ^ -1;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.mSize;
        zzbd zzbd = new zzbd(i);
        System.arraycopy(this.zzcm, 0, zzbd.zzcm, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            if (this.zzcn[i2] != null) {
                zzbd.zzcn[i2] = (zzbe) this.zzcn[i2].clone();
            }
        }
        zzbd.mSize = i;
        return zzbd;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbd)) {
            return false;
        }
        zzbd zzbd = (zzbd) obj;
        if (this.mSize != zzbd.mSize) {
            return false;
        }
        int[] iArr = this.zzcm;
        int[] iArr2 = zzbd.zzcm;
        int i = this.mSize;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            zzbe[] zzbeArr = this.zzcn;
            zzbe[] zzbeArr2 = zzbd.zzcn;
            int i3 = this.mSize;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!zzbeArr[i4].equals(zzbeArr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.mSize; i2++) {
            i = (((i * 31) + this.zzcm[i2]) * 31) + this.zzcn[i2].hashCode();
        }
        return i;
    }

    public final boolean isEmpty() {
        return this.mSize == 0;
    }

    /* access modifiers changed from: 0000 */
    public final int size() {
        return this.mSize;
    }

    /* access modifiers changed from: 0000 */
    public final void zza(int i, zzbe zzbe) {
        int zzq = zzq(i);
        if (zzq >= 0) {
            this.zzcn[zzq] = zzbe;
            return;
        }
        int i2 = zzq ^ -1;
        if (i2 >= this.mSize || this.zzcn[i2] != zzck) {
            if (this.mSize >= this.zzcm.length) {
                int idealIntArraySize = idealIntArraySize(this.mSize + 1);
                int[] iArr = new int[idealIntArraySize];
                zzbe[] zzbeArr = new zzbe[idealIntArraySize];
                System.arraycopy(this.zzcm, 0, iArr, 0, this.zzcm.length);
                System.arraycopy(this.zzcn, 0, zzbeArr, 0, this.zzcn.length);
                this.zzcm = iArr;
                this.zzcn = zzbeArr;
            }
            if (this.mSize - i2 != 0) {
                int i3 = i2 + 1;
                System.arraycopy(this.zzcm, i2, this.zzcm, i3, this.mSize - i2);
                System.arraycopy(this.zzcn, i2, this.zzcn, i3, this.mSize - i2);
            }
            this.zzcm[i2] = i;
            this.zzcn[i2] = zzbe;
            this.mSize++;
            return;
        }
        this.zzcm[i2] = i;
        this.zzcn[i2] = zzbe;
    }

    /* access modifiers changed from: 0000 */
    public final zzbe zzo(int i) {
        int zzq = zzq(i);
        if (zzq < 0 || this.zzcn[zzq] == zzck) {
            return null;
        }
        return this.zzcn[zzq];
    }

    /* access modifiers changed from: 0000 */
    public final zzbe zzp(int i) {
        return this.zzcn[i];
    }
}
