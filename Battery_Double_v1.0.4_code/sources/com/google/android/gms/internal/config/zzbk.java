package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzbk {
    private static final int zzct = 11;
    private static final int zzcu = 12;
    private static final int zzcv = 16;
    private static final int zzcw = 26;
    private static final int[] zzcx = new int[0];
    private static final long[] zzcy = new long[0];
    private static final float[] zzcz = new float[0];
    private static final double[] zzda = new double[0];
    private static final boolean[] zzdb = new boolean[0];
    private static final String[] zzdc = new String[0];
    public static final byte[][] zzdd = new byte[0][];
    public static final byte[] zzde = new byte[0];

    public static final int zzb(zzay zzay, int i) throws IOException {
        int position = zzay.getPosition();
        zzay.zzh(i);
        int i2 = 1;
        while (zzay.zzy() == i) {
            zzay.zzh(i);
            i2++;
        }
        zzay.zzb(position, i);
        return i2;
    }
}
