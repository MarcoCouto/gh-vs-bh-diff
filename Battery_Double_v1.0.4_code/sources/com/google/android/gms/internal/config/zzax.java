package com.google.android.gms.internal.config;

import java.io.IOException;

public final class zzax extends zzbb<zzax> {
    private static volatile zzax[] zzbv;
    public String namespace;
    public int resourceId;
    public long zzbw;

    public zzax() {
        this.resourceId = 0;
        this.zzbw = 0;
        this.namespace = "";
        this.zzci = null;
        this.zzcr = -1;
    }

    public static zzax[] zzx() {
        if (zzbv == null) {
            synchronized (zzbf.zzcq) {
                if (zzbv == null) {
                    zzbv = new zzax[0];
                }
            }
        }
        return zzbv;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzax)) {
            return false;
        }
        zzax zzax = (zzax) obj;
        if (this.resourceId != zzax.resourceId || this.zzbw != zzax.zzbw) {
            return false;
        }
        if (this.namespace == null) {
            if (zzax.namespace != null) {
                return false;
            }
        } else if (!this.namespace.equals(zzax.namespace)) {
            return false;
        }
        return (this.zzci == null || this.zzci.isEmpty()) ? zzax.zzci == null || zzax.zzci.isEmpty() : this.zzci.equals(zzax.zzci);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((527 + getClass().getName().hashCode()) * 31) + this.resourceId) * 31) + ((int) (this.zzbw ^ (this.zzbw >>> 32)))) * 31) + (this.namespace == null ? 0 : this.namespace.hashCode())) * 31;
        if (this.zzci != null && !this.zzci.isEmpty()) {
            i = this.zzci.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzbh zza(zzay zzay) throws IOException {
        while (true) {
            int zzy = zzay.zzy();
            if (zzy == 0) {
                return this;
            }
            if (zzy == 8) {
                this.resourceId = zzay.zzz();
            } else if (zzy == 17) {
                this.zzbw = zzay.zzaa();
            } else if (zzy == 26) {
                this.namespace = zzay.readString();
            } else if (!super.zza(zzay, zzy)) {
                return this;
            }
        }
    }

    public final void zza(zzaz zzaz) throws IOException {
        if (this.resourceId != 0) {
            zzaz.zzc(1, this.resourceId);
        }
        if (this.zzbw != 0) {
            zzaz.zza(2, this.zzbw);
        }
        if (this.namespace != null && !this.namespace.equals("")) {
            zzaz.zza(3, this.namespace);
        }
        super.zza(zzaz);
    }

    /* access modifiers changed from: protected */
    public final int zzu() {
        int zzu = super.zzu();
        if (this.resourceId != 0) {
            zzu += zzaz.zzd(1, this.resourceId);
        }
        if (this.zzbw != 0) {
            zzu += zzaz.zzl(2) + 8;
        }
        return (this.namespace == null || this.namespace.equals("")) ? zzu : zzu + zzaz.zzb(3, this.namespace);
    }
}
