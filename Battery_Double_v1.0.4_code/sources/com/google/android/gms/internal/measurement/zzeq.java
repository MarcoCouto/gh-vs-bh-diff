package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.Preconditions;

final class zzeq {
    final String name;
    final long zzafr;
    final long zzafs;
    final long zzaft;
    final long zzafu;
    final Long zzafv;
    final Long zzafw;
    final Boolean zzafx;
    final String zzti;

    zzeq(String str, String str2, long j, long j2, long j3, long j4, Long l, Long l2, Boolean bool) {
        long j5 = j;
        long j6 = j2;
        long j7 = j4;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        boolean z = false;
        Preconditions.checkArgument(j5 >= 0);
        Preconditions.checkArgument(j6 >= 0);
        if (j7 >= 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        this.zzti = str;
        this.name = str2;
        this.zzafr = j5;
        this.zzafs = j6;
        this.zzaft = j3;
        this.zzafu = j7;
        this.zzafv = l;
        this.zzafw = l2;
        this.zzafx = bool;
    }

    /* access modifiers changed from: 0000 */
    public final zzeq zza(Long l, Long l2, Boolean bool) {
        zzeq zzeq = new zzeq(this.zzti, this.name, this.zzafr, this.zzafs, this.zzaft, this.zzafu, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
        return zzeq;
    }

    /* access modifiers changed from: 0000 */
    public final zzeq zzac(long j) {
        zzeq zzeq = new zzeq(this.zzti, this.name, this.zzafr, this.zzafs, j, this.zzafu, this.zzafv, this.zzafw, this.zzafx);
        return zzeq;
    }

    /* access modifiers changed from: 0000 */
    public final zzeq zzad(long j) {
        zzeq zzeq = new zzeq(this.zzti, this.name, this.zzafr, this.zzafs, this.zzaft, j, this.zzafv, this.zzafw, this.zzafx);
        return zzeq;
    }

    /* access modifiers changed from: 0000 */
    public final zzeq zzie() {
        String str = this.zzti;
        String str2 = this.name;
        long j = this.zzafr + 1;
        long j2 = this.zzafs + 1;
        long j3 = this.zzaft;
        long j4 = this.zzafu;
        long j5 = j4;
        zzeq zzeq = new zzeq(str, str2, j, j2, j3, j5, this.zzafv, this.zzafw, this.zzafx);
        return zzeq;
    }
}
