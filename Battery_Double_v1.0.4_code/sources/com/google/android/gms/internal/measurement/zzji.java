package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.support.annotation.WorkerThread;

final class zzji extends zzem {
    private final /* synthetic */ zzjh zzapx;

    zzji(zzjh zzjh, zzhi zzhi) {
        this.zzapx = zzjh;
        super(zzhi);
    }

    @WorkerThread
    public final void run() {
        zzjh zzjh = this.zzapx;
        zzjh.zzab();
        zzjh.zzge().zzit().zzg("Session started, time", Long.valueOf(zzjh.zzbt().elapsedRealtime()));
        zzjh.zzgf().zzakk.set(false);
        zzjh.zzfu().zza("auto", "_s", new Bundle());
        zzjh.zzgf().zzakl.set(zzjh.zzbt().currentTimeMillis());
    }
}
