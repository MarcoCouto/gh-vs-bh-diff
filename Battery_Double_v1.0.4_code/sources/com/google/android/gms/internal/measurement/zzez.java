package com.google.android.gms.internal.measurement;

import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class zzez extends zzo implements zzey {
    public zzez() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00dc, code lost:
        r9.writeNoException();
        r9.writeTypedList(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0136, code lost:
        r9.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x013a, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        List list;
        switch (i) {
            case 1:
                zza((zzeu) zzp.zza(parcel, zzeu.CREATOR), (zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 2:
                zza((zzjx) zzp.zza(parcel, zzjx.CREATOR), (zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 4:
                zza((zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 5:
                zza((zzeu) zzp.zza(parcel, zzeu.CREATOR), parcel.readString(), parcel.readString());
                break;
            case 6:
                zzb((zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 7:
                list = zza((zzdz) zzp.zza(parcel, zzdz.CREATOR), zzp.zza(parcel));
                break;
            case 9:
                byte[] zza = zza((zzeu) zzp.zza(parcel, zzeu.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(zza);
                break;
            case 10:
                zza(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                break;
            case 11:
                String zzc = zzc((zzdz) zzp.zza(parcel, zzdz.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(zzc);
                break;
            case 12:
                zza((zzed) zzp.zza(parcel, zzed.CREATOR), (zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 13:
                zzb((zzed) zzp.zza(parcel, zzed.CREATOR));
                break;
            case 14:
                list = zza(parcel.readString(), parcel.readString(), zzp.zza(parcel), (zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 15:
                list = zza(parcel.readString(), parcel.readString(), parcel.readString(), zzp.zza(parcel));
                break;
            case 16:
                list = zza(parcel.readString(), parcel.readString(), (zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            case 17:
                list = zze(parcel.readString(), parcel.readString(), parcel.readString());
                break;
            case 18:
                zzd((zzdz) zzp.zza(parcel, zzdz.CREATOR));
                break;
            default:
                return false;
        }
    }
}
