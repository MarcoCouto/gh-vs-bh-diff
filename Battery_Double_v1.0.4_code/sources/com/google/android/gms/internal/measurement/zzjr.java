package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzjr implements zzec {
    private zzgl zzacw;
    zzgf zzaqa;
    zzfk zzaqb;
    private zzei zzaqc;
    private zzfp zzaqd;
    private zzjn zzaqe;
    private zzeb zzaqf;
    private boolean zzaqg;
    @VisibleForTesting
    private long zzaqh;
    private List<Runnable> zzaqi;
    private int zzaqj;
    private int zzaqk;
    private boolean zzaql;
    private boolean zzaqm;
    private boolean zzaqn;
    private FileLock zzaqo;
    private FileChannel zzaqp;
    private List<Long> zzaqq;
    private List<Long> zzaqr;
    long zzaqs;
    private boolean zzvo = false;

    @WorkerThread
    @VisibleForTesting
    private final int zza(FileChannel fileChannel) {
        zzab();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzge().zzim().log("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    zzge().zzip().zzg("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e) {
            zzge().zzim().zzg("Failed to read from channel", e);
            return 0;
        }
    }

    private final zzdz zza(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j) {
        int i;
        String str3 = str;
        String str4 = "Unknown";
        String str5 = "Unknown";
        String str6 = "Unknown";
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            zzge().zzim().log("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str3);
        } catch (IllegalArgumentException unused) {
            zzge().zzim().zzg("Error retrieving installer package name. appId", zzfg.zzbm(str));
        }
        if (str4 == null) {
            str4 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str4 = "";
        }
        String str7 = str4;
        try {
            PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo(str3, 0);
            if (packageInfo != null) {
                CharSequence applicationLabel = Wrappers.packageManager(context).getApplicationLabel(str3);
                if (!TextUtils.isEmpty(applicationLabel)) {
                    String charSequence = applicationLabel.toString();
                }
                str5 = packageInfo.versionName;
                i = packageInfo.versionCode;
            } else {
                i = Integer.MIN_VALUE;
            }
            zzdz zzdz = new zzdz(str3, str2, str5, (long) i, str7, 12451, zzgb().zzd(context, str3), (String) null, z, false, "", 0, zzgg().zzba(str3) ? j : 0, 0, z2, z3, false);
            return zzdz;
        } catch (NameNotFoundException unused2) {
            zzge().zzim().zze("Error retrieving newly installed package info. appId, appName", zzfg.zzbm(str), str6);
            return null;
        }
    }

    private static void zza(zzjq zzjq) {
        if (zzjq == null) {
            throw new IllegalStateException("Upload component not created");
        } else if (!zzjq.isInitialized()) {
            String valueOf = String.valueOf(zzjq.getClass());
            StringBuilder sb = new StringBuilder(27 + String.valueOf(valueOf).length());
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final boolean zza(int i, FileChannel fileChannel) {
        zzab();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzge().zzim().log("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                zzge().zzim().zzg("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e) {
            zzge().zzim().zzg("Failed to write to channel", e);
            return false;
        }
    }

    private final boolean zza(String str, zzeu zzeu) {
        long j;
        zzjz zzjz;
        String string = zzeu.zzafq.getString(Param.CURRENCY);
        if (Event.ECOMMERCE_PURCHASE.equals(zzeu.name)) {
            double doubleValue = zzeu.zzafq.zzbh(Param.VALUE).doubleValue() * 1000000.0d;
            if (doubleValue == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                doubleValue = ((double) zzeu.zzafq.getLong(Param.VALUE).longValue()) * 1000000.0d;
            }
            if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                zzge().zzip().zze("Data lost. Currency value is too big. appId", zzfg.zzbm(str), Double.valueOf(doubleValue));
                return false;
            }
            j = Math.round(doubleValue);
        } else {
            j = zzeu.zzafq.getLong(Param.VALUE).longValue();
        }
        if (!TextUtils.isEmpty(string)) {
            String upperCase = string.toUpperCase(Locale.US);
            if (upperCase.matches("[A-Z]{3}")) {
                String valueOf = String.valueOf("_ltv_");
                String valueOf2 = String.valueOf(upperCase);
                String concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                zzjz zzh = zzix().zzh(str, concat);
                if (zzh == null || !(zzh.value instanceof Long)) {
                    zzei zzix = zzix();
                    int zzb = zzgg().zzb(str, zzew.zzahm) - 1;
                    Preconditions.checkNotEmpty(str);
                    zzix.zzab();
                    zzix.zzch();
                    try {
                        zzix.getWritableDatabase().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str, str, String.valueOf(zzb)});
                    } catch (SQLiteException e) {
                        zzix.zzge().zzim().zze("Error pruning currencies. appId", zzfg.zzbm(str), e);
                    }
                    zzjz = new zzjz(str, zzeu.origin, concat, zzbt().currentTimeMillis(), Long.valueOf(j));
                } else {
                    zzjz zzjz2 = new zzjz(str, zzeu.origin, concat, zzbt().currentTimeMillis(), Long.valueOf(((Long) zzh.value).longValue() + j));
                    zzjz = zzjz2;
                }
                if (!zzix().zza(zzjz)) {
                    zzge().zzim().zzd("Too many unique user properties are set. Ignoring user property. appId", zzfg.zzbm(str), zzga().zzbl(zzjz.name), zzjz.value);
                    zzgb().zza(str, 9, (String) null, (String) null, 0);
                }
            }
        }
        return true;
    }

    private final zzkm[] zza(String str, zzks[] zzksArr, zzkn[] zzknArr) {
        Preconditions.checkNotEmpty(str);
        return zziw().zza(str, zzknArr, zzksArr);
    }

    @WorkerThread
    private final void zzb(zzdy zzdy) {
        Map map;
        zzab();
        if (TextUtils.isEmpty(zzdy.getGmpAppId())) {
            zzb(zzdy.zzah(), 204, null, null, null);
            return;
        }
        String gmpAppId = zzdy.getGmpAppId();
        String appInstanceId = zzdy.getAppInstanceId();
        Builder builder = new Builder();
        Builder encodedAuthority = builder.scheme((String) zzew.zzagm.get()).encodedAuthority((String) zzew.zzagn.get());
        String str = "config/app/";
        String valueOf = String.valueOf(gmpAppId);
        encodedAuthority.path(valueOf.length() != 0 ? str.concat(valueOf) : new String(str)).appendQueryParameter("app_instance_id", appInstanceId).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", "12451");
        String uri = builder.build().toString();
        try {
            URL url = new URL(uri);
            zzge().zzit().zzg("Fetching remote configuration", zzdy.zzah());
            zzkk zzbu = zzkm().zzbu(zzdy.zzah());
            String zzbv = zzkm().zzbv(zzdy.zzah());
            if (zzbu == null || TextUtils.isEmpty(zzbv)) {
                map = null;
            } else {
                ArrayMap arrayMap = new ArrayMap();
                arrayMap.put("If-Modified-Since", zzbv);
                map = arrayMap;
            }
            this.zzaql = true;
            zzfk zzkn = zzkn();
            String zzah = zzdy.zzah();
            zzjt zzjt = new zzjt(this);
            zzkn.zzab();
            zzkn.zzch();
            Preconditions.checkNotNull(url);
            Preconditions.checkNotNull(zzjt);
            zzgg zzgd = zzkn.zzgd();
            zzfo zzfo = new zzfo(zzkn, zzah, url, null, map, zzjt);
            zzgd.zzd((Runnable) zzfo);
        } catch (MalformedURLException unused) {
            zzge().zzim().zze("Failed to parse config URL. Not fetching. appId", zzfg.zzbm(zzdy.zzah()), uri);
        }
    }

    @WorkerThread
    private final Boolean zzc(zzdy zzdy) {
        try {
            if (zzdy.zzgm() != -2147483648L) {
                if (zzdy.zzgm() == ((long) Wrappers.packageManager(getContext()).getPackageInfo(zzdy.zzah(), 0).versionCode)) {
                    return Boolean.valueOf(true);
                }
            } else {
                String str = Wrappers.packageManager(getContext()).getPackageInfo(zzdy.zzah(), 0).versionName;
                if (zzdy.zzag() != null && zzdy.zzag().equals(str)) {
                    return Boolean.valueOf(true);
                }
            }
            return Boolean.valueOf(false);
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:144:0x0586 A[Catch:{ IOException -> 0x0589, all -> 0x05f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x05b0 A[Catch:{ IOException -> 0x0589, all -> 0x05f0 }] */
    @WorkerThread
    private final void zzc(zzeu zzeu, zzdz zzdz) {
        zzep zzep;
        zzeq zzeq;
        zzkq zzkq;
        zzeu zzeu2 = zzeu;
        zzdz zzdz2 = zzdz;
        Preconditions.checkNotNull(zzdz);
        Preconditions.checkNotEmpty(zzdz2.packageName);
        long nanoTime = System.nanoTime();
        zzab();
        zzkq();
        String str = zzdz2.packageName;
        zzgb();
        if (zzka.zzd(zzeu, zzdz)) {
            if (!zzdz2.zzadw) {
                zzg(zzdz2);
                return;
            }
            boolean z = true;
            if (zzkm().zzn(str, zzeu2.name)) {
                zzge().zzip().zze("Dropping blacklisted event. appId", zzfg.zzbm(str), zzga().zzbj(zzeu2.name));
                if (!zzkm().zzby(str) && !zzkm().zzbz(str)) {
                    z = false;
                }
                if (!z && !"_err".equals(zzeu2.name)) {
                    zzgb().zza(str, 11, "_ev", zzeu2.name, 0);
                }
                if (z) {
                    zzdy zzbc = zzix().zzbc(str);
                    if (zzbc != null) {
                        if (Math.abs(zzbt().currentTimeMillis() - Math.max(zzbc.zzgs(), zzbc.zzgr())) > ((Long) zzew.zzahh.get()).longValue()) {
                            zzge().zzis().log("Fetching config for blacklisted app");
                            zzb(zzbc);
                        }
                    }
                }
                return;
            }
            if (zzge().isLoggable(2)) {
                zzge().zzit().zzg("Logging event", zzga().zzb(zzeu2));
            }
            zzix().beginTransaction();
            zzg(zzdz2);
            if (("_iap".equals(zzeu2.name) || Event.ECOMMERCE_PURCHASE.equals(zzeu2.name)) && !zza(str, zzeu2)) {
                zzix().setTransactionSuccessful();
                zzix().endTransaction();
                return;
            }
            try {
                boolean zzcc = zzka.zzcc(zzeu2.name);
                boolean equals = "_err".equals(zzeu2.name);
                long j = nanoTime;
                zzej zza = zzix().zza(zzkr(), str, true, zzcc, false, equals, false);
                long intValue = zza.zzafe - ((long) ((Integer) zzew.zzags.get()).intValue());
                if (intValue > 0) {
                    if (intValue % 1000 == 1) {
                        zzge().zzim().zze("Data loss. Too many events logged. appId, count", zzfg.zzbm(str), Long.valueOf(zza.zzafe));
                    }
                    zzix().setTransactionSuccessful();
                    zzix().endTransaction();
                    return;
                }
                if (zzcc) {
                    long intValue2 = zza.zzafd - ((long) ((Integer) zzew.zzagu.get()).intValue());
                    if (intValue2 > 0) {
                        if (intValue2 % 1000 == 1) {
                            zzge().zzim().zze("Data loss. Too many public events logged. appId, count", zzfg.zzbm(str), Long.valueOf(zza.zzafd));
                        }
                        zzgb().zza(str, 16, "_ev", zzeu2.name, 0);
                        zzix().setTransactionSuccessful();
                        zzix().endTransaction();
                        return;
                    }
                }
                if (equals) {
                    long max = zza.zzafg - ((long) Math.max(0, Math.min(1000000, zzgg().zzb(zzdz2.packageName, zzew.zzagt))));
                    if (max > 0) {
                        if (max == 1) {
                            zzge().zzim().zze("Too many error events logged. appId, count", zzfg.zzbm(str), Long.valueOf(zza.zzafg));
                        }
                        zzix().setTransactionSuccessful();
                        zzix().endTransaction();
                        return;
                    }
                }
                Bundle zzif = zzeu2.zzafq.zzif();
                zzgb().zza(zzif, "_o", (Object) zzeu2.origin);
                if (zzgb().zzcj(str)) {
                    zzgb().zza(zzif, "_dbg", (Object) Long.valueOf(1));
                    zzgb().zza(zzif, "_r", (Object) Long.valueOf(1));
                }
                long zzbd = zzix().zzbd(str);
                if (zzbd > 0) {
                    zzge().zzip().zze("Data lost. Too many events stored on disk, deleted. appId", zzfg.zzbm(str), Long.valueOf(zzbd));
                }
                zzgl zzgl = this.zzacw;
                String str2 = zzeu2.origin;
                String str3 = zzeu2.name;
                long j2 = zzeu2.zzagb;
                zzep zzep2 = r6;
                String str4 = str;
                zzep zzep3 = new zzep(zzgl, str2, str, str3, j2, 0, zzif);
                zzeq zzf = zzix().zzf(str4, zzep2.name);
                if (zzf != null) {
                    zzep = zzep2.zza(this.zzacw, zzf.zzaft);
                    zzeq = zzf.zzac(zzep.timestamp);
                } else if (zzix().zzbg(str4) < 500 || !zzcc) {
                    zzeq = new zzeq(str4, zzep2.name, 0, 0, zzep2.timestamp, 0, null, null, null);
                    zzep = zzep2;
                } else {
                    zzge().zzim().zzd("Too many event names used, ignoring event. appId, name, supported count", zzfg.zzbm(str4), zzga().zzbj(zzep2.name), Integer.valueOf(500));
                    zzgb().zza(str4, 8, (String) null, (String) null, 0);
                    zzix().endTransaction();
                    return;
                }
                zzix().zza(zzeq);
                zzab();
                zzkq();
                Preconditions.checkNotNull(zzep);
                Preconditions.checkNotNull(zzdz);
                Preconditions.checkNotEmpty(zzep.zzti);
                Preconditions.checkArgument(zzep.zzti.equals(zzdz2.packageName));
                zzkq = new zzkq();
                boolean z2 = true;
                zzkq.zzath = Integer.valueOf(1);
                zzkq.zzatp = "android";
                zzkq.zzti = zzdz2.packageName;
                zzkq.zzadt = zzdz2.zzadt;
                zzkq.zzth = zzdz2.zzth;
                zzkq.zzaub = zzdz2.zzads == -2147483648L ? null : Integer.valueOf((int) zzdz2.zzads);
                zzkq.zzatt = Long.valueOf(zzdz2.zzadu);
                zzkq.zzadm = zzdz2.zzadm;
                zzkq.zzatx = zzdz2.zzadv == 0 ? null : Long.valueOf(zzdz2.zzadv);
                Pair zzbo = zzgf().zzbo(zzdz2.packageName);
                if (zzbo == null || TextUtils.isEmpty((CharSequence) zzbo.first)) {
                    if (!zzfw().zzf(getContext()) && zzdz2.zzadz) {
                        String string = Secure.getString(getContext().getContentResolver(), "android_id");
                        if (string == null) {
                            zzge().zzip().zzg("null secure ID. appId", zzfg.zzbm(zzkq.zzti));
                            string = "null";
                        } else if (string.isEmpty()) {
                            zzge().zzip().zzg("empty secure ID. appId", zzfg.zzbm(zzkq.zzti));
                        }
                        zzkq.zzaue = string;
                    }
                } else if (zzdz2.zzady) {
                    zzkq.zzatv = (String) zzbo.first;
                    zzkq.zzatw = (Boolean) zzbo.second;
                }
                zzfw().zzch();
                zzkq.zzatr = Build.MODEL;
                zzfw().zzch();
                zzkq.zzatq = VERSION.RELEASE;
                zzkq.zzats = Integer.valueOf((int) zzfw().zzic());
                zzkq.zzafn = zzfw().zzid();
                zzkq.zzatu = null;
                zzkq.zzatk = null;
                zzkq.zzatl = null;
                zzkq.zzatm = null;
                zzkq.zzaug = Long.valueOf(zzdz2.zzadx);
                if (this.zzacw.isEnabled() && zzef.zzhk()) {
                    zzkq.zzauh = null;
                }
                zzdy zzbc2 = zzix().zzbc(zzdz2.packageName);
                if (zzbc2 == null) {
                    zzbc2 = new zzdy(this.zzacw, zzdz2.packageName);
                    zzbc2.zzal(this.zzacw.zzfv().zzii());
                    zzbc2.zzao(zzdz2.zzado);
                    zzbc2.zzam(zzdz2.zzadm);
                    zzbc2.zzan(zzgf().zzbp(zzdz2.packageName));
                    zzbc2.zzr(0);
                    zzbc2.zzm(0);
                    zzbc2.zzn(0);
                    zzbc2.setAppVersion(zzdz2.zzth);
                    zzbc2.zzo(zzdz2.zzads);
                    zzbc2.zzap(zzdz2.zzadt);
                    zzbc2.zzp(zzdz2.zzadu);
                    zzbc2.zzq(zzdz2.zzadv);
                    zzbc2.setMeasurementEnabled(zzdz2.zzadw);
                    zzbc2.zzaa(zzdz2.zzadx);
                    zzix().zza(zzbc2);
                }
                zzkq.zzadl = zzbc2.getAppInstanceId();
                zzkq.zzado = zzbc2.zzgj();
                List zzbb = zzix().zzbb(zzdz2.packageName);
                zzkq.zzatj = new zzks[zzbb.size()];
                for (int i = 0; i < zzbb.size(); i++) {
                    zzks zzks = new zzks();
                    zzkq.zzatj[i] = zzks;
                    zzks.name = ((zzjz) zzbb.get(i)).name;
                    zzks.zzaun = Long.valueOf(((zzjz) zzbb.get(i)).zzaqz);
                    zzgb().zza(zzks, ((zzjz) zzbb.get(i)).value);
                }
                long zza2 = zzix().zza(zzkq);
                zzei zzix = zzix();
                if (zzep.zzafq != null) {
                    Iterator it = zzep.zzafq.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if ("_r".equals((String) it.next())) {
                                break;
                            }
                        } else {
                            boolean zzo = zzkm().zzo(zzep.zzti, zzep.name);
                            zzej zza3 = zzix().zza(zzkr(), zzep.zzti, false, false, false, false, false);
                            if (zzo && zza3.zzafh < ((long) zzgg().zzar(zzep.zzti))) {
                            }
                        }
                    }
                    if (zzix.zza(zzep, zza2, z2)) {
                        this.zzaqh = 0;
                    }
                    zzix().setTransactionSuccessful();
                    if (zzge().isLoggable(2)) {
                        zzge().zzit().zzg("Event recorded", zzga().zza(zzep));
                    }
                    zzix().endTransaction();
                    zzku();
                    zzge().zzit().zzg("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j) + 500000) / 1000000));
                }
                z2 = false;
                if (zzix.zza(zzep, zza2, z2)) {
                }
                zzix().setTransactionSuccessful();
                if (zzge().isLoggable(2)) {
                }
                zzix().endTransaction();
                zzku();
                zzge().zzit().zzg("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j) + 500000) / 1000000));
            } catch (IOException e) {
                zzge().zzim().zze("Data loss. Failed to insert raw event metadata. appId", zzfg.zzbm(zzkq.zzti), e);
            } catch (Throwable th) {
                Throwable th2 = th;
                zzix().endTransaction();
                throw th2;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0225, code lost:
        if (r5 != null) goto L_0x01d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0248, code lost:
        if (r6 == null) goto L_0x0287;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r2 = r0;
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0284, code lost:
        if (r6 != null) goto L_0x024a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        r6 = null;
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0087, code lost:
        if (r3 != null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a0, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01d7, code lost:
        if (r5 != null) goto L_0x01d9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[ExcHandler: all (r0v20 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r3 
  PHI: (r3v48 android.database.Cursor) = (r3v42 android.database.Cursor), (r3v53 android.database.Cursor), (r3v53 android.database.Cursor), (r3v53 android.database.Cursor), (r3v53 android.database.Cursor), (r3v1 android.database.Cursor), (r3v1 android.database.Cursor) binds: [B:48:0x00e5, B:25:0x0081, B:31:0x008e, B:33:0x0092, B:34:?, B:9:0x0031, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x028b A[Catch:{ SQLiteException -> 0x0aee, all -> 0x0b29 }] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0299 A[Catch:{ SQLiteException -> 0x0aee, all -> 0x0b29 }] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x055a A[Catch:{ SQLiteException -> 0x0aee, all -> 0x0b29 }] */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x0637 A[Catch:{ SQLiteException -> 0x0aee, all -> 0x0b29 }] */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x0651 A[Catch:{ SQLiteException -> 0x0aee, all -> 0x0b29 }] */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x0671 A[Catch:{ SQLiteException -> 0x0aee, all -> 0x0b29 }] */
    /* JADX WARNING: Removed duplicated region for block: B:434:0x0b11 A[SYNTHETIC, Splitter:B:434:0x0b11] */
    /* JADX WARNING: Removed duplicated region for block: B:441:0x0b25 A[SYNTHETIC, Splitter:B:441:0x0b25] */
    /* JADX WARNING: Removed duplicated region for block: B:463:0x064e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0125 A[SYNTHETIC, Splitter:B:61:0x0125] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0147 A[SYNTHETIC, Splitter:B:70:0x0147] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:146:0x0287=Splitter:B:146:0x0287, B:29:0x0089=Splitter:B:29:0x0089, B:99:0x01d9=Splitter:B:99:0x01d9, B:125:0x024a=Splitter:B:125:0x024a} */
    @WorkerThread
    private final boolean zzd(String str, long j) {
        Cursor cursor;
        Throwable th;
        boolean z;
        zzjv zzjv;
        zzkq zzkq;
        String str2;
        zzei zzix;
        Long valueOf;
        zzkq zzkq2;
        zzjv zzjv2;
        SecureRandom secureRandom;
        int i;
        zzkn[] zzknArr;
        boolean z2;
        int i2;
        zzjz zzjz;
        int i3;
        boolean z3;
        int i4;
        long j2;
        zzfi zzip;
        String str3;
        Object zzbm;
        int i5;
        boolean z4;
        String str4;
        Object obj;
        Cursor cursor2;
        String str5;
        String str6;
        Cursor query;
        String[] strArr;
        String str7;
        Cursor query2;
        String[] strArr2;
        zzix().beginTransaction();
        try {
            Cursor cursor3 = null;
            zzjv zzjv3 = new zzjv(this, null);
            zzei zzix2 = zzix();
            long j3 = this.zzaqs;
            Preconditions.checkNotNull(zzjv3);
            zzix2.zzab();
            zzix2.zzch();
            try {
                SQLiteDatabase writableDatabase = zzix2.getWritableDatabase();
                if (TextUtils.isEmpty(null)) {
                    if (j3 != -1) {
                        try {
                            strArr2 = new String[]{String.valueOf(j3), String.valueOf(j)};
                        } catch (SQLiteException e) {
                            e = e;
                            cursor = cursor3;
                            str4 = null;
                        } catch (Throwable th2) {
                        }
                    } else {
                        strArr2 = new String[]{String.valueOf(j)};
                    }
                    String str8 = j3 != -1 ? "rowid <= ? and " : "";
                    StringBuilder sb = new StringBuilder(148 + String.valueOf(str8).length());
                    sb.append("select app_id, metadata_fingerprint from raw_events where ");
                    sb.append(str8);
                    sb.append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;");
                    cursor3 = writableDatabase.rawQuery(sb.toString(), strArr2);
                    if (cursor3.moveToFirst()) {
                        str4 = cursor3.getString(0);
                        String string = cursor3.getString(1);
                        cursor3.close();
                        cursor2 = cursor3;
                        str6 = str4;
                        str5 = string;
                        try {
                            SQLiteDatabase sQLiteDatabase = writableDatabase;
                            query = writableDatabase.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str6, str5}, null, null, "rowid", "2");
                            try {
                                if (query.moveToFirst()) {
                                    try {
                                        zzix2.zzge().zzim().zzg("Raw event metadata record is missing. appId", zzfg.zzbm(str6));
                                        if (query != null) {
                                            query.close();
                                        }
                                    } catch (SQLiteException e2) {
                                        e = e2;
                                        str4 = str6;
                                        cursor = query;
                                        obj = e;
                                        try {
                                            zzix2.zzge().zzim().zze("Data loss. Error selecting raw event. appId", zzfg.zzbm(str4), obj);
                                        } catch (Throwable th3) {
                                            th = th3;
                                            th = th;
                                            if (cursor != null) {
                                                cursor.close();
                                            }
                                            throw th;
                                        }
                                    } catch (Throwable th4) {
                                        th = th4;
                                        cursor = query;
                                        if (cursor != null) {
                                        }
                                        throw th;
                                    }
                                    if (zzjv3.zzaqx != null) {
                                        if (!zzjv3.zzaqx.isEmpty()) {
                                            z = false;
                                            if (z) {
                                                zzkq zzkq3 = zzjv3.zzaqv;
                                                zzkq3.zzati = new zzkn[zzjv3.zzaqx.size()];
                                                boolean zzav = zzgg().zzav(zzkq3.zzti);
                                                int i6 = 0;
                                                boolean z5 = false;
                                                int i7 = 0;
                                                long j4 = 0;
                                                while (i6 < zzjv3.zzaqx.size()) {
                                                    zzkn zzkn = (zzkn) zzjv3.zzaqx.get(i6);
                                                    if (zzkm().zzn(zzjv3.zzaqv.zzti, zzkn.name)) {
                                                        zzge().zzip().zze("Dropping blacklisted raw event. appId", zzfg.zzbm(zzjv3.zzaqv.zzti), zzga().zzbj(zzkn.name));
                                                        if (!zzkm().zzby(zzjv3.zzaqv.zzti)) {
                                                            if (!zzkm().zzbz(zzjv3.zzaqv.zzti)) {
                                                                z4 = false;
                                                                if (!z4 && !"_err".equals(zzkn.name)) {
                                                                    zzgb().zza(zzjv3.zzaqv.zzti, 11, "_ev", zzkn.name, 0);
                                                                }
                                                                i4 = i6;
                                                            }
                                                        }
                                                        z4 = true;
                                                        zzgb().zza(zzjv3.zzaqv.zzti, 11, "_ev", zzkn.name, 0);
                                                        i4 = i6;
                                                    } else {
                                                        boolean zzo = zzkm().zzo(zzjv3.zzaqv.zzti, zzkn.name);
                                                        if (!zzo) {
                                                            zzgb();
                                                            if (!zzka.zzcl(zzkn.name)) {
                                                                i4 = i6;
                                                                if (zzav && "_e".equals(zzkn.name)) {
                                                                    if (zzkn.zzata != null) {
                                                                        if (zzkn.zzata.length != 0) {
                                                                            zzgb();
                                                                            Long l = (Long) zzka.zzb(zzkn, "_et");
                                                                            if (l == null) {
                                                                                zzip = zzge().zzip();
                                                                                str3 = "Engagement event does not include duration. appId";
                                                                                zzbm = zzfg.zzbm(zzjv3.zzaqv.zzti);
                                                                                zzip.zzg(str3, zzbm);
                                                                            } else {
                                                                                j2 = j4 + l.longValue();
                                                                                int i8 = i7 + 1;
                                                                                zzkq3.zzati[i7] = zzkn;
                                                                                i7 = i8;
                                                                                j4 = j2;
                                                                            }
                                                                        }
                                                                    }
                                                                    zzip = zzge().zzip();
                                                                    str3 = "Engagement event does not contain any parameters. appId";
                                                                    zzbm = zzfg.zzbm(zzjv3.zzaqv.zzti);
                                                                    zzip.zzg(str3, zzbm);
                                                                }
                                                                j2 = j4;
                                                                int i82 = i7 + 1;
                                                                zzkq3.zzati[i7] = zzkn;
                                                                i7 = i82;
                                                                j4 = j2;
                                                            }
                                                        }
                                                        if (zzkn.zzata == null) {
                                                            zzkn.zzata = new zzko[0];
                                                        }
                                                        zzko[] zzkoArr = zzkn.zzata;
                                                        int length = zzkoArr.length;
                                                        int i9 = 0;
                                                        boolean z6 = false;
                                                        boolean z7 = false;
                                                        while (i9 < length) {
                                                            int i10 = length;
                                                            zzko zzko = zzkoArr[i9];
                                                            zzko[] zzkoArr2 = zzkoArr;
                                                            boolean z8 = z5;
                                                            if ("_c".equals(zzko.name)) {
                                                                i5 = i6;
                                                                zzko.zzate = Long.valueOf(1);
                                                                z6 = true;
                                                            } else {
                                                                i5 = i6;
                                                                if ("_r".equals(zzko.name)) {
                                                                    zzko.zzate = Long.valueOf(1);
                                                                    z7 = true;
                                                                }
                                                            }
                                                            i9++;
                                                            length = i10;
                                                            zzkoArr = zzkoArr2;
                                                            z5 = z8;
                                                            i6 = i5;
                                                        }
                                                        i4 = i6;
                                                        boolean z9 = z5;
                                                        if (!z6 && zzo) {
                                                            zzge().zzit().zzg("Marking event as conversion", zzga().zzbj(zzkn.name));
                                                            zzko[] zzkoArr3 = (zzko[]) Arrays.copyOf(zzkn.zzata, zzkn.zzata.length + 1);
                                                            zzko zzko2 = new zzko();
                                                            zzko2.name = "_c";
                                                            zzko2.zzate = Long.valueOf(1);
                                                            zzkoArr3[zzkoArr3.length - 1] = zzko2;
                                                            zzkn.zzata = zzkoArr3;
                                                        }
                                                        if (!z7) {
                                                            zzge().zzit().zzg("Marking event as real-time", zzga().zzbj(zzkn.name));
                                                            zzko[] zzkoArr4 = (zzko[]) Arrays.copyOf(zzkn.zzata, zzkn.zzata.length + 1);
                                                            zzko zzko3 = new zzko();
                                                            zzko3.name = "_r";
                                                            zzko3.zzate = Long.valueOf(1);
                                                            zzkoArr4[zzkoArr4.length - 1] = zzko3;
                                                            zzkn.zzata = zzkoArr4;
                                                        }
                                                        if (zzix().zza(zzkr(), zzjv3.zzaqv.zzti, false, false, false, false, true).zzafh > ((long) zzgg().zzar(zzjv3.zzaqv.zzti))) {
                                                            int i11 = 0;
                                                            while (true) {
                                                                if (i11 >= zzkn.zzata.length) {
                                                                    break;
                                                                } else if ("_r".equals(zzkn.zzata[i11].name)) {
                                                                    zzko[] zzkoArr5 = new zzko[(zzkn.zzata.length - 1)];
                                                                    if (i11 > 0) {
                                                                        System.arraycopy(zzkn.zzata, 0, zzkoArr5, 0, i11);
                                                                    }
                                                                    if (i11 < zzkoArr5.length) {
                                                                        System.arraycopy(zzkn.zzata, i11 + 1, zzkoArr5, i11, zzkoArr5.length - i11);
                                                                    }
                                                                    zzkn.zzata = zzkoArr5;
                                                                } else {
                                                                    i11++;
                                                                }
                                                            }
                                                            z5 = z9;
                                                        } else {
                                                            z5 = true;
                                                        }
                                                        if (zzka.zzcc(zzkn.name) && zzo && zzix().zza(zzkr(), zzjv3.zzaqv.zzti, false, false, true, false, false).zzaff > ((long) zzgg().zzb(zzjv3.zzaqv.zzti, zzew.zzagv))) {
                                                            zzge().zzip().zzg("Too many conversions. Not logging as conversion. appId", zzfg.zzbm(zzjv3.zzaqv.zzti));
                                                            zzko[] zzkoArr6 = zzkn.zzata;
                                                            int length2 = zzkoArr6.length;
                                                            int i12 = 0;
                                                            boolean z10 = false;
                                                            zzko zzko4 = null;
                                                            while (i12 < length2) {
                                                                zzko zzko5 = zzkoArr6[i12];
                                                                zzko[] zzkoArr7 = zzkoArr6;
                                                                int i13 = length2;
                                                                if ("_c".equals(zzko5.name)) {
                                                                    zzko4 = zzko5;
                                                                } else if ("_err".equals(zzko5.name)) {
                                                                    z10 = true;
                                                                }
                                                                i12++;
                                                                zzkoArr6 = zzkoArr7;
                                                                length2 = i13;
                                                            }
                                                            if (z10 && zzko4 != null) {
                                                                zzkn.zzata = (zzko[]) ArrayUtils.removeAll((T[]) zzkn.zzata, (T[]) new zzko[]{zzko4});
                                                            } else if (zzko4 != null) {
                                                                zzko4.name = "_err";
                                                                zzko4.zzate = Long.valueOf(10);
                                                            } else {
                                                                zzge().zzim().zzg("Did not find conversion parameter. appId", zzfg.zzbm(zzjv3.zzaqv.zzti));
                                                            }
                                                        }
                                                        if (zzkn.zzata != null) {
                                                        }
                                                        zzip = zzge().zzip();
                                                        str3 = "Engagement event does not contain any parameters. appId";
                                                        zzbm = zzfg.zzbm(zzjv3.zzaqv.zzti);
                                                        zzip.zzg(str3, zzbm);
                                                        j2 = j4;
                                                        int i822 = i7 + 1;
                                                        zzkq3.zzati[i7] = zzkn;
                                                        i7 = i822;
                                                        j4 = j2;
                                                    }
                                                    i6 = i4 + 1;
                                                }
                                                boolean z11 = z5;
                                                if (i7 < zzjv3.zzaqx.size()) {
                                                    zzkq3.zzati = (zzkn[]) Arrays.copyOf(zzkq3.zzati, i7);
                                                }
                                                if (zzav) {
                                                    zzjz zzh = zzix().zzh(zzkq3.zzti, "_lte");
                                                    if (zzh != null) {
                                                        if (zzh.value != null) {
                                                            zzjz zzjz2 = new zzjz(zzkq3.zzti, "auto", "_lte", zzbt().currentTimeMillis(), Long.valueOf(((Long) zzh.value).longValue() + j4));
                                                            zzjz = zzjz2;
                                                            zzks zzks = new zzks();
                                                            zzks.name = "_lte";
                                                            zzks.zzaun = Long.valueOf(zzbt().currentTimeMillis());
                                                            zzks.zzate = (Long) zzjz.value;
                                                            i3 = 0;
                                                            while (true) {
                                                                if (i3 < zzkq3.zzatj.length) {
                                                                    z3 = false;
                                                                    break;
                                                                } else if ("_lte".equals(zzkq3.zzatj[i3].name)) {
                                                                    zzkq3.zzatj[i3] = zzks;
                                                                    z3 = true;
                                                                    break;
                                                                } else {
                                                                    i3++;
                                                                }
                                                            }
                                                            if (!z3) {
                                                                zzkq3.zzatj = (zzks[]) Arrays.copyOf(zzkq3.zzatj, zzkq3.zzatj.length + 1);
                                                                zzkq3.zzatj[zzjv3.zzaqv.zzatj.length - 1] = zzks;
                                                            }
                                                            if (j4 > 0) {
                                                                zzix().zza(zzjz);
                                                                zzge().zzis().zzg("Updated lifetime engagement user property with value. Value", zzjz.value);
                                                            }
                                                        }
                                                    }
                                                    zzjz = new zzjz(zzkq3.zzti, "auto", "_lte", zzbt().currentTimeMillis(), Long.valueOf(j4));
                                                    zzks zzks2 = new zzks();
                                                    zzks2.name = "_lte";
                                                    zzks2.zzaun = Long.valueOf(zzbt().currentTimeMillis());
                                                    zzks2.zzate = (Long) zzjz.value;
                                                    i3 = 0;
                                                    while (true) {
                                                        if (i3 < zzkq3.zzatj.length) {
                                                        }
                                                        i3++;
                                                    }
                                                    if (!z3) {
                                                    }
                                                    if (j4 > 0) {
                                                    }
                                                }
                                                zzkq3.zzaua = zza(zzkq3.zzti, zzkq3.zzatj, zzkq3.zzati);
                                                if (zzgg().zzau(zzjv3.zzaqv.zzti)) {
                                                    HashMap hashMap = new HashMap();
                                                    zzkn[] zzknArr2 = new zzkn[zzkq3.zzati.length];
                                                    SecureRandom zzlc = zzgb().zzlc();
                                                    zzkn[] zzknArr3 = zzkq3.zzati;
                                                    int length3 = zzknArr3.length;
                                                    int i14 = 0;
                                                    int i15 = 0;
                                                    while (i14 < length3) {
                                                        zzkn zzkn2 = zzknArr3[i14];
                                                        if (zzkn2.name.equals("_ep")) {
                                                            zzgb();
                                                            String str9 = (String) zzka.zzb(zzkn2, "_en");
                                                            zzeq zzeq = (zzeq) hashMap.get(str9);
                                                            if (zzeq == null) {
                                                                zzeq = zzix().zzf(zzjv3.zzaqv.zzti, str9);
                                                                hashMap.put(str9, zzeq);
                                                            }
                                                            if (zzeq.zzafv == null) {
                                                                if (zzeq.zzafw.longValue() > 1) {
                                                                    zzgb();
                                                                    zzkn2.zzata = zzka.zza(zzkn2.zzata, "_sr", (Object) zzeq.zzafw);
                                                                }
                                                                if (zzeq.zzafx == null || !zzeq.zzafx.booleanValue()) {
                                                                    zzknArr = zzknArr3;
                                                                } else {
                                                                    zzgb();
                                                                    zzknArr = zzknArr3;
                                                                    zzkn2.zzata = zzka.zza(zzkn2.zzata, "_efs", (Object) Long.valueOf(1));
                                                                }
                                                                int i16 = i15 + 1;
                                                                zzknArr2[i15] = zzkn2;
                                                                zzjv2 = zzjv3;
                                                                zzkq2 = zzkq3;
                                                                secureRandom = zzlc;
                                                                i15 = i16;
                                                            } else {
                                                                zzknArr = zzknArr3;
                                                                zzjv2 = zzjv3;
                                                                zzkq2 = zzkq3;
                                                                secureRandom = zzlc;
                                                            }
                                                            i = length3;
                                                        } else {
                                                            zzknArr = zzknArr3;
                                                            String str10 = "_dbg";
                                                            Long valueOf2 = Long.valueOf(1);
                                                            if (!TextUtils.isEmpty(str10)) {
                                                                if (valueOf2 != null) {
                                                                    zzko[] zzkoArr8 = zzkn2.zzata;
                                                                    int length4 = zzkoArr8.length;
                                                                    int i17 = 0;
                                                                    while (true) {
                                                                        if (i17 >= length4) {
                                                                            break;
                                                                        }
                                                                        i = length3;
                                                                        zzko zzko6 = zzkoArr8[i17];
                                                                        zzko[] zzkoArr9 = zzkoArr8;
                                                                        if (!str10.equals(zzko6.name)) {
                                                                            i17++;
                                                                            length3 = i;
                                                                            zzkoArr8 = zzkoArr9;
                                                                        } else if (((valueOf2 instanceof Long) && valueOf2.equals(zzko6.zzate)) || (((valueOf2 instanceof String) && valueOf2.equals(zzko6.zzajf)) || ((valueOf2 instanceof Double) && valueOf2.equals(zzko6.zzarc)))) {
                                                                            z2 = true;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            i = length3;
                                                            z2 = false;
                                                            int zzp = !z2 ? zzkm().zzp(zzjv3.zzaqv.zzti, zzkn2.name) : 1;
                                                            if (zzp <= 0) {
                                                                zzge().zzip().zze("Sample rate must be positive. event, rate", zzkn2.name, Integer.valueOf(zzp));
                                                                i2 = i15 + 1;
                                                                zzknArr2[i15] = zzkn2;
                                                            } else {
                                                                zzeq zzeq2 = (zzeq) hashMap.get(zzkn2.name);
                                                                if (zzeq2 == null) {
                                                                    zzeq2 = zzix().zzf(zzjv3.zzaqv.zzti, zzkn2.name);
                                                                    if (zzeq2 == null) {
                                                                        zzge().zzip().zze("Event being bundled has no eventAggregate. appId, eventName", zzjv3.zzaqv.zzti, zzkn2.name);
                                                                        zzeq2 = new zzeq(zzjv3.zzaqv.zzti, zzkn2.name, 1, 1, zzkn2.zzatb.longValue(), 0, null, null, null);
                                                                    }
                                                                }
                                                                zzgb();
                                                                Long l2 = (Long) zzka.zzb(zzkn2, "_eid");
                                                                Boolean valueOf3 = Boolean.valueOf(l2 != null);
                                                                if (zzp == 1) {
                                                                    i2 = i15 + 1;
                                                                    zzknArr2[i15] = zzkn2;
                                                                    if (valueOf3.booleanValue() && !(zzeq2.zzafv == null && zzeq2.zzafw == null && zzeq2.zzafx == null)) {
                                                                        hashMap.put(zzkn2.name, zzeq2.zza(null, null, null));
                                                                    }
                                                                } else if (zzlc.nextInt(zzp) == 0) {
                                                                    zzgb();
                                                                    secureRandom = zzlc;
                                                                    long j5 = (long) zzp;
                                                                    zzkn2.zzata = zzka.zza(zzkn2.zzata, "_sr", (Object) Long.valueOf(j5));
                                                                    int i18 = i15 + 1;
                                                                    zzknArr2[i15] = zzkn2;
                                                                    if (valueOf3.booleanValue()) {
                                                                        zzeq2 = zzeq2.zza(null, Long.valueOf(j5), null);
                                                                    }
                                                                    hashMap.put(zzkn2.name, zzeq2.zzad(zzkn2.zzatb.longValue()));
                                                                    zzjv2 = zzjv3;
                                                                    zzkq2 = zzkq3;
                                                                    i15 = i18;
                                                                } else {
                                                                    secureRandom = zzlc;
                                                                    zzjv2 = zzjv3;
                                                                    zzkq2 = zzkq3;
                                                                    if (Math.abs(zzkn2.zzatb.longValue() - zzeq2.zzafu) >= 86400000) {
                                                                        zzgb();
                                                                        zzkn2.zzata = zzka.zza(zzkn2.zzata, "_efs", (Object) Long.valueOf(1));
                                                                        zzgb();
                                                                        long j6 = (long) zzp;
                                                                        zzkn2.zzata = zzka.zza(zzkn2.zzata, "_sr", (Object) Long.valueOf(j6));
                                                                        int i19 = i15 + 1;
                                                                        zzknArr2[i15] = zzkn2;
                                                                        if (valueOf3.booleanValue()) {
                                                                            zzeq2 = zzeq2.zza(null, Long.valueOf(j6), Boolean.valueOf(true));
                                                                        }
                                                                        hashMap.put(zzkn2.name, zzeq2.zzad(zzkn2.zzatb.longValue()));
                                                                        i15 = i19;
                                                                    } else if (valueOf3.booleanValue()) {
                                                                        hashMap.put(zzkn2.name, zzeq2.zza(l2, null, null));
                                                                    }
                                                                    i14++;
                                                                    zzknArr3 = zzknArr;
                                                                    length3 = i;
                                                                    zzlc = secureRandom;
                                                                    zzjv3 = zzjv2;
                                                                    zzkq3 = zzkq2;
                                                                }
                                                            }
                                                            zzjv2 = zzjv3;
                                                            zzkq2 = zzkq3;
                                                            secureRandom = zzlc;
                                                            i15 = i2;
                                                        }
                                                        i14++;
                                                        zzknArr3 = zzknArr;
                                                        length3 = i;
                                                        zzlc = secureRandom;
                                                        zzjv3 = zzjv2;
                                                        zzkq3 = zzkq2;
                                                    }
                                                    zzjv = zzjv3;
                                                    zzkq = zzkq3;
                                                    if (i15 < zzkq.zzati.length) {
                                                        zzkq.zzati = (zzkn[]) Arrays.copyOf(zzknArr2, i15);
                                                    }
                                                    for (Entry value : hashMap.entrySet()) {
                                                        zzix().zza((zzeq) value.getValue());
                                                    }
                                                } else {
                                                    zzjv = zzjv3;
                                                    zzkq = zzkq3;
                                                }
                                                zzkq.zzatl = Long.valueOf(Long.MAX_VALUE);
                                                zzkq.zzatm = Long.valueOf(Long.MIN_VALUE);
                                                for (zzkn zzkn3 : zzkq.zzati) {
                                                    if (zzkn3.zzatb.longValue() < zzkq.zzatl.longValue()) {
                                                        zzkq.zzatl = zzkn3.zzatb;
                                                    }
                                                    if (zzkn3.zzatb.longValue() > zzkq.zzatm.longValue()) {
                                                        zzkq.zzatm = zzkn3.zzatb;
                                                    }
                                                }
                                                zzjv zzjv4 = zzjv;
                                                str2 = zzjv4.zzaqv.zzti;
                                                zzdy zzbc = zzix().zzbc(str2);
                                                if (zzbc == null) {
                                                    zzge().zzim().zzg("Bundling raw events w/o app info. appId", zzfg.zzbm(zzjv4.zzaqv.zzti));
                                                } else if (zzkq.zzati.length > 0) {
                                                    long zzgl = zzbc.zzgl();
                                                    zzkq.zzato = zzgl != 0 ? Long.valueOf(zzgl) : null;
                                                    long zzgk = zzbc.zzgk();
                                                    if (zzgk != 0) {
                                                        zzgl = zzgk;
                                                    }
                                                    zzkq.zzatn = zzgl != 0 ? Long.valueOf(zzgl) : null;
                                                    zzbc.zzgt();
                                                    zzkq.zzaty = Integer.valueOf((int) zzbc.zzgq());
                                                    zzbc.zzm(zzkq.zzatl.longValue());
                                                    zzbc.zzn(zzkq.zzatm.longValue());
                                                    zzkq.zzaek = zzbc.zzhb();
                                                    zzix().zza(zzbc);
                                                }
                                                if (zzkq.zzati.length > 0) {
                                                    zzkk zzbu = zzkm().zzbu(zzjv4.zzaqv.zzti);
                                                    if (zzbu != null) {
                                                        if (zzbu.zzasp != null) {
                                                            valueOf = zzbu.zzasp;
                                                            zzkq.zzauf = valueOf;
                                                            zzix().zza(zzkq, z11);
                                                        }
                                                    }
                                                    if (TextUtils.isEmpty(zzjv4.zzaqv.zzadm)) {
                                                        valueOf = Long.valueOf(-1);
                                                        zzkq.zzauf = valueOf;
                                                        zzix().zza(zzkq, z11);
                                                    } else {
                                                        zzge().zzip().zzg("Did not find measurement config or missing version info. appId", zzfg.zzbm(zzjv4.zzaqv.zzti));
                                                        zzix().zza(zzkq, z11);
                                                    }
                                                }
                                                zzei zzix3 = zzix();
                                                List<Long> list = zzjv4.zzaqw;
                                                Preconditions.checkNotNull(list);
                                                zzix3.zzab();
                                                zzix3.zzch();
                                                StringBuilder sb2 = new StringBuilder("rowid in (");
                                                for (int i20 = 0; i20 < list.size(); i20++) {
                                                    if (i20 != 0) {
                                                        sb2.append(",");
                                                    }
                                                    sb2.append(((Long) list.get(i20)).longValue());
                                                }
                                                sb2.append(")");
                                                int delete = zzix3.getWritableDatabase().delete("raw_events", sb2.toString(), null);
                                                if (delete != list.size()) {
                                                    zzix3.zzge().zzim().zze("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
                                                }
                                                zzix = zzix();
                                                zzix.getWritableDatabase().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{str2, str2});
                                                zzix().setTransactionSuccessful();
                                                zzix().endTransaction();
                                                return true;
                                            }
                                            zzix().setTransactionSuccessful();
                                            zzix().endTransaction();
                                            return false;
                                        }
                                    }
                                    z = true;
                                    if (z) {
                                    }
                                } else {
                                    byte[] blob = query.getBlob(0);
                                    zzabv zza = zzabv.zza(blob, 0, blob.length);
                                    zzkq zzkq4 = new zzkq();
                                    try {
                                        zzkq4.zzb(zza);
                                        if (query.moveToNext()) {
                                            zzix2.zzge().zzip().zzg("Get multiple raw event metadata records, expected one. appId", zzfg.zzbm(str6));
                                        }
                                        query.close();
                                        zzjv3.zzb(zzkq4);
                                        if (j3 != -1) {
                                            String[] strArr3 = {str6, str5, String.valueOf(j3)};
                                            str7 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                                            strArr = strArr3;
                                        } else {
                                            str7 = "app_id = ? and metadata_fingerprint = ?";
                                            strArr = new String[]{str6, str5};
                                        }
                                        cursor = query;
                                        try {
                                            query2 = sQLiteDatabase.query("raw_events", new String[]{"rowid", "name", AppMeasurement.Param.TIMESTAMP, DataBufferSafeParcelable.DATA_FIELD}, str7, strArr, null, null, "rowid", null);
                                        } catch (SQLiteException e3) {
                                            e = e3;
                                            str4 = str6;
                                            obj = e;
                                            zzix2.zzge().zzim().zze("Data loss. Error selecting raw event. appId", zzfg.zzbm(str4), obj);
                                        }
                                    } catch (IOException e4) {
                                        cursor = query;
                                        zzix2.zzge().zzim().zze("Data loss. Failed to merge raw event metadata. appId", zzfg.zzbm(str6), e4);
                                    }
                                    try {
                                        if (!query2.moveToFirst()) {
                                            zzix2.zzge().zzip().zzg("Raw event data disappeared while in transaction. appId", zzfg.zzbm(str6));
                                        } else {
                                            while (true) {
                                                long j7 = query2.getLong(0);
                                                byte[] blob2 = query2.getBlob(3);
                                                zzabv zza2 = zzabv.zza(blob2, 0, blob2.length);
                                                zzkn zzkn4 = new zzkn();
                                                try {
                                                    zzkn4.zzb(zza2);
                                                    zzkn4.name = query2.getString(1);
                                                    zzkn4.zzatb = Long.valueOf(query2.getLong(2));
                                                    if (!zzjv3.zza(j7, zzkn4)) {
                                                        if (query2 != null) {
                                                        }
                                                    }
                                                } catch (IOException e5) {
                                                    zzix2.zzge().zzim().zze("Data loss. Failed to merge raw event. appId", zzfg.zzbm(str6), e5);
                                                }
                                                if (!query2.moveToNext()) {
                                                }
                                            }
                                            if (zzjv3.zzaqx != null) {
                                            }
                                            z = true;
                                            if (z) {
                                            }
                                        }
                                        query2.close();
                                    } catch (SQLiteException e6) {
                                        e = e6;
                                        str4 = str6;
                                        cursor = query2;
                                        obj = e;
                                        zzix2.zzge().zzim().zze("Data loss. Error selecting raw event. appId", zzfg.zzbm(str4), obj);
                                    } catch (Throwable th5) {
                                        th = th5;
                                        cursor = query2;
                                        if (cursor != null) {
                                        }
                                        throw th;
                                    }
                                    if (zzjv3.zzaqx != null) {
                                    }
                                    z = true;
                                    if (z) {
                                    }
                                }
                            } catch (SQLiteException e7) {
                                e = e7;
                                cursor = query;
                                str4 = str6;
                                obj = e;
                                zzix2.zzge().zzim().zze("Data loss. Error selecting raw event. appId", zzfg.zzbm(str4), obj);
                            } catch (Throwable th6) {
                                th = th6;
                                cursor = query;
                                th = th;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                        } catch (SQLiteException e8) {
                            e = e8;
                            str4 = str6;
                            cursor = cursor2;
                            obj = e;
                            zzix2.zzge().zzim().zze("Data loss. Error selecting raw event. appId", zzfg.zzbm(str4), obj);
                        } catch (Throwable th7) {
                            th = th7;
                            cursor = cursor2;
                            if (cursor != null) {
                            }
                            throw th;
                        }
                    }
                } else {
                    String[] strArr4 = j3 != -1 ? new String[]{null, String.valueOf(j3)} : new String[]{null};
                    String str11 = j3 != -1 ? " and rowid <= ?" : "";
                    StringBuilder sb3 = new StringBuilder(84 + String.valueOf(str11).length());
                    sb3.append("select metadata_fingerprint from raw_events where app_id = ?");
                    sb3.append(str11);
                    sb3.append(" order by rowid limit 1;");
                    cursor3 = writableDatabase.rawQuery(sb3.toString(), strArr4);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                        }
                        if (zzjv3.zzaqx != null) {
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        String string2 = cursor3.getString(0);
                        cursor3.close();
                        cursor2 = cursor3;
                        str5 = string2;
                        str6 = null;
                        SQLiteDatabase sQLiteDatabase2 = writableDatabase;
                        query = writableDatabase.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str6, str5}, null, null, "rowid", "2");
                        if (query.moveToFirst()) {
                        }
                    }
                }
                cursor3.close();
            } catch (SQLiteException e9) {
                obj = e9;
                cursor = null;
                str4 = null;
                zzix2.zzge().zzim().zze("Data loss. Error selecting raw event. appId", zzfg.zzbm(str4), obj);
            } catch (Throwable th8) {
                th = th8;
                cursor = null;
                if (cursor != null) {
                }
                throw th;
            }
            if (zzjv3.zzaqx != null) {
            }
            z = true;
            if (z) {
            }
        } catch (SQLiteException e10) {
            zzix.zzge().zzim().zze("Failed to remove unused event metadata. appId", zzfg.zzbm(str2), e10);
        } catch (Throwable th9) {
            Throwable th10 = th9;
            zzix().endTransaction();
            throw th10;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x014e  */
    @WorkerThread
    public final zzdy zzg(zzdz zzdz) {
        boolean z;
        zzab();
        zzkq();
        Preconditions.checkNotNull(zzdz);
        Preconditions.checkNotEmpty(zzdz.packageName);
        zzdy zzbc = zzix().zzbc(zzdz.packageName);
        String zzbp = zzgf().zzbp(zzdz.packageName);
        if (zzbc == null) {
            zzbc = new zzdy(this.zzacw, zzdz.packageName);
            zzbc.zzal(this.zzacw.zzfv().zzii());
            zzbc.zzan(zzbp);
        } else if (!zzbp.equals(zzbc.zzgi())) {
            zzbc.zzan(zzbp);
            zzbc.zzal(this.zzacw.zzfv().zzii());
        } else {
            z = false;
            if (!TextUtils.isEmpty(zzdz.zzadm) && !zzdz.zzadm.equals(zzbc.getGmpAppId())) {
                zzbc.zzam(zzdz.zzadm);
                z = true;
            }
            if (!TextUtils.isEmpty(zzdz.zzado) && !zzdz.zzado.equals(zzbc.zzgj())) {
                zzbc.zzao(zzdz.zzado);
                z = true;
            }
            if (!(zzdz.zzadu == 0 || zzdz.zzadu == zzbc.zzgo())) {
                zzbc.zzp(zzdz.zzadu);
                z = true;
            }
            if (!TextUtils.isEmpty(zzdz.zzth) && !zzdz.zzth.equals(zzbc.zzag())) {
                zzbc.setAppVersion(zzdz.zzth);
                z = true;
            }
            if (zzdz.zzads != zzbc.zzgm()) {
                zzbc.zzo(zzdz.zzads);
                z = true;
            }
            if (zzdz.zzadt != null && !zzdz.zzadt.equals(zzbc.zzgn())) {
                zzbc.zzap(zzdz.zzadt);
                z = true;
            }
            if (zzdz.zzadv != zzbc.zzgp()) {
                zzbc.zzq(zzdz.zzadv);
                z = true;
            }
            if (zzdz.zzadw != zzbc.isMeasurementEnabled()) {
                zzbc.setMeasurementEnabled(zzdz.zzadw);
                z = true;
            }
            if (!TextUtils.isEmpty(zzdz.zzaek) && !zzdz.zzaek.equals(zzbc.zzha())) {
                zzbc.zzaq(zzdz.zzaek);
                z = true;
            }
            if (zzdz.zzadx != zzbc.zzhc()) {
                zzbc.zzaa(zzdz.zzadx);
                z = true;
            }
            if (zzdz.zzady != zzbc.zzhd()) {
                zzbc.zzd(zzdz.zzady);
                z = true;
            }
            if (zzdz.zzadz != zzbc.zzhe()) {
                zzbc.zze(zzdz.zzadz);
                z = true;
            }
            if (z) {
                zzix().zza(zzbc);
            }
            return zzbc;
        }
        z = true;
        zzbc.zzam(zzdz.zzadm);
        z = true;
        zzbc.zzao(zzdz.zzado);
        z = true;
        zzbc.zzp(zzdz.zzadu);
        z = true;
        zzbc.setAppVersion(zzdz.zzth);
        z = true;
        if (zzdz.zzads != zzbc.zzgm()) {
        }
        zzbc.zzap(zzdz.zzadt);
        z = true;
        if (zzdz.zzadv != zzbc.zzgp()) {
        }
        if (zzdz.zzadw != zzbc.isMeasurementEnabled()) {
        }
        zzbc.zzaq(zzdz.zzaek);
        z = true;
        if (zzdz.zzadx != zzbc.zzhc()) {
        }
        if (zzdz.zzady != zzbc.zzhd()) {
        }
        if (zzdz.zzadz != zzbc.zzhe()) {
        }
        if (z) {
        }
        return zzbc;
    }

    private final zzgf zzkm() {
        zza((zzjq) this.zzaqa);
        return this.zzaqa;
    }

    private final zzfp zzko() {
        if (this.zzaqd != null) {
            return this.zzaqd;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    private final zzjn zzkp() {
        zza((zzjq) this.zzaqe);
        return this.zzaqe;
    }

    private final long zzkr() {
        long currentTimeMillis = zzbt().currentTimeMillis();
        zzfr zzgf = zzgf();
        zzgf.zzch();
        zzgf.zzab();
        long j = zzgf.zzajy.get();
        if (j == 0) {
            long nextInt = 1 + ((long) zzgf.zzgb().zzlc().nextInt(86400000));
            zzgf.zzajy.set(nextInt);
            j = nextInt;
        }
        return ((((currentTimeMillis + j) / 1000) / 60) / 60) / 24;
    }

    private final boolean zzkt() {
        zzab();
        zzkq();
        return zzix().zzhs() || !TextUtils.isEmpty(zzix().zzhn());
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x018e  */
    @WorkerThread
    private final void zzku() {
        zzex<Long> zzex;
        long j;
        zzab();
        zzkq();
        if (zzky()) {
            if (this.zzaqh > 0) {
                long abs = 3600000 - Math.abs(zzbt().elapsedRealtime() - this.zzaqh);
                if (abs > 0) {
                    zzge().zzit().zzg("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    zzko().unregister();
                    zzkp().cancel();
                    return;
                }
                this.zzaqh = 0;
            }
            if (!this.zzacw.zzjv() || !zzkt()) {
                zzge().zzit().log("Nothing to upload or uploading impossible");
                zzko().unregister();
                zzkp().cancel();
                return;
            }
            long currentTimeMillis = zzbt().currentTimeMillis();
            long max = Math.max(0, ((Long) zzew.zzahi.get()).longValue());
            boolean z = zzix().zzht() || zzix().zzho();
            if (z) {
                String zzhj = zzgg().zzhj();
                zzex = (TextUtils.isEmpty(zzhj) || ".none.".equals(zzhj)) ? zzew.zzahc : zzew.zzahd;
            } else {
                zzex = zzew.zzahb;
            }
            long max2 = Math.max(0, ((Long) zzex.get()).longValue());
            long j2 = zzgf().zzaju.get();
            long j3 = zzgf().zzajv.get();
            long j4 = max2;
            long j5 = max;
            long max3 = Math.max(zzix().zzhq(), zzix().zzhr());
            if (max3 != 0) {
                long abs2 = currentTimeMillis - Math.abs(max3 - currentTimeMillis);
                long abs3 = currentTimeMillis - Math.abs(j3 - currentTimeMillis);
                long max4 = Math.max(currentTimeMillis - Math.abs(j2 - currentTimeMillis), abs3);
                j = abs2 + j5;
                if (z && max4 > 0) {
                    j = Math.min(abs2, max4) + j4;
                }
                long j6 = j4;
                if (!zzgb().zza(max4, j6)) {
                    j = max4 + j6;
                }
                if (abs3 != 0 && abs3 >= abs2) {
                    int i = 0;
                    while (true) {
                        if (i >= Math.min(20, Math.max(0, ((Integer) zzew.zzahk.get()).intValue()))) {
                            break;
                        }
                        long max5 = j + (Math.max(0, ((Long) zzew.zzahj.get()).longValue()) * (1 << i));
                        if (max5 > abs3) {
                            j = max5;
                            break;
                        } else {
                            i++;
                            j = max5;
                        }
                    }
                }
                if (j != 0) {
                    zzge().zzit().log("Next upload time is 0");
                    zzko().unregister();
                    zzkp().cancel();
                    return;
                } else if (!zzkn().zzex()) {
                    zzge().zzit().log("No network");
                    zzko().zzeu();
                    zzkp().cancel();
                    return;
                } else {
                    long j7 = zzgf().zzajw.get();
                    long max6 = Math.max(0, ((Long) zzew.zzagz.get()).longValue());
                    if (!zzgb().zza(j7, max6)) {
                        j = Math.max(j, j7 + max6);
                    }
                    zzko().unregister();
                    long currentTimeMillis2 = j - zzbt().currentTimeMillis();
                    if (currentTimeMillis2 <= 0) {
                        currentTimeMillis2 = Math.max(0, ((Long) zzew.zzahe.get()).longValue());
                        zzgf().zzaju.set(zzbt().currentTimeMillis());
                    }
                    zzge().zzit().zzg("Upload scheduled in approximately ms", Long.valueOf(currentTimeMillis2));
                    zzkp().zzh(currentTimeMillis2);
                    return;
                }
            }
            j = 0;
            if (j != 0) {
            }
        }
    }

    @WorkerThread
    private final void zzkv() {
        zzab();
        if (this.zzaql || this.zzaqm || this.zzaqn) {
            zzge().zzit().zzd("Not stopping services. fetch, network, upload", Boolean.valueOf(this.zzaql), Boolean.valueOf(this.zzaqm), Boolean.valueOf(this.zzaqn));
            return;
        }
        zzge().zzit().log("Stopping uploading service(s)");
        if (this.zzaqi != null) {
            for (Runnable run : this.zzaqi) {
                run.run();
            }
            this.zzaqi.clear();
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final boolean zzkw() {
        String str;
        zzfi zzfi;
        zzab();
        try {
            this.zzaqp = new RandomAccessFile(new File(getContext().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
            this.zzaqo = this.zzaqp.tryLock();
            if (this.zzaqo != null) {
                zzge().zzit().log("Storage concurrent access okay");
                return true;
            }
            zzge().zzim().log("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e) {
            e = e;
            zzfi = zzge().zzim();
            str = "Failed to acquire storage lock";
            zzfi.zzg(str, e);
            return false;
        } catch (IOException e2) {
            e = e2;
            zzfi = zzge().zzim();
            str = "Failed to access storage lock file";
            zzfi.zzg(str, e);
            return false;
        }
    }

    @WorkerThread
    private final boolean zzky() {
        zzab();
        zzkq();
        return this.zzaqg;
    }

    public Context getContext() {
        return this.zzacw.getContext();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void start() {
        zzab();
        zzix().zzhp();
        if (zzgf().zzaju.get() == 0) {
            zzgf().zzaju.set(zzbt().currentTimeMillis());
        }
        zzku();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    @WorkerThread
    @VisibleForTesting
    public final void zza(int i, Throwable th, byte[] bArr, String str) {
        zzei zzix;
        zzab();
        zzkq();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.zzaqm = false;
                zzkv();
                throw th2;
            }
        }
        List<Long> list = this.zzaqq;
        this.zzaqq = null;
        boolean z = true;
        if ((i == 200 || i == 204) && th == null) {
            try {
                zzgf().zzaju.set(zzbt().currentTimeMillis());
                zzgf().zzajv.set(0);
                zzku();
                zzge().zzit().zze("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                zzix().beginTransaction();
                try {
                    for (Long l : list) {
                        try {
                            zzix = zzix();
                            long longValue = l.longValue();
                            zzix.zzab();
                            zzix.zzch();
                            if (zzix.getWritableDatabase().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e) {
                            zzix.zzge().zzim().zzg("Failed to delete a bundle in a queue table", e);
                            throw e;
                        } catch (SQLiteException e2) {
                            if (this.zzaqr == null || !this.zzaqr.contains(l)) {
                                throw e2;
                            }
                        }
                    }
                    zzix().setTransactionSuccessful();
                    zzix().endTransaction();
                    this.zzaqr = null;
                    if (!zzkn().zzex() || !zzkt()) {
                        this.zzaqs = -1;
                        zzku();
                    } else {
                        zzks();
                    }
                    this.zzaqh = 0;
                } catch (Throwable th3) {
                    zzix().endTransaction();
                    throw th3;
                }
            } catch (SQLiteException e3) {
                zzge().zzim().zzg("Database error while trying to delete uploaded bundles", e3);
                this.zzaqh = zzbt().elapsedRealtime();
                zzge().zzit().zzg("Disable upload, time", Long.valueOf(this.zzaqh));
            }
        } else {
            zzge().zzit().zze("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            zzgf().zzajv.set(zzbt().currentTimeMillis());
            if (i != 503) {
                if (i != 429) {
                    z = false;
                }
            }
            if (z) {
                zzgf().zzajw.set(zzbt().currentTimeMillis());
            }
            if (zzgg().zzax(str)) {
                zzix().zzc(list);
            }
            zzku();
        }
        this.zzaqm = false;
        zzkv();
    }

    /* access modifiers changed from: 0000 */
    public final void zza(zzgl zzgl) {
        this.zzacw = zzgl;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zza(zzjw zzjw) {
        zzab();
        zzei zzei = new zzei(this.zzacw);
        zzei.zzm();
        this.zzaqc = zzei;
        zzgg().zza(this.zzaqa);
        zzeb zzeb = new zzeb(this.zzacw);
        zzeb.zzm();
        this.zzaqf = zzeb;
        zzjn zzjn = new zzjn(this.zzacw);
        zzjn.zzm();
        this.zzaqe = zzjn;
        this.zzaqd = new zzfp(this.zzacw);
        if (this.zzaqj != this.zzaqk) {
            zzge().zzim().zze("Not all upload components initialized", Integer.valueOf(this.zzaqj), Integer.valueOf(this.zzaqk));
        }
        this.zzvo = true;
    }

    /* JADX WARNING: type inference failed for: r18v1 */
    /* JADX WARNING: type inference failed for: r18v3 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    @WorkerThread
    public final byte[] zza(@NonNull zzeu zzeu, @Size(min = 1) String str) {
        zzjz zzjz;
        zzkp zzkp;
        zzdy zzdy;
        zzkq zzkq;
        long j;
        byte[] bArr;
        Bundle bundle;
        long j2;
        zzfi zzip;
        String str2;
        Object zzbm;
        zzeu zzeu2 = zzeu;
        String str3 = str;
        zzkq();
        zzab();
        zzgl.zzfr();
        Preconditions.checkNotNull(zzeu);
        Preconditions.checkNotEmpty(str);
        zzkp zzkp2 = new zzkp();
        zzix().beginTransaction();
        try {
            zzdy zzbc = zzix().zzbc(str3);
            if (zzbc == null) {
                zzge().zzis().zzg("Log and bundle not available. package_name", str3);
            } else if (!zzbc.isMeasurementEnabled()) {
                zzge().zzis().zzg("Log and bundle disabled. package_name", str3);
            } else {
                if (("_iap".equals(zzeu2.name) || Event.ECOMMERCE_PURCHASE.equals(zzeu2.name)) && !zza(str3, zzeu2)) {
                    zzge().zzip().zzg("Failed to handle purchase event at single event bundle creation. appId", zzfg.zzbm(str));
                }
                boolean zzav = zzgg().zzav(str3);
                Long valueOf = Long.valueOf(0);
                if (zzav && "_e".equals(zzeu2.name)) {
                    if (zzeu2.zzafq != null) {
                        if (zzeu2.zzafq.size() != 0) {
                            if (zzeu2.zzafq.getLong("_et") == null) {
                                zzip = zzge().zzip();
                                str2 = "The engagement event does not include duration. appId";
                                zzbm = zzfg.zzbm(str);
                                zzip.zzg(str2, zzbm);
                            } else {
                                valueOf = zzeu2.zzafq.getLong("_et");
                            }
                        }
                    }
                    zzip = zzge().zzip();
                    str2 = "The engagement event does not contain any parameters. appId";
                    zzbm = zzfg.zzbm(str);
                    zzip.zzg(str2, zzbm);
                }
                zzkq zzkq2 = new zzkq();
                zzkp2.zzatf = new zzkq[]{zzkq2};
                zzkq2.zzath = Integer.valueOf(1);
                zzkq2.zzatp = "android";
                zzkq2.zzti = zzbc.zzah();
                zzkq2.zzadt = zzbc.zzgn();
                zzkq2.zzth = zzbc.zzag();
                long zzgm = zzbc.zzgm();
                zzkp zzkp3 = zzkp2;
                zzkq2.zzaub = zzgm == -2147483648L ? null : Integer.valueOf((int) zzgm);
                zzkq2.zzatt = Long.valueOf(zzbc.zzgo());
                zzkq2.zzadm = zzbc.getGmpAppId();
                zzkq2.zzatx = Long.valueOf(zzbc.zzgp());
                if (this.zzacw.isEnabled() && zzef.zzhk() && zzgg().zzat(zzkq2.zzti)) {
                    zzkq2.zzauh = null;
                }
                Pair zzbo = zzgf().zzbo(zzbc.zzah());
                if (zzbc.zzhd() && zzbo != null && !TextUtils.isEmpty((CharSequence) zzbo.first)) {
                    zzkq2.zzatv = (String) zzbo.first;
                    zzkq2.zzatw = (Boolean) zzbo.second;
                }
                zzfw().zzch();
                zzkq2.zzatr = Build.MODEL;
                zzfw().zzch();
                zzkq2.zzatq = VERSION.RELEASE;
                zzkq2.zzats = Integer.valueOf((int) zzfw().zzic());
                zzkq2.zzafn = zzfw().zzid();
                zzkq2.zzadl = zzbc.getAppInstanceId();
                zzkq2.zzado = zzbc.zzgj();
                List zzbb = zzix().zzbb(zzbc.zzah());
                zzkq2.zzatj = new zzks[zzbb.size()];
                if (zzav) {
                    zzjz = zzix().zzh(zzkq2.zzti, "_lte");
                    if (zzjz != null) {
                        if (zzjz.value != null) {
                            if (valueOf.longValue() > 0) {
                                zzjz zzjz2 = new zzjz(zzkq2.zzti, "auto", "_lte", zzbt().currentTimeMillis(), Long.valueOf(((Long) zzjz.value).longValue() + valueOf.longValue()));
                                zzjz = zzjz2;
                            }
                        }
                    }
                    zzjz = new zzjz(zzkq2.zzti, "auto", "_lte", zzbt().currentTimeMillis(), valueOf);
                } else {
                    zzjz = null;
                }
                zzks zzks = null;
                for (int i = 0; i < zzbb.size(); i++) {
                    zzks zzks2 = new zzks();
                    zzkq2.zzatj[i] = zzks2;
                    zzks2.name = ((zzjz) zzbb.get(i)).name;
                    zzks2.zzaun = Long.valueOf(((zzjz) zzbb.get(i)).zzaqz);
                    zzgb().zza(zzks2, ((zzjz) zzbb.get(i)).value);
                    if (zzav && "_lte".equals(zzks2.name)) {
                        zzks2.zzate = (Long) zzjz.value;
                        zzks2.zzaun = Long.valueOf(zzbt().currentTimeMillis());
                        zzks = zzks2;
                    }
                }
                if (zzav && zzks == null) {
                    zzks zzks3 = new zzks();
                    zzks3.name = "_lte";
                    zzks3.zzaun = Long.valueOf(zzbt().currentTimeMillis());
                    zzks3.zzate = (Long) zzjz.value;
                    zzkq2.zzatj = (zzks[]) Arrays.copyOf(zzkq2.zzatj, zzkq2.zzatj.length + 1);
                    zzkq2.zzatj[zzkq2.zzatj.length - 1] = zzks3;
                }
                if (valueOf.longValue() > 0) {
                    zzix().zza(zzjz);
                }
                Bundle zzif = zzeu2.zzafq.zzif();
                if ("_iap".equals(zzeu2.name)) {
                    zzif.putLong("_c", 1);
                    zzge().zzis().log("Marking in-app purchase as real-time");
                    zzif.putLong("_r", 1);
                }
                zzif.putString("_o", zzeu2.origin);
                if (zzgb().zzcj(zzkq2.zzti)) {
                    zzgb().zza(zzif, "_dbg", (Object) Long.valueOf(1));
                    zzgb().zza(zzif, "_r", (Object) Long.valueOf(1));
                }
                zzeq zzf = zzix().zzf(str3, zzeu2.name);
                if (zzf == null) {
                    zzkq = zzkq2;
                    j = 0;
                    zzdy = zzbc;
                    zzeq zzeq = r3;
                    bundle = zzif;
                    zzkp = zzkp3;
                    zzeq zzeq2 = new zzeq(str3, zzeu2.name, 1, 0, zzeu2.zzagb, 0, null, null, null);
                    zzix().zza(zzeq);
                    j2 = 0;
                    bArr = 0;
                } else {
                    zzkq = zzkq2;
                    j = 0;
                    zzdy = zzbc;
                    bundle = zzif;
                    zzkp = zzkp3;
                    long j3 = zzf.zzaft;
                    zzix().zza(zzf.zzac(zzeu2.zzagb).zzie());
                    j2 = j3;
                    bArr = 0;
                }
                zzep zzep = new zzep(this.zzacw, zzeu2.origin, str, zzeu2.name, zzeu2.zzagb, j2, bundle);
                zzkn zzkn = new zzkn();
                zzkq zzkq3 = zzkq;
                zzkq3.zzati = new zzkn[]{zzkn};
                zzkn.zzatb = Long.valueOf(zzep.timestamp);
                zzkn.name = zzep.name;
                zzkn.zzatc = Long.valueOf(zzep.zzafp);
                zzkn.zzata = new zzko[zzep.zzafq.size()];
                Iterator it = zzep.zzafq.iterator();
                int i2 = 0;
                while (it.hasNext()) {
                    String str4 = (String) it.next();
                    zzko zzko = new zzko();
                    int i3 = i2 + 1;
                    zzkn.zzata[i2] = zzko;
                    zzko.name = str4;
                    zzgb().zza(zzko, zzep.zzafq.get(str4));
                    i2 = i3;
                }
                zzdy zzdy2 = zzdy;
                zzkq3.zzaua = zza(zzdy2.zzah(), zzkq3.zzatj, zzkq3.zzati);
                zzkq3.zzatl = zzkn.zzatb;
                zzkq3.zzatm = zzkn.zzatb;
                long zzgl = zzdy2.zzgl();
                zzkq3.zzato = zzgl != j ? Long.valueOf(zzgl) : bArr;
                long zzgk = zzdy2.zzgk();
                if (zzgk != j) {
                    zzgl = zzgk;
                }
                zzkq3.zzatn = zzgl != j ? Long.valueOf(zzgl) : bArr;
                zzdy2.zzgt();
                zzkq3.zzaty = Integer.valueOf((int) zzdy2.zzgq());
                zzkq3.zzatu = Long.valueOf(12451);
                zzkq3.zzatk = Long.valueOf(zzbt().currentTimeMillis());
                zzkq3.zzatz = Boolean.TRUE;
                zzdy2.zzm(zzkq3.zzatl.longValue());
                zzdy2.zzn(zzkq3.zzatm.longValue());
                zzix().zza(zzdy2);
                zzix().setTransactionSuccessful();
                zzix().endTransaction();
                zzkp zzkp4 = zzkp;
                try {
                    byte[] bArr2 = new byte[zzkp4.zzvm()];
                    zzabw zzb = zzabw.zzb(bArr2, 0, bArr2.length);
                    zzkp4.zza(zzb);
                    zzb.zzve();
                    return zzgb().zza(bArr2);
                } catch (IOException e) {
                    zzge().zzim().zze("Data loss. Failed to bundle and serialize. appId", zzfg.zzbm(str), e);
                    return bArr;
                }
            }
            byte[] bArr3 = new byte[0];
            zzix().endTransaction();
            return bArr3;
        } catch (Throwable th) {
            Throwable th2 = th;
            zzix().endTransaction();
            throw th2;
        }
    }

    @WorkerThread
    public void zzab() {
        zzgd().zzab();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzb(zzed zzed, zzdz zzdz) {
        zzfi zzim;
        String str;
        Object zzbm;
        String zzbl;
        Object value;
        zzfi zzim2;
        String str2;
        Object zzbm2;
        String zzbl2;
        Object obj;
        Preconditions.checkNotNull(zzed);
        Preconditions.checkNotEmpty(zzed.packageName);
        Preconditions.checkNotNull(zzed.origin);
        Preconditions.checkNotNull(zzed.zzaep);
        Preconditions.checkNotEmpty(zzed.zzaep.name);
        zzab();
        zzkq();
        if (!TextUtils.isEmpty(zzdz.zzadm)) {
            if (!zzdz.zzadw) {
                zzg(zzdz);
                return;
            }
            zzed zzed2 = new zzed(zzed);
            boolean z = false;
            zzed2.active = false;
            zzix().beginTransaction();
            try {
                zzed zzi = zzix().zzi(zzed2.packageName, zzed2.zzaep.name);
                if (zzi != null && !zzi.origin.equals(zzed2.origin)) {
                    zzge().zzip().zzd("Updating a conditional user property with different origin. name, origin, origin (from DB)", zzga().zzbl(zzed2.zzaep.name), zzed2.origin, zzi.origin);
                }
                if (zzi != null && zzi.active) {
                    zzed2.origin = zzi.origin;
                    zzed2.creationTimestamp = zzi.creationTimestamp;
                    zzed2.triggerTimeout = zzi.triggerTimeout;
                    zzed2.triggerEventName = zzi.triggerEventName;
                    zzed2.zzaer = zzi.zzaer;
                    zzed2.active = zzi.active;
                    zzjx zzjx = new zzjx(zzed2.zzaep.name, zzi.zzaep.zzaqz, zzed2.zzaep.getValue(), zzi.zzaep.origin);
                    zzed2.zzaep = zzjx;
                } else if (TextUtils.isEmpty(zzed2.triggerEventName)) {
                    zzjx zzjx2 = new zzjx(zzed2.zzaep.name, zzed2.creationTimestamp, zzed2.zzaep.getValue(), zzed2.zzaep.origin);
                    zzed2.zzaep = zzjx2;
                    zzed2.active = true;
                    z = true;
                }
                if (zzed2.active) {
                    zzjx zzjx3 = zzed2.zzaep;
                    zzjz zzjz = new zzjz(zzed2.packageName, zzed2.origin, zzjx3.name, zzjx3.zzaqz, zzjx3.getValue());
                    if (zzix().zza(zzjz)) {
                        zzim2 = zzge().zzis();
                        str2 = "User property updated immediately";
                        zzbm2 = zzed2.packageName;
                        zzbl2 = zzga().zzbl(zzjz.name);
                        obj = zzjz.value;
                    } else {
                        zzim2 = zzge().zzim();
                        str2 = "(2)Too many active user properties, ignoring";
                        zzbm2 = zzfg.zzbm(zzed2.packageName);
                        zzbl2 = zzga().zzbl(zzjz.name);
                        obj = zzjz.value;
                    }
                    zzim2.zzd(str2, zzbm2, zzbl2, obj);
                    if (z && zzed2.zzaer != null) {
                        zzc(new zzeu(zzed2.zzaer, zzed2.creationTimestamp), zzdz);
                    }
                }
                if (zzix().zza(zzed2)) {
                    zzim = zzge().zzis();
                    str = "Conditional property added";
                    zzbm = zzed2.packageName;
                    zzbl = zzga().zzbl(zzed2.zzaep.name);
                    value = zzed2.zzaep.getValue();
                } else {
                    zzim = zzge().zzim();
                    str = "Too many conditional properties, ignoring";
                    zzbm = zzfg.zzbm(zzed2.packageName);
                    zzbl = zzga().zzbl(zzed2.zzaep.name);
                    value = zzed2.zzaep.getValue();
                }
                zzim.zzd(str, zzbm, zzbl, value);
                zzix().setTransactionSuccessful();
            } finally {
                zzix().endTransaction();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzb(zzeu zzeu, zzdz zzdz) {
        List<zzed> list;
        List<zzed> list2;
        List list3;
        zzfi zzim;
        String str;
        Object zzbm;
        String zzbl;
        Object obj;
        zzeu zzeu2 = zzeu;
        zzdz zzdz2 = zzdz;
        Preconditions.checkNotNull(zzdz);
        Preconditions.checkNotEmpty(zzdz2.packageName);
        zzab();
        zzkq();
        String str2 = zzdz2.packageName;
        long j = zzeu2.zzagb;
        zzgb();
        if (zzka.zzd(zzeu, zzdz)) {
            if (!zzdz2.zzadw) {
                zzg(zzdz2);
                return;
            }
            zzix().beginTransaction();
            try {
                zzei zzix = zzix();
                Preconditions.checkNotEmpty(str2);
                zzix.zzab();
                zzix.zzch();
                if (j < 0) {
                    zzix.zzge().zzip().zze("Invalid time querying timed out conditional properties", zzfg.zzbm(str2), Long.valueOf(j));
                    list = Collections.emptyList();
                } else {
                    list = zzix.zzb("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str2, String.valueOf(j)});
                }
                for (zzed zzed : list) {
                    if (zzed != null) {
                        zzge().zzis().zzd("User property timed out", zzed.packageName, zzga().zzbl(zzed.zzaep.name), zzed.zzaep.getValue());
                        if (zzed.zzaeq != null) {
                            zzc(new zzeu(zzed.zzaeq, j), zzdz2);
                        }
                        zzix().zzj(str2, zzed.zzaep.name);
                    }
                }
                zzei zzix2 = zzix();
                Preconditions.checkNotEmpty(str2);
                zzix2.zzab();
                zzix2.zzch();
                if (j < 0) {
                    zzix2.zzge().zzip().zze("Invalid time querying expired conditional properties", zzfg.zzbm(str2), Long.valueOf(j));
                    list2 = Collections.emptyList();
                } else {
                    list2 = zzix2.zzb("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str2, String.valueOf(j)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (zzed zzed2 : list2) {
                    if (zzed2 != null) {
                        zzge().zzis().zzd("User property expired", zzed2.packageName, zzga().zzbl(zzed2.zzaep.name), zzed2.zzaep.getValue());
                        zzix().zzg(str2, zzed2.zzaep.name);
                        if (zzed2.zzaes != null) {
                            arrayList.add(zzed2.zzaes);
                        }
                        zzix().zzj(str2, zzed2.zzaep.name);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i = 0;
                while (i < size) {
                    Object obj2 = arrayList2.get(i);
                    i++;
                    zzc(new zzeu((zzeu) obj2, j), zzdz2);
                }
                zzei zzix3 = zzix();
                String str3 = zzeu2.name;
                Preconditions.checkNotEmpty(str2);
                Preconditions.checkNotEmpty(str3);
                zzix3.zzab();
                zzix3.zzch();
                if (j < 0) {
                    zzix3.zzge().zzip().zzd("Invalid time querying triggered conditional properties", zzfg.zzbm(str2), zzix3.zzga().zzbj(str3), Long.valueOf(j));
                    list3 = Collections.emptyList();
                } else {
                    list3 = zzix3.zzb("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str2, str3, String.valueOf(j)});
                }
                ArrayList arrayList3 = new ArrayList(list3.size());
                Iterator it = list3.iterator();
                while (it.hasNext()) {
                    zzed zzed3 = (zzed) it.next();
                    if (zzed3 != null) {
                        zzjx zzjx = zzed3.zzaep;
                        zzjz zzjz = r5;
                        Iterator it2 = it;
                        zzed zzed4 = zzed3;
                        zzjz zzjz2 = new zzjz(zzed3.packageName, zzed3.origin, zzjx.name, j, zzjx.getValue());
                        if (zzix().zza(zzjz)) {
                            zzim = zzge().zzis();
                            str = "User property triggered";
                            zzbm = zzed4.packageName;
                            zzbl = zzga().zzbl(zzjz.name);
                            obj = zzjz.value;
                        } else {
                            zzim = zzge().zzim();
                            str = "Too many active user properties, ignoring";
                            zzbm = zzfg.zzbm(zzed4.packageName);
                            zzbl = zzga().zzbl(zzjz.name);
                            obj = zzjz.value;
                        }
                        zzim.zzd(str, zzbm, zzbl, obj);
                        if (zzed4.zzaer != null) {
                            arrayList3.add(zzed4.zzaer);
                        }
                        zzed4.zzaep = new zzjx(zzjz);
                        zzed4.active = true;
                        zzix().zza(zzed4);
                        it = it2;
                    }
                }
                zzc(zzeu, zzdz);
                ArrayList arrayList4 = arrayList3;
                int size2 = arrayList4.size();
                int i2 = 0;
                while (i2 < size2) {
                    Object obj3 = arrayList4.get(i2);
                    i2++;
                    zzc(new zzeu((zzeu) obj3, j), zzdz2);
                }
                zzix().setTransactionSuccessful();
                zzix().endTransaction();
            } catch (Throwable th) {
                Throwable th2 = th;
                zzix().endTransaction();
                throw th2;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzb(zzjq zzjq) {
        this.zzaqj++;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzb(zzjx zzjx, zzdz zzdz) {
        zzab();
        zzkq();
        if (!TextUtils.isEmpty(zzdz.zzadm)) {
            if (!zzdz.zzadw) {
                zzg(zzdz);
                return;
            }
            int zzcf = zzgb().zzcf(zzjx.name);
            int i = 0;
            if (zzcf != 0) {
                zzgb();
                zzgb().zza(zzdz.packageName, zzcf, "_ev", zzka.zza(zzjx.name, 24, true), zzjx.name != null ? zzjx.name.length() : 0);
                return;
            }
            int zzi = zzgb().zzi(zzjx.name, zzjx.getValue());
            if (zzi != 0) {
                zzgb();
                String zza = zzka.zza(zzjx.name, 24, true);
                Object value = zzjx.getValue();
                if (value != null && ((value instanceof String) || (value instanceof CharSequence))) {
                    i = String.valueOf(value).length();
                }
                zzgb().zza(zzdz.packageName, zzi, "_ev", zza, i);
                return;
            }
            Object zzj = zzgb().zzj(zzjx.name, zzjx.getValue());
            if (zzj != null) {
                zzjz zzjz = new zzjz(zzdz.packageName, zzjx.origin, zzjx.name, zzjx.zzaqz, zzj);
                zzge().zzis().zze("Setting user property", zzga().zzbl(zzjz.name), zzj);
                zzix().beginTransaction();
                try {
                    zzg(zzdz);
                    boolean zza2 = zzix().zza(zzjz);
                    zzix().setTransactionSuccessful();
                    if (zza2) {
                        zzge().zzis().zze("User property set", zzga().zzbl(zzjz.name), zzjz.value);
                    } else {
                        zzge().zzim().zze("Too many unique user properties are set. Ignoring user property", zzga().zzbl(zzjz.name), zzjz.value);
                        zzgb().zza(zzdz.packageName, 9, (String) null, (String) null, 0);
                    }
                } finally {
                    zzix().endTransaction();
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0124 A[Catch:{ all -> 0x0167, all -> 0x000f }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0132 A[Catch:{ all -> 0x0167, all -> 0x000f }] */
    @WorkerThread
    @VisibleForTesting
    public final void zzb(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        zzei zzix;
        zzab();
        zzkq();
        Preconditions.checkNotEmpty(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.zzaql = false;
                zzkv();
                throw th2;
            }
        }
        zzge().zzit().zzg("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        zzix().beginTransaction();
        zzdy zzbc = zzix().zzbc(str);
        boolean z = true;
        boolean z2 = (i == 200 || i == 204 || i == 304) && th == null;
        if (zzbc == null) {
            zzge().zzip().zzg("App does not exist in onConfigFetched. appId", zzfg.zzbm(str));
            zzix().setTransactionSuccessful();
            zzix = zzix();
        } else {
            if (!z2) {
                if (i != 404) {
                    zzbc.zzt(zzbt().currentTimeMillis());
                    zzix().zza(zzbc);
                    zzge().zzit().zze("Fetching config failed. code, error", Integer.valueOf(i), th);
                    zzkm().zzbw(str);
                    zzgf().zzajv.set(zzbt().currentTimeMillis());
                    if (i != 503) {
                        if (i != 429) {
                            z = false;
                        }
                    }
                    if (z) {
                        zzgf().zzajw.set(zzbt().currentTimeMillis());
                    }
                    zzku();
                    zzix().setTransactionSuccessful();
                    zzix = zzix();
                }
            }
            List list = map != null ? (List) map.get("Last-Modified") : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i != 404) {
                if (i != 304) {
                    if (!zzkm().zza(str, bArr, str2)) {
                        zzix = zzix();
                    }
                    zzbc.zzs(zzbt().currentTimeMillis());
                    zzix().zza(zzbc);
                    if (i != 404) {
                        zzge().zziq().zzg("Config not found. Using empty config. appId", str);
                    } else {
                        zzge().zzit().zze("Successfully fetched config. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                    }
                    if (zzkn().zzex() && zzkt()) {
                        zzks();
                        zzix().setTransactionSuccessful();
                        zzix = zzix();
                    }
                    zzku();
                    zzix().setTransactionSuccessful();
                    zzix = zzix();
                }
            }
            if (zzkm().zzbu(str) == null && !zzkm().zza(str, null, null)) {
                zzix = zzix();
            }
            zzbc.zzs(zzbt().currentTimeMillis());
            zzix().zza(zzbc);
            if (i != 404) {
            }
            zzks();
            zzix().setTransactionSuccessful();
            zzix = zzix();
        }
        zzix.endTransaction();
        this.zzaql = false;
        zzkv();
    }

    public Clock zzbt() {
        return this.zzacw.zzbt();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzc(zzed zzed, zzdz zzdz) {
        Preconditions.checkNotNull(zzed);
        Preconditions.checkNotEmpty(zzed.packageName);
        Preconditions.checkNotNull(zzed.zzaep);
        Preconditions.checkNotEmpty(zzed.zzaep.name);
        zzab();
        zzkq();
        if (!TextUtils.isEmpty(zzdz.zzadm)) {
            if (!zzdz.zzadw) {
                zzg(zzdz);
                return;
            }
            zzix().beginTransaction();
            try {
                zzg(zzdz);
                zzed zzi = zzix().zzi(zzed.packageName, zzed.zzaep.name);
                if (zzi != null) {
                    zzge().zzis().zze("Removing conditional user property", zzed.packageName, zzga().zzbl(zzed.zzaep.name));
                    zzix().zzj(zzed.packageName, zzed.zzaep.name);
                    if (zzi.active) {
                        zzix().zzg(zzed.packageName, zzed.zzaep.name);
                    }
                    if (zzed.zzaes != null) {
                        Bundle bundle = null;
                        if (zzed.zzaes.zzafq != null) {
                            bundle = zzed.zzaes.zzafq.zzif();
                        }
                        Bundle bundle2 = bundle;
                        zzc(zzgb().zza(zzed.zzaes.name, bundle2, zzi.origin, zzed.zzaes.zzagb, true, false), zzdz);
                    }
                } else {
                    zzge().zzip().zze("Conditional user property doesn't exist", zzfg.zzbm(zzed.packageName), zzga().zzbl(zzed.zzaep.name));
                }
                zzix().setTransactionSuccessful();
            } finally {
                zzix().endTransaction();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzc(zzeu zzeu, String str) {
        zzeu zzeu2 = zzeu;
        String str2 = str;
        zzdy zzbc = zzix().zzbc(str2);
        if (zzbc == null || TextUtils.isEmpty(zzbc.zzag())) {
            zzge().zzis().zzg("No app data available; dropping event", str2);
            return;
        }
        Boolean zzc = zzc(zzbc);
        if (zzc == null) {
            if (!"_ui".equals(zzeu2.name)) {
                zzge().zzip().zzg("Could not find package. appId", zzfg.zzbm(str));
            }
        } else if (!zzc.booleanValue()) {
            zzge().zzim().zzg("App version does not match; dropping event. appId", zzfg.zzbm(str));
            return;
        }
        zzdz zzdz = r2;
        zzdz zzdz2 = new zzdz(str2, zzbc.getGmpAppId(), zzbc.zzag(), zzbc.zzgm(), zzbc.zzgn(), zzbc.zzgo(), zzbc.zzgp(), (String) null, zzbc.isMeasurementEnabled(), false, zzbc.zzgj(), zzbc.zzhc(), 0, 0, zzbc.zzhd(), zzbc.zzhe(), false);
        zzb(zzeu2, zzdz);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzc(zzjx zzjx, zzdz zzdz) {
        zzab();
        zzkq();
        if (!TextUtils.isEmpty(zzdz.zzadm)) {
            if (!zzdz.zzadw) {
                zzg(zzdz);
                return;
            }
            zzge().zzis().zzg("Removing user property", zzga().zzbl(zzjx.name));
            zzix().beginTransaction();
            try {
                zzg(zzdz);
                zzix().zzg(zzdz.packageName, zzjx.name);
                zzix().setTransactionSuccessful();
                zzge().zzis().zzg("User property removed", zzga().zzbl(zzjx.name));
            } finally {
                zzix().endTransaction();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final zzdz zzcb(String str) {
        zzfi zzis;
        String str2;
        Object obj;
        String str3 = str;
        zzdy zzbc = zzix().zzbc(str3);
        if (zzbc == null || TextUtils.isEmpty(zzbc.zzag())) {
            zzis = zzge().zzis();
            str2 = "No app data available; dropping";
            obj = str3;
        } else {
            Boolean zzc = zzc(zzbc);
            if (zzc == null || zzc.booleanValue()) {
                zzdz zzdz = new zzdz(str3, zzbc.getGmpAppId(), zzbc.zzag(), zzbc.zzgm(), zzbc.zzgn(), zzbc.zzgo(), zzbc.zzgp(), (String) null, zzbc.isMeasurementEnabled(), false, zzbc.zzgj(), zzbc.zzhc(), 0, 0, zzbc.zzhd(), zzbc.zzhe(), false);
                return zzdz;
            }
            zzis = zzge().zzim();
            str2 = "App version does not match; dropping. appId";
            obj = zzfg.zzbm(str);
        }
        zzis.zzg(str2, obj);
        return null;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    @VisibleForTesting
    public final void zzd(zzdz zzdz) {
        if (this.zzaqq != null) {
            this.zzaqr = new ArrayList();
            this.zzaqr.addAll(this.zzaqq);
        }
        zzei zzix = zzix();
        String str = zzdz.packageName;
        Preconditions.checkNotEmpty(str);
        zzix.zzab();
        zzix.zzch();
        try {
            SQLiteDatabase writableDatabase = zzix.getWritableDatabase();
            String[] strArr = {str};
            int delete = 0 + writableDatabase.delete("apps", "app_id=?", strArr) + writableDatabase.delete("events", "app_id=?", strArr) + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("conditional_properties", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("queue", "app_id=?", strArr) + writableDatabase.delete("audience_filter_values", "app_id=?", strArr) + writableDatabase.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                zzix.zzge().zzit().zze("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzix.zzge().zzim().zze("Error resetting analytics data. appId, error", zzfg.zzbm(str), e);
        }
        zzdz zza = zza(getContext(), zzdz.packageName, zzdz.zzadm, zzdz.zzadw, zzdz.zzady, zzdz.zzadz, zzdz.zzaem);
        if (!zzgg().zzaz(zzdz.packageName) || zzdz.zzadw) {
            zzf(zza);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zze(zzdz zzdz) {
        zzab();
        zzkq();
        Preconditions.checkNotEmpty(zzdz.packageName);
        zzg(zzdz);
    }

    /* JADX WARNING: Removed duplicated region for block: B:115:0x03a0 A[Catch:{ SQLiteException -> 0x013e, all -> 0x03c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01e4 A[Catch:{ SQLiteException -> 0x013e, all -> 0x03c9 }] */
    @WorkerThread
    public final void zzf(zzdz zzdz) {
        zzdy zzbc;
        zzeq zzeq;
        zzeu zzeu;
        long j;
        zzeu zzeu2;
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        boolean z;
        zzei zzix;
        String str;
        String str2;
        zzeu zzeu3;
        zzei zzix2;
        String zzah;
        zzdz zzdz2 = zzdz;
        zzab();
        zzkq();
        Preconditions.checkNotNull(zzdz);
        Preconditions.checkNotEmpty(zzdz2.packageName);
        if (!TextUtils.isEmpty(zzdz2.zzadm)) {
            zzdy zzbc2 = zzix().zzbc(zzdz2.packageName);
            if (zzbc2 != null && TextUtils.isEmpty(zzbc2.getGmpAppId()) && !TextUtils.isEmpty(zzdz2.zzadm)) {
                zzbc2.zzs(0);
                zzix().zza(zzbc2);
                zzkm().zzbx(zzdz2.packageName);
            }
            if (!zzdz2.zzadw) {
                zzg(zzdz);
                return;
            }
            long j2 = zzdz2.zzaem;
            if (j2 == 0) {
                j2 = zzbt().currentTimeMillis();
            }
            int i = zzdz2.zzaen;
            if (!(i == 0 || i == 1)) {
                zzge().zzip().zze("Incorrect app type, assuming installed app. appId, appType", zzfg.zzbm(zzdz2.packageName), Integer.valueOf(i));
                i = 0;
            }
            zzix().beginTransaction();
            try {
                zzbc = zzix().zzbc(zzdz2.packageName);
                if (!(zzbc == null || zzbc.getGmpAppId() == null || zzbc.getGmpAppId().equals(zzdz2.zzadm))) {
                    zzge().zzip().zzg("New GMP App Id passed in. Removing cached database data. appId", zzfg.zzbm(zzbc.zzah()));
                    zzix2 = zzix();
                    zzah = zzbc.zzah();
                    zzix2.zzch();
                    zzix2.zzab();
                    Preconditions.checkNotEmpty(zzah);
                    SQLiteDatabase writableDatabase = zzix2.getWritableDatabase();
                    String[] strArr = {zzah};
                    int delete = writableDatabase.delete("events", "app_id=?", strArr) + 0 + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("conditional_properties", "app_id=?", strArr) + writableDatabase.delete("apps", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("event_filters", "app_id=?", strArr) + writableDatabase.delete("property_filters", "app_id=?", strArr) + writableDatabase.delete("audience_filter_values", "app_id=?", strArr);
                    if (delete > 0) {
                        zzix2.zzge().zzit().zze("Deleted application data. app, records", zzah, Integer.valueOf(delete));
                    }
                    zzbc = null;
                }
            } catch (SQLiteException e) {
                zzix2.zzge().zzim().zze("Error deleting application data. appId, error", zzfg.zzbm(zzah), e);
            } catch (Throwable th) {
                Throwable th2 = th;
                zzix().endTransaction();
                throw th2;
            }
            if (zzbc != null) {
                if (zzbc.zzgm() != -2147483648L) {
                    if (zzbc.zzgm() != zzdz2.zzads) {
                        Bundle bundle = new Bundle();
                        bundle.putString("_pv", zzbc.zzag());
                        zzeu3 = new zzeu("_au", new zzer(bundle), "auto", j2);
                    }
                } else if (zzbc.zzag() != null && !zzbc.zzag().equals(zzdz2.zzth)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_pv", zzbc.zzag());
                    zzeu3 = new zzeu("_au", new zzer(bundle2), "auto", j2);
                }
                zzb(zzeu3, zzdz2);
            }
            zzg(zzdz);
            if (i == 0) {
                zzix = zzix();
                str = zzdz2.packageName;
                str2 = "_f";
            } else if (i == 1) {
                zzix = zzix();
                str = zzdz2.packageName;
                str2 = "_v";
            } else {
                zzeq = null;
                if (zzeq != null) {
                    long j3 = 3600000 * (1 + (j2 / 3600000));
                    if (i == 0) {
                        j = 1;
                        zzjx zzjx = new zzjx("_fot", j2, Long.valueOf(j3), "auto");
                        zzb(zzjx, zzdz2);
                        zzab();
                        zzkq();
                        Bundle bundle3 = new Bundle();
                        bundle3.putLong("_c", 1);
                        bundle3.putLong("_r", 1);
                        bundle3.putLong("_uwa", 0);
                        bundle3.putLong("_pfo", 0);
                        bundle3.putLong("_sys", 0);
                        bundle3.putLong("_sysu", 0);
                        if (zzgg().zzaz(zzdz2.packageName) && zzdz2.zzaeo) {
                            bundle3.putLong("_dac", 1);
                        }
                        if (getContext().getPackageManager() == null) {
                            zzge().zzim().zzg("PackageManager is null, first open report might be inaccurate. appId", zzfg.zzbm(zzdz2.packageName));
                        } else {
                            try {
                                packageInfo = Wrappers.packageManager(getContext()).getPackageInfo(zzdz2.packageName, 0);
                            } catch (NameNotFoundException e2) {
                                zzge().zzim().zze("Package info is null, first open report might be inaccurate. appId", zzfg.zzbm(zzdz2.packageName), e2);
                                packageInfo = null;
                            }
                            if (!(packageInfo == null || packageInfo.firstInstallTime == 0)) {
                                if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                    bundle3.putLong("_uwa", 1);
                                    z = false;
                                } else {
                                    z = true;
                                }
                                zzjx zzjx2 = new zzjx("_fi", j2, Long.valueOf(z ? 1 : 0), "auto");
                                zzb(zzjx2, zzdz2);
                            }
                            try {
                                applicationInfo = Wrappers.packageManager(getContext()).getApplicationInfo(zzdz2.packageName, 0);
                            } catch (NameNotFoundException e3) {
                                zzge().zzim().zze("Application info is null, first open report might be inaccurate. appId", zzfg.zzbm(zzdz2.packageName), e3);
                                applicationInfo = null;
                            }
                            if (applicationInfo != null) {
                                if ((applicationInfo.flags & 1) != 0) {
                                    bundle3.putLong("_sys", 1);
                                }
                                if ((applicationInfo.flags & 128) != 0) {
                                    bundle3.putLong("_sysu", 1);
                                }
                            }
                        }
                        zzei zzix3 = zzix();
                        String str3 = zzdz2.packageName;
                        Preconditions.checkNotEmpty(str3);
                        zzix3.zzab();
                        zzix3.zzch();
                        long zzm = zzix3.zzm(str3, "first_open_count");
                        if (zzm >= 0) {
                            bundle3.putLong("_pfo", zzm);
                        }
                        zzeu2 = new zzeu("_f", new zzer(bundle3), "auto", j2);
                    } else {
                        j = 1;
                        if (i == 1) {
                            zzjx zzjx3 = new zzjx("_fvt", j2, Long.valueOf(j3), "auto");
                            zzb(zzjx3, zzdz2);
                            zzab();
                            zzkq();
                            Bundle bundle4 = new Bundle();
                            bundle4.putLong("_c", 1);
                            bundle4.putLong("_r", 1);
                            if (zzgg().zzaz(zzdz2.packageName) && zzdz2.zzaeo) {
                                bundle4.putLong("_dac", 1);
                            }
                            zzeu2 = new zzeu("_v", new zzer(bundle4), "auto", j2);
                        }
                        Bundle bundle5 = new Bundle();
                        bundle5.putLong("_et", j);
                        zzeu = new zzeu("_e", new zzer(bundle5), "auto", j2);
                    }
                    zzb(zzeu2, zzdz2);
                    Bundle bundle52 = new Bundle();
                    bundle52.putLong("_et", j);
                    zzeu = new zzeu("_e", new zzer(bundle52), "auto", j2);
                } else {
                    if (zzdz2.zzael) {
                        zzeu = new zzeu("_cd", new zzer(new Bundle()), "auto", j2);
                    }
                    zzix().setTransactionSuccessful();
                    zzix().endTransaction();
                }
                zzb(zzeu, zzdz2);
                zzix().setTransactionSuccessful();
                zzix().endTransaction();
            }
            zzeq = zzix.zzf(str, str2);
            if (zzeq != null) {
            }
            zzb(zzeu, zzdz2);
            zzix().setTransactionSuccessful();
            zzix().endTransaction();
        }
    }

    public zzeo zzfw() {
        return this.zzacw.zzfw();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzg(Runnable runnable) {
        zzab();
        if (this.zzaqi == null) {
            this.zzaqi = new ArrayList();
        }
        this.zzaqi.add(runnable);
    }

    public zzfe zzga() {
        return this.zzacw.zzga();
    }

    public zzka zzgb() {
        return this.zzacw.zzgb();
    }

    public zzgg zzgd() {
        return this.zzacw.zzgd();
    }

    public zzfg zzge() {
        return this.zzacw.zzge();
    }

    public zzfr zzgf() {
        return this.zzacw.zzgf();
    }

    public zzef zzgg() {
        return this.zzacw.zzgg();
    }

    public final String zzh(zzdz zzdz) {
        try {
            return (String) zzgd().zzb((Callable<V>) new zzju<V>(this, zzdz)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            zzge().zzim().zze("Failed to get app instance id. appId", zzfg.zzbm(zzdz.packageName), e);
            return null;
        }
    }

    public final zzeb zziw() {
        zza((zzjq) this.zzaqf);
        return this.zzaqf;
    }

    public final zzei zzix() {
        zza((zzjq) this.zzaqc);
        return this.zzaqc;
    }

    public final zzfk zzkn() {
        zza((zzjq) this.zzaqb);
        return this.zzaqb;
    }

    /* access modifiers changed from: 0000 */
    public final void zzkq() {
        if (!this.zzvo) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:77|78) */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        zzge().zzim().zze("Failed to parse upload URL. Not uploading. appId", com.google.android.gms.internal.measurement.zzfg.zzbm(r4), r5);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x024e */
    @WorkerThread
    public final void zzks() {
        String str;
        zzfi zzit;
        String str2;
        zzab();
        zzkq();
        this.zzaqn = true;
        try {
            Boolean zzkf = this.zzacw.zzfx().zzkf();
            if (zzkf == null) {
                zzit = zzge().zzip();
                str2 = "Upload data called on the client side before use of service was decided";
            } else if (zzkf.booleanValue()) {
                zzit = zzge().zzim();
                str2 = "Upload called in the client side when service should be used";
            } else {
                if (this.zzaqh <= 0) {
                    zzab();
                    if (this.zzaqq != null) {
                        zzit = zzge().zzit();
                        str2 = "Uploading requested multiple times";
                    } else if (!zzkn().zzex()) {
                        zzge().zzit().log("Network not connected, ignoring upload request");
                    } else {
                        long currentTimeMillis = zzbt().currentTimeMillis();
                        String str3 = null;
                        zzd(null, currentTimeMillis - zzef.zzhi());
                        long j = zzgf().zzaju.get();
                        if (j != 0) {
                            zzge().zzis().zzg("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(currentTimeMillis - j)));
                        }
                        String zzhn = zzix().zzhn();
                        if (!TextUtils.isEmpty(zzhn)) {
                            if (this.zzaqs == -1) {
                                this.zzaqs = zzix().zzhu();
                            }
                            List zzb = zzix().zzb(zzhn, zzgg().zzb(zzhn, zzew.zzago), Math.max(0, zzgg().zzb(zzhn, zzew.zzagp)));
                            if (!zzb.isEmpty()) {
                                Iterator it = zzb.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        str = null;
                                        break;
                                    }
                                    zzkq zzkq = (zzkq) ((Pair) it.next()).first;
                                    if (!TextUtils.isEmpty(zzkq.zzatv)) {
                                        str = zzkq.zzatv;
                                        break;
                                    }
                                }
                                if (str != null) {
                                    int i = 0;
                                    while (true) {
                                        if (i >= zzb.size()) {
                                            break;
                                        }
                                        zzkq zzkq2 = (zzkq) ((Pair) zzb.get(i)).first;
                                        if (!TextUtils.isEmpty(zzkq2.zzatv) && !zzkq2.zzatv.equals(str)) {
                                            zzb = zzb.subList(0, i);
                                            break;
                                        }
                                        i++;
                                    }
                                }
                                zzkp zzkp = new zzkp();
                                zzkp.zzatf = new zzkq[zzb.size()];
                                ArrayList arrayList = new ArrayList(zzb.size());
                                boolean z = zzef.zzhk() && zzgg().zzat(zzhn);
                                for (int i2 = 0; i2 < zzkp.zzatf.length; i2++) {
                                    zzkp.zzatf[i2] = (zzkq) ((Pair) zzb.get(i2)).first;
                                    arrayList.add((Long) ((Pair) zzb.get(i2)).second);
                                    zzkp.zzatf[i2].zzatu = Long.valueOf(12451);
                                    zzkp.zzatf[i2].zzatk = Long.valueOf(currentTimeMillis);
                                    zzkp.zzatf[i2].zzatz = Boolean.valueOf(false);
                                    if (!z) {
                                        zzkp.zzatf[i2].zzauh = null;
                                    }
                                }
                                if (zzge().isLoggable(2)) {
                                    str3 = zzga().zza(zzkp);
                                }
                                byte[] zzb2 = zzgb().zzb(zzkp);
                                String str4 = (String) zzew.zzagy.get();
                                URL url = new URL(str4);
                                Preconditions.checkArgument(!arrayList.isEmpty());
                                if (this.zzaqq != null) {
                                    zzge().zzim().log("Set uploading progress before finishing the previous upload");
                                } else {
                                    this.zzaqq = new ArrayList(arrayList);
                                }
                                zzgf().zzajv.set(currentTimeMillis);
                                String str5 = "?";
                                if (zzkp.zzatf.length > 0) {
                                    str5 = zzkp.zzatf[0].zzti;
                                }
                                zzge().zzit().zzd("Uploading data. app, uncompressed size, data", str5, Integer.valueOf(zzb2.length), str3);
                                this.zzaqm = true;
                                zzfk zzkn = zzkn();
                                zzjs zzjs = new zzjs(this, zzhn);
                                zzkn.zzab();
                                zzkn.zzch();
                                Preconditions.checkNotNull(url);
                                Preconditions.checkNotNull(zzb2);
                                Preconditions.checkNotNull(zzjs);
                                zzgg zzgd = zzkn.zzgd();
                                zzfo zzfo = new zzfo(zzkn, zzhn, url, zzb2, null, zzjs);
                                zzgd.zzd((Runnable) zzfo);
                            }
                        } else {
                            this.zzaqs = -1;
                            String zzab = zzix().zzab(currentTimeMillis - zzef.zzhi());
                            if (!TextUtils.isEmpty(zzab)) {
                                zzdy zzbc = zzix().zzbc(zzab);
                                if (zzbc != null) {
                                    zzb(zzbc);
                                }
                            }
                        }
                    }
                }
                zzku();
            }
            zzit.log(str2);
        } finally {
            this.zzaqn = false;
            zzkv();
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzkx() {
        zzfi zzim;
        String str;
        zzab();
        zzkq();
        if (!this.zzaqg) {
            zzge().zzir().log("This instance being marked as an uploader");
            zzab();
            zzkq();
            if (zzky() && zzkw()) {
                int zza = zza(this.zzaqp);
                int zzij = this.zzacw.zzfv().zzij();
                zzab();
                if (zza > zzij) {
                    zzim = zzge().zzim();
                    str = "Panic: can't downgrade version. Previous, current version";
                } else if (zza < zzij) {
                    if (zza(zzij, this.zzaqp)) {
                        zzim = zzge().zzit();
                        str = "Storage version upgraded. Previous, current version";
                    } else {
                        zzim = zzge().zzim();
                        str = "Storage version upgrade failed. Previous, current version";
                    }
                }
                zzim.zze(str, Integer.valueOf(zza), Integer.valueOf(zzij));
            }
            this.zzaqg = true;
            zzku();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzkz() {
        this.zzaqk++;
    }

    /* access modifiers changed from: 0000 */
    public final zzgl zzla() {
        return this.zzacw;
    }

    public final void zzm(boolean z) {
        zzku();
    }
}
