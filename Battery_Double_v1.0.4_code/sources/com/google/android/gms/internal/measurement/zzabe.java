package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map.Entry;

final class zzabe implements Iterator<Entry<K, V>> {
    private int pos;
    private final /* synthetic */ zzaay zzbuf;
    private boolean zzbug;
    private Iterator<Entry<K, V>> zzbuh;

    private zzabe(zzaay zzaay) {
        this.zzbuf = zzaay;
        this.pos = -1;
    }

    /* synthetic */ zzabe(zzaay zzaay, zzaaz zzaaz) {
        this(zzaay);
    }

    private final Iterator<Entry<K, V>> zzup() {
        if (this.zzbuh == null) {
            this.zzbuh = this.zzbuf.zzbtz.entrySet().iterator();
        }
        return this.zzbuh;
    }

    public final boolean hasNext() {
        if (this.pos + 1 >= this.zzbuf.zzbty.size()) {
            return !this.zzbuf.zzbtz.isEmpty() && zzup().hasNext();
        }
        return true;
    }

    public final /* synthetic */ Object next() {
        this.zzbug = true;
        int i = this.pos + 1;
        this.pos = i;
        return (Entry) (i < this.zzbuf.zzbty.size() ? this.zzbuf.zzbty.get(this.pos) : zzup().next());
    }

    public final void remove() {
        if (!this.zzbug) {
            throw new IllegalStateException("remove() was called before next()");
        }
        this.zzbug = false;
        this.zzbuf.zzul();
        if (this.pos < this.zzbuf.zzbty.size()) {
            zzaay zzaay = this.zzbuf;
            int i = this.pos;
            this.pos = i - 1;
            zzaay.zzai(i);
            return;
        }
        zzup().remove();
    }
}
