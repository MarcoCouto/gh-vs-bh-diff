package com.google.android.gms.internal.firebase_abt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class zzg implements Cloneable {
    private Object value;
    private zze<?, ?> zzy;
    private List<zzl> zzz = new ArrayList();

    zzg() {
    }

    private final byte[] toByteArray() throws IOException {
        if (this.value != null) {
            throw new NoSuchMethodError();
        }
        int i = 0;
        for (zzl zzl : this.zzz) {
            i += zzb.zzf(zzl.tag) + 0 + zzl.zzac.length;
        }
        byte[] bArr = new byte[i];
        zzb zzb = zzb.zzb(bArr);
        if (this.value != null) {
            throw new NoSuchMethodError();
        }
        for (zzl zzl2 : this.zzz) {
            zzb.zze(zzl2.tag);
            zzb.zzc(zzl2.zzac);
        }
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzk */
    public final zzg clone() {
        Object clone;
        zzg zzg = new zzg();
        try {
            zzg.zzy = this.zzy;
            if (this.zzz == null) {
                zzg.zzz = null;
            } else {
                zzg.zzz.addAll(this.zzz);
            }
            if (this.value != null) {
                if (this.value instanceof zzj) {
                    clone = (zzj) ((zzj) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    clone = ((byte[]) this.value).clone();
                } else {
                    int i = 0;
                    if (this.value instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.value;
                        byte[][] bArr2 = new byte[bArr.length][];
                        zzg.value = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.value instanceof boolean[]) {
                        clone = ((boolean[]) this.value).clone();
                    } else if (this.value instanceof int[]) {
                        clone = ((int[]) this.value).clone();
                    } else if (this.value instanceof long[]) {
                        clone = ((long[]) this.value).clone();
                    } else if (this.value instanceof float[]) {
                        clone = ((float[]) this.value).clone();
                    } else if (this.value instanceof double[]) {
                        clone = ((double[]) this.value).clone();
                    } else if (this.value instanceof zzj[]) {
                        zzj[] zzjArr = (zzj[]) this.value;
                        zzj[] zzjArr2 = new zzj[zzjArr.length];
                        zzg.value = zzjArr2;
                        while (i < zzjArr.length) {
                            zzjArr2[i] = (zzj) zzjArr[i].clone();
                            i++;
                        }
                    }
                }
                zzg.value = clone;
                return zzg;
            }
            return zzg;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzg)) {
            return false;
        }
        zzg zzg = (zzg) obj;
        if (this.value == null || zzg.value == null) {
            if (this.zzz != null && zzg.zzz != null) {
                return this.zzz.equals(zzg.zzz);
            }
            try {
                return Arrays.equals(toByteArray(), zzg.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.zzy != zzg.zzy) {
            return false;
        } else {
            return !this.zzy.zzt.isArray() ? this.value.equals(zzg.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zzg.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zzg.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zzg.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zzg.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zzg.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zzg.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zzg.value);
        }
    }

    public final int hashCode() {
        try {
            return 527 + Arrays.hashCode(toByteArray());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zza(zzl zzl) throws IOException {
        if (this.zzz != null) {
            this.zzz.add(zzl);
        } else if (this.value instanceof zzj) {
            byte[] bArr = zzl.zzac;
            zza zza = zza.zza(bArr, 0, bArr.length);
            int zzg = zza.zzg();
            if (zzg != bArr.length - (zzg >= 0 ? zzb.zzf(zzg) : 10)) {
                throw zzi.zzl();
            }
            zzj zza2 = ((zzj) this.value).zza(zza);
            this.zzy = this.zzy;
            this.value = zza2;
            this.zzz = null;
        } else if (this.value instanceof zzj[]) {
            Collections.singletonList(zzl);
            throw new NoSuchMethodError();
        } else {
            Collections.singletonList(zzl);
            throw new NoSuchMethodError();
        }
    }
}
