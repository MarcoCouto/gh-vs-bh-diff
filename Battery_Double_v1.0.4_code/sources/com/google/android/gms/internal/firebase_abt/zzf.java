package com.google.android.gms.internal.firebase_abt;

public final class zzf implements Cloneable {
    private static final zzg zzu = new zzg();
    private int mSize;
    private boolean zzv;
    private int[] zzw;
    private zzg[] zzx;

    zzf() {
        this(10);
    }

    private zzf(int i) {
        this.zzv = false;
        int idealIntArraySize = idealIntArraySize(i);
        this.zzw = new int[idealIntArraySize];
        this.zzx = new zzg[idealIntArraySize];
        this.mSize = 0;
    }

    private static int idealIntArraySize(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        return i2 / 4;
    }

    private final int zzh(int i) {
        int i2 = this.mSize - 1;
        int i3 = 0;
        while (i3 <= i2) {
            int i4 = (i3 + i2) >>> 1;
            int i5 = this.zzw[i4];
            if (i5 < i) {
                i3 = i4 + 1;
            } else if (i5 <= i) {
                return i4;
            } else {
                i2 = i4 - 1;
            }
        }
        return i3 ^ -1;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.mSize;
        zzf zzf = new zzf(i);
        System.arraycopy(this.zzw, 0, zzf.zzw, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            if (this.zzx[i2] != null) {
                zzf.zzx[i2] = (zzg) this.zzx[i2].clone();
            }
        }
        zzf.mSize = i;
        return zzf;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzf)) {
            return false;
        }
        zzf zzf = (zzf) obj;
        if (this.mSize != zzf.mSize) {
            return false;
        }
        int[] iArr = this.zzw;
        int[] iArr2 = zzf.zzw;
        int i = this.mSize;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            zzg[] zzgArr = this.zzx;
            zzg[] zzgArr2 = zzf.zzx;
            int i3 = this.mSize;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!zzgArr[i4].equals(zzgArr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.mSize; i2++) {
            i = (((i * 31) + this.zzw[i2]) * 31) + this.zzx[i2].hashCode();
        }
        return i;
    }

    /* access modifiers changed from: 0000 */
    public final void zza(int i, zzg zzg) {
        int zzh = zzh(i);
        if (zzh >= 0) {
            this.zzx[zzh] = zzg;
            return;
        }
        int i2 = zzh ^ -1;
        if (i2 >= this.mSize || this.zzx[i2] != zzu) {
            if (this.mSize >= this.zzw.length) {
                int idealIntArraySize = idealIntArraySize(this.mSize + 1);
                int[] iArr = new int[idealIntArraySize];
                zzg[] zzgArr = new zzg[idealIntArraySize];
                System.arraycopy(this.zzw, 0, iArr, 0, this.zzw.length);
                System.arraycopy(this.zzx, 0, zzgArr, 0, this.zzx.length);
                this.zzw = iArr;
                this.zzx = zzgArr;
            }
            if (this.mSize - i2 != 0) {
                int i3 = i2 + 1;
                System.arraycopy(this.zzw, i2, this.zzw, i3, this.mSize - i2);
                System.arraycopy(this.zzx, i2, this.zzx, i3, this.mSize - i2);
            }
            this.zzw[i2] = i;
            this.zzx[i2] = zzg;
            this.mSize++;
            return;
        }
        this.zzw[i2] = i;
        this.zzx[i2] = zzg;
    }

    /* access modifiers changed from: 0000 */
    public final zzg zzg(int i) {
        int zzh = zzh(i);
        if (zzh < 0 || this.zzx[zzh] == zzu) {
            return null;
        }
        return this.zzx[zzh];
    }
}
