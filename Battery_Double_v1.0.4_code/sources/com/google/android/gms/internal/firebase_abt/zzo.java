package com.google.android.gms.internal.firebase_abt;

import java.io.IOException;

public final class zzo extends zzd<zzo> {
    public String zzaq;
    public String zzar;
    public long zzas;
    public String zzat;
    public long zzau;
    public long zzav;
    private String zzaw;
    private String zzax;
    private String zzay;
    private String zzaz;
    private String zzba;
    public zzn[] zzbb;
    public int zzc;

    public zzo() {
        this.zzaq = "";
        this.zzar = "";
        this.zzas = 0;
        this.zzat = "";
        this.zzau = 0;
        this.zzav = 0;
        this.zzaw = "";
        this.zzax = "";
        this.zzay = "";
        this.zzaz = "";
        this.zzba = "";
        this.zzc = 0;
        this.zzbb = zzn.zzo();
        this.zzs = null;
        this.zzab = -1;
    }

    public final /* synthetic */ zzj zza(zza zza) throws IOException {
        while (true) {
            int zzd = zza.zzd();
            switch (zzd) {
                case 0:
                    return this;
                case 10:
                    this.zzaq = zza.readString();
                    break;
                case 18:
                    this.zzar = zza.readString();
                    break;
                case 24:
                    this.zzas = zza.zze();
                    break;
                case 34:
                    this.zzat = zza.readString();
                    break;
                case 40:
                    this.zzau = zza.zze();
                    break;
                case 48:
                    this.zzav = zza.zze();
                    break;
                case 58:
                    this.zzaw = zza.readString();
                    break;
                case 66:
                    this.zzax = zza.readString();
                    break;
                case 74:
                    this.zzay = zza.readString();
                    break;
                case 82:
                    this.zzaz = zza.readString();
                    break;
                case 90:
                    this.zzba = zza.readString();
                    break;
                case 96:
                    this.zzc = zza.zzf();
                    break;
                case 106:
                    int zzb = zzm.zzb(zza, 106);
                    int length = this.zzbb == null ? 0 : this.zzbb.length;
                    zzn[] zznArr = new zzn[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzbb, 0, zznArr, 0, length);
                    }
                    while (length < zznArr.length - 1) {
                        zznArr[length] = new zzn();
                        zza.zza((zzj) zznArr[length]);
                        zza.zzd();
                        length++;
                    }
                    zznArr[length] = new zzn();
                    zza.zza((zzj) zznArr[length]);
                    this.zzbb = zznArr;
                    break;
                default:
                    if (super.zza(zza, zzd)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
