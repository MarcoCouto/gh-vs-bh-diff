package com.google.android.gms.internal.firebase_abt;

import java.io.IOException;

public final class zzn extends zzd<zzn> {
    private static volatile zzn[] zzap;
    public String zzaq;

    public zzn() {
        this.zzaq = "";
        this.zzs = null;
        this.zzab = -1;
    }

    public static zzn[] zzo() {
        if (zzap == null) {
            synchronized (zzh.zzaa) {
                if (zzap == null) {
                    zzap = new zzn[0];
                }
            }
        }
        return zzap;
    }

    public final /* synthetic */ zzj zza(zza zza) throws IOException {
        while (true) {
            int zzd = zza.zzd();
            if (zzd == 0) {
                return this;
            }
            if (zzd == 10) {
                this.zzaq = zza.readString();
            } else if (!super.zza(zza, zzd)) {
                return this;
            }
        }
    }
}
