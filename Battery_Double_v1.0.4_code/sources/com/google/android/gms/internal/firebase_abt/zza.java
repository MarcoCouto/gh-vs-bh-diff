package com.google.android.gms.internal.firebase_abt;

import java.io.IOException;

public final class zza {
    private final byte[] buffer;
    private final int zzh;
    private final int zzi;
    private int zzj;
    private int zzk;
    private int zzl;
    private int zzm;
    private int zzn = Integer.MAX_VALUE;
    private int zzo;
    private int zzp = 64;
    private int zzq = 67108864;

    private zza(byte[] bArr, int i, int i2) {
        this.buffer = bArr;
        this.zzh = i;
        int i3 = i2 + i;
        this.zzj = i3;
        this.zzi = i3;
        this.zzl = i;
    }

    public static zza zza(byte[] bArr, int i, int i2) {
        return new zza(bArr, 0, i2);
    }

    private final void zzc(int i) throws IOException {
        if (i < 0) {
            throw zzi.zzm();
        } else if (this.zzl + i > this.zzn) {
            zzc(this.zzn - this.zzl);
            throw zzi.zzl();
        } else if (i <= this.zzj - this.zzl) {
            this.zzl += i;
        } else {
            throw zzi.zzl();
        }
    }

    private final void zzh() {
        this.zzj += this.zzk;
        int i = this.zzj;
        if (i > this.zzn) {
            this.zzk = i - this.zzn;
            this.zzj -= this.zzk;
            return;
        }
        this.zzk = 0;
    }

    private final byte zzi() throws IOException {
        if (this.zzl == this.zzj) {
            throw zzi.zzl();
        }
        byte[] bArr = this.buffer;
        int i = this.zzl;
        this.zzl = i + 1;
        return bArr[i];
    }

    public final int getPosition() {
        return this.zzl - this.zzh;
    }

    public final String readString() throws IOException {
        int zzg = zzg();
        if (zzg < 0) {
            throw zzi.zzm();
        } else if (zzg > this.zzj - this.zzl) {
            throw zzi.zzl();
        } else {
            String str = new String(this.buffer, this.zzl, zzg, zzh.UTF_8);
            this.zzl += zzg;
            return str;
        }
    }

    public final void zza(int i) throws zzi {
        if (this.zzm != i) {
            throw new zzi("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final void zza(zzj zzj2) throws IOException {
        int zzg = zzg();
        if (this.zzo >= this.zzp) {
            throw new zzi("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
        } else if (zzg < 0) {
            throw zzi.zzm();
        } else {
            int i = zzg + this.zzl;
            int i2 = this.zzn;
            if (i > i2) {
                throw zzi.zzl();
            }
            this.zzn = i;
            zzh();
            this.zzo++;
            zzj2.zza(this);
            zza(0);
            this.zzo--;
            this.zzn = i2;
            zzh();
        }
    }

    public final byte[] zza(int i, int i2) {
        if (i2 == 0) {
            return zzm.zzao;
        }
        byte[] bArr = new byte[i2];
        System.arraycopy(this.buffer, this.zzh + i, bArr, 0, i2);
        return bArr;
    }

    /* access modifiers changed from: 0000 */
    public final void zzb(int i, int i2) {
        if (i > this.zzl - this.zzh) {
            int i3 = this.zzl - this.zzh;
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i);
            sb.append(" is beyond current ");
            sb.append(i3);
            throw new IllegalArgumentException(sb.toString());
        } else if (i < 0) {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i);
            throw new IllegalArgumentException(sb2.toString());
        } else {
            this.zzl = this.zzh + i;
            this.zzm = 106;
        }
    }

    public final boolean zzb(int i) throws IOException {
        int zzd;
        switch (i & 7) {
            case 0:
                zzg();
                return true;
            case 1:
                zzi();
                zzi();
                zzi();
                zzi();
                zzi();
                zzi();
                zzi();
                zzi();
                return true;
            case 2:
                zzc(zzg());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                zzi();
                zzi();
                zzi();
                zzi();
                return true;
            default:
                throw new zzi("Protocol message tag had invalid wire type.");
        }
        do {
            zzd = zzd();
            if (zzd != 0) {
            }
            zza(((i >>> 3) << 3) | 4);
            return true;
        } while (zzb(zzd));
        zza(((i >>> 3) << 3) | 4);
        return true;
    }

    public final int zzd() throws IOException {
        if (this.zzl == this.zzj) {
            this.zzm = 0;
            return 0;
        }
        this.zzm = zzg();
        if (this.zzm != 0) {
            return this.zzm;
        }
        throw new zzi("Protocol message contained an invalid tag (zero).");
    }

    public final long zze() throws IOException {
        int i = 0;
        long j = 0;
        while (i < 64) {
            byte zzi2 = zzi();
            long j2 = j | (((long) (zzi2 & Byte.MAX_VALUE)) << i);
            if ((zzi2 & 128) == 0) {
                return j2;
            }
            i += 7;
            j = j2;
        }
        throw zzi.zzn();
    }

    public final int zzf() throws IOException {
        return zzg();
    }

    public final int zzg() throws IOException {
        int i;
        byte zzi2 = zzi();
        if (zzi2 >= 0) {
            return zzi2;
        }
        byte b = zzi2 & Byte.MAX_VALUE;
        byte zzi3 = zzi();
        if (zzi3 >= 0) {
            i = zzi3 << 7;
        } else {
            b |= (zzi3 & Byte.MAX_VALUE) << 7;
            byte zzi4 = zzi();
            if (zzi4 >= 0) {
                i = zzi4 << 14;
            } else {
                b |= (zzi4 & Byte.MAX_VALUE) << 14;
                byte zzi5 = zzi();
                if (zzi5 >= 0) {
                    i = zzi5 << 21;
                } else {
                    byte b2 = b | ((zzi5 & Byte.MAX_VALUE) << 21);
                    byte zzi6 = zzi();
                    byte b3 = b2 | (zzi6 << 28);
                    if (zzi6 >= 0) {
                        return b3;
                    }
                    for (int i2 = 0; i2 < 5; i2++) {
                        if (zzi() >= 0) {
                            return b3;
                        }
                    }
                    throw zzi.zzn();
                }
            }
        }
        return b | i;
    }
}
