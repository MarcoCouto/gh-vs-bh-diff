package com.google.android.gms.internal.ads;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzbc;
import com.google.android.gms.common.util.PlatformVersion;
import javax.annotation.ParametersAreNonnullByDefault;

@zzadh
@ParametersAreNonnullByDefault
public final class zzabl {
    /* JADX WARNING: type inference failed for: r7v0, types: [java.lang.Object, com.google.android.gms.internal.ads.zzalc] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.google.android.gms.internal.ads.zzabo] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.google.android.gms.internal.ads.zzabt] */
    /* JADX WARNING: type inference failed for: r7v3, types: [com.google.android.gms.internal.ads.zzabn] */
    /* JADX WARNING: type inference failed for: r7v4, types: [com.google.android.gms.internal.ads.zzabq] */
    /* JADX WARNING: type inference failed for: r0v13, types: [com.google.android.gms.internal.ads.zzabr] */
    /* JADX WARNING: type inference failed for: r7v6 */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.google.android.gms.internal.ads.zzabt] */
    /* JADX WARNING: type inference failed for: r7v7 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r0v15, types: [com.google.android.gms.internal.ads.zzabr] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r7v6
  assigns: [com.google.android.gms.internal.ads.zzabt, com.google.android.gms.internal.ads.zzabr]
  uses: [java.lang.Object, com.google.android.gms.internal.ads.zzalc, com.google.android.gms.internal.ads.zzabt, com.google.android.gms.internal.ads.zzabr]
  mth insns count: 60
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    public static zzalc zza(Context context, zza zza, zzaji zzaji, zzci zzci, @Nullable zzaqw zzaqw, zzxn zzxn, zzabm zzabm, zznx zznx) {
        ? r7;
        zzaej zzaej = zzaji.zzcos;
        if (zzaej.zzceq) {
            ? zzabr = new zzabr(context, zzaji, zzxn, zzabm, zznx, zzaqw);
            r7 = zzabr;
        } else if (!zzaej.zzare && !(zza instanceof zzbc)) {
            r7 = (!((Boolean) zzkb.zzik().zzd(zznk.zzaxd)).booleanValue() || !PlatformVersion.isAtLeastKitKat() || PlatformVersion.isAtLeastLollipop() || zzaqw == null || !zzaqw.zzud().zzvs()) ? new zzabn(context, zzaji, zzaqw, zzabm) : new zzabq(context, zzaji, zzaqw, zzabm);
        } else if (!zzaej.zzare || !(zza instanceof zzbc)) {
            r7 = new zzabo(zzaji, zzabm);
        } else {
            ? zzabt = new zzabt(context, (zzbc) zza, zzaji, zzci, zzabm, zznx);
            r7 = zzabt;
        }
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(r7.getClass().getName());
        zzakb.zzck(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        r7.zznt();
        return r7;
    }
}
