package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;

public abstract class zzzk extends zzek implements zzzj {
    public zzzk() {
        super("com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
    }

    public static zzzj zzt(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
        return queryLocalInterface instanceof zzzj ? (zzzj) queryLocalInterface : new zzzl(iBinder);
    }

    /* JADX WARNING: type inference failed for: r12v1 */
    /* JADX WARNING: type inference failed for: r12v2, types: [com.google.android.gms.internal.ads.zzzm] */
    /* JADX WARNING: type inference failed for: r12v5, types: [com.google.android.gms.internal.ads.zzzn] */
    /* JADX WARNING: type inference failed for: r12v6, types: [com.google.android.gms.internal.ads.zzzm] */
    /* JADX WARNING: type inference failed for: r12v9, types: [com.google.android.gms.internal.ads.zzzg] */
    /* JADX WARNING: type inference failed for: r12v10, types: [com.google.android.gms.internal.ads.zzzf] */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.google.android.gms.internal.ads.zzzf] */
    /* JADX WARNING: type inference failed for: r12v14, types: [com.google.android.gms.internal.ads.zzzi] */
    /* JADX WARNING: type inference failed for: r12v15, types: [com.google.android.gms.internal.ads.zzzh] */
    /* JADX WARNING: type inference failed for: r12v16 */
    /* JADX WARNING: type inference failed for: r5v1, types: [com.google.android.gms.internal.ads.zzzh] */
    /* JADX WARNING: type inference failed for: r12v17 */
    /* JADX WARNING: type inference failed for: r12v18 */
    /* JADX WARNING: type inference failed for: r12v19 */
    /* JADX WARNING: type inference failed for: r12v20 */
    /* JADX WARNING: type inference failed for: r12v21 */
    /* JADX WARNING: type inference failed for: r12v22 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00af, code lost:
        r11.writeNoException();
        com.google.android.gms.internal.ads.zzel.zzb(r11, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e9, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        r11.writeNoException();
     */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v1
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.zzzg, com.google.android.gms.internal.ads.zzzn, com.google.android.gms.internal.ads.zzzm, com.google.android.gms.internal.ads.zzzf, com.google.android.gms.internal.ads.zzzi, com.google.android.gms.internal.ads.zzzh]
  uses: [com.google.android.gms.internal.ads.zzzm, ?[OBJECT, ARRAY]]
  mth insns count: 80
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 9 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        zzzt zzzt;
        ? r12 = 0;
        switch (i) {
            case 1:
                IObjectWrapper asInterface = Stub.asInterface(parcel.readStrongBinder());
                String readString = parcel.readString();
                Bundle bundle = (Bundle) zzel.zza(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.ISignalsCallback");
                    r12 = queryLocalInterface instanceof zzzm ? (zzzm) queryLocalInterface : new zzzn(readStrongBinder);
                }
                zza(asInterface, readString, bundle, r12);
                break;
            case 2:
                zzzt = zznc();
                break;
            case 3:
                zzzt = zznd();
                break;
            case 4:
                byte[] createByteArray = parcel.createByteArray();
                String readString2 = parcel.readString();
                Bundle bundle2 = (Bundle) zzel.zza(parcel, Bundle.CREATOR);
                IObjectWrapper asInterface2 = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IBannerCallback");
                    r12 = queryLocalInterface2 instanceof zzzf ? (zzzf) queryLocalInterface2 : new zzzg(readStrongBinder2);
                }
                zza(createByteArray, readString2, bundle2, asInterface2, r12, zzxu.zzs(parcel.readStrongBinder()), (zzjn) zzel.zza(parcel, zzjn.CREATOR));
                break;
            case 5:
                zzlo videoController = getVideoController();
                parcel2.writeNoException();
                zzel.zza(parcel2, (IInterface) videoController);
                break;
            case 6:
                byte[] createByteArray2 = parcel.createByteArray();
                String readString3 = parcel.readString();
                Bundle bundle3 = (Bundle) zzel.zza(parcel, Bundle.CREATOR);
                IObjectWrapper asInterface3 = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IInterstitialCallback");
                    r12 = queryLocalInterface3 instanceof zzzh ? (zzzh) queryLocalInterface3 : new zzzi(readStrongBinder3);
                }
                zza(createByteArray2, readString3, bundle3, asInterface3, r12, zzxu.zzs(parcel.readStrongBinder()));
                break;
            case 7:
                showInterstitial();
                break;
            default:
                return false;
        }
    }
}
