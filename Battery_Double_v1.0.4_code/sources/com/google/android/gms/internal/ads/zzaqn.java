package com.google.android.gms.internal.ads;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzadh
public final class zzaqn extends zzaqh {
    private static final Set<String> zzdbg = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzdbh = new DecimalFormat("#,###");
    private File zzdbi;
    private boolean zzdbj;

    public zzaqn(zzapw zzapw) {
        super(zzapw);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzakb.zzdk("Context.getCacheDir() returned null");
            return;
        }
        this.zzdbi = new File(cacheDir, "admobVideoStreams");
        if (!this.zzdbi.isDirectory() && !this.zzdbi.mkdirs()) {
            String str = "Could not create preload cache directory at ";
            String valueOf = String.valueOf(this.zzdbi.getAbsolutePath());
            zzakb.zzdk(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.zzdbi = null;
        } else if (!this.zzdbi.setReadable(true, false) || !this.zzdbi.setExecutable(true, false)) {
            String str2 = "Could not set cache file permissions at ";
            String valueOf2 = String.valueOf(this.zzdbi.getAbsolutePath());
            zzakb.zzdk(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            this.zzdbi = null;
        }
    }

    private final File zzc(File file) {
        return new File(this.zzdbi, String.valueOf(file.getName()).concat(".done"));
    }

    public final void abort() {
        this.zzdbj = true;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:223|224|225|(4:226|227|228|229)) */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01ed, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01f3, code lost:
        com.google.android.gms.internal.ads.zzakb.zzck(r3);
        r5.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01f9, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01fe, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) == false) goto L_0x025e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
        r1 = r5.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0209, code lost:
        if (r1 < 400) goto L_0x025e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x020b, code lost:
        r2 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x020d, code lost:
        r3 = "HTTP request failed. Code: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
        r4 = java.lang.String.valueOf(java.lang.Integer.toString(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x021b, code lost:
        if (r4.length() == 0) goto L_0x0222;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x021d, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0227, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
        r6 = new java.lang.StringBuilder(32 + java.lang.String.valueOf(r34).length());
        r6.append("HTTP status code ");
        r6.append(r1);
        r6.append(" at ");
        r6.append(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0251, code lost:
        throw new java.io.IOException(r6.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0252, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0253, code lost:
        r1 = r0;
        r4 = r3;
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0257, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0258, code lost:
        r1 = r0;
        r3 = r2;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x025b, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        r7 = r5.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0262, code lost:
        if (r7 >= 0) goto L_0x028d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0264, code lost:
        r1 = "Stream cache aborted, missing content-length header at ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        r2 = java.lang.String.valueOf(r34);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x026e, code lost:
        if (r2.length() == 0) goto L_0x0275;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0270, code lost:
        r1 = r1.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0275, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x027b, code lost:
        com.google.android.gms.internal.ads.zzakb.zzdk(r1);
        zza(r9, r12.getAbsolutePath(), "contentLengthMissing", null);
        zzdbg.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x028c, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        r1 = zzdbh.format((long) r7);
        r3 = ((java.lang.Integer) com.google.android.gms.internal.ads.zzkb.zzik().zzd(com.google.android.gms.internal.ads.zznk.zzauy)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x02a4, code lost:
        if (r7 <= r3) goto L_0x02fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        r3 = new java.lang.StringBuilder((33 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r34).length());
        r3.append("Content length ");
        r3.append(r1);
        r3.append(" exceeds limit at ");
        r3.append(r9);
        com.google.android.gms.internal.ads.zzakb.zzdk(r3.toString());
        r2 = "File too big for full file cache. Size: ";
        r1 = java.lang.String.valueOf(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x02e0, code lost:
        if (r1.length() == 0) goto L_0x02e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02e2, code lost:
        r1 = r2.concat(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x02e7, code lost:
        r1 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x02ec, code lost:
        zza(r9, r12.getAbsolutePath(), "sizeExceeded", r1);
        zzdbg.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x02fa, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        r2 = new java.lang.StringBuilder((20 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r34).length());
        r2.append("Caching ");
        r2.append(r1);
        r2.append(" bytes from ");
        r2.append(r9);
        com.google.android.gms.internal.ads.zzakb.zzck(r2.toString());
        r5 = java.nio.channels.Channels.newChannel(r5.getInputStream());
        r4 = new java.io.FileOutputStream(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:?, code lost:
        r2 = r4.getChannel();
        r1 = java.nio.ByteBuffer.allocate(1048576);
        r10 = com.google.android.gms.ads.internal.zzbv.zzer();
        r17 = r10.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0354, code lost:
        r20 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:?, code lost:
        r11 = new com.google.android.gms.internal.ads.zzamj(((java.lang.Long) com.google.android.gms.internal.ads.zzkb.zzik().zzd(com.google.android.gms.internal.ads.zznk.zzavb)).longValue());
        r14 = ((java.lang.Long) com.google.android.gms.internal.ads.zzkb.zzik().zzd(com.google.android.gms.internal.ads.zznk.zzava)).longValue();
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0370, code lost:
        r21 = r5.read(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0374, code lost:
        if (r21 < 0) goto L_0x0480;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0376, code lost:
        r6 = r6 + r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0378, code lost:
        if (r6 <= r3) goto L_0x03be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x037a, code lost:
        r1 = "sizeExceeded";
        r2 = "File too big for full file cache. Size: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x038a, code lost:
        if (r3.length() == 0) goto L_0x0392;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x038c, code lost:
        r10 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0397, code lost:
        r10 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x039f, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03a0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03a1, code lost:
        r3 = r1;
        r2 = r20;
        r1 = r0;
        r32 = r10;
        r10 = r4;
        r4 = r32;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03ac, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03ad, code lost:
        r3 = r1;
        r10 = r4;
        r2 = r20;
        r4 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03b8, code lost:
        r3 = r16;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:?, code lost:
        r1.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03c5, code lost:
        if (r2.write(r1) > 0) goto L_0x0476;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x03c7, code lost:
        r1.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x03d6, code lost:
        if ((r10.currentTimeMillis() - r17) <= (1000 * r14)) goto L_0x0407;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03d8, code lost:
        r1 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        r2 = java.lang.Long.toString(r14);
        r5 = new java.lang.StringBuilder(29 + java.lang.String.valueOf(r2).length());
        r5.append("Timeout exceeded. Limit: ");
        r5.append(r2);
        r5.append(" sec");
        r10 = r5.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0406, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0407, code lost:
        r26 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x040b, code lost:
        if (r8.zzdbj == false) goto L_0x0417;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x040d, code lost:
        r1 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0416, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x041b, code lost:
        if (r11.tryAcquire() == false) goto L_0x044d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x041d, code lost:
        r27 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0427, code lost:
        r28 = r11;
        r23 = r26;
        r11 = com.google.android.gms.internal.ads.zzamu.zzsy;
        r1 = r1;
        r24 = r2;
        r25 = r3;
        r29 = r14;
        r14 = r4;
        r19 = r6;
        r31 = r5;
        r21 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:?, code lost:
        r1 = new com.google.android.gms.internal.ads.zzaqi(r8, r9, r12.getAbsolutePath(), r6, r7, false);
        r11.post(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x044d, code lost:
        r24 = r2;
        r25 = r3;
        r31 = r5;
        r19 = r6;
        r21 = r7;
        r27 = r10;
        r28 = r11;
        r29 = r14;
        r23 = r26;
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x0461, code lost:
        r4 = r14;
        r6 = r19;
        r7 = r21;
        r1 = r23;
        r2 = r24;
        r3 = r25;
        r10 = r27;
        r11 = r28;
        r14 = r29;
        r5 = r31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x0476, code lost:
        r29 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x047a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x047b, code lost:
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x047c, code lost:
        r1 = r0;
        r10 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0480, code lost:
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:?, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0489, code lost:
        if (com.google.android.gms.internal.ads.zzakb.isLoggable(3) == false) goto L_0x04c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:?, code lost:
        r1 = zzdbh.format((long) r6);
        r3 = new java.lang.StringBuilder((22 + java.lang.String.valueOf(r1).length()) + java.lang.String.valueOf(r34).length());
        r3.append("Preloaded ");
        r3.append(r1);
        r3.append(" bytes from ");
        r3.append(r9);
        com.google.android.gms.internal.ads.zzakb.zzck(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x04c3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:?, code lost:
        r12.setReadable(true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x04ce, code lost:
        if (r13.isFile() == false) goto L_0x04d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:?, code lost:
        r13.setLastModified(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:?, code lost:
        r13.createNewFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:?, code lost:
        zza(r9, r12.getAbsolutePath(), r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x04e4, code lost:
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:?, code lost:
        zzdbg.remove(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04ea, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04eb, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x04ed, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x04ef, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x04f0, code lost:
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x04f1, code lost:
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x04f4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x04f5, code lost:
        r14 = r4;
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x04f7, code lost:
        r1 = r0;
        r10 = r14;
        r3 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x04fb, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x04fd, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0505, code lost:
        throw new java.io.IOException("Too many redirects (20)");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x0506, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x0508, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x0509, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x050a, code lost:
        r1 = r0;
        r3 = r16;
        r4 = null;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0511, code lost:
        if ((r1 instanceof java.lang.RuntimeException) != false) goto L_0x0513;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0513, code lost:
        com.google.android.gms.ads.internal.zzbv.zzeo().zza(r1, "VideoStreamFullFileCache.preload");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0521, code lost:
        if (r8.zzdbj == false) goto L_0x0548;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0523, code lost:
        r5 = new java.lang.StringBuilder(26 + java.lang.String.valueOf(r34).length());
        r5.append("Preload aborted for URL \"");
        r5.append(r9);
        r5.append("\"");
        com.google.android.gms.internal.ads.zzakb.zzdj(r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x0548, code lost:
        r6 = new java.lang.StringBuilder(25 + java.lang.String.valueOf(r34).length());
        r6.append("Preload failed for URL \"");
        r6.append(r9);
        r6.append("\"");
        com.google.android.gms.internal.ads.zzakb.zzc(r6.toString(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x0578, code lost:
        r1 = "Could not delete partial cache file at ";
        r5 = java.lang.String.valueOf(r12.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x0586, code lost:
        if (r5.length() == 0) goto L_0x058d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x0588, code lost:
        r1 = r1.concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x058d, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x0593, code lost:
        com.google.android.gms.internal.ads.zzakb.zzdk(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x0596, code lost:
        zza(r9, r12.getAbsolutePath(), r3, r4);
        zzdbg.remove(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x05a3, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0134, code lost:
        r16 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        com.google.android.gms.ads.internal.zzbv.zzew();
        r1 = ((java.lang.Integer) com.google.android.gms.internal.ads.zzkb.zzik().zzd(com.google.android.gms.internal.ads.zznk.zzavc)).intValue();
        r3 = new java.net.URL(r9);
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0150, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0153, code lost:
        if (r2 > 20) goto L_0x04fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0155, code lost:
        r5 = r3.openConnection();
        r5.setConnectTimeout(r1);
        r5.setReadTimeout(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0161, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) != false) goto L_0x0173;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x016a, code lost:
        throw new java.io.IOException("Invalid protocol.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x016c, code lost:
        r1 = r0;
        r4 = null;
        r2 = r15;
        r3 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        r5 = (java.net.HttpURLConnection) r5;
        r6 = new com.google.android.gms.internal.ads.zzamy();
        r6.zza(r5, (byte[]) null);
        r5.setInstanceFollowRedirects(false);
        r7 = r5.getResponseCode();
        r6.zza(r5, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x018a, code lost:
        if ((r7 / 100) != 3) goto L_0x01fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        r4 = r5.getHeaderField("Location");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0192, code lost:
        if (r4 != null) goto L_0x019c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x019b, code lost:
        throw new java.io.IOException("Missing Location header in redirect");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x019c, code lost:
        r6 = new java.net.URL(r3, r4);
        r3 = r6.getProtocol();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01a5, code lost:
        if (r3 != null) goto L_0x01af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01ae, code lost:
        throw new java.io.IOException("Protocol is null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01b5, code lost:
        if (r3.equals("http") != false) goto L_0x01dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01bd, code lost:
        if (r3.equals("https") != false) goto L_0x01dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01bf, code lost:
        r2 = "Unsupported scheme: ";
        r3 = java.lang.String.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01cb, code lost:
        if (r3.length() == 0) goto L_0x01d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01cd, code lost:
        r2 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01d2, code lost:
        r2 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01db, code lost:
        throw new java.io.IOException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01dc, code lost:
        r3 = "Redirecting to ";
        r4 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01e6, code lost:
        if (r4.length() == 0) goto L_0x01ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01e8, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:223:0x04db */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x0523  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0548  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0588  */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x058d  */
    public final boolean zzdp(String str) {
        int i;
        String str2;
        String str3;
        boolean z;
        String str4;
        File[] listFiles;
        String str5 = str;
        FileOutputStream fileOutputStream = null;
        if (this.zzdbi == null) {
            str4 = "noCacheDir";
        } else {
            do {
                if (this.zzdbi == null) {
                    i = 0;
                } else {
                    i = 0;
                    for (File name : this.zzdbi.listFiles()) {
                        if (!name.getName().endsWith(".done")) {
                            i++;
                        }
                    }
                }
                if (i > ((Integer) zzkb.zzik().zzd(zznk.zzaux)).intValue()) {
                    if (this.zzdbi != null) {
                        long j = Long.MAX_VALUE;
                        File file = null;
                        for (File file2 : this.zzdbi.listFiles()) {
                            if (!file2.getName().endsWith(".done")) {
                                long lastModified = file2.lastModified();
                                if (lastModified < j) {
                                    file = file2;
                                    j = lastModified;
                                }
                            }
                        }
                        if (file != null) {
                            z = file.delete();
                            File zzc = zzc(file);
                            if (zzc.isFile()) {
                                z &= zzc.delete();
                                continue;
                            } else {
                                continue;
                            }
                        }
                    }
                    z = false;
                    continue;
                } else {
                    zzkb.zzif();
                    File file3 = new File(this.zzdbi, zzamu.zzde(str));
                    File zzc2 = zzc(file3);
                    if (!file3.isFile() || !zzc2.isFile()) {
                        String valueOf = String.valueOf(this.zzdbi.getAbsolutePath());
                        String valueOf2 = String.valueOf(str);
                        Object concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                        synchronized (zzdbg) {
                            try {
                                if (zzdbg.contains(concat)) {
                                    String str6 = "Stream cache already in progress at ";
                                    String valueOf3 = String.valueOf(str);
                                    if (valueOf3.length() != 0) {
                                        str3 = str6.concat(valueOf3);
                                    } else {
                                        th = new String(str6);
                                    }
                                    zzakb.zzdk(str3);
                                    zza(str5, file3.getAbsolutePath(), "inProgress", null);
                                    return false;
                                }
                                zzdbg.add(concat);
                            } finally {
                                while (true) {
                                    str2 = th;
                                }
                            }
                        }
                    } else {
                        int length = (int) file3.length();
                        String str7 = "Stream cache hit at ";
                        String valueOf4 = String.valueOf(str);
                        zzakb.zzck(valueOf4.length() != 0 ? str7.concat(valueOf4) : new String(str7));
                        zza(str5, file3.getAbsolutePath(), length);
                        return true;
                    }
                }
            } while (z);
            zzakb.zzdk("Unable to expire stream cache");
            str4 = "expireFailed";
        }
        zza(str5, null, str4, null);
        return false;
    }
}
