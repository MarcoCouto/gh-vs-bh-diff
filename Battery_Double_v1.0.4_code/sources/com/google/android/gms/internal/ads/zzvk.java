package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.zzv;
import java.util.Map;

final class zzvk implements zzv<zzwb> {
    private final /* synthetic */ zzvw zzbqi;
    private final /* synthetic */ zzuu zzbqj;
    private final /* synthetic */ zzvf zzbqk;

    zzvk(zzvf zzvf, zzvw zzvw, zzuu zzuu) {
        this.zzbqk = zzvf;
        this.zzbqi = zzvw;
        this.zzbqj = zzuu;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        return;
     */
    public final /* synthetic */ void zza(Object obj, Map map) {
        synchronized (this.zzbqk.mLock) {
            if (this.zzbqi.getStatus() != -1) {
                if (this.zzbqi.getStatus() != 1) {
                    this.zzbqk.zzbqb = 0;
                    this.zzbqk.zzbpy.zze(this.zzbqj);
                    this.zzbqi.zzk(this.zzbqj);
                    this.zzbqk.zzbqa = this.zzbqi;
                    zzakb.v("Successfully loaded JS Engine.");
                }
            }
        }
    }
}
