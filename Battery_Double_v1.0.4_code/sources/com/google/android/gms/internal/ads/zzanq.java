package com.google.android.gms.internal.ads;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

final /* synthetic */ class zzanq implements Runnable {
    private final zzaoj zzbnu;
    private final zzank zzcvl;
    private final zzanz zzcvm;

    zzanq(zzaoj zzaoj, zzank zzank, zzanz zzanz) {
        this.zzbnu = zzaoj;
        this.zzcvl = zzank;
        this.zzcvm = zzanz;
    }

    /* JADX WARNING: type inference failed for: r2v1, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r1v3, types: [java.lang.Throwable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void run() {
        zzaoj zzaoj = this.zzbnu;
        try {
            zzaoj.set(this.zzcvl.apply(this.zzcvm.get()));
        } catch (CancellationException unused) {
            zzaoj.cancel(true);
        } catch (ExecutionException e) {
            e = e;
            ? cause = e.getCause();
            if (cause != 0) {
                e = cause;
            }
            zzaoj.setException(e);
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            zzaoj.setException(e2);
        } catch (Exception e3) {
            zzaoj.setException(e3);
        }
    }
}
