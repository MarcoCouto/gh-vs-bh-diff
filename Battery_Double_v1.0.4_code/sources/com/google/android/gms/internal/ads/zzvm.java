package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

final class zzvm implements Runnable {
    private final /* synthetic */ zzvw zzbqi;
    private final /* synthetic */ zzuu zzbqj;
    private final /* synthetic */ zzvf zzbqk;

    zzvm(zzvf zzvf, zzvw zzvw, zzuu zzuu) {
        this.zzbqk = zzvf;
        this.zzbqi = zzvw;
        this.zzbqj = zzuu;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        return;
     */
    public final void run() {
        synchronized (this.zzbqk.mLock) {
            if (this.zzbqi.getStatus() != -1) {
                if (this.zzbqi.getStatus() != 1) {
                    this.zzbqi.reject();
                    Executor executor = zzaoe.zzcvy;
                    zzuu zzuu = this.zzbqj;
                    zzuu.getClass();
                    executor.execute(zzvn.zza(zzuu));
                    zzakb.v("Could not receive loaded message in a timely manner. Rejecting.");
                }
            }
        }
    }
}
