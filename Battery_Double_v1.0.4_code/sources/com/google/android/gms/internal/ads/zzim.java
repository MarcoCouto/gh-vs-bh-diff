package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzim extends zzbfc<zzim> {
    private Integer zzamf;
    private Integer zzanx;

    public zzim() {
        this.zzamf = null;
        this.zzanx = null;
        this.zzebk = null;
        this.zzebt = -1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0073, code lost:
        throw new java.lang.IllegalArgumentException(r5.toString());
     */
    /* renamed from: zzo */
    public final zzim zza(zzbez zzbez) throws IOException {
        int i;
        int zzacc;
        while (true) {
            int zzabk = zzbez.zzabk();
            if (zzabk == 0) {
                return this;
            }
            if (zzabk == 8) {
                i = zzbez.getPosition();
                int zzacc2 = zzbez.zzacc();
                if (zzacc2 < 0 || zzacc2 > 2) {
                    StringBuilder sb = new StringBuilder(43);
                    sb.append(zzacc2);
                    sb.append(" is not a valid enum NetworkType");
                } else {
                    this.zzamf = Integer.valueOf(zzacc2);
                }
            } else if (zzabk == 16) {
                i = zzbez.getPosition();
                try {
                    zzacc = zzbez.zzacc();
                    if (zzacc < 0 || zzacc > 2) {
                        if (zzacc < 4 || zzacc > 4) {
                            StringBuilder sb2 = new StringBuilder(51);
                            sb2.append(zzacc);
                            sb2.append(" is not a valid enum CellularNetworkType");
                        }
                    }
                    this.zzanx = Integer.valueOf(zzacc);
                } catch (IllegalArgumentException unused) {
                    zzbez.zzdc(i);
                    zza(zzbez, zzabk);
                }
            } else if (!super.zza(zzbez, zzabk)) {
                return this;
            }
        }
        StringBuilder sb22 = new StringBuilder(51);
        sb22.append(zzacc);
        sb22.append(" is not a valid enum CellularNetworkType");
        throw new IllegalArgumentException(sb22.toString());
    }

    public final void zza(zzbfa zzbfa) throws IOException {
        if (this.zzamf != null) {
            zzbfa.zzm(1, this.zzamf.intValue());
        }
        if (this.zzanx != null) {
            zzbfa.zzm(2, this.zzanx.intValue());
        }
        super.zza(zzbfa);
    }

    /* access modifiers changed from: protected */
    public final int zzr() {
        int zzr = super.zzr();
        if (this.zzamf != null) {
            zzr += zzbfa.zzq(1, this.zzamf.intValue());
        }
        return this.zzanx != null ? zzr + zzbfa.zzq(2, this.zzanx.intValue()) : zzr;
    }
}
