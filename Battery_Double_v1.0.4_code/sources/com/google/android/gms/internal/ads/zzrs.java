package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class zzrs extends zzek implements zzrr {
    public zzrs() {
        super("com.google.android.gms.ads.internal.formats.client.IUnifiedNativeAd");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0075, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a6, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c1, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c8, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        IInterface iInterface;
        zzro zzro;
        switch (i) {
            case 2:
                str = getHeadline();
                break;
            case 3:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                break;
            case 4:
                str = getBody();
                break;
            case 5:
                iInterface = zzjz();
                break;
            case 6:
                str = getCallToAction();
                break;
            case 7:
                str = getAdvertiser();
                break;
            case 8:
                double starRating = getStarRating();
                parcel2.writeNoException();
                parcel2.writeDouble(starRating);
                break;
            case 9:
                str = getStore();
                break;
            case 10:
                str = getPrice();
                break;
            case 11:
                iInterface = getVideoController();
                break;
            case 12:
                str = getMediationAdapterClassName();
                break;
            case 13:
                destroy();
                break;
            case 14:
                iInterface = zzkf();
                break;
            case 15:
                performClick((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                break;
            case 16:
                boolean recordImpression = recordImpression((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                zzel.zza(parcel2, recordImpression);
                break;
            case 17:
                reportTouchEvent((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                break;
            case 18:
                iInterface = zzka();
                break;
            case 19:
                iInterface = zzke();
                break;
            case 20:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzel.zzb(parcel2, extras);
                break;
            case 21:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    zzro = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.IUnconfirmedClickListener");
                    zzro = queryLocalInterface instanceof zzro ? (zzro) queryLocalInterface : new zzrq(readStrongBinder);
                }
                zza(zzro);
                break;
            case 22:
                cancelUnconfirmedClick();
                break;
            default:
                return false;
        }
    }
}
