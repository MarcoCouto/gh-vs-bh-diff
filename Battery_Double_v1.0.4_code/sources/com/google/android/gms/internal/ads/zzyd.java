package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;
import java.util.List;

public abstract class zzyd extends zzek implements zzyc {
    public zzyd() {
        super("com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005b, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007d, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008f, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00aa, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b1, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        IInterface iInterface;
        boolean z;
        switch (i) {
            case 2:
                str = getHeadline();
                break;
            case 3:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                break;
            case 4:
                str = getBody();
                break;
            case 5:
                iInterface = zzkg();
                break;
            case 6:
                str = getCallToAction();
                break;
            case 7:
                str = getAdvertiser();
                break;
            case 8:
                recordImpression();
                break;
            case 9:
                zzj(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 10:
                zzk(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 11:
                z = getOverrideImpressionRecording();
                break;
            case 12:
                z = getOverrideClickHandling();
                break;
            case 13:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzel.zzb(parcel2, extras);
                break;
            case 14:
                zzl(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 15:
                iInterface = zzmv();
                break;
            case 16:
                iInterface = getVideoController();
                break;
            case 19:
                iInterface = zzkf();
                break;
            case 20:
                iInterface = zzmw();
                break;
            case 21:
                iInterface = zzke();
                break;
            case 22:
                zzb(Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()), Stub.asInterface(parcel.readStrongBinder()));
                break;
            default:
                return false;
        }
    }
}
