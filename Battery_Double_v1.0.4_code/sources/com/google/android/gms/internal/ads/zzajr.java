package com.google.android.gms.internal.ads;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import com.google.android.gms.ads.AdActivity;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import javax.annotation.concurrent.GuardedBy;

@zzadh
public final class zzajr {
    private final Object mLock = new Object();
    @VisibleForTesting
    private final String zzasc;
    @VisibleForTesting
    private long zzcqd = -1;
    @VisibleForTesting
    private long zzcqe = -1;
    @GuardedBy("mLock")
    @VisibleForTesting
    private int zzcqf = -1;
    @VisibleForTesting
    int zzcqg = -1;
    @VisibleForTesting
    private long zzcqh = 0;
    @GuardedBy("mLock")
    @VisibleForTesting
    private int zzcqi = 0;
    @GuardedBy("mLock")
    @VisibleForTesting
    private int zzcqj = 0;

    public zzajr(String str) {
        this.zzasc = str;
    }

    private static boolean zzah(Context context) {
        int identifier = context.getResources().getIdentifier("Theme.Translucent", "style", "android");
        if (identifier != 0) {
            try {
                if (identifier == context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), AdActivity.CLASS_NAME), 0).theme) {
                    return true;
                }
                zzakb.zzdj("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
                return false;
            } catch (NameNotFoundException unused) {
                zzakb.zzdk("Fail to fetch AdActivity theme");
            }
        }
        zzakb.zzdj("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0090, code lost:
        return;
     */
    public final void zzb(zzjj zzjj, long j) {
        synchronized (this.mLock) {
            long zzrb = zzbv.zzeo().zzqh().zzrb();
            long currentTimeMillis = zzbv.zzer().currentTimeMillis();
            if (this.zzcqe == -1) {
                if (currentTimeMillis - zzrb > ((Long) zzkb.zzik().zzd(zznk.zzayi)).longValue()) {
                    this.zzcqg = -1;
                } else {
                    this.zzcqg = zzbv.zzeo().zzqh().zzrc();
                }
                this.zzcqe = j;
                j = this.zzcqe;
            }
            this.zzcqd = j;
            if (zzjj == null || zzjj.extras == null || zzjj.extras.getInt("gw", 2) != 1) {
                this.zzcqf++;
                this.zzcqg++;
                if (this.zzcqg == 0) {
                    this.zzcqh = 0;
                    zzbv.zzeo().zzqh().zzk(currentTimeMillis);
                } else {
                    this.zzcqh = currentTimeMillis - zzbv.zzeo().zzqh().zzrd();
                }
            }
        }
    }

    public final Bundle zzk(Context context, String str) {
        Bundle bundle;
        synchronized (this.mLock) {
            bundle = new Bundle();
            bundle.putString("session_id", this.zzasc);
            bundle.putLong("basets", this.zzcqe);
            bundle.putLong("currts", this.zzcqd);
            bundle.putString("seq_num", str);
            bundle.putInt("preqs", this.zzcqf);
            bundle.putInt("preqs_in_session", this.zzcqg);
            bundle.putLong("time_in_session", this.zzcqh);
            bundle.putInt("pclick", this.zzcqi);
            bundle.putInt("pimp", this.zzcqj);
            bundle.putBoolean("support_transparent_background", zzah(context));
        }
        return bundle;
    }

    public final void zzpm() {
        synchronized (this.mLock) {
            this.zzcqj++;
        }
    }

    public final void zzpn() {
        synchronized (this.mLock) {
            this.zzcqi++;
        }
    }
}
