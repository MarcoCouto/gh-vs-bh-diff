package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.formats.AdChoicesView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdAssetNames;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;

@zzadh
@ParametersAreNonnullByDefault
public final class zzpn extends zzqb implements OnClickListener, OnTouchListener, OnGlobalLayoutListener, OnScrollChangedListener {
    @VisibleForTesting
    private static final String[] zzbjs = {NativeAppInstallAd.ASSET_MEDIA_VIDEO, NativeContentAd.ASSET_MEDIA_VIDEO, UnifiedNativeAdAssetNames.ASSET_MEDIA_VIDEO};
    private final Object mLock = new Object();
    @Nullable
    @VisibleForTesting
    private zzoz zzbij;
    private final FrameLayout zzbjt;
    private View zzbju;
    private final boolean zzbjv;
    @VisibleForTesting
    private Map<String, WeakReference<View>> zzbjw = Collections.synchronizedMap(new HashMap());
    @Nullable
    @VisibleForTesting
    private View zzbjx;
    @VisibleForTesting
    private boolean zzbjy = false;
    @VisibleForTesting
    private Point zzbjz = new Point();
    @VisibleForTesting
    private Point zzbka = new Point();
    @VisibleForTesting
    private WeakReference<zzfp> zzbkb = new WeakReference<>(null);
    @Nullable
    @VisibleForTesting
    private FrameLayout zzvh;

    @TargetApi(21)
    public zzpn(FrameLayout frameLayout, FrameLayout frameLayout2) {
        this.zzbjt = frameLayout;
        this.zzvh = frameLayout2;
        zzbv.zzfg();
        zzaor.zza((View) this.zzbjt, (OnGlobalLayoutListener) this);
        zzbv.zzfg();
        zzaor.zza((View) this.zzbjt, (OnScrollChangedListener) this);
        this.zzbjt.setOnTouchListener(this);
        this.zzbjt.setOnClickListener(this);
        if (frameLayout2 != null && PlatformVersion.isAtLeastLollipop()) {
            frameLayout2.setElevation(Float.MAX_VALUE);
        }
        zznk.initialize(this.zzbjt.getContext());
        this.zzbjv = ((Boolean) zzkb.zzik().zzd(zznk.zzbcd)).booleanValue();
    }

    private final void zzkt() {
        synchronized (this.mLock) {
            if (!this.zzbjv && this.zzbjy) {
                int measuredWidth = this.zzbjt.getMeasuredWidth();
                int measuredHeight = this.zzbjt.getMeasuredHeight();
                if (!(measuredWidth == 0 || measuredHeight == 0 || this.zzvh == null)) {
                    this.zzvh.setLayoutParams(new LayoutParams(measuredWidth, measuredHeight));
                    this.zzbjy = false;
                }
            }
        }
    }

    private final void zzl(@Nullable View view) {
        if (this.zzbij != null) {
            zzoz zzkn = this.zzbij instanceof zzoy ? ((zzoy) this.zzbij).zzkn() : this.zzbij;
            if (zzkn != null) {
                zzkn.zzl(view);
            }
        }
    }

    @VisibleForTesting
    private final int zzv(int i) {
        zzkb.zzif();
        return zzamu.zzb(this.zzbij.getContext(), i);
    }

    public final void destroy() {
        synchronized (this.mLock) {
            if (this.zzvh != null) {
                this.zzvh.removeAllViews();
            }
            this.zzvh = null;
            this.zzbjw = null;
            this.zzbjx = null;
            this.zzbij = null;
            this.zzbjz = null;
            this.zzbka = null;
            this.zzbkb = null;
            this.zzbju = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008d, code lost:
        return;
     */
    public final void onClick(View view) {
        zzoz zzoz;
        String str;
        Map<String, WeakReference<View>> map;
        FrameLayout frameLayout;
        synchronized (this.mLock) {
            if (this.zzbij != null) {
                this.zzbij.cancelUnconfirmedClick();
                Bundle bundle = new Bundle();
                bundle.putFloat("x", (float) zzv(this.zzbjz.x));
                bundle.putFloat("y", (float) zzv(this.zzbjz.y));
                bundle.putFloat("start_x", (float) zzv(this.zzbka.x));
                bundle.putFloat("start_y", (float) zzv(this.zzbka.y));
                if (this.zzbjx == null || !this.zzbjx.equals(view)) {
                    this.zzbij.zza(view, this.zzbjw, bundle, this.zzbjt);
                } else {
                    if (!(this.zzbij instanceof zzoy)) {
                        zzoz = this.zzbij;
                        str = NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE;
                        map = this.zzbjw;
                        frameLayout = this.zzbjt;
                    } else if (((zzoy) this.zzbij).zzkn() != null) {
                        zzoz = ((zzoy) this.zzbij).zzkn();
                        str = NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE;
                        map = this.zzbjw;
                        frameLayout = this.zzbjt;
                    }
                    zzoz.zza(view, str, bundle, map, frameLayout);
                }
            }
        }
    }

    public final void onGlobalLayout() {
        synchronized (this.mLock) {
            zzkt();
            if (this.zzbij != null) {
                this.zzbij.zzc(this.zzbjt, this.zzbjw);
            }
        }
    }

    public final void onScrollChanged() {
        synchronized (this.mLock) {
            if (this.zzbij != null) {
                this.zzbij.zzc(this.zzbjt, this.zzbjw);
            }
            zzkt();
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.mLock) {
            if (this.zzbij == null) {
                return false;
            }
            int[] iArr = new int[2];
            this.zzbjt.getLocationOnScreen(iArr);
            Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
            this.zzbjz = point;
            if (motionEvent.getAction() == 0) {
                this.zzbka = point;
            }
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            obtain.setLocation((float) point.x, (float) point.y);
            this.zzbij.zzd(obtain);
            obtain.recycle();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0255, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01be A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0213 A[Catch:{ Exception -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x011a A[Catch:{ Exception -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0165 A[Catch:{ Exception -> 0x0196 }] */
    public final void zza(IObjectWrapper iObjectWrapper) {
        ViewGroup viewGroup;
        zzaqw zzaqw;
        View view;
        synchronized (this.mLock) {
            View view2 = null;
            zzl(null);
            Object unwrap = ObjectWrapper.unwrap(iObjectWrapper);
            if (!(unwrap instanceof zzpd)) {
                zzakb.zzdk("Not an instance of native engine. This is most likely a transient error");
                return;
            }
            int i = 0;
            if (!this.zzbjv && this.zzvh != null) {
                this.zzvh.setLayoutParams(new LayoutParams(0, 0));
                this.zzbjt.requestLayout();
            }
            boolean z = true;
            this.zzbjy = true;
            zzpd zzpd = (zzpd) unwrap;
            if (this.zzbij != null) {
                if (((Boolean) zzkb.zzik().zzd(zznk.zzbbu)).booleanValue()) {
                    this.zzbij.zzb(this.zzbjt, this.zzbjw);
                }
            }
            if (this.zzbij instanceof zzpd) {
                zzpd zzpd2 = (zzpd) this.zzbij;
                if (!(zzpd2 == null || zzpd2.getContext() == null || !zzbv.zzfh().zzu(this.zzbjt.getContext()))) {
                    zzaix zzks = zzpd2.zzks();
                    if (zzks != null) {
                        zzks.zzx(false);
                    }
                    zzfp zzfp = (zzfp) this.zzbkb.get();
                    if (!(zzfp == null || zzks == null)) {
                        zzfp.zzb(zzks);
                    }
                }
            }
            if (!(this.zzbij instanceof zzoy) || !((zzoy) this.zzbij).zzkm()) {
                this.zzbij = zzpd;
                if (zzpd instanceof zzoy) {
                    ((zzoy) zzpd).zzc(null);
                }
            } else {
                ((zzoy) this.zzbij).zzc(zzpd);
            }
            if (this.zzvh != null) {
                if (((Boolean) zzkb.zzik().zzd(zznk.zzbbu)).booleanValue()) {
                    this.zzvh.setClickable(false);
                }
                this.zzvh.removeAllViews();
                boolean zzkj = zzpd.zzkj();
                if (zzkj) {
                    if (this.zzbjw != null) {
                        String[] strArr = {NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW, UnifiedNativeAdAssetNames.ASSET_ADCHOICES_CONTAINER_VIEW};
                        int i2 = 0;
                        while (true) {
                            if (i2 >= 2) {
                                break;
                            }
                            WeakReference weakReference = (WeakReference) this.zzbjw.get(strArr[i2]);
                            if (weakReference != null) {
                                view = (View) weakReference.get();
                                break;
                            }
                            i2++;
                        }
                    }
                    view = null;
                    if (view instanceof ViewGroup) {
                        viewGroup = (ViewGroup) view;
                        if (zzkj || viewGroup == null) {
                            z = false;
                        }
                        this.zzbjx = zzpd.zza((OnClickListener) this, z);
                        if (this.zzbjx != null) {
                            if (this.zzbjw != null) {
                                this.zzbjw.put(NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE, new WeakReference(this.zzbjx));
                            }
                            if (z) {
                                viewGroup.removeAllViews();
                                viewGroup.addView(this.zzbjx);
                            } else {
                                AdChoicesView adChoicesView = new AdChoicesView(zzpd.getContext());
                                adChoicesView.setLayoutParams(new LayoutParams(-1, -1));
                                adChoicesView.addView(this.zzbjx);
                                if (this.zzvh != null) {
                                    this.zzvh.addView(adChoicesView);
                                }
                            }
                        }
                        zzpd.zza((View) this.zzbjt, this.zzbjw, null, (OnTouchListener) this, (OnClickListener) this);
                        if (this.zzbjv) {
                            if (this.zzbju == null) {
                                this.zzbju = new View(this.zzbjt.getContext());
                                this.zzbju.setLayoutParams(new LayoutParams(-1, 0));
                            }
                            if (this.zzbjt != this.zzbju.getParent()) {
                                this.zzbjt.addView(this.zzbju);
                            }
                        }
                        zzaqw = zzpd.zzko();
                        if (!(zzaqw == null || this.zzvh == null)) {
                            this.zzvh.addView(zzaqw.getView());
                        }
                        synchronized (this.mLock) {
                            zzpd.zzf(this.zzbjw);
                            if (this.zzbjw != null) {
                                String[] strArr2 = zzbjs;
                                int length = strArr2.length;
                                while (true) {
                                    if (i >= length) {
                                        break;
                                    }
                                    WeakReference weakReference2 = (WeakReference) this.zzbjw.get(strArr2[i]);
                                    if (weakReference2 != null) {
                                        view2 = (View) weakReference2.get();
                                        break;
                                    }
                                    i++;
                                }
                            }
                            if (!(view2 instanceof FrameLayout)) {
                                zzpd.zzkq();
                            } else {
                                zzpo zzpo = new zzpo(this, view2);
                                if (zzpd instanceof zzoy) {
                                    zzpd.zzb(view2, (zzox) zzpo);
                                } else {
                                    zzpd.zza(view2, (zzox) zzpo);
                                }
                            }
                        }
                        zzpd.zzi(this.zzbjt);
                        zzl(this.zzbjt);
                        this.zzbij.zzj(this.zzbjt);
                        if (this.zzbij instanceof zzpd) {
                            zzpd zzpd3 = (zzpd) this.zzbij;
                            if (!(zzpd3 == null || zzpd3.getContext() == null || !zzbv.zzfh().zzu(this.zzbjt.getContext()))) {
                                zzfp zzfp2 = (zzfp) this.zzbkb.get();
                                if (zzfp2 == null) {
                                    zzfp2 = new zzfp(this.zzbjt.getContext(), this.zzbjt);
                                    this.zzbkb = new WeakReference<>(zzfp2);
                                }
                                zzfp2.zza((zzft) zzpd3.zzks());
                            }
                        }
                    }
                }
                viewGroup = null;
                if (zzkj) {
                }
                z = false;
                this.zzbjx = zzpd.zza((OnClickListener) this, z);
                if (this.zzbjx != null) {
                }
                zzpd.zza((View) this.zzbjt, this.zzbjw, null, (OnTouchListener) this, (OnClickListener) this);
                if (this.zzbjv) {
                }
                try {
                    zzaqw = zzpd.zzko();
                } catch (Exception e) {
                    zzbv.zzem();
                    if (zzakq.zzrp()) {
                        zzakb.zzdk("Privileged processes cannot create HTML overlays.");
                    } else {
                        zzakb.zzb("Error obtaining overlay.", e);
                    }
                    zzaqw = null;
                }
                this.zzvh.addView(zzaqw.getView());
                synchronized (this.mLock) {
                }
                zzpd.zzi(this.zzbjt);
                zzl(this.zzbjt);
                this.zzbij.zzj(this.zzbjt);
                if (this.zzbij instanceof zzpd) {
                }
            }
        }
    }

    public final IObjectWrapper zzak(String str) {
        synchronized (this.mLock) {
            View view = null;
            if (this.zzbjw == null) {
                return null;
            }
            WeakReference weakReference = (WeakReference) this.zzbjw.get(str);
            if (weakReference != null) {
                view = (View) weakReference.get();
            }
            IObjectWrapper wrap = ObjectWrapper.wrap(view);
            return wrap;
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper, int i) {
        if (zzbv.zzfh().zzu(this.zzbjt.getContext()) && this.zzbkb != null) {
            zzfp zzfp = (zzfp) this.zzbkb.get();
            if (zzfp != null) {
                zzfp.zzgm();
            }
        }
        zzkt();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        return;
     */
    public final void zzb(String str, IObjectWrapper iObjectWrapper) {
        View view = (View) ObjectWrapper.unwrap(iObjectWrapper);
        synchronized (this.mLock) {
            if (this.zzbjw != null) {
                if (view == null) {
                    this.zzbjw.remove(str);
                } else {
                    this.zzbjw.put(str, new WeakReference(view));
                    if (!NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str)) {
                        if (!UnifiedNativeAdAssetNames.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str)) {
                            view.setOnTouchListener(this);
                            view.setClickable(true);
                            view.setOnClickListener(this);
                        }
                    }
                }
            }
        }
    }

    public final void zzc(IObjectWrapper iObjectWrapper) {
        this.zzbij.setClickConfirmingView((View) ObjectWrapper.unwrap(iObjectWrapper));
    }
}
