package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzcz {
    private static final String TAG = "zzcz";
    private volatile boolean zzqt = false;
    protected Context zzrt;
    private ExecutorService zzru;
    private DexClassLoader zzrv;
    private zzck zzrw;
    private byte[] zzrx;
    private volatile AdvertisingIdClient zzry = null;
    private Future zzrz = null;
    private boolean zzsa;
    /* access modifiers changed from: private */
    public volatile zzba zzsb = null;
    private Future zzsc = null;
    private zzcc zzsd;
    private boolean zzse = false;
    private boolean zzsf = false;
    private Map<Pair<String, String>, zzeg> zzsg;
    private boolean zzsh = false;
    /* access modifiers changed from: private */
    public boolean zzsi;
    private boolean zzsj;

    final class zza extends BroadcastReceiver {
        private zza() {
        }

        /* synthetic */ zza(zzcz zzcz, zzda zzda) {
            this();
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                zzcz.this.zzsi = true;
                return;
            }
            if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                zzcz.this.zzsi = false;
            }
        }
    }

    private zzcz(Context context) {
        boolean z = true;
        this.zzsi = true;
        this.zzsj = false;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext == null) {
            z = false;
        }
        this.zzsa = z;
        if (this.zzsa) {
            context = applicationContext;
        }
        this.zzrt = context;
        this.zzsg = new HashMap();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(18:1|2|(1:4)|5|6|7|8|(1:10)(1:11)|12|(1:14)(1:15)|16|17|18|(2:20|(2:22|23))|24|25|26|(15:27|28|(2:30|(2:32|33))|34|(1:36)|37|38|39|40|41|42|43|(1:45)|46|47)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004b */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054 A[Catch:{ zzcl -> 0x0150, zzcw -> 0x0157 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0086 A[Catch:{ all -> 0x011d, FileNotFoundException -> 0x0149, IOException -> 0x0142, zzcl -> 0x013b, NullPointerException -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b0 A[Catch:{ all -> 0x011d, FileNotFoundException -> 0x0149, IOException -> 0x0142, zzcl -> 0x013b, NullPointerException -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f8 A[Catch:{ zzcl -> 0x0150, zzcw -> 0x0157 }] */
    public static zzcz zza(Context context, String str, String str2, boolean z) {
        File cacheDir;
        String str3;
        File file;
        zzcz zzcz = new zzcz(context);
        try {
            zzcz.zzru = Executors.newCachedThreadPool(new zzda());
            zzcz.zzqt = z;
            if (z) {
                zzcz.zzrz = zzcz.zzru.submit(new zzdb(zzcz));
            }
            zzcz.zzru.execute(new zzdd(zzcz));
            GoogleApiAvailabilityLight instance = GoogleApiAvailabilityLight.getInstance();
            zzcz.zzse = instance.getApkVersion(zzcz.zzrt) > 0;
            zzcz.zzsf = instance.isGooglePlayServicesAvailable(zzcz.zzrt) == 0;
            zzcz.zza(0, true);
            if (zzdg.isMainThread()) {
                if (((Boolean) zzkb.zzik().zzd(zznk.zzbaz)).booleanValue()) {
                    throw new IllegalStateException("Task Context initialization must not be called from the UI thread.");
                }
            }
            zzcz.zzrw = new zzck(null);
            zzcz.zzrx = zzcz.zzrw.zzl(str);
            try {
                cacheDir = zzcz.zzrt.getCacheDir();
                if (cacheDir == null) {
                    cacheDir = zzcz.zzrt.getDir("dex", 0);
                    if (cacheDir == null) {
                        throw new zzcw();
                    }
                }
                str3 = "1521499837408";
                file = new File(String.format("%s/%s.jar", new Object[]{cacheDir, str3}));
                if (!file.exists()) {
                    byte[] zza2 = zzcz.zzrw.zza(zzcz.zzrx, str2);
                    file.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(zza2, 0, zza2.length);
                    fileOutputStream.close();
                }
                zzcz.zzb(cacheDir, str3);
                zzcz.zzrv = new DexClassLoader(file.getAbsolutePath(), cacheDir.getAbsolutePath(), null, zzcz.zzrt.getClassLoader());
                zzb(file);
                zzcz.zza(cacheDir, str3);
                zzm(String.format("%s/%s.dex", new Object[]{cacheDir, str3}));
                if (!zzcz.zzsj) {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.USER_PRESENT");
                    intentFilter.addAction("android.intent.action.SCREEN_OFF");
                    zzcz.zzrt.registerReceiver(new zza(zzcz, null), intentFilter);
                    zzcz.zzsj = true;
                }
                zzcz.zzsd = new zzcc(zzcz);
                zzcz.zzsh = true;
                return zzcz;
            } catch (FileNotFoundException e) {
                throw new zzcw(e);
            } catch (IOException e2) {
                throw new zzcw(e2);
            } catch (zzcl e3) {
                throw new zzcw(e3);
            } catch (NullPointerException e4) {
                throw new zzcw(e4);
            } catch (Throwable th) {
                zzb(file);
                zzcz.zza(cacheDir, str3);
                zzm(String.format("%s/%s.dex", new Object[]{cacheDir, str3}));
                throw th;
            }
        } catch (zzcl e5) {
            throw new zzcw(e5);
        } catch (zzcw unused) {
            return zzcz;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0091 */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a3 A[SYNTHETIC, Splitter:B:39:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a8 A[SYNTHETIC, Splitter:B:43:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b2 A[SYNTHETIC, Splitter:B:52:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b7 A[SYNTHETIC, Splitter:B:56:0x00b7] */
    private final void zza(File file, String str) {
        FileInputStream fileInputStream;
        File file2 = new File(String.format("%s/%s.tmp", new Object[]{file, str}));
        if (!file2.exists()) {
            File file3 = new File(String.format("%s/%s.dex", new Object[]{file, str}));
            if (file3.exists()) {
                long length = file3.length();
                if (length > 0) {
                    byte[] bArr = new byte[((int) length)];
                    FileOutputStream fileOutputStream = null;
                    try {
                        fileInputStream = new FileInputStream(file3);
                        try {
                            if (fileInputStream.read(bArr) <= 0) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException unused) {
                                }
                                zzb(file3);
                                return;
                            }
                            zzbe zzbe = new zzbe();
                            zzbe.zzgs = VERSION.SDK.getBytes();
                            zzbe.zzgr = str.getBytes();
                            byte[] bytes = this.zzrw.zzb(this.zzrx, bArr).getBytes();
                            zzbe.data = bytes;
                            zzbe.zzgq = zzbk.zzb(bytes);
                            file2.createNewFile();
                            FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
                            try {
                                byte[] zzb = zzbfi.zzb(zzbe);
                                fileOutputStream2.write(zzb, 0, zzb.length);
                                fileOutputStream2.close();
                                fileInputStream.close();
                                fileOutputStream2.close();
                                zzb(file3);
                            } catch (zzcl | IOException | NoSuchAlgorithmException unused2) {
                                fileOutputStream = fileOutputStream2;
                                if (fileInputStream != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                zzb(file3);
                            } catch (Throwable th) {
                                th = th;
                                fileOutputStream = fileOutputStream2;
                                if (fileInputStream != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                zzb(file3);
                                throw th;
                            }
                        } catch (zzcl | IOException | NoSuchAlgorithmException unused3) {
                            if (fileInputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            zzb(file3);
                        } catch (Throwable th2) {
                            th = th2;
                            if (fileInputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            zzb(file3);
                            throw th;
                        }
                    } catch (zzcl | IOException | NoSuchAlgorithmException unused4) {
                        fileInputStream = null;
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException unused5) {
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException unused6) {
                            }
                        }
                        zzb(file3);
                    } catch (Throwable th3) {
                        th = th3;
                        fileInputStream = null;
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException unused7) {
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException unused8) {
                            }
                        }
                        zzb(file3);
                        throw th;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean zza(int i, zzba zzba) {
        if (i < 4) {
            if (zzba == null) {
                return true;
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzbbc)).booleanValue() && (zzba.zzcx == null || zzba.zzcx.equals("0000000000000000000000000000000000000000000000000000000000000000"))) {
                return true;
            }
            if (((Boolean) zzkb.zzik().zzd(zznk.zzbbd)).booleanValue() && (zzba.zzfn == null || zzba.zzfn.zzgl == null || zzba.zzfn.zzgl.longValue() == -2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public final void zzal() {
        try {
            if (this.zzry == null && this.zzsa) {
                AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(this.zzrt);
                advertisingIdClient.start();
                this.zzry = advertisingIdClient;
            }
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException unused) {
            this.zzry = null;
        }
    }

    @VisibleForTesting
    private final zzba zzam() {
        try {
            return zzatq.zzl(this.zzrt, this.zzrt.getPackageName(), Integer.toString(this.zzrt.getPackageManager().getPackageInfo(this.zzrt.getPackageName(), 0).versionCode));
        } catch (Throwable unused) {
            return null;
        }
    }

    private static void zzb(File file) {
        if (!file.exists()) {
            Log.d(TAG, String.format("File %s not found. No need for deletion", new Object[]{file.getAbsolutePath()}));
            return;
        }
        file.delete();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:29|30|31|32|33|34|35|36) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x00b1 */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c7 A[SYNTHETIC, Splitter:B:52:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00cc A[SYNTHETIC, Splitter:B:56:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00d3 A[SYNTHETIC, Splitter:B:64:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00d8 A[SYNTHETIC, Splitter:B:68:0x00d8] */
    private final boolean zzb(File file, String str) {
        FileInputStream fileInputStream;
        File file2 = new File(String.format("%s/%s.tmp", new Object[]{file, str}));
        if (!file2.exists()) {
            return false;
        }
        File file3 = new File(String.format("%s/%s.dex", new Object[]{file, str}));
        if (file3.exists()) {
            return false;
        }
        FileOutputStream fileOutputStream = null;
        try {
            long length = file2.length();
            if (length <= 0) {
                zzb(file2);
                return false;
            }
            byte[] bArr = new byte[((int) length)];
            fileInputStream = new FileInputStream(file2);
            try {
                if (fileInputStream.read(bArr) <= 0) {
                    Log.d(TAG, "Cannot read the cache data.");
                    zzb(file2);
                    try {
                        fileInputStream.close();
                    } catch (IOException unused) {
                    }
                    return false;
                }
                zzbe zzbe = (zzbe) zzbfi.zza(new zzbe(), bArr);
                if (str.equals(new String(zzbe.zzgr)) && Arrays.equals(zzbe.zzgq, zzbk.zzb(zzbe.data))) {
                    if (Arrays.equals(zzbe.zzgs, VERSION.SDK.getBytes())) {
                        byte[] zza2 = this.zzrw.zza(this.zzrx, new String(zzbe.data));
                        file3.createNewFile();
                        FileOutputStream fileOutputStream2 = new FileOutputStream(file3);
                        try {
                            fileOutputStream2.write(zza2, 0, zza2.length);
                            fileInputStream.close();
                            fileOutputStream2.close();
                            return true;
                        } catch (zzcl | IOException | NoSuchAlgorithmException unused2) {
                            fileOutputStream = fileOutputStream2;
                            if (fileInputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream = fileOutputStream2;
                            if (fileInputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            throw th;
                        }
                    }
                }
                zzb(file2);
                try {
                    fileInputStream.close();
                } catch (IOException unused3) {
                }
                return false;
            } catch (zzcl | IOException | NoSuchAlgorithmException unused4) {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException unused5) {
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException unused6) {
                    }
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException unused7) {
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException unused8) {
                    }
                }
                throw th;
            }
        } catch (zzcl | IOException | NoSuchAlgorithmException unused9) {
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            throw th;
        }
    }

    private static void zzm(String str) {
        zzb(new File(str));
    }

    public final Context getContext() {
        return this.zzrt;
    }

    public final boolean isInitialized() {
        return this.zzsh;
    }

    public final Method zza(String str, String str2) {
        zzeg zzeg = (zzeg) this.zzsg.get(new Pair(str, str2));
        if (zzeg == null) {
            return null;
        }
        return zzeg.zzaw();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final void zza(int i, boolean z) {
        if (this.zzsf) {
            Future submit = this.zzru.submit(new zzdc(this, i, z));
            if (i == 0) {
                this.zzsc = submit;
            }
        }
    }

    public final boolean zza(String str, String str2, Class<?>... clsArr) {
        if (this.zzsg.containsKey(new Pair(str, str2))) {
            return false;
        }
        this.zzsg.put(new Pair(str, str2), new zzeg(this, str, str2, clsArr));
        return true;
    }

    public final ExecutorService zzab() {
        return this.zzru;
    }

    public final DexClassLoader zzac() {
        return this.zzrv;
    }

    public final zzck zzad() {
        return this.zzrw;
    }

    public final byte[] zzae() {
        return this.zzrx;
    }

    public final boolean zzaf() {
        return this.zzse;
    }

    public final zzcc zzag() {
        return this.zzsd;
    }

    public final boolean zzah() {
        return this.zzsf;
    }

    public final boolean zzai() {
        return this.zzsi;
    }

    public final zzba zzaj() {
        return this.zzsb;
    }

    public final Future zzak() {
        return this.zzsc;
    }

    public final AdvertisingIdClient zzan() {
        if (!this.zzqt) {
            return null;
        }
        if (this.zzry != null) {
            return this.zzry;
        }
        if (this.zzrz != null) {
            try {
                this.zzrz.get(2000, TimeUnit.MILLISECONDS);
                this.zzrz = null;
            } catch (InterruptedException | ExecutionException unused) {
            } catch (TimeoutException unused2) {
                this.zzrz.cancel(true);
            }
        }
        return this.zzry;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final zzba zzb(int i, boolean z) {
        if (i > 0 && z) {
            try {
                Thread.sleep((long) (i * 1000));
            } catch (InterruptedException unused) {
            }
        }
        return zzam();
    }

    public final int zzx() {
        if (this.zzsd != null) {
            return zzcc.zzx();
        }
        return Integer.MIN_VALUE;
    }
}
