package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class zzqp extends zzek implements zzqo {
    public zzqp() {
        super("com.google.android.gms.ads.internal.formats.client.INativeContentAd");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004a, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007c, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0087, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        IInterface iInterface;
        String str;
        switch (i) {
            case 2:
                iInterface = zzka();
                break;
            case 3:
                str = getHeadline();
                break;
            case 4:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                break;
            case 5:
                str = getBody();
                break;
            case 6:
                iInterface = zzkg();
                break;
            case 7:
                str = getCallToAction();
                break;
            case 8:
                str = getAdvertiser();
                break;
            case 9:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzel.zzb(parcel2, extras);
                break;
            case 10:
                destroy();
                break;
            case 11:
                iInterface = getVideoController();
                break;
            case 12:
                performClick((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                break;
            case 13:
                boolean recordImpression = recordImpression((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                zzel.zza(parcel2, recordImpression);
                break;
            case 14:
                reportTouchEvent((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                break;
            case 15:
                iInterface = zzkf();
                break;
            case 16:
                iInterface = zzke();
                break;
            case 17:
                str = getMediationAdapterClassName();
                break;
            default:
                return false;
        }
    }
}
