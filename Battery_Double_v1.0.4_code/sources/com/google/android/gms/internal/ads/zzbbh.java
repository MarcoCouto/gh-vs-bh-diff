package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbbh {
    static final /* synthetic */ int[] zzdql = new int[zzbes.values().length];
    static final /* synthetic */ int[] zzdrb = new int[zzbex.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(54:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|(3:71|72|74)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(55:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|(3:71|72|74)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(57:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(58:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(59:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|(2:21|22)|23|(2:25|26)|27|(2:29|30)|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(61:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|17|18|19|(2:21|22)|23|(2:25|26)|27|29|30|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|(2:51|52)|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(63:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|17|18|19|(2:21|22)|23|25|26|27|29|30|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(64:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|(2:21|22)|23|25|26|27|29|30|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Can't wrap try/catch for region: R(65:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|25|26|27|29|30|31|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|74) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x006e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x007a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x0086 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x0092 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x009e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00aa */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00b6 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00c2 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00ce */
    /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00ed */
    /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00f7 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x0101 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x010b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x0115 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:67:0x011f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:69:0x0129 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:71:0x0133 */
    static {
        try {
            zzdql[zzbes.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzdql[zzbes.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zzdql[zzbes.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            zzdql[zzbes.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            zzdql[zzbes.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            zzdql[zzbes.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            zzdql[zzbes.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            zzdql[zzbes.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        zzdql[zzbes.GROUP.ordinal()] = 9;
        zzdql[zzbes.MESSAGE.ordinal()] = 10;
        zzdql[zzbes.STRING.ordinal()] = 11;
        zzdql[zzbes.BYTES.ordinal()] = 12;
        zzdql[zzbes.UINT32.ordinal()] = 13;
        zzdql[zzbes.SFIXED32.ordinal()] = 14;
        zzdql[zzbes.SFIXED64.ordinal()] = 15;
        zzdql[zzbes.SINT32.ordinal()] = 16;
        zzdql[zzbes.SINT64.ordinal()] = 17;
        try {
            zzdql[zzbes.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError unused9) {
        }
        zzdrb[zzbex.INT.ordinal()] = 1;
        zzdrb[zzbex.LONG.ordinal()] = 2;
        zzdrb[zzbex.FLOAT.ordinal()] = 3;
        zzdrb[zzbex.DOUBLE.ordinal()] = 4;
        zzdrb[zzbex.BOOLEAN.ordinal()] = 5;
        zzdrb[zzbex.STRING.ordinal()] = 6;
        zzdrb[zzbex.BYTE_STRING.ordinal()] = 7;
        zzdrb[zzbex.ENUM.ordinal()] = 8;
        try {
            zzdrb[zzbex.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError unused10) {
        }
    }
}
