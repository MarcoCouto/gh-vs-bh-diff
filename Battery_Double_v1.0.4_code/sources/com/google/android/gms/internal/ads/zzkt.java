package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public abstract class zzkt extends zzek implements zzks {
    public zzkt() {
        super("com.google.android.gms.ads.internal.client.IAdManager");
    }

    public static zzks zzb(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        return queryLocalInterface instanceof zzks ? (zzks) queryLocalInterface : new zzku(iBinder);
    }

    /* JADX WARNING: type inference failed for: r4v1 */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.google.android.gms.internal.ads.zzkh] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.google.android.gms.internal.ads.zzkj] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.google.android.gms.internal.ads.zzkh] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.google.android.gms.internal.ads.zzla] */
    /* JADX WARNING: type inference failed for: r4v9, types: [com.google.android.gms.internal.ads.zzlc] */
    /* JADX WARNING: type inference failed for: r4v11, types: [com.google.android.gms.internal.ads.zzla] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.google.android.gms.internal.ads.zzke] */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.google.android.gms.internal.ads.zzkg] */
    /* JADX WARNING: type inference failed for: r4v16, types: [com.google.android.gms.internal.ads.zzke] */
    /* JADX WARNING: type inference failed for: r4v17, types: [com.google.android.gms.internal.ads.zzlg] */
    /* JADX WARNING: type inference failed for: r4v19, types: [com.google.android.gms.internal.ads.zzli] */
    /* JADX WARNING: type inference failed for: r4v21, types: [com.google.android.gms.internal.ads.zzlg] */
    /* JADX WARNING: type inference failed for: r4v22, types: [com.google.android.gms.internal.ads.zzkx] */
    /* JADX WARNING: type inference failed for: r4v24, types: [com.google.android.gms.internal.ads.zzkz] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.google.android.gms.internal.ads.zzkx] */
    /* JADX WARNING: type inference failed for: r4v27 */
    /* JADX WARNING: type inference failed for: r4v28 */
    /* JADX WARNING: type inference failed for: r4v29 */
    /* JADX WARNING: type inference failed for: r4v30 */
    /* JADX WARNING: type inference failed for: r4v31 */
    /* JADX WARNING: type inference failed for: r4v32 */
    /* JADX WARNING: type inference failed for: r4v33 */
    /* JADX WARNING: type inference failed for: r4v34 */
    /* JADX WARNING: type inference failed for: r4v35 */
    /* JADX WARNING: type inference failed for: r4v36 */
    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.zzel.zza(android.os.Parcel, boolean):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.zzel.zza(android.os.Parcel, android.os.IInterface):null, types can be incorrect */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e0, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0117, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zzb(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0182, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x018c, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0194, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x019b, code lost:
        return true;
     */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r4v1
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.zzlc, com.google.android.gms.internal.ads.zzkj, com.google.android.gms.internal.ads.zzkh, com.google.android.gms.internal.ads.zzla, com.google.android.gms.internal.ads.zzkg, com.google.android.gms.internal.ads.zzke, com.google.android.gms.internal.ads.zzli, com.google.android.gms.internal.ads.zzlg, com.google.android.gms.internal.ads.zzkz, com.google.android.gms.internal.ads.zzkx]
  uses: [com.google.android.gms.internal.ads.zzkh, com.google.android.gms.internal.ads.zzla, com.google.android.gms.internal.ads.zzke, com.google.android.gms.internal.ads.zzlg, com.google.android.gms.internal.ads.zzkx]
  mth insns count: 125
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 11 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        IInterface iInterface;
        boolean z;
        Parcelable parcelable;
        String str;
        ? r4 = 0;
        switch (i) {
            case 1:
                iInterface = zzbj();
                break;
            case 2:
                destroy();
                break;
            case 3:
                z = isReady();
                break;
            case 4:
                z = zzb((zzjj) zzel.zza(parcel, zzjj.CREATOR));
                break;
            case 5:
                pause();
                break;
            case 6:
                resume();
                break;
            case 7:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    r4 = queryLocalInterface instanceof zzkh ? (zzkh) queryLocalInterface : new zzkj(readStrongBinder);
                }
                zza((zzkh) r4);
                break;
            case 8:
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
                    r4 = queryLocalInterface2 instanceof zzla ? (zzla) queryLocalInterface2 : new zzlc(readStrongBinder2);
                }
                zza((zzla) r4);
                break;
            case 9:
                showInterstitial();
                break;
            case 10:
                stopLoading();
                break;
            case 11:
                zzbm();
                break;
            case 12:
                parcelable = zzbk();
                break;
            case 13:
                zza((zzjn) zzel.zza(parcel, zzjn.CREATOR));
                break;
            case 14:
                zza(zzaax.zzv(parcel.readStrongBinder()));
                break;
            case 15:
                zza(zzabd.zzx(parcel.readStrongBinder()), parcel.readString());
                break;
            case 18:
                str = getMediationAdapterClassName();
                break;
            case 19:
                zza(zzoe.zzf(parcel.readStrongBinder()));
                break;
            case 20:
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdClickListener");
                    r4 = queryLocalInterface3 instanceof zzke ? (zzke) queryLocalInterface3 : new zzkg(readStrongBinder3);
                }
                zza((zzke) r4);
                break;
            case 21:
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    r4 = queryLocalInterface4 instanceof zzlg ? (zzlg) queryLocalInterface4 : new zzli(readStrongBinder4);
                }
                zza((zzlg) r4);
                break;
            case 22:
                setManualImpressionsEnabled(zzel.zza(parcel));
                break;
            case 23:
                z = isLoading();
                break;
            case 24:
                zza(zzahf.zzz(parcel.readStrongBinder()));
                break;
            case 25:
                setUserId(parcel.readString());
                break;
            case 26:
                iInterface = getVideoController();
                break;
            case 29:
                zza((zzmu) zzel.zza(parcel, zzmu.CREATOR));
                break;
            case 30:
                zza((zzlu) zzel.zza(parcel, zzlu.CREATOR));
                break;
            case 31:
                str = getAdUnitId();
                break;
            case 32:
                iInterface = zzbw();
                break;
            case 33:
                iInterface = zzbx();
                break;
            case 34:
                setImmersiveMode(zzel.zza(parcel));
                break;
            case 35:
                str = zzck();
                break;
            case 36:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdMetadataListener");
                    r4 = queryLocalInterface5 instanceof zzkx ? (zzkx) queryLocalInterface5 : new zzkz(readStrongBinder5);
                }
                zza((zzkx) r4);
                break;
            case 37:
                parcelable = zzba();
                break;
            default:
                return false;
        }
    }
}
