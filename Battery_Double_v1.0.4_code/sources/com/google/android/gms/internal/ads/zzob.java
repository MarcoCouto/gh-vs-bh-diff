package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;

public abstract class zzob extends zzek implements zzoa {
    public zzob() {
        super("com.google.android.gms.ads.internal.customrenderedad.client.ICustomRenderedAd");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        switch (i) {
            case 1:
                str = zzjn();
                break;
            case 2:
                str = getContent();
                break;
            case 3:
                zzg(Stub.asInterface(parcel.readStrongBinder()));
                break;
            case 4:
                recordClick();
                break;
            case 5:
                recordImpression();
                break;
            default:
                return false;
        }
    }
}
