package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public abstract class zzql extends zzek implements zzqk {
    public zzql() {
        super("com.google.android.gms.ads.internal.formats.client.INativeAppInstallAd");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004b, code lost:
        r3.writeNoException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0098, code lost:
        r3.writeNoException();
        com.google.android.gms.internal.ads.zzel.zza(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009f, code lost:
        return true;
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        IInterface iInterface;
        String str;
        switch (i) {
            case 2:
                iInterface = zzka();
                break;
            case 3:
                str = getHeadline();
                break;
            case 4:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                break;
            case 5:
                str = getBody();
                break;
            case 6:
                iInterface = zzjz();
                break;
            case 7:
                str = getCallToAction();
                break;
            case 8:
                double starRating = getStarRating();
                parcel2.writeNoException();
                parcel2.writeDouble(starRating);
                break;
            case 9:
                str = getStore();
                break;
            case 10:
                str = getPrice();
                break;
            case 11:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzel.zzb(parcel2, extras);
                break;
            case 12:
                destroy();
                break;
            case 13:
                iInterface = getVideoController();
                break;
            case 14:
                performClick((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                break;
            case 15:
                boolean recordImpression = recordImpression((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                parcel2.writeNoException();
                zzel.zza(parcel2, recordImpression);
                break;
            case 16:
                reportTouchEvent((Bundle) zzel.zza(parcel, Bundle.CREATOR));
                break;
            case 17:
                iInterface = zzkf();
                break;
            case 18:
                iInterface = zzke();
                break;
            case 19:
                str = getMediationAdapterClassName();
                break;
            default:
                return false;
        }
    }
}
