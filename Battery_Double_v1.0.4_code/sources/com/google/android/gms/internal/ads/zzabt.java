package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbc;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;

@zzadh
public final class zzabt extends zzajx {
    private final Object mLock;
    /* access modifiers changed from: private */
    public final zzabm zzbzd;
    private final zzaji zzbze;
    private final zzaej zzbzf;
    private final zzabv zzbzu;
    private Future<zzajh> zzbzv;

    public zzabt(Context context, zzbc zzbc, zzaji zzaji, zzci zzci, zzabm zzabm, zznx zznx) {
        zzabv zzabv = new zzabv(context, zzbc, new zzalt(context), zzci, zzaji, zznx);
        this(zzaji, zzabm, zzabv);
    }

    private zzabt(zzaji zzaji, zzabm zzabm, zzabv zzabv) {
        this.mLock = new Object();
        this.zzbze = zzaji;
        this.zzbzf = zzaji.zzcos;
        this.zzbzd = zzabm;
        this.zzbzu = zzabv;
    }

    public final void onStop() {
        synchronized (this.mLock) {
            if (this.zzbzv != null) {
                this.zzbzv.cancel(true);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0037  */
    public final void zzdn() {
        int i;
        zzajh zzajh;
        int i2 = 0;
        zzajh zzajh2 = null;
        try {
            synchronized (this.mLock) {
                this.zzbzv = zzaki.zza(this.zzbzu);
            }
            i = -2;
            zzajh2 = (zzajh) this.zzbzv.get(60000, TimeUnit.MILLISECONDS);
        } catch (TimeoutException unused) {
            zzakb.zzdk("Timed out waiting for native ad.");
            i2 = 2;
            this.zzbzv.cancel(true);
        } catch (InterruptedException | CancellationException | ExecutionException unused2) {
        } catch (Throwable th) {
            while (true) {
            }
            throw th;
        }
        if (zzajh2 == null) {
            zzajh = zzajh2;
        } else {
            zzjj zzjj = this.zzbze.zzcgs.zzccv;
            int i3 = this.zzbzf.orientation;
            long j = this.zzbzf.zzbsu;
            String str = this.zzbze.zzcgs.zzccy;
            long j2 = this.zzbzf.zzcer;
            zzjn zzjn = this.zzbze.zzacv;
            int i4 = i3;
            long j3 = this.zzbzf.zzcep;
            long j4 = this.zzbze.zzcoh;
            long j5 = this.zzbzf.zzceu;
            String str2 = this.zzbzf.zzcev;
            JSONObject jSONObject = this.zzbze.zzcob;
            boolean z = this.zzbze.zzcos.zzcfh;
            long j6 = j2;
            zzael zzael = this.zzbze.zzcos.zzcfi;
            String str3 = this.zzbzf.zzcfl;
            zzhs zzhs = this.zzbze.zzcoq;
            boolean z2 = this.zzbze.zzcos.zzzl;
            boolean z3 = this.zzbze.zzcos.zzcfp;
            boolean z4 = z;
            JSONObject jSONObject2 = jSONObject;
            String str4 = str2;
            long j7 = j5;
            long j8 = j3;
            long j9 = j4;
            int i5 = i4;
            zzjn zzjn2 = zzjn;
            zzael zzael2 = zzael;
            zzajh = new zzajh(zzjj, null, null, i, null, null, i5, j, str, false, null, null, null, null, null, j6, zzjn2, j8, j9, j7, str4, jSONObject2, null, null, null, null, z4, zzael2, null, null, str3, zzhs, z2, false, z3, null, this.zzbze.zzcos.zzzm, this.zzbze.zzcos.zzcfq);
        }
        zzakk.zzcrm.post(new zzabu(this, zzajh));
        i = i2;
        if (zzajh2 == null) {
        }
        zzakk.zzcrm.post(new zzabu(this, zzajh));
    }
}
