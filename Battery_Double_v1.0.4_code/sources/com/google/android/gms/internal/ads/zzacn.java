package com.google.android.gms.internal.ads;

import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.gms.ads.impl.R;
import com.google.android.gms.ads.internal.zzbv;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
public final class zzacn implements zzacd<zzoo> {
    private final boolean zzbto;
    private final boolean zzcbk;
    private final boolean zzcbl;

    public zzacn(boolean z, boolean z2, boolean z3) {
        this.zzcbk = z;
        this.zzcbl = z2;
        this.zzbto = z3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00eb  */
    public final /* synthetic */ zzpb zza(zzabv zzabv, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException {
        String str;
        zzabv zzabv2 = zzabv;
        JSONObject jSONObject2 = jSONObject;
        List<zzanz> zza = zzabv2.zza(jSONObject2, "images", false, this.zzcbk, this.zzcbl);
        zzanz zza2 = zzabv2.zza(jSONObject2, "app_icon", true, this.zzcbk);
        zzanz zzc = zzabv2.zzc(jSONObject2, "video");
        zzanz zzg = zzabv.zzg(jSONObject);
        ArrayList arrayList = new ArrayList();
        for (zzanz zzanz : zza) {
            arrayList.add((zzon) zzanz.get());
        }
        zzaqw zzc2 = zzabv.zzc(zzc);
        String string = jSONObject2.getString("headline");
        if (this.zzbto) {
            if (((Boolean) zzkb.zzik().zzd(zznk.zzbfr)).booleanValue()) {
                Resources resources = zzbv.zzeo().getResources();
                str = resources != null ? resources.getString(R.string.s7) : "Test Ad";
                if (string != null) {
                    StringBuilder sb = new StringBuilder(3 + String.valueOf(str).length() + String.valueOf(string).length());
                    sb.append(str);
                    sb.append(" : ");
                    sb.append(string);
                    string = sb.toString();
                }
                zzoo zzoo = new zzoo(str, arrayList, jSONObject2.getString("body"), (zzpw) zza2.get(), jSONObject2.getString("call_to_action"), jSONObject2.optDouble("rating", -1.0d), jSONObject2.optString("store"), jSONObject2.optString(Param.PRICE), (zzoj) zzg.get(), new Bundle(), zzc2 == null ? zzc2.zztm() : null, zzc2 == null ? zzc2.getView() : null, null, null);
                return zzoo;
            }
        }
        str = string;
        zzoo zzoo2 = new zzoo(str, arrayList, jSONObject2.getString("body"), (zzpw) zza2.get(), jSONObject2.getString("call_to_action"), jSONObject2.optDouble("rating", -1.0d), jSONObject2.optString("store"), jSONObject2.optString(Param.PRICE), (zzoj) zzg.get(), new Bundle(), zzc2 == null ? zzc2.zztm() : null, zzc2 == null ? zzc2.getView() : null, null, null);
        return zzoo2;
    }
}
