package com.google.android.gms.internal.ads;

import java.io.IOException;

abstract class zzbee<T, B> {
    zzbee() {
    }

    /* access modifiers changed from: 0000 */
    public abstract void zza(B b, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void zza(B b, int i, zzbah zzbah);

    /* access modifiers changed from: 0000 */
    public abstract void zza(B b, int i, T t);

    /* access modifiers changed from: 0000 */
    public abstract void zza(T t, zzbey zzbey) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract boolean zza(zzbdl zzbdl);

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
    public final boolean zza(B b, zzbdl zzbdl) throws IOException {
        int tag = zzbdl.getTag();
        int i = tag >>> 3;
        switch (tag & 7) {
            case 0:
                zza(b, i, zzbdl.zzabm());
                return true;
            case 1:
                zzb(b, i, zzbdl.zzabo());
                return true;
            case 2:
                zza(b, i, zzbdl.zzabs());
                return true;
            case 3:
                Object zzagb = zzagb();
                int i2 = (i << 3) | 4;
                while (zzbdl.zzaci() != Integer.MAX_VALUE) {
                    if (!zza((B) zzagb, zzbdl)) {
                        if (i2 == zzbdl.getTag()) {
                            throw zzbbu.zzadp();
                        }
                        zza(b, i, (T) zzv(zzagb));
                        return true;
                    }
                }
                if (i2 == zzbdl.getTag()) {
                }
            case 4:
                return false;
            case 5:
                zzc(b, i, zzbdl.zzabp());
                return true;
            default:
                throw zzbbu.zzadq();
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract T zzac(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract B zzad(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract int zzae(T t);

    /* access modifiers changed from: 0000 */
    public abstract B zzagb();

    /* access modifiers changed from: 0000 */
    public abstract void zzb(B b, int i, long j);

    /* access modifiers changed from: 0000 */
    public abstract void zzc(B b, int i, int i2);

    /* access modifiers changed from: 0000 */
    public abstract void zzc(T t, zzbey zzbey) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void zze(Object obj, T t);

    /* access modifiers changed from: 0000 */
    public abstract void zzf(Object obj, B b);

    /* access modifiers changed from: 0000 */
    public abstract T zzg(T t, T t2);

    /* access modifiers changed from: 0000 */
    public abstract void zzo(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract T zzv(B b);

    /* access modifiers changed from: 0000 */
    public abstract int zzy(T t);
}
