package com.google.android.gms.internal.ads;

final class zzbeq extends zzben {
    zzbeq() {
    }

    private static int zza(byte[] bArr, int i, long j, int i2) {
        switch (i2) {
            case 0:
                return zzbem.zzda(i);
            case 1:
                return zzbem.zzz(i, zzbek.zza(bArr, j));
            case 2:
                return zzbem.zzf(i, (int) zzbek.zza(bArr, j), (int) zzbek.zza(bArr, j + 1));
            default:
                throw new AssertionError();
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ab, code lost:
        return -1;
     */
    public final int zzb(int i, byte[] bArr, int i2, int i3) {
        int i4;
        if ((i2 | i3 | (bArr.length - i3)) < 0) {
            throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i2), Integer.valueOf(i3)}));
        }
        long j = (long) i2;
        int i5 = (int) (((long) i3) - j);
        if (i5 >= 16) {
            i4 = 0;
            long j2 = j;
            while (true) {
                if (i4 >= i5) {
                    i4 = i5;
                    break;
                }
                long j3 = j2 + 1;
                if (zzbek.zza(bArr, j2) < 0) {
                    break;
                }
                i4++;
                j2 = j3;
            }
        } else {
            i4 = 0;
        }
        int i6 = i5 - i4;
        long j4 = j + ((long) i4);
        while (true) {
            int i7 = 0;
            while (true) {
                if (i6 <= 0) {
                    break;
                }
                long j5 = j4 + 1;
                byte zza = zzbek.zza(bArr, j4);
                if (zza < 0) {
                    j4 = j5;
                    i7 = zza;
                    break;
                }
                i6--;
                j4 = j5;
                i7 = zza;
            }
            if (i6 != 0) {
                int i8 = i6 - 1;
                if (i7 >= -32) {
                    if (i7 >= -16) {
                        if (i8 >= 3) {
                            i6 = i8 - 3;
                            long j6 = j4 + 1;
                            byte zza2 = zzbek.zza(bArr, j4);
                            if (zza2 > -65 || (((i7 << 28) + (zza2 + 112)) >> 30) != 0) {
                                break;
                            }
                            long j7 = j6 + 1;
                            if (zzbek.zza(bArr, j6) > -65) {
                                break;
                            }
                            j4 = j7 + 1;
                            if (zzbek.zza(bArr, j7) > -65) {
                                break;
                            }
                        } else {
                            return zza(bArr, i7, j4, i8);
                        }
                    } else if (i8 >= 2) {
                        i6 = i8 - 2;
                        long j8 = j4 + 1;
                        byte zza3 = zzbek.zza(bArr, j4);
                        if (zza3 > -65 || ((i7 == -32 && zza3 < -96) || (i7 == -19 && zza3 >= -96))) {
                            break;
                        }
                        j4 = j8 + 1;
                        if (zzbek.zza(bArr, j8) > -65) {
                            break;
                        }
                    } else {
                        return zza(bArr, i7, j4, i8);
                    }
                } else if (i8 == 0) {
                    return i7;
                } else {
                    i6 = i8 - 1;
                    if (i7 < -62) {
                        return -1;
                    }
                    long j9 = j4 + 1;
                    if (zzbek.zza(bArr, j4) > -65) {
                        return -1;
                    }
                    j4 = j9;
                }
            } else {
                return 0;
            }
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    public final int zzb(CharSequence charSequence, byte[] bArr, int i, int i2) {
        long j;
        CharSequence charSequence2 = charSequence;
        byte[] bArr2 = bArr;
        int i3 = i;
        int i4 = i2;
        long j2 = (long) i3;
        long j3 = j2 + ((long) i4);
        int length = charSequence.length();
        if (length > i4 || bArr2.length - i4 < i3) {
            char charAt = charSequence2.charAt(length - 1);
            int i5 = i3 + i4;
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt);
            sb.append(" at index ");
            sb.append(i5);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i6 = 0;
        while (i6 < length) {
            char charAt2 = charSequence2.charAt(i6);
            if (charAt2 >= 128) {
                break;
            }
            long j4 = j + 1;
            zzbek.zza(bArr2, j, (byte) charAt2);
            i6++;
            j2 = j4;
        }
        if (i6 == length) {
            return (int) j;
        }
        while (i6 < length) {
            char charAt3 = charSequence2.charAt(i6);
            if (charAt3 < 128 && j < j3) {
                long j5 = j + 1;
                zzbek.zza(bArr2, j, (byte) charAt3);
                j = j5;
            } else if (charAt3 < 2048 && j <= j3 - 2) {
                long j6 = j + 1;
                zzbek.zza(bArr2, j, (byte) (960 | (charAt3 >>> 6)));
                j = j6 + 1;
                zzbek.zza(bArr2, j6, (byte) ((charAt3 & '?') | 128));
            } else if ((charAt3 < 55296 || 57343 < charAt3) && j <= j3 - 3) {
                long j7 = j + 1;
                zzbek.zza(bArr2, j, (byte) (480 | (charAt3 >>> 12)));
                long j8 = j7 + 1;
                zzbek.zza(bArr2, j7, (byte) (((charAt3 >>> 6) & 63) | 128));
                long j9 = j8 + 1;
                zzbek.zza(bArr2, j8, (byte) ((charAt3 & '?') | 128));
                j = j9;
            } else if (j <= j3 - 4) {
                int i7 = i6 + 1;
                if (i7 != length) {
                    char charAt4 = charSequence2.charAt(i7);
                    if (Character.isSurrogatePair(charAt3, charAt4)) {
                        int codePoint = Character.toCodePoint(charAt3, charAt4);
                        long j10 = j + 1;
                        zzbek.zza(bArr2, j, (byte) (240 | (codePoint >>> 18)));
                        long j11 = j10 + 1;
                        zzbek.zza(bArr2, j10, (byte) (((codePoint >>> 12) & 63) | 128));
                        long j12 = j11 + 1;
                        zzbek.zza(bArr2, j11, (byte) (((codePoint >>> 6) & 63) | 128));
                        j = j12 + 1;
                        zzbek.zza(bArr2, j12, (byte) ((codePoint & 63) | 128));
                        i6 = i7;
                    }
                } else {
                    i7 = i6;
                }
                throw new zzbep(i7 - 1, length);
            } else {
                if (55296 <= charAt3 && charAt3 <= 57343) {
                    int i8 = i6 + 1;
                    if (i8 == length || !Character.isSurrogatePair(charAt3, charSequence2.charAt(i8))) {
                        throw new zzbep(i6, length);
                    }
                }
                StringBuilder sb2 = new StringBuilder(46);
                sb2.append("Failed writing ");
                sb2.append(charAt3);
                sb2.append(" at index ");
                sb2.append(j);
                throw new ArrayIndexOutOfBoundsException(sb2.toString());
            }
            i6++;
        }
        return (int) j;
    }
}
