package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.AdChoicesInfo;
import com.google.android.gms.ads.formats.NativeAd.Image;
import java.util.ArrayList;
import java.util.List;

@zzadh
public final class zzpv extends AdChoicesInfo {
    private final List<Image> zzbhf = new ArrayList();
    private final zzps zzbkk;
    private String zzbkl;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[Catch:{ RemoteException -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0027 A[SYNTHETIC] */
    public zzpv(zzps zzps) {
        zzpw zzpw;
        this.zzbkk = zzps;
        try {
            this.zzbkl = this.zzbkk.getText();
        } catch (RemoteException e) {
            zzane.zzb("", e);
            this.zzbkl = "";
        }
        try {
            for (Object next : zzps.zzjr()) {
                if (next instanceof IBinder) {
                    IBinder iBinder = (IBinder) next;
                    if (iBinder != null) {
                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                        zzpw = queryLocalInterface instanceof zzpw ? (zzpw) queryLocalInterface : new zzpy(iBinder);
                        if (zzpw == null) {
                            this.zzbhf.add(new zzpz(zzpw));
                        }
                    }
                }
                zzpw = null;
                if (zzpw == null) {
                }
            }
        } catch (RemoteException e2) {
            zzane.zzb("", e2);
        }
    }

    public final List<Image> getImages() {
        return this.zzbhf;
    }

    public final CharSequence getText() {
        return this.zzbkl;
    }
}
