package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzkl extends zzek implements zzkk {
    public zzkl() {
        super("com.google.android.gms.ads.internal.client.IAdLoader");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003f, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0029, code lost:
        r3.writeNoException();
        r3.writeString(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003b, code lost:
        r3.writeNoException();
     */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        switch (i) {
            case 1:
                zzd((zzjj) zzel.zza(parcel, zzjj.CREATOR));
                break;
            case 2:
                str = getMediationAdapterClassName();
                break;
            case 3:
                boolean isLoading = isLoading();
                parcel2.writeNoException();
                zzel.zza(parcel2, isLoading);
                break;
            case 4:
                str = zzck();
                break;
            case 5:
                zza((zzjj) zzel.zza(parcel, zzjj.CREATOR), parcel.readInt());
                break;
            default:
                return false;
        }
    }
}
