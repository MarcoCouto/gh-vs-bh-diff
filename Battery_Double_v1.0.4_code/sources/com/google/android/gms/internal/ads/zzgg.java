package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.common.util.PlatformVersion;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;

@zzadh
@ParametersAreNonnullByDefault
public final class zzgg {
    private final Object zzaho = new Object();
    @GuardedBy("mActivityTrackerLock")
    private zzgh zzahp = null;
    @GuardedBy("mActivityTrackerLock")
    private boolean zzahq = false;

    @Nullable
    public final Activity getActivity() {
        synchronized (this.zzaho) {
            if (!PlatformVersion.isAtLeastIceCreamSandwich()) {
                return null;
            }
            if (this.zzahp == null) {
                return null;
            }
            Activity activity = this.zzahp.getActivity();
            return activity;
        }
    }

    @Nullable
    public final Context getContext() {
        synchronized (this.zzaho) {
            if (!PlatformVersion.isAtLeastIceCreamSandwich()) {
                return null;
            }
            if (this.zzahp == null) {
                return null;
            }
            Context context = this.zzahp.getContext();
            return context;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004f, code lost:
        return;
     */
    public final void initialize(Context context) {
        synchronized (this.zzaho) {
            if (!this.zzahq) {
                if (PlatformVersion.isAtLeastIceCreamSandwich()) {
                    if (((Boolean) zzkb.zzik().zzd(zznk.zzayg)).booleanValue()) {
                        Application application = null;
                        Context applicationContext = context.getApplicationContext();
                        if (applicationContext == null) {
                            applicationContext = context;
                        }
                        if (applicationContext instanceof Application) {
                            application = (Application) applicationContext;
                        }
                        if (application == null) {
                            zzakb.zzdk("Can not cast Context to Application");
                            return;
                        }
                        if (this.zzahp == null) {
                            this.zzahp = new zzgh();
                        }
                        this.zzahp.zza(application, context);
                        this.zzahq = true;
                    }
                }
            }
        }
    }

    public final void zza(zzgj zzgj) {
        synchronized (this.zzaho) {
            if (PlatformVersion.isAtLeastIceCreamSandwich()) {
                if (((Boolean) zzkb.zzik().zzd(zznk.zzayg)).booleanValue()) {
                    if (this.zzahp == null) {
                        this.zzahp = new zzgh();
                    }
                    this.zzahp.zza(zzgj);
                }
            }
        }
    }
}
