package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzv;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.Map;
import org.json.JSONObject;

final class zzacw implements zzv<zzaqw> {
    private final /* synthetic */ zzaqw zzcbq;
    private final /* synthetic */ zzaoj zzcbr;
    private final /* synthetic */ zzacq zzcbs;

    zzacw(zzacq zzacq, zzaqw zzaqw, zzaoj zzaoj) {
        this.zzcbs = zzacq;
        this.zzcbq = zzaqw;
        this.zzcbr = zzaoj;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        boolean z;
        JSONObject jSONObject;
        try {
            String str = (String) map.get(Param.SUCCESS);
            String str2 = (String) map.get("failure");
            if (!TextUtils.isEmpty(str2)) {
                jSONObject = new JSONObject(str2);
                z = false;
            } else {
                JSONObject jSONObject2 = new JSONObject(str);
                z = true;
                jSONObject = jSONObject2;
            }
            if (this.zzcbs.zzaae.equals(jSONObject.optString("ads_id", ""))) {
                this.zzcbq.zzb("/nativeAdPreProcess", this);
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put(Param.SUCCESS, z);
                jSONObject3.put("json", jSONObject);
                this.zzcbr.set(jSONObject3);
            }
        } catch (Throwable th) {
            zzakb.zzb("Error while preprocessing json.", th);
            this.zzcbr.setException(th);
        }
    }
}
