package com.google.android.gms.internal.ads;

final /* synthetic */ class zzavi {
    static final /* synthetic */ int[] zzdhz = new int[zzaxa.values().length];
    static final /* synthetic */ int[] zzdia = new int[zzawy.values().length];
    static final /* synthetic */ int[] zzdib = new int[zzawk.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(21:0|(2:1|2)|3|5|6|7|9|10|11|12|13|15|16|17|19|20|21|22|23|24|26) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|9|10|11|12|13|15|16|17|19|20|21|22|23|24|26) */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0064 */
    static {
        try {
            zzdib[zzawk.UNCOMPRESSED.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzdib[zzawk.COMPRESSED.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        zzdia[zzawy.NIST_P256.ordinal()] = 1;
        zzdia[zzawy.NIST_P384.ordinal()] = 2;
        try {
            zzdia[zzawy.NIST_P521.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        zzdhz[zzaxa.SHA1.ordinal()] = 1;
        zzdhz[zzaxa.SHA256.ordinal()] = 2;
        zzdhz[zzaxa.SHA512.ordinal()] = 3;
    }
}
