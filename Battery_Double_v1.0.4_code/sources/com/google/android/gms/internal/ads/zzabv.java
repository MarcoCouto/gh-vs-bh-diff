package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzv;
import com.google.android.gms.ads.internal.zzbc;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.internal.ImagesContract;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzadh
public final class zzabv implements Callable<zzajh> {
    @VisibleForTesting
    private static long zzbzx = 10;
    private final Context mContext;
    private int mErrorCode;
    private final Object mLock = new Object();
    private final zzacm zzaad;
    private final zzci zzbjc;
    private final zzaji zzbze;
    private final zzalt zzbzy;
    /* access modifiers changed from: private */
    public final zzbc zzbzz;
    private boolean zzcaa;
    private List<String> zzcab;
    private JSONObject zzcac;
    private String zzcad;
    @Nullable
    private String zzcae;
    private final zznx zzvr;

    public zzabv(Context context, zzbc zzbc, zzalt zzalt, zzci zzci, zzaji zzaji, zznx zznx) {
        this.mContext = context;
        this.zzbzz = zzbc;
        this.zzbzy = zzalt;
        this.zzbze = zzaji;
        this.zzbjc = zzci;
        this.zzvr = zznx;
        this.zzaad = zzbc.zzdr();
        this.zzcaa = false;
        this.mErrorCode = -2;
        this.zzcab = null;
        this.zzcad = null;
        this.zzcae = null;
    }

    private final zzajh zza(zzpb zzpb, boolean z) {
        int i;
        synchronized (this.mLock) {
            try {
                int i2 = this.mErrorCode;
                if (zzpb == null && this.mErrorCode == -2) {
                    i2 = 0;
                }
            } finally {
                while (true) {
                    i = th;
                }
            }
        }
        zzpb zzpb2 = i != -2 ? null : zzpb;
        zzjj zzjj = this.zzbze.zzcgs.zzccv;
        List<String> list = this.zzbze.zzcos.zzbsn;
        List<String> list2 = this.zzbze.zzcos.zzbso;
        List<String> list3 = this.zzcab;
        int i3 = this.zzbze.zzcos.orientation;
        long j = this.zzbze.zzcos.zzbsu;
        String str = this.zzbze.zzcgs.zzccy;
        zzjn zzjn = this.zzbze.zzacv;
        long j2 = this.zzbze.zzcos.zzcep;
        List<String> list4 = list;
        long j3 = this.zzbze.zzcoh;
        long j4 = j2;
        long j5 = this.zzbze.zzcoi;
        String str2 = this.zzbze.zzcos.zzcev;
        JSONObject jSONObject = this.zzcac;
        boolean z2 = this.zzbze.zzcos.zzcfh;
        zzael zzael = this.zzbze.zzcos.zzcfi;
        List<String> list5 = this.zzbze.zzcos.zzbsr;
        long j6 = j3;
        String str3 = this.zzcad;
        zzhs zzhs = this.zzbze.zzcoq;
        List<String> list6 = list5;
        boolean z3 = this.zzbze.zzcos.zzzl;
        boolean z4 = this.zzbze.zzcor;
        long j7 = j6;
        String str4 = str3;
        zzhs zzhs2 = zzhs;
        List<String> list7 = list4;
        zzjn zzjn2 = zzjn;
        String str5 = str2;
        boolean z5 = z;
        zzajh zzajh = new zzajh(zzjj, null, list7, i, list2, list3, i3, j, str, false, null, null, null, null, null, 0, zzjn2, j4, j7, j5, str5, jSONObject, zzpb2, null, null, null, z2, zzael, null, list6, str4, zzhs2, z3, z4, z5, this.zzbze.zzcos.zzbsp, this.zzbze.zzcos.zzzm, this.zzcae);
        return zzajh;
    }

    private final zzanz<zzon> zza(JSONObject jSONObject, boolean z, boolean z2) throws JSONException {
        String string = z ? jSONObject.getString(ImagesContract.URL) : jSONObject.optString(ImagesContract.URL);
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        if (TextUtils.isEmpty(string)) {
            zzd(0, z);
            return zzano.zzi(null);
        } else if (z2) {
            return zzano.zzi(new zzon(null, Uri.parse(string), optDouble));
        } else {
            zzalt zzalt = this.zzbzy;
            zzacb zzacb = new zzacb(this, z, optDouble, optBoolean, string);
            return zzalt.zza(string, zzacb);
        }
    }

    private final void zzab(int i) {
        synchronized (this.mLock) {
            this.zzcaa = true;
            this.mErrorCode = i;
        }
    }

    private static zzaqw zzb(zzanz<zzaqw> zzanz) {
        try {
            return (zzaqw) zzanz.get((long) ((Integer) zzkb.zzik().zzd(zznk.zzbby)).intValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            zzane.zzc("", e);
            Thread.currentThread().interrupt();
            return null;
        } catch (CancellationException | ExecutionException | TimeoutException e2) {
            zzane.zzc("", e2);
            return null;
        }
    }

    private static Integer zzb(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt("r"), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException unused) {
            return null;
        }
    }

    static zzaqw zzc(zzanz<zzaqw> zzanz) {
        try {
            return (zzaqw) zzanz.get((long) ((Integer) zzkb.zzik().zzd(zznk.zzbbx)).intValue(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            zzakb.zzc("InterruptedException occurred while waiting for video to load", e);
            Thread.currentThread().interrupt();
            return null;
        } catch (CancellationException | ExecutionException | TimeoutException e2) {
            zzakb.zzc("Exception occurred while waiting for video to load", e2);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public final void zzc(zzqs zzqs, String str) {
        try {
            zzrc zzr = this.zzbzz.zzr(zzqs.getCustomTemplateId());
            if (zzr != null) {
                zzr.zzb(zzqs, str);
            }
        } catch (RemoteException e) {
            StringBuilder sb = new StringBuilder(40 + String.valueOf(str).length());
            sb.append("Failed to call onCustomClick for asset ");
            sb.append(str);
            sb.append(".");
            zzakb.zzc(sb.toString(), e);
        }
    }

    /* access modifiers changed from: private */
    public static <V> List<V> zzk(List<zzanz<V>> list) throws ExecutionException, InterruptedException {
        ArrayList arrayList = new ArrayList();
        for (zzanz zzanz : list) {
            Object obj = zzanz.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        if (r3.length() != 0) goto L_0x0044;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007d A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x01e7, TimeoutException -> 0x01e3, Exception -> 0x01df }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a9 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x01e7, TimeoutException -> 0x01e3, Exception -> 0x01df }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0157 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x01e7, TimeoutException -> 0x01e3, Exception -> 0x01df }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0158 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x01e7, TimeoutException -> 0x01e3, Exception -> 0x01df }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01c2 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x01e7, TimeoutException -> 0x01e3, Exception -> 0x01df }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01f1  */
    /* renamed from: zznw */
    public final zzajh call() {
        String str;
        JSONObject jSONObject;
        boolean optBoolean;
        zzanz zzanz;
        zzacd zzacd;
        zzpb zzpb;
        Object[] objArr;
        try {
            String uuid = this.zzbzz.getUuid();
            if (!zznx()) {
                JSONObject jSONObject2 = new JSONObject(this.zzbze.zzcos.zzceo);
                JSONObject jSONObject3 = new JSONObject(this.zzbze.zzcos.zzceo);
                if (jSONObject3.length() != 0) {
                    JSONArray optJSONArray = jSONObject3.optJSONArray("ads");
                    JSONObject optJSONObject = optJSONArray != null ? optJSONArray.optJSONObject(0) : null;
                    if (optJSONObject != null) {
                    }
                }
                zzab(3);
                JSONObject jSONObject4 = (JSONObject) this.zzaad.zzh(jSONObject2).get(zzbzx, TimeUnit.SECONDS);
                if (jSONObject4.optBoolean(Param.SUCCESS, false)) {
                    jSONObject = jSONObject4.getJSONObject("json").optJSONArray("ads").getJSONObject(0);
                    optBoolean = jSONObject.optBoolean("enable_omid");
                    if (optBoolean) {
                        JSONObject optJSONObject2 = jSONObject.optJSONObject("omid_settings");
                        if (optJSONObject2 != null) {
                            String optString = optJSONObject2.optString("omid_html");
                            if (!TextUtils.isEmpty(optString)) {
                                zzaoj zzaoj = new zzaoj();
                                zzaoe.zzcvy.execute(new zzabw(this, zzaoj, optString));
                                zzanz = zzaoj;
                                if (!zznx()) {
                                    if (jSONObject != null) {
                                        String string = jSONObject.getString("template_id");
                                        boolean z = this.zzbze.zzcgs.zzadj != null ? this.zzbze.zzcgs.zzadj.zzbjn : false;
                                        boolean z2 = this.zzbze.zzcgs.zzadj != null ? this.zzbze.zzcgs.zzadj.zzbjp : false;
                                        if ("2".equals(string)) {
                                            zzacd = new zzacn(z, z2, this.zzbze.zzcor);
                                        } else if ("1".equals(string)) {
                                            zzacd = new zzaco(z, z2, this.zzbze.zzcor);
                                        } else if ("3".equals(string)) {
                                            String string2 = jSONObject.getString("custom_template_id");
                                            zzaoj zzaoj2 = new zzaoj();
                                            zzakk.zzcrm.post(new zzaby(this, zzaoj2, string2));
                                            if (zzaoj2.get(zzbzx, TimeUnit.SECONDS) != null) {
                                                zzacd = new zzacp(z);
                                            } else {
                                                String str2 = "No handler for custom template: ";
                                                String valueOf = String.valueOf(jSONObject.getString("custom_template_id"));
                                                zzakb.e(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                                            }
                                        } else {
                                            zzab(0);
                                        }
                                        if (!zznx() && zzacd != null) {
                                            if (jSONObject == null) {
                                                JSONObject jSONObject5 = jSONObject.getJSONObject("tracking_urls_and_actions");
                                                JSONArray optJSONArray2 = jSONObject5.optJSONArray("impression_tracking_urls");
                                                if (optJSONArray2 == null) {
                                                    objArr = null;
                                                } else {
                                                    objArr = new String[optJSONArray2.length()];
                                                    for (int i = 0; i < optJSONArray2.length(); i++) {
                                                        objArr[i] = optJSONArray2.getString(i);
                                                    }
                                                }
                                                this.zzcab = objArr == null ? null : Arrays.asList(objArr);
                                                this.zzcac = jSONObject5.optJSONObject("active_view");
                                                this.zzcad = jSONObject.optString("debug_signals");
                                                this.zzcae = jSONObject.optString("omid_settings");
                                                zzpb = zzacd.zza(this, jSONObject);
                                                zzpd zzpd = new zzpd(this.mContext, this.zzbzz, this.zzaad, this.zzbjc, jSONObject, zzpb, this.zzbze.zzcgs.zzacr, uuid);
                                                zzpb.zzb(zzpd);
                                                if (zzpb instanceof zzos) {
                                                    this.zzaad.zza("/nativeAdCustomClick", (zzv<? super T>) new zzabz<Object>(this, (zzos) zzpb));
                                                }
                                                zzajh zza = zza(zzpb, optBoolean);
                                                this.zzbzz.zzg(zzb(zzanz));
                                                return zza;
                                            }
                                        }
                                        zzpb = null;
                                        if (zzpb instanceof zzos) {
                                        }
                                        zzajh zza2 = zza(zzpb, optBoolean);
                                        this.zzbzz.zzg(zzb(zzanz));
                                        return zza2;
                                    }
                                }
                                zzacd = null;
                                if (jSONObject == null) {
                                }
                            }
                        }
                    }
                    zzanz = zzano.zzi(null);
                    if (!zznx()) {
                    }
                    zzacd = null;
                    if (jSONObject == null) {
                    }
                }
            }
            jSONObject = null;
            optBoolean = jSONObject.optBoolean("enable_omid");
            if (optBoolean) {
            }
            zzanz = zzano.zzi(null);
            if (!zznx()) {
            }
            zzacd = null;
            if (jSONObject == null) {
            }
        } catch (InterruptedException | CancellationException | ExecutionException | JSONException e) {
            e = e;
            str = "Malformed native JSON response.";
            zzakb.zzc(str, e);
            if (!this.zzcaa) {
                zzab(0);
            }
            return zza((zzpb) null, false);
        } catch (TimeoutException e2) {
            e = e2;
            str = "Timeout when loading native ad.";
            zzakb.zzc(str, e);
            if (!this.zzcaa) {
            }
            return zza((zzpb) null, false);
        } catch (Exception e3) {
            e = e3;
            str = "Error occured while doing native ads initialization.";
            zzakb.zzc(str, e);
            if (!this.zzcaa) {
            }
            return zza((zzpb) null, false);
        }
    }

    private final boolean zznx() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzcaa;
        }
        return z;
    }

    public final zzanz<zzon> zza(JSONObject jSONObject, String str, boolean z, boolean z2) throws JSONException {
        JSONObject jSONObject2 = z ? jSONObject.getJSONObject(str) : jSONObject.optJSONObject(str);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, z, z2);
    }

    public final List<zzanz<zzon>> zza(JSONObject jSONObject, String str, boolean z, boolean z2, boolean z3) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() == 0) {
            zzd(0, false);
            return arrayList;
        }
        int length = z3 ? optJSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            if (jSONObject2 == null) {
                jSONObject2 = new JSONObject();
            }
            arrayList.add(zza(jSONObject2, false, z2));
        }
        return arrayList;
    }

    public final Future<zzon> zza(JSONObject jSONObject, String str, boolean z) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        boolean optBoolean = jSONObject2.optBoolean("require", true);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, optBoolean, z);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zza(zzaoj zzaoj, String str) {
        try {
            zzbv.zzel();
            zzaqw zza = zzarc.zza(this.mContext, zzasi.zzvq(), "native-omid", false, false, this.zzbjc, this.zzbze.zzcgs.zzacr, this.zzvr, null, this.zzbzz.zzbi(), this.zzbze.zzcoq);
            zza.zzuf().zza((zzasd) new zzabx(zzaoj, zza));
            zza.loadData(str, "text/html", "UTF-8");
        } catch (Exception e) {
            zzaoj.set(null);
            zzane.zzc("", e);
        }
    }

    public final zzanz<zzaqw> zzc(JSONObject jSONObject, String str) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return zzano.zzi(null);
        }
        if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
            zzakb.zzdk("Required field 'vast_xml' is missing");
            return zzano.zzi(null);
        }
        zzace zzace = new zzace(this.mContext, this.zzbjc, this.zzbze, this.zzvr, this.zzbzz);
        zzaoj zzaoj = new zzaoj();
        zzaoe.zzcvy.execute(new zzacf(zzace, optJSONObject, zzaoj));
        return zzaoj;
    }

    public final void zzd(int i, boolean z) {
        if (z) {
            zzab(i);
        }
    }

    public final zzanz<zzoj> zzg(JSONObject jSONObject) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject("attribution");
        if (optJSONObject == null) {
            return zzano.zzi(null);
        }
        String optString = optJSONObject.optString("text");
        int optInt = optJSONObject.optInt("text_size", -1);
        Integer zzb = zzb(optJSONObject, "text_color");
        Integer zzb2 = zzb(optJSONObject, "bg_color");
        int optInt2 = optJSONObject.optInt("animation_ms", 1000);
        int optInt3 = optJSONObject.optInt("presentation_ms", 4000);
        int i = (this.zzbze.zzcgs.zzadj == null || this.zzbze.zzcgs.zzadj.versionCode < 2) ? 1 : this.zzbze.zzcgs.zzadj.zzbjq;
        boolean optBoolean = optJSONObject.optBoolean("allow_pub_rendering");
        List<zzanz> arrayList = new ArrayList<>();
        if (optJSONObject.optJSONArray("images") != null) {
            arrayList = zza(optJSONObject, "images", false, false, true);
        } else {
            arrayList.add(zza(optJSONObject, "image", false, false));
        }
        zzaoj zzaoj = new zzaoj();
        int size = arrayList.size();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for (zzanz zza : arrayList) {
            List list = arrayList;
            zza.zza(new zzacc(atomicInteger, size, zzaoj, arrayList), zzaki.zzcrj);
            arrayList = list;
        }
        zzaoj zzaoj2 = zzaoj;
        zzaca zzaca = new zzaca(this, optString, zzb2, zzb, optInt, optInt3, optInt2, i, optBoolean);
        return zzano.zza((zzanz<A>) zzaoj2, (zzank<A, B>) zzaca, (Executor) zzaki.zzcrj);
    }
}
