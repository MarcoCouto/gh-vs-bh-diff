package com.google.firebase.remoteconfig;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.config.zzal;
import com.google.android.gms.internal.config.zzam;
import com.google.android.gms.internal.config.zzan;
import com.google.android.gms.internal.config.zzao;
import com.google.android.gms.internal.config.zzap;
import com.google.android.gms.internal.config.zzaq;
import com.google.android.gms.internal.config.zzar;
import com.google.android.gms.internal.config.zzas;
import com.google.android.gms.internal.config.zzat;
import com.google.android.gms.internal.config.zzau;
import com.google.android.gms.internal.config.zzav;
import com.google.android.gms.internal.config.zzaw;
import com.google.android.gms.internal.config.zzax;
import com.google.android.gms.internal.config.zzay;
import com.google.android.gms.internal.config.zze;
import com.google.android.gms.internal.config.zzj;
import com.google.android.gms.internal.config.zzk;
import com.google.android.gms.internal.config.zzv;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApp;
import com.google.firebase.abt.FirebaseABTesting;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings.Builder;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.annotation.concurrent.GuardedBy;

public class FirebaseRemoteConfig {
    public static final boolean DEFAULT_VALUE_FOR_BOOLEAN = false;
    public static final byte[] DEFAULT_VALUE_FOR_BYTE_ARRAY = new byte[0];
    public static final double DEFAULT_VALUE_FOR_DOUBLE = 0.0d;
    public static final long DEFAULT_VALUE_FOR_LONG = 0;
    public static final String DEFAULT_VALUE_FOR_STRING = "";
    public static final int LAST_FETCH_STATUS_FAILURE = 1;
    public static final int LAST_FETCH_STATUS_NO_FETCH_YET = 0;
    public static final int LAST_FETCH_STATUS_SUCCESS = -1;
    public static final int LAST_FETCH_STATUS_THROTTLED = 2;
    public static final int VALUE_SOURCE_DEFAULT = 1;
    public static final int VALUE_SOURCE_REMOTE = 2;
    public static final int VALUE_SOURCE_STATIC = 0;
    @GuardedBy("FirebaseRemoteConfig.class")
    private static FirebaseRemoteConfig zzaf;
    private final Context mContext;
    private zzao zzag;
    private zzao zzah;
    private zzao zzai;
    private zzar zzaj;
    private final FirebaseApp zzak;
    private final ReadWriteLock zzal = new ReentrantReadWriteLock(true);
    private final FirebaseABTesting zzam;

    private FirebaseRemoteConfig(Context context, @Nullable zzao zzao, @Nullable zzao zzao2, @Nullable zzao zzao3, zzar zzar) {
        this.mContext = context;
        this.zzaj = zzar;
        this.zzaj.zzc(zzd(this.mContext));
        this.zzag = zzao;
        this.zzah = zzao2;
        this.zzai = zzao3;
        this.zzak = FirebaseApp.initializeApp(this.mContext);
        this.zzam = zzf(this.mContext);
    }

    public static FirebaseRemoteConfig getInstance() {
        return zzc(FirebaseApp.getInstance().getApplicationContext());
    }

    private static zzao zza(zzas zzas) {
        zzav[] zzavArr;
        zzat[] zzatArr;
        if (zzas == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (zzav zzav : zzas.zzbg) {
            String str = zzav.namespace;
            HashMap hashMap2 = new HashMap();
            for (zzat zzat : zzav.zzbp) {
                hashMap2.put(zzat.zzbj, zzat.zzbk);
            }
            hashMap.put(str, hashMap2);
        }
        byte[][] bArr = zzas.zzbh;
        ArrayList arrayList = new ArrayList();
        for (byte[] add : bArr) {
            arrayList.add(add);
        }
        return new zzao(hashMap, zzas.timestamp, arrayList);
    }

    /* JADX INFO: finally extract failed */
    @VisibleForTesting
    private final Task<Void> zza(long j, zzv zzv) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.zzal.readLock().lock();
        try {
            zzj zzj = new zzj();
            zzj.zza(j);
            if (this.zzak != null) {
                zzj.zza(this.zzak.getOptions().getApplicationId());
            }
            if (this.zzaj.isDeveloperModeEnabled()) {
                zzj.zza("_rcn_developer", "true");
            }
            zzj.zza(10300);
            int i = Integer.MAX_VALUE;
            if (!(this.zzah == null || this.zzah.getTimestamp() == -1)) {
                long convert = TimeUnit.SECONDS.convert(System.currentTimeMillis() - this.zzah.getTimestamp(), TimeUnit.MILLISECONDS);
                zzj.zzc(convert < 2147483647L ? (int) convert : Integer.MAX_VALUE);
            }
            if (!(this.zzag == null || this.zzag.getTimestamp() == -1)) {
                long convert2 = TimeUnit.SECONDS.convert(System.currentTimeMillis() - this.zzag.getTimestamp(), TimeUnit.MILLISECONDS);
                if (convert2 < 2147483647L) {
                    i = (int) convert2;
                }
                zzj.zzb(i);
            }
            zze.zze.zza(zzv.asGoogleApiClient(), zzj.zzf()).setResultCallback(new zza(this, taskCompletionSource));
            this.zzal.readLock().unlock();
            return taskCompletionSource.getTask();
        } catch (Throwable th) {
            this.zzal.readLock().unlock();
            throw th;
        }
    }

    private final void zza(TaskCompletionSource<Void> taskCompletionSource, Status status) {
        if (status == null) {
            Log.w("FirebaseRemoteConfig", "Received null IPC status for failure.");
        } else {
            int statusCode = status.getStatusCode();
            String statusMessage = status.getStatusMessage();
            StringBuilder sb = new StringBuilder(25 + String.valueOf(statusMessage).length());
            sb.append("IPC failure: ");
            sb.append(statusCode);
            sb.append(":");
            sb.append(statusMessage);
            Log.w("FirebaseRemoteConfig", sb.toString());
        }
        this.zzal.writeLock().lock();
        try {
            this.zzaj.zzf(1);
            taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
            zzn();
        } finally {
            this.zzal.writeLock().unlock();
        }
    }

    private static void zza(Runnable runnable) {
        AsyncTask.SERIAL_EXECUTOR.execute(runnable);
    }

    private final void zza(Map<String, Object> map, String str, boolean z) {
        zzao zzao;
        long currentTimeMillis;
        String bool;
        byte[] bArr;
        if (str != null) {
            boolean z2 = map == null || map.isEmpty();
            HashMap hashMap = new HashMap();
            if (!z2) {
                for (String str2 : map.keySet()) {
                    Object obj = map.get(str2);
                    if (obj instanceof String) {
                        bool = (String) obj;
                    } else if (obj instanceof Long) {
                        bool = ((Long) obj).toString();
                    } else if (obj instanceof Integer) {
                        bool = ((Integer) obj).toString();
                    } else if (obj instanceof Double) {
                        bool = ((Double) obj).toString();
                    } else if (obj instanceof Float) {
                        bool = ((Float) obj).toString();
                    } else if (obj instanceof byte[]) {
                        bArr = (byte[]) obj;
                        hashMap.put(str2, bArr);
                    } else if (obj instanceof Boolean) {
                        bool = ((Boolean) obj).toString();
                    } else {
                        throw new IllegalArgumentException("The type of a default value needs to beone of String, Long, Double, Boolean, or byte[].");
                    }
                    bArr = bool.getBytes(zzaq.UTF_8);
                    hashMap.put(str2, bArr);
                }
            }
            this.zzal.writeLock().lock();
            if (z2) {
                try {
                    if (this.zzai != null) {
                        if (this.zzai.zzb(str)) {
                            this.zzai.zza(null, str);
                            zzao = this.zzai;
                            currentTimeMillis = System.currentTimeMillis();
                        }
                    }
                    this.zzal.writeLock().unlock();
                } catch (Throwable th) {
                    this.zzal.writeLock().unlock();
                    throw th;
                }
            } else {
                if (this.zzai == null) {
                    this.zzai = new zzao(new HashMap(), System.currentTimeMillis(), null);
                }
                this.zzai.zza(hashMap, str);
                zzao = this.zzai;
                currentTimeMillis = System.currentTimeMillis();
            }
            zzao.setTimestamp(currentTimeMillis);
            if (z) {
                this.zzaj.zzc(str);
            }
            zzn();
            this.zzal.writeLock().unlock();
        }
    }

    /* JADX WARNING: type inference failed for: r4v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    private static FirebaseRemoteConfig zzc(Context context) {
        FirebaseRemoteConfig firebaseRemoteConfig;
        zzar zzar;
        zzao zzao;
        zzao zzao2;
        zzao zzao3;
        zzar zzar2;
        synchronized (FirebaseRemoteConfig.class) {
            if (zzaf == null) {
                zzar zzar3 = new zzar();
                zzaw zze = zze(context);
                ? r4 = 0;
                if (zze == null) {
                    if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                        Log.d("FirebaseRemoteConfig", "No persisted config was found. Initializing from scratch.");
                    }
                    zzar = zzar3;
                    zzao3 = null;
                    zzao = null;
                    zzao2 = r4;
                } else {
                    if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                        Log.d("FirebaseRemoteConfig", "Initializing from persisted config.");
                    }
                    zzao zza = zza(zze.zzbq);
                    zzao zza2 = zza(zze.zzbr);
                    zzao = zza(zze.zzbs);
                    zzau zzau = zze.zzbt;
                    if (zzau == null) {
                        zzar2 = r4;
                    } else {
                        zzar zzar4 = new zzar();
                        zzar4.zzf(zzau.zzbl);
                        zzar4.zza(zzau.zzbm);
                        zzar4.zzd(zzau.zzbn);
                        zzar2 = zzar4;
                    }
                    if (zzar2 != 0) {
                        zzax[] zzaxArr = zze.zzbu;
                        HashMap hashMap = new HashMap();
                        if (zzaxArr != null) {
                            for (zzax zzax : zzaxArr) {
                                hashMap.put(zzax.namespace, new zzal(zzax.resourceId, zzax.zzbw));
                            }
                        }
                        zzar2.zza((Map<String, zzal>) hashMap);
                    }
                    zzar = zzar2;
                    zzao2 = zza2;
                    zzao3 = zza;
                }
                FirebaseRemoteConfig firebaseRemoteConfig2 = new FirebaseRemoteConfig(context, zzao3, zzao2, zzao, zzar);
                zzaf = firebaseRemoteConfig2;
            }
            firebaseRemoteConfig = zzaf;
        }
        return firebaseRemoteConfig;
    }

    private final long zzd(Context context) {
        try {
            return Wrappers.packageManager(this.mContext).getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
        } catch (NameNotFoundException unused) {
            String packageName = context.getPackageName();
            StringBuilder sb = new StringBuilder(25 + String.valueOf(packageName).length());
            sb.append("Package [");
            sb.append(packageName);
            sb.append("] was not found!");
            Log.e("FirebaseRemoteConfig", sb.toString());
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0053 A[SYNTHETIC, Splitter:B:29:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x006b A[Catch:{ all -> 0x0081 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0074 A[SYNTHETIC, Splitter:B:43:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0084 A[SYNTHETIC, Splitter:B:51:0x0084] */
    private static zzaw zze(Context context) {
        FileInputStream fileInputStream;
        if (context == null) {
            return null;
        }
        try {
            fileInputStream = context.openFileInput("persisted_config");
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                zzay zza = zzay.zza(byteArray, 0, byteArray.length);
                zzaw zzaw = new zzaw();
                zzaw.zza(zza);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                        return zzaw;
                    } catch (IOException e) {
                        Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e);
                    }
                }
                return zzaw;
            } catch (FileNotFoundException e2) {
                e = e2;
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "Persisted config file was not found.", e);
                }
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                        return null;
                    } catch (IOException e3) {
                        Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e3);
                        return null;
                    }
                }
                return null;
            } catch (IOException e4) {
                e = e4;
                try {
                    Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e);
                    if (fileInputStream != null) {
                        try {
                            fileInputStream.close();
                            return null;
                        } catch (IOException e5) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e5);
                            return null;
                        }
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e6) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e6);
                        }
                    }
                    throw th;
                }
            }
        } catch (FileNotFoundException e7) {
            e = e7;
            fileInputStream = null;
            if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
            }
            if (fileInputStream != null) {
            }
            return null;
        } catch (IOException e8) {
            e = e8;
            fileInputStream = null;
            Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e);
            if (fileInputStream != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            throw th;
        }
    }

    private static FirebaseABTesting zzf(Context context) {
        try {
            return new FirebaseABTesting(context, "frc", 1);
        } catch (NoClassDefFoundError unused) {
            Log.w("FirebaseRemoteConfig", "Unable to use ABT: Analytics library is missing.");
            return null;
        }
    }

    private final void zzn() {
        this.zzal.readLock().lock();
        try {
            zzan zzan = new zzan(this.mContext, this.zzag, this.zzah, this.zzai, this.zzaj);
            zza((Runnable) zzan);
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public boolean activateFetched() {
        this.zzal.writeLock().lock();
        try {
            if (this.zzag != null) {
                if (this.zzah == null || this.zzah.getTimestamp() < this.zzag.getTimestamp()) {
                    long timestamp = this.zzag.getTimestamp();
                    this.zzah = this.zzag;
                    this.zzah.setTimestamp(System.currentTimeMillis());
                    this.zzag = new zzao(null, timestamp, null);
                    zza((Runnable) new zzam(this.zzam, this.zzah.zzg()));
                    zzn();
                    this.zzal.writeLock().unlock();
                    return true;
                }
            }
            return false;
        } finally {
            this.zzal.writeLock().unlock();
        }
    }

    public Task<Void> fetch() {
        return fetch(43200);
    }

    public Task<Void> fetch(long j) {
        return zza(j, new zzv(this.mContext));
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, "configns:firebase");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        if (com.google.android.gms.internal.config.zzaq.zzm.matcher(r1).matches() != false) goto L_0x0049;
     */
    public boolean getBoolean(String str, String str2) {
        if (str2 == null) {
            return false;
        }
        this.zzal.readLock().lock();
        try {
            if (this.zzah != null && this.zzah.zzb(str, str2)) {
                String str3 = new String(this.zzah.zzc(str, str2), zzaq.UTF_8);
                if (!zzaq.zzl.matcher(str3).matches()) {
                }
                return true;
            }
            if (this.zzai != null && this.zzai.zzb(str, str2)) {
                String str4 = new String(this.zzai.zzc(str, str2), zzaq.UTF_8);
                if (zzaq.zzl.matcher(str4).matches()) {
                    return true;
                }
                boolean matches = zzaq.zzm.matcher(str4).matches();
            }
            this.zzal.readLock().unlock();
            return false;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public byte[] getByteArray(String str) {
        return getByteArray(str, "configns:firebase");
    }

    public byte[] getByteArray(String str, String str2) {
        byte[] bArr;
        zzao zzao;
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_BYTE_ARRAY;
        }
        this.zzal.readLock().lock();
        try {
            if (this.zzah != null && this.zzah.zzb(str, str2)) {
                zzao = this.zzah;
            } else if (this.zzai == null || !this.zzai.zzb(str, str2)) {
                bArr = DEFAULT_VALUE_FOR_BYTE_ARRAY;
                return bArr;
            } else {
                zzao = this.zzai;
            }
            bArr = zzao.zzc(str, str2);
            return bArr;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public double getDouble(String str) {
        return getDouble(str, "configns:firebase");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:3|4|5|(5:9|10|11|12|13)|14|15|(5:19|20|21|22|23)|24|26) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0039 */
    public double getDouble(String str, String str2) {
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_DOUBLE;
        }
        this.zzal.readLock().lock();
        try {
            if (this.zzah != null && this.zzah.zzb(str, str2)) {
                double doubleValue = Double.valueOf(new String(this.zzah.zzc(str, str2), zzaq.UTF_8)).doubleValue();
                return doubleValue;
            }
            if (this.zzai != null && this.zzai.zzb(str, str2)) {
                try {
                    double doubleValue2 = Double.valueOf(new String(this.zzai.zzc(str, str2), zzaq.UTF_8)).doubleValue();
                    this.zzal.readLock().unlock();
                    return doubleValue2;
                } catch (NumberFormatException unused) {
                }
            }
            this.zzal.readLock().unlock();
            return DEFAULT_VALUE_FOR_DOUBLE;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public FirebaseRemoteConfigInfo getInfo() {
        zzap zzap = new zzap();
        this.zzal.readLock().lock();
        try {
            zzap.zzb(this.zzag == null ? -1 : this.zzag.getTimestamp());
            zzap.zzf(this.zzaj.getLastFetchStatus());
            zzap.setConfigSettings(new Builder().setDeveloperModeEnabled(this.zzaj.isDeveloperModeEnabled()).build());
            return zzap;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public Set<String> getKeysByPrefix(String str) {
        return getKeysByPrefix(str, "configns:firebase");
    }

    public Set<String> getKeysByPrefix(String str, String str2) {
        this.zzal.readLock().lock();
        try {
            TreeSet treeSet = new TreeSet();
            if (this.zzah != null) {
                treeSet.addAll(this.zzah.zzd(str, str2));
            }
            if (this.zzai != null) {
                treeSet.addAll(this.zzai.zzd(str, str2));
            }
            return treeSet;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public long getLong(String str) {
        return getLong(str, "configns:firebase");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:3|4|5|(5:9|10|11|12|13)|14|15|(5:19|20|21|22|23)|24|26) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0039 */
    public long getLong(String str, String str2) {
        if (str2 == null) {
            return 0;
        }
        this.zzal.readLock().lock();
        try {
            if (this.zzah != null && this.zzah.zzb(str, str2)) {
                long longValue = Long.valueOf(new String(this.zzah.zzc(str, str2), zzaq.UTF_8)).longValue();
                return longValue;
            }
            if (this.zzai != null && this.zzai.zzb(str, str2)) {
                try {
                    long longValue2 = Long.valueOf(new String(this.zzai.zzc(str, str2), zzaq.UTF_8)).longValue();
                    this.zzal.readLock().unlock();
                    return longValue2;
                } catch (NumberFormatException unused) {
                }
            }
            this.zzal.readLock().unlock();
            return 0;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public String getString(String str) {
        return getString(str, "configns:firebase");
    }

    public String getString(String str, String str2) {
        String str3;
        if (str2 == null) {
            return "";
        }
        this.zzal.readLock().lock();
        try {
            if (this.zzah != null && this.zzah.zzb(str, str2)) {
                str3 = new String(this.zzah.zzc(str, str2), zzaq.UTF_8);
            } else if (this.zzai == null || !this.zzai.zzb(str, str2)) {
                String str4 = "";
                this.zzal.readLock().unlock();
                return str4;
            } else {
                str3 = new String(this.zzai.zzc(str, str2), zzaq.UTF_8);
            }
            return str3;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public FirebaseRemoteConfigValue getValue(String str) {
        return getValue(str, "configns:firebase");
    }

    public FirebaseRemoteConfigValue getValue(String str, String str2) {
        zzaq zzaq;
        if (str2 == null) {
            return new zzaq(DEFAULT_VALUE_FOR_BYTE_ARRAY, 0);
        }
        this.zzal.readLock().lock();
        try {
            if (this.zzah != null && this.zzah.zzb(str, str2)) {
                zzaq = new zzaq(this.zzah.zzc(str, str2), 2);
            } else if (this.zzai == null || !this.zzai.zzb(str, str2)) {
                zzaq zzaq2 = new zzaq(DEFAULT_VALUE_FOR_BYTE_ARRAY, 0);
                this.zzal.readLock().unlock();
                return zzaq2;
            } else {
                zzaq = new zzaq(this.zzai.zzc(str, str2), 1);
            }
            return zzaq;
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public void setConfigSettings(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.zzal.writeLock().lock();
        try {
            boolean isDeveloperModeEnabled = this.zzaj.isDeveloperModeEnabled();
            boolean isDeveloperModeEnabled2 = firebaseRemoteConfigSettings == null ? false : firebaseRemoteConfigSettings.isDeveloperModeEnabled();
            this.zzaj.zza(isDeveloperModeEnabled2);
            if (isDeveloperModeEnabled != isDeveloperModeEnabled2) {
                zzn();
            }
        } finally {
            this.zzal.writeLock().unlock();
        }
    }

    public void setDefaults(int i) {
        setDefaults(i, "configns:firebase");
    }

    public void setDefaults(int i, String str) {
        if (str == null) {
            if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                Log.d("FirebaseRemoteConfig", "namespace cannot be null for setDefaults.");
            }
            return;
        }
        this.zzal.readLock().lock();
        try {
            if (!(this.zzaj == null || this.zzaj.zzr() == null || this.zzaj.zzr().get(str) == null)) {
                zzal zzal2 = (zzal) this.zzaj.zzr().get(str);
                if (i == zzal2.getResourceId() && this.zzaj.zzs() == zzal2.zzo()) {
                    if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                        Log.d("FirebaseRemoteConfig", "Skipped setting defaults from resource file as this resource file was already applied.");
                    }
                    return;
                }
            }
            this.zzal.readLock().unlock();
            HashMap hashMap = new HashMap();
            try {
                XmlResourceParser xml = this.mContext.getResources().getXml(i);
                Object obj = null;
                Object obj2 = null;
                Object obj3 = null;
                for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                    if (eventType == 2) {
                        obj = xml.getName();
                    } else if (eventType == 3) {
                        if (!(!"entry".equals(xml.getName()) || obj2 == null || obj3 == null)) {
                            hashMap.put(obj2, obj3);
                            obj2 = null;
                            obj3 = null;
                        }
                        obj = null;
                    } else if (eventType == 4) {
                        if ("key".equals(obj)) {
                            obj2 = xml.getText();
                        } else if (Param.VALUE.equals(obj)) {
                            obj3 = xml.getText();
                        }
                    }
                }
                this.zzaj.zza(str, new zzal(i, this.zzaj.zzs()));
                zza(hashMap, str, false);
            } catch (Exception e) {
                Log.e("FirebaseRemoteConfig", "Caught exception while parsing XML resource. Skipping setDefaults.", e);
            }
        } finally {
            this.zzal.readLock().unlock();
        }
    }

    public void setDefaults(Map<String, Object> map) {
        setDefaults(map, "configns:firebase");
    }

    public void setDefaults(Map<String, Object> map, String str) {
        zza(map, str, true);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0051, code lost:
        zza(r11, r12);
     */
    @VisibleForTesting
    public final void zza(TaskCompletionSource<Void> taskCompletionSource, zzk zzk) {
        Status status;
        if (zzk == null || zzk.getStatus() == null) {
            zza(taskCompletionSource, (Status) null);
            return;
        }
        int statusCode = zzk.getStatus().getStatusCode();
        this.zzal.writeLock().lock();
        if (statusCode != -6508) {
            if (statusCode != 6507) {
                switch (statusCode) {
                    case -6506:
                        break;
                    case -6505:
                        Map zzh = zzk.zzh();
                        HashMap hashMap = new HashMap();
                        for (String str : zzh.keySet()) {
                            HashMap hashMap2 = new HashMap();
                            for (String str2 : (Set) zzh.get(str)) {
                                hashMap2.put(str2, zzk.zza(str2, null, str));
                            }
                            hashMap.put(str, hashMap2);
                        }
                        this.zzag = new zzao(hashMap, System.currentTimeMillis(), zzk.zzg());
                        this.zzaj.zzf(-1);
                        taskCompletionSource.setResult(null);
                        break;
                    default:
                        switch (statusCode) {
                            case 6500:
                            case 6501:
                            case 6503:
                            case 6504:
                                status = zzk.getStatus();
                                break;
                            case 6502:
                                break;
                            default:
                                try {
                                    if (zzk.getStatus().isSuccess()) {
                                        StringBuilder sb = new StringBuilder(45);
                                        sb.append("Unknown (successful) status code: ");
                                        sb.append(statusCode);
                                        Log.w("FirebaseRemoteConfig", sb.toString());
                                    }
                                    status = zzk.getStatus();
                                    break;
                                } catch (Throwable th) {
                                    this.zzal.writeLock().unlock();
                                    throw th;
                                }
                        }
                }
            }
            this.zzaj.zzf(2);
            taskCompletionSource.setException(new FirebaseRemoteConfigFetchThrottledException(zzk.getThrottleEndTimeMillis()));
            zzn();
            this.zzal.writeLock().unlock();
        }
        this.zzaj.zzf(-1);
        if (this.zzag != null && !this.zzag.zzq()) {
            Map zzh2 = zzk.zzh();
            HashMap hashMap3 = new HashMap();
            for (String str3 : zzh2.keySet()) {
                HashMap hashMap4 = new HashMap();
                for (String str4 : (Set) zzh2.get(str3)) {
                    hashMap4.put(str4, zzk.zza(str4, null, str3));
                }
                hashMap3.put(str3, hashMap4);
            }
            this.zzag = new zzao(hashMap3, this.zzag.getTimestamp(), zzk.zzg());
        }
        taskCompletionSource.setResult(null);
        zzn();
        this.zzal.writeLock().unlock();
    }
}
