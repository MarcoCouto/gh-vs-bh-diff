package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import java.util.ArrayDeque;
import java.util.Queue;
import javax.annotation.concurrent.GuardedBy;

public final class zzan {
    private static zzan zzcl;
    @GuardedBy("serviceClassNames")
    private final SimpleArrayMap<String, String> zzcm = new SimpleArrayMap<>();
    private Boolean zzcn = null;
    @VisibleForTesting
    final Queue<Intent> zzco = new ArrayDeque();
    @VisibleForTesting
    private final Queue<Intent> zzcp = new ArrayDeque();

    private zzan() {
    }

    public static PendingIntent zza(Context context, int i, Intent intent, int i2) {
        Intent intent2 = new Intent(context, FirebaseInstanceIdReceiver.class);
        intent2.setAction("com.google.firebase.MESSAGING_EVENT");
        intent2.putExtra("wrapped_intent", intent);
        return PendingIntent.getBroadcast(context, i, intent2, ErrorDialogData.SUPPRESSED);
    }

    public static synchronized zzan zzad() {
        zzan zzan;
        synchronized (zzan.class) {
            if (zzcl == null) {
                zzcl = new zzan();
            }
            zzan = zzcl;
        }
        return zzan;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00dc A[Catch:{ SecurityException -> 0x013a, IllegalStateException -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00f3 A[Catch:{ SecurityException -> 0x013a, IllegalStateException -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00f8 A[Catch:{ SecurityException -> 0x013a, IllegalStateException -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0105 A[Catch:{ SecurityException -> 0x013a, IllegalStateException -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x010f  */
    private final int zzb(Context context, Intent intent) {
        String str;
        ComponentName componentName;
        synchronized (this.zzcm) {
            str = (String) this.zzcm.get(intent.getAction());
        }
        boolean z = false;
        if (str == null) {
            ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
            if (resolveService == null || resolveService.serviceInfo == null) {
                Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
                if (this.zzcn == null) {
                    if (context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0) {
                        z = true;
                    }
                    this.zzcn = Boolean.valueOf(z);
                }
                if (this.zzcn.booleanValue()) {
                    componentName = WakefulBroadcastReceiver.startWakefulService(context, intent);
                } else {
                    componentName = context.startService(intent);
                    Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
                }
                if (componentName != null) {
                    return -1;
                }
                Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
                return 404;
            }
            ServiceInfo serviceInfo = resolveService.serviceInfo;
            if (!context.getPackageName().equals(serviceInfo.packageName) || serviceInfo.name == null) {
                String str2 = serviceInfo.packageName;
                String str3 = serviceInfo.name;
                StringBuilder sb = new StringBuilder(94 + String.valueOf(str2).length() + String.valueOf(str3).length());
                sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
                sb.append(str2);
                sb.append("/");
                sb.append(str3);
                Log.e("FirebaseInstanceId", sb.toString());
                if (this.zzcn == null) {
                }
                if (this.zzcn.booleanValue()) {
                }
                if (componentName != null) {
                }
            } else {
                str = serviceInfo.name;
                if (str.startsWith(".")) {
                    String valueOf = String.valueOf(context.getPackageName());
                    String valueOf2 = String.valueOf(str);
                    str = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                }
                synchronized (this.zzcm) {
                    this.zzcm.put(intent.getAction(), str);
                }
            }
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String str4 = "FirebaseInstanceId";
            String str5 = "Restricting intent to a specific service: ";
            String valueOf3 = String.valueOf(str);
            Log.d(str4, valueOf3.length() != 0 ? str5.concat(valueOf3) : new String(str5));
        }
        intent.setClassName(context.getPackageName(), str);
        try {
            if (this.zzcn == null) {
            }
            if (this.zzcn.booleanValue()) {
            }
            if (componentName != null) {
            }
        } catch (SecurityException e) {
            Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e);
            return 401;
        } catch (IllegalStateException e2) {
            String valueOf4 = String.valueOf(e2);
            StringBuilder sb2 = new StringBuilder(45 + String.valueOf(valueOf4).length());
            sb2.append("Failed to start service while in background: ");
            sb2.append(valueOf4);
            Log.e("FirebaseInstanceId", sb2.toString());
            return 402;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d  */
    public final int zza(Context context, String str, Intent intent) {
        char c;
        Queue<Intent> queue;
        int hashCode = str.hashCode();
        if (hashCode != -842411455) {
            if (hashCode == 41532704 && str.equals("com.google.firebase.MESSAGING_EVENT")) {
                c = 1;
                switch (c) {
                    case 0:
                        queue = this.zzco;
                        break;
                    case 1:
                        queue = this.zzcp;
                        break;
                    default:
                        String str2 = "FirebaseInstanceId";
                        String str3 = "Unknown service action: ";
                        String valueOf = String.valueOf(str);
                        Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                        return 500;
                }
                queue.offer(intent);
                Intent intent2 = new Intent(str);
                intent2.setPackage(context.getPackageName());
                return zzb(context, intent2);
            }
        } else if (str.equals("com.google.firebase.INSTANCE_ID_EVENT")) {
            c = 0;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
            }
            queue.offer(intent);
            Intent intent22 = new Intent(str);
            intent22.setPackage(context.getPackageName());
            return zzb(context, intent22);
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
        queue.offer(intent);
        Intent intent222 = new Intent(str);
        intent222.setPackage(context.getPackageName());
        return zzb(context, intent222);
    }

    public final Intent zzae() {
        return (Intent) this.zzcp.poll();
    }
}
