package com.google.firebase.abt;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.internal.firebase_abt.zzi;
import com.google.android.gms.internal.firebase_abt.zzj;
import com.google.android.gms.internal.firebase_abt.zzn;
import com.google.android.gms.internal.firebase_abt.zzo;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurement.ConditionalUserProperty;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@KeepForSdk
public class FirebaseABTesting {
    private AppMeasurement zza;
    private String zzb;
    private int zzc;
    private long zzd;
    private SharedPreferences zze;
    private String zzf;
    @Nullable
    private Integer zzg = null;

    @KeepForSdk
    public FirebaseABTesting(Context context, String str, int i) throws NoClassDefFoundError {
        this.zza = AppMeasurement.getInstance(context);
        this.zzb = str;
        this.zzc = i;
        this.zze = context.getSharedPreferences("com.google.firebase.abt", 0);
        this.zzf = String.format("%s_lastKnownExperimentStartTime", new Object[]{str});
        this.zzd = this.zze.getLong(this.zzf, 0);
    }

    private static zzo zza(byte[] bArr) {
        try {
            return (zzo) zzj.zza(new zzo(), bArr, 0, bArr.length);
        } catch (zzi e) {
            Log.e("FirebaseABTesting", "Payload was not defined or could not be deserialized.", e);
            return null;
        }
    }

    private final void zza() {
        if (this.zze.getLong(this.zzf, 0) != this.zzd) {
            Editor edit = this.zze.edit();
            edit.putLong(this.zzf, this.zzd);
            edit.apply();
        }
    }

    @VisibleForTesting
    private final void zza(String str) {
        this.zza.clearConditionalUserProperty(str, null, null);
    }

    private final void zza(Collection<ConditionalUserProperty> collection) {
        for (ConditionalUserProperty conditionalUserProperty : collection) {
            zza(conditionalUserProperty.mName);
        }
    }

    private final boolean zza(zzo zzo) {
        int i = zzo.zzc;
        int i2 = this.zzc;
        if (i == 0) {
            i = i2 != 0 ? i2 : 1;
        }
        if (i == 1) {
            return true;
        }
        if (Log.isLoggable("FirebaseABTesting", 3)) {
            Log.d("FirebaseABTesting", String.format("Experiment won't be set due to the overflow policy: [%s, %s]", new Object[]{zzo.zzaq, zzo.zzar}));
        }
        return false;
    }

    private final ConditionalUserProperty zzb(zzo zzo) {
        ConditionalUserProperty conditionalUserProperty = new ConditionalUserProperty();
        conditionalUserProperty.mOrigin = this.zzb;
        conditionalUserProperty.mCreationTimestamp = zzo.zzas;
        conditionalUserProperty.mName = zzo.zzaq;
        conditionalUserProperty.mValue = zzo.zzar;
        conditionalUserProperty.mTriggerEventName = TextUtils.isEmpty(zzo.zzat) ? null : zzo.zzat;
        conditionalUserProperty.mTriggerTimeout = zzo.zzau;
        conditionalUserProperty.mTimeToLive = zzo.zzav;
        return conditionalUserProperty;
    }

    @WorkerThread
    private final List<ConditionalUserProperty> zzb() {
        return this.zza.getConditionalUserProperties(this.zzb, "");
    }

    @WorkerThread
    private final int zzc() {
        if (this.zzg == null) {
            this.zzg = Integer.valueOf(this.zza.getMaxUserProperties(this.zzb));
        }
        return this.zzg.intValue();
    }

    @WorkerThread
    @KeepForSdk
    public void addExperiment(byte[] bArr) {
        boolean z;
        zzo zza2 = zza(bArr);
        if (zza2 != null) {
            if (zza2.zzbb == null || zza2.zzbb.length == 0) {
                Log.e("FirebaseABTesting", "The ongoingExperiments field of the payload is not defined.");
            } else {
                zzn[] zznArr = zza2.zzbb;
                HashSet hashSet = new HashSet();
                for (zzn zzn : zznArr) {
                    hashSet.add(zzn.zzaq);
                }
                if (!hashSet.contains(zza2.zzaq)) {
                    Log.e("FirebaseABTesting", String.format("The payload experiment [%s, %s] is not in ongoingExperiments.", new Object[]{zza2.zzaq, zza2.zzar}));
                    return;
                }
                this.zzd = Math.max(this.zzd, zza2.zzas);
                zza();
                ArrayDeque arrayDeque = new ArrayDeque();
                ArrayList arrayList = new ArrayList();
                for (ConditionalUserProperty conditionalUserProperty : zzb()) {
                    if (conditionalUserProperty.mName.equals(zza2.zzaq)) {
                        Log.w("FirebaseABTesting", String.format("The payload experiment [%s, %s] is already set with variant: %s", new Object[]{zza2.zzaq, zza2.zzar, conditionalUserProperty.mValue}));
                        return;
                    } else if (hashSet.contains(conditionalUserProperty.mName)) {
                        arrayDeque.offer(conditionalUserProperty);
                    } else {
                        arrayList.add(conditionalUserProperty);
                    }
                }
                int size = arrayDeque.size();
                int zzc2 = zzc();
                if (size >= zzc2) {
                    if (size > zzc2) {
                        Log.w("FirebaseABTesting", String.format("Max experiment limit exceeded: %d > %d", new Object[]{Integer.valueOf(size), Integer.valueOf(zzc2)}));
                    }
                    z = false;
                } else {
                    z = true;
                }
                if (z || zza(zza2)) {
                    zza((Collection<ConditionalUserProperty>) arrayList);
                    while (arrayDeque.size() >= zzc()) {
                        ConditionalUserProperty conditionalUserProperty2 = (ConditionalUserProperty) arrayDeque.pollFirst();
                        if (Log.isLoggable("FirebaseABTesting", 3)) {
                            Log.d("FirebaseABTesting", String.format("Clearing experiment due to the overflow policy: [%s, %s]", new Object[]{conditionalUserProperty2.mName, conditionalUserProperty2.mValue}));
                        }
                        zza(conditionalUserProperty2.mName);
                    }
                    this.zza.setConditionalUserProperty(zzb(zza2));
                }
            }
        }
    }

    @WorkerThread
    @KeepForSdk
    public void removeAllExperiments() {
        zza((Collection<ConditionalUserProperty>) zzb());
    }

    @WorkerThread
    @KeepForSdk
    public void replaceAllExperiments(List<byte[]> list) {
        String str;
        String str2;
        if (list == null) {
            str = "FirebaseABTesting";
            str2 = "Cannot replace experiments because experimentPayloads is null.";
        } else if (list.isEmpty()) {
            removeAllExperiments();
            return;
        } else {
            ArrayList arrayList = new ArrayList();
            for (byte[] zza2 : list) {
                zzo zza3 = zza(zza2);
                if (zza3 != null) {
                    arrayList.add(zza3);
                }
            }
            if (arrayList.isEmpty()) {
                str = "FirebaseABTesting";
                str2 = "All payloads are either not defined or cannot not be deserialized.";
            } else {
                HashSet hashSet = new HashSet();
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i = 0;
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList2.get(i2);
                    i2++;
                    hashSet.add(((zzo) obj).zzaq);
                }
                List<ConditionalUserProperty> zzb2 = zzb();
                HashSet hashSet2 = new HashSet();
                for (ConditionalUserProperty conditionalUserProperty : zzb2) {
                    hashSet2.add(conditionalUserProperty.mName);
                }
                ArrayList arrayList3 = new ArrayList();
                for (ConditionalUserProperty conditionalUserProperty2 : zzb2) {
                    if (!hashSet.contains(conditionalUserProperty2.mName)) {
                        arrayList3.add(conditionalUserProperty2);
                    }
                }
                zza((Collection<ConditionalUserProperty>) arrayList3);
                ArrayList arrayList4 = new ArrayList();
                int size2 = arrayList2.size();
                int i3 = 0;
                while (i3 < size2) {
                    Object obj2 = arrayList2.get(i3);
                    i3++;
                    zzo zzo = (zzo) obj2;
                    if (!hashSet2.contains(zzo.zzaq)) {
                        boolean z = true;
                        if (zzo.zzas <= this.zzd) {
                            if (Log.isLoggable("FirebaseABTesting", 3)) {
                                Log.d("FirebaseABTesting", String.format("The experiment [%s, %s, %d] is not new since its startTime is before lastKnownStartTime: %d", new Object[]{zzo.zzaq, zzo.zzar, Long.valueOf(zzo.zzas), Long.valueOf(this.zzd)}));
                            }
                            z = false;
                        }
                        if (z) {
                            arrayList4.add(zzo);
                        }
                    }
                }
                ArrayDeque arrayDeque = new ArrayDeque(zzb());
                int zzc2 = zzc();
                ArrayList arrayList5 = arrayList4;
                int size3 = arrayList5.size();
                int i4 = 0;
                while (i4 < size3) {
                    Object obj3 = arrayList5.get(i4);
                    i4++;
                    zzo zzo2 = (zzo) obj3;
                    if (arrayDeque.size() >= zzc2) {
                        if (zza(zzo2)) {
                            while (arrayDeque.size() >= zzc2) {
                                zza(((ConditionalUserProperty) arrayDeque.pollFirst()).mName);
                            }
                        }
                    }
                    ConditionalUserProperty zzb3 = zzb(zzo2);
                    this.zza.setConditionalUserProperty(zzb3);
                    arrayDeque.offer(zzb3);
                }
                int size4 = arrayList2.size();
                while (i < size4) {
                    Object obj4 = arrayList2.get(i);
                    i++;
                    this.zzd = Math.max(this.zzd, ((zzo) obj4).zzas);
                }
                zza();
                return;
            }
        }
        Log.e(str, str2);
    }
}
