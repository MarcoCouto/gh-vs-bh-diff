package com.dlten.lib;

import android.content.Context;
import com.dlten.lib.graphics.CDCView;

public class Common {
    public static int CODE_HEIGHT = 960;
    public static int CODE_WIDTH = 640;
    public static float FPS = 30.0f;
    public static int RES_HEIGHT = 720;
    public static int RES_WIDTH = 480;
    public static int SC_HEIGHT;
    public static int SC_WIDTH;
    private static float m_fDensity;
    private static float m_fScaleCode;
    private static float m_fScaleRes;
    private static int m_nOffsetHeight;

    public static void Finalize() {
    }

    public static void Initialize(Context context, int i, int i2) {
        SC_WIDTH = i;
        SC_HEIGHT = i2;
        float f = (float) i;
        float f2 = (float) i2;
        m_fScaleRes = STD.MIN(f / ((float) RES_WIDTH), f2 / ((float) RES_HEIGHT));
        m_fScaleCode = STD.MIN(f / ((float) CODE_WIDTH), f2 / ((float) CODE_HEIGHT));
        CDCView.setCodeWidth(CODE_WIDTH);
        CDCView.setCodeHeight(CODE_HEIGHT);
        CDCView.setResWidth(RES_WIDTH);
        CDCView.setResHeight(RES_HEIGHT);
        CDCView.setScaleRes(m_fScaleRes);
        CDCView.setScaleCode(m_fScaleCode);
        m_nOffsetHeight = ((int) (((float) RES_HEIGHT) * m_fScaleRes)) - i2;
        m_fDensity = context.getResources().getDisplayMetrics().density;
        StringBuilder sb = new StringBuilder();
        sb.append("Density=");
        sb.append(m_fDensity);
        STD.logout(sb.toString());
    }

    public static float getScaleRes() {
        return m_fScaleRes;
    }

    public static int getDiffHeight() {
        return m_nOffsetHeight;
    }

    public static int getHeightDip() {
        return (int) pixel2dip((float) SC_HEIGHT);
    }

    public static float RES_DIP_HEIGHT() {
        return res2dip((float) RES_HEIGHT);
    }

    public static float RES_DIP_WIDTH() {
        return res2dip((float) RES_WIDTH);
    }

    public static float SC_DIP_HEIGHT() {
        return pixel2dip((float) SC_HEIGHT);
    }

    public static float SC_DIP_WIDTH() {
        return pixel2dip((float) SC_WIDTH);
    }

    public static float code2res(float f) {
        return screen2res(code2screen(f));
    }

    public static float res2code(float f) {
        return screen2code(res2screen(f));
    }

    public static float code2screen(float f) {
        return f * m_fScaleCode;
    }

    public static float screen2code(float f) {
        return f / m_fScaleCode;
    }

    public static float res2screen(float f) {
        return f * m_fScaleRes;
    }

    public static float screen2res(float f) {
        return f / m_fScaleRes;
    }

    public static float pixel2dip(float f) {
        return f / m_fDensity;
    }

    public static float dip2pixel(float f) {
        float f2 = m_fDensity;
        return f2 != 1.0f ? (float) ((int) (((double) (f * f2)) + 0.5d)) : f;
    }

    public static float res2dip(float f) {
        return pixel2dip(res2screen(f));
    }

    public static float dip2res(float f) {
        return screen2res(dip2pixel(f));
    }
}
