package com.dlten.lib;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.frmWork.CWndMgr;
import com.dlten.lib.frmWork.HandleActivity;
import com.dlten.lib.graphics.CBmpDCView;
import java.util.Enumeration;
import java.util.Vector;

public abstract class CBaseView extends CBmpDCView implements Callback {
    public static int CYCLE_TIME = 20;
    public static int CYCLE_TIME2 = 32;
    public static int CYCLE_TIME3 = 64;
    private static final int KEY_EVENT_INTERVAL = 100;
    private static final int LPARAM = 2;
    private static final int MSG = 0;
    private static final int WPARAM = 1;
    private HandleActivity m_activity;
    private boolean m_bCalcFps = false;
    private boolean m_bLogFlag = false;
    private boolean m_bSuspended = true;
    private boolean m_bThreadFlag = false;
    private float m_fCycle = 20.0f;
    private float m_fFPS = 50.0f;
    private float m_fUserCycle = (1000.0f / Common.FPS);
    private SurfaceHolder m_holder;
    private long m_lPrevFrameTime = 0;
    private long m_lUpdateTimes = 0;
    private Vector<Object> m_msgQueue;
    private int m_nCurKey = 0;
    private int m_nDelay;
    private int m_nFrameCount = 0;
    private CWndMgr m_wndMgr;

    /* access modifiers changed from: protected */
    public abstract CWndMgr createWndMgr();

    public CBaseView(Context context) {
        super(context);
        this.m_activity = (HandleActivity) context;
        initCBaseView();
    }

    public CBaseView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.m_activity = (HandleActivity) context;
        initCBaseView();
    }

    public HandleActivity getActivity() {
        return this.m_activity;
    }

    private void initCBaseView() {
        setFocusableInTouchMode(true);
        setFocusable(true);
        this.m_wndMgr = null;
        this.m_msgQueue = new Vector<>(200, 40);
        this.m_holder = getHolder();
        this.m_holder.addCallback(this);
    }

    public void prepareDC() {
        do {
        } while (this.m_bSuspended);
        super.prepareDC();
    }

    public final void start() {
        this.m_bSuspended = true;
        this.m_wndMgr = createWndMgr();
        if (this.m_wndMgr != null) {
            this.m_bThreadFlag = true;
            this.m_wndMgr.start();
            setFPS(1000.0f / Common.FPS);
        }
    }

    public final void stop() {
        if (this.m_wndMgr != null) {
            this.m_bThreadFlag = false;
            this.m_wndMgr.stop();
        }
    }

    public final void suspend() {
        if (this.m_wndMgr != null) {
            this.m_wndMgr.suspend();
            this.m_bSuspended = true;
        }
    }

    public final void resume() {
        if (this.m_wndMgr != null) {
            this.m_wndMgr.resume();
            this.m_bSuspended = false;
        }
    }

    public CWndMgr getWndMgr() {
        return this.m_wndMgr;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.m_bSuspended) {
            return false;
        }
        int codePosX = getCodePosX((int) motionEvent.getX());
        int codePosY = getCodePosY((int) motionEvent.getY());
        switch (motionEvent.getAction()) {
            case 0:
                addMessage(8, codePosX, codePosY);
                break;
            case 1:
                addMessage(9, codePosX, codePosY);
                break;
            case 2:
                if (this.m_wndMgr != null) {
                    this.m_wndMgr.SendMessage(10, codePosX, codePosY);
                    break;
                }
                break;
            case 3:
                addMessage(9, codePosX, codePosY);
                break;
            case 4:
                addMessage(9, codePosX, codePosY);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        StringBuilder sb = new StringBuilder();
        sb.append("onFocusChanged = (");
        sb.append(getWidth());
        sb.append(", ");
        sb.append(getHeight());
        sb.append(")");
        STD.logout(sb.toString());
    }

    public void onSizeChanged(int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder();
        sb.append("onSizeChanged = (");
        sb.append(i);
        sb.append(", ");
        sb.append(i2);
        sb.append(", ");
        sb.append(i3);
        sb.append(", ");
        sb.append(i4);
        STD.logout(sb.toString());
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        super.surfaceChanged(surfaceHolder, i, i2, i3);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        super.surfaceCreated(surfaceHolder);
        resume();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        super.surfaceDestroyed(surfaceHolder);
        suspend();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    public void setEvent(Runnable runnable) {
        super.setEvent(runnable);
    }

    private final void intervalProc() {
        int currentTimeMillis = (int) (System.currentTimeMillis() - this.m_lPrevFrameTime);
        int i = CYCLE_TIME;
        if (currentTimeMillis < 5000) {
            if (currentTimeMillis < 0) {
                currentTimeMillis = 0;
            }
            if (currentTimeMillis < CYCLE_TIME) {
                STD.sleep((long) (CYCLE_TIME - currentTimeMillis));
                currentTimeMillis = CYCLE_TIME;
            } else if (currentTimeMillis < CYCLE_TIME2) {
                STD.sleep((long) (CYCLE_TIME2 - currentTimeMillis));
                currentTimeMillis = CYCLE_TIME2;
            } else if (currentTimeMillis < CYCLE_TIME3) {
                STD.sleep((long) (CYCLE_TIME3 - currentTimeMillis));
                currentTimeMillis = CYCLE_TIME3;
            }
            this.m_fFPS = 1000.0f / ((float) currentTimeMillis);
            if (this.m_fFPS <= 0.0f) {
                this.m_fFPS = 1.0f;
            }
            if (this.m_bLogFlag) {
                StringBuilder sb = new StringBuilder();
                sb.append("FPS = ");
                sb.append(this.m_fFPS);
                STD.logout(sb.toString());
            }
            updateFrameTime();
        }
    }

    /* access modifiers changed from: protected */
    public final void waitFrame(int i, int i2) {
        waitTime(this.m_lPrevFrameTime, i, i2);
    }

    /* access modifiers changed from: protected */
    public final void updateFrameTime() {
        this.m_lPrevFrameTime = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public final void waitTime(long j, int i, int i2) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (currentTimeMillis < 500) {
            long j2 = (long) i;
            if (currentTimeMillis < j2) {
                STD.sleep(j2 - currentTimeMillis);
            } else {
                i = (int) currentTimeMillis;
            }
            if (this.m_bCalcFps) {
                if (this.m_lUpdateTimes >= 10000) {
                    this.m_lUpdateTimes = 0;
                    this.m_nFrameCount = 0;
                }
                if (this.m_bLogFlag) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cycle = ");
                    sb.append(i);
                    STD.logout(sb.toString());
                }
                this.m_lUpdateTimes += (long) i;
                this.m_nFrameCount++;
                this.m_fCycle = ((float) this.m_lUpdateTimes) / ((float) this.m_nFrameCount);
                this.m_fFPS = (1000.0f * ((float) this.m_nFrameCount)) / ((float) this.m_lUpdateTimes);
                if (this.m_fFPS <= 0.0f) {
                    this.m_fFPS = 1.0f;
                }
            }
        }
    }

    public final void inverseLogFlag() {
        this.m_bLogFlag = !this.m_bLogFlag;
    }

    public final void calcFps(boolean z) {
        this.m_bCalcFps = z;
    }

    public final void PostMessage(int i, int i2, int i3) {
        addMessage(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public final void addMessage(int i) {
        addMessage(i, 0, 0);
    }

    /* access modifiers changed from: protected */
    public final void addMessage(int i, int i2, int i3) {
        addMessage(new int[]{i, i2, i3});
    }

    /* access modifiers changed from: protected */
    public final void addMessage(int[] iArr) {
        if (this.m_msgQueue != null) {
            synchronized (this.m_msgQueue) {
                this.m_msgQueue.addElement(iArr);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        com.dlten.lib.STD.ASSERT(false);
        r3 = null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0031 */
    public final void clearMsgQueue() {
        Vector vector = new Vector(5);
        synchronized (this.m_msgQueue) {
            Enumeration elements = this.m_msgQueue.elements();
            while (elements.hasMoreElements()) {
                Object nextElement = elements.nextElement();
                if (nextElement == null) {
                    STD.ASSERT(false);
                } else {
                    try {
                        if (((int[]) nextElement)[0] == 17) {
                            vector.addElement(nextElement);
                            Object obj = elements.nextElement();
                            vector.addElement(obj);
                        } else {
                            continue;
                        }
                    } catch (ClassCastException unused) {
                        STD.ASSERT(false);
                    }
                }
            }
            this.m_msgQueue.removeAllElements();
            Enumeration elements2 = vector.elements();
            while (elements2.hasMoreElements()) {
                this.m_msgQueue.addElement(elements2.nextElement());
            }
        }
    }

    private boolean isExistSameMsg(int i) {
        boolean z;
        synchronized (this.m_msgQueue) {
            Enumeration elements = this.m_msgQueue.elements();
            z = false;
            while (elements.hasMoreElements()) {
                try {
                    int[] iArr = (int[]) elements.nextElement();
                    if (iArr == null) {
                        continue;
                    } else if (iArr[0] == i) {
                        z = true;
                    }
                } catch (ClassCastException unused) {
                }
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        return false;
     */
    private boolean isNextMsg(int i) {
        synchronized (this.m_msgQueue) {
            if (this.m_msgQueue.size() >= 1 && ((int[]) this.m_msgQueue.elementAt(0))[0] == i) {
                return true;
            }
        }
    }

    public void NotifyNetEvents(int i, int i2, Object obj) {
        synchronized (this.m_msgQueue) {
            this.m_msgQueue.addElement(new int[]{17, i, i2});
            this.m_msgQueue.addElement(obj);
        }
    }

    private Object popFirstMsg() {
        synchronized (this.m_msgQueue) {
            if (this.m_msgQueue.isEmpty()) {
                return null;
            }
            Object elementAt = this.m_msgQueue.elementAt(0);
            this.m_msgQueue.removeElementAt(0);
            return elementAt;
        }
    }

    public void DeleteMsgs(int[] iArr) {
        synchronized (this.m_msgQueue) {
            Vector vector = new Vector(40, 5);
            while (!this.m_msgQueue.isEmpty()) {
                int[] iArr2 = (int[]) this.m_msgQueue.elementAt(0);
                this.m_msgQueue.removeElementAt(0);
                if (!isExistMsg(iArr2[0], iArr)) {
                    vector.addElement(iArr2);
                    if (iArr2[0] == 17 && this.m_msgQueue.size() > 0) {
                        Object elementAt = this.m_msgQueue.elementAt(0);
                        this.m_msgQueue.removeElementAt(0);
                        vector.addElement(elementAt);
                    }
                } else if (iArr2[0] == 17 && this.m_msgQueue.size() > 0) {
                    this.m_msgQueue.removeElementAt(0);
                }
            }
            while (!vector.isEmpty()) {
                Object elementAt2 = vector.elementAt(0);
                vector.removeElementAt(0);
                this.m_msgQueue.addElement(elementAt2);
            }
        }
    }

    private boolean isExistMsg(int i, int[] iArr) {
        if (iArr == null) {
            return false;
        }
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0098, code lost:
        r8.WindowProc(r0[0], r0[1], r0[2]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a1, code lost:
        r2 = r0[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a5, code lost:
        if (r2 == 12) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a7, code lost:
        switch(r2) {
            case 8: goto L_0x00ab;
            case 9: goto L_0x00ab;
            default: goto L_0x00aa;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ab, code lost:
        DeleteMsgs(new int[]{r0[0]});
     */
    public int RunProc(CEventWnd cEventWnd) {
        while (this.m_bThreadFlag) {
            try {
                if (this.m_bSuspended) {
                    STD.sleep(50);
                    updateFrameTime();
                } else {
                    Object popFirstMsg = popFirstMsg();
                    if (popFirstMsg != null) {
                        int[] iArr = (int[]) popFirstMsg;
                        switch (iArr[0]) {
                            case 2:
                                if (isNextMsg(2)) {
                                    break;
                                }
                            case 8:
                            case 9:
                                if (true != isExistSameMsg(iArr[0])) {
                                    cEventWnd.WindowProc(iArr[0], iArr[1], iArr[2]);
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                if (true != isExistSameMsg(iArr[0])) {
                                    this.m_nCurKey = iArr[1];
                                    this.m_nDelay = 0;
                                    cEventWnd.WindowProc(iArr[0], iArr[1], iArr[2]);
                                    if (this.m_nCurKey == 4096 || this.m_nCurKey == 1024 || this.m_nCurKey == 262144) {
                                        this.m_nCurKey = 0;
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            case 13:
                                if (iArr[1] == this.m_nCurKey) {
                                    this.m_nCurKey = 0;
                                }
                                cEventWnd.WindowProc(iArr[0], iArr[1], iArr[2]);
                                break;
                            case 15:
                                cEventWnd.NotifyToParentEndRun();
                                return iArr[1];
                            case 17:
                                int OnNetEvent = cEventWnd.OnNetEvent(iArr[1], iArr[2], popFirstMsg());
                                if (iArr[1] == 2 && OnNetEvent < 0) {
                                    STD.ASSERT(false);
                                    break;
                                }
                            case 19:
                                break;
                        }
                    } else if (this.m_nCurKey != 0) {
                        if (this.m_nDelay >= 300) {
                            cEventWnd.WindowProc(14, this.m_nCurKey, 0);
                            this.m_nDelay = 300;
                        } else {
                            this.m_nDelay += 100;
                        }
                        STD.sleep(100);
                    }
                    cEventWnd.UpdateWindow();
                    waitFrame((int) this.m_fUserCycle, (int) this.m_fUserCycle);
                    updateFrameTime();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public void setFPS(float f) {
        this.m_fUserCycle = f;
    }

    public float getFPS() {
        return this.m_fFPS;
    }

    private void perFrameProc() {
        waitFrame(16, 20);
    }
}
