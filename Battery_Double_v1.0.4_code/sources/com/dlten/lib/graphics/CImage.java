package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.dlten.lib.file.CResFile;

public class CImage {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_HCENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_VCENTER = 2;
    public static final int ANCHOR_V_FILTER = 7;
    private static CDCView m_dcView;
    protected byte m_byCols = 0;
    protected byte m_byRows = 0;
    protected int m_nCHeight = 0;
    protected int m_nCWidth = 0;
    protected int m_totalHeight = 0;
    protected int m_totalWidth = 0;

    public void drawImage(float f, float f2, float f3, float f4, int i, Matrix matrix) {
    }

    /* access modifiers changed from: protected */
    public void reloadImages() {
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
    }

    public static void Initialize(CDCView cDCView) {
        m_dcView = cDCView;
        CBmpManager.Initialize();
    }

    public static CDCView getDCView() {
        return m_dcView;
    }

    public static float[] getLeftTopPos(float f, float f2, int i) {
        return CBmpManager.getLeftTopPos(f, f2, i);
    }

    public void load(String str) {
        load(str, 1, 1);
    }

    public void load(String str, int i, int i2) {
        load(CResFile.load(str));
        setColRows(i, i2);
    }

    public void load(String str, int i) {
        load(CResFile.load(str), i);
        setColRows(1, 1);
    }

    public void load(int i) {
        load(i, 1, 1);
    }

    public void load(int i, int i2, int i3) {
        load(CResFile.load(i));
        setColRows(i2, i3);
    }

    public void load(Bitmap bitmap) {
        load(bitmap, 1, 1);
    }

    public void load(Bitmap bitmap, int i) {
        int height = (bitmap.getHeight() * i) / 100;
        loadResource(Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() - height, bitmap.getWidth(), height));
        setColRows(1, 1);
    }

    public void load(Bitmap bitmap, int i, int i2) {
        loadResource(bitmap);
        setColRows(i, i2);
    }

    /* access modifiers changed from: protected */
    public void load(byte[] bArr) {
        loadResource(CBmpManager.loadImage(bArr));
    }

    /* access modifiers changed from: protected */
    public void load(byte[] bArr, int i) {
        Bitmap loadImage = CBmpManager.loadImage(bArr);
        int height = (loadImage.getHeight() * i) / 100;
        loadResource(Bitmap.createBitmap(loadImage, 0, loadImage.getHeight() - height, loadImage.getWidth(), height));
    }

    public void unload() {
        unloadResource();
    }

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap bitmap) {
        this.m_totalWidth = bitmap.getWidth();
        this.m_totalHeight = bitmap.getHeight();
        return -1;
    }

    public void setColRows(int i, int i2) {
        this.m_byRows = (byte) i2;
        this.m_byCols = (byte) i;
        if (this.m_byRows <= 0) {
            this.m_byRows = 1;
        }
        if (this.m_byCols <= 0) {
            this.m_byCols = 1;
        }
        calcSize();
        reloadImages();
    }

    /* access modifiers changed from: protected */
    public void calcSize() {
        this.m_nCWidth = this.m_totalWidth / this.m_byCols;
        this.m_nCHeight = this.m_totalHeight / this.m_byRows;
    }

    public int getWidth() {
        return this.m_totalWidth;
    }

    public int getHeight() {
        return this.m_totalHeight;
    }

    public int getCols() {
        return this.m_byCols;
    }

    public int getRows() {
        return this.m_byRows;
    }

    public int getCWidth() {
        return this.m_nCWidth;
    }

    public int getCHeight() {
        return this.m_nCHeight;
    }
}
