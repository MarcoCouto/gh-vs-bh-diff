package com.dlten.lib.graphics;

public class CRect {
    public float height;
    public float left;
    public float top;
    public float width;

    public CRect() {
        this.left = 0.0f;
        this.top = 0.0f;
        this.width = 0.0f;
        this.height = 0.0f;
    }

    public CRect(float f, float f2, float f3, float f4) {
        this.left = f;
        this.top = f2;
        this.width = f3;
        this.height = f4;
    }

    public CRect(CRect cRect) {
        this.left = cRect.left;
        this.width = cRect.width;
        this.top = cRect.top;
        this.height = cRect.height;
    }

    public CRect(CPoint cPoint, CSize cSize) {
        this.left = cPoint.x;
        this.top = cPoint.y;
        this.width = cSize.w;
        this.height = cSize.h;
    }

    public CRect(CPoint cPoint, CPoint cPoint2) {
        this.left = cPoint.x;
        this.top = cPoint.y;
        this.width = cPoint2.x - cPoint.x;
        this.height = cPoint2.y - cPoint.y;
    }

    public float Right() {
        return this.left + this.width;
    }

    public float Bottom() {
        return this.top + this.height;
    }

    public CSize Size() {
        return new CSize(this.width, this.height);
    }

    public CPoint TopLeft() {
        return new CPoint(this.left, this.top);
    }

    public CPoint BottomRight() {
        return new CPoint(this.left + this.width, this.top + this.height);
    }

    public CPoint CenterPoint() {
        return new CPoint(this.left + (this.width / 2.0f), this.top + (this.height / 2.0f));
    }

    public void SwapLeftRight() {
        this.left += this.width;
        this.width = -this.width;
    }

    public boolean IsRectEmpty() {
        if (this.width > 0.0f && this.height > 0.0f) {
            return false;
        }
        return true;
    }

    public boolean IsRectNull() {
        return this.left == 0.0f && this.width == 0.0f && this.top == 0.0f && this.height == 0.0f;
    }

    public boolean PtInRect(CPoint cPoint) {
        if (this.left > cPoint.x || this.left + this.width <= cPoint.x || this.top > cPoint.y || this.top + this.height <= cPoint.y) {
            return false;
        }
        return true;
    }

    public void SetRect(float f, float f2, float f3, float f4) {
        this.left = f;
        this.top = f2;
        this.width = f3;
        this.height = f4;
    }

    public void SetRect(CPoint cPoint, CPoint cPoint2) {
        SetRect(cPoint.x, cPoint.y, cPoint2.x, cPoint2.y);
    }

    public void SetRectEmpty() {
        SetRect(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public void CopyRect(CRect cRect) {
        if (cRect != null) {
            this.left = cRect.left;
            this.top = cRect.top;
            this.width = cRect.width;
            this.height = cRect.height;
        }
    }

    public boolean EqualRect(CRect cRect) {
        return cRect != null && this.left == cRect.left && this.top == cRect.top && this.width == cRect.width && this.height == cRect.height;
    }

    public void InflateRect(float f, float f2) {
        this.left -= f;
        this.top -= f2;
        this.width += f * 2.0f;
        this.height += 2.0f * f2;
    }

    public void InflateRect(CSize cSize) {
        InflateRect(cSize.w, cSize.h);
    }

    public void DeflateRect(float f, float f2) {
        InflateRect(-f, -f2);
    }

    public void DeflateRect(CSize cSize) {
        InflateRect(-cSize.w, -cSize.h);
    }

    public void OffsetRect(float f, float f2) {
        this.left += f;
        this.top += f2;
    }

    public void OffsetRect(CPoint cPoint) {
        OffsetRect(cPoint.x, cPoint.y);
    }

    public void OffsetRect(CSize cSize) {
        OffsetRect(cSize.w, cSize.h);
    }

    public boolean IntersectRect(CRect cRect, CRect cRect2) {
        if (cRect == null || cRect2 == null) {
            return false;
        }
        this.left = Math.max(cRect.left, cRect2.left);
        float min = Math.min(cRect.Right(), cRect2.Right());
        this.width = min - this.left;
        if (this.left > min) {
            SetRectEmpty();
            return false;
        }
        this.top = Math.max(cRect.top, cRect2.top);
        float min2 = Math.min(cRect.Bottom(), cRect2.Bottom());
        this.height = min2 - this.top;
        if (this.top <= min2) {
            return true;
        }
        SetRectEmpty();
        return false;
    }

    public boolean UnionRect(CRect cRect, CRect cRect2) {
        if (cRect == null || cRect2 == null) {
            return false;
        }
        this.left = Math.max(Math.max(cRect.Right(), cRect2.Right()), Math.max(cRect.left, cRect2.left));
        this.width = Math.max(Math.max(cRect.left, cRect2.left), Math.max(cRect.Right(), cRect2.Right())) - this.left;
        this.top = Math.max(Math.max(cRect.Bottom(), cRect2.Bottom()), Math.max(cRect.top, cRect2.top));
        this.height = Math.max(Math.max(cRect.top, cRect2.top), Math.max(cRect.Bottom(), cRect2.Bottom())) - this.top;
        return true;
    }

    public void NormalizeRect() {
        if (this.width < 0.0f) {
            this.left += this.width;
            this.width = -this.width;
        }
        if (this.height < 0.0f) {
            this.top += this.height;
            this.height = -this.height;
        }
    }

    public void InflateRect(CRect cRect) {
        this.left -= cRect.left;
        this.top -= cRect.top;
        this.width += cRect.left + (cRect.width * 2.0f);
        this.height += cRect.top + (2.0f * cRect.height);
    }

    public void InflateRect(float f, float f2, float f3, float f4) {
        this.left -= f;
        this.top -= f2;
        this.width += f + f3;
        this.height += f2 + f4;
    }

    public void DeflateRect(CRect cRect) {
        this.left += cRect.left;
        this.top += cRect.top;
        this.width -= cRect.left + (cRect.width * 2.0f);
        this.height -= cRect.top + (2.0f * cRect.height);
    }

    public void DeflateRect(float f, float f2, float f3, float f4) {
        this.left += f;
        this.top += f2;
        this.width -= f + f3;
        this.height -= f2 + f4;
    }
}
