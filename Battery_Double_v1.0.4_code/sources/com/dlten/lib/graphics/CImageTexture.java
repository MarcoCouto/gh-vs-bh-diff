package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.opengl.GLES10;
import android.opengl.GLUtils;
import com.dlten.lib.STD;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class CImageTexture extends CImage {
    private int SC_RES_HEIGHT;
    private int SC_RES_WIDTH;
    private FloatBuffer mColorBuffer0;
    private FloatBuffer mFVertexBuffer;
    private FloatBuffer mFVertexBuffer0;
    private ShortBuffer mIndexBuffer;
    private FloatBuffer mTexBuffer;
    private ByteBuffer m_cbb0;
    private ByteBuffer m_ibb;
    private CRect m_rectTexture;
    private ByteBuffer m_tbb;
    private int m_textureID;
    private ByteBuffer m_vbb;
    private ByteBuffer m_vbb0;

    private int adjustValue(int i) {
        if (i <= 2) {
            return 2;
        }
        if (i <= 4) {
            return 4;
        }
        if (i <= 8) {
            return 8;
        }
        if (i <= 16) {
            return 16;
        }
        if (i <= 32) {
            return 32;
        }
        if (i <= 64) {
            return 64;
        }
        if (i <= 128) {
            return 128;
        }
        if (i <= 256) {
            return 256;
        }
        if (i <= 512) {
            return 512;
        }
        if (i <= 1024) {
            return 1024;
        }
        return i;
    }

    public CImageTexture() {
        this.m_textureID = -1;
        this.m_rectTexture = new CRect();
        this.m_vbb = ByteBuffer.allocateDirect(48);
        this.m_tbb = ByteBuffer.allocateDirect(32);
        this.m_ibb = ByteBuffer.allocateDirect(8);
        this.m_vbb0 = ByteBuffer.allocateDirect(48);
        this.m_cbb0 = ByteBuffer.allocateDirect(64);
        this.m_textureID = -1;
        CImage.getDCView();
        this.SC_RES_WIDTH = CDCView.getResWidth();
        this.SC_RES_HEIGHT = CDCView.getResHeight();
    }

    public CRect getTextureRect() {
        return this.m_rectTexture;
    }

    public int getTextureID() {
        return this.m_textureID;
    }

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap bitmap) {
        Config config;
        unloadResource();
        super.loadResource(bitmap);
        int adjustValue = adjustValue(this.m_totalWidth);
        int adjustValue2 = adjustValue(this.m_totalHeight);
        if (!(this.m_totalWidth == adjustValue && this.m_totalHeight == adjustValue2)) {
            try {
                if (bitmap.hasAlpha()) {
                    config = Config.ARGB_8888;
                } else {
                    config = Config.RGB_565;
                }
                Bitmap createBitmap = Bitmap.createBitmap(adjustValue, adjustValue2, config);
                new Canvas(createBitmap).drawBitmap(bitmap, 0.0f, 0.0f, null);
                bitmap.recycle();
                bitmap = createBitmap;
            } catch (Exception unused) {
                return -1;
            }
        }
        STD.ASSERT(this.m_textureID == -1);
        int createTexture = createTexture(bitmap);
        this.m_textureID = createTexture;
        initTexture();
        return createTexture;
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
        if (this.m_textureID != -1) {
            deleteTexture(this.m_textureID);
            this.m_textureID = -1;
        }
        super.unloadResource();
    }

    public void drawImage(float f, float f2, float f3, float f4, int i, Matrix matrix) {
        drawImage(this.m_textureID, f, f2, ((float) this.m_totalWidth) * f3, ((float) this.m_totalHeight) * f4, i);
    }

    private int createTexture(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        this.m_rectTexture.left = 0.0f;
        this.m_rectTexture.top = 0.0f;
        this.m_rectTexture.width = ((float) this.m_totalWidth) / ((float) width);
        this.m_rectTexture.height = ((float) this.m_totalHeight) / ((float) height);
        GLES10.glEnable(3553);
        int[] iArr = new int[1];
        GLES10.glGenTextures(1, iArr, 0);
        GLES10.glBindTexture(3553, iArr[0]);
        GLES10.glTexParameterf(3553, 10241, 9729.0f);
        GLES10.glTexParameterf(3553, 10240, 9729.0f);
        GLES10.glTexParameterf(3553, 10242, 33071.0f);
        GLES10.glTexParameterf(3553, 10243, 33071.0f);
        GLUtils.texImage2D(3553, 0, bitmap, 0);
        return iArr[0];
    }

    private void deleteTexture(int i) {
        GLES10.glDeleteTextures(1, new int[]{i}, 0);
    }

    private void initTexture() {
        this.m_vbb.order(ByteOrder.nativeOrder());
        this.m_tbb.order(ByteOrder.nativeOrder());
        this.m_ibb.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer = this.m_vbb.asFloatBuffer();
        this.mTexBuffer = this.m_tbb.asFloatBuffer();
        this.mIndexBuffer = this.m_ibb.asShortBuffer();
        float f = this.m_rectTexture.left;
        float f2 = this.m_rectTexture.top;
        float Right = this.m_rectTexture.Right();
        float Bottom = this.m_rectTexture.Bottom();
        this.mTexBuffer.put(f);
        this.mTexBuffer.put(f2);
        this.mTexBuffer.put(f);
        this.mTexBuffer.put(Bottom);
        this.mTexBuffer.put(Right);
        this.mTexBuffer.put(Bottom);
        this.mTexBuffer.put(Right);
        this.mTexBuffer.put(f2);
        this.mTexBuffer.position(0);
        this.mIndexBuffer.put(0);
        this.mIndexBuffer.put(1);
        this.mIndexBuffer.put(2);
        this.mIndexBuffer.put(3);
        this.mIndexBuffer.position(0);
        this.m_vbb0.order(ByteOrder.nativeOrder());
        this.m_cbb0.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer0 = this.m_vbb0.asFloatBuffer();
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.position(0);
    }

    public void drawImage(int i, float f, float f2, float f3, float f4, int i2) {
        GLES10.glBindTexture(3553, i);
        float f5 = (f / ((float) this.SC_RES_WIDTH)) - 0.5f;
        float f6 = ((f2 / ((float) this.SC_RES_HEIGHT)) - 0.5f) * -1.0f;
        float f7 = ((f + f3) / ((float) this.SC_RES_WIDTH)) - 0.5f;
        float f8 = (((f2 + f4) / ((float) this.SC_RES_HEIGHT)) - 0.5f) * -1.0f;
        this.mFVertexBuffer.put(f5);
        this.mFVertexBuffer.put(f6);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(f5);
        this.mFVertexBuffer.put(f8);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(f7);
        this.mFVertexBuffer.put(f8);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(f7);
        this.mFVertexBuffer.put(f6);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.position(0);
        GLES10.glEnable(3553);
        GLES10.glEnableClientState(32884);
        GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer);
        GLES10.glEnableClientState(32888);
        GLES10.glTexCoordPointer(2, 5126, 0, this.mTexBuffer);
        GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
        if (i2 < 255) {
            float f9 = ((float) i2) / 255.0f;
            this.mColorBuffer0 = this.m_cbb0.asFloatBuffer();
            for (int i3 = 0; i3 < 4; i3++) {
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(1.0f - f9);
            }
            this.mColorBuffer0.position(0);
            GLES10.glEnableClientState(32886);
            GLES10.glColorPointer(4, 5126, 0, this.mColorBuffer0);
            GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
            GLES10.glDisableClientState(32886);
        }
        GLES10.glDisableClientState(32888);
        GLES10.glDisableClientState(32884);
    }
}
