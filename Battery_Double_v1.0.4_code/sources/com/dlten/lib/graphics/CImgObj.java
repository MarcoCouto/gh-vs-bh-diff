package com.dlten.lib.graphics;

import android.graphics.Matrix;
import android.support.v4.view.ViewCompat;
import com.dlten.lib.Common;
import com.dlten.lib.STD;

public class CImgObj {
    private static int DRAW_MODE = 1;
    public static final int MODE_BITMAP = 0;
    public static final int MODE_TEXTURE = 1;
    private CImage m_imgParent;
    private int m_nAlpha;
    private int m_nAnchor;
    private int m_nFilterColor;
    private CRect m_rect = new CRect();
    private int m_rotate;
    private CRect m_rtTemp = new CRect();
    private float m_scaleX;
    private float m_scaleY;
    private boolean m_visible;

    public static void SetMode(int i) {
        DRAW_MODE = i;
    }

    public CImgObj() {
        init();
    }

    public CImgObj(String str) {
        init();
        load(str);
    }

    public CImgObj(String str, int i) {
        init();
        load(str, i);
    }

    private void init() {
        this.m_imgParent = null;
        setAnchor(17);
        setFilterColor(ViewCompat.MEASURED_SIZE_MASK);
        setAlpha(255);
        rotateTo(0);
        setScaleX(1.0f);
        setScaleY(1.0f);
        setVisible(true);
    }

    public void load(String str) {
        CImage cImage;
        if (str == null) {
            STD.logout("CImgObj.load()\tFile name is null!");
            return;
        }
        CRect cRect = new CRect(this.m_rect.left, this.m_rect.top, this.m_rect.width, this.m_rect.height);
        int i = this.m_nAnchor;
        int i2 = this.m_nFilterColor;
        int i3 = this.m_nAlpha;
        float f = this.m_scaleX;
        float f2 = this.m_scaleY;
        int i4 = this.m_rotate;
        boolean z = this.m_visible;
        boolean z2 = false;
        if (this.m_imgParent != null) {
            z2 = true;
        }
        if (DRAW_MODE != 0) {
            cImage = new CImageTexture();
        } else {
            cImage = new CImageBitmap();
        }
        cImage.load(str, 1, 1);
        setParent(cImage);
        if (z2) {
            this.m_rect.left = cRect.left;
            this.m_rect.top = cRect.top;
            this.m_rect.width = cRect.width;
            this.m_rect.height = cRect.height;
            this.m_nAnchor = i;
            this.m_nFilterColor = i2;
            this.m_nAlpha = i3;
            this.m_scaleX = f;
            this.m_scaleY = f2;
            this.m_rotate = i4;
            this.m_visible = z;
        }
    }

    public void load(String str, int i) {
        CImage cImage;
        if (str == null) {
            STD.logout("CImgObj.load()\tFile name is null!");
            return;
        }
        CRect cRect = new CRect(this.m_rect.left, this.m_rect.top, this.m_rect.width, this.m_rect.height);
        int i2 = this.m_nAnchor;
        int i3 = this.m_nFilterColor;
        int i4 = this.m_nAlpha;
        float f = this.m_scaleX;
        float f2 = this.m_scaleY;
        int i5 = this.m_rotate;
        boolean z = this.m_visible;
        boolean z2 = false;
        if (this.m_imgParent != null) {
            z2 = true;
        }
        if (DRAW_MODE != 0) {
            cImage = new CImageTexture();
        } else {
            cImage = new CImageBitmap();
        }
        cImage.load(str, i);
        setParent(cImage);
        if (z2) {
            this.m_rect.left = cRect.left;
            this.m_rect.top = cRect.top;
            this.m_rect.width = cRect.width;
            this.m_rect.height = cRect.height;
            this.m_nAnchor = i2;
            this.m_nFilterColor = i3;
            this.m_nAlpha = i4;
            this.m_scaleX = f;
            this.m_scaleY = f2;
            this.m_rotate = i5;
            this.m_visible = z;
        }
    }

    public void unload() {
        if (this.m_imgParent != null) {
            this.m_imgParent.unload();
            this.m_imgParent = null;
            this.m_rect.SetRectEmpty();
        }
    }

    public void setParent(CImage cImage) {
        setParent(cImage, 0);
    }

    public void setParent(CImage cImage, int i) {
        this.m_imgParent = cImage;
        this.m_rect.SetRectEmpty();
        updateSize();
    }

    private void updateSize() {
        if (this.m_imgParent != null) {
            this.m_rect.width = Common.res2code((float) this.m_imgParent.getCWidth());
            this.m_rect.height = Common.res2code((float) this.m_imgParent.getCHeight());
        }
    }

    private void updateWidth() {
        if (this.m_imgParent != null) {
            this.m_rect.width = Common.res2code((float) this.m_imgParent.getCWidth());
        }
    }

    private void updateHeight() {
        if (this.m_imgParent != null) {
            this.m_rect.height = Common.res2code((float) this.m_imgParent.getCHeight());
        }
    }

    public void draw() {
        drawImpl(this.m_rect.left, this.m_rect.top);
    }

    public void draw(CPoint cPoint) {
        if (cPoint != null) {
            draw(cPoint.x, cPoint.y);
        }
    }

    public void draw(float f, float f2) {
        drawImpl(f, f2);
    }

    public void draw(CPoint cPoint, int i) {
        if (cPoint != null) {
            draw(cPoint.x, cPoint.y, i);
        }
    }

    public void draw(float f, float f2, int i) {
        drawImpl(f, f2, i);
    }

    private void drawImpl(float f, float f2) {
        if (this.m_imgParent != null && getVisible()) {
            float code2screen = Common.code2screen(f);
            float code2screen2 = Common.code2screen(f2);
            this.m_imgParent.drawImage(code2screen, code2screen2, this.m_scaleX, this.m_scaleY, this.m_nAlpha, calcMatrix(code2screen, code2screen2));
        }
    }

    private void drawImpl(float f, float f2, int i) {
        if (this.m_imgParent != null && getVisible()) {
            CImageBitmap cImageBitmap = (CImageBitmap) this.m_imgParent;
            cImageBitmap.changeWithPer(i);
            float code2screen = Common.code2screen(f);
            float code2screen2 = Common.code2screen(f2);
            float height = (float) (this.m_imgParent.getHeight() - cImageBitmap.getHeight());
            cImageBitmap.drawImage(code2screen, code2screen2 + height, this.m_scaleX, this.m_scaleY, this.m_nAlpha, calcMatrix(code2screen, code2screen2));
        }
    }

    private Matrix calcMatrix(float f, float f2) {
        float[] leftTopPos = CImage.getLeftTopPos((float) this.m_imgParent.getCWidth(), (float) this.m_imgParent.getCHeight(), this.m_nAnchor);
        Matrix matrix = new Matrix();
        if (this.m_rotate != 0) {
            matrix.postRotate((float) this.m_rotate, -leftTopPos[0], -leftTopPos[1]);
        }
        if (!(this.m_scaleX == 1.0f && this.m_scaleY == 1.0f)) {
            matrix.postScale(this.m_scaleX, this.m_scaleY, -leftTopPos[0], -leftTopPos[1]);
        }
        matrix.postTranslate(f + leftTopPos[0], f2 + leftTopPos[1]);
        return matrix;
    }

    public void moveTo(float f, float f2) {
        this.m_rect.left = f;
        this.m_rect.top = f2;
    }

    public void moveTo(CPoint cPoint) {
        moveTo(cPoint.x, cPoint.y);
    }

    public void move(float f, float f2) {
        this.m_rect.OffsetRect(f, f2);
    }

    public CPoint getPos() {
        return getRect().TopLeft();
    }

    public CPoint getCenterPos() {
        return getRect().CenterPoint();
    }

    public CRect getRect() {
        this.m_rtTemp.CopyRect(this.m_rect);
        float[] leftTopPos = CImage.getLeftTopPos(this.m_rtTemp.width, this.m_rtTemp.height, this.m_nAnchor);
        this.m_rtTemp.OffsetRect(leftTopPos[0], leftTopPos[1]);
        return this.m_rtTemp;
    }

    public void setAnchor(int i) {
        this.m_nAnchor = i;
    }

    public int getAnchor() {
        return this.m_nAnchor;
    }

    public void setFilterColor(int i) {
        this.m_nFilterColor = i;
    }

    public int getFilterColor() {
        return this.m_nFilterColor;
    }

    public void setAlpha(int i) {
        this.m_nAlpha = i;
    }

    public int getAlpha() {
        return this.m_nAlpha;
    }

    public void rotate(int i) {
        this.m_rotate += i;
        checkImageChange();
    }

    public void rotateTo(int i) {
        this.m_rotate = i;
        checkImageChange();
    }

    public void setScale(float f, float f2) {
        setScaleX(f);
        setScaleY(f2);
    }

    public void setScaleX(float f) {
        updateWidth();
        this.m_scaleX = f;
        this.m_rect.width *= f;
        checkImageChange();
    }

    public void setScaleY(float f) {
        updateHeight();
        this.m_scaleY = f;
        this.m_rect.height *= f;
        checkImageChange();
    }

    public float getScaleX() {
        return this.m_scaleX;
    }

    public float getScaleY() {
        return this.m_scaleY;
    }

    public void setSize(float f, float f2) {
        setSizeX(f);
        setSizeY(f2);
    }

    public void setSizeX(float f) {
        updateWidth();
        this.m_scaleX = f / this.m_rect.width;
        this.m_rect.width = f;
        checkImageChange();
    }

    public void setSizeY(float f) {
        updateHeight();
        this.m_scaleY = f / this.m_rect.height;
        this.m_rect.height = f;
        checkImageChange();
    }

    public float getSizeX() {
        return this.m_rect.width;
    }

    public float getSizeY() {
        return this.m_rect.height;
    }

    public float getWidth() {
        return (float) this.m_imgParent.getWidth();
    }

    public float getHeight() {
        return (float) this.m_imgParent.getHeight();
    }

    private boolean checkImageChange() {
        return (this.m_scaleX == 1.0f && this.m_scaleY == 1.0f && this.m_rotate == 0) ? false : true;
    }

    public boolean getVisible() {
        return this.m_visible;
    }

    public void setVisible(boolean z) {
        this.m_visible = z;
    }
}
