package com.dlten.lib.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import java.io.InputStream;

public class CBmpManager {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_V_FILTER = 7;
    public static final int TRANS_MIRROR = 1;
    public static final int TRANS_MIRROR_ROT180 = 3;
    public static final int TRANS_MIRROR_ROT90 = 4;
    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT180 = 2;
    private static Options m_sBmpOption = new Options();

    public static void Initialize() {
        m_sBmpOption.inPreferredConfig = Config.RGB_565;
    }

    public static Bitmap loadImage(Context context, String str) {
        byte[] bArr;
        try {
            InputStream open = context.getAssets().open(str);
            if (open == null) {
                return null;
            }
            bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return loadImage(bArr);
        } catch (Exception unused) {
            bArr = null;
        }
    }

    public static Bitmap loadImage(Resources resources, int i) {
        return BitmapFactory.decodeResource(resources, i, m_sBmpOption);
    }

    public static Bitmap loadImage(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return loadImage(bArr, 0, bArr.length);
    }

    public static Bitmap loadImage(byte[] bArr, int i, int i2) {
        try {
            return BitmapFactory.decodeByteArray(bArr, i, i2, m_sBmpOption);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] getRGBData(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return getRGBData(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight());
    }

    public static int[] getRGBData(Bitmap bitmap, int i, int i2, int i3, int i4) {
        if (bitmap == null) {
            return null;
        }
        int[] iArr = new int[(i3 * i4)];
        try {
            bitmap.getPixels(iArr, 0, i3, i, i2, i3, i4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return iArr;
    }

    public static float[] getLeftTopPos(float f, float f2, int i) {
        float[] fArr = new float[2];
        int i2 = i & 112;
        if (i2 != 16) {
            if (i2 == 32) {
                fArr[0] = 0.0f - (f / 2.0f);
            } else if (i2 == 64) {
                fArr[0] = 0.0f - f;
            }
        }
        int i3 = i & 7;
        if (i3 != 4) {
            switch (i3) {
                case 2:
                    fArr[1] = 0.0f - (f2 / 2.0f);
                    break;
            }
        } else {
            fArr[1] = 0.0f - f2;
        }
        return fArr;
    }
}
