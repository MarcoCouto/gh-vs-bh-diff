package com.dlten.lib.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.dlten.lib.Common;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;

public abstract class CBmpDCView extends CDCView {
    private static Paint m_paintColor = new Paint();
    private Bitmap bmpBackup = null;
    private Paint m_font;
    private Typeface m_fontStyle;
    private RectF m_rtDst = new RectF();
    private Rect m_rtSrc = new Rect();
    protected Rect m_rtTDst = new Rect();
    protected Rect m_rtTSrc = new Rect();

    public CBmpDCView(Context context) {
        super(context);
        initBmpDCView();
    }

    public CBmpDCView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initBmpDCView();
    }

    private void initBmpDCView() {
        initFont();
        CImgObj.SetMode(0);
    }

    public void prepareDC() {
        super.prepareDC();
        clear(0);
        m_paintColor.setAntiAlias(false);
        m_paintColor.setDither(true);
    }

    public synchronized void update(CEventWnd cEventWnd) {
        if (cEventWnd != null) {
            Canvas lockCanvas = this.m_holder.lockCanvas();
            if (lockCanvas == null) {
                STD.ASSERT(false);
                return;
            }
            try {
                clear();
                cEventWnd.DrawPrevProc();
                cEventWnd.DrawProcess();
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
            updateGraphics(lockCanvas);
            this.m_holder.unlockCanvasAndPost(lockCanvas);
        }
    }

    public final void updateGraphics(Canvas canvas) {
        canvas.drawBitmap(this.m_bmpDblBuffer, (float) m_nDblOffsetX, (float) m_nDblOffsetY, m_paintColor);
    }

    public final int[] getRGBData(int i, int i2, int i3, int i4) {
        try {
            return CBmpManager.getRGBData(this.m_bmpDblBuffer, i, i2, i3, i4);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final void captureDisp() {
        this.bmpBackup = Bitmap.createBitmap(this.m_bmpDblBuffer);
    }

    public final void restoreDisp() {
        if (this.bmpBackup != null) {
            this.m_canvas.drawBitmap(this.bmpBackup, 0.0f, 0.0f, null);
        }
    }

    public final void drawImage(Bitmap bitmap, float f, float f2) {
        if (bitmap != null) {
            this.m_canvas.drawBitmap(bitmap, f, f2, m_paintColor);
        }
    }

    public final void drawImage(Bitmap bitmap, Rect rect, RectF rectF) {
        if (bitmap != null) {
            this.m_canvas.drawBitmap(bitmap, rect, rectF, m_paintColor);
        }
    }

    public void drawImage(Bitmap bitmap, Matrix matrix) {
        this.m_canvas.drawBitmap(bitmap, matrix, m_paintColor);
    }

    public final void drawImage(Bitmap bitmap, CRect cRect, CRect cRect2) {
        if (bitmap != null && cRect != null && cRect2 != null) {
            this.m_rtSrc.set((int) cRect.left, (int) cRect.top, (int) (cRect.left + cRect.width), (int) (cRect.top + cRect.height));
            this.m_rtDst.set(cRect2.left, cRect2.top, cRect2.left + cRect2.width, cRect2.top + cRect2.height);
            this.m_canvas.drawBitmap(bitmap, this.m_rtSrc, this.m_rtDst, m_paintColor);
        }
    }

    public void drawString(String str, float f, float f2, int i) {
        Align align;
        int i2;
        if (str != null) {
            int i3 = i & 112;
            if (i3 == 64) {
                align = Align.RIGHT;
            } else if (i3 == 32) {
                align = Align.CENTER;
            } else {
                align = Align.LEFT;
            }
            this.m_font.setTextAlign(align);
            if ((i & 2) != 0) {
                i2 = (-this.FONT_BASELINE) - (this.FONT_H / 2);
            } else if ((i & 4) != 0) {
                i2 = (-this.FONT_BASELINE) - this.FONT_H;
            } else {
                i2 = -this.FONT_BASELINE;
            }
            float f3 = f2 + ((float) i2);
            this.m_font.setColor(m_paintColor.getColor());
            this.m_canvas.drawText(str, f + ((float) m_nWndOffsetX), f3 + ((float) m_nWndOffsetY), this.m_font);
        }
    }

    /* access modifiers changed from: protected */
    public final void setClip(int[] iArr) {
        setClip(iArr[0], iArr[1], iArr[2], iArr[3]);
    }

    /* access modifiers changed from: protected */
    public final void setClip(int i, int i2, int i3, int i4) {
        this.m_canvas.clipRect((float) (m_nWndOffsetX + i), (float) (m_nWndOffsetY + i2), (float) (i + m_nWndOffsetX + i3), (float) (i2 + m_nWndOffsetY + i4), Op.REPLACE);
    }

    /* access modifiers changed from: protected */
    public final void clearClip() {
        this.m_canvas.clipRect((float) m_nWndOffsetX, (float) m_nWndOffsetY, (float) (m_nWndOffsetX + m_drawWidth), (float) (m_nWndOffsetY + m_drawHeight), Op.REPLACE);
    }

    /* access modifiers changed from: protected */
    public final void copyArea(int i, int i2, int i3, int i4, int i5, int i6) {
        copyArea(i, i2, i3, i4, i5, i6, 17);
    }

    /* access modifiers changed from: protected */
    public final void copyArea(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = i3;
        int i9 = i4;
        int i10 = i5 + m_nWndOffsetX;
        int i11 = i6 + m_nWndOffsetY;
        int[] rGBData = getRGBData(m_nWndOffsetX + i, m_nWndOffsetY + i2, i8, i9);
        float[] leftTopPos = CBmpManager.getLeftTopPos((float) i8, (float) i9, i7);
        this.m_canvas.drawBitmap(rGBData, 0, i8, leftTopPos[0] + ((float) i10), ((float) i11) + leftTopPos[1], i8, i9, true, null);
    }

    public final void setLineWidth(float f) {
        m_paintColor.setStrokeWidth(f);
    }

    public final void drawLine(float f, float f2, float f3, float f4) {
        this.m_canvas.drawLine(((float) m_nWndOffsetX) + f, f2 + ((float) m_nWndOffsetY), f3 + ((float) m_nWndOffsetX), f4 + ((float) m_nWndOffsetY), m_paintColor);
    }

    public final void drawRect(int[] iArr) {
        drawRect(iArr[0], iArr[1], iArr[2], iArr[3]);
    }

    public final void drawRect(int i, int i2, int i3, int i4) {
        m_paintColor.setStyle(Style.STROKE);
        m_paintColor.setStrokeWidth(1.0f);
        this.m_canvas.drawRect((float) (m_nWndOffsetX + i), (float) (m_nWndOffsetY + i2), (float) (i + m_nWndOffsetX + i3), (float) (i2 + m_nWndOffsetY + i4), m_paintColor);
    }

    public void fillRect(int i, int i2, int i3, int i4) {
        m_paintColor.setStyle(Style.FILL);
        this.m_canvas.drawRect(Common.code2screen((float) (m_nWndOffsetX + i)), Common.code2screen((float) (m_nWndOffsetY + i2)), Common.code2screen((float) (i + m_nWndOffsetX + i3)), Common.code2screen((float) (i2 + m_nWndOffsetY + i4)), m_paintColor);
    }

    public void setARGB(int i, int i2, int i3, int i4) {
        m_paintColor.setARGB(i, i2, i3, i4);
    }

    public void setAlpha(int i) {
        m_paintColor.setAlpha(i);
    }

    public void setRotate(float f, int i, int i2) {
        this.m_canvas.rotate(f, (float) i, (float) i2);
    }

    private void initFont() {
        this.m_fontStyle = Typeface.create(Typeface.DEFAULT, 0);
        this.m_font = new Paint();
        this.m_font.setTypeface(this.m_fontStyle);
        this.m_font.setTextSize((float) this.m_nFontSize);
        this.m_font.setAntiAlias(false);
        this.m_font.setDither(true);
        this.FONT_BASELINE = (int) this.m_font.getFontMetrics().descent;
        this.FONT_H = (int) (this.m_font.getFontMetrics().top - this.m_font.getFontMetrics().bottom);
        setFont(this.m_font);
        setFontSize(this.m_nFontSize);
    }

    /* access modifiers changed from: protected */
    public final void setFont(Paint paint) {
        this.m_font = paint;
        if (this.m_font != null) {
            this.FONT_H = (int) this.m_font.getTextSize();
            this.FONT_BASELINE = (int) this.m_font.ascent();
        }
    }

    /* access modifiers changed from: protected */
    public final void setFont() {
        this.m_font.setTextSize((float) this.m_nFontSize);
        setFont(this.m_font);
    }

    /* access modifiers changed from: protected */
    public final Paint getFont() {
        return this.m_font;
    }

    /* access modifiers changed from: protected */
    public final int getFontWidth(Paint paint) {
        float[] fArr = new float[1];
        paint.getTextWidths("W", fArr);
        return (int) fArr[0];
    }

    /* access modifiers changed from: protected */
    public final int getStrWidth(String str) {
        if (this.m_font == null || str == null || str.length() <= 0) {
            return 0;
        }
        float f = 0.0f;
        float[] fArr = new float[str.length()];
        this.m_font.getTextWidths(str, fArr);
        for (float f2 : fArr) {
            f += f2;
        }
        return (int) f;
    }
}
