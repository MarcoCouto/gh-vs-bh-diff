package com.dlten.lib.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.graphics.CBmpDCView;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class CMyGLDCView extends CBmpDCView {
    /* access modifiers changed from: private */
    public static final Semaphore sEglSemaphore = new Semaphore(1);
    private GLThread mGLThread;
    /* access modifiers changed from: private */
    public GLWrapper mGLWrapper;
    /* access modifiers changed from: private */
    public boolean mSizeChanged = true;
    private SimpleGLRenderer m_renderer = null;

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start(int[] iArr) {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            this.mEgl.eglChooseConfig(this.mEglDisplay, iArr, eGLConfigArr, 1, new int[1]);
            this.mEglConfig = eGLConfigArr[0];
            this.mEglContext = this.mEgl.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL10.EGL_NO_CONTEXT, null);
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder surfaceHolder) {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = this.mEgl.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, surfaceHolder, null);
            this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
            GL gl = this.mEglContext.getGL();
            return CMyGLDCView.this.mGLWrapper != null ? CMyGLDCView.this.mGLWrapper.wrap(gl) : gl;
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void finish() {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
            if (this.mEglContext != null) {
                this.mEgl.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }

    class GLThread extends Thread {
        private boolean mContextLost;
        private boolean mDone = false;
        private EglHelper mEglHelper;
        private Runnable mEvent;
        private boolean mHasFocus;
        private boolean mHasSurface;
        private int mHeight = 0;
        private boolean mPaused;
        private Renderer mRenderer;
        private int mWidth = 0;

        GLThread(Renderer renderer) {
            this.mRenderer = renderer;
            setName("GLThread");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
            com.dlten.lib.opengl.CMyGLDCView.access$100().release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
            throw r0;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0012 A[ExcHandler:  FINALLY, Splitter:B:0:0x0000] */
        public void run() {
            try {
                CMyGLDCView.sEglSemaphore.acquire();
                guardedRun();
            } catch (InterruptedException unused) {
            } finally {
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0059, code lost:
            if (r5 == false) goto L_0x0062;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x005b, code lost:
            r11.mEglHelper.start(r0);
            r3 = true;
            r7 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0062, code lost:
            if (r7 == false) goto L_0x0073;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
            r2 = (javax.microedition.khronos.opengles.GL10) r11.mEglHelper.createSurface(com.dlten.lib.opengl.CMyGLDCView.access$300(r11.this$0));
            r4 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0073, code lost:
            if (r3 == false) goto L_0x007b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0075, code lost:
            r11.mRenderer.surfaceCreated(r2);
            r3 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x007b, code lost:
            if (r4 == false) goto L_0x0083;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
            r11.mRenderer.sizeChanged(r2, r8, r9);
            r4 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0083, code lost:
            if (r8 <= 0) goto L_0x0018;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0085, code lost:
            if (r9 <= 0) goto L_0x0018;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0087, code lost:
            r11.mRenderer.drawFrame(r2);
            r11.mEglHelper.swap();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0095, code lost:
            if (r2 == null) goto L_0x009c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0097, code lost:
            r11.mRenderer.shutdown(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x009c, code lost:
            r11.mEglHelper.finish();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a1, code lost:
            return;
         */
        private void guardedRun() throws InterruptedException {
            boolean z;
            this.mEglHelper = new EglHelper();
            int[] configSpec = this.mRenderer.getConfigSpec();
            this.mEglHelper.start(configSpec);
            GL10 gl10 = null;
            boolean z2 = true;
            boolean z3 = true;
            while (true) {
                if (this.mDone) {
                    break;
                }
                synchronized (this) {
                    if (this.mEvent != null) {
                        this.mEvent.run();
                    }
                    if (this.mPaused) {
                        this.mEglHelper.finish();
                        z = true;
                    } else {
                        z = false;
                    }
                    if (needToWait()) {
                        while (needToWait()) {
                            wait();
                        }
                    }
                    if (!this.mDone) {
                        boolean access$200 = CMyGLDCView.this.mSizeChanged;
                        int i = this.mWidth;
                        int i2 = this.mHeight;
                        CMyGLDCView.this.mSizeChanged = false;
                    }
                }
            }
            while (true) {
            }
        }

        private boolean needToWait() {
            return (this.mPaused || !this.mHasFocus || !this.mHasSurface || this.mContextLost) && !this.mDone;
        }

        public void surfaceCreated() {
            synchronized (this) {
                this.mHasSurface = true;
                this.mContextLost = false;
                notify();
            }
        }

        public void surfaceDestroyed() {
            synchronized (this) {
                this.mHasSurface = false;
                notify();
            }
        }

        public void onPause() {
            synchronized (this) {
                this.mPaused = true;
            }
        }

        public void onResume() {
            synchronized (this) {
                this.mPaused = false;
                notify();
            }
        }

        public void onWindowFocusChanged(boolean z) {
            synchronized (this) {
                this.mHasFocus = z;
                if (this.mHasFocus) {
                    notify();
                }
            }
        }

        public void onWindowResize(int i, int i2) {
            synchronized (this) {
                this.mWidth = i;
                this.mHeight = i2;
                CMyGLDCView.this.mSizeChanged = true;
            }
        }

        public void requestExitAndWait() {
            synchronized (this) {
                this.mDone = true;
                notify();
            }
            try {
                join();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }

        public void setEvent(Runnable runnable) {
            synchronized (this) {
                this.mEvent = runnable;
            }
        }

        public void clearEvent() {
            synchronized (this) {
                this.mEvent = null;
            }
        }
    }

    public interface GLWrapper {
        GL wrap(GL gl);
    }

    public interface Renderer {
        void drawFrame(GL10 gl10);

        int[] getConfigSpec();

        void shutdown(GL10 gl10);

        void sizeChanged(GL10 gl10, int i, int i2);

        void surfaceCreated(GL10 gl10);
    }

    public void prepareDC() {
        super.prepareDC();
        createBackgroundSprite();
    }

    private void createBackgroundSprite() {
        GLSprite gLSprite = new GLSprite(-1);
        Bitmap bitmap = this.m_bmpDblBuffer;
        gLSprite.width = (float) bitmap.getWidth();
        gLSprite.height = (float) bitmap.getHeight();
        gLSprite.setBmp(bitmap);
        float scaleRes = getScaleRes();
        Grid grid = new Grid(2, 2, false);
        float f = 0.0f / scaleRes;
        float f2 = 1.0f / scaleRes;
        Grid grid2 = grid;
        float f3 = f2;
        grid2.set(0, 0, 0.0f, 0.0f, 0.0f, f, f3, null);
        grid2.set(1, 0, gLSprite.width, 0.0f, 0.0f, f2, f3, null);
        float f4 = f;
        grid2.set(0, 1, 0.0f, gLSprite.height, 0.0f, f, f4, null);
        grid2.set(1, 1, gLSprite.width, gLSprite.height, 0.0f, f2, f4, null);
        gLSprite.setGrid(grid);
        this.m_renderer.setSprites(new GLSprite[]{gLSprite});
        this.m_renderer.setVertMode(true, false);
    }

    public synchronized void update(CEventWnd cEventWnd) {
        if (cEventWnd != null) {
            try {
                synchronized (this.m_bmpDblBuffer) {
                    clear();
                    cEventWnd.DrawPrevProc();
                    cEventWnd.DrawProcess();
                }
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        } else {
            return;
        }
    }

    public CMyGLDCView(Context context) {
        super(context);
        init(context);
    }

    public CMyGLDCView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        SimpleGLRenderer simpleGLRenderer = new SimpleGLRenderer(context, this);
        setRenderer(simpleGLRenderer);
        this.m_renderer = simpleGLRenderer;
    }

    public void setGLWrapper(GLWrapper gLWrapper) {
        this.mGLWrapper = gLWrapper;
    }

    public void setRenderer(Renderer renderer) {
        this.mGLThread = new GLThread(renderer);
        this.mGLThread.start();
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.mGLThread.surfaceDestroyed();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        this.mGLThread.onWindowResize(i2, i3);
    }

    public void onPause() {
        this.mGLThread.onPause();
    }

    public void onResume() {
        this.mGLThread.onResume();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.mGLThread.onWindowFocusChanged(z);
    }

    public void setEvent(Runnable runnable) {
        this.mGLThread.setEvent(runnable);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mGLThread.requestExitAndWait();
    }
}
