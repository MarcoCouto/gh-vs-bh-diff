package com.dlten.lib.opengl;

import android.graphics.Bitmap;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11Ext;

public class GLSprite extends Renderable {
    private Bitmap mBmp = null;
    private Grid mGrid;
    private int mResourceId;
    private int mTextureName;

    public GLSprite(int i) {
        this.mResourceId = i;
    }

    public void setTextureName(int i) {
        this.mTextureName = i;
    }

    public int getTextureName() {
        return this.mTextureName;
    }

    public void setResourceId(int i) {
        this.mResourceId = i;
    }

    public int getResourceId() {
        return this.mResourceId;
    }

    public void setGrid(Grid grid) {
        this.mGrid = grid;
    }

    public Grid getGrid() {
        return this.mGrid;
    }

    public void draw(GL10 gl10) {
        gl10.glBindTexture(3553, this.mTextureName);
        if (this.mGrid == null) {
            ((GL11Ext) gl10).glDrawTexfOES(this.x, this.y, this.z, this.width, this.height);
            return;
        }
        gl10.glPushMatrix();
        gl10.glLoadIdentity();
        gl10.glTranslatef(this.x, this.y, this.z);
        this.mGrid.draw(gl10, true, false);
        gl10.glPopMatrix();
    }

    public Bitmap getBmp() {
        return this.mBmp;
    }

    public void setBmp(Bitmap bitmap) {
        this.mBmp = bitmap;
    }

    public void resetBmp() {
        if (this.mBmp != null) {
            this.mBmp.recycle();
            this.mBmp = null;
        }
    }
}
