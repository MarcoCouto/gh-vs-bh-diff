package com.dlten.lib.opengl;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

class Grid {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private Buffer mColorBuffer;
    private int mColorBufferIndex;
    private int mCoordinateSize;
    private int mCoordinateType;
    private IntBuffer mFixedColorBuffer;
    private IntBuffer mFixedTexCoordBuffer;
    private IntBuffer mFixedVertexBuffer;
    private FloatBuffer mFloatColorBuffer;
    private FloatBuffer mFloatTexCoordBuffer;
    private FloatBuffer mFloatVertexBuffer;
    private int mH;
    private CharBuffer mIndexBuffer;
    private int mIndexBufferIndex;
    private int mIndexCount;
    private Buffer mTexCoordBuffer;
    private int mTextureCoordBufferIndex;
    private boolean mUseHardwareBuffers;
    private int mVertBufferIndex;
    private Buffer mVertexBuffer;
    private int mW;

    public Grid(int i, int i2, boolean z) {
        if (i < 0 || i >= 65536) {
            throw new IllegalArgumentException("vertsAcross");
        } else if (i2 < 0 || i2 >= 65536) {
            throw new IllegalArgumentException("vertsDown");
        } else {
            int i3 = i * i2;
            if (i3 >= 65536) {
                throw new IllegalArgumentException("vertsAcross * vertsDown >= 65536");
            }
            this.mUseHardwareBuffers = false;
            this.mW = i;
            this.mH = i2;
            if (z) {
                int i4 = i3 * 4;
                this.mFixedVertexBuffer = ByteBuffer.allocateDirect(i4 * 3).order(ByteOrder.nativeOrder()).asIntBuffer();
                this.mFixedTexCoordBuffer = ByteBuffer.allocateDirect(i4 * 2).order(ByteOrder.nativeOrder()).asIntBuffer();
                this.mFixedColorBuffer = ByteBuffer.allocateDirect(i4 * 4).order(ByteOrder.nativeOrder()).asIntBuffer();
                this.mVertexBuffer = this.mFixedVertexBuffer;
                this.mTexCoordBuffer = this.mFixedTexCoordBuffer;
                this.mColorBuffer = this.mFixedColorBuffer;
                this.mCoordinateSize = 4;
                this.mCoordinateType = 5132;
            } else {
                int i5 = i3 * 4;
                this.mFloatVertexBuffer = ByteBuffer.allocateDirect(i5 * 3).order(ByteOrder.nativeOrder()).asFloatBuffer();
                this.mFloatTexCoordBuffer = ByteBuffer.allocateDirect(i5 * 2).order(ByteOrder.nativeOrder()).asFloatBuffer();
                this.mFloatColorBuffer = ByteBuffer.allocateDirect(i5 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
                this.mVertexBuffer = this.mFloatVertexBuffer;
                this.mTexCoordBuffer = this.mFloatTexCoordBuffer;
                this.mColorBuffer = this.mFloatColorBuffer;
                this.mCoordinateSize = 4;
                this.mCoordinateType = 5126;
            }
            int i6 = this.mW - 1;
            int i7 = this.mH - 1;
            int i8 = i6 * i7 * 6;
            this.mIndexCount = i8;
            this.mIndexBuffer = ByteBuffer.allocateDirect(2 * i8).order(ByteOrder.nativeOrder()).asCharBuffer();
            int i9 = 0;
            int i10 = 0;
            while (i9 < i7) {
                int i11 = i10;
                int i12 = 0;
                while (i12 < i6) {
                    char c = (char) ((this.mW * i9) + i12 + 1);
                    int i13 = i9 + 1;
                    char c2 = (char) ((this.mW * i13) + i12);
                    char c3 = (char) ((i13 * this.mW) + i12 + 1);
                    int i14 = i11 + 1;
                    this.mIndexBuffer.put(i11, (char) ((this.mW * i9) + i12));
                    int i15 = i14 + 1;
                    this.mIndexBuffer.put(i14, c);
                    int i16 = i15 + 1;
                    this.mIndexBuffer.put(i15, c2);
                    int i17 = i16 + 1;
                    this.mIndexBuffer.put(i16, c);
                    int i18 = i17 + 1;
                    this.mIndexBuffer.put(i17, c2);
                    int i19 = i18 + 1;
                    this.mIndexBuffer.put(i18, c3);
                    i12++;
                    i11 = i19;
                }
                i9++;
                i10 = i11;
            }
            this.mVertBufferIndex = 0;
        }
    }

    /* access modifiers changed from: 0000 */
    public void set(int i, int i2, float f, float f2, float f3, float f4, float f5, float[] fArr) {
        if (i < 0 || i >= this.mW) {
            throw new IllegalArgumentException("i");
        } else if (i2 < 0 || i2 >= this.mH) {
            throw new IllegalArgumentException("j");
        } else {
            int i3 = (this.mW * i2) + i;
            int i4 = i3 * 3;
            int i5 = i3 * 2;
            int i6 = i3 * 4;
            if (this.mCoordinateType == 5126) {
                this.mFloatVertexBuffer.put(i4, f);
                this.mFloatVertexBuffer.put(i4 + 1, f2);
                this.mFloatVertexBuffer.put(i4 + 2, f3);
                this.mFloatTexCoordBuffer.put(i5, f4);
                this.mFloatTexCoordBuffer.put(i5 + 1, f5);
                if (fArr != null) {
                    this.mFloatColorBuffer.put(i6, fArr[0]);
                    this.mFloatColorBuffer.put(i6 + 1, fArr[1]);
                    this.mFloatColorBuffer.put(i6 + 2, fArr[2]);
                    this.mFloatColorBuffer.put(i6 + 3, fArr[3]);
                    return;
                }
                return;
            }
            this.mFixedVertexBuffer.put(i4, (int) (f * 65536.0f));
            this.mFixedVertexBuffer.put(i4 + 1, (int) (f2 * 65536.0f));
            this.mFixedVertexBuffer.put(i4 + 2, (int) (f3 * 65536.0f));
            this.mFixedTexCoordBuffer.put(i5, (int) (f4 * 65536.0f));
            this.mFixedTexCoordBuffer.put(i5 + 1, (int) (f5 * 65536.0f));
            if (fArr != null) {
                this.mFixedColorBuffer.put(i6, (int) (fArr[0] * 65536.0f));
                this.mFixedColorBuffer.put(i6 + 1, (int) (fArr[1] * 65536.0f));
                this.mFixedColorBuffer.put(i6 + 2, (int) (fArr[2] * 65536.0f));
                this.mFixedColorBuffer.put(i6 + 3, (int) (fArr[3] * 65536.0f));
            }
        }
    }

    public static void beginDrawing(GL10 gl10, boolean z, boolean z2) {
        gl10.glEnableClientState(32884);
        if (z) {
            gl10.glEnableClientState(32888);
            gl10.glEnable(3553);
        } else {
            gl10.glDisableClientState(32888);
            gl10.glDisable(3553);
        }
        if (z2) {
            gl10.glEnableClientState(32886);
        } else {
            gl10.glDisableClientState(32886);
        }
    }

    public void draw(GL10 gl10, boolean z, boolean z2) {
        if (!this.mUseHardwareBuffers) {
            gl10.glVertexPointer(3, this.mCoordinateType, 0, this.mVertexBuffer);
            if (z) {
                gl10.glTexCoordPointer(2, this.mCoordinateType, 0, this.mTexCoordBuffer);
            }
            if (z2) {
                gl10.glColorPointer(4, this.mCoordinateType, 0, this.mColorBuffer);
            }
            gl10.glDrawElements(4, this.mIndexCount, 5123, this.mIndexBuffer);
            return;
        }
        GL11 gl11 = (GL11) gl10;
        gl11.glBindBuffer(34962, this.mVertBufferIndex);
        gl11.glVertexPointer(3, this.mCoordinateType, 0, 0);
        if (z) {
            gl11.glBindBuffer(34962, this.mTextureCoordBufferIndex);
            gl11.glTexCoordPointer(2, this.mCoordinateType, 0, 0);
        }
        if (z2) {
            gl11.glBindBuffer(34962, this.mColorBufferIndex);
            gl11.glColorPointer(4, this.mCoordinateType, 0, 0);
        }
        gl11.glBindBuffer(34963, this.mIndexBufferIndex);
        gl11.glDrawElements(4, this.mIndexCount, 5123, 0);
        gl11.glBindBuffer(34962, 0);
        gl11.glBindBuffer(34963, 0);
    }

    public static void endDrawing(GL10 gl10) {
        gl10.glDisableClientState(32884);
    }

    public boolean usingHardwareBuffers() {
        return this.mUseHardwareBuffers;
    }

    public void invalidateHardwareBuffers() {
        this.mVertBufferIndex = 0;
        this.mIndexBufferIndex = 0;
        this.mTextureCoordBufferIndex = 0;
        this.mColorBufferIndex = 0;
        this.mUseHardwareBuffers = false;
    }

    public void releaseHardwareBuffers(GL10 gl10) {
        if (this.mUseHardwareBuffers) {
            if (gl10 instanceof GL11) {
                GL11 gl11 = (GL11) gl10;
                int[] iArr = {this.mVertBufferIndex};
                gl11.glDeleteBuffers(1, iArr, 0);
                iArr[0] = this.mTextureCoordBufferIndex;
                gl11.glDeleteBuffers(1, iArr, 0);
                iArr[0] = this.mColorBufferIndex;
                gl11.glDeleteBuffers(1, iArr, 0);
                iArr[0] = this.mIndexBufferIndex;
                gl11.glDeleteBuffers(1, iArr, 0);
            }
            invalidateHardwareBuffers();
        }
    }

    public void generateHardwareBuffers(GL10 gl10) {
        if (!this.mUseHardwareBuffers && (gl10 instanceof GL11)) {
            GL11 gl11 = (GL11) gl10;
            int[] iArr = new int[1];
            gl11.glGenBuffers(1, iArr, 0);
            this.mVertBufferIndex = iArr[0];
            gl11.glBindBuffer(34962, this.mVertBufferIndex);
            gl11.glBufferData(34962, this.mVertexBuffer.capacity() * this.mCoordinateSize, this.mVertexBuffer, 35044);
            gl11.glGenBuffers(1, iArr, 0);
            this.mTextureCoordBufferIndex = iArr[0];
            gl11.glBindBuffer(34962, this.mTextureCoordBufferIndex);
            gl11.glBufferData(34962, this.mTexCoordBuffer.capacity() * this.mCoordinateSize, this.mTexCoordBuffer, 35044);
            gl11.glGenBuffers(1, iArr, 0);
            this.mColorBufferIndex = iArr[0];
            gl11.glBindBuffer(34962, this.mColorBufferIndex);
            gl11.glBufferData(34962, this.mColorBuffer.capacity() * this.mCoordinateSize, this.mColorBuffer, 35044);
            gl11.glBindBuffer(34962, 0);
            gl11.glGenBuffers(1, iArr, 0);
            this.mIndexBufferIndex = iArr[0];
            gl11.glBindBuffer(34963, this.mIndexBufferIndex);
            gl11.glBufferData(34963, this.mIndexBuffer.capacity() * 2, this.mIndexBuffer, 35044);
            gl11.glBindBuffer(34963, 0);
            this.mUseHardwareBuffers = true;
        }
    }

    public final int getVertexBuffer() {
        return this.mVertBufferIndex;
    }

    public final int getTextureBuffer() {
        return this.mTextureCoordBufferIndex;
    }

    public final int getIndexBuffer() {
        return this.mIndexBufferIndex;
    }

    public final int getColorBuffer() {
        return this.mColorBufferIndex;
    }

    public final int getIndexCount() {
        return this.mIndexCount;
    }

    public boolean getFixedPoint() {
        return this.mCoordinateType == 5132;
    }
}
