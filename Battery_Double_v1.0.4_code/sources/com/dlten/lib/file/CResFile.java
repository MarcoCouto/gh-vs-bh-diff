package com.dlten.lib.file;

import android.content.Context;
import com.dlten.lib.STD;
import java.io.InputStream;

public class CResFile {
    private static Context m_context;

    public static void Initialize(Context context) {
        m_context = context;
    }

    public static Context getAppContext() {
        return m_context;
    }

    public static byte[] load(String str) {
        try {
            InputStream open = m_context.getAssets().open(str);
            if (open == null) {
                return null;
            }
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return bArr;
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadBitmap failed : name=");
            sb.append(str);
            STD.logout(sb.toString());
            STD.printStackTrace(e);
            return null;
        }
    }

    public static byte[] load(int i) {
        try {
            InputStream openRawResource = m_context.getResources().openRawResource(i);
            if (openRawResource == null) {
                return null;
            }
            byte[] bArr = new byte[openRawResource.available()];
            openRawResource.read(bArr);
            openRawResource.close();
            return bArr;
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadBitmap failed : id=");
            sb.append(i);
            STD.logout(sb.toString());
            STD.printStackTrace(e);
            return null;
        }
    }
}
