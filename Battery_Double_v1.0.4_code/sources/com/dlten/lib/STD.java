package com.dlten.lib;

import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.dlten.lib.frmWork.CAnimation;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Random;

public class STD {
    private static Random m_rnd;

    public static byte HIBYTE(int i) {
        return (byte) (((i & SupportMenu.USER_MASK) >>> 8) & 255);
    }

    public static short HIWORD(int i) {
        return (short) ((i >>> 16) & SupportMenu.USER_MASK);
    }

    public static byte LOBYTE(int i) {
        return (byte) (i & 255);
    }

    public static short LOWORD(int i) {
        return (short) (i & SupportMenu.USER_MASK);
    }

    public static long MAKELONG(short s, short s2) {
        return (long) ((s & CAnimation.IDA_NONE) | ((s2 & CAnimation.IDA_NONE) << 16));
    }

    public static short MAKEWORD(byte b, byte b2) {
        return (short) ((b & 255) | ((b2 & 255) << 8));
    }

    public static byte MAX(byte b, byte b2) {
        return b >= b2 ? b : b2;
    }

    public static float MAX(float f, float f2) {
        return f >= f2 ? f : f2;
    }

    public static int MAX(int i, int i2) {
        return i >= i2 ? i : i2;
    }

    public static long MAX(long j, long j2) {
        return j >= j2 ? j : j2;
    }

    public static short MAX(short s, short s2) {
        return s >= s2 ? s : s2;
    }

    public static byte MIN(byte b, byte b2) {
        return b <= b2 ? b : b2;
    }

    public static float MIN(float f, float f2) {
        return f <= f2 ? f : f2;
    }

    public static int MIN(int i, int i2) {
        return i <= i2 ? i : i2;
    }

    public static long MIN(long j, long j2) {
        return j <= j2 ? j : j2;
    }

    public static short MIN(short s, short s2) {
        return s <= s2 ? s : s2;
    }

    public static void MEMSET(boolean[] zArr, boolean z) {
        if (zArr != null) {
            for (int i = 0; i < zArr.length; i++) {
                zArr[i] = z;
            }
        }
    }

    public static void MEMSET(byte[] bArr, byte b) {
        if (bArr != null) {
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = b;
            }
        }
    }

    public static void MEMSET(char[] cArr, char c) {
        if (cArr != null) {
            for (int i = 0; i < cArr.length; i++) {
                cArr[i] = c;
            }
        }
    }

    public static void MEMSET(short[] sArr, short s) {
        if (sArr != null) {
            for (int i = 0; i < sArr.length; i++) {
                sArr[i] = s;
            }
        }
    }

    public static void MEMSET(int[] iArr, int i) {
        if (iArr != null) {
            for (int i2 = 0; i2 < iArr.length; i2++) {
                iArr[i2] = i;
            }
        }
    }

    public static void MEMSET(byte[][] bArr, byte b) {
        if (bArr != null) {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != null) {
                    for (int i2 = 0; i2 < bArr[i].length; i2++) {
                        bArr[i][i2] = b;
                    }
                }
            }
        }
    }

    public static void MEMSET(char[][] cArr, char c) {
        if (cArr != null) {
            for (int i = 0; i < cArr.length; i++) {
                if (cArr[i] != null) {
                    for (int i2 = 0; i2 < cArr[i].length; i2++) {
                        cArr[i][i2] = c;
                    }
                }
            }
        }
    }

    public static void MEMSET(short[][] sArr, short s) {
        if (sArr != null) {
            for (int i = 0; i < sArr.length; i++) {
                if (sArr[i] != null) {
                    for (int i2 = 0; i2 < sArr[i].length; i2++) {
                        sArr[i][i2] = s;
                    }
                }
            }
        }
    }

    public static void MEMSET(int[][] iArr, int i) {
        if (iArr != null) {
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (iArr[i2] != null) {
                    for (int i3 = 0; i3 < iArr[i2].length; i3++) {
                        iArr[i2][i3] = i;
                    }
                }
            }
        }
    }

    public static void MEMCPY(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        if (i < bArr.length && i2 < bArr2.length) {
            int min = Math.min(Math.min(bArr.length - i, bArr2.length - i2), i3);
            for (int i4 = 0; i4 < min; i4++) {
                bArr[i + i4] = bArr2[i2 + i4];
            }
        }
    }

    public static void MEMCPY(byte[] bArr, byte[] bArr2, int i) {
        int min = Math.min(Math.min(bArr.length, bArr2.length), i);
        for (int i2 = 0; i2 < min; i2++) {
            bArr[i2] = bArr2[i2];
        }
    }

    public static void MEMCPY(short[] sArr, short[] sArr2, int i) {
        int min = Math.min(Math.min(sArr.length, sArr2.length), i);
        for (int i2 = 0; i2 < min; i2++) {
            sArr[i2] = sArr2[i2];
        }
    }

    public static void MEMCPY(int[] iArr, int[] iArr2, int i) {
        int min = Math.min(Math.min(iArr.length, iArr2.length), i);
        for (int i2 = 0; i2 < min; i2++) {
            iArr[i2] = iArr2[i2];
        }
    }

    public static void MEMCPY(boolean[] zArr, boolean[] zArr2) {
        int min = Math.min(zArr.length, zArr2.length);
        for (int i = 0; i < min; i++) {
            zArr[i] = zArr2[i];
        }
    }

    public static void MEMCPY(byte[] bArr, byte[] bArr2) {
        int min = Math.min(bArr.length, bArr2.length);
        for (int i = 0; i < min; i++) {
            bArr[i] = bArr2[i];
        }
    }

    public static void MEMCPY(char[] cArr, char[] cArr2) {
        int min = Math.min(cArr.length, cArr2.length);
        for (int i = 0; i < min; i++) {
            cArr[i] = cArr2[i];
        }
    }

    public static void MEMCPY(short[] sArr, short[] sArr2) {
        int min = Math.min(sArr.length, sArr2.length);
        for (int i = 0; i < min; i++) {
            sArr[i] = sArr2[i];
        }
    }

    public static void MEMCPY(int[] iArr, int[] iArr2) {
        int min = Math.min(iArr.length, iArr2.length);
        for (int i = 0; i < min; i++) {
            iArr[i] = iArr2[i];
        }
    }

    public static void MEMCPY(byte[][] bArr, byte[][] bArr2) {
        int i = 0;
        while (i < bArr.length && i < bArr2.length) {
            int i2 = 0;
            while (i2 < bArr[i].length && i2 < bArr2[i].length) {
                bArr[i][i2] = bArr2[i][i2];
                i2++;
            }
            i++;
        }
    }

    public static void MEMCPY(char[][] cArr, char[][] cArr2) {
        int i = 0;
        while (i < cArr.length && i < cArr2.length) {
            int i2 = 0;
            while (i2 < cArr[i].length && i2 < cArr2[i].length) {
                cArr[i][i2] = cArr2[i][i2];
                i2++;
            }
            i++;
        }
    }

    public static void MEMCPY(short[][] sArr, short[][] sArr2) {
        int i = 0;
        while (i < sArr.length && i < sArr2.length) {
            int i2 = 0;
            while (i2 < sArr[i].length && i2 < sArr2[i].length) {
                sArr[i][i2] = sArr2[i][i2];
                i2++;
            }
            i++;
        }
    }

    public static void MEMCPY(int[][] iArr, int[][] iArr2) {
        int i = 0;
        while (i < iArr.length && i < iArr2.length) {
            int i2 = 0;
            while (i2 < iArr[i].length && i2 < iArr2[i].length) {
                iArr[i][i2] = iArr2[i][i2];
                i2++;
            }
            i++;
        }
    }

    public static int STRCMP(String str, String str2) {
        if (str.equals(str2)) {
            return 0;
        }
        return str.compareTo(str2);
    }

    public static byte[] str2ByteArray(String str, int i) {
        byte[] bArr;
        byte[] bArr2 = new byte[i];
        if (str != null) {
            bArr = str.getBytes();
        } else {
            bArr = new byte[0];
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (i2 >= bArr.length) {
                bArr2[i2] = 0;
            } else {
                bArr2[i2] = bArr[i2];
            }
        }
        return bArr2;
    }

    public static String dis2Str(DataInputStream dataInputStream, int i) {
        byte[] bArr = new byte[i];
        MEMSET(bArr, 0);
        try {
            dataInputStream.read(bArr);
        } catch (IOException unused) {
        }
        int i2 = 0;
        int i3 = 0;
        while (i2 < bArr.length && bArr[i2] != 0) {
            i3++;
            i2++;
        }
        return new String(bArr, 0, i3);
    }

    public final String[] splitString(String str, char c) {
        String[] strArr = new String[200];
        String str2 = "";
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt == c) {
                strArr[i] = str2;
                str2 = "";
                i++;
            } else if (charAt != 13) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(charAt);
                str2 = sb.toString();
            }
        }
        if (str2 != "") {
            strArr[i] = str2;
            i++;
        }
        String[] strArr2 = new String[i];
        for (int i3 = 0; i3 < i; i3++) {
            strArr2[i3] = strArr[i3];
        }
        return strArr2;
    }

    public static String getnString(int i, int i2) {
        String str;
        String sb;
        if (i >= 0) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(i);
            str = sb2.toString();
        } else {
            str = "";
        }
        for (int length = str.length(); length < i2; length++) {
            if (i >= 0) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("0");
                sb3.append(str);
                sb = sb3.toString();
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("-");
                sb4.append(str);
                sb = sb4.toString();
            }
        }
        return str;
    }

    public static String getCommaString(int i) {
        String str = "";
        if (i == 0) {
            return "0";
        }
        while (i > 0) {
            if (str == "") {
                StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(getModStr(i, 1000));
                str = sb.toString();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(getModStr(i, 1000));
                sb2.append(",");
                sb2.append(str);
                str = sb2.toString();
            }
            i /= 1000;
        }
        return str;
    }

    public static String getModStr(int i, int i2) {
        String str = "";
        if (i < i2) {
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(i % i2);
            return sb.toString();
        }
        while (i2 > 1) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            int i3 = i % i2;
            i2 /= 10;
            sb2.append(i3 / i2);
            str = sb2.toString();
        }
        return str;
    }

    public static String getPercentString(int i) {
        if (i >= 10000) {
            return "100.00%";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i % 10);
        sb.append("%");
        String sb2 = sb.toString();
        int i2 = i / 10;
        StringBuilder sb3 = new StringBuilder();
        sb3.append(".");
        sb3.append(i2 % 10);
        sb3.append(sb2);
        String sb4 = sb3.toString();
        int i3 = i2 / 10;
        StringBuilder sb5 = new StringBuilder();
        sb5.append(i3);
        sb5.append(sb4);
        return sb5.toString();
    }

    public static long Bytes2Long(byte[] bArr, int i) {
        return (long) ((bArr[i] & 255) | ((bArr[i + 7] & 255) << 56) | ((bArr[i + 6] & 255) << 48) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 1] & 255) << 8));
    }

    public static int Bytes2Int(byte[] bArr, int i) {
        return (bArr[i] & 255) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 1] & 255) << 8);
    }

    public static short Bytes2Short(byte[] bArr, int i) {
        return (short) ((bArr[i] & 255) | ((bArr[i + 1] & 255) << 8));
    }

    public static void Long2Bytes(byte[] bArr, int i, long j) {
        bArr[i + 7] = (byte) ((int) ((j >> 56) & 255));
        bArr[i + 6] = (byte) ((int) ((j >> 48) & 255));
        bArr[i + 5] = (byte) ((int) ((j >> 40) & 255));
        bArr[i + 4] = (byte) ((int) ((j >> 32) & 255));
        bArr[i + 3] = (byte) ((int) ((j >> 24) & 255));
        bArr[i + 2] = (byte) ((int) ((j >> 16) & 255));
        bArr[i + 1] = (byte) ((int) ((j >> 8) & 255));
        bArr[i + 0] = (byte) ((int) ((j >> 0) & 255));
    }

    public static void Int2Bytes(byte[] bArr, int i, int i2) {
        bArr[i + 3] = (byte) ((-16777216 & i2) >> 24);
        bArr[i + 2] = (byte) ((16711680 & i2) >> 16);
        bArr[i + 1] = (byte) ((65280 & i2) >> 8);
        bArr[i + 0] = (byte) (i2 & 255);
    }

    public static void Short2Bytes(byte[] bArr, int i, short s) {
        bArr[i + 1] = (byte) ((65280 & s) >> 8);
        bArr[i] = (byte) (s & 255);
    }

    public static byte[] getByteArrayFromLong(long j) {
        byte[] bArr = new byte[8];
        for (int i = 0; i < 8; i++) {
            bArr[i] = (byte) ((int) ((j >>> (i * 8)) & 255));
        }
        return bArr;
    }

    public static long getLongFromByteArray(byte[] bArr) {
        int i = 8;
        int length = bArr.length;
        if (length <= 8) {
            i = length;
        }
        int i2 = 0;
        long j = 0;
        while (i2 < i) {
            i2++;
            j |= ((long) (bArr[i2] & 255)) << (i2 * 8);
        }
        return j;
    }

    public static final void sleep(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException unused) {
        }
    }

    public static long GetTickCount() {
        return System.currentTimeMillis();
    }

    public static void DeviceInfo() {
        Log.e("BOARD", Build.BOARD);
        Log.e("BRAND", Build.BRAND);
        Log.e("DEVICE", Build.DEVICE);
        Log.e("DISPLAY", Build.DISPLAY);
        Log.e("FINGERPRINT", Build.FINGERPRINT);
        Log.e("HOST", Build.HOST);
        Log.e("ID", Build.ID);
        Log.e("MODEL", Build.MODEL);
        Log.e("PRODUCT", Build.PRODUCT);
        Log.e("TAGS", Build.TAGS);
        Log.e("TYPE", Build.TYPE);
        Log.e("USER", Build.USER);
    }

    public static final String getHeapState() {
        long freeMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long j = Runtime.getRuntime().totalMemory();
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(freeMemory / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
        sb.append("/");
        sb.append(j / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
        return sb.toString();
    }

    public static void logHeap() {
        long maxMemory = Runtime.getRuntime().maxMemory();
        long j = Runtime.getRuntime().totalMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();
        long j2 = j - freeMemory;
        StringBuilder sb = new StringBuilder();
        sb.append("Heap - ( m : ");
        sb.append(Long.toString(maxMemory));
        sb.append(", t : ");
        sb.append(Long.toString(j));
        sb.append(", f : ");
        sb.append(Long.toString(freeMemory));
        sb.append(", u : ");
        sb.append(Long.toString(j2));
        sb.append(")");
        logout(sb.toString());
        System.gc();
    }

    public static void logout(String str) {
        Log.d("slib", str);
    }

    public static void printStackTrace(Exception exc) {
        if (exc != null) {
            StackTraceElement[] stackTrace = exc.getStackTrace();
            if (stackTrace != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Exception =");
                sb.append(exc.toString());
                logout(sb.toString());
                for (int i = 0; i < stackTrace.length; i++) {
                    if (stackTrace[i] != null) {
                        logout(stackTrace[i].toString());
                    }
                }
                logout("Exception Stack End");
            }
        }
    }

    public static void initRand() {
        m_rnd = new Random();
        m_rnd.setSeed(System.currentTimeMillis());
    }

    public static int rand(int i) {
        return Math.abs(m_rnd.nextInt()) % i;
    }

    public static void ASSERT(boolean z) {
        if (!z) {
            try {
                throw new Exception("ASSERT");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static int[] SETAEERECT(int i, int i2, int i3, int i4) {
        return new int[]{i, i2, i3, i4};
    }
}
