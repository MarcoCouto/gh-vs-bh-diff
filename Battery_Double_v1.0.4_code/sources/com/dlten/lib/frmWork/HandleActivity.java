package com.dlten.lib.frmWork;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import com.dlten.lib.Common;
import com.dlten.lib.STD;
import java.io.File;
import java.util.Iterator;

public abstract class HandleActivity extends Activity implements MainThreadListner {
    /* access modifiers changed from: private */
    public int prevState = 0;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        firstInit();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        clearApplicationCache(null);
        ((ActivityManager) getSystemService("activity")).restartPackage(getPackageName());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 24:
                mediaVolumeUp();
                break;
            case 25:
                mediaVolumeDown();
                break;
            default:
                return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    private void firstInit() {
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        StringBuilder sb = new StringBuilder();
        sb.append("width=");
        sb.append(width);
        sb.append(", height=");
        sb.append(height);
        STD.logout(sb.toString());
        Common.Initialize(this, width, height);
    }

    private void mediaVolumeDown() {
        ((AudioManager) getSystemService("audio")).adjustStreamVolume(3, -1, 1);
    }

    private void mediaVolumeUp() {
        ((AudioManager) getSystemService("audio")).adjustStreamVolume(3, 1, 1);
    }

    public void requestKillProcess() {
        new Thread(new Runnable() {
            public void run() {
                ActivityManager activityManager = (ActivityManager) HandleActivity.this.getSystemService("activity");
                String str = HandleActivity.this.getApplicationInfo().processName;
                while (true) {
                    Iterator it = activityManager.getRunningAppProcesses().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        RunningAppProcessInfo runningAppProcessInfo = (RunningAppProcessInfo) it.next();
                        if (runningAppProcessInfo.processName.equals(str)) {
                            if (HandleActivity.this.prevState != runningAppProcessInfo.importance) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("process '");
                                sb.append(str);
                                sb.append("',s state is ");
                                sb.append(runningAppProcessInfo.importance);
                                STD.logout(sb.toString());
                                HandleActivity.this.prevState = runningAppProcessInfo.importance;
                            }
                            if (runningAppProcessInfo.importance >= 400) {
                                HandleActivity.this.clearApplicationCache(null);
                                activityManager.restartPackage(HandleActivity.this.getPackageName());
                            }
                        } else {
                            Thread.yield();
                        }
                    }
                    STD.sleep(30);
                }
            }
        }, "Process Killer").start();
    }

    /* access modifiers changed from: private */
    public void clearApplicationCache(File file) {
        if (file == null) {
            file = getCacheDir();
        }
        if (file != null) {
            File[] listFiles = file.listFiles();
            int i = 0;
            while (i < listFiles.length) {
                try {
                    if (listFiles[i].isDirectory()) {
                        clearApplicationCache(listFiles[i]);
                    } else {
                        listFiles[i].delete();
                    }
                    i++;
                } catch (Exception unused) {
                }
            }
        }
    }
}
