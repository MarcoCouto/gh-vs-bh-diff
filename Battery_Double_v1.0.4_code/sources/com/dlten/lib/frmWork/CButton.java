package com.dlten.lib.frmWork;

import com.dlten.lib.graphics.CImgObj;
import com.dlten.lib.graphics.CPoint;
import com.dlten.lib.graphics.CRect;

public class CButton {
    public static final int BS_FOCUS = 2;
    public static final int BS_NORMAL = 1;
    public static final int CMD_NONE = -1;
    private boolean m_bEnable = false;
    private boolean m_bVisible = false;
    private int m_command = -1;
    private CImgObj m_dis = null;
    private CImgObj m_foc = null;
    private CImgObj m_nor = null;
    private CEventWnd m_parent = null;
    private CPoint m_pos = new CPoint();
    private CRect m_rect = new CRect();
    private CRect m_rectTouch = new CRect();
    private int m_state = 1;

    public CButton() {
    }

    public CButton(CEventWnd cEventWnd, CPoint cPoint, CImgObj cImgObj, CImgObj cImgObj2, CImgObj cImgObj3) {
        create(cEventWnd, cPoint, cImgObj, cImgObj2, cImgObj3);
    }

    public CButton(CEventWnd cEventWnd, CImgObj cImgObj, CImgObj cImgObj2, CImgObj cImgObj3) {
        create(cEventWnd, new CPoint(0, 0), cImgObj, cImgObj2, cImgObj3);
    }

    public void create(CEventWnd cEventWnd, String str, String str2, String str3) {
        CEventWnd cEventWnd2 = cEventWnd;
        create(cEventWnd2, new CPoint(0, 0), new CImgObj(str), new CImgObj(str2), new CImgObj(str3));
    }

    public void create(CEventWnd cEventWnd, CPoint cPoint, String str, String str2, String str3) {
        create(cEventWnd, cPoint, new CImgObj(str), new CImgObj(str2), new CImgObj(str3));
    }

    public void create(CEventWnd cEventWnd, CPoint cPoint, CImgObj cImgObj, CImgObj cImgObj2, CImgObj cImgObj3) {
        this.m_parent = cEventWnd;
        this.m_parent.AddButton(this);
        setImage_Normal(cImgObj);
        setImage_Focus(cImgObj2);
        setImage_Disable(cImgObj3);
        setPoint(cPoint);
        setEnable(true);
        setVisible(true);
        setNormal();
    }

    public void setImage_Normal(String str) {
        this.m_nor = new CImgObj(str);
    }

    public void setImage_Focus(String str) {
        this.m_foc = new CImgObj(str);
    }

    public void setImage_Disable(String str) {
        this.m_dis = new CImgObj(str);
    }

    public void setImage_Normal(CImgObj cImgObj) {
        this.m_nor = cImgObj;
    }

    public void setImage_Focus(CImgObj cImgObj) {
        this.m_foc = cImgObj;
    }

    public void setImage_Disable(CImgObj cImgObj) {
        this.m_dis = cImgObj;
    }

    public void destroy() {
        this.m_parent.RemoveButton(this);
        this.m_nor.unload();
        this.m_foc.unload();
        this.m_dis.unload();
        this.m_pos = null;
        this.m_nor = null;
        this.m_foc = null;
        this.m_dis = null;
        this.m_pos = null;
        this.m_rect = null;
        this.m_rectTouch = null;
    }

    public void Draw() {
        if (isVisible()) {
            if (!isEnable()) {
                this.m_dis.draw(this.m_pos);
                return;
            }
            switch (this.m_state) {
                case 1:
                    this.m_nor.draw(this.m_pos);
                    break;
                case 2:
                    this.m_foc.draw(this.m_pos);
                    break;
            }
        }
    }

    public CPoint getPoint() {
        return this.m_pos;
    }

    public void setPoint(float f, float f2) {
        this.m_pos.x = f;
        this.m_pos.y = f2;
        setRect();
    }

    public void setPoint(CPoint cPoint) {
        this.m_pos = cPoint;
        setRect();
    }

    private void setRect() {
        this.m_rect.left = this.m_pos.x;
        this.m_rect.top = this.m_pos.y;
        this.m_rect.width = this.m_nor.getSizeX();
        this.m_rect.height = this.m_nor.getSizeY();
    }

    public boolean isInside(CPoint cPoint) {
        if (this.m_rectTouch == null || this.m_rect == null) {
            return false;
        }
        float f = (float) 5;
        this.m_rectTouch.left = this.m_rect.left - f;
        this.m_rectTouch.top = this.m_rect.top - f;
        float f2 = (float) 10;
        this.m_rectTouch.width = this.m_rect.width + f2;
        this.m_rectTouch.height = this.m_rect.height + f2;
        if (this.m_rectTouch.left < 0.0f) {
            this.m_rectTouch.left = 0.0f;
        }
        if (this.m_rectTouch.top < 0.0f) {
            this.m_rectTouch.top = 0.0f;
        }
        return this.m_rectTouch.PtInRect(cPoint);
    }

    public boolean isEnable() {
        return this.m_bEnable;
    }

    public void setEnable(boolean z) {
        this.m_bEnable = z;
    }

    public boolean isVisible() {
        return this.m_bVisible;
    }

    public void setVisible(boolean z) {
        this.m_bVisible = z;
    }

    public boolean isUseful() {
        if (isVisible() && isEnable()) {
            return true;
        }
        return false;
    }

    public int getState() {
        return this.m_state;
    }

    public void setState(int i) {
        this.m_state = i;
    }

    public void setNormal() {
        this.m_state = 1;
    }

    public void setFocus() {
        this.m_state = 2;
    }

    public int getCommand() {
        return this.m_command;
    }

    public void setCommand(int i) {
        this.m_command = i;
    }
}
