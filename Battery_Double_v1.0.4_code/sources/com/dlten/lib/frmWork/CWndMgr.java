package com.dlten.lib.frmWork;

import com.dlten.lib.CBaseView;
import com.dlten.lib.STD;
import com.dlten.lib.graphics.CDCView;

public abstract class CWndMgr implements Runnable {
    private static CWndMgr _instance;
    private CDCView m_DCView;
    private CBaseView m_baseView;
    private CWnd m_pCurShowWnd;
    private CEventWnd m_pOldFore;
    private CEventWnd m_pOldModaless;
    private Thread m_thread = null;

    public int SwitchWindow(int i) {
        return 0;
    }

    public int SwitchWindow(int i, int i2) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public abstract CWnd createWindow(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract void runProc();

    public static CWndMgr getInstance() {
        if (_instance == null) {
            STD.ASSERT(false);
        }
        return _instance;
    }

    public CWndMgr(CBaseView cBaseView) {
        _instance = this;
        this.m_DCView = cBaseView;
        this.m_baseView = cBaseView;
        CEventWnd.setView(this.m_baseView);
        this.m_pCurShowWnd = null;
        this.m_pOldModaless = null;
        this.m_pOldFore = null;
    }

    public final void start() {
        this.m_thread = new Thread(this);
        if (this.m_thread != null) {
            this.m_thread.start();
        }
    }

    public final void stop() {
        if (this.m_thread != null) {
            boolean z = true;
            while (z) {
                try {
                    this.m_thread.join();
                    z = false;
                } catch (InterruptedException unused) {
                }
            }
            this.m_thread = null;
        }
    }

    public final void suspend() {
        if (!(this.m_thread == null || this.m_pCurShowWnd == null)) {
            this.m_pCurShowWnd.OnSuspend();
        }
    }

    public final void resume() {
        if (!(this.m_thread == null || this.m_pCurShowWnd == null)) {
            this.m_pCurShowWnd.OnResume();
        }
    }

    public final void run() {
        Initialize();
        runProc();
        Finalize();
    }

    public final void SendMessage(int i, int i2, int i3) {
        if (this.m_pCurShowWnd != null) {
            this.m_pCurShowWnd.SendMessage(i, i2, i3);
        }
    }

    public final CWnd GetCurWnd() {
        return this.m_pCurShowWnd;
    }

    /* access modifiers changed from: protected */
    public void Initialize() {
        this.m_DCView.prepareDC();
        CEventWnd.prepareDC();
    }

    /* access modifiers changed from: protected */
    public void Finalize() {
        this.m_DCView.releaseDC();
    }

    /* access modifiers changed from: protected */
    public final int NewWindow(int i) {
        return NewWindow(i, 0);
    }

    /* access modifiers changed from: protected */
    public final int NewWindow(int i, int i2) {
        this.m_pCurShowWnd = createWindow(i, i2);
        if (this.m_pCurShowWnd == null) {
            return -1;
        }
        int WndEventLoop = WndEventLoop(this.m_pCurShowWnd);
        this.m_pCurShowWnd = null;
        return WndEventLoop;
    }

    private final int WndEventLoop(CWnd cWnd) {
        this.m_pCurShowWnd = cWnd;
        cWnd.SetActiveWnd(cWnd);
        cWnd.OnInitWindow();
        cWnd.OnLoadResource();
        if (cWnd.m_strName != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("\tShow(");
            sb.append(cWnd.m_strName);
            sb.append(")");
            STD.logout(sb.toString());
        }
        cWnd.OnShowWindow();
        if (cWnd.m_strName != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("\tRun(");
            sb2.append(cWnd.m_strName);
            sb2.append(")");
            STD.logout(sb2.toString());
        }
        int RunProc = cWnd.RunProc();
        if (cWnd.m_strName != null) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("\tDestroy(");
            sb3.append(cWnd.m_strName);
            sb3.append(")");
            STD.logout(sb3.toString());
        }
        cWnd.OnDestroy();
        return RunProc;
    }

    /* access modifiers changed from: protected */
    public final int SwitchingWnd(int i) {
        return SwitchingWnd(i, 0);
    }

    /* access modifiers changed from: protected */
    public final int SwitchingWnd(int i, int i2) {
        return SwitchWindow_Proc(createWindow(i, i2), this.m_pCurShowWnd);
    }

    /* access modifiers changed from: protected */
    public final void SwitchWinodw_Prepare() {
        this.m_pCurShowWnd.OnDestroy();
    }

    private final int SwitchWindow_Proc(CWnd cWnd, CWnd cWnd2) {
        cWnd.SetParent(cWnd2);
        int WndEventLoop = WndEventLoop(cWnd);
        this.m_pCurShowWnd = cWnd2;
        return WndEventLoop;
    }

    /* access modifiers changed from: protected */
    public final void SwitchWinodw_Finish() {
        this.m_pCurShowWnd.OnShowWindow();
    }

    public final int DialogDoModal(CDialog cDialog, CEventWnd cEventWnd) {
        cDialog.SetParent(cEventWnd);
        boolean EnableWindow = cDialog.GetParent().EnableWindow(false);
        CEventWnd SetActiveWnd = cEventWnd.SetActiveWnd(cDialog);
        CEventWnd SetForegroundWnd = cEventWnd.SetForegroundWnd(cDialog);
        cDialog.OnInitWindow();
        cDialog.OnLoadResource();
        if (cDialog.m_strName != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("\tShow(");
            sb.append(cDialog.m_strName);
            sb.append(")");
            STD.logout(sb.toString());
        }
        cDialog.OnShowWindow();
        if (cDialog.m_strName != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("\tRun(");
            sb2.append(cDialog.m_strName);
            sb2.append(")");
            STD.logout(sb2.toString());
        }
        int RunProc = cDialog.RunProc();
        if (cDialog.m_strName != null) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("\tDestroy(");
            sb3.append(cDialog.m_strName);
            sb3.append(")");
            STD.logout(sb3.toString());
        }
        cDialog.OnDestroy();
        cEventWnd.SetForegroundWnd(SetForegroundWnd);
        cEventWnd.SetActiveWnd(SetActiveWnd);
        cDialog.GetParent().EnableWindow(EnableWindow);
        return RunProc;
    }

    public final void DialogModaless(CDialog cDialog, CEventWnd cEventWnd) {
        cDialog.SetParent(cEventWnd);
        this.m_pOldModaless = cEventWnd.SetModalessWnd(cDialog);
        this.m_pOldFore = cEventWnd.SetForegroundWnd(cDialog);
        cDialog.OnInitWindow();
        cDialog.OnLoadResource();
        cDialog.OnShowWindow();
    }

    public final void DialogDestroy(CDialog cDialog, CEventWnd cEventWnd) {
        cEventWnd.SetForegroundWnd(this.m_pOldFore);
        cEventWnd.SetModalessWnd(this.m_pOldModaless);
        cDialog.OnDestroy();
        cDialog.GetParent().UpdateWindow();
    }
}
