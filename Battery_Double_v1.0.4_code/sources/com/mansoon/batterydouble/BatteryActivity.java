package com.mansoon.batterydouble;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.dlten.lib.frmWork.HandleActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.mansoon.BatteryDouble.C0002R;

public class BatteryActivity extends HandleActivity implements OnClickListener {
    private static String APPLICATION_ID = "510b55ea4ae52f120000002d";
    public static final int MSG_CHANGE_VIEW = 1;
    public static final int MSG_SETTING_VIEW = 2;
    public static final int MSG_SETTING_VIEW1 = 3;
    final int STATE_CHARGING = 1;
    final int STATE_DISCHARGING = 2;
    final int STATE_FULL = 0;
    final int STATE_NOTCHARGING = 3;
    private AdView adView;
    private BroadcastReceiver battery_receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean booleanExtra = intent.getBooleanExtra("present", false);
            BatteryActivity.this.m_strTech = intent.getStringExtra("technology");
            int intExtra = intent.getIntExtra("plugged", -1);
            intent.getIntExtra("scale", -1);
            int intExtra2 = intent.getIntExtra("health", 0);
            int intExtra3 = intent.getIntExtra(NotificationCompat.CATEGORY_STATUS, 0);
            int intExtra4 = intent.getIntExtra(Param.LEVEL, -1);
            BatteryActivity.this.m_nVoltage = intent.getIntExtra("voltage", 0);
            Log.i("BatteryLevel", intent.getExtras().toString());
            if (booleanExtra) {
                BatteryActivity.this.m_nLevel = intExtra4;
                BatteryActivity.this.m_nLevel1 = Math.max((intExtra4 - 50) * 2, 0);
                BatteryActivity.this.m_nLevel2 = Math.min(intExtra4 * 2, 100);
                BatteryActivity.this.m_strPlug = BatteryActivity.this.getPlugTypeString(intExtra);
                BatteryActivity.this.m_strHealth = BatteryActivity.this.getHealthString(intExtra2);
                BatteryActivity.this.getStatusString(intExtra3);
                return;
            }
            BatteryActivity.this.setBatteryLevelText("Battery not present!!!");
        }
    };
    InterstitialAd mInterstitialAd;
    FirebaseRemoteConfig mRemoteConfig;
    private boolean m_bShowHelp = false;
    boolean m_bShowSetting = false;
    private Button m_btnHelpBack = null;
    private RelativeLayout m_lytHelp = null;
    private RelativeLayout m_lytSet = null;
    int m_nHeightPixels;
    int m_nLevel;
    int m_nLevel1;
    int m_nLevel2;
    int m_nState;
    int m_nTheme;
    int m_nVoltage;
    int m_nWidthPixels;
    String m_strHealth;
    String m_strPlug;
    String m_strTech;
    private WebView m_viewHelp = null;
    private frmView m_viewMain = null;

    /* access modifiers changed from: private */
    public String getHealthString(int i) {
        String str = "Unknown";
        switch (i) {
            case 2:
                return "Good";
            case 3:
                return "Over Heat";
            case 4:
                return "Dead";
            case 5:
                return "Over Voltage";
            case 6:
                return "Failure";
            default:
                return str;
        }
    }

    /* access modifiers changed from: private */
    public String getPlugTypeString(int i) {
        String str = "Unknown";
        switch (i) {
            case 1:
                return "AC";
            case 2:
                return "USB";
            default:
                return str;
        }
    }

    /* access modifiers changed from: private */
    public void setBatteryLevelText(String str) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.m_nWidthPixels = displayMetrics.widthPixels;
        this.m_nHeightPixels = displayMetrics.heightPixels;
        registerBatteryLevelReceiver();
        setContentView(C0002R.layout.main);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(C0002R.id.relativeLayout1);
        createView_Main(relativeLayout);
        createView_Help(relativeLayout);
        this.m_bShowSetting = false;
        this.m_lytSet.setVisibility(8);
        this.m_nTheme = 1;
        this.adView = (AdView) findViewById(C0002R.id.adView);
        this.adView.loadAd(new Builder().addTestDevice("2F4FCE3D2D008DBE17DAF1B46F033D6B").build());
        this.mInterstitialAd = new InterstitialAd(this);
        this.mInterstitialAd.setAdUnitId("ca-app-pub-7372783568928829/2035331486");
        this.mInterstitialAd.setAdListener(new AdListener() {
            public void onAdClosed() {
                BatteryActivity.this.requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        showHelp(false);
    }

    /* access modifiers changed from: private */
    public void requestNewInterstitial() {
        this.mInterstitialAd.loadAd(new Builder().addTestDevice("2F4FCE3D2D008DBE17DAF1B46F033D6B").build());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.battery_receiver);
        if (this.m_viewMain != null) {
            this.m_viewMain.Finish();
        }
        this.m_viewMain = null;
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.m_viewMain == null) {
            return super.onKeyDown(i, keyEvent);
        }
        if (i == 4) {
            this.m_viewMain.PostMessage(12, 4096, 0);
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            this.m_viewMain.PostMessage(12, 1024, 0);
        }
        return true;
    }

    public void onRecvMessage(int i, int i2, int i3) {
        switch (i) {
            case 1:
                if (i2 == 0) {
                    showHelp(false);
                    return;
                } else {
                    showHelp(true);
                    return;
                }
            case 2:
                this.m_bShowSetting = true;
                this.m_lytSet.setVisibility(0);
                if (this.mInterstitialAd.isLoaded()) {
                    this.mInterstitialAd.show();
                    return;
                }
                return;
            case 3:
                this.m_bShowSetting = false;
                this.m_lytSet.setVisibility(8);
                this.m_viewMain.PostMessage(16, this.m_nTheme, this.m_nTheme);
                return;
            default:
                return;
        }
    }

    private void createView_Main(RelativeLayout relativeLayout) {
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        this.m_viewMain = new frmView(this);
        relativeLayout.addView(this.m_viewMain, layoutParams);
    }

    private void createView_Help(RelativeLayout relativeLayout) {
        this.m_lytHelp = (RelativeLayout) findViewById(C0002R.id.relativeLayout3);
        this.m_lytSet = (RelativeLayout) findViewById(C0002R.id.setting_layout);
        this.m_btnHelpBack = (Button) findViewById(C0002R.id.wndhelp_btn_back);
        this.m_btnHelpBack.setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_1)).setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_2)).setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_3)).setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_4)).setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_5)).setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_6)).setOnClickListener(this);
        ((Button) findViewById(C0002R.id.btn_set_back)).setOnClickListener(this);
        this.m_viewHelp = (WebView) findViewById(C0002R.id.webView1);
        this.m_viewHelp.getSettings().setJavaScriptEnabled(true);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id != C0002R.id.wndhelp_btn_back) {
            switch (id) {
                case C0002R.id.btn_set_1 /*2131165223*/:
                    setBackImage(1);
                    return;
                case C0002R.id.btn_set_2 /*2131165224*/:
                    setBackImage(2);
                    return;
                case C0002R.id.btn_set_3 /*2131165225*/:
                    setBackImage(3);
                    return;
                case C0002R.id.btn_set_4 /*2131165226*/:
                    setBackImage(4);
                    return;
                case C0002R.id.btn_set_5 /*2131165227*/:
                    setBackImage(5);
                    return;
                case C0002R.id.btn_set_6 /*2131165228*/:
                    setBackImage(6);
                    return;
                case C0002R.id.btn_set_back /*2131165229*/:
                    this.m_bShowSetting = false;
                    this.m_lytSet.setVisibility(8);
                    this.m_viewMain.PostMessage(16, this.m_nTheme, this.m_nTheme);
                    return;
                default:
                    return;
            }
        } else if (this.m_bShowHelp) {
            showHelp(false);
        }
    }

    public void setBackImage(int i) {
        this.m_nTheme = i;
        ImageView imageView = (ImageView) findViewById(C0002R.id.wnd_img);
        switch (i) {
            case 1:
                imageView.setBackgroundResource(C0002R.drawable.set_img1);
                return;
            case 2:
                imageView.setBackgroundResource(C0002R.drawable.set_img2);
                return;
            case 3:
                imageView.setBackgroundResource(C0002R.drawable.set_img3);
                return;
            case 4:
                imageView.setBackgroundResource(C0002R.drawable.set_img4);
                return;
            case 5:
                imageView.setBackgroundResource(C0002R.drawable.set_img5);
                return;
            case 6:
                imageView.setBackgroundResource(C0002R.drawable.set_img6);
                return;
            default:
                return;
        }
    }

    public void showHelp(boolean z) {
        if (z) {
            this.m_viewMain.setVisibility(0);
            this.m_viewHelp.setVisibility(8);
            this.m_lytHelp.setVisibility(8);
            this.m_btnHelpBack.setVisibility(8);
            this.mRemoteConfig = FirebaseRemoteConfig.getInstance();
            this.mRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());
            long j = 3600;
            if (this.mRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
                j = 0;
            }
            this.mRemoteConfig.fetch(j).addOnCompleteListener((Activity) this, (OnCompleteListener<TResult>) new OnCompleteListener<Void>() {
                public void onComplete(Task<Void> task) {
                    if (task.isSuccessful()) {
                        BatteryActivity.this.mRemoteConfig.activateFetched();
                        String string = BatteryActivity.this.mRemoteConfig.getString("promo_url");
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(string));
                        BatteryActivity.this.startActivity(intent);
                        StringBuilder sb = new StringBuilder();
                        sb.append("mRemoteConfig.getString");
                        sb.append(BatteryActivity.this.mRemoteConfig.getString("promo_url"));
                        Log.e("", sb.toString());
                    }
                }
            });
        } else {
            this.m_viewMain.setVisibility(0);
            this.m_viewHelp.setVisibility(8);
            this.m_lytHelp.setVisibility(8);
            this.m_btnHelpBack.setVisibility(8);
        }
        this.m_bShowHelp = z;
        ((RelativeLayout) findViewById(C0002R.id.relativeLayout1)).invalidate();
    }

    /* access modifiers changed from: private */
    public void getStatusString(int i) {
        switch (i) {
            case 2:
                this.m_nState = 1;
                return;
            case 3:
                this.m_nState = 2;
                return;
            case 4:
                this.m_nState = 3;
                return;
            case 5:
                this.m_nState = 0;
                return;
            default:
                return;
        }
    }

    private void registerBatteryLevelReceiver() {
        registerReceiver(this.battery_receiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }
}
