package com.mansoon.batterydouble;

import com.dlten.lib.STD;
import com.dlten.lib.file.CConFile;
import com.dlten.lib.frmWork.CWnd;
import com.dlten.lib.frmWork.CWndMgr;
import com.dlten.lib.graphics.CImage;

public class frmWndMgr extends CWndMgr {
    public static final int WND_DESTROYAPP = 0;
    public static final int WND_GAME = 10;
    public static final int WND_GAMEEND = 14;
    public static final int WND_GAMEOVER = 13;
    public static final int WND_LOGO = 3;
    public static final int WND_RESULT = 12;
    public static final int WND_SELCHR = 7;
    public static final int WND_SELFREENPC = 6;
    public static final int WND_SELRULE = 5;
    public static final int WND_SELUNIFYNPC = 9;
    public static final int WND_TEST1 = 1;
    public static final int WND_TEST2 = 2;
    public static final int WND_TITLE = 4;
    public static final int WND_UNIFYMENU = 8;
    public static final int WND_VICTORY = 11;
    private BatteryActivity m_activity;
    private frmView m_view;

    public frmWndMgr(BatteryActivity batteryActivity, frmView frmview) {
        super(frmview);
        this.m_activity = batteryActivity;
        this.m_view = frmview;
        this.m_view.calcFps(true);
    }

    /* access modifiers changed from: protected */
    public void Initialize() {
        super.Initialize();
        STD.initRand();
        CConFile.Initialize(this.m_activity);
        CImage.Initialize(this.m_view);
    }

    /* access modifiers changed from: protected */
    public void Finalize() {
        super.Finalize();
        this.m_activity.finish();
    }

    /* access modifiers changed from: protected */
    public void runProc() {
        int i = 1;
        while (i > 0) {
            i = NewWindow(i);
        }
    }

    /* access modifiers changed from: protected */
    public CWnd createWindow(int i, int i2) {
        WndGameWnd wndGameWnd = new WndGameWnd();
        Runtime.getRuntime().gc();
        return wndGameWnd;
    }
}
