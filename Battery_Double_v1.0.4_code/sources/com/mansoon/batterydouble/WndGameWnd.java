package com.mansoon.batterydouble;

import android.support.v4.view.ViewCompat;
import com.dlten.lib.frmWork.CButton;
import com.dlten.lib.graphics.CImgObj;
import com.dlten.lib.graphics.CPoint;

public class WndGameWnd extends WndCommon {
    final int BTN_MOREBTN = 0;
    final int BTN_SETTING = 1;
    final int PRO_HEIGHT = 370;
    final int PRO_WIDTH = 244;
    final int STATE_CHARGING = 1;
    final int STATE_DISCHARGING = 2;
    final int STATE_FULL = 0;
    final int STATE_NOTCHARGING = 3;
    final int TIME_AUDIO = 20;
    final int TIME_INTERNET = 5;
    final int TIME_STANDBY = 100;
    final int TIME_TALK = 10;
    final int TIME_VIDEO = 4;
    final int XINTERVAL_TEXT = 380;
    final int XPOS_PRO1 = 16;
    final int XPOS_PRO2 = 376;
    final int XPOS_TEXT = 100;
    final int YINTERVAL_TEXT = 36;
    final int YPOS_PRO1 = 144;
    final int YPOS_PRO2 = 144;
    final int YPOS_TEXT = 536;
    private BatteryActivity m_activity;
    private CImgObj m_imgBack = null;
    private CImgObj m_imgBatteryBoxBackLeft = null;
    private CImgObj m_imgBatteryBoxBackOrg = null;
    private CImgObj m_imgBatteryBoxBackRight = null;
    private CImgObj m_imgBatteryBoxLeft = null;
    private CImgObj m_imgBatteryBoxRight = null;
    private CImgObj m_imgCharge = null;
    private CButton m_moreGame = null;
    private CPoint m_pos = new CPoint();
    private CButton m_settGame = null;
    private String m_strAudio;
    private String m_strInternet;
    private String m_strLevel;
    private String m_strLevel1;
    private String m_strLevel2;
    private String m_strStandby;
    private String m_strTalk;
    private String m_strVideo;
    private String m_strVoltage;

    public void OnMenu() {
    }

    public void debug_drawStatus() {
    }

    public void OnLoadResource() {
        this.m_activity = (BatteryActivity) getView().getActivity();
        setString("logo Wnd");
        this.m_imgBack = new CImgObj("background.jpg");
        this.m_imgBatteryBoxLeft = new CImgObj("batterydocklet.png");
        this.m_imgBatteryBoxBackOrg = new CImgObj("n1.png");
        if (this.m_activity.m_nLevel1 > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("n");
            sb.append(this.m_activity.m_nTheme);
            sb.append(".png");
            this.m_imgBatteryBoxBackLeft = new CImgObj(sb.toString(), this.m_activity.m_nLevel1);
        }
        if (this.m_activity.m_nLevel2 > 0) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("n");
            sb2.append(this.m_activity.m_nTheme);
            sb2.append(".png");
            this.m_imgBatteryBoxBackRight = new CImgObj(sb2.toString(), this.m_activity.m_nLevel2);
        }
        this.m_imgBatteryBoxRight = new CImgObj("batterydocklet.png");
        this.m_imgCharge = new CImgObj("charging.png");
        calcValues();
        createButton();
    }

    public void createButton() {
        CButton createButton = createButton("more.png", "more.png", "more.png");
        createButton.setPoint(520.0f, 600.0f);
        createButton.setCommand(0);
        this.m_moreGame = createButton;
        CButton createButton2 = createButton("set.png", "set.png", "set.png");
        createButton2.setPoint(520.0f, 720.0f);
        createButton2.setCommand(1);
        this.m_settGame = createButton2;
    }

    public void OnGameMore() {
        BatteryActivity batteryActivity = this.m_activity;
        postActivityMsg(1, 1, 0);
    }

    public void OnGameSetting() {
        BatteryActivity batteryActivity = this.m_activity;
        postActivityMsg(2, 1, 0);
    }

    public void OnCommand(int i) {
        switch (i) {
            case 0:
                OnGameMore();
                return;
            case 1:
                OnGameSetting();
                return;
            default:
                return;
        }
    }

    public void calcValues() {
        float f = (float) (((double) ((float) (100 * this.m_activity.m_nLevel))) / 100.0d);
        int i = (int) f;
        this.m_strStandby = String.format(" %d hr %d min", new Object[]{Integer.valueOf(i), Integer.valueOf((int) ((f - ((float) i)) * 60.0f))});
        float f2 = (float) (((double) ((float) (20 * this.m_activity.m_nLevel))) / 100.0d);
        int i2 = (int) f2;
        this.m_strAudio = String.format(" %d hr %d min", new Object[]{Integer.valueOf(i2), Integer.valueOf((int) ((f2 - ((float) i2)) * 60.0f))});
        float f3 = (float) (((double) ((float) (4 * this.m_activity.m_nLevel))) / 100.0d);
        int i3 = (int) f3;
        this.m_strVideo = String.format(" %d hr %d min", new Object[]{Integer.valueOf(i3), Integer.valueOf((int) ((f3 - ((float) i3)) * 60.0f))});
        float f4 = (float) (((double) ((float) (5 * this.m_activity.m_nLevel))) / 100.0d);
        int i4 = (int) f4;
        this.m_strInternet = String.format(" %d hr %d min", new Object[]{Integer.valueOf(i4), Integer.valueOf((int) ((f4 - ((float) i4)) * 60.0f))});
        float f5 = (float) (((double) ((float) (10 * this.m_activity.m_nLevel))) / 100.0d);
        int i5 = (int) f5;
        this.m_strTalk = String.format(" %d hr %d min", new Object[]{Integer.valueOf(i5), Integer.valueOf((int) ((f5 - ((float) i5)) * 60.0f))});
        this.m_strVoltage = String.format("%d v", new Object[]{Integer.valueOf(this.m_activity.m_nVoltage)});
        this.m_strLevel = String.format("Battery Level Total %d", new Object[]{Integer.valueOf(this.m_activity.m_nLevel)});
        StringBuilder sb = new StringBuilder();
        sb.append(this.m_strLevel);
        sb.append("%");
        this.m_strLevel = sb.toString();
        this.m_strLevel1 = String.format("%d", new Object[]{Integer.valueOf(this.m_activity.m_nLevel1)});
        StringBuilder sb2 = new StringBuilder();
        sb2.append(this.m_strLevel1);
        sb2.append("%");
        this.m_strLevel1 = sb2.toString();
        this.m_strLevel2 = String.format("%d", new Object[]{Integer.valueOf(this.m_activity.m_nLevel2)});
        StringBuilder sb3 = new StringBuilder();
        sb3.append(this.m_strLevel2);
        sb3.append("%");
        this.m_strLevel2 = sb3.toString();
    }

    public void OnDestroy() {
        this.m_imgBack = unload(this.m_imgBack);
        this.m_imgBatteryBoxLeft = unload(this.m_imgBatteryBoxLeft);
        this.m_imgBatteryBoxBackOrg = unload(this.m_imgBatteryBoxBackOrg);
        this.m_imgBatteryBoxBackLeft = unload(this.m_imgBatteryBoxBackLeft);
        this.m_imgBatteryBoxBackRight = unload(this.m_imgBatteryBoxBackRight);
        this.m_imgBatteryBoxRight = unload(this.m_imgBatteryBoxRight);
        this.m_imgCharge = unload(this.m_imgCharge);
        super.OnDestroy();
    }

    private void drawBackGround() {
        if (this.m_imgBack != null) {
            this.m_imgBack.draw(0.0f, 0.0f);
        }
    }

    public void OnPaint() {
        drawBackGround();
        drawBatteryPRoRect();
        drawBattery();
        drawCharging();
        drawString();
        drawStringValue();
        drawLevel();
    }

    private void drawBattery() {
        this.m_imgBatteryBoxLeft.getSizeY();
        float sizeX = this.m_imgBatteryBoxLeft.getSizeX();
        CPoint cPoint = new CPoint();
        cPoint.x = (584.0f - (sizeX * 2.0f)) / 2.0f;
        cPoint.y = 76.0f;
        this.m_imgBatteryBoxLeft.draw(cPoint);
        cPoint.x = 348.0f;
        this.m_imgBatteryBoxRight.draw(cPoint);
    }

    private void drawCharging() {
        if (this.m_activity.m_nState == 0 || this.m_activity.m_nState == 1) {
            this.m_imgCharge.draw(320.0f - (this.m_imgCharge.getSizeY() / 2.0f), 420.0f);
        }
    }

    private void drawString() {
        getView().setFontSize(34);
        drawStr(100, 572, ViewCompat.MEASURED_SIZE_MASK, 18, "Technology:");
        drawStr(100, 608, ViewCompat.MEASURED_SIZE_MASK, 18, "Health:");
        drawStr(100, 644, ViewCompat.MEASURED_SIZE_MASK, 18, "Voltage:");
        drawStr(100, 680, ViewCompat.MEASURED_SIZE_MASK, 18, "Standby:");
        drawStr(100, 716, ViewCompat.MEASURED_SIZE_MASK, 18, "Audio:");
        drawStr(100, 752, ViewCompat.MEASURED_SIZE_MASK, 18, "Video:");
        drawStr(100, 788, ViewCompat.MEASURED_SIZE_MASK, 18, "Internet:");
        drawStr(100, 824, ViewCompat.MEASURED_SIZE_MASK, 18, "Talk:");
    }

    private void drawStringValue() {
        getView().setFontSize(34);
        drawStr(480, 572, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_activity.m_strTech);
        drawStr(480, 608, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_activity.m_strHealth);
        drawStr(480, 644, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strVoltage);
        drawStr(480, 680, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strStandby);
        drawStr(480, 716, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strAudio);
        drawStr(480, 752, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strVideo);
        drawStr(480, 788, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strInternet);
        drawStr(480, 824, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strTalk);
    }

    private void drawLevel() {
        getView().setFontSize(44);
        drawStr(320, 42, ViewCompat.MEASURED_SIZE_MASK, 34, this.m_strLevel);
        getView().setFontSize(36);
        drawStr(155, 475, 13092807, 34, this.m_strLevel1);
        drawStr(515, 475, 13092807, 34, this.m_strLevel2);
    }

    private void drawBatteryPRoRect() {
        float sizeX = this.m_imgBatteryBoxLeft.getSizeX();
        float sizeY = this.m_imgBatteryBoxBackOrg.getSizeY();
        if (this.m_imgBatteryBoxBackLeft != null) {
            float sizeY2 = sizeY - this.m_imgBatteryBoxBackLeft.getSizeY();
            CPoint cPoint = new CPoint();
            cPoint.x = ((584.0f - (sizeX * 2.0f)) / 2.0f) + 10.0f;
            cPoint.y = sizeY2 + 114.0f;
            this.m_imgBatteryBoxBackLeft.draw(cPoint);
        }
        if (this.m_imgBatteryBoxBackRight != null) {
            float sizeY3 = sizeY - this.m_imgBatteryBoxBackRight.getSizeY();
            CPoint cPoint2 = new CPoint();
            cPoint2.x = 358.0f;
            cPoint2.y = 114.0f + sizeY3;
            this.m_imgBatteryBoxBackRight.draw(cPoint2);
        }
    }

    private void setProColor(int i) {
        int i2 = 100 - i;
        if (i > 50) {
            getView().setColor(0, 255, 0);
        } else if (i > 10) {
            getView().setColor((2 * i2) + 0, 255 - (1 * i2), 0);
        } else {
            getView().setColor(255, 0, 0);
        }
    }

    public void OnExit() {
        if (this.m_activity.m_bShowSetting) {
            BatteryActivity batteryActivity = this.m_activity;
            postActivityMsg(3, 1, 0);
            return;
        }
        DestroyWindow(0);
    }

    public void OnKeyDown(int i) {
        if (i == 1024) {
            OnMenu();
        } else if (i != 4096) {
            super.OnKeyDown(i);
        } else {
            OnExit();
        }
    }

    public void OnAnimEvent(int i) {
        this.m_imgBatteryBoxBackOrg = unload(this.m_imgBatteryBoxBackOrg);
        this.m_imgBatteryBoxBackLeft = unload(this.m_imgBatteryBoxBackLeft);
        this.m_imgBatteryBoxBackRight = unload(this.m_imgBatteryBoxBackRight);
        StringBuilder sb = new StringBuilder();
        sb.append("n");
        sb.append(this.m_activity.m_nTheme);
        sb.append(".png");
        this.m_imgBatteryBoxBackOrg = new CImgObj(sb.toString());
        if (this.m_activity.m_nLevel1 > 0) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("n");
            sb2.append(this.m_activity.m_nTheme);
            sb2.append(".png");
            this.m_imgBatteryBoxBackLeft = new CImgObj(sb2.toString(), this.m_activity.m_nLevel1);
        }
        if (this.m_activity.m_nLevel2 > 0) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("n");
            sb3.append(this.m_activity.m_nTheme);
            sb3.append(".png");
            this.m_imgBatteryBoxBackRight = new CImgObj(sb3.toString(), this.m_activity.m_nLevel2);
        }
    }
}
