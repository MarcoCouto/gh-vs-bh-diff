package android.support.constraint.solver.widgets;

import android.support.constraint.solver.LinearSystem;
import android.support.constraint.solver.Metrics;
import android.support.constraint.solver.widgets.ConstraintWidget.DimensionBehaviour;

public class Optimizer {
    static final int FLAG_CHAIN_DANGLING = 1;
    static final int FLAG_RECOMPUTE_BOUNDS = 2;
    static final int FLAG_USE_OPTIMIZE = 0;
    public static final int OPTIMIZATION_BARRIER = 2;
    public static final int OPTIMIZATION_CHAIN = 4;
    public static final int OPTIMIZATION_DIMENSIONS = 8;
    public static final int OPTIMIZATION_DIRECT = 1;
    public static final int OPTIMIZATION_NONE = 0;
    public static final int OPTIMIZATION_RATIO = 16;
    public static final int OPTIMIZATION_STANDARD = 3;
    static boolean[] flags = new boolean[3];

    static void checkMatchParent(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, ConstraintWidget constraintWidget) {
        if (constraintWidgetContainer.mListDimensionBehaviors[0] != DimensionBehaviour.WRAP_CONTENT && constraintWidget.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_PARENT) {
            int i = constraintWidget.mLeft.mMargin;
            int width = constraintWidgetContainer.getWidth() - constraintWidget.mRight.mMargin;
            constraintWidget.mLeft.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mLeft);
            constraintWidget.mRight.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mRight);
            linearSystem.addEquality(constraintWidget.mLeft.mSolverVariable, i);
            linearSystem.addEquality(constraintWidget.mRight.mSolverVariable, width);
            constraintWidget.mHorizontalResolution = 2;
            constraintWidget.setHorizontalDimension(i, width);
        }
        if (constraintWidgetContainer.mListDimensionBehaviors[1] != DimensionBehaviour.WRAP_CONTENT && constraintWidget.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_PARENT) {
            int i2 = constraintWidget.mTop.mMargin;
            int height = constraintWidgetContainer.getHeight() - constraintWidget.mBottom.mMargin;
            constraintWidget.mTop.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mTop);
            constraintWidget.mBottom.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mBottom);
            linearSystem.addEquality(constraintWidget.mTop.mSolverVariable, i2);
            linearSystem.addEquality(constraintWidget.mBottom.mSolverVariable, height);
            if (constraintWidget.mBaselineDistance > 0 || constraintWidget.getVisibility() == 8) {
                constraintWidget.mBaseline.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mBaseline);
                linearSystem.addEquality(constraintWidget.mBaseline.mSolverVariable, constraintWidget.mBaselineDistance + i2);
            }
            constraintWidget.mVerticalResolution = 2;
            constraintWidget.setVerticalDimension(i2, height);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x003e A[RETURN] */
    private static boolean optimizableMatchConstraint(ConstraintWidget constraintWidget, int i) {
        if (constraintWidget.mListDimensionBehaviors[i] != DimensionBehaviour.MATCH_CONSTRAINT) {
            return false;
        }
        char c = 1;
        if (constraintWidget.mDimensionRatio != 0.0f) {
            DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.mListDimensionBehaviors;
            if (i != 0) {
                c = 0;
            }
            return dimensionBehaviourArr[c] == DimensionBehaviour.MATCH_CONSTRAINT ? false : false;
        }
        if (i == 0) {
            if (constraintWidget.mMatchConstraintDefaultWidth == 0 && constraintWidget.mMatchConstraintMinWidth == 0 && constraintWidget.mMatchConstraintMaxWidth == 0) {
                return true;
            }
            return false;
        } else if (constraintWidget.mMatchConstraintDefaultHeight != 0 || constraintWidget.mMatchConstraintMinHeight != 0 || constraintWidget.mMatchConstraintMaxHeight != 0) {
            return false;
        }
        return true;
    }

    static void analyze(int i, ConstraintWidget constraintWidget) {
        constraintWidget.updateResolutionNodes();
        ResolutionAnchor resolutionNode = constraintWidget.mLeft.getResolutionNode();
        ResolutionAnchor resolutionNode2 = constraintWidget.mTop.getResolutionNode();
        ResolutionAnchor resolutionNode3 = constraintWidget.mRight.getResolutionNode();
        ResolutionAnchor resolutionNode4 = constraintWidget.mBottom.getResolutionNode();
        boolean z = (i & 8) == 8;
        if (!(resolutionNode.type == 4 || resolutionNode3.type == 4)) {
            if (constraintWidget.mListDimensionBehaviors[0] == DimensionBehaviour.FIXED) {
                if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget == null) {
                    resolutionNode.setType(1);
                    resolutionNode3.setType(1);
                    if (z) {
                        resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                    } else {
                        resolutionNode3.dependsOn(resolutionNode, constraintWidget.getWidth());
                    }
                } else if (constraintWidget.mLeft.mTarget != null && constraintWidget.mRight.mTarget == null) {
                    resolutionNode.setType(1);
                    resolutionNode3.setType(1);
                    if (z) {
                        resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                    } else {
                        resolutionNode3.dependsOn(resolutionNode, constraintWidget.getWidth());
                    }
                } else if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget != null) {
                    resolutionNode.setType(1);
                    resolutionNode3.setType(1);
                    resolutionNode.dependsOn(resolutionNode3, -constraintWidget.getWidth());
                    if (z) {
                        resolutionNode.dependsOn(resolutionNode3, -1, constraintWidget.getResolutionWidth());
                    } else {
                        resolutionNode.dependsOn(resolutionNode3, -constraintWidget.getWidth());
                    }
                } else if (!(constraintWidget.mLeft.mTarget == null || constraintWidget.mRight.mTarget == null)) {
                    resolutionNode.setType(2);
                    resolutionNode3.setType(2);
                    if (z) {
                        constraintWidget.getResolutionWidth().addDependent(resolutionNode);
                        constraintWidget.getResolutionWidth().addDependent(resolutionNode3);
                        resolutionNode.setOpposite(resolutionNode3, -1, constraintWidget.getResolutionWidth());
                        resolutionNode3.setOpposite(resolutionNode, 1, constraintWidget.getResolutionWidth());
                    } else {
                        resolutionNode.setOpposite(resolutionNode3, (float) (-constraintWidget.getWidth()));
                        resolutionNode3.setOpposite(resolutionNode, (float) constraintWidget.getWidth());
                    }
                }
            } else if (constraintWidget.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && optimizableMatchConstraint(constraintWidget, 0)) {
                int width = constraintWidget.getWidth();
                resolutionNode.setType(1);
                resolutionNode3.setType(1);
                if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget == null) {
                    if (z) {
                        resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                    } else {
                        resolutionNode3.dependsOn(resolutionNode, width);
                    }
                } else if (constraintWidget.mLeft.mTarget == null || constraintWidget.mRight.mTarget != null) {
                    if (constraintWidget.mLeft.mTarget != null || constraintWidget.mRight.mTarget == null) {
                        if (!(constraintWidget.mLeft.mTarget == null || constraintWidget.mRight.mTarget == null)) {
                            if (z) {
                                constraintWidget.getResolutionWidth().addDependent(resolutionNode);
                                constraintWidget.getResolutionWidth().addDependent(resolutionNode3);
                            }
                            if (constraintWidget.mDimensionRatio == 0.0f) {
                                resolutionNode.setType(3);
                                resolutionNode3.setType(3);
                                resolutionNode.setOpposite(resolutionNode3, 0.0f);
                                resolutionNode3.setOpposite(resolutionNode, 0.0f);
                            } else {
                                resolutionNode.setType(2);
                                resolutionNode3.setType(2);
                                resolutionNode.setOpposite(resolutionNode3, (float) (-width));
                                resolutionNode3.setOpposite(resolutionNode, (float) width);
                                constraintWidget.setWidth(width);
                            }
                        }
                    } else if (z) {
                        resolutionNode.dependsOn(resolutionNode3, -1, constraintWidget.getResolutionWidth());
                    } else {
                        resolutionNode.dependsOn(resolutionNode3, -width);
                    }
                } else if (z) {
                    resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                } else {
                    resolutionNode3.dependsOn(resolutionNode, width);
                }
            }
        }
        if (resolutionNode2.type != 4 && resolutionNode4.type != 4) {
            if (constraintWidget.mListDimensionBehaviors[1] == DimensionBehaviour.FIXED) {
                if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget == null) {
                    resolutionNode2.setType(1);
                    resolutionNode4.setType(1);
                    if (z) {
                        resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                    } else {
                        resolutionNode4.dependsOn(resolutionNode2, constraintWidget.getHeight());
                    }
                    if (constraintWidget.mBaseline.mTarget != null) {
                        constraintWidget.mBaseline.getResolutionNode().setType(1);
                        resolutionNode2.dependsOn(1, constraintWidget.mBaseline.getResolutionNode(), -constraintWidget.mBaselineDistance);
                    }
                } else if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget == null) {
                    resolutionNode2.setType(1);
                    resolutionNode4.setType(1);
                    if (z) {
                        resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                    } else {
                        resolutionNode4.dependsOn(resolutionNode2, constraintWidget.getHeight());
                    }
                    if (constraintWidget.mBaselineDistance > 0) {
                        constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                    }
                } else if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget != null) {
                    resolutionNode2.setType(1);
                    resolutionNode4.setType(1);
                    if (z) {
                        resolutionNode2.dependsOn(resolutionNode4, -1, constraintWidget.getResolutionHeight());
                    } else {
                        resolutionNode2.dependsOn(resolutionNode4, -constraintWidget.getHeight());
                    }
                    if (constraintWidget.mBaselineDistance > 0) {
                        constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                    }
                } else if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget != null) {
                    resolutionNode2.setType(2);
                    resolutionNode4.setType(2);
                    if (z) {
                        resolutionNode2.setOpposite(resolutionNode4, -1, constraintWidget.getResolutionHeight());
                        resolutionNode4.setOpposite(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                        constraintWidget.getResolutionHeight().addDependent(resolutionNode2);
                        constraintWidget.getResolutionWidth().addDependent(resolutionNode4);
                    } else {
                        resolutionNode2.setOpposite(resolutionNode4, (float) (-constraintWidget.getHeight()));
                        resolutionNode4.setOpposite(resolutionNode2, (float) constraintWidget.getHeight());
                    }
                    if (constraintWidget.mBaselineDistance > 0) {
                        constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                    }
                }
            } else if (constraintWidget.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT && optimizableMatchConstraint(constraintWidget, 1)) {
                int height = constraintWidget.getHeight();
                resolutionNode2.setType(1);
                resolutionNode4.setType(1);
                if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget == null) {
                    if (z) {
                        resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                    } else {
                        resolutionNode4.dependsOn(resolutionNode2, height);
                    }
                } else if (constraintWidget.mTop.mTarget == null || constraintWidget.mBottom.mTarget != null) {
                    if (constraintWidget.mTop.mTarget != null || constraintWidget.mBottom.mTarget == null) {
                        if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget != null) {
                            if (z) {
                                constraintWidget.getResolutionHeight().addDependent(resolutionNode2);
                                constraintWidget.getResolutionWidth().addDependent(resolutionNode4);
                            }
                            if (constraintWidget.mDimensionRatio == 0.0f) {
                                resolutionNode2.setType(3);
                                resolutionNode4.setType(3);
                                resolutionNode2.setOpposite(resolutionNode4, 0.0f);
                                resolutionNode4.setOpposite(resolutionNode2, 0.0f);
                                return;
                            }
                            resolutionNode2.setType(2);
                            resolutionNode4.setType(2);
                            resolutionNode2.setOpposite(resolutionNode4, (float) (-height));
                            resolutionNode4.setOpposite(resolutionNode2, (float) height);
                            constraintWidget.setHeight(height);
                            if (constraintWidget.mBaselineDistance > 0) {
                                constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                            }
                        }
                    } else if (z) {
                        resolutionNode2.dependsOn(resolutionNode4, -1, constraintWidget.getResolutionHeight());
                    } else {
                        resolutionNode2.dependsOn(resolutionNode4, -height);
                    }
                } else if (z) {
                    resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                } else {
                    resolutionNode4.dependsOn(resolutionNode2, height);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0035, code lost:
        if (r6.mListAnchors[r24].mTarget.mOwner == r5) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0053, code lost:
        if (r5.mHorizontalChainStyle == 2) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0057, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0069, code lost:
        if (r5.mVerticalChainStyle == 2) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x012d, code lost:
        if (r3.mListAnchors[r24].mTarget.mOwner == r9) goto L_0x0130;
     */
    static boolean applyChainOptimized(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, int i, int i2, ConstraintWidget constraintWidget) {
        ConstraintWidget constraintWidget2;
        boolean z;
        boolean z2;
        float f;
        float f2;
        float f3;
        ResolutionAnchor resolutionAnchor;
        ResolutionAnchor resolutionAnchor2;
        ConstraintWidget constraintWidget3;
        ConstraintWidget constraintWidget4;
        LinearSystem linearSystem2 = linearSystem;
        DimensionBehaviour dimensionBehaviour = constraintWidgetContainer.mListDimensionBehaviors[i];
        DimensionBehaviour dimensionBehaviour2 = DimensionBehaviour.WRAP_CONTENT;
        if (i != 0 || !constraintWidgetContainer.isRtl()) {
            constraintWidget2 = constraintWidget;
        } else {
            constraintWidget2 = constraintWidget;
            boolean z3 = false;
            while (!z3) {
                ConstraintAnchor constraintAnchor = constraintWidget2.mListAnchors[i2 + 1].mTarget;
                if (constraintAnchor != null) {
                    constraintWidget4 = constraintAnchor.mOwner;
                    if (constraintWidget4.mListAnchors[i2].mTarget != null) {
                    }
                }
                constraintWidget4 = null;
                if (constraintWidget4 != null) {
                    constraintWidget2 = constraintWidget4;
                } else {
                    z3 = true;
                }
            }
        }
        if (i == 0) {
            z2 = constraintWidget2.mHorizontalChainStyle == 0;
            z = constraintWidget2.mHorizontalChainStyle == 1;
        } else {
            z2 = constraintWidget2.mVerticalChainStyle == 0;
            z = constraintWidget2.mVerticalChainStyle == 1;
        }
        boolean z4 = true;
        ConstraintWidget constraintWidget5 = constraintWidget;
        ConstraintWidget constraintWidget6 = null;
        ConstraintWidget constraintWidget7 = null;
        ConstraintWidget constraintWidget8 = null;
        ConstraintWidget constraintWidget9 = null;
        boolean z5 = false;
        int i3 = 0;
        int i4 = 0;
        float f4 = 0.0f;
        float f5 = 0.0f;
        float f6 = 0.0f;
        while (!z5) {
            constraintWidget5.mListNextVisibleWidget[i] = null;
            if (constraintWidget5.getVisibility() != 8) {
                if (constraintWidget7 != null) {
                    constraintWidget7.mListNextVisibleWidget[i] = constraintWidget5;
                }
                if (constraintWidget8 == null) {
                    constraintWidget8 = constraintWidget5;
                }
                i3++;
                if (i == 0) {
                    f4 += (float) constraintWidget5.getWidth();
                } else {
                    f4 += (float) constraintWidget5.getHeight();
                }
                if (constraintWidget5 != constraintWidget8) {
                    f4 += (float) constraintWidget5.mListAnchors[i2].getMargin();
                }
                f5 = f5 + ((float) constraintWidget5.mListAnchors[i2].getMargin()) + ((float) constraintWidget5.mListAnchors[i2 + 1].getMargin());
                constraintWidget7 = constraintWidget5;
            }
            ConstraintAnchor constraintAnchor2 = constraintWidget5.mListAnchors[i2];
            constraintWidget5.mListNextMatchConstraintsWidget[i] = null;
            if (constraintWidget5.getVisibility() != 8 && constraintWidget5.mListDimensionBehaviors[i] == DimensionBehaviour.MATCH_CONSTRAINT) {
                i4++;
                if (i == 0) {
                    if (constraintWidget5.mMatchConstraintDefaultWidth != 0) {
                        return false;
                    }
                    if (!(constraintWidget5.mMatchConstraintMinWidth == 0 && constraintWidget5.mMatchConstraintMaxWidth == 0)) {
                        return false;
                    }
                } else if (constraintWidget5.mMatchConstraintDefaultHeight != 0) {
                    return false;
                } else {
                    if (!(constraintWidget5.mMatchConstraintMinHeight == 0 && constraintWidget5.mMatchConstraintMaxHeight == 0)) {
                        return false;
                    }
                }
                f6 += constraintWidget5.mWeight[i];
                if (constraintWidget9 == null) {
                    constraintWidget9 = constraintWidget5;
                } else {
                    constraintWidget6.mListNextMatchConstraintsWidget[i] = constraintWidget5;
                }
                constraintWidget6 = constraintWidget5;
            }
            ConstraintAnchor constraintAnchor3 = constraintWidget5.mListAnchors[i2 + 1].mTarget;
            if (constraintAnchor3 != null) {
                constraintWidget3 = constraintAnchor3.mOwner;
                if (constraintWidget3.mListAnchors[i2].mTarget != null) {
                }
            }
            constraintWidget3 = null;
            if (constraintWidget3 != null) {
                constraintWidget5 = constraintWidget3;
            } else {
                z5 = true;
            }
        }
        ResolutionAnchor resolutionNode = constraintWidget.mListAnchors[i2].getResolutionNode();
        int i5 = i2 + 1;
        ResolutionAnchor resolutionNode2 = constraintWidget5.mListAnchors[i5].getResolutionNode();
        if (resolutionNode.target == null || resolutionNode2.target == null) {
            return false;
        }
        if (resolutionNode.target.state != 1 && resolutionNode2.target.state != 1) {
            return false;
        }
        if (i4 > 0 && i4 != i3) {
            return false;
        }
        if (z4 || z2 || z) {
            f = constraintWidget8 != null ? (float) constraintWidget8.mListAnchors[i2].getMargin() : 0.0f;
            if (constraintWidget7 != null) {
                f += (float) constraintWidget7.mListAnchors[i5].getMargin();
            }
        } else {
            f = 0.0f;
        }
        float f7 = resolutionNode.target.resolvedOffset;
        float f8 = resolutionNode2.target.resolvedOffset;
        float f9 = f7 < f8 ? (f8 - f7) - f4 : (f7 - f8) - f4;
        if (i4 <= 0 || i4 != i3) {
            ResolutionAnchor resolutionAnchor3 = resolutionNode;
            if (f9 < f4) {
                return false;
            }
            if (z4) {
                float horizontalBiasPercent = f7 + ((f9 - f) * constraintWidget.getHorizontalBiasPercent());
                while (constraintWidget8 != null) {
                    if (LinearSystem.sMetrics != null) {
                        Metrics metrics = LinearSystem.sMetrics;
                        metrics.nonresolvedWidgets--;
                        Metrics metrics2 = LinearSystem.sMetrics;
                        metrics2.resolvedWidgets++;
                        Metrics metrics3 = LinearSystem.sMetrics;
                        metrics3.chainConnectionResolved++;
                    }
                    ConstraintWidget constraintWidget10 = constraintWidget8.mListNextVisibleWidget[i];
                    if (constraintWidget10 != null || constraintWidget8 == constraintWidget7) {
                        if (i == 0) {
                            f3 = (float) constraintWidget8.getWidth();
                        } else {
                            f3 = (float) constraintWidget8.getHeight();
                        }
                        float margin = horizontalBiasPercent + ((float) constraintWidget8.mListAnchors[i2].getMargin());
                        constraintWidget8.mListAnchors[i2].getResolutionNode().resolve(resolutionAnchor3.resolvedTarget, margin);
                        float f10 = margin + f3;
                        constraintWidget8.mListAnchors[i5].getResolutionNode().resolve(resolutionAnchor3.resolvedTarget, f10);
                        constraintWidget8.mListAnchors[i2].getResolutionNode().addResolvedValue(linearSystem2);
                        constraintWidget8.mListAnchors[i5].getResolutionNode().addResolvedValue(linearSystem2);
                        horizontalBiasPercent = f10 + ((float) constraintWidget8.mListAnchors[i5].getMargin());
                    }
                    constraintWidget8 = constraintWidget10;
                }
            } else if (z2 || z) {
                if (z2) {
                    f9 -= f;
                } else if (z) {
                    f9 -= f;
                }
                float f11 = f9 / ((float) (i3 + 1));
                if (z) {
                    f11 = i3 > 1 ? f9 / ((float) (i3 - 1)) : f9 / 2.0f;
                }
                float f12 = f7 + f11;
                if (z && i3 > 1) {
                    f12 = ((float) constraintWidget8.mListAnchors[i2].getMargin()) + f7;
                }
                if (z2 && constraintWidget8 != null) {
                    f12 += (float) constraintWidget8.mListAnchors[i2].getMargin();
                }
                while (constraintWidget8 != null) {
                    if (LinearSystem.sMetrics != null) {
                        Metrics metrics4 = LinearSystem.sMetrics;
                        metrics4.nonresolvedWidgets--;
                        Metrics metrics5 = LinearSystem.sMetrics;
                        metrics5.resolvedWidgets++;
                        Metrics metrics6 = LinearSystem.sMetrics;
                        metrics6.chainConnectionResolved++;
                    }
                    ConstraintWidget constraintWidget11 = constraintWidget8.mListNextVisibleWidget[i];
                    if (constraintWidget11 != null || constraintWidget8 == constraintWidget7) {
                        if (i == 0) {
                            f2 = (float) constraintWidget8.getWidth();
                        } else {
                            f2 = (float) constraintWidget8.getHeight();
                        }
                        constraintWidget8.mListAnchors[i2].getResolutionNode().resolve(resolutionAnchor3.resolvedTarget, f12);
                        constraintWidget8.mListAnchors[i5].getResolutionNode().resolve(resolutionAnchor3.resolvedTarget, f12 + f2);
                        constraintWidget8.mListAnchors[i2].getResolutionNode().addResolvedValue(linearSystem2);
                        constraintWidget8.mListAnchors[i5].getResolutionNode().addResolvedValue(linearSystem2);
                        f12 += f2 + f11;
                    }
                    constraintWidget8 = constraintWidget11;
                }
            }
            return true;
        } else if (constraintWidget5.getParent() != null && constraintWidget5.getParent().mListDimensionBehaviors[i] == DimensionBehaviour.WRAP_CONTENT) {
            return false;
        } else {
            float f13 = (f9 + f4) - f5;
            if (z2) {
                f13 -= f5 - f;
            }
            if (z2) {
                f7 += (float) constraintWidget8.mListAnchors[i5].getMargin();
                ConstraintWidget constraintWidget12 = constraintWidget8.mListNextVisibleWidget[i];
                if (constraintWidget12 != null) {
                    f7 += (float) constraintWidget12.mListAnchors[i2].getMargin();
                }
            }
            while (constraintWidget8 != null) {
                if (LinearSystem.sMetrics != null) {
                    Metrics metrics7 = LinearSystem.sMetrics;
                    resolutionAnchor = resolutionNode;
                    metrics7.nonresolvedWidgets--;
                    Metrics metrics8 = LinearSystem.sMetrics;
                    metrics8.resolvedWidgets++;
                    Metrics metrics9 = LinearSystem.sMetrics;
                    metrics9.chainConnectionResolved++;
                } else {
                    resolutionAnchor = resolutionNode;
                }
                ConstraintWidget constraintWidget13 = constraintWidget8.mListNextVisibleWidget[i];
                if (constraintWidget13 != null || constraintWidget8 == constraintWidget7) {
                    float f14 = f13 / ((float) i4);
                    if (f6 > 0.0f) {
                        f14 = (constraintWidget8.mWeight[i] * f13) / f6;
                    }
                    float margin2 = f7 + ((float) constraintWidget8.mListAnchors[i2].getMargin());
                    resolutionAnchor2 = resolutionAnchor;
                    constraintWidget8.mListAnchors[i2].getResolutionNode().resolve(resolutionAnchor2.resolvedTarget, margin2);
                    float f15 = margin2 + f14;
                    constraintWidget8.mListAnchors[i5].getResolutionNode().resolve(resolutionAnchor2.resolvedTarget, f15);
                    constraintWidget8.mListAnchors[i2].getResolutionNode().addResolvedValue(linearSystem2);
                    constraintWidget8.mListAnchors[i5].getResolutionNode().addResolvedValue(linearSystem2);
                    f7 = f15 + ((float) constraintWidget8.mListAnchors[i5].getMargin());
                } else {
                    resolutionAnchor2 = resolutionAnchor;
                }
                constraintWidget8 = constraintWidget13;
                resolutionNode = resolutionAnchor2;
            }
            return true;
        }
    }
}
