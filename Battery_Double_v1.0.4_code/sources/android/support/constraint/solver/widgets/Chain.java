package android.support.constraint.solver.widgets;

import android.support.constraint.solver.ArrayRow;
import android.support.constraint.solver.LinearSystem;
import android.support.constraint.solver.SolverVariable;
import android.support.constraint.solver.widgets.ConstraintWidget.DimensionBehaviour;

class Chain {
    private static final boolean DEBUG = false;

    Chain() {
    }

    static void applyChainConstraints(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, int i) {
        int i2;
        ConstraintWidget[] constraintWidgetArr;
        int i3;
        if (i == 0) {
            int i4 = constraintWidgetContainer.mHorizontalChainsSize;
            constraintWidgetArr = constraintWidgetContainer.mHorizontalChainsArray;
            i2 = i4;
            i3 = 0;
        } else {
            i3 = 2;
            int i5 = constraintWidgetContainer.mVerticalChainsSize;
            i2 = i5;
            constraintWidgetArr = constraintWidgetContainer.mVerticalChainsArray;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            ConstraintWidget constraintWidget = constraintWidgetArr[i6];
            if (!constraintWidgetContainer.optimizeFor(4)) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, i, i3, constraintWidget);
            } else if (!Optimizer.applyChainOptimized(constraintWidgetContainer, linearSystem, i, i3, constraintWidget)) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, i, i3, constraintWidget);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        if (r6.mListAnchors[r42].mTarget.mOwner == r5) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0059, code lost:
        if (r5.mHorizontalChainStyle == 2) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x005d, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x007d, code lost:
        if (r5.mVerticalChainStyle == 2) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x015e, code lost:
        if (r2.mListAnchors[r42].mTarget.mOwner == r13) goto L_0x0161;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01b6 A[LOOP:2: B:104:0x01b6->B:120:0x0210, LOOP_START, PHI: r7 
  PHI: (r7v31 android.support.constraint.solver.widgets.ConstraintWidget) = (r7v6 android.support.constraint.solver.widgets.ConstraintWidget), (r7v32 android.support.constraint.solver.widgets.ConstraintWidget) binds: [B:103:0x01b4, B:120:0x0210] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0372  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x042d  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0467  */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x047d A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x048f  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x0494  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x0499  */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x049f  */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x04a2  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x04ac A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:289:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    static void applyChainConstraints(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, int i, int i2, ConstraintWidget constraintWidget) {
        ConstraintWidget constraintWidget2;
        boolean z;
        boolean z2;
        ConstraintWidget constraintWidget3;
        ConstraintWidget constraintWidget4;
        ConstraintWidget constraintWidget5;
        SolverVariable solverVariable;
        ConstraintWidget constraintWidget6;
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        int i3;
        int i4;
        ConstraintWidget constraintWidget7;
        SolverVariable solverVariable2;
        ConstraintWidget constraintWidget8;
        SolverVariable solverVariable3;
        ConstraintAnchor constraintAnchor4;
        ConstraintWidget constraintWidget9;
        ConstraintAnchor constraintAnchor5;
        SolverVariable solverVariable4;
        ConstraintWidget constraintWidget10;
        ConstraintWidget constraintWidget11;
        ConstraintWidget constraintWidget12;
        SolverVariable solverVariable5;
        ConstraintWidget constraintWidget13;
        ConstraintAnchor constraintAnchor6;
        SolverVariable solverVariable6;
        ConstraintWidget constraintWidget14;
        float f;
        int i5;
        int i6;
        boolean z3;
        ConstraintWidget constraintWidget15;
        ConstraintWidget constraintWidget16;
        ConstraintWidget constraintWidget17;
        ConstraintWidget constraintWidget18;
        ConstraintWidget constraintWidget19;
        ConstraintWidgetContainer constraintWidgetContainer2 = constraintWidgetContainer;
        LinearSystem linearSystem2 = linearSystem;
        ConstraintWidget constraintWidget20 = constraintWidget;
        boolean z4 = constraintWidgetContainer2.mListDimensionBehaviors[i] == DimensionBehaviour.WRAP_CONTENT;
        SolverVariable solverVariable7 = null;
        if (i != 0 || !constraintWidgetContainer.isRtl()) {
            constraintWidget2 = constraintWidget20;
        } else {
            constraintWidget2 = constraintWidget20;
            boolean z5 = false;
            while (!z5) {
                ConstraintAnchor constraintAnchor7 = constraintWidget2.mListAnchors[i2 + 1].mTarget;
                if (constraintAnchor7 != null) {
                    constraintWidget19 = constraintAnchor7.mOwner;
                    if (constraintWidget19.mListAnchors[i2].mTarget != null) {
                    }
                }
                constraintWidget19 = null;
                if (constraintWidget19 != null) {
                    constraintWidget2 = constraintWidget19;
                } else {
                    z5 = true;
                }
            }
        }
        if (i == 0) {
            z = constraintWidget2.mHorizontalChainStyle == 0;
            z2 = constraintWidget2.mHorizontalChainStyle == 1;
        } else {
            z = constraintWidget2.mVerticalChainStyle == 0;
            z2 = constraintWidget2.mVerticalChainStyle == 1;
        }
        boolean z6 = true;
        boolean z7 = z6;
        float f2 = 0.0f;
        boolean z8 = z;
        boolean z9 = z2;
        ConstraintWidget constraintWidget21 = constraintWidget20;
        ConstraintWidget constraintWidget22 = null;
        ConstraintWidget constraintWidget23 = null;
        ConstraintWidget constraintWidget24 = null;
        ConstraintWidget constraintWidget25 = null;
        boolean z10 = false;
        int i7 = 0;
        while (!z10) {
            constraintWidget21.mListNextVisibleWidget[i] = solverVariable7;
            if (constraintWidget21.getVisibility() != 8) {
                if (constraintWidget25 != null) {
                    constraintWidget25.mListNextVisibleWidget[i] = constraintWidget21;
                }
                if (constraintWidget24 == null) {
                    constraintWidget24 = constraintWidget21;
                }
                constraintWidget25 = constraintWidget21;
            }
            ConstraintAnchor constraintAnchor8 = constraintWidget21.mListAnchors[i2];
            int margin = constraintAnchor8.getMargin();
            if (constraintAnchor8.mTarget == null || constraintWidget21 == constraintWidget20) {
                z3 = z10;
            } else {
                z3 = z10;
                if (constraintWidget21.getVisibility() != 8) {
                    margin += constraintAnchor8.mTarget.getMargin();
                }
            }
            int i8 = margin;
            int i9 = (!z7 || constraintWidget21 == constraintWidget20 || constraintWidget21 == constraintWidget24) ? 1 : 6;
            if (constraintWidget21 == constraintWidget24) {
                constraintWidget17 = constraintWidget24;
                constraintWidget16 = constraintWidget25;
                constraintWidget15 = constraintWidget2;
                linearSystem2.addGreaterThan(constraintAnchor8.mSolverVariable, constraintAnchor8.mTarget.mSolverVariable, i8, 5);
            } else {
                constraintWidget15 = constraintWidget2;
                constraintWidget17 = constraintWidget24;
                constraintWidget16 = constraintWidget25;
                linearSystem2.addGreaterThan(constraintAnchor8.mSolverVariable, constraintAnchor8.mTarget.mSolverVariable, i8, 6);
            }
            linearSystem2.addEquality(constraintAnchor8.mSolverVariable, constraintAnchor8.mTarget.mSolverVariable, i8, i9);
            solverVariable7 = null;
            constraintWidget21.mListNextMatchConstraintsWidget[i] = null;
            if (constraintWidget21.getVisibility() != 8 && constraintWidget21.mListDimensionBehaviors[i] == DimensionBehaviour.MATCH_CONSTRAINT) {
                i7++;
                f2 += constraintWidget21.mWeight[i];
                if (constraintWidget23 == null) {
                    constraintWidget23 = constraintWidget21;
                } else {
                    constraintWidget22.mListNextMatchConstraintsWidget[i] = constraintWidget21;
                }
                if (z4) {
                    linearSystem2.addGreaterThan(constraintWidget21.mListAnchors[i2 + 1].mSolverVariable, constraintWidget21.mListAnchors[i2].mSolverVariable, 0, 6);
                }
                constraintWidget22 = constraintWidget21;
            }
            if (z4) {
                linearSystem2.addGreaterThan(constraintWidget21.mListAnchors[i2].mSolverVariable, constraintWidgetContainer2.mListAnchors[i2].mSolverVariable, 0, 6);
            }
            ConstraintAnchor constraintAnchor9 = constraintWidget21.mListAnchors[i2 + 1].mTarget;
            if (constraintAnchor9 != null) {
                constraintWidget18 = constraintAnchor9.mOwner;
                if (constraintWidget18.mListAnchors[i2].mTarget != null) {
                }
            }
            constraintWidget18 = null;
            if (constraintWidget18 != null) {
                constraintWidget21 = constraintWidget18;
                z10 = z3;
            } else {
                z10 = true;
            }
            constraintWidget24 = constraintWidget17;
            constraintWidget25 = constraintWidget16;
            constraintWidget2 = constraintWidget15;
        }
        ConstraintWidget constraintWidget26 = constraintWidget2;
        if (constraintWidget25 != null) {
            int i10 = i2 + 1;
            if (constraintWidget21.mListAnchors[i10].mTarget != null) {
                ConstraintAnchor constraintAnchor10 = constraintWidget25.mListAnchors[i10];
                linearSystem2.addLowerThan(constraintAnchor10.mSolverVariable, constraintWidget21.mListAnchors[i10].mTarget.mSolverVariable, -constraintAnchor10.getMargin(), 5);
                if (z4) {
                    int i11 = i2 + 1;
                    linearSystem2.addGreaterThan(constraintWidgetContainer2.mListAnchors[i11].mSolverVariable, constraintWidget21.mListAnchors[i11].mSolverVariable, constraintWidget21.mListAnchors[i11].getMargin(), 6);
                }
                if (i7 > 0) {
                    while (constraintWidget23 != null) {
                        ConstraintWidget constraintWidget27 = constraintWidget23.mListNextMatchConstraintsWidget[i];
                        if (constraintWidget27 != null) {
                            float f3 = constraintWidget23.mWeight[i];
                            float f4 = constraintWidget27.mWeight[i];
                            SolverVariable solverVariable8 = constraintWidget23.mListAnchors[i2].mSolverVariable;
                            int i12 = i2 + 1;
                            SolverVariable solverVariable9 = constraintWidget23.mListAnchors[i12].mSolverVariable;
                            SolverVariable solverVariable10 = constraintWidget27.mListAnchors[i2].mSolverVariable;
                            SolverVariable solverVariable11 = constraintWidget27.mListAnchors[i12].mSolverVariable;
                            if (i == 0) {
                                i6 = constraintWidget23.mMatchConstraintDefaultWidth;
                                i5 = constraintWidget27.mMatchConstraintDefaultWidth;
                            } else {
                                i6 = constraintWidget23.mMatchConstraintDefaultHeight;
                                i5 = constraintWidget27.mMatchConstraintDefaultHeight;
                            }
                            if ((i6 == 0 || i6 == 3) && (i5 == 0 || i5 == 3)) {
                                ArrayRow createRow = linearSystem.createRow();
                                createRow.createRowEqualMatchDimensions(f3, f2, f4, solverVariable8, solverVariable9, solverVariable10, solverVariable11);
                                linearSystem2.addConstraint(createRow);
                            }
                        }
                        constraintWidget23 = constraintWidget27;
                    }
                }
                if (constraintWidget24 == null && (constraintWidget24 == constraintWidget25 || z7)) {
                    ConstraintAnchor constraintAnchor11 = constraintWidget20.mListAnchors[i2];
                    int i13 = i2 + 1;
                    ConstraintAnchor constraintAnchor12 = constraintWidget21.mListAnchors[i13];
                    SolverVariable solverVariable12 = constraintWidget20.mListAnchors[i2].mTarget != null ? constraintWidget20.mListAnchors[i2].mTarget.mSolverVariable : solverVariable7;
                    SolverVariable solverVariable13 = constraintWidget21.mListAnchors[i13].mTarget != null ? constraintWidget21.mListAnchors[i13].mTarget.mSolverVariable : solverVariable7;
                    if (constraintWidget24 == constraintWidget25) {
                        constraintAnchor11 = constraintWidget24.mListAnchors[i2];
                        constraintAnchor12 = constraintWidget24.mListAnchors[i13];
                    }
                    if (solverVariable12 == null || solverVariable13 == null) {
                        constraintWidget14 = constraintWidget24;
                    } else {
                        if (i == 0) {
                            f = constraintWidget26.mHorizontalBiasPercent;
                        } else {
                            f = constraintWidget26.mVerticalBiasPercent;
                        }
                        int margin2 = constraintAnchor11.getMargin();
                        if (constraintWidget25 == null) {
                            constraintWidget25 = constraintWidget21;
                        }
                        int margin3 = constraintWidget25.mListAnchors[i13].getMargin();
                        SolverVariable solverVariable14 = constraintAnchor11.mSolverVariable;
                        SolverVariable solverVariable15 = constraintAnchor12.mSolverVariable;
                        SolverVariable solverVariable16 = solverVariable14;
                        SolverVariable solverVariable17 = solverVariable12;
                        int i14 = margin2;
                        SolverVariable solverVariable18 = solverVariable15;
                        constraintWidget14 = constraintWidget24;
                        linearSystem2.addCentering(solverVariable16, solverVariable17, i14, f, solverVariable13, solverVariable18, margin3, 5);
                    }
                    constraintWidget4 = constraintWidget14;
                } else if (z8 || constraintWidget24 == null) {
                    constraintWidget4 = constraintWidget24;
                    if (z9 && constraintWidget4 != null) {
                        ConstraintWidget constraintWidget28 = constraintWidget4;
                        constraintWidget6 = constraintWidget28;
                        while (constraintWidget6 != null) {
                            ConstraintWidget constraintWidget29 = constraintWidget6.mListNextVisibleWidget[i];
                            if (constraintWidget6 == constraintWidget4 || constraintWidget6 == constraintWidget25 || constraintWidget29 == null) {
                                constraintWidget7 = constraintWidget6;
                                constraintWidget6 = constraintWidget29;
                            } else {
                                ConstraintWidget constraintWidget30 = constraintWidget29 == constraintWidget25 ? null : constraintWidget29;
                                ConstraintAnchor constraintAnchor13 = constraintWidget6.mListAnchors[i2];
                                SolverVariable solverVariable19 = constraintAnchor13.mSolverVariable;
                                if (constraintAnchor13.mTarget != null) {
                                    SolverVariable solverVariable20 = constraintAnchor13.mTarget.mSolverVariable;
                                }
                                int i15 = i2 + 1;
                                SolverVariable solverVariable21 = constraintWidget28.mListAnchors[i15].mSolverVariable;
                                int margin4 = constraintAnchor13.getMargin();
                                int margin5 = constraintWidget6.mListAnchors[i15].getMargin();
                                if (constraintWidget30 != null) {
                                    constraintAnchor4 = constraintWidget30.mListAnchors[i2];
                                    constraintWidget8 = constraintWidget30;
                                    solverVariable2 = constraintAnchor4.mSolverVariable;
                                    solverVariable3 = constraintAnchor4.mTarget != null ? constraintAnchor4.mTarget.mSolverVariable : null;
                                } else {
                                    constraintWidget8 = constraintWidget30;
                                    ConstraintAnchor constraintAnchor14 = constraintWidget6.mListAnchors[i15].mTarget;
                                    if (constraintAnchor14 != null) {
                                        solverVariable4 = constraintAnchor14.mSolverVariable;
                                        constraintAnchor5 = constraintAnchor14;
                                    } else {
                                        constraintAnchor5 = constraintAnchor14;
                                        solverVariable4 = null;
                                    }
                                    solverVariable2 = solverVariable4;
                                    solverVariable3 = constraintWidget6.mListAnchors[i15].mSolverVariable;
                                    constraintAnchor4 = constraintAnchor5;
                                }
                                if (constraintAnchor4 != null) {
                                    margin5 += constraintAnchor4.getMargin();
                                }
                                int i16 = margin5;
                                if (constraintWidget28 != null) {
                                    margin4 += constraintWidget28.mListAnchors[i15].getMargin();
                                }
                                int i17 = margin4;
                                if (solverVariable19 == null || solverVariable21 == null || solverVariable2 == null || solverVariable3 == null) {
                                    constraintWidget7 = constraintWidget6;
                                    constraintWidget9 = constraintWidget8;
                                } else {
                                    SolverVariable solverVariable22 = solverVariable3;
                                    constraintWidget9 = constraintWidget8;
                                    int i18 = i16;
                                    constraintWidget7 = constraintWidget6;
                                    linearSystem2.addCentering(solverVariable19, solverVariable21, i17, 0.5f, solverVariable2, solverVariable22, i18, 4);
                                }
                                constraintWidget6 = constraintWidget9;
                            }
                            constraintWidget28 = constraintWidget7;
                        }
                        ConstraintAnchor constraintAnchor15 = constraintWidget4.mListAnchors[i2];
                        constraintAnchor = constraintWidget20.mListAnchors[i2].mTarget;
                        int i19 = i2 + 1;
                        constraintAnchor2 = constraintWidget25.mListAnchors[i19];
                        constraintAnchor3 = constraintWidget21.mListAnchors[i19].mTarget;
                        if (constraintAnchor == null) {
                            if (constraintWidget4 != constraintWidget25) {
                                i4 = 5;
                                linearSystem2.addEquality(constraintAnchor15.mSolverVariable, constraintAnchor.mSolverVariable, constraintAnchor15.getMargin(), 5);
                            } else {
                                i4 = 5;
                                if (constraintAnchor3 != null) {
                                    constraintWidget3 = constraintWidget21;
                                    i3 = 5;
                                    linearSystem2.addCentering(constraintAnchor15.mSolverVariable, constraintAnchor.mSolverVariable, constraintAnchor15.getMargin(), 0.5f, constraintAnchor2.mSolverVariable, constraintAnchor3.mSolverVariable, constraintAnchor2.getMargin(), 5);
                                }
                            }
                            constraintWidget3 = constraintWidget21;
                            i3 = i4;
                        } else {
                            constraintWidget3 = constraintWidget21;
                            i3 = 5;
                        }
                        if (!(constraintAnchor3 == null || constraintWidget4 == constraintWidget25)) {
                            linearSystem2.addEquality(constraintAnchor2.mSolverVariable, constraintAnchor3.mSolverVariable, -constraintAnchor2.getMargin(), i3);
                        }
                        constraintWidget5 = constraintWidget25;
                        if ((!z8 || z9) && constraintWidget4 != null) {
                            ConstraintAnchor constraintAnchor16 = constraintWidget4.mListAnchors[i2];
                            int i20 = i2 + 1;
                            ConstraintAnchor constraintAnchor17 = constraintWidget5.mListAnchors[i20];
                            solverVariable = constraintAnchor16.mTarget != null ? constraintAnchor16.mTarget.mSolverVariable : null;
                            SolverVariable solverVariable23 = constraintAnchor17.mTarget != null ? constraintAnchor17.mTarget.mSolverVariable : null;
                            if (constraintWidget4 == constraintWidget5) {
                                constraintAnchor16 = constraintWidget4.mListAnchors[i2];
                                constraintAnchor17 = constraintWidget4.mListAnchors[i20];
                            }
                            if (solverVariable == null && solverVariable23 != null) {
                                int margin6 = constraintAnchor16.getMargin();
                                if (constraintWidget5 == null) {
                                    constraintWidget5 = constraintWidget3;
                                }
                                linearSystem2.addCentering(constraintAnchor16.mSolverVariable, solverVariable, margin6, 0.5f, solverVariable23, constraintAnchor17.mSolverVariable, constraintWidget5.mListAnchors[i20].getMargin(), 5);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                } else {
                    ConstraintWidget constraintWidget31 = constraintWidget24;
                    ConstraintWidget constraintWidget32 = constraintWidget31;
                    while (constraintWidget32 != null) {
                        ConstraintWidget constraintWidget33 = constraintWidget32.mListNextVisibleWidget[i];
                        if (constraintWidget33 != null || constraintWidget32 == constraintWidget25) {
                            ConstraintAnchor constraintAnchor18 = constraintWidget32.mListAnchors[i2];
                            SolverVariable solverVariable24 = constraintAnchor18.mSolverVariable;
                            SolverVariable solverVariable25 = constraintAnchor18.mTarget != null ? constraintAnchor18.mTarget.mSolverVariable : solverVariable7;
                            if (constraintWidget31 != constraintWidget32) {
                                solverVariable25 = constraintWidget31.mListAnchors[i2 + 1].mSolverVariable;
                            } else if (constraintWidget32 == constraintWidget24 && constraintWidget31 == constraintWidget32) {
                                solverVariable25 = constraintWidget20.mListAnchors[i2].mTarget != null ? constraintWidget20.mListAnchors[i2].mTarget.mSolverVariable : solverVariable7;
                            }
                            int margin7 = constraintAnchor18.getMargin();
                            int i21 = i2 + 1;
                            int margin8 = constraintWidget32.mListAnchors[i21].getMargin();
                            if (constraintWidget33 != null) {
                                constraintAnchor6 = constraintWidget33.mListAnchors[i2];
                                constraintWidget13 = constraintWidget33;
                                solverVariable5 = constraintAnchor6.mSolverVariable;
                                solverVariable6 = constraintAnchor6.mTarget != null ? constraintAnchor6.mTarget.mSolverVariable : null;
                            } else {
                                constraintWidget13 = constraintWidget33;
                                constraintAnchor6 = constraintWidget21.mListAnchors[i21].mTarget;
                                SolverVariable solverVariable26 = constraintAnchor6 != null ? constraintAnchor6.mSolverVariable : null;
                                solverVariable6 = constraintWidget32.mListAnchors[i21].mSolverVariable;
                                solverVariable5 = solverVariable26;
                            }
                            if (constraintAnchor6 != null) {
                                margin8 += constraintAnchor6.getMargin();
                            }
                            if (constraintWidget31 != null) {
                                margin7 += constraintWidget31.mListAnchors[i21].getMargin();
                            }
                            if (solverVariable24 == null || solverVariable25 == null || solverVariable5 == null || solverVariable6 == null) {
                                constraintWidget11 = constraintWidget32;
                                constraintWidget12 = constraintWidget24;
                                constraintWidget10 = constraintWidget13;
                            } else {
                                constraintWidget11 = constraintWidget32;
                                constraintWidget10 = constraintWidget13;
                                constraintWidget12 = constraintWidget24;
                                linearSystem2.addCentering(solverVariable24, solverVariable25, constraintWidget32 == constraintWidget24 ? constraintWidget24.mListAnchors[i2].getMargin() : margin7, 0.5f, solverVariable5, solverVariable6, constraintWidget32 == constraintWidget25 ? constraintWidget25.mListAnchors[i21].getMargin() : margin8, 4);
                            }
                        } else {
                            constraintWidget10 = constraintWidget33;
                            constraintWidget11 = constraintWidget32;
                            constraintWidget12 = constraintWidget24;
                        }
                        constraintWidget24 = constraintWidget12;
                        constraintWidget31 = constraintWidget11;
                        constraintWidget32 = constraintWidget10;
                        solverVariable7 = null;
                    }
                    constraintWidget4 = constraintWidget24;
                }
                constraintWidget3 = constraintWidget21;
                constraintWidget5 = constraintWidget25;
                if (!z8) {
                }
                ConstraintAnchor constraintAnchor162 = constraintWidget4.mListAnchors[i2];
                int i202 = i2 + 1;
                ConstraintAnchor constraintAnchor172 = constraintWidget5.mListAnchors[i202];
                if (constraintAnchor162.mTarget != null) {
                }
                if (constraintAnchor172.mTarget != null) {
                }
                if (constraintWidget4 == constraintWidget5) {
                }
                if (solverVariable == null) {
                    return;
                }
                return;
            }
        }
        if (z4) {
        }
        if (i7 > 0) {
        }
        if (constraintWidget24 == null) {
        }
        if (z8) {
        }
        constraintWidget4 = constraintWidget24;
        ConstraintWidget constraintWidget282 = constraintWidget4;
        constraintWidget6 = constraintWidget282;
        while (constraintWidget6 != null) {
        }
        ConstraintAnchor constraintAnchor152 = constraintWidget4.mListAnchors[i2];
        constraintAnchor = constraintWidget20.mListAnchors[i2].mTarget;
        int i192 = i2 + 1;
        constraintAnchor2 = constraintWidget25.mListAnchors[i192];
        constraintAnchor3 = constraintWidget21.mListAnchors[i192].mTarget;
        if (constraintAnchor == null) {
        }
        linearSystem2.addEquality(constraintAnchor2.mSolverVariable, constraintAnchor3.mSolverVariable, -constraintAnchor2.getMargin(), i3);
        constraintWidget5 = constraintWidget25;
        if (!z8) {
        }
        ConstraintAnchor constraintAnchor1622 = constraintWidget4.mListAnchors[i2];
        int i2022 = i2 + 1;
        ConstraintAnchor constraintAnchor1722 = constraintWidget5.mListAnchors[i2022];
        if (constraintAnchor1622.mTarget != null) {
        }
        if (constraintAnchor1722.mTarget != null) {
        }
        if (constraintWidget4 == constraintWidget5) {
        }
        if (solverVariable == null) {
        }
    }
}
