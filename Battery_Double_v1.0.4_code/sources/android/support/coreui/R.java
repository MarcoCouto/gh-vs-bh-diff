package android.support.coreui;

import com.mansoon.BatteryDouble.C0002R;

public final class R {

    public static final class attr {
        public static final int coordinatorLayoutStyle = 2130837610;
        public static final int font = 2130837630;
        public static final int fontProviderAuthority = 2130837632;
        public static final int fontProviderCerts = 2130837633;
        public static final int fontProviderFetchStrategy = 2130837634;
        public static final int fontProviderFetchTimeout = 2130837635;
        public static final int fontProviderPackage = 2130837636;
        public static final int fontProviderQuery = 2130837637;
        public static final int fontStyle = 2130837638;
        public static final int fontWeight = 2130837639;
        public static final int keylines = 2130837657;
        public static final int layout_anchor = 2130837659;
        public static final int layout_anchorGravity = 2130837660;
        public static final int layout_behavior = 2130837661;
        public static final int layout_dodgeInsetEdges = 2130837703;
        public static final int layout_insetEdge = 2130837712;
        public static final int layout_keyline = 2130837713;
        public static final int statusBarBackground = 2130837773;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130903040;
    }

    public static final class color {
        public static final int notification_action_color_filter = 2130968649;
        public static final int notification_icon_bg_color = 2130968650;
        public static final int ripple_material_light = 2130968661;
        public static final int secondary_text_default_material_light = 2130968663;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131034187;
        public static final int compat_button_inset_vertical_material = 2131034188;
        public static final int compat_button_padding_horizontal_material = 2131034189;
        public static final int compat_button_padding_vertical_material = 2131034190;
        public static final int compat_control_corner_material = 2131034191;
        public static final int notification_action_icon_size = 2131034201;
        public static final int notification_action_text_size = 2131034202;
        public static final int notification_big_circle_margin = 2131034203;
        public static final int notification_content_margin_start = 2131034204;
        public static final int notification_large_icon_height = 2131034205;
        public static final int notification_large_icon_width = 2131034206;
        public static final int notification_main_column_padding_top = 2131034207;
        public static final int notification_media_narrow_margin = 2131034208;
        public static final int notification_right_icon_size = 2131034209;
        public static final int notification_right_side_padding_top = 2131034210;
        public static final int notification_small_icon_background_padding = 2131034211;
        public static final int notification_small_icon_size_as_large = 2131034212;
        public static final int notification_subtext_size = 2131034213;
        public static final int notification_top_pad = 2131034214;
        public static final int notification_top_pad_large_text = 2131034215;
    }

    public static final class drawable {
        public static final int notification_action_background = 2131099762;
        public static final int notification_bg = 2131099763;
        public static final int notification_bg_low = 2131099764;
        public static final int notification_bg_low_normal = 2131099765;
        public static final int notification_bg_low_pressed = 2131099766;
        public static final int notification_bg_normal = 2131099767;
        public static final int notification_bg_normal_pressed = 2131099768;
        public static final int notification_icon_background = 2131099769;
        public static final int notification_template_icon_bg = 2131099770;
        public static final int notification_template_icon_low_bg = 2131099771;
        public static final int notification_tile_bg = 2131099772;
        public static final int notify_panel_notification_icon_bg = 2131099773;
    }

    public static final class id {
        public static final int action_container = 2131165198;
        public static final int action_divider = 2131165200;
        public static final int action_image = 2131165201;
        public static final int action_text = 2131165207;
        public static final int actions = 2131165208;
        public static final int async = 2131165217;
        public static final int blocking = 2131165221;
        public static final int bottom = 2131165222;
        public static final int chronometer = 2131165237;
        public static final int end = 2131165251;
        public static final int forever = 2131165258;
        public static final int icon = 2131165262;
        public static final int icon_group = 2131165263;
        public static final int info = 2131165267;
        public static final int italic = 2131165269;
        public static final int left = 2131165270;
        public static final int line1 = 2131165272;
        public static final int line3 = 2131165273;
        public static final int none = 2131165281;
        public static final int normal = 2131165282;
        public static final int notification_background = 2131165283;
        public static final int notification_main_column = 2131165284;
        public static final int notification_main_column_container = 2131165285;
        public static final int right = 2131165296;
        public static final int right_icon = 2131165297;
        public static final int right_side = 2131165298;
        public static final int start = 2131165327;
        public static final int tag_transition_group = 2131165332;
        public static final int text = 2131165333;
        public static final int text2 = 2131165334;
        public static final int time = 2131165337;
        public static final int title = 2131165338;
        public static final int top = 2131165341;
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131230725;
    }

    public static final class layout {
        public static final int notification_action = 2131296285;
        public static final int notification_action_tombstone = 2131296286;
        public static final int notification_template_custom_big = 2131296293;
        public static final int notification_template_icon_group = 2131296294;
        public static final int notification_template_part_chronometer = 2131296298;
        public static final int notification_template_part_time = 2131296299;
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131427392;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131493095;
        public static final int TextAppearance_Compat_Notification_Info = 2131493096;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131493098;
        public static final int TextAppearance_Compat_Notification_Time = 2131493101;
        public static final int TextAppearance_Compat_Notification_Title = 2131493103;
        public static final int Widget_Compat_NotificationActionContainer = 2131493209;
        public static final int Widget_Compat_NotificationActionText = 2131493210;
        public static final int Widget_Support_CoordinatorLayout = 2131493211;
    }

    public static final class styleable {
        public static final int[] CoordinatorLayout = {C0002R.attr.keylines, C0002R.attr.statusBarBackground};
        public static final int[] CoordinatorLayout_Layout = {16842931, C0002R.attr.layout_anchor, C0002R.attr.layout_anchorGravity, C0002R.attr.layout_behavior, C0002R.attr.layout_dodgeInsetEdges, C0002R.attr.layout_insetEdge, C0002R.attr.layout_keyline};
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily = {C0002R.attr.fontProviderAuthority, C0002R.attr.fontProviderCerts, C0002R.attr.fontProviderFetchStrategy, C0002R.attr.fontProviderFetchTimeout, C0002R.attr.fontProviderPackage, C0002R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, C0002R.attr.font, C0002R.attr.fontStyle, C0002R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
    }
}
