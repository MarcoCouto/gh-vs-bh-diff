package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.annotation.RestrictTo.Scope;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.google.android.gms.common.util.CrashUtils.ErrorDialogData;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LinearLayoutCompat extends ViewGroup {
    public static final int HORIZONTAL = 0;
    private static final int INDEX_BOTTOM = 2;
    private static final int INDEX_CENTER_VERTICAL = 0;
    private static final int INDEX_FILL = 3;
    private static final int INDEX_TOP = 1;
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    public static final int VERTICAL = 1;
    private static final int VERTICAL_GRAVITY_COUNT = 4;
    private boolean mBaselineAligned;
    private int mBaselineAlignedChildIndex;
    private int mBaselineChildTop;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;
    private int mGravity;
    private int[] mMaxAscent;
    private int[] mMaxDescent;
    private int mOrientation;
    private int mShowDividers;
    private int mTotalLength;
    private boolean mUseLargestChild;
    private float mWeightSum;

    @RestrictTo({Scope.LIBRARY_GROUP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DividerMode {
    }

    public static class LayoutParams extends MarginLayoutParams {
        public int gravity;
        public float weight;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.gravity = -1;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.LinearLayoutCompat_Layout);
            this.weight = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0f);
            this.gravity = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.gravity = -1;
            this.weight = 0.0f;
        }

        public LayoutParams(int i, int i2, float f) {
            super(i, i2);
            this.gravity = -1;
            this.weight = f;
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = -1;
        }

        public LayoutParams(MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.gravity = -1;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = -1;
            this.weight = layoutParams.weight;
            this.gravity = layoutParams.gravity;
        }
    }

    @RestrictTo({Scope.LIBRARY_GROUP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface OrientationMode {
    }

    /* access modifiers changed from: 0000 */
    public int getChildrenSkipCount(View view, int i) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int getLocationOffset(View view) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int getNextLocationOffset(View view) {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int measureNullChild(int i) {
        return 0;
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public LinearLayoutCompat(Context context) {
        this(context, null);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mBaselineAligned = true;
        this.mBaselineAlignedChildIndex = -1;
        this.mBaselineChildTop = 0;
        this.mGravity = 8388659;
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, attributeSet, R.styleable.LinearLayoutCompat, i, 0);
        int i2 = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_orientation, -1);
        if (i2 >= 0) {
            setOrientation(i2);
        }
        int i3 = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_gravity, -1);
        if (i3 >= 0) {
            setGravity(i3);
        }
        boolean z = obtainStyledAttributes.getBoolean(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
        if (!z) {
            setBaselineAligned(z);
        }
        this.mWeightSum = obtainStyledAttributes.getFloat(R.styleable.LinearLayoutCompat_android_weightSum, -1.0f);
        this.mBaselineAlignedChildIndex = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.mUseLargestChild = obtainStyledAttributes.getBoolean(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
        setDividerDrawable(obtainStyledAttributes.getDrawable(R.styleable.LinearLayoutCompat_divider));
        this.mShowDividers = obtainStyledAttributes.getInt(R.styleable.LinearLayoutCompat_showDividers, 0);
        this.mDividerPadding = obtainStyledAttributes.getDimensionPixelSize(R.styleable.LinearLayoutCompat_dividerPadding, 0);
        obtainStyledAttributes.recycle();
    }

    public void setShowDividers(int i) {
        if (i != this.mShowDividers) {
            requestLayout();
        }
        this.mShowDividers = i;
    }

    public int getShowDividers() {
        return this.mShowDividers;
    }

    public Drawable getDividerDrawable() {
        return this.mDivider;
    }

    public void setDividerDrawable(Drawable drawable) {
        if (drawable != this.mDivider) {
            this.mDivider = drawable;
            boolean z = false;
            if (drawable != null) {
                this.mDividerWidth = drawable.getIntrinsicWidth();
                this.mDividerHeight = drawable.getIntrinsicHeight();
            } else {
                this.mDividerWidth = 0;
                this.mDividerHeight = 0;
            }
            if (drawable == null) {
                z = true;
            }
            setWillNotDraw(z);
            requestLayout();
        }
    }

    public void setDividerPadding(int i) {
        this.mDividerPadding = i;
    }

    public int getDividerPadding() {
        return this.mDividerPadding;
    }

    @RestrictTo({Scope.LIBRARY_GROUP})
    public int getDividerWidth() {
        return this.mDividerWidth;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.mDivider != null) {
            if (this.mOrientation == 1) {
                drawDividersVertical(canvas);
            } else {
                drawDividersHorizontal(canvas);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void drawDividersVertical(Canvas canvas) {
        int i;
        int virtualChildCount = getVirtualChildCount();
        for (int i2 = 0; i2 < virtualChildCount; i2++) {
            View virtualChildAt = getVirtualChildAt(i2);
            if (!(virtualChildAt == null || virtualChildAt.getVisibility() == 8 || !hasDividerBeforeChildAt(i2))) {
                drawHorizontalDivider(canvas, (virtualChildAt.getTop() - ((LayoutParams) virtualChildAt.getLayoutParams()).topMargin) - this.mDividerHeight);
            }
        }
        if (hasDividerBeforeChildAt(virtualChildCount)) {
            View virtualChildAt2 = getVirtualChildAt(virtualChildCount - 1);
            if (virtualChildAt2 == null) {
                i = (getHeight() - getPaddingBottom()) - this.mDividerHeight;
            } else {
                i = virtualChildAt2.getBottom() + ((LayoutParams) virtualChildAt2.getLayoutParams()).bottomMargin;
            }
            drawHorizontalDivider(canvas, i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void drawDividersHorizontal(Canvas canvas) {
        int i;
        int i2;
        int virtualChildCount = getVirtualChildCount();
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        for (int i3 = 0; i3 < virtualChildCount; i3++) {
            View virtualChildAt = getVirtualChildAt(i3);
            if (!(virtualChildAt == null || virtualChildAt.getVisibility() == 8 || !hasDividerBeforeChildAt(i3))) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (isLayoutRtl) {
                    i2 = virtualChildAt.getRight() + layoutParams.rightMargin;
                } else {
                    i2 = (virtualChildAt.getLeft() - layoutParams.leftMargin) - this.mDividerWidth;
                }
                drawVerticalDivider(canvas, i2);
            }
        }
        if (hasDividerBeforeChildAt(virtualChildCount)) {
            View virtualChildAt2 = getVirtualChildAt(virtualChildCount - 1);
            if (virtualChildAt2 != null) {
                LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                if (isLayoutRtl) {
                    i = (virtualChildAt2.getLeft() - layoutParams2.leftMargin) - this.mDividerWidth;
                } else {
                    i = virtualChildAt2.getRight() + layoutParams2.rightMargin;
                }
            } else if (isLayoutRtl) {
                i = getPaddingLeft();
            } else {
                i = (getWidth() - getPaddingRight()) - this.mDividerWidth;
            }
            drawVerticalDivider(canvas, i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void drawHorizontalDivider(Canvas canvas, int i) {
        this.mDivider.setBounds(getPaddingLeft() + this.mDividerPadding, i, (getWidth() - getPaddingRight()) - this.mDividerPadding, this.mDividerHeight + i);
        this.mDivider.draw(canvas);
    }

    /* access modifiers changed from: 0000 */
    public void drawVerticalDivider(Canvas canvas, int i) {
        this.mDivider.setBounds(i, getPaddingTop() + this.mDividerPadding, this.mDividerWidth + i, (getHeight() - getPaddingBottom()) - this.mDividerPadding);
        this.mDivider.draw(canvas);
    }

    public boolean isBaselineAligned() {
        return this.mBaselineAligned;
    }

    public void setBaselineAligned(boolean z) {
        this.mBaselineAligned = z;
    }

    public boolean isMeasureWithLargestChildEnabled() {
        return this.mUseLargestChild;
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.mUseLargestChild = z;
    }

    public int getBaseline() {
        if (this.mBaselineAlignedChildIndex < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.mBaselineAlignedChildIndex) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        View childAt = getChildAt(this.mBaselineAlignedChildIndex);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i = this.mBaselineChildTop;
            if (this.mOrientation == 1) {
                int i2 = this.mGravity & 112;
                if (i2 != 48) {
                    if (i2 == 16) {
                        i += ((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.mTotalLength) / 2;
                    } else if (i2 == 80) {
                        i = ((getBottom() - getTop()) - getPaddingBottom()) - this.mTotalLength;
                    }
                }
            }
            return i + ((LayoutParams) childAt.getLayoutParams()).topMargin + baseline;
        } else if (this.mBaselineAlignedChildIndex == 0) {
            return -1;
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.mBaselineAlignedChildIndex;
    }

    public void setBaselineAlignedChildIndex(int i) {
        if (i < 0 || i >= getChildCount()) {
            StringBuilder sb = new StringBuilder();
            sb.append("base aligned child index out of range (0, ");
            sb.append(getChildCount());
            sb.append(")");
            throw new IllegalArgumentException(sb.toString());
        }
        this.mBaselineAlignedChildIndex = i;
    }

    /* access modifiers changed from: 0000 */
    public View getVirtualChildAt(int i) {
        return getChildAt(i);
    }

    /* access modifiers changed from: 0000 */
    public int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.mWeightSum;
    }

    public void setWeightSum(float f) {
        this.mWeightSum = Math.max(0.0f, f);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.mOrientation == 1) {
            measureVertical(i, i2);
        } else {
            measureHorizontal(i, i2);
        }
    }

    /* access modifiers changed from: protected */
    @RestrictTo({Scope.LIBRARY})
    public boolean hasDividerBeforeChildAt(int i) {
        boolean z = false;
        if (i == 0) {
            if ((this.mShowDividers & 1) != 0) {
                z = true;
            }
            return z;
        } else if (i == getChildCount()) {
            if ((this.mShowDividers & 4) != 0) {
                z = true;
            }
            return z;
        } else if ((this.mShowDividers & 2) == 0) {
            return false;
        } else {
            int i2 = i - 1;
            while (true) {
                if (i2 < 0) {
                    break;
                } else if (getChildAt(i2).getVisibility() != 8) {
                    z = true;
                    break;
                } else {
                    i2--;
                }
            }
            return z;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0334  */
    public void measureVertical(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        float f;
        int i9;
        int i10;
        boolean z;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        int i19;
        int i20;
        View view;
        boolean z2;
        int i21;
        int i22;
        int i23;
        int i24 = i;
        int i25 = i2;
        this.mTotalLength = 0;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        int i26 = this.mBaselineAlignedChildIndex;
        boolean z3 = this.mUseLargestChild;
        int i27 = 0;
        int i28 = 0;
        int i29 = 0;
        int i30 = 0;
        int i31 = 0;
        int i32 = 0;
        boolean z4 = false;
        boolean z5 = false;
        float f2 = 0.0f;
        boolean z6 = true;
        while (true) {
            int i33 = 8;
            int i34 = i30;
            if (i32 < virtualChildCount) {
                View virtualChildAt = getVirtualChildAt(i32);
                if (virtualChildAt == null) {
                    this.mTotalLength += measureNullChild(i32);
                    i14 = virtualChildCount;
                    i15 = mode2;
                    i30 = i34;
                } else {
                    int i35 = i27;
                    if (virtualChildAt.getVisibility() == 8) {
                        i32 += getChildrenSkipCount(virtualChildAt, i32);
                        i14 = virtualChildCount;
                        i15 = mode2;
                        i30 = i34;
                        i27 = i35;
                    } else {
                        if (hasDividerBeforeChildAt(i32)) {
                            this.mTotalLength += this.mDividerHeight;
                        }
                        LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                        float f3 = f2 + layoutParams.weight;
                        if (mode2 == 1073741824 && layoutParams.height == 0 && layoutParams.weight > 0.0f) {
                            int i36 = this.mTotalLength;
                            int i37 = i28;
                            this.mTotalLength = Math.max(i36, layoutParams.topMargin + i36 + layoutParams.bottomMargin);
                            view = virtualChildAt;
                            i16 = i31;
                            i14 = virtualChildCount;
                            i15 = mode2;
                            z4 = true;
                            i18 = i34;
                            i17 = i35;
                            i19 = i37;
                            i20 = i32;
                        } else {
                            int i38 = i28;
                            if (layoutParams.height != 0 || layoutParams.weight <= 0.0f) {
                                i23 = Integer.MIN_VALUE;
                            } else {
                                layoutParams.height = -2;
                                i23 = 0;
                            }
                            i15 = mode2;
                            int i39 = i23;
                            i19 = i38;
                            i14 = virtualChildCount;
                            int i40 = i29;
                            int i41 = i24;
                            view = virtualChildAt;
                            i17 = i35;
                            i18 = i34;
                            i16 = i31;
                            i20 = i32;
                            measureChildBeforeLayout(virtualChildAt, i32, i41, 0, i25, f3 == 0.0f ? this.mTotalLength : 0);
                            int i42 = i39;
                            if (i42 != Integer.MIN_VALUE) {
                                layoutParams.height = i42;
                            }
                            int measuredHeight = view.getMeasuredHeight();
                            int i43 = this.mTotalLength;
                            this.mTotalLength = Math.max(i43, i43 + measuredHeight + layoutParams.topMargin + layoutParams.bottomMargin + getNextLocationOffset(view));
                            i29 = z3 ? Math.max(measuredHeight, i40) : i40;
                        }
                        if (i26 >= 0 && i26 == i20 + 1) {
                            this.mBaselineChildTop = this.mTotalLength;
                        }
                        if (i20 >= i26 || layoutParams.weight <= 0.0f) {
                            if (mode == 1073741824 || layoutParams.width != -1) {
                                z2 = false;
                            } else {
                                z2 = true;
                                z5 = true;
                            }
                            int i44 = layoutParams.leftMargin + layoutParams.rightMargin;
                            int measuredWidth = view.getMeasuredWidth() + i44;
                            int max = Math.max(i19, measuredWidth);
                            int combineMeasuredStates = View.combineMeasuredStates(i17, view.getMeasuredState());
                            boolean z7 = z6 && layoutParams.width == -1;
                            if (layoutParams.weight > 0.0f) {
                                if (!z2) {
                                    i44 = measuredWidth;
                                }
                                i21 = Math.max(i18, i44);
                                i22 = i16;
                            } else {
                                i21 = i18;
                                if (z2) {
                                    measuredWidth = i44;
                                }
                                i22 = Math.max(i16, measuredWidth);
                            }
                            i28 = max;
                            z6 = z7;
                            i30 = i21;
                            i32 = getChildrenSkipCount(view, i20) + i20;
                            i27 = combineMeasuredStates;
                            i31 = i22;
                            f2 = f3;
                        } else {
                            throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                        }
                    }
                }
                i32++;
                mode2 = i15;
                virtualChildCount = i14;
                i24 = i;
            } else {
                int i45 = i27;
                int i46 = i28;
                int i47 = i31;
                int i48 = virtualChildCount;
                int i49 = mode2;
                int i50 = i34;
                int i51 = i29;
                if (this.mTotalLength > 0) {
                    i3 = i48;
                    if (hasDividerBeforeChildAt(i3)) {
                        this.mTotalLength += this.mDividerHeight;
                    }
                } else {
                    i3 = i48;
                }
                if (z3) {
                    i5 = i49;
                    if (i5 == Integer.MIN_VALUE || i5 == 0) {
                        this.mTotalLength = 0;
                        int i52 = 0;
                        while (i52 < i3) {
                            View virtualChildAt2 = getVirtualChildAt(i52);
                            if (virtualChildAt2 == null) {
                                this.mTotalLength += measureNullChild(i52);
                            } else if (virtualChildAt2.getVisibility() == i33) {
                                i52 += getChildrenSkipCount(virtualChildAt2, i52);
                            } else {
                                LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                                int i53 = this.mTotalLength;
                                i13 = i46;
                                this.mTotalLength = Math.max(i53, i53 + i51 + layoutParams2.topMargin + layoutParams2.bottomMargin + getNextLocationOffset(virtualChildAt2));
                                i52++;
                                i46 = i13;
                                i33 = 8;
                            }
                            i13 = i46;
                            i52++;
                            i46 = i13;
                            i33 = 8;
                        }
                    }
                    i4 = i46;
                } else {
                    i4 = i46;
                    i5 = i49;
                }
                this.mTotalLength += getPaddingTop() + getPaddingBottom();
                int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumHeight()), i25, 0);
                int i54 = (16777215 & resolveSizeAndState) - this.mTotalLength;
                if (z4 || (i54 != 0 && f2 > 0.0f)) {
                    if (this.mWeightSum > 0.0f) {
                        f2 = this.mWeightSum;
                    }
                    this.mTotalLength = 0;
                    float f4 = f2;
                    int i55 = 0;
                    int i56 = i54;
                    int i57 = i47;
                    i7 = i4;
                    while (i55 < i3) {
                        View virtualChildAt3 = getVirtualChildAt(i55);
                        if (virtualChildAt3.getVisibility() == 8) {
                            f = f4;
                            int i58 = i;
                        } else {
                            LayoutParams layoutParams3 = (LayoutParams) virtualChildAt3.getLayoutParams();
                            float f5 = layoutParams3.weight;
                            if (f5 > 0.0f) {
                                int i59 = (int) ((((float) i56) * f5) / f4);
                                i9 = i56 - i59;
                                f = f4 - f5;
                                int childMeasureSpec = getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + layoutParams3.leftMargin + layoutParams3.rightMargin, layoutParams3.width);
                                if (layoutParams3.height == 0) {
                                    i12 = ErrorDialogData.SUPPRESSED;
                                    if (i5 == 1073741824) {
                                        if (i59 <= 0) {
                                            i59 = 0;
                                        }
                                        virtualChildAt3.measure(childMeasureSpec, MeasureSpec.makeMeasureSpec(i59, ErrorDialogData.SUPPRESSED));
                                        i45 = View.combineMeasuredStates(i45, virtualChildAt3.getMeasuredState() & InputDeviceCompat.SOURCE_ANY);
                                    }
                                } else {
                                    i12 = ErrorDialogData.SUPPRESSED;
                                }
                                int measuredHeight2 = virtualChildAt3.getMeasuredHeight() + i59;
                                if (measuredHeight2 < 0) {
                                    measuredHeight2 = 0;
                                }
                                virtualChildAt3.measure(childMeasureSpec, MeasureSpec.makeMeasureSpec(measuredHeight2, i12));
                                i45 = View.combineMeasuredStates(i45, virtualChildAt3.getMeasuredState() & InputDeviceCompat.SOURCE_ANY);
                            } else {
                                float f6 = f4;
                                int i60 = i;
                                f = f6;
                                i9 = i56;
                            }
                            int i61 = layoutParams3.leftMargin + layoutParams3.rightMargin;
                            int measuredWidth2 = virtualChildAt3.getMeasuredWidth() + i61;
                            int max2 = Math.max(i7, measuredWidth2);
                            if (mode != 1073741824) {
                                i10 = max2;
                                i11 = -1;
                                if (layoutParams3.width == -1) {
                                    z = true;
                                    if (!z) {
                                        i61 = measuredWidth2;
                                    }
                                    int max3 = Math.max(i57, i61);
                                    boolean z8 = !z6 && layoutParams3.width == i11;
                                    int i62 = this.mTotalLength;
                                    this.mTotalLength = Math.max(i62, virtualChildAt3.getMeasuredHeight() + i62 + layoutParams3.topMargin + layoutParams3.bottomMargin + getNextLocationOffset(virtualChildAt3));
                                    z6 = z8;
                                    i56 = i9;
                                    i7 = i10;
                                    i57 = max3;
                                }
                            } else {
                                i10 = max2;
                                i11 = -1;
                            }
                            z = false;
                            if (!z) {
                            }
                            int max32 = Math.max(i57, i61);
                            if (!z6) {
                            }
                            int i622 = this.mTotalLength;
                            this.mTotalLength = Math.max(i622, virtualChildAt3.getMeasuredHeight() + i622 + layoutParams3.topMargin + layoutParams3.bottomMargin + getNextLocationOffset(virtualChildAt3));
                            z6 = z8;
                            i56 = i9;
                            i7 = i10;
                            i57 = max32;
                        }
                        i55++;
                        f4 = f;
                    }
                    i6 = i;
                    this.mTotalLength += getPaddingTop() + getPaddingBottom();
                    i8 = i57;
                } else {
                    i8 = Math.max(i47, i50);
                    if (z3 && i5 != 1073741824) {
                        for (int i63 = 0; i63 < i3; i63++) {
                            View virtualChildAt4 = getVirtualChildAt(i63);
                            if (!(virtualChildAt4 == null || virtualChildAt4.getVisibility() == 8 || ((LayoutParams) virtualChildAt4.getLayoutParams()).weight <= 0.0f)) {
                                virtualChildAt4.measure(MeasureSpec.makeMeasureSpec(virtualChildAt4.getMeasuredWidth(), ErrorDialogData.SUPPRESSED), MeasureSpec.makeMeasureSpec(i51, ErrorDialogData.SUPPRESSED));
                            }
                        }
                    }
                    i7 = i4;
                    i6 = i;
                }
                if (z6 || mode == 1073741824) {
                    i8 = i7;
                }
                setMeasuredDimension(View.resolveSizeAndState(Math.max(i8 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i6, i45), resolveSizeAndState);
                if (z5) {
                    forceUniformWidth(i3, i25);
                    return;
                }
                return;
            }
        }
    }

    private void forceUniformWidth(int i, int i2) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), ErrorDialogData.SUPPRESSED);
        for (int i3 = 0; i3 < i; i3++) {
            View virtualChildAt = getVirtualChildAt(i3);
            if (virtualChildAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (layoutParams.width == -1) {
                    int i4 = layoutParams.height;
                    layoutParams.height = virtualChildAt.getMeasuredHeight();
                    measureChildWithMargins(virtualChildAt, makeMeasureSpec, 0, i2, 0);
                    layoutParams.height = i4;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x043e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0187  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01d6  */
    public void measureHorizontal(int i, int i2) {
        int[] iArr;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        float f;
        int i8;
        int i9;
        boolean z;
        int i10;
        int i11;
        boolean z2;
        boolean z3;
        float f2;
        int i12;
        int i13;
        int i14;
        View view;
        boolean z4;
        char c;
        int i15;
        int i16 = i;
        int i17 = i2;
        this.mTotalLength = 0;
        int virtualChildCount = getVirtualChildCount();
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        if (this.mMaxAscent == null || this.mMaxDescent == null) {
            this.mMaxAscent = new int[4];
            this.mMaxDescent = new int[4];
        }
        int[] iArr2 = this.mMaxAscent;
        int[] iArr3 = this.mMaxDescent;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        iArr3[3] = -1;
        iArr3[2] = -1;
        iArr3[1] = -1;
        iArr3[0] = -1;
        boolean z5 = this.mBaselineAligned;
        boolean z6 = this.mUseLargestChild;
        int i18 = ErrorDialogData.SUPPRESSED;
        boolean z7 = mode == 1073741824;
        int i19 = 0;
        int i20 = 0;
        int i21 = 0;
        boolean z8 = false;
        int i22 = 0;
        int i23 = 0;
        int i24 = 0;
        boolean z9 = false;
        boolean z10 = true;
        float f3 = 0.0f;
        while (true) {
            iArr = iArr3;
            if (i19 >= virtualChildCount) {
                break;
            }
            View virtualChildAt = getVirtualChildAt(i19);
            if (virtualChildAt == null) {
                this.mTotalLength += measureNullChild(i19);
            } else if (virtualChildAt.getVisibility() == 8) {
                i19 += getChildrenSkipCount(virtualChildAt, i19);
            } else {
                if (hasDividerBeforeChildAt(i19)) {
                    this.mTotalLength += this.mDividerWidth;
                }
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                f2 = f3 + layoutParams.weight;
                if (mode == i18 && layoutParams.width == 0 && layoutParams.weight > 0.0f) {
                    if (z7) {
                        this.mTotalLength += layoutParams.leftMargin + layoutParams.rightMargin;
                    } else {
                        int i25 = this.mTotalLength;
                        this.mTotalLength = Math.max(i25, layoutParams.leftMargin + i25 + layoutParams.rightMargin);
                    }
                    if (z5) {
                        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(0, 0);
                        virtualChildAt.measure(makeMeasureSpec, makeMeasureSpec);
                        i14 = i19;
                        z3 = z6;
                        z2 = z5;
                        view = virtualChildAt;
                    } else {
                        i14 = i19;
                        z3 = z6;
                        z2 = z5;
                        view = virtualChildAt;
                        z8 = true;
                        i12 = ErrorDialogData.SUPPRESSED;
                        if (mode2 == i12 && layoutParams.height == -1) {
                            z4 = true;
                            z9 = true;
                        } else {
                            z4 = false;
                        }
                        int i26 = layoutParams.topMargin + layoutParams.bottomMargin;
                        int measuredHeight = view.getMeasuredHeight() + i26;
                        int combineMeasuredStates = View.combineMeasuredStates(i24, view.getMeasuredState());
                        if (z2) {
                            int baseline = view.getBaseline();
                            if (baseline != -1) {
                                int i27 = ((((layoutParams.gravity < 0 ? this.mGravity : layoutParams.gravity) & 112) >> 4) & -2) >> 1;
                                iArr2[i27] = Math.max(iArr2[i27], baseline);
                                iArr[i27] = Math.max(iArr[i27], measuredHeight - baseline);
                            }
                        }
                        int max = Math.max(i21, measuredHeight);
                        boolean z11 = !z10 && layoutParams.height == -1;
                        if (layoutParams.weight <= 0.0f) {
                            if (!z4) {
                                i26 = measuredHeight;
                            }
                            i23 = Math.max(i23, i26);
                        } else {
                            int i28 = i23;
                            if (z4) {
                                measuredHeight = i26;
                            }
                            i22 = Math.max(i22, measuredHeight);
                            i23 = i28;
                        }
                        int i29 = i14;
                        i13 = getChildrenSkipCount(view, i29) + i29;
                        i24 = combineMeasuredStates;
                        i21 = max;
                        z10 = z11;
                        i18 = i12;
                        iArr3 = iArr;
                        z6 = z3;
                        z5 = z2;
                        i16 = i;
                        i17 = i2;
                        i19 = i13 + 1;
                        f3 = f2;
                    }
                } else {
                    if (layoutParams.width != 0 || layoutParams.weight <= 0.0f) {
                        c = 65534;
                        i15 = Integer.MIN_VALUE;
                    } else {
                        c = 65534;
                        layoutParams.width = -2;
                        i15 = 0;
                    }
                    i14 = i19;
                    int i30 = i15;
                    z3 = z6;
                    z2 = z5;
                    char c2 = c;
                    view = virtualChildAt;
                    measureChildBeforeLayout(virtualChildAt, i14, i16, f2 == 0.0f ? this.mTotalLength : 0, i17, 0);
                    int i31 = i30;
                    if (i31 != Integer.MIN_VALUE) {
                        layoutParams.width = i31;
                    }
                    int measuredWidth = view.getMeasuredWidth();
                    if (z7) {
                        this.mTotalLength += layoutParams.leftMargin + measuredWidth + layoutParams.rightMargin + getNextLocationOffset(view);
                    } else {
                        int i32 = this.mTotalLength;
                        this.mTotalLength = Math.max(i32, i32 + measuredWidth + layoutParams.leftMargin + layoutParams.rightMargin + getNextLocationOffset(view));
                    }
                    if (z3) {
                        i20 = Math.max(measuredWidth, i20);
                    }
                }
                i12 = ErrorDialogData.SUPPRESSED;
                if (mode2 == i12) {
                }
                z4 = false;
                int i262 = layoutParams.topMargin + layoutParams.bottomMargin;
                int measuredHeight2 = view.getMeasuredHeight() + i262;
                int combineMeasuredStates2 = View.combineMeasuredStates(i24, view.getMeasuredState());
                if (z2) {
                }
                int max2 = Math.max(i21, measuredHeight2);
                if (!z10) {
                }
                if (layoutParams.weight <= 0.0f) {
                }
                int i292 = i14;
                i13 = getChildrenSkipCount(view, i292) + i292;
                i24 = combineMeasuredStates2;
                i21 = max2;
                z10 = z11;
                i18 = i12;
                iArr3 = iArr;
                z6 = z3;
                z5 = z2;
                i16 = i;
                i17 = i2;
                i19 = i13 + 1;
                f3 = f2;
            }
            f2 = f3;
            i13 = i19;
            i12 = i18;
            z3 = z6;
            z2 = z5;
            i18 = i12;
            iArr3 = iArr;
            z6 = z3;
            z5 = z2;
            i16 = i;
            i17 = i2;
            i19 = i13 + 1;
            f3 = f2;
        }
        int i33 = i18;
        boolean z12 = z6;
        boolean z13 = z5;
        int i34 = i21;
        int i35 = i22;
        int i36 = i23;
        int i37 = i24;
        if (this.mTotalLength > 0 && hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerWidth;
        }
        if (!(iArr2[1] == -1 && iArr2[0] == -1 && iArr2[2] == -1 && iArr2[3] == -1)) {
            i34 = Math.max(i34, Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))) + Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))));
        }
        if (z12 && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.mTotalLength = 0;
            int i38 = 0;
            while (i38 < virtualChildCount) {
                View virtualChildAt2 = getVirtualChildAt(i38);
                if (virtualChildAt2 == null) {
                    this.mTotalLength += measureNullChild(i38);
                } else if (virtualChildAt2.getVisibility() == 8) {
                    i38 += getChildrenSkipCount(virtualChildAt2, i38);
                } else {
                    LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                    if (z7) {
                        this.mTotalLength += layoutParams2.leftMargin + i20 + layoutParams2.rightMargin + getNextLocationOffset(virtualChildAt2);
                    } else {
                        int i39 = this.mTotalLength;
                        i11 = i38;
                        this.mTotalLength = Math.max(i39, i39 + i20 + layoutParams2.leftMargin + layoutParams2.rightMargin + getNextLocationOffset(virtualChildAt2));
                        i38 = i11 + 1;
                    }
                }
                i11 = i38;
                i38 = i11 + 1;
            }
        }
        this.mTotalLength += getPaddingLeft() + getPaddingRight();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumWidth()), i, 0);
        int i40 = (16777215 & resolveSizeAndState) - this.mTotalLength;
        if (z8 || (i40 != 0 && f3 > 0.0f)) {
            if (this.mWeightSum > 0.0f) {
                f3 = this.mWeightSum;
            }
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            this.mTotalLength = 0;
            int i41 = i35;
            i3 = -1;
            float f4 = f3;
            int i42 = 0;
            while (i42 < virtualChildCount) {
                View virtualChildAt3 = getVirtualChildAt(i42);
                if (virtualChildAt3 == null || virtualChildAt3.getVisibility() == 8) {
                    i6 = i40;
                    int i43 = i2;
                } else {
                    LayoutParams layoutParams3 = (LayoutParams) virtualChildAt3.getLayoutParams();
                    float f5 = layoutParams3.weight;
                    if (f5 > 0.0f) {
                        int i44 = (int) ((((float) i40) * f5) / f4);
                        float f6 = f4 - f5;
                        int i45 = i40 - i44;
                        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingTop() + getPaddingBottom() + layoutParams3.topMargin + layoutParams3.bottomMargin, layoutParams3.height);
                        if (layoutParams3.width == 0) {
                            i10 = ErrorDialogData.SUPPRESSED;
                            if (mode == 1073741824) {
                                if (i44 <= 0) {
                                    i44 = 0;
                                }
                                virtualChildAt3.measure(MeasureSpec.makeMeasureSpec(i44, ErrorDialogData.SUPPRESSED), childMeasureSpec);
                                i37 = View.combineMeasuredStates(i37, virtualChildAt3.getMeasuredState() & ViewCompat.MEASURED_STATE_MASK);
                                f4 = f6;
                                i7 = i45;
                            }
                        } else {
                            i10 = ErrorDialogData.SUPPRESSED;
                        }
                        int measuredWidth2 = virtualChildAt3.getMeasuredWidth() + i44;
                        if (measuredWidth2 < 0) {
                            measuredWidth2 = 0;
                        }
                        virtualChildAt3.measure(MeasureSpec.makeMeasureSpec(measuredWidth2, i10), childMeasureSpec);
                        i37 = View.combineMeasuredStates(i37, virtualChildAt3.getMeasuredState() & ViewCompat.MEASURED_STATE_MASK);
                        f4 = f6;
                        i7 = i45;
                    } else {
                        i7 = i40;
                        int i46 = i2;
                    }
                    if (z7) {
                        f = f4;
                        this.mTotalLength += virtualChildAt3.getMeasuredWidth() + layoutParams3.leftMargin + layoutParams3.rightMargin + getNextLocationOffset(virtualChildAt3);
                        i8 = i7;
                    } else {
                        f = f4;
                        int i47 = this.mTotalLength;
                        i8 = i7;
                        this.mTotalLength = Math.max(i47, virtualChildAt3.getMeasuredWidth() + i47 + layoutParams3.leftMargin + layoutParams3.rightMargin + getNextLocationOffset(virtualChildAt3));
                    }
                    boolean z14 = mode2 != 1073741824 && layoutParams3.height == -1;
                    int i48 = layoutParams3.topMargin + layoutParams3.bottomMargin;
                    int measuredHeight3 = virtualChildAt3.getMeasuredHeight() + i48;
                    i3 = Math.max(i3, measuredHeight3);
                    if (!z14) {
                        i48 = measuredHeight3;
                    }
                    int max3 = Math.max(i41, i48);
                    if (z10) {
                        i9 = -1;
                        if (layoutParams3.height == -1) {
                            z = true;
                            if (z13) {
                                int baseline2 = virtualChildAt3.getBaseline();
                                if (baseline2 != i9) {
                                    int i49 = ((((layoutParams3.gravity < 0 ? this.mGravity : layoutParams3.gravity) & 112) >> 4) & -2) >> 1;
                                    iArr2[i49] = Math.max(iArr2[i49], baseline2);
                                    iArr[i49] = Math.max(iArr[i49], measuredHeight3 - baseline2);
                                    i41 = max3;
                                    z10 = z;
                                    f4 = f;
                                    i6 = i8;
                                }
                            }
                            i41 = max3;
                            z10 = z;
                            f4 = f;
                            i6 = i8;
                        }
                    } else {
                        i9 = -1;
                    }
                    z = false;
                    if (z13) {
                    }
                    i41 = max3;
                    z10 = z;
                    f4 = f;
                    i6 = i8;
                }
                i42++;
                i40 = i6;
                int i50 = i;
            }
            i4 = i2;
            this.mTotalLength += getPaddingLeft() + getPaddingRight();
            if (!(iArr2[1] == -1 && iArr2[0] == -1 && iArr2[2] == -1 && iArr2[3] == -1)) {
                i3 = Math.max(i3, Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))) + Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))));
            }
            i5 = i41;
        } else {
            i5 = Math.max(i35, i36);
            if (z12 && mode != 1073741824) {
                for (int i51 = 0; i51 < virtualChildCount; i51++) {
                    View virtualChildAt4 = getVirtualChildAt(i51);
                    if (!(virtualChildAt4 == null || virtualChildAt4.getVisibility() == 8 || ((LayoutParams) virtualChildAt4.getLayoutParams()).weight <= 0.0f)) {
                        virtualChildAt4.measure(MeasureSpec.makeMeasureSpec(i20, ErrorDialogData.SUPPRESSED), MeasureSpec.makeMeasureSpec(virtualChildAt4.getMeasuredHeight(), ErrorDialogData.SUPPRESSED));
                    }
                }
            }
            i4 = i2;
        }
        if (z10 || mode2 == 1073741824) {
            i5 = i3;
        }
        setMeasuredDimension(resolveSizeAndState | (-16777216 & i37), View.resolveSizeAndState(Math.max(i5 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i4, i37 << 16));
        if (z9) {
            forceUniformHeight(virtualChildCount, i);
        }
    }

    private void forceUniformHeight(int i, int i2) {
        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), ErrorDialogData.SUPPRESSED);
        for (int i3 = 0; i3 < i; i3++) {
            View virtualChildAt = getVirtualChildAt(i3);
            if (virtualChildAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (layoutParams.height == -1) {
                    int i4 = layoutParams.width;
                    layoutParams.width = virtualChildAt.getMeasuredWidth();
                    measureChildWithMargins(virtualChildAt, i2, 0, makeMeasureSpec, 0);
                    layoutParams.width = i4;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void measureChildBeforeLayout(View view, int i, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.mOrientation == 1) {
            layoutVertical(i, i2, i3, i4);
        } else {
            layoutHorizontal(i, i2, i3, i4);
        }
    }

    /* access modifiers changed from: 0000 */
    public void layoutVertical(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int paddingLeft = getPaddingLeft();
        int i8 = i3 - i;
        int paddingRight = i8 - getPaddingRight();
        int paddingRight2 = (i8 - paddingLeft) - getPaddingRight();
        int virtualChildCount = getVirtualChildCount();
        int i9 = this.mGravity & 112;
        int i10 = this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        if (i9 == 16) {
            i5 = (((i4 - i2) - this.mTotalLength) / 2) + getPaddingTop();
        } else if (i9 != 80) {
            i5 = getPaddingTop();
        } else {
            i5 = ((getPaddingTop() + i4) - i2) - this.mTotalLength;
        }
        int i11 = 0;
        while (i11 < virtualChildCount) {
            View virtualChildAt = getVirtualChildAt(i11);
            if (virtualChildAt == null) {
                i5 += measureNullChild(i11);
            } else if (virtualChildAt.getVisibility() != 8) {
                int measuredWidth = virtualChildAt.getMeasuredWidth();
                int measuredHeight = virtualChildAt.getMeasuredHeight();
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                int i12 = layoutParams.gravity;
                if (i12 < 0) {
                    i12 = i10;
                }
                int absoluteGravity = GravityCompat.getAbsoluteGravity(i12, ViewCompat.getLayoutDirection(this)) & 7;
                if (absoluteGravity == 1) {
                    i7 = ((((paddingRight2 - measuredWidth) / 2) + paddingLeft) + layoutParams.leftMargin) - layoutParams.rightMargin;
                } else if (absoluteGravity != 5) {
                    i7 = layoutParams.leftMargin + paddingLeft;
                } else {
                    i7 = (paddingRight - measuredWidth) - layoutParams.rightMargin;
                }
                int i13 = i7;
                if (hasDividerBeforeChildAt(i11)) {
                    i5 += this.mDividerHeight;
                }
                int i14 = i5 + layoutParams.topMargin;
                LayoutParams layoutParams2 = layoutParams;
                setChildFrame(virtualChildAt, i13, i14 + getLocationOffset(virtualChildAt), measuredWidth, measuredHeight);
                i11 += getChildrenSkipCount(virtualChildAt, i11);
                i5 = i14 + measuredHeight + layoutParams2.bottomMargin + getNextLocationOffset(virtualChildAt);
                i6 = 1;
                i11 += i6;
            }
            i6 = 1;
            i11 += i6;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010a  */
    public void layoutHorizontal(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z;
        int i11;
        boolean z2;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        int paddingTop = getPaddingTop();
        int i17 = i4 - i2;
        int paddingBottom = i17 - getPaddingBottom();
        int paddingBottom2 = (i17 - paddingTop) - getPaddingBottom();
        int virtualChildCount = getVirtualChildCount();
        int i18 = this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        int i19 = this.mGravity & 112;
        boolean z3 = this.mBaselineAligned;
        int[] iArr = this.mMaxAscent;
        int[] iArr2 = this.mMaxDescent;
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i18, ViewCompat.getLayoutDirection(this));
        boolean z4 = true;
        if (absoluteGravity == 1) {
            i5 = (((i3 - i) - this.mTotalLength) / 2) + getPaddingLeft();
        } else if (absoluteGravity != 5) {
            i5 = getPaddingLeft();
        } else {
            i5 = ((getPaddingLeft() + i3) - i) - this.mTotalLength;
        }
        if (isLayoutRtl) {
            i7 = virtualChildCount - 1;
            i6 = -1;
        } else {
            i7 = 0;
            i6 = 1;
        }
        int i20 = 0;
        while (i20 < virtualChildCount) {
            int i21 = i7 + (i6 * i20);
            View virtualChildAt = getVirtualChildAt(i21);
            if (virtualChildAt == null) {
                i5 += measureNullChild(i21);
                z2 = z4;
                i8 = paddingTop;
                i11 = virtualChildCount;
                i9 = i19;
            } else if (virtualChildAt.getVisibility() != 8) {
                int measuredWidth = virtualChildAt.getMeasuredWidth();
                int measuredHeight = virtualChildAt.getMeasuredHeight();
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (z3) {
                    i12 = i20;
                    i10 = virtualChildCount;
                    if (layoutParams.height != -1) {
                        i13 = virtualChildAt.getBaseline();
                        i14 = layoutParams.gravity;
                        if (i14 < 0) {
                            i14 = i19;
                        }
                        i15 = i14 & 112;
                        i9 = i19;
                        if (i15 != 16) {
                            z = true;
                            i16 = ((((paddingBottom2 - measuredHeight) / 2) + paddingTop) + layoutParams.topMargin) - layoutParams.bottomMargin;
                        } else if (i15 != 48) {
                            if (i15 != 80) {
                                i16 = paddingTop;
                            } else {
                                int i22 = (paddingBottom - measuredHeight) - layoutParams.bottomMargin;
                                if (i13 != -1) {
                                    i22 -= iArr2[2] - (virtualChildAt.getMeasuredHeight() - i13);
                                }
                                i16 = i22;
                            }
                            z = true;
                        } else {
                            int i23 = layoutParams.topMargin + paddingTop;
                            if (i13 != -1) {
                                z = true;
                                i23 += iArr[1] - i13;
                            } else {
                                z = true;
                            }
                            i16 = i23;
                        }
                        if (hasDividerBeforeChildAt(i21)) {
                            i5 += this.mDividerWidth;
                        }
                        int i24 = layoutParams.leftMargin + i5;
                        View view = virtualChildAt;
                        int i25 = i21;
                        int locationOffset = i24 + getLocationOffset(virtualChildAt);
                        int i26 = i12;
                        i8 = paddingTop;
                        LayoutParams layoutParams2 = layoutParams;
                        setChildFrame(virtualChildAt, locationOffset, i16, measuredWidth, measuredHeight);
                        View view2 = view;
                        i20 = i26 + getChildrenSkipCount(view2, i25);
                        i5 = i24 + measuredWidth + layoutParams2.rightMargin + getNextLocationOffset(view2);
                        i20++;
                        z4 = z;
                        virtualChildCount = i10;
                        i19 = i9;
                        paddingTop = i8;
                    }
                } else {
                    i12 = i20;
                    i10 = virtualChildCount;
                }
                i13 = -1;
                i14 = layoutParams.gravity;
                if (i14 < 0) {
                }
                i15 = i14 & 112;
                i9 = i19;
                if (i15 != 16) {
                }
                if (hasDividerBeforeChildAt(i21)) {
                }
                int i242 = layoutParams.leftMargin + i5;
                View view3 = virtualChildAt;
                int i252 = i21;
                int locationOffset2 = i242 + getLocationOffset(virtualChildAt);
                int i262 = i12;
                i8 = paddingTop;
                LayoutParams layoutParams22 = layoutParams;
                setChildFrame(virtualChildAt, locationOffset2, i16, measuredWidth, measuredHeight);
                View view22 = view3;
                i20 = i262 + getChildrenSkipCount(view22, i252);
                i5 = i242 + measuredWidth + layoutParams22.rightMargin + getNextLocationOffset(view22);
                i20++;
                z4 = z;
                virtualChildCount = i10;
                i19 = i9;
                paddingTop = i8;
            } else {
                int i27 = i20;
                i8 = paddingTop;
                i11 = virtualChildCount;
                i9 = i19;
                z2 = true;
            }
            i20++;
            z4 = z;
            virtualChildCount = i10;
            i19 = i9;
            paddingTop = i8;
        }
    }

    private void setChildFrame(View view, int i, int i2, int i3, int i4) {
        view.layout(i, i2, i3 + i, i4 + i2);
    }

    public void setOrientation(int i) {
        if (this.mOrientation != i) {
            this.mOrientation = i;
            requestLayout();
        }
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public void setGravity(int i) {
        if (this.mGravity != i) {
            if ((8388615 & i) == 0) {
                i |= GravityCompat.START;
            }
            if ((i & 112) == 0) {
                i |= 48;
            }
            this.mGravity = i;
            requestLayout();
        }
    }

    public int getGravity() {
        return this.mGravity;
    }

    public void setHorizontalGravity(int i) {
        int i2 = i & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        if ((8388615 & this.mGravity) != i2) {
            this.mGravity = i2 | (this.mGravity & -8388616);
            requestLayout();
        }
    }

    public void setVerticalGravity(int i) {
        int i2 = i & 112;
        if ((this.mGravity & 112) != i2) {
            this.mGravity = i2 | (this.mGravity & -113);
            requestLayout();
        }
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        if (this.mOrientation == 0) {
            return new LayoutParams(-2, -2);
        }
        if (this.mOrientation == 1) {
            return new LayoutParams(-1, -2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(LinearLayoutCompat.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(LinearLayoutCompat.class.getName());
    }
}
