package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.NetworkDetails;
import com.mansoon.BatteryDouble.models.data.NetworkStatistics;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class NetworkDetailsRealmProxy extends NetworkDetails implements RealmObjectProxy, NetworkDetailsRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private NetworkDetailsColumnInfo columnInfo;
    private ProxyState<NetworkDetails> proxyState;

    static final class NetworkDetailsColumnInfo extends ColumnInfo {
        long mccIndex;
        long mncIndex;
        long mobileDataActivityIndex;
        long mobileDataStatusIndex;
        long mobileNetworkTypeIndex;
        long networkOperatorIndex;
        long networkStatisticsIndex;
        long networkTypeIndex;
        long roamingEnabledIndex;
        long simOperatorIndex;
        long wifiApStatusIndex;
        long wifiLinkSpeedIndex;
        long wifiSignalStrengthIndex;
        long wifiStatusIndex;

        NetworkDetailsColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(14);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("NetworkDetails");
            this.networkTypeIndex = addColumnDetails("networkType", objectSchemaInfo);
            this.mobileNetworkTypeIndex = addColumnDetails("mobileNetworkType", objectSchemaInfo);
            this.mobileDataStatusIndex = addColumnDetails("mobileDataStatus", objectSchemaInfo);
            this.mobileDataActivityIndex = addColumnDetails("mobileDataActivity", objectSchemaInfo);
            this.roamingEnabledIndex = addColumnDetails("roamingEnabled", objectSchemaInfo);
            this.wifiStatusIndex = addColumnDetails("wifiStatus", objectSchemaInfo);
            this.wifiSignalStrengthIndex = addColumnDetails("wifiSignalStrength", objectSchemaInfo);
            this.wifiLinkSpeedIndex = addColumnDetails("wifiLinkSpeed", objectSchemaInfo);
            this.networkStatisticsIndex = addColumnDetails("networkStatistics", objectSchemaInfo);
            this.wifiApStatusIndex = addColumnDetails("wifiApStatus", objectSchemaInfo);
            this.networkOperatorIndex = addColumnDetails("networkOperator", objectSchemaInfo);
            this.simOperatorIndex = addColumnDetails("simOperator", objectSchemaInfo);
            this.mccIndex = addColumnDetails("mcc", objectSchemaInfo);
            this.mncIndex = addColumnDetails("mnc", objectSchemaInfo);
        }

        NetworkDetailsColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new NetworkDetailsColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            NetworkDetailsColumnInfo networkDetailsColumnInfo = (NetworkDetailsColumnInfo) columnInfo;
            NetworkDetailsColumnInfo networkDetailsColumnInfo2 = (NetworkDetailsColumnInfo) columnInfo2;
            networkDetailsColumnInfo2.networkTypeIndex = networkDetailsColumnInfo.networkTypeIndex;
            networkDetailsColumnInfo2.mobileNetworkTypeIndex = networkDetailsColumnInfo.mobileNetworkTypeIndex;
            networkDetailsColumnInfo2.mobileDataStatusIndex = networkDetailsColumnInfo.mobileDataStatusIndex;
            networkDetailsColumnInfo2.mobileDataActivityIndex = networkDetailsColumnInfo.mobileDataActivityIndex;
            networkDetailsColumnInfo2.roamingEnabledIndex = networkDetailsColumnInfo.roamingEnabledIndex;
            networkDetailsColumnInfo2.wifiStatusIndex = networkDetailsColumnInfo.wifiStatusIndex;
            networkDetailsColumnInfo2.wifiSignalStrengthIndex = networkDetailsColumnInfo.wifiSignalStrengthIndex;
            networkDetailsColumnInfo2.wifiLinkSpeedIndex = networkDetailsColumnInfo.wifiLinkSpeedIndex;
            networkDetailsColumnInfo2.networkStatisticsIndex = networkDetailsColumnInfo.networkStatisticsIndex;
            networkDetailsColumnInfo2.wifiApStatusIndex = networkDetailsColumnInfo.wifiApStatusIndex;
            networkDetailsColumnInfo2.networkOperatorIndex = networkDetailsColumnInfo.networkOperatorIndex;
            networkDetailsColumnInfo2.simOperatorIndex = networkDetailsColumnInfo.simOperatorIndex;
            networkDetailsColumnInfo2.mccIndex = networkDetailsColumnInfo.mccIndex;
            networkDetailsColumnInfo2.mncIndex = networkDetailsColumnInfo.mncIndex;
        }
    }

    public static String getTableName() {
        return "class_NetworkDetails";
    }

    static {
        ArrayList arrayList = new ArrayList(14);
        arrayList.add("networkType");
        arrayList.add("mobileNetworkType");
        arrayList.add("mobileDataStatus");
        arrayList.add("mobileDataActivity");
        arrayList.add("roamingEnabled");
        arrayList.add("wifiStatus");
        arrayList.add("wifiSignalStrength");
        arrayList.add("wifiLinkSpeed");
        arrayList.add("networkStatistics");
        arrayList.add("wifiApStatus");
        arrayList.add("networkOperator");
        arrayList.add("simOperator");
        arrayList.add("mcc");
        arrayList.add("mnc");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    NetworkDetailsRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (NetworkDetailsColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$networkType() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.networkTypeIndex);
    }

    public void realmSet$networkType(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.networkTypeIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.networkTypeIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.networkTypeIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.networkTypeIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$mobileNetworkType() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.mobileNetworkTypeIndex);
    }

    public void realmSet$mobileNetworkType(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.mobileNetworkTypeIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.mobileNetworkTypeIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.mobileNetworkTypeIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.mobileNetworkTypeIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$mobileDataStatus() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.mobileDataStatusIndex);
    }

    public void realmSet$mobileDataStatus(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.mobileDataStatusIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.mobileDataStatusIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.mobileDataStatusIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.mobileDataStatusIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$mobileDataActivity() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.mobileDataActivityIndex);
    }

    public void realmSet$mobileDataActivity(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.mobileDataActivityIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.mobileDataActivityIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.mobileDataActivityIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.mobileDataActivityIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public int realmGet$roamingEnabled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.roamingEnabledIndex);
    }

    public void realmSet$roamingEnabled(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.roamingEnabledIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.roamingEnabledIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$wifiStatus() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.wifiStatusIndex);
    }

    public void realmSet$wifiStatus(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.wifiStatusIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.wifiStatusIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.wifiStatusIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.wifiStatusIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public int realmGet$wifiSignalStrength() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.wifiSignalStrengthIndex);
    }

    public void realmSet$wifiSignalStrength(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.wifiSignalStrengthIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.wifiSignalStrengthIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$wifiLinkSpeed() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.wifiLinkSpeedIndex);
    }

    public void realmSet$wifiLinkSpeed(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.wifiLinkSpeedIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.wifiLinkSpeedIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public NetworkStatistics realmGet$networkStatistics() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.networkStatisticsIndex)) {
            return null;
        }
        return (NetworkStatistics) this.proxyState.getRealm$realm().get(NetworkStatistics.class, this.proxyState.getRow$realm().getLink(this.columnInfo.networkStatisticsIndex), false, Collections.emptyList());
    }

    public void realmSet$networkStatistics(NetworkStatistics networkStatistics) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (networkStatistics == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.networkStatisticsIndex);
                return;
            }
            this.proxyState.checkValidObject(networkStatistics);
            this.proxyState.getRow$realm().setLink(this.columnInfo.networkStatisticsIndex, ((RealmObjectProxy) networkStatistics).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("networkStatistics")) {
            if (networkStatistics != null && !RealmObject.isManaged(networkStatistics)) {
                networkStatistics = (NetworkStatistics) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(networkStatistics);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (networkStatistics == null) {
                row$realm.nullifyLink(this.columnInfo.networkStatisticsIndex);
                return;
            }
            this.proxyState.checkValidObject(networkStatistics);
            row$realm.getTable().setLink(this.columnInfo.networkStatisticsIndex, row$realm.getIndex(), ((RealmObjectProxy) networkStatistics).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public String realmGet$wifiApStatus() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.wifiApStatusIndex);
    }

    public void realmSet$wifiApStatus(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.wifiApStatusIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.wifiApStatusIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.wifiApStatusIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.wifiApStatusIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$networkOperator() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.networkOperatorIndex);
    }

    public void realmSet$networkOperator(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.networkOperatorIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.networkOperatorIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.networkOperatorIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.networkOperatorIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$simOperator() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.simOperatorIndex);
    }

    public void realmSet$simOperator(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.simOperatorIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.simOperatorIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.simOperatorIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.simOperatorIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$mcc() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.mccIndex);
    }

    public void realmSet$mcc(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.mccIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.mccIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.mccIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.mccIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$mnc() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.mncIndex);
    }

    public void realmSet$mnc(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.mncIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.mncIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.mncIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.mncIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("NetworkDetails", 14, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("networkType", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("mobileNetworkType", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("mobileDataStatus", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("mobileDataActivity", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("roamingEnabled", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("wifiStatus", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("wifiSignalStrength", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("wifiLinkSpeed", RealmFieldType.INTEGER, false, false, true);
        builder.addPersistedLinkProperty("networkStatistics", RealmFieldType.OBJECT, "NetworkStatistics");
        Builder builder3 = builder;
        builder3.addPersistedProperty("wifiApStatus", RealmFieldType.STRING, false, false, false);
        builder3.addPersistedProperty("networkOperator", RealmFieldType.STRING, false, false, false);
        builder3.addPersistedProperty("simOperator", RealmFieldType.STRING, false, false, false);
        builder3.addPersistedProperty("mcc", RealmFieldType.STRING, false, false, false);
        builder3.addPersistedProperty("mnc", RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static NetworkDetailsColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new NetworkDetailsColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static NetworkDetails createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        ArrayList arrayList = new ArrayList(1);
        if (jSONObject.has("networkStatistics")) {
            arrayList.add("networkStatistics");
        }
        NetworkDetails networkDetails = (NetworkDetails) realm.createObjectInternal(NetworkDetails.class, true, arrayList);
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails;
        if (jSONObject.has("networkType")) {
            if (jSONObject.isNull("networkType")) {
                networkDetailsRealmProxyInterface.realmSet$networkType(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$networkType(jSONObject.getString("networkType"));
            }
        }
        if (jSONObject.has("mobileNetworkType")) {
            if (jSONObject.isNull("mobileNetworkType")) {
                networkDetailsRealmProxyInterface.realmSet$mobileNetworkType(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$mobileNetworkType(jSONObject.getString("mobileNetworkType"));
            }
        }
        if (jSONObject.has("mobileDataStatus")) {
            if (jSONObject.isNull("mobileDataStatus")) {
                networkDetailsRealmProxyInterface.realmSet$mobileDataStatus(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$mobileDataStatus(jSONObject.getString("mobileDataStatus"));
            }
        }
        if (jSONObject.has("mobileDataActivity")) {
            if (jSONObject.isNull("mobileDataActivity")) {
                networkDetailsRealmProxyInterface.realmSet$mobileDataActivity(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$mobileDataActivity(jSONObject.getString("mobileDataActivity"));
            }
        }
        if (jSONObject.has("roamingEnabled")) {
            if (jSONObject.isNull("roamingEnabled")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'roamingEnabled' to null.");
            }
            networkDetailsRealmProxyInterface.realmSet$roamingEnabled(jSONObject.getInt("roamingEnabled"));
        }
        if (jSONObject.has("wifiStatus")) {
            if (jSONObject.isNull("wifiStatus")) {
                networkDetailsRealmProxyInterface.realmSet$wifiStatus(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$wifiStatus(jSONObject.getString("wifiStatus"));
            }
        }
        if (jSONObject.has("wifiSignalStrength")) {
            if (jSONObject.isNull("wifiSignalStrength")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'wifiSignalStrength' to null.");
            }
            networkDetailsRealmProxyInterface.realmSet$wifiSignalStrength(jSONObject.getInt("wifiSignalStrength"));
        }
        if (jSONObject.has("wifiLinkSpeed")) {
            if (jSONObject.isNull("wifiLinkSpeed")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'wifiLinkSpeed' to null.");
            }
            networkDetailsRealmProxyInterface.realmSet$wifiLinkSpeed(jSONObject.getInt("wifiLinkSpeed"));
        }
        if (jSONObject.has("networkStatistics")) {
            if (jSONObject.isNull("networkStatistics")) {
                networkDetailsRealmProxyInterface.realmSet$networkStatistics(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$networkStatistics(NetworkStatisticsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("networkStatistics"), z));
            }
        }
        if (jSONObject.has("wifiApStatus")) {
            if (jSONObject.isNull("wifiApStatus")) {
                networkDetailsRealmProxyInterface.realmSet$wifiApStatus(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$wifiApStatus(jSONObject.getString("wifiApStatus"));
            }
        }
        if (jSONObject.has("networkOperator")) {
            if (jSONObject.isNull("networkOperator")) {
                networkDetailsRealmProxyInterface.realmSet$networkOperator(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$networkOperator(jSONObject.getString("networkOperator"));
            }
        }
        if (jSONObject.has("simOperator")) {
            if (jSONObject.isNull("simOperator")) {
                networkDetailsRealmProxyInterface.realmSet$simOperator(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$simOperator(jSONObject.getString("simOperator"));
            }
        }
        if (jSONObject.has("mcc")) {
            if (jSONObject.isNull("mcc")) {
                networkDetailsRealmProxyInterface.realmSet$mcc(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$mcc(jSONObject.getString("mcc"));
            }
        }
        if (jSONObject.has("mnc")) {
            if (jSONObject.isNull("mnc")) {
                networkDetailsRealmProxyInterface.realmSet$mnc(null);
            } else {
                networkDetailsRealmProxyInterface.realmSet$mnc(jSONObject.getString("mnc"));
            }
        }
        return networkDetails;
    }

    @TargetApi(11)
    public static NetworkDetails createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        NetworkDetails networkDetails = new NetworkDetails();
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("networkType")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$networkType(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$networkType(null);
                }
            } else if (nextName.equals("mobileNetworkType")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$mobileNetworkType(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$mobileNetworkType(null);
                }
            } else if (nextName.equals("mobileDataStatus")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$mobileDataStatus(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$mobileDataStatus(null);
                }
            } else if (nextName.equals("mobileDataActivity")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$mobileDataActivity(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$mobileDataActivity(null);
                }
            } else if (nextName.equals("roamingEnabled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$roamingEnabled(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'roamingEnabled' to null.");
                }
            } else if (nextName.equals("wifiStatus")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$wifiStatus(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$wifiStatus(null);
                }
            } else if (nextName.equals("wifiSignalStrength")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$wifiSignalStrength(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'wifiSignalStrength' to null.");
                }
            } else if (nextName.equals("wifiLinkSpeed")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$wifiLinkSpeed(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'wifiLinkSpeed' to null.");
                }
            } else if (nextName.equals("networkStatistics")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$networkStatistics(null);
                } else {
                    networkDetailsRealmProxyInterface.realmSet$networkStatistics(NetworkStatisticsRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("wifiApStatus")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$wifiApStatus(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$wifiApStatus(null);
                }
            } else if (nextName.equals("networkOperator")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$networkOperator(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$networkOperator(null);
                }
            } else if (nextName.equals("simOperator")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$simOperator(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$simOperator(null);
                }
            } else if (nextName.equals("mcc")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    networkDetailsRealmProxyInterface.realmSet$mcc(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    networkDetailsRealmProxyInterface.realmSet$mcc(null);
                }
            } else if (!nextName.equals("mnc")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                networkDetailsRealmProxyInterface.realmSet$mnc(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                networkDetailsRealmProxyInterface.realmSet$mnc(null);
            }
        }
        jsonReader.endObject();
        return (NetworkDetails) realm.copyToRealm(networkDetails);
    }

    public static NetworkDetails copyOrUpdate(Realm realm, NetworkDetails networkDetails, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (networkDetails instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkDetails;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return networkDetails;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(networkDetails);
        if (realmObjectProxy2 != null) {
            return (NetworkDetails) realmObjectProxy2;
        }
        return copy(realm, networkDetails, z, map);
    }

    public static NetworkDetails copy(Realm realm, NetworkDetails networkDetails, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(networkDetails);
        if (realmObjectProxy != null) {
            return (NetworkDetails) realmObjectProxy;
        }
        NetworkDetails networkDetails2 = (NetworkDetails) realm.createObjectInternal(NetworkDetails.class, false, Collections.emptyList());
        map.put(networkDetails, (RealmObjectProxy) networkDetails2);
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails;
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface2 = networkDetails2;
        networkDetailsRealmProxyInterface2.realmSet$networkType(networkDetailsRealmProxyInterface.realmGet$networkType());
        networkDetailsRealmProxyInterface2.realmSet$mobileNetworkType(networkDetailsRealmProxyInterface.realmGet$mobileNetworkType());
        networkDetailsRealmProxyInterface2.realmSet$mobileDataStatus(networkDetailsRealmProxyInterface.realmGet$mobileDataStatus());
        networkDetailsRealmProxyInterface2.realmSet$mobileDataActivity(networkDetailsRealmProxyInterface.realmGet$mobileDataActivity());
        networkDetailsRealmProxyInterface2.realmSet$roamingEnabled(networkDetailsRealmProxyInterface.realmGet$roamingEnabled());
        networkDetailsRealmProxyInterface2.realmSet$wifiStatus(networkDetailsRealmProxyInterface.realmGet$wifiStatus());
        networkDetailsRealmProxyInterface2.realmSet$wifiSignalStrength(networkDetailsRealmProxyInterface.realmGet$wifiSignalStrength());
        networkDetailsRealmProxyInterface2.realmSet$wifiLinkSpeed(networkDetailsRealmProxyInterface.realmGet$wifiLinkSpeed());
        NetworkStatistics realmGet$networkStatistics = networkDetailsRealmProxyInterface.realmGet$networkStatistics();
        if (realmGet$networkStatistics == null) {
            networkDetailsRealmProxyInterface2.realmSet$networkStatistics(null);
        } else {
            NetworkStatistics networkStatistics = (NetworkStatistics) map.get(realmGet$networkStatistics);
            if (networkStatistics != null) {
                networkDetailsRealmProxyInterface2.realmSet$networkStatistics(networkStatistics);
            } else {
                networkDetailsRealmProxyInterface2.realmSet$networkStatistics(NetworkStatisticsRealmProxy.copyOrUpdate(realm, realmGet$networkStatistics, z, map));
            }
        }
        networkDetailsRealmProxyInterface2.realmSet$wifiApStatus(networkDetailsRealmProxyInterface.realmGet$wifiApStatus());
        networkDetailsRealmProxyInterface2.realmSet$networkOperator(networkDetailsRealmProxyInterface.realmGet$networkOperator());
        networkDetailsRealmProxyInterface2.realmSet$simOperator(networkDetailsRealmProxyInterface.realmGet$simOperator());
        networkDetailsRealmProxyInterface2.realmSet$mcc(networkDetailsRealmProxyInterface.realmGet$mcc());
        networkDetailsRealmProxyInterface2.realmSet$mnc(networkDetailsRealmProxyInterface.realmGet$mnc());
        return networkDetails2;
    }

    public static long insert(Realm realm, NetworkDetails networkDetails, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        NetworkDetails networkDetails2 = networkDetails;
        Map<RealmModel, Long> map2 = map;
        if (networkDetails2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkDetails2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(NetworkDetails.class);
        long nativePtr = table.getNativePtr();
        NetworkDetailsColumnInfo networkDetailsColumnInfo = (NetworkDetailsColumnInfo) realm.getSchema().getColumnInfo(NetworkDetails.class);
        long createRow = OsObject.createRow(table);
        map2.put(networkDetails2, Long.valueOf(createRow));
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails2;
        String realmGet$networkType = networkDetailsRealmProxyInterface.realmGet$networkType();
        if (realmGet$networkType != null) {
            j = createRow;
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkTypeIndex, createRow, realmGet$networkType, false);
        } else {
            j = createRow;
        }
        String realmGet$mobileNetworkType = networkDetailsRealmProxyInterface.realmGet$mobileNetworkType();
        if (realmGet$mobileNetworkType != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileNetworkTypeIndex, j, realmGet$mobileNetworkType, false);
        }
        String realmGet$mobileDataStatus = networkDetailsRealmProxyInterface.realmGet$mobileDataStatus();
        if (realmGet$mobileDataStatus != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataStatusIndex, j, realmGet$mobileDataStatus, false);
        }
        String realmGet$mobileDataActivity = networkDetailsRealmProxyInterface.realmGet$mobileDataActivity();
        if (realmGet$mobileDataActivity != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataActivityIndex, j, realmGet$mobileDataActivity, false);
        }
        Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.roamingEnabledIndex, j, (long) networkDetailsRealmProxyInterface.realmGet$roamingEnabled(), false);
        String realmGet$wifiStatus = networkDetailsRealmProxyInterface.realmGet$wifiStatus();
        if (realmGet$wifiStatus != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiStatusIndex, j, realmGet$wifiStatus, false);
        }
        long j2 = nativePtr;
        long j3 = j;
        Table.nativeSetLong(j2, networkDetailsColumnInfo.wifiSignalStrengthIndex, j3, (long) networkDetailsRealmProxyInterface.realmGet$wifiSignalStrength(), false);
        Table.nativeSetLong(j2, networkDetailsColumnInfo.wifiLinkSpeedIndex, j3, (long) networkDetailsRealmProxyInterface.realmGet$wifiLinkSpeed(), false);
        NetworkStatistics realmGet$networkStatistics = networkDetailsRealmProxyInterface.realmGet$networkStatistics();
        if (realmGet$networkStatistics != null) {
            Long l = (Long) map2.get(realmGet$networkStatistics);
            if (l == null) {
                l = Long.valueOf(NetworkStatisticsRealmProxy.insert(realm2, realmGet$networkStatistics, map2));
            }
            Table.nativeSetLink(nativePtr, networkDetailsColumnInfo.networkStatisticsIndex, j, l.longValue(), false);
        }
        String realmGet$wifiApStatus = networkDetailsRealmProxyInterface.realmGet$wifiApStatus();
        if (realmGet$wifiApStatus != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiApStatusIndex, j, realmGet$wifiApStatus, false);
        }
        String realmGet$networkOperator = networkDetailsRealmProxyInterface.realmGet$networkOperator();
        if (realmGet$networkOperator != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkOperatorIndex, j, realmGet$networkOperator, false);
        }
        String realmGet$simOperator = networkDetailsRealmProxyInterface.realmGet$simOperator();
        if (realmGet$simOperator != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.simOperatorIndex, j, realmGet$simOperator, false);
        }
        String realmGet$mcc = networkDetailsRealmProxyInterface.realmGet$mcc();
        if (realmGet$mcc != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mccIndex, j, realmGet$mcc, false);
        }
        String realmGet$mnc = networkDetailsRealmProxyInterface.realmGet$mnc();
        if (realmGet$mnc != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mncIndex, j, realmGet$mnc, false);
        }
        return j;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(NetworkDetails.class);
        long nativePtr = table.getNativePtr();
        NetworkDetailsColumnInfo networkDetailsColumnInfo = (NetworkDetailsColumnInfo) realm.getSchema().getColumnInfo(NetworkDetails.class);
        while (it.hasNext()) {
            NetworkDetails networkDetails = (NetworkDetails) it.next();
            if (!map2.containsKey(networkDetails)) {
                if (networkDetails instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkDetails;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(networkDetails, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(networkDetails, Long.valueOf(createRow));
                NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails;
                String realmGet$networkType = networkDetailsRealmProxyInterface.realmGet$networkType();
                if (realmGet$networkType != null) {
                    j = createRow;
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkTypeIndex, createRow, realmGet$networkType, false);
                } else {
                    j = createRow;
                }
                String realmGet$mobileNetworkType = networkDetailsRealmProxyInterface.realmGet$mobileNetworkType();
                if (realmGet$mobileNetworkType != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileNetworkTypeIndex, j, realmGet$mobileNetworkType, false);
                }
                String realmGet$mobileDataStatus = networkDetailsRealmProxyInterface.realmGet$mobileDataStatus();
                if (realmGet$mobileDataStatus != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataStatusIndex, j, realmGet$mobileDataStatus, false);
                }
                String realmGet$mobileDataActivity = networkDetailsRealmProxyInterface.realmGet$mobileDataActivity();
                if (realmGet$mobileDataActivity != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataActivityIndex, j, realmGet$mobileDataActivity, false);
                }
                Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.roamingEnabledIndex, j, (long) networkDetailsRealmProxyInterface.realmGet$roamingEnabled(), false);
                String realmGet$wifiStatus = networkDetailsRealmProxyInterface.realmGet$wifiStatus();
                if (realmGet$wifiStatus != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiStatusIndex, j, realmGet$wifiStatus, false);
                }
                long j2 = j;
                Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.wifiSignalStrengthIndex, j2, (long) networkDetailsRealmProxyInterface.realmGet$wifiSignalStrength(), false);
                Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.wifiLinkSpeedIndex, j2, (long) networkDetailsRealmProxyInterface.realmGet$wifiLinkSpeed(), false);
                NetworkStatistics realmGet$networkStatistics = networkDetailsRealmProxyInterface.realmGet$networkStatistics();
                if (realmGet$networkStatistics != null) {
                    Long l = (Long) map2.get(realmGet$networkStatistics);
                    if (l == null) {
                        l = Long.valueOf(NetworkStatisticsRealmProxy.insert(realm2, realmGet$networkStatistics, map2));
                    }
                    table.setLink(networkDetailsColumnInfo.networkStatisticsIndex, j, l.longValue(), false);
                }
                String realmGet$wifiApStatus = networkDetailsRealmProxyInterface.realmGet$wifiApStatus();
                if (realmGet$wifiApStatus != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiApStatusIndex, j, realmGet$wifiApStatus, false);
                }
                String realmGet$networkOperator = networkDetailsRealmProxyInterface.realmGet$networkOperator();
                if (realmGet$networkOperator != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkOperatorIndex, j, realmGet$networkOperator, false);
                }
                String realmGet$simOperator = networkDetailsRealmProxyInterface.realmGet$simOperator();
                if (realmGet$simOperator != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.simOperatorIndex, j, realmGet$simOperator, false);
                }
                String realmGet$mcc = networkDetailsRealmProxyInterface.realmGet$mcc();
                if (realmGet$mcc != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mccIndex, j, realmGet$mcc, false);
                }
                String realmGet$mnc = networkDetailsRealmProxyInterface.realmGet$mnc();
                if (realmGet$mnc != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mncIndex, j, realmGet$mnc, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, NetworkDetails networkDetails, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        NetworkDetails networkDetails2 = networkDetails;
        Map<RealmModel, Long> map2 = map;
        if (networkDetails2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkDetails2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(NetworkDetails.class);
        long nativePtr = table.getNativePtr();
        NetworkDetailsColumnInfo networkDetailsColumnInfo = (NetworkDetailsColumnInfo) realm.getSchema().getColumnInfo(NetworkDetails.class);
        long createRow = OsObject.createRow(table);
        map2.put(networkDetails2, Long.valueOf(createRow));
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails2;
        String realmGet$networkType = networkDetailsRealmProxyInterface.realmGet$networkType();
        if (realmGet$networkType != null) {
            j = createRow;
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkTypeIndex, createRow, realmGet$networkType, false);
        } else {
            j = createRow;
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.networkTypeIndex, j, false);
        }
        String realmGet$mobileNetworkType = networkDetailsRealmProxyInterface.realmGet$mobileNetworkType();
        if (realmGet$mobileNetworkType != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileNetworkTypeIndex, j, realmGet$mobileNetworkType, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mobileNetworkTypeIndex, j, false);
        }
        String realmGet$mobileDataStatus = networkDetailsRealmProxyInterface.realmGet$mobileDataStatus();
        if (realmGet$mobileDataStatus != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataStatusIndex, j, realmGet$mobileDataStatus, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mobileDataStatusIndex, j, false);
        }
        String realmGet$mobileDataActivity = networkDetailsRealmProxyInterface.realmGet$mobileDataActivity();
        if (realmGet$mobileDataActivity != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataActivityIndex, j, realmGet$mobileDataActivity, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mobileDataActivityIndex, j, false);
        }
        Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.roamingEnabledIndex, j, (long) networkDetailsRealmProxyInterface.realmGet$roamingEnabled(), false);
        String realmGet$wifiStatus = networkDetailsRealmProxyInterface.realmGet$wifiStatus();
        if (realmGet$wifiStatus != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiStatusIndex, j, realmGet$wifiStatus, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.wifiStatusIndex, j, false);
        }
        long j2 = nativePtr;
        long j3 = j;
        Table.nativeSetLong(j2, networkDetailsColumnInfo.wifiSignalStrengthIndex, j3, (long) networkDetailsRealmProxyInterface.realmGet$wifiSignalStrength(), false);
        Table.nativeSetLong(j2, networkDetailsColumnInfo.wifiLinkSpeedIndex, j3, (long) networkDetailsRealmProxyInterface.realmGet$wifiLinkSpeed(), false);
        NetworkStatistics realmGet$networkStatistics = networkDetailsRealmProxyInterface.realmGet$networkStatistics();
        if (realmGet$networkStatistics != null) {
            Long l = (Long) map2.get(realmGet$networkStatistics);
            if (l == null) {
                l = Long.valueOf(NetworkStatisticsRealmProxy.insertOrUpdate(realm2, realmGet$networkStatistics, map2));
            }
            Table.nativeSetLink(nativePtr, networkDetailsColumnInfo.networkStatisticsIndex, j, l.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, networkDetailsColumnInfo.networkStatisticsIndex, j);
        }
        String realmGet$wifiApStatus = networkDetailsRealmProxyInterface.realmGet$wifiApStatus();
        if (realmGet$wifiApStatus != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiApStatusIndex, j, realmGet$wifiApStatus, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.wifiApStatusIndex, j, false);
        }
        String realmGet$networkOperator = networkDetailsRealmProxyInterface.realmGet$networkOperator();
        if (realmGet$networkOperator != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkOperatorIndex, j, realmGet$networkOperator, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.networkOperatorIndex, j, false);
        }
        String realmGet$simOperator = networkDetailsRealmProxyInterface.realmGet$simOperator();
        if (realmGet$simOperator != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.simOperatorIndex, j, realmGet$simOperator, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.simOperatorIndex, j, false);
        }
        String realmGet$mcc = networkDetailsRealmProxyInterface.realmGet$mcc();
        if (realmGet$mcc != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mccIndex, j, realmGet$mcc, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mccIndex, j, false);
        }
        String realmGet$mnc = networkDetailsRealmProxyInterface.realmGet$mnc();
        if (realmGet$mnc != null) {
            Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mncIndex, j, realmGet$mnc, false);
        } else {
            Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mncIndex, j, false);
        }
        return j;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(NetworkDetails.class);
        long nativePtr = table.getNativePtr();
        NetworkDetailsColumnInfo networkDetailsColumnInfo = (NetworkDetailsColumnInfo) realm.getSchema().getColumnInfo(NetworkDetails.class);
        while (it.hasNext()) {
            NetworkDetails networkDetails = (NetworkDetails) it.next();
            if (!map2.containsKey(networkDetails)) {
                if (networkDetails instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) networkDetails;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(networkDetails, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(networkDetails, Long.valueOf(createRow));
                NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails;
                String realmGet$networkType = networkDetailsRealmProxyInterface.realmGet$networkType();
                if (realmGet$networkType != null) {
                    j = createRow;
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkTypeIndex, createRow, realmGet$networkType, false);
                } else {
                    j = createRow;
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.networkTypeIndex, j, false);
                }
                String realmGet$mobileNetworkType = networkDetailsRealmProxyInterface.realmGet$mobileNetworkType();
                if (realmGet$mobileNetworkType != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileNetworkTypeIndex, j, realmGet$mobileNetworkType, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mobileNetworkTypeIndex, j, false);
                }
                String realmGet$mobileDataStatus = networkDetailsRealmProxyInterface.realmGet$mobileDataStatus();
                if (realmGet$mobileDataStatus != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataStatusIndex, j, realmGet$mobileDataStatus, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mobileDataStatusIndex, j, false);
                }
                String realmGet$mobileDataActivity = networkDetailsRealmProxyInterface.realmGet$mobileDataActivity();
                if (realmGet$mobileDataActivity != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mobileDataActivityIndex, j, realmGet$mobileDataActivity, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mobileDataActivityIndex, j, false);
                }
                Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.roamingEnabledIndex, j, (long) networkDetailsRealmProxyInterface.realmGet$roamingEnabled(), false);
                String realmGet$wifiStatus = networkDetailsRealmProxyInterface.realmGet$wifiStatus();
                if (realmGet$wifiStatus != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiStatusIndex, j, realmGet$wifiStatus, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.wifiStatusIndex, j, false);
                }
                long j2 = j;
                Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.wifiSignalStrengthIndex, j2, (long) networkDetailsRealmProxyInterface.realmGet$wifiSignalStrength(), false);
                Table.nativeSetLong(nativePtr, networkDetailsColumnInfo.wifiLinkSpeedIndex, j2, (long) networkDetailsRealmProxyInterface.realmGet$wifiLinkSpeed(), false);
                NetworkStatistics realmGet$networkStatistics = networkDetailsRealmProxyInterface.realmGet$networkStatistics();
                if (realmGet$networkStatistics != null) {
                    Long l = (Long) map2.get(realmGet$networkStatistics);
                    if (l == null) {
                        l = Long.valueOf(NetworkStatisticsRealmProxy.insertOrUpdate(realm2, realmGet$networkStatistics, map2));
                    }
                    Table.nativeSetLink(nativePtr, networkDetailsColumnInfo.networkStatisticsIndex, j, l.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, networkDetailsColumnInfo.networkStatisticsIndex, j);
                }
                String realmGet$wifiApStatus = networkDetailsRealmProxyInterface.realmGet$wifiApStatus();
                if (realmGet$wifiApStatus != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.wifiApStatusIndex, j, realmGet$wifiApStatus, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.wifiApStatusIndex, j, false);
                }
                String realmGet$networkOperator = networkDetailsRealmProxyInterface.realmGet$networkOperator();
                if (realmGet$networkOperator != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.networkOperatorIndex, j, realmGet$networkOperator, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.networkOperatorIndex, j, false);
                }
                String realmGet$simOperator = networkDetailsRealmProxyInterface.realmGet$simOperator();
                if (realmGet$simOperator != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.simOperatorIndex, j, realmGet$simOperator, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.simOperatorIndex, j, false);
                }
                String realmGet$mcc = networkDetailsRealmProxyInterface.realmGet$mcc();
                if (realmGet$mcc != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mccIndex, j, realmGet$mcc, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mccIndex, j, false);
                }
                String realmGet$mnc = networkDetailsRealmProxyInterface.realmGet$mnc();
                if (realmGet$mnc != null) {
                    Table.nativeSetString(nativePtr, networkDetailsColumnInfo.mncIndex, j, realmGet$mnc, false);
                } else {
                    Table.nativeSetNull(nativePtr, networkDetailsColumnInfo.mncIndex, j, false);
                }
            }
        }
    }

    public static NetworkDetails createDetachedCopy(NetworkDetails networkDetails, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        NetworkDetails networkDetails2;
        if (i > i2 || networkDetails == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(networkDetails);
        if (cacheData == null) {
            networkDetails2 = new NetworkDetails();
            map.put(networkDetails, new CacheData(i, networkDetails2));
        } else if (i >= cacheData.minDepth) {
            return (NetworkDetails) cacheData.object;
        } else {
            NetworkDetails networkDetails3 = (NetworkDetails) cacheData.object;
            cacheData.minDepth = i;
            networkDetails2 = networkDetails3;
        }
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface = networkDetails2;
        NetworkDetailsRealmProxyInterface networkDetailsRealmProxyInterface2 = networkDetails;
        networkDetailsRealmProxyInterface.realmSet$networkType(networkDetailsRealmProxyInterface2.realmGet$networkType());
        networkDetailsRealmProxyInterface.realmSet$mobileNetworkType(networkDetailsRealmProxyInterface2.realmGet$mobileNetworkType());
        networkDetailsRealmProxyInterface.realmSet$mobileDataStatus(networkDetailsRealmProxyInterface2.realmGet$mobileDataStatus());
        networkDetailsRealmProxyInterface.realmSet$mobileDataActivity(networkDetailsRealmProxyInterface2.realmGet$mobileDataActivity());
        networkDetailsRealmProxyInterface.realmSet$roamingEnabled(networkDetailsRealmProxyInterface2.realmGet$roamingEnabled());
        networkDetailsRealmProxyInterface.realmSet$wifiStatus(networkDetailsRealmProxyInterface2.realmGet$wifiStatus());
        networkDetailsRealmProxyInterface.realmSet$wifiSignalStrength(networkDetailsRealmProxyInterface2.realmGet$wifiSignalStrength());
        networkDetailsRealmProxyInterface.realmSet$wifiLinkSpeed(networkDetailsRealmProxyInterface2.realmGet$wifiLinkSpeed());
        networkDetailsRealmProxyInterface.realmSet$networkStatistics(NetworkStatisticsRealmProxy.createDetachedCopy(networkDetailsRealmProxyInterface2.realmGet$networkStatistics(), i + 1, i2, map));
        networkDetailsRealmProxyInterface.realmSet$wifiApStatus(networkDetailsRealmProxyInterface2.realmGet$wifiApStatus());
        networkDetailsRealmProxyInterface.realmSet$networkOperator(networkDetailsRealmProxyInterface2.realmGet$networkOperator());
        networkDetailsRealmProxyInterface.realmSet$simOperator(networkDetailsRealmProxyInterface2.realmGet$simOperator());
        networkDetailsRealmProxyInterface.realmSet$mcc(networkDetailsRealmProxyInterface2.realmGet$mcc());
        networkDetailsRealmProxyInterface.realmSet$mnc(networkDetailsRealmProxyInterface2.realmGet$mnc());
        return networkDetails2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("NetworkDetails = proxy[");
        sb.append("{networkType:");
        sb.append(realmGet$networkType() != null ? realmGet$networkType() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mobileNetworkType:");
        sb.append(realmGet$mobileNetworkType() != null ? realmGet$mobileNetworkType() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mobileDataStatus:");
        sb.append(realmGet$mobileDataStatus() != null ? realmGet$mobileDataStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mobileDataActivity:");
        sb.append(realmGet$mobileDataActivity() != null ? realmGet$mobileDataActivity() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{roamingEnabled:");
        sb.append(realmGet$roamingEnabled());
        sb.append("}");
        sb.append(",");
        sb.append("{wifiStatus:");
        sb.append(realmGet$wifiStatus() != null ? realmGet$wifiStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{wifiSignalStrength:");
        sb.append(realmGet$wifiSignalStrength());
        sb.append("}");
        sb.append(",");
        sb.append("{wifiLinkSpeed:");
        sb.append(realmGet$wifiLinkSpeed());
        sb.append("}");
        sb.append(",");
        sb.append("{networkStatistics:");
        sb.append(realmGet$networkStatistics() != null ? "NetworkStatistics" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{wifiApStatus:");
        sb.append(realmGet$wifiApStatus() != null ? realmGet$wifiApStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{networkOperator:");
        sb.append(realmGet$networkOperator() != null ? realmGet$networkOperator() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{simOperator:");
        sb.append(realmGet$simOperator() != null ? realmGet$simOperator() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mcc:");
        sb.append(realmGet$mcc() != null ? realmGet$mcc() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{mnc:");
        sb.append(realmGet$mnc() != null ? realmGet$mnc() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NetworkDetailsRealmProxy networkDetailsRealmProxy = (NetworkDetailsRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = networkDetailsRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = networkDetailsRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == networkDetailsRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
