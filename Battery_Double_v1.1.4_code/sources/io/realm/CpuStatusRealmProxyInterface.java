package io.realm;

public interface CpuStatusRealmProxyInterface {
    double realmGet$cpuUsage();

    long realmGet$sleepTime();

    long realmGet$upTime();

    void realmSet$cpuUsage(double d);

    void realmSet$sleepTime(long j);

    void realmSet$upTime(long j);
}
