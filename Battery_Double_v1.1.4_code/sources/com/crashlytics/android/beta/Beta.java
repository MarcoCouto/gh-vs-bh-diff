package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.cache.MemoryValueCache;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeviceIdentifierProvider;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType;
import io.fabric.sdk.android.services.common.SystemCurrentTimeProvider;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.persistence.PreferenceStoreImpl;
import io.fabric.sdk.android.services.settings.BetaSettingsData;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Beta extends Kit<Boolean> implements DeviceIdentifierProvider {
    private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
    private static final String CRASHLYTICS_BUILD_PROPERTIES = "crashlytics-build.properties";
    static final String NO_DEVICE_TOKEN = "";
    public static final String TAG = "Beta";
    private final MemoryValueCache<String> deviceTokenCache = new MemoryValueCache<>();
    private final DeviceTokenLoader deviceTokenLoader = new DeviceTokenLoader();
    private UpdatesController updatesController;

    public String getIdentifier() {
        return "com.crashlytics.sdk.android:beta";
    }

    public String getVersion() {
        return "1.2.7.19";
    }

    public static Beta getInstance() {
        return (Beta) Fabric.getKit(Beta.class);
    }

    /* access modifiers changed from: protected */
    @TargetApi(14)
    public boolean onPreExecute() {
        this.updatesController = createUpdatesController(VERSION.SDK_INT, (Application) getContext().getApplicationContext());
        return true;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground() {
        Fabric.getLogger().d(TAG, "Beta kit initializing...");
        Context context = getContext();
        IdManager idManager = getIdManager();
        if (TextUtils.isEmpty(getBetaDeviceToken(context, idManager.getInstallerPackageName()))) {
            Fabric.getLogger().d(TAG, "A Beta device token was not found for this app");
            return Boolean.valueOf(false);
        }
        Fabric.getLogger().d(TAG, "Beta device token is present, checking for app updates.");
        BetaSettingsData betaSettingsData = getBetaSettingsData();
        BuildProperties loadBuildProperties = loadBuildProperties(context);
        if (canCheckForUpdates(betaSettingsData, loadBuildProperties)) {
            this.updatesController.initialize(context, this, idManager, betaSettingsData, loadBuildProperties, new PreferenceStoreImpl(this), new SystemCurrentTimeProvider(), new DefaultHttpRequestFactory(Fabric.getLogger()));
        }
        return Boolean.valueOf(true);
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(14)
    public UpdatesController createUpdatesController(int i, Application application) {
        if (i >= 14) {
            return new ActivityLifecycleCheckForUpdatesController(getFabric().getActivityLifecycleManager(), getFabric().getExecutorService());
        }
        return new ImmediateCheckForUpdatesController();
    }

    public Map<DeviceIdentifierType, String> getDeviceIdentifiers() {
        String betaDeviceToken = getBetaDeviceToken(getContext(), getIdManager().getInstallerPackageName());
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(betaDeviceToken)) {
            hashMap.put(DeviceIdentifierType.FONT_TOKEN, betaDeviceToken);
        }
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    public boolean canCheckForUpdates(BetaSettingsData betaSettingsData, BuildProperties buildProperties) {
        return (betaSettingsData == null || TextUtils.isEmpty(betaSettingsData.updateUrl) || buildProperties == null) ? false : true;
    }

    private String getBetaDeviceToken(Context context, String str) {
        String str2 = null;
        try {
            String str3 = (String) this.deviceTokenCache.get(context, this.deviceTokenLoader);
            if (!"".equals(str3)) {
                str2 = str3;
            }
        } catch (Exception e) {
            Fabric.getLogger().e(TAG, "Failed to load the Beta device token", e);
        }
        Logger logger = Fabric.getLogger();
        String str4 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Beta device token present: ");
        sb.append(!TextUtils.isEmpty(str2));
        logger.d(str4, sb.toString());
        return str2;
    }

    private BetaSettingsData getBetaSettingsData() {
        SettingsData awaitSettingsData = Settings.getInstance().awaitSettingsData();
        if (awaitSettingsData != null) {
            return awaitSettingsData.betaSettingsData;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0078 A[SYNTHETIC, Splitter:B:24:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008d A[SYNTHETIC, Splitter:B:31:0x008d] */
    private BuildProperties loadBuildProperties(Context context) {
        InputStream inputStream;
        BuildProperties buildProperties;
        Throwable e;
        BuildProperties buildProperties2 = null;
        try {
            inputStream = context.getAssets().open(CRASHLYTICS_BUILD_PROPERTIES);
            if (inputStream != null) {
                try {
                    buildProperties = BuildProperties.fromPropertiesStream(inputStream);
                } catch (Exception e2) {
                    Throwable th = e2;
                    buildProperties = null;
                    e = th;
                    try {
                        Fabric.getLogger().e(TAG, "Error reading Beta build properties", e);
                        if (inputStream != null) {
                        }
                        return buildProperties;
                    } catch (Throwable th2) {
                        th = th2;
                        if (inputStream != null) {
                        }
                        throw th;
                    }
                }
                try {
                    Logger logger = Fabric.getLogger();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append(buildProperties.packageName);
                    sb.append(" build properties: ");
                    sb.append(buildProperties.versionName);
                    sb.append(" (");
                    sb.append(buildProperties.versionCode);
                    sb.append(") - ");
                    sb.append(buildProperties.buildId);
                    logger.d(str, sb.toString());
                    buildProperties2 = buildProperties;
                } catch (Exception e3) {
                    e = e3;
                    Fabric.getLogger().e(TAG, "Error reading Beta build properties", e);
                    if (inputStream != null) {
                    }
                    return buildProperties;
                }
            }
            if (inputStream == null) {
                return buildProperties2;
            }
            try {
                inputStream.close();
                return buildProperties2;
            } catch (IOException e4) {
                Fabric.getLogger().e(TAG, "Error closing Beta build properties asset", e4);
                return buildProperties2;
            }
        } catch (Exception e5) {
            buildProperties = null;
            e = e5;
            inputStream = null;
            Fabric.getLogger().e(TAG, "Error reading Beta build properties", e);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e6) {
                    Fabric.getLogger().e(TAG, "Error closing Beta build properties asset", e6);
                }
            }
            return buildProperties;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e7) {
                    Fabric.getLogger().e(TAG, "Error closing Beta build properties asset", e7);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public String getOverridenSpiEndpoint() {
        return CommonUtils.getStringsFileValue(getContext(), CRASHLYTICS_API_ENDPOINT);
    }
}
