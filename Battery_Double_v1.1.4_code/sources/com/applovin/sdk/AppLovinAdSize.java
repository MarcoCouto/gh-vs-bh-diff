package com.applovin.sdk;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class AppLovinAdSize {
    public static final AppLovinAdSize BANNER = new AppLovinAdSize(-1, 50, "BANNER");
    public static final AppLovinAdSize INTERSTITIAL = new AppLovinAdSize(-1, -1, "INTER");
    public static final AppLovinAdSize LEADER = new AppLovinAdSize(-1, 75, "LEADER");
    public static final AppLovinAdSize MREC = new AppLovinAdSize(300, Callback.DEFAULT_SWIPE_ANIMATION_DURATION, "MREC");
    public static final int SPAN = -1;
    private final int a;
    private final int b;
    private final String c;

    AppLovinAdSize(int i, int i2, String str) {
        if (i < 0 && i != -1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Ad width must be a positive number. Number provided: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        } else if (i > 9999) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Ad width must be less then 9999. Number provided: ");
            sb2.append(i);
            throw new IllegalArgumentException(sb2.toString());
        } else if (i2 < 0 && i2 != -1) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Ad height must be a positive number. Number provided: ");
            sb3.append(i2);
            throw new IllegalArgumentException(sb3.toString());
        } else if (i2 > 9999) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Ad height must be less then 9999. Number provided: ");
            sb4.append(i2);
            throw new IllegalArgumentException(sb4.toString());
        } else if (str == null) {
            throw new IllegalArgumentException("No label specified");
        } else if (str.length() > 9) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Provided label is too long. Label provided: ");
            sb5.append(str);
            throw new IllegalArgumentException(sb5.toString());
        } else {
            this.a = i;
            this.b = i2;
            this.c = str;
        }
    }

    private static int a(String str) {
        if (TtmlNode.TAG_SPAN.equalsIgnoreCase(str)) {
            return -1;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public static Set allSizes() {
        HashSet hashSet = new HashSet(4);
        hashSet.add(BANNER);
        hashSet.add(MREC);
        hashSet.add(INTERSTITIAL);
        hashSet.add(LEADER);
        return hashSet;
    }

    public static AppLovinAdSize fromString(String str) {
        if (str == null || str.length() < 1) {
            return null;
        }
        String lowerCase = str.toLowerCase(Locale.ENGLISH);
        if (lowerCase.equals("banner")) {
            return BANNER;
        }
        if (lowerCase.equals("interstitial") || lowerCase.equals("inter")) {
            return INTERSTITIAL;
        }
        if (lowerCase.equals("mrec")) {
            return MREC;
        }
        if (lowerCase.equals("leader")) {
            return LEADER;
        }
        String[] split = str.split("x");
        return split.length == 2 ? new AppLovinAdSize(a(split[0]), a(split[1]), str) : new AppLovinAdSize(0, 0, str);
    }

    public int getHeight() {
        return this.b;
    }

    public String getLabel() {
        return this.c.toUpperCase(Locale.ENGLISH);
    }

    public int getWidth() {
        return this.a;
    }

    public String toString() {
        return getLabel();
    }
}
