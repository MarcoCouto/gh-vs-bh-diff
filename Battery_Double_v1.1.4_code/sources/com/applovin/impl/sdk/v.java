package com.applovin.impl.sdk;

import android.content.Context;
import android.support.v4.media.session.PlaybackStateCompat;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class v {
    private final AppLovinLogger a;
    private final AppLovinSdkImpl b;
    private final String c = "FileManager";
    private final Object d;

    v(AppLovinSdk appLovinSdk) {
        this.b = (AppLovinSdkImpl) appLovinSdk;
        this.a = appLovinSdk.getLogger();
        this.d = new Object();
    }

    /* access modifiers changed from: 0000 */
    public long a(long j) {
        return j / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    public File a(String str, Context context, boolean z) {
        File file;
        StringBuilder sb = new StringBuilder();
        sb.append("Looking up cached resource: ");
        sb.append(str);
        this.a.d("FileManager", sb.toString());
        if (!a(context) && !z) {
            return null;
        }
        if (str.contains(SettingsJsonConstants.APP_ICON_KEY)) {
            str = str.replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR).replace(".", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        }
        synchronized (this.d) {
            File b2 = b(context);
            file = new File(b2, str);
            try {
                b2.mkdirs();
            } catch (Exception unused) {
                return null;
            }
        }
        return file;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j, Context context) {
        AppLovinLogger appLovinLogger;
        String str;
        String str2;
        long c2 = (long) c();
        if (c2 == -1) {
            appLovinLogger = this.a;
            str = "FileManager";
            str2 = "Cache has no maximum size set; skipping drop...";
        } else if (a(j) > c2) {
            this.a.d("FileManager", "Cache has exceeded maximum size; dropping...");
            f(context);
            this.b.b().a("cache_drop_count");
            return;
        } else {
            appLovinLogger = this.a;
            str = "FileManager";
            str2 = "Cache is present but under size limit; not dropping...";
        }
        appLovinLogger.d(str, str2);
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return ((Boolean) this.b.a(bb.aA)).booleanValue();
    }

    /* access modifiers changed from: protected */
    public boolean a(Context context) {
        return m.a("android.permission.WRITE_EXTERNAL_STORAGE", context);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:15|(2:28|29)|31|32) */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x0047 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0051 */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0043 A[SYNTHETIC, Splitter:B:20:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004b A[SYNTHETIC, Splitter:B:28:0x004b] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0051=Splitter:B:31:0x0051, B:24:0x0047=Splitter:B:24:0x0047} */
    public boolean a(ByteArrayOutputStream byteArrayOutputStream, File file) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append("Writing resource to filesystem: ");
        sb.append(file.getName());
        this.a.d("FileManager", sb.toString());
        synchronized (this.d) {
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    byteArrayOutputStream.writeTo(fileOutputStream2);
                    z = true;
                    if (fileOutputStream2 != null) {
                        fileOutputStream2.close();
                    }
                } catch (IOException e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    try {
                        this.a.e("FileManager", "Unable to write data to file", e);
                        if (fileOutputStream != null) {
                        }
                        z = false;
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        if (fileOutputStream != null) {
                            fileOutputStream.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    if (fileOutputStream != null) {
                    }
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                this.a.e("FileManager", "Unable to write data to file", e);
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Exception unused) {
                    }
                }
                z = false;
                return z;
            }
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(File file) {
        boolean delete;
        StringBuilder sb = new StringBuilder();
        sb.append("Removing file ");
        sb.append(file.getName());
        sb.append(" from filesystem...");
        this.a.d("FileManager", sb.toString());
        synchronized (this.d) {
            try {
                delete = file.delete();
            } catch (Exception e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to remove file ");
                sb2.append(file.getName());
                sb2.append(" from filesystem!");
                this.a.e("FileManager", sb2.toString(), e);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
        return delete;
    }

    public boolean a(String str, Context context) {
        boolean b2;
        synchronized (this.d) {
            b2 = b(str, context, false);
        }
        return b2;
    }

    /* access modifiers changed from: 0000 */
    public long b() {
        long longValue = ((Long) this.b.a(bb.aB)).longValue();
        if (longValue < 0 || !a()) {
            return -1;
        }
        return longValue;
    }

    /* access modifiers changed from: 0000 */
    public File b(Context context) {
        return a(context) ? new File(context.getExternalFilesDir(null), "al") : new File(context.getCacheDir(), "al");
    }

    public boolean b(String str, Context context, boolean z) {
        boolean z2;
        synchronized (this.d) {
            File a2 = a(str, context, z);
            z2 = a2 != null && a2.exists() && !a2.isDirectory();
        }
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        int intValue = ((Integer) this.b.a(bb.aC)).intValue();
        if (intValue < 0 || !a()) {
            return -1;
        }
        return intValue;
    }

    public List c(Context context) {
        List asList;
        File b2 = b(context);
        if (!b2.isDirectory()) {
            return new ArrayList(0);
        }
        synchronized (this.d) {
            asList = Arrays.asList(b2.listFiles());
        }
        return asList;
    }

    /* access modifiers changed from: 0000 */
    public void d(Context context) {
        try {
            if (a()) {
                if (this.b.isEnabled()) {
                    this.a.e("FileManager", "Cannot empty file cache after SDK has completed initialization and ad loads are in progress!");
                    return;
                }
                this.a.d("FileManager", "Compacting cache...");
                synchronized (this.d) {
                    a(e(context), context);
                }
            }
        } catch (Exception e) {
            this.a.e("FileManager", "Caught exception while compacting cache!", e);
            this.b.getSettingsManager().a(bb.aA, Boolean.valueOf(false));
            this.b.getSettingsManager().b();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: 0000 */
    public long e(Context context) {
        long j;
        boolean z;
        long b2 = b();
        boolean z2 = b2 != -1;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        synchronized (this.d) {
            try {
                j = 0;
                for (File file : c(context)) {
                    if (!z2 || seconds - TimeUnit.MILLISECONDS.toSeconds(file.lastModified()) <= b2) {
                        z = false;
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("File ");
                        sb.append(file.getName());
                        sb.append(" has expired, removing...");
                        this.a.d("FileManager", sb.toString());
                        a(file);
                        z = true;
                    }
                    if (z) {
                        this.b.b().a("cached_files_expired");
                    } else {
                        j += file.length();
                    }
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return j;
    }

    /* access modifiers changed from: 0000 */
    public void f(Context context) {
        synchronized (this.d) {
            for (File a2 : c(context)) {
                a(a2);
            }
        }
    }
}
