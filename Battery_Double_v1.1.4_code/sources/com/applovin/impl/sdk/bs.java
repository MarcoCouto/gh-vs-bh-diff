package com.applovin.impl.sdk;

import com.applovin.impl.adview.t;
import com.applovin.impl.sdk.AppLovinAdInternal.AdTarget;
import com.applovin.impl.sdk.AppLovinAdInternal.Builder;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinLogger;
import com.facebook.appevents.AppEventsConstants;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class bs extends ba implements cc {
    private static volatile int b;
    private static volatile int c;
    private final Collection a;
    private final JSONObject d;
    private final AppLovinAdLoadListener i;
    private final v j;
    private c k = new c(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.REGULAR);

    bs(JSONObject jSONObject, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl) {
        super("RenderAd", appLovinSdkImpl);
        this.d = jSONObject;
        this.i = appLovinAdLoadListener;
        this.a = e();
        this.j = appLovinSdkImpl.getFileManager();
    }

    private float a(String str, AppLovinAdType appLovinAdType, int i2) {
        if (appLovinAdType.equals(AppLovinAdType.INCENTIVIZED)) {
            return 0.5f;
        }
        return (!appLovinAdType.equals(AppLovinAdType.REGULAR) || str == null || i2 != -1) ? 0.0f : 0.5f;
    }

    private t a(int i2) {
        return i2 == 1 ? t.WhiteXOnTransparentGrey : t.WhiteXOnOpaqueBlack;
    }

    private t a(String str) {
        return str != null ? t.WhiteXOnTransparentGrey : t.WhiteXOnOpaqueBlack;
    }

    private String a(String str, AppLovinAdType appLovinAdType) {
        boolean equals = AppLovinAdType.INCENTIVIZED.equals(appLovinAdType);
        String str2 = equals ? "a" : "";
        int i2 = equals ? c : b;
        String substring = str.substring(str.lastIndexOf(".") + 1, str.length());
        StringBuilder sb = new StringBuilder();
        sb.append("alvideo");
        sb.append(i2);
        sb.append(str2);
        sb.append(substring);
        String sb2 = sb.toString();
        File a2 = this.j.a(sb2, this.f.getApplicationContext(), false);
        this.f.getFileManager().a(a2);
        if (!a(a2, str)) {
            return null;
        }
        if (equals) {
            c = (c + 1) % 4;
            return sb2;
        }
        b = (b + 1) % 4;
        return sb2;
    }

    private String a(String str, String str2) {
        StringBuilder sb;
        File a2 = this.j.a(str2.replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR), this.f.getApplicationContext(), true);
        if (a2 == null) {
            return null;
        }
        if (a2.exists()) {
            AppLovinLogger appLovinLogger = this.g;
            String str3 = this.e;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Loaded ");
            sb2.append(str2);
            sb2.append(" from cache: file://");
            sb2.append(a2.getAbsolutePath());
            appLovinLogger.d(str3, sb2.toString());
            sb = new StringBuilder();
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(str2);
            if (!a(a2, sb3.toString())) {
                return null;
            }
            sb = new StringBuilder();
        }
        sb.append("file://");
        sb.append(a2.getAbsolutePath());
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00e3  */
    private void a(JSONObject jSONObject) {
        int i2;
        int i3;
        float f;
        t tVar;
        String str;
        String string = jSONObject.getString("html");
        AppLovinAdSize fromString = jSONObject.has("size") ? AppLovinAdSize.fromString(jSONObject.getString("size")) : AppLovinAdSize.BANNER;
        String str2 = null;
        if (string == null || string.length() <= 0) {
            this.g.e(this.e, "No HTML received for requested ad");
            d();
            return;
        }
        String b2 = b(string);
        AdTarget valueOf = jSONObject.has("ad_target") ? AdTarget.valueOf(jSONObject.getString("ad_target").toUpperCase(Locale.ENGLISH)) : AdTarget.DEFAULT;
        AppLovinAdType fromString2 = jSONObject.has(AppEventsConstants.EVENT_PARAM_AD_TYPE) ? AppLovinAdType.fromString(jSONObject.getString(AppEventsConstants.EVENT_PARAM_AD_TYPE).toUpperCase(Locale.ENGLISH)) : AppLovinAdType.REGULAR;
        this.k = new c(fromString, fromString2);
        if (jSONObject.has("video")) {
            String string2 = jSONObject.getString("video");
            if (string2 != null && !string2.isEmpty()) {
                str2 = a(string2, fromString2);
            }
        }
        long j2 = -1;
        if (jSONObject.has("ad_id")) {
            j2 = jSONObject.getLong("ad_id");
        }
        if (jSONObject.has("countdown_length")) {
            try {
                i2 = jSONObject.getInt("countdown_length");
            } catch (JSONException unused) {
            }
            if (jSONObject.has("close_delay")) {
                try {
                    i3 = jSONObject.getInt("close_delay");
                } catch (JSONException unused2) {
                }
                if (jSONObject.has("close_delay_graphic")) {
                    try {
                        f = (float) jSONObject.getInt("close_delay_graphic");
                    } catch (JSONException unused3) {
                    }
                    if (jSONObject.has("close_style")) {
                        try {
                            tVar = a(jSONObject.getInt("close_style"));
                        } catch (JSONException unused4) {
                        }
                        if (jSONObject.has("clcodes")) {
                            try {
                                str = ((JSONArray) jSONObject.get("clcodes")).getString(0);
                            } catch (JSONException unused5) {
                            }
                            a((AppLovinAd) new Builder().setHtml(b2).setSize(fromString).setType(fromString2).setVideoFilename(str2).setTarget(valueOf).setCloseStyle(tVar).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(f).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
                        }
                        str = "";
                        a((AppLovinAd) new Builder().setHtml(b2).setSize(fromString).setType(fromString2).setVideoFilename(str2).setTarget(valueOf).setCloseStyle(tVar).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(f).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
                    }
                    tVar = a(str2);
                    if (jSONObject.has("clcodes")) {
                    }
                    str = "";
                    a((AppLovinAd) new Builder().setHtml(b2).setSize(fromString).setType(fromString2).setVideoFilename(str2).setTarget(valueOf).setCloseStyle(tVar).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(f).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
                }
                f = a(str2, fromString2, i3);
                if (jSONObject.has("close_style")) {
                }
                tVar = a(str2);
                if (jSONObject.has("clcodes")) {
                }
                str = "";
                a((AppLovinAd) new Builder().setHtml(b2).setSize(fromString).setType(fromString2).setVideoFilename(str2).setTarget(valueOf).setCloseStyle(tVar).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(f).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
            }
            i3 = 0;
            if (jSONObject.has("close_delay_graphic")) {
            }
            f = a(str2, fromString2, i3);
            if (jSONObject.has("close_style")) {
            }
            tVar = a(str2);
            if (jSONObject.has("clcodes")) {
            }
            str = "";
            a((AppLovinAd) new Builder().setHtml(b2).setSize(fromString).setType(fromString2).setVideoFilename(str2).setTarget(valueOf).setCloseStyle(tVar).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(f).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
        }
        i2 = 0;
        if (jSONObject.has("close_delay")) {
        }
        i3 = 0;
        if (jSONObject.has("close_delay_graphic")) {
        }
        f = a(str2, fromString2, i3);
        if (jSONObject.has("close_style")) {
        }
        tVar = a(str2);
        if (jSONObject.has("clcodes")) {
        }
        str = "";
        a((AppLovinAd) new Builder().setHtml(b2).setSize(fromString).setType(fromString2).setVideoFilename(str2).setTarget(valueOf).setCloseStyle(tVar).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(f).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
    }

    /* JADX WARNING: type inference failed for: r5v0, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r3v3, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r5v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r3v4, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r3v5 */
    /* JADX WARNING: type inference failed for: r5v2 */
    /* JADX WARNING: type inference failed for: r3v6 */
    /* JADX WARNING: type inference failed for: r5v3 */
    /* JADX WARNING: type inference failed for: r5v4 */
    /* JADX WARNING: type inference failed for: r3v9 */
    /* JADX WARNING: type inference failed for: r5v5 */
    /* JADX WARNING: type inference failed for: r5v6 */
    /* JADX WARNING: type inference failed for: r5v10 */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r3v18 */
    /* JADX WARNING: type inference failed for: r5v11 */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:12|13|14|15|16|17|(2:19|20)|(2:23|24)|(2:27|28)|29) */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0094, code lost:
        if (r9.j.a(r2, r10) != false) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0096, code lost:
        d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0099, code lost:
        r0 = r9.g;
        r6 = r9.e;
        r7 = new java.lang.StringBuilder();
        r7.append("Caching completed for ");
        r7.append(r10);
        r0.d(r6, r7.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b1, code lost:
        if (r5 == 0) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0078 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x007b */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0080 A[SYNTHETIC, Splitter:B:19:0x0080] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0085 A[SYNTHETIC, Splitter:B:23:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008a A[SYNTHETIC, Splitter:B:27:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00bd A[SYNTHETIC, Splitter:B:45:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0107 A[SYNTHETIC, Splitter:B:66:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x010c A[SYNTHETIC, Splitter:B:70:0x010c] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0111 A[SYNTHETIC, Splitter:B:74:0x0111] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0118 A[SYNTHETIC, Splitter:B:80:0x0118] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x011d A[SYNTHETIC, Splitter:B:84:0x011d] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0122 A[SYNTHETIC, Splitter:B:88:0x0122] */
    /* JADX WARNING: Unknown variable types count: 5 */
    private boolean a(File file, String str) {
        ? r5;
        ? r3;
        ByteArrayOutputStream byteArrayOutputStream;
        ? r52;
        ? r32;
        IOException e;
        ? r33;
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        AppLovinLogger appLovinLogger = this.g;
        String str2 = this.e;
        StringBuilder sb = new StringBuilder();
        sb.append("Starting caching of ");
        sb.append(str);
        sb.append(" into ");
        sb.append(file.getAbsoluteFile());
        appLovinLogger.d(str2, sb.toString());
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
                try {
                    httpURLConnection2.setConnectTimeout(((Integer) this.f.a(bb.t)).intValue());
                    httpURLConnection2.setReadTimeout(((Integer) this.f.a(bb.v)).intValue());
                    httpURLConnection2.setDefaultUseCaches(true);
                    httpURLConnection2.setUseCaches(true);
                    httpURLConnection2.setAllowUserInteraction(false);
                    httpURLConnection2.setInstanceFollowRedirects(true);
                    inputStream = httpURLConnection2.getInputStream();
                } catch (IOException e2) {
                    r52 = 0;
                    e = e2;
                    r32 = httpURLConnection2;
                    try {
                        AppLovinLogger appLovinLogger2 = this.g;
                        String str3 = this.e;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Failed to cache \"");
                        sb2.append(str);
                        sb2.append("\" into \"");
                        sb2.append(file.getAbsolutePath());
                        sb2.append("\"");
                        appLovinLogger2.e(str3, sb2.toString(), e);
                        if (r52 != 0) {
                        }
                        if (byteArrayOutputStream != null) {
                        }
                        if (r32 != 0) {
                        }
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        r5 = r52;
                        r3 = r32;
                        if (r5 != 0) {
                        }
                        if (byteArrayOutputStream != null) {
                        }
                        if (r3 != 0) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    r5 = 0;
                    r3 = httpURLConnection2;
                    if (r5 != 0) {
                    }
                    if (byteArrayOutputStream != null) {
                    }
                    if (r3 != 0) {
                    }
                    throw th;
                }
                try {
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream.read(bArr, 0, bArr.length);
                        if (read < 0) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr, 0, read);
                        byteArrayOutputStream.close();
                        d();
                        if (inputStream != 0) {
                            try {
                                inputStream.close();
                            } catch (Exception unused) {
                            }
                        }
                        if (byteArrayOutputStream != null) {
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception unused2) {
                            }
                        }
                        if (httpURLConnection2 != 0) {
                            try {
                                httpURLConnection2.disconnect();
                            } catch (Exception unused3) {
                            }
                        }
                        return false;
                    }
                    return true;
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception unused4) {
                        }
                    }
                    if (httpURLConnection2 != 0) {
                        try {
                            httpURLConnection2.disconnect();
                        } catch (Exception unused5) {
                        }
                    }
                    return true;
                    if (httpURLConnection2 != 0) {
                    }
                    return true;
                } catch (IOException e3) {
                    e = e3;
                    r32 = httpURLConnection2;
                    r52 = inputStream;
                    AppLovinLogger appLovinLogger22 = this.g;
                    String str32 = this.e;
                    StringBuilder sb22 = new StringBuilder();
                    sb22.append("Failed to cache \"");
                    sb22.append(str);
                    sb22.append("\" into \"");
                    sb22.append(file.getAbsolutePath());
                    sb22.append("\"");
                    appLovinLogger22.e(str32, sb22.toString(), e);
                    if (r52 != 0) {
                    }
                    if (byteArrayOutputStream != null) {
                    }
                    if (r32 != 0) {
                    }
                    return false;
                }
            } catch (IOException e4) {
                r52 = 0;
                e = e4;
                r32 = 0;
                AppLovinLogger appLovinLogger222 = this.g;
                String str322 = this.e;
                StringBuilder sb222 = new StringBuilder();
                sb222.append("Failed to cache \"");
                sb222.append(str);
                sb222.append("\" into \"");
                sb222.append(file.getAbsolutePath());
                sb222.append("\"");
                appLovinLogger222.e(str322, sb222.toString(), e);
                if (r52 != 0) {
                }
                if (byteArrayOutputStream != null) {
                }
                if (r32 != 0) {
                }
                return false;
            } catch (Throwable th3) {
                th = th3;
                httpURLConnection = 0;
                r5 = r33;
                r3 = r33;
                if (r5 != 0) {
                }
                if (byteArrayOutputStream != null) {
                }
                if (r3 != 0) {
                }
                throw th;
            }
        } catch (IOException e5) {
            r32 = 0;
            r52 = 0;
            e = e5;
            byteArrayOutputStream = null;
            AppLovinLogger appLovinLogger2222 = this.g;
            String str3222 = this.e;
            StringBuilder sb2222 = new StringBuilder();
            sb2222.append("Failed to cache \"");
            sb2222.append(str);
            sb2222.append("\" into \"");
            sb2222.append(file.getAbsolutePath());
            sb2222.append("\"");
            appLovinLogger2222.e(str3222, sb2222.toString(), e);
            if (r52 != 0) {
                try {
                    r52.close();
                } catch (Exception unused6) {
                }
            }
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (Exception unused7) {
                }
            }
            if (r32 != 0) {
                try {
                    r32.disconnect();
                } catch (Exception unused8) {
                }
            }
            return false;
        } catch (Throwable th4) {
            th = th4;
            byteArrayOutputStream = null;
            httpURLConnection = 0;
            r5 = r33;
            r3 = r33;
            if (r5 != 0) {
                try {
                    r5.close();
                } catch (Exception unused9) {
                }
            }
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (Exception unused10) {
                }
            }
            if (r3 != 0) {
                try {
                    r3.disconnect();
                } catch (Exception unused11) {
                }
            }
            throw th;
        }
    }

    private String b(String str) {
        return ((Boolean) this.f.a(bb.H)).booleanValue() ? c(str) : str;
    }

    private String c(String str) {
        String[] split;
        StringBuilder sb = new StringBuilder(str);
        for (String str2 : ((String) this.f.a(bb.I)).split(",")) {
            int i2 = 0;
            int i3 = 0;
            while (i2 < sb.length()) {
                i2 = sb.indexOf(str2, i3);
                if (i2 == -1) {
                    break;
                }
                int length = sb.length();
                int i4 = i2;
                while (!this.a.contains(Character.valueOf(sb.charAt(i4))) && i4 < length) {
                    i4++;
                }
                if (i4 <= i2 || i4 == length) {
                    this.g.d(this.e, "Unable to cache resource; ad HTML is invalid.");
                } else {
                    String a2 = a(str2, sb.substring(str2.length() + i2, i4));
                    if (a2 != null) {
                        sb.replace(i2, i4, a2);
                    }
                }
                i3 = i4;
            }
        }
        return sb.toString();
    }

    private Collection e() {
        HashSet hashSet = new HashSet();
        for (char valueOf : ((String) this.f.a(bb.ap)).toCharArray()) {
            hashSet.add(Character.valueOf(valueOf));
        }
        hashSet.add(Character.valueOf('\"'));
        return hashSet;
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAd appLovinAd) {
        if (this.i != null) {
            this.i.adReceived(appLovinAd);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        try {
            if (this.i != null) {
                if (this.i instanceof u) {
                    ((u) this.i).a(this.k, -6);
                } else {
                    this.i.failedToReceiveAd(-6);
                }
            }
        } catch (Throwable th) {
            this.g.e(this.e, "Unable process a failure to receive an ad", th);
        }
    }

    public String f() {
        return "tRA";
    }

    public void run() {
        String str;
        String str2;
        AppLovinLogger appLovinLogger;
        this.g.d(this.e, "Rendering ad...");
        try {
            a(this.d);
        } catch (JSONException e) {
            e = e;
            appLovinLogger = this.g;
            str2 = this.e;
            str = "Unable to parse ad service response";
            appLovinLogger.e(str2, str, e);
            d();
        } catch (IllegalArgumentException e2) {
            e = e2;
            appLovinLogger = this.g;
            str2 = this.e;
            str = "Ad response is not valid";
            appLovinLogger.e(str2, str, e);
            d();
        } catch (Exception e3) {
            e = e3;
            appLovinLogger = this.g;
            str2 = this.e;
            str = "Unable to render ad";
            appLovinLogger.e(str2, str, e);
            d();
        }
    }
}
