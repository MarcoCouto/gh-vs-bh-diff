package com.applovin.impl.sdk;

import com.applovin.impl.adview.t;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;

public class AppLovinAdInternal extends AppLovinAd {
    private final AdTarget a;
    private final String b;
    private final String c;
    private final float d;
    private final float e;
    private final int f;
    private final t g;

    public enum AdTarget {
        DEFAULT,
        ACTIVITY_PORTRAIT,
        ACTIVITY_LANDSCAPE
    }

    public class Builder {
        private String a;
        private AppLovinAdSize b;
        private AppLovinAdType c;
        private String d;
        private AdTarget e;
        private t f;
        private float g;
        private float h;
        private int i;
        private long j;
        private String k;

        public AppLovinAdInternal create() {
            AppLovinAdInternal appLovinAdInternal = new AppLovinAdInternal(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k);
            return appLovinAdInternal;
        }

        public Builder setClCode(String str) {
            this.k = str;
            return this;
        }

        public Builder setCloseStyle(t tVar) {
            this.f = tVar;
            return this;
        }

        public Builder setCountdownLength(int i2) {
            this.i = i2;
            return this;
        }

        public Builder setCurrentAdIdNumber(long j2) {
            this.j = j2;
            return this;
        }

        public Builder setHtml(String str) {
            this.a = str;
            return this;
        }

        public Builder setPoststitialCloseDelay(float f2) {
            this.h = f2;
            return this;
        }

        public Builder setSize(AppLovinAdSize appLovinAdSize) {
            this.b = appLovinAdSize;
            return this;
        }

        public Builder setTarget(AdTarget adTarget) {
            this.e = adTarget;
            return this;
        }

        public Builder setType(AppLovinAdType appLovinAdType) {
            this.c = appLovinAdType;
            return this;
        }

        public Builder setVideoCloseDelay(float f2) {
            this.g = f2;
            return this;
        }

        public Builder setVideoFilename(String str) {
            this.d = str;
            return this;
        }
    }

    private AppLovinAdInternal(String str, AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str2, AdTarget adTarget, t tVar, float f2, float f3, int i, long j, String str3) {
        super(appLovinAdSize, appLovinAdType, str2, j);
        this.c = str;
        this.a = adTarget;
        this.d = f2;
        this.f = i;
        this.b = str3;
        this.g = tVar;
        this.e = f3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppLovinAdInternal appLovinAdInternal = (AppLovinAdInternal) obj;
        return getAdIdNumber() == appLovinAdInternal.getAdIdNumber() && getType().equals(appLovinAdInternal.getType()) && getSize().equals(appLovinAdInternal.getSize()) && this.c.equals(appLovinAdInternal.getHtmlSource());
    }

    public String getClCode() {
        return this.b;
    }

    public t getCloseStyle() {
        return this.g;
    }

    public int getCountdownLength() {
        return this.f;
    }

    public String getHtmlSource() {
        return this.c;
    }

    public float getPoststitialCloseDelay() {
        return this.e;
    }

    public AdTarget getTarget() {
        return this.a;
    }

    public float getVideoCloseDelay() {
        return this.d;
    }

    public String getVideoFilename() {
        return this.videoFilename;
    }

    public int hashCode() {
        return (int) getAdIdNumber();
    }
}
