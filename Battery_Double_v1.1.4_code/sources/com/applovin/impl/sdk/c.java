package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;

class c {
    private AppLovinAdSize a;
    private AppLovinAdType b;

    public c(AppLovinAd appLovinAd) {
        this.a = appLovinAd.getSize();
        this.b = appLovinAd.getType();
    }

    public c(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType) {
        this.a = appLovinAdSize;
        this.b = appLovinAdType;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0033, code lost:
        if (r4.b.equals(r5.b) == false) goto L_0x003b;
     */
    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.a == null ? cVar.a == null : this.a.equals(cVar.a)) {
            if (this.b == null) {
                if (cVar.b == null) {
                    return true;
                }
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = 31 * (this.a != null ? this.a.hashCode() : 0);
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdSpec{size=");
        sb.append(this.a);
        sb.append(", type=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
