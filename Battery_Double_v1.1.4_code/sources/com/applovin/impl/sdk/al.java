package com.applovin.impl.sdk;

import android.app.Activity;
import android.widget.Toast;

public class al {
    /* access modifiers changed from: private */
    public final AppLovinSdkImpl a;
    private final String b;
    /* access modifiers changed from: private */
    public final Activity c;

    public al(AppLovinSdkImpl appLovinSdkImpl, Activity activity, String str) {
        this.a = appLovinSdkImpl;
        this.b = str;
        this.c = activity;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.c.runOnUiThread(new am(this));
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, Throwable th) {
        this.a.getLogger().userError("IncentivizedConfirmationManager", "Unable to show incentivized ad reward dialog. Have you defined com.applovin.adview.AppLovinConfirmationActivity in your manifest?", th);
        Toast.makeText(this.c, str, 1).show();
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        AppLovinSdkImpl appLovinSdkImpl;
        bd bdVar;
        if (this.b.equals("accepted")) {
            appLovinSdkImpl = this.a;
            bdVar = bb.ac;
        } else if (this.b.equals("quota_exceeded")) {
            appLovinSdkImpl = this.a;
            bdVar = bb.ad;
        } else if (this.b.equals("rejected")) {
            appLovinSdkImpl = this.a;
            bdVar = bb.ae;
        } else {
            appLovinSdkImpl = this.a;
            bdVar = bb.af;
        }
        return (String) appLovinSdkImpl.a(bdVar);
    }
}
