package com.applovin.impl.adview;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;

class n extends WebChromeClient {
    private final AppLovinLogger a;

    public n(AppLovinSdk appLovinSdk) {
        this.a = appLovinSdk.getLogger();
    }

    public void onConsoleMessage(String str, int i, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("console.log[");
        sb.append(i);
        sb.append("] :");
        sb.append(str);
        this.a.w("AdWebView", sb.toString());
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        StringBuilder sb = new StringBuilder();
        sb.append(consoleMessage.sourceId());
        sb.append(": ");
        sb.append(consoleMessage.lineNumber());
        sb.append(": ");
        sb.append(consoleMessage.message());
        this.a.d("AdWebView", sb.toString());
        return true;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("Alert attempted: ");
        sb.append(str2);
        this.a.w("AdWebView", sb.toString());
        return true;
    }

    public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("JS onBeforeUnload attempted: ");
        sb.append(str2);
        this.a.w("AdWebView", sb.toString());
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("JS confirm attempted: ");
        sb.append(str2);
        this.a.w("AdWebView", sb.toString());
        return true;
    }
}
