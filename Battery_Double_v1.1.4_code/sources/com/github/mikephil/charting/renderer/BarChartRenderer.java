package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.buffer.BarBuffer;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.highlight.Range;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.List;

public class BarChartRenderer extends BarLineScatterCandleBubbleRenderer {
    protected Paint mBarBorderPaint;
    protected BarBuffer[] mBarBuffers;
    protected RectF mBarRect = new RectF();
    private RectF mBarShadowRectBuffer = new RectF();
    protected BarDataProvider mChart;
    protected Paint mShadowPaint;

    public void drawExtras(Canvas canvas) {
    }

    public BarChartRenderer(BarDataProvider barDataProvider, ChartAnimator chartAnimator, ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mChart = barDataProvider;
        this.mHighlightPaint = new Paint(1);
        this.mHighlightPaint.setStyle(Style.FILL);
        this.mHighlightPaint.setColor(Color.rgb(0, 0, 0));
        this.mHighlightPaint.setAlpha(120);
        this.mShadowPaint = new Paint(1);
        this.mShadowPaint.setStyle(Style.FILL);
        this.mBarBorderPaint = new Paint(1);
        this.mBarBorderPaint.setStyle(Style.STROKE);
    }

    public void initBuffers() {
        BarData barData = this.mChart.getBarData();
        this.mBarBuffers = new BarBuffer[barData.getDataSetCount()];
        for (int i = 0; i < this.mBarBuffers.length; i++) {
            IBarDataSet iBarDataSet = (IBarDataSet) barData.getDataSetByIndex(i);
            this.mBarBuffers[i] = new BarBuffer(iBarDataSet.getEntryCount() * 4 * (iBarDataSet.isStacked() ? iBarDataSet.getStackSize() : 1), barData.getDataSetCount(), iBarDataSet.isStacked());
        }
    }

    public void drawData(Canvas canvas) {
        BarData barData = this.mChart.getBarData();
        for (int i = 0; i < barData.getDataSetCount(); i++) {
            IBarDataSet iBarDataSet = (IBarDataSet) barData.getDataSetByIndex(i);
            if (iBarDataSet.isVisible()) {
                drawDataSet(canvas, iBarDataSet, i);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawDataSet(Canvas canvas, IBarDataSet iBarDataSet, int i) {
        IBarDataSet iBarDataSet2 = iBarDataSet;
        int i2 = i;
        Transformer transformer = this.mChart.getTransformer(iBarDataSet.getAxisDependency());
        this.mBarBorderPaint.setColor(iBarDataSet.getBarBorderColor());
        this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(iBarDataSet.getBarBorderWidth()));
        int i3 = 0;
        boolean z = true;
        boolean z2 = iBarDataSet.getBarBorderWidth() > 0.0f;
        float phaseX = this.mAnimator.getPhaseX();
        float phaseY = this.mAnimator.getPhaseY();
        if (this.mChart.isDrawBarShadowEnabled()) {
            this.mShadowPaint.setColor(iBarDataSet.getBarShadowColor());
            float barWidth = this.mChart.getBarData().getBarWidth() / 2.0f;
            int min = Math.min((int) Math.ceil((double) (((float) iBarDataSet.getEntryCount()) * phaseX)), iBarDataSet.getEntryCount());
            for (int i4 = 0; i4 < min; i4++) {
                float x = ((BarEntry) iBarDataSet2.getEntryForIndex(i4)).getX();
                this.mBarShadowRectBuffer.left = x - barWidth;
                this.mBarShadowRectBuffer.right = x + barWidth;
                transformer.rectValueToPixel(this.mBarShadowRectBuffer);
                if (!this.mViewPortHandler.isInBoundsLeft(this.mBarShadowRectBuffer.right)) {
                    Canvas canvas2 = canvas;
                } else if (!this.mViewPortHandler.isInBoundsRight(this.mBarShadowRectBuffer.left)) {
                    break;
                } else {
                    this.mBarShadowRectBuffer.top = this.mViewPortHandler.contentTop();
                    this.mBarShadowRectBuffer.bottom = this.mViewPortHandler.contentBottom();
                    canvas.drawRect(this.mBarShadowRectBuffer, this.mShadowPaint);
                }
            }
        }
        Canvas canvas3 = canvas;
        BarBuffer barBuffer = this.mBarBuffers[i2];
        barBuffer.setPhases(phaseX, phaseY);
        barBuffer.setDataSet(i2);
        barBuffer.setInverted(this.mChart.isInverted(iBarDataSet.getAxisDependency()));
        barBuffer.setBarWidth(this.mChart.getBarData().getBarWidth());
        barBuffer.feed(iBarDataSet2);
        transformer.pointValuesToPixel(barBuffer.buffer);
        if (iBarDataSet.getColors().size() != 1) {
            z = false;
        }
        if (z) {
            this.mRenderPaint.setColor(iBarDataSet.getColor());
        }
        while (i3 < barBuffer.size()) {
            int i5 = i3 + 2;
            if (this.mViewPortHandler.isInBoundsLeft(barBuffer.buffer[i5])) {
                if (this.mViewPortHandler.isInBoundsRight(barBuffer.buffer[i3])) {
                    if (!z) {
                        this.mRenderPaint.setColor(iBarDataSet2.getColor(i3 / 4));
                    }
                    int i6 = i3 + 1;
                    int i7 = i3 + 3;
                    canvas3.drawRect(barBuffer.buffer[i3], barBuffer.buffer[i6], barBuffer.buffer[i5], barBuffer.buffer[i7], this.mRenderPaint);
                    if (z2) {
                        canvas.drawRect(barBuffer.buffer[i3], barBuffer.buffer[i6], barBuffer.buffer[i5], barBuffer.buffer[i7], this.mBarBorderPaint);
                    }
                } else {
                    return;
                }
            }
            i3 += 4;
            canvas3 = canvas;
        }
    }

    /* access modifiers changed from: protected */
    public void prepareBarHighlight(float f, float f2, float f3, float f4, Transformer transformer) {
        this.mBarRect.set(f - f4, f2, f + f4, f3);
        transformer.rectToPixelPhase(this.mBarRect, this.mAnimator.getPhaseY());
    }

    /* JADX WARNING: Removed duplicated region for block: B:79:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x024e  */
    public void drawValues(Canvas canvas) {
        float f;
        List list;
        int i;
        Transformer transformer;
        float[] fArr;
        int i2;
        BarEntry barEntry;
        float[] fArr2;
        float f2;
        float f3;
        float f4;
        float f5;
        int i3;
        BarBuffer barBuffer;
        if (isDrawingValuesAllowed(this.mChart)) {
            List dataSets = this.mChart.getBarData().getDataSets();
            float convertDpToPixel = Utils.convertDpToPixel(4.5f);
            boolean isDrawValueAboveBarEnabled = this.mChart.isDrawValueAboveBarEnabled();
            int i4 = 0;
            while (i4 < this.mChart.getBarData().getDataSetCount()) {
                IBarDataSet iBarDataSet = (IBarDataSet) dataSets.get(i4);
                if (shouldDrawValues(iBarDataSet)) {
                    applyValueTextStyle(iBarDataSet);
                    boolean isInverted = this.mChart.isInverted(iBarDataSet.getAxisDependency());
                    float calcTextHeight = (float) Utils.calcTextHeight(this.mValuePaint, "8");
                    float f6 = isDrawValueAboveBarEnabled ? -convertDpToPixel : calcTextHeight + convertDpToPixel;
                    float f7 = isDrawValueAboveBarEnabled ? calcTextHeight + convertDpToPixel : -convertDpToPixel;
                    if (isInverted) {
                        f6 = (-f6) - calcTextHeight;
                        f7 = (-f7) - calcTextHeight;
                    }
                    float f8 = f6;
                    float f9 = f7;
                    BarBuffer barBuffer2 = this.mBarBuffers[i4];
                    float phaseY = this.mAnimator.getPhaseY();
                    if (!iBarDataSet.isStacked()) {
                        int i5 = 0;
                        while (((float) i5) < ((float) barBuffer2.buffer.length) * this.mAnimator.getPhaseX()) {
                            float f10 = (barBuffer2.buffer[i5] + barBuffer2.buffer[i5 + 2]) / 2.0f;
                            if (!this.mViewPortHandler.isInBoundsRight(f10)) {
                                break;
                            }
                            int i6 = i5 + 1;
                            if (!this.mViewPortHandler.isInBoundsY(barBuffer2.buffer[i6]) || !this.mViewPortHandler.isInBoundsLeft(f10)) {
                                i3 = i5;
                                barBuffer = barBuffer2;
                            } else {
                                int i7 = i5 / 4;
                                BarEntry barEntry2 = (BarEntry) iBarDataSet.getEntryForIndex(i7);
                                float y = barEntry2.getY();
                                i3 = i5;
                                barBuffer = barBuffer2;
                                drawValue(canvas, iBarDataSet.getValueFormatter(), y, barEntry2, i4, f10, y >= 0.0f ? barBuffer2.buffer[i6] + f8 : barBuffer2.buffer[i5 + 3] + f9, iBarDataSet.getValueTextColor(i7));
                            }
                            i5 = i3 + 4;
                            barBuffer2 = barBuffer;
                        }
                    } else {
                        BarBuffer barBuffer3 = barBuffer2;
                        Transformer transformer2 = this.mChart.getTransformer(iBarDataSet.getAxisDependency());
                        int i8 = 0;
                        int i9 = 0;
                        while (((float) i8) < ((float) iBarDataSet.getEntryCount()) * this.mAnimator.getPhaseX()) {
                            BarEntry barEntry3 = (BarEntry) iBarDataSet.getEntryForIndex(i8);
                            float[] yVals = barEntry3.getYVals();
                            float f11 = (barBuffer3.buffer[i9] + barBuffer3.buffer[i9 + 2]) / 2.0f;
                            int valueTextColor = iBarDataSet.getValueTextColor(i8);
                            if (yVals != null) {
                                float f12 = f11;
                                i = i8;
                                list = dataSets;
                                f = convertDpToPixel;
                                fArr = yVals;
                                transformer = transformer2;
                                float[] fArr3 = new float[(fArr.length * 2)];
                                float f13 = -barEntry3.getNegativeSum();
                                float f14 = 0.0f;
                                int i10 = 0;
                                int i11 = 0;
                                while (i10 < fArr3.length) {
                                    float f15 = fArr[i11];
                                    if (f15 >= 0.0f) {
                                        f5 = f14 + f15;
                                        f3 = f13;
                                        f4 = f5;
                                    } else {
                                        f3 = f13 - f15;
                                        float f16 = f13;
                                        f4 = f14;
                                        f5 = f16;
                                    }
                                    fArr3[i10 + 1] = f5 * phaseY;
                                    i10 += 2;
                                    i11++;
                                    f14 = f4;
                                    f13 = f3;
                                }
                                transformer.pointValuesToPixel(fArr3);
                                int i12 = 0;
                                while (i12 < fArr3.length) {
                                    int i13 = i12 / 2;
                                    float f17 = fArr3[i12 + 1] + (fArr[i13] >= 0.0f ? f8 : f9);
                                    float f18 = f12;
                                    if (!this.mViewPortHandler.isInBoundsRight(f18)) {
                                        break;
                                    }
                                    if (!this.mViewPortHandler.isInBoundsY(f17) || !this.mViewPortHandler.isInBoundsLeft(f18)) {
                                        f2 = f18;
                                        barEntry = barEntry3;
                                        i2 = i12;
                                        fArr2 = fArr3;
                                    } else {
                                        f2 = f18;
                                        barEntry = barEntry3;
                                        i2 = i12;
                                        fArr2 = fArr3;
                                        drawValue(canvas, iBarDataSet.getValueFormatter(), fArr[i13], barEntry3, i4, f2, f17, valueTextColor);
                                    }
                                    i12 = i2 + 2;
                                    fArr3 = fArr2;
                                    barEntry3 = barEntry;
                                    f12 = f2;
                                }
                                if (fArr != null) {
                                }
                                i8 = i + 1;
                                transformer2 = transformer;
                            } else if (!this.mViewPortHandler.isInBoundsRight(f11)) {
                                break;
                            } else {
                                int i14 = i9 + 1;
                                if (!this.mViewPortHandler.isInBoundsY(barBuffer3.buffer[i14]) || !this.mViewPortHandler.isInBoundsLeft(f11)) {
                                    list = dataSets;
                                    f = convertDpToPixel;
                                    transformer2 = transformer2;
                                    i8 = i8;
                                } else {
                                    list = dataSets;
                                    fArr = yVals;
                                    i = i8;
                                    f = convertDpToPixel;
                                    transformer = transformer2;
                                    drawValue(canvas, iBarDataSet.getValueFormatter(), barEntry3.getY(), barEntry3, i4, f11, barBuffer3.buffer[i14] + (barEntry3.getY() >= 0.0f ? f8 : f9), valueTextColor);
                                    if (fArr != null) {
                                        i9 += 4;
                                    } else {
                                        i9 += 4 * fArr.length;
                                    }
                                    i8 = i + 1;
                                    transformer2 = transformer;
                                }
                            }
                            dataSets = list;
                            convertDpToPixel = f;
                        }
                    }
                }
                i4++;
                dataSets = dataSets;
                convertDpToPixel = convertDpToPixel;
            }
        }
    }

    public void drawHighlighted(Canvas canvas, Highlight[] highlightArr) {
        float f;
        float f2;
        float f3;
        float f4;
        BarData barData = this.mChart.getBarData();
        for (Highlight highlight : highlightArr) {
            IBarDataSet iBarDataSet = (IBarDataSet) barData.getDataSetByIndex(highlight.getDataSetIndex());
            if (iBarDataSet != null && iBarDataSet.isHighlightEnabled()) {
                BarEntry barEntry = (BarEntry) iBarDataSet.getEntryForXValue(highlight.getX(), highlight.getY());
                if (isInBoundsX(barEntry, iBarDataSet)) {
                    Transformer transformer = this.mChart.getTransformer(iBarDataSet.getAxisDependency());
                    this.mHighlightPaint.setColor(iBarDataSet.getHighLightColor());
                    this.mHighlightPaint.setAlpha(iBarDataSet.getHighLightAlpha());
                    if (!(highlight.getStackIndex() >= 0 && barEntry.isStacked())) {
                        f4 = barEntry.getY();
                        f3 = 0.0f;
                    } else if (this.mChart.isHighlightFullBarEnabled()) {
                        f4 = barEntry.getPositiveSum();
                        f3 = -barEntry.getNegativeSum();
                    } else {
                        Range range = barEntry.getRanges()[highlight.getStackIndex()];
                        f2 = range.from;
                        f = range.to;
                        prepareBarHighlight(barEntry.getX(), f2, f, barData.getBarWidth() / 2.0f, transformer);
                        setHighlightDrawPos(highlight, this.mBarRect);
                        canvas.drawRect(this.mBarRect, this.mHighlightPaint);
                    }
                    f = f3;
                    f2 = f4;
                    prepareBarHighlight(barEntry.getX(), f2, f, barData.getBarWidth() / 2.0f, transformer);
                    setHighlightDrawPos(highlight, this.mBarRect);
                    canvas.drawRect(this.mBarRect, this.mHighlightPaint);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setHighlightDrawPos(Highlight highlight, RectF rectF) {
        highlight.setDraw(rectF.centerX(), rectF.top);
    }
}
