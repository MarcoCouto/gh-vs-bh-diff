package com.jaredrummler.android.processes.models;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.jaredrummler.android.processes.AndroidProcesses;
import java.io.File;
import java.io.IOException;

public class AndroidAppProcess extends AndroidProcess {
    private static final String ANDROID_PROCESS_NAME_REGEX = "^([\\p{L}]{1}[\\p{L}\\p{N}_]*[\\.:])*[\\p{L}][\\p{L}\\p{N}_]*$";
    public static final Creator<AndroidAppProcess> CREATOR = new Creator<AndroidAppProcess>() {
        public AndroidAppProcess createFromParcel(Parcel parcel) {
            return new AndroidAppProcess(parcel);
        }

        public AndroidAppProcess[] newArray(int i) {
            return new AndroidAppProcess[i];
        }
    };
    private static final boolean SYS_SUPPORTS_SCHEDGROUPS = new File("/dev/cpuctl/tasks").exists();
    public final boolean foreground;
    public final int uid;

    public static final class NotAndroidAppProcessException extends Exception {
        public NotAndroidAppProcessException(int i) {
            super(String.format("The process %d does not belong to any application", new Object[]{Integer.valueOf(i)}));
        }
    }

    public AndroidAppProcess(int i) throws IOException, NotAndroidAppProcessException {
        int i2;
        boolean z;
        int uid2;
        super(i);
        if (this.name == null || !this.name.matches(ANDROID_PROCESS_NAME_REGEX) || !new File("/data/data", getPackageName()).exists()) {
            throw new NotAndroidAppProcessException(i);
        }
        if (SYS_SUPPORTS_SCHEDGROUPS) {
            Cgroup cgroup = cgroup();
            ControlGroup group = cgroup.getGroup("cpuacct");
            ControlGroup group2 = cgroup.getGroup("cpu");
            if (VERSION.SDK_INT >= 21) {
                if (group2 == null || group == null || !group.group.contains("pid_")) {
                    throw new NotAndroidAppProcessException(i);
                }
                z = !group2.group.contains("bg_non_interactive");
                try {
                    i2 = Integer.parseInt(group.group.split("/")[1].replace("uid_", ""));
                } catch (Exception unused) {
                    i2 = status().getUid();
                }
                AndroidProcesses.log("name=%s, pid=%d, uid=%d, foreground=%b, cpuacct=%s, cpu=%s", this.name, Integer.valueOf(i), Integer.valueOf(i2), Boolean.valueOf(z), group.toString(), group2.toString());
            } else if (group2 == null || group == null || !group2.group.contains("apps")) {
                throw new NotAndroidAppProcessException(i);
            } else {
                z = !group2.group.contains("bg_non_interactive");
                try {
                    uid2 = Integer.parseInt(group.group.substring(group.group.lastIndexOf("/") + 1));
                } catch (Exception unused2) {
                    uid2 = status().getUid();
                }
                AndroidProcesses.log("name=%s, pid=%d, uid=%d foreground=%b, cpuacct=%s, cpu=%s", this.name, Integer.valueOf(i), Integer.valueOf(i2), Boolean.valueOf(z), group.toString(), group2.toString());
            }
        } else {
            Stat stat = stat();
            Status status = status();
            z = stat.policy() == 0;
            i2 = status.getUid();
            AndroidProcesses.log("name=%s, pid=%d, uid=%d foreground=%b", this.name, Integer.valueOf(i), Integer.valueOf(i2), Boolean.valueOf(z));
        }
        this.foreground = z;
        this.uid = i2;
    }

    public String getPackageName() {
        return this.name.split(":")[0];
    }

    public PackageInfo getPackageInfo(Context context, int i) throws NameNotFoundException {
        return context.getPackageManager().getPackageInfo(getPackageName(), i);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeByte(this.foreground ? (byte) 1 : 0);
        parcel.writeInt(this.uid);
    }

    protected AndroidAppProcess(Parcel parcel) {
        super(parcel);
        this.foreground = parcel.readByte() != 0;
        this.uid = parcel.readInt();
    }
}
