package com.jaredrummler.android.processes;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.jaredrummler.android.processes";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = -1;
    public static final String VERSION_NAME = "";
}
