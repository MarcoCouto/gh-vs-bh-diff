package com.mansoon.BatteryDouble.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.OpenMessageEvent;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDb;
import com.mansoon.BatteryDouble.models.data.Message;
import com.mansoon.BatteryDouble.ui.adapters.MessageAdapter;
import java.util.ArrayList;
import java.util.Iterator;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class InboxActivity extends BaseActivity {
    private MessageAdapter mAdapter;
    private ArrayList<Message> mMessages;
    private TextView mNoMessagesTextView;
    private RecyclerView mRecyclerView;

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_inbox);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.mNoMessagesTextView = (TextView) findViewById(R.id.no_messages_view);
        this.mRecyclerView = (RecyclerView) findViewById(R.id.rv);
        this.mAdapter = null;
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        this.mRecyclerView.setHasFixedSize(true);
    }

    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openMessage(OpenMessageEvent openMessageEvent) {
        Message message = (Message) this.mMessages.get(openMessageEvent.index);
        if (!message.realmGet$read()) {
            GreenHubDb greenHubDb = new GreenHubDb();
            greenHubDb.markMessageAsRead(message.realmGet$id());
            greenHubDb.close();
        }
        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("id", message.realmGet$id());
        intent.putExtra("title", message.realmGet$title());
        intent.putExtra(TtmlNode.TAG_BODY, message.realmGet$body());
        intent.putExtra("date", message.realmGet$date());
        startActivity(intent);
    }

    private void loadData() {
        this.mMessages = new ArrayList<>();
        GreenHubDb greenHubDb = new GreenHubDb();
        Iterator it = greenHubDb.allMessages().iterator();
        while (it.hasNext()) {
            Message message = (Message) it.next();
            ArrayList<Message> arrayList = this.mMessages;
            Message message2 = new Message(message.realmGet$id(), message.realmGet$title(), message.realmGet$body(), message.realmGet$date(), message.realmGet$read());
            arrayList.add(message2);
        }
        greenHubDb.close();
        setAdapter();
    }

    private void setAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new MessageAdapter(this.mMessages);
            this.mRecyclerView.setAdapter(this.mAdapter);
        } else {
            this.mAdapter.swap(this.mMessages);
        }
        this.mRecyclerView.invalidate();
        if (this.mMessages.isEmpty()) {
            this.mRecyclerView.setVisibility(8);
            this.mNoMessagesTextView.setVisibility(0);
            return;
        }
        this.mRecyclerView.setVisibility(0);
        this.mNoMessagesTextView.setVisibility(8);
    }
}
