package com.mansoon.BatteryDouble.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mansoon.BatteryDouble.R;

public class HistoryFragment extends Fragment {
    private static final String TAG = "HistoryFragment";

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_history, viewGroup, false);
    }
}
