package com.mansoon.BatteryDouble.events;

import com.mansoon.BatteryDouble.models.ui.Task;

public class OpenTaskDetailsEvent {
    public final Task task;

    public OpenTaskDetailsEvent(Task task2) {
        this.task = task2;
    }
}
