package com.mansoon.BatteryDouble.util;

import android.content.Context;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import com.mansoon.BatteryDouble.Config;

public class SettingsUtils {
    public static final String PREF_APP_VERSION = "pref_app_version";
    public static final String PREF_AUTO_UPLOAD = "pref_auto_upload";
    public static final String PREF_BATTERY_ALERTS = "pref_battery_alerts";
    public static final String PREF_CHARGE_ALERTS = "pref_charge_alerts";
    public static final String PREF_DATA_HISTORY = "pref_data_history";
    public static final String PREF_DEVICE_REGISTERED = "pref_device_registered";
    public static final String PREF_HIDE_SYSTEM_APPS = "pref_system_apps";
    public static final String PREF_LAST_TEMPERATURE_ALERT = "pref_last_temperature_alert";
    public static final String PREF_MESSAGE_ALERTS = "pref_message_alerts";
    public static final String PREF_MESSAGE_LAST_ID = "pref_message_last";
    public static final String PREF_MOBILE_DATA = "pref_mobile_data";
    public static final String PREF_NOTIFICATIONS_PRIORITY = "pref_notifications_priority";
    public static final String PREF_POWER_INDICATOR = "pref_power_indicator";
    public static final String PREF_SAMPLING_SCREEN = "pref_sampling_screen";
    public static final String PREF_SEND_INSTALLED_PACKAGES = "pref_send_installed";
    public static final String PREF_SERVER_URL = "pref_server_url";
    public static final String PREF_TEMPERATURE_ALERTS = "pref_temperature_alerts";
    public static final String PREF_TEMPERATURE_HIGH = "pref_temperature_high";
    public static final String PREF_TEMPERATURE_RATE = "pref_temperature_rate";
    public static final String PREF_TEMPERATURE_WARNING = "pref_temperature_warning";
    public static final String PREF_TOS_ACCEPTED = "pref_tos_accepted";
    public static final String PREF_UPLOAD_RATE = "pref_upload_rate";
    public static final String PREF_USE_OLD_MEASUREMENT = "pref_old_measurement";
    private static final String TAG = "SettingsUtils";

    public static boolean isTosAccepted(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_TOS_ACCEPTED, false);
    }

    public static void markTosAccepted(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_TOS_ACCEPTED, z).apply();
    }

    public static void markDeviceAccepted(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_DEVICE_REGISTERED, z).apply();
    }

    public static boolean isDeviceRegistered(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_DEVICE_REGISTERED, false);
    }

    public static void saveServerUrl(Context context, String str) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(PREF_SERVER_URL, str).apply();
    }

    public static String fetchServerUrl(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_SERVER_URL, Config.SERVER_URL_DEFAULT);
    }

    public static boolean isServerUrlPresent(Context context) {
        return !PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_SERVER_URL, Config.SERVER_URL_DEFAULT).equals(Config.SERVER_URL_DEFAULT);
    }

    public static void markInstalledPackagesIncluded(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_SEND_INSTALLED_PACKAGES, z).apply();
    }

    public static boolean isInstalledPackagesIncluded(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_SEND_INSTALLED_PACKAGES, false);
    }

    public static boolean isSamplingScreenOn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_SAMPLING_SCREEN, false);
    }

    public static int fetchDataHistoryInterval(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_DATA_HISTORY, Config.DATA_HISTORY_DEFAULT));
    }

    public static boolean isMobileDataAllowed(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_MOBILE_DATA, false);
    }

    public static boolean isAutomaticUploadingAllowed(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_AUTO_UPLOAD, true);
    }

    public static int fetchUploadRate(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_UPLOAD_RATE, Config.UPLOAD_DEFAULT_RATE));
    }

    public static int fetchNotificationsPriority(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_NOTIFICATIONS_PRIORITY, "0"));
    }

    public static boolean isPowerIndicatorShown(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_POWER_INDICATOR, false);
    }

    public static boolean isBatteryAlertsOn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_BATTERY_ALERTS, true);
    }

    public static boolean isChargeAlertsOn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_CHARGE_ALERTS, true);
    }

    public static boolean isTemperatureAlertsOn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_TEMPERATURE_ALERTS, true);
    }

    public static int fetchTemperatureAlertsRate(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_TEMPERATURE_RATE, Config.NOTIFICATION_DEFAULT_TEMPERATURE_RATE));
    }

    public static int fetchTemperatureWarning(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_TEMPERATURE_WARNING, Config.NOTIFICATION_DEFAULT_TEMPERATURE_WARNING).replaceFirst("^0+(?!$)", ""));
    }

    public static int fetchTemperatureHigh(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_TEMPERATURE_HIGH, Config.NOTIFICATION_DEFAULT_TEMPERATURE_HIGH).replaceFirst("^0+(?!$)", ""));
    }

    public static void saveLastTemperatureAlertDate(Context context, long j) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(PREF_LAST_TEMPERATURE_ALERT, j).apply();
    }

    public static long fetchLastTemperatureAlertDate(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(PREF_LAST_TEMPERATURE_ALERT, 0);
    }

    public static boolean isMessageAlertsOn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_MESSAGE_ALERTS, true);
    }

    public static void saveLastMessageId(Context context, int i) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(PREF_MESSAGE_LAST_ID, i).apply();
    }

    public static int fetchLastMessageId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(PREF_MESSAGE_LAST_ID, 0);
    }

    public static boolean isSystemAppsHidden(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_HIDE_SYSTEM_APPS, true);
    }

    public static void markSystemAppsHidden(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_HIDE_SYSTEM_APPS, z).apply();
    }

    public static boolean isOldMeasurementUsed(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_USE_OLD_MEASUREMENT, false);
    }

    public static void markOldMeasurementUsed(Context context, boolean z) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PREF_USE_OLD_MEASUREMENT, z).apply();
    }

    public static void saveAppVersion(Context context, int i) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(PREF_APP_VERSION, i).apply();
    }

    public static int fetchAppVersion(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(PREF_APP_VERSION, 21);
    }

    public static void registerOnSharedPreferenceChangeListener(Context context, OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        PreferenceManager.getDefaultSharedPreferences(context).registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public static void unregisterOnSharedPreferenceChangeListener(Context context, OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        PreferenceManager.getDefaultSharedPreferences(context).unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }
}
