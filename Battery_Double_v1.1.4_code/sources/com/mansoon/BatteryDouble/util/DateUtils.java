package com.mansoon.BatteryDouble.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateUtils {
    private static final String DATE_FORMAT = "dd-MM HH:mm";
    public static final int INTERVAL_10DAYS = 4;
    public static final int INTERVAL_15DAYS = 5;
    public static final int INTERVAL_24H = 1;
    public static final int INTERVAL_3DAYS = 2;
    public static final int INTERVAL_5DAYS = 3;
    private static final String TAG = "DateUtils";
    private static SimpleDateFormat sSimpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.UK);

    public static String convertMilliSecondsToFormattedDate(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return sSimpleDateFormat.format(instance.getTime());
    }

    public static long getMilliSecondsInterval(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        if (i == 1) {
            return currentTimeMillis - 86400000;
        }
        if (i == 2) {
            return currentTimeMillis - 259200000;
        }
        if (i == 3) {
            return currentTimeMillis - 432000000;
        }
        if (i == 4) {
            return currentTimeMillis - 864000000;
        }
        return i == 5 ? currentTimeMillis - 1296000000 : currentTimeMillis;
    }
}
