package com.mansoon.BatteryDouble.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import com.mansoon.BatteryDouble.Config;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.managers.sampling.DataEstimator;
import com.mansoon.BatteryDouble.managers.sampling.Inspector;
import com.mansoon.BatteryDouble.models.Battery;
import com.mansoon.BatteryDouble.ui.InboxActivity;
import com.mansoon.BatteryDouble.ui.MainActivity;

public class Notifier {
    private static final String TAG = LogUtils.makeLogTag(Notifier.class);
    private static boolean isStatusBarShown = false;
    private static Builder sBuilder;
    private static NotificationManager sNotificationManager;

    public static void startStatusBar(Context context) {
        if (!isStatusBarShown) {
            DataEstimator dataEstimator = new DataEstimator();
            dataEstimator.getCurrentStatus(context);
            int batteryCurrentNow = Battery.getBatteryCurrentNow(context);
            int level = dataEstimator.getLevel();
            StringBuilder sb = new StringBuilder();
            sb.append(context.getString(R.string.now));
            sb.append(": ");
            sb.append(batteryCurrentNow);
            sb.append(" mA");
            String sb2 = sb.toString();
            sBuilder = new Builder(context).setContentTitle(sb2).setContentText(context.getString(R.string.notif_batteryhub_running)).setAutoCancel(false).setOngoing(true).setPriority(SettingsUtils.fetchNotificationsPriority(context));
            if (VERSION.SDK_INT >= 21) {
                sBuilder.setVisibility(1);
            }
            if (level < 100) {
                sBuilder.setSmallIcon(R.drawable.ic_stat_00_pct_charged + level);
            } else {
                sBuilder.setSmallIcon(R.drawable.ic_stat_z100_pct_charged);
            }
            Intent intent = new Intent(context, MainActivity.class);
            TaskStackBuilder create = TaskStackBuilder.create(context);
            create.addParentStack(MainActivity.class);
            create.addNextIntent(intent);
            sBuilder.setContentIntent(create.getPendingIntent(0, 134217728));
            sNotificationManager = (NotificationManager) context.getSystemService("notification");
            sNotificationManager.notify(1001, sBuilder.build());
            isStatusBarShown = true;
        }
    }

    public static void updateStatusBar(Context context) {
        if (!isStatusBarShown) {
            startStatusBar(context);
            return;
        }
        int batteryCurrentNow = Battery.getBatteryCurrentNow(context);
        int currentBatteryLevel = (int) (Inspector.getCurrentBatteryLevel() * 100.0d);
        StringBuilder sb = new StringBuilder();
        sb.append(context.getString(R.string.now));
        sb.append(": ");
        sb.append(batteryCurrentNow);
        sb.append(" mA");
        String sb2 = sb.toString();
        sBuilder.setContentTitle(sb2).setContentText(context.getString(R.string.notif_batteryhub_running)).setAutoCancel(false).setOngoing(true).setPriority(SettingsUtils.fetchNotificationsPriority(context));
        if (VERSION.SDK_INT >= 21) {
            sBuilder.setVisibility(1);
        }
        if (currentBatteryLevel < 100) {
            sBuilder.setSmallIcon(R.drawable.ic_stat_00_pct_charged + currentBatteryLevel);
        } else {
            sBuilder.setSmallIcon(R.drawable.ic_stat_z100_pct_charged);
        }
        LogUtils.LOGI(TAG, "Updating value of notification");
        sNotificationManager.notify(1001, sBuilder.build());
    }

    public static void closeStatusBar() {
        sNotificationManager.cancel(1001);
        isStatusBarShown = false;
    }

    public static void newMessageAlert(Context context) {
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService("notification");
        }
        Builder priority = new Builder(context).setSmallIcon(R.drawable.ic_email_white_24dp).setContentTitle(context.getString(R.string.notif_new_message)).setContentText(context.getString(R.string.notif_open_inbox)).setAutoCancel(true).setOngoing(false).setLights(-16711936, 500, 2000).setVibrate(new long[]{0, 800, 1500}).setPriority(SettingsUtils.fetchNotificationsPriority(context));
        if (VERSION.SDK_INT >= 21) {
            priority.setVisibility(1);
        }
        Intent intent = new Intent(context, InboxActivity.class);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        priority.setContentIntent(create.getPendingIntent(0, 134217728));
        Notification build = priority.build();
        build.flags |= 8;
        sNotificationManager.notify(1006, build);
    }

    public static void batteryFullAlert(Context context) {
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService("notification");
        }
        Builder priority = new Builder(context).setSmallIcon(R.drawable.ic_information_white_24dp).setContentTitle(context.getString(R.string.notif_battery_full)).setContentText(context.getString(R.string.notif_remove_charger)).setAutoCancel(true).setOngoing(false).setLights(-16711936, 500, 2000).setVibrate(new long[]{0, 400, 1000}).setPriority(SettingsUtils.fetchNotificationsPriority(context));
        if (VERSION.SDK_INT >= 21) {
            priority.setVisibility(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        priority.setContentIntent(create.getPendingIntent(0, 134217728));
        sNotificationManager.notify(1002, priority.build());
    }

    public static void batteryLowAlert(Context context) {
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService("notification");
        }
        Builder priority = new Builder(context).setSmallIcon(R.drawable.ic_alert_circle_white_24dp).setContentTitle(context.getString(R.string.notif_battery_low)).setContentText(context.getString(R.string.notif_connect_power)).setAutoCancel(true).setOngoing(false).setLights(SupportMenu.CATEGORY_MASK, 500, 2000).setVibrate(new long[]{0, 400, 1000}).setPriority(SettingsUtils.fetchNotificationsPriority(context));
        if (VERSION.SDK_INT >= 21) {
            priority.setVisibility(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        priority.setContentIntent(create.getPendingIntent(0, 134217728));
        sNotificationManager.notify(1003, priority.build());
    }

    public static void batteryWarningTemperature(Context context) {
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService("notification");
        }
        Builder priority = new Builder(context).setSmallIcon(R.drawable.ic_alert_circle_white_24dp).setContentTitle(context.getString(R.string.notif_battery_warning)).setContentText(context.getString(R.string.notif_battery_warm)).setAutoCancel(true).setOngoing(false).setLights(InputDeviceCompat.SOURCE_ANY, 500, 2000).setVibrate(new long[]{0, 400, 1000}).setPriority(SettingsUtils.fetchNotificationsPriority(context));
        if (VERSION.SDK_INT >= 21) {
            priority.setVisibility(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", 2);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        priority.setContentIntent(create.getPendingIntent(0, 134217728));
        Notification build = priority.build();
        build.flags |= 8;
        sNotificationManager.notify(1004, build);
    }

    public static void batteryHighTemperature(Context context) {
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService("notification");
        }
        Builder priority = new Builder(context).setSmallIcon(R.drawable.ic_alert_circle_white_24dp).setContentTitle(context.getString(R.string.notif_battery_hot)).setContentText(context.getString(R.string.notif_battery_cooldown)).setAutoCancel(true).setOngoing(false).setLights(SupportMenu.CATEGORY_MASK, 500, 2000).setVibrate(new long[]{0, 800, 1500}).setPriority(SettingsUtils.fetchNotificationsPriority(context));
        if (VERSION.SDK_INT >= 21) {
            priority.setVisibility(1);
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", 2);
        TaskStackBuilder create = TaskStackBuilder.create(context);
        create.addParentStack(InboxActivity.class);
        create.addNextIntent(intent);
        priority.setContentIntent(create.getPendingIntent(0, 134217728));
        Notification build = priority.build();
        build.flags |= 8;
        sNotificationManager.notify(Config.NOTIFICATION_TEMPERATURE_HIGH, build);
    }
}
