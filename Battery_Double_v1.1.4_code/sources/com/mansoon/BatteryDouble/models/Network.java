package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;

public class Network {
    public static String DATA_ACTIVITY_DORMANT = "dormant";
    public static String DATA_ACTIVITY_IN = "in";
    public static String DATA_ACTIVITY_INOUT = "inout";
    public static String DATA_ACTIVITY_NONE = "none";
    public static String DATA_ACTIVITY_OUT = "out";
    public static String DATA_CONNECTED = "connected";
    public static String DATA_CONNECTING = "connecting";
    public static String DATA_DISCONNECTED = "disconnected";
    public static String DATA_SUSPENDED = "suspended";
    private static final int EHRPD = 14;
    private static final int EVDO_B = 12;
    private static final int HSPAP = 15;
    private static final int LTE = 13;
    public static final String NETWORKSTATUS_CONNECTED = "connected";
    public static final String NETWORKSTATUS_CONNECTING = "connecting";
    public static final String NETWORKSTATUS_DISCONNECTED = "disconnected";
    public static final String NETWORKSTATUS_DISCONNECTING = "disconnecting";
    public static String NETWORK_TYPE_1xRTT = "1xrtt";
    public static String NETWORK_TYPE_CDMA = "cdma";
    public static String NETWORK_TYPE_EDGE = "edge";
    public static String NETWORK_TYPE_EHRPD = "ehrpd";
    public static String NETWORK_TYPE_EVDO_0 = "evdo_0";
    public static String NETWORK_TYPE_EVDO_A = "evdo_a";
    public static String NETWORK_TYPE_EVDO_B = "evdo_b";
    public static String NETWORK_TYPE_GPRS = "gprs";
    public static String NETWORK_TYPE_HSDPA = "hsdpa";
    public static String NETWORK_TYPE_HSPA = "hspa";
    public static String NETWORK_TYPE_HSPAP = "hspap";
    public static String NETWORK_TYPE_HSUPA = "hsupa";
    public static String NETWORK_TYPE_IDEN = "iden";
    public static String NETWORK_TYPE_LTE = "lte";
    public static String NETWORK_TYPE_UMTS = "utms";
    public static String NETWORK_TYPE_UNKNOWN = "unknown";
    private static final String TAG = "Network";
    public static final String TYPE_UNKNOWN = "unknown";

    /* renamed from: com.mansoon.BatteryDouble.models.Network$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$android$net$NetworkInfo$State = new int[State.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            $SwitchMap$android$net$NetworkInfo$State[State.CONNECTED.ordinal()] = 1;
            $SwitchMap$android$net$NetworkInfo$State[State.DISCONNECTED.ordinal()] = 2;
            $SwitchMap$android$net$NetworkInfo$State[State.CONNECTING.ordinal()] = 3;
            try {
                $SwitchMap$android$net$NetworkInfo$State[State.DISCONNECTING.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public static String getStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return NETWORKSTATUS_DISCONNECTED;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return NETWORKSTATUS_DISCONNECTED;
        }
        switch (AnonymousClass1.$SwitchMap$android$net$NetworkInfo$State[activeNetworkInfo.getState().ordinal()]) {
            case 1:
                return NETWORKSTATUS_CONNECTED;
            case 2:
                return NETWORKSTATUS_DISCONNECTED;
            case 3:
                return NETWORKSTATUS_CONNECTING;
            case 4:
                return NETWORKSTATUS_DISCONNECTING;
            default:
                return NETWORKSTATUS_DISCONNECTING;
        }
    }

    public static String getType(Context context) {
        String str;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return "unknown";
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            str = "unknown";
        } else {
            str = activeNetworkInfo.getTypeName();
        }
        return str;
    }

    public static boolean isAvailable(Context context) {
        return getStatus(context).equals(NETWORKSTATUS_CONNECTED);
    }

    public static boolean isMobileDataEnabled(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 0;
    }

    public static String getMobileNetworkType(Context context) {
        int networkType = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getNetworkType();
        switch (networkType) {
            case 1:
                return NETWORK_TYPE_GPRS;
            case 2:
                return NETWORK_TYPE_EDGE;
            case 3:
                return NETWORK_TYPE_UMTS;
            case 4:
                return NETWORK_TYPE_CDMA;
            case 5:
                return NETWORK_TYPE_EVDO_0;
            case 6:
                return NETWORK_TYPE_EVDO_A;
            case 7:
                return NETWORK_TYPE_1xRTT;
            case 8:
                return NETWORK_TYPE_HSDPA;
            case 9:
                return NETWORK_TYPE_HSUPA;
            case 10:
                return NETWORK_TYPE_HSPA;
            case 11:
                return NETWORK_TYPE_IDEN;
            case 12:
                return NETWORK_TYPE_EVDO_B;
            case 13:
                return NETWORK_TYPE_LTE;
            case 14:
                return NETWORK_TYPE_EHRPD;
            case 15:
                return NETWORK_TYPE_HSPAP;
            default:
                StringBuilder sb = new StringBuilder();
                sb.append(networkType);
                sb.append("");
                return sb.toString();
        }
    }

    public static int getRoamingStatus(Context context) {
        return ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).isNetworkRoaming() ? 1 : 0;
    }

    public static String getDataState(Context context) {
        switch (((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getDataState()) {
            case 0:
                return DATA_DISCONNECTED;
            case 1:
                return DATA_CONNECTING;
            case 2:
                return DATA_CONNECTED;
            default:
                return DATA_SUSPENDED;
        }
    }

    public static String getDataActivity(Context context) {
        switch (((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getDataActivity()) {
            case 1:
                return DATA_ACTIVITY_IN;
            case 2:
                return DATA_ACTIVITY_OUT;
            case 3:
                return DATA_ACTIVITY_INOUT;
            default:
                return DATA_ACTIVITY_NONE;
        }
    }
}
