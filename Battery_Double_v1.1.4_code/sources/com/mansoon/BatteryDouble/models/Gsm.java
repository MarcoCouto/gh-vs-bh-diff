package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.facebook.places.model.PlaceFields;
import com.mansoon.BatteryDouble.models.data.CellInfo;

public class Gsm {
    private static final String TAG = "Gsm";

    public static CellInfo getCellInfo(Context context) {
        CellInfo cellInfo = new CellInfo();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        String networkOperator = telephonyManager.getNetworkOperator();
        if (networkOperator == null || networkOperator.length() < 3) {
            return cellInfo;
        }
        if (Phone.getType(context).equals(Phone.PHONE_TYPE_CDMA)) {
            CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) telephonyManager.getCellLocation();
            cellInfo.realmSet$cid(cdmaCellLocation.getBaseStationId());
            cellInfo.realmSet$lac(cdmaCellLocation.getNetworkId());
            cellInfo.realmSet$mnc(cdmaCellLocation.getSystemId());
            cellInfo.realmSet$mcc(Integer.parseInt(networkOperator.substring(0, 3)));
            cellInfo.realmSet$radioType(Network.getMobileNetworkType(context));
        } else if (Phone.getType(context).equals(Phone.PHONE_TYPE_GSM)) {
            GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
            cellInfo.realmSet$mcc(Integer.parseInt(networkOperator.substring(0, 3)));
            cellInfo.realmSet$mnc(Integer.parseInt(networkOperator.substring(3)));
            cellInfo.realmSet$lac(gsmCellLocation.getLac());
            cellInfo.realmSet$cid(gsmCellLocation.getCid());
            cellInfo.realmSet$radioType(Network.getMobileNetworkType(context));
        }
        return cellInfo;
    }
}
