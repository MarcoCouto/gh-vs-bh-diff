package com.mansoon.BatteryDouble.models.data;

import io.realm.DeviceRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class Device extends RealmObject implements DeviceRealmProxyInterface {
    public String brand;
    public int isRoot;
    public String kernelVersion;
    public String manufacturer;
    public String model;
    public String osVersion;
    public String product;
    public String uuId;

    public String realmGet$brand() {
        return this.brand;
    }

    public int realmGet$isRoot() {
        return this.isRoot;
    }

    public String realmGet$kernelVersion() {
        return this.kernelVersion;
    }

    public String realmGet$manufacturer() {
        return this.manufacturer;
    }

    public String realmGet$model() {
        return this.model;
    }

    public String realmGet$osVersion() {
        return this.osVersion;
    }

    public String realmGet$product() {
        return this.product;
    }

    public String realmGet$uuId() {
        return this.uuId;
    }

    public void realmSet$brand(String str) {
        this.brand = str;
    }

    public void realmSet$isRoot(int i) {
        this.isRoot = i;
    }

    public void realmSet$kernelVersion(String str) {
        this.kernelVersion = str;
    }

    public void realmSet$manufacturer(String str) {
        this.manufacturer = str;
    }

    public void realmSet$model(String str) {
        this.model = str;
    }

    public void realmSet$osVersion(String str) {
        this.osVersion = str;
    }

    public void realmSet$product(String str) {
        this.product = str;
    }

    public void realmSet$uuId(String str) {
        this.uuId = str;
    }

    public Device() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
