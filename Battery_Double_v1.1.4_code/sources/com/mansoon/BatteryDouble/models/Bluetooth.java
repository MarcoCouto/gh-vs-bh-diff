package com.mansoon.BatteryDouble.models;

import android.bluetooth.BluetoothAdapter;

public class Bluetooth {
    private static final String TAG = "Bluetooth";

    public static boolean isEnabled() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        return defaultAdapter != null && defaultAdapter.isEnabled();
    }

    public static String getAddress() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        return defaultAdapter != null ? defaultAdapter.getAddress() : "not available";
    }
}
