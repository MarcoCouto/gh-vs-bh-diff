package com.mansoon.BatteryDouble.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.mansoon.BatteryDouble.events.PowerSourceEvent;
import com.mansoon.BatteryDouble.managers.sampling.Inspector;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDb;
import com.mansoon.BatteryDouble.util.LogUtils;
import org.greenrobot.eventbus.EventBus;

public class PowerConnectionReceiver extends BroadcastReceiver {
    private static final String TAG = LogUtils.makeLogTag(PowerConnectionReceiver.class);

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            Intent registerReceiver = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver != null) {
                int intExtra = registerReceiver.getIntExtra("plugged", -1);
                boolean z = false;
                boolean z2 = intExtra == 2;
                boolean z3 = intExtra == 1;
                if (intExtra == 4) {
                    z = true;
                }
                if (z3) {
                    EventBus.getDefault().post(new PowerSourceEvent("ac"));
                } else if (z2) {
                    EventBus.getDefault().post(new PowerSourceEvent("usb"));
                } else if (z) {
                    EventBus.getDefault().post(new PowerSourceEvent("wireless"));
                }
            } else {
                return;
            }
        } else if (intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
            EventBus.getDefault().post(new PowerSourceEvent("unplugged"));
        }
        GreenHubDb greenHubDb = new GreenHubDb();
        LogUtils.LOGI(TAG, "Getting new session");
        greenHubDb.saveSession(Inspector.getBatterySession(context, intent));
        greenHubDb.close();
    }
}
