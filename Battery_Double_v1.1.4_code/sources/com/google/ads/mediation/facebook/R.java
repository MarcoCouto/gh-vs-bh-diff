package com.google.ads.mediation.facebook;

public final class R {

    public static final class attr {
        public static final int adSize = 2130968610;
        public static final int adSizes = 2130968611;
        public static final int adUnitId = 2130968612;
        public static final int buttonSize = 2130968651;
        public static final int circleCrop = 2130968665;
        public static final int colorScheme = 2130968682;
        public static final int fastScrollEnabled = 2130968746;
        public static final int fastScrollHorizontalThumbDrawable = 2130968747;
        public static final int fastScrollHorizontalTrackDrawable = 2130968748;
        public static final int fastScrollVerticalThumbDrawable = 2130968749;
        public static final int fastScrollVerticalTrackDrawable = 2130968750;
        public static final int font = 2130968751;
        public static final int fontProviderAuthority = 2130968753;
        public static final int fontProviderCerts = 2130968754;
        public static final int fontProviderFetchStrategy = 2130968755;
        public static final int fontProviderFetchTimeout = 2130968756;
        public static final int fontProviderPackage = 2130968757;
        public static final int fontProviderQuery = 2130968758;
        public static final int fontStyle = 2130968759;
        public static final int fontWeight = 2130968760;
        public static final int imageAspectRatio = 2130968776;
        public static final int imageAspectRatioAdjust = 2130968777;
        public static final int layoutManager = 2130968790;
        public static final int reverseLayout = 2130968849;
        public static final int scopeUris = 2130968851;
        public static final int spanCount = 2130968865;
        public static final int stackFromEnd = 2130968871;
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131099723;
        public static final int common_google_signin_btn_text_dark_default = 2131099724;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099725;
        public static final int common_google_signin_btn_text_dark_focused = 2131099726;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099727;
        public static final int common_google_signin_btn_text_light = 2131099728;
        public static final int common_google_signin_btn_text_light_default = 2131099729;
        public static final int common_google_signin_btn_text_light_disabled = 2131099730;
        public static final int common_google_signin_btn_text_light_focused = 2131099731;
        public static final int common_google_signin_btn_text_light_pressed = 2131099732;
        public static final int common_google_signin_btn_tint = 2131099733;
        public static final int notification_action_color_filter = 2131099770;
        public static final int notification_icon_bg_color = 2131099771;
        public static final int notification_material_background_media_default_color = 2131099772;
        public static final int primary_text_default_material_dark = 2131099777;
        public static final int ripple_material_light = 2131099782;
        public static final int secondary_text_default_material_dark = 2131099783;
        public static final int secondary_text_default_material_light = 2131099784;
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131165283;
        public static final int compat_button_inset_vertical_material = 2131165284;
        public static final int compat_button_padding_horizontal_material = 2131165285;
        public static final int compat_button_padding_vertical_material = 2131165286;
        public static final int compat_control_corner_material = 2131165287;
        public static final int fastscroll_default_thickness = 2131165329;
        public static final int fastscroll_margin = 2131165330;
        public static final int fastscroll_minimum_range = 2131165331;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165340;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165341;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165342;
        public static final int notification_action_icon_size = 2131165349;
        public static final int notification_action_text_size = 2131165350;
        public static final int notification_big_circle_margin = 2131165351;
        public static final int notification_content_margin_start = 2131165352;
        public static final int notification_large_icon_height = 2131165353;
        public static final int notification_large_icon_width = 2131165354;
        public static final int notification_main_column_padding_top = 2131165355;
        public static final int notification_media_narrow_margin = 2131165356;
        public static final int notification_right_icon_size = 2131165357;
        public static final int notification_right_side_padding_top = 2131165358;
        public static final int notification_small_icon_background_padding = 2131165359;
        public static final int notification_small_icon_size_as_large = 2131165360;
        public static final int notification_subtext_size = 2131165361;
        public static final int notification_top_pad = 2131165362;
        public static final int notification_top_pad_large_text = 2131165363;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131230858;
        public static final int common_google_signin_btn_icon_dark = 2131230859;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230860;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230861;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230862;
        public static final int common_google_signin_btn_icon_disabled = 2131230863;
        public static final int common_google_signin_btn_icon_light = 2131230864;
        public static final int common_google_signin_btn_icon_light_focused = 2131230865;
        public static final int common_google_signin_btn_icon_light_normal = 2131230866;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230867;
        public static final int common_google_signin_btn_text_dark = 2131230868;
        public static final int common_google_signin_btn_text_dark_focused = 2131230869;
        public static final int common_google_signin_btn_text_dark_normal = 2131230870;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230871;
        public static final int common_google_signin_btn_text_disabled = 2131230872;
        public static final int common_google_signin_btn_text_light = 2131230873;
        public static final int common_google_signin_btn_text_light_focused = 2131230874;
        public static final int common_google_signin_btn_text_light_normal = 2131230875;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230876;
        public static final int googleg_disabled_color_18 = 2131230886;
        public static final int googleg_standard_color_18 = 2131230887;
        public static final int notification_action_background = 2131231057;
        public static final int notification_bg = 2131231058;
        public static final int notification_bg_low = 2131231059;
        public static final int notification_bg_low_normal = 2131231060;
        public static final int notification_bg_low_pressed = 2131231061;
        public static final int notification_bg_normal = 2131231062;
        public static final int notification_bg_normal_pressed = 2131231063;
        public static final int notification_icon_background = 2131231064;
        public static final int notification_template_icon_bg = 2131231065;
        public static final int notification_template_icon_low_bg = 2131231066;
        public static final int notification_tile_bg = 2131231067;
        public static final int notify_panel_notification_icon_bg = 2131231068;
    }

    public static final class id {
        public static final int action0 = 2131296262;
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int adjust_height = 2131296293;
        public static final int adjust_width = 2131296294;
        public static final int async = 2131296312;
        public static final int auto = 2131296313;
        public static final int blocking = 2131296329;
        public static final int button = 2131296338;
        public static final int cancel_action = 2131296343;
        public static final int center = 2131296345;
        public static final int chronometer = 2131296352;
        public static final int dark = 2131296374;
        public static final int end_padder = 2131296388;
        public static final int forever = 2131296401;
        public static final int icon = 2131296407;
        public static final int icon_group = 2131296408;
        public static final int icon_only = 2131296409;
        public static final int info = 2131296417;
        public static final int italic = 2131296422;
        public static final int item_touch_helper_previous_elevation = 2131296424;
        public static final int light = 2131296431;
        public static final int line1 = 2131296432;
        public static final int line3 = 2131296433;
        public static final int media_actions = 2131296442;
        public static final int none = 2131296467;
        public static final int normal = 2131296468;
        public static final int notification_background = 2131296469;
        public static final int notification_main_column = 2131296470;
        public static final int notification_main_column_container = 2131296471;
        public static final int radio = 2131296485;
        public static final int right_icon = 2131296487;
        public static final int right_side = 2131296488;
        public static final int standard = 2131296528;
        public static final int status_bar_latest_event_content = 2131296531;
        public static final int text = 2131296552;
        public static final int text2 = 2131296553;
        public static final int time = 2131296559;
        public static final int title = 2131296560;
        public static final int wide = 2131296584;
        public static final int wrap_content = 2131296589;
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131361796;
        public static final int google_play_services_version = 2131361799;
        public static final int status_bar_notification_info_maxnum = 2131361802;
    }

    public static final class layout {
        public static final int notification_action = 2131427403;
        public static final int notification_action_tombstone = 2131427404;
        public static final int notification_media_action = 2131427405;
        public static final int notification_media_cancel_action = 2131427406;
        public static final int notification_template_big_media = 2131427407;
        public static final int notification_template_big_media_custom = 2131427408;
        public static final int notification_template_big_media_narrow = 2131427409;
        public static final int notification_template_big_media_narrow_custom = 2131427410;
        public static final int notification_template_custom_big = 2131427411;
        public static final int notification_template_icon_group = 2131427412;
        public static final int notification_template_lines_media = 2131427413;
        public static final int notification_template_media = 2131427414;
        public static final int notification_template_media_custom = 2131427415;
        public static final int notification_template_part_chronometer = 2131427416;
        public static final int notification_template_part_time = 2131427417;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131624031;
        public static final int common_google_play_services_enable_text = 2131624032;
        public static final int common_google_play_services_enable_title = 2131624033;
        public static final int common_google_play_services_install_button = 2131624034;
        public static final int common_google_play_services_install_text = 2131624035;
        public static final int common_google_play_services_install_title = 2131624036;
        public static final int common_google_play_services_notification_ticker = 2131624038;
        public static final int common_google_play_services_unknown_issue = 2131624039;
        public static final int common_google_play_services_unsupported_text = 2131624040;
        public static final int common_google_play_services_update_button = 2131624041;
        public static final int common_google_play_services_update_text = 2131624042;
        public static final int common_google_play_services_update_title = 2131624043;
        public static final int common_google_play_services_updating_text = 2131624044;
        public static final int common_google_play_services_wear_update_text = 2131624045;
        public static final int common_open_on_phone = 2131624046;
        public static final int common_signin_button_text = 2131624047;
        public static final int common_signin_button_text_long = 2131624048;
        public static final int fcm_fallback_notification_channel_label = 2131624088;
        public static final int s1 = 2131624182;
        public static final int s2 = 2131624183;
        public static final int s3 = 2131624184;
        public static final int s4 = 2131624185;
        public static final int s5 = 2131624186;
        public static final int s6 = 2131624187;
        public static final int s7 = 2131624188;
        public static final int status_bar_notification_info_overflow = 2131624190;
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131689735;
        public static final int TextAppearance_Compat_Notification_Info = 2131689736;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131689737;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131689738;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131689739;
        public static final int TextAppearance_Compat_Notification_Media = 2131689740;
        public static final int TextAppearance_Compat_Notification_Time = 2131689741;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131689742;
        public static final int TextAppearance_Compat_Notification_Title = 2131689743;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131689744;
        public static final int Theme_IAPTheme = 2131689788;
        public static final int Widget_Compat_NotificationActionContainer = 2131689869;
        public static final int Widget_Compat_NotificationActionText = 2131689870;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.mansoon.BatteryDouble.R.attr.adSize, com.mansoon.BatteryDouble.R.attr.adSizes, com.mansoon.BatteryDouble.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] FontFamily = {com.mansoon.BatteryDouble.R.attr.fontProviderAuthority, com.mansoon.BatteryDouble.R.attr.fontProviderCerts, com.mansoon.BatteryDouble.R.attr.fontProviderFetchStrategy, com.mansoon.BatteryDouble.R.attr.fontProviderFetchTimeout, com.mansoon.BatteryDouble.R.attr.fontProviderPackage, com.mansoon.BatteryDouble.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, com.mansoon.BatteryDouble.R.attr.font, com.mansoon.BatteryDouble.R.attr.fontStyle, com.mansoon.BatteryDouble.R.attr.fontWeight};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_font = 3;
        public static final int FontFamilyFont_fontStyle = 4;
        public static final int FontFamilyFont_fontWeight = 5;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.mansoon.BatteryDouble.R.attr.circleCrop, com.mansoon.BatteryDouble.R.attr.imageAspectRatio, com.mansoon.BatteryDouble.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] RecyclerView = {16842948, 16842993, com.mansoon.BatteryDouble.R.attr.fastScrollEnabled, com.mansoon.BatteryDouble.R.attr.fastScrollHorizontalThumbDrawable, com.mansoon.BatteryDouble.R.attr.fastScrollHorizontalTrackDrawable, com.mansoon.BatteryDouble.R.attr.fastScrollVerticalThumbDrawable, com.mansoon.BatteryDouble.R.attr.fastScrollVerticalTrackDrawable, com.mansoon.BatteryDouble.R.attr.layoutManager, com.mansoon.BatteryDouble.R.attr.reverseLayout, com.mansoon.BatteryDouble.R.attr.spanCount, com.mansoon.BatteryDouble.R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 2;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
        public static final int RecyclerView_layoutManager = 7;
        public static final int RecyclerView_reverseLayout = 8;
        public static final int RecyclerView_spanCount = 9;
        public static final int RecyclerView_stackFromEnd = 10;
        public static final int[] SignInButton = {com.mansoon.BatteryDouble.R.attr.buttonSize, com.mansoon.BatteryDouble.R.attr.colorScheme, com.mansoon.BatteryDouble.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
