package com.google.android.gms.base;

public final class R {

    public static final class attr {
        public static final int buttonSize = 2130968651;
        public static final int circleCrop = 2130968665;
        public static final int colorScheme = 2130968682;
        public static final int imageAspectRatio = 2130968776;
        public static final int imageAspectRatioAdjust = 2130968777;
        public static final int scopeUris = 2130968851;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131099723;
        public static final int common_google_signin_btn_text_dark_default = 2131099724;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099725;
        public static final int common_google_signin_btn_text_dark_focused = 2131099726;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099727;
        public static final int common_google_signin_btn_text_light = 2131099728;
        public static final int common_google_signin_btn_text_light_default = 2131099729;
        public static final int common_google_signin_btn_text_light_disabled = 2131099730;
        public static final int common_google_signin_btn_text_light_focused = 2131099731;
        public static final int common_google_signin_btn_text_light_pressed = 2131099732;
        public static final int common_google_signin_btn_tint = 2131099733;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131230858;
        public static final int common_google_signin_btn_icon_dark = 2131230859;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230860;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230861;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230862;
        public static final int common_google_signin_btn_icon_disabled = 2131230863;
        public static final int common_google_signin_btn_icon_light = 2131230864;
        public static final int common_google_signin_btn_icon_light_focused = 2131230865;
        public static final int common_google_signin_btn_icon_light_normal = 2131230866;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230867;
        public static final int common_google_signin_btn_text_dark = 2131230868;
        public static final int common_google_signin_btn_text_dark_focused = 2131230869;
        public static final int common_google_signin_btn_text_dark_normal = 2131230870;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230871;
        public static final int common_google_signin_btn_text_disabled = 2131230872;
        public static final int common_google_signin_btn_text_light = 2131230873;
        public static final int common_google_signin_btn_text_light_focused = 2131230874;
        public static final int common_google_signin_btn_text_light_normal = 2131230875;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230876;
        public static final int googleg_disabled_color_18 = 2131230886;
        public static final int googleg_standard_color_18 = 2131230887;
    }

    public static final class id {
        public static final int adjust_height = 2131296293;
        public static final int adjust_width = 2131296294;
        public static final int auto = 2131296313;
        public static final int dark = 2131296374;
        public static final int icon_only = 2131296409;
        public static final int light = 2131296431;
        public static final int none = 2131296467;
        public static final int standard = 2131296528;
        public static final int wide = 2131296584;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131624031;
        public static final int common_google_play_services_enable_text = 2131624032;
        public static final int common_google_play_services_enable_title = 2131624033;
        public static final int common_google_play_services_install_button = 2131624034;
        public static final int common_google_play_services_install_text = 2131624035;
        public static final int common_google_play_services_install_title = 2131624036;
        public static final int common_google_play_services_notification_channel_name = 2131624037;
        public static final int common_google_play_services_notification_ticker = 2131624038;
        public static final int common_google_play_services_unsupported_text = 2131624040;
        public static final int common_google_play_services_update_button = 2131624041;
        public static final int common_google_play_services_update_text = 2131624042;
        public static final int common_google_play_services_update_title = 2131624043;
        public static final int common_google_play_services_updating_text = 2131624044;
        public static final int common_google_play_services_wear_update_text = 2131624045;
        public static final int common_open_on_phone = 2131624046;
        public static final int common_signin_button_text = 2131624047;
        public static final int common_signin_button_text_long = 2131624048;
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.mansoon.BatteryDouble.R.attr.circleCrop, com.mansoon.BatteryDouble.R.attr.imageAspectRatio, com.mansoon.BatteryDouble.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.mansoon.BatteryDouble.R.attr.buttonSize, com.mansoon.BatteryDouble.R.attr.colorScheme, com.mansoon.BatteryDouble.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
