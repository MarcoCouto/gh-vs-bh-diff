package com.google.android.gms.internal.measurement;

import android.os.RemoteException;

final class zzef extends zzb {
    private final /* synthetic */ zzea zzadv;
    private final /* synthetic */ boolean zzaed;

    zzef(zzea zzea, boolean z) {
        this.zzadv = zzea;
        this.zzaed = z;
        super(zzea);
    }

    /* access modifiers changed from: 0000 */
    public final void zzgd() throws RemoteException {
        this.zzadv.zzadr.setMeasurementEnabled(this.zzaed, this.timestamp);
    }
}
