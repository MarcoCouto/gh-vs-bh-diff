package com.google.android.gms.internal.ads;

import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdRequest.Gender;

final /* synthetic */ class zzamw {
    private static final /* synthetic */ int[] zzdok = new int[Gender.values().length];
    static final /* synthetic */ int[] zzdol = new int[ErrorCode.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|5|6|7|9|10|(2:11|12)|13|15|16|17|18|19|20|22) */
    /* JADX WARNING: Can't wrap try/catch for region: R(19:0|1|2|3|5|6|7|9|10|11|12|13|15|16|17|18|19|20|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0048 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0052 */
    static {
        try {
            zzdol[ErrorCode.INTERNAL_ERROR.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzdol[ErrorCode.INVALID_REQUEST.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        zzdol[ErrorCode.NETWORK_ERROR.ordinal()] = 3;
        try {
            zzdol[ErrorCode.NO_FILL.ordinal()] = 4;
        } catch (NoSuchFieldError unused3) {
        }
        zzdok[Gender.FEMALE.ordinal()] = 1;
        zzdok[Gender.MALE.ordinal()] = 2;
        zzdok[Gender.UNKNOWN.ordinal()] = 3;
    }
}
