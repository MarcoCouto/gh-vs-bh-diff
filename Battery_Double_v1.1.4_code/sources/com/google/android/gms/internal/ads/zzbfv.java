package com.google.android.gms.internal.ads;

import android.net.Uri;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.Clock;
import java.io.IOException;
import java.nio.ByteBuffer;

@zzark
public final class zzbfv extends zzbfk implements zzpn<zzov> {
    private String url;
    private ByteBuffer zzaep;
    private final zzbdy zzeuo;
    private boolean zzexf;
    private final zzbfu zzexg = new zzbfu();
    private final zzbfc zzexh = new zzbfc();
    private boolean zzexi;
    private final Object zzexj = new Object();
    private boolean zzexk;

    public zzbfv(zzbdz zzbdz, zzbdy zzbdy) {
        super(zzbdz);
        this.zzeuo = zzbdy;
    }

    public final /* bridge */ /* synthetic */ void zzc(Object obj, int i) {
    }

    public final /* bridge */ /* synthetic */ void zze(Object obj) {
    }

    public final String getUrl() {
        return this.url;
    }

    public final boolean zzadc() {
        return this.zzexk;
    }

    /* access modifiers changed from: protected */
    public final String zzey(String str) {
        String valueOf = String.valueOf("cache:");
        String valueOf2 = String.valueOf(super.zzey(str));
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    private final void zzabm() {
        int zzl = (int) this.zzexh.zzl(this.zzaep);
        int round = Math.round(((float) zzl) * (((float) this.zzaep.position()) / ((float) ((int) this.zzexg.zzadb()))));
        zza(this.url, zzey(this.url), (long) round, (long) zzl, round > 0, zzbes.zzacx(), zzbes.zzacy());
    }

    /* JADX WARNING: type inference failed for: r15v1, types: [com.google.android.gms.internal.ads.zzov] */
    /* JADX WARNING: type inference failed for: r15v2, types: [com.google.android.gms.internal.ads.zzov] */
    /* JADX WARNING: type inference failed for: r19v0 */
    /* JADX WARNING: type inference failed for: r15v3 */
    /* JADX WARNING: type inference failed for: r19v1 */
    /* JADX WARNING: type inference failed for: r19v2 */
    /* JADX WARNING: type inference failed for: r1v27, types: [com.google.android.gms.internal.ads.zzbep] */
    /* JADX WARNING: type inference failed for: r15v6 */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r9.zzexk = true;
        zzc(r10, r11, (long) ((int) r9.zzexh.zzl(r9.zzaep)));
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 3 */
    public final boolean zzex(String str) {
        Exception exc;
        long j;
        ? r19;
        String str2 = str;
        this.url = str2;
        String zzey = zzey(str);
        String str3 = "error";
        zzpb zzpb = new zzpb(this.zzeiz, null, this, this.zzeuo.zzetn, this.zzeuo.zzetp, true, null);
        if (this.zzeuo.zzets) {
            try {
                zzpb = new zzbep(this.mContext, zzpb, null, null);
            } catch (Exception e) {
                e = e;
                exc = e;
                String canonicalName = exc.getClass().getCanonicalName();
                String message = exc.getMessage();
                StringBuilder sb = new StringBuilder(1 + String.valueOf(canonicalName).length() + String.valueOf(message).length());
                sb.append(canonicalName);
                sb.append(":");
                sb.append(message);
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder(34 + String.valueOf(str).length() + String.valueOf(sb2).length());
                sb3.append("Failed to preload url ");
                sb3.append(str2);
                sb3.append(" Exception: ");
                sb3.append(sb2);
                zzaxz.zzeo(sb3.toString());
                zza(str2, zzey, str3, sb2);
                return false;
            }
        }
        try {
            zzpb.zza(new zzoz(Uri.parse(str)));
            zzbdz zzbdz = (zzbdz) this.zzewo.get();
            if (zzbdz != null) {
                zzbdz.zza(zzey, (zzbfk) this);
            }
            Clock zzlm = zzbv.zzlm();
            long currentTimeMillis = zzlm.currentTimeMillis();
            long longValue = ((Long) zzwu.zzpz().zzd(zzaan.zzcox)).longValue();
            long longValue2 = ((Long) zzwu.zzpz().zzd(zzaan.zzcow)).longValue();
            this.zzaep = ByteBuffer.allocate(this.zzeuo.zzetm);
            int i = 8192;
            byte[] bArr = new byte[8192];
            long j2 = currentTimeMillis;
            ? r15 = zzpb;
            while (true) {
                int read = r15.read(bArr, 0, Math.min(this.zzaep.remaining(), i));
                if (read == -1) {
                    break;
                }
                synchronized (this.zzexj) {
                    th = str3;
                    try {
                        if (!this.zzexf) {
                            r19 = r15;
                            this.zzaep.put(bArr, 0, read);
                        } else {
                            r19 = r15;
                        }
                    } catch (Exception e2) {
                        exc = e2;
                    } finally {
                        while (true) {
                            j = th;
                        }
                    }
                }
                if (this.zzaep.remaining() <= 0) {
                    zzabm();
                    break;
                } else if (this.zzexf) {
                    String str4 = "externalAbort";
                    int limit = this.zzaep.limit();
                    StringBuilder sb4 = new StringBuilder(35);
                    sb4.append("Precache abort at ");
                    sb4.append(limit);
                    sb4.append(" bytes");
                    throw new IOException(sb4.toString());
                } else {
                    th = zzlm.currentTimeMillis();
                    if (th - j2 >= longValue) {
                        zzabm();
                    }
                    if (th - currentTimeMillis > 1000 * longValue2) {
                        String str5 = "downloadTimeout";
                        StringBuilder sb5 = new StringBuilder(49);
                        sb5.append("Timeout exceeded. Limit: ");
                        sb5.append(longValue2);
                        sb5.append(" sec");
                        throw new IOException(sb5.toString());
                    }
                    str3 = th;
                    r15 = r19;
                    i = 8192;
                }
            }
            return true;
        } catch (Exception e3) {
            e = e3;
            String str6 = str3;
            exc = e;
            String canonicalName2 = exc.getClass().getCanonicalName();
            String message2 = exc.getMessage();
            StringBuilder sb6 = new StringBuilder(1 + String.valueOf(canonicalName2).length() + String.valueOf(message2).length());
            sb6.append(canonicalName2);
            sb6.append(":");
            sb6.append(message2);
            String sb22 = sb6.toString();
            StringBuilder sb32 = new StringBuilder(34 + String.valueOf(str).length() + String.valueOf(sb22).length());
            sb32.append("Failed to preload url ");
            sb32.append(str2);
            sb32.append(" Exception: ");
            sb32.append(sb22);
            zzaxz.zzeo(sb32.toString());
            zza(str2, zzey, str3, sb22);
            return false;
        }
    }

    public final void abort() {
        this.zzexf = true;
    }

    public final ByteBuffer getByteBuffer() {
        synchronized (this.zzexj) {
            if (this.zzaep != null && !this.zzexi) {
                this.zzaep.flip();
                this.zzexi = true;
            }
            this.zzexf = true;
        }
        return this.zzaep;
    }

    public final /* synthetic */ void zza(Object obj, zzoz zzoz) {
        zzov zzov = (zzov) obj;
        if (zzov instanceof zzpb) {
            this.zzexg.zza((zzpb) zzov);
        }
    }
}
