package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.util.MimeTypes;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.nio.ByteBuffer;

@TargetApi(16)
public final class zzqo extends zzjy {
    private static final int[] zzbis = {1920, 1600, 1440, 1280, 960, 854, 640, 540, 480};
    private int zzaak;
    private boolean zzadt;
    private final zzqs zzbit;
    private final zzqv zzbiu;
    private final long zzbiv;
    private final int zzbiw;
    private final boolean zzbix;
    private final long[] zzbiy;
    private zzfs[] zzbiz;
    private zzqq zzbja;
    private Surface zzbjb;
    private Surface zzbjc;
    private int zzbjd;
    private boolean zzbje;
    private long zzbjf;
    private long zzbjg;
    private int zzbjh;
    private int zzbji;
    private int zzbjj;
    private float zzbjk;
    private int zzbjl;
    private int zzbjm;
    private int zzbjn;
    private float zzbjo;
    private int zzbjp;
    private int zzbjq;
    private int zzbjr;
    private float zzbjs;
    zzqr zzbjt;
    private long zzbju;
    private int zzbjv;
    private final Context zzsp;

    public zzqo(Context context, zzka zzka, long j, Handler handler, zzqu zzqu, int i) {
        this(context, zzka, 0, null, false, handler, zzqu, -1);
    }

    private static boolean zzan(long j) {
        return j < -30000;
    }

    private zzqo(Context context, zzka zzka, long j, zzhu<Object> zzhu, boolean z, Handler handler, zzqu zzqu, int i) {
        boolean z2 = false;
        super(2, zzka, null, false);
        this.zzbiv = 0;
        this.zzbiw = -1;
        this.zzsp = context.getApplicationContext();
        this.zzbit = new zzqs(context);
        this.zzbiu = new zzqv(handler, zzqu);
        if (zzqe.SDK_INT <= 22 && "foster".equals(zzqe.DEVICE) && "NVIDIA".equals(zzqe.MANUFACTURER)) {
            z2 = true;
        }
        this.zzbix = z2;
        this.zzbiy = new long[10];
        this.zzbju = C.TIME_UNSET;
        this.zzbjf = C.TIME_UNSET;
        this.zzbjl = -1;
        this.zzbjm = -1;
        this.zzbjo = -1.0f;
        this.zzbjk = -1.0f;
        this.zzbjd = 1;
        zzhr();
    }

    /* access modifiers changed from: protected */
    public final int zza(zzka zzka, zzfs zzfs) throws zzke {
        boolean z;
        String str = zzfs.zzzj;
        int i = 0;
        if (!zzpt.zzac(str)) {
            return 0;
        }
        zzhp zzhp = zzfs.zzzm;
        if (zzhp != null) {
            z = false;
            for (int i2 = 0; i2 < zzhp.zzagr; i2++) {
                z |= zzhp.zzu(i2).zzags;
            }
        } else {
            z = false;
        }
        zzjx zzb = zzka.zzb(str, z);
        if (zzb == null) {
            return 1;
        }
        boolean zzu = zzb.zzu(zzfs.zzzg);
        if (zzu && zzfs.width > 0 && zzfs.height > 0) {
            if (zzqe.SDK_INT >= 21) {
                zzu = zzb.zza(zzfs.width, zzfs.height, (double) zzfs.zzzn);
            } else {
                zzu = zzfs.width * zzfs.height <= zzkc.zzer();
                if (!zzu) {
                    int i3 = zzfs.width;
                    int i4 = zzfs.height;
                    String str2 = zzqe.zzbic;
                    StringBuilder sb = new StringBuilder(56 + String.valueOf(str2).length());
                    sb.append("FalseCheck [legacyFrameSize, ");
                    sb.append(i3);
                    sb.append("x");
                    sb.append(i4);
                    sb.append("] [");
                    sb.append(str2);
                    sb.append("]");
                    Log.d("MediaCodecVideoRenderer", sb.toString());
                }
            }
        }
        int i5 = zzb.zzatq ? 8 : 4;
        if (zzb.zzadt) {
            i = 16;
        }
        return (zzu ? 3 : 2) | i5 | i;
    }

    /* access modifiers changed from: protected */
    public final void zzb(boolean z) throws zzff {
        super.zzb(z);
        this.zzaak = zzbn().zzaak;
        this.zzadt = this.zzaak != 0;
        this.zzbiu.zzc(this.zzavd);
        this.zzbit.enable();
    }

    /* access modifiers changed from: protected */
    public final void zza(zzfs[] zzfsArr, long j) throws zzff {
        this.zzbiz = zzfsArr;
        if (this.zzbju == C.TIME_UNSET) {
            this.zzbju = j;
        } else {
            if (this.zzbjv == this.zzbiy.length) {
                long j2 = this.zzbiy[this.zzbjv - 1];
                StringBuilder sb = new StringBuilder(65);
                sb.append("Too many stream changes, so dropping offset: ");
                sb.append(j2);
                Log.w("MediaCodecVideoRenderer", sb.toString());
            } else {
                this.zzbjv++;
            }
            this.zzbiy[this.zzbjv - 1] = j;
        }
        super.zza(zzfsArr, j);
    }

    /* access modifiers changed from: protected */
    public final void zza(long j, boolean z) throws zzff {
        super.zza(j, z);
        zzhp();
        this.zzbji = 0;
        if (this.zzbjv != 0) {
            this.zzbju = this.zzbiy[this.zzbjv - 1];
            this.zzbjv = 0;
        }
        if (z) {
            zzho();
        } else {
            this.zzbjf = C.TIME_UNSET;
        }
    }

    public final boolean isReady() {
        if (super.isReady() && (this.zzbje || ((this.zzbjc != null && this.zzbjb == this.zzbjc) || zzel() == null))) {
            this.zzbjf = C.TIME_UNSET;
            return true;
        } else if (this.zzbjf == C.TIME_UNSET) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.zzbjf) {
                return true;
            }
            this.zzbjf = C.TIME_UNSET;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void onStarted() {
        super.onStarted();
        this.zzbjh = 0;
        this.zzbjg = SystemClock.elapsedRealtime();
        this.zzbjf = C.TIME_UNSET;
    }

    /* access modifiers changed from: protected */
    public final void onStopped() {
        zzhu();
        super.onStopped();
    }

    /* access modifiers changed from: protected */
    public final void zzbm() {
        this.zzbjl = -1;
        this.zzbjm = -1;
        this.zzbjo = -1.0f;
        this.zzbjk = -1.0f;
        this.zzbju = C.TIME_UNSET;
        this.zzbjv = 0;
        zzhr();
        zzhp();
        this.zzbit.disable();
        this.zzbjt = null;
        this.zzadt = false;
        try {
            super.zzbm();
        } finally {
            this.zzavd.zzds();
            this.zzbiu.zzd(this.zzavd);
        }
    }

    public final void zza(int i, Object obj) throws zzff {
        if (i == 1) {
            Surface surface = (Surface) obj;
            if (surface == null) {
                if (this.zzbjc != null) {
                    surface = this.zzbjc;
                } else {
                    zzjx zzem = zzem();
                    if (zzem != null && zzl(zzem.zzatr)) {
                        this.zzbjc = zzqk.zzc(this.zzsp, zzem.zzatr);
                        surface = this.zzbjc;
                    }
                }
            }
            if (this.zzbjb != surface) {
                this.zzbjb = surface;
                int state = getState();
                if (state == 1 || state == 2) {
                    MediaCodec zzel = zzel();
                    if (zzqe.SDK_INT < 23 || zzel == null || surface == null) {
                        zzen();
                        zzek();
                    } else {
                        zzel.setOutputSurface(surface);
                    }
                }
                if (surface == null || surface == this.zzbjc) {
                    zzhr();
                    zzhp();
                } else {
                    zzht();
                    zzhp();
                    if (state == 2) {
                        zzho();
                        return;
                    }
                }
                return;
            }
            if (!(surface == null || surface == this.zzbjc)) {
                zzht();
                if (this.zzbje) {
                    this.zzbiu.zzb(this.zzbjb);
                }
            }
        } else if (i == 4) {
            this.zzbjd = ((Integer) obj).intValue();
            MediaCodec zzel2 = zzel();
            if (zzel2 != null) {
                zzel2.setVideoScalingMode(this.zzbjd);
            }
        } else {
            super.zza(i, obj);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzjx zzjx) {
        return this.zzbjb != null || zzl(zzjx.zzatr);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f7, code lost:
        r9 = null;
     */
    public final void zza(zzjx zzjx, MediaCodec mediaCodec, zzfs zzfs, MediaCrypto mediaCrypto) throws zzke {
        zzqq zzqq;
        Point point;
        zzjx zzjx2 = zzjx;
        MediaCodec mediaCodec2 = mediaCodec;
        zzfs zzfs2 = zzfs;
        zzfs[] zzfsArr = this.zzbiz;
        int i = zzfs2.width;
        int i2 = zzfs2.height;
        int zzj = zzj(zzfs);
        if (zzfsArr.length == 1) {
            zzqq = new zzqq(i, i2, zzj);
        } else {
            int i3 = i2;
            int i4 = zzj;
            boolean z = false;
            int i5 = i;
            for (zzfs zzfs3 : zzfsArr) {
                if (zza(zzjx2.zzatq, zzfs2, zzfs3)) {
                    z |= zzfs3.width == -1 || zzfs3.height == -1;
                    i5 = Math.max(i5, zzfs3.width);
                    int max = Math.max(i3, zzfs3.height);
                    i4 = Math.max(i4, zzj(zzfs3));
                    i3 = max;
                }
            }
            if (z) {
                StringBuilder sb = new StringBuilder(66);
                sb.append("Resolutions unknown. Codec max resolution: ");
                sb.append(i5);
                sb.append("x");
                sb.append(i3);
                Log.w("MediaCodecVideoRenderer", sb.toString());
                boolean z2 = zzfs2.height > zzfs2.width;
                int i6 = z2 ? zzfs2.height : zzfs2.width;
                int i7 = z2 ? zzfs2.width : zzfs2.height;
                float f = ((float) i7) / ((float) i6);
                int[] iArr = zzbis;
                int length = iArr.length;
                int i8 = 0;
                while (true) {
                    if (i8 >= length) {
                        break;
                    }
                    int i9 = iArr[i8];
                    int i10 = (int) (((float) i9) * f);
                    if (i9 <= i6 || i10 <= i7) {
                        break;
                    }
                    int i11 = i6;
                    int i12 = i7;
                    if (zzqe.SDK_INT >= 21) {
                        int i13 = z2 ? i10 : i9;
                        if (!z2) {
                            i9 = i10;
                        }
                        Point zzc = zzjx2.zzc(i13, i9);
                        Point point2 = zzc;
                        if (zzjx2.zza(zzc.x, zzc.y, (double) zzfs2.zzzn)) {
                            point = point2;
                            break;
                        }
                    } else {
                        int zzf = zzqe.zzf(i9, 16) << 4;
                        int zzf2 = zzqe.zzf(i10, 16) << 4;
                        if (zzf * zzf2 <= zzkc.zzer()) {
                            int i14 = z2 ? zzf2 : zzf;
                            if (z2) {
                                zzf2 = zzf;
                            }
                            point = new Point(i14, zzf2);
                        }
                    }
                    i8++;
                    i6 = i11;
                    i7 = i12;
                }
                if (point != null) {
                    i5 = Math.max(i5, point.x);
                    i3 = Math.max(i3, point.y);
                    i4 = Math.max(i4, zza(zzfs2.zzzj, i5, i3));
                    StringBuilder sb2 = new StringBuilder(57);
                    sb2.append("Codec max resolution adjusted to: ");
                    sb2.append(i5);
                    sb2.append("x");
                    sb2.append(i3);
                    Log.w("MediaCodecVideoRenderer", sb2.toString());
                }
            }
            zzqq = new zzqq(i5, i3, i4);
        }
        this.zzbja = zzqq;
        zzqq zzqq2 = this.zzbja;
        boolean z3 = this.zzbix;
        int i15 = this.zzaak;
        MediaFormat zzcf = zzfs.zzcf();
        zzcf.setInteger("max-width", zzqq2.width);
        zzcf.setInteger("max-height", zzqq2.height);
        if (zzqq2.zzbjw != -1) {
            zzcf.setInteger("max-input-size", zzqq2.zzbjw);
        }
        if (z3) {
            zzcf.setInteger("auto-frc", 0);
        }
        if (i15 != 0) {
            zzcf.setFeatureEnabled("tunneled-playback", true);
            zzcf.setInteger("audio-session-id", i15);
        }
        if (this.zzbjb == null) {
            zzpo.checkState(zzl(zzjx2.zzatr));
            if (this.zzbjc == null) {
                this.zzbjc = zzqk.zzc(this.zzsp, zzjx2.zzatr);
            }
            this.zzbjb = this.zzbjc;
        }
        mediaCodec2.configure(zzcf, this.zzbjb, null, 0);
        if (zzqe.SDK_INT >= 23 && this.zzadt) {
            this.zzbjt = new zzqr(this, mediaCodec2);
        }
    }

    /* access modifiers changed from: protected */
    public final void zzen() {
        try {
            super.zzen();
        } finally {
            if (this.zzbjc != null) {
                if (this.zzbjb == this.zzbjc) {
                    this.zzbjb = null;
                }
                this.zzbjc.release();
                this.zzbjc = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void zzc(String str, long j, long j2) {
        this.zzbiu.zzb(str, j, j2);
    }

    /* access modifiers changed from: protected */
    public final void zze(zzfs zzfs) throws zzff {
        super.zze(zzfs);
        this.zzbiu.zzd(zzfs);
        this.zzbjk = zzfs.zzzp == -1.0f ? 1.0f : zzfs.zzzp;
        this.zzbjj = zzk(zzfs);
    }

    /* access modifiers changed from: protected */
    public final void zza(zzho zzho) {
        if (zzqe.SDK_INT < 23 && this.zzadt) {
            zzhq();
        }
    }

    /* access modifiers changed from: protected */
    public final void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        int i;
        int i2;
        boolean z = mediaFormat.containsKey("crop-right") && mediaFormat.containsKey("crop-left") && mediaFormat.containsKey("crop-bottom") && mediaFormat.containsKey("crop-top");
        if (z) {
            i = (mediaFormat.getInteger("crop-right") - mediaFormat.getInteger("crop-left")) + 1;
        } else {
            i = mediaFormat.getInteger(SettingsJsonConstants.ICON_WIDTH_KEY);
        }
        this.zzbjl = i;
        if (z) {
            i2 = (mediaFormat.getInteger("crop-bottom") - mediaFormat.getInteger("crop-top")) + 1;
        } else {
            i2 = mediaFormat.getInteger(SettingsJsonConstants.ICON_HEIGHT_KEY);
        }
        this.zzbjm = i2;
        this.zzbjo = this.zzbjk;
        if (zzqe.SDK_INT < 21) {
            this.zzbjn = this.zzbjj;
        } else if (this.zzbjj == 90 || this.zzbjj == 270) {
            int i3 = this.zzbjl;
            this.zzbjl = this.zzbjm;
            this.zzbjm = i3;
            this.zzbjo = 1.0f / this.zzbjo;
        }
        mediaCodec.setVideoScalingMode(this.zzbjd);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(MediaCodec mediaCodec, boolean z, zzfs zzfs, zzfs zzfs2) {
        return zza(z, zzfs, zzfs2) && zzfs2.width <= this.zzbja.width && zzfs2.height <= this.zzbja.height && zzfs2.zzzk <= this.zzbja.zzbjw;
    }

    /* access modifiers changed from: protected */
    public final boolean zza(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z) {
        MediaCodec mediaCodec2 = mediaCodec;
        int i3 = i;
        long j4 = j3;
        while (this.zzbjv != 0 && j4 >= this.zzbiy[0]) {
            this.zzbju = this.zzbiy[0];
            this.zzbjv--;
            System.arraycopy(this.zzbiy, 1, this.zzbiy, 0, this.zzbjv);
        }
        long j5 = j4 - this.zzbju;
        if (z) {
            zza(mediaCodec2, i3, j5);
            return true;
        }
        long j6 = j4 - j;
        if (this.zzbjb == this.zzbjc) {
            if (!zzan(j6)) {
                return false;
            }
            zza(mediaCodec2, i3, j5);
            return true;
        } else if (!this.zzbje) {
            if (zzqe.SDK_INT >= 21) {
                zza(mediaCodec2, i3, j5, System.nanoTime());
            } else {
                zzb(mediaCodec2, i3, j5);
            }
            return true;
        } else if (getState() != 2) {
            return false;
        } else {
            long elapsedRealtime = j6 - ((SystemClock.elapsedRealtime() * 1000) - j2);
            long nanoTime = System.nanoTime();
            long zzh = this.zzbit.zzh(j4, nanoTime + (elapsedRealtime * 1000));
            long j7 = (zzh - nanoTime) / 1000;
            if (zzan(j7)) {
                zzqc.beginSection("dropVideoBuffer");
                mediaCodec2.releaseOutputBuffer(i3, false);
                zzqc.endSection();
                this.zzavd.zzagl++;
                this.zzbjh++;
                this.zzbji++;
                this.zzavd.zzagm = Math.max(this.zzbji, this.zzavd.zzagm);
                if (this.zzbjh == this.zzbiw) {
                    zzhu();
                }
                return true;
            }
            if (zzqe.SDK_INT >= 21) {
                if (j7 < 50000) {
                    zza(mediaCodec2, i3, j5, zzh);
                    return true;
                }
            } else if (j7 < DashMediaSource.DEFAULT_LIVE_PRESENTATION_DELAY_FIXED_MS) {
                if (j7 > 11000) {
                    try {
                        Thread.sleep((j7 - 10000) / 1000);
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                    }
                }
                zzb(mediaCodec2, i3, j5);
                return true;
            }
            return false;
        }
    }

    private final void zza(MediaCodec mediaCodec, int i, long j) {
        zzqc.beginSection("skipVideoBuffer");
        mediaCodec.releaseOutputBuffer(i, false);
        zzqc.endSection();
        this.zzavd.zzagk++;
    }

    private final void zzb(MediaCodec mediaCodec, int i, long j) {
        zzhs();
        zzqc.beginSection("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, true);
        zzqc.endSection();
        this.zzavd.zzagj++;
        this.zzbji = 0;
        zzhq();
    }

    @TargetApi(21)
    private final void zza(MediaCodec mediaCodec, int i, long j, long j2) {
        zzhs();
        zzqc.beginSection("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, j2);
        zzqc.endSection();
        this.zzavd.zzagj++;
        this.zzbji = 0;
        zzhq();
    }

    private final boolean zzl(boolean z) {
        return zzqe.SDK_INT >= 23 && !this.zzadt && (!z || zzqk.zzb(this.zzsp));
    }

    private final void zzho() {
        this.zzbjf = this.zzbiv > 0 ? SystemClock.elapsedRealtime() + this.zzbiv : C.TIME_UNSET;
    }

    private final void zzhp() {
        this.zzbje = false;
        if (zzqe.SDK_INT >= 23 && this.zzadt) {
            MediaCodec zzel = zzel();
            if (zzel != null) {
                this.zzbjt = new zzqr(this, zzel);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzhq() {
        if (!this.zzbje) {
            this.zzbje = true;
            this.zzbiu.zzb(this.zzbjb);
        }
    }

    private final void zzhr() {
        this.zzbjp = -1;
        this.zzbjq = -1;
        this.zzbjs = -1.0f;
        this.zzbjr = -1;
    }

    private final void zzhs() {
        if (this.zzbjp != this.zzbjl || this.zzbjq != this.zzbjm || this.zzbjr != this.zzbjn || this.zzbjs != this.zzbjo) {
            this.zzbiu.zzb(this.zzbjl, this.zzbjm, this.zzbjn, this.zzbjo);
            this.zzbjp = this.zzbjl;
            this.zzbjq = this.zzbjm;
            this.zzbjr = this.zzbjn;
            this.zzbjs = this.zzbjo;
        }
    }

    private final void zzht() {
        if (this.zzbjp != -1 || this.zzbjq != -1) {
            this.zzbiu.zzb(this.zzbjl, this.zzbjm, this.zzbjn, this.zzbjo);
        }
    }

    private final void zzhu() {
        if (this.zzbjh > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.zzbiu.zzi(this.zzbjh, elapsedRealtime - this.zzbjg);
            this.zzbjh = 0;
            this.zzbjg = elapsedRealtime;
        }
    }

    private static int zzj(zzfs zzfs) {
        if (zzfs.zzzk != -1) {
            return zzfs.zzzk;
        }
        return zza(zzfs.zzzj, zzfs.width, zzfs.height);
    }

    private static int zza(String str, int i, int i2) {
        char c;
        int i3;
        if (i == -1 || i2 == -1) {
            return -1;
        }
        int i4 = 4;
        switch (str.hashCode()) {
            case -1664118616:
                if (str.equals(MimeTypes.VIDEO_H263)) {
                    c = 0;
                    break;
                }
            case -1662541442:
                if (str.equals(MimeTypes.VIDEO_H265)) {
                    c = 4;
                    break;
                }
            case 1187890754:
                if (str.equals(MimeTypes.VIDEO_MP4V)) {
                    c = 1;
                    break;
                }
            case 1331836730:
                if (str.equals(MimeTypes.VIDEO_H264)) {
                    c = 2;
                    break;
                }
            case 1599127256:
                if (str.equals(MimeTypes.VIDEO_VP8)) {
                    c = 3;
                    break;
                }
            case 1599127257:
                if (str.equals(MimeTypes.VIDEO_VP9)) {
                    c = 5;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
                i3 = i * i2;
                break;
            case 2:
                if (!"BRAVIA 4K 2015".equals(zzqe.MODEL)) {
                    i3 = ((zzqe.zzf(i, 16) * zzqe.zzf(i2, 16)) << 4) << 4;
                    break;
                } else {
                    return -1;
                }
            case 3:
                i3 = i * i2;
                break;
            case 4:
            case 5:
                i3 = i * i2;
                break;
            default:
                return -1;
        }
        i4 = 2;
        return (i3 * 3) / (2 * i4);
    }

    private static boolean zza(boolean z, zzfs zzfs, zzfs zzfs2) {
        return zzfs.zzzj.equals(zzfs2.zzzj) && zzk(zzfs) == zzk(zzfs2) && (z || (zzfs.width == zzfs2.width && zzfs.height == zzfs2.height));
    }

    private static int zzk(zzfs zzfs) {
        if (zzfs.zzzo == -1) {
            return 0;
        }
        return zzfs.zzzo;
    }
}
