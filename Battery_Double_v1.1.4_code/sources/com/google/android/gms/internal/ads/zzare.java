package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.Wrappers;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.ParametersAreNonnullByDefault;

@zzark
@ParametersAreNonnullByDefault
public final class zzare implements zzari {
    private static final Object sLock = new Object();
    @VisibleForTesting
    private static zzari zzdvf;
    @VisibleForTesting
    private static zzari zzdvg;
    private final zzbbi zzbpt;
    private final Object zzdvh;
    private final Context zzdvi;
    private final WeakHashMap<Thread, Boolean> zzdvj;
    private final ExecutorService zzsq;

    public static zzari zzn(Context context) {
        synchronized (sLock) {
            if (zzdvf == null) {
                if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcoe)).booleanValue()) {
                    zzdvf = new zzare(context);
                } else {
                    zzdvf = new zzarj();
                }
            }
        }
        return zzdvf;
    }

    public static zzari zzc(Context context, zzbbi zzbbi) {
        synchronized (sLock) {
            if (zzdvg == null) {
                if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcoe)).booleanValue()) {
                    zzare zzare = new zzare(context, zzbbi);
                    Thread thread = Looper.getMainLooper().getThread();
                    if (thread != null) {
                        synchronized (zzare.zzdvh) {
                            zzare.zzdvj.put(thread, Boolean.valueOf(true));
                        }
                        thread.setUncaughtExceptionHandler(new zzarg(zzare, thread.getUncaughtExceptionHandler()));
                    }
                    Thread.setDefaultUncaughtExceptionHandler(new zzarf(zzare, Thread.getDefaultUncaughtExceptionHandler()));
                    zzdvg = zzare;
                } else {
                    zzdvg = new zzarj();
                }
            }
        }
        return zzdvg;
    }

    private zzare(Context context) {
        this(context, zzbbi.zzaav());
    }

    private zzare(Context context, zzbbi zzbbi) {
        this.zzdvh = new Object();
        this.zzdvj = new WeakHashMap<>();
        this.zzsq = Executors.newCachedThreadPool();
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        this.zzdvi = context;
        this.zzbpt = zzbbi;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003f, code lost:
        if (r3 == false) goto L_0x0043;
     */
    public final void zza(Thread thread, Throwable th) {
        StackTraceElement[] stackTrace;
        boolean z = true;
        if (th != null) {
            Throwable th2 = th;
            boolean z2 = false;
            boolean z3 = false;
            while (th2 != null) {
                boolean z4 = z3;
                boolean z5 = z2;
                for (StackTraceElement stackTraceElement : th2.getStackTrace()) {
                    if (zzbat.zzej(stackTraceElement.getClassName())) {
                        z5 = true;
                    }
                    if (getClass().getName().equals(stackTraceElement.getClassName())) {
                        z4 = true;
                    }
                }
                th2 = th2.getCause();
                z2 = z5;
                z3 = z4;
            }
            if (z2) {
            }
        }
        z = false;
        if (z) {
            zza(th, "", 1.0f);
        }
    }

    public final void zza(Throwable th, String str) {
        zza(th, str, 1.0f);
    }

    public final void zza(Throwable th, String str, float f) {
        if (zzbat.zzc(th) != null) {
            String name = th.getClass().getName();
            StringWriter stringWriter = new StringWriter();
            zzbpe.zza(th, new PrintWriter(stringWriter));
            String stringWriter2 = stringWriter.toString();
            int i = 0;
            int i2 = 1;
            boolean z = Math.random() < ((double) f);
            if (f > 0.0f) {
                i2 = (int) (1.0f / f);
            }
            if (z) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(zza(name, stringWriter2, str, i2).toString());
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                while (i < size) {
                    Object obj = arrayList2.get(i);
                    i++;
                    this.zzsq.submit(new zzarh(this, new zzbbh(), (String) obj));
                }
            }
        }
    }

    @VisibleForTesting
    private final Builder zza(String str, String str2, String str3, int i) {
        boolean z;
        try {
            z = Wrappers.packageManager(this.zzdvi).isCallerInstantApp();
        } catch (Throwable th) {
            zzbbd.zzb("Error fetching instant app info", th);
            z = false;
        }
        String str4 = "unknown";
        try {
            str4 = this.zzdvi.getPackageName();
        } catch (Throwable unused) {
            zzbbd.zzeo("Cannot obtain package name, proceeding.");
        }
        Builder appendQueryParameter = new Builder().scheme("https").path("//pagead2.googlesyndication.com/pagead/gen_204").appendQueryParameter("is_aia", Boolean.toString(z)).appendQueryParameter("id", "gmob-apps-report-exception").appendQueryParameter("os", VERSION.RELEASE).appendQueryParameter("api", String.valueOf(VERSION.SDK_INT));
        String str5 = "device";
        String str6 = Build.MANUFACTURER;
        String str7 = Build.MODEL;
        if (!str7.startsWith(str6)) {
            StringBuilder sb = new StringBuilder(1 + String.valueOf(str6).length() + String.valueOf(str7).length());
            sb.append(str6);
            sb.append(" ");
            sb.append(str7);
            str7 = sb.toString();
        }
        return appendQueryParameter.appendQueryParameter(str5, str7).appendQueryParameter("js", this.zzbpt.zzdp).appendQueryParameter("appid", str4).appendQueryParameter("exceptiontype", str).appendQueryParameter("stacktrace", str2).appendQueryParameter("eids", TextUtils.join(",", zzaan.zzqw())).appendQueryParameter("exceptionkey", str3).appendQueryParameter("cl", "221522000").appendQueryParameter("rc", "dev").appendQueryParameter("session_id", zzwu.zzqa()).appendQueryParameter(SettingsJsonConstants.ANALYTICS_SAMPLING_RATE_KEY, Integer.toString(i)).appendQueryParameter("pb_tm", String.valueOf(zzwu.zzpz().zzd(zzaan.zzcyf)));
    }
}
