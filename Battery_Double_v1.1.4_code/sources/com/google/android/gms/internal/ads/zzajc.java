package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

final class zzajc implements Runnable {
    private final /* synthetic */ zzajm zzdjh;
    private final /* synthetic */ zzaii zzdji;
    private final /* synthetic */ zzait zzdjj;

    zzajc(zzait zzait, zzajm zzajm, zzaii zzaii) {
        this.zzdjj = zzait;
        this.zzdjh = zzajm;
        this.zzdji = zzaii;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        return;
     */
    public final void run() {
        synchronized (this.zzdjj.mLock) {
            if (this.zzdjh.getStatus() != -1) {
                if (this.zzdjh.getStatus() != 1) {
                    this.zzdjh.reject();
                    Executor executor = zzbcg.zzepo;
                    zzaii zzaii = this.zzdji;
                    zzaii.getClass();
                    executor.execute(zzajd.zzb(zzaii));
                    zzaxz.v("Could not receive loaded message in a timely manner. Rejecting.");
                }
            }
        }
    }
}
