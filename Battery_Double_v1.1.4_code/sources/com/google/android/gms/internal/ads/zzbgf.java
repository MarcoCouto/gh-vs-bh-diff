package com.google.android.gms.internal.ads;

import android.webkit.ConsoleMessage.MessageLevel;

final /* synthetic */ class zzbgf {
    static final /* synthetic */ int[] zzexs = new int[MessageLevel.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
    static {
        zzexs[MessageLevel.ERROR.ordinal()] = 1;
        zzexs[MessageLevel.WARNING.ordinal()] = 2;
        zzexs[MessageLevel.LOG.ordinal()] = 3;
        zzexs[MessageLevel.TIP.ordinal()] = 4;
        zzexs[MessageLevel.DEBUG.ordinal()] = 5;
    }
}
