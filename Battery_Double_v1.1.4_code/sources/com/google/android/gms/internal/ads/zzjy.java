package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec.CryptoException;
import android.media.MediaCodec.CryptoInfo;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.exoplayer2.C;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
public abstract class zzjy extends zzfd {
    private static final byte[] zzatt = zzqe.zzan("0000016742C00BDA259000000168CE0F13200000016588840DCE7118A0002FBF1C31C3275D78");
    private zzfs zzaad;
    private ByteBuffer[] zzadk;
    private final zzka zzatu;
    private final zzhu<Object> zzatv;
    private final boolean zzatw;
    private final zzho zzatx;
    private final zzho zzaty;
    private final zzfu zzatz;
    private final List<Long> zzaua;
    private final BufferInfo zzaub;
    private zzhs<Object> zzauc;
    private zzhs<Object> zzaud;
    private MediaCodec zzaue;
    private zzjx zzauf;
    private boolean zzaug;
    private boolean zzauh;
    private boolean zzaui;
    private boolean zzauj;
    private boolean zzauk;
    private boolean zzaul;
    private boolean zzaum;
    private boolean zzaun;
    private boolean zzauo;
    private ByteBuffer[] zzaup;
    private long zzauq;
    private int zzaur;
    private int zzaus;
    private boolean zzaut;
    private boolean zzauu;
    private int zzauv;
    private int zzauw;
    private boolean zzaux;
    private boolean zzauy;
    private boolean zzauz;
    private boolean zzava;
    private boolean zzavb;
    private boolean zzavc;
    protected zzhn zzavd;

    public zzjy(int i, zzka zzka, zzhu<Object> zzhu, boolean z) {
        super(i);
        zzpo.checkState(zzqe.SDK_INT >= 16);
        this.zzatu = (zzka) zzpo.checkNotNull(zzka);
        this.zzatv = zzhu;
        this.zzatw = z;
        this.zzatx = new zzho(0);
        this.zzaty = new zzho(0);
        this.zzatz = new zzfu();
        this.zzaua = new ArrayList();
        this.zzaub = new BufferInfo();
        this.zzauv = 0;
        this.zzauw = 0;
    }

    /* access modifiers changed from: protected */
    public void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) throws zzff {
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
    }

    /* access modifiers changed from: protected */
    public void onStopped() {
    }

    /* access modifiers changed from: protected */
    public abstract int zza(zzka zzka, zzfs zzfs) throws zzke;

    /* access modifiers changed from: protected */
    public void zza(zzho zzho) {
    }

    /* access modifiers changed from: protected */
    public abstract void zza(zzjx zzjx, MediaCodec mediaCodec, zzfs zzfs, MediaCrypto mediaCrypto) throws zzke;

    /* access modifiers changed from: protected */
    public abstract boolean zza(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z) throws zzff;

    /* access modifiers changed from: protected */
    public boolean zza(MediaCodec mediaCodec, boolean z, zzfs zzfs, zzfs zzfs2) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzjx zzjx) {
        return true;
    }

    public final int zzbl() {
        return 4;
    }

    /* access modifiers changed from: protected */
    public void zzc(String str, long j, long j2) {
    }

    /* access modifiers changed from: protected */
    public void zzdj() throws zzff {
    }

    public final int zzb(zzfs zzfs) throws zzff {
        try {
            return zza(this.zzatu, zzfs);
        } catch (zzke e) {
            throw zzff.zza(e, getIndex());
        }
    }

    /* access modifiers changed from: protected */
    public zzjx zza(zzka zzka, zzfs zzfs, boolean z) throws zzke {
        return zzka.zzb(zzfs.zzzj, z);
    }

    /* access modifiers changed from: protected */
    public final void zzek() throws zzff {
        if (this.zzaue == null && this.zzaad != null) {
            this.zzauc = this.zzaud;
            String str = this.zzaad.zzzj;
            if (this.zzauc != null) {
                int state = this.zzauc.getState();
                if (state == 0) {
                    throw zzff.zza(this.zzauc.zzdv(), getIndex());
                } else if (state == 3 || state == 4) {
                    this.zzauc.zzdu();
                    throw new NoSuchMethodError();
                }
            } else {
                if (this.zzauf == null) {
                    try {
                        this.zzauf = zza(this.zzatu, this.zzaad, false);
                    } catch (zzke e) {
                        zza(new zzjz(this.zzaad, (Throwable) e, false, -49998));
                    }
                    if (this.zzauf == null) {
                        zza(new zzjz(this.zzaad, (Throwable) null, false, -49999));
                    }
                }
                if (zza(this.zzauf)) {
                    String str2 = this.zzauf.name;
                    this.zzaug = zzqe.SDK_INT < 21 && this.zzaad.zzzl.isEmpty() && "OMX.MTK.VIDEO.DECODER.AVC".equals(str2);
                    this.zzauh = zzqe.SDK_INT < 18 || (zzqe.SDK_INT == 18 && ("OMX.SEC.avc.dec".equals(str2) || "OMX.SEC.avc.dec.secure".equals(str2))) || (zzqe.SDK_INT == 19 && zzqe.MODEL.startsWith("SM-G800") && ("OMX.Exynos.avc.dec".equals(str2) || "OMX.Exynos.avc.dec.secure".equals(str2)));
                    this.zzaui = zzqe.SDK_INT < 24 && ("OMX.Nvidia.h264.decode".equals(str2) || "OMX.Nvidia.h264.decode.secure".equals(str2)) && ("flounder".equals(zzqe.DEVICE) || "flounder_lte".equals(zzqe.DEVICE) || "grouper".equals(zzqe.DEVICE) || "tilapia".equals(zzqe.DEVICE));
                    this.zzauj = zzqe.SDK_INT <= 17 && ("OMX.rk.video_decoder.avc".equals(str2) || "OMX.allwinner.video.decoder.avc".equals(str2));
                    this.zzauk = (zzqe.SDK_INT <= 23 && "OMX.google.vorbis.decoder".equals(str2)) || (zzqe.SDK_INT <= 19 && "hb2000".equals(zzqe.DEVICE) && ("OMX.amlogic.avc.decoder.awesome".equals(str2) || "OMX.amlogic.avc.decoder.awesome.secure".equals(str2)));
                    this.zzaul = zzqe.SDK_INT == 21 && "OMX.google.aac.decoder".equals(str2);
                    this.zzaum = zzqe.SDK_INT <= 18 && this.zzaad.zzzt == 1 && "OMX.MTK.AUDIO.DECODER.MP3".equals(str2);
                    try {
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        String str3 = "createCodec:";
                        String valueOf = String.valueOf(str2);
                        zzqc.beginSection(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                        this.zzaue = MediaCodec.createByCodecName(str2);
                        zzqc.endSection();
                        zzqc.beginSection("configureCodec");
                        zza(this.zzauf, this.zzaue, this.zzaad, (MediaCrypto) null);
                        zzqc.endSection();
                        zzqc.beginSection("startCodec");
                        this.zzaue.start();
                        zzqc.endSection();
                        long elapsedRealtime2 = SystemClock.elapsedRealtime();
                        zzc(str2, elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
                        this.zzaup = this.zzaue.getInputBuffers();
                        this.zzadk = this.zzaue.getOutputBuffers();
                    } catch (Exception e2) {
                        zza(new zzjz(this.zzaad, (Throwable) e2, false, str2));
                    }
                    this.zzauq = getState() == 2 ? SystemClock.elapsedRealtime() + 1000 : C.TIME_UNSET;
                    this.zzaur = -1;
                    this.zzaus = -1;
                    this.zzavc = true;
                    this.zzavd.zzagg++;
                }
            }
        }
    }

    private final void zza(zzjz zzjz) throws zzff {
        throw zzff.zza(zzjz, getIndex());
    }

    /* access modifiers changed from: protected */
    public final MediaCodec zzel() {
        return this.zzaue;
    }

    /* access modifiers changed from: protected */
    public final zzjx zzem() {
        return this.zzauf;
    }

    /* access modifiers changed from: protected */
    public void zzb(boolean z) throws zzff {
        this.zzavd = new zzhn();
    }

    /* access modifiers changed from: protected */
    public void zza(long j, boolean z) throws zzff {
        this.zzauz = false;
        this.zzava = false;
        if (this.zzaue != null) {
            this.zzauq = C.TIME_UNSET;
            this.zzaur = -1;
            this.zzaus = -1;
            this.zzavc = true;
            this.zzavb = false;
            this.zzaut = false;
            this.zzaua.clear();
            this.zzaun = false;
            this.zzauo = false;
            if (this.zzauh || (this.zzauk && this.zzauy)) {
                zzen();
                zzek();
            } else if (this.zzauw != 0) {
                zzen();
                zzek();
            } else {
                this.zzaue.flush();
                this.zzaux = false;
            }
            if (this.zzauu && this.zzaad != null) {
                this.zzauv = 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzbm() {
        this.zzaad = null;
        try {
            zzen();
            try {
                if (this.zzauc != null) {
                    this.zzatv.zza(this.zzauc);
                }
                try {
                    if (!(this.zzaud == null || this.zzaud == this.zzauc)) {
                        this.zzatv.zza(this.zzaud);
                    }
                } finally {
                    this.zzauc = null;
                    this.zzaud = null;
                }
            } catch (Throwable th) {
                if (!(this.zzaud == null || this.zzaud == this.zzauc)) {
                    this.zzatv.zza(this.zzaud);
                }
                throw th;
            } finally {
                this.zzauc = null;
                this.zzaud = null;
            }
        } catch (Throwable th2) {
            try {
                if (!(this.zzaud == null || this.zzaud == this.zzauc)) {
                    this.zzatv.zza(this.zzaud);
                }
                throw th2;
            } finally {
                this.zzauc = null;
                this.zzaud = null;
            }
        } finally {
            this.zzauc = null;
            this.zzaud = null;
        }
    }

    /* access modifiers changed from: protected */
    public void zzen() {
        this.zzauq = C.TIME_UNSET;
        this.zzaur = -1;
        this.zzaus = -1;
        this.zzavb = false;
        this.zzaut = false;
        this.zzaua.clear();
        this.zzaup = null;
        this.zzadk = null;
        this.zzauf = null;
        this.zzauu = false;
        this.zzaux = false;
        this.zzaug = false;
        this.zzauh = false;
        this.zzaui = false;
        this.zzauj = false;
        this.zzauk = false;
        this.zzaum = false;
        this.zzaun = false;
        this.zzauo = false;
        this.zzauy = false;
        this.zzauv = 0;
        this.zzauw = 0;
        this.zzatx.zzdd = null;
        if (this.zzaue != null) {
            this.zzavd.zzagh++;
            try {
                this.zzaue.stop();
                try {
                    this.zzaue.release();
                    this.zzaue = null;
                    if (this.zzauc != null && this.zzaud != this.zzauc) {
                        try {
                            this.zzatv.zza(this.zzauc);
                        } finally {
                            this.zzauc = null;
                        }
                    }
                } catch (Throwable th) {
                    this.zzaue = null;
                    if (!(this.zzauc == null || this.zzaud == this.zzauc)) {
                        this.zzatv.zza(this.zzauc);
                    }
                    throw th;
                } finally {
                    this.zzauc = null;
                }
            } catch (Throwable th2) {
                this.zzaue = null;
                if (!(this.zzauc == null || this.zzaud == this.zzauc)) {
                    try {
                        this.zzatv.zza(this.zzauc);
                    } finally {
                        this.zzauc = null;
                    }
                }
                throw th2;
            } finally {
                this.zzauc = null;
            }
        }
    }

    public final void zzb(long j, long j2) throws zzff {
        if (this.zzava) {
            zzdj();
            return;
        }
        if (this.zzaad == null) {
            this.zzaty.clear();
            int zza = zza(this.zzatz, this.zzaty, true);
            if (zza == -5) {
                zze(this.zzatz.zzaad);
            } else if (zza == -4) {
                zzpo.checkState(this.zzaty.zzdp());
                this.zzauz = true;
                zzep();
                return;
            } else {
                return;
            }
        }
        zzek();
        if (this.zzaue != null) {
            zzqc.beginSection("drainAndFeed");
            do {
            } while (zzd(j, j2));
            do {
            } while (zzeo());
            zzqc.endSection();
        } else {
            zze(j);
            this.zzaty.clear();
            int zza2 = zza(this.zzatz, this.zzaty, false);
            if (zza2 == -5) {
                zze(this.zzatz.zzaad);
            } else if (zza2 == -4) {
                zzpo.checkState(this.zzaty.zzdp());
                this.zzauz = true;
                zzep();
            }
        }
        this.zzavd.zzds();
    }

    /* JADX WARNING: Removed duplicated region for block: B:84:0x0153 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0154  */
    private final boolean zzeo() throws zzff {
        int i;
        int i2;
        boolean z;
        if (this.zzaue == null || this.zzauw == 2 || this.zzauz) {
            return false;
        }
        if (this.zzaur < 0) {
            this.zzaur = this.zzaue.dequeueInputBuffer(0);
            if (this.zzaur < 0) {
                return false;
            }
            this.zzatx.zzdd = this.zzaup[this.zzaur];
            this.zzatx.clear();
        }
        if (this.zzauw == 1) {
            if (!this.zzauj) {
                this.zzauy = true;
                this.zzaue.queueInputBuffer(this.zzaur, 0, 0, 0, 4);
                this.zzaur = -1;
            }
            this.zzauw = 2;
            return false;
        } else if (this.zzaun) {
            this.zzaun = false;
            this.zzatx.zzdd.put(zzatt);
            this.zzaue.queueInputBuffer(this.zzaur, 0, zzatt.length, 0, 0);
            this.zzaur = -1;
            this.zzaux = true;
            return true;
        } else {
            if (this.zzavb) {
                i2 = -4;
                i = 0;
            } else {
                if (this.zzauv == 1) {
                    for (int i3 = 0; i3 < this.zzaad.zzzl.size(); i3++) {
                        this.zzatx.zzdd.put((byte[]) this.zzaad.zzzl.get(i3));
                    }
                    this.zzauv = 2;
                }
                int position = this.zzatx.zzdd.position();
                i = position;
                i2 = zza(this.zzatz, this.zzatx, false);
            }
            if (i2 == -3) {
                return false;
            }
            if (i2 == -5) {
                if (this.zzauv == 2) {
                    this.zzatx.clear();
                    this.zzauv = 1;
                }
                zze(this.zzatz.zzaad);
                return true;
            } else if (this.zzatx.zzdp()) {
                if (this.zzauv == 2) {
                    this.zzatx.clear();
                    this.zzauv = 1;
                }
                this.zzauz = true;
                if (!this.zzaux) {
                    zzep();
                    return false;
                }
                try {
                    if (!this.zzauj) {
                        this.zzauy = true;
                        this.zzaue.queueInputBuffer(this.zzaur, 0, 0, 0, 4);
                        this.zzaur = -1;
                    }
                    return false;
                } catch (CryptoException e) {
                    throw zzff.zza(e, getIndex());
                }
            } else if (!this.zzavc || this.zzatx.zzdq()) {
                this.zzavc = false;
                boolean zzdt = this.zzatx.zzdt();
                if (this.zzauc != null) {
                    int state = this.zzauc.getState();
                    if (state == 0) {
                        throw zzff.zza(this.zzauc.zzdv(), getIndex());
                    } else if (state != 4 && (zzdt || !this.zzatw)) {
                        z = true;
                        this.zzavb = z;
                        if (!this.zzavb) {
                            return false;
                        }
                        if (this.zzaug && !zzdt) {
                            zzpu.zzk(this.zzatx.zzdd);
                            if (this.zzatx.zzdd.position() == 0) {
                                return true;
                            }
                            this.zzaug = false;
                        }
                        try {
                            long j = this.zzatx.zzago;
                            if (this.zzatx.zzdo()) {
                                this.zzaua.add(Long.valueOf(j));
                            }
                            this.zzatx.zzdd.flip();
                            zza(this.zzatx);
                            if (zzdt) {
                                CryptoInfo zzdr = this.zzatx.zzagn.zzdr();
                                if (i != 0) {
                                    if (zzdr.numBytesOfClearData == null) {
                                        zzdr.numBytesOfClearData = new int[1];
                                    }
                                    int[] iArr = zzdr.numBytesOfClearData;
                                    iArr[0] = iArr[0] + i;
                                }
                                this.zzaue.queueSecureInputBuffer(this.zzaur, 0, zzdr, j, 0);
                            } else {
                                this.zzaue.queueInputBuffer(this.zzaur, 0, this.zzatx.zzdd.limit(), j, 0);
                            }
                            this.zzaur = -1;
                            this.zzaux = true;
                            this.zzauv = 0;
                            this.zzavd.zzagi++;
                            return true;
                        } catch (CryptoException e2) {
                            throw zzff.zza(e2, getIndex());
                        }
                    }
                }
                z = false;
                this.zzavb = z;
                if (!this.zzavb) {
                }
            } else {
                this.zzatx.clear();
                if (this.zzauv == 2) {
                    this.zzauv = 1;
                }
                return true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zze(zzfs zzfs) throws zzff {
        Object obj;
        zzfs zzfs2 = this.zzaad;
        this.zzaad = zzfs;
        zzhp zzhp = this.zzaad.zzzm;
        if (zzfs2 == null) {
            obj = null;
        } else {
            obj = zzfs2.zzzm;
        }
        boolean zza = zzqe.zza((Object) zzhp, obj);
        boolean z = true;
        if (!zza) {
            if (this.zzaad.zzzm == null) {
                this.zzaud = null;
            } else if (this.zzatv == null) {
                throw zzff.zza(new IllegalStateException("Media requires a DrmSessionManager"), getIndex());
            } else {
                this.zzaud = this.zzatv.zza(Looper.myLooper(), this.zzaad.zzzm);
                if (this.zzaud == this.zzauc) {
                    this.zzatv.zza(this.zzaud);
                }
            }
        }
        if (this.zzaud == this.zzauc && this.zzaue != null && zza(this.zzaue, this.zzauf.zzatq, zzfs2, this.zzaad)) {
            this.zzauu = true;
            this.zzauv = 1;
            if (!(this.zzaui && this.zzaad.width == zzfs2.width && this.zzaad.height == zzfs2.height)) {
                z = false;
            }
            this.zzaun = z;
        } else if (this.zzaux) {
            this.zzauw = 1;
        } else {
            zzen();
            zzek();
        }
    }

    public boolean zzcj() {
        return this.zzava;
    }

    public boolean isReady() {
        return this.zzaad != null && !this.zzavb && (zzbo() || this.zzaus >= 0 || (this.zzauq != C.TIME_UNSET && SystemClock.elapsedRealtime() < this.zzauq));
    }

    private final boolean zzd(long j, long j2) throws zzff {
        boolean z;
        boolean z2;
        if (this.zzaus < 0) {
            if (!this.zzaul || !this.zzauy) {
                this.zzaus = this.zzaue.dequeueOutputBuffer(this.zzaub, 0);
            } else {
                try {
                    this.zzaus = this.zzaue.dequeueOutputBuffer(this.zzaub, 0);
                } catch (IllegalStateException unused) {
                    zzep();
                    if (this.zzava) {
                        zzen();
                    }
                    return false;
                }
            }
            if (this.zzaus >= 0) {
                if (this.zzauo) {
                    this.zzauo = false;
                    this.zzaue.releaseOutputBuffer(this.zzaus, false);
                    this.zzaus = -1;
                    return true;
                } else if ((this.zzaub.flags & 4) != 0) {
                    zzep();
                    this.zzaus = -1;
                    return false;
                } else {
                    ByteBuffer byteBuffer = this.zzadk[this.zzaus];
                    if (byteBuffer != null) {
                        byteBuffer.position(this.zzaub.offset);
                        byteBuffer.limit(this.zzaub.offset + this.zzaub.size);
                    }
                    long j3 = this.zzaub.presentationTimeUs;
                    int size = this.zzaua.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            z2 = false;
                            break;
                        } else if (((Long) this.zzaua.get(i)).longValue() == j3) {
                            this.zzaua.remove(i);
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                    this.zzaut = z2;
                }
            } else if (this.zzaus == -2) {
                MediaFormat outputFormat = this.zzaue.getOutputFormat();
                if (this.zzaui && outputFormat.getInteger(SettingsJsonConstants.ICON_WIDTH_KEY) == 32 && outputFormat.getInteger(SettingsJsonConstants.ICON_HEIGHT_KEY) == 32) {
                    this.zzauo = true;
                } else {
                    if (this.zzaum) {
                        outputFormat.setInteger("channel-count", 1);
                    }
                    onOutputFormatChanged(this.zzaue, outputFormat);
                }
                return true;
            } else if (this.zzaus == -3) {
                this.zzadk = this.zzaue.getOutputBuffers();
                return true;
            } else {
                if (this.zzauj && (this.zzauz || this.zzauw == 2)) {
                    zzep();
                }
                return false;
            }
        }
        if (!this.zzaul || !this.zzauy) {
            z = zza(j, j2, this.zzaue, this.zzadk[this.zzaus], this.zzaus, this.zzaub.flags, this.zzaub.presentationTimeUs, this.zzaut);
        } else {
            try {
                z = zza(j, j2, this.zzaue, this.zzadk[this.zzaus], this.zzaus, this.zzaub.flags, this.zzaub.presentationTimeUs, this.zzaut);
            } catch (IllegalStateException unused2) {
                zzep();
                if (this.zzava) {
                    zzen();
                }
                return false;
            }
        }
        if (!z) {
            return false;
        }
        long j4 = this.zzaub.presentationTimeUs;
        this.zzaus = -1;
        return true;
    }

    private final void zzep() throws zzff {
        if (this.zzauw == 2) {
            zzen();
            zzek();
            return;
        }
        this.zzava = true;
        zzdj();
    }
}
