package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd.AdChoicesInfo;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

@zzark
public final class zzads extends NativeAppInstallAd {
    private final VideoController zzcml = new VideoController();
    private final zzadp zzdds;
    private final List<Image> zzddt = new ArrayList();
    private final zzade zzddu;
    private final AdChoicesInfo zzddv;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004a A[Catch:{ RemoteException -> 0x0055 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007a A[Catch:{ RemoteException -> 0x0087 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0020 A[SYNTHETIC] */
    public zzads(zzadp zzadp) {
        zzade zzade;
        zzadb zzadb;
        this.zzdds = zzadp;
        AdChoicesInfo adChoicesInfo = null;
        try {
            List images = this.zzdds.getImages();
            if (images != null) {
                for (Object next : images) {
                    if (next instanceof IBinder) {
                        IBinder iBinder = (IBinder) next;
                        if (iBinder != null) {
                            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                            zzadb = queryLocalInterface instanceof zzadb ? (zzadb) queryLocalInterface : new zzadd(iBinder);
                            if (zzadb == null) {
                                this.zzddt.add(new zzade(zzadb));
                            }
                        }
                    }
                    zzadb = null;
                    if (zzadb == null) {
                    }
                }
            }
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
        }
        try {
            zzadb zzsb = this.zzdds.zzsb();
            if (zzsb != null) {
                zzade = new zzade(zzsb);
                this.zzddu = zzade;
                if (this.zzdds.zzse() != null) {
                    adChoicesInfo = new zzada(this.zzdds.zzse());
                }
                this.zzddv = adChoicesInfo;
            }
        } catch (RemoteException e2) {
            zzbbd.zzb("", e2);
        }
        zzade = null;
        this.zzddu = zzade;
        try {
            if (this.zzdds.zzse() != null) {
            }
        } catch (RemoteException e3) {
            zzbbd.zzb("", e3);
        }
        this.zzddv = adChoicesInfo;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzsc */
    public final IObjectWrapper zzhy() {
        try {
            return this.zzdds.zzsc();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final void performClick(Bundle bundle) {
        try {
            this.zzdds.performClick(bundle);
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
        }
    }

    public final boolean recordImpression(Bundle bundle) {
        try {
            return this.zzdds.recordImpression(bundle);
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return false;
        }
    }

    public final void reportTouchEvent(Bundle bundle) {
        try {
            this.zzdds.reportTouchEvent(bundle);
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
        }
    }

    public final CharSequence getHeadline() {
        try {
            return this.zzdds.getHeadline();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final List<Image> getImages() {
        return this.zzddt;
    }

    public final CharSequence getBody() {
        try {
            return this.zzdds.getBody();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final Image getIcon() {
        return this.zzddu;
    }

    public final CharSequence getCallToAction() {
        try {
            return this.zzdds.getCallToAction();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final Double getStarRating() {
        try {
            double starRating = this.zzdds.getStarRating();
            if (starRating == -1.0d) {
                return null;
            }
            return Double.valueOf(starRating);
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final CharSequence getStore() {
        try {
            return this.zzdds.getStore();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final CharSequence getPrice() {
        try {
            return this.zzdds.getPrice();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            if (this.zzdds.getVideoController() != null) {
                this.zzcml.zza(this.zzdds.getVideoController());
            }
        } catch (RemoteException e) {
            zzbbd.zzb("Exception occurred while getting video controller", e);
        }
        return this.zzcml;
    }

    public final AdChoicesInfo getAdChoicesInfo() {
        return this.zzddv;
    }

    public final CharSequence getMediationAdapterClassName() {
        try {
            return this.zzdds.getMediationAdapterClassName();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final Bundle getExtras() {
        try {
            return this.zzdds.getExtras();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            return null;
        }
    }

    public final void destroy() {
        try {
            this.zzdds.destroy();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
        }
    }
}
