package com.google.android.exoplayer2.source;

import android.net.Uri;
import android.os.Handler;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.Timeline.Period;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.MediaSource.Listener;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource.Factory;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;

public final class ExtractorMediaSource implements MediaSource, Listener {
    public static final int DEFAULT_MIN_LOADABLE_RETRY_COUNT_LIVE = 6;
    public static final int DEFAULT_MIN_LOADABLE_RETRY_COUNT_ON_DEMAND = 3;
    public static final int MIN_RETRY_COUNT_DEFAULT_FOR_MEDIA = -1;
    private final String customCacheKey;
    private final Factory dataSourceFactory;
    private final Handler eventHandler;
    private final EventListener eventListener;
    private final ExtractorsFactory extractorsFactory;
    private final int minLoadableRetryCount;
    private final Period period;
    private Listener sourceListener;
    private Timeline timeline;
    private boolean timelineHasDuration;
    private final Uri uri;

    public interface EventListener {
        void onLoadError(IOException iOException);
    }

    public void maybeThrowSourceInfoRefreshError() throws IOException {
    }

    public ExtractorMediaSource(Uri uri2, Factory factory, ExtractorsFactory extractorsFactory2, Handler handler, EventListener eventListener2) {
        this(uri2, factory, extractorsFactory2, -1, handler, eventListener2, null);
    }

    public ExtractorMediaSource(Uri uri2, Factory factory, ExtractorsFactory extractorsFactory2, Handler handler, EventListener eventListener2, String str) {
        this(uri2, factory, extractorsFactory2, -1, handler, eventListener2, str);
    }

    public ExtractorMediaSource(Uri uri2, Factory factory, ExtractorsFactory extractorsFactory2, int i, Handler handler, EventListener eventListener2, String str) {
        this.uri = uri2;
        this.dataSourceFactory = factory;
        this.extractorsFactory = extractorsFactory2;
        this.minLoadableRetryCount = i;
        this.eventHandler = handler;
        this.eventListener = eventListener2;
        this.customCacheKey = str;
        this.period = new Period();
    }

    public void prepareSource(ExoPlayer exoPlayer, boolean z, Listener listener) {
        this.sourceListener = listener;
        this.timeline = new SinglePeriodTimeline(C.TIME_UNSET, false);
        listener.onSourceInfoRefreshed(this.timeline, null);
    }

    public MediaPeriod createPeriod(int i, Allocator allocator, long j) {
        Assertions.checkArgument(i == 0);
        ExtractorMediaPeriod extractorMediaPeriod = new ExtractorMediaPeriod(this.uri, this.dataSourceFactory.createDataSource(), this.extractorsFactory.createExtractors(), this.minLoadableRetryCount, this.eventHandler, this.eventListener, this, allocator, this.customCacheKey);
        return extractorMediaPeriod;
    }

    public void releasePeriod(MediaPeriod mediaPeriod) {
        ((ExtractorMediaPeriod) mediaPeriod).release();
    }

    public void releaseSource() {
        this.sourceListener = null;
    }

    public void onSourceInfoRefreshed(Timeline timeline2, Object obj) {
        boolean z = false;
        if (timeline2.getPeriod(0, this.period).getDurationUs() != C.TIME_UNSET) {
            z = true;
        }
        if (!this.timelineHasDuration || z) {
            this.timeline = timeline2;
            this.timelineHasDuration = z;
            this.sourceListener.onSourceInfoRefreshed(this.timeline, null);
        }
    }
}
