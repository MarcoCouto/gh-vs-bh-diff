package com.facebook.ads;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.facebook.ads.internal.view.a.c;
import com.facebook.ads.internal.w.b.x;

public class NativeAdLayout extends FrameLayout {
    private View a;
    private int b = 0;
    private int c = 0;

    public NativeAdLayout(Context context) {
        super(context);
    }

    public NativeAdLayout(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeAdLayout(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void clearAdReportingLayout() {
        x.a((ViewGroup) this);
        removeView(this.a);
        this.a = null;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        super.onMeasure(i, i2);
        if (this.c > 0 && getMeasuredWidth() > this.c) {
            i3 = this.c;
        } else if (getMeasuredWidth() < this.b) {
            i3 = this.b;
        } else {
            return;
        }
        setMeasuredDimension(i3, getMeasuredHeight());
    }

    public void setAdReportingLayout(c cVar) {
        this.a = cVar;
        this.a.setLayoutParams(new LayoutParams(-1, -1));
        x.a((ViewGroup) this);
        addView(this.a);
    }

    public void setMaxWidth(int i) {
        this.c = i;
    }

    public void setMinWidth(int i) {
        this.b = i;
    }
}
