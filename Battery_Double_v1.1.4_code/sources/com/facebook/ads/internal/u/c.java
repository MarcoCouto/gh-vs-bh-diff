package com.facebook.ads.internal.u;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.ads.internal.k.d;
import com.facebook.ads.internal.k.e;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.h;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.v.a.m;
import com.facebook.ads.internal.w.b.i;
import com.facebook.ads.internal.w.b.n;
import com.facebook.ads.internal.w.b.u;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.File;
import java.security.MessageDigest;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONException;

public class c {
    /* access modifiers changed from: private */
    @Nullable
    public static a a;
    private static final n j = new n();
    private static final ThreadPoolExecutor k = ((ThreadPoolExecutor) Executors.newCachedThreadPool(j));
    /* access modifiers changed from: private */
    public final Context b;
    /* access modifiers changed from: private */
    public final d c = d.a();
    private final com.facebook.ads.internal.r.a d = com.facebook.ads.internal.r.a.af(this.b);
    /* access modifiers changed from: private */
    public Map<String, String> e;
    private b f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.v.a.a h;
    /* access modifiers changed from: private */
    public final String i;

    public interface a {
        C0010c a(c cVar, b bVar);

        void a(c cVar, Map<String, String> map);
    }

    public interface b {
        void a(com.facebook.ads.internal.protocol.a aVar);

        void a(f fVar);
    }

    /* renamed from: com.facebook.ads.internal.u.c$c reason: collision with other inner class name */
    public static class C0010c {
        @Nullable
        public final f a;
        @Nullable
        public final com.facebook.ads.internal.protocol.a b;
    }

    public c(Context context) {
        String str;
        this.b = context.getApplicationContext();
        String urlPrefix = AdInternalSettings.getUrlPrefix();
        if (TextUtils.isEmpty(urlPrefix)) {
            str = "https://graph.facebook.com/network_ads_common";
        } else {
            str = String.format(Locale.US, "https://graph.%s.facebook.com/network_ads_common", new Object[]{urlPrefix});
        }
        this.i = str;
    }

    /* access modifiers changed from: private */
    public void a(com.facebook.ads.internal.protocol.a aVar) {
        if (this.f != null) {
            this.f.a(aVar);
        }
        a();
    }

    private void a(f fVar) {
        if (this.f != null) {
            this.f.a(fVar);
        }
        a();
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        int i2;
        com.facebook.ads.internal.protocol.a aVar;
        Context context;
        Exception e2;
        String str2;
        int i3;
        Exception e3;
        try {
            e a2 = this.c.a(str);
            com.facebook.ads.internal.m.c a3 = a2.a();
            String str3 = null;
            if (a3 != null) {
                this.d.a(a3.b());
                if (AdInternalSettings.d) {
                    boolean z = true;
                    if (com.facebook.ads.internal.r.a.V(this.b)) {
                        context = this.b;
                        if (context == null) {
                            z = false;
                        }
                        if (z) {
                            try {
                                File file = new File(context.getFilesDir(), "com.facebook.ads.ipc");
                                if (!file.exists()) {
                                    z = file.createNewFile();
                                }
                            } catch (Exception e4) {
                                e2 = e4;
                            }
                        }
                        e2 = !z ? new Exception("Can't create ipc marker.") : null;
                        if (e2 != null) {
                            str2 = "ipc";
                            i3 = com.facebook.ads.internal.w.h.b.ac;
                        }
                    } else {
                        context = this.b;
                        if (context == null) {
                            z = false;
                        }
                        if (z) {
                            try {
                                File file2 = new File(context.getFilesDir(), "com.facebook.ads.ipc");
                                if (file2.exists()) {
                                    z = file2.delete();
                                }
                            } catch (Exception e5) {
                                e3 = e5;
                            }
                        }
                        e3 = !z ? new Exception("Can't delete ipc marker.") : null;
                        if (e2 != null) {
                            str2 = "ipc";
                            i3 = com.facebook.ads.internal.w.h.b.ac;
                        }
                    }
                    com.facebook.ads.internal.w.h.a.a(context, str2, i3, e2);
                }
                com.facebook.ads.internal.f.a.a(this.b, a3.c());
                a.a(a3.a().d(), this.g);
                com.facebook.ads.internal.w.g.a.a(this.b, k, a3);
            }
            switch (a2.b()) {
                case ADS:
                    if (com.facebook.ads.internal.r.a.z(this.b)) {
                        com.facebook.ads.internal.p.a.a(this.b, c());
                    }
                    f fVar = (f) a2;
                    if (a3 != null) {
                        if (a3.a().e()) {
                            a.a(str, this.g);
                        }
                        if (this.e != null) {
                            str3 = (String) this.e.get("CLIENT_REQUEST_ID");
                        }
                        String c2 = a2.c();
                        if (!TextUtils.isEmpty(c2) && !TextUtils.isEmpty(str3)) {
                            StringBuilder sb = new StringBuilder();
                            for (int i4 = 0; i4 < "73q8p304q6q511r89s8os2801s1o9sq1".length(); i4++) {
                                char charAt = "73q8p304q6q511r89s8os2801s1o9sq1".charAt(i4);
                                if ((charAt < 'a' || charAt > 'm') && (charAt < 'A' || charAt > 'M')) {
                                    if ((charAt >= 'n' && charAt <= 'z') || (charAt >= 'N' && charAt <= 'Z')) {
                                        i2 = charAt - 13;
                                    }
                                    sb.append(charAt);
                                } else {
                                    i2 = charAt + 13;
                                }
                                charAt = (char) i2;
                                sb.append(charAt);
                            }
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str3);
                            sb2.append(c2);
                            sb2.append(sb.toString());
                            byte[] bytes = sb2.toString().getBytes("iso-8859-1");
                            MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
                            instance.update(bytes, 0, bytes.length);
                            if (!a2.d().equals(i.a(instance.digest()))) {
                                com.facebook.ads.internal.w.h.a.b(this.b, "network", com.facebook.ads.internal.w.h.b.t, new h());
                            }
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(c2);
                            sb3.append(str3);
                            sb3.append(sb.toString());
                            byte[] bytes2 = sb3.toString().getBytes("iso-8859-1");
                            MessageDigest instance2 = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
                            instance2.update(bytes2, 0, bytes2.length);
                            e.a((d) new com.facebook.ads.internal.k.a(c2, i.a(instance2.digest())), this.b);
                        }
                        if (!TextUtils.isEmpty(a2.e()) && !TextUtils.isEmpty(str3)) {
                            new com.facebook.ads.internal.q.a(this.b, str3, a2.e()).a();
                        }
                    }
                    a(fVar);
                    return;
                case ERROR:
                    g gVar = (g) a2;
                    String f2 = gVar.f();
                    AdErrorType adErrorTypeFromCode = AdErrorType.adErrorTypeFromCode(gVar.g(), AdErrorType.ERROR_MESSAGE);
                    if (f2 != null) {
                        str = f2;
                    }
                    aVar = com.facebook.ads.internal.protocol.a.a(adErrorTypeFromCode, str);
                    break;
                default:
                    aVar = com.facebook.ads.internal.protocol.a.a(AdErrorType.UNKNOWN_RESPONSE, str);
                    break;
            }
            a(aVar);
        } catch (Exception e6) {
            a(com.facebook.ads.internal.protocol.a.a(AdErrorType.PARSER_FAILURE, e6.getMessage()));
        }
    }

    /* access modifiers changed from: private */
    public com.facebook.ads.internal.v.a.b c() {
        return new com.facebook.ads.internal.v.a.b() {
            /* access modifiers changed from: 0000 */
            public void a(m mVar) {
                a.b(c.this.g);
                c.this.h = null;
                try {
                    com.facebook.ads.internal.v.a.n a2 = mVar.a();
                    if (a2 != null) {
                        String e = a2.e();
                        e a3 = c.this.c.a(e);
                        if (a3.b() == a.ERROR) {
                            g gVar = (g) a3;
                            String f = gVar.f();
                            AdErrorType adErrorTypeFromCode = AdErrorType.adErrorTypeFromCode(gVar.g(), AdErrorType.ERROR_MESSAGE);
                            c cVar = c.this;
                            if (f != null) {
                                e = f;
                            }
                            cVar.a(com.facebook.ads.internal.protocol.a.a(adErrorTypeFromCode, e));
                            return;
                        }
                    }
                } catch (JSONException unused) {
                }
                c.this.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.NETWORK_ERROR, mVar.getMessage()));
            }

            public void a(com.facebook.ads.internal.v.a.n nVar) {
                if (nVar != null) {
                    String e = nVar.e();
                    a.b(c.this.g);
                    c.this.h = null;
                    c.this.a(e);
                }
            }

            public void a(Exception exc) {
                if (m.class.equals(exc.getClass())) {
                    a((m) exc);
                } else {
                    c.this.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.NETWORK_ERROR, exc.getMessage()));
                }
            }
        };
    }

    public void a() {
        if (this.h != null) {
            this.h.c(1);
            this.h.b(1);
            this.h = null;
        }
    }

    public void a(b bVar) {
        a(bVar, false);
    }

    public void a(final b bVar, final boolean z) {
        a();
        if (!z && a != null) {
            C0010c a2 = a.a(this, bVar);
            if (a2 != null) {
                if (a2.a != null) {
                    a(a2.a);
                    return;
                } else if (a2.b != null) {
                    a(a2.b);
                    return;
                }
            }
        }
        if (u.a(this.b) == com.facebook.ads.internal.w.b.u.a.NONE) {
            a(new com.facebook.ads.internal.protocol.a(AdErrorType.NETWORK_ERROR, "No network connection"));
            return;
        }
        this.g = bVar;
        com.facebook.ads.internal.l.a.a(this.b);
        if (a.a(bVar)) {
            String c2 = a.c(bVar);
            if (c2 != null) {
                a(c2);
            } else {
                a(com.facebook.ads.internal.protocol.a.a(AdErrorType.LOAD_TOO_FREQUENTLY, null));
            }
        } else {
            k.submit(new Runnable() {
                /* JADX WARNING: Can't wrap try/catch for region: R(10:8|(1:12)|13|14|15|16|(2:22|(1:24)(3:25|27|28))|26|27|28) */
                /* JADX WARNING: Code restructure failed: missing block: B:29:0x011a, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x011b, code lost:
                    com.facebook.ads.internal.u.c.a(r6.c, com.facebook.ads.internal.protocol.a.a(com.facebook.ads.internal.protocol.AdErrorType.AD_REQUEST_FAILED, r0.getMessage()));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:31:0x012a, code lost:
                    return;
                 */
                /* JADX WARNING: Failed to process nested try/catch */
                /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x00b7 */
                /* JADX WARNING: Removed duplicated region for block: B:24:0x00dd A[Catch:{ Exception -> 0x011a }] */
                /* JADX WARNING: Removed duplicated region for block: B:25:0x00de A[Catch:{ Exception -> 0x011a }] */
                public void run() {
                    boolean z;
                    com.facebook.ads.internal.g.b.a(c.this.b);
                    com.facebook.ads.internal.n.d.a(c.this.b);
                    if (bVar.f().a()) {
                        try {
                            bVar.f().a(com.facebook.ads.internal.g.b.b);
                        } catch (com.facebook.ads.internal.protocol.b e) {
                            c.this.a(com.facebook.ads.internal.protocol.a.a(e));
                        }
                        c.this.a(bVar.f().b());
                        return;
                    }
                    c.this.e = bVar.g();
                    if (z && c.a != null) {
                        c.a.a(c.this, c.this.e);
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(c.this.b.getPackageName());
                    sb.append(" ");
                    sb.append(c.this.b.getPackageManager().getInstallerPackageName(c.this.b.getPackageName()));
                    c.this.e.put("M_BANNER_KEY", new String(Base64.encode(sb.toString().getBytes(), 2)));
                    if (!(bVar.a() == com.facebook.ads.internal.protocol.e.NATIVE_250 || bVar.a() == com.facebook.ads.internal.protocol.e.NATIVE_UNKNOWN || bVar.a() == com.facebook.ads.internal.protocol.e.NATIVE_BANNER)) {
                        if (bVar.a() == null) {
                            z = false;
                            c.this.h = com.facebook.ads.internal.w.e.d.a(c.this.b, z);
                            c.this.h.b(c.this.i, c.this.h.a().a(c.this.e), c.this.c());
                        }
                    }
                    z = true;
                    c.this.h = com.facebook.ads.internal.w.e.d.a(c.this.b, z);
                    c.this.h.b(c.this.i, c.this.h.a().a(c.this.e), c.this.c());
                }
            });
        }
    }

    public void a(b bVar) {
        this.f = bVar;
    }
}
