package com.facebook.ads.internal.d;

import com.github.mikephil.charting.utils.Utils;
import java.io.Serializable;

public class c implements Serializable {
    private a a;
    private a b;

    public static class a implements Serializable {
        private double a;
        private double b;
        private double c;
        private double d;
        private double e;
        private double f;
        private double g;
        private int h;
        private double i;
        private double j;
        private double k;

        public a(double d2) {
            this.e = d2;
        }

        public void a() {
            this.a = Utils.DOUBLE_EPSILON;
            this.c = Utils.DOUBLE_EPSILON;
            this.d = Utils.DOUBLE_EPSILON;
            this.f = Utils.DOUBLE_EPSILON;
            this.h = 0;
            this.i = Utils.DOUBLE_EPSILON;
            this.j = 1.0d;
            this.k = Utils.DOUBLE_EPSILON;
        }

        public void a(double d2, double d3) {
            this.h++;
            this.i += d2;
            this.c = d3;
            this.k += d3 * d2;
            this.a = this.k / this.i;
            this.j = Math.min(this.j, d3);
            this.f = Math.max(this.f, d3);
            if (d3 >= this.e) {
                this.d += d2;
                this.b += d2;
                this.g = Math.max(this.g, this.b);
                return;
            }
            this.b = Utils.DOUBLE_EPSILON;
        }

        public void b() {
            this.b = Utils.DOUBLE_EPSILON;
        }

        public double c() {
            return this.h == 0 ? Utils.DOUBLE_EPSILON : this.j;
        }

        public double d() {
            return this.a;
        }

        public double e() {
            return this.f;
        }

        public double f() {
            return this.i;
        }

        public double g() {
            return this.d;
        }

        public double h() {
            return this.g;
        }
    }

    public c() {
        this(0.5d, 0.5d);
    }

    public c(double d) {
        this(d, 0.5d);
    }

    public c(double d, double d2) {
        this.a = new a(d);
        this.b = new a(d2);
        a();
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.a.a();
        this.b.a();
    }

    /* access modifiers changed from: 0000 */
    public void a(double d, double d2) {
        this.a.a(d, d2);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        this.a.b();
        this.b.b();
    }

    /* access modifiers changed from: 0000 */
    public void b(double d, double d2) {
        this.b.a(d, d2);
    }

    public a c() {
        return this.a;
    }

    public a d() {
        return this.b;
    }
}
