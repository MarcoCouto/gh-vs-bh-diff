package com.facebook.ads.internal.m;

import android.os.Build.VERSION;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.facebook.ads.internal.protocol.AdPlacementType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

public class d {
    private static final String c = "d";
    private static final AdPlacementType d = AdPlacementType.UNKNOWN;
    public int a = -1;
    public int b = -1;
    private final long e = System.currentTimeMillis();
    private AdPlacementType f = d;
    private int g = 1;
    private int h = 0;
    private int i = 0;
    private int j = 20;
    private int k = 0;
    private int l = 1000;
    private int m = 10000;
    private int n = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
    private int o = 3600;
    private boolean p = false;
    private List<b> q = null;

    private d(Map<String, String> map) {
        char c2;
        for (Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            switch (str.hashCode()) {
                case -1899431321:
                    if (str.equals("conv_tracking_data")) {
                        c2 = 12;
                        break;
                    }
                case -1561601017:
                    if (str.equals("refresh_threshold")) {
                        c2 = 4;
                        break;
                    }
                case -856794442:
                    if (str.equals("viewability_check_interval")) {
                        c2 = 10;
                        break;
                    }
                case -726276175:
                    if (str.equals("request_timeout")) {
                        c2 = 11;
                        break;
                    }
                case -634541425:
                    if (str.equals("invalidation_duration_in_seconds")) {
                        c2 = 5;
                        break;
                    }
                case -553208868:
                    if (str.equals("cacheable")) {
                        c2 = 6;
                        break;
                    }
                case 3575610:
                    if (str.equals("type")) {
                        c2 = 0;
                        break;
                    }
                case 700812481:
                    if (str.equals("min_viewability_percentage")) {
                        c2 = 1;
                        break;
                    }
                case 858630459:
                    if (str.equals("viewability_check_ticker")) {
                        c2 = 2;
                        break;
                    }
                case 986744879:
                    if (str.equals("video_time_polling_interval")) {
                        c2 = 13;
                        break;
                    }
                case 1085444827:
                    if (str.equals("refresh")) {
                        c2 = 3;
                        break;
                    }
                case 1183549815:
                    if (str.equals("viewability_check_initial_delay")) {
                        c2 = 9;
                        break;
                    }
                case 1503616961:
                    if (str.equals("placement_height")) {
                        c2 = 8;
                        break;
                    }
                case 2002133996:
                    if (str.equals("placement_width")) {
                        c2 = 7;
                        break;
                    }
                default:
                    c2 = 65535;
                    break;
            }
            switch (c2) {
                case 0:
                    this.f = AdPlacementType.fromString((String) entry.getValue());
                    break;
                case 1:
                    this.g = Integer.parseInt((String) entry.getValue());
                    break;
                case 2:
                    this.h = Integer.parseInt((String) entry.getValue());
                    break;
                case 3:
                    this.i = Integer.parseInt((String) entry.getValue());
                    break;
                case 4:
                    this.j = Integer.parseInt((String) entry.getValue());
                    break;
                case 5:
                    this.o = Integer.parseInt((String) entry.getValue());
                    break;
                case 6:
                    this.p = Boolean.valueOf((String) entry.getValue()).booleanValue();
                    break;
                case 7:
                    this.a = Integer.parseInt((String) entry.getValue());
                    break;
                case 8:
                    this.b = Integer.parseInt((String) entry.getValue());
                    break;
                case 9:
                    this.k = Integer.parseInt((String) entry.getValue());
                    break;
                case 10:
                    this.l = Integer.parseInt((String) entry.getValue());
                    break;
                case 11:
                    this.m = Integer.parseInt((String) entry.getValue());
                    break;
                case 12:
                    this.q = b.a((String) entry.getValue());
                    try {
                        CookieManager instance = CookieManager.getInstance();
                        boolean acceptCookie = instance.acceptCookie();
                        instance.setAcceptCookie(true);
                        for (b bVar : this.q) {
                            if (bVar.b()) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(bVar.b);
                                sb.append("=");
                                sb.append(bVar.c);
                                sb.append(";Domain=");
                                sb.append(bVar.a);
                                sb.append(";Expires=");
                                sb.append(bVar.a());
                                sb.append(";path=/");
                                instance.setCookie(bVar.a, sb.toString());
                            }
                        }
                        if (VERSION.SDK_INT < 21) {
                            CookieSyncManager.getInstance().startSync();
                        }
                        instance.setAcceptCookie(acceptCookie);
                        break;
                    } catch (Exception e2) {
                        Log.w(c, "Failed to set cookie.", e2);
                        break;
                    }
                case 13:
                    try {
                        this.n = Integer.parseInt((String) entry.getValue());
                        break;
                    } catch (NumberFormatException unused) {
                        this.n = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
                        break;
                    }
            }
        }
    }

    public static d a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        Iterator keys = jSONObject.keys();
        HashMap hashMap = new HashMap();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            hashMap.put(str, String.valueOf(jSONObject.opt(str)));
        }
        return new d(hashMap);
    }

    public long a() {
        return this.e;
    }

    public AdPlacementType b() {
        return this.f;
    }

    public long c() {
        return (long) (this.i * 1000);
    }

    public long d() {
        return (long) (this.j * 1000);
    }

    public boolean e() {
        return this.p;
    }

    public int f() {
        return this.g;
    }

    public int g() {
        return this.h;
    }

    public int h() {
        return this.k;
    }

    public int i() {
        return this.l;
    }

    public int j() {
        return this.m;
    }

    public int k() {
        return this.n;
    }

    public int l() {
        return this.o * 1000;
    }
}
