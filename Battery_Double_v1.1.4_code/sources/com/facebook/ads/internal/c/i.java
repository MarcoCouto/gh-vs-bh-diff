package com.facebook.ads.internal.c;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.util.Log;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.RewardData;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdExtendedListener;
import com.facebook.ads.S2SRewardedVideoAdListener;
import com.facebook.ads.internal.b.e;
import com.facebook.ads.internal.c.a.C0003a;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.w.b.v;
import com.facebook.ads.internal.w.h.a;
import com.facebook.ads.internal.w.h.b;

@UiThread
public class i extends b {
    private final j e;
    @Nullable
    private e f;

    public i(j jVar) {
        super(jVar.a);
        this.e = jVar;
    }

    private void h() {
        a(AdError.CACHE_ERROR_CODE, null);
        this.b.b();
        this.e.a(null);
    }

    /* access modifiers changed from: 0000 */
    public Message a() {
        Message obtain = Message.obtain(null, 2000);
        obtain.getData().putString("STR_PLACEMENT_KEY", this.e.b);
        obtain.getData().putString("STR_AD_ID_KEY", this.c);
        obtain.getData().putString("STR_BID_PAYLOAD_KEY", this.e.f);
        obtain.getData().putString("STR_EXTRA_HINTS_KEY", this.e.d);
        obtain.getData().putBoolean("BOOL_RV_FAIL_ON_CACHE_FAILURE_KEY", this.e.g);
        obtain.getData().putSerializable("SRL_RV_REWARD_DATA_KEY", this.e.e);
        obtain.getData().putBundle("BUNDLE_SETTINGS_KEY", AdInternalSettings.a);
        return obtain;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a2  */
    public void a(Message message) {
        String str;
        h hVar;
        RewardedVideoAd a = this.e.a();
        if (a == null) {
            a.b(this.a, "api", b.n, new Exception("Ad object is null"));
            return;
        }
        int i = message.what;
        if (i != 10) {
            if (i == 2100) {
                this.d.a(C0003a.LOADED);
                Bundle bundle = message.getData().getBundle("BUNDLE_EXTRAS_KEY");
                if (bundle != null) {
                    this.e.i = bundle.getLong("LONG_INVALIDATION_TIME_KEY");
                    this.e.h = bundle.getInt("INT_RV_VIDEO_DURATION_KEY");
                } else {
                    int i2 = b.m;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Missing bundle for message: ");
                    sb.append(message);
                    a.b(this.a, "api", i2, new Exception(sb.toString()));
                }
            } else if (i != 2103) {
                if (i != 2106) {
                    switch (i) {
                        case 2010:
                            hVar = this.b;
                            str = "Received load confirmation.";
                            break;
                        case 2011:
                            hVar = this.b;
                            str = "Received show confirmation.";
                            break;
                    }
                    hVar.a(str);
                    if (this.e.c == null) {
                        int i3 = message.what;
                        if (i3 != 2100) {
                            switch (i3) {
                                case 2104:
                                    this.e.c.onAdClicked(a);
                                    return;
                                case 2105:
                                    this.e.c.onLoggingImpression(a);
                                    return;
                                case 2106:
                                    if (this.e.c instanceof RewardedVideoAdExtendedListener) {
                                        ((RewardedVideoAdExtendedListener) this.e.c).onRewardedVideoActivityDestroyed();
                                    }
                                    return;
                                case 2107:
                                    this.e.c.onRewardedVideoCompleted();
                                    return;
                                case 2108:
                                    if (this.e.c instanceof S2SRewardedVideoAdListener) {
                                        ((S2SRewardedVideoAdListener) this.e.c).onRewardServerSuccess();
                                    }
                                    return;
                                case 2109:
                                    if (this.e.c instanceof S2SRewardedVideoAdListener) {
                                        ((S2SRewardedVideoAdListener) this.e.c).onRewardServerFailed();
                                    }
                                    return;
                                case 2110:
                                    this.e.c.onRewardedVideoClosed();
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            this.e.c.onAdLoaded(a);
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    this.d.a(C0003a.SHOWN);
                    if (this.b.b) {
                        h();
                    }
                }
            }
            this.e.a(null);
            if (this.e.c == null) {
            }
        }
        this.d.a(C0003a.ERROR);
        if (this.b.b) {
            h();
        }
        Bundle bundle2 = message.getData().getBundle("BUNDLE_EXTRAS_KEY");
        if (bundle2 != null) {
            int i4 = bundle2.getInt("INT_ERROR_CODE_KEY");
            String string = bundle2.getString("STR_ERROR_MESSAGE_KEY");
            if (this.e.c != null) {
                this.e.c.onError(a, new AdError(i4, string));
            } else {
                Log.e(AudienceNetworkAds.TAG, string);
            }
        } else {
            int i5 = b.m;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing bundle for message: ");
            sb2.append(message);
            a.b(this.a, "api", i5, new Exception(sb2.toString()));
        }
        this.e.a(null);
    }

    public void a(RewardData rewardData) {
        this.e.e = rewardData;
        if (this.b.b) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("SRL_RV_REWARD_DATA_KEY", rewardData);
            a(AdError.INTERNAL_ERROR_2003, bundle);
            return;
        }
        if (this.f != null) {
            this.f.a(rewardData);
        }
    }

    public void a(RewardedVideoAd rewardedVideoAd, String str, boolean z) {
        com.facebook.ads.internal.protocol.a a = e.a(this.a, Integer.valueOf(0), Integer.valueOf(1));
        if (a != null) {
            a(10, AdErrorType.MISSING_DEPENDENCIES_ERROR, a.b());
        } else if (!this.d.a(C0003a.LOADING, "load()")) {
            this.e.a(rewardedVideoAd);
            if (this.f != null) {
                this.f.a(str, z);
                return;
            }
            this.e.f = str;
            this.e.g = z;
            if (!a(this.e.a)) {
                c();
            } else if (this.b.b) {
                b();
            } else {
                this.b.c = true;
                this.b.a();
            }
        }
    }

    public boolean a(RewardedVideoAd rewardedVideoAd, int i) {
        if (this.d.a(C0003a.SHOWING, "show()")) {
            return false;
        }
        this.e.a(rewardedVideoAd);
        if (this.b.b) {
            Bundle bundle = new Bundle();
            bundle.putInt("INT_RV_APP_ORIENTATION_KEY", i);
            a(AdError.INTERNAL_ERROR_CODE, bundle);
            return true;
        } else if (this.f != null) {
            return this.f.a(i);
        } else {
            this.f = new e(this.e, this, this.c);
            this.f.a(i);
            return false;
        }
    }

    public void c() {
        this.f = new e(this.e, this, this.c);
        this.f.a(this.e.f, this.e.g);
    }

    public void d() {
        if (this.b.b) {
            h();
        }
        if (this.f != null) {
            this.f.a();
        }
        this.d.a(C0003a.DESTROYED);
    }

    public boolean f() {
        return this.f != null ? this.f.d() : this.d.a == C0003a.LOADED;
    }

    public boolean g() {
        return this.f != null ? this.f.c() : this.e.i > 0 && v.a() > this.e.i;
    }
}
