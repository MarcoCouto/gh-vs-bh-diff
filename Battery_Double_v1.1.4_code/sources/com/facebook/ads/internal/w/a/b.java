package com.facebook.ads.internal.w.a;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.facebook.ads.internal.w.h.a;
import java.lang.ref.WeakReference;

public class b implements ActivityLifecycleCallbacks {
    private static Context a;
    private static WeakReference<Activity> b = new WeakReference<>(null);

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0051 A[DONT_GENERATE] */
    @Nullable
    public static synchronized Activity a() {
        boolean z;
        synchronized (b.class) {
            Activity activity = (Activity) b.get();
            Activity activity2 = null;
            if (activity != null) {
                if (VERSION.SDK_INT >= 28) {
                    z = false;
                    if (z) {
                        activity2 = a.a();
                    }
                    if (!(a == null || !z || activity == activity2)) {
                        int i = com.facebook.ads.internal.w.h.b.Z;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Activity discrepancies res: ");
                        sb.append(activity);
                        sb.append(", ref: ");
                        sb.append(activity2);
                        a.b(a, "act_util", i, new Exception(sb.toString()));
                    }
                    return activity == null ? activity : activity2;
                }
            }
            z = true;
            if (z) {
            }
            int i2 = com.facebook.ads.internal.w.h.b.Z;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Activity discrepancies res: ");
            sb2.append(activity);
            sb2.append(", ref: ");
            sb2.append(activity2);
            a.b(a, "act_util", i2, new Exception(sb2.toString()));
            if (activity == null) {
            }
        }
    }

    public static synchronized void a(Context context) {
        synchronized (b.class) {
            a = context;
            if (a instanceof Application) {
                ((Application) a).registerActivityLifecycleCallbacks(new b());
            } else {
                a.b(a, "api", com.facebook.ads.internal.w.h.b.o, new Exception("AppContext is not Application."));
            }
        }
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        b = new WeakReference<>(null);
    }

    public void onActivityResumed(Activity activity) {
        b = new WeakReference<>(activity);
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }
}
