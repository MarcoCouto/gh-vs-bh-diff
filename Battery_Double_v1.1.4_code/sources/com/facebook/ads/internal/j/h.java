package com.facebook.ads.internal.j;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.UUID;

public class h extends g {
    public static final b a = new b(0, "token_id", "TEXT PRIMARY KEY");
    public static final b b = new b(1, "token", "TEXT");
    public static final b[] c = {a, b};
    private static final String d = "h";
    private static final String e = a("tokens", c);
    private static final String f;
    private static final String g;

    static {
        b[] bVarArr = c;
        b bVar = b;
        StringBuilder sb = new StringBuilder(g.a("tokens", bVarArr));
        sb.append(" WHERE ");
        sb.append(bVar.b);
        sb.append(" = ?");
        f = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("DELETE FROM tokens WHERE NOT EXISTS (SELECT 1 FROM events WHERE tokens.");
        sb2.append(a.b);
        sb2.append(" = ");
        sb2.append("events");
        sb2.append(".");
        sb2.append(c.b.b);
        sb2.append(")");
        g = sb2.toString();
    }

    public h(d dVar) {
        super(dVar);
    }

    public String a() {
        return "tokens";
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006c  */
    @WorkerThread
    public String a(String str) {
        Cursor cursor;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Invalid token.");
        }
        try {
            cursor = f().rawQuery(f, new String[]{str});
            try {
                String string = cursor.moveToNext() ? cursor.getString(a.a) : null;
                if (!TextUtils.isEmpty(string)) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return string;
                }
                String uuid = UUID.randomUUID().toString();
                ContentValues contentValues = new ContentValues(2);
                contentValues.put(a.b, uuid);
                contentValues.put(b.b, str);
                f().insertOrThrow("tokens", null, contentValues);
                if (cursor != null) {
                    cursor.close();
                }
                return uuid;
            } catch (Throwable th) {
                th = th;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public b[] b() {
        return c;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public Cursor c() {
        return f().rawQuery(e, null);
    }

    @WorkerThread
    public void d() {
        try {
            f().execSQL(g);
        } catch (SQLException unused) {
        }
    }
}
