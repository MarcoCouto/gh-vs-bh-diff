package io.realm;

import com.mansoon.BatteryDouble.models.data.NetworkStatistics;

public interface NetworkDetailsRealmProxyInterface {
    String realmGet$mcc();

    String realmGet$mnc();

    String realmGet$mobileDataActivity();

    String realmGet$mobileDataStatus();

    String realmGet$mobileNetworkType();

    String realmGet$networkOperator();

    NetworkStatistics realmGet$networkStatistics();

    String realmGet$networkType();

    int realmGet$roamingEnabled();

    String realmGet$simOperator();

    String realmGet$wifiApStatus();

    int realmGet$wifiLinkSpeed();

    int realmGet$wifiSignalStrength();

    String realmGet$wifiStatus();

    void realmSet$mcc(String str);

    void realmSet$mnc(String str);

    void realmSet$mobileDataActivity(String str);

    void realmSet$mobileDataStatus(String str);

    void realmSet$mobileNetworkType(String str);

    void realmSet$networkOperator(String str);

    void realmSet$networkStatistics(NetworkStatistics networkStatistics);

    void realmSet$networkType(String str);

    void realmSet$roamingEnabled(int i);

    void realmSet$simOperator(String str);

    void realmSet$wifiApStatus(String str);

    void realmSet$wifiLinkSpeed(int i);

    void realmSet$wifiSignalStrength(int i);

    void realmSet$wifiStatus(String str);
}
