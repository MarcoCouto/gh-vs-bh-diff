package io.realm;

public interface LocationProviderRealmProxyInterface {
    String realmGet$provider();

    void realmSet$provider(String str);
}
