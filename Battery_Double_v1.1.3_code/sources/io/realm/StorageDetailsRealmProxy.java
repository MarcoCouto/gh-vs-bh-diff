package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.StorageDetails;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class StorageDetailsRealmProxy extends StorageDetails implements RealmObjectProxy, StorageDetailsRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private StorageDetailsColumnInfo columnInfo;
    private ProxyState<StorageDetails> proxyState;

    static final class StorageDetailsColumnInfo extends ColumnInfo {
        long freeExternalIndex;
        long freeIndex;
        long freeSecondaryIndex;
        long freeSystemIndex;
        long totalExternalIndex;
        long totalIndex;
        long totalSecondaryIndex;
        long totalSystemIndex;

        StorageDetailsColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(8);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("StorageDetails");
            this.freeIndex = addColumnDetails("free", objectSchemaInfo);
            this.totalIndex = addColumnDetails("total", objectSchemaInfo);
            this.freeExternalIndex = addColumnDetails("freeExternal", objectSchemaInfo);
            this.totalExternalIndex = addColumnDetails("totalExternal", objectSchemaInfo);
            this.freeSystemIndex = addColumnDetails("freeSystem", objectSchemaInfo);
            this.totalSystemIndex = addColumnDetails("totalSystem", objectSchemaInfo);
            this.freeSecondaryIndex = addColumnDetails("freeSecondary", objectSchemaInfo);
            this.totalSecondaryIndex = addColumnDetails("totalSecondary", objectSchemaInfo);
        }

        StorageDetailsColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new StorageDetailsColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            StorageDetailsColumnInfo storageDetailsColumnInfo = (StorageDetailsColumnInfo) columnInfo;
            StorageDetailsColumnInfo storageDetailsColumnInfo2 = (StorageDetailsColumnInfo) columnInfo2;
            storageDetailsColumnInfo2.freeIndex = storageDetailsColumnInfo.freeIndex;
            storageDetailsColumnInfo2.totalIndex = storageDetailsColumnInfo.totalIndex;
            storageDetailsColumnInfo2.freeExternalIndex = storageDetailsColumnInfo.freeExternalIndex;
            storageDetailsColumnInfo2.totalExternalIndex = storageDetailsColumnInfo.totalExternalIndex;
            storageDetailsColumnInfo2.freeSystemIndex = storageDetailsColumnInfo.freeSystemIndex;
            storageDetailsColumnInfo2.totalSystemIndex = storageDetailsColumnInfo.totalSystemIndex;
            storageDetailsColumnInfo2.freeSecondaryIndex = storageDetailsColumnInfo.freeSecondaryIndex;
            storageDetailsColumnInfo2.totalSecondaryIndex = storageDetailsColumnInfo.totalSecondaryIndex;
        }
    }

    public static String getTableName() {
        return "class_StorageDetails";
    }

    static {
        ArrayList arrayList = new ArrayList(8);
        arrayList.add("free");
        arrayList.add("total");
        arrayList.add("freeExternal");
        arrayList.add("totalExternal");
        arrayList.add("freeSystem");
        arrayList.add("totalSystem");
        arrayList.add("freeSecondary");
        arrayList.add("totalSecondary");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    StorageDetailsRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (StorageDetailsColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$free() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.freeIndex);
    }

    public void realmSet$free(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.freeIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.freeIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$total() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalIndex);
    }

    public void realmSet$total(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$freeExternal() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.freeExternalIndex);
    }

    public void realmSet$freeExternal(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.freeExternalIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.freeExternalIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$totalExternal() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalExternalIndex);
    }

    public void realmSet$totalExternal(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalExternalIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalExternalIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$freeSystem() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.freeSystemIndex);
    }

    public void realmSet$freeSystem(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.freeSystemIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.freeSystemIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$totalSystem() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalSystemIndex);
    }

    public void realmSet$totalSystem(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalSystemIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalSystemIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$freeSecondary() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.freeSecondaryIndex);
    }

    public void realmSet$freeSecondary(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.freeSecondaryIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.freeSecondaryIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$totalSecondary() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.totalSecondaryIndex);
    }

    public void realmSet$totalSecondary(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.totalSecondaryIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.totalSecondaryIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("StorageDetails", 8, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("free", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("total", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("freeExternal", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalExternal", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("freeSystem", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalSystem", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("freeSecondary", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("totalSecondary", RealmFieldType.INTEGER, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static StorageDetailsColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new StorageDetailsColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static StorageDetails createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        StorageDetails storageDetails = (StorageDetails) realm.createObjectInternal(StorageDetails.class, true, Collections.emptyList());
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails;
        if (jSONObject.has("free")) {
            if (jSONObject.isNull("free")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'free' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$free(jSONObject.getInt("free"));
        }
        if (jSONObject.has("total")) {
            if (jSONObject.isNull("total")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'total' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$total(jSONObject.getInt("total"));
        }
        if (jSONObject.has("freeExternal")) {
            if (jSONObject.isNull("freeExternal")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'freeExternal' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$freeExternal(jSONObject.getInt("freeExternal"));
        }
        if (jSONObject.has("totalExternal")) {
            if (jSONObject.isNull("totalExternal")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalExternal' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$totalExternal(jSONObject.getInt("totalExternal"));
        }
        if (jSONObject.has("freeSystem")) {
            if (jSONObject.isNull("freeSystem")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'freeSystem' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$freeSystem(jSONObject.getInt("freeSystem"));
        }
        if (jSONObject.has("totalSystem")) {
            if (jSONObject.isNull("totalSystem")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalSystem' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$totalSystem(jSONObject.getInt("totalSystem"));
        }
        if (jSONObject.has("freeSecondary")) {
            if (jSONObject.isNull("freeSecondary")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'freeSecondary' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$freeSecondary(jSONObject.getInt("freeSecondary"));
        }
        if (jSONObject.has("totalSecondary")) {
            if (jSONObject.isNull("totalSecondary")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalSecondary' to null.");
            }
            storageDetailsRealmProxyInterface.realmSet$totalSecondary(jSONObject.getInt("totalSecondary"));
        }
        return storageDetails;
    }

    @TargetApi(11)
    public static StorageDetails createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        StorageDetails storageDetails = new StorageDetails();
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("free")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$free(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'free' to null.");
                }
            } else if (nextName.equals("total")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$total(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'total' to null.");
                }
            } else if (nextName.equals("freeExternal")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$freeExternal(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'freeExternal' to null.");
                }
            } else if (nextName.equals("totalExternal")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$totalExternal(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'totalExternal' to null.");
                }
            } else if (nextName.equals("freeSystem")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$freeSystem(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'freeSystem' to null.");
                }
            } else if (nextName.equals("totalSystem")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$totalSystem(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'totalSystem' to null.");
                }
            } else if (nextName.equals("freeSecondary")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    storageDetailsRealmProxyInterface.realmSet$freeSecondary(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'freeSecondary' to null.");
                }
            } else if (!nextName.equals("totalSecondary")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                storageDetailsRealmProxyInterface.realmSet$totalSecondary(jsonReader.nextInt());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalSecondary' to null.");
            }
        }
        jsonReader.endObject();
        return (StorageDetails) realm.copyToRealm(storageDetails);
    }

    public static StorageDetails copyOrUpdate(Realm realm, StorageDetails storageDetails, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (storageDetails instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) storageDetails;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return storageDetails;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(storageDetails);
        if (realmObjectProxy2 != null) {
            return (StorageDetails) realmObjectProxy2;
        }
        return copy(realm, storageDetails, z, map);
    }

    public static StorageDetails copy(Realm realm, StorageDetails storageDetails, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(storageDetails);
        if (realmObjectProxy != null) {
            return (StorageDetails) realmObjectProxy;
        }
        StorageDetails storageDetails2 = (StorageDetails) realm.createObjectInternal(StorageDetails.class, false, Collections.emptyList());
        map.put(storageDetails, (RealmObjectProxy) storageDetails2);
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails;
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface2 = storageDetails2;
        storageDetailsRealmProxyInterface2.realmSet$free(storageDetailsRealmProxyInterface.realmGet$free());
        storageDetailsRealmProxyInterface2.realmSet$total(storageDetailsRealmProxyInterface.realmGet$total());
        storageDetailsRealmProxyInterface2.realmSet$freeExternal(storageDetailsRealmProxyInterface.realmGet$freeExternal());
        storageDetailsRealmProxyInterface2.realmSet$totalExternal(storageDetailsRealmProxyInterface.realmGet$totalExternal());
        storageDetailsRealmProxyInterface2.realmSet$freeSystem(storageDetailsRealmProxyInterface.realmGet$freeSystem());
        storageDetailsRealmProxyInterface2.realmSet$totalSystem(storageDetailsRealmProxyInterface.realmGet$totalSystem());
        storageDetailsRealmProxyInterface2.realmSet$freeSecondary(storageDetailsRealmProxyInterface.realmGet$freeSecondary());
        storageDetailsRealmProxyInterface2.realmSet$totalSecondary(storageDetailsRealmProxyInterface.realmGet$totalSecondary());
        return storageDetails2;
    }

    public static long insert(Realm realm, StorageDetails storageDetails, Map<RealmModel, Long> map) {
        StorageDetails storageDetails2 = storageDetails;
        if (storageDetails2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) storageDetails2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(StorageDetails.class);
        long nativePtr = table.getNativePtr();
        StorageDetailsColumnInfo storageDetailsColumnInfo = (StorageDetailsColumnInfo) realm.getSchema().getColumnInfo(StorageDetails.class);
        long createRow = OsObject.createRow(table);
        map.put(storageDetails2, Long.valueOf(createRow));
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$free(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$total(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeExternal(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalExternal(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSystem(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSystem(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSecondary(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSecondary(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(StorageDetails.class);
        long nativePtr = table.getNativePtr();
        StorageDetailsColumnInfo storageDetailsColumnInfo = (StorageDetailsColumnInfo) realm.getSchema().getColumnInfo(StorageDetails.class);
        while (it.hasNext()) {
            StorageDetails storageDetails = (StorageDetails) it.next();
            if (!map2.containsKey(storageDetails)) {
                if (storageDetails instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) storageDetails;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(storageDetails, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(storageDetails, Long.valueOf(createRow));
                StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails;
                long j = createRow;
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeIndex, createRow, (long) storageDetailsRealmProxyInterface.realmGet$free(), false);
                long j2 = j;
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$total(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeExternal(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalExternal(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSystem(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSystem(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSecondary(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSecondary(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, StorageDetails storageDetails, Map<RealmModel, Long> map) {
        StorageDetails storageDetails2 = storageDetails;
        if (storageDetails2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) storageDetails2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(StorageDetails.class);
        long nativePtr = table.getNativePtr();
        StorageDetailsColumnInfo storageDetailsColumnInfo = (StorageDetailsColumnInfo) realm.getSchema().getColumnInfo(StorageDetails.class);
        long createRow = OsObject.createRow(table);
        map.put(storageDetails2, Long.valueOf(createRow));
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$free(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$total(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeExternal(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalExternal(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSystem(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSystem(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.freeSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSecondary(), false);
        Table.nativeSetLong(j, storageDetailsColumnInfo.totalSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSecondary(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(StorageDetails.class);
        long nativePtr = table.getNativePtr();
        StorageDetailsColumnInfo storageDetailsColumnInfo = (StorageDetailsColumnInfo) realm.getSchema().getColumnInfo(StorageDetails.class);
        while (it.hasNext()) {
            StorageDetails storageDetails = (StorageDetails) it.next();
            if (!map2.containsKey(storageDetails)) {
                if (storageDetails instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) storageDetails;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(storageDetails, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(storageDetails, Long.valueOf(createRow));
                StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails;
                long j = createRow;
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeIndex, createRow, (long) storageDetailsRealmProxyInterface.realmGet$free(), false);
                long j2 = j;
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$total(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeExternal(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalExternalIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalExternal(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSystem(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalSystemIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSystem(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.freeSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$freeSecondary(), false);
                Table.nativeSetLong(nativePtr, storageDetailsColumnInfo.totalSecondaryIndex, j2, (long) storageDetailsRealmProxyInterface.realmGet$totalSecondary(), false);
            }
        }
    }

    public static StorageDetails createDetachedCopy(StorageDetails storageDetails, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        StorageDetails storageDetails2;
        if (i > i2 || storageDetails == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(storageDetails);
        if (cacheData == null) {
            storageDetails2 = new StorageDetails();
            map.put(storageDetails, new CacheData(i, storageDetails2));
        } else if (i >= cacheData.minDepth) {
            return (StorageDetails) cacheData.object;
        } else {
            StorageDetails storageDetails3 = (StorageDetails) cacheData.object;
            cacheData.minDepth = i;
            storageDetails2 = storageDetails3;
        }
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface = storageDetails2;
        StorageDetailsRealmProxyInterface storageDetailsRealmProxyInterface2 = storageDetails;
        storageDetailsRealmProxyInterface.realmSet$free(storageDetailsRealmProxyInterface2.realmGet$free());
        storageDetailsRealmProxyInterface.realmSet$total(storageDetailsRealmProxyInterface2.realmGet$total());
        storageDetailsRealmProxyInterface.realmSet$freeExternal(storageDetailsRealmProxyInterface2.realmGet$freeExternal());
        storageDetailsRealmProxyInterface.realmSet$totalExternal(storageDetailsRealmProxyInterface2.realmGet$totalExternal());
        storageDetailsRealmProxyInterface.realmSet$freeSystem(storageDetailsRealmProxyInterface2.realmGet$freeSystem());
        storageDetailsRealmProxyInterface.realmSet$totalSystem(storageDetailsRealmProxyInterface2.realmGet$totalSystem());
        storageDetailsRealmProxyInterface.realmSet$freeSecondary(storageDetailsRealmProxyInterface2.realmGet$freeSecondary());
        storageDetailsRealmProxyInterface.realmSet$totalSecondary(storageDetailsRealmProxyInterface2.realmGet$totalSecondary());
        return storageDetails2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("StorageDetails = proxy[");
        sb.append("{free:");
        sb.append(realmGet$free());
        sb.append("}");
        sb.append(",");
        sb.append("{total:");
        sb.append(realmGet$total());
        sb.append("}");
        sb.append(",");
        sb.append("{freeExternal:");
        sb.append(realmGet$freeExternal());
        sb.append("}");
        sb.append(",");
        sb.append("{totalExternal:");
        sb.append(realmGet$totalExternal());
        sb.append("}");
        sb.append(",");
        sb.append("{freeSystem:");
        sb.append(realmGet$freeSystem());
        sb.append("}");
        sb.append(",");
        sb.append("{totalSystem:");
        sb.append(realmGet$totalSystem());
        sb.append("}");
        sb.append(",");
        sb.append("{freeSecondary:");
        sb.append(realmGet$freeSecondary());
        sb.append("}");
        sb.append(",");
        sb.append("{totalSecondary:");
        sb.append(realmGet$totalSecondary());
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        StorageDetailsRealmProxy storageDetailsRealmProxy = (StorageDetailsRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = storageDetailsRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = storageDetailsRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == storageDetailsRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
