package com.mansoon.BatteryDouble.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.Notifier;
import com.mansoon.BatteryDouble.util.SettingsUtils;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = LogUtils.makeLogTag(BootReceiver.class);

    public void onReceive(Context context, Intent intent) {
        LogUtils.LOGI(TAG, "BOOT_COMPLETED onReceive()");
        if (SettingsUtils.isTosAccepted(context) && SettingsUtils.isPowerIndicatorShown(context)) {
            Notifier.startStatusBar(context);
        }
    }
}
