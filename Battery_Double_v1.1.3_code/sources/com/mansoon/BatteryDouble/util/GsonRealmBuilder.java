package com.mansoon.BatteryDouble.util;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.realm.RealmObject;

public class GsonRealmBuilder {
    private static final String TAG = "GsonRealmBuilder";

    private GsonRealmBuilder() {
    }

    private static GsonBuilder getBuilder() {
        return new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            public boolean shouldSkipClass(Class<?> cls) {
                return false;
            }

            public boolean shouldSkipField(FieldAttributes fieldAttributes) {
                return fieldAttributes.getDeclaringClass().equals(RealmObject.class);
            }
        });
    }

    public static Gson get() {
        return getBuilder().create();
    }
}
