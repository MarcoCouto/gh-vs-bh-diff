package com.mansoon.BatteryDouble.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;

public abstract class BaseActivity extends AppCompatActivity implements OnSharedPreferenceChangeListener {
    private static final String TAG = LogUtils.makeLogTag(BaseActivity.class);
    private Toolbar mActionBarToolbar;

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        SettingsUtils.markTosAccepted(this, true);
        if (WelcomeActivity.shouldDisplay(this)) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setContentView(int i) {
        super.setContentView(i);
        getActionBarToolbar();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public Toolbar getActionBarToolbar() {
        if (this.mActionBarToolbar == null) {
            this.mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (this.mActionBarToolbar != null) {
                setSupportActionBar(this.mActionBarToolbar);
            }
        }
        return this.mActionBarToolbar;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }
}
