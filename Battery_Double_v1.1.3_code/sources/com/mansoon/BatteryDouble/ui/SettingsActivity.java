package com.mansoon.BatteryDouble.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import com.mansoon.BatteryDouble.BuildConfig;
import com.mansoon.BatteryDouble.GreenHubApp;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.tasks.DeleteSessionsTask;
import com.mansoon.BatteryDouble.tasks.DeleteUsagesTask;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.Notifier;
import com.mansoon.BatteryDouble.util.SettingsUtils;

public class SettingsActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(SettingsActivity.class);

    public static class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            addPreferencesFromResource(R.xml.preferences);
            getActivity().getApplicationContext();
            findPreference(SettingsUtils.PREF_APP_VERSION).setSummary(BuildConfig.VERSION_NAME);
            bindPreferenceSummaryToValue(findPreference(SettingsUtils.PREF_DATA_HISTORY));
            bindPreferenceSummaryToValue(findPreference(SettingsUtils.PREF_TEMPERATURE_RATE));
            bindPreferenceSummaryToValue(findPreference(SettingsUtils.PREF_TEMPERATURE_WARNING));
            bindPreferenceSummaryToValue(findPreference(SettingsUtils.PREF_TEMPERATURE_HIGH));
            bindPreferenceSummaryToValue(findPreference(SettingsUtils.PREF_NOTIFICATIONS_PRIORITY));
            SettingsUtils.registerOnSharedPreferenceChangeListener(getActivity(), this);
        }

        public void onDestroy() {
            super.onDestroy();
            SettingsUtils.unregisterOnSharedPreferenceChangeListener(getActivity(), this);
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
            char c;
            Context applicationContext = getActivity().getApplicationContext();
            GreenHubApp greenHubApp = (GreenHubApp) getActivity().getApplication();
            Preference findPreference = findPreference(str);
            switch (str.hashCode()) {
                case -1058682469:
                    if (str.equals(SettingsUtils.PREF_DATA_HISTORY)) {
                        c = 1;
                        break;
                    }
                case -224299966:
                    if (str.equals(SettingsUtils.PREF_UPLOAD_RATE)) {
                        c = 2;
                        break;
                    }
                case 214107177:
                    if (str.equals(SettingsUtils.PREF_TEMPERATURE_HIGH)) {
                        c = 5;
                        break;
                    }
                case 214397799:
                    if (str.equals(SettingsUtils.PREF_TEMPERATURE_RATE)) {
                        c = 6;
                        break;
                    }
                case 538500520:
                    if (str.equals(SettingsUtils.PREF_SAMPLING_SCREEN)) {
                        c = 0;
                        break;
                    }
                case 649537461:
                    if (str.equals(SettingsUtils.PREF_TEMPERATURE_WARNING)) {
                        c = 4;
                        break;
                    }
                case 991928503:
                    if (str.equals(SettingsUtils.PREF_NOTIFICATIONS_PRIORITY)) {
                        c = 7;
                        break;
                    }
                case 1155191097:
                    if (str.equals(SettingsUtils.PREF_POWER_INDICATOR)) {
                        c = 3;
                        break;
                    }
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                    LogUtils.LOGI(SettingsActivity.TAG, "Restarting GreenHub Service because of preference changes");
                    greenHubApp.stopGreenHubService();
                    greenHubApp.startGreenHubService();
                    return;
                case 1:
                    bindPreferenceSummaryToValue(findPreference);
                    int fetchDataHistoryInterval = SettingsUtils.fetchDataHistoryInterval(applicationContext);
                    new DeleteUsagesTask().execute(new Integer[]{Integer.valueOf(fetchDataHistoryInterval)});
                    new DeleteSessionsTask().execute(new Integer[]{Integer.valueOf(fetchDataHistoryInterval)});
                    return;
                case 2:
                    bindPreferenceSummaryToValue(findPreference);
                    return;
                case 3:
                    if (SettingsUtils.isPowerIndicatorShown(applicationContext)) {
                        Notifier.startStatusBar(applicationContext);
                        greenHubApp.startStatusBarUpdater();
                        return;
                    }
                    Notifier.closeStatusBar();
                    greenHubApp.stopStatusBarUpdater();
                    return;
                case 4:
                    bindPreferenceSummaryToValue(findPreference);
                    return;
                case 5:
                    bindPreferenceSummaryToValue(findPreference);
                    return;
                case 6:
                    bindPreferenceSummaryToValue(findPreference);
                    return;
                case 7:
                    bindPreferenceSummaryToValue(findPreference);
                    return;
                default:
                    return;
            }
        }

        private void bindPreferenceSummaryToValue(Preference preference) {
            String string = PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), "");
            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int findIndexOfValue = listPreference.findIndexOfValue(string);
                preference.setSummary(findIndexOfValue >= 0 ? listPreference.getEntries()[findIndexOfValue] : null);
            } else if (preference instanceof EditTextPreference) {
                EditTextPreference editTextPreference = (EditTextPreference) preference;
                String replaceFirst = string.replaceFirst("^0+(?!$)", "");
                editTextPreference.setText(replaceFirst);
                preference.setSummary(replaceFirst.replaceFirst("^0+(?!$)", ""));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
