package com.mansoon.BatteryDouble.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.google.gson.JsonObject;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.models.Specifications;
import com.mansoon.BatteryDouble.models.data.Message;
import com.mansoon.BatteryDouble.network.services.GreenHubAPIService;
import com.mansoon.BatteryDouble.util.Notifier;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckNewMessagesTask extends AsyncTask<Context, Void, Void> {
    /* access modifiers changed from: protected */
    public Void doInBackground(final Context... contextArr) {
        ((GreenHubAPIService) new Builder().baseUrl(SettingsUtils.fetchServerUrl(contextArr[0])).addConverterFactory(GsonConverterFactory.create()).build().create(GreenHubAPIService.class)).getMessages(Specifications.getAndroidId(contextArr[0]), SettingsUtils.fetchLastMessageId(contextArr[0])).enqueue(new Callback<List<JsonObject>>() {
            public void onResponse(Call<List<JsonObject>> call, Response<List<JsonObject>> response) {
                if (response == null) {
                    EventBus.getDefault().post(new StatusEvent(contextArr[0].getString(R.string.event_server_response_failed)));
                    return;
                }
                if (response.body() != null && ((List) response.body()).size() > 0) {
                    Realm defaultInstance = Realm.getDefaultInstance();
                    Message message = null;
                    for (JsonObject jsonObject : (List) response.body()) {
                        Message message2 = new Message(jsonObject.get("id").getAsInt(), jsonObject.get("title").getAsString(), jsonObject.get(TtmlNode.TAG_BODY).getAsString(), jsonObject.get("created_at").getAsString());
                        if (defaultInstance.where(Message.class).equalTo("id", Integer.valueOf(message2.realmGet$id())).count() == 0) {
                            try {
                                defaultInstance.beginTransaction();
                                defaultInstance.copyToRealm(message2);
                                defaultInstance.commitTransaction();
                            } catch (RealmPrimaryKeyConstraintException e) {
                                e.printStackTrace();
                            }
                        }
                        message = message2;
                    }
                    defaultInstance.close();
                    if (message != null) {
                        SettingsUtils.saveLastMessageId(contextArr[0], message.realmGet$id());
                    }
                    if (SettingsUtils.isMessageAlertsOn(contextArr[0])) {
                        Notifier.newMessageAlert(contextArr[0]);
                    }
                }
            }

            public void onFailure(Call<List<JsonObject>> call, Throwable th) {
                th.printStackTrace();
                EventBus.getDefault().post(new StatusEvent(contextArr[0].getString(R.string.event_server_not_responding)));
            }
        });
        return null;
    }
}
