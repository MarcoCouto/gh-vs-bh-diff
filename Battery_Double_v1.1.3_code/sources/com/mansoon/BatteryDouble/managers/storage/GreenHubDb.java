package com.mansoon.BatteryDouble.managers.storage;

import com.google.android.gms.measurement.AppMeasurement.Param;
import com.mansoon.BatteryDouble.models.data.BatterySession;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import com.mansoon.BatteryDouble.models.data.Message;
import com.mansoon.BatteryDouble.models.data.Sample;
import com.mansoon.BatteryDouble.util.LogUtils;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.exceptions.RealmMigrationNeededException;
import java.util.ArrayList;
import java.util.Iterator;

public class GreenHubDb {
    private static final String TAG = LogUtils.makeLogTag(GreenHubDb.class);
    private Realm mRealm;

    public GreenHubDb() {
        try {
            this.mRealm = Realm.getDefaultInstance();
        } catch (RealmMigrationNeededException e) {
            e.printStackTrace();
        }
    }

    public void getDefaultInstance() {
        if (this.mRealm.isClosed()) {
            this.mRealm = Realm.getDefaultInstance();
        }
    }

    public void close() {
        this.mRealm.close();
    }

    public boolean isClosed() {
        return this.mRealm.isClosed();
    }

    public long count(Class cls) {
        if (cls.equals(Sample.class)) {
            return this.mRealm.where(Sample.class).count();
        }
        if (cls.equals(BatteryUsage.class)) {
            return this.mRealm.where(BatteryUsage.class).count();
        }
        if (cls.equals(BatterySession.class)) {
            return this.mRealm.where(BatterySession.class).count();
        }
        if (cls.equals(Message.class)) {
            return this.mRealm.where(Message.class).count();
        }
        return -1;
    }

    public Sample lastSample() {
        if (this.mRealm.where(Sample.class).count() > 0) {
            return (Sample) this.mRealm.where(Sample.class).findAll().last();
        }
        return null;
    }

    public void saveSample(Sample sample) {
        this.mRealm.beginTransaction();
        this.mRealm.copyToRealm(sample);
        this.mRealm.commitTransaction();
    }

    public void saveUsage(BatteryUsage batteryUsage) {
        this.mRealm.beginTransaction();
        this.mRealm.copyToRealm(batteryUsage);
        this.mRealm.commitTransaction();
    }

    public void saveSession(BatterySession batterySession) {
        this.mRealm.beginTransaction();
        this.mRealm.copyToRealm(batterySession);
        this.mRealm.commitTransaction();
    }

    public Iterator<Integer> allSamplesIds() {
        ArrayList arrayList = new ArrayList();
        RealmResults findAll = this.mRealm.where(Sample.class).findAll();
        if (!findAll.isEmpty()) {
            Iterator it = findAll.iterator();
            while (it.hasNext()) {
                arrayList.add(Integer.valueOf(((Sample) it.next()).realmGet$id()));
            }
        }
        return arrayList.iterator();
    }

    public RealmResults<BatteryUsage> betweenUsages(long j, long j2) {
        return this.mRealm.where(BatteryUsage.class).equalTo("triggeredBy", "android.intent.action.BATTERY_CHANGED").between(Param.TIMESTAMP, j, j2).findAllSorted(Param.TIMESTAMP);
    }

    public RealmResults<Message> allMessages() {
        return this.mRealm.where(Message.class).findAllSorted("id", Sort.DESCENDING);
    }

    public void markMessageAsRead(int i) {
        this.mRealm.beginTransaction();
        Message message = (Message) this.mRealm.where(Message.class).equalTo("id", Integer.valueOf(i)).findFirst();
        if (message != null) {
            message.realmSet$read(true);
        }
        this.mRealm.commitTransaction();
    }

    public void deleteMessage(int i) {
        this.mRealm.beginTransaction();
        Message message = (Message) this.mRealm.where(Message.class).equalTo("id", Integer.valueOf(i)).findFirst();
        if (message != null) {
            message.deleteFromRealm();
        }
        this.mRealm.commitTransaction();
    }
}
