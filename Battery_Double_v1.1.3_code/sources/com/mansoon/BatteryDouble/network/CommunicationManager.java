package com.mansoon.BatteryDouble.network;

import android.content.Context;
import android.os.Handler;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDb;
import com.mansoon.BatteryDouble.models.data.AppPermission;
import com.mansoon.BatteryDouble.models.data.AppSignature;
import com.mansoon.BatteryDouble.models.data.Feature;
import com.mansoon.BatteryDouble.models.data.LocationProvider;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.models.data.Sample;
import com.mansoon.BatteryDouble.models.data.Upload;
import com.mansoon.BatteryDouble.network.services.GreenHubAPIService;
import com.mansoon.BatteryDouble.tasks.DeleteSampleTask;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.NetworkWatcher;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.realm.Realm;
import java.util.Iterator;
import org.altbeacon.beacon.BeaconManager;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommunicationManager {
    private static final int RESPONSE_ERROR = 0;
    private static final int RESPONSE_OKAY = 1;
    private static final String TAG = LogUtils.makeLogTag(CommunicationManager.class);
    public static boolean isQueued = false;
    public static boolean isUploading = false;
    public static int uploadAttempts;
    private Iterator<Integer> mCollection;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public boolean mOnBackground;
    private GreenHubAPIService mService;

    public CommunicationManager(Context context, boolean z) {
        this.mContext = context;
        this.mOnBackground = z;
        this.mService = (GreenHubAPIService) new Builder().baseUrl(SettingsUtils.fetchServerUrl(context)).addConverterFactory(GsonConverterFactory.create()).build().create(GreenHubAPIService.class);
    }

    public void sendSamples() {
        if (!NetworkWatcher.hasInternet(this.mContext, 2)) {
            EventBus.getDefault().post(new StatusEvent(this.mContext.getString(R.string.event_no_connectivity)));
            isUploading = false;
            isQueued = true;
            return;
        }
        GreenHubDb greenHubDb = new GreenHubDb();
        long count = greenHubDb.count(Sample.class);
        this.mCollection = greenHubDb.allSamplesIds();
        greenHubDb.close();
        if (!this.mCollection.hasNext()) {
            EventBus.getDefault().post(new StatusEvent(this.mContext.getString(R.string.event_no_samples)));
            isUploading = false;
            refreshStatus();
            return;
        }
        EventBus.getDefault().post(new StatusEvent(makeUploadingMessage(count)));
        isUploading = true;
        uploadSample(((Integer) this.mCollection.next()).intValue());
    }

    private void uploadSample(final int i) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Uploading Sample => ");
        sb.append(i);
        LogUtils.LOGI(str, sb.toString());
        Realm defaultInstance = Realm.getDefaultInstance();
        Upload upload = new Upload(bundleSample((Sample) defaultInstance.where(Sample.class).equalTo("id", Integer.valueOf(i)).findFirst()));
        defaultInstance.close();
        this.mService.createSample(upload).enqueue(new Callback<Integer>() {
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response == null) {
                    EventBus.getDefault().post(new StatusEvent(CommunicationManager.this.mContext.getString(R.string.event_server_response_failed)));
                    return;
                }
                if (response.body() != null) {
                    CommunicationManager.this.handleResponse(((Integer) response.body()).intValue(), i);
                } else {
                    CommunicationManager.this.handleResponse(0, -1);
                }
            }

            public void onFailure(Call<Integer> call, Throwable th) {
                th.printStackTrace();
                EventBus.getDefault().post(new StatusEvent(CommunicationManager.this.mContext.getString(R.string.event_server_not_responding)));
                if (CommunicationManager.this.mOnBackground) {
                    CommunicationManager.uploadAttempts++;
                }
                CommunicationManager.isUploading = false;
                CommunicationManager.this.refreshStatus();
            }
        });
    }

    /* access modifiers changed from: private */
    public void handleResponse(int i, int i2) {
        if (i == 1) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Sample => ");
            sb.append(i2);
            sb.append(" uploaded successfully! Deleting uploaded sample...");
            LogUtils.LOGI(str, sb.toString());
            new DeleteSampleTask().execute(new Integer[]{Integer.valueOf(i2)});
            if (!this.mCollection.hasNext()) {
                EventBus.getDefault().post(new StatusEvent(this.mContext.getString(R.string.event_upload_finished)));
                if (this.mOnBackground) {
                    uploadAttempts = 0;
                }
                isUploading = false;
                refreshStatus();
                return;
            }
            uploadSample(((Integer) this.mCollection.next()).intValue());
        } else if (i == 0) {
            EventBus.getDefault().post(new StatusEvent(this.mContext.getString(R.string.event_error_uploading_sample)));
            isUploading = false;
            refreshStatus();
        }
    }

    private JsonObject bundleSample(Sample sample) {
        if (sample == null) {
            return null;
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("uuId", sample.realmGet$uuId());
        jsonObject.addProperty(Param.TIMESTAMP, (Number) Long.valueOf(sample.realmGet$timestamp()));
        jsonObject.addProperty(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, (Number) Integer.valueOf(sample.realmGet$version()));
        jsonObject.addProperty("database", (Number) Integer.valueOf(sample.realmGet$database()));
        jsonObject.addProperty("batteryState", sample.realmGet$batteryState());
        jsonObject.addProperty("batteryLevel", (Number) Double.valueOf(sample.realmGet$batteryLevel()));
        jsonObject.addProperty("memoryWired", (Number) Integer.valueOf(sample.realmGet$memoryWired()));
        jsonObject.addProperty("memoryActive", (Number) Integer.valueOf(sample.realmGet$memoryActive()));
        jsonObject.addProperty("memoryInactive", (Number) Integer.valueOf(sample.realmGet$memoryInactive()));
        jsonObject.addProperty("memoryFree", (Number) Integer.valueOf(sample.realmGet$memoryFree()));
        jsonObject.addProperty("memoryUser", (Number) Integer.valueOf(sample.realmGet$memoryUser()));
        jsonObject.addProperty("triggeredBy", sample.realmGet$triggeredBy());
        jsonObject.addProperty("networkStatus", sample.realmGet$networkStatus());
        jsonObject.addProperty("distanceTraveled", (Number) Double.valueOf(sample.realmGet$distanceTraveled()));
        jsonObject.addProperty("screenBrightness", (Number) Integer.valueOf(sample.realmGet$screenBrightness()));
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("networkType", sample.realmGet$networkDetails().realmGet$networkType());
        jsonObject2.addProperty("mobileNetworkType", sample.realmGet$networkDetails().realmGet$mobileNetworkType());
        jsonObject2.addProperty("mobileDataStatus", sample.realmGet$networkDetails().realmGet$mobileDataStatus());
        jsonObject2.addProperty("mobileDataActivity", sample.realmGet$networkDetails().realmGet$mobileDataActivity());
        jsonObject2.addProperty("roamingEnabled", (Number) Integer.valueOf(sample.realmGet$networkDetails().realmGet$roamingEnabled()));
        jsonObject2.addProperty("wifiStatus", sample.realmGet$networkDetails().realmGet$wifiStatus());
        jsonObject2.addProperty("wifiSignalStrength", (Number) Integer.valueOf(sample.realmGet$networkDetails().realmGet$wifiSignalStrength()));
        jsonObject2.addProperty("wifiLinkSpeed", (Number) Integer.valueOf(sample.realmGet$networkDetails().realmGet$wifiLinkSpeed()));
        jsonObject2.addProperty("wifiApStatus", sample.realmGet$networkDetails().realmGet$wifiApStatus());
        jsonObject2.addProperty("networkOperator", sample.realmGet$networkDetails().realmGet$networkOperator());
        jsonObject2.addProperty("simOperator", sample.realmGet$networkDetails().realmGet$simOperator());
        jsonObject2.addProperty("mcc", sample.realmGet$networkDetails().realmGet$mcc());
        jsonObject2.addProperty("mnc", sample.realmGet$networkDetails().realmGet$mnc());
        if (sample.realmGet$networkDetails().realmGet$networkStatistics() != null) {
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty("wifiReceived", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$wifiReceived()));
            jsonObject3.addProperty("wifiSent", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$wifiSent()));
            jsonObject3.addProperty("mobileReceived", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$mobileReceived()));
            jsonObject3.addProperty("mobileSent", (Number) Double.valueOf(sample.realmGet$networkDetails().realmGet$networkStatistics().realmGet$mobileSent()));
            jsonObject2.add("networkStatistics", jsonObject3);
        }
        jsonObject.add("networkDetails", jsonObject2);
        JsonObject jsonObject4 = new JsonObject();
        jsonObject4.addProperty("charger", sample.realmGet$batteryDetails().realmGet$charger());
        jsonObject4.addProperty("health", sample.realmGet$batteryDetails().realmGet$health());
        jsonObject4.addProperty("voltage", (Number) Double.valueOf(sample.realmGet$batteryDetails().realmGet$voltage()));
        jsonObject4.addProperty("temperature", (Number) Double.valueOf(sample.realmGet$batteryDetails().realmGet$temperature()));
        jsonObject4.addProperty("technology", sample.realmGet$batteryDetails().realmGet$technology());
        jsonObject4.addProperty("capacity", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$capacity()));
        jsonObject4.addProperty("chargeCounter", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$chargeCounter()));
        jsonObject4.addProperty("currentAverage", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$currentAverage()));
        jsonObject4.addProperty("currentNow", (Number) Integer.valueOf(sample.realmGet$batteryDetails().realmGet$currentNow()));
        jsonObject4.addProperty("energyCounter", (Number) Long.valueOf(sample.realmGet$batteryDetails().realmGet$energyCounter()));
        jsonObject.add("batteryDetails", jsonObject4);
        JsonObject jsonObject5 = new JsonObject();
        jsonObject5.addProperty("cpuUsage", (Number) Double.valueOf(sample.realmGet$cpuStatus().realmGet$cpuUsage()));
        jsonObject5.addProperty("upTime", (Number) Long.valueOf(sample.realmGet$cpuStatus().realmGet$upTime()));
        jsonObject5.addProperty("sleepTime", (Number) Long.valueOf(sample.realmGet$cpuStatus().realmGet$sleepTime()));
        jsonObject.add("cpuStatus", jsonObject5);
        jsonObject.addProperty("screenOn", (Number) Integer.valueOf(sample.realmGet$screenOn()));
        jsonObject.addProperty("timeZone", sample.realmGet$timeZone());
        JsonObject jsonObject6 = new JsonObject();
        jsonObject6.addProperty("bluetoothEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$bluetoothEnabled()));
        jsonObject6.addProperty("locationEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$locationEnabled()));
        jsonObject6.addProperty("powersaverEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$powersaverEnabled()));
        jsonObject6.addProperty("flashlightEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$flashlightEnabled()));
        jsonObject6.addProperty("nfcEnabled", Boolean.valueOf(sample.realmGet$settings().realmGet$nfcEnabled()));
        jsonObject6.addProperty("unknownSources", (Number) Integer.valueOf(sample.realmGet$settings().realmGet$unknownSources()));
        jsonObject6.addProperty("developerMode", (Number) Integer.valueOf(sample.realmGet$settings().realmGet$developerMode()));
        jsonObject.add("settings", jsonObject6);
        JsonObject jsonObject7 = new JsonObject();
        jsonObject7.addProperty("free", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$free()));
        jsonObject7.addProperty("total", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$total()));
        jsonObject7.addProperty("freeExternal", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$freeExternal()));
        jsonObject7.addProperty("totalExternal", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$totalExternal()));
        jsonObject7.addProperty("freeSystem", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$freeSystem()));
        jsonObject7.addProperty("totalSystem", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$totalSystem()));
        jsonObject7.addProperty("freeSecondary", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$freeSecondary()));
        jsonObject7.addProperty("totalSecondary", (Number) Integer.valueOf(sample.realmGet$storageDetails().realmGet$totalSecondary()));
        jsonObject.add("storageDetails", jsonObject7);
        jsonObject.addProperty("countryCode", sample.realmGet$countryCode());
        if (sample.realmGet$locationProviders() != null && !sample.realmGet$locationProviders().isEmpty()) {
            JsonArray jsonArray = new JsonArray();
            Iterator it = sample.realmGet$locationProviders().iterator();
            while (it.hasNext()) {
                LocationProvider locationProvider = (LocationProvider) it.next();
                JsonObject jsonObject8 = new JsonObject();
                jsonObject8.addProperty("provider", locationProvider.realmGet$provider());
                jsonArray.add((JsonElement) jsonObject8);
            }
            jsonObject.add("locationProviders", jsonArray);
        }
        if (sample.realmGet$features() != null && !sample.realmGet$features().isEmpty()) {
            JsonArray jsonArray2 = new JsonArray();
            Iterator it2 = sample.realmGet$features().iterator();
            while (it2.hasNext()) {
                Feature feature = (Feature) it2.next();
                JsonObject jsonObject9 = new JsonObject();
                jsonObject9.addProperty("key", feature.realmGet$key());
                jsonObject9.addProperty("value", feature.realmGet$value());
                jsonArray2.add((JsonElement) jsonObject9);
            }
            jsonObject.add(SettingsJsonConstants.FEATURES_KEY, jsonArray2);
        }
        if (sample.realmGet$processInfos() != null && !sample.realmGet$processInfos().isEmpty()) {
            JsonArray jsonArray3 = new JsonArray();
            Iterator it3 = sample.realmGet$processInfos().iterator();
            while (it3.hasNext()) {
                ProcessInfo processInfo = (ProcessInfo) it3.next();
                JsonObject jsonObject10 = new JsonObject();
                jsonObject10.addProperty("processId", (Number) Integer.valueOf(processInfo.realmGet$processId()));
                jsonObject10.addProperty("name", processInfo.realmGet$name());
                jsonObject10.addProperty("applicationLabel", processInfo.realmGet$applicationLabel() == null ? "" : processInfo.realmGet$applicationLabel());
                jsonObject10.addProperty("isSystemApp", Boolean.valueOf(processInfo.realmGet$isSystemApp()));
                jsonObject10.addProperty("importance", processInfo.realmGet$importance());
                jsonObject10.addProperty("versionName", processInfo.realmGet$versionName() == null ? "" : processInfo.realmGet$versionName());
                jsonObject10.addProperty("versionCode", (Number) Integer.valueOf(processInfo.realmGet$versionCode()));
                jsonObject10.addProperty("installationPkg", processInfo.realmGet$installationPkg() == null ? "" : processInfo.realmGet$installationPkg());
                if (processInfo.realmGet$appPermissions() != null && !processInfo.realmGet$appPermissions().isEmpty()) {
                    JsonArray jsonArray4 = new JsonArray();
                    Iterator it4 = processInfo.realmGet$appPermissions().iterator();
                    while (it4.hasNext()) {
                        AppPermission appPermission = (AppPermission) it4.next();
                        JsonObject jsonObject11 = new JsonObject();
                        jsonObject11.addProperty("permission", appPermission.realmGet$permission());
                        jsonArray4.add((JsonElement) jsonObject11);
                    }
                    jsonObject10.add("appPermissions", jsonArray4);
                }
                if (processInfo.realmGet$appSignatures() != null && !processInfo.realmGet$appSignatures().isEmpty()) {
                    JsonArray jsonArray5 = new JsonArray();
                    Iterator it5 = processInfo.realmGet$appSignatures().iterator();
                    while (it5.hasNext()) {
                        AppSignature appSignature = (AppSignature) it5.next();
                        JsonObject jsonObject12 = new JsonObject();
                        jsonObject12.addProperty("signature", appSignature.realmGet$signature());
                        jsonArray5.add((JsonElement) jsonObject12);
                    }
                    jsonObject10.add("appSignatures", jsonArray5);
                }
                jsonArray3.add((JsonElement) jsonObject10);
            }
            jsonObject.add("processInfos", jsonArray3);
        }
        return jsonObject;
    }

    /* access modifiers changed from: private */
    public void refreshStatus() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                EventBus.getDefault().post(new StatusEvent(CommunicationManager.this.mContext.getString(R.string.event_idle)));
            }
        }, BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD);
    }

    private String makeUploadingMessage(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.mContext.getString(R.string.event_uploading));
        sb.append(" ");
        sb.append(j);
        sb.append(" ");
        sb.append(this.mContext.getString(R.string.event_samples));
        return sb.toString();
    }
}
