package com.mansoon.BatteryDouble.models;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.text.format.Formatter;
import java.lang.reflect.Method;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;

public class Wifi {
    private static final String TAG = "Wifi";
    public static final int WIFI_AP_STATE_DISABLED = 11;
    public static final int WIFI_AP_STATE_DISABLING = 10;
    public static final int WIFI_AP_STATE_ENABLED = 13;
    public static final int WIFI_AP_STATE_ENABLING = 12;
    public static final int WIFI_AP_STATE_FAILED = 14;
    private static final String WIFI_STATE_DISABLED = "disabled";
    private static final String WIFI_STATE_DISABLING = "disabling";
    private static final String WIFI_STATE_ENABLED = "enabled";
    private static final String WIFI_STATE_ENABLING = "enabling";
    private static final String WIFI_STATE_UNKNOWN = "unknown";

    public static int getSignalStrength(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getRssi();
    }

    @SuppressLint({"HardwareIds"})
    public static String getMacAddress(Context context) {
        if (VERSION.SDK_INT >= 23) {
            return getMacAddressMarshmallow();
        }
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        String str = null;
        if (wifiManager == null) {
            return null;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (connectionInfo != null) {
            str = connectionInfo.getMacAddress();
        }
        return str;
    }

    public static String getIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        String str = null;
        if (wifiManager == null) {
            return null;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (connectionInfo != null) {
            str = Formatter.formatIpAddress(connectionInfo.getIpAddress());
        }
        return str;
    }

    @TargetApi(23)
    private static String getMacAddressMarshmallow() {
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress == null) {
                        return "";
                    }
                    StringBuilder sb = new StringBuilder();
                    for (byte valueOf : hardwareAddress) {
                        sb.append(String.format("%02X:", new Object[]{Byte.valueOf(valueOf)}));
                    }
                    if (sb.length() > 0) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    return sb.toString();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "02:00:00:00:00:00";
    }

    public static int getLinkSpeed(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getLinkSpeed();
    }

    public static boolean isEnabled(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).isWifiEnabled();
    }

    public static String getState(Context context) {
        switch (((WifiManager) context.getSystemService("wifi")).getWifiState()) {
            case 0:
                return WIFI_STATE_DISABLING;
            case 1:
                return "disabled";
            case 2:
                return WIFI_STATE_ENABLING;
            case 3:
                return WIFI_STATE_ENABLED;
            case 4:
                return "unknown";
            default:
                return "unknown";
        }
    }

    public static WifiInfo getInfo(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
    }

    public static String getHotspotState(Context context) {
        try {
            switch (getWifiApState(context)) {
                case 10:
                    return WIFI_STATE_DISABLING;
                case 11:
                    return "disabled";
                case 12:
                    return WIFI_STATE_ENABLING;
                case 13:
                    return WIFI_STATE_ENABLED;
                case 14:
                    return "failed";
                default:
                    return "unknown";
            }
        } catch (Exception unused) {
            return "unknown";
        }
    }

    private static int getWifiApState(Context context) throws Exception {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        Method declaredMethod = wifiManager.getClass().getDeclaredMethod("getWifiApState", new Class[0]);
        declaredMethod.setAccessible(true);
        return ((Integer) declaredMethod.invoke(wifiManager, new Object[0])).intValue();
    }
}
