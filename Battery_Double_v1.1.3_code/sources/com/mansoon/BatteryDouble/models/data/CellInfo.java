package com.mansoon.BatteryDouble.models.data;

import io.realm.CellInfoRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class CellInfo extends RealmObject implements CellInfoRealmProxyInterface {
    public int cid;
    public int lac;
    public int mcc;
    public int mnc;
    public String radioType;

    public int realmGet$cid() {
        return this.cid;
    }

    public int realmGet$lac() {
        return this.lac;
    }

    public int realmGet$mcc() {
        return this.mcc;
    }

    public int realmGet$mnc() {
        return this.mnc;
    }

    public String realmGet$radioType() {
        return this.radioType;
    }

    public void realmSet$cid(int i) {
        this.cid = i;
    }

    public void realmSet$lac(int i) {
        this.lac = i;
    }

    public void realmSet$mcc(int i) {
        this.mcc = i;
    }

    public void realmSet$mnc(int i) {
        this.mnc = i;
    }

    public void realmSet$radioType(String str) {
        this.radioType = str;
    }

    public CellInfo() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$mcc(0);
        realmSet$mnc(0);
        realmSet$lac(0);
        realmSet$cid(0);
        realmSet$radioType(null);
    }
}
