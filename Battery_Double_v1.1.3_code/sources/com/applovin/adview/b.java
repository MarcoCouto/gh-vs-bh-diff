package com.applovin.adview;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;

class b implements OnPreparedListener {
    final /* synthetic */ AppLovinInterstitialActivity a;

    b(AppLovinInterstitialActivity appLovinInterstitialActivity) {
        this.a = appLovinInterstitialActivity;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.a.t.setVideoSize(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        mediaPlayer.setDisplay(this.a.t.getHolder());
        mediaPlayer.setOnErrorListener(new c(this));
    }
}
