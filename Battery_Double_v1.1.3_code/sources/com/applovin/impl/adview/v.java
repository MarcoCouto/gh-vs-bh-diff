package com.applovin.impl.adview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.sdk.bf;
import com.applovin.impl.sdk.cd;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;

class v extends Dialog implements u {
    /* access modifiers changed from: private */
    public final Activity a;
    private final AppLovinSdk b;
    /* access modifiers changed from: private */
    public final AppLovinLogger c;
    private RelativeLayout d;
    /* access modifiers changed from: private */
    public AppLovinAdView e;
    /* access modifiers changed from: private */
    public Runnable f;
    private ad g = null;
    /* access modifiers changed from: private */
    public volatile boolean h = false;
    /* access modifiers changed from: private */
    public volatile boolean i = false;

    v(AppLovinSdk appLovinSdk, Activity activity) {
        super(activity, 16973840);
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else {
            this.b = appLovinSdk;
            this.c = appLovinSdk.getLogger();
            this.a = activity;
            this.f = new ac(this, null);
            this.e = new AppLovinAdView(appLovinSdk, AppLovinAdSize.INTERSTITIAL, activity);
            this.e.setAutoDestroy(false);
            requestWindowFeature(1);
            try {
                getWindow().setFlags(activity.getWindow().getAttributes().flags, activity.getWindow().getAttributes().flags);
            } catch (Exception e2) {
                this.c.e("InterstitialAdDialog", "Set window flags failed.", e2);
            }
        }
    }

    private int a(int i2) {
        return cd.a((Context) this.a, i2);
    }

    private void a() {
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.e.setLayoutParams(layoutParams);
        this.d = new RelativeLayout(this.a);
        this.d.setLayoutParams(new LayoutParams(-1, -1));
        this.d.setBackgroundColor(-1157627904);
        this.d.addView(this.e);
        setContentView(this.d);
    }

    /* access modifiers changed from: private */
    public void a(t tVar) {
        s a2 = s.a(this.b, getContext(), tVar);
        a2.setOnClickListener(new aa(this));
        bf bfVar = new bf(this.b);
        int a3 = a(bfVar.l());
        LayoutParams layoutParams = new LayoutParams(a3, a3);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        a2.a(a3);
        int a4 = a(bfVar.n());
        int a5 = a(bfVar.p());
        layoutParams.setMargins(0, a4, a5, 0);
        this.e.addView(a2, layoutParams);
        a2.bringToFront();
        int a6 = a(new bf(this.b).r());
        View view = new View(this.a);
        view.setBackgroundColor(0);
        int i2 = a3 + a6;
        LayoutParams layoutParams2 = new LayoutParams(i2, i2);
        layoutParams2.addRule(10);
        layoutParams2.addRule(11);
        layoutParams2.setMargins(0, a4 - a(5), a5 - a(5), 0);
        view.setOnClickListener(new ab(this, a2));
        this.e.addView(view, layoutParams2);
        view.bringToFront();
    }

    public void a(ad adVar) {
        this.e.setAdDisplayListener(new w(this, adVar));
        this.e.setAdClickListener(new x(this, adVar));
        this.e.setAdVideoPlaybackListener(new y(this, adVar));
        this.g = adVar;
        adVar.a(true);
    }

    public void a(AppLovinAd appLovinAd) {
        this.a.runOnUiThread(new z(this, appLovinAd));
    }

    public void dismiss() {
        if (this.g != null) {
            this.g.g();
        }
        if (this.e != null) {
            this.e.destroy();
        }
        this.g = null;
        this.e = null;
        super.dismiss();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a();
    }
}
