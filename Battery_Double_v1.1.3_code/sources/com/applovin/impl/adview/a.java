package com.applovin.impl.adview;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinLogger;

class a implements Runnable {
    final /* synthetic */ AppLovinAd a;
    final /* synthetic */ AdViewControllerImpl b;

    a(AdViewControllerImpl adViewControllerImpl, AppLovinAd appLovinAd) {
        this.b = adViewControllerImpl;
        this.a = appLovinAd;
    }

    public void run() {
        try {
            if (this.b.s != null) {
                this.b.s.adReceived(this.a);
            }
        } catch (Throwable th) {
            AppLovinLogger b2 = this.b.d;
            String str = AppLovinLogger.SDK_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Exception while running app load callback: ");
            sb.append(th.getMessage());
            b2.userError(str, sb.toString());
        }
    }
}
