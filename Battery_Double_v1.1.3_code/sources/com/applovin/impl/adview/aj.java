package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import com.applovin.sdk.AppLovinSdk;

public class aj extends s {
    private float c = 30.0f;
    private float d = 2.0f;
    private float e = 8.0f;
    private float f = 2.0f;
    private float g = 1.0f;

    public aj(AppLovinSdk appLovinSdk, Context context) {
        super(appLovinSdk, context);
    }

    /* access modifiers changed from: protected */
    public float a() {
        return this.c * this.g;
    }

    public void a(float f2) {
        this.g = f2;
    }

    public void a(int i) {
        a(((float) i) / this.c);
    }

    /* access modifiers changed from: protected */
    public float b() {
        return this.e * this.g;
    }

    /* access modifiers changed from: protected */
    public float c() {
        return this.f * this.g;
    }

    /* access modifiers changed from: protected */
    public float d() {
        return a() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float d2 = d();
        Paint paint = new Paint(1);
        paint.setARGB(80, 0, 0, 0);
        canvas.drawCircle(d2, d2, d2, paint);
        Paint paint2 = new Paint(1);
        paint2.setColor(-1);
        paint2.setStyle(Style.STROKE);
        paint2.setStrokeWidth(c());
        float b = b();
        float a = a() - b;
        Canvas canvas2 = canvas;
        float f2 = b;
        float f3 = a;
        Paint paint3 = paint2;
        canvas2.drawLine(f2, b, f3, a, paint3);
        canvas2.drawLine(f2, a, f3, b, paint3);
    }
}
