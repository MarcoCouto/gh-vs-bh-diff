package com.applovin.impl.adview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class AppLovinVideoView extends VideoView {
    private int a;
    private int b;

    public AppLovinVideoView(Context context) {
        this(context, null);
    }

    public AppLovinVideoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppLovinVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = 0;
        this.b = 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        if (this.a <= 0 || this.b <= 0) {
            super.onMeasure(i, i2);
            return;
        }
        float height = ((float) this.b) / ((float) getHeight());
        float width = ((float) this.a) / ((float) getWidth());
        if (height > width) {
            i3 = (int) Math.ceil((double) (((float) this.b) / height));
            i4 = (int) Math.ceil((double) (((float) this.a) / height));
        } else {
            int ceil = (int) Math.ceil((double) (((float) this.a) / width));
            i3 = (int) Math.ceil((double) (((float) this.b) / width));
            i4 = ceil;
        }
        setMeasuredDimension(i4, i3);
    }

    public void setVideoSize(int i, int i2) {
        this.a = i;
        this.b = i2;
        try {
            getHolder().setFixedSize(i, i2);
            requestLayout();
            invalidate();
        } catch (Exception unused) {
        }
    }
}
