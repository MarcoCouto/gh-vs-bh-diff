package com.applovin.impl.adview;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;

class w implements AppLovinAdDisplayListener {
    final /* synthetic */ ad a;
    final /* synthetic */ v b;

    w(v vVar, ad adVar) {
        this.b = vVar;
        this.a = adVar;
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        w.super.show();
        if (!this.b.h) {
            AppLovinAdDisplayListener d = this.a.d();
            if (d != null) {
                d.adDisplayed(appLovinAd);
            }
            this.b.h = true;
        }
    }

    public void adHidden(AppLovinAd appLovinAd) {
        this.b.a.runOnUiThread(this.b.f);
        AppLovinAdDisplayListener d = this.a.d();
        if (!this.b.i && d != null) {
            d.adHidden(appLovinAd);
            this.b.i = true;
        }
        this.a.a(false);
    }
}
