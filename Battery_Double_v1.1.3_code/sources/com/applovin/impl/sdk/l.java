package com.applovin.impl.sdk;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import com.applovin.sdk.AppLovinTargetingData;
import com.facebook.appevents.UserDataStore;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

class l implements AppLovinTargetingData {
    private final Context a;

    l(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.a = appLovinSdkImpl.getApplicationContext();
    }

    private static String a(String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            if (cd.c(str)) {
                sb.append(cd.b(str));
                sb.append(",");
            }
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    private void a(String str, String str2) {
        if (cd.c(str)) {
            Editor edit = this.a.getSharedPreferences("applovin.sdk.targeting", 0).edit();
            edit.putString(str, cd.b(str2));
            edit.commit();
        }
    }

    /* access modifiers changed from: 0000 */
    public Map a() {
        HashMap hashMap = new HashMap();
        Map all = this.a.getSharedPreferences("applovin.sdk.targeting", 0).getAll();
        if (all != null && all.size() > 0) {
            for (Entry entry : all.entrySet()) {
                hashMap.put(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }
        return hashMap;
    }

    public void clearData() {
        Editor edit = this.a.getSharedPreferences("applovin.sdk.targeting", 0).edit();
        edit.clear();
        edit.commit();
    }

    public void putExtra(String str, String str2) {
        if (cd.c(str) && cd.c(str2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("ex_");
            sb.append(str);
            a(sb.toString(), str2);
        }
    }

    public void setBirthYear(int i) {
        if (i < 9999 && i > 1900) {
            a("yob", Integer.toString(i));
        }
    }

    public void setCarrier(String str) {
        if (cd.c(str)) {
            a("carrier", str);
        }
    }

    public void setCountry(String str) {
        if (cd.c(str) && str.length() == 2) {
            a(UserDataStore.COUNTRY, str.toUpperCase(Locale.ENGLISH));
        }
    }

    public void setGender(char c) {
        String str = c == 'm' ? "m" : c == 'f' ? "f" : "u";
        a("gender", str);
    }

    public void setInterests(String... strArr) {
        if (strArr != null && strArr.length > 0) {
            a("interests", a(strArr));
        }
    }

    public void setKeywords(String... strArr) {
        if (strArr != null && strArr.length > 0) {
            a("keywords", a(strArr));
        }
    }

    public void setLanguage(String str) {
        if (cd.c(str)) {
            a("language", str.toLowerCase(Locale.ENGLISH));
        }
    }

    public void setLocation(Location location) {
        if (location != null) {
            a("lat", Double.toString(location.getLatitude()));
            a("lon", Double.toString(location.getLongitude()));
        }
    }
}
