package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinLogger;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.UrlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

class n {
    private final AppLovinSdkImpl a;
    private final AppLovinLogger b;

    n(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
    }

    private int a(Throwable th) {
        if (th instanceof SocketTimeoutException) {
            return AppLovinErrorCodes.FETCH_AD_TIMEOUT;
        }
        if (!(th instanceof IOException)) {
            return th instanceof JSONException ? -104 : -1;
        }
        String message = th.getMessage();
        return (message == null || !message.toLowerCase(Locale.ENGLISH).contains("authentication challenge")) ? -100 : 401;
    }

    private HttpURLConnection a(String str, String str2, int i) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i < 0 ? ((Integer) this.a.a(bb.t)).intValue() : i);
        if (i < 0) {
            i = ((Integer) this.a.a(bb.v)).intValue();
        }
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        return httpURLConnection;
    }

    private static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception unused) {
            }
        }
    }

    private void a(String str, int i, String str2, o oVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(" received from from \"");
        sb.append(str2);
        sb.append("\": ");
        sb.append(str);
        this.b.d("ConnectionManager", sb.toString());
        if (i < 200 || i >= 300) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(i);
            sb2.append(" error received from \"");
            sb2.append(str2);
            sb2.append("\"");
            this.b.e("ConnectionManager", sb2.toString());
            oVar.a(i);
            return;
        }
        JSONObject jSONObject = new JSONObject();
        if (!(i == 204 || str == null || str.length() <= 2)) {
            jSONObject = new JSONObject(str);
        }
        oVar.a(jSONObject, i);
    }

    private void a(String str, String str2, int i, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("Successful ");
        sb.append(str);
        sb.append(" returned ");
        sb.append(i);
        sb.append(" in ");
        sb.append(((float) (System.currentTimeMillis() - j)) / 1000.0f);
        sb.append(" s");
        sb.append(" over ");
        sb.append(p.a(this.a));
        sb.append(" to \"");
        sb.append(str2);
        sb.append("\"");
        this.b.i("ConnectionManager", sb.toString());
    }

    private void a(String str, String str2, int i, long j, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed ");
        sb.append(str);
        sb.append(" returned ");
        sb.append(i);
        sb.append(" in ");
        sb.append(((float) (System.currentTimeMillis() - j)) / 1000.0f);
        sb.append(" s");
        sb.append(" over ");
        sb.append(p.a(this.a));
        sb.append(" to \"");
        sb.append(str2);
        sb.append("\"");
        this.b.e("ConnectionManager", sb.toString(), th);
    }

    private static void a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception unused) {
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, int i, o oVar) {
        a(str, HttpRequest.METHOD_GET, i, (JSONObject) null, oVar);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e4 A[SYNTHETIC, Splitter:B:38:0x00e4] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00eb A[Catch:{ all -> 0x00e9 }] */
    public void a(String str, String str2, int i, JSONObject jSONObject, o oVar) {
        HttpURLConnection httpURLConnection;
        Throwable th;
        int i2;
        InputStream inputStream;
        if (str == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No method specified");
        } else if (oVar == null) {
            throw new IllegalArgumentException("No callback specified");
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            int i3 = -1;
            InputStream inputStream2 = null;
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("Sending ");
                sb.append(str2);
                sb.append(" request to \"");
                sb.append(str);
                sb.append("\"...");
                this.b.i("ConnectionManager", sb.toString());
                httpURLConnection = a(str, str2, i);
                if (jSONObject != null) {
                    try {
                        String jSONObject2 = jSONObject.toString();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Request to \"");
                        sb2.append(str);
                        sb2.append("\" is ");
                        sb2.append(jSONObject2);
                        this.b.d("ConnectionManager", sb2.toString());
                        httpURLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setFixedLengthStreamingMode(jSONObject2.getBytes(Charset.forName("UTF-8")).length);
                        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(httpURLConnection.getOutputStream(), UrlUtils.UTF8));
                        printWriter.print(jSONObject2);
                        printWriter.close();
                    } catch (Throwable th2) {
                        th = th2;
                        if (i3 == 0) {
                        }
                        a(str2, str, i2, currentTimeMillis, th);
                        oVar.a(i2);
                        a(inputStream2);
                        a(httpURLConnection);
                    }
                }
                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode > 0) {
                    try {
                        inputStream = httpURLConnection.getInputStream();
                    } catch (Throwable th3) {
                        th = th3;
                    }
                    try {
                        String a2 = p.a(inputStream);
                        a(str2, str, responseCode, currentTimeMillis);
                        a(a2, httpURLConnection.getResponseCode(), str, oVar);
                        inputStream2 = inputStream;
                    } catch (Throwable th4) {
                        th = th4;
                        inputStream2 = inputStream;
                        a(inputStream2);
                        a(httpURLConnection);
                        throw th;
                    }
                    a(inputStream2);
                    a(httpURLConnection);
                }
                a(str2, str, responseCode, currentTimeMillis, (Throwable) null);
                oVar.a(responseCode);
                a(inputStream2);
                a(httpURLConnection);
            } catch (Throwable th5) {
                th = th5;
                httpURLConnection = null;
                a(inputStream2);
                a(httpURLConnection);
                throw th;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, JSONObject jSONObject, o oVar) {
        a(str, HttpRequest.METHOD_POST, -1, jSONObject, oVar);
    }
}
