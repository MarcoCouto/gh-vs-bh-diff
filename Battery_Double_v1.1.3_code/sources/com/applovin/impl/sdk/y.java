package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;

class y implements AppLovinAdLoadListener {
    final /* synthetic */ w a;
    /* access modifiers changed from: private */
    public final AppLovinAdLoadListener b;

    y(w wVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        this.a = wVar;
        this.b = appLovinAdLoadListener;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        this.a.c = (AppLovinAdInternal) appLovinAd;
        if (this.b != null) {
            this.a.e.post(new z(this, appLovinAd));
        }
    }

    public void failedToReceiveAd(int i) {
        if (this.b != null) {
            this.a.e.post(new aa(this, i));
        }
    }
}
