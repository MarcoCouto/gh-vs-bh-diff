package com.applovin.impl.sdk;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class bi extends ba {
    bi(AppLovinSdkImpl appLovinSdkImpl) {
        super("SubmitData", appLovinSdkImpl);
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        try {
            JSONObject a = p.a(jSONObject);
            be settingsManager = this.f.getSettingsManager();
            settingsManager.a(bb.c, a.getString("device_id"));
            settingsManager.a(bb.e, a.getString("device_token"));
            settingsManager.a(bb.d, a.getString("publisher_id"));
            settingsManager.b();
            p.a(a, this.f);
            if (a.has("adserver_parameters")) {
                settingsManager.a(bb.y, a.getJSONObject("adserver_parameters").toString());
            }
        } catch (JSONException e) {
            this.g.e(this.e, "Unable to parse API response", e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(JSONObject jSONObject) {
        q c = c();
        s b = c.b();
        t a = c.a();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("model", a.a);
        jSONObject2.put("os", a.b);
        jSONObject2.put("brand", a.c);
        jSONObject2.put("sdk_version", a.e);
        jSONObject2.put("revision", a.d);
        jSONObject2.put("country_code", a.f);
        jSONObject2.put("carrier", a.g);
        jSONObject2.put("type", "android");
        r c2 = c.c();
        String str = c2.b;
        if (!c2.a && cd.c(str)) {
            jSONObject2.put("idfa", str);
        }
        Locale locale = a.h;
        if (locale != null) {
            jSONObject2.put("locale", locale.toString());
        }
        jSONObject.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("package_name", b.c);
        jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, b.a);
        jSONObject3.put("app_version", b.b);
        jSONObject3.put("created_at", b.d / 1000);
        jSONObject3.put("applovin_sdk_version", "5.4.3");
        String str2 = (String) this.f.a(bb.P);
        jSONObject3.put("first_install", str2);
        if (str2.equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            this.f.getSettingsManager().a(bb.P, "false");
        }
        String str3 = (String) this.f.a(bb.F);
        if (str3 != null && str3.length() > 0) {
            jSONObject3.put("plugin_version", str3);
        }
        jSONObject.put("app_info", jSONObject3);
        if (((Boolean) this.f.a(bb.N)).booleanValue()) {
            Map a2 = ((l) this.f.getTargetingData()).a();
            if (a2 != null && !a2.isEmpty()) {
                jSONObject.put("targeting", au.a(a2));
            }
            jSONObject.put("stats", this.f.b().b());
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("Repeat");
        sb.append(this.e);
        bj bjVar = new bj(this, sb.toString(), bb.g, this.f, jSONObject);
        bjVar.a(bb.k);
        bjVar.run();
    }

    public void run() {
        try {
            this.g.i(this.e, "Submitting user data...");
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            c(jSONObject);
        } catch (JSONException e) {
            this.g.e(this.e, "Unable to create JSON message with collected data", e);
        }
    }
}
