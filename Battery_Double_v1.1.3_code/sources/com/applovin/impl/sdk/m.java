package com.applovin.impl.sdk;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public class m {
    public static Point a(Context context) {
        Point point = new Point();
        point.x = 480;
        point.y = ModuleDescriptor.MODULE_VERSION;
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            if (VERSION.SDK_INT >= 13) {
                defaultDisplay.getSize(point);
                return point;
            }
            point.x = defaultDisplay.getWidth();
            point.y = defaultDisplay.getHeight();
            return point;
        } catch (Throwable unused) {
        }
    }

    static void a() {
        try {
            if (VERSION.SDK_INT >= 9) {
                StrictMode.setThreadPolicy(new Builder().permitAll().build());
            }
        } catch (Throwable unused) {
        }
    }

    public static boolean a(Class cls, Context context) {
        return context.getPackageManager().resolveActivity(new Intent(context, cls), 0) != null;
    }

    public static boolean a(String str, Context context) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static boolean b() {
        return VERSION.SDK_INT >= 15;
    }
}
