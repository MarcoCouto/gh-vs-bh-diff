package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import java.util.Collection;
import java.util.HashSet;

class h {
    final AppLovinAdSize a;
    final Object b;
    AppLovinAd c;
    long d;
    boolean e;
    /* access modifiers changed from: private */
    public final Collection f;
    /* access modifiers changed from: private */
    public final Collection g;

    private h(AppLovinAdSize appLovinAdSize) {
        this.f = new HashSet();
        this.g = new HashSet();
        this.a = appLovinAdSize;
        this.b = new Object();
        this.c = null;
        this.d = 0;
        this.e = false;
    }
}
