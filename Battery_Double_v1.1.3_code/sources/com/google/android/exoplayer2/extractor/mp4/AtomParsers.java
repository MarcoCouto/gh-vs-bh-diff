package com.google.android.exoplayer2.extractor.mp4;

import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.audio.Ac3Util;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.GaplessInfoHolder;
import com.google.android.exoplayer2.extractor.mp4.FixedSampleSizeRechunker.Results;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.Metadata.Entry;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.AvcConfig;
import com.google.android.exoplayer2.video.HevcConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class AtomParsers {
    private static final String TAG = "AtomParsers";
    private static final int TYPE_cenc = Util.getIntegerCodeForString("cenc");
    private static final int TYPE_clcp = Util.getIntegerCodeForString("clcp");
    private static final int TYPE_meta = Util.getIntegerCodeForString("meta");
    private static final int TYPE_sbtl = Util.getIntegerCodeForString("sbtl");
    private static final int TYPE_soun = Util.getIntegerCodeForString("soun");
    private static final int TYPE_subt = Util.getIntegerCodeForString("subt");
    private static final int TYPE_text = Util.getIntegerCodeForString(MimeTypes.BASE_TYPE_TEXT);
    private static final int TYPE_vide = Util.getIntegerCodeForString("vide");

    private static final class ChunkIterator {
        private final ParsableByteArray chunkOffsets;
        private final boolean chunkOffsetsAreLongs;
        public int index;
        public final int length;
        private int nextSamplesPerChunkChangeIndex;
        public int numSamples;
        public long offset;
        private int remainingSamplesPerChunkChanges;
        private final ParsableByteArray stsc;

        public ChunkIterator(ParsableByteArray parsableByteArray, ParsableByteArray parsableByteArray2, boolean z) {
            this.stsc = parsableByteArray;
            this.chunkOffsets = parsableByteArray2;
            this.chunkOffsetsAreLongs = z;
            parsableByteArray2.setPosition(12);
            this.length = parsableByteArray2.readUnsignedIntToInt();
            parsableByteArray.setPosition(12);
            this.remainingSamplesPerChunkChanges = parsableByteArray.readUnsignedIntToInt();
            boolean z2 = true;
            if (parsableByteArray.readInt() != 1) {
                z2 = false;
            }
            Assertions.checkState(z2, "first_chunk must be 1");
            this.index = -1;
        }

        public boolean moveNext() {
            long j;
            int i = this.index + 1;
            this.index = i;
            if (i == this.length) {
                return false;
            }
            if (this.chunkOffsetsAreLongs) {
                j = this.chunkOffsets.readUnsignedLongToLong();
            } else {
                j = this.chunkOffsets.readUnsignedInt();
            }
            this.offset = j;
            if (this.index == this.nextSamplesPerChunkChangeIndex) {
                this.numSamples = this.stsc.readUnsignedIntToInt();
                this.stsc.skipBytes(4);
                int i2 = this.remainingSamplesPerChunkChanges - 1;
                this.remainingSamplesPerChunkChanges = i2;
                this.nextSamplesPerChunkChangeIndex = i2 > 0 ? this.stsc.readUnsignedIntToInt() - 1 : -1;
            }
            return true;
        }
    }

    private interface SampleSizeBox {
        int getSampleCount();

        boolean isFixedSampleSize();

        int readNextSampleSize();
    }

    private static final class StsdData {
        public static final int STSD_HEADER_SIZE = 8;
        public Format format;
        public int nalUnitLengthFieldLength;
        public int requiredSampleTransformation = 0;
        public final TrackEncryptionBox[] trackEncryptionBoxes;

        public StsdData(int i) {
            this.trackEncryptionBoxes = new TrackEncryptionBox[i];
        }
    }

    static final class StszSampleSizeBox implements SampleSizeBox {
        private final ParsableByteArray data;
        private final int fixedSampleSize = this.data.readUnsignedIntToInt();
        private final int sampleCount = this.data.readUnsignedIntToInt();

        public StszSampleSizeBox(LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public int readNextSampleSize() {
            return this.fixedSampleSize == 0 ? this.data.readUnsignedIntToInt() : this.fixedSampleSize;
        }

        public boolean isFixedSampleSize() {
            return this.fixedSampleSize != 0;
        }
    }

    static final class Stz2SampleSizeBox implements SampleSizeBox {
        private int currentByte;
        private final ParsableByteArray data;
        private final int fieldSize = (this.data.readUnsignedIntToInt() & 255);
        private final int sampleCount = this.data.readUnsignedIntToInt();
        private int sampleIndex;

        public boolean isFixedSampleSize() {
            return false;
        }

        public Stz2SampleSizeBox(LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public int readNextSampleSize() {
            if (this.fieldSize == 8) {
                return this.data.readUnsignedByte();
            }
            if (this.fieldSize == 16) {
                return this.data.readUnsignedShort();
            }
            int i = this.sampleIndex;
            this.sampleIndex = i + 1;
            if (i % 2 != 0) {
                return this.currentByte & 15;
            }
            this.currentByte = this.data.readUnsignedByte();
            return (this.currentByte & PsExtractor.VIDEO_STREAM_MASK) >> 4;
        }
    }

    private static final class TkhdData {
        /* access modifiers changed from: private */
        public final long duration;
        /* access modifiers changed from: private */
        public final int id;
        /* access modifiers changed from: private */
        public final int rotationDegrees;

        public TkhdData(int i, long j, int i2) {
            this.id = i;
            this.duration = j;
            this.rotationDegrees = i2;
        }
    }

    public static Track parseTrak(ContainerAtom containerAtom, LeafAtom leafAtom, long j, DrmInitData drmInitData, boolean z) throws ParserException {
        long j2;
        LeafAtom leafAtom2;
        ContainerAtom containerAtom2 = containerAtom;
        ContainerAtom containerAtomOfType = containerAtom2.getContainerAtomOfType(Atom.TYPE_mdia);
        int parseHdlr = parseHdlr(containerAtomOfType.getLeafAtomOfType(Atom.TYPE_hdlr).data);
        Track track = null;
        if (parseHdlr == -1) {
            return null;
        }
        TkhdData parseTkhd = parseTkhd(containerAtom2.getLeafAtomOfType(Atom.TYPE_tkhd).data);
        long j3 = C.TIME_UNSET;
        if (j == C.TIME_UNSET) {
            j2 = parseTkhd.duration;
            leafAtom2 = leafAtom;
        } else {
            leafAtom2 = leafAtom;
            j2 = j;
        }
        long parseMvhd = parseMvhd(leafAtom2.data);
        if (j2 != C.TIME_UNSET) {
            j3 = Util.scaleLargeTimestamp(j2, C.MICROS_PER_SECOND, parseMvhd);
        }
        long j4 = j3;
        ContainerAtom containerAtomOfType2 = containerAtomOfType.getContainerAtomOfType(Atom.TYPE_minf).getContainerAtomOfType(Atom.TYPE_stbl);
        Pair parseMdhd = parseMdhd(containerAtomOfType.getLeafAtomOfType(Atom.TYPE_mdhd).data);
        StsdData parseStsd = parseStsd(containerAtomOfType2.getLeafAtomOfType(Atom.TYPE_stsd).data, parseTkhd.id, parseTkhd.rotationDegrees, (String) parseMdhd.second, drmInitData, z);
        Pair parseEdts = parseEdts(containerAtom2.getContainerAtomOfType(Atom.TYPE_edts));
        if (parseStsd.format != null) {
            int access$100 = parseTkhd.id;
            long longValue = ((Long) parseMdhd.first).longValue();
            Format format = parseStsd.format;
            int i = parseStsd.requiredSampleTransformation;
            TrackEncryptionBox[] trackEncryptionBoxArr = parseStsd.trackEncryptionBoxes;
            int i2 = parseStsd.nalUnitLengthFieldLength;
            track = new Track(access$100, parseHdlr, longValue, parseMvhd, j4, format, i, trackEncryptionBoxArr, i2, (long[]) parseEdts.first, (long[]) parseEdts.second);
        }
        return track;
    }

    public static TrackSampleTable parseStbl(Track track, ContainerAtom containerAtom, GaplessInfoHolder gaplessInfoHolder) throws ParserException {
        SampleSizeBox sampleSizeBox;
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int[] iArr;
        long[] jArr;
        int[] iArr2;
        long[] jArr2;
        long j;
        long j2;
        int[] iArr3;
        long[] jArr3;
        boolean z2;
        int i5;
        long[] jArr4;
        long[] jArr5;
        int[] iArr4;
        int[] iArr5;
        int[] iArr6;
        long[] jArr6;
        int[] iArr7;
        ParsableByteArray parsableByteArray;
        Track track2 = track;
        ContainerAtom containerAtom2 = containerAtom;
        GaplessInfoHolder gaplessInfoHolder2 = gaplessInfoHolder;
        LeafAtom leafAtomOfType = containerAtom2.getLeafAtomOfType(Atom.TYPE_stsz);
        if (leafAtomOfType != null) {
            sampleSizeBox = new StszSampleSizeBox(leafAtomOfType);
        } else {
            LeafAtom leafAtomOfType2 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stz2);
            if (leafAtomOfType2 == null) {
                throw new ParserException("Track has no sample table size information");
            }
            sampleSizeBox = new Stz2SampleSizeBox(leafAtomOfType2);
        }
        int sampleCount = sampleSizeBox.getSampleCount();
        if (sampleCount == 0) {
            TrackSampleTable trackSampleTable = new TrackSampleTable(new long[0], new int[0], 0, new long[0], new int[0]);
            return trackSampleTable;
        }
        LeafAtom leafAtomOfType3 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stco);
        if (leafAtomOfType3 == null) {
            leafAtomOfType3 = containerAtom2.getLeafAtomOfType(Atom.TYPE_co64);
            z = true;
        } else {
            z = false;
        }
        ParsableByteArray parsableByteArray2 = leafAtomOfType3.data;
        ParsableByteArray parsableByteArray3 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stsc).data;
        ParsableByteArray parsableByteArray4 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stts).data;
        LeafAtom leafAtomOfType4 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stss);
        ParsableByteArray parsableByteArray5 = leafAtomOfType4 != null ? leafAtomOfType4.data : null;
        LeafAtom leafAtomOfType5 = containerAtom2.getLeafAtomOfType(Atom.TYPE_ctts);
        ParsableByteArray parsableByteArray6 = leafAtomOfType5 != null ? leafAtomOfType5.data : null;
        ChunkIterator chunkIterator = new ChunkIterator(parsableByteArray3, parsableByteArray2, z);
        parsableByteArray4.setPosition(12);
        int readUnsignedIntToInt = parsableByteArray4.readUnsignedIntToInt() - 1;
        int readUnsignedIntToInt2 = parsableByteArray4.readUnsignedIntToInt();
        int readUnsignedIntToInt3 = parsableByteArray4.readUnsignedIntToInt();
        if (parsableByteArray6 != null) {
            parsableByteArray6.setPosition(12);
            i = parsableByteArray6.readUnsignedIntToInt();
        } else {
            i = 0;
        }
        int i6 = -1;
        if (parsableByteArray5 != null) {
            parsableByteArray5.setPosition(12);
            i2 = parsableByteArray5.readUnsignedIntToInt();
            if (i2 > 0) {
                i6 = parsableByteArray5.readUnsignedIntToInt() - 1;
            } else {
                parsableByteArray5 = null;
            }
        } else {
            i2 = 0;
        }
        long j3 = 0;
        if (!(sampleSizeBox.isFixedSampleSize() && MimeTypes.AUDIO_RAW.equals(track2.format.sampleMimeType) && readUnsignedIntToInt == 0 && i == 0 && i2 == 0)) {
            jArr2 = new long[sampleCount];
            iArr = new int[sampleCount];
            jArr = new long[sampleCount];
            int i7 = i2;
            iArr2 = new int[sampleCount];
            ParsableByteArray parsableByteArray7 = parsableByteArray4;
            int i8 = i6;
            long j4 = 0;
            long j5 = 0;
            int i9 = i7;
            int i10 = 0;
            int i11 = 0;
            int i12 = 0;
            int i13 = readUnsignedIntToInt;
            int i14 = i;
            int i15 = 0;
            int i16 = 0;
            int i17 = readUnsignedIntToInt3;
            int i18 = readUnsignedIntToInt2;
            int i19 = i17;
            while (i15 < sampleCount) {
                while (i11 == 0) {
                    int i20 = sampleCount;
                    Assertions.checkState(chunkIterator.moveNext());
                    int i21 = i19;
                    int i22 = i13;
                    long j6 = chunkIterator.offset;
                    i11 = chunkIterator.numSamples;
                    j4 = j6;
                    sampleCount = i20;
                    i19 = i21;
                    i13 = i22;
                }
                int i23 = sampleCount;
                int i24 = i19;
                int i25 = i13;
                if (parsableByteArray6 != null) {
                    while (i12 == 0 && i14 > 0) {
                        i12 = parsableByteArray6.readUnsignedIntToInt();
                        i10 = parsableByteArray6.readInt();
                        i14--;
                    }
                    i12--;
                }
                int i26 = i10;
                jArr2[i15] = j4;
                iArr[i15] = sampleSizeBox.readNextSampleSize();
                if (iArr[i15] > i16) {
                    i16 = iArr[i15];
                }
                jArr[i15] = j5 + ((long) i26);
                iArr2[i15] = parsableByteArray5 == null ? 1 : 0;
                if (i15 == i8) {
                    iArr2[i15] = 1;
                    i9--;
                    if (i9 > 0) {
                        i8 = parsableByteArray5.readUnsignedIntToInt() - 1;
                    }
                }
                int i27 = i9;
                int i28 = i8;
                int i29 = i26;
                int i30 = i24;
                long j7 = j5 + ((long) i30);
                i18--;
                if (i18 != 0 || i25 <= 0) {
                    parsableByteArray = parsableByteArray7;
                    i13 = i25;
                } else {
                    parsableByteArray = parsableByteArray7;
                    i13 = i25 - 1;
                    i18 = parsableByteArray.readUnsignedIntToInt();
                    i30 = parsableByteArray.readUnsignedIntToInt();
                }
                ParsableByteArray parsableByteArray8 = parsableByteArray;
                i11--;
                i15++;
                j4 += (long) iArr[i15];
                sampleCount = i23;
                j5 = j7;
                i10 = i29;
                i8 = i28;
                parsableByteArray7 = parsableByteArray8;
                int i31 = i27;
                i19 = i30;
                i9 = i31;
            }
            i3 = sampleCount;
            int i32 = i13;
            Assertions.checkArgument(i12 == 0);
            while (i14 > 0) {
                Assertions.checkArgument(parsableByteArray6.readUnsignedIntToInt() == 0);
                parsableByteArray6.readInt();
                i14--;
            }
            if (i9 == 0 && i18 == 0 && i11 == 0 && i32 == 0) {
                track2 = track;
            } else {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Inconsistent stbl box for track ");
                int i33 = i9;
                track2 = track;
                sb.append(track2.id);
                sb.append(": remainingSynchronizationSamples ");
                sb.append(i33);
                sb.append(", remainingSamplesAtTimestampDelta ");
                sb.append(i18);
                sb.append(", remainingSamplesInChunk ");
                sb.append(i11);
                sb.append(", remainingTimestampDeltaChanges ");
                sb.append(i32);
                Log.w(str, sb.toString());
            }
            j = j5;
            i4 = i16;
        } else {
            i3 = sampleCount;
            long[] jArr7 = new long[chunkIterator.length];
            int[] iArr8 = new int[chunkIterator.length];
            while (chunkIterator.moveNext()) {
                jArr7[chunkIterator.index] = chunkIterator.offset;
                iArr8[chunkIterator.index] = chunkIterator.numSamples;
            }
            Results rechunk = FixedSampleSizeRechunker.rechunk(sampleSizeBox.readNextSampleSize(), jArr7, iArr8, (long) readUnsignedIntToInt3);
            jArr2 = rechunk.offsets;
            iArr = rechunk.sizes;
            int i34 = rechunk.maximumSize;
            jArr = rechunk.timestamps;
            iArr2 = rechunk.flags;
            i4 = i34;
            j = 0;
        }
        if (track2.editListDurations != null) {
            GaplessInfoHolder gaplessInfoHolder3 = gaplessInfoHolder;
            if (!gaplessInfoHolder.hasGaplessInfo()) {
                if (track2.editListDurations.length == 1 && track2.type == 1 && jArr.length >= 2) {
                    long j8 = track2.editListMediaTimes[0];
                    long scaleLargeTimestamp = j8 + Util.scaleLargeTimestamp(track2.editListDurations[0], track2.timescale, track2.movieTimescale);
                    if (jArr[0] <= j8 && j8 < jArr[1] && jArr[jArr.length - 1] < scaleLargeTimestamp && scaleLargeTimestamp <= j) {
                        long j9 = j - scaleLargeTimestamp;
                        long scaleLargeTimestamp2 = Util.scaleLargeTimestamp(j8 - jArr[0], (long) track2.format.sampleRate, track2.timescale);
                        long scaleLargeTimestamp3 = Util.scaleLargeTimestamp(j9, (long) track2.format.sampleRate, track2.timescale);
                        if (!(scaleLargeTimestamp2 == 0 && scaleLargeTimestamp3 == 0) && scaleLargeTimestamp2 <= 2147483647L && scaleLargeTimestamp3 <= 2147483647L) {
                            gaplessInfoHolder3.encoderDelay = (int) scaleLargeTimestamp2;
                            gaplessInfoHolder3.encoderPadding = (int) scaleLargeTimestamp3;
                            Util.scaleLargeTimestampsInPlace(jArr, C.MICROS_PER_SECOND, track2.timescale);
                            TrackSampleTable trackSampleTable2 = new TrackSampleTable(jArr2, iArr, i4, jArr, iArr2);
                            return trackSampleTable2;
                        }
                    }
                }
                if (track2.editListDurations.length == 1) {
                    char c = 0;
                    if (track2.editListDurations[0] == 0) {
                        int i35 = 0;
                        while (i35 < jArr.length) {
                            jArr[i35] = Util.scaleLargeTimestamp(jArr[i35] - track2.editListMediaTimes[c], C.MICROS_PER_SECOND, track2.timescale);
                            i35++;
                            c = 0;
                        }
                        TrackSampleTable trackSampleTable3 = new TrackSampleTable(jArr2, iArr, i4, jArr, iArr2);
                        return trackSampleTable3;
                    }
                }
                boolean z3 = track2.type == 1;
                int i36 = 0;
                boolean z4 = false;
                int i37 = 0;
                int i38 = 0;
                while (true) {
                    j2 = -1;
                    if (i36 >= track2.editListDurations.length) {
                        break;
                    }
                    long j10 = track2.editListMediaTimes[i36];
                    if (j10 != -1) {
                        iArr7 = iArr;
                        long scaleLargeTimestamp4 = Util.scaleLargeTimestamp(track2.editListDurations[i36], track2.timescale, track2.movieTimescale);
                        int binarySearchCeil = Util.binarySearchCeil(jArr, j10, true, true);
                        jArr6 = jArr2;
                        iArr6 = iArr2;
                        int binarySearchCeil2 = Util.binarySearchCeil(jArr, j10 + scaleLargeTimestamp4, z3, false);
                        i37 += binarySearchCeil2 - binarySearchCeil;
                        z4 |= i38 != binarySearchCeil;
                        i38 = binarySearchCeil2;
                    } else {
                        jArr6 = jArr2;
                        iArr6 = iArr2;
                        iArr7 = iArr;
                    }
                    i36++;
                    iArr = iArr7;
                    jArr2 = jArr6;
                    iArr2 = iArr6;
                }
                long[] jArr8 = jArr2;
                int[] iArr9 = iArr2;
                int[] iArr10 = iArr;
                boolean z5 = (i37 != i3) | z4;
                long[] jArr9 = z5 ? new long[i37] : jArr8;
                int[] iArr11 = z5 ? new int[i37] : iArr10;
                if (z5) {
                    i4 = 0;
                }
                int[] iArr12 = z5 ? new int[i37] : iArr9;
                long[] jArr10 = new long[i37];
                int i39 = i4;
                int i40 = 0;
                int i41 = 0;
                while (i40 < track2.editListDurations.length) {
                    long j11 = track2.editListMediaTimes[i40];
                    long j12 = track2.editListDurations[i40];
                    if (j11 != j2) {
                        jArr4 = jArr10;
                        i5 = i40;
                        long scaleLargeTimestamp5 = j11 + Util.scaleLargeTimestamp(j12, track2.timescale, track2.movieTimescale);
                        int binarySearchCeil3 = Util.binarySearchCeil(jArr, j11, true, true);
                        int binarySearchCeil4 = Util.binarySearchCeil(jArr, scaleLargeTimestamp5, z3, false);
                        if (z5) {
                            int i42 = binarySearchCeil4 - binarySearchCeil3;
                            jArr5 = jArr8;
                            System.arraycopy(jArr5, binarySearchCeil3, jArr9, i41, i42);
                            iArr4 = iArr10;
                            System.arraycopy(iArr4, binarySearchCeil3, iArr11, i41, i42);
                            z2 = z3;
                            iArr5 = iArr9;
                            System.arraycopy(iArr5, binarySearchCeil3, iArr12, i41, i42);
                        } else {
                            z2 = z3;
                            iArr4 = iArr10;
                            jArr5 = jArr8;
                            iArr5 = iArr9;
                        }
                        int i43 = i39;
                        while (binarySearchCeil3 < binarySearchCeil4) {
                            long[] jArr11 = jArr9;
                            int[] iArr13 = iArr12;
                            long j13 = j11;
                            jArr4[i41] = Util.scaleLargeTimestamp(j3, C.MICROS_PER_SECOND, track2.movieTimescale) + Util.scaleLargeTimestamp(jArr[binarySearchCeil3] - j11, C.MICROS_PER_SECOND, track2.timescale);
                            if (z5 && iArr11[i41] > i43) {
                                i43 = iArr4[binarySearchCeil3];
                            }
                            i41++;
                            binarySearchCeil3++;
                            jArr9 = jArr11;
                            iArr12 = iArr13;
                            j11 = j13;
                        }
                        jArr3 = jArr9;
                        iArr3 = iArr12;
                        i39 = i43;
                    } else {
                        z2 = z3;
                        jArr4 = jArr10;
                        i5 = i40;
                        jArr3 = jArr9;
                        iArr3 = iArr12;
                        iArr4 = iArr10;
                        jArr5 = jArr8;
                        iArr5 = iArr9;
                    }
                    iArr9 = iArr5;
                    iArr10 = iArr4;
                    j3 += j12;
                    i40 = i5 + 1;
                    jArr8 = jArr5;
                    jArr10 = jArr4;
                    z3 = z2;
                    jArr9 = jArr3;
                    iArr12 = iArr3;
                    j2 = -1;
                }
                long[] jArr12 = jArr10;
                long[] jArr13 = jArr9;
                boolean z6 = false;
                for (int i44 = 0; i44 < iArr12.length && !z6; i44++) {
                    z6 |= (iArr12[i44] & 1) != 0;
                }
                if (!z6) {
                    throw new ParserException("The edited sample sequence does not contain a sync sample.");
                }
                TrackSampleTable trackSampleTable4 = new TrackSampleTable(jArr13, iArr11, i39, jArr12, iArr12);
                return trackSampleTable4;
            }
        }
        long[] jArr14 = jArr2;
        int[] iArr14 = iArr2;
        int[] iArr15 = iArr;
        Util.scaleLargeTimestampsInPlace(jArr, C.MICROS_PER_SECOND, track2.timescale);
        TrackSampleTable trackSampleTable5 = new TrackSampleTable(jArr14, iArr15, i4, jArr, iArr14);
        return trackSampleTable5;
    }

    public static Metadata parseUdta(LeafAtom leafAtom, boolean z) {
        if (z) {
            return null;
        }
        ParsableByteArray parsableByteArray = leafAtom.data;
        parsableByteArray.setPosition(8);
        while (parsableByteArray.bytesLeft() >= 8) {
            int position = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_meta) {
                parsableByteArray.setPosition(position);
                return parseMetaAtom(parsableByteArray, position + readInt);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    private static Metadata parseMetaAtom(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.skipBytes(12);
        while (parsableByteArray.getPosition() < i) {
            int position = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_ilst) {
                parsableByteArray.setPosition(position);
                return parseIlst(parsableByteArray, position + readInt);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    private static Metadata parseIlst(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.skipBytes(8);
        ArrayList arrayList = new ArrayList();
        while (parsableByteArray.getPosition() < i) {
            Entry parseIlstElement = MetadataUtil.parseIlstElement(parsableByteArray);
            if (parseIlstElement != null) {
                arrayList.add(parseIlstElement);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata((List<? extends Entry>) arrayList);
    }

    private static long parseMvhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        if (Atom.parseFullAtomVersion(parsableByteArray.readInt()) != 0) {
            i = 16;
        }
        parsableByteArray.skipBytes(i);
        return parsableByteArray.readUnsignedInt();
    }

    private static TkhdData parseTkhd(ParsableByteArray parsableByteArray) {
        boolean z;
        int i = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        int readInt = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        int position = parsableByteArray.getPosition();
        if (parseFullAtomVersion == 0) {
            i = 4;
        }
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i3 >= i) {
                z = true;
                break;
            } else if (parsableByteArray.data[position + i3] != -1) {
                z = false;
                break;
            } else {
                i3++;
            }
        }
        long j = C.TIME_UNSET;
        if (z) {
            parsableByteArray.skipBytes(i);
        } else {
            long readUnsignedInt = parseFullAtomVersion == 0 ? parsableByteArray.readUnsignedInt() : parsableByteArray.readUnsignedLongToLong();
            if (readUnsignedInt != 0) {
                j = readUnsignedInt;
            }
        }
        parsableByteArray.skipBytes(16);
        int readInt2 = parsableByteArray.readInt();
        int readInt3 = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        int readInt4 = parsableByteArray.readInt();
        int readInt5 = parsableByteArray.readInt();
        if (readInt2 == 0 && readInt3 == 65536 && readInt4 == -65536 && readInt5 == 0) {
            i2 = 90;
        } else if (readInt2 == 0 && readInt3 == -65536 && readInt4 == 65536 && readInt5 == 0) {
            i2 = 270;
        } else if (readInt2 == -65536 && readInt3 == 0 && readInt4 == 0 && readInt5 == -65536) {
            i2 = 180;
        }
        return new TkhdData(readInt, j, i2);
    }

    private static int parseHdlr(ParsableByteArray parsableByteArray) {
        parsableByteArray.setPosition(16);
        int readInt = parsableByteArray.readInt();
        if (readInt == TYPE_soun) {
            return 1;
        }
        if (readInt == TYPE_vide) {
            return 2;
        }
        if (readInt == TYPE_text || readInt == TYPE_sbtl || readInt == TYPE_subt || readInt == TYPE_clcp) {
            return 3;
        }
        return readInt == TYPE_meta ? 4 : -1;
    }

    private static Pair<Long, String> parseMdhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        long readUnsignedInt = parsableByteArray.readUnsignedInt();
        if (parseFullAtomVersion == 0) {
            i = 4;
        }
        parsableByteArray.skipBytes(i);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append((char) (((readUnsignedShort >> 10) & 31) + 96));
        sb.append((char) (((readUnsignedShort >> 5) & 31) + 96));
        sb.append((char) ((readUnsignedShort & 31) + 96));
        return Pair.create(Long.valueOf(readUnsignedInt), sb.toString());
    }

    private static StsdData parseStsd(ParsableByteArray parsableByteArray, int i, int i2, String str, DrmInitData drmInitData, boolean z) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        parsableByteArray2.setPosition(12);
        int readInt = parsableByteArray.readInt();
        StsdData stsdData = new StsdData(readInt);
        for (int i3 = 0; i3 < readInt; i3++) {
            int position = parsableByteArray.getPosition();
            int readInt2 = parsableByteArray.readInt();
            Assertions.checkArgument(readInt2 > 0, "childAtomSize should be positive");
            int readInt3 = parsableByteArray.readInt();
            if (readInt3 == Atom.TYPE_avc1 || readInt3 == Atom.TYPE_avc3 || readInt3 == Atom.TYPE_encv || readInt3 == Atom.TYPE_mp4v || readInt3 == Atom.TYPE_hvc1 || readInt3 == Atom.TYPE_hev1 || readInt3 == Atom.TYPE_s263 || readInt3 == Atom.TYPE_vp08 || readInt3 == Atom.TYPE_vp09) {
                parseVideoSampleEntry(parsableByteArray2, readInt3, position, readInt2, i, i2, drmInitData, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_mp4a || readInt3 == Atom.TYPE_enca || readInt3 == Atom.TYPE_ac_3 || readInt3 == Atom.TYPE_ec_3 || readInt3 == Atom.TYPE_dtsc || readInt3 == Atom.TYPE_dtse || readInt3 == Atom.TYPE_dtsh || readInt3 == Atom.TYPE_dtsl || readInt3 == Atom.TYPE_samr || readInt3 == Atom.TYPE_sawb || readInt3 == Atom.TYPE_lpcm || readInt3 == Atom.TYPE_sowt || readInt3 == Atom.TYPE__mp3 || readInt3 == Atom.TYPE_alac) {
                parseAudioSampleEntry(parsableByteArray2, readInt3, position, readInt2, i, str, z, drmInitData, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_TTML || readInt3 == Atom.TYPE_tx3g || readInt3 == Atom.TYPE_wvtt || readInt3 == Atom.TYPE_stpp || readInt3 == Atom.TYPE_c608) {
                parseTextSampleEntry(parsableByteArray2, readInt3, position, readInt2, i, str, drmInitData, stsdData);
            } else if (readInt3 == Atom.TYPE_camm) {
                stsdData.format = Format.createSampleFormat(Integer.toString(i), MimeTypes.APPLICATION_CAMERA_MOTION, null, -1, drmInitData);
            } else {
                DrmInitData drmInitData2 = drmInitData;
            }
            parsableByteArray2.setPosition(position + readInt2);
        }
        return stsdData;
    }

    private static void parseTextSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, String str, DrmInitData drmInitData, StsdData stsdData) throws ParserException {
        String str2;
        String str3;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i5 = i;
        StsdData stsdData2 = stsdData;
        parsableByteArray2.setPosition(i2 + 8 + 8);
        List list = null;
        long j = Long.MAX_VALUE;
        if (i5 == Atom.TYPE_TTML) {
            str2 = MimeTypes.APPLICATION_TTML;
        } else if (i5 == Atom.TYPE_tx3g) {
            String str4 = MimeTypes.APPLICATION_TX3G;
            int i6 = (i3 - 8) - 8;
            byte[] bArr = new byte[i6];
            parsableByteArray2.readBytes(bArr, 0, i6);
            list = Collections.singletonList(bArr);
            str3 = str4;
            stsdData2.format = Format.createTextSampleFormat(Integer.toString(i4), str3, null, -1, 0, str, -1, drmInitData, j, list);
        } else if (i5 == Atom.TYPE_wvtt) {
            str2 = MimeTypes.APPLICATION_MP4VTT;
        } else if (i5 == Atom.TYPE_stpp) {
            str2 = MimeTypes.APPLICATION_TTML;
            j = 0;
        } else if (i5 == Atom.TYPE_c608) {
            str2 = MimeTypes.APPLICATION_MP4CEA608;
            stsdData2.requiredSampleTransformation = 1;
        } else {
            throw new IllegalStateException();
        }
        str3 = str2;
        stsdData2.format = Format.createTextSampleFormat(Integer.toString(i4), str3, null, -1, 0, str, -1, drmInitData, j, list);
    }

    /* JADX WARNING: Removed duplicated region for block: B:68:0x012a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x012b  */
    private static void parseVideoSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, int i5, DrmInitData drmInitData, StsdData stsdData, int i6) throws ParserException {
        int i7;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i8 = i2;
        int i9 = i3;
        StsdData stsdData2 = stsdData;
        parsableByteArray2.setPosition(i8 + 8 + 8);
        parsableByteArray2.skipBytes(16);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        int readUnsignedShort2 = parsableByteArray.readUnsignedShort();
        parsableByteArray2.skipBytes(50);
        int position = parsableByteArray.getPosition();
        int i10 = i;
        if (i10 == Atom.TYPE_encv) {
            i7 = parseSampleEntryEncryptionData(parsableByteArray2, i8, i9, stsdData2, i6);
            parsableByteArray2.setPosition(position);
        } else {
            i7 = i10;
        }
        int i11 = -1;
        String str = null;
        List list = null;
        byte[] bArr = null;
        float f = 1.0f;
        boolean z = false;
        while (position - i8 < i9) {
            parsableByteArray2.setPosition(position);
            int position2 = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (readInt != 0 || parsableByteArray.getPosition() - i8 != i9) {
                Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
                int readInt2 = parsableByteArray.readInt();
                if (readInt2 == Atom.TYPE_avcC) {
                    Assertions.checkState(str == null);
                    str = MimeTypes.VIDEO_H264;
                    parsableByteArray2.setPosition(position2 + 8);
                    AvcConfig parse = AvcConfig.parse(parsableByteArray);
                    list = parse.initializationData;
                    stsdData2.nalUnitLengthFieldLength = parse.nalUnitLengthFieldLength;
                    if (!z) {
                        f = parse.pixelWidthAspectRatio;
                    }
                } else if (readInt2 == Atom.TYPE_hvcC) {
                    Assertions.checkState(str == null);
                    str = MimeTypes.VIDEO_H265;
                    parsableByteArray2.setPosition(position2 + 8);
                    HevcConfig parse2 = HevcConfig.parse(parsableByteArray);
                    list = parse2.initializationData;
                    stsdData2.nalUnitLengthFieldLength = parse2.nalUnitLengthFieldLength;
                } else if (readInt2 == Atom.TYPE_vpcC) {
                    Assertions.checkState(str == null);
                    str = i7 == Atom.TYPE_vp08 ? MimeTypes.VIDEO_VP8 : MimeTypes.VIDEO_VP9;
                } else if (readInt2 == Atom.TYPE_d263) {
                    Assertions.checkState(str == null);
                    str = MimeTypes.VIDEO_H263;
                } else if (readInt2 == Atom.TYPE_esds) {
                    Assertions.checkState(str == null);
                    Pair parseEsdsFromParent = parseEsdsFromParent(parsableByteArray2, position2);
                    str = (String) parseEsdsFromParent.first;
                    list = Collections.singletonList(parseEsdsFromParent.second);
                } else if (readInt2 == Atom.TYPE_pasp) {
                    f = parsePaspFromParent(parsableByteArray2, position2);
                    z = true;
                } else if (readInt2 == Atom.TYPE_sv3d) {
                    bArr = parseProjFromParent(parsableByteArray2, position2, readInt);
                } else if (readInt2 == Atom.TYPE_st3d) {
                    int readUnsignedByte = parsableByteArray.readUnsignedByte();
                    parsableByteArray2.skipBytes(3);
                    if (readUnsignedByte == 0) {
                        switch (parsableByteArray.readUnsignedByte()) {
                            case 0:
                                i11 = 0;
                                break;
                            case 1:
                                i11 = 1;
                                break;
                            case 2:
                                i11 = 2;
                                break;
                            case 3:
                                i11 = 3;
                                break;
                        }
                    }
                }
                position += readInt;
                i8 = i2;
            } else if (str == null) {
                stsdData2.format = Format.createVideoSampleFormat(Integer.toString(i4), str, null, -1, -1, readUnsignedShort, readUnsignedShort2, -1.0f, list, i5, f, bArr, i11, null, drmInitData);
                return;
            } else {
                return;
            }
        }
        if (str == null) {
        }
    }

    private static Pair<long[], long[]> parseEdts(ContainerAtom containerAtom) {
        if (containerAtom != null) {
            LeafAtom leafAtomOfType = containerAtom.getLeafAtomOfType(Atom.TYPE_elst);
            if (leafAtomOfType != null) {
                ParsableByteArray parsableByteArray = leafAtomOfType.data;
                parsableByteArray.setPosition(8);
                int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
                int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
                long[] jArr = new long[readUnsignedIntToInt];
                long[] jArr2 = new long[readUnsignedIntToInt];
                for (int i = 0; i < readUnsignedIntToInt; i++) {
                    jArr[i] = parseFullAtomVersion == 1 ? parsableByteArray.readUnsignedLongToLong() : parsableByteArray.readUnsignedInt();
                    jArr2[i] = parseFullAtomVersion == 1 ? parsableByteArray.readLong() : (long) parsableByteArray.readInt();
                    if (parsableByteArray.readShort() != 1) {
                        throw new IllegalArgumentException("Unsupported media rate.");
                    }
                    parsableByteArray.skipBytes(2);
                }
                return Pair.create(jArr, jArr2);
            }
        }
        return Pair.create(null, null);
    }

    private static float parsePaspFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8);
        return ((float) parsableByteArray.readUnsignedIntToInt()) / ((float) parsableByteArray.readUnsignedIntToInt());
    }

    private static void parseAudioSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, String str, boolean z, DrmInitData drmInitData, StsdData stsdData, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        List list;
        String str2;
        int i10;
        int i11;
        boolean z2;
        boolean z3;
        int i12;
        byte[] bArr;
        StsdData stsdData2;
        int i13;
        String str3;
        byte[] bArr2;
        int i14;
        int i15;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i16 = i2;
        int i17 = i3;
        String str4 = str;
        DrmInitData drmInitData2 = drmInitData;
        StsdData stsdData3 = stsdData;
        int i18 = 8;
        parsableByteArray2.setPosition(i16 + 8 + 8);
        boolean z4 = false;
        if (z) {
            i6 = parsableByteArray.readUnsignedShort();
            parsableByteArray2.skipBytes(6);
        } else {
            parsableByteArray2.skipBytes(8);
            i6 = 0;
        }
        int i19 = 2;
        boolean z5 = true;
        if (i6 == 0 || i6 == 1) {
            int readUnsignedShort = parsableByteArray.readUnsignedShort();
            parsableByteArray2.skipBytes(6);
            i8 = parsableByteArray.readUnsignedFixedPoint1616();
            if (i6 == 1) {
                parsableByteArray2.skipBytes(16);
            }
            i7 = readUnsignedShort;
        } else if (i6 == 2) {
            parsableByteArray2.skipBytes(16);
            i8 = (int) Math.round(parsableByteArray.readDouble());
            i7 = parsableByteArray.readUnsignedIntToInt();
            parsableByteArray2.skipBytes(20);
        } else {
            return;
        }
        int position = parsableByteArray.getPosition();
        int i20 = i;
        if (i20 == Atom.TYPE_enca) {
            i9 = parseSampleEntryEncryptionData(parsableByteArray2, i16, i17, stsdData3, i5);
            parsableByteArray2.setPosition(position);
        } else {
            i9 = i20;
        }
        String str5 = i9 == Atom.TYPE_ac_3 ? MimeTypes.AUDIO_AC3 : i9 == Atom.TYPE_ec_3 ? MimeTypes.AUDIO_E_AC3 : i9 == Atom.TYPE_dtsc ? MimeTypes.AUDIO_DTS : (i9 == Atom.TYPE_dtsh || i9 == Atom.TYPE_dtsl) ? MimeTypes.AUDIO_DTS_HD : i9 == Atom.TYPE_dtse ? MimeTypes.AUDIO_DTS_EXPRESS : i9 == Atom.TYPE_samr ? MimeTypes.AUDIO_AMR_NB : i9 == Atom.TYPE_sawb ? MimeTypes.AUDIO_AMR_WB : (i9 == Atom.TYPE_lpcm || i9 == Atom.TYPE_sowt) ? MimeTypes.AUDIO_RAW : i9 == Atom.TYPE__mp3 ? MimeTypes.AUDIO_MPEG : i9 == Atom.TYPE_alac ? MimeTypes.AUDIO_ALAC : null;
        int i21 = i8;
        int i22 = i7;
        int i23 = position;
        byte[] bArr3 = null;
        while (i23 - i16 < i17) {
            parsableByteArray2.setPosition(i23);
            int readInt = parsableByteArray.readInt();
            if (readInt <= 0) {
                z5 = z4;
            }
            Assertions.checkArgument(z5, "childAtomSize should be positive");
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_esds || (z && readInt2 == Atom.TYPE_wave)) {
                byte[] bArr4 = bArr3;
                String str6 = str5;
                i12 = i23;
                z3 = z4;
                i10 = i18;
                stsdData2 = stsdData3;
                z2 = true;
                i11 = 2;
                if (readInt2 == Atom.TYPE_esds) {
                    i13 = i12;
                } else {
                    i13 = findEsdsPosition(parsableByteArray2, i12, readInt);
                }
                if (i13 != -1) {
                    Pair parseEsdsFromParent = parseEsdsFromParent(parsableByteArray2, i13);
                    str3 = (String) parseEsdsFromParent.first;
                    bArr = (byte[]) parseEsdsFromParent.second;
                    if (MimeTypes.AUDIO_AAC.equals(str3)) {
                        Pair parseAacAudioSpecificConfig = CodecSpecificDataUtil.parseAacAudioSpecificConfig(bArr);
                        i21 = ((Integer) parseAacAudioSpecificConfig.first).intValue();
                        i22 = ((Integer) parseAacAudioSpecificConfig.second).intValue();
                    }
                } else {
                    bArr = bArr4;
                    str3 = str6;
                }
                str2 = str3;
            } else {
                if (readInt2 == Atom.TYPE_dac3) {
                    parsableByteArray2.setPosition(i18 + i23);
                    stsdData3.format = Ac3Util.parseAc3AnnexFFormat(parsableByteArray2, Integer.toString(i4), str4, drmInitData2);
                } else if (readInt2 == Atom.TYPE_dec3) {
                    parsableByteArray2.setPosition(i18 + i23);
                    stsdData3.format = Ac3Util.parseEAc3AnnexFFormat(parsableByteArray2, Integer.toString(i4), str4, drmInitData2);
                } else {
                    if (readInt2 == Atom.TYPE_ddts) {
                        i14 = readInt;
                        bArr2 = bArr3;
                        str2 = str5;
                        z2 = true;
                        i15 = i23;
                        i11 = 2;
                        i10 = i18;
                        stsdData2 = stsdData3;
                        stsdData2.format = Format.createAudioSampleFormat(Integer.toString(i4), str5, null, -1, -1, i22, i21, null, drmInitData2, 0, str4);
                    } else {
                        i14 = readInt;
                        bArr2 = bArr3;
                        str2 = str5;
                        i15 = i23;
                        i10 = i18;
                        stsdData2 = stsdData3;
                        z2 = true;
                        i11 = 2;
                        if (readInt2 == Atom.TYPE_alac) {
                            readInt = i14;
                            bArr = new byte[readInt];
                            i12 = i15;
                            parsableByteArray2.setPosition(i12);
                            z3 = false;
                            parsableByteArray2.readBytes(bArr, 0, readInt);
                        }
                    }
                    readInt = i14;
                    i12 = i15;
                    z3 = false;
                    bArr = bArr2;
                }
                bArr2 = bArr3;
                str2 = str5;
                i12 = i23;
                z3 = z4;
                i10 = i18;
                stsdData2 = stsdData3;
                z2 = true;
                i11 = 2;
                bArr = bArr2;
            }
            i23 = i12 + readInt;
            drmInitData2 = drmInitData;
            stsdData3 = stsdData2;
            bArr3 = bArr;
            z4 = z3;
            z5 = z2;
            i19 = i11;
            i18 = i10;
            str5 = str2;
            i17 = i3;
        }
        byte[] bArr5 = bArr3;
        String str7 = str5;
        int i24 = i19;
        StsdData stsdData4 = stsdData3;
        if (stsdData4.format == null) {
            String str8 = str7;
            if (str8 != null) {
                int i25 = MimeTypes.AUDIO_RAW.equals(str8) ? i24 : -1;
                String num = Integer.toString(i4);
                byte[] bArr6 = bArr5;
                if (bArr6 == null) {
                    list = null;
                } else {
                    list = Collections.singletonList(bArr6);
                }
                stsdData4.format = Format.createAudioSampleFormat(num, str8, null, -1, -1, i22, i21, i25, list, drmInitData, 0, str4);
            }
        }
    }

    private static int findEsdsPosition(ParsableByteArray parsableByteArray, int i, int i2) {
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_esds) {
                return position;
            }
            position += readInt;
        }
        return -1;
    }

    private static Pair<String, byte[]> parseEsdsFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8 + 4);
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        parsableByteArray.skipBytes(2);
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            parsableByteArray.skipBytes(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            parsableByteArray.skipBytes(parsableByteArray.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            parsableByteArray.skipBytes(2);
        }
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        String str = null;
        switch (parsableByteArray.readUnsignedByte()) {
            case 32:
                str = MimeTypes.VIDEO_MP4V;
                break;
            case 33:
                str = MimeTypes.VIDEO_H264;
                break;
            case 35:
                str = MimeTypes.VIDEO_H265;
                break;
            case 64:
            case 102:
            case 103:
            case 104:
                str = MimeTypes.AUDIO_AAC;
                break;
            case 107:
                return Pair.create(MimeTypes.AUDIO_MPEG, null);
            case 165:
                str = MimeTypes.AUDIO_AC3;
                break;
            case 166:
                str = MimeTypes.AUDIO_E_AC3;
                break;
            case 169:
            case 172:
                return Pair.create(MimeTypes.AUDIO_DTS, null);
            case 170:
            case 171:
                return Pair.create(MimeTypes.AUDIO_DTS_HD, null);
        }
        parsableByteArray.skipBytes(12);
        parsableByteArray.skipBytes(1);
        int parseExpandableClassSize = parseExpandableClassSize(parsableByteArray);
        byte[] bArr = new byte[parseExpandableClassSize];
        parsableByteArray.readBytes(bArr, 0, parseExpandableClassSize);
        return Pair.create(str, bArr);
    }

    private static int parseSampleEntryEncryptionData(ParsableByteArray parsableByteArray, int i, int i2, StsdData stsdData, int i3) {
        int position = parsableByteArray.getPosition();
        while (true) {
            boolean z = false;
            if (position - i >= i2) {
                return 0;
            }
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            if (readInt > 0) {
                z = true;
            }
            Assertions.checkArgument(z, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_sinf) {
                Pair parseSinfFromParent = parseSinfFromParent(parsableByteArray, position, readInt);
                if (parseSinfFromParent != null) {
                    stsdData.trackEncryptionBoxes[i3] = (TrackEncryptionBox) parseSinfFromParent.second;
                    return ((Integer) parseSinfFromParent.first).intValue();
                }
            }
            position += readInt;
        }
    }

    private static Pair<Integer, TrackEncryptionBox> parseSinfFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        boolean z = false;
        Object obj = null;
        Object obj2 = null;
        boolean z2 = false;
        while (true) {
            boolean z3 = true;
            if (i3 - i >= i2) {
                break;
            }
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_frma) {
                obj = Integer.valueOf(parsableByteArray.readInt());
            } else if (readInt2 == Atom.TYPE_schm) {
                parsableByteArray.skipBytes(4);
                if (parsableByteArray.readInt() != TYPE_cenc) {
                    z3 = false;
                }
                z2 = z3;
            } else if (readInt2 == Atom.TYPE_schi) {
                obj2 = parseSchiFromParent(parsableByteArray, i3, readInt);
            }
            i3 += readInt;
        }
        if (!z2) {
            return null;
        }
        Assertions.checkArgument(obj != null, "frma atom is mandatory");
        if (obj2 != null) {
            z = true;
        }
        Assertions.checkArgument(z, "schi->tenc atom is mandatory");
        return Pair.create(obj, obj2);
    }

    private static TrackEncryptionBox parseSchiFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_tenc) {
                parsableByteArray.skipBytes(6);
                boolean z = true;
                if (parsableByteArray.readUnsignedByte() != 1) {
                    z = false;
                }
                int readUnsignedByte = parsableByteArray.readUnsignedByte();
                byte[] bArr = new byte[16];
                parsableByteArray.readBytes(bArr, 0, bArr.length);
                return new TrackEncryptionBox(z, readUnsignedByte, bArr);
            }
            i3 += readInt;
        }
        return null;
    }

    private static byte[] parseProjFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_proj) {
                return Arrays.copyOfRange(parsableByteArray.data, i3, readInt + i3);
            }
            i3 += readInt;
        }
        return null;
    }

    private static int parseExpandableClassSize(ParsableByteArray parsableByteArray) {
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = parsableByteArray.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }

    private AtomParsers() {
    }
}
