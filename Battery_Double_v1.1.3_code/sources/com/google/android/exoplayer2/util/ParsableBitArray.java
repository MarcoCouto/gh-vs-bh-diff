package com.google.android.exoplayer2.util;

public final class ParsableBitArray {
    private int bitOffset;
    private int byteLimit;
    private int byteOffset;
    public byte[] data;

    public ParsableBitArray() {
    }

    public ParsableBitArray(byte[] bArr) {
        this(bArr, bArr.length);
    }

    public ParsableBitArray(byte[] bArr, int i) {
        this.data = bArr;
        this.byteLimit = i;
    }

    public void reset(byte[] bArr) {
        reset(bArr, bArr.length);
    }

    public void reset(byte[] bArr, int i) {
        this.data = bArr;
        this.byteOffset = 0;
        this.bitOffset = 0;
        this.byteLimit = i;
    }

    public int bitsLeft() {
        return ((this.byteLimit - this.byteOffset) * 8) - this.bitOffset;
    }

    public int getPosition() {
        return (this.byteOffset * 8) + this.bitOffset;
    }

    public int getBytePosition() {
        Assertions.checkState(this.bitOffset == 0);
        return this.byteOffset;
    }

    public void setPosition(int i) {
        this.byteOffset = i / 8;
        this.bitOffset = i - (this.byteOffset * 8);
        assertValidOffset();
    }

    public void skipBits(int i) {
        this.byteOffset += i / 8;
        this.bitOffset += i % 8;
        if (this.bitOffset > 7) {
            this.byteOffset++;
            this.bitOffset -= 8;
        }
        assertValidOffset();
    }

    public boolean readBit() {
        return readBits(1) == 1;
    }

    public int readBits(int i) {
        byte b;
        byte b2;
        if (i == 0) {
            return 0;
        }
        int i2 = i / 8;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            if (this.bitOffset != 0) {
                b2 = ((this.data[this.byteOffset + 1] & 255) >>> (8 - this.bitOffset)) | ((this.data[this.byteOffset] & 255) << this.bitOffset);
            } else {
                b2 = this.data[this.byteOffset];
            }
            i -= 8;
            i3 |= (255 & b2) << i;
            this.byteOffset++;
        }
        if (i > 0) {
            int i5 = this.bitOffset + i;
            byte b3 = (byte) (255 >> (8 - i));
            if (i5 > 8) {
                b = (b3 & (((this.data[this.byteOffset] & 255) << (i5 - 8)) | ((255 & this.data[this.byteOffset + 1]) >> (16 - i5)))) | i3;
                this.byteOffset++;
            } else {
                b = (b3 & ((this.data[this.byteOffset] & 255) >> (8 - i5))) | i3;
                if (i5 == 8) {
                    this.byteOffset++;
                }
            }
            i3 = b;
            this.bitOffset = i5 % 8;
        }
        assertValidOffset();
        return i3;
    }

    public void byteAlign() {
        if (this.bitOffset != 0) {
            this.bitOffset = 0;
            this.byteOffset++;
            assertValidOffset();
        }
    }

    public void readBytes(byte[] bArr, int i, int i2) {
        Assertions.checkState(this.bitOffset == 0);
        System.arraycopy(this.data, this.byteOffset, bArr, i, i2);
        this.byteOffset += i2;
        assertValidOffset();
    }

    public void skipBytes(int i) {
        Assertions.checkState(this.bitOffset == 0);
        this.byteOffset += i;
        assertValidOffset();
    }

    private void assertValidOffset() {
        Assertions.checkState(this.byteOffset >= 0 && this.bitOffset >= 0 && this.bitOffset < 8 && (this.byteOffset < this.byteLimit || (this.byteOffset == this.byteLimit && this.bitOffset == 0)));
    }
}
