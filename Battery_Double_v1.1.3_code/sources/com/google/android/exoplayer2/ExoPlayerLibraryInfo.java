package com.google.android.exoplayer2;

public interface ExoPlayerLibraryInfo {
    public static final boolean ASSERTIONS_ENABLED = true;
    public static final boolean TRACE_ENABLED = true;
    public static final String VERSION = "2.4.2";
    public static final int VERSION_INT = 2004002;
    public static final String VERSION_SLASHY = "ExoPlayerLib/2.4.2";
}
