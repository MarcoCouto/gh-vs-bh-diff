package com.google.android.gms.internal.measurement;

final class zzua {
    private static final Class<?> zzbuz = zzvn();

    private static Class<?> zzvn() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    public static zzub zzvo() {
        if (zzbuz != null) {
            try {
                return zzge("getEmptyRegistry");
            } catch (Exception unused) {
            }
        }
        return zzub.zzbvd;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x000e  */
    static zzub zzvp() {
        zzub zzub;
        if (zzbuz != null) {
            try {
                zzub = zzge("loadGeneratedRegistry");
            } catch (Exception unused) {
            }
            if (zzub == null) {
                zzub = zzub.zzvp();
            }
            return zzub != null ? zzvo() : zzub;
        }
        zzub = null;
        if (zzub == null) {
        }
        if (zzub != null) {
        }
    }

    private static final zzub zzge(String str) throws Exception {
        return (zzub) zzbuz.getDeclaredMethod(str, new Class[0]).invoke(null, new Object[0]);
    }
}
