package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbv;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@zzark
public final class zzala implements zzakp {
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    /* access modifiers changed from: private */
    public final long mStartTime;
    private final zzalg zzbma;
    private final boolean zzbum;
    private final zzakr zzdmn;
    private final boolean zzdms;
    private final boolean zzdmt;
    private final zzasi zzdnh;
    /* access modifiers changed from: private */
    public final long zzdni;
    private final int zzdnj;
    /* access modifiers changed from: private */
    public boolean zzdnk = false;
    /* access modifiers changed from: private */
    public final Map<zzbcb<zzakx>, zzaku> zzdnl = new HashMap();
    private final String zzdnm;
    private List<zzakx> zzdnn = new ArrayList();

    public zzala(Context context, zzasi zzasi, zzalg zzalg, zzakr zzakr, boolean z, boolean z2, String str, long j, long j2, int i, boolean z3) {
        this.mContext = context;
        this.zzdnh = zzasi;
        this.zzbma = zzalg;
        this.zzdmn = zzakr;
        this.zzbum = z;
        this.zzdms = z2;
        this.zzdnm = str;
        this.mStartTime = j;
        this.zzdni = j2;
        this.zzdnj = 2;
        this.zzdmt = z3;
    }

    public final zzakx zzh(List<zzakq> list) {
        zzaxz.zzdn("Starting mediation.");
        ArrayList arrayList = new ArrayList();
        zzwf zzwf = this.zzdnh.zzbst;
        int[] iArr = new int[2];
        if (zzwf.zzckm != null) {
            zzbv.zzlz();
            if (zzakz.zza(this.zzdnm, iArr)) {
                int i = 0;
                int i2 = iArr[0];
                int i3 = iArr[1];
                zzwf[] zzwfArr = zzwf.zzckm;
                int length = zzwfArr.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    zzwf zzwf2 = zzwfArr[i];
                    if (i2 == zzwf2.width && i3 == zzwf2.height) {
                        zzwf = zzwf2;
                        break;
                    }
                    i++;
                }
            }
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            zzakq zzakq = (zzakq) it.next();
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(zzakq.zzdkv);
            zzaxz.zzen(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            Iterator it2 = zzakq.zzdkw.iterator();
            while (it2.hasNext()) {
                String str2 = (String) it2.next();
                Context context = this.mContext;
                zzalg zzalg = this.zzbma;
                zzakr zzakr = this.zzdmn;
                zzwb zzwb = this.zzdnh.zzdwg;
                zzbbi zzbbi = this.zzdnh.zzbsp;
                boolean z = this.zzbum;
                boolean z2 = this.zzdms;
                zzacp zzacp = this.zzdnh.zzbti;
                Iterator it3 = it;
                List<String> list2 = this.zzdnh.zzbtt;
                Iterator it4 = it2;
                ArrayList arrayList2 = arrayList;
                boolean z3 = z;
                zzakq zzakq2 = zzakq;
                zzwf zzwf3 = zzwf;
                zzbbi zzbbi2 = zzbbi;
                zzaku zzaku = new zzaku(context, str2, zzalg, zzakr, zzakq2, zzwb, zzwf3, zzbbi2, z3, z2, zzacp, list2, this.zzdnh.zzdwu, this.zzdnh.zzdxp, this.zzdmt);
                zzbcb zza = zzayf.zza(new zzalb(this, zzaku));
                this.zzdnl.put(zza, zzaku);
                ArrayList arrayList3 = arrayList2;
                arrayList3.add(zza);
                arrayList = arrayList3;
                it = it3;
                it2 = it4;
            }
        }
        ArrayList arrayList4 = arrayList;
        if (this.zzdnj != 2) {
            return zzi(arrayList4);
        }
        return zzj(arrayList4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r4.hasNext() == false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        r0 = (com.google.android.gms.internal.ads.zzbcb) r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1 = (com.google.android.gms.internal.ads.zzakx) r0.get();
        r3.zzdnn.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r1 == null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        if (r1.zzdna != 0) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0031, code lost:
        zza(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzc("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003c, code lost:
        zza(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0046, code lost:
        return new com.google.android.gms.internal.ads.zzakx(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r4 = r4.iterator();
     */
    private final zzakx zzi(List<zzbcb<zzakx>> list) {
        synchronized (this.mLock) {
            if (this.zzdnk) {
                zzakx zzakx = new zzakx(-1);
                return zzakx;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r14.zzdmn.zzdmb == -1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r0 = r14.zzdmn.zzdmb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r0 = org.altbeacon.beacon.BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        r15 = r15.iterator();
        r3 = null;
        r4 = -1;
        r1 = r0;
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        if (r15.hasNext() == false) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        r5 = (com.google.android.gms.internal.ads.zzbcb) r15.next();
        r6 = com.google.android.gms.ads.internal.zzbv.zzlm().currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        if (r1 != 0) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        if (r5.isDone() == false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r10 = (com.google.android.gms.internal.ads.zzakx) r5.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        r15 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0052, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        r10 = (com.google.android.gms.internal.ads.zzakx) r5.get(r1, java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        r14.zzdnn.add(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        if (r10 == null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0065, code lost:
        if (r10.zzdna != 0) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0067, code lost:
        r11 = r10.zzdnf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0069, code lost:
        if (r11 == null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006f, code lost:
        if (r11.zzur() <= r4) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0075, code lost:
        r3 = r5;
        r0 = r10;
        r4 = r11.zzur();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0078, code lost:
        r1 = java.lang.Math.max(r1 - (com.google.android.gms.ads.internal.zzbv.zzlm().currentTimeMillis() - r6), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzc("Exception while processing an adapter; continuing with other adapters", r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008e, code lost:
        r1 = java.lang.Math.max(r1 - (com.google.android.gms.ads.internal.zzbv.zzlm().currentTimeMillis() - r6), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x009f, code lost:
        java.lang.Math.max(r1 - (com.google.android.gms.ads.internal.zzbv.zzlm().currentTimeMillis() - r6), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ae, code lost:
        throw r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00af, code lost:
        zza(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b2, code lost:
        if (r0 != null) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ba, code lost:
        return new com.google.android.gms.internal.ads.zzakx(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bb, code lost:
        return r0;
     */
    private final zzakx zzj(List<zzbcb<zzakx>> list) {
        synchronized (this.mLock) {
            if (this.zzdnk) {
                zzakx zzakx = new zzakx(-1);
                return zzakx;
            }
        }
    }

    private final void zza(zzbcb<zzakx> zzbcb) {
        zzayh.zzelc.post(new zzalc(this, zzbcb));
    }

    public final void cancel() {
        synchronized (this.mLock) {
            this.zzdnk = true;
            for (zzaku cancel : this.zzdnl.values()) {
                cancel.cancel();
            }
        }
    }

    public final List<zzakx> zzui() {
        return this.zzdnn;
    }
}
