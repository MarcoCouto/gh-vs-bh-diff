package com.google.android.gms.internal.ads;

import com.google.android.exoplayer2.C;
import java.io.IOException;

public final class zzjw implements zzhz {
    private static final int zzatm = zzqe.zzam("RCC\u0001");
    private int version;
    private final zzfs zzaad;
    private int zzajn;
    private int zzaqe = 0;
    private zzii zzasj;
    private final zzpx zzatn = new zzpx(9);
    private long zzato;
    private int zzatp;

    public zzjw(zzfs zzfs) {
        this.zzaad = zzfs;
    }

    public final void release() {
    }

    public final void zza(zzib zzib) {
        zzib.zza(new zzih(C.TIME_UNSET));
        this.zzasj = zzib.zzb(0, 3);
        zzib.zzdy();
        this.zzasj.zzf(this.zzaad);
    }

    public final boolean zza(zzia zzia) throws IOException, InterruptedException {
        this.zzatn.reset();
        zzia.zza(this.zzatn.data, 0, 8);
        if (this.zzatn.readInt() == zzatm) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0093 A[SYNTHETIC] */
    public final int zza(zzia zzia, zzif zzif) throws IOException, InterruptedException {
        while (true) {
            boolean z = true;
            boolean z2 = false;
            switch (this.zzaqe) {
                case 0:
                    this.zzatn.reset();
                    if (zzia.zza(this.zzatn.data, 0, 8, true)) {
                        if (this.zzatn.readInt() != zzatm) {
                            throw new IOException("Input not RawCC");
                        }
                        this.version = this.zzatn.readUnsignedByte();
                        z2 = true;
                    }
                    if (z2) {
                        this.zzaqe = 1;
                        break;
                    } else {
                        return -1;
                    }
                case 1:
                    this.zzatn.reset();
                    if (this.version == 0) {
                        if (zzia.zza(this.zzatn.data, 0, 5, true)) {
                            this.zzato = (this.zzatn.zzhd() * 1000) / 45;
                            this.zzatp = this.zzatn.readUnsignedByte();
                            this.zzajn = 0;
                            if (z) {
                                this.zzaqe = 2;
                                break;
                            } else {
                                this.zzaqe = 0;
                                return -1;
                            }
                        }
                    } else if (this.version != 1) {
                        int i = this.version;
                        StringBuilder sb = new StringBuilder(39);
                        sb.append("Unsupported version number: ");
                        sb.append(i);
                        throw new zzfx(sb.toString());
                    } else if (zzia.zza(this.zzatn.data, 0, 9, true)) {
                        this.zzato = this.zzatn.readLong();
                        this.zzatp = this.zzatn.readUnsignedByte();
                        this.zzajn = 0;
                        if (z) {
                        }
                    }
                    z = false;
                    if (z) {
                    }
                case 2:
                    while (this.zzatp > 0) {
                        this.zzatn.reset();
                        zzia.readFully(this.zzatn.data, 0, 3);
                        this.zzasj.zza(this.zzatn, 3);
                        this.zzajn += 3;
                        this.zzatp--;
                    }
                    if (this.zzajn > 0) {
                        this.zzasj.zza(this.zzato, 1, this.zzajn, 0, null);
                    }
                    this.zzaqe = 1;
                    return 0;
                default:
                    throw new IllegalStateException();
            }
        }
    }

    public final void zzc(long j, long j2) {
        this.zzaqe = 0;
    }
}
