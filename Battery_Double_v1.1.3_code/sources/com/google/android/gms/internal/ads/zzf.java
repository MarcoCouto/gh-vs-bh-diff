package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class zzf implements zzt {
    private final Map<String, List<zzr<?>>> zzp = new HashMap();
    private final zzd zzq;

    zzf(zzd zzd) {
        this.zzq = zzd;
    }

    public final void zza(zzr<?> zzr, zzx<?> zzx) {
        List<zzr> list;
        if (zzx.zzbg == null || zzx.zzbg.zzb()) {
            zza(zzr);
            return;
        }
        String zzf = zzr.zzf();
        synchronized (this) {
            list = (List) this.zzp.remove(zzf);
        }
        if (list != null) {
            if (zzaf.DEBUG) {
                zzaf.v("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(list.size()), zzf);
            }
            for (zzr zzb : list) {
                this.zzq.zzk.zzb(zzb, zzx);
            }
        }
    }

    public final synchronized void zza(zzr<?> zzr) {
        String zzf = zzr.zzf();
        List list = (List) this.zzp.remove(zzf);
        if (list != null && !list.isEmpty()) {
            if (zzaf.DEBUG) {
                zzaf.v("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(list.size()), zzf);
            }
            zzr zzr2 = (zzr) list.remove(0);
            this.zzp.put(zzf, list);
            zzr2.zza((zzt) this);
            try {
                this.zzq.zzi.put(zzr2);
            } catch (InterruptedException e) {
                zzaf.e("Couldn't add request to queue. %s", e.toString());
                Thread.currentThread().interrupt();
                this.zzq.quit();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
        return false;
     */
    public final synchronized boolean zzb(zzr<?> zzr) {
        String zzf = zzr.zzf();
        if (this.zzp.containsKey(zzf)) {
            List list = (List) this.zzp.get(zzf);
            if (list == null) {
                list = new ArrayList();
            }
            zzr.zzb("waiting-for-response");
            list.add(zzr);
            this.zzp.put(zzf, list);
            if (zzaf.DEBUG) {
                zzaf.d("Request for cacheKey=%s is in flight, putting on hold.", zzf);
            }
        } else {
            this.zzp.put(zzf, null);
            zzr.zza((zzt) this);
            if (zzaf.DEBUG) {
                zzaf.d("new request, sending to network %s", zzf);
            }
        }
    }
}
