package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;

public abstract class zzaux extends zzex implements zzauw {
    public zzaux() {
        super("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    public static zzauw zzab(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
        if (queryLocalInterface instanceof zzauw) {
            return (zzauw) queryLocalInterface;
        }
        return new zzauy(iBinder);
    }

    /* JADX WARNING: type inference failed for: r4v2 */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.google.android.gms.internal.ads.zzavb] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.google.android.gms.internal.ads.zzavd] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.google.android.gms.internal.ads.zzavb] */
    /* JADX WARNING: type inference failed for: r4v8, types: [com.google.android.gms.internal.ads.zzauu] */
    /* JADX WARNING: type inference failed for: r4v10, types: [com.google.android.gms.internal.ads.zzauv] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.google.android.gms.internal.ads.zzauu] */
    /* JADX WARNING: type inference failed for: r4v13 */
    /* JADX WARNING: type inference failed for: r4v14 */
    /* JADX WARNING: type inference failed for: r4v15 */
    /* JADX WARNING: type inference failed for: r4v16 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r4v2
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.zzauv, com.google.android.gms.internal.ads.zzavd, com.google.android.gms.internal.ads.zzavb, com.google.android.gms.internal.ads.zzauu]
  uses: [com.google.android.gms.internal.ads.zzavb, com.google.android.gms.internal.ads.zzauu]
  mth insns count: 86
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 5 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 34) {
            ? r4 = 0;
            switch (i) {
                case 1:
                    zza((zzavh) zzey.zza(parcel, zzavh.CREATOR));
                    parcel2.writeNoException();
                    break;
                case 2:
                    show();
                    parcel2.writeNoException();
                    break;
                case 3:
                    IBinder readStrongBinder = parcel.readStrongBinder();
                    if (readStrongBinder != null) {
                        IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
                        if (queryLocalInterface instanceof zzavb) {
                            r4 = (zzavb) queryLocalInterface;
                        } else {
                            r4 = new zzavd(readStrongBinder);
                        }
                    }
                    zza((zzavb) r4);
                    parcel2.writeNoException();
                    break;
                default:
                    switch (i) {
                        case 5:
                            boolean isLoaded = isLoaded();
                            parcel2.writeNoException();
                            zzey.writeBoolean(parcel2, isLoaded);
                            break;
                        case 6:
                            pause();
                            parcel2.writeNoException();
                            break;
                        case 7:
                            resume();
                            parcel2.writeNoException();
                            break;
                        case 8:
                            destroy();
                            parcel2.writeNoException();
                            break;
                        case 9:
                            zze(Stub.asInterface(parcel.readStrongBinder()));
                            parcel2.writeNoException();
                            break;
                        case 10:
                            zzf(Stub.asInterface(parcel.readStrongBinder()));
                            parcel2.writeNoException();
                            break;
                        case 11:
                            zzg(Stub.asInterface(parcel.readStrongBinder()));
                            parcel2.writeNoException();
                            break;
                        case 12:
                            String mediationAdapterClassName = getMediationAdapterClassName();
                            parcel2.writeNoException();
                            parcel2.writeString(mediationAdapterClassName);
                            break;
                        case 13:
                            setUserId(parcel.readString());
                            parcel2.writeNoException();
                            break;
                        case 14:
                            zza(zzxr.zzc(parcel.readStrongBinder()));
                            parcel2.writeNoException();
                            break;
                        case 15:
                            Bundle adMetadata = getAdMetadata();
                            parcel2.writeNoException();
                            zzey.zzb(parcel2, adMetadata);
                            break;
                        case 16:
                            IBinder readStrongBinder2 = parcel.readStrongBinder();
                            if (readStrongBinder2 != null) {
                                IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedAdSkuListener");
                                if (queryLocalInterface2 instanceof zzauu) {
                                    r4 = (zzauu) queryLocalInterface2;
                                } else {
                                    r4 = new zzauv(readStrongBinder2);
                                }
                            }
                            zza((zzauu) r4);
                            parcel2.writeNoException();
                            break;
                        case 17:
                            setAppPackageName(parcel.readString());
                            parcel2.writeNoException();
                            break;
                        case 18:
                            zzd(Stub.asInterface(parcel.readStrongBinder()));
                            parcel2.writeNoException();
                            break;
                        case 19:
                            setCustomData(parcel.readString());
                            parcel2.writeNoException();
                            break;
                        default:
                            return false;
                    }
            }
        } else {
            setImmersiveMode(zzey.zza(parcel));
            parcel2.writeNoException();
        }
        return true;
    }
}
