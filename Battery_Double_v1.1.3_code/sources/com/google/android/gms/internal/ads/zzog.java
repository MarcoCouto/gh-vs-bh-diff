package com.google.android.gms.internal.ads;

import android.graphics.Point;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public final class zzog extends zzoj {
    private static final int[] zzbem = new int[0];
    private final zzon zzben;
    private final AtomicReference<zzoh> zzbeo;

    public zzog() {
        this(null);
    }

    private static int zze(int i, int i2) {
        if (i == -1) {
            return i2 == -1 ? 0 : -1;
        }
        if (i2 == -1) {
            return 1;
        }
        return i - i2;
    }

    private static boolean zze(int i, boolean z) {
        int i2 = i & 3;
        return i2 == 3 || (z && i2 == 2);
    }

    private zzog(zzon zzon) {
        this.zzben = null;
        this.zzbeo = new AtomicReference<>(new zzoh());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0348, code lost:
        r39 = r1;
        r9 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x03fe, code lost:
        r44 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x0470, code lost:
        r4 = r44;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0472, code lost:
        r2 = r2 + 1;
        r21 = r9;
        r1 = r39;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0182, code lost:
        if (r10.zzzf <= r13) goto L_0x0187;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01dc  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x01ef A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01b4  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01b9  */
    public final zzom[] zza(zzga[] zzgaArr, zzma[] zzmaArr, int[][][] iArr) throws zzff {
        int i;
        zzoh zzoh;
        zzom zzom;
        int i2;
        boolean z;
        boolean z2;
        boolean z3;
        zzoi zzoi;
        int[][] iArr2;
        zzlz zzlz;
        zzma zzma;
        boolean z4;
        boolean z5;
        int i3;
        zzoi zzoi2;
        zzma zzma2;
        int i4;
        zzom[] zzomArr;
        int i5;
        zzoh zzoh2;
        zzoi zzoi3;
        int i6;
        int i7;
        int i8;
        boolean z6;
        boolean z7;
        zzlz zzlz2;
        int i9;
        int[] iArr3;
        boolean z8;
        zzlz zzlz3;
        int i10;
        boolean z9;
        int i11;
        boolean zze;
        boolean z10;
        int i12;
        int i13;
        int i14;
        int i15;
        boolean z11;
        boolean z12;
        int i16;
        int i17;
        int i18;
        int i19;
        Point point;
        int i20;
        boolean z13;
        boolean z14;
        zzga[] zzgaArr2 = zzgaArr;
        int length = zzgaArr2.length;
        zzom[] zzomArr2 = new zzom[length];
        zzoh zzoh3 = (zzoh) this.zzbeo.get();
        int i21 = 0;
        boolean z15 = false;
        while (i21 < length) {
            if (2 == zzgaArr2[i21].getTrackType()) {
                if (!z15) {
                    zzma zzma3 = zzmaArr[i21];
                    int[][] iArr4 = iArr[i21];
                    int i22 = zzoh3.zzbet;
                    int i23 = zzoh3.zzbeu;
                    int i24 = zzoh3.zzbev;
                    int i25 = zzoh3.viewportWidth;
                    int i26 = zzoh3.viewportHeight;
                    boolean z16 = zzoh3.zzbey;
                    boolean z17 = zzoh3.zzbew;
                    boolean z18 = zzoh3.zzbex;
                    i5 = length;
                    zzoh2 = zzoh3;
                    zzlz zzlz4 = null;
                    int i27 = 0;
                    int i28 = 0;
                    int i29 = 0;
                    int i30 = -1;
                    int i31 = -1;
                    while (i28 < zzma3.length) {
                        zzlz zzau = zzma3.zzau(i28);
                        zzma zzma4 = zzma3;
                        zzom[] zzomArr3 = zzomArr2;
                        ArrayList arrayList = new ArrayList(zzau.length);
                        int i32 = i21;
                        for (int i33 = 0; i33 < zzau.length; i33++) {
                            arrayList.add(Integer.valueOf(i33));
                        }
                        int i34 = Integer.MAX_VALUE;
                        if (i25 == Integer.MAX_VALUE || i26 == Integer.MAX_VALUE) {
                            zzlz2 = zzlz4;
                            i8 = i25;
                            i9 = i27;
                            z7 = z17;
                            i6 = i24;
                            i7 = i26;
                            z6 = z16;
                        } else {
                            i9 = i27;
                            int i35 = 0;
                            while (i35 < zzau.length) {
                                zzfs zzat = zzau.zzat(i35);
                                zzlz zzlz5 = zzlz4;
                                if (zzat.width <= 0 || zzat.height <= 0) {
                                    i15 = i25;
                                    z12 = z17;
                                    i13 = i24;
                                    i14 = i26;
                                    z11 = z16;
                                } else {
                                    int i36 = zzat.width;
                                    z12 = z17;
                                    int i37 = zzat.height;
                                    if (z16) {
                                        if (i36 > i37) {
                                            z11 = z16;
                                            z13 = true;
                                        } else {
                                            z11 = z16;
                                            z13 = false;
                                        }
                                        if (i25 > i26) {
                                            i15 = i25;
                                            z14 = true;
                                        } else {
                                            i15 = i25;
                                            z14 = false;
                                        }
                                        if (z13 != z14) {
                                            i16 = i26;
                                            i14 = i16;
                                            i17 = i15;
                                            i18 = i36 * i17;
                                            i13 = i24;
                                            i19 = i37 * i16;
                                            if (i18 < i19) {
                                                point = new Point(i16, zzqe.zzf(i19, i36));
                                            } else {
                                                point = new Point(zzqe.zzf(i18, i37), i17);
                                            }
                                            i20 = zzat.width * zzat.height;
                                            if (zzat.width >= ((int) (((float) point.x) * 0.98f)) && zzat.height >= ((int) (((float) point.y) * 0.98f)) && i20 < i34) {
                                                i34 = i20;
                                            }
                                        }
                                    } else {
                                        i15 = i25;
                                        z11 = z16;
                                    }
                                    i17 = i26;
                                    i14 = i17;
                                    i16 = i15;
                                    i18 = i36 * i17;
                                    i13 = i24;
                                    i19 = i37 * i16;
                                    if (i18 < i19) {
                                    }
                                    i20 = zzat.width * zzat.height;
                                    i34 = i20;
                                }
                                i35++;
                                zzlz4 = zzlz5;
                                z17 = z12;
                                z16 = z11;
                                i25 = i15;
                                i26 = i14;
                                i24 = i13;
                            }
                            zzlz2 = zzlz4;
                            i8 = i25;
                            z7 = z17;
                            i6 = i24;
                            i7 = i26;
                            z6 = z16;
                            if (i34 != Integer.MAX_VALUE) {
                                for (int size = arrayList.size() - 1; size >= 0; size--) {
                                    int zzce = zzau.zzat(((Integer) arrayList.get(size)).intValue()).zzce();
                                    if (zzce == -1 || zzce > i34) {
                                        arrayList.remove(size);
                                    }
                                }
                            }
                        }
                        int[] iArr5 = iArr4[i28];
                        int i38 = i29;
                        int i39 = i30;
                        int i40 = i31;
                        i27 = i9;
                        int i41 = 0;
                        while (i41 < zzau.length) {
                            if (zze(iArr5[i41], z18)) {
                                zzfs zzat2 = zzau.zzat(i41);
                                if (!arrayList.contains(Integer.valueOf(i41)) || ((zzat2.width != -1 && zzat2.width > i22) || (zzat2.height != -1 && zzat2.height > i23))) {
                                    i10 = i6;
                                } else {
                                    if (zzat2.zzzf != -1) {
                                        i10 = i6;
                                    } else {
                                        i10 = i6;
                                    }
                                    z9 = true;
                                    if (!z9 || z7) {
                                        if (!z9) {
                                            zzlz3 = zzau;
                                            z8 = z18;
                                            i11 = 2;
                                        } else {
                                            zzlz3 = zzau;
                                            z8 = z18;
                                            i11 = 1;
                                        }
                                        iArr3 = iArr5;
                                        zze = zze(iArr5[i41], false);
                                        if (zze) {
                                            i11 += 1000;
                                        }
                                        z10 = i11 <= i38;
                                        if (i11 == i38) {
                                            if (zzat2.zzce() != i39) {
                                                i12 = zze(zzat2.zzce(), i39);
                                            } else {
                                                i12 = zze(zzat2.zzzf, i40);
                                            }
                                            z10 = !zze || !z9 ? i12 < 0 : i12 > 0;
                                        }
                                        if (!z10) {
                                            i40 = zzat2.zzzf;
                                            i39 = zzat2.zzce();
                                            i38 = i11;
                                            i27 = i41;
                                            zzlz2 = zzlz3;
                                        }
                                    } else {
                                        iArr3 = iArr5;
                                        zzlz3 = zzau;
                                        z8 = z18;
                                    }
                                }
                                z9 = false;
                                if (!z9) {
                                }
                                if (!z9) {
                                }
                                iArr3 = iArr5;
                                zze = zze(iArr5[i41], false);
                                if (zze) {
                                }
                                if (i11 <= i38) {
                                }
                                if (i11 == i38) {
                                }
                                if (!z10) {
                                }
                            } else {
                                iArr3 = iArr5;
                                zzlz3 = zzau;
                                z8 = z18;
                                i10 = i6;
                            }
                            i41++;
                            i6 = i10;
                            zzau = zzlz3;
                            z18 = z8;
                            iArr5 = iArr3;
                        }
                        boolean z19 = z18;
                        i28++;
                        i29 = i38;
                        i30 = i39;
                        i31 = i40;
                        i24 = i6;
                        zzma3 = zzma4;
                        zzomArr2 = zzomArr3;
                        i21 = i32;
                        zzlz4 = zzlz2;
                        z17 = z7;
                        z16 = z6;
                        i25 = i8;
                        i26 = i7;
                    }
                    zzlz zzlz6 = zzlz4;
                    int i42 = i27;
                    zzomArr = zzomArr2;
                    i4 = i21;
                    if (zzlz6 == null) {
                        zzoi3 = null;
                    } else {
                        zzoi3 = new zzoi(zzlz6, i42);
                    }
                    zzomArr[i4] = zzoi3;
                    z15 = zzomArr[i4] != null;
                } else {
                    i5 = length;
                    zzomArr = zzomArr2;
                    zzoh2 = zzoh3;
                    i4 = i21;
                }
                int i43 = zzmaArr[i4].length;
            } else {
                i5 = length;
                zzomArr = zzomArr2;
                zzoh2 = zzoh3;
                i4 = i21;
            }
            i21 = i4 + 1;
            zzoh3 = zzoh2;
            length = i5;
            zzomArr2 = zzomArr;
            zzgaArr2 = zzgaArr;
        }
        zzom[] zzomArr4 = zzomArr2;
        zzoh zzoh4 = zzoh3;
        int i44 = length;
        int i45 = 0;
        boolean z20 = false;
        boolean z21 = false;
        while (i45 < i44) {
            zzga[] zzgaArr3 = zzgaArr;
            switch (zzgaArr3[i45].getTrackType()) {
                case 1:
                    i = i44;
                    zzoh = zzoh4;
                    if (!z20) {
                        zzma zzma5 = zzmaArr[i45];
                        int[][] iArr6 = iArr[i45];
                        boolean z22 = zzoh.zzbex;
                        int i46 = 0;
                        int i47 = -1;
                        int i48 = -1;
                        int i49 = 0;
                        while (i46 < zzma5.length) {
                            zzlz zzau2 = zzma5.zzau(i46);
                            int[] iArr7 = iArr6[i46];
                            int[][] iArr8 = iArr6;
                            int i50 = i49;
                            int i51 = i48;
                            int i52 = i47;
                            int i53 = 0;
                            while (i53 < zzau2.length) {
                                if (zze(iArr7[i53], z22)) {
                                    zzfs zzat3 = zzau2.zzat(i53);
                                    z2 = z21;
                                    int i54 = iArr7[i53];
                                    z = z22;
                                    if ((zzat3.zzzz & 1) != 0) {
                                        i2 = i51;
                                        z3 = true;
                                    } else {
                                        i2 = i51;
                                        z3 = false;
                                    }
                                    int i55 = zza(zzat3, null) ? z3 ? 4 : 3 : z3 ? 2 : 1;
                                    if (zze(i54, false)) {
                                        i55 += 1000;
                                    }
                                    if (i55 > i50) {
                                        i50 = i55;
                                        i52 = i46;
                                        i2 = i53;
                                    }
                                } else {
                                    z2 = z21;
                                    z = z22;
                                    i2 = i51;
                                }
                                i53++;
                                z21 = z2;
                                z22 = z;
                                i51 = i2;
                            }
                            boolean z23 = z21;
                            boolean z24 = z22;
                            int i56 = i51;
                            i46++;
                            i47 = i52;
                            i49 = i50;
                            iArr6 = iArr8;
                            i48 = i56;
                        }
                        boolean z25 = z21;
                        if (i47 == -1) {
                            zzom = null;
                        } else {
                            zzom = new zzoi(zzma5.zzau(i47), i48);
                        }
                        zzomArr4[i45] = zzom;
                        z20 = zzomArr4[i45] != null;
                        z21 = z25;
                        break;
                    }
                    break;
                case 2:
                    break;
                case 3:
                    if (!z21) {
                        zzma zzma6 = zzmaArr[i45];
                        int[][] iArr9 = iArr[i45];
                        zzoh = zzoh4;
                        boolean z26 = zzoh.zzbex;
                        int i57 = 0;
                        zzlz zzlz7 = null;
                        int i58 = 0;
                        int i59 = 0;
                        while (i57 < zzma6.length) {
                            zzlz zzau3 = zzma6.zzau(i57);
                            int[] iArr10 = iArr9[i57];
                            int i60 = i44;
                            int i61 = i59;
                            int i62 = i58;
                            zzlz zzlz8 = zzlz7;
                            int i63 = 0;
                            while (i63 < zzau3.length) {
                                if (zze(iArr10[i63], z26)) {
                                    zzfs zzat4 = zzau3.zzat(i63);
                                    zzma = zzma6;
                                    if ((zzat4.zzzz & 1) != 0) {
                                        zzlz = zzau3;
                                        z4 = true;
                                    } else {
                                        zzlz = zzau3;
                                        z4 = false;
                                    }
                                    if ((zzat4.zzzz & 2) != 0) {
                                        iArr2 = iArr9;
                                        z5 = true;
                                    } else {
                                        iArr2 = iArr9;
                                        z5 = false;
                                    }
                                    if (zza(zzat4, null)) {
                                        i3 = z4 ? 6 : !z5 ? 5 : 4;
                                    } else if (z4) {
                                        i3 = 3;
                                    } else if (z5) {
                                        i3 = zza(zzat4, null) ? 2 : 1;
                                    }
                                    if (zze(iArr10[i63], false)) {
                                        i3 += 1000;
                                    }
                                    if (i3 > i61) {
                                        i61 = i3;
                                        i62 = i63;
                                        zzlz8 = zzlz;
                                    }
                                } else {
                                    zzma = zzma6;
                                    zzlz = zzau3;
                                    iArr2 = iArr9;
                                }
                                i63++;
                                zzma6 = zzma;
                                zzau3 = zzlz;
                                iArr9 = iArr2;
                            }
                            zzma zzma7 = zzma6;
                            int[][] iArr11 = iArr9;
                            i57++;
                            zzlz7 = zzlz8;
                            i58 = i62;
                            i44 = i60;
                            i59 = i61;
                        }
                        i = i44;
                        if (zzlz7 == null) {
                            zzoi = null;
                        } else {
                            zzoi = new zzoi(zzlz7, i58);
                        }
                        zzomArr4[i45] = zzoi;
                        z21 = zzomArr4[i45] != null;
                        break;
                    }
                    break;
                default:
                    i = i44;
                    boolean z27 = z21;
                    zzoh = zzoh4;
                    zzgaArr3[i45].getTrackType();
                    zzma zzma8 = zzmaArr[i45];
                    int[][] iArr12 = iArr[i45];
                    boolean z28 = zzoh.zzbex;
                    zzlz zzlz9 = null;
                    int i64 = 0;
                    int i65 = 0;
                    int i66 = 0;
                    while (i64 < zzma8.length) {
                        zzlz zzau4 = zzma8.zzau(i64);
                        int[] iArr13 = iArr12[i64];
                        int i67 = i66;
                        int i68 = i65;
                        zzlz zzlz10 = zzlz9;
                        int i69 = 0;
                        while (i69 < zzau4.length) {
                            if (zze(iArr13[i69], z28)) {
                                int i70 = (zzau4.zzat(i69).zzzz & 1) != 0 ? 2 : 1;
                                zzma2 = zzma8;
                                if (zze(iArr13[i69], false)) {
                                    i70 += 1000;
                                }
                                if (i70 > i67) {
                                    i68 = i69;
                                    i67 = i70;
                                    zzlz10 = zzau4;
                                }
                            } else {
                                zzma2 = zzma8;
                            }
                            i69++;
                            zzma8 = zzma2;
                        }
                        i64++;
                        zzlz9 = zzlz10;
                        i65 = i68;
                        zzma8 = zzma8;
                        i66 = i67;
                    }
                    if (zzlz9 == null) {
                        zzoi2 = null;
                    } else {
                        zzoi2 = new zzoi(zzlz9, i65);
                    }
                    zzomArr4[i45] = zzoi2;
                    break;
            }
        }
        return zzomArr4;
    }

    private static boolean zza(zzfs zzfs, String str) {
        return str != null && TextUtils.equals(str, zzqe.zzai(zzfs.zzaaa));
    }
}
