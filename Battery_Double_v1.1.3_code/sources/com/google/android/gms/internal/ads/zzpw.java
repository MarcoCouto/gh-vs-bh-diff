package com.google.android.gms.internal.ads;

public final class zzpw {
    private byte[] data;
    private int zzbhx;
    private int zzbhy;
    private int zzbhz;

    public zzpw() {
    }

    public zzpw(byte[] bArr) {
        this(bArr, bArr.length);
    }

    private zzpw(byte[] bArr, int i) {
        this.data = bArr;
        this.zzbhz = i;
    }

    public final int zzbj(int i) {
        byte b;
        byte b2;
        boolean z = false;
        if (i == 0) {
            return 0;
        }
        int i2 = i / 8;
        int i3 = i;
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            if (this.zzbhy != 0) {
                b2 = ((this.data[this.zzbhx + 1] & 255) >>> (8 - this.zzbhy)) | ((this.data[this.zzbhx] & 255) << this.zzbhy);
            } else {
                b2 = this.data[this.zzbhx];
            }
            i3 -= 8;
            i4 |= (255 & b2) << i3;
            this.zzbhx++;
        }
        if (i3 > 0) {
            int i6 = this.zzbhy + i3;
            byte b3 = (byte) (255 >> (8 - i3));
            if (i6 > 8) {
                b = (b3 & (((this.data[this.zzbhx] & 255) << (i6 - 8)) | ((255 & this.data[this.zzbhx + 1]) >> (16 - i6)))) | i4;
                this.zzbhx++;
            } else {
                b = (b3 & ((this.data[this.zzbhx] & 255) >> (8 - i6))) | i4;
                if (i6 == 8) {
                    this.zzbhx++;
                }
            }
            i4 = b;
            this.zzbhy = i6 % 8;
        }
        if (this.zzbhx >= 0 && this.zzbhy >= 0 && this.zzbhy < 8 && (this.zzbhx < this.zzbhz || (this.zzbhx == this.zzbhz && this.zzbhy == 0))) {
            z = true;
        }
        zzpo.checkState(z);
        return i4;
    }
}
