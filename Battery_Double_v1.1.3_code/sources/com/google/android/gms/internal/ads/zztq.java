package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzbv;
import javax.annotation.concurrent.GuardedBy;

@zzark
public final class zztq {
    @Nullable
    @GuardedBy("mLock")
    private Context mContext;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private final Runnable zzbzq = new zztr(this);
    /* access modifiers changed from: private */
    @Nullable
    @GuardedBy("mLock")
    public zztx zzbzr;
    /* access modifiers changed from: private */
    @Nullable
    @GuardedBy("mLock")
    public zzub zzbzs;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0047, code lost:
        return;
     */
    public final void initialize(Context context) {
        if (context != null) {
            synchronized (this.mLock) {
                if (this.mContext == null) {
                    this.mContext = context.getApplicationContext();
                    if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcvr)).booleanValue()) {
                        connect();
                    } else {
                        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcvq)).booleanValue()) {
                            zzbv.zzli().zza(new zzts(this));
                        }
                    }
                }
            }
        }
    }

    public final void zzod() {
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcvs)).booleanValue()) {
            synchronized (this.mLock) {
                connect();
                zzbv.zzlf();
                zzayh.zzelc.removeCallbacks(this.zzbzq);
                zzbv.zzlf();
                zzayh.zzelc.postDelayed(this.zzbzq, ((Long) zzwu.zzpz().zzd(zzaan.zzcvt)).longValue());
            }
        }
    }

    public final zztv zza(zzty zzty) {
        synchronized (this.mLock) {
            if (this.zzbzs == null) {
                zztv zztv = new zztv();
                return zztv;
            }
            try {
                zztv zza = this.zzbzs.zza(zzty);
                return zza;
            } catch (RemoteException e) {
                zzaxz.zzb("Unable to call into cache service.", e);
                return new zztv();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002f, code lost:
        return;
     */
    public final void connect() {
        synchronized (this.mLock) {
            if (this.mContext != null) {
                if (this.zzbzr == null) {
                    this.zzbzr = new zztx(this.mContext, zzbv.zzlv().zzaak(), new zztt(this), new zztu(this));
                    this.zzbzr.checkAvailabilityAndConnect();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public final void disconnect() {
        synchronized (this.mLock) {
            if (this.zzbzr != null) {
                if (this.zzbzr.isConnected() || this.zzbzr.isConnecting()) {
                    this.zzbzr.disconnect();
                }
                this.zzbzr = null;
                this.zzbzs = null;
                Binder.flushPendingCommands();
            }
        }
    }
}
