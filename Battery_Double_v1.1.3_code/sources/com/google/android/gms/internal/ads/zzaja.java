package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.gmsg.zzu;
import java.util.Map;

final class zzaja implements zzu<zzajr> {
    private final /* synthetic */ zzajm zzdjh;
    private final /* synthetic */ zzaii zzdji;
    private final /* synthetic */ zzait zzdjj;

    zzaja(zzait zzait, zzajm zzajm, zzaii zzaii) {
        this.zzdjj = zzait;
        this.zzdjh = zzajm;
        this.zzdji = zzaii;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        return;
     */
    public final /* synthetic */ void zza(Object obj, Map map) {
        synchronized (this.zzdjj.mLock) {
            if (this.zzdjh.getStatus() != -1) {
                if (this.zzdjh.getStatus() != 1) {
                    this.zzdjj.zzdiz = 0;
                    this.zzdjj.zzdiw.zzh(this.zzdji);
                    this.zzdjh.zzo(this.zzdji);
                    this.zzdjj.zzdiy = this.zzdjh;
                    zzaxz.v("Successfully loaded JS Engine.");
                }
            }
        }
    }
}
