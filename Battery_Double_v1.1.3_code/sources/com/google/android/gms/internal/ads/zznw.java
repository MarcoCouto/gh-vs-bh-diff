package com.google.android.gms.internal.ads;

import com.google.android.exoplayer2.C;
import java.util.List;

public final class zznw extends zznu {
    final zzoa zzbea;
    final zzoa zzbeb;

    public zznw(zzno zzno, long j, long j2, int i, long j3, List<zznx> list, zzoa zzoa, zzoa zzoa2) {
        super(zzno, j, j2, i, j3, list);
        this.zzbea = zzoa;
        this.zzbeb = zzoa2;
    }

    public final zzno zza(zznp zznp) {
        if (this.zzbea == null) {
            return super.zza(zznp);
        }
        zzno zzno = new zzno(this.zzbea.zza(zznp.zzaad.zzze, 0, zznp.zzaad.zzzf, 0), 0, -1);
        return zzno;
    }

    public final zzno zza(zznp zznp, int i) {
        long j;
        zznp zznp2 = zznp;
        if (this.zzbdy != null) {
            j = ((zznx) this.zzbdy.get(i - this.zzbdx)).startTime;
        } else {
            j = ((long) (i - this.zzbdx)) * this.zzcs;
        }
        long j2 = j;
        zzno zzno = new zzno(this.zzbeb.zza(zznp2.zzaad.zzze, i, zznp2.zzaad.zzzf, j2), 0, -1);
        return zzno;
    }

    public final int zzai(long j) {
        if (this.zzbdy != null) {
            return this.zzbdy.size();
        }
        if (j != C.TIME_UNSET) {
            return (int) zzqe.zzg(j, (this.zzcs * C.MICROS_PER_SECOND) / this.zzcr);
        }
        return -1;
    }
}
