package com.google.android.gms.internal.ads;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzbb;
import com.google.android.gms.common.util.PlatformVersion;
import javax.annotation.ParametersAreNonnullByDefault;

@zzark
@ParametersAreNonnullByDefault
public final class zzapl {
    /* JADX WARNING: type inference failed for: r7v0, types: [com.google.android.gms.internal.ads.zzazb, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.google.android.gms.internal.ads.zzapo] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.google.android.gms.internal.ads.zzapt] */
    /* JADX WARNING: type inference failed for: r7v3, types: [com.google.android.gms.internal.ads.zzapn] */
    /* JADX WARNING: type inference failed for: r7v4, types: [com.google.android.gms.internal.ads.zzapq] */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.google.android.gms.internal.ads.zzapr] */
    /* JADX WARNING: type inference failed for: r7v6 */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.google.android.gms.internal.ads.zzapt] */
    /* JADX WARNING: type inference failed for: r7v7 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r0v11, types: [com.google.android.gms.internal.ads.zzapr] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r7v6
  assigns: [com.google.android.gms.internal.ads.zzapt, com.google.android.gms.internal.ads.zzapr]
  uses: [java.lang.Object, com.google.android.gms.internal.ads.zzazb, com.google.android.gms.internal.ads.zzapt, com.google.android.gms.internal.ads.zzapr]
  mth insns count: 54
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    public static zzazb zza(Context context, zza zza, zzaxg zzaxg, zzcu zzcu, @Nullable zzbgg zzbgg, zzalg zzalg, zzapm zzapm, zzaba zzaba) {
        ? r7;
        zzasm zzasm = zzaxg.zzehy;
        if (zzasm.zzdyd) {
            ? zzapr = new zzapr(context, zzaxg, zzalg, zzapm, zzaba, zzbgg);
            r7 = zzapr;
        } else if (zzasm.zzckn || (zza instanceof zzbb)) {
            if (!zzasm.zzckn || !(zza instanceof zzbb)) {
                r7 = new zzapo(zzaxg, zzapm);
            } else {
                ? zzapt = new zzapt(context, (zzbb) zza, zzaxg, zzcu, zzapm, zzaba);
                r7 = zzapt;
            }
        } else if (!PlatformVersion.isAtLeastKitKat() || PlatformVersion.isAtLeastLollipop() || zzbgg == null || !zzbgg.zzadj().zzafb()) {
            r7 = new zzapn(context, zzaxg, zzbgg, zzapm);
        } else {
            r7 = new zzapq(context, zzaxg, zzbgg, zzapm);
        }
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(r7.getClass().getName());
        zzaxz.zzdn(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        r7.zzwa();
        return r7;
    }
}
