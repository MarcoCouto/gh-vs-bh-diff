package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.zzbgg;
import com.mansoon.BatteryDouble.Config;
import java.util.Map;

final class zzp implements zzu<zzbgg> {
    zzp() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzbgg) obj).zzaw(!Boolean.parseBoolean((String) map.get(Config.IMPORTANCE_DISABLED)));
    }
}
