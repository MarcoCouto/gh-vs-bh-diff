package com.facebook.ads.internal.k;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.internal.w.b.o;
import com.facebook.ads.internal.w.b.s;
import com.facebook.ads.internal.w.b.v;
import com.facebook.ads.internal.w.f.a;
import com.facebook.share.internal.ShareConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    private static final String a = "com.facebook.ads.internal.k.e";
    private static final Object b = new Object();
    private static final Set<String> c = Collections.synchronizedSet(new HashSet());
    private static final Map<String, Integer> d = Collections.synchronizedMap(new HashMap());
    private static AtomicInteger e = new AtomicInteger();

    public static d a(Exception exc, Context context, Map<String, String> map) {
        d dVar;
        try {
            dVar = new d(o.b(), o.c(), new b(s.a((Throwable) exc), map, true).a());
            try {
                a(dVar, context);
                return dVar;
            } catch (Exception unused) {
                return dVar;
            }
        } catch (Exception unused2) {
            dVar = null;
            return dVar;
        }
    }

    @WorkerThread
    public static JSONArray a(Context context) {
        return a(context, -1);
    }

    /* JADX WARNING: type inference failed for: r9v0, types: [int] */
    /* JADX WARNING: type inference failed for: r2v0 */
    /* JADX WARNING: type inference failed for: r9v1, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r9v2 */
    /* JADX WARNING: type inference failed for: r9v3 */
    /* JADX WARNING: type inference failed for: r4v0, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r3v0, types: [java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r2v2, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r9v5, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r4v1 */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r9v8 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r4v3 */
    /* JADX WARNING: type inference failed for: r9v9, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: type inference failed for: r3v7, types: [java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r2v5, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r3v8 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r9v14, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r3v12 */
    /* JADX WARNING: type inference failed for: r3v13, types: [java.io.Reader, java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r4v5, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r9v16, types: [int] */
    /* JADX WARNING: type inference failed for: r2v7 */
    /* JADX WARNING: type inference failed for: r9v18, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r2v9 */
    /* JADX WARNING: type inference failed for: r9v19 */
    /* JADX WARNING: type inference failed for: r9v20, types: [int] */
    /* JADX WARNING: type inference failed for: r9v22 */
    /* JADX WARNING: type inference failed for: r9v23 */
    /* JADX WARNING: type inference failed for: r2v18 */
    /* JADX WARNING: type inference failed for: r2v19 */
    /* JADX WARNING: type inference failed for: r2v20 */
    /* JADX WARNING: type inference failed for: r2v21 */
    /* JADX WARNING: type inference failed for: r9v24 */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r9v25 */
    /* JADX WARNING: type inference failed for: r9v26 */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r9v27 */
    /* JADX WARNING: type inference failed for: r9v28 */
    /* JADX WARNING: type inference failed for: r9v29 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r3v18 */
    /* JADX WARNING: type inference failed for: r3v19 */
    /* JADX WARNING: type inference failed for: r4v6 */
    /* JADX WARNING: type inference failed for: r9v30 */
    /* JADX WARNING: type inference failed for: r9v31 */
    /* JADX WARNING: type inference failed for: r9v32 */
    /* JADX WARNING: type inference failed for: r9v33 */
    /* JADX WARNING: type inference failed for: r9v34 */
    /* JADX WARNING: type inference failed for: r9v35 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=null, for r9v0, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r9v2
  assigns: []
  uses: []
  mth insns count: 149
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00e9 A[SYNTHETIC, Splitter:B:64:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f1 A[Catch:{ IOException -> 0x00ed }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00f6 A[Catch:{ IOException -> 0x00ed }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0105 A[SYNTHETIC, Splitter:B:79:0x0105] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x010f A[Catch:{ IOException -> 0x010b }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0114 A[Catch:{ IOException -> 0x010b }] */
    /* JADX WARNING: Unknown variable types count: 21 */
    @WorkerThread
    public static JSONArray a(Context context, int r9) {
        FileInputStream fileInputStream;
        ? r4;
        ? r3;
        ? r32;
        ? r2;
        String str;
        String str2;
        ? r33;
        ? r34;
        ? r35;
        ? r22;
        JSONArray jSONArray = new JSONArray();
        synchronized (b) {
            ? r23 = 0;
            try {
                if (new File(context.getFilesDir(), a.a("debuglogs", context)).exists()) {
                    fileInputStream = context.openFileInput(a.a("debuglogs", context));
                    try {
                        ? inputStreamReader = new InputStreamReader(fileInputStream);
                        try {
                            ? bufferedReader = new BufferedReader(inputStreamReader);
                            r9 = r9;
                            while (true) {
                                try {
                                    String readLine = bufferedReader.readLine();
                                    if (readLine == null || r9 == 0) {
                                        r22 = bufferedReader;
                                        r35 = inputStreamReader;
                                        r9 = r9;
                                    } else {
                                        JSONObject jSONObject = new JSONObject(readLine);
                                        if (!jSONObject.has("attempt")) {
                                            jSONObject.put("attempt", String.valueOf(0));
                                        }
                                        String string = jSONObject.getString("id");
                                        if (!c.contains(string)) {
                                            int i = jSONObject.getInt("attempt");
                                            if (d.containsKey(string)) {
                                                jSONObject.put("attempt", String.valueOf(d.get(string)));
                                            } else if (c.contains(string)) {
                                                throw new RuntimeException("finished event should not be updated to OngoingEvent.");
                                            } else {
                                                if (d.containsKey(string)) {
                                                    d.remove(string);
                                                }
                                                d.put(string, Integer.valueOf(i));
                                            }
                                            jSONArray.put(jSONObject);
                                            r9 = r9 > 0 ? r9 - 1 : r9;
                                        } else {
                                            r9 = r9;
                                        }
                                        r9 = r9;
                                    }
                                } catch (IOException | JSONException e2) {
                                    e = e2;
                                    r2 = bufferedReader;
                                    r32 = inputStreamReader;
                                    try {
                                        Log.e(a, "Failed to read crashes", e);
                                        if (r2 != 0) {
                                        }
                                        if (r32 != 0) {
                                        }
                                        if (fileInputStream != null) {
                                        }
                                        return jSONArray;
                                    } catch (Throwable th) {
                                        r4 = r2;
                                        r3 = r32;
                                        r9 = th;
                                        if (r4 != 0) {
                                            try {
                                                r4.close();
                                            } catch (IOException e3) {
                                                Log.e(a, "Failed to close buffers", e3);
                                                r9 = r9;
                                                throw r9;
                                            }
                                        }
                                        if (r3 != 0) {
                                            r3.close();
                                        }
                                        if (fileInputStream != null) {
                                            fileInputStream.close();
                                        }
                                        throw r9;
                                    }
                                } catch (Throwable th2) {
                                    r3 = inputStreamReader;
                                    r4 = bufferedReader;
                                    r9 = th2;
                                    if (r4 != 0) {
                                    }
                                    if (r3 != 0) {
                                    }
                                    if (fileInputStream != null) {
                                    }
                                    throw r9;
                                }
                            }
                            r22 = bufferedReader;
                            r35 = inputStreamReader;
                            r9 = r9;
                        } catch (IOException | JSONException e4) {
                            e = e4;
                            r2 = r23;
                            r32 = inputStreamReader;
                            Log.e(a, "Failed to read crashes", e);
                            if (r2 != 0) {
                            }
                            if (r32 != 0) {
                            }
                            if (fileInputStream != null) {
                            }
                            return jSONArray;
                        }
                    } catch (IOException | JSONException e5) {
                        e = e5;
                        r32 = 0;
                        r2 = r23;
                        Log.e(a, "Failed to read crashes", e);
                        if (r2 != 0) {
                            try {
                                r2.close();
                            } catch (IOException e6) {
                                e = e6;
                                str = a;
                                str2 = "Failed to close buffers";
                                Log.e(str, str2, e);
                                return jSONArray;
                            }
                        }
                        if (r32 != 0) {
                            r32.close();
                        }
                        if (fileInputStream != null) {
                            fileInputStream.close();
                        }
                        return jSONArray;
                    } catch (Throwable th3) {
                        r34 = 0;
                        r9 = th3;
                        r4 = r33;
                        r9 = r9;
                        r3 = r33;
                        if (r4 != 0) {
                        }
                        if (r3 != 0) {
                        }
                        if (fileInputStream != null) {
                        }
                        throw r9;
                    }
                } else {
                    fileInputStream = null;
                    r35 = 0;
                    r9 = r9;
                    r22 = r23;
                }
                if (r22 != 0) {
                    try {
                        r22.close();
                    } catch (IOException e7) {
                        e = e7;
                        r9 = r9;
                        str = a;
                        str2 = "Failed to close buffers";
                    } catch (Throwable th4) {
                        throw th4;
                    }
                }
                if (r35 != 0) {
                    r35.close();
                }
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException | JSONException e8) {
                e = e8;
                fileInputStream = null;
                r32 = 0;
                r2 = r23;
                Log.e(a, "Failed to read crashes", e);
                if (r2 != 0) {
                }
                if (r32 != 0) {
                }
                if (fileInputStream != null) {
                }
                return jSONArray;
            } catch (Throwable th5) {
                fileInputStream = null;
                r34 = 0;
                r9 = th5;
                r4 = r33;
                r9 = r9;
                r3 = r33;
                if (r4 != 0) {
                }
                if (r3 != 0) {
                }
                if (fileInputStream != null) {
                }
                throw r9;
            }
        }
        return jSONArray;
    }

    @WorkerThread
    public static void a(d dVar, Context context) {
        if (dVar != null && context != null) {
            synchronized (b) {
                try {
                    String a2 = a.a("debuglogs", context);
                    File file = new File(context.getFilesDir(), a2);
                    if (file.exists()) {
                        int ae = com.facebook.ads.internal.r.a.ae(context);
                        long length = file.length();
                        if (ae > 0 && length > ((long) ae)) {
                            boolean delete = file.delete();
                            b(context, 0);
                            c.clear();
                            d.clear();
                            if (!delete) {
                                Log.e(AudienceNetworkAds.TAG, "Can't delete debug events file.");
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Purge debug events file. File size: ");
                                sb.append(length);
                                sb.append(", DropCounter: ");
                                sb.append(e.getAndIncrement());
                                a(new Exception(sb.toString()), context, new HashMap());
                                return;
                            }
                        }
                    }
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("id", UUID.randomUUID().toString());
                    jSONObject.put("type", dVar.a());
                    jSONObject.put("time", v.a(dVar.b()));
                    jSONObject.put("session_time", v.a(dVar.c()));
                    jSONObject.put("session_id", dVar.d());
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_DATA, dVar.e() != null ? new JSONObject(dVar.e()) : new JSONObject());
                    jSONObject.put("attempt", String.valueOf(0));
                    FileOutputStream openFileOutput = context.openFileOutput(a2, 32768);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(jSONObject.toString());
                    sb2.append("\n");
                    openFileOutput.write(sb2.toString().getBytes());
                    openFileOutput.close();
                    b(context, context.getApplicationContext().getSharedPreferences(a.a("DEBUG_PREF", context), 0).getInt("EventCount", 0) + 1);
                } catch (Exception e2) {
                    Log.e(a, "Failed to store crash", e2);
                }
            }
        }
    }

    public static void a(String str) {
        Integer num = (Integer) d.get(str);
        if (num == null) {
            num = Integer.valueOf(0);
        } else {
            d.remove(str);
        }
        d.put(str, Integer.valueOf(num.intValue() + 1));
    }

    @WorkerThread
    public static void b(Context context) {
        synchronized (b) {
            File file = new File(context.getFilesDir(), a.a("debuglogs", context));
            if (file.exists()) {
                file.delete();
            }
            b(context, 0);
            c.clear();
            d.clear();
        }
    }

    private static void b(Context context, int i) {
        Editor edit = context.getApplicationContext().getSharedPreferences(a.a("DEBUG_PREF", context), 0).edit();
        String str = "EventCount";
        if (i < 0) {
            i = 0;
        }
        edit.putInt(str, i).apply();
    }

    public static void b(String str) {
        if (d.containsKey(str)) {
            d.remove(str);
        }
        c.add(str);
    }

    public static int c(Context context) {
        return context.getApplicationContext().getSharedPreferences(a.a("DEBUG_PREF", context), 0).getInt("EventCount", 0) - c.size();
    }

    public static boolean c(String str) {
        return c.contains(str) || d.containsKey(str);
    }

    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r6v0, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r5v0, types: [java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r3v2, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r6v1 */
    /* JADX WARNING: type inference failed for: r5v1 */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r5v2, types: [java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r3v4, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r0v5, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r6v2 */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r5v3 */
    /* JADX WARNING: type inference failed for: r5v4 */
    /* JADX WARNING: type inference failed for: r6v4 */
    /* JADX WARNING: type inference failed for: r5v5 */
    /* JADX WARNING: type inference failed for: r5v7, types: [java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r3v6, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r0v8, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r0v10 */
    /* JADX WARNING: type inference failed for: r5v8 */
    /* JADX WARNING: type inference failed for: r0v11 */
    /* JADX WARNING: type inference failed for: r5v9 */
    /* JADX WARNING: type inference failed for: r5v10 */
    /* JADX WARNING: type inference failed for: r5v11, types: [java.io.Reader, java.io.InputStreamReader] */
    /* JADX WARNING: type inference failed for: r0v12 */
    /* JADX WARNING: type inference failed for: r6v8 */
    /* JADX WARNING: type inference failed for: r6v9, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r0v13 */
    /* JADX WARNING: type inference failed for: r3v7 */
    /* JADX WARNING: type inference failed for: r0v14 */
    /* JADX WARNING: type inference failed for: r0v17, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r3v12 */
    /* JADX WARNING: type inference failed for: r3v13 */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r6v10 */
    /* JADX WARNING: type inference failed for: r5v12 */
    /* JADX WARNING: type inference failed for: r5v13 */
    /* JADX WARNING: type inference failed for: r0v18 */
    /* JADX WARNING: type inference failed for: r5v14 */
    /* JADX WARNING: type inference failed for: r5v15 */
    /* JADX WARNING: type inference failed for: r3v18 */
    /* JADX WARNING: type inference failed for: r0v19 */
    /* JADX WARNING: type inference failed for: r5v16 */
    /* JADX WARNING: type inference failed for: r5v17 */
    /* JADX WARNING: type inference failed for: r5v18 */
    /* JADX WARNING: type inference failed for: r5v19 */
    /* JADX WARNING: type inference failed for: r5v20 */
    /* JADX WARNING: type inference failed for: r5v21 */
    /* JADX WARNING: type inference failed for: r6v11 */
    /* JADX WARNING: type inference failed for: r6v12 */
    /* JADX WARNING: type inference failed for: r0v20 */
    /* JADX WARNING: type inference failed for: r0v21 */
    /* JADX WARNING: type inference failed for: r0v22 */
    /* JADX WARNING: type inference failed for: r0v23 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r6v1
  assigns: []
  uses: []
  mth insns count: 176
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0144 A[Catch:{ IOException -> 0x0136 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0102 A[SYNTHETIC, Splitter:B:70:0x0102] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x010a A[Catch:{ IOException -> 0x0106 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x010f A[Catch:{ IOException -> 0x0106 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0114 A[Catch:{ IOException -> 0x0106 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0130 A[SYNTHETIC, Splitter:B:89:0x0130] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x013a A[Catch:{ IOException -> 0x0136 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x013f A[Catch:{ IOException -> 0x0136 }] */
    /* JADX WARNING: Unknown variable types count: 20 */
    @WorkerThread
    public static boolean d(Context context) {
        ? r6;
        ? r5;
        FileInputStream fileInputStream;
        ? r3;
        ? r62;
        ? r52;
        ? r0;
        ? r53;
        ? r32;
        ? r02;
        ? r54;
        ? r55;
        ? r56;
        ? r33;
        ? r03;
        ? inputStreamReader;
        ? bufferedReader;
        ? r04;
        StringBuilder sb;
        ? openFileOutput;
        ? r34;
        JSONArray jSONArray = new JSONArray();
        synchronized (b) {
            ? r35 = 0;
            try {
                if (new File(context.getFilesDir(), a.a("debuglogs", context)).exists()) {
                    fileInputStream = context.openFileInput(a.a("debuglogs", context));
                    try {
                        inputStreamReader = new InputStreamReader(fileInputStream);
                        try {
                            bufferedReader = new BufferedReader(inputStreamReader);
                            while (true) {
                                try {
                                    String readLine = bufferedReader.readLine();
                                    if (readLine == null) {
                                        break;
                                    }
                                    JSONObject jSONObject = new JSONObject(readLine);
                                    String string = jSONObject.getString("id");
                                    if (!c.contains(string)) {
                                        if (d.containsKey(string)) {
                                            jSONObject.put("attempt", String.valueOf(d.get(string)));
                                        }
                                        jSONArray.put(jSONObject);
                                    }
                                } catch (IOException | JSONException e2) {
                                    e = e2;
                                    r04 = 0;
                                    r32 = bufferedReader;
                                    r53 = inputStreamReader;
                                    r02 = r04;
                                    try {
                                        Log.e(a, "Failed to rewrite File.", e);
                                        if (r32 != 0) {
                                        }
                                        if (r53 != 0) {
                                        }
                                        if (fileInputStream != null) {
                                        }
                                        if (r02 != 0) {
                                        }
                                        c.clear();
                                        d.clear();
                                        return false;
                                    } catch (Throwable th) {
                                        th = th;
                                        r62 = r32;
                                        r52 = r53;
                                        r0 = r02;
                                        r3 = r0;
                                        r6 = r62;
                                        r5 = r52;
                                        if (r6 != 0) {
                                        }
                                        if (r5 != 0) {
                                        }
                                        if (fileInputStream != null) {
                                        }
                                        if (r3 != 0) {
                                        }
                                        c.clear();
                                        d.clear();
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    r3 = r35;
                                    r5 = inputStreamReader;
                                    r6 = bufferedReader;
                                    if (r6 != 0) {
                                    }
                                    if (r5 != 0) {
                                    }
                                    if (fileInputStream != null) {
                                    }
                                    if (r3 != 0) {
                                    }
                                    c.clear();
                                    d.clear();
                                    throw th;
                                }
                            }
                            sb = new StringBuilder();
                            int length = jSONArray.length();
                            for (int i = 0; i < length; i++) {
                                sb.append(jSONArray.getJSONObject(i).toString());
                                sb.append(10);
                            }
                            openFileOutput = context.openFileOutput(a.a("debuglogs", context), 0);
                        } catch (IOException | JSONException e3) {
                            e = e3;
                            r02 = 0;
                            r32 = r35;
                            r53 = inputStreamReader;
                            Log.e(a, "Failed to rewrite File.", e);
                            if (r32 != 0) {
                            }
                            if (r53 != 0) {
                            }
                            if (fileInputStream != null) {
                            }
                            if (r02 != 0) {
                            }
                            c.clear();
                            d.clear();
                            return false;
                        } catch (Throwable th3) {
                            th = th3;
                            r6 = 0;
                            r3 = r35;
                            r5 = inputStreamReader;
                            if (r6 != 0) {
                            }
                            if (r5 != 0) {
                            }
                            if (fileInputStream != null) {
                            }
                            if (r3 != 0) {
                            }
                            c.clear();
                            d.clear();
                            throw th;
                        }
                    } catch (IOException | JSONException e4) {
                        e = e4;
                        r02 = 0;
                        r53 = 0;
                        r32 = r35;
                        Log.e(a, "Failed to rewrite File.", e);
                        if (r32 != 0) {
                        }
                        if (r53 != 0) {
                        }
                        if (fileInputStream != null) {
                        }
                        if (r02 != 0) {
                        }
                        c.clear();
                        d.clear();
                        return false;
                    } catch (Throwable th4) {
                        th = th4;
                        r55 = 0;
                        r6 = r54;
                        r3 = r35;
                        r5 = r54;
                        if (r6 != 0) {
                            try {
                                r6.close();
                            } catch (IOException e5) {
                                Log.e(a, "Failed to close buffers", e5);
                                c.clear();
                                d.clear();
                                throw th;
                            }
                        }
                        if (r5 != 0) {
                            r5.close();
                        }
                        if (fileInputStream != null) {
                            fileInputStream.close();
                        }
                        if (r3 != 0) {
                            r3.close();
                        }
                        c.clear();
                        d.clear();
                        throw th;
                    }
                    try {
                        openFileOutput.write(sb.toString().getBytes());
                        r34 = bufferedReader;
                        r56 = inputStreamReader;
                        r03 = openFileOutput;
                    } catch (IOException | JSONException e6) {
                        e = e6;
                        r04 = openFileOutput;
                        r32 = bufferedReader;
                        r53 = inputStreamReader;
                        r02 = r04;
                        Log.e(a, "Failed to rewrite File.", e);
                        if (r32 != 0) {
                        }
                        if (r53 != 0) {
                        }
                        if (fileInputStream != null) {
                        }
                        if (r02 != 0) {
                        }
                        c.clear();
                        d.clear();
                        return false;
                    } catch (Throwable th5) {
                        th = th5;
                        r52 = inputStreamReader;
                        r62 = bufferedReader;
                        r0 = openFileOutput;
                        r3 = r0;
                        r6 = r62;
                        r5 = r52;
                        if (r6 != 0) {
                        }
                        if (r5 != 0) {
                        }
                        if (fileInputStream != null) {
                        }
                        if (r3 != 0) {
                        }
                        c.clear();
                        d.clear();
                        throw th;
                    }
                } else {
                    r03 = 0;
                    fileInputStream = null;
                    r56 = 0;
                    r34 = r35;
                }
                try {
                    b(context, c(context));
                    if (r33 != 0) {
                        try {
                            r33.close();
                        } catch (IOException e7) {
                            th = e7;
                            Log.e(a, "Failed to close buffers", th);
                        } catch (Throwable th6) {
                            throw th6;
                        }
                    }
                    if (r56 != 0) {
                        r56.close();
                    }
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    if (r03 != 0) {
                        r03.close();
                    }
                    c.clear();
                    d.clear();
                    return true;
                } catch (IOException | JSONException e8) {
                    e = e8;
                    r53 = r56;
                    r32 = r33;
                    r02 = r03;
                    Log.e(a, "Failed to rewrite File.", e);
                    if (r32 != 0) {
                        try {
                            r32.close();
                        } catch (IOException e9) {
                            Log.e(a, "Failed to close buffers", e9);
                            c.clear();
                            d.clear();
                            return false;
                        }
                    }
                    if (r53 != 0) {
                        r53.close();
                    }
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    if (r02 != 0) {
                        r02.close();
                    }
                    c.clear();
                    d.clear();
                    return false;
                }
            } catch (IOException | JSONException e10) {
                e = e10;
                r02 = 0;
                fileInputStream = null;
                r53 = 0;
                r32 = r35;
                Log.e(a, "Failed to rewrite File.", e);
                if (r32 != 0) {
                }
                if (r53 != 0) {
                }
                if (fileInputStream != null) {
                }
                if (r02 != 0) {
                }
                c.clear();
                d.clear();
                return false;
            } catch (Throwable th7) {
                th = th7;
                fileInputStream = null;
                r55 = 0;
                r6 = r54;
                r3 = r35;
                r5 = r54;
                if (r6 != 0) {
                }
                if (r5 != 0) {
                }
                if (fileInputStream != null) {
                }
                if (r3 != 0) {
                }
                c.clear();
                d.clear();
                throw th;
            }
        }
    }
}
