package com.facebook.ads.internal.s;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.VisibleForTesting;
import com.facebook.ads.internal.j.c;
import com.facebook.ads.internal.j.d;
import com.facebook.ads.internal.k.e;
import com.facebook.ads.internal.s.b.a;
import com.facebook.ads.internal.w.b.v;
import com.facebook.share.internal.ShareConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class i implements a {
    private static final String a = "i";
    private Context b;
    private d c;

    @VisibleForTesting
    public i(Context context, d dVar) {
        this.b = context;
        this.c = dVar;
    }

    private static JSONArray a(JSONArray jSONArray, JSONArray jSONArray2, int i) {
        JSONArray jSONArray3 = jSONArray;
        JSONArray jSONArray4 = jSONArray2;
        if (jSONArray3 == null) {
            return jSONArray4;
        }
        if (jSONArray4 == null) {
            return jSONArray3;
        }
        int length = jSONArray.length();
        int length2 = jSONArray2.length();
        JSONArray jSONArray5 = new JSONArray();
        int i2 = 0;
        int i3 = i;
        int i4 = 0;
        double d = Double.MAX_VALUE;
        double d2 = Double.MAX_VALUE;
        JSONObject jSONObject = null;
        JSONObject jSONObject2 = null;
        while (true) {
            if ((i2 < length || i4 < length2) && i3 > 0) {
                if (i2 < length && jSONObject == null) {
                    try {
                        jSONObject = jSONArray3.getJSONObject(i2);
                        d = jSONObject.getDouble("time");
                    } catch (JSONException unused) {
                        d = Double.MAX_VALUE;
                        jSONObject = null;
                    }
                    i2++;
                }
                if (i4 < length2 && jSONObject2 == null) {
                    try {
                        jSONObject2 = jSONArray4.getJSONObject(i4);
                        d2 = jSONObject2.getDouble("time");
                    } catch (JSONException unused2) {
                        d2 = Double.MAX_VALUE;
                        jSONObject2 = null;
                    }
                    i4++;
                }
                if (jSONObject != null || jSONObject2 != null) {
                    if (jSONObject == null || d2 < d) {
                        jSONArray5.put(jSONObject2);
                        d2 = Double.MAX_VALUE;
                        jSONObject2 = null;
                    } else {
                        jSONArray5.put(jSONObject);
                        d = Double.MAX_VALUE;
                        jSONObject = null;
                    }
                    i3--;
                }
            }
        }
        if (i3 > 0) {
            if (jSONObject != null) {
                jSONArray5.put(jSONObject);
                return jSONArray5;
            } else if (jSONObject2 != null) {
                jSONArray5.put(jSONObject2);
            }
        }
        return jSONArray5;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00f0  */
    private JSONObject a(int i) {
        Cursor cursor;
        Cursor cursor2;
        JSONArray jSONArray;
        Object obj;
        JSONObject jSONObject;
        try {
            cursor2 = this.c.d();
            try {
                cursor = this.c.a(i);
            } catch (JSONException unused) {
                cursor = null;
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (Throwable th) {
                th = th;
                cursor = null;
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
            try {
                if (cursor.getCount() > 0) {
                    obj = a(cursor);
                    jSONArray = new JSONArray();
                    cursor.moveToPosition(-1);
                    while (cursor.moveToNext()) {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("id", cursor.getString(2));
                        jSONObject2.put("token_id", cursor.getString(0));
                        jSONObject2.put("type", cursor.getString(4));
                        jSONObject2.put("time", v.a(cursor.getDouble(5)));
                        jSONObject2.put("session_time", v.a(cursor.getDouble(6)));
                        jSONObject2.put("session_id", cursor.getString(7));
                        String string = cursor.getString(8);
                        jSONObject2.put(ShareConstants.WEB_DIALOG_PARAM_DATA, string != null ? new JSONObject(string) : new JSONObject());
                        jSONObject2.put("attempt", cursor.getString(9));
                        jSONArray.put(jSONObject2);
                    }
                } else {
                    obj = null;
                    jSONArray = null;
                }
                if (com.facebook.ads.internal.r.a.p(this.b)) {
                    JSONArray a2 = e.a(this.b, i);
                    if (a2 != null && a2.length() > 0) {
                        jSONArray = a(a2, jSONArray, i);
                    }
                }
                if (jSONArray != null) {
                    jSONObject = new JSONObject();
                    if (obj != null) {
                        jSONObject.put("tokens", obj);
                    }
                    jSONObject.put("events", jSONArray);
                } else {
                    jSONObject = null;
                }
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (cursor != null) {
                    cursor.close();
                }
                return jSONObject;
            } catch (JSONException unused2) {
                if (cursor2 != null) {
                }
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                }
                if (cursor != null) {
                }
                throw th;
            }
        } catch (JSONException unused3) {
            cursor2 = null;
            cursor = null;
            if (cursor2 != null) {
            }
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = null;
            cursor = null;
            if (cursor2 != null) {
            }
            if (cursor != null) {
            }
            throw th;
        }
    }

    private JSONObject a(Cursor cursor) {
        JSONObject jSONObject = new JSONObject();
        while (cursor.moveToNext()) {
            jSONObject.put(cursor.getString(0), cursor.getString(1));
        }
        return jSONObject;
    }

    private void a(String str) {
        if (e.c(str)) {
            e.a(str);
        } else {
            this.c.a(str);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x011e  */
    private JSONObject e() {
        Cursor cursor;
        Cursor cursor2;
        Throwable th;
        JSONArray jSONArray;
        Object obj;
        JSONObject jSONObject = null;
        try {
            cursor2 = this.c.f();
            try {
                cursor = this.c.e();
            } catch (JSONException unused) {
                cursor = null;
                if (cursor2 != null) {
                }
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = null;
                th = th3;
                if (cursor2 != null) {
                }
                if (cursor != null) {
                }
                throw th;
            }
            try {
                if (cursor2.getCount() <= 0 || cursor.getCount() <= 0) {
                    obj = null;
                    jSONArray = null;
                } else {
                    obj = a(cursor2);
                    jSONArray = new JSONArray();
                    cursor.moveToPosition(-1);
                    while (cursor.moveToNext()) {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("id", cursor.getString(c.a.a));
                        jSONObject2.put("token_id", cursor.getString(c.b.a));
                        jSONObject2.put("type", cursor.getString(c.d.a));
                        jSONObject2.put("time", v.a(cursor.getDouble(c.e.a)));
                        jSONObject2.put("session_time", v.a(cursor.getDouble(c.f.a)));
                        jSONObject2.put("session_id", cursor.getString(c.g.a));
                        String string = cursor.getString(c.h.a);
                        jSONObject2.put(ShareConstants.WEB_DIALOG_PARAM_DATA, string != null ? new JSONObject(string) : new JSONObject());
                        jSONObject2.put("attempt", cursor.getString(c.i.a));
                        jSONArray.put(jSONObject2);
                    }
                }
                if (com.facebook.ads.internal.r.a.p(this.b)) {
                    JSONArray a2 = e.a(this.b);
                    if (a2 != null && a2.length() > 0) {
                        int i = 0;
                        if (a2 != null) {
                            i = 0 + a2.length();
                        }
                        if (jSONArray != null) {
                            i += jSONArray.length();
                        }
                        jSONArray = a(a2, jSONArray, i);
                    }
                }
                if (jSONArray != null) {
                    JSONObject jSONObject3 = new JSONObject();
                    if (obj != null) {
                        jSONObject3.put("tokens", obj);
                    }
                    jSONObject3.put("events", jSONArray);
                    jSONObject = jSONObject3;
                }
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (cursor != null) {
                    cursor.close();
                }
                return jSONObject;
            } catch (JSONException unused2) {
                if (cursor2 != null) {
                }
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th4) {
                th = th4;
                if (cursor2 != null) {
                }
                if (cursor != null) {
                }
                throw th;
            }
        } catch (JSONException unused3) {
            cursor2 = null;
            cursor = null;
            if (cursor2 != null) {
                cursor2.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th5) {
            cursor = null;
            th = th5;
            cursor2 = null;
            if (cursor2 != null) {
                cursor2.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public JSONObject a() {
        int x = com.facebook.ads.internal.r.a.x(this.b);
        return x > 0 ? a(x) : e();
    }

    public boolean a(JSONArray jSONArray) {
        boolean p = com.facebook.ads.internal.r.a.p(this.b);
        boolean z = true;
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String string = jSONObject.getString("id");
                int i2 = jSONObject.getInt("code");
                if (i2 == 1) {
                    if (!this.c.b(string)) {
                        if (!p) {
                        }
                    }
                } else if (i2 < 1000 || i2 >= 2000) {
                    if (i2 >= 2000) {
                        if (i2 < 3000) {
                            if (!this.c.b(string)) {
                                if (!p) {
                                }
                            }
                        }
                    }
                } else {
                    a(string);
                    z = false;
                }
                e.b(string);
            } catch (JSONException unused) {
            }
        }
        return z;
    }

    public void b() {
        this.c.g();
        this.c.b();
        e.d(this.b);
    }

    public void b(JSONArray jSONArray) {
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            try {
                a(jSONArray.getJSONObject(i).getString("id"));
            } catch (JSONException unused) {
            }
        }
    }

    public void c() {
        this.c.h();
        e.b(this.b);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0035  */
    public boolean d() {
        Cursor cursor;
        int x = com.facebook.ads.internal.r.a.x(this.b);
        boolean z = true;
        if (x < 1) {
            return false;
        }
        try {
            cursor = this.c.d();
            try {
                if ((cursor.moveToFirst() ? cursor.getInt(0) : 0) + e.c(this.b) <= x) {
                    z = false;
                }
                if (cursor != null) {
                    cursor.close();
                }
                return z;
            } catch (Throwable th) {
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
