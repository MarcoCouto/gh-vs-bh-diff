package com.facebook.ads.internal.v.a;

import android.os.Build.VERSION;
import android.support.annotation.AnyThread;
import android.support.annotation.WorkerThread;
import android.util.Log;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.net.ssl.HttpsURLConnection;

@WorkerThread
public class a {
    private static int[] f = new int[20];
    private static final String g = "a";
    private static C0011a j;
    protected final q a = new f() {
    };
    protected final d b = new e();
    protected r c = new g();
    protected int d = 2000;
    protected int e = 8000;
    private int h = 3;
    private Map<String, String> i = new TreeMap();
    private boolean k;
    private Set<String> l;
    private Set<String> m;

    /* renamed from: com.facebook.ads.internal.v.a.a$a reason: collision with other inner class name */
    public interface C0011a {
        @WorkerThread
        Map<String, String> a();
    }

    static {
        if (VERSION.SDK_INT < 8) {
            System.setProperty("http.keepAlive", "false");
        }
        if (VERSION.SDK_INT > 8 && CookieHandler.getDefault() == null) {
            CookieHandler.setDefault(new CookieManager());
        }
    }

    public static synchronized void a(C0011a aVar) {
        synchronized (a.class) {
            j = aVar;
        }
    }

    /* access modifiers changed from: protected */
    public int a(int i2) {
        return 1000 * f[i2 + 2];
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x001f A[SYNTHETIC, Splitter:B:17:0x001f] */
    public int a(HttpURLConnection httpURLConnection, byte[] bArr) {
        OutputStream outputStream;
        try {
            outputStream = this.a.a(httpURLConnection);
            if (outputStream != null) {
                try {
                    this.a.a(outputStream, bArr);
                } catch (Throwable th) {
                    th = th;
                    if (outputStream != null) {
                    }
                    throw th;
                }
            }
            int responseCode = httpURLConnection.getResponseCode();
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception unused) {
                }
            }
            return responseCode;
        } catch (Throwable th2) {
            th = th2;
            outputStream = null;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception unused2) {
                }
            }
            throw th;
        }
    }

    @AnyThread
    public a a(String str, String str2) {
        this.i.put(str, str2);
        return this;
    }

    public n a(l lVar) {
        long currentTimeMillis = System.currentTimeMillis();
        int i2 = 0;
        while (i2 < this.h) {
            try {
                c(a(i2));
                if (this.c.a()) {
                    r rVar = this.c;
                    StringBuilder sb = new StringBuilder();
                    sb.append(i2 + 1);
                    sb.append("of");
                    sb.append(this.h);
                    sb.append(", trying ");
                    sb.append(lVar.a());
                    rVar.a(sb.toString());
                }
                long currentTimeMillis2 = System.currentTimeMillis();
                try {
                    n a2 = a(lVar.a(), lVar.b(), lVar.c(), lVar.d());
                    if (a2 != null) {
                        return a2;
                    }
                    currentTimeMillis = currentTimeMillis2;
                    i2++;
                } catch (m e2) {
                    long j2 = currentTimeMillis2;
                    e = e2;
                    currentTimeMillis = j2;
                    if (!a((Throwable) e, currentTimeMillis) && i2 < this.h - 1) {
                        continue;
                        i2++;
                    } else if (this.a.a(e) || i2 >= this.h - 1) {
                        throw e;
                    } else {
                        try {
                            Thread.sleep((long) this.d);
                            i2++;
                        } catch (InterruptedException unused) {
                            throw e;
                        }
                    }
                }
            } catch (m e3) {
                e = e3;
                if (!a((Throwable) e, currentTimeMillis)) {
                }
                if (this.a.a(e)) {
                }
                throw e;
            }
        }
        return null;
    }

    /* JADX INFO: used method not loaded: com.facebook.ads.internal.v.a.r.a(com.facebook.ads.internal.v.a.n):null, types can be incorrect */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:86|87|88|89|90) */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00d5, code lost:
        r7 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r7.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x010e, code lost:
        throw new com.facebook.ads.internal.v.a.m(r7, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0114, code lost:
        throw new com.facebook.ads.internal.v.a.m(r7, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x011d, code lost:
        r5.c.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0124, code lost:
        r6.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:86:0x0106 */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0124  */
    public n a(String str, j jVar, String str2, byte[] bArr) {
        HttpURLConnection httpURLConnection;
        n nVar = null;
        try {
            this.k = false;
            httpURLConnection = a(str);
            try {
                a(httpURLConnection, jVar, str2);
                for (String str3 : this.i.keySet()) {
                    httpURLConnection.setRequestProperty(str3, (String) this.i.get(str3));
                }
                synchronized (a.class) {
                    if (j != null) {
                        Map a2 = j.a();
                        for (String str4 : a2.keySet()) {
                            httpURLConnection.setRequestProperty(str4, (String) a2.get(str4));
                        }
                    }
                }
                if (this.c.a()) {
                    this.c.a(httpURLConnection, bArr);
                }
                httpURLConnection.connect();
                boolean z = true;
                this.k = true;
                boolean z2 = this.m != null && !this.m.isEmpty();
                if (this.l == null || this.l.isEmpty()) {
                    z = false;
                }
                if ((httpURLConnection instanceof HttpsURLConnection) && (z2 || z)) {
                    try {
                        o.a((HttpsURLConnection) httpURLConnection, this.m, this.l);
                    } catch (Exception e2) {
                        Log.e(g, "Unable to validate SSL certificates.", e2);
                    }
                }
                if (httpURLConnection.getDoOutput() && bArr != null) {
                    a(httpURLConnection, bArr);
                }
                n a3 = httpURLConnection.getDoInput() ? a(httpURLConnection) : new n(httpURLConnection, null);
                if (this.c.a()) {
                    this.c.a(a3);
                }
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                return a3;
            } catch (Exception e3) {
                e = e3;
            }
        } catch (Exception e4) {
            e = e4;
            httpURLConnection = null;
            n b2 = b(httpURLConnection);
            if (b2 != null) {
                try {
                    if (b2.a() > 0) {
                        if (this.c.a()) {
                            this.c.a(b2);
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        return b2;
                    }
                } catch (Throwable th) {
                    th = th;
                    nVar = b2;
                    if (this.c.a()) {
                    }
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            }
            throw new m(e, b2);
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            if (this.c.a()) {
            }
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    public n a(String str, p pVar) {
        return b((l) new i(str, pVar));
    }

    public n a(String str, String str2, byte[] bArr) {
        return b((l) new k(str, null, str2, bArr));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0021 A[SYNTHETIC, Splitter:B:17:0x0021] */
    public n a(HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        byte[] bArr = null;
        try {
            inputStream = this.a.b(httpURLConnection);
            if (inputStream != null) {
                try {
                    bArr = this.a.a(inputStream);
                } catch (Throwable th) {
                    th = th;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception unused) {
                        }
                    }
                    throw th;
                }
            }
            n nVar = new n(httpURLConnection, bArr);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception unused2) {
                }
            }
            return nVar;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            if (inputStream != null) {
            }
            throw th;
        }
    }

    public p a() {
        return new p();
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(String str) {
        try {
            new URL(str);
            return this.a.a(str);
        } catch (MalformedURLException e2) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" is not a valid URL");
            throw new IllegalArgumentException(sb.toString(), e2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(l lVar, b bVar) {
        this.b.a(this, bVar).a(lVar);
    }

    public void a(String str, p pVar, b bVar) {
        a((l) new i(str, pVar), bVar);
    }

    /* access modifiers changed from: protected */
    public void a(HttpURLConnection httpURLConnection, j jVar, String str) {
        httpURLConnection.setConnectTimeout(this.d);
        httpURLConnection.setReadTimeout(this.e);
        this.a.a(httpURLConnection, jVar, str);
    }

    @AnyThread
    public void a(Set<String> set) {
        this.m = set;
    }

    /* access modifiers changed from: protected */
    public boolean a(Throwable th, long j2) {
        long currentTimeMillis = (System.currentTimeMillis() - j2) + 10;
        if (this.c.a()) {
            r rVar = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("ELAPSED TIME = ");
            sb.append(currentTimeMillis);
            sb.append(", CT = ");
            sb.append(this.d);
            sb.append(", RT = ");
            sb.append(this.e);
            rVar.a(sb.toString());
        }
        boolean z = false;
        if (this.k) {
            if (currentTimeMillis >= ((long) this.e)) {
                z = true;
            }
            return z;
        }
        if (currentTimeMillis >= ((long) this.d)) {
            z = true;
        }
        return z;
    }

    public n b(l lVar) {
        try {
            return a(lVar.a(), lVar.b(), lVar.c(), lVar.d());
        } catch (m e2) {
            this.a.a(e2);
            return null;
        } catch (Exception e3) {
            this.a.a(new m(e3, null));
            return null;
        }
    }

    public n b(String str, p pVar) {
        return b((l) new k(str, pVar));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x001f A[SYNTHETIC, Splitter:B:17:0x001f] */
    public n b(HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        byte[] bArr = null;
        try {
            inputStream = httpURLConnection.getErrorStream();
            if (inputStream != null) {
                try {
                    bArr = this.a.a(inputStream);
                } catch (Throwable th) {
                    th = th;
                    if (inputStream != null) {
                    }
                    throw th;
                }
            }
            n nVar = new n(httpURLConnection, bArr);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception unused) {
                }
            }
            return nVar;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception unused2) {
                }
            }
            throw th;
        }
    }

    public void b() {
        this.i.clear();
    }

    @AnyThread
    public void b(int i2) {
        if (i2 < 1 || i2 > 18) {
            throw new IllegalArgumentException("Maximum retries must be between 1 and 18");
        }
        this.h = i2;
    }

    public void b(String str, p pVar, b bVar) {
        a((l) new k(str, pVar), bVar);
    }

    @AnyThread
    public void b(Set<String> set) {
        this.l = set;
    }

    @AnyThread
    public void c(int i2) {
        this.d = i2;
    }

    @AnyThread
    public void d(int i2) {
        this.e = i2;
    }
}
