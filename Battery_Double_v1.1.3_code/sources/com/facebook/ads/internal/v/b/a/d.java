package com.facebook.ads.internal.v.b.a;

import java.io.File;
import java.util.Comparator;

class d {

    private static final class a implements Comparator<File> {
        private a() {
        }

        /* renamed from: a */
        public int compare(File file, File file2) {
            long lastModified = file.lastModified();
            long lastModified2 = file2.lastModified();
            if (lastModified < lastModified2) {
                return -1;
            }
            return lastModified == lastModified2 ? 0 : 1;
        }
    }
}
