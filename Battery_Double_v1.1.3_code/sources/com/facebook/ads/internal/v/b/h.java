package com.facebook.ads.internal.v.b;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class h implements n {
    public final String a;
    private HttpURLConnection b;
    private InputStream c;
    private volatile int d;
    private volatile String e;

    public h(h hVar) {
        this.d = Integer.MIN_VALUE;
        this.a = hVar.a;
        this.e = hVar.e;
        this.d = hVar.d;
    }

    public h(String str) {
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(str);
        this(str, TextUtils.isEmpty(fileExtensionFromUrl) ? null : singleton.getMimeTypeFromExtension(fileExtensionFromUrl));
    }

    public h(String str, String str2) {
        this.d = Integer.MIN_VALUE;
        this.a = (String) j.a(str);
        this.e = str2;
    }

    private HttpURLConnection a(int i, int i2) {
        String str;
        HttpURLConnection httpURLConnection;
        boolean z;
        String str2 = this.a;
        int i3 = 0;
        do {
            String str3 = "ProxyCache";
            StringBuilder sb = new StringBuilder();
            sb.append("Open connection ");
            if (i > 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(" with offset ");
                sb2.append(i);
                str = sb2.toString();
            } else {
                str = "";
            }
            sb.append(str);
            sb.append(" to ");
            sb.append(str2);
            Log.d(str3, sb.toString());
            httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            if (i > 0) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("bytes=");
                sb3.append(i);
                sb3.append("-");
                httpURLConnection.setRequestProperty("Range", sb3.toString());
            }
            if (i2 > 0) {
                httpURLConnection.setConnectTimeout(i2);
                httpURLConnection.setReadTimeout(i2);
            }
            int responseCode = httpURLConnection.getResponseCode();
            z = responseCode == 301 || responseCode == 302 || responseCode == 303;
            if (z) {
                str2 = httpURLConnection.getHeaderField(HttpRequest.HEADER_LOCATION);
                i3++;
                httpURLConnection.disconnect();
            }
            if (i3 > 5) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Too many redirects: ");
                sb4.append(i3);
                throw new l(sb4.toString());
            }
        } while (z);
        return httpURLConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    private void d() {
        Closeable closeable;
        HttpURLConnection httpURLConnection;
        IOException e2;
        StringBuilder sb = new StringBuilder();
        sb.append("Read content info from ");
        sb.append(this.a);
        Log.d("ProxyCache", sb.toString());
        try {
            httpURLConnection = a(0, 10000);
            try {
                this.d = httpURLConnection.getContentLength();
                this.e = httpURLConnection.getContentType();
                closeable = httpURLConnection.getInputStream();
                String str = "ProxyCache";
                try {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Content info for `");
                    sb2.append(this.a);
                    sb2.append("`: mime: ");
                    sb2.append(this.e);
                    sb2.append(", content-length: ");
                    sb2.append(this.d);
                    Log.i(str, sb2.toString());
                    m.a(closeable);
                    if (httpURLConnection == null) {
                        return;
                    }
                } catch (IOException e3) {
                    e2 = e3;
                    String str2 = "ProxyCache";
                    try {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Error fetching info from ");
                        sb3.append(this.a);
                        Log.e(str2, sb3.toString(), e2);
                        m.a(closeable);
                        if (httpURLConnection == null) {
                            return;
                        }
                        httpURLConnection.disconnect();
                    } catch (Throwable th) {
                        th = th;
                        m.a(closeable);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        throw th;
                    }
                }
            } catch (IOException e4) {
                e2 = e4;
                closeable = null;
                String str22 = "ProxyCache";
                StringBuilder sb32 = new StringBuilder();
                sb32.append("Error fetching info from ");
                sb32.append(this.a);
                Log.e(str22, sb32.toString(), e2);
                m.a(closeable);
                if (httpURLConnection == null) {
                }
                httpURLConnection.disconnect();
            } catch (Throwable th2) {
                th = th2;
                closeable = null;
                m.a(closeable);
                if (httpURLConnection != null) {
                }
                throw th;
            }
        } catch (IOException e5) {
            closeable = null;
            e2 = e5;
            httpURLConnection = null;
            String str222 = "ProxyCache";
            StringBuilder sb322 = new StringBuilder();
            sb322.append("Error fetching info from ");
            sb322.append(this.a);
            Log.e(str222, sb322.toString(), e2);
            m.a(closeable);
            if (httpURLConnection == null) {
            }
            httpURLConnection.disconnect();
        } catch (Throwable th3) {
            closeable = null;
            th = th3;
            httpURLConnection = null;
            m.a(closeable);
            if (httpURLConnection != null) {
            }
            throw th;
        }
        httpURLConnection.disconnect();
    }

    public synchronized int a() {
        if (this.d == Integer.MIN_VALUE) {
            d();
        }
        return this.d;
    }

    public int a(byte[] bArr) {
        if (this.c == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error reading data from ");
            sb.append(this.a);
            sb.append(": connection is absent!");
            throw new l(sb.toString());
        }
        try {
            return this.c.read(bArr, 0, bArr.length);
        } catch (InterruptedIOException e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Reading source ");
            sb2.append(this.a);
            sb2.append(" is interrupted");
            throw new i(sb2.toString(), e2);
        } catch (IOException e3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Error reading data from ");
            sb3.append(this.a);
            throw new l(sb3.toString(), e3);
        }
    }

    public void a(int i) {
        try {
            this.b = a(i, -1);
            this.e = this.b.getContentType();
            this.c = new BufferedInputStream(this.b.getInputStream(), 8192);
            HttpURLConnection httpURLConnection = this.b;
            int responseCode = this.b.getResponseCode();
            int contentLength = httpURLConnection.getContentLength();
            if (responseCode != 200) {
                contentLength = responseCode == 206 ? contentLength + i : this.d;
            }
            this.d = contentLength;
        } catch (IOException e2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error opening connection for ");
            sb.append(this.a);
            sb.append(" with offset ");
            sb.append(i);
            throw new l(sb.toString(), e2);
        }
    }

    public void b() {
        if (this.b != null) {
            try {
                this.b.disconnect();
            } catch (NullPointerException e2) {
                throw new l("Error disconnecting HttpUrlConnection", e2);
            }
        }
    }

    public synchronized String c() {
        if (TextUtils.isEmpty(this.e)) {
            d();
        }
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("HttpUrlSource{url='");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }
}
