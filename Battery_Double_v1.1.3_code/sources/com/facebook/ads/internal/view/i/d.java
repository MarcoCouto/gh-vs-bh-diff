package com.facebook.ads.internal.view.i;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.ads.internal.view.i.c.g;
import com.facebook.ads.internal.view.i.d.c;
import com.facebook.ads.internal.w.b.x;
import java.lang.ref.WeakReference;

public class d extends RelativeLayout {
    private final c a;
    @Nullable
    private g b;
    private WeakReference<a> c;

    public interface a {
        void a();
    }

    public d(Context context, c cVar) {
        super(context);
        this.a = cVar;
        x.b((View) this.a);
        addView(this.a.getView(), new LayoutParams(-1, -1));
    }

    public void a(com.facebook.ads.internal.view.i.a.c cVar) {
        addView(cVar, new LayoutParams(-1, -1));
        this.b = (g) cVar;
    }

    public void b(com.facebook.ads.internal.view.i.a.c cVar) {
        x.b(cVar);
        this.b = null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        ((View) this.a).layout(0, 0, getWidth(), getHeight());
        if (this.b != null) {
            this.b.layout(0, 0, getWidth(), getHeight());
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
        if (r3 > r10) goto L_0x006f;
     */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        boolean z;
        int i5;
        int i6;
        int videoWidth = this.a.getVideoWidth();
        int videoHeight = this.a.getVideoHeight();
        int defaultSize = getDefaultSize(videoWidth, i);
        int defaultSize2 = getDefaultSize(videoHeight, i2);
        if (videoWidth <= 0 || videoHeight <= 0) {
            i3 = defaultSize2;
            int i7 = defaultSize;
            z = false;
            i4 = i7;
        } else {
            z = true;
            int mode = MeasureSpec.getMode(i);
            i4 = MeasureSpec.getSize(i);
            int mode2 = MeasureSpec.getMode(i2);
            i3 = MeasureSpec.getSize(i2);
            if (mode == 1073741824 && mode2 == 1073741824) {
                int i8 = videoWidth * i3;
                int i9 = i4 * videoHeight;
                if (i8 < i9) {
                    i4 = i8 / videoHeight;
                } else if (i8 > i9) {
                    i5 = i9 / videoWidth;
                }
            } else if (mode == 1073741824) {
                i5 = (videoHeight * i4) / videoWidth;
                if (mode2 == Integer.MIN_VALUE) {
                }
            } else if (mode2 == 1073741824) {
                int i10 = (videoWidth * i3) / videoHeight;
                if (mode != Integer.MIN_VALUE || i10 <= i4) {
                    i4 = i10;
                }
            } else {
                if (mode2 != Integer.MIN_VALUE || videoHeight <= i3) {
                    i6 = videoWidth;
                    i3 = videoHeight;
                } else {
                    i6 = (i3 * videoWidth) / videoHeight;
                }
                if (mode != Integer.MIN_VALUE || i6 <= i4) {
                    i4 = i6;
                } else {
                    i5 = (videoHeight * i4) / videoWidth;
                }
            }
            i3 = i5;
        }
        setMeasuredDimension(i4, i3);
        if (z && this.c != null && this.c.get() != null) {
            ((a) this.c.get()).a();
        }
    }

    public void setViewImplInflationListener(a aVar) {
        this.c = new WeakReference<>(aVar);
    }
}
