package com.facebook.ads.internal.h;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.v.a.p;
import com.facebook.ads.internal.w.c.c;
import com.facebook.ads.internal.w.h.a;
import com.facebook.ads.internal.w.h.b;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class d {
    private static final String a = "d";
    private static d b;
    private final Context c;

    private d(Context context) {
        this.c = context;
    }

    private Bitmap a(String str) {
        byte[] d = com.facebook.ads.internal.w.e.d.a(this.c).a(str, (p) null).d();
        return BitmapFactory.decodeByteArray(d, 0, d.length);
    }

    public static d a(Context context) {
        if (b == null) {
            Context applicationContext = context.getApplicationContext();
            synchronized (d.class) {
                if (b == null) {
                    b = new d(applicationContext);
                }
            }
        }
        return b;
    }

    private static void a(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:54:0x00b5=Splitter:B:54:0x00b5, B:39:0x0078=Splitter:B:39:0x0078} */
    private void a(String str, Bitmap bitmap) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        ByteArrayOutputStream byteArrayOutputStream = null;
        if (bitmap == null) {
            a((Throwable) null);
            return;
        }
        File cacheDir = this.c.getCacheDir();
        StringBuilder sb = new StringBuilder();
        sb.append(str.hashCode());
        sb.append(".png");
        File file = new File(cacheDir, sb.toString());
        try {
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
            try {
                bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream2);
                if (byteArrayOutputStream2.size() >= 3145728) {
                    Log.d(a, "Bitmap size exceeds max size for storage");
                    a((Closeable) byteArrayOutputStream2);
                    a((Closeable) null);
                    return;
                }
                fileOutputStream = new FileOutputStream(file);
                try {
                    byteArrayOutputStream2.writeTo(fileOutputStream);
                    fileOutputStream.flush();
                    a((Closeable) byteArrayOutputStream2);
                } catch (FileNotFoundException e) {
                    e = e;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    String str2 = a;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Bad output destination (file=");
                    sb2.append(file.getAbsolutePath());
                    sb2.append(").");
                    Log.e(str2, sb2.toString(), e);
                    a((Throwable) e);
                    a((Closeable) byteArrayOutputStream);
                    a((Closeable) fileOutputStream);
                } catch (IOException e2) {
                    fileOutputStream2 = fileOutputStream;
                    e = e2;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    try {
                        a((Throwable) e);
                        String str3 = a;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Unable to write bitmap to file (url=");
                        sb3.append(str);
                        sb3.append(").");
                        Log.e(str3, sb3.toString(), e);
                        a((Closeable) byteArrayOutputStream);
                        a((Closeable) fileOutputStream2);
                    } catch (Throwable th) {
                        th = th;
                        fileOutputStream = fileOutputStream2;
                        a((Closeable) byteArrayOutputStream);
                        a((Closeable) fileOutputStream);
                        throw th;
                    }
                } catch (OutOfMemoryError e3) {
                    e = e3;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    try {
                        a((Throwable) e);
                        Log.e(a, "Unable to write bitmap to output stream", e);
                        a((Closeable) byteArrayOutputStream);
                        a((Closeable) fileOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    a((Closeable) byteArrayOutputStream);
                    a((Closeable) fileOutputStream);
                    throw th;
                }
                a((Closeable) fileOutputStream);
            } catch (FileNotFoundException e4) {
                e = e4;
                fileOutputStream = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                String str22 = a;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("Bad output destination (file=");
                sb22.append(file.getAbsolutePath());
                sb22.append(").");
                Log.e(str22, sb22.toString(), e);
                a((Throwable) e);
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream);
            } catch (IOException e5) {
                e = e5;
                fileOutputStream2 = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                a((Throwable) e);
                String str32 = a;
                StringBuilder sb32 = new StringBuilder();
                sb32.append("Unable to write bitmap to file (url=");
                sb32.append(str);
                sb32.append(").");
                Log.e(str32, sb32.toString(), e);
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream2);
            } catch (OutOfMemoryError e6) {
                e = e6;
                fileOutputStream = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                a((Throwable) e);
                Log.e(a, "Unable to write bitmap to output stream", e);
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream);
            } catch (Throwable th4) {
                th = th4;
                fileOutputStream = null;
                byteArrayOutputStream = byteArrayOutputStream2;
                a((Closeable) byteArrayOutputStream);
                a((Closeable) fileOutputStream);
                throw th;
            }
        } catch (FileNotFoundException e7) {
            e = e7;
            fileOutputStream = null;
            String str222 = a;
            StringBuilder sb222 = new StringBuilder();
            sb222.append("Bad output destination (file=");
            sb222.append(file.getAbsolutePath());
            sb222.append(").");
            Log.e(str222, sb222.toString(), e);
            a((Throwable) e);
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream);
        } catch (IOException e8) {
            e = e8;
            fileOutputStream2 = null;
            a((Throwable) e);
            String str322 = a;
            StringBuilder sb322 = new StringBuilder();
            sb322.append("Unable to write bitmap to file (url=");
            sb322.append(str);
            sb322.append(").");
            Log.e(str322, sb322.toString(), e);
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream2);
        } catch (OutOfMemoryError e9) {
            e = e9;
            fileOutputStream = null;
            a((Throwable) e);
            Log.e(a, "Unable to write bitmap to output stream", e);
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream);
        } catch (Throwable th5) {
            th = th5;
            fileOutputStream = null;
            a((Closeable) byteArrayOutputStream);
            a((Closeable) fileOutputStream);
            throw th;
        }
    }

    private void a(Throwable th) {
        a.b(this.c, MessengerShareContentUtility.MEDIA_IMAGE, b.S, new com.facebook.ads.internal.protocol.b(AdErrorType.IMAGE_CACHE_ERROR, AdErrorType.IMAGE_CACHE_ERROR.getDefaultErrorMessage(), th));
    }

    private boolean a(int i, int i2) {
        return i > 0 && i2 > 0 && com.facebook.ads.internal.r.a.l(this.c);
    }

    @Nullable
    private Bitmap b(String str, int i, int i2) {
        try {
            Bitmap a2 = a(i, i2) ? c.a(str.substring("file://".length()), i, i2) : BitmapFactory.decodeStream(new FileInputStream(str.substring("file://".length())), null, null);
            a(str, a2);
            return a2;
        } catch (IOException e) {
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to copy local image into cache (url=");
            sb.append(str);
            sb.append(").");
            Log.e(str2, sb.toString(), e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0052  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x0045=Splitter:B:24:0x0045, B:17:0x003a=Splitter:B:17:0x003a} */
    @Nullable
    private Bitmap c(String str, int i, int i2) {
        Bitmap bitmap;
        InputStream inputStream;
        if (str.startsWith("asset:///")) {
            InputStream inputStream2 = null;
            try {
                inputStream = this.c.getAssets().open(str.substring(9, str.length()));
                try {
                    bitmap = a(i, i2) ? c.a(inputStream, i, i2) : BitmapFactory.decodeStream(inputStream);
                    if (inputStream != null) {
                        a((Closeable) inputStream);
                    }
                } catch (IOException e) {
                    e = e;
                    a((Throwable) e);
                    if (inputStream != null) {
                        a((Closeable) inputStream);
                    }
                    return null;
                } catch (OutOfMemoryError e2) {
                    e = e2;
                    try {
                        a((Throwable) e);
                        if (inputStream != null) {
                            a((Closeable) inputStream);
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        inputStream2 = inputStream;
                        if (inputStream2 != null) {
                        }
                        throw th;
                    }
                }
            } catch (IOException e3) {
                e = e3;
                inputStream = null;
                a((Throwable) e);
                if (inputStream != null) {
                }
                return null;
            } catch (OutOfMemoryError e4) {
                e = e4;
                inputStream = null;
                a((Throwable) e);
                if (inputStream != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (inputStream2 != null) {
                    a((Closeable) inputStream2);
                }
                throw th;
            }
        } else {
            if (a(i, i2)) {
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.connect();
                    InputStream inputStream3 = httpURLConnection.getInputStream();
                    bitmap = c.a(inputStream3, i, i2);
                    a((Closeable) inputStream3);
                } catch (IOException e5) {
                    a((Throwable) e5);
                }
            }
            bitmap = a(str);
        }
        a(str, bitmap);
        return bitmap;
    }

    @Nullable
    public Bitmap a(String str, int i, int i2) {
        File cacheDir = this.c.getCacheDir();
        StringBuilder sb = new StringBuilder();
        sb.append(str.hashCode());
        sb.append(".png");
        File file = new File(cacheDir, sb.toString());
        return !file.exists() ? str.startsWith("file://") ? b(str, i, i2) : c(str, i, i2) : a(i, i2) ? c.a(file.getAbsolutePath(), i, i2) : BitmapFactory.decodeFile(file.getAbsolutePath());
    }
}
