package com.facebook.ads.internal.a;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.internal.w.a.b;
import com.facebook.share.internal.ShareConstants;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    private static final String a = "c";

    @Nullable
    public static b a(Context context, com.facebook.ads.internal.s.c cVar, String str, Uri uri, Map<String, String> map) {
        return a(context, cVar, str, uri, map, false, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b9  */
    @Nullable
    public static b a(Context context, com.facebook.ads.internal.s.c cVar, String str, Uri uri, Map<String, String> map, boolean z, boolean z2) {
        Map<String, String> map2;
        char c;
        int hashCode;
        com.facebook.ads.internal.s.c cVar2 = cVar;
        Uri uri2 = uri;
        if (uri2 == null || uri2.getAuthority() == null) {
            return null;
        }
        String authority = uri2.getAuthority();
        String queryParameter = uri2.getQueryParameter("video_url");
        if (!TextUtils.isEmpty(uri2.getQueryParameter(ShareConstants.WEB_DIALOG_PARAM_DATA))) {
            try {
                JSONObject jSONObject = new JSONObject(uri2.getQueryParameter(ShareConstants.WEB_DIALOG_PARAM_DATA));
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str2 = (String) keys.next();
                    map2 = map;
                    try {
                        map2.put(str2, jSONObject.getString(str2));
                    } catch (JSONException e) {
                        e = e;
                    }
                }
            } catch (JSONException e2) {
                e = e2;
                map2 = map;
                Log.w(a, "Unable to parse json data in AdActionFactory.", e);
                m a2 = m.a(cVar2, b.a());
                c = 65535;
                hashCode = authority.hashCode();
                if (hashCode == -1458789996) {
                }
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        }
        map2 = map;
        m a22 = m.a(cVar2, b.a());
        c = 65535;
        hashCode = authority.hashCode();
        if (hashCode == -1458789996) {
            if (hashCode != 109770977) {
                if (hashCode == 1546100943 && authority.equals("open_link")) {
                    c = 1;
                }
            } else if (authority.equals("store")) {
                c = 0;
            }
        } else if (authority.equals("passthrough")) {
            c = 2;
        }
        switch (c) {
            case 0:
                if (queryParameter != null) {
                    return null;
                }
                f fVar = new f(context, cVar2, str, uri2, map2, a22, z);
                return fVar;
            case 1:
                if (z2) {
                    i iVar = new i(context, cVar2, str, uri2, map2);
                    return iVar;
                }
                j jVar = new j(context, cVar2, str, uri2, map2, a22);
                return jVar;
            case 2:
                k kVar = new k(context, cVar2, str, uri2, map2);
                return kVar;
            default:
                return new l(context, cVar2, str, uri2);
        }
    }

    public static boolean a(String str) {
        return "store".equalsIgnoreCase(str) || "open_link".equalsIgnoreCase(str);
    }
}
