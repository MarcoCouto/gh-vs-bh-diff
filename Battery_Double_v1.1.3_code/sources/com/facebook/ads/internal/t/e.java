package com.facebook.ads.internal.t;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.internal.adapters.AdAdapter;
import com.facebook.ads.internal.adapters.i;
import com.facebook.ads.internal.adapters.p;
import com.facebook.ads.internal.adapters.q;
import com.facebook.ads.internal.b.g;
import com.facebook.ads.internal.m.d;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.AdPlacementType;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.view.aa;
import com.facebook.ads.internal.view.ab;
import com.facebook.ads.internal.w.b.k;
import com.facebook.ads.internal.w.b.w;
import com.facebook.ads.internal.x.a.C0023a;
import com.facebook.internal.ServerProtocol;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

public class e {
    private static final String b = "e";
    private static WeakHashMap<View, WeakReference<e>> c = new WeakHashMap<>();
    private static com.facebook.ads.internal.h.b h;
    /* access modifiers changed from: private */
    public k A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    /* access modifiers changed from: private */
    @Nullable
    public com.facebook.ads.internal.view.c.c F;
    /* access modifiers changed from: private */
    public d G;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.adapters.p.a H;
    /* access modifiers changed from: private */
    public String I;
    private View J;
    @Nullable
    protected i a;
    /* access modifiers changed from: private */
    public final Context d;
    private final String e;
    private final String f;
    private final com.facebook.ads.internal.h.b g;
    /* access modifiers changed from: private */
    @Nullable
    public h i;
    private final c j;
    /* access modifiers changed from: private */
    public g k;
    private volatile boolean l;
    @Nullable
    private d m;
    private com.facebook.ads.internal.protocol.e n;
    /* access modifiers changed from: private */
    @Nullable
    public View o;
    /* access modifiers changed from: private */
    @Nullable
    public NativeAdLayout p;
    /* access modifiers changed from: private */
    @Nullable
    public f q;
    private final List<View> r;
    /* access modifiers changed from: private */
    public OnTouchListener s;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.x.a t;
    private C0023a u;
    /* access modifiers changed from: private */
    public WeakReference<C0023a> v;
    /* access modifiers changed from: private */
    public final w w;
    /* access modifiers changed from: private */
    @Nullable
    public p x;
    private a y;
    private ab z;

    private class a implements OnClickListener, OnLongClickListener, OnTouchListener {
        private a() {
        }

        /* access modifiers changed from: private */
        public Map a() {
            HashMap hashMap = new HashMap();
            hashMap.put("touch", k.a(e.this.w.e()));
            if (e.this.A != null) {
                hashMap.put("nti", String.valueOf(e.this.A.c()));
            }
            if (e.this.B) {
                hashMap.put("nhs", String.valueOf(e.this.B));
            }
            e.this.t.a((Map<String, String>) hashMap);
            return hashMap;
        }

        /* access modifiers changed from: private */
        public void a(Map<String, String> map) {
            if (e.this.a != null) {
                e.this.a.e(map);
            }
        }

        public void onClick(View view) {
            String str;
            String str2;
            if (!e.this.w.d()) {
                Log.e("FBAudienceNetworkLog", "No touch data recorded, please ensure touch events reach the ad View by returning false if you intercept the event.");
            }
            int F = com.facebook.ads.internal.r.a.F(e.this.d);
            if (F >= 0 && e.this.w.c() < ((long) F)) {
                if (!e.this.w.b()) {
                    str = "FBAudienceNetworkLog";
                    str2 = "Ad cannot be clicked before it is viewed.";
                } else {
                    str = "FBAudienceNetworkLog";
                    str2 = "Clicks happened too fast.";
                }
                Log.e(str, str2);
            } else if (e.this.w.a(e.this.d)) {
                if (e.this.a != null) {
                    e.this.a.d(a());
                }
            } else if (com.facebook.ads.internal.r.a.e(e.this.d)) {
                if (e.this.a != null) {
                    e.this.a.c(a());
                }
                com.facebook.ads.internal.w.b.g.a(new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Map a2 = a.this.a();
                        a2.put("is_two_step", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                        a.this.a(a2);
                    }
                }, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (e.this.a != null) {
                            e.this.a.b(a.this.a());
                        }
                    }
                }, com.facebook.ads.internal.w.a.b.a());
            } else {
                a(a());
            }
        }

        public boolean onLongClick(View view) {
            if (e.this.o == null || e.this.F == null) {
                return false;
            }
            e.this.F.setBounds(0, 0, e.this.o.getWidth(), e.this.o.getHeight());
            e.this.F.a(!e.this.F.a());
            return true;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            e.this.w.a(motionEvent, e.this.o, view);
            return e.this.s != null && e.this.s.onTouch(view, motionEvent);
        }
    }

    private class b extends com.facebook.ads.internal.adapters.c {
        private b() {
        }

        public void a() {
            if (e.this.i != null) {
                e.this.i.d();
            }
        }

        public void b() {
        }
    }

    public interface c {
        boolean a(View view);
    }

    public e(Context context, i iVar, d dVar, c cVar) {
        this(context, null, cVar);
        this.a = iVar;
        this.m = dVar;
        this.l = true;
        this.J = new View(context);
    }

    public e(Context context, String str, c cVar) {
        this.f = UUID.randomUUID().toString();
        this.n = com.facebook.ads.internal.protocol.e.NATIVE_UNKNOWN;
        this.r = new ArrayList();
        this.w = new w();
        this.C = false;
        this.D = false;
        this.G = d.ALL;
        this.H = com.facebook.ads.internal.adapters.p.a.ALL;
        this.d = context;
        this.e = str;
        this.j = cVar;
        this.g = h != null ? h : new com.facebook.ads.internal.h.b(context);
        this.J = new View(context);
    }

    public e(e eVar) {
        this(eVar.d, null, eVar.j);
        this.m = eVar.m;
        this.a = eVar.a;
        this.l = true;
        this.J = new View(this.d);
    }

    /* access modifiers changed from: private */
    public boolean A() {
        return this.a != null && this.a.B();
    }

    private void B() {
        if (!TextUtils.isEmpty(p())) {
            com.facebook.ads.internal.w.e.g.a(new com.facebook.ads.internal.w.e.g(), this.d, Uri.parse(p()), v());
        }
    }

    private void C() {
        for (View view : this.r) {
            view.setOnClickListener(null);
            view.setOnTouchListener(null);
            view.setOnLongClickListener(null);
        }
        this.r.clear();
    }

    /* access modifiers changed from: private */
    public void a(@Nullable final i iVar, final boolean z2) {
        if (iVar != null) {
            if (this.G.equals(d.ALL)) {
                if (iVar.l() != null) {
                    this.g.a(iVar.l().a(), iVar.l().c(), iVar.l().b());
                }
                if (!this.n.equals(com.facebook.ads.internal.protocol.e.NATIVE_BANNER)) {
                    if (iVar.m() != null) {
                        this.g.a(iVar.m().a(), iVar.m().c(), iVar.m().b());
                    }
                    if (iVar.x() != null) {
                        for (e eVar : iVar.x()) {
                            if (eVar.j() != null) {
                                this.g.a(eVar.j().a(), eVar.j().c(), eVar.j().b());
                            }
                        }
                    }
                    if (!TextUtils.isEmpty(iVar.t())) {
                        this.g.a(iVar.t());
                    }
                }
            }
            this.g.a((com.facebook.ads.internal.h.a) new com.facebook.ads.internal.h.a() {
                public void a() {
                    e.this.a = iVar;
                    if (e.this.i != null) {
                        if (e.this.G.equals(d.ALL) && !e.this.A()) {
                            e.this.i.a();
                        }
                        if (z2) {
                            e.this.i.b();
                        }
                    }
                }

                public void b() {
                    if (e.this.a != null) {
                        e.this.a.c();
                        e.this.a = null;
                    }
                    if (e.this.i != null) {
                        e.this.i.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.CACHE_FAILURE_ERROR, "Failed to download a media."));
                    }
                }
            });
        }
    }

    public static void a(g gVar, ImageView imageView) {
        if (gVar != null && imageView != null) {
            new com.facebook.ads.internal.view.c.d(imageView).a(gVar.c(), gVar.b()).a(gVar.a());
        }
    }

    private void a(List<View> list, View view) {
        if (this.j == null || !this.j.a(view)) {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    a(list, viewGroup.getChildAt(i2));
                }
            } else {
                list.add(view);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:109:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01c2  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01c9  */
    private void b(View view, f fVar, List<View> list) {
        d b2;
        int i2;
        int i3;
        com.facebook.ads.internal.view.c.c cVar;
        d b3;
        d b4;
        d b5;
        d b6;
        String str;
        String str2;
        if (view == null) {
            throw new IllegalArgumentException("Must provide a View");
        } else if (list == null || list.size() == 0) {
            throw new IllegalArgumentException("Invalid set of clickable views");
        } else if (!f()) {
            Log.e(b, "Ad not loaded");
        } else {
            if (this.p != null) {
                this.p.clearAdReportingLayout();
            }
            if (fVar == null) {
                if (this.n == com.facebook.ads.internal.protocol.e.NATIVE_UNKNOWN) {
                    if (this.i != null) {
                        this.i.a(new com.facebook.ads.internal.protocol.a(AdErrorType.NO_MEDIAVIEW_IN_NATIVEAD, "MediaView is missing."));
                    }
                    if (AdInternalSettings.isDebugBuild()) {
                        str = b;
                        str2 = "MediaView is missing.";
                    }
                    return;
                }
                if (this.i != null) {
                    this.i.a(new com.facebook.ads.internal.protocol.a(AdErrorType.NO_ICONVIEW_IN_NATIVEBANNERAD, "AdIconView is missing."));
                }
                if (AdInternalSettings.isDebugBuild()) {
                    str = b;
                    str2 = "AdIconView is missing.";
                }
                return;
                Log.e(str, str2);
            } else if (fVar.getAdContentsView() == null) {
                if (this.i != null) {
                    this.i.a(new com.facebook.ads.internal.protocol.a(AdErrorType.UNSUPPORTED_AD_ASSET_NATIVEAD, "ad media type is not supported."));
                }
            } else {
                if (this.o != null) {
                    Log.w(b, "Native Ad was already registered with a View. Auto unregistering and proceeding.");
                    z();
                }
                if (c.containsKey(view) && ((WeakReference) c.get(view)).get() != null) {
                    Log.w(b, "View already registered with a NativeAd. Auto unregistering and proceeding.");
                    ((e) ((WeakReference) c.get(view)).get()).z();
                }
                this.y = new a();
                this.o = view;
                this.q = fVar;
                if (view instanceof ViewGroup) {
                    this.z = new ab(view.getContext(), new aa() {
                        public void a(int i) {
                            if (e.this.a != null) {
                                e.this.a.a(i);
                            }
                        }
                    });
                    ((ViewGroup) view).addView(this.z);
                }
                ArrayList<View> arrayList = new ArrayList<>(list);
                if (this.J != null) {
                    arrayList.add(this.J);
                }
                for (View view2 : arrayList) {
                    this.r.add(view2);
                    view2.setOnClickListener(this.y);
                    view2.setOnTouchListener(this.y);
                    if (com.facebook.ads.internal.r.a.b(view2.getContext())) {
                        view2.setOnLongClickListener(this.y);
                    }
                }
                this.a.a(view, (List<View>) arrayList);
                int i4 = 1;
                if (this.m != null) {
                    b2 = this.m;
                } else {
                    if (!(this.k == null || this.k.b() == null)) {
                        b2 = this.k.b();
                    }
                    int i5 = i4;
                    this.u = new C0023a() {
                        public void a() {
                            if (!e.this.w.b()) {
                                e.this.w.a();
                                e.this.t.c();
                                if (!(e.this.v == null || e.this.v.get() == null)) {
                                    ((C0023a) e.this.v.get()).a();
                                }
                                if (e.this.x != null && e.this.o != null && e.this.q != null) {
                                    e.this.x.a(e.this.o);
                                    e.this.x.a(e.this.q);
                                    e.this.x.a(e.this.A);
                                    e.this.x.a(e.this.B);
                                    e.this.x.b(e.this.C);
                                    e.this.x.d(e.this.D);
                                    e.this.x.c(e.o(e.this));
                                    e.this.x.a(e.this.H);
                                    e.this.x.e(e.this.E);
                                    e.this.x.a(com.facebook.ads.internal.view.a.d.a(e.this.p));
                                    e.this.x.a(e.this.I);
                                    e.this.x.a();
                                }
                            }
                        }
                    };
                    View adContentsView = fVar == null ? fVar.getAdContentsView() : this.o;
                    int i6 = 0;
                    if (this.m == null) {
                        b6 = this.m;
                    } else if (this.k == null || this.k.b() == null) {
                        i2 = 0;
                        com.facebook.ads.internal.x.a aVar = new com.facebook.ads.internal.x.a(adContentsView, i5, i2, true, this.u);
                        this.t = aVar;
                        com.facebook.ads.internal.x.a aVar2 = this.t;
                        if (this.m != null) {
                            b5 = this.m;
                        } else {
                            if (this.a != null) {
                                i6 = this.a.j();
                            } else if (!(this.k == null || this.k.b() == null)) {
                                b5 = this.k.b();
                            }
                            aVar2.a(i6);
                            com.facebook.ads.internal.x.a aVar3 = this.t;
                            if (this.m == null) {
                                b4 = this.m;
                            } else {
                                if (this.a != null) {
                                    i3 = this.a.k();
                                } else if (this.k == null || this.k.b() == null) {
                                    i3 = 1000;
                                } else {
                                    b4 = this.k.b();
                                }
                                aVar3.b(i3);
                                this.x = new p(this.d, new b(), this.t, this.a);
                                this.x.a((List<View>) arrayList);
                                c.put(view, new WeakReference(this));
                                if (com.facebook.ads.internal.r.a.b(this.d)) {
                                    this.F = new com.facebook.ads.internal.view.c.c();
                                    this.F.a(this.e);
                                    this.F.b(this.d.getPackageName());
                                    this.F.a(this.t);
                                    if (this.a.z() > 0) {
                                        this.F.a(this.a.z(), this.a.y());
                                    }
                                    if (this.m != null) {
                                        cVar = this.F;
                                        b3 = this.m;
                                    } else {
                                        if (!(this.k == null || this.k.b() == null)) {
                                            cVar = this.F;
                                            b3 = this.k.b();
                                        }
                                        this.o.getOverlay().add(this.F);
                                    }
                                    cVar.a(b3.a());
                                    this.o.getOverlay().add(this.F);
                                }
                            }
                            i3 = b4.i();
                            aVar3.b(i3);
                            this.x = new p(this.d, new b(), this.t, this.a);
                            this.x.a((List<View>) arrayList);
                            c.put(view, new WeakReference(this));
                            if (com.facebook.ads.internal.r.a.b(this.d)) {
                            }
                        }
                        i6 = b5.h();
                        aVar2.a(i6);
                        com.facebook.ads.internal.x.a aVar32 = this.t;
                        if (this.m == null) {
                        }
                        i3 = b4.i();
                        aVar32.b(i3);
                        this.x = new p(this.d, new b(), this.t, this.a);
                        this.x.a((List<View>) arrayList);
                        c.put(view, new WeakReference(this));
                        if (com.facebook.ads.internal.r.a.b(this.d)) {
                        }
                    } else {
                        b6 = this.k.b();
                    }
                    i2 = b6.g();
                    com.facebook.ads.internal.x.a aVar4 = new com.facebook.ads.internal.x.a(adContentsView, i5, i2, true, this.u);
                    this.t = aVar4;
                    com.facebook.ads.internal.x.a aVar22 = this.t;
                    if (this.m != null) {
                    }
                    i6 = b5.h();
                    aVar22.a(i6);
                    com.facebook.ads.internal.x.a aVar322 = this.t;
                    if (this.m == null) {
                    }
                    i3 = b4.i();
                    aVar322.b(i3);
                    this.x = new p(this.d, new b(), this.t, this.a);
                    this.x.a((List<View>) arrayList);
                    c.put(view, new WeakReference(this));
                    if (com.facebook.ads.internal.r.a.b(this.d)) {
                    }
                }
                i4 = b2.f();
                int i52 = i4;
                this.u = new C0023a() {
                    public void a() {
                        if (!e.this.w.b()) {
                            e.this.w.a();
                            e.this.t.c();
                            if (!(e.this.v == null || e.this.v.get() == null)) {
                                ((C0023a) e.this.v.get()).a();
                            }
                            if (e.this.x != null && e.this.o != null && e.this.q != null) {
                                e.this.x.a(e.this.o);
                                e.this.x.a(e.this.q);
                                e.this.x.a(e.this.A);
                                e.this.x.a(e.this.B);
                                e.this.x.b(e.this.C);
                                e.this.x.d(e.this.D);
                                e.this.x.c(e.o(e.this));
                                e.this.x.a(e.this.H);
                                e.this.x.e(e.this.E);
                                e.this.x.a(com.facebook.ads.internal.view.a.d.a(e.this.p));
                                e.this.x.a(e.this.I);
                                e.this.x.a();
                            }
                        }
                    }
                };
                View adContentsView2 = fVar == null ? fVar.getAdContentsView() : this.o;
                int i62 = 0;
                if (this.m == null) {
                }
                i2 = b6.g();
                com.facebook.ads.internal.x.a aVar42 = new com.facebook.ads.internal.x.a(adContentsView2, i52, i2, true, this.u);
                this.t = aVar42;
                com.facebook.ads.internal.x.a aVar222 = this.t;
                if (this.m != null) {
                }
                i62 = b5.h();
                aVar222.a(i62);
                com.facebook.ads.internal.x.a aVar3222 = this.t;
                if (this.m == null) {
                }
                i3 = b4.i();
                aVar3222.b(i3);
                this.x = new p(this.d, new b(), this.t, this.a);
                this.x.a((List<View>) arrayList);
                c.put(view, new WeakReference(this));
                if (com.facebook.ads.internal.r.a.b(this.d)) {
                }
            }
        }
    }

    static /* synthetic */ boolean o(e eVar) {
        return eVar.t() == l.ON;
    }

    @Nullable
    public i a() {
        return this.a;
    }

    @Nullable
    public String a(String str) {
        if (!f()) {
            return null;
        }
        return this.a.a(str);
    }

    public void a(OnTouchListener onTouchListener) {
        this.s = onTouchListener;
    }

    public void a(View view, f fVar) {
        ArrayList arrayList = new ArrayList();
        a((List<View>) arrayList, view);
        b(view, fVar, arrayList);
    }

    public void a(View view, f fVar, List<View> list) {
        b(view, fVar, list);
    }

    public void a(@Nullable NativeAdLayout nativeAdLayout) {
        this.p = nativeAdLayout;
    }

    public void a(q qVar) {
        if (this.a != null) {
            this.a.a(qVar);
        }
    }

    public void a(com.facebook.ads.internal.protocol.e eVar) {
        this.n = eVar;
    }

    public void a(d dVar, String str) {
        if (this.l) {
            throw new IllegalStateException("loadAd cannot be called more than once");
        }
        this.l = true;
        this.G = dVar;
        if (dVar.equals(d.NONE)) {
            this.H = com.facebook.ads.internal.adapters.p.a.NONE;
        }
        com.facebook.ads.internal.b.a aVar = new com.facebook.ads.internal.b.a(this.e, this.n, this.n == com.facebook.ads.internal.protocol.e.NATIVE_UNKNOWN ? AdPlacementType.NATIVE : AdPlacementType.NATIVE_BANNER, null, 1);
        aVar.a(dVar);
        aVar.a(this.I);
        this.k = new g(this.d, aVar);
        this.k.a((com.facebook.ads.internal.adapters.a) new com.facebook.ads.internal.adapters.a() {
            public void a() {
                if (e.this.i != null) {
                    e.this.i.c();
                }
            }

            public void a(AdAdapter adAdapter) {
                if (e.this.k != null) {
                    e.this.k.e();
                }
            }

            public void a(i iVar) {
                e.this.a(iVar, true);
                if (e.this.i != null && iVar.x() != null) {
                    AnonymousClass1 r0 = new q() {
                        public void a(i iVar) {
                        }

                        public void a(i iVar, com.facebook.ads.internal.protocol.a aVar) {
                        }

                        public void b(i iVar) {
                        }

                        public void c(i iVar) {
                            if (e.this.i != null) {
                                e.this.i.c();
                            }
                        }
                    };
                    for (e a2 : iVar.x()) {
                        a2.a((q) r0);
                    }
                }
            }

            public void a(com.facebook.ads.internal.protocol.a aVar) {
                if (e.this.i != null) {
                    e.this.i.a(aVar);
                }
            }

            public void b() {
                throw new IllegalStateException("Native ads manager their own impressions.");
            }
        });
        this.k.b(str);
    }

    public void a(h hVar) {
        this.i = hVar;
    }

    public void a(k kVar) {
        this.A = kVar;
    }

    public void a(C0023a aVar) {
        this.v = new WeakReference<>(aVar);
    }

    public void a(boolean z2) {
        this.B = z2;
    }

    public void a(boolean z2, boolean z3) {
        if (z2) {
            if (this.G.equals(d.NONE) && !A() && this.i != null) {
                this.i.a();
            }
            if (this.t != null) {
                this.t.a();
            }
        } else {
            if (this.t != null) {
                this.t.c();
            }
            if (this.i != null && z3) {
                this.i.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.BROKEN_MEDIA_ERROR, "Failed to load Media."));
            }
        }
    }

    public void b(String str) {
        this.I = str;
    }

    public void b(boolean z2) {
        this.E = z2;
    }

    public boolean b() {
        return this.k == null || this.k.g();
    }

    public void c() {
        if (this.G.equals(d.NONE)) {
            this.H = com.facebook.ads.internal.adapters.p.a.MANUAL;
        }
        this.G = d.ALL;
        a(this.a, false);
    }

    public void c(boolean z2) {
        this.C = z2;
    }

    public void d() {
        if (this.k != null) {
            this.k.a(true);
            this.k = null;
        }
    }

    public void d(boolean z2) {
        this.D = z2;
    }

    public String e() {
        return this.e;
    }

    public boolean f() {
        return this.a != null && this.a.A();
    }

    public boolean g() {
        return f() && this.a.e();
    }

    public boolean h() {
        return this.a != null && this.a.f();
    }

    @Nullable
    public g i() {
        if (!f()) {
            return null;
        }
        return this.a.l();
    }

    @Nullable
    public g j() {
        if (!f()) {
            return null;
        }
        return this.a.m();
    }

    @Nullable
    public j k() {
        if (!f()) {
            return null;
        }
        return this.a.n();
    }

    @Nullable
    public String l() {
        if (!f()) {
            return null;
        }
        return this.a.o();
    }

    @Nullable
    public i m() {
        if (!f()) {
            return null;
        }
        return this.a.p();
    }

    @Nullable
    public String n() {
        if (!f()) {
            return null;
        }
        return this.f;
    }

    @Nullable
    public g o() {
        if (!f()) {
            return null;
        }
        return this.a.q();
    }

    @Nullable
    public String p() {
        if (!f()) {
            return null;
        }
        return this.a.r();
    }

    @Nullable
    public String q() {
        if (!f()) {
            return null;
        }
        return this.a.s();
    }

    @Nullable
    public String r() {
        if (!f() || TextUtils.isEmpty(this.a.t())) {
            return null;
        }
        return this.g.c(this.a.t());
    }

    @Nullable
    public String s() {
        if (!f()) {
            return null;
        }
        return this.a.u();
    }

    @Nullable
    public l t() {
        return !f() ? l.DEFAULT : this.a.v();
    }

    @Nullable
    public List<e> u() {
        if (!f()) {
            return null;
        }
        return this.a.x();
    }

    @Nullable
    public String v() {
        if (!f()) {
            return null;
        }
        return this.a.getClientToken();
    }

    public void w() {
        this.J.performClick();
    }

    public k x() {
        return this.A;
    }

    public void y() {
        if (!com.facebook.ads.internal.f.a.a(this.d, false)) {
            B();
            return;
        }
        com.facebook.ads.internal.view.a.c a2 = com.facebook.ads.internal.view.a.d.a(this.d, com.facebook.ads.internal.s.d.a(this.d), v(), this.p);
        if (a2 == null) {
            B();
            return;
        }
        this.p.setAdReportingLayout(a2);
        a2.a();
    }

    public void z() {
        if (this.o != null && this.q != null) {
            if (!c.containsKey(this.o) || ((WeakReference) c.get(this.o)).get() != this) {
                throw new IllegalStateException("View not registered with this NativeAd");
            }
            if ((this.o instanceof ViewGroup) && this.z != null) {
                ((ViewGroup) this.o).removeView(this.z);
                this.z = null;
            }
            if (this.a != null) {
                this.a.c();
            }
            if (this.F != null && com.facebook.ads.internal.r.a.b(this.d)) {
                this.F.b();
                this.o.getOverlay().remove(this.F);
            }
            c.remove(this.o);
            C();
            this.o = null;
            this.q = null;
            if (this.t != null) {
                this.t.c();
                this.t = null;
            }
            this.x = null;
        }
    }
}
