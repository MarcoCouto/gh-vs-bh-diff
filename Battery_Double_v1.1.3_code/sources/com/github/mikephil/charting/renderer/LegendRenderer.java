package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Typeface;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendDirection;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment;
import com.github.mikephil.charting.components.Legend.LegendOrientation;
import com.github.mikephil.charting.components.Legend.LegendVerticalAlignment;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LegendRenderer extends Renderer {
    protected List<LegendEntry> computedEntries = new ArrayList(16);
    protected FontMetrics legendFontMetrics = new FontMetrics();
    protected Legend mLegend;
    protected Paint mLegendFormPaint;
    protected Paint mLegendLabelPaint;
    private Path mLineFormPath = new Path();

    public LegendRenderer(ViewPortHandler viewPortHandler, Legend legend) {
        super(viewPortHandler);
        this.mLegend = legend;
        this.mLegendLabelPaint = new Paint(1);
        this.mLegendLabelPaint.setTextSize(Utils.convertDpToPixel(9.0f));
        this.mLegendLabelPaint.setTextAlign(Align.LEFT);
        this.mLegendFormPaint = new Paint(1);
        this.mLegendFormPaint.setStyle(Style.FILL);
    }

    public Paint getLabelPaint() {
        return this.mLegendLabelPaint;
    }

    public Paint getFormPaint() {
        return this.mLegendFormPaint;
    }

    public void computeLegend(ChartData<?> chartData) {
        ChartData<?> chartData2;
        String str;
        ChartData<?> chartData3 = chartData;
        if (!this.mLegend.isLegendCustom()) {
            this.computedEntries.clear();
            int i = 0;
            while (i < chartData.getDataSetCount()) {
                IDataSet dataSetByIndex = chartData3.getDataSetByIndex(i);
                List colors = dataSetByIndex.getColors();
                int entryCount = dataSetByIndex.getEntryCount();
                if (dataSetByIndex instanceof IBarDataSet) {
                    IBarDataSet iBarDataSet = (IBarDataSet) dataSetByIndex;
                    if (iBarDataSet.isStacked()) {
                        String[] stackLabels = iBarDataSet.getStackLabels();
                        int i2 = 0;
                        while (i2 < colors.size() && i2 < iBarDataSet.getStackSize()) {
                            List<LegendEntry> list = this.computedEntries;
                            LegendEntry legendEntry = r10;
                            LegendEntry legendEntry2 = new LegendEntry(stackLabels[i2 % stackLabels.length], dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), ((Integer) colors.get(i2)).intValue());
                            list.add(legendEntry);
                            i2++;
                        }
                        if (iBarDataSet.getLabel() != null) {
                            List<LegendEntry> list2 = this.computedEntries;
                            LegendEntry legendEntry3 = new LegendEntry(dataSetByIndex.getLabel(), LegendForm.NONE, Float.NaN, Float.NaN, null, ColorTemplate.COLOR_NONE);
                            list2.add(legendEntry3);
                        }
                        chartData2 = chartData3;
                        i++;
                        chartData3 = chartData2;
                    }
                }
                if (dataSetByIndex instanceof IPieDataSet) {
                    IPieDataSet iPieDataSet = (IPieDataSet) dataSetByIndex;
                    int i3 = 0;
                    while (i3 < colors.size() && i3 < entryCount) {
                        List<LegendEntry> list3 = this.computedEntries;
                        LegendEntry legendEntry4 = r9;
                        LegendEntry legendEntry5 = new LegendEntry(((PieEntry) iPieDataSet.getEntryForIndex(i3)).getLabel(), dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), ((Integer) colors.get(i3)).intValue());
                        list3.add(legendEntry4);
                        i3++;
                        ChartData<?> chartData4 = chartData;
                    }
                    if (iPieDataSet.getLabel() != null) {
                        List<LegendEntry> list4 = this.computedEntries;
                        LegendEntry legendEntry6 = new LegendEntry(dataSetByIndex.getLabel(), LegendForm.NONE, Float.NaN, Float.NaN, null, ColorTemplate.COLOR_NONE);
                        list4.add(legendEntry6);
                    }
                } else {
                    if (dataSetByIndex instanceof ICandleDataSet) {
                        ICandleDataSet iCandleDataSet = (ICandleDataSet) dataSetByIndex;
                        if (iCandleDataSet.getDecreasingColor() != 1122867) {
                            int decreasingColor = iCandleDataSet.getDecreasingColor();
                            int increasingColor = iCandleDataSet.getIncreasingColor();
                            List<LegendEntry> list5 = this.computedEntries;
                            LegendEntry legendEntry7 = new LegendEntry(null, dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), decreasingColor);
                            list5.add(legendEntry7);
                            List<LegendEntry> list6 = this.computedEntries;
                            LegendEntry legendEntry8 = new LegendEntry(dataSetByIndex.getLabel(), dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), increasingColor);
                            list6.add(legendEntry8);
                        }
                    }
                    int i4 = 0;
                    while (i4 < colors.size() && i4 < entryCount) {
                        if (i4 >= colors.size() - 1 || i4 >= entryCount - 1) {
                            str = chartData.getDataSetByIndex(i).getLabel();
                        } else {
                            str = null;
                            ChartData<?> chartData5 = chartData;
                        }
                        List<LegendEntry> list7 = this.computedEntries;
                        LegendEntry legendEntry9 = new LegendEntry(str, dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), ((Integer) colors.get(i4)).intValue());
                        list7.add(legendEntry9);
                        i4++;
                    }
                }
                chartData2 = chartData;
                i++;
                chartData3 = chartData2;
            }
            if (this.mLegend.getExtraEntries() != null) {
                Collections.addAll(this.computedEntries, this.mLegend.getExtraEntries());
            }
            this.mLegend.setEntries(this.computedEntries);
        }
        Typeface typeface = this.mLegend.getTypeface();
        if (typeface != null) {
            this.mLegendLabelPaint.setTypeface(typeface);
        }
        this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
        this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
        this.mLegend.calculateDimensions(this.mLegendLabelPaint, this.mViewPortHandler);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x027a, code lost:
        r13 = r5;
        r10 = r20;
        r5 = r24;
        r14 = r6.mLegend.getCalculatedLineSizes();
        r4 = r6.mLegend.getCalculatedLabelSizes();
        r3 = r6.mLegend.getCalculatedLabelBreakPoints();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0299, code lost:
        switch(r1) {
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.TOP :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x02bc;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x02af;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.CENTER :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x029f;
            default: goto L_0x029c;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x029c, code lost:
        r2 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x029f, code lost:
        r2 = r2 + ((r6.mViewPortHandler.getChartHeight() - r6.mLegend.mNeededHeight) / 2.0f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02af, code lost:
        r2 = (r6.mViewPortHandler.getChartHeight() - r2) - r6.mLegend.mNeededHeight;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x02bc, code lost:
        r1 = r12.length;
        r0 = r2;
        r28 = r4;
        r17 = r9;
        r2 = 0;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02c4, code lost:
        if (r2 >= r1) goto L_0x03e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02c6, code lost:
        r29 = r10;
        r10 = r12[r2];
        r30 = r1;
        r31 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x02d2, code lost:
        if (r10.form == com.github.mikephil.charting.components.Legend.LegendForm.NONE) goto L_0x02d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x02d4, code lost:
        r18 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x02d7, code lost:
        r18 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x02df, code lost:
        if (java.lang.Float.isNaN(r10.formSize) == false) goto L_0x02e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02e1, code lost:
        r20 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x02e4, code lost:
        r20 = com.github.mikephil.charting.utils.Utils.convertDpToPixel(r10.formSize);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x02f0, code lost:
        if (r2 >= r3.size()) goto L_0x0306;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02fc, code lost:
        if (((java.lang.Boolean) r3.get(r2)).booleanValue() == false) goto L_0x0306;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x02fe, code lost:
        r22 = r0 + (r8 + r21);
        r17 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0306, code lost:
        r22 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x030a, code lost:
        if (r17 != r9) goto L_0x0336;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x030e, code lost:
        if (r15 != com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.CENTER) goto L_0x0336;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0314, code lost:
        if (r4 >= r14.size()) goto L_0x0336;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0318, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x0325;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x031a, code lost:
        r0 = ((com.github.mikephil.charting.utils.FSize) r14.get(r4)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0325, code lost:
        r0 = -((com.github.mikephil.charting.utils.FSize) r14.get(r4)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x032f, code lost:
        r17 = r17 + (r0 / 2.0f);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0338, code lost:
        r23 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x033c, code lost:
        if (r10.label != null) goto L_0x0341;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x033e, code lost:
        r24 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0341, code lost:
        r24 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0343, code lost:
        if (r18 == false) goto L_0x036e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0347, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x034b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0349, code lost:
        r17 = r17 - r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x034b, code lost:
        r26 = r30;
        r32 = r9;
        r9 = r2;
        r27 = r3;
        r33 = r11;
        r11 = r28;
        r34 = r12;
        r12 = r31;
        drawForm(r7, r17, r22 + r11, r10, r6.mLegend);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0369, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x037d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x036b, code lost:
        r17 = r17 + r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x036e, code lost:
        r27 = r3;
        r32 = r9;
        r33 = r11;
        r34 = r12;
        r11 = r28;
        r26 = r30;
        r12 = r31;
        r9 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x037d, code lost:
        if (r24 != false) goto L_0x03bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x037f, code lost:
        if (r18 == false) goto L_0x038a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0383, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x0387;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0385, code lost:
        r0 = -r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0387, code lost:
        r0 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0388, code lost:
        r17 = r17 + r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x038c, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x0398;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x038e, code lost:
        r17 = r17 - ((com.github.mikephil.charting.utils.FSize) r11.get(r9)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0398, code lost:
        r0 = r17;
        drawLabel(r7, r0, r22 + r8, r10.label);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x03a3, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x03ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x03a5, code lost:
        r0 = r0 + ((com.github.mikephil.charting.utils.FSize) r11.get(r9)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x03b0, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x03b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x03b2, code lost:
        r1 = r25;
        r2 = -r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03b6, code lost:
        r1 = r25;
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x03b9, code lost:
        r17 = r0 + r2;
        r0 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x03bf, code lost:
        r1 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x03c3, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x03c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03c5, code lost:
        r0 = r29;
        r4 = -r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x03c9, code lost:
        r0 = r29;
        r4 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03cc, code lost:
        r17 = r17 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03ce, code lost:
        r2 = r9 + 1;
        r10 = r0;
        r25 = r1;
        r28 = r11;
        r5 = r12;
        r0 = r22;
        r4 = r23;
        r1 = r26;
        r3 = r27;
        r9 = r32;
        r11 = r33;
        r12 = r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03e6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x014a, code lost:
        r9 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0169, code lost:
        r9 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0172, code lost:
        switch(r0) {
            case com.github.mikephil.charting.components.Legend.LegendOrientation.HORIZONTAL :com.github.mikephil.charting.components.Legend$LegendOrientation: goto L_0x027a;
            case com.github.mikephil.charting.components.Legend.LegendOrientation.VERTICAL :com.github.mikephil.charting.components.Legend$LegendOrientation: goto L_0x0177;
            default: goto L_0x0175;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x017f, code lost:
        switch(r1) {
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.TOP :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x01b4;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x019c;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.CENTER :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x0185;
            default: goto L_0x0182;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0182, code lost:
        r0 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0185, code lost:
        r0 = ((r6.mViewPortHandler.getChartHeight() / 2.0f) - (r6.mLegend.mNeededHeight / 2.0f)) + r6.mLegend.getYOffset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x019e, code lost:
        if (r15 != com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.CENTER) goto L_0x01a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a0, code lost:
        r0 = r6.mViewPortHandler.getChartHeight();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01a7, code lost:
        r0 = r6.mViewPortHandler.contentBottom();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01ad, code lost:
        r0 = r0 - (r6.mLegend.mNeededHeight + r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01b6, code lost:
        if (r15 != com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.CENTER) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01b8, code lost:
        r0 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01bb, code lost:
        r0 = r6.mViewPortHandler.contentTop();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01c1, code lost:
        r0 = r0 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01c2, code lost:
        r17 = r0;
        r15 = 0.0f;
        r14 = 0;
        r19 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01ca, code lost:
        if (r14 >= r12.length) goto L_0x03e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01cc, code lost:
        r4 = r12[r14];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01d2, code lost:
        if (r4.form == com.github.mikephil.charting.components.Legend.LegendForm.NONE) goto L_0x01d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01d4, code lost:
        r22 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01d7, code lost:
        r22 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01df, code lost:
        if (java.lang.Float.isNaN(r4.formSize) == false) goto L_0x01e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01e1, code lost:
        r23 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01e4, code lost:
        r23 = com.github.mikephil.charting.utils.Utils.convertDpToPixel(r4.formSize);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01ec, code lost:
        if (r22 == false) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01f0, code lost:
        if (r5 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01f2, code lost:
        r0 = r9 + r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01f4, code lost:
        r25 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01f7, code lost:
        r0 = r9 - (r23 - r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01fc, code lost:
        r27 = r4;
        r10 = r20;
        r13 = r5;
        drawForm(r7, r25, r17 + r11, r4, r6.mLegend);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0212, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x0218;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0214, code lost:
        r25 = r25 + r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0218, code lost:
        r0 = r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x021b, code lost:
        r13 = r5;
        r10 = r20;
        r0 = r4;
        r25 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0223, code lost:
        if (r0.label == null) goto L_0x0269;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0225, code lost:
        if (r22 == false) goto L_0x0237;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0227, code lost:
        if (r19 != false) goto L_0x0237;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x022b, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x0231;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x022d, code lost:
        r1 = r24;
        r5 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0231, code lost:
        r5 = r24;
        r1 = -r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0234, code lost:
        r1 = r25 + r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0237, code lost:
        r5 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0239, code lost:
        if (r19 == false) goto L_0x023d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x023b, code lost:
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x023d, code lost:
        r1 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0241, code lost:
        if (r13 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0243, code lost:
        r1 = r1 - ((float) com.github.mikephil.charting.utils.Utils.calcTextWidth(r6.mLegendLabelPaint, r0.label));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x024d, code lost:
        if (r19 != false) goto L_0x0257;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x024f, code lost:
        drawLabel(r7, r1, r17 + r8, r0.label);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0257, code lost:
        r17 = r17 + (r8 + r21);
        drawLabel(r7, r1, r17 + r8, r0.label);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0262, code lost:
        r17 = r17 + (r8 + r21);
        r15 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0269, code lost:
        r5 = r24;
        r15 = r15 + (r23 + r10);
        r19 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0271, code lost:
        r14 = r14 + 1;
        r24 = r5;
        r20 = r10;
        r5 = r13;
     */
    public void renderLegend(Canvas canvas) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        double d;
        Canvas canvas2 = canvas;
        if (this.mLegend.isEnabled()) {
            Typeface typeface = this.mLegend.getTypeface();
            if (typeface != null) {
                this.mLegendLabelPaint.setTypeface(typeface);
            }
            this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
            this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
            float lineHeight = Utils.getLineHeight(this.mLegendLabelPaint, this.legendFontMetrics);
            float lineSpacing = Utils.getLineSpacing(this.mLegendLabelPaint, this.legendFontMetrics) + Utils.convertDpToPixel(this.mLegend.getYEntrySpace());
            float calcTextHeight = lineHeight - (((float) Utils.calcTextHeight(this.mLegendLabelPaint, "ABC")) / 2.0f);
            LegendEntry[] entries = this.mLegend.getEntries();
            float convertDpToPixel = Utils.convertDpToPixel(this.mLegend.getFormToTextSpace());
            float convertDpToPixel2 = Utils.convertDpToPixel(this.mLegend.getXEntrySpace());
            LegendOrientation orientation = this.mLegend.getOrientation();
            LegendHorizontalAlignment horizontalAlignment = this.mLegend.getHorizontalAlignment();
            LegendVerticalAlignment verticalAlignment = this.mLegend.getVerticalAlignment();
            LegendDirection direction = this.mLegend.getDirection();
            float convertDpToPixel3 = Utils.convertDpToPixel(this.mLegend.getFormSize());
            float convertDpToPixel4 = Utils.convertDpToPixel(this.mLegend.getStackSpace());
            float yOffset = this.mLegend.getYOffset();
            float xOffset = this.mLegend.getXOffset();
            switch (horizontalAlignment) {
                case LEFT:
                    f4 = convertDpToPixel4;
                    f3 = lineSpacing;
                    f2 = convertDpToPixel;
                    f = convertDpToPixel2;
                    if (orientation != LegendOrientation.VERTICAL) {
                        xOffset += this.mViewPortHandler.contentLeft();
                    }
                    if (direction == LegendDirection.RIGHT_TO_LEFT) {
                        xOffset += this.mLegend.mNeededWidth;
                        break;
                    }
                    break;
                case RIGHT:
                    f4 = convertDpToPixel4;
                    f3 = lineSpacing;
                    f2 = convertDpToPixel;
                    f = convertDpToPixel2;
                    if (orientation == LegendOrientation.VERTICAL) {
                        f5 = this.mViewPortHandler.getChartWidth() - xOffset;
                    } else {
                        f5 = this.mViewPortHandler.contentRight() - xOffset;
                    }
                    if (direction == LegendDirection.LEFT_TO_RIGHT) {
                        xOffset = f5 - this.mLegend.mNeededWidth;
                        break;
                    }
                    break;
                case CENTER:
                    if (orientation == LegendOrientation.VERTICAL) {
                        f6 = this.mViewPortHandler.getChartWidth() / 2.0f;
                        f4 = convertDpToPixel4;
                    } else {
                        f4 = convertDpToPixel4;
                        f6 = this.mViewPortHandler.contentLeft() + (this.mViewPortHandler.contentWidth() / 2.0f);
                    }
                    f5 = (direction == LegendDirection.LEFT_TO_RIGHT ? xOffset : -xOffset) + f6;
                    if (orientation != LegendOrientation.VERTICAL) {
                        f3 = lineSpacing;
                        f2 = convertDpToPixel;
                        f = convertDpToPixel2;
                        break;
                    } else {
                        f3 = lineSpacing;
                        double d2 = (double) f5;
                        if (direction == LegendDirection.LEFT_TO_RIGHT) {
                            f2 = convertDpToPixel;
                            f = convertDpToPixel2;
                            d = (((double) (-this.mLegend.mNeededWidth)) / 2.0d) + ((double) xOffset);
                        } else {
                            f2 = convertDpToPixel;
                            f = convertDpToPixel2;
                            d = (((double) this.mLegend.mNeededWidth) / 2.0d) - ((double) xOffset);
                        }
                        xOffset = (float) (d2 + d);
                        break;
                    }
                default:
                    f4 = convertDpToPixel4;
                    f3 = lineSpacing;
                    f2 = convertDpToPixel;
                    f = convertDpToPixel2;
                    float f7 = 0.0f;
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawForm(Canvas canvas, float f, float f2, LegendEntry legendEntry, Legend legend) {
        if (legendEntry.formColor != 1122868 && legendEntry.formColor != 1122867 && legendEntry.formColor != 0) {
            int save = canvas.save();
            LegendForm legendForm = legendEntry.form;
            if (legendForm == LegendForm.DEFAULT) {
                legendForm = legend.getForm();
            }
            this.mLegendFormPaint.setColor(legendEntry.formColor);
            float convertDpToPixel = Utils.convertDpToPixel(Float.isNaN(legendEntry.formSize) ? legend.getFormSize() : legendEntry.formSize);
            float f3 = convertDpToPixel / 2.0f;
            switch (legendForm) {
                case DEFAULT:
                case CIRCLE:
                    this.mLegendFormPaint.setStyle(Style.FILL);
                    canvas.drawCircle(f + f3, f2, f3, this.mLegendFormPaint);
                    break;
                case SQUARE:
                    this.mLegendFormPaint.setStyle(Style.FILL);
                    canvas.drawRect(f, f2 - f3, f + convertDpToPixel, f2 + f3, this.mLegendFormPaint);
                    break;
                case LINE:
                    float convertDpToPixel2 = Utils.convertDpToPixel(Float.isNaN(legendEntry.formLineWidth) ? legend.getFormLineWidth() : legendEntry.formLineWidth);
                    DashPathEffect formLineDashEffect = legendEntry.formLineDashEffect == null ? legend.getFormLineDashEffect() : legendEntry.formLineDashEffect;
                    this.mLegendFormPaint.setStyle(Style.STROKE);
                    this.mLegendFormPaint.setStrokeWidth(convertDpToPixel2);
                    this.mLegendFormPaint.setPathEffect(formLineDashEffect);
                    this.mLineFormPath.reset();
                    this.mLineFormPath.moveTo(f, f2);
                    this.mLineFormPath.lineTo(f + convertDpToPixel, f2);
                    canvas.drawPath(this.mLineFormPath, this.mLegendFormPaint);
                    break;
            }
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    public void drawLabel(Canvas canvas, float f, float f2, String str) {
        canvas.drawText(str, f, f2, this.mLegendLabelPaint);
    }
}
