package com.revmob.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.facebook.Session;

public class RevMobSocialInfo {
    public String getFacebookToken(Context context) {
        Session activeSession = Session.getActiveSession();
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor edit = defaultSharedPreferences.edit();
        if (activeSession != null && activeSession.isOpened()) {
            edit.putString("facebookToken", activeSession.getAccessToken());
            edit.commit();
        }
        return defaultSharedPreferences.getString("facebookToken", null);
    }
}
