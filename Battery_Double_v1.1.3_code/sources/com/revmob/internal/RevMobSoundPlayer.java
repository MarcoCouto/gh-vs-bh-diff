package com.revmob.internal;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class RevMobSoundPlayer implements AsyncTaskCompleteListener {
    private DownloadManager downloader;
    /* access modifiers changed from: private */
    public MediaPlayer media;

    public void playFullscreenSound(Activity activity, String str) throws IOException {
        playSoundFromUrl(activity, str);
    }

    public void playBannerSound(Activity activity, String str) throws IOException {
        playSoundFromUrl(activity, str);
    }

    public void playPopupSound(Activity activity, String str) throws IOException {
        playSoundFromUrl(activity, str);
    }

    private void playSoundFromUrl(Activity activity, String str) {
        if (str != null && str.length() > 0) {
            this.downloader = new DownloadManager(activity, str, str.substring(str.lastIndexOf(47) + 1), this);
            this.downloader.execute(new String[0]);
        }
    }

    public void onTaskComplete(String str) {
        if (this.downloader.getIsSuccessful()) {
            this.media = new MediaPlayer();
            File file = this.downloader.getFile();
            this.media.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    RevMobSoundPlayer.this.media.release();
                }
            });
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                this.media.setDataSource(fileInputStream.getFD());
                this.media.prepare();
                this.media.start();
                fileInputStream.close();
            } catch (IOException e) {
                RMLog.d(e.toString());
            }
        }
    }
}
