package com.revmob.internal;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.webkit.WebView;
import com.revmob.FullscreenActivity;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.interstitial.internal.FullscreenWebview;
import com.revmob.client.AdData;

public class MarketAsyncManager extends AsyncTask<Void, Void, Void> {
    /* access modifiers changed from: private */
    public Activity activity;
    private AdData data;
    private boolean firstClick;
    private MarketAsyncManagerListener listener;
    private RevMobAdsListener publisherListener;
    private String relativePosition;

    public interface MarketAsyncManagerListener {
        void onPostExecute();

        void onPreExecute();
    }

    public MarketAsyncManager(Activity activity2, AdData adData, RevMobAdsListener revMobAdsListener) {
        this(activity2, adData, revMobAdsListener, null);
    }

    public MarketAsyncManager(Activity activity2, AdData adData, RevMobAdsListener revMobAdsListener, MarketAsyncManagerListener marketAsyncManagerListener) {
        this(activity2, adData, revMobAdsListener, null, null);
    }

    public MarketAsyncManager(Activity activity2, AdData adData, RevMobAdsListener revMobAdsListener, MarketAsyncManagerListener marketAsyncManagerListener, String str) {
        this.firstClick = true;
        this.relativePosition = null;
        this.activity = activity2;
        this.publisherListener = revMobAdsListener;
        this.data = adData;
        this.listener = marketAsyncManagerListener;
        this.relativePosition = str;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Void... voidArr) {
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdClicked();
        }
        openAdvertisement();
        return null;
    }

    public void openAdvertisement() {
        if (this.data.shouldFollowRedirect()) {
            openClickUrlFollowingRedirect(this.data.getClickUrl(this.relativePosition));
        } else {
            openClickUrlDirectly(this.data.getClickUrl(this.relativePosition));
        }
    }

    public void openClickUrlFollowingRedirect(String str) {
        String marketUrl = new MarketRedirector(str).getMarketUrl(this.data.getDspUrl());
        if (this.firstClick && this.data.getClickUrl() != null && this.data.getDspUrl() != null && !this.data.getDspUrl().endsWith("#click")) {
            if (this.relativePosition != null) {
                dspServerRequest(this.data.getClickUrl(this.relativePosition), "");
            } else {
                dspServerRequest(this.data.getClickUrl(this.relativePosition), "");
            }
        }
        if (marketUrl == null) {
            RMLog.e("Redirect link not received.");
            this.publisherListener.onRevMobAdNotReceived("Redirect link not received.");
        } else if (marketUrl != null && !str.equals(marketUrl)) {
            boolean booleanValue = FullscreenActivity.isFullscreenActivityAvailable(this.activity).booleanValue();
            if (this.data.getAppOrSite() != "site" || !this.data.isOpenInside() || !booleanValue) {
                openUrlInTheBrowser(marketUrl);
            } else {
                openUrlInTheWebView(marketUrl);
            }
        }
    }

    public void openClickUrlDirectly(String str) {
        if (this.data.isOpenInside()) {
            openUrlInTheWebViewWithPost(this.data.getClickUrl(this.relativePosition));
        } else {
            openUrlInTheBrowser(this.data.getClickUrl(this.relativePosition));
        }
    }

    public void openUrlInTheWebView(final String str) {
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                Intent intent = new Intent(MarketAsyncManager.this.activity, FullscreenActivity.class);
                intent.putExtra("marketURL", str);
                MarketAsyncManager.this.activity.startActivityForResult(intent, 0);
            }
        });
    }

    public void openUrlInTheWebViewWithPost(final String str) {
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                new FullscreenWebview(MarketAsyncManager.this.activity, new RevMobWebViewClient(null, null) {
                    public void onPageFinished(WebView webView, String str) {
                        MarketAsyncManager.this.openUrlInTheBrowser(str);
                    }
                }).postUrl(str, "".getBytes());
            }
        });
    }

    public void openUrlInTheBrowser(String str) {
        try {
            this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception e) {
            if (str.startsWith("market://")) {
                String substring = str.substring(20);
                Activity activity2 = this.activity;
                StringBuilder sb = new StringBuilder();
                sb.append("https://play.google.com/store/apps/details?id=");
                sb.append(substring);
                activity2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(sb.toString())));
            } else if (!str.startsWith("http")) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("http://");
                sb2.append(str);
                this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(sb2.toString())));
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Click url ( ");
                sb3.append(str);
                sb3.append(" ) not valid. Please report this to support@revmob.com with the exception stack trace: ");
                RMLog.e(sb3.toString(), e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.listener != null) {
            this.listener.onPreExecute();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Void voidR) {
        if (this.listener != null) {
            this.listener.onPostExecute();
        }
    }

    private void dspServerRequest(final String str, final String str2) {
        this.firstClick = false;
        new Thread() {
            public void run() {
                new HTTPHelper().post(str, str2);
            }
        }.start();
    }
}
