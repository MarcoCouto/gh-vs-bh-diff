package com.revmob.internal;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.revmob.RevMobAdsListener;

public class RevMobWebViewClient extends WebViewClient {
    private RevMobWebViewClickListener clickListener;
    private RevMobAdsListener publisherListener;

    public interface RevMobWebViewClickListener {
        boolean handleClick(WebView webView, String str);

        void handlePageFinished(WebView webView, String str);
    }

    public RevMobWebViewClient(RevMobAdsListener revMobAdsListener, RevMobWebViewClickListener revMobWebViewClickListener) {
        this.publisherListener = revMobAdsListener;
        this.clickListener = revMobWebViewClickListener;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.clickListener != null) {
            return this.clickListener.handleClick(webView, str);
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdNotReceived("Content was not loaded");
        }
    }

    public void onPageFinished(WebView webView, String str) {
        if (this.clickListener != null) {
            this.clickListener.handlePageFinished(webView, str);
        }
    }
}
