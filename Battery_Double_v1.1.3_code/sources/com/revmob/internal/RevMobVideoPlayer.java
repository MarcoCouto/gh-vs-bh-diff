package com.revmob.internal;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import java.io.IOException;

public class RevMobVideoPlayer implements AsyncTaskCompleteListener {
    private DownloadManager downloader;
    /* access modifiers changed from: private */
    public MediaPlayer media;

    public void playFullscreenVideo(Activity activity, String str) throws IOException {
        playVideoFromUrl(activity, str);
    }

    private void playVideoFromUrl(Activity activity, String str) {
        if (str != null && str.length() > 0) {
            this.downloader = new DownloadManager(activity, str, str.substring(str.lastIndexOf(47) + 1), this);
            this.downloader.execute(new String[0]);
        }
    }

    public void onTaskComplete(String str) {
        if (this.downloader.getIsSuccessful()) {
            this.media = new MediaPlayer();
            this.downloader.getFile();
            Log.e("onTaskComplete", "revmobvideoplayer");
            this.media.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    RevMobVideoPlayer.this.media.release();
                }
            });
        }
    }
}
