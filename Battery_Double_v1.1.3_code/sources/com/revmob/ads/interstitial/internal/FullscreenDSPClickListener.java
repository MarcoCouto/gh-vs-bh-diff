package com.revmob.ads.interstitial.internal;

import android.webkit.WebView;
import com.revmob.FullscreenActivity;
import com.revmob.internal.RevMobWebViewClient.RevMobWebViewClickListener;

public class FullscreenDSPClickListener extends FullscreenClickListener implements RevMobWebViewClickListener {
    public FullscreenDSPClickListener(FullscreenActivity fullscreenActivity) {
        super(fullscreenActivity);
    }

    public boolean handleClick(WebView webView, String str) {
        if (str.endsWith("#close") || str.endsWith("inneractive-skip")) {
            return onClose();
        }
        this.fullscreenActivity.data.setDspUrl(str);
        onClick();
        return false;
    }

    public void handlePageFinished(WebView webView, String str) {
        this.fullscreenActivity.removeProgressBar();
    }
}
