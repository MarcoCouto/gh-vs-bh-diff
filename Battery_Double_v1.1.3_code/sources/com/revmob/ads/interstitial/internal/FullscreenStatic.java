package com.revmob.ads.interstitial.internal;

import android.app.Activity;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.revmob.ads.interstitial.client.FullscreenData;
import com.revmob.internal.AndroidHelper;

public class FullscreenStatic extends RelativeLayout implements FullscreenView {
    private int amplitudeMax = 28;
    private FullscreenData data;
    private ImageView fullscreenImageView;
    private int height;
    private int initialAngleX = -999;
    private int initialAngleY = -999;
    private int initialX;
    private int initialY;
    private int parallaxDelta = 0;
    private boolean parallaxEnabled = false;
    private int width;
    private int x = 0;
    private int y = 0;

    public FullscreenStatic(Activity activity, FullscreenData fullscreenData, FullscreenClickListener fullscreenClickListener, boolean z, int i) {
        super(activity);
        this.data = fullscreenData;
        this.parallaxEnabled = z;
        if (i > this.amplitudeMax) {
            i = this.amplitudeMax;
        }
        this.parallaxDelta = i;
        if (z) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            this.width = displayMetrics.widthPixels;
            this.height = displayMetrics.heightPixels;
            this.initialX = 0;
            this.initialY = 0;
            this.x = this.initialX;
            this.y = this.initialY;
            setWillNotDraw(false);
            LayoutParams layoutParams = new LayoutParams(this.width, this.height);
            setGravity(48);
            layoutParams.leftMargin = 0;
            layoutParams.topMargin = 0;
            addView(createImageAdView(fullscreenClickListener), layoutParams);
            return;
        }
        addView(createImageAdView(fullscreenClickListener), new LayoutParams(-1, -1));
    }

    public void update() {
        if (this.fullscreenImageView != null) {
            this.fullscreenImageView.setImageBitmap(this.data.getAdImage(getResources().getConfiguration().orientation));
            if (this.data.isStaticMultiOrientationFullscreen() || this.data.getOrientationLock() == 1) {
                this.fullscreenImageView.setScaleType(ScaleType.FIT_XY);
            } else {
                this.fullscreenImageView.setScaleType(ScaleType.CENTER_CROP);
            }
        }
    }

    private View createImageAdView(final FullscreenClickListener fullscreenClickListener) {
        this.fullscreenImageView = new ImageView(getContext());
        update();
        this.fullscreenImageView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                fullscreenClickListener.onClick();
            }
        });
        return this.fullscreenImageView;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.parallaxEnabled) {
            canvas.translate((float) this.x, (float) this.y);
        }
        super.onDraw(canvas);
    }

    public void updateAccordingToDevicePosition(int i, int i2) {
        if (this.parallaxEnabled) {
            if (this.initialAngleX == -999) {
                this.initialAngleX = i;
            }
            if (this.initialAngleY == -999) {
                this.initialAngleY = i2;
            }
            int i3 = i - this.initialAngleX;
            if (i3 >= 0) {
                this.x = Math.max(this.initialX - this.parallaxDelta, this.initialX - Math.abs(i3));
            } else {
                this.x = Math.min(this.initialX + this.parallaxDelta, this.initialX + Math.abs(i3));
            }
            int i4 = i2 - this.initialAngleY;
            if (i4 >= 0) {
                this.y = Math.max(this.initialY - this.parallaxDelta, this.initialY - Math.abs(i4));
            } else {
                this.y = Math.min(this.initialY + this.parallaxDelta, this.initialY + Math.abs(i4));
            }
            if (AndroidHelper.isUIThread()) {
                invalidate();
            } else {
                postInvalidate();
            }
        }
    }
}
