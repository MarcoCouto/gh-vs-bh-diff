package com.revmob.ads.interstitial.internal;

import android.webkit.WebView;
import com.revmob.FullscreenActivity;
import com.revmob.internal.RevMobWebViewClient.RevMobWebViewClickListener;

public class FullscreenWebViewClickListener extends FullscreenClickListener implements RevMobWebViewClickListener {
    public FullscreenWebViewClickListener(FullscreenActivity fullscreenActivity) {
        super(fullscreenActivity);
    }

    public boolean handleClick(WebView webView, String str) {
        if (str.endsWith("#close")) {
            return onClose();
        }
        if (str.endsWith("#click")) {
            return onClick();
        }
        return true;
    }

    public void handlePageFinished(WebView webView, String str) {
        this.fullscreenActivity.removeProgressBar();
    }
}
