package com.revmob.ads.popup.client;

import com.revmob.client.AdData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class PopupData extends AdData {
    public static final String NO_MESSAGE = "No, thanks.";
    public static final String YES_MESSAGE = "Yes, sure!";
    private String clickSoundURL;
    private String message;
    private String showSoundURL;

    public PopupData(String str, String str2, boolean z, String str3, String str4, boolean z2, String str5, String str6) {
        super(str, str2, z, str4, z2);
        if (str3 == null || str3.length() <= 0) {
            str3 = "Download a FREE game!";
        }
        this.message = str3;
        this.showSoundURL = str5;
        this.clickSoundURL = str6;
    }

    public String getCampaignId() {
        String clickUrl = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parse = URLEncodedUtils.parse(new URI(clickUrl), "UTF-8");
            if (parse.size() > 0) {
                for (NameValuePair nameValuePair : parse) {
                    if (nameValuePair.getName().equals("campaign_id")) {
                        this.campaignId = nameValuePair.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }

    public String getMessage() {
        return this.message;
    }

    public String getShowSoundURL() {
        return this.showSoundURL;
    }

    public String getClickSoundURL() {
        return this.clickSoundURL;
    }
}
