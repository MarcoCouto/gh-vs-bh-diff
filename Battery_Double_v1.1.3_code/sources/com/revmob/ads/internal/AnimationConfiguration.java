package com.revmob.ads.internal;

import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import java.util.ArrayList;
import java.util.List;

public abstract class AnimationConfiguration {
    private List<String> animations = new ArrayList();
    private long time = 500;

    public abstract Animation createClockwise();

    public abstract Animation createCounterClockwise();

    public abstract Animation createFadeIn();

    public abstract Animation createFadeOut();

    public abstract Animation createSlideDown();

    public abstract Animation createSlideLeft();

    public abstract Animation createSlideRight();

    public abstract Animation createSlideUp();

    public abstract Animation createZoomIn();

    public abstract Animation createZoomOut();

    public void setTime(long j) {
        this.time = j;
    }

    public void addAnimation(String str) {
        this.animations.add(str);
    }

    public Animation getAnimation() {
        AnimationSet animationSet = new AnimationSet(true);
        for (String str : this.animations) {
            if (str.equals("fade_in")) {
                animationSet.addAnimation(createFadeIn());
            } else if (str.equals("fade_out")) {
                animationSet.addAnimation(createFadeOut());
            } else if (str.equals("zoom_in")) {
                animationSet.addAnimation(createZoomIn());
            } else if (str.equals("zoom_out")) {
                animationSet.addAnimation(createZoomOut());
            } else if (str.equals("slide_up")) {
                animationSet.addAnimation(createSlideUp());
            } else if (str.equals("slide_down")) {
                animationSet.addAnimation(createSlideDown());
            } else if (str.equals("slide_right")) {
                animationSet.addAnimation(createSlideRight());
            } else if (str.equals("slide_left")) {
                animationSet.addAnimation(createSlideLeft());
            } else if (str.equals("rotate_clockwise")) {
                animationSet.addAnimation(createClockwise());
            } else if (str.equals("rotate_counterclockwise")) {
                animationSet.addAnimation(createCounterClockwise());
            }
        }
        animationSet.setDuration(this.time);
        return animationSet;
    }
}
