package com.revmob.ads.internal;

import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.revmob.RevMobAdsListener;
import com.revmob.client.RevMobClientListener;
import com.revmob.internal.HTTPHelper;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class AdRevMobClientListener implements RevMobClientListener {
    protected Ad ad;
    protected RevMobAdsListener publisherListener;

    public void handleResponse(String str) throws JSONException {
    }

    public AdRevMobClientListener(Ad ad2, RevMobAdsListener revMobAdsListener) {
        this.ad = ad2;
        this.publisherListener = revMobAdsListener;
    }

    public void handleError(String str) {
        if (this.publisherListener != null) {
            RevMobAdsListener revMobAdsListener = this.publisherListener;
            StringBuilder sb = new StringBuilder();
            sb.append("Ad not received: ");
            sb.append(str);
            revMobAdsListener.onRevMobAdNotReceived(sb.toString());
        }
    }

    public static String optionalGetString(JSONObject jSONObject, String str, String str2) {
        try {
            return jSONObject.getString(str);
        } catch (JSONException unused) {
            return str2;
        }
    }

    public static String getLinkByRel(JSONArray jSONArray, String str) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.getJSONObject(i).getString("rel").equals(str)) {
                return jSONArray.getJSONObject(i).getString(ShareConstants.WEB_DIALOG_PARAM_HREF);
            }
        }
        return null;
    }

    public static String getStringKeyByRel(JSONArray jSONArray, String str, String str2) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.getJSONObject(i).getString("rel").equals(str)) {
                return jSONArray.getJSONObject(i).getString(str2);
            }
        }
        return null;
    }

    public static int getIntKeyByRel(JSONArray jSONArray, String str, String str2) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.getJSONObject(i).getString("rel").equals(str)) {
                return jSONArray.getJSONObject(i).getInt(str2);
            }
        }
        return 999;
    }

    public static double getDoubleKeyByRel(JSONArray jSONArray, String str, String str2) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.getJSONObject(i).getString("rel").equals(str)) {
                return jSONArray.getJSONObject(i).getDouble(str2);
            }
        }
        return 999.99d;
    }

    public static String getTextByRel(JSONArray jSONArray, String str) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.getJSONObject(i).getString("rel").equals(str)) {
                return jSONArray.getJSONObject(i).getString(MimeTypes.BASE_TYPE_TEXT);
            }
        }
        return null;
    }

    public static void getAllLinksByRel(JSONArray jSONArray, String str, ArrayList<String> arrayList) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.getJSONObject(i).getString("rel").equals(str)) {
                arrayList.add(jSONArray.getJSONObject(i).getString(ShareConstants.WEB_DIALOG_PARAM_HREF));
            }
        }
    }

    public static void getAllStringsInJSONArray(JSONArray jSONArray, ArrayList<String> arrayList) throws JSONException {
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
    }

    public static int getFullscreenType(JSONObject jSONObject, String str) throws JSONException {
        if (jSONObject.has(str)) {
            return jSONObject.getInt(str);
        }
        return 0;
    }

    public static int getVideoInt(JSONObject jSONObject, String str) throws JSONException {
        if (jSONObject.has(str)) {
            return jSONObject.getInt(str);
        }
        return 0;
    }

    public static double getVideoDouble(JSONObject jSONObject, String str) throws JSONException {
        return jSONObject.has(str) ? jSONObject.getDouble(str) : Utils.DOUBLE_EPSILON;
    }

    public static String getVideoString(JSONObject jSONObject, String str) throws JSONException {
        if (str != null) {
            return jSONObject.getString(str);
        }
        return null;
    }

    public static boolean getFollowRedirect(JSONObject jSONObject) throws JSONException {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "follow_redirect", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE));
    }

    public static String getClickUrl(JSONArray jSONArray) throws JSONException {
        return getLinkByRel(jSONArray, "clicks");
    }

    public static String getImpressionUrl(JSONArray jSONArray) throws JSONException {
        return getLinkByRel(jSONArray, "impressions");
    }

    public static String getAppOrSite(JSONObject jSONObject) throws JSONException {
        return optionalGetString(jSONObject, "app_or_site", SettingsJsonConstants.APP_KEY);
    }

    public static boolean getOpenInside(JSONObject jSONObject) throws JSONException {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "open_inside", "false"));
    }

    public static String getCustomUserAgent(JSONObject jSONObject) throws JSONException {
        return optionalGetString(jSONObject, "customUserAgent", HTTPHelper.getUserAgent());
    }

    public static String getIpAddress(JSONObject jSONObject) throws JSONException {
        return optionalGetString(jSONObject, "ip_address", HTTPHelper.getIpAddress());
    }

    public static boolean getOpenAdLink(JSONObject jSONObject) throws JSONException {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "shouldOpenAdLink", "false"));
    }

    public static boolean getShouldExtractSocial(JSONObject jSONObject) {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "shouldExtractSocial", "false"));
    }

    public static boolean getShouldExtractGeolocation(JSONObject jSONObject) {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "shouldExtractGeolocation", "false"));
    }

    public static boolean getShouldExtractOtherAppsData(JSONObject jSONObject) {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "shouldExtractOtherAppsData", "false"));
    }

    public static boolean getShouldContinueOnBackground(JSONObject jSONObject) {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "shouldContinueOnBackground", "false"));
    }

    public static String getEulaUrl(JSONObject jSONObject) {
        return optionalGetString(jSONObject, "eula_url", HTTPHelper.getEulaUrl());
    }

    public static String getEulaVersion(JSONObject jSONObject) {
        return optionalGetString(jSONObject, "eula_version", HTTPHelper.getEulaVersion());
    }

    public static boolean getShouldShowEula(JSONObject jSONObject) {
        return Boolean.parseBoolean(optionalGetString(jSONObject, "should_show_eula", "false"));
    }
}
