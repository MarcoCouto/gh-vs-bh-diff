package org.altbeacon.beacon.distance;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import com.facebook.internal.ServerProtocol;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.altbeacon.beacon.logging.LogManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ModelSpecificDistanceCalculator implements DistanceCalculator {
    private static final String CONFIG_FILE = "model-distance-calculations.json";
    private static final String TAG = "ModelSpecificDistanceCalculator";
    private Context mContext;
    private AndroidModel mDefaultModel;
    /* access modifiers changed from: private */
    public DistanceCalculator mDistanceCalculator;
    private AndroidModel mModel;
    Map<AndroidModel, DistanceCalculator> mModelMap;
    /* access modifiers changed from: private */
    public String mRemoteUpdateUrlString;
    /* access modifiers changed from: private */
    public AndroidModel mRequestedModel;

    public ModelSpecificDistanceCalculator(Context context, String str) {
        this(context, str, AndroidModel.forThisDevice());
    }

    public ModelSpecificDistanceCalculator(Context context, String str, AndroidModel androidModel) {
        this.mRemoteUpdateUrlString = null;
        this.mRequestedModel = androidModel;
        this.mRemoteUpdateUrlString = str;
        this.mContext = context;
        loadModelMap();
        this.mDistanceCalculator = findCalculatorForModel(androidModel);
    }

    public AndroidModel getModel() {
        return this.mModel;
    }

    public AndroidModel getRequestedModel() {
        return this.mRequestedModel;
    }

    public double calculateDistance(int i, double d) {
        if (this.mDistanceCalculator != null) {
            return this.mDistanceCalculator.calculateDistance(i, d);
        }
        LogManager.w(TAG, "distance calculator has not been set", new Object[0]);
        return -1.0d;
    }

    /* access modifiers changed from: private */
    public DistanceCalculator findCalculatorForModel(AndroidModel androidModel) {
        LogManager.d(TAG, "Finding best distance calculator for %s, %s, %s, %s", androidModel.getVersion(), androidModel.getBuildNumber(), androidModel.getModel(), androidModel.getManufacturer());
        if (this.mModelMap == null) {
            LogManager.d(TAG, "Cannot get distance calculator because modelMap was never initialized", new Object[0]);
            return null;
        }
        AndroidModel androidModel2 = null;
        int i = 0;
        for (AndroidModel androidModel3 : this.mModelMap.keySet()) {
            if (androidModel3.matchScore(androidModel) > i) {
                i = androidModel3.matchScore(androidModel);
                androidModel2 = androidModel3;
            }
        }
        if (androidModel2 != null) {
            LogManager.d(TAG, "found a match with score %s", Integer.valueOf(i));
            LogManager.d(TAG, "Finding best distance calculator for %s, %s, %s, %s", androidModel2.getVersion(), androidModel2.getBuildNumber(), androidModel2.getModel(), androidModel2.getManufacturer());
            this.mModel = androidModel2;
        } else {
            this.mModel = this.mDefaultModel;
            LogManager.w(TAG, "Cannot find match for this device.  Using default", new Object[0]);
        }
        return (DistanceCalculator) this.mModelMap.get(this.mModel);
    }

    private void loadModelMap() {
        boolean z;
        if (this.mRemoteUpdateUrlString != null) {
            z = loadModelMapFromFile();
            if (!z) {
                requestModelMapFromWeb();
            }
        } else {
            z = false;
        }
        if (!z) {
            loadDefaultModelMap();
        }
        this.mDistanceCalculator = findCalculatorForModel(this.mRequestedModel);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:3|4|(3:5|6|(1:8)(1:67))|(2:10|11)|(2:14|15)|16|17|18) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
        org.altbeacon.beacon.logging.LogManager.e(TAG, "Cannot update distance models from online database at %s with JSON", r0, r8.mRemoteUpdateUrlString, r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005d, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x003d */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007a A[SYNTHETIC, Splitter:B:37:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x007f A[SYNTHETIC, Splitter:B:41:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0086 A[SYNTHETIC, Splitter:B:47:0x0086] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x008b A[SYNTHETIC, Splitter:B:51:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0092 A[SYNTHETIC, Splitter:B:59:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0097 A[SYNTHETIC, Splitter:B:63:0x0097] */
    public boolean loadModelMapFromFile() {
        FileInputStream fileInputStream;
        File file = new File(this.mContext.getFilesDir(), CONFIG_FILE);
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(fileInputStream));
                while (true) {
                    try {
                        String readLine = bufferedReader2.readLine();
                        if (readLine == null) {
                            break;
                        }
                        sb.append(readLine);
                        sb.append("\n");
                    } catch (FileNotFoundException unused) {
                        bufferedReader = bufferedReader2;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception unused2) {
                            }
                        }
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (Exception unused3) {
                            }
                        }
                        return false;
                    } catch (IOException e) {
                        e = e;
                        bufferedReader = bufferedReader2;
                        try {
                            LogManager.e(e, TAG, "Cannot open distance model file %s", file);
                            if (bufferedReader != null) {
                                try {
                                    bufferedReader.close();
                                } catch (Exception unused4) {
                                }
                            }
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                } catch (Exception unused5) {
                                }
                            }
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            if (bufferedReader != null) {
                                try {
                                    bufferedReader.close();
                                } catch (Exception unused6) {
                                }
                            }
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                } catch (Exception unused7) {
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        bufferedReader = bufferedReader2;
                        if (bufferedReader != null) {
                        }
                        if (fileInputStream != null) {
                        }
                        throw th;
                    }
                }
                if (bufferedReader2 != null) {
                    try {
                        bufferedReader2.close();
                    } catch (Exception unused8) {
                    }
                }
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                buildModelMap(sb.toString());
                return true;
            } catch (FileNotFoundException unused9) {
                if (bufferedReader != null) {
                }
                if (fileInputStream != null) {
                }
                return false;
            } catch (IOException e2) {
                e = e2;
                LogManager.e(e, TAG, "Cannot open distance model file %s", file);
                if (bufferedReader != null) {
                }
                if (fileInputStream != null) {
                }
                return false;
            }
        } catch (FileNotFoundException unused10) {
            fileInputStream = null;
            if (bufferedReader != null) {
            }
            if (fileInputStream != null) {
            }
            return false;
        } catch (IOException e3) {
            e = e3;
            fileInputStream = null;
            LogManager.e(e, TAG, "Cannot open distance model file %s", file);
            if (bufferedReader != null) {
            }
            if (fileInputStream != null) {
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (bufferedReader != null) {
            }
            if (fileInputStream != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[SYNTHETIC, Splitter:B:21:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003e A[SYNTHETIC, Splitter:B:26:0x003e] */
    public boolean saveJson(String str) {
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream openFileOutput = this.mContext.openFileOutput(CONFIG_FILE, 0);
            try {
                openFileOutput.write(str.getBytes());
                openFileOutput.close();
                if (openFileOutput != null) {
                    try {
                        openFileOutput.close();
                    } catch (Exception unused) {
                    }
                }
                LogManager.i(TAG, "Successfully saved new distance model file", new Object[0]);
                return true;
            } catch (Exception e) {
                e = e;
                fileOutputStream = openFileOutput;
                try {
                    LogManager.w(e, TAG, "Cannot write updated distance model to local storage", new Object[0]);
                    if (fileOutputStream != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    if (fileOutputStream != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = openFileOutput;
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Exception unused2) {
                    }
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            LogManager.w(e, TAG, "Cannot write updated distance model to local storage", new Object[0]);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception unused3) {
                }
            }
            return false;
        }
    }

    @TargetApi(11)
    private void requestModelMapFromWeb() {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            LogManager.w(TAG, "App has no android.permission.INTERNET permission.  Cannot check for distance model updates", new Object[0]);
        } else {
            new ModelSpecificDistanceUpdater(this.mContext, this.mRemoteUpdateUrlString, new CompletionHandler() {
                public void onComplete(String str, Exception exc, int i) {
                    if (exc != null) {
                        LogManager.w(ModelSpecificDistanceCalculator.TAG, "Cannot updated distance models from online database at %s", exc, ModelSpecificDistanceCalculator.this.mRemoteUpdateUrlString);
                    } else if (i != 200) {
                        LogManager.w(ModelSpecificDistanceCalculator.TAG, "Cannot updated distance models from online database at %s due to HTTP status code %s", ModelSpecificDistanceCalculator.this.mRemoteUpdateUrlString, Integer.valueOf(i));
                    } else {
                        LogManager.d(ModelSpecificDistanceCalculator.TAG, "Successfully downloaded distance models from online database", new Object[0]);
                        try {
                            ModelSpecificDistanceCalculator.this.buildModelMap(str);
                            if (ModelSpecificDistanceCalculator.this.saveJson(str)) {
                                ModelSpecificDistanceCalculator.this.loadModelMapFromFile();
                                ModelSpecificDistanceCalculator.this.mDistanceCalculator = ModelSpecificDistanceCalculator.this.findCalculatorForModel(ModelSpecificDistanceCalculator.this.mRequestedModel);
                                LogManager.i(ModelSpecificDistanceCalculator.TAG, "Successfully updated distance model with latest from online database", new Object[0]);
                            }
                        } catch (JSONException e) {
                            LogManager.w(e, ModelSpecificDistanceCalculator.TAG, "Cannot parse json from downloaded distance model", new Object[0]);
                        }
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public void buildModelMap(String str) throws JSONException {
        this.mModelMap = new HashMap();
        JSONArray jSONArray = new JSONObject(str).getJSONArray("models");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            boolean z = jSONObject.has("default") ? jSONObject.getBoolean("default") : false;
            Double valueOf = Double.valueOf(jSONObject.getDouble("coefficient1"));
            Double valueOf2 = Double.valueOf(jSONObject.getDouble("coefficient2"));
            Double valueOf3 = Double.valueOf(jSONObject.getDouble("coefficient3"));
            String string = jSONObject.getString(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
            String string2 = jSONObject.getString("build_number");
            String string3 = jSONObject.getString("model");
            String string4 = jSONObject.getString("manufacturer");
            double doubleValue = valueOf.doubleValue();
            double doubleValue2 = valueOf2.doubleValue();
            double doubleValue3 = valueOf3.doubleValue();
            CurveFittedDistanceCalculator curveFittedDistanceCalculator = r12;
            CurveFittedDistanceCalculator curveFittedDistanceCalculator2 = new CurveFittedDistanceCalculator(doubleValue, doubleValue2, doubleValue3);
            AndroidModel androidModel = new AndroidModel(string, string2, string3, string4);
            this.mModelMap.put(androidModel, curveFittedDistanceCalculator);
            if (z) {
                this.mDefaultModel = androidModel;
            }
        }
    }

    private void loadDefaultModelMap() {
        this.mModelMap = new HashMap();
        try {
            buildModelMap(stringFromFilePath(CONFIG_FILE));
        } catch (Exception e) {
            LogManager.e(e, TAG, "Cannot build model distance calculations", new Object[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0094  */
    private String stringFromFilePath(String str) throws IOException {
        InputStream inputStream;
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = null;
        Class<ModelSpecificDistanceCalculator> cls = ModelSpecificDistanceCalculator.class;
        try {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("/");
            sb2.append(str);
            inputStream = cls.getResourceAsStream(sb2.toString());
            if (inputStream == null) {
                try {
                    ClassLoader classLoader = getClass().getClassLoader();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("/");
                    sb3.append(str);
                    inputStream = classLoader.getResourceAsStream(sb3.toString());
                } catch (Throwable th) {
                    th = th;
                    if (bufferedReader != null) {
                    }
                    if (inputStream != null) {
                    }
                    throw th;
                }
            }
            if (inputStream == null) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Cannot load resource at ");
                sb4.append(str);
                throw new RuntimeException(sb4.toString());
            }
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            try {
                for (String readLine = bufferedReader2.readLine(); readLine != null; readLine = bufferedReader2.readLine()) {
                    sb.append(readLine);
                    sb.append(10);
                }
                if (bufferedReader2 != null) {
                    bufferedReader2.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                return sb.toString();
            } catch (Throwable th2) {
                bufferedReader = bufferedReader2;
                th = th2;
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            if (bufferedReader != null) {
            }
            if (inputStream != null) {
            }
            throw th;
        }
    }
}
