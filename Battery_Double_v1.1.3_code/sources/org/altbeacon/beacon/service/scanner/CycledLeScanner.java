package org.altbeacon.beacon.service.scanner;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import java.util.Date;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.startup.StartupBroadcastReceiver;
import org.altbeacon.bluetooth.BluetoothCrashResolver;

@TargetApi(18)
public abstract class CycledLeScanner {
    private static final String TAG = "CycledLeScanner";
    protected boolean mBackgroundFlag = false;
    protected long mBetweenScanPeriod;
    private BluetoothAdapter mBluetoothAdapter;
    protected final BluetoothCrashResolver mBluetoothCrashResolver;
    protected final Context mContext;
    protected final CycledLeScanCallback mCycledLeScanCallback;
    protected final Handler mHandler = new Handler();
    private long mLastScanCycleEndTime = 0;
    private long mLastScanCycleStartTime = 0;
    protected long mNextScanCycleStartTime = 0;
    protected boolean mRestartNeeded = false;
    private long mScanCycleStopTime = 0;
    private boolean mScanCyclerStarted = false;
    private long mScanPeriod;
    private boolean mScanning;
    private boolean mScanningEnabled = false;
    protected boolean mScanningPaused;
    private PendingIntent mWakeUpOperation = null;

    /* access modifiers changed from: protected */
    public abstract boolean deferScanIfNeeded();

    /* access modifiers changed from: protected */
    public abstract void finishScan();

    /* access modifiers changed from: protected */
    public abstract void startScan();

    /* access modifiers changed from: protected */
    public abstract void stopScan();

    protected CycledLeScanner(Context context, long j, long j2, boolean z, CycledLeScanCallback cycledLeScanCallback, BluetoothCrashResolver bluetoothCrashResolver) {
        this.mScanPeriod = j;
        this.mBetweenScanPeriod = j2;
        this.mContext = context;
        this.mCycledLeScanCallback = cycledLeScanCallback;
        this.mBluetoothCrashResolver = bluetoothCrashResolver;
        this.mBackgroundFlag = z;
    }

    public static CycledLeScanner createScanner(Context context, long j, long j2, boolean z, CycledLeScanCallback cycledLeScanCallback, BluetoothCrashResolver bluetoothCrashResolver) {
        boolean z2 = false;
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.", new Object[0]);
            return null;
        }
        if (VERSION.SDK_INT < 21) {
            LogManager.i(TAG, "This is not Android 5.0.  We are using old scanning APIs", new Object[0]);
        } else if (BeaconManager.isAndroidLScanningDisabled()) {
            LogManager.i(TAG, "This Android 5.0, but L scanning is disabled. We are using old scanning APIs", new Object[0]);
        } else {
            LogManager.i(TAG, "This Android 5.0.  We are using new scanning APIs", new Object[0]);
            z2 = true;
        }
        if (z2) {
            CycledLeScannerForLollipop cycledLeScannerForLollipop = new CycledLeScannerForLollipop(context, j, j2, z, cycledLeScanCallback, bluetoothCrashResolver);
            return cycledLeScannerForLollipop;
        }
        CycledLeScannerForJellyBeanMr2 cycledLeScannerForJellyBeanMr2 = new CycledLeScannerForJellyBeanMr2(context, j, j2, z, cycledLeScanCallback, bluetoothCrashResolver);
        return cycledLeScannerForJellyBeanMr2;
    }

    public void setScanPeriods(long j, long j2, boolean z) {
        LogManager.d(TAG, "Set scan periods called with %s, %s Background mode must have changed.", Long.valueOf(j), Long.valueOf(j2));
        if (this.mBackgroundFlag != z) {
            this.mRestartNeeded = true;
        }
        this.mBackgroundFlag = z;
        this.mScanPeriod = j;
        this.mBetweenScanPeriod = j2;
        if (this.mBackgroundFlag) {
            LogManager.d(TAG, "We are in the background.  Setting wakeup alarm", new Object[0]);
            setWakeUpAlarm();
        } else {
            LogManager.d(TAG, "We are not in the background.  Cancelling wakeup alarm", new Object[0]);
            cancelWakeUpAlarm();
        }
        long time = new Date().getTime();
        if (this.mNextScanCycleStartTime > time) {
            long j3 = this.mLastScanCycleEndTime + j2;
            if (j3 < this.mNextScanCycleStartTime) {
                this.mNextScanCycleStartTime = j3;
                LogManager.i(TAG, "Adjusted nextScanStartTime to be %s", new Date(this.mNextScanCycleStartTime));
            }
        }
        if (this.mScanCycleStopTime > time) {
            long j4 = this.mLastScanCycleStartTime + j;
            if (j4 < this.mScanCycleStopTime) {
                this.mScanCycleStopTime = j4;
                LogManager.i(TAG, "Adjusted scanStopTime to be %s", Long.valueOf(this.mScanCycleStopTime));
            }
        }
    }

    public void start() {
        LogManager.d(TAG, "start called", new Object[0]);
        this.mScanningEnabled = true;
        if (!this.mScanCyclerStarted) {
            scanLeDevice(Boolean.valueOf(true));
        } else {
            LogManager.d(TAG, "scanning already started", new Object[0]);
        }
    }

    @SuppressLint({"NewApi"})
    public void stop() {
        LogManager.d(TAG, "stop called", new Object[0]);
        this.mScanningEnabled = false;
        if (this.mScanCyclerStarted) {
            scanLeDevice(Boolean.valueOf(false));
        }
        if (this.mBluetoothAdapter != null) {
            stopScan();
            this.mLastScanCycleEndTime = new Date().getTime();
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public void scanLeDevice(Boolean bool) {
        this.mScanCyclerStarted = true;
        if (getBluetoothAdapter() == null) {
            LogManager.e(TAG, "No bluetooth adapter.  beaconService cannot scan.", new Object[0]);
        }
        if (!bool.booleanValue()) {
            LogManager.d(TAG, "disabling scan", new Object[0]);
            this.mScanning = false;
            stopScan();
            this.mLastScanCycleEndTime = new Date().getTime();
        } else if (!deferScanIfNeeded()) {
            LogManager.d(TAG, "starting a new scan cycle", new Object[0]);
            if (!this.mScanning || this.mScanningPaused || this.mRestartNeeded) {
                this.mScanning = true;
                this.mScanningPaused = false;
                try {
                    if (getBluetoothAdapter() != null && getBluetoothAdapter().isEnabled()) {
                        if (this.mBluetoothCrashResolver != null && this.mBluetoothCrashResolver.isRecoveryInProgress()) {
                            LogManager.w(TAG, "Skipping scan because crash recovery is in progress.", new Object[0]);
                        } else if (this.mScanningEnabled) {
                            if (this.mRestartNeeded) {
                                this.mRestartNeeded = false;
                                LogManager.d(TAG, "restarting a bluetooth le scan", new Object[0]);
                            } else {
                                LogManager.d(TAG, "starting a new bluetooth le scan", new Object[0]);
                            }
                            try {
                                startScan();
                            } catch (Exception e) {
                                LogManager.e(e, TAG, "Internal Android exception scanning for beacons", new Object[0]);
                            }
                        } else {
                            LogManager.d(TAG, "Scanning unnecessary - no monitoring or ranging active.", new Object[0]);
                        }
                        this.mLastScanCycleStartTime = new Date().getTime();
                    }
                } catch (Exception e2) {
                    LogManager.e(e2, TAG, "Exception starting bluetooth scan.  Perhaps bluetooth is disabled or unavailable?", new Object[0]);
                }
            } else {
                LogManager.d(TAG, "We are already scanning", new Object[0]);
            }
            this.mScanCycleStopTime = new Date().getTime() + this.mScanPeriod;
            scheduleScanCycleStop();
            LogManager.d(TAG, "Scan started", new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void scheduleScanCycleStop() {
        long time = this.mScanCycleStopTime - new Date().getTime();
        if (time > 0) {
            LogManager.d(TAG, "Waiting to stop scan cycle for another %s milliseconds", Long.valueOf(time));
            if (this.mBackgroundFlag) {
                setWakeUpAlarm();
            }
            Handler handler = this.mHandler;
            AnonymousClass1 r1 = new Runnable() {
                public void run() {
                    CycledLeScanner.this.scheduleScanCycleStop();
                }
            };
            long j = 1000;
            if (time <= 1000) {
                j = time;
            }
            handler.postDelayed(r1, j);
            return;
        }
        finishScanCycle();
    }

    private void finishScanCycle() {
        LogManager.d(TAG, "Done with scan cycle", new Object[0]);
        this.mCycledLeScanCallback.onCycleEnd();
        if (this.mScanning) {
            if (getBluetoothAdapter() != null && getBluetoothAdapter().isEnabled()) {
                try {
                    LogManager.d(TAG, "stopping bluetooth le scan", new Object[0]);
                    finishScan();
                } catch (Exception e) {
                    LogManager.w(e, TAG, "Internal Android exception scanning for beacons", new Object[0]);
                }
                this.mLastScanCycleEndTime = new Date().getTime();
            }
            this.mNextScanCycleStartTime = getNextScanStartTime();
            if (this.mScanningEnabled) {
                scanLeDevice(Boolean.valueOf(true));
                return;
            }
            LogManager.d(TAG, "Scanning disabled.  No ranging or monitoring regions are active.", new Object[0]);
            this.mScanCyclerStarted = false;
            cancelWakeUpAlarm();
        }
    }

    /* access modifiers changed from: protected */
    public BluetoothAdapter getBluetoothAdapter() {
        if (this.mBluetoothAdapter == null) {
            this.mBluetoothAdapter = ((BluetoothManager) this.mContext.getApplicationContext().getSystemService("bluetooth")).getAdapter();
            if (this.mBluetoothAdapter == null) {
                LogManager.w(TAG, "Failed to construct a BluetoothAdapter", new Object[0]);
            }
        }
        return this.mBluetoothAdapter;
    }

    /* access modifiers changed from: protected */
    public void setWakeUpAlarm() {
        long j = this.mBetweenScanPeriod;
        long j2 = BeaconManager.DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD;
        if (BeaconManager.DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD < j) {
            j2 = this.mBetweenScanPeriod;
        }
        if (j2 < this.mScanPeriod) {
            j2 = this.mScanPeriod;
        }
        AlarmManager alarmManager = (AlarmManager) this.mContext.getSystemService(NotificationCompat.CATEGORY_ALARM);
        Intent intent = new Intent();
        intent.setClassName(this.mContext, StartupBroadcastReceiver.class.getName());
        intent.putExtra("wakeup", true);
        cancelWakeUpAlarm();
        this.mWakeUpOperation = PendingIntent.getBroadcast(this.mContext, 0, intent, 268435456);
        alarmManager.set(2, System.currentTimeMillis() + j2, this.mWakeUpOperation);
        LogManager.d(TAG, "Set a wakeup alarm to go off in %s ms: %s", Long.valueOf(j2), this.mWakeUpOperation);
    }

    /* access modifiers changed from: protected */
    public void cancelWakeUpAlarm() {
        LogManager.d(TAG, "cancel wakeup alarm: %s", this.mWakeUpOperation);
        if (this.mWakeUpOperation != null) {
            ((AlarmManager) this.mContext.getSystemService(NotificationCompat.CATEGORY_ALARM)).cancel(this.mWakeUpOperation);
        }
    }

    private long getNextScanStartTime() {
        if (this.mBetweenScanPeriod == 0) {
            return System.currentTimeMillis();
        }
        long currentTimeMillis = this.mBetweenScanPeriod - (System.currentTimeMillis() % (this.mScanPeriod + this.mBetweenScanPeriod));
        LogManager.d(TAG, "Normalizing between scan period from %s to %s", Long.valueOf(this.mBetweenScanPeriod), Long.valueOf(currentTimeMillis));
        return System.currentTimeMillis() + currentTimeMillis;
    }
}
