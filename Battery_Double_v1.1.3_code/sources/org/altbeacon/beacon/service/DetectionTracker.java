package org.altbeacon.beacon.service;

public class DetectionTracker {
    private static DetectionTracker sDetectionTracker;
    private long mLastDetectionTime = 0;

    private DetectionTracker() {
    }

    public static synchronized DetectionTracker getInstance() {
        DetectionTracker detectionTracker;
        synchronized (DetectionTracker.class) {
            if (sDetectionTracker == null) {
                sDetectionTracker = new DetectionTracker();
            }
            detectionTracker = sDetectionTracker;
        }
        return detectionTracker;
    }

    public long getLastDetectionTime() {
        return this.mLastDetectionTime;
    }

    public void recordDetection() {
        this.mLastDetectionTime = System.currentTimeMillis();
    }
}
