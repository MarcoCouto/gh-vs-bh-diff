package org.altbeacon.beacon;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Identifier implements Comparable<Identifier> {
    private static final Pattern DECIMAL_PATTERN = Pattern.compile("^[0-9]+$");
    private static final Pattern HEX_PATTERN = Pattern.compile("^0x[0-9A-Fa-f]*$");
    private static final Pattern UUID_PATTERN = Pattern.compile("^[0-9A-Fa-f]{8}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{12}$");
    private static final char[] hexArray = "0123456789abcdef".toCharArray();
    private static final BigInteger maxInteger = BigInteger.valueOf(65535);
    private final byte[] mValue;

    public static Identifier parse(String str) {
        if (str == null) {
            throw new NullPointerException("stringValue == null");
        } else if (HEX_PATTERN.matcher(str).matches()) {
            return parseHex(str.substring(2));
        } else {
            if (UUID_PATTERN.matcher(str).matches()) {
                return parseHex(str.replace("-", ""));
            }
            if (DECIMAL_PATTERN.matcher(str).matches()) {
                BigInteger bigInteger = new BigInteger(str);
                if (bigInteger.compareTo(BigInteger.ZERO) >= 0 && bigInteger.compareTo(maxInteger) <= 0) {
                    return fromInt(bigInteger.intValue());
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Decimal formatted integers must be between 0 and 65535. Value: ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to parse identifier: ");
            sb2.append(str);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    private static Identifier parseHex(String str) {
        String str2;
        if (str.length() % 2 == 0) {
            str2 = str.toLowerCase();
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("0");
            sb.append(str.toLowerCase());
            str2 = sb.toString();
        }
        byte[] bArr = new byte[(str2.length() / 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) Integer.parseInt(str2.substring(i2, i2 + 2), 16);
        }
        return new Identifier(bArr);
    }

    public static Identifier fromInt(int i) {
        if (i < 0 || i > 65535) {
            throw new IllegalArgumentException("value must be between 0 and 65535");
        }
        return new Identifier(new byte[]{(byte) (i >> 8), (byte) i});
    }

    public static Identifier fromBytes(byte[] bArr, int i, int i2, boolean z) {
        if (bArr == null) {
            throw new NullPointerException("bytes == null");
        } else if (i < 0 || i > bArr.length) {
            throw new ArrayIndexOutOfBoundsException("start < 0 || start > bytes.length");
        } else if (i2 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException("end > bytes.length");
        } else if (i > i2) {
            throw new IllegalArgumentException("start > end");
        } else {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
            if (z) {
                reverseArray(copyOfRange);
            }
            return new Identifier(copyOfRange);
        }
    }

    @Deprecated
    public Identifier(Identifier identifier) {
        if (identifier == null) {
            throw new NullPointerException("identifier == null");
        }
        this.mValue = identifier.mValue;
    }

    protected Identifier(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("identifier == null");
        }
        this.mValue = bArr;
    }

    public String toString() {
        if (this.mValue.length == 2) {
            return Integer.toString(toInt());
        }
        if (this.mValue.length == 16) {
            return toUuidString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("0x");
        sb.append(toHexString());
        return sb.toString();
    }

    public int toInt() {
        if (this.mValue.length > 2) {
            throw new UnsupportedOperationException("Only supported for Identifiers with max byte length of 2");
        }
        int i = 0;
        for (int i2 = 0; i2 < this.mValue.length; i2++) {
            i |= (this.mValue[i2] & 255) << (((this.mValue.length - i2) - 1) * 8);
        }
        return i;
    }

    public byte[] toByteArrayOfSpecifiedEndianness(boolean z) {
        byte[] copyOf = Arrays.copyOf(this.mValue, this.mValue.length);
        if (!z) {
            reverseArray(copyOf);
        }
        return copyOf;
    }

    private static void reverseArray(byte[] bArr) {
        for (int i = 0; i < bArr.length / 2; i++) {
            byte b = bArr[i];
            bArr[i] = bArr[(bArr.length - i) - 1];
            bArr[(bArr.length - i) - 1] = b;
        }
    }

    public int getByteCount() {
        return this.mValue.length;
    }

    public int hashCode() {
        return Arrays.hashCode(this.mValue);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Identifier)) {
            return false;
        }
        return Arrays.equals(this.mValue, ((Identifier) obj).mValue);
    }

    private static String toHexString(byte[] bArr, int i, int i2) {
        char[] cArr = new char[(i2 * 2)];
        for (int i3 = 0; i3 < i2; i3++) {
            byte b = bArr[i + i3] & 255;
            int i4 = i3 * 2;
            cArr[i4] = hexArray[b >>> 4];
            cArr[i4 + 1] = hexArray[b & 15];
        }
        return new String(cArr);
    }

    public String toHexString() {
        return toHexString(this.mValue, 0, this.mValue.length);
    }

    public String toUuidString() {
        if (this.mValue.length != 16) {
            throw new UnsupportedOperationException("Only available for values with length of 16 bytes");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(toHexString(this.mValue, 0, 4));
        sb.append("-");
        sb.append(toHexString(this.mValue, 4, 2));
        sb.append("-");
        sb.append(toHexString(this.mValue, 6, 2));
        sb.append("-");
        sb.append(toHexString(this.mValue, 8, 2));
        sb.append("-");
        sb.append(toHexString(this.mValue, 10, 6));
        return sb.toString();
    }

    public int compareTo(Identifier identifier) {
        int i = -1;
        if (this.mValue.length != identifier.mValue.length) {
            if (this.mValue.length >= identifier.mValue.length) {
                i = 1;
            }
            return i;
        }
        for (int i2 = 0; i2 < this.mValue.length; i2++) {
            if (this.mValue[i2] != identifier.mValue[i2]) {
                if (this.mValue[i2] >= identifier.mValue[i2]) {
                    i = 1;
                }
                return i;
            }
        }
        return 0;
    }
}
