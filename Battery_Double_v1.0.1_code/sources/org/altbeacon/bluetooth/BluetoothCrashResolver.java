package org.altbeacon.bluetooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.altbeacon.beacon.logging.LogManager;

@TargetApi(5)
public class BluetoothCrashResolver {
    private static final int BLUEDROID_MAX_BLUETOOTH_MAC_COUNT = 1990;
    private static final int BLUEDROID_POST_DISCOVERY_ESTIMATED_BLUETOOTH_MAC_COUNT = 400;
    private static final String DISTINCT_BLUETOOTH_ADDRESSES_FILE = "BluetoothCrashResolverState.txt";
    private static final long MIN_TIME_BETWEEN_STATE_SAVES_MILLIS = 60000;
    private static final boolean PREEMPTIVE_ACTION_ENABLED = true;
    private static final long SUSPICIOUSLY_SHORT_BLUETOOTH_OFF_INTERVAL_MILLIS = 600;
    private static final String TAG = "BluetoothCrashResolver";
    private static final int TIME_TO_LET_DISCOVERY_RUN_MILLIS = 5000;
    private Context context = null;
    private int detectedCrashCount = 0;
    /* access modifiers changed from: private */
    public boolean discoveryStartConfirmed = false;
    private final Set<String> distinctBluetoothAddresses = new HashSet();
    private long lastBluetoothCrashDetectionTime = 0;
    /* access modifiers changed from: private */
    public long lastBluetoothOffTime = 0;
    /* access modifiers changed from: private */
    public long lastBluetoothTurningOnTime = 0;
    private boolean lastRecoverySucceeded = false;
    private long lastStateSaveTime = 0;
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.bluetooth.adapter.action.DISCOVERY_FINISHED")) {
                if (BluetoothCrashResolver.this.recoveryInProgress) {
                    LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth discovery finished", new Object[0]);
                    BluetoothCrashResolver.this.finishRecovery();
                } else {
                    LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth discovery finished (external)", new Object[0]);
                }
            }
            if (action.equals("android.bluetooth.adapter.action.DISCOVERY_STARTED")) {
                if (BluetoothCrashResolver.this.recoveryInProgress) {
                    BluetoothCrashResolver.this.discoveryStartConfirmed = BluetoothCrashResolver.PREEMPTIVE_ACTION_ENABLED;
                    LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth discovery started", new Object[0]);
                } else {
                    LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth discovery started (external)", new Object[0]);
                }
            }
            if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)) {
                    case Integer.MIN_VALUE:
                        LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth state is ERROR", new Object[0]);
                        return;
                    case 10:
                        LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth state is OFF", new Object[0]);
                        BluetoothCrashResolver.this.lastBluetoothOffTime = System.currentTimeMillis();
                        return;
                    case 11:
                        BluetoothCrashResolver.this.lastBluetoothTurningOnTime = new Date().getTime();
                        LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth state is TURNING_ON", new Object[0]);
                        return;
                    case 12:
                        LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth state is ON", new Object[0]);
                        LogManager.d(BluetoothCrashResolver.TAG, "Bluetooth was turned off for %s milliseconds", Long.valueOf(BluetoothCrashResolver.this.lastBluetoothTurningOnTime - BluetoothCrashResolver.this.lastBluetoothOffTime));
                        if (BluetoothCrashResolver.this.lastBluetoothTurningOnTime - BluetoothCrashResolver.this.lastBluetoothOffTime < BluetoothCrashResolver.SUSPICIOUSLY_SHORT_BLUETOOTH_OFF_INTERVAL_MILLIS) {
                            BluetoothCrashResolver.this.crashDetected();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private int recoveryAttemptCount = 0;
    /* access modifiers changed from: private */
    public boolean recoveryInProgress = false;
    private UpdateNotifier updateNotifier;

    public interface UpdateNotifier {
        void dataUpdated();
    }

    public BluetoothCrashResolver(Context context2) {
        this.context = context2.getApplicationContext();
        LogManager.d(TAG, "constructed", new Object[0]);
        loadState();
    }

    public void start() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        filter.addAction("android.bluetooth.adapter.action.DISCOVERY_STARTED");
        filter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
        this.context.registerReceiver(this.receiver, filter);
        LogManager.d(TAG, "started listening for BluetoothAdapter events", new Object[0]);
    }

    public void stop() {
        this.context.unregisterReceiver(this.receiver);
        LogManager.d(TAG, "stopped listening for BluetoothAdapter events", new Object[0]);
        saveState();
    }

    @Deprecated
    public void enableDebug() {
    }

    @Deprecated
    public void disableDebug() {
    }

    @TargetApi(18)
    public void notifyScannedDevice(BluetoothDevice device, LeScanCallback scanner) {
        int oldSize = this.distinctBluetoothAddresses.size();
        synchronized (this.distinctBluetoothAddresses) {
            this.distinctBluetoothAddresses.add(device.getAddress());
        }
        int newSize = this.distinctBluetoothAddresses.size();
        if (oldSize != newSize && newSize % 100 == 0) {
            LogManager.d(TAG, "Distinct bluetooth devices seen: %s", Integer.valueOf(this.distinctBluetoothAddresses.size()));
        }
        if (this.distinctBluetoothAddresses.size() > getCrashRiskDeviceCount() && !this.recoveryInProgress) {
            LogManager.w(TAG, "Large number of bluetooth devices detected: %s Proactively attempting to clear out address list to prevent a crash", Integer.valueOf(this.distinctBluetoothAddresses.size()));
            LogManager.w(TAG, "Stopping LE Scan", new Object[0]);
            BluetoothAdapter.getDefaultAdapter().stopLeScan(scanner);
            startRecovery();
            processStateChange();
        }
    }

    public void crashDetected() {
        if (VERSION.SDK_INT < 18) {
            LogManager.d(TAG, "Ignoring crashes before SDK 18, because BLE is unsupported.", new Object[0]);
            return;
        }
        LogManager.w(TAG, "BluetoothService crash detected", new Object[0]);
        if (this.distinctBluetoothAddresses.size() > 0) {
            LogManager.d(TAG, "Distinct bluetooth devices seen at crash: %s", Integer.valueOf(this.distinctBluetoothAddresses.size()));
        }
        this.lastBluetoothCrashDetectionTime = new Date().getTime();
        this.detectedCrashCount++;
        if (this.recoveryInProgress) {
            LogManager.d(TAG, "Ignoring bluetooth crash because recovery is already in progress.", new Object[0]);
        } else {
            startRecovery();
        }
        processStateChange();
    }

    public long getLastBluetoothCrashDetectionTime() {
        return this.lastBluetoothCrashDetectionTime;
    }

    public int getDetectedCrashCount() {
        return this.detectedCrashCount;
    }

    public int getRecoveryAttemptCount() {
        return this.recoveryAttemptCount;
    }

    public boolean isLastRecoverySucceeded() {
        return this.lastRecoverySucceeded;
    }

    public boolean isRecoveryInProgress() {
        return this.recoveryInProgress;
    }

    public void setUpdateNotifier(UpdateNotifier updateNotifier2) {
        this.updateNotifier = updateNotifier2;
    }

    public void forceFlush() {
        startRecovery();
        processStateChange();
    }

    private int getCrashRiskDeviceCount() {
        return 1590;
    }

    private void processStateChange() {
        if (this.updateNotifier != null) {
            this.updateNotifier.dataUpdated();
        }
        if (System.currentTimeMillis() - this.lastStateSaveTime > MIN_TIME_BETWEEN_STATE_SAVES_MILLIS) {
            saveState();
        }
    }

    @TargetApi(17)
    private void startRecovery() {
        this.recoveryAttemptCount++;
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        LogManager.d(TAG, "about to check if discovery is active", new Object[0]);
        if (!adapter.isDiscovering()) {
            LogManager.w(TAG, "Recovery attempt started", new Object[0]);
            this.recoveryInProgress = PREEMPTIVE_ACTION_ENABLED;
            this.discoveryStartConfirmed = false;
            LogManager.d(TAG, "about to command discovery", new Object[0]);
            if (!adapter.startDiscovery()) {
                LogManager.w(TAG, "Can't start discovery.  Is bluetooth turned on?", new Object[0]);
            }
            LogManager.d(TAG, "startDiscovery commanded.  isDiscovering()=%s", Boolean.valueOf(adapter.isDiscovering()));
            LogManager.d(TAG, "We will be cancelling this discovery in %s milliseconds.", Integer.valueOf(TIME_TO_LET_DISCOVERY_RUN_MILLIS));
            cancelDiscovery();
            return;
        }
        LogManager.w(TAG, "Already discovering.  Recovery attempt abandoned.", new Object[0]);
    }

    /* access modifiers changed from: private */
    public void finishRecovery() {
        LogManager.w(TAG, "Recovery attempt finished", new Object[0]);
        synchronized (this.distinctBluetoothAddresses) {
            this.distinctBluetoothAddresses.clear();
        }
        this.recoveryInProgress = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cb A[SYNTHETIC, Splitter:B:39:0x00cb] */
    private void saveState() {
        OutputStreamWriter writer = null;
        this.lastStateSaveTime = new Date().getTime();
        try {
            OutputStreamWriter writer2 = new OutputStreamWriter(this.context.openFileOutput(DISTINCT_BLUETOOTH_ADDRESSES_FILE, 0));
            try {
                writer2.write(this.lastBluetoothCrashDetectionTime + "\n");
                writer2.write(this.detectedCrashCount + "\n");
                writer2.write(this.recoveryAttemptCount + "\n");
                writer2.write(this.lastRecoverySucceeded ? "1\n" : "0\n");
                synchronized (this.distinctBluetoothAddresses) {
                    for (String mac : this.distinctBluetoothAddresses) {
                        writer2.write(mac);
                        writer2.write("\n");
                    }
                }
                if (writer2 != null) {
                    try {
                        writer2.close();
                        OutputStreamWriter outputStreamWriter = writer2;
                    } catch (IOException e) {
                        OutputStreamWriter outputStreamWriter2 = writer2;
                    }
                }
            } catch (IOException e2) {
                writer = writer2;
            } catch (Throwable th) {
                th = th;
                writer = writer2;
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        } catch (IOException e4) {
            try {
                LogManager.w(TAG, "Can't write macs to %s", DISTINCT_BLUETOOTH_ADDRESSES_FILE);
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e5) {
                    }
                }
                LogManager.d(TAG, "Wrote %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
            } catch (Throwable th2) {
                th = th2;
                if (writer != null) {
                }
                throw th;
            }
        }
        LogManager.d(TAG, "Wrote %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x006c A[SYNTHETIC, Splitter:B:27:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a1 A[SYNTHETIC, Splitter:B:42:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00aa A[SYNTHETIC, Splitter:B:47:0x00aa] */
    private void loadState() {
        BufferedReader reader = null;
        try {
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(this.context.openFileInput(DISTINCT_BLUETOOTH_ADDRESSES_FILE)));
            try {
                String line = reader2.readLine();
                if (line != null) {
                    this.lastBluetoothCrashDetectionTime = Long.parseLong(line);
                }
                String line2 = reader2.readLine();
                if (line2 != null) {
                    this.detectedCrashCount = Integer.parseInt(line2);
                }
                String line3 = reader2.readLine();
                if (line3 != null) {
                    this.recoveryAttemptCount = Integer.parseInt(line3);
                }
                String line4 = reader2.readLine();
                if (line4 != null) {
                    this.lastRecoverySucceeded = false;
                    if (line4.equals("1")) {
                        this.lastRecoverySucceeded = PREEMPTIVE_ACTION_ENABLED;
                    }
                }
                while (true) {
                    String mac = reader2.readLine();
                    if (mac == null) {
                        break;
                    }
                    this.distinctBluetoothAddresses.add(mac);
                }
                if (reader2 != null) {
                    try {
                        reader2.close();
                        BufferedReader bufferedReader = reader2;
                    } catch (IOException e) {
                        BufferedReader bufferedReader2 = reader2;
                    }
                }
            } catch (IOException e2) {
                reader = reader2;
                try {
                    LogManager.w(TAG, "Can't read macs from %s", DISTINCT_BLUETOOTH_ADDRESSES_FILE);
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e3) {
                        }
                    }
                    LogManager.d(TAG, "Read %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
                } catch (Throwable th) {
                    th = th;
                    if (reader != null) {
                    }
                    throw th;
                }
            } catch (NumberFormatException e4) {
                reader = reader2;
                LogManager.w(TAG, "Can't parse file %s", DISTINCT_BLUETOOTH_ADDRESSES_FILE);
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e5) {
                    }
                }
                LogManager.d(TAG, "Read %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
            } catch (Throwable th2) {
                th = th2;
                reader = reader2;
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e6) {
                    }
                }
                throw th;
            }
        } catch (IOException e7) {
            LogManager.w(TAG, "Can't read macs from %s", DISTINCT_BLUETOOTH_ADDRESSES_FILE);
            if (reader != null) {
            }
            LogManager.d(TAG, "Read %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
        } catch (NumberFormatException e8) {
            LogManager.w(TAG, "Can't parse file %s", DISTINCT_BLUETOOTH_ADDRESSES_FILE);
            if (reader != null) {
            }
            LogManager.d(TAG, "Read %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
        }
        LogManager.d(TAG, "Read %s bluetooth addresses", Integer.valueOf(this.distinctBluetoothAddresses.size()));
    }

    private void cancelDiscovery() {
        try {
            Thread.sleep(5000);
            if (!this.discoveryStartConfirmed) {
                LogManager.w(TAG, "BluetoothAdapter.ACTION_DISCOVERY_STARTED never received.  Recovery may fail.", new Object[0]);
            }
            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (adapter.isDiscovering()) {
                LogManager.d(TAG, "Cancelling discovery", new Object[0]);
                adapter.cancelDiscovery();
                return;
            }
            LogManager.d(TAG, "Discovery not running.  Won't cancel it", new Object[0]);
        } catch (InterruptedException e) {
            LogManager.d(TAG, "DiscoveryCanceller sleep interrupted.", new Object[0]);
        }
    }
}
