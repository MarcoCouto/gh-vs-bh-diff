package org.altbeacon.beacon.distance;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.altbeacon.beacon.logging.LogManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ModelSpecificDistanceCalculator implements DistanceCalculator {
    private static final String CONFIG_FILE = "model-distance-calculations.json";
    private static final String TAG = "ModelSpecificDistanceCalculator";
    private Context mContext;
    private AndroidModel mDefaultModel;
    /* access modifiers changed from: private */
    public DistanceCalculator mDistanceCalculator;
    private AndroidModel mModel;
    Map<AndroidModel, DistanceCalculator> mModelMap;
    /* access modifiers changed from: private */
    public String mRemoteUpdateUrlString;
    /* access modifiers changed from: private */
    public AndroidModel mRequestedModel;

    public ModelSpecificDistanceCalculator(Context context, String remoteUpdateUrlString) {
        this(context, remoteUpdateUrlString, AndroidModel.forThisDevice());
    }

    public ModelSpecificDistanceCalculator(Context context, String remoteUpdateUrlString, AndroidModel model) {
        this.mRemoteUpdateUrlString = null;
        this.mRequestedModel = model;
        this.mRemoteUpdateUrlString = remoteUpdateUrlString;
        this.mContext = context;
        loadModelMap();
        this.mDistanceCalculator = findCalculatorForModel(model);
    }

    public AndroidModel getModel() {
        return this.mModel;
    }

    public AndroidModel getRequestedModel() {
        return this.mRequestedModel;
    }

    public double calculateDistance(int txPower, double rssi) {
        if (this.mDistanceCalculator != null) {
            return this.mDistanceCalculator.calculateDistance(txPower, rssi);
        }
        LogManager.w(TAG, "distance calculator has not been set", new Object[0]);
        return -1.0d;
    }

    /* access modifiers changed from: private */
    public DistanceCalculator findCalculatorForModel(AndroidModel model) {
        LogManager.d(TAG, "Finding best distance calculator for %s, %s, %s, %s", model.getVersion(), model.getBuildNumber(), model.getModel(), model.getManufacturer());
        if (this.mModelMap == null) {
            LogManager.d(TAG, "Cannot get distance calculator because modelMap was never initialized", new Object[0]);
            return null;
        }
        int highestScore = 0;
        AndroidModel bestMatchingModel = null;
        for (AndroidModel candidateModel : this.mModelMap.keySet()) {
            if (candidateModel.matchScore(model) > highestScore) {
                highestScore = candidateModel.matchScore(model);
                bestMatchingModel = candidateModel;
            }
        }
        if (bestMatchingModel != null) {
            LogManager.d(TAG, "found a match with score %s", Integer.valueOf(highestScore));
            LogManager.d(TAG, "Finding best distance calculator for %s, %s, %s, %s", bestMatchingModel.getVersion(), bestMatchingModel.getBuildNumber(), bestMatchingModel.getModel(), bestMatchingModel.getManufacturer());
            this.mModel = bestMatchingModel;
        } else {
            this.mModel = this.mDefaultModel;
            LogManager.w(TAG, "Cannot find match for this device.  Using default", new Object[0]);
        }
        return (DistanceCalculator) this.mModelMap.get(this.mModel);
    }

    private void loadModelMap() {
        boolean mapLoaded = false;
        if (this.mRemoteUpdateUrlString != null) {
            mapLoaded = loadModelMapFromFile();
            if (!mapLoaded) {
                requestModelMapFromWeb();
            }
        }
        if (!mapLoaded) {
            loadDefaultModelMap();
        }
        this.mDistanceCalculator = findCalculatorForModel(this.mRequestedModel);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a A[SYNTHETIC, Splitter:B:12:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003f A[SYNTHETIC, Splitter:B:15:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0068 A[SYNTHETIC, Splitter:B:32:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006d A[SYNTHETIC, Splitter:B:35:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0075 A[SYNTHETIC, Splitter:B:40:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007a A[SYNTHETIC, Splitter:B:43:0x007a] */
    public boolean loadModelMapFromFile() {
        File file = new File(this.mContext.getFilesDir(), CONFIG_FILE);
        FileInputStream inputStream = null;
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            FileInputStream inputStream2 = new FileInputStream(file);
            try {
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(inputStream2));
                while (true) {
                    try {
                        String line = reader2.readLine();
                        if (line == null) {
                            break;
                        }
                        sb.append(line).append("\n");
                    } catch (FileNotFoundException e) {
                        reader = reader2;
                        inputStream = inputStream2;
                        if (reader != null) {
                        }
                        if (inputStream != null) {
                        }
                        return false;
                    } catch (IOException e2) {
                        e = e2;
                        reader = reader2;
                        inputStream = inputStream2;
                        try {
                            LogManager.e(e, TAG, "Cannot open distance model file %s", file);
                            if (reader != null) {
                            }
                            if (inputStream != null) {
                            }
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            if (reader != null) {
                                try {
                                    reader.close();
                                } catch (Exception e3) {
                                }
                            }
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (Exception e4) {
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        reader = reader2;
                        inputStream = inputStream2;
                        if (reader != null) {
                        }
                        if (inputStream != null) {
                        }
                        throw th;
                    }
                }
                if (reader2 != null) {
                    try {
                        reader2.close();
                    } catch (Exception e5) {
                    }
                }
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (Exception e6) {
                    }
                }
                try {
                    buildModelMap(sb.toString());
                    BufferedReader bufferedReader = reader2;
                    FileInputStream fileInputStream = inputStream2;
                    return true;
                } catch (JSONException e7) {
                    LogManager.e(TAG, "Cannot update distance models from online database at %s with JSON", e7, this.mRemoteUpdateUrlString, sb.toString());
                    BufferedReader bufferedReader2 = reader2;
                    FileInputStream fileInputStream2 = inputStream2;
                    return false;
                }
            } catch (FileNotFoundException e8) {
                inputStream = inputStream2;
                if (reader != null) {
                }
                if (inputStream != null) {
                }
                return false;
            } catch (IOException e9) {
                e = e9;
                inputStream = inputStream2;
                LogManager.e(e, TAG, "Cannot open distance model file %s", file);
                if (reader != null) {
                }
                if (inputStream != null) {
                }
                return false;
            } catch (Throwable th3) {
                th = th3;
                inputStream = inputStream2;
                if (reader != null) {
                }
                if (inputStream != null) {
                }
                throw th;
            }
        } catch (FileNotFoundException e10) {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e11) {
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e12) {
                }
            }
            return false;
        } catch (IOException e13) {
            e = e13;
            LogManager.e(e, TAG, "Cannot open distance model file %s", file);
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e14) {
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e15) {
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean saveJson(String jsonString) {
        FileOutputStream outputStream = null;
        try {
            outputStream = this.mContext.openFileOutput(CONFIG_FILE, 0);
            outputStream.write(jsonString.getBytes());
            outputStream.close();
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                }
            }
            LogManager.i(TAG, "Successfully saved new distance model file", new Object[0]);
            return true;
        } catch (Exception e2) {
            LogManager.w(e2, TAG, "Cannot write updated distance model to local storage", new Object[0]);
            if (outputStream == null) {
                return false;
            }
            try {
                outputStream.close();
                return false;
            } catch (Exception e3) {
                return false;
            }
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e4) {
                }
            }
        }
    }

    @TargetApi(11)
    private void requestModelMapFromWeb() {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            LogManager.w(TAG, "App has no android.permission.INTERNET permission.  Cannot check for distance model updates", new Object[0]);
        } else {
            new ModelSpecificDistanceUpdater(this.mContext, this.mRemoteUpdateUrlString, new CompletionHandler() {
                public void onComplete(String body, Exception ex, int code) {
                    if (ex != null) {
                        LogManager.w(ModelSpecificDistanceCalculator.TAG, "Cannot updated distance models from online database at %s", ex, ModelSpecificDistanceCalculator.this.mRemoteUpdateUrlString);
                    } else if (code != 200) {
                        LogManager.w(ModelSpecificDistanceCalculator.TAG, "Cannot updated distance models from online database at %s due to HTTP status code %s", ModelSpecificDistanceCalculator.this.mRemoteUpdateUrlString, Integer.valueOf(code));
                    } else {
                        LogManager.d(ModelSpecificDistanceCalculator.TAG, "Successfully downloaded distance models from online database", new Object[0]);
                        try {
                            ModelSpecificDistanceCalculator.this.buildModelMap(body);
                            if (ModelSpecificDistanceCalculator.this.saveJson(body)) {
                                ModelSpecificDistanceCalculator.this.loadModelMapFromFile();
                                ModelSpecificDistanceCalculator.this.mDistanceCalculator = ModelSpecificDistanceCalculator.this.findCalculatorForModel(ModelSpecificDistanceCalculator.this.mRequestedModel);
                                LogManager.i(ModelSpecificDistanceCalculator.TAG, "Successfully updated distance model with latest from online database", new Object[0]);
                            }
                        } catch (JSONException e) {
                            LogManager.w(e, ModelSpecificDistanceCalculator.TAG, "Cannot parse json from downloaded distance model", new Object[0]);
                        }
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public void buildModelMap(String jsonString) throws JSONException {
        this.mModelMap = new HashMap();
        JSONObject jSONObject = new JSONObject(jsonString);
        JSONArray array = jSONObject.getJSONArray("models");
        for (int i = 0; i < array.length(); i++) {
            JSONObject modelObject = array.getJSONObject(i);
            boolean defaultFlag = false;
            if (modelObject.has("default")) {
                defaultFlag = modelObject.getBoolean("default");
            }
            Double coefficient1 = Double.valueOf(modelObject.getDouble("coefficient1"));
            Double coefficient2 = Double.valueOf(modelObject.getDouble("coefficient2"));
            Double coefficient3 = Double.valueOf(modelObject.getDouble("coefficient3"));
            String version = modelObject.getString("version");
            String buildNumber = modelObject.getString("build_number");
            String model = modelObject.getString("model");
            String manufacturer = modelObject.getString("manufacturer");
            CurveFittedDistanceCalculator distanceCalculator = new CurveFittedDistanceCalculator(coefficient1.doubleValue(), coefficient2.doubleValue(), coefficient3.doubleValue());
            AndroidModel androidModel = new AndroidModel(version, buildNumber, model, manufacturer);
            this.mModelMap.put(androidModel, distanceCalculator);
            if (defaultFlag) {
                this.mDefaultModel = androidModel;
            }
        }
    }

    private void loadDefaultModelMap() {
        this.mModelMap = new HashMap();
        try {
            buildModelMap(stringFromFilePath(CONFIG_FILE));
        } catch (Exception e) {
            LogManager.e(e, TAG, "Cannot build model distance calculations", new Object[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0064  */
    private String stringFromFilePath(String path) throws IOException {
        InputStream stream = null;
        BufferedReader bufferedReader = null;
        StringBuilder inputStringBuilder = new StringBuilder();
        try {
            stream = ModelSpecificDistanceCalculator.class.getResourceAsStream("/" + path);
            if (stream == null) {
                stream = getClass().getClassLoader().getResourceAsStream("/" + path);
            }
            if (stream == null) {
                throw new RuntimeException("Cannot load resource at " + path);
            }
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            try {
                for (String line = bufferedReader2.readLine(); line != null; line = bufferedReader2.readLine()) {
                    inputStringBuilder.append(line);
                    inputStringBuilder.append(10);
                }
                if (bufferedReader2 != null) {
                    bufferedReader2.close();
                }
                if (stream != null) {
                    stream.close();
                }
                return inputStringBuilder.toString();
            } catch (Throwable th) {
                th = th;
                bufferedReader = bufferedReader2;
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (stream != null) {
                    stream.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (bufferedReader != null) {
            }
            if (stream != null) {
            }
            throw th;
        }
    }
}
