package org.altbeacon.beacon.distance;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import org.altbeacon.beacon.logging.LogManager;

public class CurveFittedDistanceCalculator implements DistanceCalculator {
    public static final String TAG = "CurveFittedDistanceCalculator";
    private double mCoefficient1;
    private double mCoefficient2;
    private double mCoefficient3;

    public CurveFittedDistanceCalculator(double coefficient1, double coefficient2, double coefficient3) {
        this.mCoefficient1 = coefficient1;
        this.mCoefficient2 = coefficient2;
        this.mCoefficient3 = coefficient3;
    }

    public double calculateDistance(int txPower, double rssi) {
        double distance;
        if (rssi == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            return -1.0d;
        }
        LogManager.d(TAG, "calculating distance based on mRssi of %s and txPower of %s", Double.valueOf(rssi), Integer.valueOf(txPower));
        double ratio = (1.0d * rssi) / ((double) txPower);
        if (ratio < 1.0d) {
            distance = Math.pow(ratio, 10.0d);
        } else {
            distance = (this.mCoefficient1 * Math.pow(ratio, this.mCoefficient2)) + this.mCoefficient3;
        }
        LogManager.d(TAG, "avg mRssi: %s distance: %s", Double.valueOf(rssi), Double.valueOf(distance));
        return distance;
    }
}
