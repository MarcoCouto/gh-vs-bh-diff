package org.altbeacon.beacon;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.logging.Loggers;
import org.altbeacon.beacon.service.BeaconService;
import org.altbeacon.beacon.service.StartRMData;
import org.altbeacon.beacon.simulator.BeaconSimulator;

@TargetApi(4)
public class BeaconManager {
    public static final long DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD = 300000;
    public static final long DEFAULT_BACKGROUND_SCAN_PERIOD = 10000;
    public static final long DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD = 0;
    public static final long DEFAULT_FOREGROUND_SCAN_PERIOD = 1100;
    private static final String TAG = "BeaconManager";
    protected static BeaconSimulator beaconSimulator;
    protected static BeaconManager client = null;
    protected static String distanceModelUpdateUrl = "http://data.altbeacon.org/android-distance.json";
    private static boolean sAndroidLScanningDisabled = false;
    private static boolean sManifestCheckingDisabled = false;
    private long backgroundBetweenScanPeriod = DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD;
    private long backgroundScanPeriod = DEFAULT_BACKGROUND_SCAN_PERIOD;
    private final ArrayList<BeaconParser> beaconParsers = new ArrayList<>();
    private ServiceConnection beaconServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            LogManager.d(BeaconManager.TAG, "we have a connection to the service now", new Object[0]);
            BeaconManager.this.serviceMessenger = new Messenger(service);
            synchronized (BeaconManager.this.consumers) {
                for (Entry<BeaconConsumer, ConsumerInfo> entry : BeaconManager.this.consumers.entrySet()) {
                    if (!((ConsumerInfo) entry.getValue()).isConnected) {
                        ((BeaconConsumer) entry.getKey()).onBeaconServiceConnect();
                        ((ConsumerInfo) entry.getValue()).isConnected = true;
                    }
                }
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            LogManager.e(BeaconManager.TAG, "onServiceDisconnected", new Object[0]);
            BeaconManager.this.serviceMessenger = null;
        }
    };
    /* access modifiers changed from: private */
    public final ConcurrentMap<BeaconConsumer, ConsumerInfo> consumers = new ConcurrentHashMap();
    protected RangeNotifier dataRequestNotifier = null;
    private long foregroundBetweenScanPeriod = 0;
    private long foregroundScanPeriod = DEFAULT_FOREGROUND_SCAN_PERIOD;
    private boolean mBackgroundMode = false;
    private boolean mBackgroundModeUninitialized = true;
    private Context mContext;
    protected MonitorNotifier monitorNotifier = null;
    private final ArrayList<Region> monitoredRegions = new ArrayList<>();
    protected RangeNotifier rangeNotifier = null;
    private final ArrayList<Region> rangedRegions = new ArrayList<>();
    /* access modifiers changed from: private */
    public Messenger serviceMessenger = null;

    private class ConsumerInfo {
        public boolean isConnected;

        private ConsumerInfo() {
            this.isConnected = false;
        }
    }

    public class ServiceNotDeclaredException extends RuntimeException {
        public ServiceNotDeclaredException() {
            super("The BeaconService is not properly declared in AndroidManifest.xml.  If using Eclipse, please verify that your project.properties has manifestmerger.enabled=true");
        }
    }

    @Deprecated
    public static void setDebug(boolean debug) {
        if (debug) {
            LogManager.setLogger(Loggers.verboseLogger());
            LogManager.setVerboseLoggingEnabled(true);
            return;
        }
        LogManager.setLogger(Loggers.empty());
        LogManager.setVerboseLoggingEnabled(false);
    }

    public void setForegroundScanPeriod(long p) {
        this.foregroundScanPeriod = p;
    }

    public void setForegroundBetweenScanPeriod(long p) {
        this.foregroundBetweenScanPeriod = p;
    }

    public void setBackgroundScanPeriod(long p) {
        this.backgroundScanPeriod = p;
    }

    public void setBackgroundBetweenScanPeriod(long p) {
        this.backgroundBetweenScanPeriod = p;
    }

    public static BeaconManager getInstanceForApplication(Context context) {
        if (client == null) {
            LogManager.d(TAG, "BeaconManager instance creation", new Object[0]);
            client = new BeaconManager(context);
        }
        return client;
    }

    public List<BeaconParser> getBeaconParsers() {
        if (isAnyConsumerBound()) {
            return Collections.unmodifiableList(this.beaconParsers);
        }
        return this.beaconParsers;
    }

    protected BeaconManager(Context context) {
        this.mContext = context;
        if (!sManifestCheckingDisabled) {
            verifyServiceDeclaration();
        }
        this.beaconParsers.add(new AltBeaconParser());
    }

    @TargetApi(18)
    public boolean checkAvailability() throws BleNotAvailableException {
        if (VERSION.SDK_INT < 18) {
            throw new BleNotAvailableException("Bluetooth LE not supported by this device");
        } else if (!this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le")) {
            throw new BleNotAvailableException("Bluetooth LE not supported by this device");
        } else if (((BluetoothManager) this.mContext.getSystemService("bluetooth")).getAdapter().isEnabled()) {
            return true;
        } else {
            return false;
        }
    }

    public void bind(BeaconConsumer consumer) {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
            return;
        }
        synchronized (this.consumers) {
            if (((ConsumerInfo) this.consumers.putIfAbsent(consumer, new ConsumerInfo())) != null) {
                LogManager.d(TAG, "This consumer is already bound", new Object[0]);
            } else {
                LogManager.d(TAG, "This consumer is not bound.  binding: %s", consumer);
                consumer.bindService(new Intent(consumer.getApplicationContext(), BeaconService.class), this.beaconServiceConnection, 1);
                LogManager.d(TAG, "consumer count is now: %s", Integer.valueOf(this.consumers.size()));
            }
        }
    }

    public void unbind(BeaconConsumer consumer) {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
            return;
        }
        synchronized (this.consumers) {
            if (this.consumers.containsKey(consumer)) {
                LogManager.d(TAG, "Unbinding", new Object[0]);
                consumer.unbindService(this.beaconServiceConnection);
                this.consumers.remove(consumer);
                if (this.consumers.size() == 0) {
                    this.serviceMessenger = null;
                }
            } else {
                LogManager.d(TAG, "This consumer is not bound to: %s", consumer);
                LogManager.d(TAG, "Bound consumers: ", new Object[0]);
                for (Entry<BeaconConsumer, ConsumerInfo> consumerEntry : this.consumers.entrySet()) {
                    LogManager.d(TAG, String.valueOf(consumerEntry.getValue()), new Object[0]);
                }
            }
        }
    }

    public boolean isBound(BeaconConsumer consumer) {
        boolean z;
        synchronized (this.consumers) {
            if (consumer != null) {
                if (!(this.consumers.get(consumer) == null || this.serviceMessenger == null)) {
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }

    public boolean isAnyConsumerBound() {
        boolean z;
        synchronized (this.consumers) {
            z = this.consumers.size() > 0 && this.serviceMessenger != null;
        }
        return z;
    }

    public void setBackgroundMode(boolean backgroundMode) {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
        }
        this.mBackgroundModeUninitialized = false;
        if (backgroundMode != this.mBackgroundMode) {
            this.mBackgroundMode = backgroundMode;
            try {
                updateScanPeriods();
            } catch (RemoteException e) {
                LogManager.e(TAG, "Cannot contact service to set scan periods", new Object[0]);
            }
        }
    }

    public boolean isBackgroundModeUninitialized() {
        return this.mBackgroundModeUninitialized;
    }

    public void setRangeNotifier(RangeNotifier notifier) {
        this.rangeNotifier = notifier;
    }

    public void setMonitorNotifier(MonitorNotifier notifier) {
        this.monitorNotifier = notifier;
    }

    @TargetApi(18)
    public void startRangingBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message msg = Message.obtain(null, 2, 0, 0);
            msg.obj = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            this.serviceMessenger.send(msg);
            synchronized (this.rangedRegions) {
                this.rangedRegions.add(region);
            }
        }
    }

    @TargetApi(18)
    public void stopRangingBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to SDK 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message msg = Message.obtain(null, 3, 0, 0);
            msg.obj = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            this.serviceMessenger.send(msg);
            synchronized (this.rangedRegions) {
                Region regionToRemove = null;
                Iterator i$ = this.rangedRegions.iterator();
                while (i$.hasNext()) {
                    Region rangedRegion = (Region) i$.next();
                    if (region.getUniqueId().equals(rangedRegion.getUniqueId())) {
                        regionToRemove = rangedRegion;
                    }
                }
                this.rangedRegions.remove(regionToRemove);
            }
        }
    }

    @TargetApi(18)
    public void startMonitoringBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message msg = Message.obtain(null, 4, 0, 0);
            msg.obj = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            this.serviceMessenger.send(msg);
            synchronized (this.monitoredRegions) {
                this.monitoredRegions.add(region);
            }
        }
    }

    @TargetApi(18)
    public void stopMonitoringBeaconsInRegion(Region region) throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message msg = Message.obtain(null, 5, 0, 0);
            msg.obj = new StartRMData(region, callbackPackageName(), getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            this.serviceMessenger.send(msg);
            synchronized (this.monitoredRegions) {
                Region regionToRemove = null;
                Iterator i$ = this.monitoredRegions.iterator();
                while (i$.hasNext()) {
                    Region monitoredRegion = (Region) i$.next();
                    if (region.getUniqueId().equals(monitoredRegion.getUniqueId())) {
                        regionToRemove = monitoredRegion;
                    }
                }
                this.monitoredRegions.remove(regionToRemove);
            }
        }
    }

    @TargetApi(18)
    public void updateScanPeriods() throws RemoteException {
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not supported prior to API 18.  Method invocation will be ignored", new Object[0]);
        } else if (this.serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        } else {
            Message msg = Message.obtain(null, 6, 0, 0);
            LogManager.d(TAG, "updating background flag to %s", Boolean.valueOf(this.mBackgroundMode));
            LogManager.d(TAG, "updating scan period to %s, %s", Long.valueOf(getScanPeriod()), Long.valueOf(getBetweenScanPeriod()));
            msg.obj = new StartRMData(getScanPeriod(), getBetweenScanPeriod(), this.mBackgroundMode);
            this.serviceMessenger.send(msg);
        }
    }

    private String callbackPackageName() {
        String packageName = this.mContext.getPackageName();
        LogManager.d(TAG, "callback packageName: %s", packageName);
        return packageName;
    }

    public MonitorNotifier getMonitoringNotifier() {
        return this.monitorNotifier;
    }

    public RangeNotifier getRangingNotifier() {
        return this.rangeNotifier;
    }

    public Collection<Region> getMonitoredRegions() {
        ArrayList arrayList;
        synchronized (this.monitoredRegions) {
            arrayList = new ArrayList(this.monitoredRegions);
        }
        return arrayList;
    }

    public Collection<Region> getRangedRegions() {
        ArrayList arrayList;
        synchronized (this.rangedRegions) {
            arrayList = new ArrayList(this.rangedRegions);
        }
        return arrayList;
    }

    @Deprecated
    public static void logDebug(String tag, String message) {
        LogManager.d(tag, message, new Object[0]);
    }

    @Deprecated
    public static void logDebug(String tag, String message, Throwable t) {
        LogManager.d(t, tag, message, new Object[0]);
    }

    public static String getDistanceModelUpdateUrl() {
        return distanceModelUpdateUrl;
    }

    public static void setDistanceModelUpdateUrl(String url) {
        distanceModelUpdateUrl = url;
    }

    public static void setBeaconSimulator(BeaconSimulator beaconSimulator2) {
        beaconSimulator = beaconSimulator2;
    }

    public static BeaconSimulator getBeaconSimulator() {
        return beaconSimulator;
    }

    /* access modifiers changed from: protected */
    public void setDataRequestNotifier(RangeNotifier notifier) {
        this.dataRequestNotifier = notifier;
    }

    /* access modifiers changed from: protected */
    public RangeNotifier getDataRequestNotifier() {
        return this.dataRequestNotifier;
    }

    private long getScanPeriod() {
        if (this.mBackgroundMode) {
            return this.backgroundScanPeriod;
        }
        return this.foregroundScanPeriod;
    }

    private long getBetweenScanPeriod() {
        if (this.mBackgroundMode) {
            return this.backgroundBetweenScanPeriod;
        }
        return this.foregroundBetweenScanPeriod;
    }

    private void verifyServiceDeclaration() {
        if (this.mContext.getPackageManager().queryIntentServices(new Intent(this.mContext, BeaconService.class), 65536).size() == 0) {
            throw new ServiceNotDeclaredException();
        }
    }

    public static boolean isAndroidLScanningDisabled() {
        return sAndroidLScanningDisabled;
    }

    public static void setAndroidLScanningDisabled(boolean disabled) {
        sAndroidLScanningDisabled = disabled;
    }

    public static void setsManifestCheckingDisabled(boolean disabled) {
        sManifestCheckingDisabled = disabled;
    }
}
