package org.altbeacon.beacon.startup;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.logging.LogManager;

@TargetApi(4)
public class StartupBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "StartupBroadcastReceiver";

    public void onReceive(Context context, Intent intent) {
        LogManager.d(TAG, "onReceive called in startup broadcast receiver", new Object[0]);
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "Not starting up beacon service because we do not have SDK version 18 (Android 4.3).  We have: %s", Integer.valueOf(VERSION.SDK_INT));
        } else if (!BeaconManager.getInstanceForApplication(context.getApplicationContext()).isAnyConsumerBound()) {
        } else {
            if (intent.getBooleanExtra("wakeup", false)) {
                LogManager.d(TAG, "got wake up intent", new Object[0]);
                return;
            }
            LogManager.d(TAG, "Already started.  Ignoring intent: %s of type: %s", intent, intent.getStringExtra("wakeup"));
        }
    }
}
