package org.altbeacon.beacon.startup;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.logging.LogManager;

public class RegionBootstrap {
    protected static final String TAG = "AppStarter";
    /* access modifiers changed from: private */
    public BootstrapNotifier application;
    private BeaconConsumer beaconConsumer;
    /* access modifiers changed from: private */
    public BeaconManager beaconManager;
    private boolean disabled = false;
    /* access modifiers changed from: private */
    public List<Region> regions;

    private class InternalBeaconConsumer implements BeaconConsumer {
        private InternalBeaconConsumer() {
        }

        public void onBeaconServiceConnect() {
            LogManager.d(RegionBootstrap.TAG, "Activating background region monitoring", new Object[0]);
            RegionBootstrap.this.beaconManager.setMonitorNotifier(RegionBootstrap.this.application);
            try {
                for (Region region : RegionBootstrap.this.regions) {
                    LogManager.d(RegionBootstrap.TAG, "Background region monitoring activated for region %s", region);
                    RegionBootstrap.this.beaconManager.startMonitoringBeaconsInRegion(region);
                    if (RegionBootstrap.this.beaconManager.isBackgroundModeUninitialized()) {
                        RegionBootstrap.this.beaconManager.setBackgroundMode(true);
                    }
                }
            } catch (RemoteException e) {
                LogManager.e(e, RegionBootstrap.TAG, "Can't set up bootstrap regions", new Object[0]);
            }
        }

        public boolean bindService(Intent intent, ServiceConnection conn, int arg2) {
            return RegionBootstrap.this.application.getApplicationContext().bindService(intent, conn, arg2);
        }

        public Context getApplicationContext() {
            return RegionBootstrap.this.application.getApplicationContext();
        }

        public void unbindService(ServiceConnection conn) {
            RegionBootstrap.this.application.getApplicationContext().unbindService(conn);
        }
    }

    public RegionBootstrap(BootstrapNotifier application2, Region region) {
        if (application2.getApplicationContext() == null) {
            throw new NullPointerException("The BootstrapNotifier instance is returning null from its getApplicationContext() method.  Have you implemented this method?");
        }
        this.beaconManager = BeaconManager.getInstanceForApplication(application2.getApplicationContext());
        this.application = application2;
        this.regions = new ArrayList();
        this.regions.add(region);
        this.beaconConsumer = new InternalBeaconConsumer();
        this.beaconManager.bind(this.beaconConsumer);
        LogManager.d(TAG, "Waiting for BeaconService connection", new Object[0]);
    }

    public RegionBootstrap(BootstrapNotifier application2, List<Region> regions2) {
        if (application2.getApplicationContext() == null) {
            throw new NullPointerException("The BootstrapNotifier instance is returning null from its getApplicationContext() method.  Have you implemented this method?");
        }
        this.beaconManager = BeaconManager.getInstanceForApplication(application2.getApplicationContext());
        this.application = application2;
        this.regions = regions2;
        this.beaconConsumer = new InternalBeaconConsumer();
        this.beaconManager.bind(this.beaconConsumer);
        LogManager.d(TAG, "Waiting for BeaconService connection", new Object[0]);
    }

    public void disable() {
        if (!this.disabled) {
            this.disabled = true;
            try {
                for (Region region : this.regions) {
                    this.beaconManager.stopMonitoringBeaconsInRegion(region);
                }
            } catch (RemoteException e) {
                LogManager.e(e, TAG, "Can't stop bootstrap regions", new Object[0]);
            }
            this.beaconManager.unbind(this.beaconConsumer);
        }
    }
}
