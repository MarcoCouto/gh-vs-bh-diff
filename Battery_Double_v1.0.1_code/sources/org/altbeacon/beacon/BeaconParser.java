package org.altbeacon.beacon;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.support.v4.view.InputDeviceCompat;
import com.applovin.sdk.AppLovinTargetingData;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.altbeacon.beacon.logging.LogManager;

public class BeaconParser {
    private static final Pattern D_PATTERN = Pattern.compile("d\\:(\\d+)\\-(\\d+)([bl]?)");
    private static final char[] HEX_ARRAY = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', AppLovinTargetingData.GENDER_FEMALE};
    private static final Pattern I_PATTERN = Pattern.compile("i\\:(\\d+)\\-(\\d+)(l?)");
    private static final Pattern M_PATTERN = Pattern.compile("m\\:(\\d+)-(\\d+)\\=([0-9A-Fa-f]+)");
    private static final Pattern P_PATTERN = Pattern.compile("p\\:(\\d+)\\-(\\d+)");
    private static final Pattern S_PATTERN = Pattern.compile("s\\:(\\d+)-(\\d+)\\=([0-9A-Fa-f]+)");
    private static final String TAG = "BeaconParser";
    protected List<Integer> mDataEndOffsets = new ArrayList();
    protected List<Boolean> mDataLittleEndianFlags = new ArrayList();
    protected List<Integer> mDataStartOffsets = new ArrayList();
    protected int[] mHardwareAssistManufacturers = {76};
    protected List<Integer> mIdentifierEndOffsets = new ArrayList();
    protected List<Boolean> mIdentifierLittleEndianFlags = new ArrayList();
    protected List<Integer> mIdentifierStartOffsets = new ArrayList();
    private Long mMatchingBeaconTypeCode;
    protected Integer mMatchingBeaconTypeCodeEndOffset;
    protected Integer mMatchingBeaconTypeCodeStartOffset;
    protected Integer mPowerEndOffset;
    protected Integer mPowerStartOffset;
    protected Long mServiceUuid;
    protected Integer mServiceUuidEndOffset;
    protected Integer mServiceUuidStartOffset;

    public static class BeaconLayoutException extends RuntimeException {
        public BeaconLayoutException(String s) {
            super(s);
        }
    }

    public BeaconParser setBeaconLayout(String beaconLayout) {
        String[] arr$;
        for (String term : beaconLayout.split(",")) {
            boolean found = false;
            Matcher matcher = I_PATTERN.matcher(term);
            while (matcher.find()) {
                found = true;
                try {
                    int startOffset = Integer.parseInt(matcher.group(1));
                    int endOffset = Integer.parseInt(matcher.group(2));
                    this.mIdentifierLittleEndianFlags.add(Boolean.valueOf(matcher.group(3).equals("l")));
                    this.mIdentifierStartOffsets.add(Integer.valueOf(startOffset));
                    this.mIdentifierEndOffsets.add(Integer.valueOf(endOffset));
                } catch (NumberFormatException e) {
                    throw new BeaconLayoutException("Cannot parse integer byte offset in term: " + term);
                }
            }
            Matcher matcher2 = D_PATTERN.matcher(term);
            while (matcher2.find()) {
                found = true;
                try {
                    int startOffset2 = Integer.parseInt(matcher2.group(1));
                    int endOffset2 = Integer.parseInt(matcher2.group(2));
                    this.mDataLittleEndianFlags.add(Boolean.valueOf(matcher2.group(3).equals("l")));
                    this.mDataStartOffsets.add(Integer.valueOf(startOffset2));
                    this.mDataEndOffsets.add(Integer.valueOf(endOffset2));
                } catch (NumberFormatException e2) {
                    throw new BeaconLayoutException("Cannot parse integer byte offset in term: " + term);
                }
            }
            Matcher matcher3 = P_PATTERN.matcher(term);
            while (matcher3.find()) {
                found = true;
                try {
                    int startOffset3 = Integer.parseInt(matcher3.group(1));
                    int endOffset3 = Integer.parseInt(matcher3.group(2));
                    this.mPowerStartOffset = Integer.valueOf(startOffset3);
                    this.mPowerEndOffset = Integer.valueOf(endOffset3);
                } catch (NumberFormatException e3) {
                    throw new BeaconLayoutException("Cannot parse integer power byte offset in term: " + term);
                }
            }
            Matcher matcher4 = M_PATTERN.matcher(term);
            while (matcher4.find()) {
                found = true;
                try {
                    int startOffset4 = Integer.parseInt(matcher4.group(1));
                    int endOffset4 = Integer.parseInt(matcher4.group(2));
                    this.mMatchingBeaconTypeCodeStartOffset = Integer.valueOf(startOffset4);
                    this.mMatchingBeaconTypeCodeEndOffset = Integer.valueOf(endOffset4);
                    String hexString = matcher4.group(3);
                    try {
                        this.mMatchingBeaconTypeCode = Long.decode("0x" + hexString);
                    } catch (NumberFormatException e4) {
                        throw new BeaconLayoutException("Cannot parse beacon type code: " + hexString + " in term: " + term);
                    }
                } catch (NumberFormatException e5) {
                    throw new BeaconLayoutException("Cannot parse integer byte offset in term: " + term);
                }
            }
            Matcher matcher5 = S_PATTERN.matcher(term);
            while (matcher5.find()) {
                found = true;
                try {
                    int startOffset5 = Integer.parseInt(matcher5.group(1));
                    int endOffset5 = Integer.parseInt(matcher5.group(2));
                    this.mServiceUuidStartOffset = Integer.valueOf(startOffset5);
                    this.mServiceUuidEndOffset = Integer.valueOf(endOffset5);
                    String hexString2 = matcher5.group(3);
                    try {
                        this.mServiceUuid = Long.decode("0x" + hexString2);
                    } catch (NumberFormatException e6) {
                        throw new BeaconLayoutException("Cannot parse serviceUuid: " + hexString2 + " in term: " + term);
                    }
                } catch (NumberFormatException e7) {
                    throw new BeaconLayoutException("Cannot parse integer byte offset in term: " + term);
                }
            }
            if (!found) {
                LogManager.d(TAG, "cannot parse term %s", term);
                throw new BeaconLayoutException("Cannot parse beacon layout term: " + term);
            }
        }
        if (this.mPowerStartOffset == null || this.mPowerEndOffset == null) {
            throw new BeaconLayoutException("You must supply a power byte offset with a prefix of 'p'");
        } else if (this.mMatchingBeaconTypeCodeStartOffset == null || this.mMatchingBeaconTypeCodeEndOffset == null) {
            throw new BeaconLayoutException("You must supply a matching beacon type expression with a prefix of 'm'");
        } else if (this.mIdentifierStartOffsets.size() != 0 && this.mIdentifierEndOffsets.size() != 0) {
            return this;
        } else {
            throw new BeaconLayoutException("You must supply at least one identifier offset withh a prefix of 'i'");
        }
    }

    public int[] getHardwareAssistManufacturers() {
        return this.mHardwareAssistManufacturers;
    }

    public void setHardwareAssistManufacturerCodes(int[] manufacturers) {
        this.mHardwareAssistManufacturers = manufacturers;
    }

    public Long getMatchingBeaconTypeCode() {
        return this.mMatchingBeaconTypeCode;
    }

    public int getMatchingBeaconTypeCodeStartOffset() {
        return this.mMatchingBeaconTypeCodeStartOffset.intValue();
    }

    public int getMatchingBeaconTypeCodeEndOffset() {
        return this.mMatchingBeaconTypeCodeEndOffset.intValue();
    }

    public Long getServiceUuid() {
        return this.mServiceUuid;
    }

    public int getMServiceUuidStartOffset() {
        return this.mServiceUuidStartOffset.intValue();
    }

    public int getServiceUuidEndOffset() {
        return this.mServiceUuidEndOffset.intValue();
    }

    @TargetApi(5)
    public Beacon fromScanData(byte[] scanData, int rssi, BluetoothDevice device) {
        return fromScanData(scanData, rssi, device, new Beacon());
    }

    /* access modifiers changed from: protected */
    @TargetApi(5)
    public Beacon fromScanData(byte[] scanData, int rssi, BluetoothDevice device, Beacon beacon) {
        int maxByteForMatch = 5;
        byte[] serviceUuidBytes = null;
        byte[] typeCodeBytes = longToByteArray(getMatchingBeaconTypeCode().longValue(), (this.mMatchingBeaconTypeCodeEndOffset.intValue() - this.mMatchingBeaconTypeCodeStartOffset.intValue()) + 1);
        if (getServiceUuid() != null) {
            maxByteForMatch = 11;
            serviceUuidBytes = longToByteArray(getServiceUuid().longValue(), (this.mServiceUuidEndOffset.intValue() - this.mServiceUuidStartOffset.intValue()) + 1);
        }
        int startByte = 2;
        boolean patternFound = false;
        while (true) {
            if (startByte > maxByteForMatch) {
                break;
            }
            if (getServiceUuid() != null) {
                if (byteArraysMatch(scanData, this.mServiceUuidStartOffset.intValue() + startByte, serviceUuidBytes, 0) && byteArraysMatch(scanData, this.mMatchingBeaconTypeCodeStartOffset.intValue() + startByte, typeCodeBytes, 0)) {
                    patternFound = true;
                    break;
                }
            } else if (byteArraysMatch(scanData, this.mMatchingBeaconTypeCodeStartOffset.intValue() + startByte, typeCodeBytes, 0)) {
                patternFound = true;
                break;
            }
            startByte++;
        }
        if (!patternFound) {
            if (getServiceUuid() == null) {
                if (LogManager.isVerboseLoggingEnabled()) {
                    LogManager.d(TAG, "This is not a matching Beacon advertisement. (Was expecting %s.  The bytes I see are: %s", byteArrayToString(typeCodeBytes), bytesToHex(scanData));
                }
            } else if (LogManager.isVerboseLoggingEnabled()) {
                LogManager.d(TAG, "This is not a matching Beacon advertisement. (Was expecting %s and %s.  The bytes I see are: %s", byteArrayToString(serviceUuidBytes), byteArrayToString(typeCodeBytes), bytesToHex(scanData));
            }
            return null;
        }
        if (LogManager.isVerboseLoggingEnabled()) {
            LogManager.d(TAG, "This is a recognized beacon advertisement -- %s seen", byteArrayToString(typeCodeBytes));
        }
        ArrayList<Identifier> identifiers = new ArrayList<>();
        for (int i = 0; i < this.mIdentifierEndOffsets.size(); i++) {
            identifiers.add(Identifier.fromBytes(scanData, ((Integer) this.mIdentifierStartOffsets.get(i)).intValue() + startByte, ((Integer) this.mIdentifierEndOffsets.get(i)).intValue() + startByte + 1, ((Boolean) this.mIdentifierLittleEndianFlags.get(i)).booleanValue()));
        }
        ArrayList<Long> dataFields = new ArrayList<>();
        for (int i2 = 0; i2 < this.mDataEndOffsets.size(); i2++) {
            dataFields.add(Long.valueOf(Long.parseLong(byteArrayToFormattedString(scanData, ((Integer) this.mDataStartOffsets.get(i2)).intValue() + startByte, ((Integer) this.mDataEndOffsets.get(i2)).intValue() + startByte, ((Boolean) this.mDataLittleEndianFlags.get(i2)).booleanValue()))));
        }
        int txPower = Integer.parseInt(byteArrayToFormattedString(scanData, this.mPowerStartOffset.intValue() + startByte, this.mPowerEndOffset.intValue() + startByte, false));
        if (txPower > 127) {
            txPower += InputDeviceCompat.SOURCE_ANY;
        }
        int beaconTypeCode = Integer.parseInt(byteArrayToFormattedString(scanData, this.mMatchingBeaconTypeCodeStartOffset.intValue() + startByte, this.mMatchingBeaconTypeCodeEndOffset.intValue() + startByte, false));
        int manufacturer = Integer.parseInt(byteArrayToFormattedString(scanData, startByte, startByte + 1, true));
        String macAddress = null;
        String name = null;
        if (device != null) {
            macAddress = device.getAddress();
            name = device.getName();
        }
        beacon.mIdentifiers = identifiers;
        beacon.mDataFields = dataFields;
        beacon.mTxPower = txPower;
        beacon.mRssi = rssi;
        beacon.mBeaconTypeCode = beaconTypeCode;
        if (this.mServiceUuid != null) {
            beacon.mServiceUuid = (int) this.mServiceUuid.longValue();
        } else {
            beacon.mServiceUuid = -1;
        }
        beacon.mBluetoothAddress = macAddress;
        beacon.mBluetoothName = name;
        beacon.mManufacturer = manufacturer;
        return beacon;
    }

    public byte[] getBeaconAdvertisementData(Beacon beacon) {
        int lastIndex = -1;
        if (this.mMatchingBeaconTypeCodeEndOffset != null && this.mMatchingBeaconTypeCodeEndOffset.intValue() > -1) {
            lastIndex = this.mMatchingBeaconTypeCodeEndOffset.intValue();
        }
        if (this.mPowerEndOffset != null && this.mPowerEndOffset.intValue() > lastIndex) {
            lastIndex = this.mPowerEndOffset.intValue();
        }
        for (int identifierNum = 0; identifierNum < this.mIdentifierStartOffsets.size(); identifierNum++) {
            if (this.mIdentifierEndOffsets.get(identifierNum) != null && ((Integer) this.mIdentifierEndOffsets.get(identifierNum)).intValue() > lastIndex) {
                lastIndex = ((Integer) this.mIdentifierEndOffsets.get(identifierNum)).intValue();
            }
        }
        for (int identifierNum2 = 0; identifierNum2 < this.mDataEndOffsets.size(); identifierNum2++) {
            if (this.mDataEndOffsets.get(identifierNum2) != null && ((Integer) this.mDataEndOffsets.get(identifierNum2)).intValue() > lastIndex) {
                lastIndex = ((Integer) this.mDataEndOffsets.get(identifierNum2)).intValue();
            }
        }
        byte[] advertisingBytes = new byte[((lastIndex + 1) - 2)];
        long longValue = getMatchingBeaconTypeCode().longValue();
        for (int index = this.mMatchingBeaconTypeCodeStartOffset.intValue(); index <= this.mMatchingBeaconTypeCodeEndOffset.intValue(); index++) {
            advertisingBytes[index - 2] = (byte) ((int) ((getMatchingBeaconTypeCode().longValue() >> ((this.mMatchingBeaconTypeCodeEndOffset.intValue() - index) * 8)) & 255));
        }
        for (int identifierNum3 = 0; identifierNum3 < this.mIdentifierStartOffsets.size(); identifierNum3++) {
            byte[] identifierBytes = beacon.getIdentifier(identifierNum3).toByteArrayOfSpecifiedEndianness(((Boolean) this.mIdentifierLittleEndianFlags.get(identifierNum3)).booleanValue());
            for (int index2 = ((Integer) this.mIdentifierStartOffsets.get(identifierNum3)).intValue(); index2 <= ((Integer) this.mIdentifierEndOffsets.get(identifierNum3)).intValue(); index2++) {
                if (((Integer) this.mIdentifierEndOffsets.get(identifierNum3)).intValue() - index2 < identifierBytes.length) {
                    advertisingBytes[index2 - 2] = identifierBytes[((Integer) this.mIdentifierEndOffsets.get(identifierNum3)).intValue() - index2];
                } else {
                    advertisingBytes[index2 - 2] = 0;
                }
            }
        }
        for (int index3 = this.mPowerStartOffset.intValue(); index3 <= this.mPowerEndOffset.intValue(); index3++) {
            advertisingBytes[index3 - 2] = (byte) ((beacon.getTxPower() >> ((index3 - this.mPowerStartOffset.intValue()) * 8)) & 255);
        }
        for (int dataFieldNum = 0; dataFieldNum < this.mDataStartOffsets.size(); dataFieldNum++) {
            long dataField = ((Long) beacon.getDataFields().get(dataFieldNum)).longValue();
            for (int index4 = ((Integer) this.mDataStartOffsets.get(dataFieldNum)).intValue(); index4 <= ((Integer) this.mDataEndOffsets.get(dataFieldNum)).intValue(); index4++) {
                int endianCorrectedIndex = index4;
                if (((Boolean) this.mDataLittleEndianFlags.get(dataFieldNum)).booleanValue()) {
                    endianCorrectedIndex = ((Integer) this.mDataEndOffsets.get(dataFieldNum)).intValue() - index4;
                }
                advertisingBytes[endianCorrectedIndex - 2] = (byte) ((int) ((dataField >> ((index4 - ((Integer) this.mDataStartOffsets.get(dataFieldNum)).intValue()) * 8)) & 255));
            }
        }
        return advertisingBytes;
    }

    public BeaconParser setMatchingBeaconTypeCode(Long typeCode) {
        this.mMatchingBeaconTypeCode = typeCode;
        return this;
    }

    public int getIdentifierByteCount(int identifierNum) {
        return (((Integer) this.mIdentifierEndOffsets.get(identifierNum)).intValue() - ((Integer) this.mIdentifierStartOffsets.get(identifierNum)).intValue()) + 1;
    }

    public int getIdentifierCount() {
        return this.mIdentifierStartOffsets.size();
    }

    public int getDataFieldCount() {
        return this.mDataStartOffsets.size();
    }

    protected static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[(bytes.length * 2)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[(j * 2) + 1] = HEX_ARRAY[v & 15];
        }
        return new String(hexChars);
    }

    public static byte[] longToByteArray(long longValue, int length) {
        byte[] array = new byte[length];
        for (int i = 0; i < length; i++) {
            array[i] = (byte) ((int) ((longValue & (255 << (((length - i) - 1) * 8))) >> ((int) ((long) (((length - i) - 1) * 8)))));
        }
        return array;
    }

    private boolean byteArraysMatch(byte[] array1, int offset1, byte[] array2, int offset2) {
        int minSize = array1.length > array2.length ? array2.length : array1.length;
        for (int i = 0; i < minSize; i++) {
            if (array1[i + offset1] != array2[i + offset2]) {
                return false;
            }
        }
        return true;
    }

    private String byteArrayToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte valueOf : bytes) {
            sb.append(String.format("%02x", new Object[]{Byte.valueOf(valueOf)}));
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    private String byteArrayToFormattedString(byte[] byteBuffer, int startIndex, int endIndex, boolean littleEndian) {
        byte[] bytes = new byte[((endIndex - startIndex) + 1)];
        if (littleEndian) {
            for (int i = 0; i <= endIndex - startIndex; i++) {
                bytes[i] = byteBuffer[((bytes.length + startIndex) - 1) - i];
            }
        } else {
            for (int i2 = 0; i2 <= endIndex - startIndex; i2++) {
                bytes[i2] = byteBuffer[startIndex + i2];
            }
        }
        if ((endIndex - startIndex) + 1 < 5) {
            long number = 0;
            for (int i3 = 0; i3 < bytes.length; i3++) {
                number += ((long) (bytes[(bytes.length - i3) - 1] & 255)) * ((long) Math.pow(256.0d, ((double) i3) * 1.0d));
            }
            return Long.toString(number);
        }
        String hexString = bytesToHex(bytes);
        if (bytes.length != 16) {
            return "0x" + hexString;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(hexString.substring(0, 8));
        sb.append("-");
        sb.append(hexString.substring(8, 12));
        sb.append("-");
        sb.append(hexString.substring(12, 16));
        sb.append("-");
        sb.append(hexString.substring(16, 20));
        sb.append("-");
        sb.append(hexString.substring(20, 32));
        return sb.toString();
    }
}
