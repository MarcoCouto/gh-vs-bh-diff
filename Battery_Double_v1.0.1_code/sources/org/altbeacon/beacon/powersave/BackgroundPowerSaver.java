package org.altbeacon.beacon.powersave;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.logging.LogManager;

@TargetApi(18)
public class BackgroundPowerSaver implements ActivityLifecycleCallbacks {
    private static final String TAG = "BackgroundPowerSaver";
    private int activeActivityCount;
    private BeaconManager beaconManager;

    public BackgroundPowerSaver(Context context, boolean countActiveActivityStrategy) {
        this.activeActivityCount = 0;
        if (VERSION.SDK_INT < 18) {
            LogManager.w(TAG, "BackgroundPowerSaver requires SDK 18 or higher.", new Object[0]);
            return;
        }
        if (context instanceof Application) {
            ((Application) context).registerActivityLifecycleCallbacks(this);
        } else {
            LogManager.e(TAG, "Context is not an application instance, so we cannot use the BackgroundPowerSaver", new Object[0]);
        }
        BeaconManager beaconManager2 = this.beaconManager;
        this.beaconManager = BeaconManager.getInstanceForApplication(context);
    }

    public BackgroundPowerSaver(Context context) {
        this(context, false);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
        this.activeActivityCount++;
        if (this.activeActivityCount < 1) {
            LogManager.d(TAG, "reset active activity count on resume.  It was %s", Integer.valueOf(this.activeActivityCount));
            this.activeActivityCount = 1;
        }
        this.beaconManager.setBackgroundMode(false);
        LogManager.d(TAG, "activity resumed: %s active activities: %s", activity, Integer.valueOf(this.activeActivityCount));
    }

    public void onActivityPaused(Activity activity) {
        this.activeActivityCount--;
        LogManager.d(TAG, "activity paused: %s active activities: %s", activity, Integer.valueOf(this.activeActivityCount));
        if (this.activeActivityCount < 1) {
            LogManager.d(TAG, "setting background mode", new Object[0]);
            this.beaconManager.setBackgroundMode(true);
        }
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }
}
