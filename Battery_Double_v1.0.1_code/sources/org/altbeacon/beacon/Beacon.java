package org.altbeacon.beacon;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.altbeacon.beacon.client.BeaconDataFactory;
import org.altbeacon.beacon.client.NullBeaconDataFactory;
import org.altbeacon.beacon.distance.DistanceCalculator;
import org.altbeacon.beacon.logging.LogManager;

public class Beacon implements Parcelable {
    public static final Creator<Beacon> CREATOR = new Creator<Beacon>() {
        public Beacon createFromParcel(Parcel in) {
            return new Beacon(in);
        }

        public Beacon[] newArray(int size) {
            return new Beacon[size];
        }
    };
    private static final String TAG = "Beacon";
    protected static BeaconDataFactory beaconDataFactory = new NullBeaconDataFactory();
    protected static DistanceCalculator sDistanceCalculator = null;
    protected int mBeaconTypeCode;
    protected String mBluetoothAddress;
    protected String mBluetoothName;
    protected List<Long> mDataFields;
    protected Double mDistance;
    protected List<Identifier> mIdentifiers;
    protected int mManufacturer;
    protected int mRssi;
    private Double mRunningAverageRssi;
    protected int mServiceUuid;
    protected int mTxPower;

    public static class Builder {
        protected Beacon mBeacon = new Beacon();
        private Identifier mId1;
        private Identifier mId2;
        private Identifier mId3;

        public Beacon build() {
            if (this.mId1 != null) {
                this.mBeacon.mIdentifiers.add(this.mId1);
                if (this.mId2 != null) {
                    this.mBeacon.mIdentifiers.add(this.mId2);
                    if (this.mId3 != null) {
                        this.mBeacon.mIdentifiers.add(this.mId3);
                    }
                }
            }
            return this.mBeacon;
        }

        public Builder setIdentifiers(List<Identifier> identifiers) {
            this.mId1 = null;
            this.mId2 = null;
            this.mId3 = null;
            this.mBeacon.mIdentifiers = identifiers;
            return this;
        }

        public Builder setId1(String id1String) {
            this.mId1 = Identifier.parse(id1String);
            return this;
        }

        public Builder setId2(String id2String) {
            this.mId2 = Identifier.parse(id2String);
            return this;
        }

        public Builder setId3(String id3String) {
            this.mId3 = Identifier.parse(id3String);
            return this;
        }

        public Builder setRssi(int rssi) {
            this.mBeacon.mRssi = rssi;
            return this;
        }

        public Builder setTxPower(int txPower) {
            this.mBeacon.mTxPower = txPower;
            return this;
        }

        public Builder setBeaconTypeCode(int beaconTypeCode) {
            this.mBeacon.mBeaconTypeCode = beaconTypeCode;
            return this;
        }

        public Builder setServiceUuid(int serviceUuid) {
            this.mBeacon.mServiceUuid = serviceUuid;
            return this;
        }

        public Builder setBluetoothAddress(String bluetoothAddress) {
            this.mBeacon.mBluetoothAddress = bluetoothAddress;
            return this;
        }

        public Builder setDataFields(List<Long> dataFields) {
            this.mBeacon.mDataFields = dataFields;
            return this;
        }

        public Builder setManufacturer(int manufacturer) {
            this.mBeacon.mManufacturer = manufacturer;
            return this;
        }

        public Builder setBluetoothName(String name) {
            this.mBeacon.mBluetoothName = name;
            return this;
        }
    }

    public static void setDistanceCalculator(DistanceCalculator dc) {
        sDistanceCalculator = dc;
    }

    public static DistanceCalculator getDistanceCalculator() {
        return sDistanceCalculator;
    }

    protected Beacon(Parcel in) {
        this.mRunningAverageRssi = null;
        int size = in.readInt();
        this.mIdentifiers = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            this.mIdentifiers.add(Identifier.parse(in.readString()));
        }
        this.mDistance = Double.valueOf(in.readDouble());
        this.mRssi = in.readInt();
        this.mTxPower = in.readInt();
        this.mBluetoothAddress = in.readString();
        this.mBeaconTypeCode = in.readInt();
        this.mServiceUuid = in.readInt();
        int dataSize = in.readInt();
        this.mDataFields = new ArrayList(dataSize);
        for (int i2 = 0; i2 < dataSize; i2++) {
            this.mDataFields.add(Long.valueOf(in.readLong()));
        }
        this.mManufacturer = in.readInt();
        this.mBluetoothName = in.readString();
    }

    protected Beacon(Beacon otherBeacon) {
        this.mRunningAverageRssi = null;
        this.mIdentifiers = new ArrayList(otherBeacon.mIdentifiers.size());
        this.mDataFields = new ArrayList(otherBeacon.mDataFields.size());
        for (int i = 0; i < otherBeacon.mIdentifiers.size(); i++) {
            this.mIdentifiers.add(otherBeacon.mIdentifiers.get(i));
        }
        this.mDistance = otherBeacon.mDistance;
        this.mRunningAverageRssi = otherBeacon.mRunningAverageRssi;
        this.mRssi = otherBeacon.mRssi;
        this.mTxPower = otherBeacon.mTxPower;
        this.mBluetoothAddress = otherBeacon.mBluetoothAddress;
        this.mBeaconTypeCode = otherBeacon.getBeaconTypeCode();
        this.mServiceUuid = otherBeacon.getServiceUuid();
        this.mBluetoothName = otherBeacon.mBluetoothName;
    }

    protected Beacon() {
        this.mRunningAverageRssi = null;
        this.mIdentifiers = new ArrayList(1);
        this.mDataFields = new ArrayList(1);
    }

    public void setRunningAverageRssi(double rssi) {
        this.mRunningAverageRssi = Double.valueOf(rssi);
        this.mDistance = null;
    }

    public void setRssi(int rssi) {
        this.mRssi = rssi;
    }

    public int getManufacturer() {
        return this.mManufacturer;
    }

    public int getServiceUuid() {
        return this.mServiceUuid;
    }

    public Identifier getIdentifier(int i) {
        return (Identifier) this.mIdentifiers.get(i);
    }

    public Identifier getId1() {
        return (Identifier) this.mIdentifiers.get(0);
    }

    public Identifier getId2() {
        return (Identifier) this.mIdentifiers.get(1);
    }

    public Identifier getId3() {
        return (Identifier) this.mIdentifiers.get(2);
    }

    public List<Long> getDataFields() {
        return Collections.unmodifiableList(this.mDataFields);
    }

    public List<Identifier> getIdentifiers() {
        return Collections.unmodifiableList(this.mIdentifiers);
    }

    public double getDistance() {
        if (this.mDistance == null) {
            double bestRssiAvailable = (double) this.mRssi;
            if (this.mRunningAverageRssi != null) {
                bestRssiAvailable = this.mRunningAverageRssi.doubleValue();
            } else {
                LogManager.d(TAG, "Not using running average RSSI because it is null", new Object[0]);
            }
            this.mDistance = calculateDistance(this.mTxPower, bestRssiAvailable);
        }
        return this.mDistance.doubleValue();
    }

    public int getRssi() {
        return this.mRssi;
    }

    public int getTxPower() {
        return this.mTxPower;
    }

    public int getBeaconTypeCode() {
        return this.mBeaconTypeCode;
    }

    public String getBluetoothAddress() {
        return this.mBluetoothAddress;
    }

    public String getBluetoothName() {
        return this.mBluetoothName;
    }

    public int hashCode() {
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (Identifier identifier : this.mIdentifiers) {
            sb.append("id");
            sb.append(i);
            sb.append(": ");
            sb.append(identifier.toString());
            sb.append(" ");
            i++;
        }
        return sb.toString().hashCode();
    }

    public boolean equals(Object that) {
        if (!(that instanceof Beacon)) {
            return false;
        }
        Beacon thatBeacon = (Beacon) that;
        if (this.mIdentifiers.size() != thatBeacon.mIdentifiers.size()) {
            return false;
        }
        for (int i = 0; i < this.mIdentifiers.size(); i++) {
            if (!((Identifier) this.mIdentifiers.get(i)).equals(thatBeacon.mIdentifiers.get(i))) {
                return false;
            }
        }
        return true;
    }

    public void requestData(BeaconDataNotifier notifier) {
        beaconDataFactory.requestBeaconData(this, notifier);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (Identifier identifier : this.mIdentifiers) {
            if (i > 1) {
                sb.append(" ");
            }
            sb.append("id");
            sb.append(i);
            sb.append(": ");
            sb.append(identifier == null ? "null" : identifier.toString());
            i++;
        }
        return sb.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.mIdentifiers.size());
        LogManager.d(TAG, "serializing identifiers of size %s", Integer.valueOf(this.mIdentifiers.size()));
        for (Identifier identifier : this.mIdentifiers) {
            out.writeString(identifier == null ? null : identifier.toString());
        }
        out.writeDouble(getDistance());
        out.writeInt(this.mRssi);
        out.writeInt(this.mTxPower);
        out.writeString(this.mBluetoothAddress);
        out.writeInt(this.mBeaconTypeCode);
        out.writeInt(this.mServiceUuid);
        out.writeInt(this.mDataFields.size());
        for (Long dataField : this.mDataFields) {
            out.writeLong(dataField.longValue());
        }
        out.writeInt(this.mManufacturer);
        out.writeString(this.mBluetoothName);
    }

    protected static Double calculateDistance(int txPower, double bestRssiAvailable) {
        if (getDistanceCalculator() != null) {
            return Double.valueOf(getDistanceCalculator().calculateDistance(txPower, bestRssiAvailable));
        }
        LogManager.e(TAG, "Distance calculator not set.  Distance will bet set to -1", new Object[0]);
        return Double.valueOf(-1.0d);
    }
}
