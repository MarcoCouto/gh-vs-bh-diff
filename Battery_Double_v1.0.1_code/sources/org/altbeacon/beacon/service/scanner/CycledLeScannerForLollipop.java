package org.altbeacon.beacon.service.scanner;

import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.bluetooth.le.ScanSettings.Builder;
import android.content.Context;
import android.os.Handler;
import java.util.ArrayList;
import java.util.List;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.service.DetectionTracker;
import org.altbeacon.bluetooth.BluetoothCrashResolver;

@TargetApi(21)
public class CycledLeScannerForLollipop extends CycledLeScanner {
    private static final long BACKGROUND_L_SCAN_DETECTION_PERIOD_MILLIS = 10000;
    private static final String TAG = "CycledLeScannerForLollipop";
    private ScanCallback leScanCallback;
    private long mBackgroundLScanFirstDetectionTime = 0;
    /* access modifiers changed from: private */
    public long mBackgroundLScanStartTime = 0;
    private BeaconManager mBeaconManager = BeaconManager.getInstanceForApplication(this.mContext);
    private boolean mScanDeferredBefore = false;
    private BluetoothLeScanner mScanner;

    public CycledLeScannerForLollipop(Context context, long scanPeriod, long betweenScanPeriod, boolean backgroundFlag, CycledLeScanCallback cycledLeScanCallback, BluetoothCrashResolver crashResolver) {
        super(context, scanPeriod, betweenScanPeriod, backgroundFlag, cycledLeScanCallback, crashResolver);
    }

    /* access modifiers changed from: protected */
    public void stopScan() {
        try {
            if (getScanner() != null) {
                getScanner().stopScan(getNewLeScanCallback());
            }
        } catch (Exception e) {
            LogManager.w(e, TAG, "Internal Android exception scanning for beacons", new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public boolean deferScanIfNeeded() {
        long millisecondsUntilStart = this.mNextScanCycleStartTime - System.currentTimeMillis();
        if (millisecondsUntilStart > 0) {
            long secsSinceLastDetection = System.currentTimeMillis() - DetectionTracker.getInstance().getLastDetectionTime();
            if (!this.mScanDeferredBefore) {
                if (secsSinceLastDetection > 10000) {
                    this.mBackgroundLScanStartTime = System.currentTimeMillis();
                    this.mBackgroundLScanFirstDetectionTime = 0;
                    LogManager.d(TAG, "This is Android L. Doing a filtered scan for the background.", new Object[0]);
                    ScanSettings settings = new Builder().setScanMode(0).build();
                    try {
                        if (getScanner() != null) {
                            getScanner().startScan(new ScanFilterUtils().createScanFiltersForBeaconParsers(this.mBeaconManager.getBeaconParsers()), settings, getNewLeScanCallback());
                        }
                    } catch (IllegalStateException e) {
                        LogManager.w(TAG, "Cannot start scan.  Bluetooth may be turned off.", new Object[0]);
                    }
                } else {
                    LogManager.d(TAG, "This is Android L, but we last saw a beacon only %s ago, so we will not keep scanning in background.", Long.valueOf(secsSinceLastDetection));
                }
            }
            if (this.mBackgroundLScanStartTime > 0 && DetectionTracker.getInstance().getLastDetectionTime() > this.mBackgroundLScanStartTime) {
                if (this.mBackgroundLScanFirstDetectionTime == 0) {
                    this.mBackgroundLScanFirstDetectionTime = DetectionTracker.getInstance().getLastDetectionTime();
                }
                if (System.currentTimeMillis() - this.mBackgroundLScanFirstDetectionTime >= 10000) {
                    LogManager.d(TAG, "We've been detecting for a bit.  Stopping Android L background scanning", new Object[0]);
                    try {
                        if (getScanner() != null) {
                            getScanner().stopScan(getNewLeScanCallback());
                        }
                    } catch (IllegalStateException e2) {
                        LogManager.w(TAG, "Cannot stop scan.  Bluetooth may be turned off.", new Object[0]);
                    }
                    this.mBackgroundLScanStartTime = 0;
                } else {
                    LogManager.d(TAG, "Delivering Android L background scanning results", new Object[0]);
                    this.mCycledLeScanCallback.onCycleEnd();
                }
            }
            LogManager.d(TAG, "Waiting to start full bluetooth scan for another %s milliseconds", Long.valueOf(millisecondsUntilStart));
            if (!this.mScanDeferredBefore && this.mBackgroundFlag) {
                setWakeUpAlarm();
            }
            Handler handler = this.mHandler;
            AnonymousClass1 r7 = new Runnable() {
                public void run() {
                    CycledLeScannerForLollipop.this.scanLeDevice(Boolean.valueOf(true));
                }
            };
            if (millisecondsUntilStart > 1000) {
                millisecondsUntilStart = 1000;
            }
            handler.postDelayed(r7, millisecondsUntilStart);
            this.mScanDeferredBefore = true;
            return true;
        }
        if (this.mBackgroundLScanStartTime > 0) {
            try {
                if (getScanner() != null) {
                    getScanner().stopScan(getNewLeScanCallback());
                }
            } catch (IllegalStateException e3) {
                LogManager.w(TAG, "Cannot stop scan.  Bluetooth may be turned off.", new Object[0]);
            }
            this.mBackgroundLScanStartTime = 0;
        }
        this.mScanDeferredBefore = false;
        return false;
    }

    /* access modifiers changed from: protected */
    public void startScan() {
        ScanSettings settings;
        List<ScanFilter> filters = new ArrayList<>();
        if (this.mBackgroundFlag) {
            LogManager.d(TAG, "starting scan in SCAN_MODE_LOW_POWER", new Object[0]);
            settings = new Builder().setScanMode(0).build();
        } else {
            LogManager.d(TAG, "starting scan in SCAN_MODE_LOW_LATENCY", new Object[0]);
            settings = new Builder().setScanMode(2).build();
        }
        try {
            if (getScanner() != null) {
                getScanner().startScan(filters, settings, getNewLeScanCallback());
            }
        } catch (IllegalStateException e) {
            LogManager.w(TAG, "Cannot start scan.  Bluetooth may be turned off.", new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void finishScan() {
        try {
            if (getScanner() != null) {
                getScanner().stopScan(getNewLeScanCallback());
            }
        } catch (IllegalStateException e) {
            LogManager.w(TAG, "Cannot stop scan.  Bluetooth may be turned off.", new Object[0]);
        }
        this.mScanningPaused = true;
    }

    private BluetoothLeScanner getScanner() {
        if (this.mScanner == null) {
            LogManager.d(TAG, "Making new Android L scanner", new Object[0]);
            this.mScanner = getBluetoothAdapter().getBluetoothLeScanner();
            if (this.mScanner == null) {
                LogManager.w(TAG, "Failed to make new Android L scanner", new Object[0]);
            }
        }
        return this.mScanner;
    }

    private ScanCallback getNewLeScanCallback() {
        if (this.leScanCallback == null) {
            this.leScanCallback = new ScanCallback() {
                public void onScanResult(int callbackType, ScanResult scanResult) {
                    LogManager.d(CycledLeScannerForLollipop.TAG, "got record", new Object[0]);
                    CycledLeScannerForLollipop.this.mCycledLeScanCallback.onLeScan(scanResult.getDevice(), scanResult.getRssi(), scanResult.getScanRecord().getBytes());
                    if (CycledLeScannerForLollipop.this.mBackgroundLScanStartTime > 0) {
                        LogManager.d(CycledLeScannerForLollipop.TAG, "got a filtered scan result in the background.", new Object[0]);
                    }
                }

                public void onBatchScanResults(List<ScanResult> results) {
                    LogManager.d(CycledLeScannerForLollipop.TAG, "got batch records", new Object[0]);
                    for (ScanResult scanResult : results) {
                        CycledLeScannerForLollipop.this.mCycledLeScanCallback.onLeScan(scanResult.getDevice(), scanResult.getRssi(), scanResult.getScanRecord().getBytes());
                    }
                    if (CycledLeScannerForLollipop.this.mBackgroundLScanStartTime > 0) {
                        LogManager.d(CycledLeScannerForLollipop.TAG, "got a filtered batch scan result in the background.", new Object[0]);
                    }
                }

                public void onScanFailed(int i) {
                    LogManager.e(CycledLeScannerForLollipop.TAG, "Scan Failed", new Object[0]);
                }
            };
        }
        return this.leScanCallback;
    }
}
