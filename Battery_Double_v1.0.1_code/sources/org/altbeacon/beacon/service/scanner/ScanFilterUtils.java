package org.altbeacon.beacon.service.scanner;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanFilter.Builder;
import java.util.ArrayList;
import java.util.List;
import org.altbeacon.beacon.BeaconParser;

@TargetApi(21)
public class ScanFilterUtils {

    class ScanFilterData {
        public byte[] filter;
        public int manufacturer;
        public byte[] mask;

        ScanFilterData() {
        }
    }

    public List<ScanFilterData> createScanFilterDataForBeaconParser(BeaconParser beaconParser) {
        int[] arr$;
        ArrayList<ScanFilterData> scanFilters = new ArrayList<>();
        for (int manufacturer : beaconParser.getHardwareAssistManufacturers()) {
            long typeCode = beaconParser.getMatchingBeaconTypeCode().longValue();
            int startOffset = beaconParser.getMatchingBeaconTypeCodeStartOffset();
            int endOffset = beaconParser.getMatchingBeaconTypeCodeEndOffset();
            byte[] filter = new byte[((endOffset + 1) - 2)];
            byte[] mask = new byte[((endOffset + 1) - 2)];
            byte[] typeCodeBytes = BeaconParser.longToByteArray(typeCode, (endOffset - startOffset) + 1);
            for (int layoutIndex = 2; layoutIndex <= endOffset; layoutIndex++) {
                int filterIndex = layoutIndex - 2;
                if (layoutIndex < startOffset) {
                    filter[filterIndex] = 0;
                    mask[filterIndex] = 0;
                } else {
                    filter[filterIndex] = typeCodeBytes[layoutIndex - startOffset];
                    mask[filterIndex] = -1;
                }
            }
            ScanFilterData sfd = new ScanFilterData();
            sfd.manufacturer = manufacturer;
            sfd.filter = filter;
            sfd.mask = mask;
            scanFilters.add(sfd);
        }
        return scanFilters;
    }

    public List<ScanFilter> createScanFiltersForBeaconParsers(List<BeaconParser> beaconParsers) {
        List<ScanFilter> scanFilters = new ArrayList<>();
        for (BeaconParser beaconParser : beaconParsers) {
            for (ScanFilterData sfd : createScanFilterDataForBeaconParser(beaconParser)) {
                Builder builder = new Builder();
                builder.setManufacturerData(sfd.manufacturer, sfd.filter, sfd.mask);
                scanFilters.add(builder.build());
            }
        }
        return scanFilters;
    }
}
