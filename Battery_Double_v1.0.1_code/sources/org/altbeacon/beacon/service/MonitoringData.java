package org.altbeacon.beacon.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.altbeacon.beacon.Region;

public class MonitoringData implements Parcelable {
    public static final Creator<MonitoringData> CREATOR = new Creator<MonitoringData>() {
        public MonitoringData createFromParcel(Parcel in) {
            return new MonitoringData(in);
        }

        public MonitoringData[] newArray(int size) {
            return new MonitoringData[size];
        }
    };
    private static final String TAG = "MonitoringData";
    private boolean inside;
    private Region region;

    public MonitoringData(boolean inside2, Region region2) {
        this.inside = inside2;
        this.region = region2;
    }

    public boolean isInside() {
        return this.inside;
    }

    public Region getRegion() {
        return this.region;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeByte((byte) (this.inside ? 1 : 0));
        out.writeParcelable(this.region, flags);
    }

    private MonitoringData(Parcel in) {
        boolean z = true;
        if (in.readByte() != 1) {
            z = false;
        }
        this.inside = z;
        this.region = (Region) in.readParcelable(getClass().getClassLoader());
    }
}
