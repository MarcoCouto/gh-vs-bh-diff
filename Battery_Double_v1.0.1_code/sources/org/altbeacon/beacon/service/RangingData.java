package org.altbeacon.beacon.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Collection;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.logging.LogManager;

public class RangingData implements Parcelable {
    public static final Creator<RangingData> CREATOR = new Creator<RangingData>() {
        public RangingData createFromParcel(Parcel in) {
            return new RangingData(in);
        }

        public RangingData[] newArray(int size) {
            return new RangingData[size];
        }
    };
    private static final String TAG = "RangingData";
    private Collection<Beacon> beacons;
    private Region region;

    public RangingData(Collection<Beacon> beacons2, Region region2) {
        synchronized (beacons2) {
            this.beacons = beacons2;
        }
        this.region = region2;
    }

    public Collection<Beacon> getBeacons() {
        return this.beacons;
    }

    public Region getRegion() {
        return this.region;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        LogManager.d(TAG, "writing RangingData", new Object[0]);
        out.writeParcelableArray((Parcelable[]) this.beacons.toArray(new Parcelable[0]), flags);
        out.writeParcelable(this.region, flags);
        LogManager.d(TAG, "done writing RangingData", new Object[0]);
    }

    private RangingData(Parcel in) {
        LogManager.d(TAG, "parsing RangingData", new Object[0]);
        Parcelable[] parcelables = in.readParcelableArray(getClass().getClassLoader());
        this.beacons = new ArrayList(parcelables.length);
        for (Parcelable parcelable : parcelables) {
            this.beacons.add((Beacon) parcelable);
        }
        this.region = (Region) in.readParcelable(getClass().getClassLoader());
    }
}
