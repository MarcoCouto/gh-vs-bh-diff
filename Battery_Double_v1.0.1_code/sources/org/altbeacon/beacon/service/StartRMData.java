package org.altbeacon.beacon.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.altbeacon.beacon.Region;

public class StartRMData implements Parcelable {
    public static final Creator<StartRMData> CREATOR = new Creator<StartRMData>() {
        public StartRMData createFromParcel(Parcel in) {
            return new StartRMData(in);
        }

        public StartRMData[] newArray(int size) {
            return new StartRMData[size];
        }
    };
    private boolean backgroundFlag;
    private long betweenScanPeriod;
    private String callbackPackageName;
    private Region region;
    private long scanPeriod;

    public StartRMData(Region region2, String callbackPackageName2) {
        this.region = region2;
        this.callbackPackageName = callbackPackageName2;
    }

    public StartRMData(long scanPeriod2, long betweenScanPeriod2, boolean backgroundFlag2) {
        this.scanPeriod = scanPeriod2;
        this.betweenScanPeriod = betweenScanPeriod2;
        this.backgroundFlag = backgroundFlag2;
    }

    public StartRMData(Region region2, String callbackPackageName2, long scanPeriod2, long betweenScanPeriod2, boolean backgroundFlag2) {
        this.scanPeriod = scanPeriod2;
        this.betweenScanPeriod = betweenScanPeriod2;
        this.region = region2;
        this.callbackPackageName = callbackPackageName2;
        this.backgroundFlag = backgroundFlag2;
    }

    public long getScanPeriod() {
        return this.scanPeriod;
    }

    public long getBetweenScanPeriod() {
        return this.betweenScanPeriod;
    }

    public Region getRegionData() {
        return this.region;
    }

    public String getCallbackPackageName() {
        return this.callbackPackageName;
    }

    public boolean getBackgroundFlag() {
        return this.backgroundFlag;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(this.region, flags);
        out.writeString(this.callbackPackageName);
        out.writeLong(this.scanPeriod);
        out.writeLong(this.betweenScanPeriod);
        out.writeByte((byte) (this.backgroundFlag ? 1 : 0));
    }

    private StartRMData(Parcel in) {
        this.region = (Region) in.readParcelable(StartRMData.class.getClassLoader());
        this.callbackPackageName = in.readString();
        this.scanPeriod = in.readLong();
        this.betweenScanPeriod = in.readLong();
        this.backgroundFlag = in.readByte() != 0;
    }
}
