package org.altbeacon.beacon.service;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.logging.LogManager;

public class RangedBeacon {
    public static long DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS = 20000;
    private static final String TAG = "RangedBeacon";
    private static long sampleExpirationMilliseconds = DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS;
    Beacon mBeacon;
    private ArrayList<Measurement> mMeasurements = new ArrayList<>();
    private boolean mTracked = true;

    private class Measurement implements Comparable<Measurement> {
        Integer rssi;
        long timestamp;

        private Measurement() {
        }

        public int compareTo(Measurement arg0) {
            return this.rssi.compareTo(arg0.rssi);
        }
    }

    public RangedBeacon(Beacon beacon) {
        updateBeacon(beacon);
    }

    public void updateBeacon(Beacon beacon) {
        this.mBeacon = beacon;
        addMeasurement(Integer.valueOf(this.mBeacon.getRssi()));
    }

    public boolean isTracked() {
        return this.mTracked;
    }

    public void setTracked(boolean tracked) {
        this.mTracked = tracked;
    }

    public Beacon getBeacon() {
        return this.mBeacon;
    }

    public void commitMeasurements() {
        if (this.mMeasurements.size() > 0) {
            double runningAverage = calculateRunningAverage();
            this.mBeacon.setRunningAverageRssi(runningAverage);
            LogManager.d(TAG, "calculated new runningAverageRssi: %s", Double.valueOf(runningAverage));
            return;
        }
        LogManager.d(TAG, "No measurements available to calculate running average", new Object[0]);
    }

    public static void setSampleExpirationMilliseconds(long milliseconds) {
        sampleExpirationMilliseconds = milliseconds;
    }

    public void addMeasurement(Integer rssi) {
        this.mTracked = true;
        Measurement measurement = new Measurement();
        measurement.rssi = rssi;
        measurement.timestamp = new Date().getTime();
        this.mMeasurements.add(measurement);
    }

    public boolean noMeasurementsAvailable() {
        return this.mMeasurements.size() == 0;
    }

    private synchronized void refreshMeasurements() {
        Date now = new Date();
        ArrayList<Measurement> newMeasurements = new ArrayList<>();
        Iterator<Measurement> iterator = this.mMeasurements.iterator();
        while (iterator.hasNext()) {
            Measurement measurement = (Measurement) iterator.next();
            if (now.getTime() - measurement.timestamp < sampleExpirationMilliseconds) {
                newMeasurements.add(measurement);
            }
        }
        this.mMeasurements = newMeasurements;
        Collections.sort(this.mMeasurements);
    }

    private double calculateRunningAverage() {
        refreshMeasurements();
        int size = this.mMeasurements.size();
        int startIndex = 0;
        int endIndex = size - 1;
        if (size > 2) {
            startIndex = (size / 10) + 1;
            endIndex = (size - (size / 10)) - 2;
        }
        double sum = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        for (int i = startIndex; i <= endIndex; i++) {
            sum += (double) ((Measurement) this.mMeasurements.get(i)).rssi.intValue();
        }
        double runningAverage = sum / ((double) ((endIndex - startIndex) + 1));
        LogManager.d(TAG, "Running average mRssi based on %s measurements: %s", Integer.valueOf(size), Double.valueOf(runningAverage));
        return runningAverage;
    }
}
