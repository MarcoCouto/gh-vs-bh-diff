package org.altbeacon.beacon;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class Region implements Parcelable {
    public static final Creator<Region> CREATOR = new Creator<Region>() {
        public Region createFromParcel(Parcel in) {
            return new Region(in);
        }

        public Region[] newArray(int size) {
            return new Region[size];
        }
    };
    private static final String TAG = "Region";
    protected final List<Identifier> mIdentifiers;
    protected final String mUniqueId;

    public Region(String uniqueId, Identifier id1, Identifier id2, Identifier id3) {
        this.mIdentifiers = new ArrayList(3);
        this.mIdentifiers.add(id1);
        this.mIdentifiers.add(id2);
        this.mIdentifiers.add(id3);
        this.mUniqueId = uniqueId;
        if (uniqueId == null) {
            throw new NullPointerException("uniqueId may not be null");
        }
    }

    public Region(String uniqueId, List<Identifier> identifiers) {
        this.mIdentifiers = new ArrayList(identifiers);
        this.mUniqueId = uniqueId;
        if (uniqueId == null) {
            throw new NullPointerException("uniqueId may not be null");
        }
    }

    public Identifier getId1() {
        return getIdentifier(0);
    }

    public Identifier getId2() {
        return getIdentifier(1);
    }

    public Identifier getId3() {
        return getIdentifier(2);
    }

    public Identifier getIdentifier(int i) {
        if (this.mIdentifiers.size() > i) {
            return (Identifier) this.mIdentifiers.get(i);
        }
        return null;
    }

    public String getUniqueId() {
        return this.mUniqueId;
    }

    public boolean matchesBeacon(Beacon beacon) {
        for (int i = 0; i < this.mIdentifiers.size(); i++) {
            if ((beacon.getIdentifiers().size() > i || this.mIdentifiers.get(i) != null) && this.mIdentifiers.get(i) != null && !((Identifier) this.mIdentifiers.get(i)).equals(beacon.mIdentifiers.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return this.mUniqueId.hashCode();
    }

    public boolean equals(Object other) {
        if (other instanceof Region) {
            return ((Region) other).mUniqueId.equals(this.mUniqueId);
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (Identifier identifier : this.mIdentifiers) {
            if (i > 1) {
                sb.append(" ");
            }
            sb.append("id");
            sb.append(i);
            sb.append(": ");
            sb.append(identifier == null ? "null" : identifier.toString());
            i++;
        }
        return sb.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.mUniqueId);
        out.writeInt(this.mIdentifiers.size());
        for (Identifier identifier : this.mIdentifiers) {
            if (identifier != null) {
                out.writeString(identifier.toString());
            } else {
                out.writeString(null);
            }
        }
    }

    protected Region(Parcel in) {
        this.mUniqueId = in.readString();
        int size = in.readInt();
        this.mIdentifiers = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            String identifierString = in.readString();
            if (identifierString == null) {
                this.mIdentifiers.add(null);
            } else {
                this.mIdentifiers.add(Identifier.parse(identifierString));
            }
        }
    }

    @Deprecated
    public Region clone() {
        return new Region(this.mUniqueId, this.mIdentifiers);
    }
}
