package org.altbeacon.beacon;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Identifier implements Comparable<Identifier> {
    private static final Pattern DECIMAL_PATTERN = Pattern.compile("^[0-9]+$");
    private static final Pattern HEX_PATTERN = Pattern.compile("^0x[0-9A-Fa-f]*$");
    private static final Pattern UUID_PATTERN = Pattern.compile("^[0-9A-Fa-f]{8}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{12}$");
    private static final char[] hexArray = "0123456789abcdef".toCharArray();
    private static final BigInteger maxInteger = BigInteger.valueOf(65535);
    private final byte[] mValue;

    public static Identifier parse(String stringValue) {
        if (stringValue == null) {
            throw new NullPointerException("stringValue == null");
        } else if (HEX_PATTERN.matcher(stringValue).matches()) {
            return parseHex(stringValue.substring(2));
        } else {
            if (UUID_PATTERN.matcher(stringValue).matches()) {
                return parseHex(stringValue.replace("-", ""));
            }
            if (DECIMAL_PATTERN.matcher(stringValue).matches()) {
                BigInteger i = new BigInteger(stringValue);
                if (i.compareTo(BigInteger.ZERO) >= 0 && i.compareTo(maxInteger) <= 0) {
                    return fromInt(i.intValue());
                }
                throw new IllegalArgumentException("Decimal formatted integers must be between 0 and 65535. Value: " + stringValue);
            }
            throw new IllegalArgumentException("Unable to parse identifier: " + stringValue);
        }
    }

    private static Identifier parseHex(String identifierString) {
        String str = identifierString.length() % 2 == 0 ? identifierString.toLowerCase() : "0" + identifierString.toLowerCase();
        byte[] result = new byte[(str.length() / 2)];
        for (int i = 0; i < result.length; i++) {
            result[i] = (byte) Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16);
        }
        return new Identifier(result);
    }

    public static Identifier fromInt(int intValue) {
        if (intValue < 0 || intValue > 65535) {
            throw new IllegalArgumentException("value must be between 0 and 65535");
        }
        return new Identifier(new byte[]{(byte) (intValue >> 8), (byte) intValue});
    }

    public static Identifier fromBytes(byte[] bytes, int start, int end, boolean littleEndian) {
        if (bytes == null) {
            throw new NullPointerException("bytes == null");
        } else if (start < 0 || start > bytes.length) {
            throw new ArrayIndexOutOfBoundsException("start < 0 || start > bytes.length");
        } else if (end > bytes.length) {
            throw new ArrayIndexOutOfBoundsException("end > bytes.length");
        } else if (start > end) {
            throw new IllegalArgumentException("start > end");
        } else {
            byte[] byteRange = Arrays.copyOfRange(bytes, start, end);
            if (littleEndian) {
                reverseArray(byteRange);
            }
            return new Identifier(byteRange);
        }
    }

    @Deprecated
    public Identifier(Identifier identifier) {
        if (identifier == null) {
            throw new NullPointerException("identifier == null");
        }
        this.mValue = identifier.mValue;
    }

    protected Identifier(byte[] value) {
        if (value == null) {
            throw new NullPointerException("identifier == null");
        }
        this.mValue = value;
    }

    public String toString() {
        if (this.mValue.length == 2) {
            return Integer.toString(toInt());
        }
        if (this.mValue.length == 16) {
            return toUuidString();
        }
        return "0x" + toHexString();
    }

    public int toInt() {
        if (this.mValue.length > 2) {
            throw new UnsupportedOperationException("Only supported for Identifiers with max byte length of 2");
        }
        int result = 0;
        for (int i = 0; i < this.mValue.length; i++) {
            result |= (this.mValue[i] & 255) << (((this.mValue.length - i) - 1) * 8);
        }
        return result;
    }

    public byte[] toByteArrayOfSpecifiedEndianness(boolean bigEndian) {
        byte[] copy = Arrays.copyOf(this.mValue, this.mValue.length);
        if (!bigEndian) {
            reverseArray(copy);
        }
        return copy;
    }

    private static void reverseArray(byte[] bytes) {
        for (int i = 0; i < bytes.length / 2; i++) {
            byte a = bytes[i];
            bytes[i] = bytes[(bytes.length - i) - 1];
            bytes[(bytes.length - i) - 1] = a;
        }
    }

    public int getByteCount() {
        return this.mValue.length;
    }

    public int hashCode() {
        return Arrays.hashCode(this.mValue);
    }

    public boolean equals(Object that) {
        if (!(that instanceof Identifier)) {
            return false;
        }
        return Arrays.equals(this.mValue, ((Identifier) that).mValue);
    }

    private static String toHexString(byte[] bytes, int start, int length) {
        char[] hexChars = new char[(length * 2)];
        for (int i = 0; i < length; i++) {
            int v = bytes[start + i] & 255;
            hexChars[i * 2] = hexArray[v >>> 4];
            hexChars[(i * 2) + 1] = hexArray[v & 15];
        }
        return new String(hexChars);
    }

    public String toHexString() {
        return toHexString(this.mValue, 0, this.mValue.length);
    }

    public String toUuidString() {
        if (this.mValue.length == 16) {
            return toHexString(this.mValue, 0, 4) + "-" + toHexString(this.mValue, 4, 2) + "-" + toHexString(this.mValue, 6, 2) + "-" + toHexString(this.mValue, 8, 2) + "-" + toHexString(this.mValue, 10, 6);
        }
        throw new UnsupportedOperationException("Only available for values with length of 16 bytes");
    }

    public int compareTo(Identifier that) {
        if (this.mValue.length == that.mValue.length) {
            int i = 0;
            while (i < this.mValue.length) {
                if (this.mValue[i] == that.mValue[i]) {
                    i++;
                } else if (this.mValue[i] >= that.mValue[i]) {
                    return 1;
                } else {
                    return -1;
                }
            }
            return 0;
        } else if (this.mValue.length < that.mValue.length) {
            return -1;
        } else {
            return 1;
        }
    }
}
