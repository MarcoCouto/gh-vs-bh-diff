package org.altbeacon.beacon;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import java.util.Collection;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.service.MonitoringData;
import org.altbeacon.beacon.service.RangingData;

@TargetApi(3)
public class BeaconIntentProcessor extends IntentService {
    private static final String TAG = "BeaconIntentProcessor";

    public BeaconIntentProcessor() {
        super(TAG);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        int i = 1;
        LogManager.d(TAG, "got an intent to process", new Object[0]);
        MonitoringData monitoringData = null;
        RangingData rangingData = null;
        if (!(intent == null || intent.getExtras() == null)) {
            monitoringData = (MonitoringData) intent.getExtras().get("monitoringData");
            rangingData = (RangingData) intent.getExtras().get("rangingData");
        }
        if (rangingData != null) {
            LogManager.d(TAG, "got ranging data", new Object[0]);
            if (rangingData.getBeacons() == null) {
                LogManager.w(TAG, "Ranging data has a null beacons collection", new Object[0]);
            }
            RangeNotifier notifier = BeaconManager.getInstanceForApplication(this).getRangingNotifier();
            Collection<Beacon> beacons = rangingData.getBeacons();
            if (notifier != null) {
                notifier.didRangeBeaconsInRegion(beacons, rangingData.getRegion());
            } else {
                LogManager.d(TAG, "but ranging notifier is null, so we're dropping it.", new Object[0]);
            }
            RangeNotifier dataNotifier = BeaconManager.getInstanceForApplication(this).getDataRequestNotifier();
            if (dataNotifier != null) {
                dataNotifier.didRangeBeaconsInRegion(beacons, rangingData.getRegion());
            }
        }
        if (monitoringData != null) {
            LogManager.d(TAG, "got monitoring data", new Object[0]);
            MonitorNotifier notifier2 = BeaconManager.getInstanceForApplication(this).getMonitoringNotifier();
            if (notifier2 != null) {
                LogManager.d(TAG, "Calling monitoring notifier: %s", notifier2);
                if (!monitoringData.isInside()) {
                    i = 0;
                }
                notifier2.didDetermineStateForRegion(i, monitoringData.getRegion());
                if (monitoringData.isInside()) {
                    notifier2.didEnterRegion(monitoringData.getRegion());
                } else {
                    notifier2.didExitRegion(monitoringData.getRegion());
                }
            }
        }
    }
}
