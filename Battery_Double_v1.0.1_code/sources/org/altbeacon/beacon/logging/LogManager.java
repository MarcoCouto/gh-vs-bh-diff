package org.altbeacon.beacon.logging;

public final class LogManager {
    private static Logger sLogger = Loggers.warningLogger();
    private static boolean sVerboseLoggingEnabled = false;

    public static void setLogger(Logger logger) {
        if (logger == null) {
            throw new NullPointerException("Logger may not be null.");
        }
        sLogger = logger;
    }

    public static Logger getLogger() {
        return sLogger;
    }

    public static boolean isVerboseLoggingEnabled() {
        return sVerboseLoggingEnabled;
    }

    public static void setVerboseLoggingEnabled(boolean enabled) {
        sVerboseLoggingEnabled = enabled;
    }

    public static void v(String tag, String message, Object... args) {
        sLogger.v(tag, message, args);
    }

    public static void v(Throwable t, String tag, String message, Object... args) {
        sLogger.v(t, tag, message, args);
    }

    public static void d(String tag, String message, Object... args) {
        sLogger.d(tag, message, args);
    }

    public static void d(Throwable t, String tag, String message, Object... args) {
        sLogger.d(t, tag, message, args);
    }

    public static void i(String tag, String message, Object... args) {
        sLogger.i(tag, message, args);
    }

    public static void i(Throwable t, String tag, String message, Object... args) {
        sLogger.i(t, tag, message, args);
    }

    public static void w(String tag, String message, Object... args) {
        sLogger.w(tag, message, args);
    }

    public static void w(Throwable t, String tag, String message, Object... args) {
        sLogger.w(t, tag, message, args);
    }

    public static void e(String tag, String message, Object... args) {
        sLogger.e(tag, message, args);
    }

    public static void e(Throwable t, String tag, String message, Object... args) {
        sLogger.e(t, tag, message, args);
    }

    private LogManager() {
    }
}
