package org.altbeacon.beacon.logging;

import android.util.Log;

final class VerboseAndroidLogger extends AbstractAndroidLogger {
    VerboseAndroidLogger() {
    }

    public void v(String tag, String message, Object... args) {
        Log.v(tag, formatString(message, args));
    }

    public void v(Throwable t, String tag, String message, Object... args) {
        Log.v(tag, formatString(message, args), t);
    }

    public void d(String tag, String message, Object... args) {
        Log.d(tag, formatString(message, args));
    }

    public void d(Throwable t, String tag, String message, Object... args) {
        Log.d(tag, formatString(message, args), t);
    }

    public void i(String tag, String message, Object... args) {
        Log.i(tag, formatString(message, args));
    }

    public void i(Throwable t, String tag, String message, Object... args) {
        Log.i(tag, formatString(message, args), t);
    }

    public void w(String tag, String message, Object... args) {
        Log.w(tag, formatString(message, args));
    }

    public void w(Throwable t, String tag, String message, Object... args) {
        Log.w(tag, formatString(message, args), t);
    }

    public void e(String tag, String message, Object... args) {
        Log.e(tag, formatString(message, args));
    }

    public void e(Throwable t, String tag, String message, Object... args) {
        Log.e(tag, formatString(message, args), t);
    }
}
