package org.altbeacon.beacon.logging;

abstract class AbstractAndroidLogger implements Logger {
    AbstractAndroidLogger() {
    }

    /* access modifiers changed from: protected */
    public String formatString(String message, Object... args) {
        return args.length == 0 ? message : String.format(message, args);
    }
}
