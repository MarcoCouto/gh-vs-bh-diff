package org.altbeacon.beacon.logging;

final class EmptyLogger implements Logger {
    EmptyLogger() {
    }

    public void v(String tag, String message, Object... args) {
    }

    public void v(Throwable t, String tag, String message, Object... args) {
    }

    public void d(String tag, String message, Object... args) {
    }

    public void d(Throwable t, String tag, String message, Object... args) {
    }

    public void i(String tag, String message, Object... args) {
    }

    public void i(Throwable t, String tag, String message, Object... args) {
    }

    public void w(String tag, String message, Object... args) {
    }

    public void w(Throwable t, String tag, String message, Object... args) {
    }

    public void e(String tag, String message, Object... args) {
    }

    public void e(Throwable t, String tag, String message, Object... args) {
    }
}
