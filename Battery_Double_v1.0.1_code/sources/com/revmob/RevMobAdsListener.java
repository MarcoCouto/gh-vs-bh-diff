package com.revmob;

public class RevMobAdsListener {
    public void onRevMobSessionIsStarted() {
    }

    public void onRevMobSessionNotStarted(String message) {
    }

    public void onRevMobAdReceived() {
    }

    public void onRevMobAdNotReceived(String message) {
    }

    public void onRevMobAdDisplayed() {
    }

    public void onRevMobAdDismissed() {
    }

    public void onRevMobAdClicked() {
    }

    public void onRevMobVideoNotCompletelyLoaded() {
    }

    public void onRevMobVideoLoaded() {
    }

    public void onRevMobVideoStarted() {
    }

    public void onRevMobVideoFinished() {
    }

    public void onRevMobRewardedVideoNotCompletelyLoaded() {
    }

    public void onRevMobRewardedVideoLoaded() {
    }

    public void onRevMobRewardedVideoStarted() {
    }

    public void onRevMobRewardedVideoCompleted() {
    }

    public void onRevMobRewardedVideoFinished() {
    }

    public void onRevMobRewardedPreRollDisplayed() {
    }

    public void onRevMobEulaIsShown() {
    }

    public void onRevMobEulaWasAcceptedAndDismissed() {
    }

    public void onRevMobEulaWasRejected() {
    }
}
