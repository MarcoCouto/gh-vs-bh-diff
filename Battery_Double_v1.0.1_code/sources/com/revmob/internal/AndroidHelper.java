package com.revmob.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;

public class AndroidHelper {
    public static boolean isPermissionEnabled(Context context, String permission) {
        if (context.checkCallingOrSelfPermission("android.permission." + permission) == 0) {
            return true;
        }
        return false;
    }

    public static int dipToPixels(Context context, int dip) {
        return (int) ((((float) dip) * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static boolean isUIThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static boolean isIntentAvailable(Context context, String action) {
        try {
            if (context.getPackageManager().queryIntentActivities(new Intent(action), 65536).size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isIntentAvailable(Context context, Class<?> class1) {
        try {
            if (context.getPackageManager().queryIntentActivities(new Intent(context, class1), 65536).size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
