package com.revmob.internal;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.HttpContext;

public class MarketRedirector {
    private static Pattern AMAZON_URL_PATTERN = Pattern.compile("android\\?p=[a-zA-Z0-9\\.]+");
    private static Pattern GOOGLE_PLAY_URL_PATTERN = Pattern.compile("details\\?id=[a-zA-Z0-9\\.]+");
    private static int TIMEOUT_IN_SECONDS = 30;
    private String entity;
    private AbstractHttpClient httpclient;
    private RedirectHandler redirectHandler;
    private String url;

    static class MarketRedirectHandler extends DefaultRedirectHandler {
        private URI lastRedirectedUri;

        MarketRedirectHandler() {
        }

        public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
            Header locationHeader = response.getLastHeader("Location");
            if (locationHeader != null) {
                String locationUri = locationHeader.getValue();
                if (MarketRedirector.isGooglePlayUri(locationUri) || MarketRedirector.isAmazonAppStoreUri(locationUri)) {
                    try {
                        this.lastRedirectedUri = new URI(locationUri);
                        return false;
                    } catch (URISyntaxException e) {
                        return MarketRedirector.super.isRedirectRequested(response, context);
                    }
                }
            }
            return MarketRedirector.super.isRedirectRequested(response, context);
        }

        public URI getLocationURI(HttpResponse response, HttpContext context) {
            try {
                RMLog.d("Get location URI...");
                this.lastRedirectedUri = MarketRedirector.super.getLocationURI(response, context);
                RMLog.d("getLocationURI: " + this.lastRedirectedUri);
                return this.lastRedirectedUri;
            } catch (ProtocolException e) {
                e.printStackTrace();
                return null;
            }
        }

        public URI getLastRedirectedUrl() {
            return this.lastRedirectedUri;
        }
    }

    public static boolean isGooglePlayUri(String uri) {
        return uri.startsWith("market://");
    }

    public static boolean isAmazonAppStoreUri(String uri) {
        return uri.startsWith("amzn://");
    }

    static String rewriteMarketUrl(String url2) {
        Matcher googleplayMatcher = GOOGLE_PLAY_URL_PATTERN.matcher(url2);
        Matcher amazonMatcher = AMAZON_URL_PATTERN.matcher(url2);
        if (googleplayMatcher.find()) {
            return "market://" + googleplayMatcher.group();
        } else if (!amazonMatcher.find()) {
            return url2;
        } else {
            return "amzn://apps/" + amazonMatcher.group();
        }
    }

    public MarketRedirector(String url2) {
        this(url2, "", new DefaultHttpClient(), new MarketRedirectHandler());
    }

    public MarketRedirector(String url2, String entity2) {
        this(url2, entity2, new DefaultHttpClient(), new MarketRedirectHandler());
    }

    MarketRedirector(String url2, String entity2, AbstractHttpClient client, RedirectHandler handler) {
        this.url = url2;
        this.entity = entity2;
        this.httpclient = client;
        this.redirectHandler = handler;
        this.httpclient.setRedirectHandler(this.redirectHandler);
    }

    public String getMarketUrl() {
        return getMarketUrl(null);
    }

    public String getMarketUrl(String dspUrl) {
        if (dspUrl != null && !dspUrl.endsWith("#click")) {
            return dspUrl;
        }
        if (isGooglePlayUri(this.url) || isAmazonAppStoreUri(this.url)) {
            return this.url;
        }
        post();
        URI locationUrl = this.redirectHandler.getLastRedirectedUrl();
        if (locationUrl != null) {
            return rewriteMarketUrl(locationUrl.toString());
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public HttpResponse post() {
        HTTPHelper httpHelper = new HTTPHelper(this.httpclient);
        httpHelper.setTimeout(TIMEOUT_IN_SECONDS);
        return httpHelper.post(this.url, this.entity);
    }
}
