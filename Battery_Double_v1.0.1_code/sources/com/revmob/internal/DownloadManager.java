package com.revmob.internal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.util.Log;
import com.revmob.ads.interstitial.client.AeSimpleSHA1;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import org.apache.http.util.ByteArrayBuffer;

@SuppressLint({"NewApi"})
public class DownloadManager extends AsyncTask<String, Void, String> {
    private Activity activity;
    private boolean callResponse;
    private AsyncTaskCompleteListener callback;
    private File file;
    private String filePath;
    private String filename;
    private ConnectionHandler handler;
    private boolean isSuccessful;
    private URL url;
    private boolean willCache;

    public DownloadManager(Activity activity2, String downloadURL, String fileName, AsyncTaskCompleteListener callback2) {
        this.activity = activity2;
        this.callback = callback2;
        this.willCache = false;
        this.callResponse = false;
        try {
            this.filename = new AeSimpleSHA1().SHA1(downloadURL);
        } catch (NoSuchAlgorithmException e1) {
            this.filename = fileName;
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e12) {
            this.filename = fileName;
            e12.printStackTrace();
        }
        try {
            this.url = new URL(downloadURL);
        } catch (MalformedURLException e) {
            Log.e("download manager malformed url error, download url", downloadURL);
            e.printStackTrace();
        }
    }

    public DownloadManager(Activity activity2, String downloadURL, String fileName, ConnectionHandler handler2, boolean willCache2, boolean callResponse2) {
        this.activity = activity2;
        this.handler = handler2;
        this.willCache = willCache2;
        this.callResponse = callResponse2;
        try {
            this.filename = new AeSimpleSHA1().SHA1(downloadURL);
        } catch (NoSuchAlgorithmException e1) {
            this.filename = fileName;
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e12) {
            this.filename = fileName;
            e12.printStackTrace();
        }
        try {
            this.url = new URL(downloadURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.e("onPostExecute", "download manager");
        if (this.callback != null) {
            this.callback.onTaskComplete(result);
        } else if (this.callResponse) {
            this.handler.onResponse(result, this.isSuccessful);
        }
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... params) {
        try {
            if (!this.willCache) {
                RMLog.d("Download Manager : wont cache file");
            } else {
                RMLog.d("Download Manager : will cache file");
            }
            createFilePath(this.activity);
            this.file = new File(this.filePath, this.filename);
            this.filePath = this.file.getAbsolutePath();
            if (!this.file.exists()) {
                long startTime = System.currentTimeMillis();
                RMLog.d("Download begining");
                RMLog.d("Download url:" + this.url);
                RMLog.d("Downloaded file name:" + this.filename);
                RMLog.d("filepath " + this.filePath);
                BufferedInputStream bis = new BufferedInputStream(this.url.openConnection().getInputStream());
                ByteArrayBuffer baf = new ByteArrayBuffer(50);
                while (true) {
                    int current = bis.read();
                    if (current == -1) {
                        break;
                    }
                    baf.append((byte) current);
                }
                FileOutputStream fos = new FileOutputStream(this.file);
                fos.write(baf.toByteArray());
                fos.close();
                if (VERSION.SDK_INT >= 9 && VERSION.SDK_INT < 15) {
                    this.file.setReadable(true, false);
                }
                RMLog.d("Download ready in " + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
            } else {
                Log.e("download Managaer", "file alreadyExists");
                Log.e("file path", "" + this.filePath);
                Log.e("file name", "" + this.filename);
            }
            this.isSuccessful = true;
        } catch (IOException e) {
            this.isSuccessful = false;
            RMLog.i("Download Error: " + e);
            e.printStackTrace();
        }
        return null;
    }

    private void createFilePath(Activity activity2) {
        if (!this.willCache) {
            this.filePath = activity2.getApplicationContext().getFilesDir().getPath();
        } else {
            this.filePath = activity2.getApplicationContext().getCacheDir().getPath();
        }
    }

    public String getFilePath() {
        return this.filePath;
    }

    public File getFile() {
        return this.file;
    }

    public boolean getIsSuccessful() {
        return this.isSuccessful;
    }
}
