package com.revmob.internal;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import java.io.File;
import java.io.IOException;

public class RevMobVideoPlayer implements AsyncTaskCompleteListener {
    private DownloadManager downloader;
    /* access modifiers changed from: private */
    public MediaPlayer media;

    public void playFullscreenVideo(Activity activity, String urlString) throws IOException {
        playVideoFromUrl(activity, urlString);
    }

    private void playVideoFromUrl(Activity activity, String url) {
        if (url != null && url.length() > 0) {
            this.downloader = new DownloadManager(activity, url, url.substring(url.lastIndexOf(47) + 1), this);
            this.downloader.execute(new String[0]);
        }
    }

    public void onTaskComplete(String result) {
        if (this.downloader.getIsSuccessful()) {
            this.media = new MediaPlayer();
            File file = this.downloader.getFile();
            Log.e("onTaskComplete", "revmobvideoplayer");
            this.media.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    RevMobVideoPlayer.this.media.release();
                }
            });
        }
    }
}
