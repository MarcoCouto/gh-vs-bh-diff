package com.revmob.internal;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class RevMobSoundPlayer implements AsyncTaskCompleteListener {
    private DownloadManager downloader;
    /* access modifiers changed from: private */
    public MediaPlayer media;

    public void playFullscreenSound(Activity activity, String urlString) throws IOException {
        playSoundFromUrl(activity, urlString);
    }

    public void playBannerSound(Activity activity, String urlString) throws IOException {
        playSoundFromUrl(activity, urlString);
    }

    public void playPopupSound(Activity activity, String urlString) throws IOException {
        playSoundFromUrl(activity, urlString);
    }

    private void playSoundFromUrl(Activity activity, String url) {
        if (url != null && url.length() > 0) {
            this.downloader = new DownloadManager(activity, url, url.substring(url.lastIndexOf(47) + 1), this);
            this.downloader.execute(new String[0]);
        }
    }

    public void onTaskComplete(String result) {
        if (this.downloader.getIsSuccessful()) {
            this.media = new MediaPlayer();
            File file = this.downloader.getFile();
            this.media.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    RevMobSoundPlayer.this.media.release();
                }
            });
            try {
                FileInputStream fis = new FileInputStream(file);
                this.media.setDataSource(fis.getFD());
                this.media.prepare();
                this.media.start();
                fis.close();
            } catch (IOException e) {
                RMLog.d(e.toString());
            }
        }
    }
}
