package com.revmob.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.revmob.android.RevMobContext;
import com.revmob.android.RevMobContext.RUNNING_APPS_STATUS;
import org.json.JSONObject;

public class RevMobServicesManager extends BroadcastReceiver {
    public static JSONObject config;
    public static Context context;

    public static Context getMainContext() {
        return context;
    }

    public static void initialize(Context ctx, JSONObject runningAppsConfig) {
        context = ctx;
        config = runningAppsConfig;
        setServiceStatus();
        Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        editor.putString("runningAppsConfiguration", runningAppsConfig.toString());
        editor.putString("deviceIdentifierJSON", RevMobContext.deviceIdentifierJSON().toString());
        editor.apply();
        ctx.startService(new Intent(context, RevMobServices.class));
    }

    public void onReceive(Context ctx, Intent intent) {
        ctx.startService(new Intent(ctx, RevMobServices.class));
    }

    private static void setServiceStatus() {
        int status = config.optInt("status");
        RevMobContext.runningAppsStatus = RUNNING_APPS_STATUS.NOTIFY_AND_FETCH;
        if (status >= 0 && status <= 2) {
            RevMobContext.runningAppsStatus = RUNNING_APPS_STATUS.values()[status];
        }
        if (status == 0) {
            context.stopService(new Intent(context, RevMobServices.class));
        }
    }
}
