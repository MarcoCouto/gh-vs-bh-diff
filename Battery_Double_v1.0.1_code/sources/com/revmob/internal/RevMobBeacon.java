package com.revmob.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.revmob.android.RevMobContext;
import org.json.JSONObject;

public class RevMobBeacon extends BroadcastReceiver {
    public static JSONObject config;
    public static Context context;

    public static Context getMainContext() {
        return context;
    }

    public static void initialize(Context ctx, JSONObject beaconConfig) {
        context = ctx;
        config = beaconConfig;
        Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        editor.putString("beaconConfiguration", beaconConfig.toString());
        editor.putString("revmobJSON", RevMobContext.revmobJSON().toString());
        editor.apply();
        ctx.startService(new Intent(context, RevMobBeaconManager.class));
    }

    public void onReceive(Context ctx, Intent intent) {
        RMLog.i("BroadcastReceiver received!!");
        ctx.startService(new Intent(ctx, RevMobBeaconManager.class));
    }
}
