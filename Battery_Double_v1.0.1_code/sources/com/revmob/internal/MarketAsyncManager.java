package com.revmob.internal;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.webkit.WebView;
import com.revmob.FullscreenActivity;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.interstitial.internal.FullscreenWebview;
import com.revmob.client.AdData;
import org.apache.http.HttpResponse;

public class MarketAsyncManager extends AsyncTask<Void, Void, Void> {
    /* access modifiers changed from: private */
    public Activity activity;
    private AdData data;
    private boolean firstClick;
    private MarketAsyncManagerListener listener;
    private RevMobAdsListener publisherListener;
    private String relativePosition;

    public interface MarketAsyncManagerListener {
        void onPostExecute();

        void onPreExecute();
    }

    public MarketAsyncManager(Activity activity2, AdData data2, RevMobAdsListener publisherListener2) {
        this(activity2, data2, publisherListener2, null);
    }

    public MarketAsyncManager(Activity activity2, AdData data2, RevMobAdsListener publisherListener2, MarketAsyncManagerListener listener2) {
        this(activity2, data2, publisherListener2, null, null);
    }

    public MarketAsyncManager(Activity activity2, AdData data2, RevMobAdsListener publisherListener2, MarketAsyncManagerListener listener2, String relativeVideoPosition) {
        this.firstClick = true;
        this.relativePosition = null;
        this.activity = activity2;
        this.publisherListener = publisherListener2;
        this.data = data2;
        this.listener = listener2;
        this.relativePosition = relativeVideoPosition;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Void... params) {
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdClicked();
        }
        openAdvertisement();
        return null;
    }

    public void openAdvertisement() {
        if (this.data.shouldFollowRedirect()) {
            openClickUrlFollowingRedirect(this.data.getClickUrl(this.relativePosition));
        } else {
            openClickUrlDirectly(this.data.getClickUrl(this.relativePosition));
        }
    }

    public void openClickUrlFollowingRedirect(String clickUrl) {
        String marketUrl = new MarketRedirector(clickUrl).getMarketUrl(this.data.getDspUrl());
        if (this.firstClick && this.data.getClickUrl() != null && this.data.getDspUrl() != null && !this.data.getDspUrl().endsWith("#click")) {
            if (this.relativePosition != null) {
                dspServerRequest(this.data.getClickUrl(this.relativePosition), "");
            } else {
                dspServerRequest(this.data.getClickUrl(this.relativePosition), "");
            }
        }
        if (marketUrl == null) {
            RMLog.e("Redirect link not received.");
            this.publisherListener.onRevMobAdNotReceived("Redirect link not received.");
        } else if (marketUrl != null && !clickUrl.equals(marketUrl)) {
            boolean activityAvailable = FullscreenActivity.isFullscreenActivityAvailable(this.activity).booleanValue();
            if (this.data.getAppOrSite() != "site" || !this.data.isOpenInside() || !activityAvailable) {
                openUrlInTheBrowser(marketUrl);
            } else {
                openUrlInTheWebView(marketUrl);
            }
        }
    }

    public void openClickUrlDirectly(String clickUrl) {
        if (this.data.isOpenInside()) {
            openUrlInTheWebViewWithPost(this.data.getClickUrl(this.relativePosition));
        } else {
            openUrlInTheBrowser(this.data.getClickUrl(this.relativePosition));
        }
    }

    public void openUrlInTheWebView(final String url) {
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                Intent i = new Intent(MarketAsyncManager.this.activity, FullscreenActivity.class);
                i.putExtra("marketURL", url);
                MarketAsyncManager.this.activity.startActivityForResult(i, 0);
            }
        });
    }

    public void openUrlInTheWebViewWithPost(final String url) {
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                new FullscreenWebview(MarketAsyncManager.this.activity, new RevMobWebViewClient(null, null) {
                    public void onPageFinished(WebView view, String url) {
                        MarketAsyncManager.this.openUrlInTheBrowser(url);
                    }
                }).postUrl(url, "".getBytes());
            }
        });
    }

    public void openUrlInTheBrowser(String url) {
        try {
            this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
        } catch (Exception e) {
            if (url.startsWith("market://")) {
                this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + url.substring(20))));
            } else if (!url.startsWith("http")) {
                this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://" + url)));
            } else {
                RMLog.e("Click url ( " + url + " ) not valid. Please report this to support@revmob.com with the exception stack trace: ", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.listener != null) {
            this.listener.onPreExecute();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Void result) {
        if (this.listener != null) {
            this.listener.onPostExecute();
        }
    }

    private void dspServerRequest(final String url, final String payload) {
        this.firstClick = false;
        new Thread() {
            public void run() {
                HttpResponse post = new HTTPHelper().post(url, payload);
            }
        }.start();
    }
}
