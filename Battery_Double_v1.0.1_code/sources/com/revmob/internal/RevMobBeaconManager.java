package com.revmob.internal;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.revmob.client.RevMobClient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RevMobBeaconManager extends Service implements BootstrapNotifier, RangeNotifier {
    private int BKGND_SCAN_PERIOD = 5000;
    private int BTW_BKGND_SCAN_PERIOD = 2000;
    private int BTW_FRGND_SCAN_PERIOD = 5000;
    private int FRGND_SCAN_PERIOD = 2000;
    Analytics analytics;
    /* access modifiers changed from: private */
    public ArrayList<Object> beaconData = new ArrayList<>();
    private String beaconLayout = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
    RevMobFullscreen fullscreen;
    private ArrayList<Identifier> identifiers = new ArrayList<>();
    private BeaconManager mBeaconManager;
    private Beacon mClosestBeacon = null;
    private HashMap<String, Object> mConfig;
    private RegionBootstrap mRegionBootstrap;
    private JSONObject mRevmobJSON;
    boolean monitoring;
    double now = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private HashMap<Region, HashMap<String, Beaconx>> rangedRegions;
    boolean ranging;
    RealTime realTime;
    private ArrayList<Region> regions = new ArrayList<>();
    private ArrayList<String> uuidArray;

    class Analytics {
        int distance;
        double lastRequest;
        double lastSample;
        int requestFreq;
        int sampleFreq;
        int timeout;
        String url;

        Analytics() {
        }
    }

    class Beaconx {
        Beacon b;
        double timeout;

        public Beaconx(Beacon b2) {
            this.b = b2;
        }
    }

    class RealTime {
        int distance;
        double lastDidRange;
        String url;

        RealTime() {
        }
    }

    public RevMobBeaconManager() {
        RMLog.i("RevMobBeaconManager constructor");
        this.rangedRegions = new HashMap<>();
        this.analytics = new Analytics();
        this.realTime = new RealTime();
    }

    public void startBeaconScan() {
        RMLog.i("startBeaconScan");
        Analytics analytics2 = this.analytics;
        Analytics analytics3 = this.analytics;
        RealTime realTime2 = this.realTime;
        double now2 = getNow();
        realTime2.lastDidRange = now2;
        analytics3.lastSample = now2;
        analytics2.lastRequest = now2;
        Iterator i$ = this.uuidArray.iterator();
        while (i$.hasNext()) {
            String uuid = (String) i$.next();
            this.regions.add(new Region(uuid, Identifier.parse(uuid), null, null));
            this.identifiers.add(Identifier.parse(uuid));
        }
        if (getApplicationContext() != null) {
            this.mBeaconManager = BeaconManager.getInstanceForApplication(this);
            this.mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(this.beaconLayout));
        }
        this.mRegionBootstrap = new RegionBootstrap((BootstrapNotifier) this, (List<Region>) this.regions);
        startMonitoringBeacons();
    }

    private void startMonitoringBeacons() {
        if (this.monitoring) {
            this.mBeaconManager.setBackgroundScanPeriod((long) this.BKGND_SCAN_PERIOD);
            this.mBeaconManager.setBackgroundBetweenScanPeriod((long) this.BTW_BKGND_SCAN_PERIOD);
            this.mBeaconManager.setForegroundScanPeriod((long) this.FRGND_SCAN_PERIOD);
            this.mBeaconManager.setForegroundBetweenScanPeriod((long) this.BTW_FRGND_SCAN_PERIOD);
        }
    }

    public HashMap<String, Object> createRegionData(Region region) {
        HashMap<String, Object> regionJSON = new HashMap<>();
        regionJSON.put("identifier", region.getUniqueId());
        return regionJSON;
    }

    public HashMap<String, Object> createBeaconData(Beacon beacon) {
        HashMap<String, Object> beaconJSON = new HashMap<>();
        beaconJSON.put("accuracy", Double.valueOf(((double) Math.round(beacon.getDistance() * 1000.0d)) / 1000.0d));
        beaconJSON.put("rssi", Integer.valueOf(beacon.getRssi()));
        beaconJSON.put("major", Integer.valueOf(beacon.getId2().toInt()));
        beaconJSON.put("minor", Integer.valueOf(beacon.getId3().toInt()));
        beaconJSON.put("mac", beacon.getBluetoothAddress());
        beaconJSON.put("txpower", Integer.valueOf(beacon.getTxPower()));
        beaconJSON.put("bltName", beacon.getBluetoothName());
        beaconJSON.put("manufacturer", Integer.valueOf(beacon.getManufacturer()));
        return beaconJSON;
    }

    public void didDetermineStateForRegion(int state, Region region) {
        RMLog.i("didDetermineStateForRegion " + state + " " + region.toString());
        HashMap<String, Object> regionJSON = createRegionData(region);
        if (state == 1) {
            try {
                if (this.ranging) {
                    this.mBeaconManager.startRangingBeaconsInRegion(region);
                    this.mBeaconManager.setRangeNotifier(this);
                    regionJSON.put("event", "didDetermineState");
                    regionJSON.put("state", Integer.valueOf(state));
                    regionJSON.put("timestamp", Double.valueOf(getNow()));
                    notifyBeaconData(new JSONObject(regionJSON), this.realTime.url);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }
        }
        this.mBeaconManager.stopRangingBeaconsInRegion(region);
        regionJSON.put("event", "didDetermineState");
        regionJSON.put("state", Integer.valueOf(state));
        regionJSON.put("timestamp", Double.valueOf(getNow()));
        notifyBeaconData(new JSONObject(regionJSON), this.realTime.url);
    }

    public void didEnterRegion(Region region) {
        RMLog.i("didEnterRegion " + region.toString());
        HashMap<String, Object> regionJSON = createRegionData(region);
        regionJSON.put("event", "didEnterRegion");
        regionJSON.put("timestamp", Double.valueOf(getNow()));
        notifyBeaconData(new JSONObject(regionJSON), this.realTime.url);
    }

    public void didExitRegion(Region region) {
        RMLog.i("didExitRegion " + region.toString());
        HashMap<String, Object> regionJSON = createRegionData(region);
        regionJSON.put("event", "didExitRegion");
        regionJSON.put("timestamp", Double.valueOf(getNow()));
        notifyBeaconData(new JSONObject(regionJSON), this.realTime.url);
    }

    public double getNow() {
        return (double) (System.currentTimeMillis() / 1000);
    }

    public void collectData() {
        ArrayList<Object> regionData = new ArrayList<>();
        HashMap<String, Object> sampleJSON = new HashMap<>();
        sampleJSON.put("timestamp", Double.valueOf(this.now));
        sampleJSON.put("event", "didRangeBeacons");
        for (Entry<Region, HashMap<String, Beaconx>> entry : this.rangedRegions.entrySet()) {
            HashMap<String, Object> regionJSON = createRegionData((Region) entry.getKey());
            ArrayList<HashMap<String, Object>> selectedBeacons = new ArrayList<>();
            for (Entry<String, Beaconx> entry2 : ((HashMap) entry.getValue()).entrySet()) {
                Beaconx beacon = (Beaconx) entry2.getValue();
                if (beacon.b.getDistance() <= ((double) this.analytics.distance) && beacon.b.getDistance() >= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                    selectedBeacons.add(createBeaconData(beacon.b));
                }
            }
            regionJSON.put("beacons", selectedBeacons);
            if (selectedBeacons.size() > 0) {
                regionData.add(regionJSON);
            }
        }
        sampleJSON.put("regions", regionData);
        this.beaconData.add(sampleJSON);
        if (this.now - this.analytics.lastRequest > ((double) this.analytics.requestFreq) && this.beaconData.size() != 0) {
            this.analytics.lastRequest = this.now;
            notifyBeaconData(new JSONArray(this.beaconData), this.analytics.url, new Runnable() {
                public void run() {
                    RevMobBeaconManager.this.beaconData.clear();
                }
            });
        }
    }

    public void checkClosestBeacon() {
        Beacon closest = null;
        for (Entry<Region, HashMap<String, Beaconx>> entry : this.rangedRegions.entrySet()) {
            Iterator<Entry<String, Beaconx>> it = ((HashMap) entry.getValue()).entrySet().iterator();
            while (it.hasNext()) {
                Beaconx beacon = (Beaconx) ((Entry) it.next()).getValue();
                if (beacon.b.getDistance() > FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE && beacon.b.getDistance() < ((double) this.realTime.distance) && this.now < beacon.timeout && (closest == null || closest.getRssi() < beacon.b.getRssi())) {
                    closest = beacon.b;
                }
                if (this.now > beacon.timeout) {
                    it.remove();
                }
            }
        }
        if (closest == null) {
            return;
        }
        if (this.mClosestBeacon == null || !this.mClosestBeacon.equals(closest)) {
            this.mClosestBeacon = closest;
            HashMap<String, Object> beaconJSON = createBeaconData(this.mClosestBeacon);
            beaconJSON.put("event", "closestBeacon");
            beaconJSON.put("uuid", this.mClosestBeacon.getId1().toString());
            beaconJSON.put("timestamp", Double.valueOf(getNow()));
            notifyBeaconData(new JSONObject(beaconJSON), this.realTime.url);
        }
    }

    public void printBeacon(Beacon b) {
        RMLog.d("UUID: " + b.getId1() + " Major: " + b.getId2() + " Minor: " + b.getId3() + " RSSI: " + b.getRssi());
    }

    public void printRangedRegions() {
        for (Entry<Region, HashMap<String, Beaconx>> entry : this.rangedRegions.entrySet()) {
            for (Entry<String, Beaconx> entry2 : ((HashMap) entry.getValue()).entrySet()) {
                printBeacon(((Beaconx) entry2.getValue()).b);
            }
        }
    }

    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
        this.now = getNow();
        RMLog.i("didRangeBeaconsInRegion " + beacons.size() + " " + region.toString());
        if (beacons.size() != 0) {
            HashMap<String, Beaconx> regionList = (HashMap) this.rangedRegions.get(region);
            if (regionList == null) {
                regionList = new HashMap<>();
            }
            for (Beacon b : beacons) {
                Beaconx g = new Beaconx(b);
                g.timeout = this.now + ((double) this.analytics.timeout);
                regionList.put(b.getId1() + "|" + b.getId2() + "|" + b.getId3(), g);
            }
            this.rangedRegions.put(region, regionList);
            if (this.now - this.realTime.lastDidRange > 0.6d) {
                this.realTime.lastDidRange = this.now;
                checkClosestBeacon();
                if (this.now - this.analytics.lastSample > ((double) this.analytics.sampleFreq)) {
                    this.analytics.lastSample = this.now;
                    collectData();
                }
            }
        }
    }

    public void notifyBeaconData(Object data, String url) {
        notifyBeaconData(data, url, null);
    }

    public void notifyBeaconData(Object data, String url, Runnable bl) {
        try {
            this.mRevmobJSON.put("beaconData", data);
            RevMobClient.getInstance().serverRequest(url, this.mRevmobJSON.toString(), null);
            if (bl != null) {
                RMLog.i("Sending beaconData: " + this.beaconData.size());
                bl.run();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseParameters(JSONObject beaconConfig) {
        this.mConfig = new HashMap<>();
        try {
            this.uuidArray = parseJSONArrayToArrayList(beaconConfig.getJSONArray("uuidArray"));
            this.monitoring = Boolean.parseBoolean(beaconConfig.getString("monitoring"));
            this.ranging = Boolean.parseBoolean(beaconConfig.getString("ranging"));
            this.beaconLayout = beaconConfig.getString("beaconLayout");
            JSONObject jsonAnalytics = beaconConfig.getJSONObject("analytics");
            this.analytics.url = jsonAnalytics.getString("url");
            this.analytics.distance = Integer.parseInt(jsonAnalytics.getString("distance"));
            this.analytics.requestFreq = Integer.parseInt(jsonAnalytics.getString("refresh"));
            this.analytics.sampleFreq = Integer.parseInt(jsonAnalytics.getString("sample"));
            this.analytics.timeout = Integer.parseInt(jsonAnalytics.getString("timeout"));
            JSONObject frequencies = beaconConfig.getJSONObject("frequencies");
            this.BKGND_SCAN_PERIOD = Integer.parseInt(frequencies.getString("background"));
            this.BTW_BKGND_SCAN_PERIOD = Integer.parseInt(frequencies.getString("btwBackground"));
            this.FRGND_SCAN_PERIOD = Integer.parseInt(frequencies.getString("foreground"));
            this.BTW_FRGND_SCAN_PERIOD = Integer.parseInt(frequencies.getString("btwForeground"));
            JSONObject jsonRealTime = beaconConfig.getJSONObject("realTime");
            this.realTime.url = jsonRealTime.getString("url");
            this.realTime.distance = Integer.parseInt(jsonRealTime.getString("distance"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> parseJSONArrayToArrayList(JSONArray jsonArray) throws JSONException {
        ArrayList<String> list = new ArrayList<>();
        if (jsonArray != null) {
            int len = jsonArray.length();
            for (int i = 0; i < len; i++) {
                list.add(jsonArray.get(i).toString());
            }
        }
        return list;
    }

    public IBinder onBind(Intent intent) {
        RMLog.i("Beacon onBind service");
        return null;
    }

    public void onCreate() {
        RMLog.i("Beacons monitoring service created");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            this.mRevmobJSON = new JSONObject(settings.getString("revmobJSON", ""));
            parseParameters(new JSONObject(settings.getString("beaconConfiguration", "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        RMLog.i("Beacons monitoring service destroyed");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        RMLog.i("onStartCommand beacon");
        startBeaconScan();
        return 2;
    }
}
