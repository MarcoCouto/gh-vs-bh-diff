package com.revmob.internal;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.BigPictureStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@SuppressLint({"NewApi"})
public class RevMobNotificationManager {
    private static final String TAG = "RevMobNotificationManager";
    private Context mContext;
    private Bitmap mImage;

    public RevMobNotificationManager(Context context) {
        this.mContext = context;
    }

    public void showBigPictureNotification() {
        this.mImage = getBitmapFromURL("http://beekn.net/wp-content/uploads/2013/12/estimote-2-beacons.png");
        NotificationManager notificationManager = (NotificationManager) this.mContext.getSystemService("notification");
        Notification notif = new Builder(this.mContext).setContentTitle("BigPicture").setContentText("Test").setSmallIcon(17301508).setLargeIcon(this.mImage).setStyle(new BigPictureStyle().bigPicture(this.mImage)).setContentIntent(null).build();
        Log.i(TAG, "showBigPictureNotification");
        notificationManager.notify(1, notif);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(src).openConnection();
            connection.setDoInput(true);
            connection.connect();
            return BitmapFactory.decodeStream(connection.getInputStream());
        } catch (IOException e) {
            return null;
        }
    }
}
