package com.revmob.internal;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.revmob.RevMobAdsListener;

public class RevMobWebViewClient extends WebViewClient {
    private RevMobWebViewClickListener clickListener;
    private RevMobAdsListener publisherListener;

    public interface RevMobWebViewClickListener {
        boolean handleClick(WebView webView, String str);

        void handlePageFinished(WebView webView, String str);
    }

    public RevMobWebViewClient(RevMobAdsListener publisherListener2, RevMobWebViewClickListener clickListener2) {
        this.publisherListener = publisherListener2;
        this.clickListener = clickListener2;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (this.clickListener != null) {
            return this.clickListener.handleClick(view, url);
        }
        return super.shouldOverrideUrlLoading(view, url);
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdNotReceived("Content was not loaded");
        }
    }

    public void onPageFinished(WebView view, String url) {
        if (this.clickListener != null) {
            this.clickListener.handlePageFinished(view, url);
        }
    }
}
