package com.revmob.internal;

import android.util.Log;

public class RMLog {
    private static final String PREFIX = "[RevMob]";

    public static void d(String message) {
        Log.d(PREFIX, message);
    }

    public static void d(String message, Throwable e) {
        Log.d(PREFIX, message, e);
    }

    public static void i(String message) {
        Log.i(PREFIX, message);
    }

    public static void i(String message, Throwable e) {
        Log.d(PREFIX, message, e);
    }

    public static void w(String message) {
        Log.w(PREFIX, message);
    }

    public static void w(String message, Throwable e) {
        Log.d(PREFIX, message, e);
    }

    public static void e(String message) {
        Log.e(PREFIX, message);
    }

    public static void e(String message, Throwable e) {
        Log.d(PREFIX, message, e);
    }
}
