package com.revmob.internal;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable;
import android.webkit.WebView;
import com.revmob.android.RevMobScreen;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

public class HTTPHelper {
    private static String eulaUrl;
    private static String eulaVersion;
    public static int globalTimeoutInSeconds = 30;
    private static String ipAddress;
    private static boolean shouldContinueOnBackground;
    private static boolean shouldExtractGeolocation;
    private static boolean shouldExtractOtherAppsData;
    private static boolean shouldExtractSocial;
    private static boolean shouldShowEula;
    private static String userAgent;
    private AbstractHttpClient httpclient;

    public static void setUserAgent(String ua, Activity activity) {
        if (activity != null) {
            userAgent = System.getProperty("http.agent");
            try {
                if (AndroidHelper.isUIThread()) {
                    userAgent = new WebView(activity).getSettings().getUserAgentString();
                }
            } catch (Exception e) {
                RMLog.e(e.getMessage());
            }
        } else {
            userAgent = ua;
        }
    }

    public static String getUserAgent() {
        return userAgent;
    }

    public static void setIpAddress(String ip) {
        ipAddress = ip;
    }

    public static String getIpAddress() {
        return ipAddress;
    }

    public HTTPHelper() {
        this(new DefaultHttpClient());
    }

    public HTTPHelper(AbstractHttpClient client) {
        this.httpclient = client;
        setTimeout(globalTimeoutInSeconds);
        this.httpclient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    }

    public void setTimeout(int timeoutInSeconds) {
        HttpConnectionParams.setConnectionTimeout(this.httpclient.getParams(), timeoutInSeconds * 1000);
    }

    public HttpResponse get(String url) {
        RMLog.d("Sending GET to " + url);
        try {
            return sendRequestDealingWithSSLErrors(new HttpGet(url));
        } catch (IllegalArgumentException e) {
            RMLog.e("Unknown error", e);
            return null;
        }
    }

    public HttpResponse post(String url, String entity) {
        try {
            RMLog.d("Sending POST to: " + url);
            this.httpclient.getParams().setParameter("http.useragent", userAgent);
            HttpPost httpPost = new HttpPost(url);
            if (RevMobConstants.ENCRYPTION.booleanValue()) {
                httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
                httpPost.addHeader("x-revmob-crypt", RevMobConstants.REVMOB_ENCRYPTION_TYPE);
                entity = new RevMobEncryption().encrypt(entity);
            } else {
                httpPost.setHeader("Content-type", "application/json");
            }
            httpPost.addHeader("User-Agent", userAgent);
            httpPost.setEntity(new StringEntity(entity, "UTF-8"));
            return sendRequestDealingWithSSLErrors(httpPost);
        } catch (UnsupportedEncodingException e) {
            RMLog.w("Encoding error.", e);
            return null;
        } catch (RuntimeException e2) {
            RMLog.e("Unknown error", e2);
            return null;
        }
    }

    public InputStream getAndReturnTheStream(String url) {
        try {
            HttpResponse response = get(url);
            if (!(response == null || response.getEntity() == null)) {
                return response.getEntity().getContent();
            }
        } catch (IllegalStateException e) {
            RMLog.e("Read error.");
        } catch (IOException e2) {
            RMLog.w("Read error.");
        }
        return null;
    }

    public static String encodedResponseBody(HttpEntity entity) {
        if (entity == null) {
            return null;
        }
        String response = "";
        try {
            StringBuffer sb = new StringBuffer(1024);
            InputStreamReader isr = new InputStreamReader(entity.getContent(), "UTF-8");
            char[] buff = new char[1024];
            while (true) {
                int cnt = isr.read(buff, 0, 1023);
                if (cnt > 0) {
                    sb.append(buff, 0, cnt);
                } else {
                    response = sb.toString();
                    isr.close();
                    return response;
                }
            }
        } catch (IOException e) {
            RMLog.w("Read error.");
            return response;
        }
    }

    private HttpResponse sendRequest(HttpRequestBase requestInfo) throws SSLException {
        try {
            return this.httpclient.execute(requestInfo);
        } catch (UnknownHostException e) {
            RMLog.w("Error on requesting path " + requestInfo.getRequestLine() + ". Is the device connected to the internet?", e);
        } catch (HttpHostConnectException e2) {
            throw new SSLException(e2);
        } catch (SSLPeerUnverifiedException e3) {
            throw new SSLException(e3);
        } catch (SocketException e4) {
            RMLog.w("Server took too long to respond.");
        } catch (SSLException e5) {
            throw e5;
        } catch (IOException e6) {
            RMLog.w("Error on requesting path " + requestInfo.getRequestLine() + ". Did the device lost its connection?", e6);
        }
        return null;
    }

    private HttpResponse sendRequestDealingWithSSLErrors(HttpRequestBase requestInfo) {
        try {
            return sendRequest(requestInfo);
        } catch (SSLException e) {
            try {
                if (requestInfo.getURI().toString().startsWith("https://")) {
                    requestInfo.setURI(new URI(requestInfo.getURI().toString().replace("https://", "http://")));
                    return sendRequest(requestInfo);
                }
            } catch (SSLException e2) {
                RMLog.i("Problem with SSL. What is the version of your Android?");
            } catch (URISyntaxException e3) {
                RMLog.e("Invalid url: " + requestInfo.getURI().toString());
            }
            return null;
        }
    }

    public Drawable downloadImage(String url) {
        return Drawable.createFromStream(getAndReturnTheStream(url), "src");
    }

    public int[] getImageSize(String url) {
        InputStream stream = getAndReturnTheStream(url);
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(stream, null, options);
        int imageWidth = options.outWidth;
        int imageHeight = options.outHeight;
        try {
            stream.close();
        } catch (IOException e) {
        }
        return new int[]{imageWidth, imageHeight};
    }

    public static int calculateInSampleSize(int[] imageSize, int reqWidth, int reqHeight) {
        int width = imageSize[0];
        int height = imageSize[1];
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int heightRatio = Math.round(((float) height) / ((float) reqHeight));
            int widthRatio = Math.round(((float) width) / ((float) reqWidth));
            if (heightRatio < widthRatio) {
                inSampleSize = heightRatio;
            } else {
                inSampleSize = widthRatio;
            }
        }
        if (inSampleSize < 4) {
            return inSampleSize;
        }
        return 5;
    }

    public Bitmap downloadBitmap(String url, String imageSizeString) {
        int[] imageSize = new int[2];
        if (imageSizeString == null) {
            imageSize = getImageSize(url);
        } else {
            try {
                String[] parts = imageSizeString.split(",");
                imageSize[0] = Integer.parseInt(parts[0]);
                imageSize[1] = Integer.parseInt(parts[1]);
            } catch (Exception e) {
                imageSize[0] = 320;
                imageSize[1] = 50;
            }
        }
        int inSampleSize = calculateInSampleSize(imageSize, RevMobScreen.getScreenWidth(), RevMobScreen.getScreenHeight());
        InputStream stream = getAndReturnTheStream(url);
        Options options = new Options();
        options.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(stream, null, options);
    }

    public String downloadHtml(String url) {
        HttpResponse response = get(url);
        if (response == null || response.getEntity() == null) {
            return null;
        }
        return encodedResponseBody(response.getEntity());
    }

    public static void setShouldExtractSocial(boolean shouldExtractSocial2) {
        shouldExtractSocial = shouldExtractSocial2;
    }

    public static void setShouldExtractGeolocation(boolean shouldExtractGeolocation2) {
        shouldExtractGeolocation = shouldExtractGeolocation2;
    }

    public static void setShouldExtractOtherAppsData(boolean shouldExtractOtherAppsData2) {
        shouldExtractOtherAppsData = shouldExtractOtherAppsData2;
    }

    public static void setShouldContinueOnBackground(boolean shouldContinueOnBackground2) {
        shouldContinueOnBackground = shouldContinueOnBackground2;
    }

    public static void setShouldShowEula(boolean shouldShowEula2, Activity activity) {
        if (activity != null) {
            shouldShowEula = false;
        } else {
            shouldShowEula = shouldShowEula2;
        }
    }

    public static boolean getShouldExtractSocial() {
        return shouldExtractSocial;
    }

    public static boolean getShouldExtractGeolocation() {
        return shouldExtractGeolocation;
    }

    public static boolean getShouldExtractOtherAppsData() {
        return shouldExtractOtherAppsData;
    }

    public static boolean getShouldContinueOnBackground() {
        return shouldContinueOnBackground;
    }

    public static boolean getShouldShowEula() {
        return shouldShowEula;
    }

    public static void setEulaUrl(String url, Activity activity) {
        if (activity != null) {
            eulaUrl = "https://s3.amazonaws.com/www.revmob.com/Revmob_i_agree_terms.txt";
        } else {
            eulaUrl = url;
        }
    }

    public static void setEulaVersion(String version, Activity activity) {
        if (activity != null) {
            eulaVersion = "default";
        } else {
            eulaVersion = version;
        }
    }

    public static String getEulaUrl() {
        return eulaUrl;
    }

    public static String getEulaVersion() {
        return eulaVersion;
    }
}
