package com.revmob.internal;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.revmob.RevMobAdsListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class RevMobEula implements AsyncTaskCompleteListener {
    private String EULA_PREFIX = "RevMobEula_v";
    private DialogInterface dialog;
    private DownloadManager downloader;
    private String eulaKey;
    private Activity mActivity;
    private String message;
    private SharedPreferences prefs;
    private RevMobAdsListener revmobListener;
    private boolean wasDismissed;

    public RevMobEula(Activity context, RevMobAdsListener listener) {
        this.mActivity = context;
        this.revmobListener = listener;
    }

    public void loadAndShow() {
        if (HTTPHelper.getShouldShowEula()) {
            this.wasDismissed = false;
            this.eulaKey = this.EULA_PREFIX + HTTPHelper.getEulaVersion();
            this.prefs = PreferenceManager.getDefaultSharedPreferences(this.mActivity);
            if (!this.prefs.getBoolean(this.eulaKey, false)) {
                load();
            }
        }
    }

    private void show() {
        this.dialog = new Builder(this.mActivity).setTitle("RevMob EULA").setMessage(this.message).setPositiveButton("I agree", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                RevMobEula.this.acceptAndDismiss();
            }
        }).setNegativeButton("I don't agree", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RevMobEula.this.reject();
            }
        }).create();
        ((Dialog) this.dialog).show();
        if (this.revmobListener != null) {
            this.revmobListener.onRevMobEulaIsShown();
        }
    }

    private void load() {
        String url = HTTPHelper.getEulaUrl();
        if (url != null && url.length() > 0) {
            this.downloader = new DownloadManager(this.mActivity, url, url.substring(url.lastIndexOf(47) + 1), this);
            this.downloader.execute(new String[0]);
        }
    }

    public void onTaskComplete(String result) {
        if (this.downloader.getIsSuccessful()) {
            File file = this.downloader.getFile();
            try {
                FileInputStream fis = new FileInputStream(file);
                StringBuffer messageContent = new StringBuffer("");
                byte[] buffer = new byte[((int) file.length())];
                while (true) {
                    int n = fis.read(buffer);
                    if (n != -1) {
                        messageContent.append(new String(buffer, 0, n));
                    } else {
                        this.message = messageContent.toString();
                        fis.close();
                        show();
                        return;
                    }
                }
            } catch (IOException e) {
                RMLog.d(e.toString());
            }
        }
    }

    public void acceptAndDismiss() {
        if (!this.wasDismissed) {
            RMLog.i("Eula accepted and dismissed.");
            Editor editor = this.prefs.edit();
            editor.putBoolean(this.eulaKey, true);
            editor.commit();
            this.dialog.dismiss();
            this.wasDismissed = true;
            if (this.revmobListener != null) {
                this.revmobListener.onRevMobEulaWasAcceptedAndDismissed();
            }
        }
    }

    public void reject() {
        if (!this.wasDismissed) {
            RMLog.i("Eula rejected.");
            this.wasDismissed = true;
            if (this.revmobListener != null) {
                this.revmobListener.onRevMobEulaWasRejected();
            }
            this.mActivity.setResult(0);
            this.mActivity.finish();
            System.exit(0);
        }
    }
}
