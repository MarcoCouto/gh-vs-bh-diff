package com.revmob.internal;

import android.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class RevMobEncryption {
    private int MINIMIUM_DATA_LENGTH = 24;

    public String encrypt(String data) {
        if (!RevMobConstants.ENCRYPTION.booleanValue() || data.length() <= 0) {
            return data;
        }
        byte[] key = hexStringToByteArray("f188c2f6176602368ab346d0b40f1098ed350c3c46595e9981a8db1db9d865b7");
        byte[] iv = hexStringToByteArray("3066546c3043314e614c4b764f433338");
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES/CBC/PKCS5Padding");
        byte[] encrypted = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, skeySpec, new IvParameterSpec(iv));
            encrypted = cipher.doFinal(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(encrypted, 2);
    }

    public String decrypt(String data) {
        if (!RevMobConstants.ENCRYPTION.booleanValue() || data.length() < this.MINIMIUM_DATA_LENGTH) {
            return data;
        }
        byte[] key = hexStringToByteArray("f188c2f6176602368ab346d0b40f1098ed350c3c46595e9981a8db1db9d865b7");
        byte[] iv = hexStringToByteArray("3066546c3043314e614c4b764f433338");
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES/CBC/PKCS5Padding");
        byte[] encrypted = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(2, skeySpec, new IvParameterSpec(iv));
            encrypted = cipher.doFinal(Base64.decode(data, 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(encrypted);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[(len / 2)];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
