package com.revmob.internal;

import android.content.Context;
import android.webkit.WebView;

public class RevMobWebView extends WebView {
    public RevMobWebView(Context context, RevMobWebViewClient client) {
        this(context, null, null, client);
    }

    public RevMobWebView(Context context, String url, String html, RevMobWebViewClient client) {
        super(context);
        setScrollContainer(false);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        getSettings().setAppCacheEnabled(false);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setLoadsImagesAutomatically(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getSettings().setSaveFormData(false);
        getSettings().setSavePassword(false);
        if (client != null) {
            setWebViewClient(client);
        }
        if (html != null && !html.equals("")) {
            loadData(html, "text/html; charset=UTF-8", "utf-8");
        } else if (url != null) {
            loadUrl(url);
        }
    }

    public void loadData(String data, String mimeType, String encoding) {
        super.loadData(solveAndroidBug(data), mimeType, encoding);
    }

    private String solveAndroidBug(String data) {
        StringBuilder sb = new StringBuilder(data.length() + 100);
        char[] dataChars = data.toCharArray();
        for (int i = 0; i < dataChars.length; i++) {
            char ch = data.charAt(i);
            switch (ch) {
                case '#':
                    sb.append("%23");
                    break;
                case '%':
                    sb.append("%25");
                    break;
                case '\'':
                    sb.append("%27");
                    break;
                default:
                    sb.append(ch);
                    break;
            }
        }
        return sb.toString();
    }
}
