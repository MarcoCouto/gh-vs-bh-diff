package com.revmob.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.facebook.Session;

public class RevMobSocialInfo {
    public String getFacebookToken(Context context) {
        Session session = Session.getActiveSession();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = settings.edit();
        if (session != null && session.isOpened()) {
            editor.putString("facebookToken", session.getAccessToken());
            editor.commit();
        }
        return settings.getString("facebookToken", null);
    }
}
