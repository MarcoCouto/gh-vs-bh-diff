package com.revmob.internal;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.revmob.client.RevMobClient;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RevMobServices extends Service {
    private int NOTIFY_FREQUENCY;
    private String NOTIFY_URL;
    private int SCAN_FREQUENCY;
    Handler handler = new Handler();
    private JSONObject mRevmobJSON;
    /* access modifiers changed from: private */
    public Runnable notifyService;
    /* access modifiers changed from: private */
    public Runnable runningAppsService;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        RMLog.i("onStartCommand beacon");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            this.mRevmobJSON = new JSONObject(settings.getString("deviceIdentifierJSON", ""));
            parseParameters(new JSONObject(settings.getString("runningAppsConfiguration", "")));
            startRunningAppsService(this.SCAN_FREQUENCY, this.NOTIFY_FREQUENCY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 2;
    }

    private void startRunningAppsService(int scanFrequency, int notifyFrequency) {
        if (scanFrequency > 0) {
            startScanRunningApps(scanFrequency);
        }
        if (notifyFrequency > 0) {
            startNotifyRunningApps(notifyFrequency);
        }
    }

    private void startScanRunningApps(final int scanFrequency) {
        this.runningAppsService = new Runnable() {
            public void run() {
                RevMobServices.this.detectRunningApps();
                RevMobServices.this.handler.postDelayed(RevMobServices.this.runningAppsService, (long) (scanFrequency * 1000));
            }
        };
        this.runningAppsService.run();
    }

    private void startNotifyRunningApps(final int notifyFrequency) {
        this.notifyService = new Runnable() {
            public void run() {
                RevMobServices.this.notifyRunningApps();
                RevMobServices.this.handler.postDelayed(RevMobServices.this.notifyService, (long) (notifyFrequency * 1000));
            }
        };
        this.notifyService.run();
    }

    /* access modifiers changed from: private */
    public void detectRunningApps() {
        List<RunningAppProcessInfo> procInfos = ((ActivityManager) getSystemService("activity")).getRunningAppProcesses();
        ArrayList<String> runningApps = new ArrayList<>();
        for (int i = 0; i < procInfos.size(); i++) {
            runningApps.add(((RunningAppProcessInfo) procInfos.get(i)).processName);
        }
        saveRunningApps(runningApps);
        RMLog.i("fetchRunningApps");
    }

    private void saveRunningApps(ArrayList<String> runningApps) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Editor editor = prefs.edit();
        JSONArray savedRunningApps = new JSONArray();
        try {
            JSONArray savedRunningApps2 = new JSONArray(prefs.getString("runningApps", new JSONArray().toString()));
            try {
                JSONObject currData = new JSONObject();
                currData.put("apps", new JSONArray(runningApps));
                currData.put("timestamp", getCurrentTimestamp());
                savedRunningApps2.put(currData);
                savedRunningApps = savedRunningApps2;
            } catch (JSONException e) {
                e = e;
                savedRunningApps = savedRunningApps2;
                e.printStackTrace();
                editor.putString("runningApps", savedRunningApps.toString());
                editor.commit();
            }
        } catch (JSONException e2) {
            e = e2;
            e.printStackTrace();
            editor.putString("runningApps", savedRunningApps.toString());
            editor.commit();
        }
        editor.putString("runningApps", savedRunningApps.toString());
        editor.commit();
    }

    /* access modifiers changed from: private */
    public void notifyRunningApps() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Editor editor = prefs.edit();
        try {
            try {
                this.mRevmobJSON.put("runningApps", new JSONArray(prefs.getString("runningApps", new JSONArray().toString())));
                if (this.mRevmobJSON.optJSONObject("identifiers").length() > 0) {
                    RevMobClient.getInstance().serverRequest(this.NOTIFY_URL, this.mRevmobJSON.toString(), null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        editor.remove("runningApps");
        editor.commit();
    }

    private void parseParameters(JSONObject runningAppsConfig) {
        try {
            this.SCAN_FREQUENCY = runningAppsConfig.getInt("scanFrequency");
            this.NOTIFY_FREQUENCY = runningAppsConfig.getInt("notifyFrequency");
            this.NOTIFY_URL = runningAppsConfig.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getCurrentTimestamp() {
        return ((int) System.currentTimeMillis()) / 1000;
    }
}
