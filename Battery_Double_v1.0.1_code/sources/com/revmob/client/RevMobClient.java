package com.revmob.client;

import android.util.SparseArray;
import com.applovin.sdk.AppLovinErrorCodes;
import com.revmob.RevMobAdsListener;
import com.revmob.RevMobParallaxMode;
import com.revmob.RevMobTestingMode;
import com.revmob.android.RevMobContext;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobEncryption;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.json.JSONException;

public class RevMobClient {
    private static final String ADS_PATH = "/api/v4/mobile_apps/%s/%s/fetch_only.json";
    private static final String BAD_RESPONSE_MESSAGE = "Bad response from server.";
    private static final String DEFAULT_DOWNLOAD_ERROR_MESSAGE = "RevMob did not answered as expected.";
    private static final int DEFAULT_TIMEOUT = 30;
    private static final String INSTALL_URL = "/api/v4/mobile_apps/%s/install.json";
    private static final String INVALID_APP_ID = "Invalid App ID.";
    private static final String INVALID_TIMEOUT = "Invalid timeout.";
    private static final SparseArray<String> LOG_MESSAGES = new SparseArray<>();
    private static final String NO_MEMORY_MESSAGE = "It was not possible to load the RevMob banner because your device run out of RAM memory.";
    private static final String ONE_APP_ID_PER_APP = "You can use just one App Id per application.";
    private static final String PARSE_ERROR_MESSAGE = "Error on parse response from server.";
    private static final String PARSE_ERROR_MESSAGE2 = "Error on parse response from server. Unknown error.";
    private static final String PLACEMENT_ADS_PATH = "/api/v4/mobile_apps/%s/placements/%s/%s/fetch_only.json";
    private static String PRODUCTION_SERVER_ADDRESS = "https://android.revmob.com";
    public static String SDK_NAME = "android";
    public static String SDK_SOURCE_NAME = "android";
    public static String SDK_SOURCE_VERSION = "9.0.4";
    public static String SDK_VERSION = "9.0.4";
    private static final String SESSIONS_PATH = "/api/v4/mobile_apps/%s/sessions.json";
    public static final String SESSION_WARNING = "Call RevMobAds.start(activity, APP_ID) on application start/resume. It will help us to improve tracking and increase the eCPM.";
    private static RevMobClient instance = new RevMobClient();
    private static final Map<String, String> serverEndPoints = new HashMap();
    public static long t0;
    public static long t1;
    public static long t2;
    public static long t3;
    private String appId;
    private RevMobParallaxMode parallaxMode = RevMobParallaxMode.DISABLED;
    private RevMobAdsListener publisherListener;
    private boolean reportingImpression = false;
    private boolean requestInstalledApps = false;
    private boolean requestIsStartSession = false;
    private boolean sessionStarted = false;
    private RevMobTestingMode testingMode = RevMobTestingMode.DISABLED;

    static {
        LOG_MESSAGES.put(200, "OK.");
        LOG_MESSAGES.put(202, "OK.");
        LOG_MESSAGES.put(AppLovinErrorCodes.NO_FILL, "Ad retrieval failed: No ads for this device/country right now or your App ID is paused.");
        LOG_MESSAGES.put(404, "No ad retrieved: did you set a valid App ID? Get one at http://revmob.com.");
        LOG_MESSAGES.put(409, "No ad retrieved: did you set a valid Placement ID? Get one at http://revmob.com.");
        LOG_MESSAGES.put(422, "Request requirements did not met. Did you set required permissions?");
        LOG_MESSAGES.put(423, "Is your ad unit paused? Please, check it in the RevMob Console.");
        LOG_MESSAGES.put(500, "Unexpected server error.");
        LOG_MESSAGES.put(503, "Unexpected server error.");
    }

    public static RevMobClient getInstance() {
        return instance;
    }

    public static void setProductionAdress(String addr, int key) {
        if (key == 714823364) {
            PRODUCTION_SERVER_ADDRESS = addr;
        }
    }

    public boolean startSession(String appId2, String payload, RevMobClientListener listener, RevMobAdsListener publisherListener2) {
        if (this.appId != null) {
            RMLog.w(ONE_APP_ID_PER_APP);
        } else if (isAppIdValid(appId2)) {
            this.requestIsStartSession = true;
            this.sessionStarted = true;
            this.appId = appId2;
            this.publisherListener = publisherListener2;
            serverRequest(PRODUCTION_SERVER_ADDRESS + String.format(SESSIONS_PATH, new Object[]{appId2}), payload, listener);
            HTTPHelper.globalTimeoutInSeconds = 30;
            return true;
        } else {
            RMLog.w(INVALID_APP_ID);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void addServerEndPoint(String key, String url) {
        if (key != null && url != null) {
            serverEndPoints.put(key, url);
        }
    }

    public void registerInstall(String payload, RevMobClientListener listener) {
        String url;
        String key = "install";
        if (serverEndPoints.containsKey(key)) {
            url = (String) serverEndPoints.get(key);
        } else {
            url = PRODUCTION_SERVER_ADDRESS + String.format(INSTALL_URL, new Object[]{this.appId});
        }
        serverRequestWithSessionVerification(url, payload, listener);
    }

    public void fetchFullscreen(String placementId, String payload, RevMobClientListener listener) {
        fetch(createFetchUrl("fullscreen", "fullscreens", this.appId, placementId, false), payload, listener);
    }

    public void fetchVideo(String placementId, String payload, RevMobClientListener listener, int videoParam) {
        String url;
        if (videoParam == 3) {
            url = createFetchUrl("video", "videos", this.appId, placementId, true);
        } else {
            url = createFetchUrl("video", "videos", this.appId, placementId, false);
        }
        fetch(url, payload, listener);
    }

    public void fetchVideoOrFullscreen(String placementId, String payload, RevMobClientListener listener) {
        fetch(createFetchUrl("fullscreen", "fullscreens", this.appId, placementId, true), payload, listener);
    }

    public void fetchBanner(String placementId, String payload, RevMobClientListener listener) {
        fetch(createFetchUrl("banner", "banners", this.appId, placementId, false), payload, listener);
    }

    public void fetchAdLink(String placementId, String payload, RevMobClientListener listener) {
        fetch(createFetchUrl("link", "anchors", this.appId, placementId, false), payload, listener);
    }

    public void fetchPopup(String placementId, String payload, RevMobClientListener listener) {
        fetch(createFetchUrl("pop_up", "pop_ups", this.appId, placementId, false), payload, listener);
    }

    public void fetch(String url, String payload, RevMobClientListener listener) {
        if (this.testingMode != RevMobTestingMode.DISABLED) {
            RMLog.i("Fetching ad using testing mode: " + this.testingMode.getValue());
        }
        serverRequestWithSessionVerification(url, payload, listener);
    }

    public void registerUserInformation(String payload, RevMobClientListener listener) {
        if (serverEndPoints.containsKey("user_information")) {
            String postUrl = (String) serverEndPoints.get("user_information");
            try {
                RevMobContext.adThread.join();
                serverRequest(postUrl, payload, listener);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void reportImpression(String url, String payload) {
        if (url != null) {
            if (this.testingMode != RevMobTestingMode.DISABLED) {
                RMLog.i("Reporting impression using testing mode: " + this.testingMode.getValue());
            }
            this.reportingImpression = true;
            serverRequestWithSessionVerification(url, payload, null);
        }
    }

    public void serverRequestWithSessionVerification(String url, String payload, RevMobClientListener listener) {
        if (!this.sessionStarted) {
            RMLog.w(SESSION_WARNING);
        } else {
            serverRequest(url, payload, listener);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isPlacementIdValid(String placementId) {
        return placementId != null && placementId.length() == 24;
    }

    /* access modifiers changed from: 0000 */
    public String createFetchUrl(String adUnitKey, String adUnitOldKey, String appId2, String placementId, boolean appendVideoTrue) {
        String fetchUrl;
        if ((adUnitKey == "videos" || adUnitKey == "video") && appendVideoTrue) {
            adUnitKey = "rewardedVideos";
            adUnitOldKey = "rewardedVideos";
        }
        if (isPlacementIdValid(placementId)) {
            String key = "fetch_" + adUnitKey + "_with_placement";
            if (serverEndPoints.containsKey(key)) {
                fetchUrl = ((String) serverEndPoints.get(key)).replaceFirst("PLACEMENT_ID", placementId);
            } else {
                fetchUrl = PRODUCTION_SERVER_ADDRESS + String.format(PLACEMENT_ADS_PATH, new Object[]{appId2, placementId, adUnitOldKey});
            }
        } else {
            String key2 = "fetch_" + adUnitKey;
            if (serverEndPoints.containsKey(key2)) {
                fetchUrl = (String) serverEndPoints.get(key2);
            } else {
                fetchUrl = PRODUCTION_SERVER_ADDRESS + String.format(ADS_PATH, new Object[]{appId2, adUnitOldKey});
            }
        }
        if (!appendVideoTrue) {
            return fetchUrl;
        }
        if (adUnitKey == "fullscreens" || adUnitKey == "fullscreen") {
            return fetchUrl + "?video=true";
        }
        return fetchUrl;
    }

    public void serverRequest(final String url, final String payload, final RevMobClientListener listener) {
        if (!this.requestIsStartSession && !this.reportingImpression && !this.requestInstalledApps) {
            t0 = System.currentTimeMillis();
        }
        new Thread() {
            public void run() {
                try {
                    RevMobClient.this.processResponse(new HTTPHelper().post(url, payload), listener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: 0000 */
    public void processResponse(HttpResponse response, RevMobClientListener listener) throws JSONException {
        if (response == null) {
            handleDownloadError(0, listener);
            return;
        }
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200 || statusCode == 202) {
            String encodedResponseBody = HTTPHelper.encodedResponseBody(response.getEntity());
            if (encodedResponseBody == null) {
                RMLog.w(BAD_RESPONSE_MESSAGE);
                if (listener != null) {
                    listener.handleError(BAD_RESPONSE_MESSAGE);
                    return;
                }
                return;
            }
            handleSuccess(encodedResponseBody, listener);
            return;
        }
        handleDownloadError(statusCode, listener);
    }

    /* access modifiers changed from: 0000 */
    public void handleSuccess(String encodedResponseBody, RevMobClientListener listener) {
        try {
            String responseData = new RevMobEncryption().decrypt(encodedResponseBody);
            if (this.requestIsStartSession) {
                if (this.publisherListener != null) {
                    this.publisherListener.onRevMobSessionIsStarted();
                }
                this.requestIsStartSession = false;
                this.requestInstalledApps = true;
            } else if (this.requestInstalledApps) {
                this.requestInstalledApps = false;
            } else if (!this.reportingImpression) {
                t1 = System.currentTimeMillis();
            }
            if (this.reportingImpression) {
                this.reportingImpression = false;
                if (!(t0 == 0 || t1 == 0 || t2 == 0 || t3 == 0)) {
                    t0 = 0;
                    t1 = 0;
                    t2 = 0;
                    t3 = 0;
                }
            }
            if (listener != null) {
                listener.handleResponse(responseData);
            }
        } catch (OutOfMemoryError e) {
            RMLog.w(NO_MEMORY_MESSAGE);
            if (listener != null) {
                listener.handleError(NO_MEMORY_MESSAGE);
            }
        } catch (JSONException e2) {
            RMLog.w(PARSE_ERROR_MESSAGE);
            RMLog.d(encodedResponseBody, e2);
            if (listener != null) {
                listener.handleError(PARSE_ERROR_MESSAGE);
            }
        } catch (NullPointerException e3) {
            RMLog.d(encodedResponseBody, e3);
            if (listener != null) {
                listener.handleError(PARSE_ERROR_MESSAGE2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void handleDownloadError(int statusCode, RevMobClientListener listener) {
        String message = ((String) LOG_MESSAGES.get(statusCode, DEFAULT_DOWNLOAD_ERROR_MESSAGE)) + " (" + statusCode + ")";
        RMLog.w(message);
        if (listener != null) {
            listener.handleError(message);
        }
        if (this.requestIsStartSession) {
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobSessionNotStarted(message);
            }
            this.requestIsStartSession = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isAppIdValid(String appId2) {
        return appId2 != null && appId2.length() == 24;
    }

    public RevMobTestingMode getTestingMode() {
        return this.testingMode;
    }

    public void setTestingMode(RevMobTestingMode testingMode2) {
        this.testingMode = testingMode2;
        if (testingMode2 != RevMobTestingMode.DISABLED) {
            RMLog.i("Testing mode enabled: " + testingMode2.getValue());
        } else {
            RMLog.i("Testing mode disabled");
        }
    }

    public RevMobParallaxMode getParallaxMode() {
        return this.parallaxMode;
    }

    public void setParallaxMode(RevMobParallaxMode parallaxMode2) {
        this.parallaxMode = parallaxMode2;
        if (parallaxMode2 != RevMobParallaxMode.DISABLED) {
            RMLog.i("Parallax mode enabled");
        } else {
            RMLog.i("Parallax mode disabled");
        }
    }

    public void setTimeoutInSeconds(int timeoutInSeconds) {
        if (timeoutInSeconds <= 1 || timeoutInSeconds >= 300) {
            RMLog.w(INVALID_TIMEOUT);
            return;
        }
        HTTPHelper.globalTimeoutInSeconds = timeoutInSeconds;
        RMLog.i("Timeout changed to " + timeoutInSeconds + "s");
    }

    public static void setSDKName(String sdkName) {
        if (sdkName != null) {
            SDK_NAME = sdkName;
            SDK_SOURCE_NAME = "android";
        }
    }

    public static void setSDKVersion(String sdkVersion) {
        if (sdkVersion != null) {
            SDK_VERSION = sdkVersion;
            SDK_SOURCE_VERSION = "9.0.4";
        }
    }

    public String getAppId() {
        return this.appId;
    }

    public static void sett0(long t) {
        t0 = t;
    }

    public static void sett1(long t) {
        t1 = t;
    }

    public static void sett2(long t) {
        t2 = t;
    }

    public static void sett3(long t) {
        t3 = t;
    }
}
