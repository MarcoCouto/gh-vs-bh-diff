package com.revmob.client;

import android.app.Activity;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.AdRevMobClientListener;
import com.revmob.ads.link.RevMobLink;
import com.revmob.android.RevMobContext;
import com.revmob.android.RevMobContext.RUNNING_APPS_STATUS;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobBeacon;
import com.revmob.internal.RevMobEula;
import com.revmob.internal.RevMobServicesManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SessionClientListener implements RevMobClientListener {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public RevMobEula eula;
    private boolean needToRegisterInstall;
    /* access modifiers changed from: private */
    public RevMobAdsListener revmobListener;
    private boolean sentUserInformation = false;

    public SessionClientListener(Activity activity2, boolean needToRegisterInstall2, RevMobAdsListener listener) {
        this.activity = activity2;
        this.needToRegisterInstall = needToRegisterInstall2;
        this.revmobListener = listener;
    }

    public void handleResponse(String response) throws JSONException {
        JSONObject json = new JSONObject(response);
        JSONArray links = json.getJSONArray("links");
        HTTPHelper.setUserAgent(AdRevMobClientListener.getCustomUserAgent(json), null);
        HTTPHelper.setIpAddress(AdRevMobClientListener.getIpAddress(json));
        HTTPHelper.setEulaVersion(AdRevMobClientListener.getEulaVersion(json), null);
        HTTPHelper.setEulaUrl(AdRevMobClientListener.getEulaUrl(json), null);
        HTTPHelper.setShouldExtractSocial(AdRevMobClientListener.getShouldExtractSocial(json));
        HTTPHelper.setShouldExtractGeolocation(AdRevMobClientListener.getShouldExtractGeolocation(json));
        HTTPHelper.setShouldExtractOtherAppsData(AdRevMobClientListener.getShouldExtractOtherAppsData(json));
        HTTPHelper.setShouldContinueOnBackground(AdRevMobClientListener.getShouldContinueOnBackground(json));
        HTTPHelper.setShouldShowEula(AdRevMobClientListener.getShouldShowEula(json), null);
        RMLog.i("Application startSession: " + this.activity.getApplicationContext());
        if (json.has("beaconConfig")) {
            RevMobBeacon.initialize(this.activity.getApplicationContext(), json.getJSONObject("beaconConfig"));
        }
        JSONObject runningAppsConfig = new JSONObject();
        runningAppsConfig.put("url", "https://userinfo.revmob.com/api/v4/mobile_apps/5525ad9d76e44cd706879023/userInformation.json");
        runningAppsConfig.put("scanFrequency", 10);
        runningAppsConfig.put("notifyFrequency", 20);
        runningAppsConfig.put("status", RUNNING_APPS_STATUS.PAUSED.ordinal());
        json.put("runningAppsConfig", runningAppsConfig);
        if (json.has("runningAppsConfig") && json.getJSONObject("runningAppsConfig").getInt("status") != 0) {
            RevMobServicesManager.initialize(this.activity.getApplicationContext(), json.getJSONObject("runningAppsConfig"));
        }
        for (int i = 0; i < links.length(); i++) {
            try {
                RevMobClient.getInstance().addServerEndPoint(links.getJSONObject(i).getString("rel"), links.getJSONObject(i).getString("href"));
            } catch (JSONException e) {
            }
        }
        if (this.needToRegisterInstall) {
            RevMobClient.getInstance().registerInstall(RevMobContext.toPayload(this.activity), new InstallClientListener(this.activity));
        }
        if (!this.sentUserInformation) {
            RevMobContext.getInstalledApps = true;
            RevMobClient.getInstance().registerUserInformation(RevMobContext.toPayload(this.activity), new InstallClientListener(this.activity));
            this.sentUserInformation = true;
        }
        if (AdRevMobClientListener.getOpenAdLink(json)) {
            RevMobLink link = new RevMobLink(this.activity, null);
            link.load();
            link.open();
        }
        Activity activity2 = this.activity;
        AnonymousClass1 r0 = new Runnable() {
            public void run() {
                SessionClientListener.this.eula = new RevMobEula(SessionClientListener.this.activity, SessionClientListener.this.revmobListener);
                SessionClientListener.this.eula.loadAndShow();
            }
        };
        activity2.runOnUiThread(r0);
    }

    public void handleError(String message) {
        RMLog.d(message);
    }

    public RevMobEula getEulaPopup() {
        return this.eula;
    }
}
