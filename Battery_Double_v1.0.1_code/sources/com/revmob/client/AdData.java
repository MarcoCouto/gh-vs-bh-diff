package com.revmob.client;

public abstract class AdData {
    protected String appOrSite;
    protected String campaignId;
    protected String clickUrl;
    protected boolean followRedirect;
    protected String impressionUrl;
    protected boolean openInside;
    protected int parallaxDelta;

    protected AdData(String impressionUrl2, String clickUrl2, boolean followRedirect2, String appOrSite2, boolean openInside2) {
        this.impressionUrl = impressionUrl2;
        this.clickUrl = clickUrl2;
        this.followRedirect = followRedirect2;
        this.appOrSite = appOrSite2;
        this.openInside = openInside2;
    }

    public String getImpressionUrl() {
        return this.impressionUrl;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public String getClickUrl(String videoPosition) {
        if (videoPosition != null) {
            this.clickUrl = this.clickUrl.concat("&videoPosition=");
            this.clickUrl = this.clickUrl.concat(videoPosition);
        }
        return this.clickUrl;
    }

    public boolean shouldFollowRedirect() {
        return this.followRedirect;
    }

    public String getAppOrSite() {
        return this.appOrSite;
    }

    public boolean isOpenInside() {
        return this.openInside;
    }

    public String getDspUrl() {
        return null;
    }
}
