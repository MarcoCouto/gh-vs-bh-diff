package com.revmob;

public enum RevMobTestingMode {
    WITH_ADS("with_ads"),
    WITHOUT_ADS("without_ads"),
    DISABLED(null);
    
    private String value;

    private RevMobTestingMode(String value2) {
        this.value = value2;
    }

    public String getValue() {
        return this.value;
    }
}
