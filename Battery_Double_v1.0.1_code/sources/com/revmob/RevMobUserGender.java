package com.revmob;

public enum RevMobUserGender {
    MALE("male"),
    FEMALE("female"),
    UNDEFINED(null);
    
    private String value;

    private RevMobUserGender(String value2) {
        this.value = value2;
    }

    public String getValue() {
        return this.value;
    }
}
