package com.revmob.ads.banner;

import android.app.Activity;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.banner.client.BannerClientListener;
import com.revmob.ads.banner.client.BannerData;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.internal.AnimationConfiguration;
import com.revmob.ads.internal.CloseAnimationConfiguration;
import com.revmob.ads.internal.ShowAnimationConfiguration;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.MarketAsyncManager;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobSoundPlayer;
import com.revmob.internal.RevMobWebView;
import com.revmob.internal.RevMobWebViewClient;
import com.revmob.internal.RevMobWebViewClient.RevMobWebViewClickListener;
import java.io.IOException;
import java.util.ArrayList;

public class RevMobBanner extends RelativeLayout implements Ad {
    public static final int DEFAULT_HEIGHT_IN_DIP = 50;
    public static int DEFAULT_WIDTH_IN_DIP = 320;
    public static final int MAXIMUM_HEIGHT_IN_DIP = 60;
    public static final int MINIMUM_HEIGHT_IN_DIP = 40;
    public static int bannerCount;
    public static boolean isBannerImpression = false;
    public static ArrayList<String> usedCampaigns;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public View adView;
    /* access modifiers changed from: private */
    public View adView2;
    public boolean changedBannerParams;
    private boolean containerTooSmall = false;
    private int customHeight = 0;
    private int customWidth = 0;
    /* access modifiers changed from: private */
    public BannerData data;
    private int height = 50;
    private DisplayMetrics metrics = new DisplayMetrics();
    /* access modifiers changed from: private */
    public OnClickListener onClickListener;
    /* access modifiers changed from: private */
    public String placementId;
    /* access modifiers changed from: private */
    public RevMobAdsListener publisherListener;
    /* access modifiers changed from: private */
    public AdState state;
    private int width = DEFAULT_WIDTH_IN_DIP;

    public RevMobBanner(Activity activity2, RevMobAdsListener publisherListener2) {
        super(activity2);
        this.activity = activity2;
        this.publisherListener = publisherListener2;
        this.state = AdState.CREATED;
        isBannerImpression = false;
    }

    private OnClickListener onClickListener() {
        return new OnClickListener() {
            public void onClick(View v) {
                RevMobBanner.this.activity.runOnUiThread(new Runnable() {
                    public void run() {
                        RevMobBanner.this.state = AdState.CLICKED;
                        new MarketAsyncManager(RevMobBanner.this.activity, RevMobBanner.this.data, RevMobBanner.this.publisherListener).execute(new Void[0]);
                        RevMobBanner.this.hide();
                        RevMobBanner.this.load(RevMobBanner.this.placementId);
                    }
                });
            }
        };
    }

    public void setChangeBannerParams(boolean b) {
        this.changedBannerParams = b;
    }

    public static void setBannerImpression(boolean b) {
        isBannerImpression = b;
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void load() {
        load(null);
    }

    public void load(String placementId2) {
        this.placementId = placementId2;
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED || this.state == AdState.CLICKED) {
            RMLog.i(placementId2 != null ? "Loading Banner " + placementId2 : "Loading Banner");
            RevMobContext.getRunningApps = true;
            RevMobClient.getInstance().fetchBanner(placementId2, RevMobContext.toPayload(this.activity), new BannerClientListener(this, this.publisherListener));
        }
    }

    public void updateWithData(AdData advertisement) {
        if (this.state == AdState.HIDDEN) {
            RMLog.d("state == hidden");
            return;
        }
        this.state = AdState.LOADED;
        this.data = (BannerData) advertisement;
        RMLog.i("Banner loaded - " + this.data.getCampaignId());
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdReceived();
        }
        this.onClickListener = onClickListener();
        setOnClickListener(this.onClickListener);
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {
                if (RevMobBanner.this.adView == null && RevMobBanner.this.adView2 == null) {
                    RevMobBanner.bannerCount = 0;
                    RevMobBanner.usedCampaigns = null;
                    if (RevMobBanner.this.adView == null) {
                        RevMobBanner.this.addAdView(true);
                    } else {
                        RevMobBanner.this.addAdView(false);
                    }
                } else {
                    RevMobBanner.this.hide(true);
                }
            }
        });
        final ViewParent parent = getParent();
        if (parent != null && this.data.getRefreshTime() > 0) {
            new Handler(this.activity.getApplicationContext().getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (RevMobBanner.this.state != AdState.HIDDEN && parent != null) {
                        RevMobBanner.this.state = AdState.CREATED;
                        RevMobBanner.this.load(RevMobBanner.this.placementId);
                    }
                }
            }, (long) this.data.getRefreshTime());
        }
    }

    public void addAdView(boolean view1) {
        if (this.data.isHtmlBanner()) {
            addHtmlAdView(view1);
        } else if (this.data.isDspBanner()) {
            addDSPAdView(view1);
        } else {
            addImageView(view1);
        }
    }

    private void addHtmlAdView(boolean view1) {
        RMLog.d("html adview");
        final boolean v = view1;
        RevMobWebViewClient client = new RevMobWebViewClient(this.publisherListener, new RevMobWebViewClickListener() {
            public boolean handleClick(WebView view, String url) {
                if (url.endsWith("#click")) {
                    if (v) {
                        RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView);
                    } else {
                        RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView2);
                    }
                }
                return true;
            }

            public void handlePageFinished(WebView view, String url) {
            }
        });
        if (view1) {
            this.adView = new RevMobWebView(getContext(), this.data.getHtmlAdUrl(), null, client);
            this.adView.setBackgroundColor(0);
        } else {
            this.adView2 = new RevMobWebView(getContext(), this.data.getHtmlAdUrl(), null, client);
            this.adView2.setBackgroundColor(0);
        }
        configureAndAnimateView(view1);
    }

    private void addDSPAdView(boolean view1) {
        RMLog.d("dspadview");
        final boolean v = view1;
        RevMobWebViewClient client = new RevMobWebViewClient(this.publisherListener, new RevMobWebViewClickListener() {
            public boolean handleClick(WebView view, String url) {
                RevMobBanner.this.data.setDspUrl(url);
                if (v) {
                    RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView);
                } else {
                    RevMobBanner.this.onClickListener.onClick(RevMobBanner.this.adView2);
                }
                return true;
            }

            public void handlePageFinished(WebView view, String url) {
            }
        });
        if (view1) {
            this.adView = new RevMobWebView(getContext(), this.data.getDspUrl(), this.data.getDspHtml(), client);
            this.adView.setBackgroundColor(0);
        } else {
            this.adView2 = new RevMobWebView(getContext(), this.data.getDspUrl(), this.data.getDspHtml(), client);
            this.adView2.setBackgroundColor(0);
        }
        configureAndAnimateView(view1);
    }

    private void addImageView(boolean view1) {
        RMLog.d("banner imageview");
        if (view1) {
            this.adView = new ImageView(getContext());
            ((ImageView) this.adView).setImageBitmap(this.data.getDrawable());
            if (getParent() != null) {
                ((View) getParent()).setVisibility(0);
            }
            ((ImageView) this.adView).setScaleType(ScaleType.FIT_XY);
            this.adView.setOnClickListener(this.onClickListener);
        } else {
            this.adView2 = new ImageView(getContext());
            ((ImageView) this.adView2).setImageBitmap(this.data.getDrawable());
            if (getParent() != null) {
                ((View) getParent()).setVisibility(0);
            }
            ((ImageView) this.adView2).setScaleType(ScaleType.FIT_XY);
            this.adView2.setOnClickListener(this.onClickListener);
        }
        configureAndAnimateView(view1);
    }

    private void configureAndAnimateView(boolean view1) {
        Animation showAnimation;
        calculateDimensions();
        if (!this.containerTooSmall) {
            LayoutParams viewParams = new LayoutParams(this.customWidth == 0 ? this.width : this.customWidth, this.customHeight == 0 ? this.height : this.customHeight);
            viewParams.addRule(14);
            if (this.data.getRefreshTime() > 0) {
                AnimationConfiguration animation = new ShowAnimationConfiguration();
                animation.addAnimation("slide_up");
                animation.setTime(500);
                showAnimation = animation.getAnimation();
            } else {
                showAnimation = this.data.getShowAnimation();
            }
            if (view1) {
                this.adView.setLayoutParams(viewParams);
                this.adView.setAnimation(showAnimation);
                addView(this.adView);
            } else {
                this.adView2.setLayoutParams(viewParams);
                this.adView2.setAnimation(showAnimation);
                addView(this.adView2);
            }
            playSoundOnShow();
            reportShowOrHidden();
        }
    }

    public void setCustomSize(int w, int h) {
        this.customWidth = w;
        this.customHeight = h;
        LayoutParams newParams = new LayoutParams(this.customWidth, this.customHeight);
        if (this.adView != null) {
            this.adView.setLayoutParams(newParams);
        } else if (this.adView2 != null) {
            this.adView.setLayoutParams(newParams);
        }
    }

    public int dipToPixels(int pixels) {
        return (int) ((((float) pixels) * this.metrics.density) + 0.5f);
    }

    private void calculateDimensions() {
        int i;
        View parentView = (View) getParent();
        int displayWidth = ((Activity) getContext()).getWindowManager().getDefaultDisplay().getWidth();
        int displayHeight = ((Activity) getContext()).getWindowManager().getDefaultDisplay().getHeight();
        this.activity.getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
        if (displayWidth < displayHeight) {
            DEFAULT_WIDTH_IN_DIP = (int) (((float) displayWidth) / this.metrics.density);
        } else {
            DEFAULT_WIDTH_IN_DIP = (int) (((float) displayHeight) / this.metrics.density);
        }
        int idealHeight = dipToPixels(50);
        if (!(parentView == null || parentView.getHeight() == 0 || parentView.getHeight() >= dipToPixels(40))) {
            this.containerTooSmall = true;
            RMLog.i("The container height must be at least 40dp");
        }
        if (!(parentView == null || parentView.getHeight() == 0)) {
            idealHeight = parentView.getHeight();
        }
        this.height = idealHeight;
        int auxiliarwidth = (this.height * DEFAULT_WIDTH_IN_DIP) / 50;
        if (auxiliarwidth >= displayWidth || auxiliarwidth >= displayHeight) {
            auxiliarwidth = Math.min(displayHeight, displayWidth);
            if ((auxiliarwidth * 50) / DEFAULT_WIDTH_IN_DIP > dipToPixels(60)) {
                i = dipToPixels(60);
            } else {
                i = (auxiliarwidth * 50) / DEFAULT_WIDTH_IN_DIP;
            }
            this.height = i;
        }
        if (!(parentView == null || parentView.getWidth() == 0)) {
            auxiliarwidth = Math.min(parentView.getWidth(), auxiliarwidth);
        }
        this.width = auxiliarwidth;
        if (parentView != null && parentView.getWidth() != 0) {
            if (parentView.getWidth() < (DEFAULT_WIDTH_IN_DIP * 40) / 50) {
                this.containerTooSmall = true;
                RMLog.i("The container width must be at least " + ((DEFAULT_WIDTH_IN_DIP * 40) / 50) + "dp");
            } else if (this.width == parentView.getWidth()) {
                this.height = (this.width * 50) / DEFAULT_WIDTH_IN_DIP;
            }
        }
    }

    public void hide() {
        bannerCount = 0;
        usedCampaigns = null;
        hide(false);
    }

    public void hide(boolean refresh) {
        final boolean adView1Shown;
        Animation animation;
        final boolean refreshBanner = refresh;
        if (this.adView != null) {
            adView1Shown = true;
        } else {
            adView1Shown = false;
        }
        if (this.data == null || this.data.getRefreshTime() != 0) {
            AnimationConfiguration fadeout = new CloseAnimationConfiguration();
            fadeout.addAnimation("slide_up");
            fadeout.setTime(500);
            animation = fadeout.getAnimation();
        } else {
            animation = this.data.getCloseAnimation();
        }
        animation.setAnimationListener(new AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (adView1Shown) {
                    RevMobBanner.this.removeAdViewBlock();
                } else {
                    RevMobBanner.this.removeAdView2Block();
                }
                if (!refreshBanner) {
                    RevMobBanner.this.state = AdState.HIDDEN;
                }
            }
        });
        if (this.adView != null) {
            if (refreshBanner) {
                addAdView(false);
            }
            this.adView.startAnimation(animation);
        } else if (refreshBanner) {
            addAdView(true);
            this.adView2.startAnimation(animation);
        }
    }

    /* access modifiers changed from: private */
    public void removeAdViewBlock() {
        if (this.adView != null) {
            this.adView.setVisibility(4);
            new Handler(this.activity.getApplicationContext().getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (RevMobBanner.this.adView != null) {
                        ViewGroup viewParent = (ViewGroup) RevMobBanner.this.adView.getParent();
                        if (viewParent != null) {
                            viewParent.removeView(RevMobBanner.this.adView);
                        }
                        RevMobBanner.this.adView = null;
                    }
                }
            }, 1000);
        }
    }

    /* access modifiers changed from: private */
    public void removeAdView2Block() {
        if (this.adView2 != null) {
            this.adView2.setVisibility(4);
            new Handler(this.activity.getApplicationContext().getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    if (RevMobBanner.this.adView2 != null) {
                        ViewGroup viewParent = (ViewGroup) RevMobBanner.this.adView2.getParent();
                        if (viewParent != null) {
                            viewParent.removeView(RevMobBanner.this.adView2);
                        }
                        RevMobBanner.this.adView2 = null;
                    }
                }
            }, 1000);
        }
    }

    public void reportShowOrHidden() {
        if (!isShown()) {
            this.state = AdState.HIDDEN;
        } else if (isLoaded() && this.state != AdState.DISPLAYED) {
            this.state = AdState.DISPLAYED;
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdDisplayed();
            }
            if (usedCampaigns == null) {
                usedCampaigns = new ArrayList<>();
            }
            if (!usedCampaigns.contains(this.data.getCampaignId())) {
                usedCampaigns.add(this.data.getCampaignId());
            }
            bannerCount++;
            isBannerImpression = true;
            RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
        }
    }

    private void playSoundOnShow() {
        if (this.data.getShowSoundURL() != null && this.data.getShowSoundURL().length() != 0) {
            try {
                new RevMobSoundPlayer().playBannerSound(this.activity, this.data.getShowSoundURL());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
