package com.revmob.ads.banner.client;

import android.graphics.Bitmap;
import android.view.animation.Animation;
import com.revmob.ads.internal.AnimationConfiguration;
import com.revmob.client.AdData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class BannerData extends AdData {
    private String clickSoundURL;
    private AnimationConfiguration closeAnimation;
    private Bitmap drawable;
    private String dspHtml;
    private String dspUrl;
    private String htmlAdUrl;
    private int[] imageSize;
    private int refreshTime;
    private AnimationConfiguration showAnimation;
    private String showSoundURL;

    public BannerData(String impressionUrl, String clickUrl, boolean followRedirect, Bitmap image, String htmlAd, String dspUrl2, String dspHtml2, AnimationConfiguration showAnimation2, AnimationConfiguration closeAnimation2, String appOrSite, boolean openInside, String showSoundURL2, String clickSoundURL2, int[] imageSize2, int refreshableTime) {
        super(impressionUrl, clickUrl, followRedirect, appOrSite, openInside);
        this.drawable = image;
        this.htmlAdUrl = htmlAd;
        this.dspUrl = dspUrl2;
        this.dspHtml = dspHtml2;
        this.showAnimation = showAnimation2;
        this.closeAnimation = closeAnimation2;
        this.showSoundURL = showSoundURL2;
        this.clickSoundURL = clickSoundURL2;
        this.imageSize = imageSize2;
        this.refreshTime = refreshableTime;
    }

    public String getCampaignId() {
        String url = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parameters = URLEncodedUtils.parse(new URI(url), "UTF-8");
            if (parameters.size() > 0) {
                for (NameValuePair p : parameters) {
                    if (p.getName().equals("campaign_id")) {
                        this.campaignId = p.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }

    public String getHtmlAdUrl() {
        return this.htmlAdUrl;
    }

    public Bitmap getDrawable() {
        return this.drawable;
    }

    public String getDspUrl() {
        return this.dspUrl;
    }

    public String getDspHtml() {
        return this.dspHtml;
    }

    public boolean isHtmlBanner() {
        return this.htmlAdUrl != null;
    }

    public boolean isDspBanner() {
        return (this.dspUrl == null && this.dspHtml == null) ? false : true;
    }

    public Animation getShowAnimation() {
        return this.showAnimation.getAnimation();
    }

    public Animation getCloseAnimation() {
        return this.closeAnimation.getAnimation();
    }

    public String getShowSoundURL() {
        return this.showSoundURL;
    }

    public String getClickSoundURL() {
        return this.clickSoundURL;
    }

    public void setDspUrl(String dspUrl2) {
        this.dspUrl = dspUrl2;
    }

    public int[] getImageSize() {
        return this.imageSize;
    }

    public int getRefreshTime() {
        return this.refreshTime;
    }
}
