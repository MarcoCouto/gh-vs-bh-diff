package com.revmob.ads.banner.client;

import android.graphics.Bitmap;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import com.revmob.ads.internal.AnimationConfiguration;
import com.revmob.ads.internal.CloseAnimationConfiguration;
import com.revmob.ads.internal.ShowAnimationConfiguration;
import com.revmob.client.RevMobClient;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BannerClientListener extends AdRevMobClientListener {
    public BannerClientListener(Ad ad, RevMobAdsListener publisherListener) {
        super(ad, publisherListener);
    }

    public void handleResponse(String response) throws JSONException {
        int refreshTime;
        RevMobClient.sett2(System.currentTimeMillis());
        JSONObject jSONObject = new JSONObject(response);
        JSONObject json = jSONObject.getJSONArray("banners").getJSONObject(0);
        String appOrSite = getAppOrSite(json);
        boolean openInside = getOpenInside(json);
        JSONArray links = json.getJSONArray("links");
        String clickUrl = getClickUrl(links);
        boolean followRedirect = getFollowRedirect(json);
        String impressionUrl = getImpressionUrl(links);
        String htmlUrl = getLinkByRel(links, "html");
        String dspUrl = getLinkByRel(links, "dsp_url");
        String dspHtml = getLinkByRel(links, "dsp_html");
        String imageSizeString = getLinkByRel(links, "imageSize");
        if (json.has("refreshTime")) {
            refreshTime = json.getInt("refreshTime");
        } else {
            refreshTime = 0;
        }
        int[] imageSize = new int[2];
        try {
            String[] parts = imageSizeString.split(",");
            imageSize[0] = Integer.parseInt(parts[0]);
            imageSize[1] = Integer.parseInt(parts[1]);
        } catch (Exception e) {
            imageSize[0] = 0;
            imageSize[1] = 0;
        }
        AnimationConfiguration showAnimation = new ShowAnimationConfiguration();
        AnimationConfiguration closeAnimation = new CloseAnimationConfiguration();
        try {
            JSONObject animation = json.getJSONObject("animation");
            long duration = animation.getLong("duration");
            showAnimation.setTime(duration);
            closeAnimation.setTime(duration);
            JSONArray jsonArray = animation.getJSONArray("show");
            for (int i = 0; i < jsonArray.length(); i++) {
                showAnimation.addAnimation(jsonArray.getString(i));
            }
            JSONArray jsonArray2 = animation.getJSONArray("close");
            for (int i2 = 0; i2 < jsonArray2.length(); i2++) {
                closeAnimation.addAnimation(jsonArray2.getString(i2));
            }
        } catch (JSONException e2) {
        }
        String showSoundURL = new String();
        String clickSoundURL = new String();
        try {
            JSONObject sound = json.getJSONObject("sound");
            showSoundURL = sound.getString("on_show");
            clickSoundURL = sound.getString("on_click");
        } catch (JSONException e3) {
        }
        Bitmap image = null;
        if (dspUrl != null || dspHtml != null) {
            RMLog.d("Banner DSP");
        } else if (htmlUrl == null) {
            image = new HTTPHelper().downloadBitmap(getLinkByRel(links, "image"), imageSizeString);
        }
        if ((clickUrl != null && (htmlUrl != null || image != null)) || dspUrl != null || dspHtml != null) {
            RevMobClient.sett3(System.currentTimeMillis());
            this.ad.updateWithData(new BannerData(impressionUrl, clickUrl, followRedirect, image, htmlUrl, dspUrl, dspHtml, showAnimation, closeAnimation, appOrSite, openInside, showSoundURL, clickSoundURL, imageSize, refreshTime));
        }
    }
}
