package com.revmob.ads.link;

import android.app.Activity;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.link.client.LinkClientListener;
import com.revmob.ads.link.client.LinkData;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.MarketAsyncManager;
import com.revmob.internal.RMLog;

public class RevMobLink implements Ad {
    /* access modifiers changed from: private */
    public Activity activity;
    public boolean autoopen = false;
    /* access modifiers changed from: private */
    public LinkData data;
    /* access modifiers changed from: private */
    public RevMobAdsListener publisherListener;
    private AdState state;

    public RevMobLink(Activity activity2, RevMobAdsListener publisherListener2) {
        this.activity = activity2;
        this.publisherListener = publisherListener2;
        this.state = AdState.CREATED;
    }

    public void load() {
        load(null);
    }

    public void load(String placementId) {
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED) {
            RMLog.i(placementId != null ? "Loading Link " + placementId : "Loading Link");
            RevMobContext.getRunningApps = true;
            RevMobClient.getInstance().fetchAdLink(placementId, RevMobContext.toPayload(this.activity), new LinkClientListener(this, this.publisherListener));
        }
    }

    public void updateWithData(AdData advertisement) {
        this.state = AdState.LOADED;
        this.data = (LinkData) advertisement;
        RMLog.i("Link loaded - " + this.data.getCampaignId());
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdReceived();
        }
        if (this.autoopen) {
            open();
        }
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void cancel() {
        this.autoopen = false;
    }

    public void open() {
        this.autoopen = true;
        if (isLoaded() && this.state != AdState.DISPLAYED) {
            this.state = AdState.DISPLAYED;
            if (this.publisherListener != null) {
                this.publisherListener.onRevMobAdDisplayed();
            }
            RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
            this.activity.runOnUiThread(new Runnable() {
                public void run() {
                    new MarketAsyncManager(RevMobLink.this.activity, RevMobLink.this.data, RevMobLink.this.publisherListener).execute(new Void[0]);
                }
            });
        } else if (this.state != AdState.CREATED && this.state != AdState.CLOSED) {
            RMLog.i(Ad.SHOWING_TOO_SOON_MSG);
        }
    }
}
