package com.revmob.ads.link.client;

import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LinkClientListener extends AdRevMobClientListener {
    public LinkClientListener(Ad ad, RevMobAdsListener publisherListener) {
        super(ad, publisherListener);
    }

    public void handleResponse(String response) throws JSONException {
        this.ad.updateWithData(createData(response));
    }

    public static LinkData createData(String response) throws JSONException {
        JSONObject json = new JSONObject(response).getJSONObject("anchor");
        String appOrSite = getAppOrSite(json);
        boolean openInside = getOpenInside(json);
        JSONArray links = json.getJSONArray("links");
        return new LinkData(getImpressionUrl(links), getClickUrl(links), getFollowRedirect(json), appOrSite, openInside);
    }
}
