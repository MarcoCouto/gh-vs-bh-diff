package com.revmob.ads.link.client;

import com.revmob.client.AdData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class LinkData extends AdData {
    public LinkData(String impressionUrl, String clickUrl, boolean followRedirect, String appOrSite, boolean openInside) {
        super(impressionUrl, clickUrl, followRedirect, appOrSite, openInside);
    }

    public String getCampaignId() {
        String url = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parameters = URLEncodedUtils.parse(new URI(url), "UTF-8");
            if (parameters.size() > 0) {
                for (NameValuePair p : parameters) {
                    if (p.getName().equals("campaign_id")) {
                        this.campaignId = p.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }
}
