package com.revmob.ads.popup;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.WindowManager.BadTokenException;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.popup.client.PopupClientListener;
import com.revmob.ads.popup.client.PopupData;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.MarketAsyncManager;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobSoundPlayer;
import java.io.IOException;

public class RevMobPopup implements Ad {
    /* access modifiers changed from: private */
    public Activity activity;
    public boolean autoshow = false;
    /* access modifiers changed from: private */
    public PopupData data;
    /* access modifiers changed from: private */
    public RevMobAdsListener publisherListener;
    /* access modifiers changed from: private */
    public AdState state;

    public RevMobPopup(Activity activity2, RevMobAdsListener publisherListener2) {
        this.activity = activity2;
        this.publisherListener = publisherListener2;
        this.state = AdState.CREATED;
    }

    public void load() {
        load(null);
    }

    public void load(String placementId) {
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED) {
            RMLog.i(placementId != null ? "Loading Popup " + placementId : "Loading Popup");
            RevMobContext.getRunningApps = true;
            RevMobClient.getInstance().fetchPopup(placementId, RevMobContext.toPayload(this.activity), new PopupClientListener(this, this.publisherListener));
        }
    }

    public void updateWithData(AdData advertisement) {
        this.state = AdState.LOADED;
        this.data = (PopupData) advertisement;
        RMLog.i("Popup loaded - " + this.data.getCampaignId());
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdReceived();
        }
        if (this.autoshow) {
            show();
        }
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void show() {
        this.autoshow = true;
        if (isLoaded() && this.state != AdState.DISPLAYED) {
            this.state = AdState.DISPLAYED;
            this.activity.runOnUiThread(new Runnable() {
                public void run() {
                    RevMobPopup.this.buildAndDisplayDialog();
                    RevMobPopup.this.playSoundOnShow();
                }
            });
        } else if (this.state != AdState.CREATED && this.state != AdState.CLOSED) {
            RMLog.i(Ad.SHOWING_TOO_SOON_MSG);
        }
    }

    public void hide() {
        this.autoshow = false;
    }

    /* access modifiers changed from: private */
    public void buildAndDisplayDialog() {
        try {
            if (this.activity != null && !this.activity.isFinishing()) {
                new Builder(this.activity).setTitle(this.data.getMessage()).setPositiveButton(PopupData.YES_MESSAGE, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RevMobPopup.this.activity.runOnUiThread(new Runnable() {
                            public void run() {
                                new MarketAsyncManager(RevMobPopup.this.activity, RevMobPopup.this.data, RevMobPopup.this.publisherListener).execute(new Void[0]);
                            }
                        });
                    }
                }).setNegativeButton(PopupData.NO_MESSAGE, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        RevMobPopup.this.state = AdState.CLOSED;
                    }
                }).show();
                if (this.publisherListener != null) {
                    this.publisherListener.onRevMobAdDisplayed();
                }
                RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
            }
        } catch (BadTokenException e) {
            RMLog.w("Invalid activity as argument: is there an activity running?");
        }
    }

    /* access modifiers changed from: private */
    public void playSoundOnShow() {
        if (this.data.getShowSoundURL() != null && this.data.getShowSoundURL().length() != 0) {
            try {
                new RevMobSoundPlayer().playPopupSound(this.activity, this.data.getShowSoundURL());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
