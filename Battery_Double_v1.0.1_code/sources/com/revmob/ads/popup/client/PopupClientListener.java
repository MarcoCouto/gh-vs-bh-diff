package com.revmob.ads.popup.client;

import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PopupClientListener extends AdRevMobClientListener {
    public PopupClientListener(Ad ad, RevMobAdsListener publisherListener) {
        super(ad, publisherListener);
    }

    public void handleResponse(String response) throws JSONException {
        this.ad.updateWithData(createData(response));
    }

    public static PopupData createData(String response) throws JSONException {
        JSONObject json = new JSONObject(response).getJSONObject("pop_up");
        String appOrSite = getAppOrSite(json);
        boolean openInside = getOpenInside(json);
        JSONArray links = json.getJSONArray("links");
        String clickUrl = getClickUrl(links);
        boolean followRedirect = getFollowRedirect(json);
        String impressionUrl = getImpressionUrl(links);
        String message = new String();
        try {
            message = json.getString("message");
        } catch (JSONException e) {
        }
        String showSoundURL = new String();
        String clickSoundURL = new String();
        try {
            JSONObject sound = json.getJSONObject("sound");
            showSoundURL = sound.getString("on_show");
            clickSoundURL = sound.getString("on_click");
        } catch (JSONException e2) {
        }
        return new PopupData(impressionUrl, clickUrl, followRedirect, message, appOrSite, openInside, showSoundURL, clickSoundURL);
    }
}
