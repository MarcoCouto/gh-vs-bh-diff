package com.revmob.ads.popup.client;

import com.revmob.client.AdData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class PopupData extends AdData {
    public static final String NO_MESSAGE = "No, thanks.";
    public static final String YES_MESSAGE = "Yes, sure!";
    private String clickSoundURL;
    private String message;
    private String showSoundURL;

    public PopupData(String impressionUrl, String clickUrl, boolean followRedirect, String message2, String appOrSite, boolean openInside, String showSoundURL2, String clickSoundURL2) {
        super(impressionUrl, clickUrl, followRedirect, appOrSite, openInside);
        if (message2 == null || message2.length() <= 0) {
            message2 = "Download a FREE game!";
        }
        this.message = message2;
        this.showSoundURL = showSoundURL2;
        this.clickSoundURL = clickSoundURL2;
    }

    public String getCampaignId() {
        String url = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parameters = URLEncodedUtils.parse(new URI(url), "UTF-8");
            if (parameters.size() > 0) {
                for (NameValuePair p : parameters) {
                    if (p.getName().equals("campaign_id")) {
                        this.campaignId = p.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }

    public String getMessage() {
        return this.message;
    }

    public String getShowSoundURL() {
        return this.showSoundURL;
    }

    public String getClickSoundURL() {
        return this.clickSoundURL;
    }
}
