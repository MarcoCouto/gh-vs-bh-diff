package com.revmob.ads.button;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.link.RevMobLink;
import com.revmob.android.RevMobContext;

public class RevMobButton extends Button {
    /* access modifiers changed from: private */
    public Activity activity;
    private String placementId;
    private RevMobAdsListener publisherListener;
    private AdState state;

    public RevMobButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.state = AdState.CREATED;
        setClickListener();
    }

    public RevMobButton(Activity activity2, RevMobAdsListener publisherListener2) {
        this(activity2.getApplicationContext(), (AttributeSet) null);
        this.publisherListener = publisherListener2;
    }

    public RevMobButton(Activity activity2, RevMobAdsListener publisherListener2, String placementId2) {
        this(activity2, publisherListener2);
        this.placementId = placementId2;
    }

    public void setListener(RevMobAdsListener listener) {
        this.publisherListener = listener;
    }

    public void setPlacementId(String placementId2) {
        this.placementId = placementId2;
    }

    private boolean isLoaded() {
        return true;
    }

    private void setClickListener() {
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RevMobButton.this.activity = (Activity) RevMobContext.activity;
                if (RevMobButton.this.activity == null) {
                    RevMobButton.this.performLinkClick();
                } else {
                    RevMobButton.this.activity.runOnUiThread(new Runnable() {
                        public void run() {
                            RevMobButton.this.performLinkClick();
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void performLinkClick() {
        this.state = AdState.CLICKED;
        createLink().open();
    }

    private RevMobLink createLink() {
        RevMobLink ad = new RevMobLink(this.activity, getPublisherListener());
        ad.load(this.placementId);
        return ad;
    }

    private RevMobAdsListener getPublisherListener() {
        return this.publisherListener != null ? this.publisherListener : RevMobContext.publisherListener;
    }
}
