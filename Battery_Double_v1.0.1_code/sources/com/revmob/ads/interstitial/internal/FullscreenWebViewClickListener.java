package com.revmob.ads.interstitial.internal;

import android.webkit.WebView;
import com.revmob.FullscreenActivity;
import com.revmob.internal.RevMobWebViewClient.RevMobWebViewClickListener;

public class FullscreenWebViewClickListener extends FullscreenClickListener implements RevMobWebViewClickListener {
    public FullscreenWebViewClickListener(FullscreenActivity fullscreenActivity) {
        super(fullscreenActivity);
    }

    public boolean handleClick(WebView view, String url) {
        if (url.endsWith("#close")) {
            return onClose();
        }
        if (url.endsWith("#click")) {
            return onClick();
        }
        return true;
    }

    public void handlePageFinished(WebView view, String url) {
        this.fullscreenActivity.removeProgressBar();
    }
}
