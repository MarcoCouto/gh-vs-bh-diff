package com.revmob.ads.interstitial.internal;

import android.webkit.WebView;
import com.revmob.FullscreenActivity;
import com.revmob.internal.RevMobWebViewClient.RevMobWebViewClickListener;

public class FullscreenDSPClickListener extends FullscreenClickListener implements RevMobWebViewClickListener {
    public FullscreenDSPClickListener(FullscreenActivity fullscreenActivity) {
        super(fullscreenActivity);
    }

    public boolean handleClick(WebView view, String url) {
        if (url.endsWith("#close") || url.endsWith("inneractive-skip")) {
            return onClose();
        }
        this.fullscreenActivity.data.setDspUrl(url);
        onClick();
        return false;
    }

    public void handlePageFinished(WebView view, String url) {
        this.fullscreenActivity.removeProgressBar();
    }
}
