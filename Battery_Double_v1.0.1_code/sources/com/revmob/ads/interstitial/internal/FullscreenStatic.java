package com.revmob.ads.interstitial.internal;

import android.app.Activity;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.revmob.ads.interstitial.client.FullscreenData;
import com.revmob.internal.AndroidHelper;

public class FullscreenStatic extends RelativeLayout implements FullscreenView {
    private int amplitudeMax = 28;
    private FullscreenData data;
    private ImageView fullscreenImageView;
    private int height;
    private int initialAngleX = -999;
    private int initialAngleY = -999;
    private int initialX;
    private int initialY;
    private int parallaxDelta = 0;
    private boolean parallaxEnabled = false;
    private int width;
    private int x = 0;
    private int y = 0;

    public FullscreenStatic(Activity context, FullscreenData data2, FullscreenClickListener clickListener, boolean parallaxEnabled2, int parallaxDelta2) {
        super(context);
        this.data = data2;
        this.parallaxEnabled = parallaxEnabled2;
        if (parallaxDelta2 > this.amplitudeMax) {
            parallaxDelta2 = this.amplitudeMax;
        }
        this.parallaxDelta = parallaxDelta2;
        if (parallaxEnabled2) {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            this.width = metrics.widthPixels;
            this.height = metrics.heightPixels;
            this.initialX = 0;
            this.initialY = 0;
            this.x = this.initialX;
            this.y = this.initialY;
            setWillNotDraw(false);
            LayoutParams params = new LayoutParams(this.width, this.height);
            setGravity(48);
            params.leftMargin = 0;
            params.topMargin = 0;
            addView(createImageAdView(clickListener), params);
            return;
        }
        addView(createImageAdView(clickListener), new LayoutParams(-1, -1));
    }

    public void update() {
        if (this.fullscreenImageView != null) {
            this.fullscreenImageView.setImageBitmap(this.data.getAdImage(getResources().getConfiguration().orientation));
            if (this.data.isStaticMultiOrientationFullscreen() || this.data.getOrientationLock() == 1) {
                this.fullscreenImageView.setScaleType(ScaleType.FIT_XY);
            } else {
                this.fullscreenImageView.setScaleType(ScaleType.CENTER_CROP);
            }
        }
    }

    private View createImageAdView(final FullscreenClickListener clickListener) {
        this.fullscreenImageView = new ImageView(getContext());
        update();
        this.fullscreenImageView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                clickListener.onClick();
            }
        });
        return this.fullscreenImageView;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.parallaxEnabled) {
            canvas.translate((float) this.x, (float) this.y);
        }
        super.onDraw(canvas);
    }

    public void updateAccordingToDevicePosition(int xAxisAngle, int yAxisAngle) {
        if (this.parallaxEnabled) {
            if (this.initialAngleX == -999) {
                this.initialAngleX = xAxisAngle;
            }
            if (this.initialAngleY == -999) {
                this.initialAngleY = yAxisAngle;
            }
            int deltaX = xAxisAngle - this.initialAngleX;
            if (deltaX >= 0) {
                this.x = Math.max(this.initialX - this.parallaxDelta, this.initialX - Math.abs(deltaX));
            } else {
                this.x = Math.min(this.initialX + this.parallaxDelta, this.initialX + Math.abs(deltaX));
            }
            int deltaY = yAxisAngle - this.initialAngleY;
            if (deltaY >= 0) {
                this.y = Math.max(this.initialY - this.parallaxDelta, this.initialY - Math.abs(deltaY));
            } else {
                this.y = Math.min(this.initialY + this.parallaxDelta, this.initialY + Math.abs(deltaY));
            }
            if (AndroidHelper.isUIThread()) {
                invalidate();
            } else {
                postInvalidate();
            }
        }
    }
}
