package com.revmob.ads.interstitial.internal;

import android.content.Context;
import com.revmob.internal.RevMobWebView;
import com.revmob.internal.RevMobWebViewClient;

public class FullscreenWebview extends RevMobWebView implements FullscreenView {
    public FullscreenWebview(Context context, RevMobWebViewClient client) {
        super(context, client);
    }

    public FullscreenWebview(Context context, String url, String html, RevMobWebViewClient client) {
        super(context, url, html, client);
    }

    public void update() {
    }
}
