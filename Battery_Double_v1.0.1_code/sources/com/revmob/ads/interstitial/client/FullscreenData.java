package com.revmob.ads.interstitial.client;

import android.graphics.Bitmap;
import android.view.animation.Animation;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.AnimationConfiguration;
import com.revmob.client.AdData;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class FullscreenData extends AdData {
    public static final String KEY = "com.revmob.ads.fullscreen.adUrl";
    private static Map<String, FullscreenData> loadedFullscreens = new HashMap();
    private String clickSoundURL;
    private AnimationConfiguration closeAnimation;
    private boolean doNotShow = false;
    private String dspHtml;
    private String dspUrl;
    private Bitmap frameLandscape = null;
    private Bitmap framePortrait = null;
    private double fullscreenPercentage;
    private ArrayList<ArrayList> gettingEvents;
    private String htmlAdUrl;
    private String htmlCode;
    private int imageHeight;
    private Bitmap imageLandscape;
    private Bitmap imagePortrait;
    private int[] imageSize;
    private Bitmap imageSquare;
    private int imageWidth;
    private int orientationLock;
    private int parallaxDelta = 0;
    private Bitmap postRoll;
    private int postRollAlpha;
    private int postRollBlue;
    private int postRollGreen;
    private double postRollHeight;
    private int postRollRed;
    private String postRollText;
    private double postRollWidth;
    private int preRollAlpha;
    private int preRollBlue;
    private int preRollGreen;
    private double preRollHeight;
    private Bitmap preRollLandscape;
    private Bitmap preRollPortrait;
    private int preRollRed;
    private String preRollText;
    private double preRollWidth;
    private RevMobAdsListener publisherListener;
    private Bitmap replayButton;
    private Bitmap revmobLogo;
    private AnimationConfiguration showAnimation;
    private String showSoundURL;
    private Bitmap soundOff;
    private Bitmap soundOn;
    private int timeLeftTime;
    private ArrayList<ArrayList> trackingEvents;
    private int videoEnd;
    private File videoFile;
    private int videoOrientation;
    private double videoSkipTime;
    private String videoUrl;
    private String volumeMute;
    private String volumeUp;

    public static void addLoadedFullscreen(FullscreenData ad) {
        loadedFullscreens.put(ad.getClickUrl(), ad);
    }

    public static FullscreenData getLoadedFullscreen(String key) {
        return (FullscreenData) loadedFullscreens.get(key);
    }

    public static void cleanLoadedFullscreen(FullscreenData ad) {
        if (ad != null) {
            loadedFullscreens.remove(ad.getClickUrl());
        }
    }

    public FullscreenData(String impressionUrl, String clickUrl, boolean followRedirect, RevMobAdsListener publisherListener2, String htmlAdUrl2, String htmlCode2, String dspUrl2, String dspHtml2, Bitmap imageSquare2, Bitmap imageLandscape2, Bitmap imagePortrait2, AnimationConfiguration showAnimation2, AnimationConfiguration closeAnimation2, String appOrSite, boolean openInside, String showSoundURL2, String clickSoundURL2, int parallaxDelta2, double videoSkipTime2, int timeLeftTime2, int videoEnd2, String videoUrl2, Bitmap replayButton2, Bitmap revmobLogo2, String volumeMute2, String volumeUp2, File videoFile2, Bitmap soundOn2, Bitmap soundOff2, ArrayList<ArrayList> trackingEvent, ArrayList<ArrayList> gettingEvents2, Bitmap framePortrait2, Bitmap frameLandscape2, int imageHeight2, int imageWidth2, Bitmap preRollLandscape2, Bitmap preRollPortrait2, Bitmap postRoll2, double preRollHeight2, double preRollWidth2, double postRollHeight2, double postRollWidth2, String preRollText2, String postRollText2, int preRollRed2, int preRollGreen2, int preRollBlue2, int preRollAlpha2, int postRollRed2, int postRollGreen2, int postRollBlue2, int postRollAlpha2, int videoOrientation2, int[] imageSize2, double fullscreenPercentage2, int orientationLock2, boolean doNotShow2) {
        super(impressionUrl, clickUrl, followRedirect, appOrSite, openInside);
        this.publisherListener = publisherListener2;
        this.htmlCode = htmlCode2;
        this.htmlAdUrl = htmlAdUrl2;
        this.dspUrl = dspUrl2;
        this.dspHtml = dspHtml2;
        this.imagePortrait = imagePortrait2;
        this.imageLandscape = imageLandscape2;
        this.framePortrait = framePortrait2;
        this.frameLandscape = frameLandscape2;
        this.imageHeight = imageHeight2;
        this.imageWidth = imageWidth2;
        this.imageSquare = imageSquare2;
        this.showAnimation = showAnimation2;
        this.closeAnimation = closeAnimation2;
        this.showSoundURL = showSoundURL2;
        this.clickSoundURL = clickSoundURL2;
        this.parallaxDelta = parallaxDelta2;
        this.videoSkipTime = videoSkipTime2;
        this.timeLeftTime = timeLeftTime2;
        this.soundOff = soundOff2;
        this.soundOn = soundOn2;
        this.videoEnd = videoEnd2;
        this.videoUrl = videoUrl2;
        this.replayButton = replayButton2;
        this.revmobLogo = revmobLogo2;
        this.volumeMute = volumeMute2;
        this.volumeUp = volumeUp2;
        this.videoFile = videoFile2;
        this.trackingEvents = trackingEvent;
        this.gettingEvents = gettingEvents2;
        this.preRollLandscape = preRollLandscape2;
        this.preRollPortrait = preRollPortrait2;
        this.preRollHeight = preRollHeight2;
        this.preRollWidth = preRollWidth2;
        this.postRoll = postRoll2;
        this.postRollHeight = postRollHeight2;
        this.postRollWidth = postRollWidth2;
        this.preRollText = preRollText2;
        this.preRollRed = preRollRed2;
        this.preRollGreen = preRollGreen2;
        this.preRollBlue = preRollBlue2;
        this.preRollAlpha = preRollAlpha2;
        this.postRollText = postRollText2;
        this.postRollRed = postRollRed2;
        this.postRollGreen = postRollGreen2;
        this.postRollBlue = postRollBlue2;
        this.postRollAlpha = postRollAlpha2;
        this.videoOrientation = videoOrientation2;
        this.imageSize = imageSize2;
        this.fullscreenPercentage = fullscreenPercentage2;
        this.orientationLock = orientationLock2;
        this.doNotShow = doNotShow2;
    }

    public String getCampaignId() {
        String url = getClickUrl();
        this.campaignId = "";
        try {
            List<NameValuePair> parameters = URLEncodedUtils.parse(new URI(url), "UTF-8");
            if (parameters.size() > 0) {
                for (NameValuePair p : parameters) {
                    if (p.getName().equals("campaign_id")) {
                        this.campaignId = p.getValue();
                    }
                }
            } else {
                this.campaignId = "Testing Mode";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return this.campaignId;
    }

    public Bitmap getAdImage(int orientation) {
        if (orientation == 2 && this.imageLandscape != null) {
            return this.imageLandscape;
        }
        if (orientation != 1 || this.imagePortrait == null) {
            return this.imageSquare;
        }
        return this.imagePortrait;
    }

    public boolean isHtmlCodeLoaded() {
        return (this.htmlCode == null || this.htmlCode == "") ? false : true;
    }

    public String getHtmlCode() {
        return this.htmlCode;
    }

    public String getHtmlAdUrl() {
        return this.htmlAdUrl;
    }

    public String getDspUrl() {
        return this.dspUrl;
    }

    public String getDspHtml() {
        return this.dspHtml;
    }

    public RevMobAdsListener getPublisherListener() {
        return this.publisherListener;
    }

    public boolean isHtmlFullscreen() {
        return (this.htmlAdUrl == null && this.htmlCode == null) ? false : true;
    }

    public boolean isDspFullscreen() {
        return (this.dspUrl == null && this.dspHtml == null) ? false : true;
    }

    public boolean isStaticMultiOrientationFullscreen() {
        return (this.imagePortrait == null || this.imageLandscape == null) ? false : true;
    }

    public Animation getShowAnimation() {
        return this.showAnimation.getAnimation();
    }

    public Animation getCloseAnimation() {
        return this.closeAnimation.getAnimation();
    }

    public int getParallaxDelta() {
        return this.parallaxDelta;
    }

    public String getShowSoundURL() {
        return this.showSoundURL;
    }

    public String getClickSoundURL() {
        return this.clickSoundURL;
    }

    public void setDspUrl(String dspUrl2) {
        this.dspUrl = dspUrl2;
    }

    public double getVideoSkipTime() {
        return this.videoSkipTime;
    }

    public int getTimeLeftTime() {
        return this.timeLeftTime;
    }

    public int getVideoEnd() {
        return this.videoEnd;
    }

    public String getVideoUrl() {
        return this.videoUrl;
    }

    public Bitmap getReplayButton() {
        return this.replayButton;
    }

    public Bitmap getRevmobLogo() {
        return this.revmobLogo;
    }

    public String getVolumeMute() {
        return this.volumeMute;
    }

    public String getVolumeUp() {
        return this.volumeUp;
    }

    public File getVideoFile() {
        return this.videoFile;
    }

    public Bitmap getSoundOn() {
        return this.soundOn;
    }

    public Bitmap getSoundOff() {
        return this.soundOff;
    }

    public ArrayList<ArrayList> getTrackingEvents() {
        return this.trackingEvents;
    }

    public ArrayList<ArrayList> getGettingEvents() {
        return this.gettingEvents;
    }

    public Bitmap getFrame(int currentOrientation) {
        if (currentOrientation == 2) {
            return this.frameLandscape;
        }
        return this.framePortrait;
    }

    public int getImageWidth() {
        return this.imageWidth;
    }

    public int getImageHeight() {
        return this.imageHeight;
    }

    public double getPreRollHeight() {
        return this.preRollHeight;
    }

    public double getPreRollWidth() {
        return this.preRollWidth;
    }

    public double getPostRollHeight() {
        return this.postRollHeight;
    }

    public double getPostRollWidth() {
        return this.postRollWidth;
    }

    public Bitmap getPreRollLandscape() {
        return this.preRollLandscape;
    }

    public Bitmap getPreRollPortrait() {
        return this.preRollPortrait;
    }

    public Bitmap getPostRoll() {
        return this.postRoll;
    }

    public String getPreRollText() {
        return this.preRollText;
    }

    public String getPostRollText() {
        return this.postRollText;
    }

    public int getPreRollRed() {
        return this.preRollRed;
    }

    public int getPreRollGreen() {
        return this.preRollGreen;
    }

    public int getPreRollBlue() {
        return this.preRollBlue;
    }

    public int getPreRollAlpha() {
        return this.preRollAlpha;
    }

    public int getPostRollRed() {
        return this.postRollRed;
    }

    public int getPostRollGreen() {
        return this.postRollGreen;
    }

    public int getPostRollBlue() {
        return this.postRollBlue;
    }

    public int getPostRollAlpha() {
        return this.postRollAlpha;
    }

    public int getVideoOrientation() {
        return this.videoOrientation;
    }

    public int[] getImageSize() {
        return this.imageSize;
    }

    public double getFullscreenPercentage() {
        return this.fullscreenPercentage;
    }

    public int getOrientationLock() {
        return this.orientationLock;
    }

    public boolean getDoNotShow() {
        return this.doNotShow;
    }
}
