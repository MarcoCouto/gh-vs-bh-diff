package com.revmob.ads.interstitial.client;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import com.revmob.ads.internal.CloseAnimationConfiguration;
import com.revmob.ads.internal.ShowAnimationConfiguration;
import com.revmob.ads.internal.StaticAssets;
import com.revmob.client.RevMobClient;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FullscreenClientListener extends AdRevMobClientListener {
    private static Activity activity;
    private static boolean success;
    private boolean shallUpdateData = true;

    public FullscreenClientListener(Ad ad, RevMobAdsListener publisherListener, Activity activity2) {
        super(ad, publisherListener);
        activity = activity2;
    }

    public void handleResponse(String response) throws JSONException {
        FullscreenData data = createData(response, this.publisherListener);
        if (data != null && this.shallUpdateData) {
            RevMobClient.sett3(System.currentTimeMillis());
            this.ad.updateWithData(data);
        } else if (!this.shallUpdateData) {
            this.shallUpdateData = true;
        } else if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdNotReceived("An error occurred during the ad download.");
        }
    }

    public static FullscreenData createData(String response, RevMobAdsListener publisherListener) throws JSONException {
        Bitmap replayButton;
        Bitmap soundOn;
        Bitmap soundOff;
        Bitmap revMobLogo;
        File videoFile;
        JSONArray vastTrackingEvents;
        JSONArray vastClicks;
        JSONArray vastImpressions;
        JSONArray vastErrors;
        JSONObject jSONObject = new JSONObject(response);
        JSONObject json = jSONObject.getJSONObject("fullscreen");
        String appOrSite = getAppOrSite(json);
        boolean openInside = getOpenInside(json);
        JSONArray links = json.getJSONArray("links");
        int currentOrientation = activity.getResources().getConfiguration().orientation;
        String clickUrl = getClickUrl(links);
        boolean followRedirect = getFollowRedirect(json);
        String impressionUrl = getImpressionUrl(links);
        String htmlUrl = getLinkByRel(links, "html");
        String dspUrl = getLinkByRel(links, "dsp_url");
        String dspHtml = getLinkByRel(links, "dsp_html");
        String imagePortraitUrl = getLinkByRel(links, "image_portrait");
        String imageLandscapeUrl = getLinkByRel(links, "image_landscape");
        String framePortraitUrl = getLinkByRel(links, "framePortrait");
        String frameLandscapeUrl = getLinkByRel(links, "frameLandscape");
        String preRollUrlLandscape = getStringKeyByRel(links, "preRoll", "landscape");
        String preRollUrlPortrait = getStringKeyByRel(links, "preRoll", "portrait");
        String preRollText = getStringKeyByRel(links, "preRoll", "message");
        int preRollRed = getIntKeyByRel(links, "preRoll", "red");
        int preRollGreen = getIntKeyByRel(links, "preRoll", "green");
        int preRollBlue = getIntKeyByRel(links, "preRoll", "blue");
        int preRollAlpha = getIntKeyByRel(links, "preRoll", "alpha");
        String postRollUrl = getStringKeyByRel(links, "postRoll", "href");
        String postRollText = getStringKeyByRel(links, "postRoll", "message");
        int postRollRed = getIntKeyByRel(links, "preRoll", "red");
        int postRollGreen = getIntKeyByRel(links, "preRoll", "green");
        int postRollBlue = getIntKeyByRel(links, "preRoll", "blue");
        int postRollAlpha = getIntKeyByRel(links, "preRoll", "alpha");
        Bitmap preRollPortrait = null;
        Bitmap preRollLandscape = null;
        Bitmap postRoll = null;
        double preRollWidth = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        double preRollHeight = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        double postRollWidth = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        double postRollHeight = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        int videoOrientation = getVideoInt(json, "orientation");
        int orientationLock = getVideoInt(json, "orientationLock");
        String imageSquareUrl = getLinkByRel(links, "image");
        String imageSizeString = getLinkByRel(links, "imageSize");
        double videoSkipTime = getVideoDouble(json, "videoSkipTime");
        int timeLeftTime = getVideoInt(json, "timeLeftTime");
        int videoEnd = getVideoInt(json, "videoEnd");
        double fullscreenPercentage = getVideoDouble(json, "fullscreenPercentage");
        String videoUrl = getLinkByRel(links, "video");
        String replayButtonUrl = getLinkByRel(links, "replay_button");
        String revmobLogoUrl = getLinkByRel(links, "revmob_logo");
        String volumeMute = getLinkByRel(links, "volume_mute");
        String volumeUp = getLinkByRel(links, "volume_up");
        String soundOnUrl = getLinkByRel(links, "sound");
        String soundOffUrl = getLinkByRel(links, "sound_off");
        String closeButtonUrl = getLinkByRel(links, "close_button");
        int imageHeight = 0;
        int imageWidth = 0;
        ArrayList<ArrayList> trackingEvents = new ArrayList<>();
        ArrayList<ArrayList> gettingEvents = new ArrayList<>();
        ArrayList<String> clicks = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();
        ArrayList<String> impressions = new ArrayList<>();
        ArrayList arrayList = new ArrayList();
        int[] imageSize = new int[2];
        boolean imageSizeWrong = false;
        boolean doNotShow = false;
        success = true;
        clicks.add("clicks");
        errors.add("errors");
        impressions.add("impressions");
        try {
            String[] parts = imageSizeString.split(",");
            imageSize[0] = Integer.parseInt(parts[0]);
            imageSize[1] = Integer.parseInt(parts[1]);
        } catch (Exception e) {
            imageSize[0] = 0;
            imageSize[1] = 0;
        }
        if (videoUrl != null) {
            if (json.has("vast")) {
                JSONObject vast = json.getJSONObject("vast");
                if (vast.has("trackingEvents")) {
                    vastTrackingEvents = vast.getJSONArray("trackingEvents");
                } else {
                    vastTrackingEvents = null;
                }
                if (vast.has("clicks")) {
                    vastClicks = vast.getJSONArray("clicks");
                } else {
                    vastClicks = null;
                }
                if (vast.has("impressions")) {
                    vastImpressions = vast.getJSONArray("impressions");
                } else {
                    vastImpressions = null;
                }
                if (vast.has("errors")) {
                    vastErrors = vast.getJSONArray("errors");
                } else {
                    vastErrors = null;
                }
                if (vastTrackingEvents != null) {
                    parseTrackingEvents(arrayList, trackingEvents, vastTrackingEvents);
                }
                if (vastClicks != null) {
                    getAllStringsInJSONArray(vastClicks, clicks);
                }
                if (vastErrors != null) {
                    getAllStringsInJSONArray(vastErrors, errors);
                }
                if (vastImpressions != null) {
                    getAllStringsInJSONArray(vastImpressions, impressions);
                }
                gettingEvents.add(clicks);
                gettingEvents.add(impressions);
                gettingEvents.add(errors);
            }
        }
        int parallaxDelta = 0;
        ShowAnimationConfiguration showAnimationConfiguration = new ShowAnimationConfiguration();
        CloseAnimationConfiguration closeAnimationConfiguration = new CloseAnimationConfiguration();
        try {
            JSONObject animation = json.getJSONObject("animation");
            long duration = animation.getLong("duration");
            showAnimationConfiguration.setTime(duration);
            closeAnimationConfiguration.setTime(duration);
            JSONArray jsonArray = animation.getJSONArray("show");
            for (int i = 0; i < jsonArray.length(); i++) {
                showAnimationConfiguration.addAnimation(jsonArray.getString(i));
            }
            JSONArray jsonArray2 = animation.getJSONArray("close");
            for (int i2 = 0; i2 < jsonArray2.length(); i2++) {
                closeAnimationConfiguration.addAnimation(jsonArray2.getString(i2));
            }
            parallaxDelta = animation.getInt("parallax_delta");
        } catch (JSONException e2) {
        }
        String showSoundURL = new String();
        String clickSoundURL = new String();
        try {
            if (json.has("sound")) {
                JSONObject sound = json.getJSONObject("sound");
                if (sound.has("on_show")) {
                    showSoundURL = sound.getString("on_show");
                }
                if (sound.has("on_click")) {
                    clickSoundURL = sound.getString("on_click");
                }
            }
        } catch (JSONException e3) {
        }
        HTTPHelper helper = new HTTPHelper();
        String htmlCode = null;
        Bitmap imageSquare = null;
        Bitmap imagePortrait = null;
        Bitmap imageLandscape = null;
        Bitmap framePortrait = null;
        Bitmap frameLandscape = null;
        RevMobClient.sett2(System.currentTimeMillis());
        if (dspUrl == null && dspHtml == null) {
            if (htmlUrl != null) {
                htmlCode = helper.downloadHtml(htmlUrl);
            } else if (imagePortraitUrl != null && imageLandscapeUrl != null) {
                imagePortrait = BitmapFactory.decodeFile(downloadFile(imagePortraitUrl, true).getAbsolutePath());
                imageLandscape = BitmapFactory.decodeFile(downloadFile(imageLandscapeUrl, true).getAbsolutePath());
                if (imagePortrait == null || imageLandscape == null) {
                    doNotShow = true;
                } else {
                    imageWidth = imagePortrait.getWidth();
                    imageHeight = imagePortrait.getHeight();
                    int imageLandscapeWidth = imageLandscape.getWidth();
                    if (imageLandscape.getHeight() == imageHeight || imageWidth == imageLandscapeWidth) {
                        if ((imageHeight <= imageWidth || currentOrientation != 2) && (imageHeight >= imageWidth || currentOrientation != 1)) {
                            orientationLock = 1;
                            imageSquare = imagePortrait;
                            imageSizeWrong = true;
                        } else {
                            imagePortrait = null;
                            imageLandscape = null;
                            doNotShow = true;
                        }
                    }
                    if (!(imageSize == null || imageSize[0] == 0 || imageSize[1] == 0)) {
                        if (!(imageHeight == imageSize[0] || imageHeight == imageSize[1] || imageHeight % imageSize[0] == 0 || imageHeight % imageSize[1] == 0 || imageSize[0] % imageHeight == 0 || imageSize[1] % imageHeight == 0)) {
                            imageSizeWrong = true;
                        }
                        if (!(imageWidth == imageSize[0] || imageWidth == imageSize[1] || imageWidth % imageSize[0] == 0 || imageWidth % imageSize[1] == 0 || imageSize[0] % imageWidth == 0 || imageSize[1] % imageWidth == 0)) {
                            imageSizeWrong = true;
                        }
                    }
                }
                if (!imageSizeWrong) {
                    if (framePortraitUrl != null) {
                        framePortrait = BitmapFactory.decodeFile(downloadFile(framePortraitUrl, true).getAbsolutePath());
                        success = true;
                    }
                    if (frameLandscapeUrl != null) {
                        frameLandscape = BitmapFactory.decodeFile(downloadFile(frameLandscapeUrl, true).getAbsolutePath());
                        success = true;
                    }
                }
            } else if (imageSquareUrl != null) {
                imageSquare = BitmapFactory.decodeFile(downloadFile(imageSquareUrl, true).getAbsolutePath());
                imageWidth = imageSquare.getWidth();
                imageHeight = imageSquare.getHeight();
            } else {
                imageSquare = null;
            }
        } else if (0 == 0) {
            if (framePortraitUrl != null) {
                framePortrait = BitmapFactory.decodeFile(downloadFile(framePortraitUrl, true).getAbsolutePath());
                success = true;
                RMLog.e("framePortrait " + framePortrait);
            }
            if (frameLandscapeUrl != null) {
                frameLandscape = BitmapFactory.decodeFile(downloadFile(frameLandscapeUrl, true).getAbsolutePath());
                success = true;
                RMLog.e("frameLandscape " + frameLandscape);
            }
        }
        if (closeButtonUrl != null) {
            StaticAssets.setCloseButton(Drawable.createFromPath(downloadFile(closeButtonUrl, true).getAbsolutePath()));
        }
        if (replayButtonUrl != null) {
            replayButton = BitmapFactory.decodeFile(downloadFile(replayButtonUrl, true).getAbsolutePath());
        } else {
            replayButton = null;
        }
        if (soundOnUrl != null) {
            soundOn = BitmapFactory.decodeFile(downloadFile(soundOnUrl, true).getAbsolutePath());
        } else {
            soundOn = null;
        }
        if (soundOffUrl != null) {
            soundOff = BitmapFactory.decodeFile(downloadFile(soundOffUrl, true).getAbsolutePath());
        } else {
            soundOff = null;
        }
        try {
            boolean rewarded = json.getBoolean("rewarded");
        } catch (JSONException e4) {
        }
        if (revmobLogoUrl != null) {
            revMobLogo = BitmapFactory.decodeFile(downloadFile(revmobLogoUrl, true).getAbsolutePath());
        } else {
            revMobLogo = null;
        }
        if (videoUrl != null) {
            videoFile = downloadFile(videoUrl, true);
            if (!(preRollUrlLandscape == null || preRollUrlPortrait == null)) {
                preRollLandscape = BitmapFactory.decodeFile(downloadFile(preRollUrlLandscape, true).getAbsolutePath());
                preRollPortrait = BitmapFactory.decodeFile(downloadFile(preRollUrlPortrait, true).getAbsolutePath());
                if (preRollPortrait != null) {
                    preRollWidth = (double) preRollPortrait.getWidth();
                    preRollHeight = (double) preRollPortrait.getHeight();
                }
            }
            if (postRollUrl != null) {
                postRoll = BitmapFactory.decodeFile(downloadFile(postRollUrl, true).getAbsolutePath());
                if (postRoll != null) {
                    postRollWidth = (double) postRoll.getWidth();
                    postRollHeight = (double) postRoll.getHeight();
                }
            }
        } else {
            videoFile = null;
        }
        if (imageSizeWrong) {
            framePortrait = null;
            frameLandscape = null;
        }
        if (success) {
            return new FullscreenData(impressionUrl, clickUrl, followRedirect, publisherListener, htmlUrl, htmlCode, dspUrl, dspHtml, imageSquare, imageLandscape, imagePortrait, showAnimationConfiguration, closeAnimationConfiguration, appOrSite, openInside, showSoundURL, clickSoundURL, parallaxDelta, videoSkipTime, timeLeftTime, videoEnd, videoUrl, replayButton, revMobLogo, volumeMute, volumeUp, videoFile, soundOn, soundOff, trackingEvents, gettingEvents, framePortrait, frameLandscape, imageHeight, imageWidth, preRollPortrait, preRollLandscape, postRoll, preRollHeight, preRollWidth, postRollHeight, postRollWidth, preRollText, postRollText, preRollRed, preRollGreen, preRollBlue, preRollAlpha, postRollRed, postRollGreen, postRollBlue, postRollAlpha, videoOrientation, imageSize, fullscreenPercentage, orientationLock, doNotShow);
        }
        return null;
    }

    public void setShallUpdateData(boolean update) {
        this.shallUpdateData = update;
    }

    public boolean getShallUpdateData() {
        return this.shallUpdateData;
    }

    private static File downloadFile(String fileUrl, boolean willCache) {
        String filePath;
        String fileName;
        File file = null;
        AeSimpleSHA1 sha1 = new AeSimpleSHA1();
        try {
            URL url = new URL(fileUrl);
            if (!willCache) {
                filePath = activity.getApplicationContext().getFilesDir().getPath();
            } else {
                filePath = activity.getApplicationContext().getCacheDir().getPath();
            }
            try {
                fileName = sha1.SHA1(fileUrl);
            } catch (NoSuchAlgorithmException e) {
                fileName = Uri.parse(fileUrl).getLastPathSegment();
                e.printStackTrace();
            }
            File file2 = new File(filePath, fileName);
            try {
                String filePath2 = file2.getAbsolutePath();
                if (!file2.exists()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream());
                    ByteArrayBuffer baf = new ByteArrayBuffer(50);
                    while (true) {
                        int current = bis.read();
                        if (current == -1) {
                            break;
                        }
                        baf.append((byte) current);
                    }
                    FileOutputStream fos = new FileOutputStream(file2);
                    fos.write(baf.toByteArray());
                    fos.close();
                }
                return file2;
            } catch (IOException e2) {
                e = e2;
                file = file2;
                success = false;
                e.printStackTrace();
                return file;
            }
        } catch (IOException e3) {
            e = e3;
            success = false;
            e.printStackTrace();
            return file;
        }
    }

    private static void parseTrackingEvents(List<String> relArray, ArrayList<ArrayList> trackingEvents, JSONArray vastTrackingEvents) throws JSONException {
        for (int i = 0; i < 13; i++) {
            String rel = null;
            switch (i) {
                case 0:
                    rel = "midpoint";
                    break;
                case 1:
                    rel = "thirdQuartile";
                    break;
                case 2:
                    rel = "complete";
                    break;
                case 3:
                    rel = "creativeView";
                    break;
                case 4:
                    rel = "start";
                    break;
                case 5:
                    rel = "firstQuartile";
                    break;
                case 6:
                    rel = "mute";
                    break;
                case 7:
                    rel = "unmute";
                    break;
                case 8:
                    rel = "close";
                    break;
                case 9:
                    rel = "pause";
                    break;
                case 10:
                    rel = "resume";
                    break;
                case 11:
                    rel = "fullscreenPortraitView";
                    break;
                case 12:
                    rel = "fullscreenLandscapeView";
                    break;
            }
            relArray.add(rel);
        }
        for (int i2 = 0; i2 < relArray.size(); i2++) {
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(relArray.get(i2));
            trackingEvents.add(arrayList);
            getAllLinksByRel(vastTrackingEvents, (String) arrayList.get(0), arrayList);
            for (int j = 1; j < arrayList.size(); j++) {
            }
        }
    }
}
