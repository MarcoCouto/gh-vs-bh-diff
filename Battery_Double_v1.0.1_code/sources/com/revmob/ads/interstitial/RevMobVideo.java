package com.revmob.ads.interstitial;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.revmob.FullscreenActivity;
import com.revmob.RevMob;
import com.revmob.ads.internal.StaticAssets;
import com.revmob.ads.interstitial.client.FullscreenData;
import com.revmob.ads.interstitial.internal.FullscreenClickListener;
import com.revmob.ads.interstitial.internal.FullscreenView;
import com.revmob.android.RevMobContext;
import com.revmob.client.RevMobClient;
import com.revmob.internal.AndroidHelper;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobVideoPlayer;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;

@TargetApi(10)
public class RevMobVideo extends RelativeLayout implements FullscreenView {
    private static int buttonSize;
    private static int padding;
    public final String FORMAT = "%02d";
    public ImageView blackBackground;
    int cPosition = 0;
    private boolean changedVideoBackground;
    private int chosenVideoOrientation;
    public boolean clicked = false;
    public boolean closed = false;
    private boolean completedVideo;
    public TextView countdownTimer;
    public FullscreenData data;
    private DownloadManager dm;
    private long enqueue;
    private RelativeLayout fatherLayout;
    private Bitmap finalFrame;
    private float firstQuartile;
    private FrameLayout frameLayout;
    protected FullscreenActivity fullScreenActivity;
    private ImageView fullscreenImageViewForVideo;
    public Handler handler = new Handler();
    public ImageView imageCloseButton;
    public Boolean isTherePostRoll = Boolean.valueOf(false);
    public RelativeLayout layout;
    public RevMobVideoPlayer mediaPlayer;
    private float midPoint;
    public ImageView muteButton;
    private boolean passedFirstQuartile;
    private boolean passedMidPoint;
    private boolean passedThirdQuartile;
    public ImageView pauseButton;
    private boolean pausedVideo;
    private boolean postedFullScreenLandscape;
    private boolean postedFullScreenPortrait;
    private int previousOrientation;
    private RelativeLayout relativeLayout;
    double relativePosition = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public ImageView replayButton;
    public boolean replayClicked = false;
    private boolean resumedVideo;
    private MediaMetadataRetriever retriever;
    public ImageView revmobLogo;
    public Boolean rewarded = Boolean.valueOf(false);
    public Runnable runnable;
    public double skipTime;
    public boolean started = false;
    private float thirdQuartile;
    public Handler timerHandler;
    /* access modifiers changed from: private */
    public Runnable timerRunnable;
    public VideoView video;
    public ImageView videoCloseButton;
    private int videoDuration;
    public File videoFile;
    public MediaPlayer videoMp;
    public float videoVolume;

    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage2) {
            this.bmImage = bmImage2;
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... urls) {
            Bitmap mIcon11 = null;
            try {
                return BitmapFactory.decodeStream(new URL(urls[0]).openStream());
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                return mIcon11;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            this.bmImage.setImageBitmap(result);
        }
    }

    @SuppressLint({"NewApi"})
    @TargetApi(10)
    public RevMobVideo(Activity context, FullscreenData data2, FullscreenClickListener clickListener, FullscreenActivity FullscreenActivity) {
        super(context);
        this.data = data2;
        this.fullScreenActivity = FullscreenActivity;
        this.fullScreenActivity.shouldNotSkipVideo = true;
        if (data2.getPostRoll() != null) {
            this.isTherePostRoll = Boolean.valueOf(true);
        }
        if (data2.getVideoSkipTime() > FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            this.skipTime = data2.getVideoSkipTime();
        } else {
            this.skipTime = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
        this.previousOrientation = this.fullScreenActivity.getRequestedOrientation();
        if (!this.fullScreenActivity.followAppOrientation) {
            if (data2.getVideoOrientation() == 1) {
                this.fullScreenActivity.setRequestedOrientation(7);
                this.chosenVideoOrientation = 1;
            } else {
                this.fullScreenActivity.setRequestedOrientation(6);
                this.chosenVideoOrientation = 2;
            }
        }
        this.rewarded = this.fullScreenActivity.isRewarded;
        setLayoutParams(new LayoutParams(-1, -1));
        this.relativeLayout = this;
        buttonSize = AndroidHelper.dipToPixels(getContext(), 40);
        padding = AndroidHelper.dipToPixels(getContext(), 20);
        FullscreenActivity fullscreenActivity = this.fullScreenActivity;
        getContext();
        this.videoVolume = fullscreenActivity.getPreferences(0).getFloat("RevMobVideoVolume", 1.0f);
        this.fatherLayout = new RelativeLayout(getContext());
        this.fatherLayout.setLayoutParams(new LayoutParams(-1, -1));
        this.fatherLayout.setVisibility(0);
        LayoutParams closeLayoutParams = new LayoutParams(buttonSize, buttonSize);
        closeLayoutParams.rightMargin = padding;
        closeLayoutParams.topMargin = padding;
        closeLayoutParams.addRule(11);
        closeLayoutParams.addRule(6);
        LayoutParams closeLayoutParams2 = new LayoutParams(buttonSize, buttonSize);
        closeLayoutParams2.leftMargin = padding;
        closeLayoutParams2.topMargin = padding;
        closeLayoutParams2.addRule(9);
        closeLayoutParams2.addRule(6);
        LayoutParams cTimerLayout = new LayoutParams(AndroidHelper.dipToPixels(getContext(), 30), AndroidHelper.dipToPixels(getContext(), 30));
        cTimerLayout.rightMargin = padding;
        cTimerLayout.bottomMargin = padding;
        cTimerLayout.addRule(11);
        cTimerLayout.addRule(12);
        addView(createVideoView());
        addBlackBackground();
        this.fatherLayout.addView(createCountDownTimer(), cTimerLayout);
        this.fatherLayout.addView(createMuteButton());
        this.fatherLayout.addView(createImageAdView(clickListener), new LayoutParams(-1, -1));
        this.fatherLayout.addView(createVideoCloseButton(), closeLayoutParams);
        this.fatherLayout.addView(createReplayButton(), closeLayoutParams2);
        this.fatherLayout.addView(createRevMobLogo());
        addView(this.fatherLayout);
        this.postedFullScreenPortrait = false;
        this.postedFullScreenLandscape = false;
        initializeBooelans();
    }

    private void addBlackBackground() {
        LayoutParams blackParams = new LayoutParams(-1, -1);
        this.blackBackground = new ImageView(getContext());
        blackParams.addRule(13);
        this.blackBackground.setLayoutParams(blackParams);
        this.blackBackground.setVisibility(8);
        this.blackBackground.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        addView(this.blackBackground);
    }

    private View createRevMobLogo() {
        this.revmobLogo = new ImageView(getContext());
        this.revmobLogo.setVisibility(0);
        this.revmobLogo.setImageBitmap(this.data.getRevmobLogo());
        putRevMobLogoUp();
        return this.revmobLogo;
    }

    private void putRevMobLogoUp() {
        LayoutParams revLayoutParams = new LayoutParams(100, 50);
        revLayoutParams.addRule(9);
        revLayoutParams.addRule(10);
        revLayoutParams.leftMargin = 0;
        revLayoutParams.topMargin = 0;
        this.revmobLogo.setLayoutParams(revLayoutParams);
    }

    private View createVideoCloseButton() {
        this.videoCloseButton = new ImageView(getContext());
        this.videoCloseButton.setVisibility(4);
        this.videoCloseButton.setImageDrawable(StaticAssets.getCloseButton());
        this.videoCloseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RevMobVideo.this.closed = true;
                if (!RevMobVideo.this.replayClicked) {
                    RevMobVideo.this.postEvent("close");
                }
                RevMobVideo.this.cancelCountDownTimer();
                RevMobVideo.this.removeHandlerCallbacks();
                RevMobVideo.this.video.stopPlayback();
                if (!RevMobVideo.this.isTherePostRoll.booleanValue() && RevMobVideo.this.fullScreenActivity.publisherListener != null) {
                    if (RevMobVideo.this.rewarded.booleanValue()) {
                        RevMobVideo.this.fullScreenActivity.publisherListener.onRevMobRewardedVideoCompleted();
                    } else {
                        RevMobVideo.this.fullScreenActivity.publisherListener.onRevMobAdDismissed();
                    }
                }
                RevMobVideo.this.fullScreenActivity.setRequestedOrientation(-1);
                RevMobVideo.this.fullScreenActivity.finishVideo();
                if (!RevMobVideo.this.isTherePostRoll.booleanValue()) {
                    RevMobVideo.this.fullScreenActivity.finish();
                }
            }
        });
        return this.videoCloseButton;
    }

    private View createCountDownTimer() {
        this.countdownTimer = new TextView(getContext());
        float radius = (float) AndroidHelper.dipToPixels(getContext(), 6);
        float radius2 = (float) AndroidHelper.dipToPixels(getContext(), 9);
        ShapeDrawable rectShapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{radius, radius2, radius, radius2, radius, radius2, radius, radius2}, null, null));
        Paint paint = rectShapeDrawable.getPaint();
        paint.setColor(-1);
        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth((float) AndroidHelper.dipToPixels(getContext(), 3));
        this.countdownTimer.setBackgroundDrawable(rectShapeDrawable);
        this.countdownTimer.setTextSize(15.0f);
        this.countdownTimer.setTextColor(-1);
        this.countdownTimer.setGravity(17);
        this.countdownTimer.setVisibility(4);
        return this.countdownTimer;
    }

    @SuppressLint({"NewApi"})
    public View createVideoView() {
        LayoutParams videoLayoutParams = new LayoutParams(-1, -1);
        videoLayoutParams.addRule(13);
        this.video = new VideoView(getContext());
        this.video.setLayoutParams(videoLayoutParams);
        this.video.setMediaController(null);
        configureVideo();
        return this.video;
    }

    public void configureVideo() {
        this.video.setVideoPath(this.data.getVideoFile().getAbsolutePath());
        this.video.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.changedVideoBackground = false;
        this.video.setVisibility(0);
        this.runnable = new Runnable() {
            public void run() {
                RevMobVideo.this.execTimer();
            }
        };
        final FullscreenClickListener videoClickListener = new FullscreenClickListener(this.fullScreenActivity);
        if (!this.rewarded.booleanValue()) {
            this.video.setOnTouchListener(new OnTouchListener() {
                public boolean requestSent = false;

                public boolean onTouch(View v, MotionEvent event) {
                    RevMobVideo.this.clicked = true;
                    if (!this.requestSent) {
                        if (!RevMobVideo.this.replayClicked) {
                            RevMobVideo.this.getEvent("clicks");
                        }
                        this.requestSent = true;
                        videoClickListener.onClick(String.format("%.2f", new Object[]{Double.valueOf(RevMobVideo.this.relativePosition)}));
                        RevMobVideo.this.removeHandlerCallbacks();
                    }
                    return this.requestSent;
                }
            });
        }
        this.video.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                RevMobVideo.this.videoMp = mp;
                mp.setVolume(RevMobVideo.this.videoVolume, RevMobVideo.this.videoVolume);
                if (!(RevMobVideo.this.data.getSoundOff() == null || RevMobVideo.this.data.getSoundOn() == null)) {
                    RevMobVideo.this.muteButton.setVisibility(0);
                }
                RevMobVideo.this.video.requestFocus();
                if (!RevMobVideo.this.started) {
                    if (!RevMobVideo.this.replayClicked) {
                        RevMobVideo.this.postEvent("start");
                    }
                    RevMobVideo.this.started = true;
                    RevMobVideo.this.video.start();
                }
                RevMobVideo.this.countdownTimer.setText("" + String.format("%02d", new Object[]{Integer.valueOf((RevMobVideo.this.configureVideoDurationMethods() - RevMobVideo.this.fullScreenActivity.getStopPosition()) / 1000)}));
                RevMobVideo.this.timerHandler.postDelayed(RevMobVideo.this.timerRunnable, 1000);
                RevMobVideo.this.runnable.run();
            }
        });
        this.video.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer arg0) {
                RevMobVideo.this.videoCompleted();
            }
        });
    }

    public void videoCompleted() {
        this.fullScreenActivity.abandonAudioFocus();
        this.handler.removeCallbacks(this.runnable);
        this.video.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.blackBackground.setVisibility(0);
        this.blackBackground.bringToFront();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                RevMobVideo.this.removeHandlerCallbacks();
                RevMobVideo.this.onVideoFinish();
            }
        }, 100);
    }

    public void removeHandlerCallbacks() {
        if (this.handler != null && this.runnable != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    public int configureVideoDurationMethods() {
        int finalDuration;
        final int finalDuration2;
        this.videoDuration = this.video.getDuration();
        if (this.fullScreenActivity.getStopPosition() == 0) {
            if (this.data.getVideoEnd() <= this.videoDuration) {
                finalDuration2 = this.data.getVideoEnd();
            } else {
                finalDuration2 = this.videoDuration;
            }
            this.firstQuartile = ((float) finalDuration2) / 4.0f;
            this.midPoint = ((float) finalDuration2) / 2.0f;
            this.thirdQuartile = (((float) finalDuration2) * 3.0f) / 4.0f;
        } else {
            if (this.data.getVideoEnd() <= this.videoDuration) {
                finalDuration = this.data.getVideoEnd() - this.fullScreenActivity.getStopPosition();
            } else {
                finalDuration = this.videoDuration - this.fullScreenActivity.getStopPosition();
            }
            this.video.seekTo(this.fullScreenActivity.getStopPosition());
            this.changedVideoBackground = true;
            this.fullScreenActivity.setStopPosition(0);
        }
        this.timerHandler = new Handler();
        this.timerRunnable = new Runnable() {
            int timeLeft = finalDuration2;

            public void run() {
                if (this.timeLeft - 1000 < 0) {
                    this.timeLeft = 0;
                    RevMobVideo.this.timerHandler.removeCallbacks(this);
                    return;
                }
                this.timeLeft -= 1000;
                RevMobVideo.this.countdownTimer.setText("" + String.format("%02d", new Object[]{Integer.valueOf(this.timeLeft / 1000)}));
                RevMobVideo.this.timerHandler.postDelayed(RevMobVideo.this.timerRunnable, 1000);
            }
        };
        return finalDuration2;
    }

    /* access modifiers changed from: private */
    public void execTimer() {
        if (this.video != null) {
            this.cPosition = this.video.getCurrentPosition();
            if (this.video.getCurrentPosition() > 100 && !this.changedVideoBackground) {
                this.changedVideoBackground = true;
                this.blackBackground.setVisibility(8);
                this.video.setBackgroundColor(0);
                if (this.rewarded.booleanValue()) {
                    RevMob.rewardedVideoIsLoaded = false;
                }
                this.fullScreenActivity.publisherListener.onRevMobRewardedVideoStarted();
                if (!this.replayClicked) {
                    RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.fullScreenActivity));
                    getEvent("impressions");
                    postEvent("creativeView");
                }
            }
            this.relativePosition = ((double) this.cPosition) / ((double) this.videoDuration);
            if ((this.relativePosition >= this.skipTime || this.replayClicked) && (!this.rewarded.booleanValue() || this.replayClicked)) {
                this.fullScreenActivity.shouldNotSkipVideo = false;
                this.videoCloseButton.setVisibility(0);
                this.videoCloseButton.requestFocus();
                this.fatherLayout.requestLayout();
            }
            if (this.cPosition >= this.data.getTimeLeftTime()) {
                this.countdownTimer.setVisibility(0);
                this.countdownTimer.requestFocus();
                this.fatherLayout.requestLayout();
            }
            if (((float) this.cPosition) >= this.firstQuartile && !this.passedFirstQuartile) {
                this.passedFirstQuartile = true;
                if (!this.replayClicked) {
                    postEvent("firstQuartile");
                }
            }
            if (((float) this.cPosition) >= this.midPoint && !this.passedMidPoint) {
                this.passedMidPoint = true;
                if (!this.replayClicked) {
                    postEvent("midpoint");
                }
            }
            if (((float) this.cPosition) >= this.thirdQuartile && !this.passedThirdQuartile) {
                this.passedThirdQuartile = true;
                if (!this.replayClicked) {
                    postEvent("thirdQuartile");
                }
            }
            this.handler.postDelayed(this.runnable, 0);
            if (this.data.getVideoEnd() <= this.videoDuration && !this.closed) {
                if (this.cPosition < this.data.getVideoEnd() && this.data.getVideoEnd() - this.cPosition <= 100 && this.data.getAdImage(2) != null && this.data.getAdImage(1) != null) {
                    this.blackBackground.setVisibility(0);
                    this.blackBackground.bringToFront();
                }
                if (this.cPosition >= this.data.getVideoEnd()) {
                    this.handler.removeCallbacks(this.runnable);
                    this.blackBackground.bringToFront();
                    this.video.stopPlayback();
                    removeHandlerCallbacks();
                    onVideoFinish();
                }
            } else if (this.cPosition >= this.videoDuration || this.clicked || this.closed) {
                removeHandlerCallbacks();
            }
        }
    }

    private View createImageAdView(final FullscreenClickListener clickListener) {
        this.fullscreenImageViewForVideo = new ImageView(getContext());
        this.fullscreenImageViewForVideo.setVisibility(4);
        update();
        this.fullscreenImageViewForVideo.setOnClickListener(new OnClickListener() {
            public boolean requestSent = false;

            public void onClick(View v) {
                if (!this.requestSent) {
                    this.requestSent = true;
                    if (!RevMobVideo.this.replayClicked) {
                        RevMobVideo.this.getEvent("clicks");
                    }
                    String relativeVideoPosition = String.format("%.2f", new Object[]{Double.valueOf(RevMobVideo.this.relativePosition)});
                    if (RevMobVideo.this.rewarded.booleanValue()) {
                        RevMobVideo.this.fullScreenActivity.publisherListener.onRevMobRewardedVideoCompleted();
                    }
                    clickListener.onClick(relativeVideoPosition);
                }
            }
        });
        return this.fullscreenImageViewForVideo;
    }

    private View createReplayButton() {
        this.replayButton = new ImageView(getContext());
        this.replayButton.setImageBitmap(this.data.getReplayButton());
        this.replayButton.setVisibility(4);
        this.replayButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RevMobVideo.this.blackBackground.bringToFront();
                RevMobVideo.this.replayVideo();
            }
        });
        return this.replayButton;
    }

    private View createMuteButton() {
        this.muteButton = new ImageView(getContext());
        LayoutParams muteButtonParams = new LayoutParams(buttonSize, buttonSize);
        muteButtonParams.addRule(10);
        muteButtonParams.topMargin = padding;
        muteButtonParams.addRule(9);
        muteButtonParams.leftMargin = padding;
        this.muteButton.setLayoutParams(muteButtonParams);
        if (this.videoVolume == 1.0f) {
            this.muteButton.setImageBitmap(this.data.getSoundOn());
        } else {
            this.muteButton.setImageBitmap(this.data.getSoundOff());
        }
        this.muteButton.setVisibility(8);
        this.muteButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (RevMobVideo.this.videoVolume == 1.0f) {
                    RevMobVideo.this.videoVolume = 0.0f;
                    RevMobVideo.this.muteButton.setImageBitmap(RevMobVideo.this.data.getSoundOff());
                    if (!RevMobVideo.this.replayClicked) {
                        RevMobVideo.this.postEvent("mute");
                    }
                } else {
                    RevMobVideo.this.videoVolume = 1.0f;
                    RevMobVideo.this.muteButton.setImageBitmap(RevMobVideo.this.data.getSoundOn());
                    if (!RevMobVideo.this.replayClicked) {
                        RevMobVideo.this.postEvent("unmute");
                    }
                }
                RevMobVideo.this.videoMp.setVolume(RevMobVideo.this.videoVolume, RevMobVideo.this.videoVolume);
                FullscreenActivity fullscreenActivity = RevMobVideo.this.fullScreenActivity;
                RevMobVideo.this.fullScreenActivity.getApplicationContext();
                Editor sharedEdit = fullscreenActivity.getPreferences(0).edit();
                sharedEdit.putFloat("RevMobVideoVolume", RevMobVideo.this.videoVolume);
                sharedEdit.commit();
            }
        });
        return this.muteButton;
    }

    public void cancelCountDownTimer() {
        if (this.timerHandler != null && this.timerRunnable != null) {
            this.timerHandler.removeCallbacks(this.timerRunnable);
            this.timerHandler = null;
            this.timerRunnable = null;
        }
    }

    private void initializeBooelans() {
        this.passedFirstQuartile = false;
        this.passedMidPoint = false;
        this.passedThirdQuartile = false;
        this.completedVideo = false;
        this.pausedVideo = false;
        this.resumedVideo = false;
    }

    public void update() {
        if (this.fullscreenImageViewForVideo != null) {
            int orientation = this.chosenVideoOrientation;
            this.fullscreenImageViewForVideo.setImageBitmap(this.data.getAdImage(orientation));
            if (orientation == 1) {
                postPortrait();
            } else {
                postLandscape();
            }
            if (this.data.isStaticMultiOrientationFullscreen()) {
                this.fullscreenImageViewForVideo.setScaleType(ScaleType.FIT_XY);
            } else {
                this.fullscreenImageViewForVideo.setScaleType(ScaleType.FIT_CENTER);
            }
        }
    }

    /* access modifiers changed from: private */
    public void onVideoFinish() {
        if (!this.replayClicked) {
            postEvent("complete");
        }
        if (this.fullScreenActivity.publisherListener != null) {
            if (!this.rewarded.booleanValue()) {
                this.fullScreenActivity.publisherListener.onRevMobVideoFinished();
            } else {
                this.fullScreenActivity.publisherListener.onRevMobRewardedVideoFinished();
            }
        }
        cancelCountDownTimer();
        this.fullscreenImageViewForVideo.setVisibility(0);
        this.videoCloseButton.setVisibility(0);
        this.video.stopPlayback();
        this.countdownTimer.setVisibility(4);
        this.muteButton.setVisibility(4);
        this.replayButton.setVisibility(0);
        this.revmobLogo.setVisibility(4);
        this.fatherLayout.bringToFront();
        if (this.fullscreenImageViewForVideo.getVisibility() != 0) {
            return;
        }
        if (this.fullScreenActivity.getRequestedOrientation() == 1) {
            postPortrait();
        } else {
            postLandscape();
        }
    }

    public void replayVideo() {
        RMLog.d("replayVideo");
        this.replayClicked = true;
        this.started = false;
        this.replayButton.setVisibility(4);
        this.fullscreenImageViewForVideo.setVisibility(4);
        this.revmobLogo.setVisibility(0);
        this.muteButton.setVisibility(0);
        this.countdownTimer.setVisibility(0);
        this.changedVideoBackground = false;
        configureVideo();
    }

    public void releaseAllResources() {
        cancelCountDownTimer();
        this.countdownTimer = null;
        this.videoCloseButton = null;
        this.muteButton = null;
        this.video = null;
        this.fullscreenImageViewForVideo = null;
        this.replayButton = null;
        this.revmobLogo = null;
        this.videoMp = null;
        this.blackBackground = null;
        this.fatherLayout = null;
    }

    private void postPortrait() {
        if (!this.postedFullScreenPortrait) {
            this.postedFullScreenPortrait = true;
            if (!this.replayClicked) {
                postEvent("fullscreenPortraitView");
            }
        }
    }

    private void postLandscape() {
        if (!this.postedFullScreenLandscape) {
            this.postedFullScreenLandscape = true;
            if (!this.replayClicked) {
                postEvent("fullscreenLandscapeView");
            }
        }
    }

    /* access modifiers changed from: private */
    public void postEvent(final String event) {
        new Thread() {
            public void run() {
                for (int i = 0; i < RevMobVideo.this.data.getTrackingEvents().size(); i++) {
                    ArrayList<String> eventUrls = (ArrayList) RevMobVideo.this.data.getTrackingEvents().get(i);
                    if (eventUrls.get(0) == event) {
                        HTTPHelper httphelper = new HTTPHelper();
                        for (int j = 1; j < eventUrls.size(); j++) {
                            String eventUrl = (String) eventUrls.get(j);
                            if (RevMobVideo.this.relativePosition > FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                                eventUrl = eventUrl.concat("&videoPosition=").concat(String.valueOf(String.format("%.2f", new Object[]{Double.valueOf(RevMobVideo.this.relativePosition)})));
                            }
                            httphelper.post(eventUrl, RevMobContext.toPayload(RevMobVideo.this.fullScreenActivity));
                        }
                    }
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void getEvent(final String event) {
        new Thread() {
            public void run() {
                int getSize = RevMobVideo.this.data.getGettingEvents().size();
                if (getSize > 0) {
                    for (int i = 0; i < getSize; i++) {
                        ArrayList<String> eventUrls = (ArrayList) RevMobVideo.this.data.getGettingEvents().get(i);
                        if (eventUrls.get(0) == event) {
                            HTTPHelper httphelper = new HTTPHelper();
                            if (eventUrls.size() > 0) {
                                for (int j = 1; j < eventUrls.size(); j++) {
                                    httphelper.get((String) eventUrls.get(j));
                                }
                            }
                        }
                    }
                }
            }
        }.start();
    }
}
