package com.revmob.ads.interstitial;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Intent;
import com.revmob.FullscreenActivity;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdState;
import com.revmob.ads.interstitial.client.FullscreenClientListener;
import com.revmob.ads.interstitial.client.FullscreenData;
import com.revmob.android.RevMobContext;
import com.revmob.client.AdData;
import com.revmob.client.RevMobClient;
import com.revmob.internal.RMLog;
import java.util.List;

public class RevMobFullscreen implements Ad {
    private Activity activity;
    public boolean autoshow = false;
    private FullscreenData data;
    private FullscreenClientListener fullscreenClientListener;
    private boolean isRewarded = false;
    private boolean isVideo = false;
    private RevMobAdsListener publisherListener;
    private AdState state;
    private boolean videoFollowAppOrientation = false;

    public RevMobFullscreen(Activity activity2, RevMobAdsListener publisherListener2) {
        this.activity = activity2;
        this.publisherListener = publisherListener2;
        this.state = AdState.CREATED;
        this.fullscreenClientListener = new FullscreenClientListener(this, publisherListener2, activity2);
    }

    public void loadFullscreen(int videoParam) {
        load((String) null, videoParam);
    }

    public void loadFullscreen(String placementId, int videoParam) {
        load(placementId, videoParam);
    }

    public void loadVideo(String placementId) {
        load(placementId, 1);
    }

    public void loadRewardedVideo(String placementId) {
        load(placementId, 3);
    }

    public void load(String placementId, int videoParam) {
        RevMobContext.getRunningApps = true;
        load(placementId, videoParam, RevMobContext.toPayload(this.activity), null);
    }

    public void load(String payload, String url) {
        load(null, 2, payload, url);
    }

    public void load(String placementId, int videoParam, String payload, String url) {
        FullscreenClientListener listener;
        String adUnit = "Fullscreen ";
        if (videoParam == 1) {
            RevMob.videoIsLoaded = false;
            this.isVideo = true;
            this.isRewarded = false;
            adUnit = "Video ";
        } else if (videoParam == 3) {
            RevMob.rewardedVideoIsLoaded = false;
            this.isVideo = true;
            this.isRewarded = true;
            adUnit = "Rewarded Video ";
        } else {
            RevMob.adIsLoaded = false;
            this.isVideo = false;
        }
        if (this.state == AdState.CREATED || this.state == AdState.CLOSED) {
            this.state = AdState.LOADING;
            RMLog.i(placementId != null ? "Loading " + adUnit + placementId : "Loading " + adUnit);
            if (!this.fullscreenClientListener.getShallUpdateData()) {
                this.fullscreenClientListener = new FullscreenClientListener(this, this.publisherListener, this.activity);
                listener = this.fullscreenClientListener;
            } else {
                listener = this.fullscreenClientListener;
            }
            RevMobContext.getRunningApps = true;
            if (videoParam == 0) {
                RevMobClient.getInstance().fetchFullscreen(placementId, RevMobContext.toPayload(this.activity), listener);
            } else if (videoParam == 1 || videoParam == 3) {
                RevMobClient.getInstance().fetchVideo(placementId, RevMobContext.toPayload(this.activity), listener, videoParam);
            } else {
                RevMobClient.getInstance().fetchVideoOrFullscreen(placementId, payload, listener);
            }
        }
    }

    public void updateWithData(AdData advertisement) {
        this.state = AdState.LOADED;
        this.data = (FullscreenData) advertisement;
        FullscreenData.addLoadedFullscreen(this.data);
        if (this.publisherListener != null) {
            if (this.data.getVideoUrl() == null || !this.isVideo) {
                RevMob.adIsLoaded = true;
                this.publisherListener.onRevMobAdReceived();
            } else if (this.isRewarded) {
                RevMob.rewardedVideoIsLoaded = true;
                this.publisherListener.onRevMobRewardedVideoLoaded();
            } else {
                RevMob.videoIsLoaded = true;
                this.publisherListener.onRevMobVideoLoaded();
            }
        }
        if (this.autoshow && !this.isVideo) {
            show();
        }
    }

    private boolean isLoaded() {
        return this.data != null;
    }

    public void show() {
        this.autoshow = true;
        boolean isInForeground = false;
        if (isLoaded() && this.state != AdState.DISPLAYED) {
            List<RunningAppProcessInfo> procInfos = ((ActivityManager) this.activity.getSystemService("activity")).getRunningAppProcesses();
            for (int i = 0; i < procInfos.size(); i++) {
                if (((RunningAppProcessInfo) procInfos.get(i)).processName.equals(this.activity.getPackageName()) && ((RunningAppProcessInfo) procInfos.get(i)).importance == 100) {
                    isInForeground = true;
                }
            }
            if (FullscreenActivity.isFullscreenActivityAvailable(this.activity).booleanValue()) {
                this.state = AdState.DISPLAYED;
                if (!this.isVideo) {
                    this.videoFollowAppOrientation = true;
                }
                Intent i2 = new Intent(this.activity, FullscreenActivity.class);
                i2.putExtra(FullscreenData.KEY, this.data.getClickUrl());
                i2.putExtra("followAppOrientation", this.videoFollowAppOrientation);
                i2.putExtra("isRewarded", this.isRewarded);
                if (isInForeground) {
                    this.activity.startActivityForResult(i2, 0);
                    if (this.publisherListener != null) {
                        if (!this.isVideo) {
                            RevMob.adIsLoaded = false;
                            this.publisherListener.onRevMobAdDisplayed();
                        } else if (!this.isRewarded) {
                            RevMob.videoIsLoaded = false;
                            this.publisherListener.onRevMobVideoStarted();
                        }
                    }
                    if (!this.isVideo) {
                        RevMobClient.getInstance().reportImpression(this.data.getImpressionUrl(), RevMobContext.toPayload(this.activity));
                        return;
                    }
                    return;
                }
                this.state = AdState.CLOSED;
                return;
            }
            this.state = AdState.CLOSED;
            RMLog.e("You must declare the RevMob FullscreenActivity in the AndroidManifest.xml file");
        } else if (this.state == AdState.LOADING) {
            this.state = AdState.CREATED;
            this.fullscreenClientListener.setShallUpdateData(false);
            load((String) null, 0);
        } else if (this.state != AdState.CREATED && this.state != AdState.CLOSED) {
            RMLog.i(Ad.SHOWING_TOO_SOON_MSG);
        }
    }

    public void showVideo() {
        if (this.isVideo && isLoaded()) {
            this.videoFollowAppOrientation = false;
            show();
        } else if (this.publisherListener == null) {
        } else {
            if (this.isRewarded) {
                this.publisherListener.onRevMobRewardedVideoNotCompletelyLoaded();
            } else {
                this.publisherListener.onRevMobVideoNotCompletelyLoaded();
            }
        }
    }

    public void showVideo(boolean followAppOrientation) {
        if (this.isVideo && isLoaded()) {
            this.videoFollowAppOrientation = followAppOrientation;
            show();
        } else if (this.publisherListener == null) {
        } else {
            if (this.isRewarded) {
                this.publisherListener.onRevMobRewardedVideoNotCompletelyLoaded();
            } else {
                this.publisherListener.onRevMobVideoNotCompletelyLoaded();
            }
        }
    }

    public void showRewardedVideo() {
        this.isRewarded = true;
        showVideo();
    }

    public void showRewardedVideo(boolean followAppOrientation) {
        this.isRewarded = true;
        showVideo(followAppOrientation);
    }

    public void setAutoShow(boolean autoShow) {
        this.autoshow = autoShow;
    }

    public void hide() {
        this.autoshow = false;
        this.activity.finishActivity(0);
    }
}
