package com.revmob.ads.internal;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class CloseAnimationConfiguration extends AnimationConfiguration {
    public Animation createFadeIn() {
        return new AlphaAnimation(1.0f, 2.0f);
    }

    public Animation createFadeOut() {
        return new AlphaAnimation(1.0f, 0.0f);
    }

    public Animation createSlideUp() {
        return new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
    }

    public Animation createSlideDown() {
        return new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 2.0f);
    }

    public Animation createSlideRight() {
        return new TranslateAnimation(1, 0.0f, 1, 2.0f, 1, 0.0f, 1, 0.0f);
    }

    public Animation createSlideLeft() {
        return new TranslateAnimation(1, 0.0f, 1, -2.0f, 1, 0.0f, 1, 0.0f);
    }

    public Animation createZoomIn() {
        return new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f, 1, 0.5f, 1, 0.5f);
    }

    public Animation createZoomOut() {
        return new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, 1, 0.5f, 1, 0.5f);
    }

    public Animation createClockwise() {
        return new RotateAnimation(0.0f, 90.0f);
    }

    public Animation createCounterClockwise() {
        return new RotateAnimation(0.0f, -90.0f);
    }
}
