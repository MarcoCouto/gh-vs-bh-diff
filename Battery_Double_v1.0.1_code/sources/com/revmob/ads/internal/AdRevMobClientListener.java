package com.revmob.ads.internal;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.revmob.RevMobAdsListener;
import com.revmob.client.RevMobClientListener;
import com.revmob.internal.HTTPHelper;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class AdRevMobClientListener implements RevMobClientListener {
    protected Ad ad;
    protected RevMobAdsListener publisherListener;

    public AdRevMobClientListener(Ad ad2, RevMobAdsListener publisherListener2) {
        this.ad = ad2;
        this.publisherListener = publisherListener2;
    }

    public void handleResponse(String response) throws JSONException {
    }

    public void handleError(String message) {
        if (this.publisherListener != null) {
            this.publisherListener.onRevMobAdNotReceived("Ad not received: " + message);
        }
    }

    public static String optionalGetString(JSONObject json, String key, String defaultValue) {
        try {
            return json.getString(key);
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public static String getLinkByRel(JSONArray links, String rel) throws JSONException {
        for (int i = 0; i < links.length(); i++) {
            if (links.getJSONObject(i).getString("rel").equals(rel)) {
                return links.getJSONObject(i).getString("href");
            }
        }
        return null;
    }

    public static String getStringKeyByRel(JSONArray links, String rel, String key) throws JSONException {
        for (int i = 0; i < links.length(); i++) {
            if (links.getJSONObject(i).getString("rel").equals(rel)) {
                return links.getJSONObject(i).getString(key);
            }
        }
        return null;
    }

    public static int getIntKeyByRel(JSONArray links, String rel, String key) throws JSONException {
        for (int i = 0; i < links.length(); i++) {
            if (links.getJSONObject(i).getString("rel").equals(rel)) {
                return links.getJSONObject(i).getInt(key);
            }
        }
        return 999;
    }

    public static double getDoubleKeyByRel(JSONArray links, String rel, String key) throws JSONException {
        for (int i = 0; i < links.length(); i++) {
            if (links.getJSONObject(i).getString("rel").equals(rel)) {
                return links.getJSONObject(i).getDouble(key);
            }
        }
        return 999.99d;
    }

    public static String getTextByRel(JSONArray links, String rel) throws JSONException {
        for (int i = 0; i < links.length(); i++) {
            if (links.getJSONObject(i).getString("rel").equals(rel)) {
                return links.getJSONObject(i).getString("text");
            }
        }
        return null;
    }

    public static void getAllLinksByRel(JSONArray links, String rel, ArrayList<String> arrayList) throws JSONException {
        for (int i = 0; i < links.length(); i++) {
            if (links.getJSONObject(i).getString("rel").equals(rel)) {
                arrayList.add(links.getJSONObject(i).getString("href"));
            }
        }
    }

    public static void getAllStringsInJSONArray(JSONArray array, ArrayList<String> arrayList) throws JSONException {
        for (int i = 0; i < array.length(); i++) {
            arrayList.add(array.getString(i));
        }
    }

    public static int getFullscreenType(JSONObject json, String elem) throws JSONException {
        if (json.has(elem)) {
            return json.getInt(elem);
        }
        return 0;
    }

    public static int getVideoInt(JSONObject json, String elem) throws JSONException {
        if (json.has(elem)) {
            return json.getInt(elem);
        }
        return 0;
    }

    public static double getVideoDouble(JSONObject json, String elem) throws JSONException {
        if (json.has(elem)) {
            return json.getDouble(elem);
        }
        return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    }

    public static String getVideoString(JSONObject json, String elem) throws JSONException {
        if (elem != null) {
            return json.getString(elem);
        }
        return null;
    }

    public static boolean getFollowRedirect(JSONObject json) throws JSONException {
        return Boolean.parseBoolean(optionalGetString(json, "follow_redirect", "true"));
    }

    public static String getClickUrl(JSONArray links) throws JSONException {
        return getLinkByRel(links, "clicks");
    }

    public static String getImpressionUrl(JSONArray links) throws JSONException {
        return getLinkByRel(links, "impressions");
    }

    public static String getAppOrSite(JSONObject json) throws JSONException {
        return optionalGetString(json, "app_or_site", "app");
    }

    public static boolean getOpenInside(JSONObject json) throws JSONException {
        return Boolean.parseBoolean(optionalGetString(json, "open_inside", "false"));
    }

    public static String getCustomUserAgent(JSONObject json) throws JSONException {
        return optionalGetString(json, "customUserAgent", HTTPHelper.getUserAgent());
    }

    public static String getIpAddress(JSONObject json) throws JSONException {
        return optionalGetString(json, "ip_address", HTTPHelper.getIpAddress());
    }

    public static boolean getOpenAdLink(JSONObject json) throws JSONException {
        return Boolean.parseBoolean(optionalGetString(json, "shouldOpenAdLink", "false"));
    }

    public static boolean getShouldExtractSocial(JSONObject json) {
        return Boolean.parseBoolean(optionalGetString(json, "shouldExtractSocial", "false"));
    }

    public static boolean getShouldExtractGeolocation(JSONObject json) {
        return Boolean.parseBoolean(optionalGetString(json, "shouldExtractGeolocation", "false"));
    }

    public static boolean getShouldExtractOtherAppsData(JSONObject json) {
        return Boolean.parseBoolean(optionalGetString(json, "shouldExtractOtherAppsData", "false"));
    }

    public static boolean getShouldContinueOnBackground(JSONObject json) {
        return Boolean.parseBoolean(optionalGetString(json, "shouldContinueOnBackground", "false"));
    }

    public static String getEulaUrl(JSONObject json) {
        return optionalGetString(json, "eula_url", HTTPHelper.getEulaUrl());
    }

    public static String getEulaVersion(JSONObject json) {
        return optionalGetString(json, "eula_version", HTTPHelper.getEulaVersion());
    }

    public static boolean getShouldShowEula(JSONObject json) {
        return Boolean.parseBoolean(optionalGetString(json, "should_show_eula", "false"));
    }
}
