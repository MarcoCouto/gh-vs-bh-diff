package com.revmob.ads.internal;

import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import java.util.ArrayList;
import java.util.List;

public abstract class AnimationConfiguration {
    private List<String> animations = new ArrayList();
    private long time = 500;

    public abstract Animation createClockwise();

    public abstract Animation createCounterClockwise();

    public abstract Animation createFadeIn();

    public abstract Animation createFadeOut();

    public abstract Animation createSlideDown();

    public abstract Animation createSlideLeft();

    public abstract Animation createSlideRight();

    public abstract Animation createSlideUp();

    public abstract Animation createZoomIn();

    public abstract Animation createZoomOut();

    public void setTime(long time2) {
        this.time = time2;
    }

    public void addAnimation(String type) {
        this.animations.add(type);
    }

    public Animation getAnimation() {
        AnimationSet animations2 = new AnimationSet(true);
        for (String animation : this.animations) {
            if (animation.equals("fade_in")) {
                animations2.addAnimation(createFadeIn());
            } else if (animation.equals("fade_out")) {
                animations2.addAnimation(createFadeOut());
            } else if (animation.equals("zoom_in")) {
                animations2.addAnimation(createZoomIn());
            } else if (animation.equals("zoom_out")) {
                animations2.addAnimation(createZoomOut());
            } else if (animation.equals("slide_up")) {
                animations2.addAnimation(createSlideUp());
            } else if (animation.equals("slide_down")) {
                animations2.addAnimation(createSlideDown());
            } else if (animation.equals("slide_right")) {
                animations2.addAnimation(createSlideRight());
            } else if (animation.equals("slide_left")) {
                animations2.addAnimation(createSlideLeft());
            } else if (animation.equals("rotate_clockwise")) {
                animations2.addAnimation(createClockwise());
            } else if (animation.equals("rotate_counterclockwise")) {
                animations2.addAnimation(createCounterClockwise());
            }
        }
        animations2.setDuration(this.time);
        return animations2;
    }
}
