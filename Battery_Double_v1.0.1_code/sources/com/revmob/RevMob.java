package com.revmob;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.revmob.ads.banner.RevMobBanner;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.revmob.ads.link.RevMobLink;
import com.revmob.ads.popup.RevMobPopup;
import com.revmob.android.RevMobContext;
import com.revmob.android.RevMobScreen;
import com.revmob.android.StoredData;
import com.revmob.android.UserData;
import com.revmob.client.RevMobClient;
import com.revmob.client.SessionClientListener;
import com.revmob.internal.AndroidHelper;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import com.revmob.internal.RevMobEula;
import java.util.Calendar;
import java.util.List;

public class RevMob {
    public static boolean adIsLoaded = false;
    /* access modifiers changed from: private */
    public static Activity bannerActivity;
    /* access modifiers changed from: private */
    public static RevMobBanner bannerAd;
    /* access modifiers changed from: private */
    public static RelativeLayout bannerLayout;
    /* access modifiers changed from: private */
    public static LayoutParams bannerParams;
    private static RevMobEula eula;
    private static SessionClientListener listener;
    private static RevMobAdsListener revmobListener;
    public static boolean rewardedVideoIsLoaded = false;
    protected static RevMob session;
    public static boolean videoIsLoaded = false;
    private boolean didSetBannerParams = false;

    private static void validateActivity(Activity activity) {
        if (activity == null) {
            throw new RuntimeException("RevMob: Activity must not be a null value.");
        }
    }

    private static void validatePermissions(Activity activity) {
        String str = "Permission %s is required. Add it to your AndroidManifest.xml file";
        if (!AndroidHelper.isPermissionEnabled(activity, "INTERNET")) {
            RMLog.e(String.format("Permission %s is required. Add it to your AndroidManifest.xml file", new Object[]{"INTERNET"}));
        }
    }

    private static void validateFullscreenActivity(Activity activity) {
        if (!FullscreenActivity.isFullscreenActivityAvailable(activity).booleanValue()) {
            RMLog.e("You must declare the RevMob FullscreenActivity in the AndroidManifest.xml file");
        }
    }

    @Deprecated
    public static RevMob start(Activity activity, String appId) {
        if (session == null) {
            validatePermissions(activity);
            validateActivity(activity);
            validateFullscreenActivity(activity);
            session = new RevMob(activity, appId);
        }
        return session;
    }

    public static RevMob start(Activity activity) {
        if (session == null) {
            try {
                return start(activity, activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.getString("com.revmob.app.id"));
            } catch (NameNotFoundException e) {
                throw new RuntimeException("You must put the revmob.app.id value in the AndroidManifest.xml file.");
            }
        } else {
            if (eula != null) {
                eula.loadAndShow();
            }
            return session;
        }
    }

    public static RevMob startWithListener(Activity activity, RevMobAdsListener listener2) {
        if (session == null) {
            try {
                String appId = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.getString("com.revmob.app.id");
                revmobListener = listener2;
                return start(activity, appId);
            } catch (NameNotFoundException e) {
                throw new RuntimeException("You must the revmob.app.id value in the AndroidManifest.xml file.");
            }
        } else {
            if (eula != null) {
                eula.loadAndShow();
            }
            return session;
        }
    }

    public static RevMob startWithListener(Activity activity, RevMobAdsListener listener2, String addr, int key) {
        if (addr != null) {
            RevMobClient.setProductionAdress(addr, key);
        }
        return startWithListener(activity, listener2);
    }

    public static RevMob startWithListenerForWrapper(Activity activity, String appId, RevMobAdsListener listener2) {
        if (session == null) {
            revmobListener = listener2;
            return start(activity, appId);
        }
        if (eula != null) {
            eula.loadAndShow();
        }
        return session;
    }

    public static RevMob session() {
        if (session == null) {
            RMLog.w(RevMobClient.SESSION_WARNING);
        }
        return session;
    }

    protected RevMob(Activity activity, String appId) {
        boolean needToRegisterInstall;
        validateActivity(activity);
        if (!new StoredData(activity).isAlreadyTracked()) {
            needToRegisterInstall = true;
        } else {
            needToRegisterInstall = false;
        }
        HTTPHelper.setUserAgent(null, activity);
        HTTPHelper.setEulaUrl(null, activity);
        HTTPHelper.setEulaVersion(null, activity);
        HTTPHelper.setShouldShowEula(false, activity);
        listener = new SessionClientListener(activity, needToRegisterInstall, revmobListener);
        RevMobContext.loadAdvertisingInfo(appId, listener, revmobListener, activity);
        RevMobScreen.load(activity);
    }

    public void acceptAndDismissEula() {
        eula = listener.getEulaPopup();
        eula.acceptAndDismiss();
    }

    public void rejectEula() {
        eula = listener.getEulaPopup();
        eula.reject();
    }

    public void showFullscreen(Activity activity) {
        returnFullScreen(activity, null, null, 0).setAutoShow(true);
    }

    public void showFullscreen(Activity activity, String placementId) {
        returnFullScreen(activity, placementId, null, 0).setAutoShow(true);
    }

    public void showFullscreen(Activity activity, RevMobAdsListener listener2) {
        returnFullScreen(activity, null, listener2, 0).setAutoShow(true);
    }

    public void showFullscreen(Activity activity, String placementId, RevMobAdsListener listener2) {
        returnFullScreen(activity, placementId, listener2, 0).setAutoShow(true);
    }

    public RevMobFullscreen createFullscreen(Activity activity, RevMobAdsListener listener2) {
        return returnFullScreen(activity, null, listener2, 2);
    }

    public boolean isAdLoaded() {
        return adIsLoaded;
    }

    public RevMobFullscreen createVideo(Activity activity, RevMobAdsListener listener2) {
        return returnVideo(activity, null, listener2);
    }

    public RevMobFullscreen createVideo(Activity activity, String placementId, RevMobAdsListener listener2) {
        return returnVideo(activity, placementId, listener2);
    }

    public boolean isVideoLoaded() {
        return videoIsLoaded;
    }

    public RevMobFullscreen createRewardedVideo(Activity activity, RevMobAdsListener listener2) {
        return returnRewardedVideo(activity, null, listener2);
    }

    public RevMobFullscreen createRewardedVideo(Activity activity, String placementId, RevMobAdsListener listener2) {
        return returnRewardedVideo(activity, placementId, listener2);
    }

    public boolean isRewardedVideoLoaded() {
        return rewardedVideoIsLoaded;
    }

    public RevMobFullscreen createFullscreen(Activity activity, String placementId, RevMobAdsListener listener2) {
        return returnFullScreen(activity, placementId, listener2, 2);
    }

    private RevMobFullscreen returnFullScreen(Activity activity, String placementId, RevMobAdsListener listener2, int videoParam) {
        validateActivity(activity);
        RevMobFullscreen ad = new RevMobFullscreen(activity, listener2);
        ad.loadFullscreen(placementId, videoParam);
        return ad;
    }

    private RevMobFullscreen returnVideo(Activity activity, String placementId, RevMobAdsListener listener2) {
        validateActivity(activity);
        RevMobFullscreen ad = new RevMobFullscreen(activity, listener2);
        ad.loadVideo(placementId);
        return ad;
    }

    private RevMobFullscreen returnRewardedVideo(Activity activity, String placementId, RevMobAdsListener listener2) {
        validateActivity(activity);
        RevMobFullscreen ad = new RevMobFullscreen(activity, listener2);
        ad.loadRewardedVideo(placementId);
        return ad;
    }

    public RevMobBanner createBanner(Activity activity) {
        return createBanner(activity, null, null);
    }

    public RevMobBanner createBanner(Activity activity, RevMobAdsListener listener2) {
        return createBanner(activity, null, listener2);
    }

    public RevMobBanner createBanner(Activity activity, String placementId) {
        return createBanner(activity, placementId, null);
    }

    public RevMobBanner createBanner(Activity activity, String placementId, RevMobAdsListener listener2) {
        validateActivity(activity);
        RevMobBanner ad = new RevMobBanner(activity, listener2);
        ad.setChangeBannerParams(false);
        ad.load(placementId);
        return ad;
    }

    public void releaseBanner(Activity activity) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    RevMob.bannerLayout.removeView(RevMob.bannerAd);
                    RevMob.bannerLayout.setVisibility(8);
                    RevMob.bannerLayout = null;
                    RevMob.bannerAd = null;
                    RevMob.bannerActivity = null;
                } catch (Exception e) {
                    RMLog.e(e.toString());
                }
            }
        });
    }

    public void hideBanner(Activity activity) {
        releaseBanner(activity);
    }

    public RelativeLayout showBanner(Activity activity) {
        return showBanner(activity, null, null);
    }

    public RelativeLayout showBanner(Activity activity, int gravity) {
        return showBanner(activity, gravity, null, null);
    }

    public RelativeLayout showBanner(Activity activity, String placementId, RevMobAdsListener listener2) {
        return showBanner(activity, 80, placementId, listener2);
    }

    public RelativeLayout showBanner(Activity activity, int gravity, String placementId, RevMobAdsListener listener2) {
        int idealHeight = AndroidHelper.dipToPixels(activity, 50);
        return showBanner(activity, gravity, 0, 0, AndroidHelper.dipToPixels(activity, RevMobBanner.DEFAULT_WIDTH_IN_DIP), idealHeight, placementId, listener2);
    }

    public RelativeLayout showBanner(Activity activity, int gravity, int left, int top, int width, int height) {
        return showBanner(activity, gravity, left, top, width, height, null, null);
    }

    public RelativeLayout showBanner(Activity activity, int gravity, int left, int top, int width, int height, String placementId, RevMobAdsListener listener2) {
        RelativeLayout banner = preloadBanner(activity, gravity, left, top, width, height, placementId, listener2);
        showLoadedBanner();
        return banner;
    }

    public RelativeLayout preloadBanner(Activity activity, int gravity, int left, int top, int width, int height, String placementId, RevMobAdsListener listener2) {
        final Activity activity2 = activity;
        final String str = placementId;
        final RevMobAdsListener revMobAdsListener = listener2;
        final int i = width;
        final int i2 = height;
        final int i3 = left;
        final int i4 = top;
        final int i5 = gravity;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (RevMob.bannerAd != null) {
                        RevMob.this.releaseBanner(activity2);
                    }
                    RevMob.bannerAd = RevMob.this.createBanner(activity2, str, revMobAdsListener);
                    RevMob.bannerLayout = new RelativeLayout(activity2.getApplicationContext());
                    RevMob.bannerParams = new LayoutParams(i, i2);
                    if (i3 == 0 && i4 == 0 && i == AndroidHelper.dipToPixels(activity2, RevMobBanner.DEFAULT_WIDTH_IN_DIP) && i2 == AndroidHelper.dipToPixels(activity2, 50)) {
                        RevMob.bannerParams.addRule(14);
                    } else {
                        RevMob.bannerParams.leftMargin = i3;
                        RevMob.bannerParams.topMargin = i4;
                        RevMob.bannerAd.setChangeBannerParams(true);
                        RevMob.bannerAd.setCustomSize(i, i2);
                    }
                    if (i5 == 48) {
                        RevMob.bannerLayout.setGravity(48);
                    } else {
                        RevMob.bannerLayout.setGravity(80);
                    }
                    activity2.addContentView(RevMob.bannerLayout, new ViewGroup.LayoutParams(-1, -1));
                    RevMob.bannerActivity = activity2;
                } catch (Exception e) {
                    RMLog.e(e.toString());
                }
            }
        });
        return bannerLayout;
    }

    public boolean showLoadedBanner() {
        if (bannerActivity == null) {
            return false;
        }
        hideLoadedBanner();
        if (bannerLayout == null || bannerAd == null || bannerParams == null) {
            return false;
        }
        bannerActivity.runOnUiThread(new Runnable() {
            public void run() {
                RevMob.bannerLayout.addView(RevMob.bannerAd, RevMob.bannerParams);
            }
        });
        return true;
    }

    public boolean hideLoadedBanner() {
        if (bannerActivity == null) {
            return false;
        }
        bannerActivity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    RevMob.bannerAd.hide();
                } catch (Exception e) {
                }
                RevMob.bannerLayout.removeView(RevMob.bannerAd);
            }
        });
        return true;
    }

    public void releaseLoadedBanner() {
        if (bannerActivity != null) {
            hideLoadedBanner();
            releaseBanner(bannerActivity);
        }
    }

    public void openLink(Activity activity, RevMobAdsListener listener2) {
        createLink(activity, null, listener2).open();
    }

    public void openLink(Activity activity, String placementId, RevMobAdsListener listener2) {
        createLink(activity, placementId, listener2).open();
    }

    public RevMobLink createLink(Activity activity, RevMobAdsListener listener2) {
        return createLink(activity, null, listener2);
    }

    public RevMobLink createLink(Activity activity, String placementId, RevMobAdsListener listener2) {
        validateActivity(activity);
        RevMobLink ad = new RevMobLink(activity, listener2);
        ad.load(placementId);
        return ad;
    }

    public void showPopup(Activity activity) {
        createPopup(activity, null, null).show();
    }

    public void showPopup(Activity activity, String placementId) {
        createPopup(activity, placementId, null).show();
    }

    public void showPopup(Activity activity, String placementId, RevMobAdsListener listener2) {
        createPopup(activity, null, listener2).show();
    }

    public RevMobPopup createPopup(Activity activity, RevMobAdsListener listener2) {
        return createPopup(activity, null, listener2);
    }

    public RevMobPopup createPopup(Activity activity, String placementId, RevMobAdsListener listener2) {
        validateActivity(activity);
        RevMobPopup ad = new RevMobPopup(activity, listener2);
        ad.load(placementId);
        return ad;
    }

    public void printEnvironmentInformation(Activity activity) {
        validateActivity(activity);
        RevMobContext.printEnvironmentInformation(RevMobClient.getInstance().getAppId(), activity);
    }

    public RevMobTestingMode getTestingMode() {
        return RevMobClient.getInstance().getTestingMode();
    }

    public void setTestingMode(RevMobTestingMode testingMode) {
        RevMobClient.getInstance().setTestingMode(testingMode);
    }

    public RevMobParallaxMode getParallaxMode() {
        return RevMobClient.getInstance().getParallaxMode();
    }

    public void setParallaxMode(RevMobParallaxMode parallaxMode) {
        RevMobClient.getInstance().setParallaxMode(parallaxMode);
    }

    public void setTimeoutInSeconds(int timeoutInSeconds) {
        RevMobClient.getInstance().setTimeoutInSeconds(timeoutInSeconds);
    }

    public void setUserEmail(String email) {
        UserData.setEmail(email);
    }

    public RevMobUserGender getUserGender() {
        return UserData.getUserGender();
    }

    public void setUserGender(RevMobUserGender userGender) {
        UserData.setUserGender(userGender);
    }

    public int getUserAgeRangeMin() {
        return UserData.getUserAgeRangeMin();
    }

    public void setUserAgeRangeMin(int userAgeRangeMin) {
        UserData.setUserAgeRangeMin(userAgeRangeMin);
    }

    public int getUserAgeRangeMax() {
        return UserData.getUserAgeRangeMax();
    }

    public void setUserAgeRangeMax(int userAgeRangeMax) {
        UserData.setUserAgeRangeMax(userAgeRangeMax);
    }

    public Calendar getUserBirthday() {
        return UserData.getUserBirthday();
    }

    public void setUserBirthday(Calendar userBirthday) {
        UserData.setUserBirthday(userBirthday);
    }

    public String getUserPage() {
        return UserData.getUserPage();
    }

    public void setUserPage(String userPage) {
        UserData.setUserPage(userPage);
    }

    public List<String> getUserInterests() {
        return UserData.getUserInterests();
    }

    public void setUserInterests(List<String> userInterests) {
        UserData.setUserInterests(userInterests);
    }
}
