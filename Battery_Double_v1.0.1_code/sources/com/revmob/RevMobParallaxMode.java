package com.revmob;

public enum RevMobParallaxMode {
    DEFAULT("enabled"),
    DISABLED("disabled");
    
    private String value;

    private RevMobParallaxMode(String value2) {
        this.value = value2;
    }

    public String getValue() {
        return this.value;
    }
}
