package com.revmob.android;

import com.revmob.RevMobUserGender;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserData {
    private static String addr = null;
    private static final int ageLimit = 13;
    private static Calendar birth = null;
    private static int maxAge = -1;
    private static int minAge = -1;
    private static List<String> preferenc = null;
    private static RevMobUserGender sex = RevMobUserGender.UNDEFINED;
    private static String url = null;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0063, code lost:
        if (r4.compareTo(r0) != -1) goto L_0x0065;
     */
    public static JSONObject addUserInfo(JSONObject device) throws JSONException {
        if (minAge != -1) {
            device.put("age_range_min", minAge);
        }
        if (maxAge != -1) {
            device.put("age_range_max", maxAge);
        }
        if (minAge <= 0 || maxAge >= 13) {
            if (birth != null) {
                device.put("birthday", new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(birth.getTime()));
                Calendar today = Calendar.getInstance(Locale.US);
                today.setTime(new Date());
                Calendar birthDatePlusMinimumAge = Calendar.getInstance(Locale.US);
                birthDatePlusMinimumAge.setTime(birth.getTime());
                birthDatePlusMinimumAge.add(1, 13);
            }
            if (addr != null) {
                device.put("email", addr);
            }
            if (sex != null) {
                device.put("gender", sex.getValue());
            }
            if (url != null) {
                device.put("user_page", url);
            }
            if (preferenc != null && preferenc.size() > 0) {
                JSONArray list = new JSONArray();
                for (int i = 0; i < preferenc.size(); i++) {
                    list.put(preferenc.get(i));
                }
                device.put("interests", list);
            }
        }
        return device;
    }

    public static RevMobUserGender getUserGender() {
        return sex;
    }

    public static void setUserGender(RevMobUserGender anUserGender) {
        sex = anUserGender;
    }

    public static int getUserAgeRangeMin() {
        return minAge;
    }

    public static void setUserAgeRangeMin(int anUserAgeRangeMin) {
        minAge = anUserAgeRangeMin;
    }

    public static int getUserAgeRangeMax() {
        return maxAge;
    }

    public static void setUserAgeRangeMax(int anUserAgeRangeMax) {
        maxAge = anUserAgeRangeMax;
    }

    public static Calendar getUserBirthday() {
        return birth;
    }

    public static void setUserBirthday(Calendar anUserBirthday) {
        birth = anUserBirthday;
    }

    public static String getUserPage() {
        return url;
    }

    public static void setUserPage(String anUserPage) {
        url = anUserPage;
    }

    public static List<String> getUserInterests() {
        return preferenc;
    }

    public static void setUserInterests(List<String> anUserInterests) {
        preferenc = anUserInterests;
    }

    public static String getEmail() {
        return addr;
    }

    public static void setEmail(String addr2) {
        addr = addr2;
    }
}
