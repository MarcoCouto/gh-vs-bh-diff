package com.revmob.android;

import android.content.Context;
import android.content.SharedPreferences.Editor;

public class StoredData {
    private static final String BASE_KEY = "RevMob";
    private Context context;

    public StoredData(Context context2) {
        this.context = context2;
    }

    public void markAsTracked() {
        Editor editor = this.context.getSharedPreferences(BASE_KEY, 0).edit();
        editor.putBoolean("Registered", true);
        editor.commit();
    }

    public boolean isAlreadyTracked() {
        return this.context.getSharedPreferences(BASE_KEY, 0).getBoolean("Registered", false);
    }
}
