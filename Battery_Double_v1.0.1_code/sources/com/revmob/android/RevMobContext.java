package com.revmob.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.revmob.RevMobAdsListener;
import com.revmob.RevMobTestingMode;
import com.revmob.ads.banner.RevMobBanner;
import com.revmob.android.AdvertisingIdClient.AdInfo;
import com.revmob.client.RevMobClient;
import com.revmob.client.RevMobClientListener;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RevMobContext {
    public static Context activity;
    public static Thread adThread;
    /* access modifiers changed from: private */
    public static String adTrackingEnabled;
    /* access modifiers changed from: private */
    public static String advertisingId;
    private static JSONObject age = new JSONObject();
    /* access modifiers changed from: private */
    public static String appId;
    private static String birthday;
    private static boolean facebook = false;
    private static boolean facebookSent = false;
    public static boolean getInstalledApps = false;
    public static boolean getRunningApps = false;
    private static boolean hasFacebookSDK = false;
    private static String lastName;
    /* access modifiers changed from: private */
    public static RevMobClientListener listener;
    private static DisplayMetrics metrics = new DisplayMetrics();
    private static String name;
    private static String[] permissions;
    public static RevMobAdsListener publisherListener;
    public static RUNNING_APPS_STATUS runningAppsStatus = RUNNING_APPS_STATUS.PAUSED;
    private static String token;
    private static JSONObject userInformation = new JSONObject();
    private static String user_ID;
    private final int MAX_INSTALLED_APPS = 40;

    public enum RUNNING_APPS_STATUS {
        PAUSED,
        ONLY_NOTIFY,
        NOTIFY_AND_FETCH
    }

    public static String toPayload(Activity act) {
        activity = act;
        ((Activity) activity).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return revmobJSONString();
    }

    public static String revmobJSONString() {
        return revmobJSON().toString();
    }

    public static JSONArray getAppPermissions() {
        try {
            permissions = activity.getPackageManager().getPackageInfo(activity.getApplicationContext().getPackageName(), 4096).requestedPermissions;
            return new JSONArray(Arrays.asList(permissions));
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    private static String getFacebookToken() {
        String facebookToken = null;
        try {
            Object socialInfo = Class.forName("com.revmob.internal.RevMobSocialInfo").getConstructor(new Class[0]).newInstance(new Object[0]);
            return (String) Class.forName("com.revmob.internal.RevMobSocialInfo").getMethod("getFacebookToken", new Class[]{Context.class}).invoke(socialInfo, new Object[]{activity.getApplicationContext()});
        } catch (Exception e) {
            e.printStackTrace();
            return facebookToken;
        }
    }

    public static JSONObject revmobJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("device", getDeviceJSON());
            json.put("sdk", getSDKJSON());
            json.put("app", getAppJSON());
            json.put("social", getSocialJSON());
            if (getInstalledApps) {
                getInstalledApps = false;
                json.put("installedApps", getInstalledApps());
            }
            if (runningAppsStatus == RUNNING_APPS_STATUS.NOTIFY_AND_FETCH && getRunningApps) {
                getRunningApps = false;
                json.put("runningApps", getRunningApps());
            }
            if (!(RevMobClient.t0 == 0 || RevMobClient.t1 == 0 || RevMobClient.t2 == 0 || RevMobClient.t3 == 0)) {
                json.put("time", getFetchTimeInfo());
            }
            if (RevMobBanner.isBannerImpression) {
                json.put("bannerImpressions", getBannerJSON());
                RevMobBanner.setBannerImpression(false);
            }
            if (RevMobClient.getInstance().getTestingMode() == RevMobTestingMode.DISABLED) {
                return json;
            }
            JSONObject testing = new JSONObject();
            testing.put("response", RevMobClient.getInstance().getTestingMode().getValue());
            json.put("testing", testing);
            return json;
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject deviceIdentifierJSON() {
        try {
            JSONObject json = new JSONObject();
            JSONObject identifierJSON = new JSONObject();
            putIfNotEmpty(json, "android_id", getAndroidID());
            putIfNotEmpty(json, "serial", getSerial());
            putIfNotEmpty(json, "identifier_for_advertising", getAdvertisingId());
            identifierJSON.put("identifiers", json);
            return identifierJSON;
        } catch (JSONException e) {
            return null;
        }
    }

    private static JSONObject getSocialJSON() throws JSONException {
        JSONObject json = new JSONObject();
        UserData.addUserInfo(json);
        try {
            Class.forName("com.facebook.Session");
            RMLog.i("Has Facebook SDK!");
            json.put("facebook_token", getFacebookToken());
        } catch (ClassNotFoundException e) {
            RMLog.i("Facebook SDK not found.");
        }
        return json;
    }

    public static JSONObject getSDKJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("name", RevMobClient.SDK_NAME);
        json.put("version", RevMobClient.SDK_VERSION);
        json.put("testing_mode", RevMobClient.getInstance().getTestingMode().getValue());
        return json;
    }

    public static JSONObject getDeviceJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("screen", getDeviceScreenJSON());
        putIfNotEmpty(json, "model", getModel());
        putIfNotEmpty(json, "api", "" + getApiVersion());
        putIfNotEmpty(json, "manufacturer", getManufacturer());
        putIfNotEmpty(json, "os_version", getOsVersion());
        putIfNotEmpty(json, "orientation", getOrientation());
        putIfNotEmpty(json, "locale", getLocale());
        putIfNotEmpty(json, "ua", HTTPHelper.getUserAgent());
        if (HTTPHelper.getShouldExtractGeolocation()) {
            json.put(Param.LOCATION, getUserLocation());
        }
        putIfNotEmpty(json, "android_id", getAndroidID());
        putIfNotEmpty(json, "serial", getSerial());
        putIfNotEmpty(json, "identifier_for_advertising", getAdvertisingId());
        putIfNotEmpty(json, "limit_ad_tracking", getAdTrackingEnabled());
        return json;
    }

    @TargetApi(8)
    private static String getOrientation() {
        Display display = ((WindowManager) activity.getSystemService("window")).getDefaultDisplay();
        int rotation = api8OrNewer() ? display.getRotation() : display.getOrientation();
        if (rotation == 0) {
            return "0";
        }
        if (rotation == 1) {
            return "90";
        }
        if (rotation == 2) {
            return "180";
        }
        return rotation == 3 ? "270" : "-1";
    }

    private static boolean api8OrNewer() {
        if (VERSION.RELEASE.startsWith("1.") || VERSION.RELEASE.startsWith("2.0") || VERSION.RELEASE.startsWith("2.1")) {
            return false;
        }
        return true;
    }

    private static JSONObject getIdentitiesJSON() throws JSONException {
        JSONObject json = new JSONObject();
        putIfNotEmpty(json, "android_id", getAndroidID());
        putIfNotEmpty(json, "serial", getSerial());
        putIfNotEmpty(json, "identifier_for_advertising", getAdvertisingId());
        putIfNotEmpty(json, "limit_ad_tracking", getAdTrackingEnabled());
        return json;
    }

    public static JSONObject getDeviceScreenJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("width", metrics.widthPixels);
        json.put("height", metrics.heightPixels);
        json.put("scale", (double) metrics.density);
        json.put("density_dpi", metrics.densityDpi);
        return json;
    }

    public static JSONObject getUserLocation() throws JSONException {
        JSONObject userLocation = new JSONObject();
        try {
            LocationManager locationManager = (LocationManager) activity.getSystemService(Param.LOCATION);
            if (locationManager != null && (activity.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || activity.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                Location gpsLocation = locationManager.getLastKnownLocation("gps");
                Location netLocation = locationManager.getLastKnownLocation("network");
                if (gpsLocation == null || netLocation == null) {
                    if (gpsLocation != null) {
                        userLocation.put("latitude", gpsLocation.getLatitude());
                        userLocation.put("longitude", gpsLocation.getLongitude());
                        userLocation.put("accuracy", (double) gpsLocation.getAccuracy());
                    } else if (netLocation != null) {
                        userLocation.put("latitude", netLocation.getLatitude());
                        userLocation.put("longitude", netLocation.getLongitude());
                        userLocation.put("accuracy", (double) netLocation.getAccuracy());
                    }
                } else if (gpsLocation.getTime() > netLocation.getTime()) {
                    userLocation.put("latitude", gpsLocation.getLatitude());
                    userLocation.put("longitude", gpsLocation.getLongitude());
                    userLocation.put("accuracy", (double) gpsLocation.getAccuracy());
                } else {
                    userLocation.put("latitude", netLocation.getLatitude());
                    userLocation.put("longitude", netLocation.getLongitude());
                    userLocation.put("accuracy", (double) netLocation.getAccuracy());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userLocation;
    }

    private static JSONObject getFetchTimeInfo() throws JSONException {
        JSONObject json = new JSONObject();
        double sdkTime = (double) (RevMobClient.t2 - RevMobClient.t1);
        double creativeTime = (double) (RevMobClient.t3 - RevMobClient.t2);
        json.put("fetchTime", ((double) (RevMobClient.t1 - RevMobClient.t0)) / 1000.0d);
        json.put("sdkTime", sdkTime / 1000.0d);
        json.put("creativeTime", creativeTime / 1000.0d);
        return json;
    }

    private static JSONObject getBannerJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("bannerCount", RevMobBanner.bannerCount);
        JSONObject campaignArray = new JSONObject();
        for (int i = 0; i < RevMobBanner.usedCampaigns.size(); i++) {
            campaignArray.put(String.valueOf(i + 1), RevMobBanner.usedCampaigns.get(i));
        }
        json.put("campaigns", campaignArray);
        return json;
    }

    private static JSONObject getAppJSON() throws JSONException {
        boolean needToRegisterInstall = false;
        JSONObject json = new JSONObject();
        putIfNotEmpty(json, "bundle_identifier", activity.getPackageName());
        try {
            Resources resources = activity.getResources();
            putIfNotEmpty(json, "app_name", resources.getText(resources.getIdentifier("app_name", "string", activity.getPackageName())).toString());
        } catch (Exception e) {
        }
        try {
            PackageInfo pkgInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            putIfNotEmpty(json, "app_version", "" + pkgInfo.versionCode);
            if (getAppPermissions() != null) {
                putArrayIfNotEmpty(json, "permissions", getAppPermissions());
            }
            putIfNotEmpty(json, "app_version_name", pkgInfo.versionName);
        } catch (Exception e2) {
        }
        if (!new StoredData(activity).isAlreadyTracked()) {
            needToRegisterInstall = true;
        }
        if (needToRegisterInstall) {
            putIfNotEmpty(json, "install_not_registered", "true");
        }
        return json;
    }

    public static void printEnvironmentInformation(String appId2, Activity act) {
        activity = act;
        if (RevMobClient.SDK_SOURCE_NAME != null) {
            RMLog.i("RevMob SDK Version: " + RevMobClient.SDK_VERSION + " (" + RevMobClient.SDK_SOURCE_NAME + "-" + RevMobClient.SDK_SOURCE_VERSION + ")");
        } else {
            RMLog.i("RevMob SDK Version: " + RevMobClient.SDK_VERSION);
        }
        RMLog.i("App ID: " + appId2);
        RMLog.i("IP Address: " + HTTPHelper.getIpAddress());
        RMLog.i("Simulator: " + isSimulator());
        RMLog.i("OS Version: " + getOsVersion());
        RMLog.i("Android API: " + getApiVersion());
        RMLog.i("Manufacturer: " + getManufacturer());
        RMLog.i("Model: " + getModel());
        RMLog.i("Android ID: " + getAndroidID());
        RMLog.i("Serial number: " + getSerial());
        RMLog.i("ID for Advertising: " + getAdvertisingId());
        RMLog.i("Limit Ad Tracking: " + getAdTrackingEnabled());
        RMLog.i("Language: " + getLanguage());
        RMLog.i("Locale: " + getLocale());
        RMLog.i("User Agent: " + HTTPHelper.getUserAgent());
        RMLog.i("Screen size: " + metrics.widthPixels + "," + metrics.heightPixels);
        RMLog.i("Density scale: " + metrics.density);
        RMLog.i("Density dpi: " + metrics.densityDpi);
        try {
            RMLog.i("Installed Apps: " + getInstalledApps());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            RMLog.i("User Location: " + getUserLocation());
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    private static String getSerial() {
        return "";
    }

    public static String getAndroidID() {
        return "";
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getOsVersion() {
        return VERSION.RELEASE;
    }

    public static int getApiVersion() {
        return VERSION.SDK_INT;
    }

    public static boolean isSimulator() {
        return getModel().contains("sdk") || getModel().contains("Emulator");
    }

    public static String getLocale() {
        return Locale.getDefault().toString().replace('_', '-');
    }

    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    private static void putIfNotEmpty(JSONObject target, String name2, String value) throws JSONException {
        if (value != null && !value.equals("")) {
            target.put(name2, value);
        }
    }

    private static void putArrayIfNotEmpty(JSONObject target, String name2, JSONArray value) throws JSONException {
        if (value != null && !value.equals("")) {
            target.put(name2, value);
        }
    }

    private static JSONArray getInstalledApps() throws JSONException {
        JSONArray list = new JSONArray();
        HTTPHelper.setShouldExtractOtherAppsData(true);
        if (!HTTPHelper.getShouldExtractOtherAppsData()) {
            return null;
        }
        PackageManager packageManager = activity.getPackageManager();
        for (ApplicationInfo appInfo : packageManager.getInstalledApplications(128)) {
            if ((appInfo.flags & 1) != 1) {
                JSONObject object = new JSONObject();
                object.put("packageName", appInfo.packageName);
                object.put("name", appInfo.loadLabel(packageManager));
                list.put(object);
            }
        }
        return list;
    }

    private static JSONObject getRunningApps() throws JSONException {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        new JSONArray();
        JSONArray savedRunningApps = new JSONArray(prefs.getString("runningApps", new JSONArray().toString()));
        return (JSONObject) savedRunningApps.get(savedRunningApps.length() - 1);
    }

    private static String getAdTrackingEnabled() {
        return adTrackingEnabled;
    }

    private static String getAdvertisingId() {
        return advertisingId;
    }

    public static void loadAdvertisingInfo(String mediaId, RevMobClientListener listenerx, RevMobAdsListener publisherListenerx, Activity act) {
        try {
            appId = mediaId;
            listener = listenerx;
            publisherListener = publisherListenerx;
            activity = act;
            if (adThread == null) {
                adThread = new Thread(new Runnable() {
                    public void run() {
                        try {
                            AdInfo adInfo = AdvertisingIdClient.getAdvertisingIdInfo(RevMobContext.activity);
                            RevMobContext.advertisingId = adInfo.getId();
                            RevMobContext.adTrackingEnabled = adInfo.isLimitAdTrackingEnabled() ? "true" : "false";
                            RevMobClient.getInstance().startSession(RevMobContext.appId, RevMobContext.toPayload((Activity) RevMobContext.activity), RevMobContext.listener, RevMobContext.publisherListener);
                        } catch (Exception e) {
                            RMLog.e("Error with Google Play Services: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
                adThread.start();
            }
        } catch (Exception e) {
            RMLog.e("Error loading advertising info: " + e.getMessage());
        }
    }

    public static boolean isApplicationStarted() {
        return activity != null;
    }

    public static Context getApplicationContext() {
        return activity;
    }
}
