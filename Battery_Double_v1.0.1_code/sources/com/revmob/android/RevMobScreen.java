package com.revmob.android;

import android.app.Activity;
import android.util.DisplayMetrics;

public class RevMobScreen {
    static int densityDpi;
    static float scale;
    static int screenHeight;
    static int screenWidth;

    public static void load(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        scale = metrics.density;
        densityDpi = metrics.densityDpi;
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static float getScale() {
        return scale;
    }

    public static int getDensityDpi() {
        return densityDpi;
    }
}
