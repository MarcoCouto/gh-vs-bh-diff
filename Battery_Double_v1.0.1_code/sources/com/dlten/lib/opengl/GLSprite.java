package com.dlten.lib.opengl;

import android.graphics.Bitmap;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11Ext;

public class GLSprite extends Renderable {
    private Bitmap mBmp = null;
    private Grid mGrid;
    private int mResourceId;
    private int mTextureName;

    public GLSprite(int resourceId) {
        this.mResourceId = resourceId;
    }

    public void setTextureName(int name) {
        this.mTextureName = name;
    }

    public int getTextureName() {
        return this.mTextureName;
    }

    public void setResourceId(int id) {
        this.mResourceId = id;
    }

    public int getResourceId() {
        return this.mResourceId;
    }

    public void setGrid(Grid grid) {
        this.mGrid = grid;
    }

    public Grid getGrid() {
        return this.mGrid;
    }

    public void draw(GL10 gl) {
        gl.glBindTexture(3553, this.mTextureName);
        if (this.mGrid == null) {
            ((GL11Ext) gl).glDrawTexfOES(this.x, this.y, this.z, this.width, this.height);
            return;
        }
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glTranslatef(this.x, this.y, this.z);
        this.mGrid.draw(gl, true, false);
        gl.glPopMatrix();
    }

    public Bitmap getBmp() {
        return this.mBmp;
    }

    public void setBmp(Bitmap bmp) {
        this.mBmp = bmp;
    }

    public void resetBmp() {
        if (this.mBmp != null) {
            this.mBmp.recycle();
            this.mBmp = null;
        }
    }
}
