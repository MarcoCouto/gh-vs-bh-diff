package com.dlten.lib.opengl;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

class Grid {
    static final /* synthetic */ boolean $assertionsDisabled = (!Grid.class.desiredAssertionStatus());
    private Buffer mColorBuffer;
    private int mColorBufferIndex;
    private int mCoordinateSize;
    private int mCoordinateType;
    private IntBuffer mFixedColorBuffer;
    private IntBuffer mFixedTexCoordBuffer;
    private IntBuffer mFixedVertexBuffer;
    private FloatBuffer mFloatColorBuffer;
    private FloatBuffer mFloatTexCoordBuffer;
    private FloatBuffer mFloatVertexBuffer;
    private int mH;
    private CharBuffer mIndexBuffer;
    private int mIndexBufferIndex;
    private int mIndexCount;
    private Buffer mTexCoordBuffer;
    private int mTextureCoordBufferIndex;
    private boolean mUseHardwareBuffers;
    private int mVertBufferIndex;
    private Buffer mVertexBuffer;
    private int mW;

    public Grid(int vertsAcross, int vertsDown, boolean useFixedPoint) {
        if (vertsAcross < 0 || vertsAcross >= 65536) {
            throw new IllegalArgumentException("vertsAcross");
        } else if (vertsDown < 0 || vertsDown >= 65536) {
            throw new IllegalArgumentException("vertsDown");
        } else if (vertsAcross * vertsDown >= 65536) {
            throw new IllegalArgumentException("vertsAcross * vertsDown >= 65536");
        } else {
            this.mUseHardwareBuffers = false;
            this.mW = vertsAcross;
            this.mH = vertsDown;
            int size = vertsAcross * vertsDown;
            if (useFixedPoint) {
                this.mFixedVertexBuffer = ByteBuffer.allocateDirect(size * 4 * 3).order(ByteOrder.nativeOrder()).asIntBuffer();
                this.mFixedTexCoordBuffer = ByteBuffer.allocateDirect(size * 4 * 2).order(ByteOrder.nativeOrder()).asIntBuffer();
                this.mFixedColorBuffer = ByteBuffer.allocateDirect(size * 4 * 4).order(ByteOrder.nativeOrder()).asIntBuffer();
                this.mVertexBuffer = this.mFixedVertexBuffer;
                this.mTexCoordBuffer = this.mFixedTexCoordBuffer;
                this.mColorBuffer = this.mFixedColorBuffer;
                this.mCoordinateSize = 4;
                this.mCoordinateType = 5132;
            } else {
                this.mFloatVertexBuffer = ByteBuffer.allocateDirect(size * 4 * 3).order(ByteOrder.nativeOrder()).asFloatBuffer();
                this.mFloatTexCoordBuffer = ByteBuffer.allocateDirect(size * 4 * 2).order(ByteOrder.nativeOrder()).asFloatBuffer();
                this.mFloatColorBuffer = ByteBuffer.allocateDirect(size * 4 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
                this.mVertexBuffer = this.mFloatVertexBuffer;
                this.mTexCoordBuffer = this.mFloatTexCoordBuffer;
                this.mColorBuffer = this.mFloatColorBuffer;
                this.mCoordinateSize = 4;
                this.mCoordinateType = 5126;
            }
            int quadW = this.mW - 1;
            int quadH = this.mH - 1;
            int indexCount = quadW * quadH * 6;
            this.mIndexCount = indexCount;
            this.mIndexBuffer = ByteBuffer.allocateDirect(indexCount * 2).order(ByteOrder.nativeOrder()).asCharBuffer();
            int i = 0;
            int y = 0;
            while (y < quadH) {
                int i2 = i;
                for (int x = 0; x < quadW; x++) {
                    char b = (char) ((this.mW * y) + x + 1);
                    char c = (char) (((y + 1) * this.mW) + x);
                    char d = (char) (((y + 1) * this.mW) + x + 1);
                    int i3 = i2 + 1;
                    this.mIndexBuffer.put(i2, (char) ((this.mW * y) + x));
                    int i4 = i3 + 1;
                    this.mIndexBuffer.put(i3, b);
                    int i5 = i4 + 1;
                    this.mIndexBuffer.put(i4, c);
                    int i6 = i5 + 1;
                    this.mIndexBuffer.put(i5, b);
                    int i7 = i6 + 1;
                    this.mIndexBuffer.put(i6, c);
                    i2 = i7 + 1;
                    this.mIndexBuffer.put(i7, d);
                }
                y++;
                i = i2;
            }
            this.mVertBufferIndex = 0;
        }
    }

    /* access modifiers changed from: 0000 */
    public void set(int i, int j, float x, float y, float z, float u, float v, float[] color) {
        if (i < 0 || i >= this.mW) {
            throw new IllegalArgumentException("i");
        } else if (j < 0 || j >= this.mH) {
            throw new IllegalArgumentException("j");
        } else {
            int index = (this.mW * j) + i;
            int posIndex = index * 3;
            int texIndex = index * 2;
            int colorIndex = index * 4;
            if (this.mCoordinateType == 5126) {
                this.mFloatVertexBuffer.put(posIndex, x);
                this.mFloatVertexBuffer.put(posIndex + 1, y);
                this.mFloatVertexBuffer.put(posIndex + 2, z);
                this.mFloatTexCoordBuffer.put(texIndex, u);
                this.mFloatTexCoordBuffer.put(texIndex + 1, v);
                if (color != null) {
                    this.mFloatColorBuffer.put(colorIndex, color[0]);
                    this.mFloatColorBuffer.put(colorIndex + 1, color[1]);
                    this.mFloatColorBuffer.put(colorIndex + 2, color[2]);
                    this.mFloatColorBuffer.put(colorIndex + 3, color[3]);
                    return;
                }
                return;
            }
            this.mFixedVertexBuffer.put(posIndex, (int) (65536.0f * x));
            this.mFixedVertexBuffer.put(posIndex + 1, (int) (65536.0f * y));
            this.mFixedVertexBuffer.put(posIndex + 2, (int) (65536.0f * z));
            this.mFixedTexCoordBuffer.put(texIndex, (int) (65536.0f * u));
            this.mFixedTexCoordBuffer.put(texIndex + 1, (int) (65536.0f * v));
            if (color != null) {
                this.mFixedColorBuffer.put(colorIndex, (int) (color[0] * 65536.0f));
                this.mFixedColorBuffer.put(colorIndex + 1, (int) (color[1] * 65536.0f));
                this.mFixedColorBuffer.put(colorIndex + 2, (int) (color[2] * 65536.0f));
                this.mFixedColorBuffer.put(colorIndex + 3, (int) (color[3] * 65536.0f));
            }
        }
    }

    public static void beginDrawing(GL10 gl, boolean useTexture, boolean useColor) {
        gl.glEnableClientState(32884);
        if (useTexture) {
            gl.glEnableClientState(32888);
            gl.glEnable(3553);
        } else {
            gl.glDisableClientState(32888);
            gl.glDisable(3553);
        }
        if (useColor) {
            gl.glEnableClientState(32886);
        } else {
            gl.glDisableClientState(32886);
        }
    }

    public void draw(GL10 gl, boolean useTexture, boolean useColor) {
        if (!this.mUseHardwareBuffers) {
            gl.glVertexPointer(3, this.mCoordinateType, 0, this.mVertexBuffer);
            if (useTexture) {
                gl.glTexCoordPointer(2, this.mCoordinateType, 0, this.mTexCoordBuffer);
            }
            if (useColor) {
                gl.glColorPointer(4, this.mCoordinateType, 0, this.mColorBuffer);
            }
            gl.glDrawElements(4, this.mIndexCount, 5123, this.mIndexBuffer);
            return;
        }
        GL11 gl11 = (GL11) gl;
        gl11.glBindBuffer(34962, this.mVertBufferIndex);
        gl11.glVertexPointer(3, this.mCoordinateType, 0, 0);
        if (useTexture) {
            gl11.glBindBuffer(34962, this.mTextureCoordBufferIndex);
            gl11.glTexCoordPointer(2, this.mCoordinateType, 0, 0);
        }
        if (useColor) {
            gl11.glBindBuffer(34962, this.mColorBufferIndex);
            gl11.glColorPointer(4, this.mCoordinateType, 0, 0);
        }
        gl11.glBindBuffer(34963, this.mIndexBufferIndex);
        gl11.glDrawElements(4, this.mIndexCount, 5123, 0);
        gl11.glBindBuffer(34962, 0);
        gl11.glBindBuffer(34963, 0);
    }

    public static void endDrawing(GL10 gl) {
        gl.glDisableClientState(32884);
    }

    public boolean usingHardwareBuffers() {
        return this.mUseHardwareBuffers;
    }

    public void invalidateHardwareBuffers() {
        this.mVertBufferIndex = 0;
        this.mIndexBufferIndex = 0;
        this.mTextureCoordBufferIndex = 0;
        this.mColorBufferIndex = 0;
        this.mUseHardwareBuffers = false;
    }

    public void releaseHardwareBuffers(GL10 gl) {
        if (this.mUseHardwareBuffers) {
            if (gl instanceof GL11) {
                GL11 gl11 = (GL11) gl;
                int[] buffer = {this.mVertBufferIndex};
                gl11.glDeleteBuffers(1, buffer, 0);
                buffer[0] = this.mTextureCoordBufferIndex;
                gl11.glDeleteBuffers(1, buffer, 0);
                buffer[0] = this.mColorBufferIndex;
                gl11.glDeleteBuffers(1, buffer, 0);
                buffer[0] = this.mIndexBufferIndex;
                gl11.glDeleteBuffers(1, buffer, 0);
            }
            invalidateHardwareBuffers();
        }
    }

    public void generateHardwareBuffers(GL10 gl) {
        if (!this.mUseHardwareBuffers && (gl instanceof GL11)) {
            GL11 gl11 = (GL11) gl;
            int[] buffer = new int[1];
            gl11.glGenBuffers(1, buffer, 0);
            this.mVertBufferIndex = buffer[0];
            gl11.glBindBuffer(34962, this.mVertBufferIndex);
            gl11.glBufferData(34962, this.mVertexBuffer.capacity() * this.mCoordinateSize, this.mVertexBuffer, 35044);
            gl11.glGenBuffers(1, buffer, 0);
            this.mTextureCoordBufferIndex = buffer[0];
            gl11.glBindBuffer(34962, this.mTextureCoordBufferIndex);
            gl11.glBufferData(34962, this.mTexCoordBuffer.capacity() * this.mCoordinateSize, this.mTexCoordBuffer, 35044);
            gl11.glGenBuffers(1, buffer, 0);
            this.mColorBufferIndex = buffer[0];
            gl11.glBindBuffer(34962, this.mColorBufferIndex);
            gl11.glBufferData(34962, this.mColorBuffer.capacity() * this.mCoordinateSize, this.mColorBuffer, 35044);
            gl11.glBindBuffer(34962, 0);
            gl11.glGenBuffers(1, buffer, 0);
            this.mIndexBufferIndex = buffer[0];
            gl11.glBindBuffer(34963, this.mIndexBufferIndex);
            gl11.glBufferData(34963, this.mIndexBuffer.capacity() * 2, this.mIndexBuffer, 35044);
            gl11.glBindBuffer(34963, 0);
            this.mUseHardwareBuffers = true;
            if (!$assertionsDisabled && this.mVertBufferIndex == 0) {
                throw new AssertionError();
            } else if (!$assertionsDisabled && this.mTextureCoordBufferIndex == 0) {
                throw new AssertionError();
            } else if (!$assertionsDisabled && this.mIndexBufferIndex == 0) {
                throw new AssertionError();
            } else if (!$assertionsDisabled && gl11.glGetError() != 0) {
                throw new AssertionError();
            }
        }
    }

    public final int getVertexBuffer() {
        return this.mVertBufferIndex;
    }

    public final int getTextureBuffer() {
        return this.mTextureCoordBufferIndex;
    }

    public final int getIndexBuffer() {
        return this.mIndexBufferIndex;
    }

    public final int getColorBuffer() {
        return this.mColorBufferIndex;
    }

    public final int getIndexCount() {
        return this.mIndexCount;
    }

    public boolean getFixedPoint() {
        return this.mCoordinateType == 5132;
    }
}
