package com.dlten.lib.opengl;

import android.content.Context;
import android.opengl.GLES10;
import android.opengl.GLU;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.graphics.CDCView;
import com.dlten.lib.graphics.CImgObj;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class CBeijingGLDCView extends CDCView {
    private GL10 GLThread_local_gl = null;
    private EglHelper mEglHelper;

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start(int[] configSpec) {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            EGLConfig[] configs = new EGLConfig[1];
            this.mEgl.eglChooseConfig(this.mEglDisplay, configSpec, configs, 1, new int[1]);
            this.mEglConfig = configs[0];
            this.mEglContext = this.mEgl.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL10.EGL_NO_CONTEXT, null);
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder holder) {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = this.mEgl.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, holder, null);
            this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
            return this.mEglContext.getGL();
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void finish() {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
            if (this.mEglContext != null) {
                this.mEgl.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }

    public CBeijingGLDCView(Context context) {
        super(context);
        initCBaseView();
    }

    public CBeijingGLDCView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCBaseView();
    }

    private void initCBaseView() {
        CImgObj.SetMode(1);
    }

    public void prepareDC() {
        super.prepareDC();
        GLThread_run_start();
    }

    public void releaseDC() {
        GLThread_run_end();
    }

    public final synchronized void update(CEventWnd pWnd) {
        if (pWnd != null) {
            try {
                if (surfaceInitialized()) {
                    clear();
                    pWnd.DrawPrevProc();
                    pWnd.DrawProcess();
                    updateGraphics();
                }
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
        return;
    }

    public void GLThread_run_start() {
        this.mEglHelper = new EglHelper();
        this.mEglHelper.start(null);
        this.GLThread_local_gl = (GL10) this.mEglHelper.createSurface(this.m_holder);
        setCamera();
    }

    public void GLThread_run_end() {
        this.mEglHelper.finish();
    }

    public void updateGraphics() {
        this.mEglHelper.swap();
    }

    public boolean surfaceInitialized() {
        return this.GLThread_local_gl != null;
    }

    private void setCamera() {
        GLES10.glViewport(m_nDblOffsetX, m_nDblOffsetY, SC_WIDTH - (m_nDblOffsetX * 2), SC_HEIGHT - (m_nDblOffsetY * 2));
        GLES10.glMatrixMode(5889);
        GLES10.glLoadIdentity();
        GLES10.glFrustumf(-0.25f, 0.25f, -0.25f, 0.25f, 2.5f, 5.5f);
    }

    public void clear() {
        GLES10.glDisable(3024);
        GLES10.glTexEnvx(8960, 8704, 8448);
        GLES10.glClear(19797);
        GLES10.glMatrixMode(5888);
        GLES10.glLoadIdentity();
        GLU.gluLookAt(this.GLThread_local_gl, 0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        GLES10.glEnable(3042);
        GLES10.glBlendFunc(770, 771);
    }
}
