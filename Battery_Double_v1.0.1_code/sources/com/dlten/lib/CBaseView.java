package com.dlten.lib;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.frmWork.CWndMgr;
import com.dlten.lib.frmWork.HandleActivity;
import com.dlten.lib.graphics.CBmpDCView;
import java.util.Enumeration;
import java.util.Vector;
import org.altbeacon.beacon.BeaconManager;

public abstract class CBaseView extends CBmpDCView implements Callback {
    public static int CYCLE_TIME = 20;
    public static int CYCLE_TIME2 = 32;
    public static int CYCLE_TIME3 = 64;
    private static final int KEY_EVENT_INTERVAL = 100;
    private static final int LPARAM = 2;
    private static final int MSG = 0;
    private static final int WPARAM = 1;
    private HandleActivity m_activity;
    private boolean m_bCalcFps = false;
    private boolean m_bLogFlag = false;
    private boolean m_bSuspended = true;
    private boolean m_bThreadFlag = false;
    private float m_fCycle = 20.0f;
    private float m_fFPS = 50.0f;
    private float m_fUserCycle = (1000.0f / Common.FPS);
    private SurfaceHolder m_holder;
    private long m_lPrevFrameTime = 0;
    private long m_lUpdateTimes = 0;
    private Vector<Object> m_msgQueue;
    private int m_nCurKey = 0;
    private int m_nDelay;
    private int m_nFrameCount = 0;
    private CWndMgr m_wndMgr;

    /* access modifiers changed from: protected */
    public abstract CWndMgr createWndMgr();

    public CBaseView(Context context) {
        super(context);
        this.m_activity = (HandleActivity) context;
        initCBaseView();
    }

    public CBaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_activity = (HandleActivity) context;
        initCBaseView();
    }

    public HandleActivity getActivity() {
        return this.m_activity;
    }

    private void initCBaseView() {
        setFocusableInTouchMode(true);
        setFocusable(true);
        this.m_wndMgr = null;
        this.m_msgQueue = new Vector<>(200, 40);
        this.m_holder = getHolder();
        this.m_holder.addCallback(this);
    }

    public void prepareDC() {
        do {
        } while (this.m_bSuspended);
        super.prepareDC();
    }

    public final void start() {
        this.m_bSuspended = true;
        this.m_wndMgr = createWndMgr();
        if (this.m_wndMgr != null) {
            this.m_bThreadFlag = true;
            this.m_wndMgr.start();
            setFPS(1000.0f / Common.FPS);
        }
    }

    public final void stop() {
        if (this.m_wndMgr != null) {
            this.m_bThreadFlag = false;
            this.m_wndMgr.stop();
        }
    }

    public final void suspend() {
        if (this.m_wndMgr != null) {
            this.m_wndMgr.suspend();
            this.m_bSuspended = true;
        }
    }

    public final void resume() {
        if (this.m_wndMgr != null) {
            this.m_wndMgr.resume();
            this.m_bSuspended = false;
        }
    }

    public CWndMgr getWndMgr() {
        return this.m_wndMgr;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.m_bSuspended) {
            return false;
        }
        int nPosX = getCodePosX((int) event.getX());
        int nPosY = getCodePosY((int) event.getY());
        switch (event.getAction()) {
            case 0:
                addMessage(8, nPosX, nPosY);
                break;
            case 1:
                addMessage(9, nPosX, nPosY);
                break;
            case 2:
                if (this.m_wndMgr != null) {
                    this.m_wndMgr.SendMessage(10, nPosX, nPosY);
                    break;
                }
                break;
            case 3:
                addMessage(9, nPosX, nPosY);
                break;
            case 4:
                addMessage(9, nPosX, nPosY);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        STD.logout("onFocusChanged = (" + getWidth() + ", " + getHeight() + ")");
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        STD.logout("onSizeChanged = (" + w + ", " + h + ", " + oldw + ", " + oldh);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        super.surfaceChanged(holder, format, width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        super.surfaceCreated(holder);
        resume();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        super.surfaceDestroyed(holder);
        suspend();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    public void setEvent(Runnable r) {
        super.setEvent(r);
    }

    private final void intervalProc() {
        int nCurCycle;
        int nDiff = (int) (System.currentTimeMillis() - this.m_lPrevFrameTime);
        int i = CYCLE_TIME;
        if (nDiff < 5000) {
            if (nDiff < 0) {
                nDiff = 0;
            }
            if (nDiff < CYCLE_TIME) {
                STD.sleep((long) (CYCLE_TIME - nDiff));
                nCurCycle = CYCLE_TIME;
            } else if (nDiff < CYCLE_TIME2) {
                STD.sleep((long) (CYCLE_TIME2 - nDiff));
                nCurCycle = CYCLE_TIME2;
            } else if (nDiff < CYCLE_TIME3) {
                STD.sleep((long) (CYCLE_TIME3 - nDiff));
                nCurCycle = CYCLE_TIME3;
            } else {
                nCurCycle = nDiff;
            }
            this.m_fFPS = 1000.0f / ((float) nCurCycle);
            if (this.m_fFPS <= 0.0f) {
                this.m_fFPS = 1.0f;
            }
            if (this.m_bLogFlag) {
                STD.logout("FPS = " + this.m_fFPS);
            }
            updateFrameTime();
        }
    }

    /* access modifiers changed from: protected */
    public final void waitFrame(int nTimeGap1, int nTimeGap2) {
        waitTime(this.m_lPrevFrameTime, nTimeGap1, nTimeGap2);
    }

    /* access modifiers changed from: protected */
    public final void updateFrameTime() {
        this.m_lPrevFrameTime = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public final void waitTime(long lPrevTime, int nTimeGap1, int nTimeGap2) {
        int nLastCycle;
        long lDiff = System.currentTimeMillis() - lPrevTime;
        if (lDiff < 500) {
            if (lDiff < ((long) nTimeGap1)) {
                STD.sleep(((long) nTimeGap1) - lDiff);
                nLastCycle = nTimeGap1;
            } else {
                nLastCycle = (int) lDiff;
            }
            if (this.m_bCalcFps) {
                if (this.m_lUpdateTimes >= BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD) {
                    this.m_lUpdateTimes = 0;
                    this.m_nFrameCount = 0;
                }
                if (this.m_bLogFlag) {
                    STD.logout("Cycle = " + nLastCycle);
                }
                this.m_lUpdateTimes += (long) nLastCycle;
                this.m_nFrameCount++;
                this.m_fCycle = ((float) this.m_lUpdateTimes) / ((float) this.m_nFrameCount);
                this.m_fFPS = (1000.0f * ((float) this.m_nFrameCount)) / ((float) this.m_lUpdateTimes);
                if (this.m_fFPS <= 0.0f) {
                    this.m_fFPS = 1.0f;
                }
            }
        }
    }

    public final void inverseLogFlag() {
        this.m_bLogFlag = !this.m_bLogFlag;
    }

    public final void calcFps(boolean bCalc) {
        this.m_bCalcFps = bCalc;
    }

    public final void PostMessage(int msg, int wParam, int lParam) {
        addMessage(msg, wParam, lParam);
    }

    /* access modifiers changed from: protected */
    public final void addMessage(int msg) {
        addMessage(msg, 0, 0);
    }

    /* access modifiers changed from: protected */
    public final void addMessage(int msg, int wParam, int lParam) {
        addMessage(new int[]{msg, wParam, lParam});
    }

    /* access modifiers changed from: protected */
    public final void addMessage(int[] msg) {
        if (this.m_msgQueue != null) {
            synchronized (this.m_msgQueue) {
                this.m_msgQueue.addElement(msg);
            }
        }
    }

    public final void clearMsgQueue() {
        Object obj;
        Vector<Object> tempQueue = new Vector<>(5);
        synchronized (this.m_msgQueue) {
            Enumeration<Object> e = this.m_msgQueue.elements();
            while (e.hasMoreElements()) {
                Object obj2 = e.nextElement();
                if (obj2 == null) {
                    STD.ASSERT(false);
                } else {
                    try {
                        if (((int[]) obj2)[0] == 17) {
                            tempQueue.addElement(obj2);
                            try {
                                obj = e.nextElement();
                            } catch (Exception e2) {
                                STD.ASSERT(false);
                                obj = null;
                            }
                            tempQueue.addElement(obj);
                        } else {
                            continue;
                        }
                    } catch (ClassCastException e3) {
                        STD.ASSERT(false);
                    }
                }
            }
            this.m_msgQueue.removeAllElements();
            Enumeration<Object> e4 = tempQueue.elements();
            while (e4.hasMoreElements()) {
                this.m_msgQueue.addElement(e4.nextElement());
            }
        }
    }

    private boolean isExistSameMsg(int nMsg) {
        boolean bFind = false;
        synchronized (this.m_msgQueue) {
            Enumeration<Object> e = this.m_msgQueue.elements();
            while (e.hasMoreElements()) {
                try {
                    int[] bufMessage = (int[]) e.nextElement();
                    if (bufMessage == null) {
                        continue;
                    } else if (bufMessage[0] == nMsg) {
                        bFind = true;
                    }
                } catch (ClassCastException e2) {
                }
            }
        }
        return bFind;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return false;
     */
    private boolean isNextMsg(int nMsg) {
        synchronized (this.m_msgQueue) {
            if (this.m_msgQueue.size() >= 1 && ((int[]) this.m_msgQueue.elementAt(0))[0] == nMsg) {
                return true;
            }
        }
    }

    public void NotifyNetEvents(int nEvtType, int nParam, Object objData) {
        synchronized (this.m_msgQueue) {
            this.m_msgQueue.addElement(new int[]{17, nEvtType, nParam});
            this.m_msgQueue.addElement(objData);
        }
    }

    private Object popFirstMsg() {
        Object elementAt;
        synchronized (this.m_msgQueue) {
            if (this.m_msgQueue.isEmpty()) {
                elementAt = null;
            } else {
                elementAt = this.m_msgQueue.elementAt(0);
                this.m_msgQueue.removeElementAt(0);
            }
        }
        return elementAt;
    }

    public void DeleteMsgs(int[] messages) {
        synchronized (this.m_msgQueue) {
            Vector<Object> msgs = new Vector<>(40, 5);
            while (!this.m_msgQueue.isEmpty()) {
                int[] msg = (int[]) this.m_msgQueue.elementAt(0);
                this.m_msgQueue.removeElementAt(0);
                if (!isExistMsg(msg[0], messages)) {
                    msgs.addElement(msg);
                    if (msg[0] == 17 && this.m_msgQueue.size() > 0) {
                        Object objTemp = this.m_msgQueue.elementAt(0);
                        this.m_msgQueue.removeElementAt(0);
                        msgs.addElement(objTemp);
                    }
                } else if (msg[0] == 17 && this.m_msgQueue.size() > 0) {
                    this.m_msgQueue.removeElementAt(0);
                }
            }
            while (!msgs.isEmpty()) {
                Object msg2 = msgs.elementAt(0);
                msgs.removeElementAt(0);
                this.m_msgQueue.addElement(msg2);
            }
        }
    }

    private boolean isExistMsg(int msg, int[] messages) {
        if (messages == null) {
            return false;
        }
        for (int i : messages) {
            if (msg == i) {
                return true;
            }
        }
        return false;
    }

    public int RunProc(CEventWnd pWnd) {
        while (this.m_bThreadFlag) {
            try {
                if (this.m_bSuspended) {
                    STD.sleep(50);
                    updateFrameTime();
                } else {
                    Object objTemp = popFirstMsg();
                    if (objTemp != null) {
                        int[] msg = (int[]) objTemp;
                        switch (msg[0]) {
                            case 2:
                                if (isNextMsg(2)) {
                                    break;
                                }
                            case 8:
                            case 9:
                                if (true != isExistSameMsg(msg[0])) {
                                    pWnd.WindowProc(msg[0], msg[1], msg[2]);
                                    break;
                                }
                                break;
                            case 12:
                                if (true != isExistSameMsg(msg[0])) {
                                    this.m_nCurKey = msg[1];
                                    this.m_nDelay = 0;
                                    pWnd.WindowProc(msg[0], msg[1], msg[2]);
                                    if (this.m_nCurKey == 4096 || this.m_nCurKey == 1024 || this.m_nCurKey == 262144) {
                                        this.m_nCurKey = 0;
                                        break;
                                    }
                                }
                                break;
                            case 13:
                                if (msg[1] == this.m_nCurKey) {
                                    this.m_nCurKey = 0;
                                }
                                pWnd.WindowProc(msg[0], msg[1], msg[2]);
                                break;
                            case 15:
                                pWnd.NotifyToParentEndRun();
                                return msg[1];
                            case 17:
                                CEventWnd cEventWnd = pWnd;
                                int nret = cEventWnd.OnNetEvent(msg[1], msg[2], popFirstMsg());
                                if (msg[1] == 2 && nret < 0) {
                                    STD.ASSERT(false);
                                    break;
                                }
                            case 19:
                                break;
                            default:
                                pWnd.WindowProc(msg[0], msg[1], msg[2]);
                                break;
                        }
                        switch (msg[0]) {
                            case 8:
                            case 9:
                            case 12:
                                DeleteMsgs(new int[]{msg[0]});
                                break;
                        }
                    } else if (this.m_nCurKey != 0) {
                        if (this.m_nDelay >= 300) {
                            pWnd.WindowProc(14, this.m_nCurKey, 0);
                            this.m_nDelay = 300;
                        } else {
                            this.m_nDelay += 100;
                        }
                        STD.sleep(100);
                    }
                    pWnd.UpdateWindow();
                    waitFrame((int) this.m_fUserCycle, (int) this.m_fUserCycle);
                    updateFrameTime();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public void setFPS(float fCycle) {
        this.m_fUserCycle = fCycle;
    }

    public float getFPS() {
        return this.m_fFPS;
    }

    private void perFrameProc() {
        waitFrame(16, 20);
    }
}
