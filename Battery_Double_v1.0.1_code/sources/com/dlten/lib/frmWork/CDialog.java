package com.dlten.lib.frmWork;

import android.support.v4.view.MotionEventCompat;
import com.dlten.lib.STD;

public abstract class CDialog extends CEventWnd {
    private boolean m_bModal;

    public CDialog() {
        this.m_pParent = null;
        this.m_bModal = true;
    }

    public void DrawWindowName() {
        if (this.m_strName != null) {
            drawStr(0, 50, MotionEventCompat.ACTION_POINTER_INDEX_MASK, 17, this.m_strName);
        }
    }

    public void DrawProcess() {
        if (this.m_bModal) {
            GetParent().DrawProcess();
        }
        OnPaint();
        for (int i = 0; i < this.m_Anims.size(); i++) {
            CAnimation animObj = (CAnimation) this.m_Anims.elementAt(i);
            if (animObj.IsDrawBylib()) {
                animObj.Draw();
            }
            animObj.UpdateFrame();
        }
        for (int i2 = 0; i2 < this.m_Btns.size(); i2++) {
            ((CButton) this.m_Btns.elementAt(i2)).Draw();
        }
    }

    public void DrawPrevProc() {
        if (this.m_bModal) {
            GetParent().DrawPrevProc();
        }
    }

    public int OnNetEvent(int nEvtType, int nParam, Object objData) {
        return -1;
    }

    public void OnPaint() {
    }

    public void OnTouchDown(int x, int y) {
    }

    public void OnKeyDown(int keycode) {
    }

    public int DoModal(CEventWnd pParent) {
        this.m_bModal = true;
        return CWndMgr.getInstance().DialogDoModal(this, pParent);
    }

    public void Modaless(CEventWnd pParent) {
        this.m_bModal = false;
        CWndMgr.getInstance().DialogModaless(this, pParent);
    }

    public void Destroy() {
        STD.ASSERT(!this.m_bModal);
        this.m_bModal = true;
        CWndMgr.getInstance().DialogDestroy(this, GetParent());
    }
}
