package com.dlten.lib.frmWork;

import com.dlten.lib.CBaseView;
import com.dlten.lib.STD;
import com.dlten.lib.graphics.CDCView;

public abstract class CWndMgr implements Runnable {
    private static CWndMgr _instance = null;
    private CDCView m_DCView;
    private CBaseView m_baseView;
    private CWnd m_pCurShowWnd;
    private CEventWnd m_pOldFore;
    private CEventWnd m_pOldModaless;
    private Thread m_thread = null;

    /* access modifiers changed from: protected */
    public abstract CWnd createWindow(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract void runProc();

    public static CWndMgr getInstance() {
        if (_instance == null) {
            STD.ASSERT(false);
        }
        return _instance;
    }

    public CWndMgr(CBaseView view) {
        _instance = this;
        this.m_DCView = view;
        this.m_baseView = view;
        CEventWnd.setView(this.m_baseView);
        this.m_pCurShowWnd = null;
        this.m_pOldModaless = null;
        this.m_pOldFore = null;
    }

    public final void start() {
        this.m_thread = new Thread(this);
        if (this.m_thread != null) {
            this.m_thread.start();
        }
    }

    public final void stop() {
        if (this.m_thread != null) {
            boolean retry = true;
            while (retry) {
                try {
                    this.m_thread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            this.m_thread = null;
        }
    }

    public final void suspend() {
        if (this.m_thread != null && this.m_pCurShowWnd != null) {
            this.m_pCurShowWnd.OnSuspend();
        }
    }

    public final void resume() {
        if (this.m_thread != null && this.m_pCurShowWnd != null) {
            this.m_pCurShowWnd.OnResume();
        }
    }

    public final void run() {
        Initialize();
        runProc();
        Finalize();
    }

    public final void SendMessage(int message, int wParam, int lParam) {
        if (this.m_pCurShowWnd != null) {
            this.m_pCurShowWnd.SendMessage(message, wParam, lParam);
        }
    }

    public final CWnd GetCurWnd() {
        return this.m_pCurShowWnd;
    }

    public int SwitchWindow(int index) {
        return 0;
    }

    public int SwitchWindow(int index, int nParam) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void Initialize() {
        this.m_DCView.prepareDC();
        CEventWnd.prepareDC();
    }

    /* access modifiers changed from: protected */
    public void Finalize() {
        this.m_DCView.releaseDC();
    }

    /* access modifiers changed from: protected */
    public final int NewWindow(int nWndID) {
        return NewWindow(nWndID, 0);
    }

    /* access modifiers changed from: protected */
    public final int NewWindow(int nWndID, int nParam) {
        this.m_pCurShowWnd = createWindow(nWndID, nParam);
        if (this.m_pCurShowWnd == null) {
            return -1;
        }
        int WndEventLoop = WndEventLoop(this.m_pCurShowWnd);
        this.m_pCurShowWnd = null;
        return WndEventLoop;
    }

    private final int WndEventLoop(CWnd pWnd) {
        this.m_pCurShowWnd = pWnd;
        pWnd.SetActiveWnd(pWnd);
        pWnd.OnInitWindow();
        pWnd.OnLoadResource();
        if (pWnd.m_strName != null) {
            STD.logout("\tShow(" + pWnd.m_strName + ")");
        }
        pWnd.OnShowWindow();
        if (pWnd.m_strName != null) {
            STD.logout("\tRun(" + pWnd.m_strName + ")");
        }
        int nRet = pWnd.RunProc();
        if (pWnd.m_strName != null) {
            STD.logout("\tDestroy(" + pWnd.m_strName + ")");
        }
        pWnd.OnDestroy();
        return nRet;
    }

    /* access modifiers changed from: protected */
    public final int SwitchingWnd(int nWndID) {
        return SwitchingWnd(nWndID, 0);
    }

    /* access modifiers changed from: protected */
    public final int SwitchingWnd(int nWndID, int nParam) {
        return SwitchWindow_Proc(createWindow(nWndID, nParam), this.m_pCurShowWnd);
    }

    /* access modifiers changed from: protected */
    public final void SwitchWinodw_Prepare() {
        this.m_pCurShowWnd.OnDestroy();
    }

    private final int SwitchWindow_Proc(CWnd pNewWnd, CWnd pBeforeWnd) {
        pNewWnd.SetParent(pBeforeWnd);
        int nRet = WndEventLoop(pNewWnd);
        this.m_pCurShowWnd = pBeforeWnd;
        return nRet;
    }

    /* access modifiers changed from: protected */
    public final void SwitchWinodw_Finish() {
        this.m_pCurShowWnd.OnShowWindow();
    }

    public final int DialogDoModal(CDialog pDialogWnd, CEventWnd pParent) {
        pDialogWnd.SetParent(pParent);
        boolean bEnable = pDialogWnd.GetParent().EnableWindow(false);
        CEventWnd pOldActive = pParent.SetActiveWnd(pDialogWnd);
        CEventWnd pOldFore = pParent.SetForegroundWnd(pDialogWnd);
        pDialogWnd.OnInitWindow();
        pDialogWnd.OnLoadResource();
        if (pDialogWnd.m_strName != null) {
            STD.logout("\tShow(" + pDialogWnd.m_strName + ")");
        }
        pDialogWnd.OnShowWindow();
        if (pDialogWnd.m_strName != null) {
            STD.logout("\tRun(" + pDialogWnd.m_strName + ")");
        }
        int nRet = pDialogWnd.RunProc();
        if (pDialogWnd.m_strName != null) {
            STD.logout("\tDestroy(" + pDialogWnd.m_strName + ")");
        }
        pDialogWnd.OnDestroy();
        pParent.SetForegroundWnd(pOldFore);
        pParent.SetActiveWnd(pOldActive);
        pDialogWnd.GetParent().EnableWindow(bEnable);
        return nRet;
    }

    public final void DialogModaless(CDialog pDialogWnd, CEventWnd pParent) {
        pDialogWnd.SetParent(pParent);
        this.m_pOldModaless = pParent.SetModalessWnd(pDialogWnd);
        this.m_pOldFore = pParent.SetForegroundWnd(pDialogWnd);
        pDialogWnd.OnInitWindow();
        pDialogWnd.OnLoadResource();
        pDialogWnd.OnShowWindow();
    }

    public final void DialogDestroy(CDialog pDialogWnd, CEventWnd pParent) {
        pParent.SetForegroundWnd(this.m_pOldFore);
        pParent.SetModalessWnd(this.m_pOldModaless);
        pDialogWnd.OnDestroy();
        pDialogWnd.GetParent().UpdateWindow();
    }
}
