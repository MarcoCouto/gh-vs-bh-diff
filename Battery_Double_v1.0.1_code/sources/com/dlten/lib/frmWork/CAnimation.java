package com.dlten.lib.frmWork;

public abstract class CAnimation implements AnimListner {
    public static final int CALLBACK_EVER_FRM = 3;
    public static final int CALLBACK_LAST_FRM = 1;
    public static final int CALLBACK_NONE = 0;
    public static final int CALLBACK_SPEC_FRM = 2;
    public static final int CallbackType_MAX = 4;
    public static final short IDA_NONE = -1;
    private boolean m_bActive;
    private boolean m_bEnd;
    private boolean m_bLoop;
    private boolean m_bOwnerDraw;
    private boolean m_bParentRegistered;
    private boolean m_bPause;
    private boolean m_bVisible;
    private int m_callBackType;
    private short m_nAnimID;
    private int m_nCallBackFrameNum;
    private int m_nCurFrame;
    int m_nDirection;
    private int m_nEndFrame;
    private short m_nSpeed;
    private int m_nSpeedFrame;
    private int m_nStartFrame;
    private Object m_pParam;
    private CEventWnd m_pParent;
    private AnimListner m_pProc;

    /* access modifiers changed from: protected */
    public abstract void DrawProc();

    public void onAnimAction(CAnimation anim, Object param) {
        CAnimation pAnim = anim;
        CEventWnd pWnd = pAnim.m_pParent;
        if (pWnd == CWndMgr.getInstance().GetCurWnd() && pWnd != null) {
            pWnd.PostMessage(16, pAnim.GetID());
        }
    }

    public void ChangeCallBackType(int callBack, short nAnimID, int nFrameNum) {
        ChangeCallBackType(this, null, callBack, nFrameNum, nAnimID);
    }

    public void ChangeCallBackType(AnimListner proc, Object param, int callBack, int nFrameNum, short nAnimID) {
        this.m_nAnimID = nAnimID;
        this.m_pProc = proc;
        this.m_pParam = param;
        this.m_callBackType = callBack;
        this.m_nCallBackFrameNum = nFrameNum;
    }

    public CAnimation() {
        Reset();
    }

    public short GetID() {
        return this.m_nAnimID;
    }

    public void Create(CEventWnd pParent, int nStartFrame, int nEndFrame, boolean bLoop, boolean bOwnerDraw) {
        Reset();
        this.m_pParent = pParent;
        this.m_nStartFrame = nStartFrame;
        this.m_nEndFrame = nEndFrame;
        this.m_bLoop = bLoop;
        this.m_bOwnerDraw = bOwnerDraw;
    }

    private void Reset() {
        this.m_pParent = null;
        this.m_nAnimID = -1;
        this.m_nStartFrame = 0;
        this.m_nEndFrame = 0;
        this.m_nCurFrame = 0;
        this.m_bLoop = false;
        this.m_nSpeed = 8;
        this.m_nSpeed = 16;
        this.m_bVisible = true;
        this.m_bPause = false;
        this.m_bEnd = false;
        this.m_bOwnerDraw = true;
        this.m_nDirection = 1;
        this.m_bActive = false;
        this.m_bParentRegistered = false;
        this.m_callBackType = 0;
        this.m_nCallBackFrameNum = 0;
        this.m_pProc = null;
        this.m_pParam = null;
        Stop();
    }

    private void Register() {
        if (this.m_pParent != null && !this.m_bParentRegistered) {
            this.m_pParent.AddAnimation(this);
            this.m_bParentRegistered = true;
        }
    }

    private void Unregister() {
        if (this.m_pParent != null && this.m_bParentRegistered) {
            this.m_pParent.RemoveAnimation(this);
            this.m_bParentRegistered = false;
        }
    }

    public void Start() {
        Stop();
        this.m_nCurFrame = this.m_nStartFrame;
        this.m_bPause = false;
        this.m_bEnd = false;
        this.m_nSpeedFrame = 0;
        this.m_nDirection = 1;
        this.m_bActive = true;
        Register();
        BindResource();
        SetFrame(this.m_nCurFrame);
    }

    public void Stop() {
        this.m_bActive = false;
        this.m_nSpeedFrame = 0;
        Unregister();
        UnbindResource();
    }

    public void Pause() {
        this.m_bPause = true;
    }

    public void Resume() {
        this.m_bPause = false;
    }

    public boolean Animate(int dwKeys) {
        if (!IsActive()) {
            Start();
        }
        while (!IsEnd()) {
            if (this.m_pParent == null || !this.m_bParentRegistered) {
                Show();
            } else {
                this.m_pParent.UpdateWindow();
            }
        }
        return true;
    }

    public void Show() {
        Draw();
        UpdateFrame();
    }

    public void Draw() {
        if (GetVisible()) {
            DrawProc();
        }
    }

    public boolean UpdateFrame() {
        if (!IsActive() || IsPaused()) {
            return false;
        }
        this.m_nCurFrame = CalcNextFrame();
        EndFrameProc();
        SetFrame(this.m_nCurFrame);
        CallBackFrameProc();
        return true;
    }

    public void SetFrame(int xx) {
    }

    public void Reverse() {
        this.m_nDirection = -this.m_nDirection;
    }

    public int GetDirection() {
        return this.m_nDirection;
    }

    public void SetVisible(boolean bVisible) {
        this.m_bVisible = bVisible;
    }

    public final boolean GetVisible() {
        return this.m_bVisible;
    }

    public void SetSpeed(short nSpeed) {
        this.m_nSpeed = nSpeed;
    }

    public short GetSpeed() {
        return this.m_nSpeed;
    }

    public boolean IsPaused() {
        return this.m_bPause;
    }

    public boolean IsEnd() {
        return this.m_bEnd || this.m_nEndFrame == this.m_nStartFrame;
    }

    public boolean IsLoop() {
        return this.m_bLoop;
    }

    public boolean IsDrawBylib() {
        return this.m_bOwnerDraw;
    }

    public void SetDrawBylib(boolean bOwnerDraw) {
        this.m_bOwnerDraw = bOwnerDraw;
    }

    public boolean IsActive() {
        return this.m_bActive;
    }

    /* access modifiers changed from: protected */
    public void BindResource() {
    }

    /* access modifiers changed from: protected */
    public void UnbindResource() {
    }

    /* access modifiers changed from: protected */
    public int GetCurFrameNum() {
        return this.m_nCurFrame;
    }

    private int CalcNextFrame() {
        int nNextFrame = this.m_nCurFrame;
        this.m_nSpeedFrame += GetSpeed();
        int nStep = this.m_nSpeedFrame / 8;
        this.m_nSpeedFrame %= 8;
        return nNextFrame + (nStep * this.m_nDirection);
    }

    private void EndFrameProc() {
        if (this.m_nDirection > 0) {
            if (this.m_nCurFrame <= this.m_nEndFrame - 1) {
                return;
            }
            if (this.m_bLoop) {
                this.m_nCurFrame = this.m_nStartFrame;
                return;
            }
            this.m_nCurFrame = this.m_nEndFrame - 1;
            this.m_bActive = false;
            this.m_bEnd = true;
        } else if (this.m_nCurFrame >= this.m_nStartFrame) {
        } else {
            if (this.m_bLoop) {
                this.m_nCurFrame = this.m_nEndFrame - 1;
                return;
            }
            this.m_nCurFrame = this.m_nStartFrame;
            this.m_bActive = false;
            this.m_bEnd = true;
        }
    }

    private void CallBackFrameProc() {
        switch (this.m_callBackType) {
            case 1:
                if (IsEnd() && this.m_pProc != null) {
                    this.m_pProc.onAnimAction(this, this.m_pParam);
                    return;
                }
                return;
            case 2:
                if (this.m_nCurFrame == this.m_nCallBackFrameNum && this.m_pProc != null) {
                    this.m_pProc.onAnimAction(this, this.m_pParam);
                    return;
                }
                return;
            case 3:
                if (this.m_pProc != null) {
                    this.m_pProc.onAnimAction(this, this.m_pParam);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
