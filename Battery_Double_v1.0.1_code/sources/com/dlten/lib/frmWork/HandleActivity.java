package com.dlten.lib.frmWork;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import com.dlten.lib.Common;
import com.dlten.lib.STD;
import java.io.File;
import java.util.Iterator;

public abstract class HandleActivity extends Activity implements MainThreadListner {
    /* access modifiers changed from: private */
    public int prevState = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstInit();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        clearApplicationCache(null);
        ((ActivityManager) getSystemService("activity")).restartPackage(getPackageName());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 24:
                mediaVolumeUp();
                break;
            case 25:
                mediaVolumeDown();
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    private void firstInit() {
        Display d = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        int width = d.getWidth();
        int height = d.getHeight();
        STD.logout("width=" + width + ", height=" + height);
        Common.Initialize(this, width, height);
    }

    private void mediaVolumeDown() {
        ((AudioManager) getSystemService("audio")).adjustStreamVolume(3, -1, 1);
    }

    private void mediaVolumeUp() {
        ((AudioManager) getSystemService("audio")).adjustStreamVolume(3, 1, 1);
    }

    public void requestKillProcess() {
        new Thread(new Runnable() {
            public void run() {
                ActivityManager am = (ActivityManager) HandleActivity.this.getSystemService("activity");
                String name = HandleActivity.this.getApplicationInfo().processName;
                while (true) {
                    Iterator it = am.getRunningAppProcesses().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        RunningAppProcessInfo i = (RunningAppProcessInfo) it.next();
                        if (i.processName.equals(name)) {
                            if (HandleActivity.this.prevState != i.importance) {
                                STD.logout("process '" + name + "',s state is " + i.importance);
                                HandleActivity.this.prevState = i.importance;
                            }
                            if (i.importance >= 400) {
                                HandleActivity.this.clearApplicationCache(null);
                                am.restartPackage(HandleActivity.this.getPackageName());
                            }
                        } else {
                            Thread.yield();
                        }
                    }
                    STD.sleep(30);
                }
            }
        }, "Process Killer").start();
    }

    /* access modifiers changed from: private */
    public void clearApplicationCache(File dir) {
        if (dir == null) {
            dir = getCacheDir();
        }
        if (dir != null) {
            File[] children = dir.listFiles();
            int i = 0;
            while (i < children.length) {
                try {
                    if (children[i].isDirectory()) {
                        clearApplicationCache(children[i]);
                    } else {
                        children[i].delete();
                    }
                    i++;
                } catch (Exception e) {
                    return;
                }
            }
        }
    }
}
