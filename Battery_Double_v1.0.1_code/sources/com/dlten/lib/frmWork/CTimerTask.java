package com.dlten.lib.frmWork;

import java.util.TimerTask;

/* compiled from: CWnd */
class CTimerTask extends TimerTask {
    private int m_nTimerID;
    private CTimerListner m_pListner;
    private CWnd m_pWnd = null;

    public CTimerTask(CWnd pWnd) {
        this.m_pWnd = pWnd;
        this.m_pListner = null;
        this.m_nTimerID = -1;
    }

    public boolean SetTimer(int nTimerID, CTimerListner listner) {
        this.m_nTimerID = nTimerID;
        this.m_pListner = listner;
        return true;
    }

    public void KillTimer() {
        cancel();
    }

    public void run() {
        if (this.m_pListner != null) {
            this.m_pListner.TimerProc(this.m_pWnd, this.m_nTimerID);
        } else if (this.m_pWnd != null) {
            this.m_pWnd.PostMessage(4, this.m_nTimerID, 0);
        }
    }
}
