package com.dlten.lib.frmWork;

import com.dlten.lib.graphics.CImgObj;
import com.dlten.lib.graphics.CPoint;
import com.dlten.lib.graphics.CRect;

public class CToggleButton extends CButton {
    public static final int BS_FOCUS = 2;
    public static final int BS_NORMAL = 1;
    public static final int CMD_NONE = -1;
    private boolean m_bEnable = false;
    private boolean m_bVisible = false;
    private int m_command = -1;
    private CImgObj m_dis = null;
    private CImgObj m_foc = null;
    private CImgObj m_nor = null;
    private CEventWnd m_parent = null;
    private CPoint m_pos = new CPoint();
    private CRect m_rect = new CRect();
    private int m_state = 1;

    public CToggleButton() {
    }

    public CToggleButton(CEventWnd parent, CPoint point, CImgObj nor, CImgObj foc, CImgObj dis) {
        create(parent, point, nor, foc, dis);
    }

    public CToggleButton(CEventWnd parent, CImgObj nor, CImgObj foc, CImgObj dis) {
        create(parent, new CPoint(0, 0), nor, foc, dis);
    }

    public void create(CEventWnd parent, String norName, String focName, String disName) {
        CEventWnd cEventWnd = parent;
        create(cEventWnd, new CPoint(0, 0), new CImgObj(norName), new CImgObj(focName), new CImgObj(disName));
    }

    public void create(CEventWnd parent, CPoint point, String norName, String focName, String disName) {
        create(parent, point, new CImgObj(norName), new CImgObj(focName), new CImgObj(disName));
    }

    public void create(CEventWnd parent, CPoint point, CImgObj nor, CImgObj foc, CImgObj dis) {
        this.m_nor = nor;
        this.m_foc = foc;
        this.m_dis = dis;
        this.m_parent = parent;
        this.m_parent.AddButton(this);
        setPoint(point);
        setEnable(true);
        setVisible(true);
        setNormal();
    }

    public void destroy() {
        this.m_parent.RemoveButton(this);
        this.m_pos = null;
        this.m_nor = null;
        this.m_foc = null;
        this.m_dis = null;
        this.m_pos = null;
        this.m_rect = null;
    }

    public void Draw() {
        if (getVisible()) {
            if (!isEnable()) {
                this.m_dis.draw(this.m_pos);
                return;
            }
            switch (this.m_state) {
                case 1:
                    this.m_nor.draw(this.m_pos);
                    return;
                case 2:
                    this.m_foc.draw(this.m_pos);
                    return;
                default:
                    return;
            }
        }
    }

    public CPoint getPoint() {
        return this.m_pos;
    }

    public void setPoint(float x, float y) {
        this.m_pos.x = x;
        this.m_pos.y = y;
        setRect();
    }

    public void setPoint(CPoint point) {
        this.m_pos = point;
        setRect();
    }

    public CRect getRect() {
        return this.m_rect;
    }

    private void setRect() {
        this.m_rect.left = this.m_pos.x;
        this.m_rect.top = this.m_pos.y;
        this.m_rect.width = this.m_nor.getSizeX();
        this.m_rect.height = this.m_nor.getSizeY();
    }

    public boolean isInside(CPoint pt) {
        return this.m_rect.PtInRect(pt);
    }

    public boolean isEnable() {
        return this.m_bEnable;
    }

    public void setEnable(boolean enable) {
        this.m_bEnable = enable;
    }

    public boolean getVisible() {
        return this.m_bVisible;
    }

    public void setVisible(boolean bVisible) {
        this.m_bVisible = bVisible;
    }

    public int getState() {
        return this.m_state;
    }

    public void setState(int state) {
        this.m_state = state;
    }

    public void setNormal() {
        this.m_state = 1;
    }

    public void setFocus() {
        this.m_state = 2;
    }

    public int getCommand() {
        return this.m_command;
    }

    public void setCommand(int cmd) {
        this.m_command = cmd;
    }
}
