package com.dlten.lib.graphics;

public class CRect {
    public float height;
    public float left;
    public float top;
    public float width;

    public CRect() {
        this.left = 0.0f;
        this.top = 0.0f;
        this.width = 0.0f;
        this.height = 0.0f;
    }

    public CRect(float l, float t, float w, float h) {
        this.left = l;
        this.top = t;
        this.width = w;
        this.height = h;
    }

    public CRect(CRect srcRect) {
        this.left = srcRect.left;
        this.width = srcRect.width;
        this.top = srcRect.top;
        this.height = srcRect.height;
    }

    public CRect(CPoint point, CSize size) {
        this.left = point.x;
        this.top = point.y;
        this.width = size.w;
        this.height = size.h;
    }

    public CRect(CPoint topLeft, CPoint bottomRight) {
        this.left = topLeft.x;
        this.top = topLeft.y;
        this.width = bottomRight.x - topLeft.x;
        this.height = bottomRight.y - topLeft.y;
    }

    public float Right() {
        return this.left + this.width;
    }

    public float Bottom() {
        return this.top + this.height;
    }

    public CSize Size() {
        return new CSize(this.width, this.height);
    }

    public CPoint TopLeft() {
        return new CPoint(this.left, this.top);
    }

    public CPoint BottomRight() {
        return new CPoint(this.left + this.width, this.top + this.height);
    }

    public CPoint CenterPoint() {
        return new CPoint(this.left + (this.width / 2.0f), this.top + (this.height / 2.0f));
    }

    public void SwapLeftRight() {
        this.left += this.width;
        this.width = -this.width;
    }

    public boolean IsRectEmpty() {
        if (this.width > 0.0f && this.height > 0.0f) {
            return false;
        }
        return true;
    }

    public boolean IsRectNull() {
        return this.left == 0.0f && this.width == 0.0f && this.top == 0.0f && this.height == 0.0f;
    }

    public boolean PtInRect(CPoint point) {
        if (this.left > point.x || this.left + this.width <= point.x || this.top > point.y || this.top + this.height <= point.y) {
            return false;
        }
        return true;
    }

    public void SetRect(float l, float t, float w, float h) {
        this.left = l;
        this.top = t;
        this.width = w;
        this.height = h;
    }

    public void SetRect(CPoint topLeft, CPoint bottomRight) {
        SetRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    public void SetRectEmpty() {
        SetRect(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public void CopyRect(CRect rectSrc) {
        if (rectSrc != null) {
            this.left = rectSrc.left;
            this.top = rectSrc.top;
            this.width = rectSrc.width;
            this.height = rectSrc.height;
        }
    }

    public boolean EqualRect(CRect rect) {
        if (rect != null && this.left == rect.left && this.top == rect.top && this.width == rect.width && this.height == rect.height) {
            return true;
        }
        return false;
    }

    public void InflateRect(float dx, float dy) {
        this.left -= dx;
        this.top -= dy;
        this.width += 2.0f * dx;
        this.height += 2.0f * dy;
    }

    public void InflateRect(CSize size) {
        InflateRect(size.w, size.h);
    }

    public void DeflateRect(float x, float y) {
        InflateRect(-x, -y);
    }

    public void DeflateRect(CSize size) {
        InflateRect(-size.w, -size.h);
    }

    public void OffsetRect(float dx, float dy) {
        this.left += dx;
        this.top += dy;
    }

    public void OffsetRect(CPoint point) {
        OffsetRect(point.x, point.y);
    }

    public void OffsetRect(CSize size) {
        OffsetRect(size.w, size.h);
    }

    public boolean IntersectRect(CRect rectSrc1, CRect rectSrc2) {
        if (rectSrc1 == null || rectSrc2 == null) {
            return false;
        }
        this.left = Math.max(rectSrc1.left, rectSrc2.left);
        float right = Math.min(rectSrc1.Right(), rectSrc2.Right());
        this.width = right - this.left;
        if (this.left > right) {
            SetRectEmpty();
            return false;
        }
        this.top = Math.max(rectSrc1.top, rectSrc2.top);
        float bottom = Math.min(rectSrc1.Bottom(), rectSrc2.Bottom());
        this.height = bottom - this.top;
        if (this.top <= bottom) {
            return true;
        }
        SetRectEmpty();
        return false;
    }

    public boolean UnionRect(CRect rectSrc1, CRect rectSrc2) {
        if (rectSrc1 == null || rectSrc2 == null) {
            return false;
        }
        this.left = Math.max(Math.max(rectSrc1.Right(), rectSrc2.Right()), Math.max(rectSrc1.left, rectSrc2.left));
        this.width = Math.max(Math.max(rectSrc1.left, rectSrc2.left), Math.max(rectSrc1.Right(), rectSrc2.Right())) - this.left;
        this.top = Math.max(Math.max(rectSrc1.Bottom(), rectSrc2.Bottom()), Math.max(rectSrc1.top, rectSrc2.top));
        this.height = Math.max(Math.max(rectSrc1.top, rectSrc2.top), Math.max(rectSrc1.Bottom(), rectSrc2.Bottom())) - this.top;
        return true;
    }

    public void NormalizeRect() {
        if (this.width < 0.0f) {
            this.left += this.width;
            this.width = -this.width;
        }
        if (this.height < 0.0f) {
            this.top += this.height;
            this.height = -this.height;
        }
    }

    public void InflateRect(CRect rect) {
        this.left -= rect.left;
        this.top -= rect.top;
        this.width += rect.left + (rect.width * 2.0f);
        this.height += rect.top + (rect.height * 2.0f);
    }

    public void InflateRect(float l, float t, float r, float b) {
        this.left -= l;
        this.top -= t;
        this.width += l + r;
        this.height += t + b;
    }

    public void DeflateRect(CRect rect) {
        this.left += rect.left;
        this.top += rect.top;
        this.width -= rect.left + (rect.width * 2.0f);
        this.height -= rect.top + (rect.height * 2.0f);
    }

    public void DeflateRect(float l, float t, float r, float b) {
        this.left += l;
        this.top += t;
        this.width -= l + r;
        this.height -= t + b;
    }
}
