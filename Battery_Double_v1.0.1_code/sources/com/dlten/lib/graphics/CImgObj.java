package com.dlten.lib.graphics;

import android.graphics.Matrix;
import android.support.v4.view.ViewCompat;
import com.dlten.lib.Common;
import com.dlten.lib.STD;

public class CImgObj {
    private static int DRAW_MODE = 1;
    public static final int MODE_BITMAP = 0;
    public static final int MODE_TEXTURE = 1;
    private CImage m_imgParent;
    private int m_nAlpha;
    private int m_nAnchor;
    private int m_nFilterColor;
    private CRect m_rect = new CRect();
    private int m_rotate;
    private CRect m_rtTemp = new CRect();
    private float m_scaleX;
    private float m_scaleY;
    private boolean m_visible;

    public static void SetMode(int mode) {
        DRAW_MODE = mode;
    }

    public CImgObj() {
        init();
    }

    public CImgObj(String strAsset) {
        init();
        load(strAsset);
    }

    public CImgObj(String strAsset, int nPer) {
        init();
        load(strAsset, nPer);
    }

    private void init() {
        this.m_imgParent = null;
        setAnchor(17);
        setFilterColor(ViewCompat.MEASURED_SIZE_MASK);
        setAlpha(255);
        rotateTo(0);
        setScaleX(1.0f);
        setScaleY(1.0f);
        setVisible(true);
    }

    public void load(String strAsset) {
        CImage img;
        if (strAsset == null) {
            STD.logout("CImgObj.load()\tFile name is null!");
            return;
        }
        CRect rect = new CRect(this.m_rect.left, this.m_rect.top, this.m_rect.width, this.m_rect.height);
        int nAnchor = this.m_nAnchor;
        int nFilterColor = this.m_nFilterColor;
        int nAlpha = this.m_nAlpha;
        float scaleX = this.m_scaleX;
        float scaleY = this.m_scaleY;
        int rotate = this.m_rotate;
        boolean visible = this.m_visible;
        boolean bReload = false;
        if (this.m_imgParent != null) {
            bReload = true;
        }
        switch (DRAW_MODE) {
            case 0:
                img = new CImageBitmap();
                break;
            default:
                img = new CImageTexture();
                break;
        }
        img.load(strAsset, 1, 1);
        setParent(img);
        if (bReload) {
            this.m_rect.left = rect.left;
            this.m_rect.top = rect.top;
            this.m_rect.width = rect.width;
            this.m_rect.height = rect.height;
            this.m_nAnchor = nAnchor;
            this.m_nFilterColor = nFilterColor;
            this.m_nAlpha = nAlpha;
            this.m_scaleX = scaleX;
            this.m_scaleY = scaleY;
            this.m_rotate = rotate;
            this.m_visible = visible;
        }
    }

    public void load(String strAsset, int nPer) {
        CImage img;
        if (strAsset == null) {
            STD.logout("CImgObj.load()\tFile name is null!");
            return;
        }
        CRect rect = new CRect(this.m_rect.left, this.m_rect.top, this.m_rect.width, this.m_rect.height);
        int nAnchor = this.m_nAnchor;
        int nFilterColor = this.m_nFilterColor;
        int nAlpha = this.m_nAlpha;
        float scaleX = this.m_scaleX;
        float scaleY = this.m_scaleY;
        int rotate = this.m_rotate;
        boolean visible = this.m_visible;
        boolean bReload = false;
        if (this.m_imgParent != null) {
            bReload = true;
        }
        switch (DRAW_MODE) {
            case 0:
                img = new CImageBitmap();
                break;
            default:
                img = new CImageTexture();
                break;
        }
        img.load(strAsset, nPer);
        setParent(img);
        if (bReload) {
            this.m_rect.left = rect.left;
            this.m_rect.top = rect.top;
            this.m_rect.width = rect.width;
            this.m_rect.height = rect.height;
            this.m_nAnchor = nAnchor;
            this.m_nFilterColor = nFilterColor;
            this.m_nAlpha = nAlpha;
            this.m_scaleX = scaleX;
            this.m_scaleY = scaleY;
            this.m_rotate = rotate;
            this.m_visible = visible;
        }
    }

    public void unload() {
        if (this.m_imgParent != null) {
            this.m_imgParent.unload();
            this.m_imgParent = null;
            this.m_rect.SetRectEmpty();
        }
    }

    public void setParent(CImage imgParent) {
        setParent(imgParent, 0);
    }

    public void setParent(CImage imgParent, int nIndex) {
        this.m_imgParent = imgParent;
        this.m_rect.SetRectEmpty();
        updateSize();
    }

    private void updateSize() {
        if (this.m_imgParent != null) {
            this.m_rect.width = Common.res2code((float) this.m_imgParent.getCWidth());
            this.m_rect.height = Common.res2code((float) this.m_imgParent.getCHeight());
        }
    }

    private void updateWidth() {
        if (this.m_imgParent != null) {
            this.m_rect.width = Common.res2code((float) this.m_imgParent.getCWidth());
        }
    }

    private void updateHeight() {
        if (this.m_imgParent != null) {
            this.m_rect.height = Common.res2code((float) this.m_imgParent.getCHeight());
        }
    }

    public void draw() {
        drawImpl(this.m_rect.left, this.m_rect.top);
    }

    public void draw(CPoint pt) {
        if (pt != null) {
            draw(pt.x, pt.y);
        }
    }

    public void draw(float x, float y) {
        drawImpl(x, y);
    }

    public void draw(CPoint pt, int nPer) {
        if (pt != null) {
            draw(pt.x, pt.y, nPer);
        }
    }

    public void draw(float x, float y, int nPer) {
        drawImpl(x, y, nPer);
    }

    private void drawImpl(float fPosX, float fPosY) {
        if (this.m_imgParent != null && getVisible()) {
            float fPosX2 = Common.code2screen(fPosX);
            float fPosY2 = Common.code2screen(fPosY);
            float f = fPosX2;
            float f2 = fPosY2;
            this.m_imgParent.drawImage(f, f2, this.m_scaleX, this.m_scaleY, this.m_nAlpha, calcMatrix(fPosX2, fPosY2));
        }
    }

    private void drawImpl(float fPosX, float fPosY, int nPer) {
        if (this.m_imgParent != null && getVisible()) {
            CImageBitmap img = (CImageBitmap) this.m_imgParent;
            img.changeWithPer(nPer);
            CImageBitmap cImageBitmap = img;
            float fPosX2 = Common.code2screen(fPosX);
            float fPosY2 = Common.code2screen(fPosY);
            float offset = (float) (this.m_imgParent.getHeight() - cImageBitmap.getHeight());
            float f = fPosX2;
            cImageBitmap.drawImage(f, fPosY2 + offset, this.m_scaleX, this.m_scaleY, this.m_nAlpha, calcMatrix(fPosX2, fPosY2));
        }
    }

    private Matrix calcMatrix(float fPosX, float fPosY) {
        float[] offset = CImage.getLeftTopPos((float) this.m_imgParent.getCWidth(), (float) this.m_imgParent.getCHeight(), this.m_nAnchor);
        Matrix matrix = new Matrix();
        if (this.m_rotate != 0) {
            matrix.postRotate((float) this.m_rotate, -offset[0], -offset[1]);
        }
        if (!(this.m_scaleX == 1.0f && this.m_scaleY == 1.0f)) {
            matrix.postScale(this.m_scaleX, this.m_scaleY, -offset[0], -offset[1]);
        }
        matrix.postTranslate(offset[0] + fPosX, offset[1] + fPosY);
        return matrix;
    }

    public void moveTo(float x, float y) {
        this.m_rect.left = x;
        this.m_rect.top = y;
    }

    public void moveTo(CPoint pt) {
        moveTo(pt.x, pt.y);
    }

    public void move(float dx, float dy) {
        this.m_rect.OffsetRect(dx, dy);
    }

    public CPoint getPos() {
        return getRect().TopLeft();
    }

    public CPoint getCenterPos() {
        return getRect().CenterPoint();
    }

    public CRect getRect() {
        this.m_rtTemp.CopyRect(this.m_rect);
        float[] posOffset = CImage.getLeftTopPos(this.m_rtTemp.width, this.m_rtTemp.height, this.m_nAnchor);
        this.m_rtTemp.OffsetRect(posOffset[0], posOffset[1]);
        return this.m_rtTemp;
    }

    public void setAnchor(int nAnchor) {
        this.m_nAnchor = nAnchor;
    }

    public int getAnchor() {
        return this.m_nAnchor;
    }

    public void setFilterColor(int nColor) {
        this.m_nFilterColor = nColor;
    }

    public int getFilterColor() {
        return this.m_nFilterColor;
    }

    public void setAlpha(int nAlpha) {
        this.m_nAlpha = nAlpha;
    }

    public int getAlpha() {
        return this.m_nAlpha;
    }

    public void rotate(int dr) {
        this.m_rotate += dr;
        checkImageChange();
    }

    public void rotateTo(int r) {
        this.m_rotate = r;
        checkImageChange();
    }

    public void setScale(float scaleX, float scaleY) {
        setScaleX(scaleX);
        setScaleY(scaleY);
    }

    public void setScaleX(float scaleX) {
        updateWidth();
        this.m_scaleX = scaleX;
        this.m_rect.width *= scaleX;
        checkImageChange();
    }

    public void setScaleY(float scaleY) {
        updateHeight();
        this.m_scaleY = scaleY;
        this.m_rect.height *= scaleY;
        checkImageChange();
    }

    public float getScaleX() {
        return this.m_scaleX;
    }

    public float getScaleY() {
        return this.m_scaleY;
    }

    public void setSize(float sizeX, float sizeY) {
        setSizeX(sizeX);
        setSizeY(sizeY);
    }

    public void setSizeX(float sizeX) {
        updateWidth();
        this.m_scaleX = sizeX / this.m_rect.width;
        this.m_rect.width = sizeX;
        checkImageChange();
    }

    public void setSizeY(float sizeY) {
        updateHeight();
        this.m_scaleY = sizeY / this.m_rect.height;
        this.m_rect.height = sizeY;
        checkImageChange();
    }

    public float getSizeX() {
        return this.m_rect.width;
    }

    public float getSizeY() {
        return this.m_rect.height;
    }

    public float getWidth() {
        return (float) this.m_imgParent.getWidth();
    }

    public float getHeight() {
        return (float) this.m_imgParent.getHeight();
    }

    private boolean checkImageChange() {
        if (this.m_scaleX == 1.0f && this.m_scaleY == 1.0f && this.m_rotate == 0) {
            return false;
        }
        return true;
    }

    public boolean getVisible() {
        return this.m_visible;
    }

    public void setVisible(boolean visible) {
        this.m_visible = visible;
    }
}
