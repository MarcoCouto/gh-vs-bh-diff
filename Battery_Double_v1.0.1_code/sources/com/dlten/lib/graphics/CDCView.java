package com.dlten.lib.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.dlten.lib.Common;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;

public abstract class CDCView extends SurfaceView {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_HCENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_VCENTER = 2;
    public static final int ANCHOR_V_FILTER = 7;
    public static int CODE_HEIGHT = 0;
    public static int CODE_WIDTH = 0;
    public static int HALF_HEIGHT = 0;
    public static int HALF_WIDTH = 0;
    public static int RES_HEIGHT = 0;
    public static int RES_WIDTH = 0;
    private static final char RETURN_CHAR = '\n';
    public static int SC_HEIGHT;
    public static int SC_WIDTH;
    private static int m_bufHeight = 1024;
    private static int m_bufWidth = 1024;
    public static int m_drawHeight;
    public static int m_drawWidth;
    public static float m_fScaleCode;
    public static float m_fScaleRes;
    protected static int m_nDblOffsetX;
    protected static int m_nDblOffsetY;
    protected static int m_nWndOffsetX;
    protected static int m_nWndOffsetY;
    protected int FONT_BASELINE;
    protected int FONT_H;
    protected Bitmap m_bmpDblBuffer;
    protected Canvas m_canvas;
    protected SurfaceHolder m_holder;
    Matrix m_matrix = new Matrix();
    protected int m_nFontSize = 36;

    public static void setCodeWidth(int nCodeWidth) {
        CODE_WIDTH = nCodeWidth;
    }

    public static int getCodeWidth() {
        return CODE_WIDTH;
    }

    public static void setCodeHeight(int nCodeHeight) {
        CODE_HEIGHT = nCodeHeight;
    }

    public static int getCodeHeight() {
        return CODE_HEIGHT;
    }

    public static void setResWidth(int nResWidth) {
        RES_WIDTH = nResWidth;
    }

    public static int getResWidth() {
        return RES_WIDTH;
    }

    public static void setResHeight(int nResHeight) {
        RES_HEIGHT = nResHeight;
    }

    public static int getResHeight() {
        return RES_HEIGHT;
    }

    public static int getDrawWidth() {
        return m_drawWidth;
    }

    public static int getDrawHeight() {
        return m_drawHeight;
    }

    public static void setScaleCode(float fScale) {
        m_fScaleCode = fScale;
    }

    public static float getScaleCode() {
        return m_fScaleCode;
    }

    public static void setScaleRes(float fScale) {
        m_fScaleRes = fScale;
    }

    public static float getScaleRes() {
        return m_fScaleRes;
    }

    public static int getDblOffsetX() {
        return m_nDblOffsetX;
    }

    public static int getDblOffsetY() {
        return m_nDblOffsetY;
    }

    public int getCodePosX(int drawPosX) {
        return (int) Common.screen2code((float) (drawPosX - m_nDblOffsetX));
    }

    public int getCodePosY(int drawPosY) {
        return (int) Common.screen2code((float) (drawPosY - m_nDblOffsetY));
    }

    public static int getBufWidth() {
        return m_bufWidth;
    }

    public static int getBufHeight() {
        return m_bufHeight;
    }

    public CDCView(Context context) {
        super(context);
        init();
    }

    public CDCView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.m_holder = getHolder();
        this.m_holder.setType(2);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void setEvent(Runnable r) {
    }

    public synchronized void update(CEventWnd pWnd) {
    }

    public SurfaceHolder getSurfaceHolder() {
        return this.m_holder;
    }

    public void prepareDC() {
        int nBufWidth = getWidth();
        int nBufHeight = getHeight();
        SC_WIDTH = nBufWidth;
        SC_HEIGHT = nBufHeight;
        m_drawWidth = (int) Common.res2screen((float) RES_WIDTH);
        m_drawHeight = (int) Common.res2screen((float) RES_HEIGHT);
        HALF_WIDTH = RES_WIDTH / 2;
        HALF_HEIGHT = RES_HEIGHT / 2;
        m_nWndOffsetX = 0;
        m_nWndOffsetY = 0;
        m_nDblOffsetX = (nBufWidth - m_drawWidth) / 2;
        m_nDblOffsetY = (nBufHeight - m_drawHeight) / 2;
        STD.logout("init(" + nBufWidth + ", " + nBufHeight + ")");
        STD.logout("Drawing(" + RES_WIDTH + ", " + RES_HEIGHT + ")");
        this.m_matrix.postScale(m_fScaleRes, m_fScaleRes);
        createDoubleBuffer();
    }

    public void releaseDC() {
    }

    private void createDoubleBuffer() {
        m_bufWidth = 1024;
        m_bufHeight = 1024;
        this.m_bmpDblBuffer = Bitmap.createBitmap(m_bufWidth, m_bufHeight, Config.RGB_565);
        this.m_canvas = new Canvas(this.m_bmpDblBuffer);
    }

    public void setARGB(int nAlpha, int nR, int nG, int nB) {
    }

    public void setAlpha(int nAlpha) {
    }

    /* access modifiers changed from: protected */
    public void fillRect(int nLeft, int nTop, int nWidth, int nHeight) {
    }

    public void setRotate(float fDegress, int x, int y) {
    }

    public void drawString(String str, float nPosX, float nPosY, int nAnchor) {
    }

    /* access modifiers changed from: protected */
    public void setFont() {
    }

    public void drawImage(Bitmap img, Matrix matrix) {
    }

    public void drawImage(int textureID, float x, float y, float w, float h, float scaleX, float scaleY, CRect rectTexture) {
    }

    /* access modifiers changed from: protected */
    public final int MAKE_RGB(int nR, int nG, int nB) {
        return ((nR & 255) << 16) | ((nG & 255) << 8) | ((nB & 255) << 0);
    }

    public void clear() {
        clear(0);
    }

    public final void clear(int nColor) {
        setColor(nColor);
        fillRect(0, 0, m_bufWidth, m_bufHeight);
    }

    public final void setColor(int nColor) {
        setColor((nColor >>> 16) & 255, (nColor >>> 8) & 255, nColor & 255);
    }

    public final void setColor(int nR, int nG, int nB) {
        setARGB(255, nR, nG, nB);
    }

    /* access modifiers changed from: protected */
    public final void fillRect(int[] rect) {
        fillRect(rect[0], rect[1], rect[2], rect[3]);
    }

    public final void setRotate(float fDegress) {
        setRotate(fDegress, 0, 0);
    }

    public final void drawString(String str, int nPosX, int nPosY) {
        drawString(str, (float) nPosX, (float) nPosY, 17);
    }

    public int drawRectString(String str, int x, int y, int width, int height, int cur, int maxlines, int align) {
        int drawX;
        int option;
        int offsetY;
        if (str == null) {
            return -1;
        }
        int length = str.length();
        if (length == 0 || cur > length - 1) {
            return -1;
        }
        if (maxlines < 0) {
            maxlines = 50;
        }
        int nLineHieght = this.FONT_H + 6;
        char[] chArray = str.toCharArray();
        int offsetY2 = 0;
        int nLines = 0;
        String[] strArray = new String[maxlines];
        while (nLines < maxlines && cur < length) {
            int nCharCount = 0;
            while (true) {
                if (chArray[cur] != ' ' && chArray[cur] != 10) {
                    break;
                }
                cur++;
            }
            if (this.FONT_H + offsetY2 <= height) {
                while (true) {
                    if (cur + nCharCount >= length) {
                        break;
                    }
                    nCharCount++;
                    if (0 <= width || chArray[(cur + nCharCount) - 1] == 12290 || chArray[(cur + nCharCount) - 1] == 65289 || chArray[(cur + nCharCount) - 1] == '?') {
                        if (chArray[(cur + nCharCount) - 1] == 10) {
                            nCharCount--;
                            break;
                        }
                    } else {
                        nCharCount--;
                        break;
                    }
                }
                strArray[nLines] = new String(chArray, cur, nCharCount);
                if (nCharCount == 0) {
                    break;
                }
                offsetY2 += nLineHieght;
                cur += nCharCount;
                nLines++;
            } else {
                break;
            }
        }
        if ((align & 112) == 64) {
            drawX = x + width;
            option = 65;
        } else if ((align & 112) == 32) {
            drawX = x + (width / 2);
            option = 33;
        } else {
            drawX = x;
            option = 17;
        }
        if ((align & 7) == 1) {
            offsetY = 2;
        } else if ((align & 7) == 2) {
            offsetY = (height - offsetY2) / 2;
        } else {
            offsetY = height - offsetY2;
        }
        for (int i = 0; i < nLines; i++) {
            drawString(strArray[i], (float) drawX, (float) (y + offsetY), option);
            offsetY += nLineHieght;
        }
        if (cur > length - 1) {
            return -1;
        }
        return cur;
    }

    public final int setFontSize(int nSize) {
        int nSize2 = (int) Common.code2screen((float) nSize);
        int nOldSize = this.m_nFontSize;
        if (this.m_nFontSize != nSize2) {
            this.m_nFontSize = nSize2;
            setFont();
        }
        return (int) Common.screen2code((float) nOldSize);
    }

    public final int getFontSize() {
        return (int) Common.screen2code((float) this.m_nFontSize);
    }
}
