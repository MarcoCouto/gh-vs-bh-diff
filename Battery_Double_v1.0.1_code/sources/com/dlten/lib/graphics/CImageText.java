package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.opengl.GLES10;
import android.opengl.GLUtils;
import com.dlten.lib.STD;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class CImageText extends CImage {
    private int SC_RES_HEIGHT;
    private int SC_RES_WIDTH;
    private FloatBuffer mColorBuffer0;
    private FloatBuffer mFVertexBuffer;
    private FloatBuffer mFVertexBuffer0;
    private ShortBuffer mIndexBuffer;
    private FloatBuffer mTexBuffer;
    private ByteBuffer m_cbb0;
    private ByteBuffer m_ibb;
    private CRect m_rectTexture;
    private String m_str;
    private ByteBuffer m_tbb;
    private int m_textureID;
    private ByteBuffer m_vbb;
    private ByteBuffer m_vbb0;

    public CImageText() {
        this.m_textureID = -1;
        this.m_rectTexture = new CRect();
        this.m_str = "";
        this.m_vbb = ByteBuffer.allocateDirect(48);
        this.m_tbb = ByteBuffer.allocateDirect(32);
        this.m_ibb = ByteBuffer.allocateDirect(8);
        this.m_vbb0 = ByteBuffer.allocateDirect(48);
        this.m_cbb0 = ByteBuffer.allocateDirect(64);
        this.m_textureID = -1;
        this.m_str = "";
        CDCView dCView = CImage.getDCView();
        this.SC_RES_WIDTH = CDCView.getResWidth();
        this.SC_RES_HEIGHT = CDCView.getResHeight();
    }

    public CImageText(String text) {
        this.m_textureID = -1;
        this.m_rectTexture = new CRect();
        this.m_str = "";
        this.m_vbb = ByteBuffer.allocateDirect(48);
        this.m_tbb = ByteBuffer.allocateDirect(32);
        this.m_ibb = ByteBuffer.allocateDirect(8);
        this.m_vbb0 = ByteBuffer.allocateDirect(48);
        this.m_cbb0 = ByteBuffer.allocateDirect(64);
        String fontname = Typeface.DEFAULT.toString();
        createTexture(text, calculateTextSize(text, fontname, 36.0f), 33, fontname, 36.0f);
    }

    public CImageText(String text, String fontname, float fontSize) {
        this.m_textureID = -1;
        this.m_rectTexture = new CRect();
        this.m_str = "";
        this.m_vbb = ByteBuffer.allocateDirect(48);
        this.m_tbb = ByteBuffer.allocateDirect(32);
        this.m_ibb = ByteBuffer.allocateDirect(8);
        this.m_vbb0 = ByteBuffer.allocateDirect(48);
        this.m_cbb0 = ByteBuffer.allocateDirect(64);
        createTexture(text, calculateTextSize(text, fontname, fontSize), 33, fontname, fontSize);
    }

    public void setText(String text) {
        String fontname = Typeface.DEFAULT.toString();
        createTexture(text, calculateTextSize(text, fontname, 36.0f), 33, fontname, 36.0f);
    }

    public void createTexture(String text, CSize dimensions, int alignment, String fontname, float fontSize) {
        Typeface typeface = Typeface.create(fontname, 0);
        Paint textPaint = new Paint();
        textPaint.setTypeface(typeface);
        textPaint.setTextSize(fontSize);
        int ascent = (int) Math.ceil((double) (-textPaint.ascent()));
        int textWidth = (int) Math.ceil((double) textPaint.measureText(text));
        int textHeight = ascent + ((int) Math.ceil((double) textPaint.descent()));
        int width = (int) dimensions.w;
        if (!(width == 1 || ((width - 1) & width) == 0)) {
            int i = 1;
            while (i < width) {
                i *= 2;
            }
            width = i;
        }
        int height = (int) dimensions.h;
        if (!(height == 1 || ((height - 1) & height) == 0)) {
            int i2 = 1;
            while (i2 < height) {
                i2 *= 2;
            }
            height = i2;
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ALPHA_8);
        Canvas canvas = new Canvas(bitmap);
        bitmap.eraseColor(0);
        int centerOffsetHeight = (((int) dimensions.h) - textHeight) / 2;
        int centerOffsetWidth = (((int) dimensions.w) - textWidth) / 2;
        switch (alignment) {
            case 16:
                centerOffsetWidth = 0;
                break;
            case 64:
                centerOffsetWidth = ((int) dimensions.w) - textWidth;
                break;
        }
        canvas.drawText(text, (float) centerOffsetWidth, (float) (ascent + centerOffsetHeight), textPaint);
        loadResource(bitmap);
    }

    private CSize calculateTextSize(String text, String fontname, float fontSize) {
        Typeface typeface = Typeface.create(fontname, 0);
        Paint textPaint = new Paint();
        textPaint.setTypeface(typeface);
        textPaint.setTextSize(fontSize);
        return CSize.make((float) ((int) Math.ceil((double) textPaint.measureText(text))), (float) (((int) Math.ceil((double) (-textPaint.ascent()))) + ((int) Math.ceil((double) textPaint.descent()))));
    }

    public CRect getTextureRect() {
        return this.m_rectTexture;
    }

    public int getTextureID() {
        return this.m_textureID;
    }

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap img) {
        Config config;
        unloadResource();
        super.loadResource(img);
        int width = adjustValue(this.m_totalWidth);
        int height = adjustValue(this.m_totalHeight);
        if (!(this.m_totalWidth == width && this.m_totalHeight == height)) {
            try {
                if (img.hasAlpha()) {
                    config = Config.ARGB_8888;
                } else {
                    config = Config.RGB_565;
                }
                Bitmap bmpTexture = Bitmap.createBitmap(width, height, config);
                new Canvas(bmpTexture).drawBitmap(img, 0.0f, 0.0f, null);
                img.recycle();
                img = bmpTexture;
            } catch (Exception e) {
                int n = 1 + 1;
                return -1;
            }
        }
        STD.ASSERT(this.m_textureID == -1);
        int textureID = createTexture(img);
        this.m_textureID = textureID;
        initTexture();
        return textureID;
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
        if (this.m_textureID != -1) {
            deleteTexture(this.m_textureID);
            this.m_textureID = -1;
        }
        super.unloadResource();
    }

    public void draw(float fPosX, float fPosY) {
        drawImage(this.m_textureID, fPosX, fPosY, (float) this.m_totalWidth, (float) this.m_totalHeight, 255);
    }

    private int adjustValue(int val) {
        if (val <= 2) {
            return 2;
        }
        if (val <= 4) {
            return 4;
        }
        if (val <= 8) {
            return 8;
        }
        if (val <= 16) {
            return 16;
        }
        if (val <= 32) {
            return 32;
        }
        if (val <= 64) {
            return 64;
        }
        if (val <= 128) {
            return 128;
        }
        if (val <= 256) {
            return 256;
        }
        if (val <= 512) {
            return 512;
        }
        if (val <= 1024) {
            return 1024;
        }
        return val;
    }

    private int createTexture(Bitmap img) {
        int width = img.getWidth();
        int height = img.getHeight();
        this.m_rectTexture.left = 0.0f;
        this.m_rectTexture.top = 0.0f;
        this.m_rectTexture.width = ((float) this.m_totalWidth) / ((float) width);
        this.m_rectTexture.height = ((float) this.m_totalHeight) / ((float) height);
        GLES10.glEnable(3553);
        int[] textures = new int[1];
        GLES10.glGenTextures(1, textures, 0);
        GLES10.glBindTexture(3553, textures[0]);
        GLES10.glTexParameterf(3553, 10241, 9729.0f);
        GLES10.glTexParameterf(3553, 10240, 9729.0f);
        GLES10.glTexParameterf(3553, 10242, 33071.0f);
        GLES10.glTexParameterf(3553, 10243, 33071.0f);
        GLUtils.texImage2D(3553, 0, img, 0);
        return textures[0];
    }

    private void deleteTexture(int textureID) {
        GLES10.glDeleteTextures(1, new int[]{textureID}, 0);
    }

    private void initTexture() {
        this.m_vbb.order(ByteOrder.nativeOrder());
        this.m_tbb.order(ByteOrder.nativeOrder());
        this.m_ibb.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer = this.m_vbb.asFloatBuffer();
        this.mTexBuffer = this.m_tbb.asFloatBuffer();
        this.mIndexBuffer = this.m_ibb.asShortBuffer();
        float left = this.m_rectTexture.left;
        float top = this.m_rectTexture.top;
        float right = this.m_rectTexture.Right();
        float bottom = this.m_rectTexture.Bottom();
        this.mTexBuffer.put(left);
        this.mTexBuffer.put(top);
        this.mTexBuffer.put(left);
        this.mTexBuffer.put(bottom);
        this.mTexBuffer.put(right);
        this.mTexBuffer.put(bottom);
        this.mTexBuffer.put(right);
        this.mTexBuffer.put(top);
        this.mTexBuffer.position(0);
        this.mIndexBuffer.put(0);
        this.mIndexBuffer.put(1);
        this.mIndexBuffer.put(2);
        this.mIndexBuffer.put(3);
        this.mIndexBuffer.position(0);
        this.m_vbb0.order(ByteOrder.nativeOrder());
        this.m_cbb0.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer0 = this.m_vbb0.asFloatBuffer();
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.0f);
        this.mFVertexBuffer0.position(0);
    }

    public void drawImage(int textureID, float x, float y, float w, float h, int nAlpha) {
        GLES10.glBindTexture(3553, textureID);
        float left = (x / ((float) this.SC_RES_WIDTH)) - 0.5f;
        float top = ((y / ((float) this.SC_RES_HEIGHT)) - 0.5f) * -1.0f;
        float right = ((x + w) / ((float) this.SC_RES_WIDTH)) - 0.5f;
        float bottom = (((y + h) / ((float) this.SC_RES_HEIGHT)) - 0.5f) * -1.0f;
        this.mFVertexBuffer.put(left);
        this.mFVertexBuffer.put(top);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(left);
        this.mFVertexBuffer.put(bottom);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(right);
        this.mFVertexBuffer.put(bottom);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.put(right);
        this.mFVertexBuffer.put(top);
        this.mFVertexBuffer.put(0.0f);
        this.mFVertexBuffer.position(0);
        GLES10.glEnable(3553);
        GLES10.glEnableClientState(32884);
        GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer);
        GLES10.glEnableClientState(32888);
        GLES10.glTexCoordPointer(2, 5126, 0, this.mTexBuffer);
        GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
        if (nAlpha < 255) {
            float fAlpha = ((float) nAlpha) / 255.0f;
            this.mColorBuffer0 = this.m_cbb0.asFloatBuffer();
            for (int i = 0; i < 4; i++) {
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(0.0f);
                this.mColorBuffer0.put(1.0f - fAlpha);
            }
            this.mColorBuffer0.position(0);
            GLES10.glEnableClientState(32886);
            GLES10.glColorPointer(4, 5126, 0, this.mColorBuffer0);
            GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
            GLES10.glDisableClientState(32886);
        }
        GLES10.glDisableClientState(32888);
        GLES10.glDisableClientState(32884);
    }
}
