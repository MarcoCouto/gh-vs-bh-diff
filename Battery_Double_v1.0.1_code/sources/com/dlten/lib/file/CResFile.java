package com.dlten.lib.file;

import android.content.Context;
import com.dlten.lib.STD;
import java.io.InputStream;

public class CResFile {
    private static Context m_context;

    public static void Initialize(Context context) {
        m_context = context;
    }

    public static Context getAppContext() {
        return m_context;
    }

    public static byte[] load(String strAsset) {
        try {
            InputStream is = m_context.getAssets().open(strAsset);
            if (is == null) {
                return null;
            }
            byte[] byResult = new byte[is.available()];
            is.read(byResult);
            is.close();
            return byResult;
        } catch (Exception e) {
            STD.logout("loadBitmap failed : name=" + strAsset);
            STD.printStackTrace(e);
            return null;
        }
    }

    public static byte[] load(int nResID) {
        try {
            InputStream is = m_context.getResources().openRawResource(nResID);
            if (is == null) {
                return null;
            }
            byte[] byResult = new byte[is.available()];
            is.read(byResult);
            is.close();
            return byResult;
        } catch (Exception e) {
            STD.logout("loadBitmap failed : id=" + nResID);
            STD.printStackTrace(e);
            return null;
        }
    }
}
