package com.dlten.lib;

import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.dlten.lib.frmWork.CAnimation;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Random;

public class STD {
    private static Random m_rnd;

    public static void MEMSET(boolean[] bArrData, boolean bVal) {
        if (bArrData != null) {
            for (int i = 0; i < bArrData.length; i++) {
                bArrData[i] = bVal;
            }
        }
    }

    public static void MEMSET(byte[] byArrData, byte byVal) {
        if (byArrData != null) {
            for (int i = 0; i < byArrData.length; i++) {
                byArrData[i] = byVal;
            }
        }
    }

    public static void MEMSET(char[] chArrData, char chVal) {
        if (chArrData != null) {
            for (int i = 0; i < chArrData.length; i++) {
                chArrData[i] = chVal;
            }
        }
    }

    public static void MEMSET(short[] shArrData, short shVal) {
        if (shArrData != null) {
            for (int i = 0; i < shArrData.length; i++) {
                shArrData[i] = shVal;
            }
        }
    }

    public static void MEMSET(int[] nArrData, int nVal) {
        if (nArrData != null) {
            for (int i = 0; i < nArrData.length; i++) {
                nArrData[i] = nVal;
            }
        }
    }

    public static void MEMSET(byte[][] byArrData, byte byVal) {
        if (byArrData != null) {
            for (int i = 0; i < byArrData.length; i++) {
                if (byArrData[i] != null) {
                    for (int j = 0; j < byArrData[i].length; j++) {
                        byArrData[i][j] = byVal;
                    }
                }
            }
        }
    }

    public static void MEMSET(char[][] chArrData, char chVal) {
        if (chArrData != null) {
            for (int i = 0; i < chArrData.length; i++) {
                if (chArrData[i] != null) {
                    for (int j = 0; j < chArrData[i].length; j++) {
                        chArrData[i][j] = chVal;
                    }
                }
            }
        }
    }

    public static void MEMSET(short[][] shArrData, short shVal) {
        if (shArrData != null) {
            for (int i = 0; i < shArrData.length; i++) {
                if (shArrData[i] != null) {
                    for (int j = 0; j < shArrData[i].length; j++) {
                        shArrData[i][j] = shVal;
                    }
                }
            }
        }
    }

    public static void MEMSET(int[][] nArrData, int nVal) {
        if (nArrData != null) {
            for (int i = 0; i < nArrData.length; i++) {
                if (nArrData[i] != null) {
                    for (int j = 0; j < nArrData[i].length; j++) {
                        nArrData[i][j] = nVal;
                    }
                }
            }
        }
    }

    public static void MEMCPY(byte[] nArrDest, int dstIndex, byte[] nArrSrc, int srcIndex, int nCount) {
        if (dstIndex < nArrDest.length && srcIndex < nArrSrc.length) {
            int nLen = Math.min(Math.min(nArrDest.length - dstIndex, nArrSrc.length - srcIndex), nCount);
            for (int i = 0; i < nLen; i++) {
                nArrDest[dstIndex + i] = nArrSrc[srcIndex + i];
            }
        }
    }

    public static void MEMCPY(byte[] byArrDest, byte[] byArrSrc, int nCount) {
        int nLen = Math.min(Math.min(byArrDest.length, byArrSrc.length), nCount);
        for (int i = 0; i < nLen; i++) {
            byArrDest[i] = byArrSrc[i];
        }
    }

    public static void MEMCPY(short[] shArrDest, short[] shArrSrc, int nCount) {
        int nLen = Math.min(Math.min(shArrDest.length, shArrSrc.length), nCount);
        for (int i = 0; i < nLen; i++) {
            shArrDest[i] = shArrSrc[i];
        }
    }

    public static void MEMCPY(int[] nArrDest, int[] nArrSrc, int nCount) {
        int nLen = Math.min(Math.min(nArrDest.length, nArrSrc.length), nCount);
        for (int i = 0; i < nLen; i++) {
            nArrDest[i] = nArrSrc[i];
        }
    }

    public static void MEMCPY(boolean[] bArrDest, boolean[] bArrSrc) {
        int nLen = Math.min(bArrDest.length, bArrSrc.length);
        for (int i = 0; i < nLen; i++) {
            bArrDest[i] = bArrSrc[i];
        }
    }

    public static void MEMCPY(byte[] byArrDest, byte[] byArrSrc) {
        int nLen = Math.min(byArrDest.length, byArrSrc.length);
        for (int i = 0; i < nLen; i++) {
            byArrDest[i] = byArrSrc[i];
        }
    }

    public static void MEMCPY(char[] chArrDest, char[] chArrSrc) {
        int nLen = Math.min(chArrDest.length, chArrSrc.length);
        for (int i = 0; i < nLen; i++) {
            chArrDest[i] = chArrSrc[i];
        }
    }

    public static void MEMCPY(short[] shArrDest, short[] shArrSrc) {
        int nLen = Math.min(shArrDest.length, shArrSrc.length);
        for (int i = 0; i < nLen; i++) {
            shArrDest[i] = shArrSrc[i];
        }
    }

    public static void MEMCPY(int[] nArrDest, int[] nArrSrc) {
        int nLen = Math.min(nArrDest.length, nArrSrc.length);
        for (int i = 0; i < nLen; i++) {
            nArrDest[i] = nArrSrc[i];
        }
    }

    public static void MEMCPY(byte[][] byArrDest, byte[][] byArrSrc) {
        int i = 0;
        while (i < byArrDest.length && i < byArrSrc.length) {
            int j = 0;
            while (j < byArrDest[i].length && j < byArrSrc[i].length) {
                byArrDest[i][j] = byArrSrc[i][j];
                j++;
            }
            i++;
        }
    }

    public static void MEMCPY(char[][] chArrDest, char[][] chArrSrc) {
        int i = 0;
        while (i < chArrDest.length && i < chArrSrc.length) {
            int j = 0;
            while (j < chArrDest[i].length && j < chArrSrc[i].length) {
                chArrDest[i][j] = chArrSrc[i][j];
                j++;
            }
            i++;
        }
    }

    public static void MEMCPY(short[][] shArrDest, short[][] shArrSrc) {
        int i = 0;
        while (i < shArrDest.length && i < shArrSrc.length) {
            int j = 0;
            while (j < shArrDest[i].length && j < shArrSrc[i].length) {
                shArrDest[i][j] = shArrSrc[i][j];
                j++;
            }
            i++;
        }
    }

    public static void MEMCPY(int[][] nArrDest, int[][] nArrSrc) {
        int i = 0;
        while (i < nArrDest.length && i < nArrSrc.length) {
            int j = 0;
            while (j < nArrDest[i].length && j < nArrSrc[i].length) {
                nArrDest[i][j] = nArrSrc[i][j];
                j++;
            }
            i++;
        }
    }

    public static int STRCMP(String strSrc, String strDest) {
        if (strSrc.equals(strDest)) {
            return 0;
        }
        return strSrc.compareTo(strDest);
    }

    public static byte[] str2ByteArray(String str, int nLen) {
        byte[] byStr;
        byte[] byRet = new byte[nLen];
        if (str != null) {
            byStr = str.getBytes();
        } else {
            byStr = new byte[0];
        }
        for (int i = 0; i < byRet.length; i++) {
            if (i >= byStr.length) {
                byRet[i] = 0;
            } else {
                byRet[i] = byStr[i];
            }
        }
        return byRet;
    }

    public static String dis2Str(DataInputStream dis, int nLen) {
        String str = "";
        byte[] byStr = new byte[nLen];
        MEMSET(byStr, 0);
        try {
            dis.read(byStr);
        } catch (IOException e) {
        }
        int nStrLen = 0;
        int i = 0;
        while (i < byStr.length && byStr[i] != 0) {
            nStrLen++;
            i++;
        }
        return new String(byStr, 0, nStrLen);
    }

    public final String[] splitString(String strSrc, char chBound) {
        String[] strResult = new String[200];
        String strBuf = "";
        int nCurIndex = 0;
        for (int i = 0; i < strSrc.length(); i++) {
            char chCur = strSrc.charAt(i);
            if (chCur == chBound) {
                strResult[nCurIndex] = strBuf;
                strBuf = "";
                nCurIndex++;
            } else if (chCur != 13) {
                strBuf = strBuf + chCur;
            }
        }
        if (strBuf != "") {
            strResult[nCurIndex] = strBuf;
            nCurIndex++;
        }
        String[] strs = new String[nCurIndex];
        for (int i2 = 0; i2 < nCurIndex; i2++) {
            strs[i2] = strResult[i2];
        }
        return strs;
    }

    public static String getnString(int nValue, int n) {
        String strResult;
        String strResult2;
        if (nValue >= 0) {
            strResult = "" + nValue;
        } else {
            strResult = "";
        }
        for (int i = strResult2.length(); i < n; i++) {
            if (nValue >= 0) {
                strResult2 = "0" + strResult2;
            } else {
                strResult2 = "-" + strResult2;
            }
        }
        return strResult2;
    }

    public static String getCommaString(int nValue) {
        String str = "";
        if (nValue == 0) {
            return "0";
        }
        while (nValue > 0) {
            if (str == "") {
                str = "" + getModStr(nValue, 1000);
            } else {
                str = getModStr(nValue, 1000) + "," + str;
            }
            nValue /= 1000;
        }
        return str;
    }

    public static String getModStr(int nMaster, int nSlave) {
        String strResult = "";
        if (nMaster < nSlave) {
            return "" + (nMaster % nSlave);
        }
        for (int nBuf = nSlave; nBuf > 1; nBuf /= 10) {
            strResult = strResult + ((nMaster % nBuf) / (nBuf / 10));
        }
        return strResult;
    }

    public static String getPercentString(int nValue) {
        if (nValue >= 10000) {
            return "100.00%";
        }
        String str = (nValue % 10) + "%";
        int nValue2 = nValue / 10;
        return (nValue2 / 10) + ("." + (nValue2 % 10) + str);
    }

    public static long Bytes2Long(byte[] src, int offset) {
        return (long) (((src[offset + 7] & 255) << 56) | ((src[offset + 6] & 255) << 48) | ((src[offset + 5] & 255) << 40) | ((src[offset + 4] & 255) << 32) | ((src[offset + 3] & 255) << 24) | ((src[offset + 2] & 255) << 16) | ((src[offset + 1] & 255) << 8) | (src[offset] & 255));
    }

    public static int Bytes2Int(byte[] src, int offset) {
        return ((src[offset + 3] & 255) << 24) | ((src[offset + 2] & 255) << 16) | ((src[offset + 1] & 255) << 8) | (src[offset] & 255);
    }

    public static short Bytes2Short(byte[] src, int offset) {
        return (short) (((src[offset + 1] & 255) << 8) | (src[offset] & 255));
    }

    public static void Long2Bytes(byte[] dest, int offset, long val) {
        dest[offset + 7] = (byte) ((int) ((val >> 56) & 255));
        dest[offset + 6] = (byte) ((int) ((val >> 48) & 255));
        dest[offset + 5] = (byte) ((int) ((val >> 40) & 255));
        dest[offset + 4] = (byte) ((int) ((val >> 32) & 255));
        dest[offset + 3] = (byte) ((int) ((val >> 24) & 255));
        dest[offset + 2] = (byte) ((int) ((val >> 16) & 255));
        dest[offset + 1] = (byte) ((int) ((val >> 8) & 255));
        dest[offset + 0] = (byte) ((int) ((val >> 0) & 255));
    }

    public static void Int2Bytes(byte[] dest, int offset, int value) {
        dest[offset + 3] = (byte) ((-16777216 & value) >> 24);
        dest[offset + 2] = (byte) ((16711680 & value) >> 16);
        dest[offset + 1] = (byte) ((65280 & value) >> 8);
        dest[offset + 0] = (byte) (value & 255);
    }

    public static void Short2Bytes(byte[] dest, int offset, short value) {
        dest[offset + 1] = (byte) ((65280 & value) >> 8);
        dest[offset] = (byte) (value & 255);
    }

    public static byte[] getByteArrayFromLong(long val) {
        byte[] byData = new byte[8];
        for (int i = 0; i < 8; i++) {
            byData[i] = (byte) ((int) ((val >>> (i * 8)) & 255));
        }
        return byData;
    }

    public static long getLongFromByteArray(byte[] byData) {
        long val = 0;
        int nLen = byData.length;
        if (nLen > 8) {
            nLen = 8;
        }
        for (int i = 0; i < nLen; i++) {
            val |= ((long) (byData[i] & 255)) << (i * 8);
        }
        return val;
    }

    public static short MAKEWORD(byte a, byte b) {
        return (short) (((b & 255) << 8) | (a & 255));
    }

    public static long MAKELONG(short a, short b) {
        return (long) (((b & CAnimation.IDA_NONE) << 16) | (65535 & a));
    }

    public static short LOWORD(int l) {
        return (short) (65535 & l);
    }

    public static short HIWORD(int l) {
        return (short) ((l >>> 16) & SupportMenu.USER_MASK);
    }

    public static byte LOBYTE(int w) {
        return (byte) (w & 255);
    }

    public static byte HIBYTE(int w) {
        return (byte) (((65535 & w) >>> 8) & 255);
    }

    public static byte MAX(byte a, byte b) {
        return a >= b ? a : b;
    }

    public static short MAX(short a, short b) {
        return a >= b ? a : b;
    }

    public static int MAX(int a, int b) {
        return a >= b ? a : b;
    }

    public static long MAX(long a, long b) {
        return a >= b ? a : b;
    }

    public static float MAX(float a, float b) {
        return a >= b ? a : b;
    }

    public static byte MIN(byte a, byte b) {
        return a <= b ? a : b;
    }

    public static short MIN(short a, short b) {
        return a <= b ? a : b;
    }

    public static int MIN(int a, int b) {
        return a <= b ? a : b;
    }

    public static long MIN(long a, long b) {
        return a <= b ? a : b;
    }

    public static float MIN(float a, float b) {
        return a <= b ? a : b;
    }

    public static final void sleep(long lTime) {
        try {
            Thread.sleep(lTime);
        } catch (InterruptedException e) {
        }
    }

    public static long GetTickCount() {
        return System.currentTimeMillis();
    }

    public static void DeviceInfo() {
        Log.e("BOARD", Build.BOARD);
        Log.e("BRAND", Build.BRAND);
        Log.e("DEVICE", Build.DEVICE);
        Log.e("DISPLAY", Build.DISPLAY);
        Log.e("FINGERPRINT", Build.FINGERPRINT);
        Log.e("HOST", Build.HOST);
        Log.e("ID", Build.ID);
        Log.e("MODEL", Build.MODEL);
        Log.e("PRODUCT", Build.PRODUCT);
        Log.e("TAGS", Build.TAGS);
        Log.e("TYPE", Build.TYPE);
        Log.e("USER", Build.USER);
    }

    public static final String getHeapState() {
        return "" + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) + "/" + (Runtime.getRuntime().totalMemory() / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
    }

    public static void logHeap() {
        long max = Runtime.getRuntime().maxMemory();
        long total = Runtime.getRuntime().totalMemory();
        long free = Runtime.getRuntime().freeMemory();
        logout("Heap - ( m : " + Long.toString(max) + "," + " t : " + Long.toString(total) + "," + " f : " + Long.toString(free) + "," + " u : " + Long.toString(total - free) + ")");
        System.gc();
    }

    public static void logout(String strLog) {
        Log.d("slib", strLog);
    }

    public static void printStackTrace(Exception e) {
        if (e != null) {
            StackTraceElement[] stack = e.getStackTrace();
            if (stack != null) {
                logout("Exception =" + e.toString());
                for (int i = 0; i < stack.length; i++) {
                    if (stack[i] != null) {
                        logout(stack[i].toString());
                    }
                }
                logout("Exception Stack End");
            }
        }
    }

    public static void initRand() {
        m_rnd = new Random();
        m_rnd.setSeed(System.currentTimeMillis());
    }

    public static int rand(int nMax) {
        return Math.abs(m_rnd.nextInt()) % nMax;
    }

    public static void ASSERT(boolean value) {
        if (!value) {
            try {
                throw new Exception("ASSERT");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static int[] SETAEERECT(int x, int y, int width, int height) {
        return new int[]{x, y, width, height};
    }
}
