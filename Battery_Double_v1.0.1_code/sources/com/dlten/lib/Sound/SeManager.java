package com.dlten.lib.Sound;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;

public class SeManager {
    private static SeManager _instance;
    private AudioManager mAudioManager;
    private Context mContext;
    private HashMap<Integer, Integer> mSoundMap;
    private SoundPool mSoundPool;
    private boolean m_bEnable = false;
    private float speed = 1.0f;

    private SeManager() {
        _instance = this;
    }

    public static synchronized SeManager getInstance() {
        SeManager seManager;
        synchronized (SeManager.class) {
            if (_instance == null) {
                _instance = new SeManager();
            }
            seManager = _instance;
        }
        return seManager;
    }

    public void init(Context theContext) {
        this.mContext = theContext;
        this.mSoundPool = new SoundPool(4, 3, 0);
        this.mSoundMap = new HashMap<>();
        this.mAudioManager = (AudioManager) this.mContext.getSystemService("audio");
        this.m_bEnable = true;
    }

    public void destroy() {
        this.mSoundPool.release();
        this.mSoundPool = null;
        this.mSoundMap.clear();
        this.mAudioManager.unloadSoundEffects();
        _instance = null;
    }

    public void addSound(int index, int SoundID) {
        this.mSoundMap.put(Integer.valueOf(index), Integer.valueOf(this.mSoundPool.load(this.mContext, SoundID, 1)));
    }

    public void delSound(int index) {
        this.mSoundPool.unload(((Integer) this.mSoundMap.get(Integer.valueOf(index))).intValue());
        this.mSoundMap.remove(Integer.valueOf(index));
    }

    public void play(int index) {
        if (this.m_bEnable) {
            float streamVolume = ((float) this.mAudioManager.getStreamVolume(3)) / ((float) this.mAudioManager.getStreamMaxVolume(3));
            this.mSoundPool.play(((Integer) this.mSoundMap.get(Integer.valueOf(index))).intValue(), streamVolume, streamVolume, 1, 0, this.speed);
        }
    }

    public void playLooped(int index) {
        if (this.m_bEnable) {
            float streamVolume = ((float) this.mAudioManager.getStreamVolume(3)) / ((float) this.mAudioManager.getStreamMaxVolume(3));
            this.mSoundPool.play(((Integer) this.mSoundMap.get(Integer.valueOf(index))).intValue(), streamVolume, streamVolume, 1, -1, this.speed);
        }
    }

    public void stop(int index) {
        this.mSoundPool.stop(((Integer) this.mSoundMap.get(Integer.valueOf(index))).intValue());
    }

    public void stopAll() {
        for (Integer intValue : this.mSoundMap.values()) {
            stop(intValue.intValue());
        }
    }

    public void setEnable(boolean enable) {
        this.m_bEnable = enable;
        if (!this.m_bEnable) {
            stopAll();
        }
    }
}
