package com.dlten.lib.Sound;

import android.content.Context;

public class SoundManager {
    protected BgmManager mBgmManager;
    protected SeManager mSeManager;
    private boolean m_bEnable;

    protected SoundManager() {
        this.mBgmManager = null;
        this.mSeManager = null;
        this.m_bEnable = false;
        this.mBgmManager = BgmManager.getInstance();
        this.mSeManager = SeManager.getInstance();
    }

    public void init(Context context) {
        this.mBgmManager.init(context);
        this.mSeManager.init(context);
        loadSounds();
        setEnable(true);
    }

    public void destroy() {
        this.mBgmManager.destroy();
        this.mSeManager.destroy();
        this.mBgmManager = null;
        this.mSeManager = null;
    }

    /* access modifiers changed from: protected */
    public void loadSounds() {
    }

    public void playBGM(int index) {
        if (this.m_bEnable) {
            this.mBgmManager.playBGM(index);
        }
    }

    public void playBGM(int index, boolean bLoop) {
        if (this.m_bEnable) {
            this.mBgmManager.playBGM(index, bLoop);
        }
    }

    public void stopBGM() {
        if (this.m_bEnable) {
            this.mBgmManager.stopBGM();
        }
    }

    public void pauseBGM() {
        this.mBgmManager.pauseBGM();
    }

    public void resumeBGM() {
        this.mBgmManager.resumeBGM();
    }

    public void playSE(int index) {
        if (this.m_bEnable) {
            this.mSeManager.play(index);
        }
    }

    public void stopSE(int index) {
        this.mSeManager.stop(index);
    }

    public void stopSE() {
        this.mSeManager.stopAll();
    }

    public void setEnable(boolean enable) {
        this.m_bEnable = enable;
        this.mBgmManager.setEnable(this.m_bEnable);
        this.mSeManager.setEnable(this.m_bEnable);
    }
}
