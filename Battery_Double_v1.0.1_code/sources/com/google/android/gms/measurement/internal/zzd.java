package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri.Builder;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzt;
import com.google.android.gms.common.zzc;
import com.google.android.gms.internal.zzqf;
import com.google.android.gms.measurement.internal.zzl.zza;

public class zzd extends zzz {
    static final String ail = String.valueOf(zzc.GOOGLE_PLAY_SERVICES_VERSION_CODE / 1000).replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");
    private Boolean zzczb;

    zzd(zzx zzx) {
        super(zzx);
    }

    @Nullable
    private Boolean zzli(@Size(min = 1) String str) {
        zzab.zzhr(str);
        try {
            PackageManager packageManager = getContext().getPackageManager();
            if (packageManager == null) {
                zzbsd().zzbsv().log("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getContext().getPackageName(), 128);
            if (applicationInfo == null) {
                zzbsd().zzbsv().log("Failed to load metadata: ApplicationInfo is null");
                return null;
            } else if (applicationInfo.metaData == null) {
                zzbsd().zzbsv().log("Failed to load metadata: Metadata bundle is null");
                return null;
            } else if (applicationInfo.metaData.containsKey(str)) {
                return Boolean.valueOf(applicationInfo.metaData.getBoolean(str));
            } else {
                return null;
            }
        } catch (NameNotFoundException e) {
            zzbsd().zzbsv().zzj("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public long zza(String str, zza<Long> zza) {
        if (str == null) {
            return ((Long) zza.get()).longValue();
        }
        String zzaw = zzbsa().zzaw(str, zza.getKey());
        if (TextUtils.isEmpty(zzaw)) {
            return ((Long) zza.get()).longValue();
        }
        try {
            return ((Long) zza.get(Long.valueOf(Long.valueOf(zzaw).longValue()))).longValue();
        } catch (NumberFormatException e) {
            return ((Long) zza.get()).longValue();
        }
    }

    public boolean zzabc() {
        return false;
    }

    public boolean zzabd() {
        if (this.zzczb == null) {
            synchronized (this) {
                if (this.zzczb == null) {
                    ApplicationInfo applicationInfo = getContext().getApplicationInfo();
                    String zzawa = zzt.zzawa();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.zzczb = Boolean.valueOf(str != null && str.equals(zzawa));
                    }
                    if (this.zzczb == null) {
                        this.zzczb = Boolean.TRUE;
                        zzbsd().zzbsv().log("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.zzczb.booleanValue();
    }

    /* access modifiers changed from: 0000 */
    public long zzabx() {
        return ((Long) zzl.ajl.get()).longValue();
    }

    public String zzacc() {
        return "google_app_measurement.db";
    }

    public String zzacd() {
        return "google_app_measurement2.db";
    }

    public long zzaci() {
        return Math.max(0, ((Long) zzl.aiP.get()).longValue());
    }

    public String zzap(String str, String str2) {
        Builder builder = new Builder();
        Builder encodedAuthority = builder.scheme((String) zzl.aiR.get()).encodedAuthority((String) zzl.aiS.get());
        String str3 = "config/app/";
        String valueOf = String.valueOf(str);
        encodedAuthority.path(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3)).appendQueryParameter("app_instance_id", str2).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", String.valueOf(zzbpz()));
        return builder.build().toString();
    }

    public boolean zzaqp() {
        return zzqf.zzaqp();
    }

    public int zzb(String str, zza<Integer> zza) {
        if (str == null) {
            return ((Integer) zza.get()).intValue();
        }
        String zzaw = zzbsa().zzaw(str, zza.getKey());
        if (TextUtils.isEmpty(zzaw)) {
            return ((Integer) zza.get()).intValue();
        }
        try {
            return ((Integer) zza.get(Integer.valueOf(Integer.valueOf(zzaw).intValue()))).intValue();
        } catch (NumberFormatException e) {
            return ((Integer) zza.get()).intValue();
        }
    }

    public long zzbpz() {
        return 9452;
    }

    /* access modifiers changed from: 0000 */
    public String zzbql() {
        return (String) zzl.aiN.get();
    }

    public int zzbqm() {
        return 25;
    }

    public int zzbqn() {
        return 32;
    }

    public int zzbqo() {
        return 24;
    }

    /* access modifiers changed from: 0000 */
    public int zzbqp() {
        return 24;
    }

    /* access modifiers changed from: 0000 */
    public int zzbqq() {
        return 36;
    }

    /* access modifiers changed from: 0000 */
    public int zzbqr() {
        return 256;
    }

    public int zzbqs() {
        return 36;
    }

    public int zzbqt() {
        return 2048;
    }

    /* access modifiers changed from: 0000 */
    public int zzbqu() {
        return 500;
    }

    public long zzbqv() {
        return (long) ((Integer) zzl.aiX.get()).intValue();
    }

    public long zzbqw() {
        return (long) ((Integer) zzl.aiY.get()).intValue();
    }

    public long zzbqx() {
        return 1000;
    }

    /* access modifiers changed from: 0000 */
    public int zzbqy() {
        return 25;
    }

    /* access modifiers changed from: 0000 */
    public int zzbqz() {
        return 50;
    }

    /* access modifiers changed from: 0000 */
    public long zzbra() {
        return 3600000;
    }

    /* access modifiers changed from: 0000 */
    public long zzbrb() {
        return 60000;
    }

    /* access modifiers changed from: 0000 */
    public long zzbrc() {
        return 61000;
    }

    public boolean zzbrd() {
        if (zzabc()) {
            return false;
        }
        Boolean zzli = zzli("firebase_analytics_collection_deactivated");
        return zzli != null && !zzli.booleanValue();
    }

    public Boolean zzbre() {
        if (zzabc()) {
            return null;
        }
        return zzli("firebase_analytics_collection_enabled");
    }

    public long zzbrf() {
        return ((Long) zzl.ajj.get()).longValue();
    }

    public long zzbrg() {
        return ((Long) zzl.ajf.get()).longValue();
    }

    public long zzbrh() {
        return 1000;
    }

    public int zzbri() {
        return Math.max(0, ((Integer) zzl.aiV.get()).intValue());
    }

    public int zzbrj() {
        return Math.max(1, ((Integer) zzl.aiW.get()).intValue());
    }

    public String zzbrk() {
        return (String) zzl.ajb.get();
    }

    public long zzbrl() {
        return ((Long) zzl.aiQ.get()).longValue();
    }

    public long zzbrm() {
        return Math.max(0, ((Long) zzl.ajc.get()).longValue());
    }

    public long zzbrn() {
        return Math.max(0, ((Long) zzl.aje.get()).longValue());
    }

    public long zzbro() {
        return ((Long) zzl.ajd.get()).longValue();
    }

    public long zzbrp() {
        return Math.max(0, ((Long) zzl.ajg.get()).longValue());
    }

    public long zzbrq() {
        return Math.max(0, ((Long) zzl.ajh.get()).longValue());
    }

    public int zzbrr() {
        return Math.min(20, Math.max(0, ((Integer) zzl.aji.get()).intValue()));
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    public int zzlf(@Size(min = 1) String str) {
        return zzb(str, zzl.aiZ);
    }

    /* access modifiers changed from: 0000 */
    public long zzlg(String str) {
        return zza(str, zzl.aiO);
    }

    /* access modifiers changed from: 0000 */
    public int zzlh(String str) {
        return zzb(str, zzl.ajk);
    }

    public int zzlj(String str) {
        return zzb(str, zzl.aiT);
    }

    public int zzlk(String str) {
        return Math.max(0, zzb(str, zzl.aiU));
    }

    public int zzll(String str) {
        return Math.max(0, Math.min(1000000, zzb(str, zzl.aja)));
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
