package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.measurement.AppMeasurement;

public class zzp extends zzaa {
    private final long ahR = zzbsf().zzbpz();
    private final char ajp;
    private final zza ajq;
    private final zza ajr;
    private final zza ajs;
    private final zza ajt;
    private final zza aju;
    private final zza ajv;
    private final zza ajw;
    private final zza ajx;
    private final zza ajy;
    private final String zc = zzbsf().zzbql();

    public class zza {
        private final boolean ajB;
        private final boolean ajC;
        private final int mPriority;

        zza(int i, boolean z, boolean z2) {
            this.mPriority = i;
            this.ajB = z;
            this.ajC = z2;
        }

        public void log(String str) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, null, null, null);
        }

        public void zzd(String str, Object obj, Object obj2, Object obj3) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, obj, obj2, obj3);
        }

        public void zze(String str, Object obj, Object obj2) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, obj, obj2, null);
        }

        public void zzj(String str, Object obj) {
            zzp.this.zza(this.mPriority, this.ajB, this.ajC, str, obj, null, null);
        }
    }

    zzp(zzx zzx) {
        super(zzx);
        if (zzbsf().zzabd()) {
            this.ajp = zzbsf().zzabc() ? 'P' : 'C';
        } else {
            this.ajp = zzbsf().zzabc() ? 'p' : 'c';
        }
        this.ajq = new zza(6, false, false);
        this.ajr = new zza(6, true, false);
        this.ajs = new zza(6, false, true);
        this.ajt = new zza(5, false, false);
        this.aju = new zza(5, true, false);
        this.ajv = new zza(5, false, true);
        this.ajw = new zza(4, false, false);
        this.ajx = new zza(3, false, false);
        this.ajy = new zza(2, false, false);
    }

    static String zza(boolean z, String str, Object obj, Object obj2, Object obj3) {
        if (str == null) {
            str = "";
        }
        String zzc = zzc(z, obj);
        String zzc2 = zzc(z, obj2);
        String zzc3 = zzc(z, obj3);
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(zzc)) {
            sb.append(str2);
            sb.append(zzc);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(zzc2)) {
            sb.append(str2);
            sb.append(zzc2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(zzc3)) {
            sb.append(str2);
            sb.append(zzc3);
        }
        return sb.toString();
    }

    static String zzc(boolean z, Object obj) {
        StackTraceElement stackTraceElement;
        if (obj == null) {
            return "";
        }
        Object obj2 = obj instanceof Integer ? Long.valueOf((long) ((Integer) obj).intValue()) : obj;
        if (obj2 instanceof Long) {
            if (!z) {
                return String.valueOf(obj2);
            }
            if (Math.abs(((Long) obj2).longValue()) < 100) {
                return String.valueOf(obj2);
            }
            String str = String.valueOf(obj2).charAt(0) == '-' ? "-" : "";
            String valueOf = String.valueOf(Math.abs(((Long) obj2).longValue()));
            return new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(str).length()).append(str).append(Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1)))).append("...").append(str).append(Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d)).toString();
        } else if (obj2 instanceof Boolean) {
            return String.valueOf(obj2);
        } else {
            if (!(obj2 instanceof Throwable)) {
                return z ? "-" : String.valueOf(obj2);
            }
            Throwable th = (Throwable) obj2;
            StringBuilder sb = new StringBuilder(z ? th.getClass().getName() : th.toString());
            String zzlw = zzlw(AppMeasurement.class.getCanonicalName());
            String zzlw2 = zzlw(zzx.class.getCanonicalName());
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                stackTraceElement = stackTrace[i];
                if (!stackTraceElement.isNativeMethod()) {
                    String className = stackTraceElement.getClassName();
                    if (className != null) {
                        String zzlw3 = zzlw(className);
                        if (zzlw3.equals(zzlw) || zzlw3.equals(zzlw2)) {
                            sb.append(": ");
                            sb.append(stackTraceElement);
                        }
                    } else {
                        continue;
                    }
                }
                i++;
            }
            sb.append(": ");
            sb.append(stackTraceElement);
            return sb.toString();
        }
    }

    private static String zzlw(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf != -1 ? str.substring(0, lastIndexOf) : str;
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    /* access modifiers changed from: protected */
    public void zza(int i, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && zzaz(i)) {
            zzo(i, zza(false, str, obj, obj2, obj3));
        }
        if (!z2 && i >= 5) {
            zzb(i, str, obj, obj2, obj3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzaz(int i) {
        return Log.isLoggable(this.zc, i);
    }

    public void zzb(int i, String str, Object obj, Object obj2, Object obj3) {
        zzab.zzy(str);
        zzw zzbtq = this.ahD.zzbtq();
        if (zzbtq == null) {
            zzo(6, "Scheduler not set. Not logging error/warn.");
        } else if (!zzbtq.isInitialized()) {
            zzo(6, "Scheduler not initialized. Not logging error/warn.");
        } else if (zzbtq.zzbul()) {
            zzo(6, "Scheduler shutdown. Not logging error/warn.");
        } else {
            if (i < 0) {
                i = 0;
            }
            if (i >= "01VDIWEA?".length()) {
                i = "01VDIWEA?".length() - 1;
            }
            String valueOf = String.valueOf("1");
            char charAt = "01VDIWEA?".charAt(i);
            char c = this.ajp;
            long j = this.ahR;
            String valueOf2 = String.valueOf(zza(true, str, obj, obj2, obj3));
            final String sb = new StringBuilder(String.valueOf(valueOf).length() + 23 + String.valueOf(valueOf2).length()).append(valueOf).append(charAt).append(c).append(j).append(":").append(valueOf2).toString();
            if (sb.length() > 1024) {
                sb = str.substring(0, 1024);
            }
            zzbtq.zzm(new Runnable() {
                public void run() {
                    zzt zzbse = zzp.this.ahD.zzbse();
                    if (!zzbse.isInitialized() || zzbse.zzbul()) {
                        zzp.this.zzo(6, "Persisted config not initialized . Not logging error/warn.");
                    } else {
                        zzbse.ajX.zzev(sb);
                    }
                }
            });
        }
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    public zza zzbsv() {
        return this.ajq;
    }

    public zza zzbsw() {
        return this.ajr;
    }

    public zza zzbsx() {
        return this.ajt;
    }

    public zza zzbsy() {
        return this.aju;
    }

    public zza zzbsz() {
        return this.ajv;
    }

    public zza zzbta() {
        return this.ajw;
    }

    public zza zzbtb() {
        return this.ajx;
    }

    public zza zzbtc() {
        return this.ajy;
    }

    public String zzbtd() {
        Pair<String, Long> zzadv = zzbse().ajX.zzadv();
        if (zzadv == null || zzadv == zzt.ajW) {
            return null;
        }
        String valueOf = String.valueOf(String.valueOf(zzadv.second));
        String str = (String) zzadv.first;
        return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length()).append(valueOf).append(":").append(str).toString();
    }

    /* access modifiers changed from: protected */
    public void zzo(int i, String str) {
        Log.println(i, this.zc, str);
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
