package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.internal.zzab;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Locale;
import org.altbeacon.beacon.BeaconManager;

class zzt extends zzaa {
    static final Pair<String, Long> ajW = new Pair<>("", Long.valueOf(0));
    /* access modifiers changed from: private */
    public SharedPreferences ah;
    public final zzc ajX = new zzc("health_monitor", zzbsf().zzaci());
    public final zzb ajY = new zzb("last_upload", 0);
    public final zzb ajZ = new zzb("last_upload_attempt", 0);
    public final zzb aka = new zzb("backoff", 0);
    public final zzb akb = new zzb("last_delete_stale", 0);
    public final zzb akc = new zzb("midnight_offset", 0);
    private String akd;
    private boolean ake;
    private long akf;
    private SecureRandom akg;
    public final zzb akh = new zzb("time_before_start", BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD);
    public final zzb aki = new zzb("session_timeout", 1800000);
    public final zza akj = new zza("start_new_session", true);
    public final zzb akk = new zzb("last_pause_time", 0);
    public final zzb akl = new zzb("time_active", 0);
    public boolean akm;

    public final class zza {
        private final boolean akn;
        private boolean ako;
        private boolean rN;
        private final String zzaxp;

        public zza(String str, boolean z) {
            zzab.zzhr(str);
            this.zzaxp = str;
            this.akn = z;
        }

        @WorkerThread
        private void zzbtm() {
            if (!this.ako) {
                this.ako = true;
                this.rN = zzt.this.ah.getBoolean(this.zzaxp, this.akn);
            }
        }

        @WorkerThread
        public boolean get() {
            zzbtm();
            return this.rN;
        }

        @WorkerThread
        public void set(boolean z) {
            Editor edit = zzt.this.ah.edit();
            edit.putBoolean(this.zzaxp, z);
            edit.apply();
            this.rN = z;
        }
    }

    public final class zzb {
        private boolean ako;
        private final long akq;
        private final String zzaxp;
        private long zzcve;

        public zzb(String str, long j) {
            zzab.zzhr(str);
            this.zzaxp = str;
            this.akq = j;
        }

        @WorkerThread
        private void zzbtm() {
            if (!this.ako) {
                this.ako = true;
                this.zzcve = zzt.this.ah.getLong(this.zzaxp, this.akq);
            }
        }

        @WorkerThread
        public long get() {
            zzbtm();
            return this.zzcve;
        }

        @WorkerThread
        public void set(long j) {
            Editor edit = zzt.this.ah.edit();
            edit.putLong(this.zzaxp, j);
            edit.apply();
            this.zzcve = j;
        }
    }

    public final class zzc {
        final String akr;
        private final String aks;
        private final String akt;
        private final long al;

        private zzc(String str, long j) {
            zzab.zzhr(str);
            zzab.zzbo(j > 0);
            this.akr = String.valueOf(str).concat(":start");
            this.aks = String.valueOf(str).concat(":count");
            this.akt = String.valueOf(str).concat(":value");
            this.al = j;
        }

        @WorkerThread
        private void zzadt() {
            zzt.this.zzwu();
            long currentTimeMillis = zzt.this.zzyw().currentTimeMillis();
            Editor edit = zzt.this.ah.edit();
            edit.remove(this.aks);
            edit.remove(this.akt);
            edit.putLong(this.akr, currentTimeMillis);
            edit.apply();
        }

        @WorkerThread
        private long zzadu() {
            zzt.this.zzwu();
            long zzadw = zzadw();
            if (zzadw != 0) {
                return Math.abs(zzadw - zzt.this.zzyw().currentTimeMillis());
            }
            zzadt();
            return 0;
        }

        @WorkerThread
        private long zzadw() {
            return zzt.this.zzbth().getLong(this.akr, 0);
        }

        @WorkerThread
        public Pair<String, Long> zzadv() {
            zzt.this.zzwu();
            long zzadu = zzadu();
            if (zzadu < this.al) {
                return null;
            }
            if (zzadu > this.al * 2) {
                zzadt();
                return null;
            }
            String string = zzt.this.zzbth().getString(this.akt, null);
            long j = zzt.this.zzbth().getLong(this.aks, 0);
            zzadt();
            return (string == null || j <= 0) ? zzt.ajW : new Pair<>(string, Long.valueOf(j));
        }

        @WorkerThread
        public void zzev(String str) {
            zzh(str, 1);
        }

        @WorkerThread
        public void zzh(String str, long j) {
            zzt.this.zzwu();
            if (zzadw() == 0) {
                zzadt();
            }
            if (str == null) {
                str = "";
            }
            long j2 = zzt.this.ah.getLong(this.aks, 0);
            if (j2 <= 0) {
                Editor edit = zzt.this.ah.edit();
                edit.putString(this.akt, str);
                edit.putLong(this.aks, j);
                edit.apply();
                return;
            }
            boolean z = (zzt.this.zzbte().nextLong() & Long.MAX_VALUE) < (Long.MAX_VALUE / (j2 + j)) * j;
            Editor edit2 = zzt.this.ah.edit();
            if (z) {
                edit2.putString(this.akt, str);
            }
            edit2.putLong(this.aks, j2 + j);
            edit2.apply();
        }
    }

    zzt(zzx zzx) {
        super(zzx);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public SecureRandom zzbte() {
        zzwu();
        if (this.akg == null) {
            this.akg = new SecureRandom();
        }
        return this.akg;
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public SharedPreferences zzbth() {
        zzwu();
        zzzg();
        return this.ah;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void setMeasurementEnabled(boolean z) {
        zzwu();
        zzbsd().zzbtc().zzj("Setting measurementEnabled", Boolean.valueOf(z));
        Editor edit = zzbth().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public String zzbpu() {
        zzwu();
        return com.google.firebase.iid.zzc.zzcwr().getId();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public String zzbtf() {
        byte[] bArr = new byte[16];
        zzbte().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new Object[]{new BigInteger(1, bArr)});
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public long zzbtg() {
        zzzg();
        zzwu();
        long j = this.akc.get();
        if (j != 0) {
            return j;
        }
        long nextInt = (long) (zzbte().nextInt(86400000) + 1);
        this.akc.set(nextInt);
        return nextInt;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public String zzbti() {
        zzwu();
        return zzbth().getString("gmp_app_id", null);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public Boolean zzbtj() {
        zzwu();
        if (!zzbth().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(zzbth().getBoolean("use_service", false));
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzbtk() {
        boolean z = true;
        zzwu();
        zzbsd().zzbtc().log("Clearing collection preferences.");
        boolean contains = zzbth().contains("measurement_enabled");
        if (contains) {
            z = zzcc(true);
        }
        Editor edit = zzbth().edit();
        edit.clear();
        edit.apply();
        if (contains) {
            setMeasurementEnabled(z);
        }
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public String zzbtl() {
        zzwu();
        String string = zzbth().getString("previous_os_version", null);
        String zzbso = zzbrw().zzbso();
        if (!TextUtils.isEmpty(zzbso) && !zzbso.equals(string)) {
            Editor edit = zzbth().edit();
            edit.putString("previous_os_version", zzbso);
            edit.apply();
        }
        return string;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzcb(boolean z) {
        zzwu();
        zzbsd().zzbtc().zzj("Setting useService", Boolean.valueOf(z));
        Editor edit = zzbth().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public boolean zzcc(boolean z) {
        zzwu();
        return zzbth().getBoolean("measurement_enabled", z);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    @NonNull
    public Pair<String, Boolean> zzlx(String str) {
        zzwu();
        long elapsedRealtime = zzyw().elapsedRealtime();
        if (this.akd != null && elapsedRealtime < this.akf) {
            return new Pair<>(this.akd, Boolean.valueOf(this.ake));
        }
        this.akf = elapsedRealtime + zzbsf().zzlg(str);
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(getContext());
            this.akd = advertisingIdInfo.getId();
            if (this.akd == null) {
                this.akd = "";
            }
            this.ake = advertisingIdInfo.isLimitAdTrackingEnabled();
        } catch (Throwable th) {
            zzbsd().zzbtb().zzj("Unable to get advertising id", th);
            this.akd = "";
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair<>(this.akd, Boolean.valueOf(this.ake));
    }

    /* access modifiers changed from: 0000 */
    public String zzly(String str) {
        String str2 = (String) zzlx(str).first;
        MessageDigest zzfa = zzal.zzfa("MD5");
        if (zzfa == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, zzfa.digest(str2.getBytes()))});
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzlz(String str) {
        zzwu();
        Editor edit = zzbth().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
        this.ah = getContext().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.akm = this.ah.getBoolean("has_been_opened", false);
        if (!this.akm) {
            Editor edit = this.ah.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
    }
}
