package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzuf.zza;
import com.google.android.gms.internal.zzuf.zzb;
import com.google.android.gms.internal.zzuf.zze;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzuh.zzf;
import com.google.android.gms.internal.zzuh.zzg;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurement.zzd;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

class zzc extends zzaa {
    zzc(zzx zzx) {
        super(zzx);
    }

    private Boolean zza(zzb zzb, zzuh.zzb zzb2, long j) {
        com.google.android.gms.internal.zzuf.zzc[] zzcArr;
        com.google.android.gms.internal.zzuh.zzc[] zzcArr2;
        com.google.android.gms.internal.zzuf.zzc[] zzcArr3;
        if (zzb.amH != null) {
            Boolean zzbk = new zzs(zzb.amH).zzbk(j);
            if (zzbk == null) {
                return null;
            }
            if (!zzbk.booleanValue()) {
                return Boolean.valueOf(false);
            }
        }
        HashSet hashSet = new HashSet();
        for (com.google.android.gms.internal.zzuf.zzc zzc : zzb.amF) {
            if (TextUtils.isEmpty(zzc.amM)) {
                zzbsd().zzbsx().zzj("null or empty param name in filter. event", zzb2.name);
                return null;
            }
            hashSet.add(zzc.amM);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (com.google.android.gms.internal.zzuh.zzc zzc2 : zzb2.ann) {
            if (hashSet.contains(zzc2.name)) {
                if (zzc2.anr != null) {
                    arrayMap.put(zzc2.name, zzc2.anr);
                } else if (zzc2.amw != null) {
                    arrayMap.put(zzc2.name, zzc2.amw);
                } else if (zzc2.zD != null) {
                    arrayMap.put(zzc2.name, zzc2.zD);
                } else {
                    zzbsd().zzbsx().zze("Unknown value for param. event, param", zzb2.name, zzc2.name);
                    return null;
                }
            }
        }
        for (com.google.android.gms.internal.zzuf.zzc zzc3 : zzb.amF) {
            boolean equals = Boolean.TRUE.equals(zzc3.amL);
            String str = zzc3.amM;
            if (TextUtils.isEmpty(str)) {
                zzbsd().zzbsx().zzj("Event has empty param name. event", zzb2.name);
                return null;
            }
            Object obj = arrayMap.get(str);
            if (obj instanceof Long) {
                if (zzc3.amK == null) {
                    zzbsd().zzbsx().zze("No number filter for long param. event, param", zzb2.name, str);
                    return null;
                }
                Boolean zzbk2 = new zzs(zzc3.amK).zzbk(((Long) obj).longValue());
                if (zzbk2 == null) {
                    return null;
                }
                if ((!zzbk2.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj instanceof Double) {
                if (zzc3.amK == null) {
                    zzbsd().zzbsx().zze("No number filter for double param. event, param", zzb2.name, str);
                    return null;
                }
                Boolean zzj = new zzs(zzc3.amK).zzj(((Double) obj).doubleValue());
                if (zzj == null) {
                    return null;
                }
                if ((!zzj.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj instanceof String) {
                if (zzc3.amJ == null) {
                    zzbsd().zzbsx().zze("No string filter for String param. event, param", zzb2.name, str);
                    return null;
                }
                Boolean zzmi = new zzag(zzc3.amJ).zzmi((String) obj);
                if (zzmi == null) {
                    return null;
                }
                if ((!zzmi.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj == null) {
                zzbsd().zzbtc().zze("Missing param for filter. event, param", zzb2.name, str);
                return Boolean.valueOf(false);
            } else {
                zzbsd().zzbsx().zze("Unknown param type. event, param", zzb2.name, str);
                return null;
            }
        }
        return Boolean.valueOf(true);
    }

    private Boolean zza(zze zze, zzg zzg) {
        Boolean bool = null;
        com.google.android.gms.internal.zzuf.zzc zzc = zze.amU;
        if (zzc == null) {
            zzbsd().zzbsx().zzj("Missing property filter. property", zzg.name);
            return bool;
        }
        boolean equals = Boolean.TRUE.equals(zzc.amL);
        if (zzg.anr != null) {
            if (zzc.amK != null) {
                return zza(new zzs(zzc.amK).zzbk(zzg.anr.longValue()), equals);
            }
            zzbsd().zzbsx().zzj("No number filter for long property. property", zzg.name);
            return bool;
        } else if (zzg.amw != null) {
            if (zzc.amK != null) {
                return zza(new zzs(zzc.amK).zzj(zzg.amw.doubleValue()), equals);
            }
            zzbsd().zzbsx().zzj("No number filter for double property. property", zzg.name);
            return bool;
        } else if (zzg.zD == null) {
            zzbsd().zzbsx().zzj("User property has no value, property", zzg.name);
            return bool;
        } else if (zzc.amJ != null) {
            return zza(new zzag(zzc.amJ).zzmi(zzg.zD), equals);
        } else {
            if (zzc.amK == null) {
                zzbsd().zzbsx().zzj("No string or number filter defined. property", zzg.name);
                return bool;
            }
            zzs zzs = new zzs(zzc.amK);
            if (zzc.amK.amO == null || !zzc.amK.amO.booleanValue()) {
                if (zzld(zzg.zD)) {
                    try {
                        return zza(zzs.zzbk(Long.parseLong(zzg.zD)), equals);
                    } catch (NumberFormatException e) {
                        zzbsd().zzbsx().zze("User property value exceeded Long value range. property, value", zzg.name, zzg.zD);
                        return bool;
                    }
                } else {
                    zzbsd().zzbsx().zze("Invalid user property value for Long number filter. property, value", zzg.name, zzg.zD);
                    return bool;
                }
            } else if (zzle(zzg.zD)) {
                try {
                    double parseDouble = Double.parseDouble(zzg.zD);
                    if (!Double.isInfinite(parseDouble)) {
                        return zza(zzs.zzj(parseDouble), equals);
                    }
                    zzbsd().zzbsx().zze("User property value exceeded Double value range. property, value", zzg.name, zzg.zD);
                    return bool;
                } catch (NumberFormatException e2) {
                    zzbsd().zzbsx().zze("User property value exceeded Double value range. property, value", zzg.name, zzg.zD);
                    return bool;
                }
            } else {
                zzbsd().zzbsx().zze("Invalid user property value for Double number filter. property, value", zzg.name, zzg.zD);
                return bool;
            }
        }
    }

    static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zza(String str, zza[] zzaArr) {
        zzb[] zzbArr;
        zze[] zzeArr;
        com.google.android.gms.internal.zzuf.zzc[] zzcArr;
        zzab.zzy(zzaArr);
        for (zza zza : zzaArr) {
            for (zzb zzb : zza.amB) {
                String str2 = (String) AppMeasurement.zza.ahE.get(zzb.amE);
                if (str2 != null) {
                    zzb.amE = str2;
                }
                for (com.google.android.gms.internal.zzuf.zzc zzc : zzb.amF) {
                    String str3 = (String) zzd.ahF.get(zzc.amM);
                    if (str3 != null) {
                        zzc.amM = str3;
                    }
                }
            }
            for (zze zze : zza.amA) {
                String str4 = (String) AppMeasurement.zze.ahG.get(zze.amT);
                if (str4 != null) {
                    zze.amT = str4;
                }
            }
        }
        zzbry().zzb(str, zzaArr);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public zzuh.zza[] zza(String str, zzuh.zzb[] zzbArr, zzg[] zzgArr) {
        Map map;
        zze zze;
        zzi zzbsr;
        Map map2;
        zzab.zzhr(str);
        HashSet hashSet = new HashSet();
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        Map zzlr = zzbry().zzlr(str);
        if (zzlr != null) {
            for (Integer intValue : zzlr.keySet()) {
                int intValue2 = intValue.intValue();
                zzf zzf = (zzf) zzlr.get(Integer.valueOf(intValue2));
                BitSet bitSet = (BitSet) arrayMap2.get(Integer.valueOf(intValue2));
                BitSet bitSet2 = (BitSet) arrayMap3.get(Integer.valueOf(intValue2));
                if (bitSet == null) {
                    bitSet = new BitSet();
                    arrayMap2.put(Integer.valueOf(intValue2), bitSet);
                    bitSet2 = new BitSet();
                    arrayMap3.put(Integer.valueOf(intValue2), bitSet2);
                }
                for (int i = 0; i < zzf.anT.length * 64; i++) {
                    if (zzal.zza(zzf.anT, i)) {
                        zzbsd().zzbtc().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue2), Integer.valueOf(i));
                        bitSet2.set(i);
                        if (zzal.zza(zzf.anU, i)) {
                            bitSet.set(i);
                        }
                    }
                }
                zzuh.zza zza = new zzuh.zza();
                arrayMap.put(Integer.valueOf(intValue2), zza);
                zza.anl = Boolean.valueOf(false);
                zza.ank = zzf;
                zza.anj = new zzf();
                zza.anj.anU = zzal.zza(bitSet);
                zza.anj.anT = zzal.zza(bitSet2);
            }
        }
        if (zzbArr != null) {
            ArrayMap arrayMap4 = new ArrayMap();
            int length = zzbArr.length;
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= length) {
                    break;
                }
                zzuh.zzb zzb = zzbArr[i3];
                zzi zzaq = zzbry().zzaq(str, zzb.name);
                if (zzaq == null) {
                    zzbsd().zzbsx().zzj("Event aggregate wasn't created during raw event logging. event", zzb.name);
                    zzbsr = new zzi(str, zzb.name, 1, 1, zzb.ano.longValue());
                } else {
                    zzbsr = zzaq.zzbsr();
                }
                zzbry().zza(zzbsr);
                long j = zzbsr.aiC;
                Map map3 = (Map) arrayMap4.get(zzb.name);
                if (map3 == null) {
                    Map zzat = zzbry().zzat(str, zzb.name);
                    if (zzat == null) {
                        zzat = new ArrayMap();
                    }
                    arrayMap4.put(zzb.name, zzat);
                    map2 = zzat;
                } else {
                    map2 = map3;
                }
                zzbsd().zzbtc().zze("event, affected audience count", zzb.name, Integer.valueOf(map2.size()));
                for (Integer intValue3 : map2.keySet()) {
                    int intValue4 = intValue3.intValue();
                    if (hashSet.contains(Integer.valueOf(intValue4))) {
                        zzbsd().zzbtc().zzj("Skipping failed audience ID", Integer.valueOf(intValue4));
                    } else {
                        BitSet bitSet3 = (BitSet) arrayMap2.get(Integer.valueOf(intValue4));
                        BitSet bitSet4 = (BitSet) arrayMap3.get(Integer.valueOf(intValue4));
                        if (((zzuh.zza) arrayMap.get(Integer.valueOf(intValue4))) == null) {
                            zzuh.zza zza2 = new zzuh.zza();
                            arrayMap.put(Integer.valueOf(intValue4), zza2);
                            zza2.anl = Boolean.valueOf(true);
                            bitSet3 = new BitSet();
                            arrayMap2.put(Integer.valueOf(intValue4), bitSet3);
                            bitSet4 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue4), bitSet4);
                        }
                        for (zzb zzb2 : (List) map2.get(Integer.valueOf(intValue4))) {
                            if (zzbsd().zzaz(2)) {
                                zzbsd().zzbtc().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(intValue4), zzb2.amD, zzb2.amE);
                                zzbsd().zzbtc().zzj("Filter definition", zzal.zza(zzb2));
                            }
                            if (zzb2.amD == null || zzb2.amD.intValue() > 256) {
                                zzbsd().zzbsx().zzj("Invalid event filter ID. id", String.valueOf(zzb2.amD));
                            } else if (bitSet3.get(zzb2.amD.intValue())) {
                                zzbsd().zzbtc().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue4), zzb2.amD);
                            } else {
                                Boolean zza3 = zza(zzb2, zzb, j);
                                zzbsd().zzbtc().zzj("Event filter result", zza3 == 0 ? "null" : zza3);
                                if (zza3 == 0) {
                                    hashSet.add(Integer.valueOf(intValue4));
                                } else {
                                    bitSet4.set(zzb2.amD.intValue());
                                    if (zza3.booleanValue()) {
                                        bitSet3.set(zzb2.amD.intValue());
                                    }
                                }
                            }
                        }
                    }
                }
                i2 = i3 + 1;
            }
        }
        if (zzgArr != null) {
            ArrayMap arrayMap5 = new ArrayMap();
            for (zzg zzg : zzgArr) {
                Map map4 = (Map) arrayMap5.get(zzg.name);
                if (map4 == null) {
                    Map zzau = zzbry().zzau(str, zzg.name);
                    if (zzau == null) {
                        zzau = new ArrayMap();
                    }
                    arrayMap5.put(zzg.name, zzau);
                    map = zzau;
                } else {
                    map = map4;
                }
                zzbsd().zzbtc().zze("property, affected audience count", zzg.name, Integer.valueOf(map.size()));
                for (Integer intValue5 : map.keySet()) {
                    int intValue6 = intValue5.intValue();
                    if (hashSet.contains(Integer.valueOf(intValue6))) {
                        zzbsd().zzbtc().zzj("Skipping failed audience ID", Integer.valueOf(intValue6));
                    } else {
                        BitSet bitSet5 = (BitSet) arrayMap2.get(Integer.valueOf(intValue6));
                        BitSet bitSet6 = (BitSet) arrayMap3.get(Integer.valueOf(intValue6));
                        if (((zzuh.zza) arrayMap.get(Integer.valueOf(intValue6))) == null) {
                            zzuh.zza zza4 = new zzuh.zza();
                            arrayMap.put(Integer.valueOf(intValue6), zza4);
                            zza4.anl = Boolean.valueOf(true);
                            bitSet5 = new BitSet();
                            arrayMap2.put(Integer.valueOf(intValue6), bitSet5);
                            bitSet6 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue6), bitSet6);
                        }
                        Iterator it = ((List) map.get(Integer.valueOf(intValue6))).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            zze = (zze) it.next();
                            if (zzbsd().zzaz(2)) {
                                zzbsd().zzbtc().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(intValue6), zze.amD, zze.amT);
                                zzbsd().zzbtc().zzj("Filter definition", zzal.zza(zze));
                            }
                            if (zze.amD == null || zze.amD.intValue() > 256) {
                                zzbsd().zzbsx().zzj("Invalid property filter ID. id", String.valueOf(zze.amD));
                                hashSet.add(Integer.valueOf(intValue6));
                            } else if (bitSet5.get(zze.amD.intValue())) {
                                zzbsd().zzbtc().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue6), zze.amD);
                            } else {
                                Boolean zza5 = zza(zze, zzg);
                                zzbsd().zzbtc().zzj("Property filter result", zza5 == 0 ? "null" : zza5);
                                if (zza5 == 0) {
                                    hashSet.add(Integer.valueOf(intValue6));
                                } else {
                                    bitSet6.set(zze.amD.intValue());
                                    if (zza5.booleanValue()) {
                                        bitSet5.set(zze.amD.intValue());
                                    }
                                }
                            }
                        }
                        zzbsd().zzbsx().zzj("Invalid property filter ID. id", String.valueOf(zze.amD));
                        hashSet.add(Integer.valueOf(intValue6));
                    }
                }
            }
        }
        zzuh.zza[] zzaArr = new zzuh.zza[arrayMap2.size()];
        int i4 = 0;
        for (Integer intValue7 : arrayMap2.keySet()) {
            int intValue8 = intValue7.intValue();
            if (!hashSet.contains(Integer.valueOf(intValue8))) {
                zzuh.zza zza6 = (zzuh.zza) arrayMap.get(Integer.valueOf(intValue8));
                if (zza6 == null) {
                    zza6 = new zzuh.zza();
                }
                zzuh.zza zza7 = zza6;
                int i5 = i4 + 1;
                zzaArr[i4] = zza7;
                zza7.amz = Integer.valueOf(intValue8);
                zza7.anj = new zzf();
                zza7.anj.anU = zzal.zza((BitSet) arrayMap2.get(Integer.valueOf(intValue8)));
                zza7.anj.anT = zzal.zza((BitSet) arrayMap3.get(Integer.valueOf(intValue8)));
                zzbry().zza(str, intValue8, zza7.anj);
                i4 = i5;
            }
        }
        return (zzuh.zza[]) Arrays.copyOf(zzaArr, i4);
    }

    /* access modifiers changed from: 0000 */
    public boolean zzld(String str) {
        return Pattern.matches("[+-]?[0-9]+", str);
    }

    /* access modifiers changed from: 0000 */
    public boolean zzle(String str) {
        return Pattern.matches("[+-]?(([0-9]+\\.?)|([0-9]*\\.[0-9]+))", str);
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }
}
