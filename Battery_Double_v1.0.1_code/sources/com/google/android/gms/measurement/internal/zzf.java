package com.google.android.gms.measurement.internal;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.zzab;

abstract class zzf {
    private static volatile Handler zzczf;
    /* access modifiers changed from: private */
    public final zzx ahD;
    /* access modifiers changed from: private */
    public boolean aiu = true;
    /* access modifiers changed from: private */
    public volatile long zzczg;
    private final Runnable zzw = new Runnable() {
        public void run() {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                zzf.this.ahD.zzbsc().zzm(this);
                return;
            }
            boolean zzfc = zzf.this.zzfc();
            zzf.this.zzczg = 0;
            if (zzfc && zzf.this.aiu) {
                zzf.this.run();
            }
        }
    };

    zzf(zzx zzx) {
        zzab.zzy(zzx);
        this.ahD = zzx;
    }

    private Handler getHandler() {
        Handler handler;
        if (zzczf != null) {
            return zzczf;
        }
        synchronized (zzf.class) {
            if (zzczf == null) {
                zzczf = new Handler(this.ahD.getContext().getMainLooper());
            }
            handler = zzczf;
        }
        return handler;
    }

    public void cancel() {
        this.zzczg = 0;
        getHandler().removeCallbacks(this.zzw);
    }

    public abstract void run();

    public boolean zzfc() {
        return this.zzczg != 0;
    }

    public void zzv(long j) {
        cancel();
        if (j >= 0) {
            this.zzczg = this.ahD.zzyw().currentTimeMillis();
            if (!getHandler().postDelayed(this.zzw, j)) {
                this.ahD.zzbsd().zzbsv().zzj("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }
}
