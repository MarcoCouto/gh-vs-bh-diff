package com.google.android.gms.measurement.internal;

abstract class zzaa extends zzz {
    private boolean zzcwq;

    zzaa(zzx zzx) {
        super(zzx);
        this.ahD.zzb(this);
    }

    public final void initialize() {
        if (this.zzcwq) {
            throw new IllegalStateException("Can't initialize twice");
        }
        zzwv();
        this.ahD.zzbug();
        this.zzcwq = true;
    }

    /* access modifiers changed from: 0000 */
    public boolean isInitialized() {
        return this.zzcwq;
    }

    /* access modifiers changed from: 0000 */
    public boolean zzbul() {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract void zzwv();

    /* access modifiers changed from: protected */
    public void zzzg() {
        if (!isInitialized()) {
            throw new IllegalStateException("Not initialized");
        }
    }
}
