package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.Process;
import android.support.annotation.BinderThread;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.measurement.internal.zzm.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class zzy extends zza {
    /* access modifiers changed from: private */
    public final zzx ahD;
    private final boolean alt;

    public zzy(zzx zzx) {
        zzab.zzy(zzx);
        this.ahD = zzx;
        this.alt = false;
    }

    public zzy(zzx zzx, boolean z) {
        zzab.zzy(zzx);
        this.ahD = zzx;
        this.alt = z;
    }

    @BinderThread
    private void zzf(AppMetadata appMetadata) {
        zzab.zzy(appMetadata);
        zzmf(appMetadata.packageName);
        this.ahD.zzbrz().zzmq(appMetadata.aic);
    }

    @BinderThread
    private void zzmf(String str) throws SecurityException {
        if (TextUtils.isEmpty(str)) {
            this.ahD.zzbsd().zzbsv().log("Measurement Service called without app package");
            throw new SecurityException("Measurement Service called without app package");
        }
        try {
            zzmg(str);
        } catch (SecurityException e) {
            this.ahD.zzbsd().zzbsv().zzj("Measurement Service called with invalid calling package", str);
            throw e;
        }
    }

    @BinderThread
    public List<UserAttributeParcel> zza(final AppMetadata appMetadata, boolean z) {
        zzf(appMetadata);
        try {
            List<zzak> list = (List) this.ahD.zzbsc().zzd((Callable<V>) new Callable<List<zzak>>() {
                /* renamed from: zzbuk */
                public List<zzak> call() throws Exception {
                    zzy.this.ahD.zzbuh();
                    return zzy.this.ahD.zzbry().zzlm(appMetadata.packageName);
                }
            }).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (zzak zzak : list) {
                if (z || !zzal.zzmt(zzak.mName)) {
                    arrayList.add(new UserAttributeParcel(zzak));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.ahD.zzbsd().zzbsv().zzj("Failed to get user attributes", e);
            return null;
        }
    }

    @BinderThread
    public void zza(final AppMetadata appMetadata) {
        zzf(appMetadata);
        this.ahD.zzbsc().zzm(new Runnable() {
            public void run() {
                zzy.this.ahD.zzbuh();
                zzy.this.zzme(appMetadata.aig);
                zzy.this.ahD.zzd(appMetadata);
            }
        });
    }

    @BinderThread
    public void zza(final EventParcel eventParcel, final AppMetadata appMetadata) {
        zzab.zzy(eventParcel);
        zzf(appMetadata);
        this.ahD.zzbsc().zzm(new Runnable() {
            public void run() {
                zzy.this.ahD.zzbuh();
                zzy.this.zzme(appMetadata.aig);
                zzy.this.ahD.zzb(eventParcel, appMetadata);
            }
        });
    }

    @BinderThread
    public void zza(final EventParcel eventParcel, final String str, final String str2) {
        zzab.zzy(eventParcel);
        zzab.zzhr(str);
        zzmf(str);
        this.ahD.zzbsc().zzm(new Runnable() {
            public void run() {
                zzy.this.ahD.zzbuh();
                zzy.this.zzme(str2);
                zzy.this.ahD.zzb(eventParcel, str);
            }
        });
    }

    @BinderThread
    public void zza(final UserAttributeParcel userAttributeParcel, final AppMetadata appMetadata) {
        zzab.zzy(userAttributeParcel);
        zzf(appMetadata);
        if (userAttributeParcel.getValue() == null) {
            this.ahD.zzbsc().zzm(new Runnable() {
                public void run() {
                    zzy.this.ahD.zzbuh();
                    zzy.this.zzme(appMetadata.aig);
                    zzy.this.ahD.zzc(userAttributeParcel, appMetadata);
                }
            });
        } else {
            this.ahD.zzbsc().zzm(new Runnable() {
                public void run() {
                    zzy.this.ahD.zzbuh();
                    zzy.this.zzme(appMetadata.aig);
                    zzy.this.ahD.zzb(userAttributeParcel, appMetadata);
                }
            });
        }
    }

    @BinderThread
    public byte[] zza(final EventParcel eventParcel, final String str) {
        zzab.zzhr(str);
        zzab.zzy(eventParcel);
        zzmf(str);
        this.ahD.zzbsd().zzbtb().zzj("Log and bundle. event", eventParcel.name);
        long nanoTime = this.ahD.zzyw().nanoTime() / 1000000;
        try {
            byte[] bArr = (byte[]) this.ahD.zzbsc().zze((Callable<V>) new Callable<byte[]>() {
                /* renamed from: zzbuj */
                public byte[] call() throws Exception {
                    zzy.this.ahD.zzbuh();
                    return zzy.this.ahD.zza(eventParcel, str);
                }
            }).get();
            if (bArr == null) {
                this.ahD.zzbsd().zzbsv().log("Log and bundle returned null");
                bArr = new byte[0];
            }
            this.ahD.zzbsd().zzbtb().zzd("Log and bundle processed. event, size, time_ms", eventParcel.name, Integer.valueOf(bArr.length), Long.valueOf((this.ahD.zzyw().nanoTime() / 1000000) - nanoTime));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.ahD.zzbsd().zzbsv().zze("Failed to log and bundle. event, error", eventParcel.name, e);
            return null;
        }
    }

    @BinderThread
    public void zzb(final AppMetadata appMetadata) {
        zzf(appMetadata);
        this.ahD.zzbsc().zzm(new Runnable() {
            public void run() {
                zzy.this.ahD.zzbuh();
                zzy.this.zzme(appMetadata.aig);
                zzy.this.ahD.zzc(appMetadata);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzme(String str) {
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split(":", 2);
            if (split.length == 2) {
                try {
                    long longValue = Long.valueOf(split[0]).longValue();
                    if (longValue > 0) {
                        this.ahD.zzbse().ajX.zzh(split[1], longValue);
                    } else {
                        this.ahD.zzbsd().zzbsx().zzj("Combining sample with a non-positive weight", Long.valueOf(longValue));
                    }
                } catch (NumberFormatException e) {
                    this.ahD.zzbsd().zzbsx().zzj("Combining sample with a non-number weight", split[0]);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzmg(String str) throws SecurityException {
        int callingUid = this.alt ? Process.myUid() : Binder.getCallingUid();
        if (!com.google.android.gms.common.util.zzy.zzb(this.ahD.getContext(), callingUid, str)) {
            if (!com.google.android.gms.common.util.zzy.zze(this.ahD.getContext(), callingUid) || this.ahD.zzbty()) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
            }
        }
    }
}
