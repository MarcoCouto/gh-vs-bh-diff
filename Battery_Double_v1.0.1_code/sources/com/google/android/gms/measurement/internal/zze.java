package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzapn;
import com.google.android.gms.internal.zzapo;
import com.google.android.gms.internal.zzuh.zzf;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class zze extends zzaa {
    /* access modifiers changed from: private */
    public static final Map<String, String> aim = new ArrayMap(16);
    private final zzc ain = new zzc(getContext(), zzaab());
    /* access modifiers changed from: private */
    public final zzah aio = new zzah(zzyw());

    public static class zza {
        long aip;
        long aiq;
        long air;
        long ais;
    }

    interface zzb {
        boolean zza(long j, com.google.android.gms.internal.zzuh.zzb zzb);

        void zzc(com.google.android.gms.internal.zzuh.zze zze);
    }

    private class zzc extends SQLiteOpenHelper {
        zzc(Context context, String str) {
            super(context, str, null, 1);
        }

        @WorkerThread
        private void zza(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, Map<String, String> map) throws SQLiteException {
            if (!zza(sQLiteDatabase, str)) {
                sQLiteDatabase.execSQL(str2);
            }
            try {
                zza(sQLiteDatabase, str, str3, map);
            } catch (SQLiteException e) {
                zze.this.zzbsd().zzbsv().zzj("Failed to verify columns on table that was just created", str);
                throw e;
            }
        }

        @WorkerThread
        private void zza(SQLiteDatabase sQLiteDatabase, String str, String str2, Map<String, String> map) throws SQLiteException {
            String[] split;
            Set zzb = zzb(sQLiteDatabase, str);
            for (String str3 : str2.split(",")) {
                if (!zzb.remove(str3)) {
                    throw new SQLiteException(new StringBuilder(String.valueOf(str).length() + 35 + String.valueOf(str3).length()).append("Table ").append(str).append(" is missing required column: ").append(str3).toString());
                }
            }
            if (map != null) {
                for (Entry entry : map.entrySet()) {
                    if (!zzb.remove(entry.getKey())) {
                        sQLiteDatabase.execSQL((String) entry.getValue());
                    }
                }
            }
            if (!zzb.isEmpty()) {
                throw new SQLiteException(new StringBuilder(String.valueOf(str).length() + 30).append("Table ").append(str).append(" table has extra columns").toString());
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0041  */
        @WorkerThread
        private boolean zza(SQLiteDatabase sQLiteDatabase, String str) {
            Cursor cursor;
            Cursor cursor2 = null;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                cursor = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = cursor.moveToFirst();
                    if (cursor == null) {
                        return moveToFirst;
                    }
                    cursor.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    e = e;
                }
            } catch (SQLiteException e2) {
                e = e2;
                cursor = null;
            } catch (Throwable th) {
                th = th;
                if (cursor2 != null) {
                }
                throw th;
            }
            try {
                zze.this.zzbsd().zzbsx().zze("Error querying for table", str, e);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                cursor2 = cursor;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        }

        @WorkerThread
        private Set<String> zzb(SQLiteDatabase sQLiteDatabase, String str) {
            HashSet hashSet = new HashSet();
            Cursor rawQuery = sQLiteDatabase.rawQuery(new StringBuilder(String.valueOf(str).length() + 22).append("SELECT * FROM ").append(str).append(" LIMIT 0").toString(), null);
            try {
                Collections.addAll(hashSet, rawQuery.getColumnNames());
                return hashSet;
            } finally {
                rawQuery.close();
            }
        }

        @WorkerThread
        public SQLiteDatabase getWritableDatabase() {
            if (!zze.this.aio.zzx(zze.this.zzbsf().zzbra())) {
                throw new SQLiteException("Database open failed");
            }
            try {
                return super.getWritableDatabase();
            } catch (SQLiteException e) {
                zze.this.aio.start();
                zze.this.zzbsd().zzbsv().log("Opening the database failed, dropping and recreating it");
                zze.this.getContext().getDatabasePath(zze.this.zzaab()).delete();
                try {
                    SQLiteDatabase writableDatabase = super.getWritableDatabase();
                    zze.this.aio.clear();
                    return writableDatabase;
                } catch (SQLiteException e2) {
                    zze.this.zzbsd().zzbsv().zzj("Failed to open freshly created database", e2);
                    throw e2;
                }
            }
        }

        @WorkerThread
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT >= 9) {
                File file = new File(sQLiteDatabase.getPath());
                file.setReadable(false, false);
                file.setWritable(false, false);
                file.setReadable(true, true);
                file.setWritable(true, true);
            }
        }

        @WorkerThread
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            zza(sQLiteDatabase, "events", "CREATE TABLE IF NOT EXISTS events ( app_id TEXT NOT NULL, name TEXT NOT NULL, lifetime_count INTEGER NOT NULL, current_bundle_count INTEGER NOT NULL, last_fire_timestamp INTEGER NOT NULL, PRIMARY KEY (app_id, name)) ;", "app_id,name,lifetime_count,current_bundle_count,last_fire_timestamp", null);
            zza(sQLiteDatabase, "user_attributes", "CREATE TABLE IF NOT EXISTS user_attributes ( app_id TEXT NOT NULL, name TEXT NOT NULL, set_timestamp INTEGER NOT NULL, value BLOB NOT NULL, PRIMARY KEY (app_id, name)) ;", "app_id,name,set_timestamp,value", null);
            zza(sQLiteDatabase, "apps", "CREATE TABLE IF NOT EXISTS apps ( app_id TEXT NOT NULL, app_instance_id TEXT, gmp_app_id TEXT, resettable_device_id_hash TEXT, last_bundle_index INTEGER NOT NULL, last_bundle_end_timestamp INTEGER NOT NULL, PRIMARY KEY (app_id)) ;", "app_id,app_instance_id,gmp_app_id,resettable_device_id_hash,last_bundle_index,last_bundle_end_timestamp", zze.aim);
            zza(sQLiteDatabase, "queue", "CREATE TABLE IF NOT EXISTS queue ( app_id TEXT NOT NULL, bundle_end_timestamp INTEGER NOT NULL, data BLOB NOT NULL);", "app_id,bundle_end_timestamp,data", null);
            zza(sQLiteDatabase, "raw_events_metadata", "CREATE TABLE IF NOT EXISTS raw_events_metadata ( app_id TEXT NOT NULL, metadata_fingerprint INTEGER NOT NULL, metadata BLOB NOT NULL, PRIMARY KEY (app_id, metadata_fingerprint));", "app_id,metadata_fingerprint,metadata", null);
            zza(sQLiteDatabase, "raw_events", "CREATE TABLE IF NOT EXISTS raw_events ( app_id TEXT NOT NULL, name TEXT NOT NULL, timestamp INTEGER NOT NULL, metadata_fingerprint INTEGER NOT NULL, data BLOB NOT NULL);", "app_id,name,timestamp,metadata_fingerprint,data", null);
            zza(sQLiteDatabase, "event_filters", "CREATE TABLE IF NOT EXISTS event_filters ( app_id TEXT NOT NULL, audience_id INTEGER NOT NULL, filter_id INTEGER NOT NULL, event_name TEXT NOT NULL, data BLOB NOT NULL, PRIMARY KEY (app_id, event_name, audience_id, filter_id));", "app_id,audience_id,filter_id,event_name,data", null);
            zza(sQLiteDatabase, "property_filters", "CREATE TABLE IF NOT EXISTS property_filters ( app_id TEXT NOT NULL, audience_id INTEGER NOT NULL, filter_id INTEGER NOT NULL, property_name TEXT NOT NULL, data BLOB NOT NULL, PRIMARY KEY (app_id, property_name, audience_id, filter_id));", "app_id,audience_id,filter_id,property_name,data", null);
            zza(sQLiteDatabase, "audience_filter_values", "CREATE TABLE IF NOT EXISTS audience_filter_values ( app_id TEXT NOT NULL, audience_id INTEGER NOT NULL, current_results BLOB, PRIMARY KEY (app_id, audience_id));", "app_id,audience_id,current_results", null);
        }

        @WorkerThread
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    static {
        aim.put("app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;");
        aim.put("app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;");
        aim.put("gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;");
        aim.put("dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;");
        aim.put("measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;");
        aim.put("last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;");
        aim.put("day", "ALTER TABLE apps ADD COLUMN day INTEGER;");
        aim.put("daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;");
        aim.put("daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;");
        aim.put("daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;");
        aim.put("remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;");
        aim.put("config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;");
        aim.put("failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;");
        aim.put("app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;");
        aim.put("firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;");
        aim.put("daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;");
    }

    zze(zzx zzx) {
        super(zzx);
    }

    @WorkerThread
    @TargetApi(11)
    static int zza(Cursor cursor, int i) {
        if (VERSION.SDK_INT >= 11) {
            return cursor.getType(i);
        }
        CursorWindow window = ((SQLiteCursor) cursor).getWindow();
        int position = cursor.getPosition();
        if (window.isNull(position, i)) {
            return 0;
        }
        if (window.isLong(position, i)) {
            return 1;
        }
        if (window.isFloat(position, i)) {
            return 2;
        }
        if (window.isString(position, i)) {
            return 3;
        }
        return window.isBlob(position, i) ? 4 : -1;
    }

    @WorkerThread
    private long zza(String str, String[] strArr, long j) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = getWritableDatabase().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                j = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } else if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zze("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    @WorkerThread
    private void zza(String str, com.google.android.gms.internal.zzuf.zza zza2) {
        boolean z = false;
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzy(zza2);
        zzab.zzy(zza2.amB);
        zzab.zzy(zza2.amA);
        if (zza2.amz == null) {
            zzbsd().zzbsx().log("Audience with no ID");
            return;
        }
        int intValue = zza2.amz.intValue();
        for (com.google.android.gms.internal.zzuf.zzb zzb2 : zza2.amB) {
            if (zzb2.amD == null) {
                zzbsd().zzbsx().zze("Event filter with no ID. Audience definition ignored. appId, audienceId", str, zza2.amz);
                return;
            }
        }
        for (com.google.android.gms.internal.zzuf.zze zze : zza2.amA) {
            if (zze.amD == null) {
                zzbsd().zzbsx().zze("Property filter with no ID. Audience definition ignored. appId, audienceId", str, zza2.amz);
                return;
            }
        }
        boolean z2 = true;
        com.google.android.gms.internal.zzuf.zzb[] zzbArr = zza2.amB;
        int length = zzbArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (!zza(str, intValue, zzbArr[i])) {
                z2 = false;
                break;
            } else {
                i++;
            }
        }
        if (z2) {
            com.google.android.gms.internal.zzuf.zze[] zzeArr = zza2.amA;
            int length2 = zzeArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length2) {
                    break;
                } else if (!zza(str, intValue, zzeArr[i2])) {
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                zzz(str, intValue);
                return;
            }
            return;
        }
        z = z2;
        if (z) {
        }
    }

    @WorkerThread
    private boolean zza(String str, int i, com.google.android.gms.internal.zzuf.zzb zzb2) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzy(zzb2);
        if (TextUtils.isEmpty(zzb2.amE)) {
            zzbsd().zzbsx().zze("Event filter had no event name. Audience definition ignored. audienceId, filterId", Integer.valueOf(i), String.valueOf(zzb2.amD));
            return false;
        }
        try {
            byte[] bArr = new byte[zzb2.aM()];
            zzapo zzbe = zzapo.zzbe(bArr);
            zzb2.zza(zzbe);
            zzbe.az();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("filter_id", zzb2.amD);
            contentValues.put("event_name", zzb2.amE);
            contentValues.put("data", bArr);
            try {
                if (getWritableDatabase().insertWithOnConflict("event_filters", null, contentValues, 5) == -1) {
                    zzbsd().zzbsv().log("Failed to insert event filter (got -1)");
                }
                return true;
            } catch (SQLiteException e) {
                zzbsd().zzbsv().zzj("Error storing event filter", e);
                return false;
            }
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Configuration loss. Failed to serialize event filter", e2);
            return false;
        }
    }

    @WorkerThread
    private boolean zza(String str, int i, com.google.android.gms.internal.zzuf.zze zze) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzy(zze);
        if (TextUtils.isEmpty(zze.amT)) {
            zzbsd().zzbsx().zze("Property filter had no property name. Audience definition ignored. audienceId, filterId", Integer.valueOf(i), String.valueOf(zze.amD));
            return false;
        }
        try {
            byte[] bArr = new byte[zze.aM()];
            zzapo zzbe = zzapo.zzbe(bArr);
            zze.zza(zzbe);
            zzbe.az();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("filter_id", zze.amD);
            contentValues.put("property_name", zze.amT);
            contentValues.put("data", bArr);
            try {
                if (getWritableDatabase().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                    return true;
                }
                zzbsd().zzbsv().log("Failed to insert property filter (got -1)");
                return false;
            } catch (SQLiteException e) {
                zzbsd().zzbsv().zzj("Error storing property filter", e);
                return false;
            }
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Configuration loss. Failed to serialize property filter", e2);
            return false;
        }
    }

    @WorkerThread
    private long zzb(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = getWritableDatabase().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zze("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private boolean zzbsm() {
        return getContext().getDatabasePath(zzaab()).exists();
    }

    @WorkerThread
    public void beginTransaction() {
        zzzg();
        getWritableDatabase().beginTransaction();
    }

    @WorkerThread
    public void endTransaction() {
        zzzg();
        getWritableDatabase().endTransaction();
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public SQLiteDatabase getWritableDatabase() {
        zzwu();
        try {
            return this.ain.getWritableDatabase();
        } catch (SQLiteException e) {
            zzbsd().zzbsx().zzj("Error opening database", e);
            throw e;
        }
    }

    @WorkerThread
    public void setTransactionSuccessful() {
        zzzg();
        getWritableDatabase().setTransactionSuccessful();
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x010b  */
    @WorkerThread
    public zza zza(long j, String str, boolean z, boolean z2, boolean z3) {
        Cursor cursor;
        zzab.zzhr(str);
        zzwu();
        zzzg();
        String[] strArr = {str};
        zza zza2 = new zza();
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            cursor = writableDatabase.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    zzbsd().zzbsx().zzj("Not updating daily counts, app is not known", str);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return zza2;
                }
                if (cursor.getLong(0) == j) {
                    zza2.aiq = cursor.getLong(1);
                    zza2.aip = cursor.getLong(2);
                    zza2.air = cursor.getLong(3);
                    zza2.ais = cursor.getLong(4);
                }
                zza2.aiq++;
                if (z) {
                    zza2.aip++;
                }
                if (z2) {
                    zza2.air++;
                }
                if (z3) {
                    zza2.ais++;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("day", Long.valueOf(j));
                contentValues.put("daily_public_events_count", Long.valueOf(zza2.aip));
                contentValues.put("daily_events_count", Long.valueOf(zza2.aiq));
                contentValues.put("daily_conversions_count", Long.valueOf(zza2.air));
                contentValues.put("daily_error_events_count", Long.valueOf(zza2.ais));
                writableDatabase.update("apps", contentValues, "app_id=?", strArr);
                if (cursor != null) {
                    cursor.close();
                }
                return zza2;
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzbsd().zzbsv().zzj("Error updating daily counts", e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return zza2;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zza(ContentValues contentValues, String str, Object obj) {
        zzab.zzhr(str);
        zzab.zzy(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @WorkerThread
    public void zza(com.google.android.gms.internal.zzuh.zze zze) {
        zzwu();
        zzzg();
        zzab.zzy(zze);
        zzab.zzhr(zze.zzck);
        zzab.zzy(zze.anz);
        zzbsh();
        long currentTimeMillis = zzyw().currentTimeMillis();
        if (zze.anz.longValue() < currentTimeMillis - zzbsf().zzbrf() || zze.anz.longValue() > zzbsf().zzbrf() + currentTimeMillis) {
            zzbsd().zzbsx().zze("Storing bundle outside of the max uploading time span. now, timestamp", Long.valueOf(currentTimeMillis), zze.anz);
        }
        try {
            byte[] bArr = new byte[zze.aM()];
            zzapo zzbe = zzapo.zzbe(bArr);
            zze.zza(zzbe);
            zzbe.az();
            byte[] zzj = zzbrz().zzj(bArr);
            zzbsd().zzbtc().zzj("Saving bundle, size", Integer.valueOf(zzj.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zze.zzck);
            contentValues.put("bundle_end_timestamp", zze.anz);
            contentValues.put("data", zzj);
            try {
                if (getWritableDatabase().insert("queue", null, contentValues) == -1) {
                    zzbsd().zzbsv().log("Failed to insert bundle (got -1)");
                }
            } catch (SQLiteException e) {
                zzbsd().zzbsv().zzj("Error storing bundle", e);
            }
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Data loss. Failed to serialize bundle", e2);
        }
    }

    @WorkerThread
    public void zza(zza zza2) {
        zzab.zzy(zza2);
        zzwu();
        zzzg();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zza2.zzsh());
        contentValues.put("app_instance_id", zza2.zzawo());
        contentValues.put("gmp_app_id", zza2.zzbps());
        contentValues.put("resettable_device_id_hash", zza2.zzbpt());
        contentValues.put("last_bundle_index", Long.valueOf(zza2.zzbqc()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(zza2.zzbpv()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(zza2.zzbpw()));
        contentValues.put("app_version", zza2.zzxc());
        contentValues.put("app_store", zza2.zzbpy());
        contentValues.put("gmp_version", Long.valueOf(zza2.zzbpz()));
        contentValues.put("dev_cert_hash", Long.valueOf(zza2.zzbqa()));
        contentValues.put("measurement_enabled", Boolean.valueOf(zza2.zzbqb()));
        contentValues.put("day", Long.valueOf(zza2.zzbqg()));
        contentValues.put("daily_public_events_count", Long.valueOf(zza2.zzbqh()));
        contentValues.put("daily_events_count", Long.valueOf(zza2.zzbqi()));
        contentValues.put("daily_conversions_count", Long.valueOf(zza2.zzbqj()));
        contentValues.put("config_fetched_time", Long.valueOf(zza2.zzbqd()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(zza2.zzbqe()));
        contentValues.put("app_version_int", Long.valueOf(zza2.zzbpx()));
        contentValues.put("firebase_instance_id", zza2.zzbpu());
        contentValues.put("daily_error_events_count", Long.valueOf(zza2.zzbqk()));
        try {
            if (getWritableDatabase().insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                zzbsd().zzbsv().log("Failed to insert/update app (got -1)");
            }
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzj("Error storing app", e);
        }
    }

    public void zza(zzh zzh, long j) {
        zzwu();
        zzzg();
        zzab.zzy(zzh);
        zzab.zzhr(zzh.zzcjf);
        com.google.android.gms.internal.zzuh.zzb zzb2 = new com.google.android.gms.internal.zzuh.zzb();
        zzb2.anp = Long.valueOf(zzh.aiA);
        zzb2.ann = new com.google.android.gms.internal.zzuh.zzc[zzh.aiB.size()];
        Iterator it = zzh.aiB.iterator();
        int i = 0;
        while (it.hasNext()) {
            String str = (String) it.next();
            com.google.android.gms.internal.zzuh.zzc zzc2 = new com.google.android.gms.internal.zzuh.zzc();
            int i2 = i + 1;
            zzb2.ann[i] = zzc2;
            zzc2.name = str;
            zzbrz().zza(zzc2, zzh.aiB.get(str));
            i = i2;
        }
        try {
            byte[] bArr = new byte[zzb2.aM()];
            zzapo zzbe = zzapo.zzbe(bArr);
            zzb2.zza(zzbe);
            zzbe.az();
            zzbsd().zzbtc().zze("Saving event, name, data size", zzh.mName, Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zzh.zzcjf);
            contentValues.put("name", zzh.mName);
            contentValues.put("timestamp", Long.valueOf(zzh.pJ));
            contentValues.put("metadata_fingerprint", Long.valueOf(j));
            contentValues.put("data", bArr);
            try {
                if (getWritableDatabase().insert("raw_events", null, contentValues) == -1) {
                    zzbsd().zzbsv().log("Failed to insert raw event (got -1)");
                }
            } catch (SQLiteException e) {
                zzbsd().zzbsv().zzj("Error storing raw event", e);
            }
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Data loss. Failed to serialize event params/data", e2);
        }
    }

    @WorkerThread
    public void zza(zzi zzi) {
        zzab.zzy(zzi);
        zzwu();
        zzzg();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzi.zzcjf);
        contentValues.put("name", zzi.mName);
        contentValues.put("lifetime_count", Long.valueOf(zzi.aiC));
        contentValues.put("current_bundle_count", Long.valueOf(zzi.aiD));
        contentValues.put("last_fire_timestamp", Long.valueOf(zzi.aiE));
        try {
            if (getWritableDatabase().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                zzbsd().zzbsv().log("Failed to insert/update event aggregates (got -1)");
            }
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzj("Error storing event aggregates", e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zza(String str, int i, zzf zzf) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzy(zzf);
        try {
            byte[] bArr = new byte[zzf.aM()];
            zzapo zzbe = zzapo.zzbe(bArr);
            zzf.zza(zzbe);
            zzbe.az();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i));
            contentValues.put("current_results", bArr);
            try {
                if (getWritableDatabase().insertWithOnConflict("audience_filter_values", null, contentValues, 5) == -1) {
                    zzbsd().zzbsv().log("Failed to insert filter results (got -1)");
                }
            } catch (SQLiteException e) {
                zzbsd().zzbsv().zzj("Error storing filter results", e);
            }
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Configuration loss. Failed to serialize filter results", e2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:72:0x017a, code lost:
        r2 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x017a A[ExcHandler: SQLiteException (e android.database.sqlite.SQLiteException), Splitter:B:1:0x000a] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0193  */
    public void zza(String str, long j, zzb zzb2) {
        Cursor cursor;
        String str2;
        Cursor cursor2 = null;
        zzab.zzy(zzb2);
        zzwu();
        zzzg();
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (TextUtils.isEmpty(str)) {
                Cursor rawQuery = writableDatabase.rawQuery("select app_id, metadata_fingerprint from raw_events where app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;", new String[]{String.valueOf(j)});
                if (rawQuery.moveToFirst()) {
                    str = rawQuery.getString(0);
                    String string = rawQuery.getString(1);
                    rawQuery.close();
                    str2 = string;
                    cursor = rawQuery;
                } else if (rawQuery != null) {
                    rawQuery.close();
                    return;
                } else {
                    return;
                }
            } else {
                Cursor rawQuery2 = writableDatabase.rawQuery("select metadata_fingerprint from raw_events where app_id = ? order by rowid limit 1;", new String[]{str});
                if (rawQuery2.moveToFirst()) {
                    String string2 = rawQuery2.getString(0);
                    rawQuery2.close();
                    str2 = string2;
                    cursor = rawQuery2;
                } else if (rawQuery2 != null) {
                    rawQuery2.close();
                    return;
                } else {
                    return;
                }
            }
            try {
                cursor = writableDatabase.query("raw_events_metadata", new String[]{"metadata"}, "app_id=? and metadata_fingerprint=?", new String[]{str, str2}, null, null, "rowid", "2");
                if (!cursor.moveToFirst()) {
                    zzbsd().zzbsv().log("Raw event metadata record is missing");
                    if (cursor != null) {
                        cursor.close();
                        return;
                    }
                    return;
                }
                zzapn zzbd = zzapn.zzbd(cursor.getBlob(0));
                com.google.android.gms.internal.zzuh.zze zze = new com.google.android.gms.internal.zzuh.zze();
                try {
                    com.google.android.gms.internal.zzuh.zze zze2 = (com.google.android.gms.internal.zzuh.zze) zze.zzb(zzbd);
                    if (cursor.moveToNext()) {
                        zzbsd().zzbsx().log("Get multiple raw event metadata records, expected one");
                    }
                    cursor.close();
                    zzb2.zzc(zze);
                    Cursor query = writableDatabase.query("raw_events", new String[]{"rowid", "name", "timestamp", "data"}, "app_id=? and metadata_fingerprint=?", new String[]{str, str2}, null, null, "rowid", null);
                    if (!query.moveToFirst()) {
                        zzbsd().zzbsx().log("Raw event data disappeared while in transaction");
                        if (query != null) {
                            query.close();
                            return;
                        }
                        return;
                    }
                    do {
                        long j2 = query.getLong(0);
                        zzapn zzbd2 = zzapn.zzbd(query.getBlob(3));
                        com.google.android.gms.internal.zzuh.zzb zzb3 = new com.google.android.gms.internal.zzuh.zzb();
                        try {
                            com.google.android.gms.internal.zzuh.zzb zzb4 = (com.google.android.gms.internal.zzuh.zzb) zzb3.zzb(zzbd2);
                            zzb3.name = query.getString(1);
                            zzb3.ano = Long.valueOf(query.getLong(2));
                            if (!zzb2.zza(j2, zzb3)) {
                                if (query != null) {
                                    query.close();
                                    return;
                                }
                                return;
                            }
                        } catch (IOException e) {
                            zzbsd().zzbsv().zze("Data loss. Failed to merge raw event", str, e);
                        }
                    } while (query.moveToNext());
                    if (query != null) {
                        query.close();
                    }
                } catch (IOException e2) {
                    zzbsd().zzbsv().zze("Data loss. Failed to merge raw event metadata", str, e2);
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                cursor2 = cursor;
                try {
                    zzbsd().zzbsv().zzj("Data loss. Error selecting raw event", e);
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = cursor2;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (SQLiteException e4) {
        } catch (Throwable th3) {
            th = th3;
            cursor = cursor2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @WorkerThread
    public boolean zza(zzak zzak) {
        zzab.zzy(zzak);
        zzwu();
        zzzg();
        if (zzas(zzak.zzcjf, zzak.mName) == null) {
            if (zzal.zzmj(zzak.mName)) {
                if (zzb("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{zzak.zzcjf}) >= ((long) zzbsf().zzbqy())) {
                    return false;
                }
            } else {
                if (zzb("select count(1) from user_attributes where app_id=?", new String[]{zzak.zzcjf}) >= ((long) zzbsf().zzbqz())) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzak.zzcjf);
        contentValues.put("name", zzak.mName);
        contentValues.put("set_timestamp", Long.valueOf(zzak.amx));
        zza(contentValues, Param.VALUE, zzak.zzcnn);
        try {
            if (getWritableDatabase().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                zzbsd().zzbsv().log("Failed to insert/update user property (got -1)");
            }
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzj("Error storing user property", e);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public String zzaab() {
        if (!zzbsf().zzabc()) {
            return zzbsf().zzacc();
        }
        if (zzbsf().zzabd()) {
            return zzbsf().zzacc();
        }
        zzbsd().zzbsy().log("Using secondary database");
        return zzbsf().zzacd();
    }

    public void zzac(List<Long> list) {
        zzab.zzy(list);
        zzwu();
        zzzg();
        StringBuilder sb = new StringBuilder("rowid in (");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                break;
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(((Long) list.get(i2)).longValue());
            i = i2 + 1;
        }
        sb.append(")");
        int delete = getWritableDatabase().delete("raw_events", sb.toString(), null);
        if (delete != list.size()) {
            zzbsd().zzbsv().zze("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c  */
    @WorkerThread
    public zzi zzaq(String str, String str2) {
        Cursor cursor;
        Cursor cursor2 = null;
        zzab.zzhr(str);
        zzab.zzhr(str2);
        zzwu();
        zzzg();
        try {
            Cursor query = getWritableDatabase().query("events", new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp"}, "app_id=? and name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return null;
                }
                zzi zzi = new zzi(str, str2, query.getLong(0), query.getLong(1), query.getLong(2));
                if (query.moveToNext()) {
                    zzbsd().zzbsv().log("Got multiple records for event aggregates, expected one");
                }
                if (query == null) {
                    return zzi;
                }
                query.close();
                return zzi;
            } catch (SQLiteException e) {
                e = e;
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zzd("Error querying events", str, str2, e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }

    @WorkerThread
    public void zzar(String str, String str2) {
        zzab.zzhr(str);
        zzab.zzhr(str2);
        zzwu();
        zzzg();
        try {
            zzbsd().zzbtc().zzj("Deleted user attribute rows:", Integer.valueOf(getWritableDatabase().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzd("Error deleting user attribute", str, str2, e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082  */
    @WorkerThread
    public zzak zzas(String str, String str2) {
        Cursor cursor;
        Cursor cursor2 = null;
        zzab.zzhr(str);
        zzab.zzhr(str2);
        zzwu();
        zzzg();
        try {
            Cursor query = getWritableDatabase().query("user_attributes", new String[]{"set_timestamp", Param.VALUE}, "app_id=? and name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return null;
                }
                zzak zzak = new zzak(str, str2, query.getLong(0), zzb(query, 1));
                if (query.moveToNext()) {
                    zzbsd().zzbsv().log("Got multiple records for user property, expected one");
                }
                if (query == null) {
                    return zzak;
                }
                query.close();
                return zzak;
            } catch (SQLiteException e) {
                e = e;
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zzd("Error querying user property", str, str2, e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
            if (cursor2 != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b0  */
    public Map<Integer, List<com.google.android.gms.internal.zzuf.zzb>> zzat(String str, String str2) {
        Cursor cursor;
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzhr(str2);
        ArrayMap arrayMap = new ArrayMap();
        try {
            cursor = getWritableDatabase().query("event_filters", new String[]{"audience_id", "data"}, "app_id=? AND event_name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<com.google.android.gms.internal.zzuf.zzb>> emptyMap = Collections.emptyMap();
                    if (cursor == null) {
                        return emptyMap;
                    }
                    cursor.close();
                    return emptyMap;
                }
                do {
                    zzapn zzbd = zzapn.zzbd(cursor.getBlob(1));
                    com.google.android.gms.internal.zzuf.zzb zzb2 = new com.google.android.gms.internal.zzuf.zzb();
                    try {
                        com.google.android.gms.internal.zzuf.zzb zzb3 = (com.google.android.gms.internal.zzuf.zzb) zzb2.zzb(zzbd);
                        int i = cursor.getInt(0);
                        List list = (List) arrayMap.get(Integer.valueOf(i));
                        if (list == null) {
                            list = new ArrayList();
                            arrayMap.put(Integer.valueOf(i), list);
                        }
                        list.add(zzb2);
                    } catch (IOException e) {
                        zzbsd().zzbsv().zze("Failed to merge filter", str, e);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayMap;
            } catch (SQLiteException e2) {
                e = e2;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zzj("Database error querying filters", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b0  */
    public Map<Integer, List<com.google.android.gms.internal.zzuf.zze>> zzau(String str, String str2) {
        Cursor cursor;
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzhr(str2);
        ArrayMap arrayMap = new ArrayMap();
        try {
            cursor = getWritableDatabase().query("property_filters", new String[]{"audience_id", "data"}, "app_id=? AND property_name=?", new String[]{str, str2}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<com.google.android.gms.internal.zzuf.zze>> emptyMap = Collections.emptyMap();
                    if (cursor == null) {
                        return emptyMap;
                    }
                    cursor.close();
                    return emptyMap;
                }
                do {
                    zzapn zzbd = zzapn.zzbd(cursor.getBlob(1));
                    com.google.android.gms.internal.zzuf.zze zze = new com.google.android.gms.internal.zzuf.zze();
                    try {
                        com.google.android.gms.internal.zzuf.zze zze2 = (com.google.android.gms.internal.zzuf.zze) zze.zzb(zzbd);
                        int i = cursor.getInt(0);
                        List list = (List) arrayMap.get(Integer.valueOf(i));
                        if (list == null) {
                            list = new ArrayList();
                            arrayMap.put(Integer.valueOf(i), list);
                        }
                        list.add(zze);
                    } catch (IOException e) {
                        zzbsd().zzbsv().zze("Failed to merge filter", str, e);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayMap;
            } catch (SQLiteException e2) {
                e = e2;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zzj("Database error querying filters", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public long zzb(com.google.android.gms.internal.zzuh.zze zze) throws IOException {
        zzwu();
        zzzg();
        zzab.zzy(zze);
        zzab.zzhr(zze.zzck);
        try {
            byte[] bArr = new byte[zze.aM()];
            zzapo zzbe = zzapo.zzbe(bArr);
            zze.zza(zzbe);
            zzbe.az();
            long zzy = zzbrz().zzy(bArr);
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", zze.zzck);
            contentValues.put("metadata_fingerprint", Long.valueOf(zzy));
            contentValues.put("metadata", bArr);
            try {
                getWritableDatabase().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
                return zzy;
            } catch (SQLiteException e) {
                zzbsd().zzbsv().zzj("Error storing raw event metadata", e);
                throw e;
            }
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Data loss. Failed to serialize event metadata", e2);
            throw e2;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public Object zzb(Cursor cursor, int i) {
        int zza2 = zza(cursor, i);
        switch (zza2) {
            case 0:
                zzbsd().zzbsv().log("Loaded invalid null value from database");
                return null;
            case 1:
                return Long.valueOf(cursor.getLong(i));
            case 2:
                return Double.valueOf(cursor.getDouble(i));
            case 3:
                return cursor.getString(i);
            case 4:
                zzbsd().zzbsv().log("Loaded invalid blob type value, ignoring it");
                return null;
            default:
                zzbsd().zzbsv().zzj("Loaded invalid unknown value type, ignoring it", Integer.valueOf(zza2));
                return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzb(String str, com.google.android.gms.internal.zzuf.zza[] zzaArr) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzab.zzy(zzaArr);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            zzlq(str);
            for (com.google.android.gms.internal.zzuf.zza zza2 : zzaArr) {
                zza(str, zza2);
            }
            writableDatabase.setTransactionSuccessful();
        } finally {
            writableDatabase.endTransaction();
        }
    }

    @WorkerThread
    public void zzbh(long j) {
        zzwu();
        zzzg();
        if (getWritableDatabase().delete("queue", "rowid=?", new String[]{String.valueOf(j)}) != 1) {
            zzbsd().zzbsv().log("Deleted fewer rows from queue than expected");
        }
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v1 */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v3 */
    /* JADX WARNING: type inference failed for: r2v6 */
    /* JADX WARNING: type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059  */
    /* JADX WARNING: Unknown variable types count: 2 */
    public String zzbi(long j) {
        ? r2;
        Throwable th;
        ? r22;
        String str = 0;
        zzwu();
        zzzg();
        try {
            Cursor rawQuery = getWritableDatabase().rawQuery("select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;", new String[]{String.valueOf(j)});
            try {
                if (!rawQuery.moveToFirst()) {
                    zzbsd().zzbtc().log("No expired configs for apps with pending events");
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                } else {
                    str = rawQuery.getString(0);
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                }
            } catch (SQLiteException e) {
                e = e;
                r22 = rawQuery;
            }
        } catch (SQLiteException e2) {
            e = e2;
            r22 = str;
        } catch (Throwable th2) {
            r2 = str;
            th = th2;
            if (r2 != 0) {
            }
            throw th;
        }
        return str;
        try {
            zzbsd().zzbsv().zzj("Error selecting expired configs", e);
            if (r22 != 0) {
                r22.close();
            }
            return str;
        } catch (Throwable th3) {
            th = th3;
            r2 = r22;
            if (r2 != 0) {
                r2.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d  */
    @WorkerThread
    public String zzbsg() {
        Cursor cursor;
        Throwable th;
        String str = null;
        try {
            cursor = getWritableDatabase().rawQuery("select app_id from queue where app_id not in (select app_id from apps where measurement_enabled=0) order by rowid limit 1;", null);
            try {
                if (cursor.moveToFirst()) {
                    str = cursor.getString(0);
                    if (cursor != null) {
                        cursor.close();
                    }
                } else if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    zzbsd().zzbsv().zzj("Database error getting next bundle app id", e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return str;
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th3) {
            cursor = null;
            th = th3;
            if (cursor != null) {
            }
            throw th;
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzbsh() {
        zzwu();
        zzzg();
        if (zzbsm()) {
            long j = zzbse().akb.get();
            long elapsedRealtime = zzyw().elapsedRealtime();
            if (Math.abs(elapsedRealtime - j) > zzbsf().zzbrg()) {
                zzbse().akb.set(elapsedRealtime);
                zzbsi();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzbsi() {
        zzwu();
        zzzg();
        if (zzbsm()) {
            int delete = getWritableDatabase().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(zzyw().currentTimeMillis()), String.valueOf(zzbsf().zzbrf())});
            if (delete > 0) {
                zzbsd().zzbtc().zzj("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
            }
        }
    }

    @WorkerThread
    public long zzbsj() {
        return zza("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    @WorkerThread
    public long zzbsk() {
        return zza("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    public boolean zzbsl() {
        return zzb("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    @WorkerThread
    public void zzd(String str, byte[] bArr) {
        zzab.zzhr(str);
        zzwu();
        zzzg();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) getWritableDatabase().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                zzbsd().zzbsv().log("Failed to update remote config (got 0)");
            }
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzj("Error storing remote config", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x009e  */
    @WorkerThread
    public List<zzak> zzlm(String str) {
        Cursor cursor;
        Cursor cursor2 = null;
        zzab.zzhr(str);
        zzwu();
        zzzg();
        ArrayList arrayList = new ArrayList();
        try {
            Cursor query = getWritableDatabase().query("user_attributes", new String[]{"name", "set_timestamp", Param.VALUE}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(zzbsf().zzbqz()));
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return arrayList;
                }
                do {
                    String string = query.getString(0);
                    long j = query.getLong(1);
                    Object zzb2 = zzb(query, 2);
                    if (zzb2 == null) {
                        zzbsd().zzbsv().log("Read invalid user property value, ignoring it");
                    } else {
                        arrayList.add(new zzak(str, string, j, zzb2));
                    }
                } while (query.moveToNext());
                if (query != null) {
                    query.close();
                }
                return arrayList;
            } catch (SQLiteException e) {
                e = e;
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zze("Error querying user properties", str, e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x01a9  */
    @WorkerThread
    public zza zzln(String str) {
        Cursor cursor;
        zzab.zzhr(str);
        zzwu();
        zzzg();
        try {
            cursor = getWritableDatabase().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_start_timestamp", "last_bundle_end_timestamp", "app_version", "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled", "day", "daily_public_events_count", "daily_events_count", "daily_conversions_count", "config_fetched_time", "failed_config_fetch_time", "app_version_int", "firebase_instance_id", "daily_error_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                zza zza2 = new zza(this.ahD, str);
                zza2.zzky(cursor.getString(0));
                zza2.zzkz(cursor.getString(1));
                zza2.zzla(cursor.getString(2));
                zza2.zzaz(cursor.getLong(3));
                zza2.zzau(cursor.getLong(4));
                zza2.zzav(cursor.getLong(5));
                zza2.setAppVersion(cursor.getString(6));
                zza2.zzlc(cursor.getString(7));
                zza2.zzax(cursor.getLong(8));
                zza2.zzay(cursor.getLong(9));
                zza2.setMeasurementEnabled((cursor.isNull(10) ? 1 : cursor.getInt(10)) != 0);
                zza2.zzbc(cursor.getLong(11));
                zza2.zzbd(cursor.getLong(12));
                zza2.zzbe(cursor.getLong(13));
                zza2.zzbf(cursor.getLong(14));
                zza2.zzba(cursor.getLong(15));
                zza2.zzbb(cursor.getLong(16));
                zza2.zzaw(cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                zza2.zzlb(cursor.getString(18));
                zza2.zzbg(cursor.getLong(19));
                zza2.zzbpr();
                if (cursor.moveToNext()) {
                    zzbsd().zzbsv().log("Got multiple records for app, expected one");
                }
                if (cursor == null) {
                    return zza2;
                }
                cursor.close();
                return zza2;
            } catch (SQLiteException e) {
                e = e;
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zze("Error querying app", str, e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public long zzlo(String str) {
        zzab.zzhr(str);
        zzwu();
        zzzg();
        try {
            return (long) getWritableDatabase().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(zzbsf().zzll(str))});
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzj("Error deleting over the limit events", e);
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x006c  */
    @WorkerThread
    public byte[] zzlp(String str) {
        Cursor cursor;
        zzab.zzhr(str);
        zzwu();
        zzzg();
        try {
            cursor = getWritableDatabase().query("apps", new String[]{"remote_config"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                if (cursor.moveToNext()) {
                    zzbsd().zzbsv().log("Got multiple records for app config, expected one");
                }
                if (cursor == null) {
                    return blob;
                }
                cursor.close();
                return blob;
            } catch (SQLiteException e) {
                e = e;
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th) {
            th = th;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zze("Error querying remote config", str, e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzlq(String str) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.delete("property_filters", "app_id=?", new String[]{str});
        writableDatabase.delete("event_filters", "app_id=?", new String[]{str});
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0098  */
    public Map<Integer, zzf> zzlr(String str) {
        Cursor cursor;
        Cursor cursor2;
        zzzg();
        zzwu();
        zzab.zzhr(str);
        try {
            cursor2 = getWritableDatabase().query("audience_filter_values", new String[]{"audience_id", "current_results"}, "app_id=?", new String[]{str}, null, null, null);
            try {
                if (!cursor2.moveToFirst()) {
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    return null;
                }
                ArrayMap arrayMap = new ArrayMap();
                do {
                    int i = cursor2.getInt(0);
                    zzapn zzbd = zzapn.zzbd(cursor2.getBlob(1));
                    zzf zzf = new zzf();
                    try {
                        zzf zzf2 = (zzf) zzf.zzb(zzbd);
                        arrayMap.put(Integer.valueOf(i), zzf);
                    } catch (IOException e) {
                        zzbsd().zzbsv().zzd("Failed to merge filter results. appId, audienceId, error", str, Integer.valueOf(i), e);
                    }
                } while (cursor2.moveToNext());
                if (cursor2 != null) {
                    cursor2.close();
                }
                return arrayMap;
            } catch (SQLiteException e2) {
                e = e2;
                cursor = cursor2;
            } catch (Throwable th) {
                th = th;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor2 = null;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zzj("Database error querying filter results", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
            if (cursor2 != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public void zzls(String str) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            String[] strArr = {str};
            int delete = writableDatabase.delete("audience_filter_values", "app_id=?", strArr) + writableDatabase.delete("events", "app_id=?", strArr) + 0 + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("apps", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("event_filters", "app_id=?", strArr) + writableDatabase.delete("property_filters", "app_id=?", strArr);
            if (delete > 0) {
                zzbsd().zzbtc().zze("Deleted application data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zze("Error deleting application data. appId, error", str, e);
        }
    }

    public void zzlt(String str) {
        try {
            getWritableDatabase().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{str, str});
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zzj("Failed to remove unused event metadata", e);
        }
    }

    public long zzlu(String str) {
        zzab.zzhr(str);
        return zza("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x00de  */
    @WorkerThread
    public List<Pair<com.google.android.gms.internal.zzuh.zze, Long>> zzn(String str, int i, int i2) {
        Cursor cursor;
        Cursor cursor2;
        int i3;
        boolean z = true;
        zzwu();
        zzzg();
        zzab.zzbo(i > 0);
        if (i2 <= 0) {
            z = false;
        }
        zzab.zzbo(z);
        zzab.zzhr(str);
        try {
            cursor2 = getWritableDatabase().query("queue", new String[]{"rowid", "data"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i));
            try {
                if (!cursor2.moveToFirst()) {
                    List<Pair<com.google.android.gms.internal.zzuh.zze, Long>> emptyList = Collections.emptyList();
                    if (cursor2 == null) {
                        return emptyList;
                    }
                    cursor2.close();
                    return emptyList;
                }
                ArrayList arrayList = new ArrayList();
                int i4 = 0;
                while (true) {
                    long j = cursor2.getLong(0);
                    try {
                        byte[] zzw = zzbrz().zzw(cursor2.getBlob(1));
                        if (!arrayList.isEmpty() && zzw.length + i4 > i2) {
                            break;
                        }
                        zzapn zzbd = zzapn.zzbd(zzw);
                        com.google.android.gms.internal.zzuh.zze zze = new com.google.android.gms.internal.zzuh.zze();
                        try {
                            com.google.android.gms.internal.zzuh.zze zze2 = (com.google.android.gms.internal.zzuh.zze) zze.zzb(zzbd);
                            i3 = zzw.length + i4;
                            arrayList.add(Pair.create(zze, Long.valueOf(j)));
                        } catch (IOException e) {
                            zzbsd().zzbsv().zze("Failed to merge queued bundle", str, e);
                            i3 = i4;
                        }
                        if (!cursor2.moveToNext() || i3 > i2) {
                            break;
                        }
                        i4 = i3;
                    } catch (IOException e2) {
                        zzbsd().zzbsv().zze("Failed to unzip queued bundle", str, e2);
                        i3 = i4;
                    }
                }
                if (cursor2 != null) {
                    cursor2.close();
                }
                return arrayList;
            } catch (SQLiteException e3) {
                e = e3;
                cursor = cursor2;
            } catch (Throwable th) {
                th = th;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor2 = null;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            zzbsd().zzbsv().zze("Error querying bundles", str, e);
            List<Pair<com.google.android.gms.internal.zzuh.zze, Long>> emptyList2 = Collections.emptyList();
            if (cursor == null) {
                return emptyList2;
            }
            cursor.close();
            return emptyList2;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = cursor;
        }
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    @WorkerThread
    public void zzy(String str, int i) {
        zzab.zzhr(str);
        zzwu();
        zzzg();
        try {
            getWritableDatabase().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str, str, String.valueOf(i)});
        } catch (SQLiteException e) {
            zzbsd().zzbsv().zze("Error pruning currencies", str, e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void zzz(String str, int i) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.delete("property_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(i)});
        writableDatabase.delete("event_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(i)});
    }
}
