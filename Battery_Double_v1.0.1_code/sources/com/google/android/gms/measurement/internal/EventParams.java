package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzab;
import java.util.Iterator;

public class EventParams extends AbstractSafeParcelable implements Iterable<String> {
    public static final zzj CREATOR = new zzj();
    /* access modifiers changed from: private */
    public final Bundle aiF;
    public final int versionCode;

    EventParams(int i, Bundle bundle) {
        this.versionCode = i;
        this.aiF = bundle;
    }

    EventParams(Bundle bundle) {
        zzab.zzy(bundle);
        this.aiF = bundle;
        this.versionCode = 1;
    }

    /* access modifiers changed from: 0000 */
    public Object get(String str) {
        return this.aiF.get(str);
    }

    public Iterator<String> iterator() {
        return new Iterator<String>() {
            Iterator<String> aiG = EventParams.this.aiF.keySet().iterator();

            public boolean hasNext() {
                return this.aiG.hasNext();
            }

            public String next() {
                return (String) this.aiG.next();
            }

            public void remove() {
                throw new UnsupportedOperationException("Remove not supported");
            }
        };
    }

    public int size() {
        return this.aiF.size();
    }

    public String toString() {
        return this.aiF.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzj.zza(this, parcel, i);
    }

    public Bundle zzbss() {
        return new Bundle(this.aiF);
    }
}
