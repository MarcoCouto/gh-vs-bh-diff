package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;

class zzz {
    protected final zzx ahD;

    zzz(zzx zzx) {
        zzab.zzy(zzx);
        this.ahD = zzx;
    }

    public Context getContext() {
        return this.ahD.getContext();
    }

    public void zzbrs() {
        this.ahD.zzbsc().zzbrs();
    }

    public zzc zzbrt() {
        return this.ahD.zzbrt();
    }

    public zzac zzbru() {
        return this.ahD.zzbru();
    }

    public zzn zzbrv() {
        return this.ahD.zzbrv();
    }

    public zzg zzbrw() {
        return this.ahD.zzbrw();
    }

    public zzad zzbrx() {
        return this.ahD.zzbrx();
    }

    public zze zzbry() {
        return this.ahD.zzbry();
    }

    public zzal zzbrz() {
        return this.ahD.zzbrz();
    }

    public zzv zzbsa() {
        return this.ahD.zzbsa();
    }

    public zzaf zzbsb() {
        return this.ahD.zzbsb();
    }

    public zzw zzbsc() {
        return this.ahD.zzbsc();
    }

    public zzp zzbsd() {
        return this.ahD.zzbsd();
    }

    public zzt zzbse() {
        return this.ahD.zzbse();
    }

    public zzd zzbsf() {
        return this.ahD.zzbsf();
    }

    public void zzwu() {
        this.ahD.zzbsc().zzwu();
    }

    public void zzyv() {
        this.ahD.zzyv();
    }

    public zze zzyw() {
        return this.ahD.zzyw();
    }
}
