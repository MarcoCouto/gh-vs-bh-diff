package com.google.android.gms.common.api;

import android.os.Looper;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzpo;
import com.google.android.gms.internal.zzqq;
import com.google.android.gms.internal.zzqu;

public final class PendingResults {

    private static final class zza<R extends Result> extends zzpo<R> {
        private final R sl;

        public zza(R r) {
            super(Looper.getMainLooper());
            this.sl = r;
        }

        /* access modifiers changed from: protected */
        public R zzc(Status status) {
            if (status.getStatusCode() == this.sl.getStatus().getStatusCode()) {
                return this.sl;
            }
            throw new UnsupportedOperationException("Creating failed results is not supported");
        }
    }

    private static final class zzb<R extends Result> extends zzpo<R> {
        private final R sm;

        public zzb(GoogleApiClient googleApiClient, R r) {
            super(googleApiClient);
            this.sm = r;
        }

        /* access modifiers changed from: protected */
        public R zzc(Status status) {
            return this.sm;
        }
    }

    private static final class zzc<R extends Result> extends zzpo<R> {
        public zzc(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* access modifiers changed from: protected */
        public R zzc(Status status) {
            throw new UnsupportedOperationException("Creating failed results is not supported");
        }
    }

    private PendingResults() {
    }

    public static PendingResult<Status> canceledPendingResult() {
        zzqu zzqu = new zzqu(Looper.getMainLooper());
        zzqu.cancel();
        return zzqu;
    }

    public static <R extends Result> PendingResult<R> canceledPendingResult(R r) {
        zzab.zzb(r, (Object) "Result must not be null");
        zzab.zzb(r.getStatus().getStatusCode() == 16, (Object) "Status code must be CommonStatusCodes.CANCELED");
        zza zza2 = new zza(r);
        zza2.cancel();
        return zza2;
    }

    public static <R extends Result> OptionalPendingResult<R> immediatePendingResult(R r) {
        zzab.zzb(r, (Object) "Result must not be null");
        zzc zzc2 = new zzc(null);
        zzc2.zzc(r);
        return new zzqq(zzc2);
    }

    public static PendingResult<Status> immediatePendingResult(Status status) {
        zzab.zzb(status, (Object) "Result must not be null");
        zzqu zzqu = new zzqu(Looper.getMainLooper());
        zzqu.zzc(status);
        return zzqu;
    }

    public static <R extends Result> PendingResult<R> zza(R r, GoogleApiClient googleApiClient) {
        zzab.zzb(r, (Object) "Result must not be null");
        zzab.zzb(!r.getStatus().isSuccess(), (Object) "Status code must not be SUCCESS");
        zzb zzb2 = new zzb(googleApiClient, r);
        zzb2.zzc(r);
        return zzb2;
    }

    public static PendingResult<Status> zza(Status status, GoogleApiClient googleApiClient) {
        zzab.zzb(status, (Object) "Result must not be null");
        zzqu zzqu = new zzqu(googleApiClient);
        zzqu.zzc(status);
        return zzqu;
    }

    public static <R extends Result> OptionalPendingResult<R> zzb(R r, GoogleApiClient googleApiClient) {
        zzab.zzb(r, (Object) "Result must not be null");
        zzc zzc2 = new zzc(googleApiClient);
        zzc2.zzc(r);
        return new zzqq(zzc2);
    }
}
