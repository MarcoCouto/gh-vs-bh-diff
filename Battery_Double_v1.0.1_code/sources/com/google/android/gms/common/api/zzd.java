package com.google.android.gms.common.api;

import java.util.Map;
import java.util.WeakHashMap;

public abstract class zzd {
    private static final Map<Object, zzd> so = new WeakHashMap();
    private static final Object zzamr = new Object();

    public abstract void remove(int i);
}
