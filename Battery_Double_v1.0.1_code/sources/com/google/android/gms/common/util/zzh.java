package com.google.android.gms.common.util;

import android.os.SystemClock;

public final class zzh implements zze {
    private static zzh AW;

    public static synchronized zze zzavm() {
        zzh zzh;
        synchronized (zzh.class) {
            if (AW == null) {
                AW = new zzh();
            }
            zzh = AW;
        }
        return zzh;
    }

    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public long elapsedRealtime() {
        return SystemClock.elapsedRealtime();
    }

    public long nanoTime() {
        return System.nanoTime();
    }
}
