package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.BinderThread;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class zzd<T extends IInterface> {
    public static final String[] xE = {"service_esmobile", "service_googleme"};
    private final Context mContext;
    final Handler mHandler;
    private final com.google.android.gms.common.zzc tz;
    /* access modifiers changed from: private */
    public final zzc xA;
    private final int xB;
    private final String xC;
    protected AtomicInteger xD;
    private int xm;
    private long xn;
    private long xo;
    private int xp;
    private long xq;
    private final zzm xr;
    /* access modifiers changed from: private */
    public final Object xs;
    /* access modifiers changed from: private */
    public zzu xt;
    /* access modifiers changed from: private */
    public zzf xu;
    private T xv;
    /* access modifiers changed from: private */
    public final ArrayList<zze<?>> xw;
    private zzh xx;
    private int xy;
    /* access modifiers changed from: private */
    public final zzb xz;
    private final Looper zzahv;
    private final Object zzail;

    private abstract class zza extends zze<Boolean> {
        public final int statusCode;
        public final Bundle xF;

        @BinderThread
        protected zza(int i, Bundle bundle) {
            super(Boolean.valueOf(true));
            this.statusCode = i;
            this.xF = bundle;
        }

        /* access modifiers changed from: protected */
        public abstract boolean zzasd();

        /* access modifiers changed from: protected */
        public void zzase() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: zzc */
        public void zzv(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool == null) {
                zzd.this.zzb(1, null);
                return;
            }
            switch (this.statusCode) {
                case 0:
                    if (!zzasd()) {
                        zzd.this.zzb(1, null);
                        zzl(new ConnectionResult(8, null));
                        return;
                    }
                    return;
                case 10:
                    zzd.this.zzb(1, null);
                    throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                default:
                    zzd.this.zzb(1, null);
                    if (this.xF != null) {
                        pendingIntent = (PendingIntent) this.xF.getParcelable("pendingIntent");
                    }
                    zzl(new ConnectionResult(this.statusCode, pendingIntent));
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public abstract void zzl(ConnectionResult connectionResult);
    }

    public interface zzb {
        void onConnected(@Nullable Bundle bundle);

        void onConnectionSuspended(int i);
    }

    public interface zzc {
        void onConnectionFailed(@NonNull ConnectionResult connectionResult);
    }

    /* renamed from: com.google.android.gms.common.internal.zzd$zzd reason: collision with other inner class name */
    final class C0029zzd extends Handler {
        public C0029zzd(Looper looper) {
            super(looper);
        }

        private void zza(Message message) {
            zze zze = (zze) message.obj;
            zze.zzase();
            zze.unregister();
        }

        private boolean zzb(Message message) {
            return message.what == 2 || message.what == 1 || message.what == 5;
        }

        public void handleMessage(Message message) {
            PendingIntent pendingIntent = null;
            if (zzd.this.xD.get() != message.arg1) {
                if (zzb(message)) {
                    zza(message);
                }
            } else if ((message.what == 1 || message.what == 5) && !zzd.this.isConnecting()) {
                zza(message);
            } else if (message.what == 3) {
                if (message.obj instanceof PendingIntent) {
                    pendingIntent = (PendingIntent) message.obj;
                }
                ConnectionResult connectionResult = new ConnectionResult(message.arg2, pendingIntent);
                zzd.this.xu.zzh(connectionResult);
                zzd.this.onConnectionFailed(connectionResult);
            } else if (message.what == 4) {
                zzd.this.zzb(4, null);
                if (zzd.this.xz != null) {
                    zzd.this.xz.onConnectionSuspended(message.arg2);
                }
                zzd.this.onConnectionSuspended(message.arg2);
                zzd.this.zza(4, 1, null);
            } else if (message.what == 2 && !zzd.this.isConnected()) {
                zza(message);
            } else if (zzb(message)) {
                ((zze) message.obj).zzasf();
            } else {
                Log.wtf("GmsClient", "Don't know how to handle message: " + message.what, new Exception());
            }
        }
    }

    protected abstract class zze<TListener> {
        private TListener mListener;
        private boolean xH = false;

        public zze(TListener tlistener) {
            this.mListener = tlistener;
        }

        public void unregister() {
            zzasg();
            synchronized (zzd.this.xw) {
                zzd.this.xw.remove(this);
            }
        }

        /* access modifiers changed from: protected */
        public abstract void zzase();

        public void zzasf() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.mListener;
                if (this.xH) {
                    String valueOf = String.valueOf(this);
                    Log.w("GmsClient", new StringBuilder(String.valueOf(valueOf).length() + 47).append("Callback proxy ").append(valueOf).append(" being reused. This is not safe.").toString());
                }
            }
            if (tlistener != null) {
                try {
                    zzv(tlistener);
                } catch (RuntimeException e) {
                    zzase();
                    throw e;
                }
            } else {
                zzase();
            }
            synchronized (this) {
                this.xH = true;
            }
            unregister();
        }

        public void zzasg() {
            synchronized (this) {
                this.mListener = null;
            }
        }

        /* access modifiers changed from: protected */
        public abstract void zzv(TListener tlistener);
    }

    public interface zzf {
        void zzh(@NonNull ConnectionResult connectionResult);
    }

    public static final class zzg extends com.google.android.gms.common.internal.zzt.zza {
        private zzd xI;
        private final int xJ;

        public zzg(@NonNull zzd zzd, int i) {
            this.xI = zzd;
            this.xJ = i;
        }

        private void zzash() {
            this.xI = null;
        }

        @BinderThread
        public void zza(int i, @NonNull IBinder iBinder, @Nullable Bundle bundle) {
            zzab.zzb(this.xI, (Object) "onPostInitComplete can be called only once per call to getRemoteService");
            this.xI.zza(i, iBinder, bundle, this.xJ);
            zzash();
        }

        @BinderThread
        public void zzb(int i, @Nullable Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }
    }

    public final class zzh implements ServiceConnection {
        private final int xJ;

        public zzh(int i) {
            this.xJ = i;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            zzab.zzb(iBinder, (Object) "Expecting a valid IBinder");
            synchronized (zzd.this.xs) {
                zzd.this.xt = com.google.android.gms.common.internal.zzu.zza.zzdt(iBinder);
            }
            zzd.this.zza(0, (Bundle) null, this.xJ);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            synchronized (zzd.this.xs) {
                zzd.this.xt = null;
            }
            zzd.this.mHandler.sendMessage(zzd.this.mHandler.obtainMessage(4, this.xJ, 1));
        }
    }

    protected class zzi implements zzf {
        public zzi() {
        }

        public void zzh(@NonNull ConnectionResult connectionResult) {
            if (connectionResult.isSuccess()) {
                zzd.this.zza((zzq) null, zzd.this.zzasc());
            } else if (zzd.this.xA != null) {
                zzd.this.xA.onConnectionFailed(connectionResult);
            }
        }
    }

    protected final class zzj extends zza {
        public final IBinder xK;

        @BinderThread
        public zzj(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.xK = iBinder;
        }

        /* access modifiers changed from: protected */
        public boolean zzasd() {
            try {
                String interfaceDescriptor = this.xK.getInterfaceDescriptor();
                if (!zzd.this.zzra().equals(interfaceDescriptor)) {
                    String valueOf = String.valueOf(zzd.this.zzra());
                    Log.e("GmsClient", new StringBuilder(String.valueOf(valueOf).length() + 34 + String.valueOf(interfaceDescriptor).length()).append("service descriptor mismatch: ").append(valueOf).append(" vs. ").append(interfaceDescriptor).toString());
                    return false;
                }
                IInterface zzbb = zzd.this.zzbb(this.xK);
                if (zzbb == null || !zzd.this.zza(2, 3, zzbb)) {
                    return false;
                }
                Bundle zzamh = zzd.this.zzamh();
                if (zzd.this.xz != null) {
                    zzd.this.xz.onConnected(zzamh);
                }
                return true;
            } catch (RemoteException e) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void zzl(ConnectionResult connectionResult) {
            if (zzd.this.xA != null) {
                zzd.this.xA.onConnectionFailed(connectionResult);
            }
            zzd.this.onConnectionFailed(connectionResult);
        }
    }

    protected final class zzk extends zza {
        @BinderThread
        public zzk(int i, Bundle bundle) {
            super(i, bundle);
        }

        /* access modifiers changed from: protected */
        public boolean zzasd() {
            zzd.this.xu.zzh(ConnectionResult.rb);
            return true;
        }

        /* access modifiers changed from: protected */
        public void zzl(ConnectionResult connectionResult) {
            zzd.this.xu.zzh(connectionResult);
            zzd.this.onConnectionFailed(connectionResult);
        }
    }

    protected zzd(Context context, Looper looper, int i, zzb zzb2, zzc zzc2, String str) {
        this(context, looper, zzm.zzce(context), com.google.android.gms.common.zzc.zzang(), i, (zzb) zzab.zzy(zzb2), (zzc) zzab.zzy(zzc2), str);
    }

    protected zzd(Context context, Looper looper, zzm zzm, com.google.android.gms.common.zzc zzc2, int i, zzb zzb2, zzc zzc3, String str) {
        this.zzail = new Object();
        this.xs = new Object();
        this.xw = new ArrayList<>();
        this.xy = 1;
        this.xD = new AtomicInteger(0);
        this.mContext = (Context) zzab.zzb(context, (Object) "Context must not be null");
        this.zzahv = (Looper) zzab.zzb(looper, (Object) "Looper must not be null");
        this.xr = (zzm) zzab.zzb(zzm, (Object) "Supervisor must not be null");
        this.tz = (com.google.android.gms.common.zzc) zzab.zzb(zzc2, (Object) "API availability must not be null");
        this.mHandler = new C0029zzd(looper);
        this.xB = i;
        this.xz = zzb2;
        this.xA = zzc3;
        this.xC = str;
    }

    /* access modifiers changed from: private */
    public boolean zza(int i, int i2, T t) {
        boolean z;
        synchronized (this.zzail) {
            if (this.xy != i) {
                z = false;
            } else {
                zzb(i2, t);
                z = true;
            }
        }
        return z;
    }

    private void zzarv() {
        if (this.xx != null) {
            String valueOf = String.valueOf(zzqz());
            String valueOf2 = String.valueOf(zzart());
            Log.e("GmsClient", new StringBuilder(String.valueOf(valueOf).length() + 70 + String.valueOf(valueOf2).length()).append("Calling connect() while still connected, missing disconnect() for ").append(valueOf).append(" on ").append(valueOf2).toString());
            this.xr.zzb(zzqz(), zzart(), this.xx, zzaru());
            this.xD.incrementAndGet();
        }
        this.xx = new zzh(this.xD.get());
        if (!this.xr.zza(zzqz(), zzart(), this.xx, zzaru())) {
            String valueOf3 = String.valueOf(zzqz());
            String valueOf4 = String.valueOf(zzart());
            Log.e("GmsClient", new StringBuilder(String.valueOf(valueOf3).length() + 34 + String.valueOf(valueOf4).length()).append("unable to connect to service: ").append(valueOf3).append(" on ").append(valueOf4).toString());
            zza(16, (Bundle) null, this.xD.get());
        }
    }

    private void zzarw() {
        if (this.xx != null) {
            this.xr.zzb(zzqz(), zzart(), this.xx, zzaru());
            this.xx = null;
        }
    }

    /* access modifiers changed from: private */
    public void zzb(int i, T t) {
        boolean z = true;
        if ((i == 3) != (t != null)) {
            z = false;
        }
        zzab.zzbo(z);
        synchronized (this.zzail) {
            this.xy = i;
            this.xv = t;
            zzc(i, t);
            switch (i) {
                case 1:
                    zzarw();
                    break;
                case 2:
                    zzarv();
                    break;
                case 3:
                    zza(t);
                    break;
            }
        }
    }

    public void disconnect() {
        this.xD.incrementAndGet();
        synchronized (this.xw) {
            int size = this.xw.size();
            for (int i = 0; i < size; i++) {
                ((zze) this.xw.get(i)).zzasg();
            }
            this.xw.clear();
        }
        synchronized (this.xs) {
            this.xt = null;
        }
        zzb(1, null);
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i;
        T t;
        synchronized (this.zzail) {
            i = this.xy;
            t = this.xv;
        }
        printWriter.append(str).append("mConnectState=");
        switch (i) {
            case 1:
                printWriter.print("DISCONNECTED");
                break;
            case 2:
                printWriter.print("CONNECTING");
                break;
            case 3:
                printWriter.print("CONNECTED");
                break;
            case 4:
                printWriter.print("DISCONNECTING");
                break;
            default:
                printWriter.print("UNKNOWN");
                break;
        }
        printWriter.append(" mService=");
        if (t == null) {
            printWriter.println("null");
        } else {
            printWriter.append(zzra()).append("@").println(Integer.toHexString(System.identityHashCode(t.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.xo > 0) {
            PrintWriter append = printWriter.append(str).append("lastConnectedTime=");
            long j = this.xo;
            String valueOf = String.valueOf(simpleDateFormat.format(new Date(this.xo)));
            append.println(new StringBuilder(String.valueOf(valueOf).length() + 21).append(j).append(" ").append(valueOf).toString());
        }
        if (this.xn > 0) {
            printWriter.append(str).append("lastSuspendedCause=");
            switch (this.xm) {
                case 1:
                    printWriter.append("CAUSE_SERVICE_DISCONNECTED");
                    break;
                case 2:
                    printWriter.append("CAUSE_NETWORK_LOST");
                    break;
                default:
                    printWriter.append(String.valueOf(this.xm));
                    break;
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j2 = this.xn;
            String valueOf2 = String.valueOf(simpleDateFormat.format(new Date(this.xn)));
            append2.println(new StringBuilder(String.valueOf(valueOf2).length() + 21).append(j2).append(" ").append(valueOf2).toString());
        }
        if (this.xq > 0) {
            printWriter.append(str).append("lastFailedStatus=").append(CommonStatusCodes.getStatusCodeString(this.xp));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j3 = this.xq;
            String valueOf3 = String.valueOf(simpleDateFormat.format(new Date(this.xq)));
            append3.println(new StringBuilder(String.valueOf(valueOf3).length() + 21).append(j3).append(" ").append(valueOf3).toString());
        }
    }

    public Account getAccount() {
        return null;
    }

    public final Context getContext() {
        return this.mContext;
    }

    public final Looper getLooper() {
        return this.zzahv;
    }

    public boolean isConnected() {
        boolean z;
        synchronized (this.zzail) {
            z = this.xy == 3;
        }
        return z;
    }

    public boolean isConnecting() {
        boolean z;
        synchronized (this.zzail) {
            z = this.xy == 2;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onConnectionFailed(ConnectionResult connectionResult) {
        this.xp = connectionResult.getErrorCode();
        this.xq = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onConnectionSuspended(int i) {
        this.xm = i;
        this.xn = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public void zza(int i, @Nullable Bundle bundle, int i2) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(5, i2, -1, new zzk(i, bundle)));
    }

    /* access modifiers changed from: protected */
    @BinderThread
    public void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(1, i2, -1, new zzj(i, iBinder, bundle)));
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void zza(@NonNull T t) {
        this.xo = System.currentTimeMillis();
    }

    public void zza(@NonNull zzf zzf2) {
        this.xu = (zzf) zzab.zzb(zzf2, (Object) "Connection progress callbacks cannot be null.");
        zzb(2, null);
    }

    public void zza(zzf zzf2, ConnectionResult connectionResult) {
        this.xu = (zzf) zzab.zzb(zzf2, (Object) "Connection progress callbacks cannot be null.");
        this.mHandler.sendMessage(this.mHandler.obtainMessage(3, this.xD.get(), connectionResult.getErrorCode(), connectionResult.getResolution()));
    }

    @WorkerThread
    public void zza(zzq zzq, Set<Scope> set) {
        try {
            GetServiceRequest zzn = new GetServiceRequest(this.xB).zzhl(this.mContext.getPackageName()).zzn(zzaeu());
            if (set != null) {
                zzn.zzf(set);
            }
            if (zzafk()) {
                zzn.zzd(zzary()).zzb(zzq);
            } else if (zzasb()) {
                zzn.zzd(getAccount());
            }
            synchronized (this.xs) {
                if (this.xt != null) {
                    this.xt.zza((zzt) new zzg(this, this.xD.get()), zzn);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e) {
            Log.w("GmsClient", "service died");
            zzgc(1);
        } catch (RemoteException e2) {
            Log.w("GmsClient", "Remote exception occurred", e2);
        }
    }

    /* access modifiers changed from: protected */
    public Bundle zzaeu() {
        return new Bundle();
    }

    public boolean zzafk() {
        return false;
    }

    public boolean zzafz() {
        return false;
    }

    public Intent zzaga() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    public Bundle zzamh() {
        return null;
    }

    public boolean zzanu() {
        return true;
    }

    @Nullable
    public IBinder zzanv() {
        IBinder asBinder;
        synchronized (this.xs) {
            asBinder = this.xt == null ? null : this.xt.asBinder();
        }
        return asBinder;
    }

    /* access modifiers changed from: protected */
    public String zzart() {
        return "com.google.android.gms";
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final String zzaru() {
        return this.xC == null ? this.mContext.getClass().getName() : this.xC;
    }

    public void zzarx() {
        int isGooglePlayServicesAvailable = this.tz.isGooglePlayServicesAvailable(this.mContext);
        if (isGooglePlayServicesAvailable != 0) {
            zzb(1, null);
            this.xu = new zzi();
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, this.xD.get(), isGooglePlayServicesAvailable));
            return;
        }
        zza((zzf) new zzi());
    }

    public final Account zzary() {
        return getAccount() != null ? getAccount() : new Account("<<default account>>", "com.google");
    }

    /* access modifiers changed from: protected */
    public final void zzarz() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public final T zzasa() throws DeadObjectException {
        T t;
        synchronized (this.zzail) {
            if (this.xy == 4) {
                throw new DeadObjectException();
            }
            zzarz();
            zzab.zza(this.xv != null, (Object) "Client is connected but service is null");
            t = this.xv;
        }
        return t;
    }

    public boolean zzasb() {
        return false;
    }

    /* access modifiers changed from: protected */
    public Set<Scope> zzasc() {
        return Collections.EMPTY_SET;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T zzbb(IBinder iBinder);

    /* access modifiers changed from: 0000 */
    public void zzc(int i, T t) {
    }

    public void zzgc(int i) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, this.xD.get(), i));
    }

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String zzqz();

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String zzra();
}
