package com.google.android.gms.common.data;

import java.util.ArrayList;

public abstract class zzf<T> extends AbstractDataBuffer<T> {
    private boolean wo = false;
    private ArrayList<Integer> wp;

    protected zzf(DataHolder dataHolder) {
        super(dataHolder);
    }

    private void zzarl() {
        synchronized (this) {
            if (!this.wo) {
                int count = this.tu.getCount();
                this.wp = new ArrayList<>();
                if (count > 0) {
                    this.wp.add(Integer.valueOf(0));
                    String zzark = zzark();
                    String zzd = this.tu.zzd(zzark, 0, this.tu.zzfs(0));
                    int i = 1;
                    while (i < count) {
                        int zzfs = this.tu.zzfs(i);
                        String zzd2 = this.tu.zzd(zzark, i, zzfs);
                        if (zzd2 == null) {
                            throw new NullPointerException(new StringBuilder(String.valueOf(zzark).length() + 78).append("Missing value for markerColumn: ").append(zzark).append(", at row: ").append(i).append(", for window: ").append(zzfs).toString());
                        }
                        if (!zzd2.equals(zzd)) {
                            this.wp.add(Integer.valueOf(i));
                        } else {
                            zzd2 = zzd;
                        }
                        i++;
                        zzd = zzd2;
                    }
                }
                this.wo = true;
            }
        }
    }

    public final T get(int i) {
        zzarl();
        return zzl(zzfw(i), zzfx(i));
    }

    public int getCount() {
        zzarl();
        return this.wp.size();
    }

    /* access modifiers changed from: protected */
    public abstract String zzark();

    /* access modifiers changed from: protected */
    public String zzarm() {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public int zzfw(int i) {
        if (i >= 0 && i < this.wp.size()) {
            return ((Integer) this.wp.get(i)).intValue();
        }
        throw new IllegalArgumentException("Position " + i + " is out of bounds for this buffer");
    }

    /* access modifiers changed from: protected */
    public int zzfx(int i) {
        if (i < 0 || i == this.wp.size()) {
            return 0;
        }
        int intValue = i == this.wp.size() + -1 ? this.tu.getCount() - ((Integer) this.wp.get(i)).intValue() : ((Integer) this.wp.get(i + 1)).intValue() - ((Integer) this.wp.get(i)).intValue();
        if (intValue != 1) {
            return intValue;
        }
        int zzfw = zzfw(i);
        int zzfs = this.tu.zzfs(zzfw);
        String zzarm = zzarm();
        if (zzarm == null || this.tu.zzd(zzarm, zzfw, zzfs) != null) {
            return intValue;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public abstract T zzl(int i, int i2);
}
