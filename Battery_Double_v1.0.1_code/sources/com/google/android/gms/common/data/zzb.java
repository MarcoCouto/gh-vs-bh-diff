package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.zzab;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class zzb<T> implements Iterator<T> {
    protected final DataBuffer<T> vU;
    protected int vV = -1;

    public zzb(DataBuffer<T> dataBuffer) {
        this.vU = (DataBuffer) zzab.zzy(dataBuffer);
    }

    public boolean hasNext() {
        return this.vV < this.vU.getCount() + -1;
    }

    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException("Cannot advance the iterator beyond " + this.vV);
        }
        DataBuffer<T> dataBuffer = this.vU;
        int i = this.vV + 1;
        this.vV = i;
        return dataBuffer.get(i);
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
