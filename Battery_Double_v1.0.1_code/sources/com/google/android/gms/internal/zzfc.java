package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzu;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzin
public class zzfc implements Iterable<zzfb> {
    private final List<zzfb> zzbje = new LinkedList();

    private zzfb zzf(zzlh zzlh) {
        Iterator it = zzu.zzgj().iterator();
        while (it.hasNext()) {
            zzfb zzfb = (zzfb) it.next();
            if (zzfb.zzbgf == zzlh) {
                return zzfb;
            }
        }
        return null;
    }

    public Iterator<zzfb> iterator() {
        return this.zzbje.iterator();
    }

    public void zza(zzfb zzfb) {
        this.zzbje.add(zzfb);
    }

    public void zzb(zzfb zzfb) {
        this.zzbje.remove(zzfb);
    }

    public boolean zzd(zzlh zzlh) {
        zzfb zzf = zzf(zzlh);
        if (zzf == null) {
            return false;
        }
        zzf.zzbjb.abort();
        return true;
    }

    public boolean zze(zzlh zzlh) {
        return zzf(zzlh) != null;
    }

    public int zzlk() {
        return this.zzbje.size();
    }
}
