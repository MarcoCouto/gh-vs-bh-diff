package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.internal.zzdr.zza;
import java.util.ArrayList;
import java.util.List;

@zzin
public class zzdw extends NativeAppInstallAd {
    private final zzdv zzbhc;
    private final List<Image> zzbhd = new ArrayList();
    private final zzds zzbhe;

    public zzdw(zzdv zzdv) {
        zzds zzds;
        this.zzbhc = zzdv;
        try {
            List<Object> images = this.zzbhc.getImages();
            if (images != null) {
                for (Object zze : images) {
                    zzdr zze2 = zze(zze);
                    if (zze2 != null) {
                        this.zzbhd.add(new zzds(zze2));
                    }
                }
            }
        } catch (RemoteException e) {
            zzb.zzb("Failed to get image.", e);
        }
        try {
            zzdr zzku = this.zzbhc.zzku();
            if (zzku != null) {
                zzds = new zzds(zzku);
                this.zzbhe = zzds;
            }
        } catch (RemoteException e2) {
            zzb.zzb("Failed to get icon.", e2);
        }
        zzds = null;
        this.zzbhe = zzds;
    }

    public void destroy() {
        try {
            this.zzbhc.destroy();
        } catch (RemoteException e) {
            zzb.zzb("Failed to destroy", e);
        }
    }

    public CharSequence getBody() {
        try {
            return this.zzbhc.getBody();
        } catch (RemoteException e) {
            zzb.zzb("Failed to get body.", e);
            return null;
        }
    }

    public CharSequence getCallToAction() {
        try {
            return this.zzbhc.getCallToAction();
        } catch (RemoteException e) {
            zzb.zzb("Failed to get call to action.", e);
            return null;
        }
    }

    public Bundle getExtras() {
        try {
            return this.zzbhc.getExtras();
        } catch (RemoteException e) {
            zzb.zzb("Failed to get extras", e);
            return null;
        }
    }

    public CharSequence getHeadline() {
        try {
            return this.zzbhc.getHeadline();
        } catch (RemoteException e) {
            zzb.zzb("Failed to get headline.", e);
            return null;
        }
    }

    public Image getIcon() {
        return this.zzbhe;
    }

    public List<Image> getImages() {
        return this.zzbhd;
    }

    public CharSequence getPrice() {
        try {
            return this.zzbhc.getPrice();
        } catch (RemoteException e) {
            zzb.zzb("Failed to get price.", e);
            return null;
        }
    }

    public Double getStarRating() {
        try {
            double starRating = this.zzbhc.getStarRating();
            if (starRating == -1.0d) {
                return null;
            }
            return Double.valueOf(starRating);
        } catch (RemoteException e) {
            zzb.zzb("Failed to get star rating.", e);
            return null;
        }
    }

    public CharSequence getStore() {
        try {
            return this.zzbhc.getStore();
        } catch (RemoteException e) {
            zzb.zzb("Failed to get store", e);
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public zzdr zze(Object obj) {
        if (obj instanceof IBinder) {
            return zza.zzy((IBinder) obj);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzkv */
    public zzd zzdg() {
        try {
            return this.zzbhc.zzkv();
        } catch (RemoteException e) {
            zzb.zzb("Failed to retrieve native ad engine.", e);
            return null;
        }
    }
}
