package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza.C0023zza;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzin
public class zzjj extends C0023zza {
    private zzjk zzchn;
    private zzjh zzchu;
    private zzji zzchv;

    public zzjj(zzji zzji) {
        this.zzchv = zzji;
    }

    public void zza(zzd zzd, RewardItemParcel rewardItemParcel) {
        if (this.zzchv != null) {
            this.zzchv.zzc(rewardItemParcel);
        }
    }

    public void zza(zzjh zzjh) {
        this.zzchu = zzjh;
    }

    public void zza(zzjk zzjk) {
        this.zzchn = zzjk;
    }

    public void zzb(zzd zzd, int i) {
        if (this.zzchu != null) {
            this.zzchu.zzaw(i);
        }
    }

    public void zzc(zzd zzd, int i) {
        if (this.zzchn != null) {
            this.zzchn.zza(zze.zzad(zzd).getClass().getName(), i);
        }
    }

    public void zzp(zzd zzd) {
        if (this.zzchu != null) {
            this.zzchu.zzrs();
        }
    }

    public void zzq(zzd zzd) {
        if (this.zzchn != null) {
            this.zzchn.zzcg(zze.zzad(zzd).getClass().getName());
        }
    }

    public void zzr(zzd zzd) {
        if (this.zzchv != null) {
            this.zzchv.onRewardedVideoAdOpened();
        }
    }

    public void zzs(zzd zzd) {
        if (this.zzchv != null) {
            this.zzchv.onRewardedVideoStarted();
        }
    }

    public void zzt(zzd zzd) {
        if (this.zzchv != null) {
            this.zzchv.onRewardedVideoAdClosed();
        }
    }

    public void zzu(zzd zzd) {
        if (this.zzchv != null) {
            this.zzchv.zzrr();
        }
    }

    public void zzv(zzd zzd) {
        if (this.zzchv != null) {
            this.zzchv.onRewardedVideoAdLeftApplication();
        }
    }
}
