package com.google.android.gms.internal;

public final class zzaob implements zzani {
    private final zzanp bdR;

    public zzaob(zzanp zzanp) {
        this.bdR = zzanp;
    }

    static zzanh<?> zza(zzanp zzanp, zzamp zzamp, zzaol<?> zzaol, zzanj zzanj) {
        Class value = zzanj.value();
        if (zzanh.class.isAssignableFrom(value)) {
            return (zzanh) zzanp.zzb(zzaol.zzr(value)).zzczu();
        }
        if (zzani.class.isAssignableFrom(value)) {
            return ((zzani) zzanp.zzb(zzaol.zzr(value)).zzczu()).zza(zzamp, zzaol);
        }
        throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
    }

    public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
        zzanj zzanj = (zzanj) zzaol.m().getAnnotation(zzanj.class);
        if (zzanj == null) {
            return null;
        }
        return zza(this.bdR, zzamp, zzaol, zzanj);
    }
}
