package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

@zzin
public class zzkx {

    public interface zza<D, R> {
        R apply(D d);
    }

    public static <A, B> zzky<B> zza(final zzky<A> zzky, final zza<A, B> zza2) {
        final zzkv zzkv = new zzkv();
        zzky.zzc(new Runnable() {
            public void run() {
                try {
                    zzkv.this.zzh(zza2.apply(zzky.get()));
                } catch (InterruptedException | CancellationException | ExecutionException e) {
                    zzkv.this.cancel(true);
                }
            }
        });
        return zzkv;
    }

    public static <V> zzky<List<V>> zzn(final List<zzky<V>> list) {
        final zzkv zzkv = new zzkv();
        final int size = list.size();
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        for (zzky zzc : list) {
            zzc.zzc(new Runnable() {
                public void run() {
                    if (atomicInteger.incrementAndGet() >= size) {
                        try {
                            zzkv.zzh(zzkx.zzo(list));
                        } catch (InterruptedException | ExecutionException e) {
                            zzkd.zzd("Unable to convert list of futures to a future of list", e);
                        }
                    }
                }
            });
        }
        return zzkv;
    }

    /* access modifiers changed from: private */
    public static <V> List<V> zzo(List<zzky<V>> list) throws ExecutionException, InterruptedException {
        ArrayList arrayList = new ArrayList();
        for (zzky zzky : list) {
            Object obj = zzky.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }
}
