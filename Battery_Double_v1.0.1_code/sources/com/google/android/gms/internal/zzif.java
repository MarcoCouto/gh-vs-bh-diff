package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.zzn;
import com.google.android.gms.internal.zzju.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@zzin
public class zzif extends zzib {
    private final zzdk zzajn;
    private zzgj zzajz;
    /* access modifiers changed from: private */
    public final zzlh zzbgf;
    private zzga zzboe;
    zzfy zzbym;
    protected zzge zzbyn;
    /* access modifiers changed from: private */
    public boolean zzbyo;

    zzif(Context context, zza zza, zzgj zzgj, zzic.zza zza2, zzdk zzdk, zzlh zzlh) {
        super(context, zza, zza2);
        this.zzajz = zzgj;
        this.zzboe = zza.zzcig;
        this.zzajn = zzdk;
        this.zzbgf = zzlh;
    }

    private static String zza(zzge zzge) {
        String str = zzge.zzbon.zzbmx;
        int zzal = zzal(zzge.zzbom);
        return new StringBuilder(String.valueOf(str).length() + 33).append(str).append(".").append(zzal).append(".").append(zzge.zzbos).toString();
    }

    private static int zzal(int i) {
        switch (i) {
            case -1:
                return 4;
            case 0:
                return 0;
            case 1:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 5;
            default:
                return 6;
        }
    }

    private static String zzg(List<zzge> list) {
        String str = "";
        if (list == null) {
            return str.toString();
        }
        String str2 = str;
        for (zzge zzge : list) {
            if (!(zzge == null || zzge.zzbon == null || TextUtils.isEmpty(zzge.zzbon.zzbmx))) {
                String valueOf = String.valueOf(str2);
                String valueOf2 = String.valueOf(zza(zzge));
                str2 = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append(valueOf2).append("_").toString();
            }
        }
        return str2.substring(0, Math.max(0, str2.length() - 1));
    }

    private void zzqf() throws zza {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                synchronized (zzif.this.zzbxu) {
                    zzif.this.zzbyo = zzn.zza(zzif.this.zzbgf, zzif.this.zzbyn, countDownLatch);
                }
            }
        });
        try {
            countDownLatch.await(10, TimeUnit.SECONDS);
            synchronized (this.zzbxu) {
                if (!this.zzbyo) {
                    throw new zza("View could not be prepared", 0);
                } else if (this.zzbgf.isDestroyed()) {
                    throw new zza("Assets not loaded, web view is destroyed", 0);
                }
            }
        } catch (InterruptedException e) {
            String valueOf = String.valueOf(e);
            throw new zza(new StringBuilder(String.valueOf(valueOf).length() + 38).append("Interrupted while waiting for latch : ").append(valueOf).toString(), 0);
        }
    }

    public void onStop() {
        synchronized (this.zzbxu) {
            super.onStop();
            if (this.zzbym != null) {
                this.zzbym.cancel();
            }
        }
    }

    /* access modifiers changed from: protected */
    public zzju zzak(int i) {
        AdRequestInfoParcel adRequestInfoParcel = this.zzbxr.zzcip;
        return new zzju(adRequestInfoParcel.zzcar, this.zzbgf, this.zzbxs.zzbnm, i, this.zzbxs.zzbnn, this.zzbxs.zzcca, this.zzbxs.orientation, this.zzbxs.zzbns, adRequestInfoParcel.zzcau, this.zzbxs.zzcby, this.zzbyn != null ? this.zzbyn.zzbon : null, this.zzbyn != null ? this.zzbyn.zzboo : null, this.zzbyn != null ? this.zzbyn.zzbop : AdMobAdapter.class.getName(), this.zzboe, this.zzbyn != null ? this.zzbyn.zzboq : null, this.zzbxs.zzcbz, this.zzbxr.zzapa, this.zzbxs.zzcbx, this.zzbxr.zzcik, this.zzbxs.zzccc, this.zzbxs.zzccd, this.zzbxr.zzcie, null, this.zzbxs.zzccn, this.zzbxs.zzcco, this.zzbxs.zzccp, this.zzboe != null ? this.zzboe.zzbnx : false, this.zzbxs.zzccr, this.zzbym != null ? zzg(this.zzbym.zzmg()) : null, this.zzbxs.zzbnp);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0084  */
    public void zzh(long j) throws zza {
        boolean z;
        synchronized (this.zzbxu) {
            this.zzbym = zzi(j);
        }
        ArrayList arrayList = new ArrayList(this.zzboe.zzbnk);
        Bundle bundle = this.zzbxr.zzcip.zzcar.zzatw;
        String str = "com.google.ads.mediation.admob.AdMobAdapter";
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle(str);
            if (bundle2 != null) {
                z = bundle2.getBoolean("_skipMediation");
                if (z) {
                    ListIterator listIterator = arrayList.listIterator();
                    while (listIterator.hasNext()) {
                        if (!((zzfz) listIterator.next()).zzbmw.contains(str)) {
                            listIterator.remove();
                        }
                    }
                }
                this.zzbyn = this.zzbym.zzd(arrayList);
                switch (this.zzbyn.zzbom) {
                    case 0:
                        if (this.zzbyn.zzbon != null && this.zzbyn.zzbon.zzbnf != null) {
                            zzqf();
                            return;
                        }
                        return;
                    case 1:
                        throw new zza("No fill from any mediation ad networks.", 3);
                    default:
                        throw new zza("Unexpected mediation result: " + this.zzbyn.zzbom, 0);
                }
            }
        }
        z = false;
        if (z) {
        }
        this.zzbyn = this.zzbym.zzd(arrayList);
        switch (this.zzbyn.zzbom) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    public zzfy zzi(long j) {
        if (this.zzboe.zzbnv != -1) {
            return new zzgg(this.mContext, this.zzbxr.zzcip, this.zzajz, this.zzboe, this.zzbxs.zzauu, this.zzbxs.zzauw, j, ((Long) zzdc.zzbbh.get()).longValue(), 2);
        }
        return new zzgh(this.mContext, this.zzbxr.zzcip, this.zzajz, this.zzboe, this.zzbxs.zzauu, this.zzbxs.zzauw, j, ((Long) zzdc.zzbbh.get()).longValue(), this.zzajn);
    }
}
