package com.google.android.gms.internal;

import com.google.android.gms.internal.zzah.zzf;
import com.google.android.gms.internal.zzah.zzj;
import java.io.IOException;

public interface zzadu {

    public static final class zza extends zzapp<zza> {
        public long aCV;
        public zzj aCW;
        public zzf zzwr;

        public zza() {
            zzcgx();
        }

        public static zza zzao(byte[] bArr) throws zzapu {
            return (zza) zzapv.zza(new zza(), bArr);
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.aCV != zza.aCV) {
                return false;
            }
            if (this.zzwr == null) {
                if (zza.zzwr != null) {
                    return false;
                }
            } else if (!this.zzwr.equals(zza.zzwr)) {
                return false;
            }
            if (this.aCW == null) {
                if (zza.aCW != null) {
                    return false;
                }
            } else if (!this.aCW.equals(zza.aCW)) {
                return false;
            }
            return (this.bjx == null || this.bjx.isEmpty()) ? zza.bjx == null || zza.bjx.isEmpty() : this.bjx.equals(zza.bjx);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.aCW == null ? 0 : this.aCW.hashCode()) + (((this.zzwr == null ? 0 : this.zzwr.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + ((int) (this.aCV ^ (this.aCV >>> 32)))) * 31)) * 31)) * 31;
            if (this.bjx != null && !this.bjx.isEmpty()) {
                i = this.bjx.hashCode();
            }
            return hashCode + i;
        }

        public void zza(zzapo zzapo) throws IOException {
            zzapo.zzb(1, this.aCV);
            if (this.zzwr != null) {
                zzapo.zza(2, (zzapv) this.zzwr);
            }
            if (this.aCW != null) {
                zzapo.zza(3, (zzapv) this.aCW);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzas */
        public zza zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 8:
                        this.aCV = zzapn.ak();
                        continue;
                    case 18:
                        if (this.zzwr == null) {
                            this.zzwr = new zzf();
                        }
                        zzapn.zza(this.zzwr);
                        continue;
                    case 26:
                        if (this.aCW == null) {
                            this.aCW = new zzj();
                        }
                        zzapn.zza(this.aCW);
                        continue;
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public zza zzcgx() {
            this.aCV = 0;
            this.zzwr = null;
            this.aCW = null;
            this.bjx = null;
            this.bjG = -1;
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx() + zzapo.zze(1, this.aCV);
            if (this.zzwr != null) {
                zzx += zzapo.zzc(2, (zzapv) this.zzwr);
            }
            return this.aCW != null ? zzx + zzapo.zzc(3, (zzapv) this.aCW) : zzx;
        }
    }
}
