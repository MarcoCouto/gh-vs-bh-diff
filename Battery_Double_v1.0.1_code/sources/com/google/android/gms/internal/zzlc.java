package com.google.android.gms.internal;

import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;

@zzin
public class zzlc {
    public void zza(View view, OnGlobalLayoutListener onGlobalLayoutListener) {
        new zzld(view, onGlobalLayoutListener).zzua();
    }

    public void zza(View view, OnScrollChangedListener onScrollChangedListener) {
        new zzle(view, onScrollChangedListener).zzua();
    }
}
