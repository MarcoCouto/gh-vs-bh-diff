package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class zzaog implements zzani {
    private final zzanp bdR;
    private final zzanq bea;
    private final zzamo bec;

    public static final class zza<T> extends zzanh<T> {
        private final Map<String, zzb> bfR;
        private final zzanu<T> bfy;

        private zza(zzanu<T> zzanu, Map<String, zzb> map) {
            this.bfy = zzanu;
            this.bfR = map;
        }

        public void zza(zzaoo zzaoo, T t) throws IOException {
            if (t == null) {
                zzaoo.l();
                return;
            }
            zzaoo.j();
            try {
                for (zzb zzb : this.bfR.values()) {
                    if (zzb.zzco(t)) {
                        zzaoo.zztr(zzb.name);
                        zzb.zza(zzaoo, (Object) t);
                    }
                }
                zzaoo.k();
            } catch (IllegalAccessException e) {
                throw new AssertionError();
            }
        }

        public T zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            Object zzczu = this.bfy.zzczu();
            try {
                zzaom.beginObject();
                while (zzaom.hasNext()) {
                    zzb zzb = (zzb) this.bfR.get(zzaom.nextName());
                    if (zzb == null || !zzb.bfT) {
                        zzaom.skipValue();
                    } else {
                        zzb.zza(zzaom, zzczu);
                    }
                }
                zzaom.endObject();
                return zzczu;
            } catch (IllegalStateException e) {
                throw new zzane((Throwable) e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    static abstract class zzb {
        final boolean bfS;
        final boolean bfT;
        final String name;

        protected zzb(String str, boolean z, boolean z2) {
            this.name = str;
            this.bfS = z;
            this.bfT = z2;
        }

        /* access modifiers changed from: 0000 */
        public abstract void zza(zzaom zzaom, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: 0000 */
        public abstract void zza(zzaoo zzaoo, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: 0000 */
        public abstract boolean zzco(Object obj) throws IOException, IllegalAccessException;
    }

    public zzaog(zzanp zzanp, zzamo zzamo, zzanq zzanq) {
        this.bdR = zzanp;
        this.bec = zzamo;
        this.bea = zzanq;
    }

    /* access modifiers changed from: private */
    public zzanh<?> zza(zzamp zzamp, Field field, zzaol<?> zzaol) {
        zzanj zzanj = (zzanj) field.getAnnotation(zzanj.class);
        if (zzanj != null) {
            zzanh<?> zza2 = zzaob.zza(this.bdR, zzamp, zzaol, zzanj);
            if (zza2 != null) {
                return zza2;
            }
        }
        return zzamp.zza(zzaol);
    }

    private zzb zza(zzamp zzamp, Field field, String str, zzaol<?> zzaol, boolean z, boolean z2) {
        final boolean zzk = zzanv.zzk(zzaol.m());
        final zzamp zzamp2 = zzamp;
        final Field field2 = field;
        final zzaol<?> zzaol2 = zzaol;
        return new zzb(str, z, z2) {
            final zzanh<?> bfL = zzaog.this.zza(zzamp2, field2, zzaol2);

            /* access modifiers changed from: 0000 */
            public void zza(zzaom zzaom, Object obj) throws IOException, IllegalAccessException {
                Object zzb = this.bfL.zzb(zzaom);
                if (zzb != null || !zzk) {
                    field2.set(obj, zzb);
                }
            }

            /* access modifiers changed from: 0000 */
            public void zza(zzaoo zzaoo, Object obj) throws IOException, IllegalAccessException {
                new zzaoj(zzamp2, this.bfL, zzaol2.n()).zza(zzaoo, field2.get(obj));
            }

            public boolean zzco(Object obj) throws IOException, IllegalAccessException {
                return this.bfS && field2.get(obj) != obj;
            }
        };
    }

    static List<String> zza(zzamo zzamo, Field field) {
        zzank zzank = (zzank) field.getAnnotation(zzank.class);
        LinkedList linkedList = new LinkedList();
        if (zzank == null) {
            linkedList.add(zzamo.zzc(field));
        } else {
            linkedList.add(zzank.value());
            for (String add : zzank.zzczs()) {
                linkedList.add(add);
            }
        }
        return linkedList;
    }

    private Map<String, zzb> zza(zzamp zzamp, zzaol<?> zzaol, Class<?> cls) {
        Field[] declaredFields;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type n = zzaol.n();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean zza2 = zza(field, true);
                boolean zza3 = zza(field, false);
                if (zza2 || zza3) {
                    field.setAccessible(true);
                    Type zza4 = zzano.zza(zzaol.n(), cls, field.getGenericType());
                    List zzd = zzd(field);
                    zzb zzb2 = null;
                    int i = 0;
                    while (i < zzd.size()) {
                        String str = (String) zzd.get(i);
                        if (i != 0) {
                            zza2 = false;
                        }
                        zzb zzb3 = (zzb) linkedHashMap.put(str, zza(zzamp, field, str, zzaol.zzl(zza4), zza2, zza3));
                        if (zzb2 != null) {
                            zzb3 = zzb2;
                        }
                        i++;
                        zzb2 = zzb3;
                    }
                    if (zzb2 != null) {
                        String valueOf = String.valueOf(n);
                        String str2 = zzb2.name;
                        throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 37 + String.valueOf(str2).length()).append(valueOf).append(" declares multiple JSON fields named ").append(str2).toString());
                    }
                }
            }
            zzaol = zzaol.zzl(zzano.zza(zzaol.n(), cls, cls.getGenericSuperclass()));
            cls = zzaol.m();
        }
        return linkedHashMap;
    }

    static boolean zza(Field field, boolean z, zzanq zzanq) {
        return !zzanq.zza(field.getType(), z) && !zzanq.zza(field, z);
    }

    private List<String> zzd(Field field) {
        return zza(this.bec, field);
    }

    public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
        Class m = zzaol.m();
        if (!Object.class.isAssignableFrom(m)) {
            return null;
        }
        return new zza(this.bdR.zzb(zzaol), zza(zzamp, zzaol, m));
    }

    public boolean zza(Field field, boolean z) {
        return zza(field, z, this.bea);
    }
}
