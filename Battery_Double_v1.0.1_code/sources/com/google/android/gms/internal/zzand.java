package com.google.android.gms.internal;

import java.lang.reflect.Type;

public interface zzand<T> {
    zzamv zza(T t, Type type, zzanc zzanc);
}
