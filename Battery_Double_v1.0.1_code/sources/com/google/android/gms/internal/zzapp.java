package com.google.android.gms.internal;

import com.google.android.gms.internal.zzapp;
import java.io.IOException;

public abstract class zzapp<M extends zzapp<M>> extends zzapv {
    protected zzapr bjx;

    /* renamed from: aA */
    public M clone() throws CloneNotSupportedException {
        M m = (zzapp) super.clone();
        zzapt.zza(this, (zzapp) m);
        return m;
    }

    public /* synthetic */ zzapv aB() throws CloneNotSupportedException {
        return (zzapp) clone();
    }

    public final <T> T zza(zzapq<M, T> zzapq) {
        if (this.bjx == null) {
            return null;
        }
        zzaps zzagf = this.bjx.zzagf(zzapy.zzagj(zzapq.tag));
        if (zzagf != null) {
            return zzagf.zzb(zzapq);
        }
        return null;
    }

    public void zza(zzapo zzapo) throws IOException {
        if (this.bjx != null) {
            for (int i = 0; i < this.bjx.size(); i++) {
                this.bjx.zzagg(i).zza(zzapo);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzapn zzapn, int i) throws IOException {
        int position = zzapn.getPosition();
        if (!zzapn.zzafp(i)) {
            return false;
        }
        int zzagj = zzapy.zzagj(i);
        zzapx zzapx = new zzapx(i, zzapn.zzad(position, zzapn.getPosition() - position));
        zzaps zzaps = null;
        if (this.bjx == null) {
            this.bjx = new zzapr();
        } else {
            zzaps = this.bjx.zzagf(zzagj);
        }
        if (zzaps == null) {
            zzaps = new zzaps();
            this.bjx.zza(zzagj, zzaps);
        }
        zzaps.zza(zzapx);
        return true;
    }

    /* access modifiers changed from: protected */
    public int zzx() {
        if (this.bjx == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.bjx.size(); i2++) {
            i += this.bjx.zzagg(i2).zzx();
        }
        return i;
    }
}
