package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzab;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class zzpk extends zzpn {
    private final SparseArray<zza> sC = new SparseArray<>();

    private class zza implements OnConnectionFailedListener {
        public final int sD;
        public final GoogleApiClient sE;
        public final OnConnectionFailedListener sF;

        public zza(int i, GoogleApiClient googleApiClient, OnConnectionFailedListener onConnectionFailedListener) {
            this.sD = i;
            this.sE = googleApiClient;
            this.sF = onConnectionFailedListener;
            googleApiClient.registerConnectionFailedListener(this);
        }

        public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.append(str).append("GoogleApiClient #").print(this.sD);
            printWriter.println(":");
            this.sE.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        }

        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 27).append("beginFailureResolution for ").append(valueOf).toString());
            zzpk.this.zzb(connectionResult, this.sD);
        }

        public void zzaop() {
            this.sE.unregisterConnectionFailedListener(this);
            this.sE.disconnect();
        }
    }

    private zzpk(zzqk zzqk) {
        super(zzqk);
        this.vm.zza("AutoManageHelper", (zzqj) this);
    }

    public static zzpk zza(zzqi zzqi) {
        zzqk zzc = zzc(zzqi);
        zzpk zzpk = (zzpk) zzc.zza("AutoManageHelper", zzpk.class);
        return zzpk != null ? zzpk : new zzpk(zzc);
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.sC.size()) {
                ((zza) this.sC.valueAt(i2)).dump(str, fileDescriptor, printWriter, strArr);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void onStart() {
        super.onStart();
        boolean z = this.mStarted;
        String valueOf = String.valueOf(this.sC);
        Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 14).append("onStart ").append(z).append(" ").append(valueOf).toString());
        if (!this.sL) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.sC.size()) {
                    ((zza) this.sC.valueAt(i2)).sE.connect();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void onStop() {
        super.onStop();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.sC.size()) {
                ((zza) this.sC.valueAt(i2)).sE.disconnect();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void zza(int i, GoogleApiClient googleApiClient, OnConnectionFailedListener onConnectionFailedListener) {
        zzab.zzb(googleApiClient, (Object) "GoogleApiClient instance cannot be null");
        zzab.zza(this.sC.indexOfKey(i) < 0, (Object) "Already managing a GoogleApiClient with id " + i);
        Log.d("AutoManageHelper", "starting AutoManage for client " + i + " " + this.mStarted + " " + this.sL);
        this.sC.put(i, new zza(i, googleApiClient, onConnectionFailedListener));
        if (this.mStarted && !this.sL) {
            String valueOf = String.valueOf(googleApiClient);
            Log.d("AutoManageHelper", new StringBuilder(String.valueOf(valueOf).length() + 11).append("connecting ").append(valueOf).toString());
            googleApiClient.connect();
        }
    }

    /* access modifiers changed from: protected */
    public void zza(ConnectionResult connectionResult, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        zza zza2 = (zza) this.sC.get(i);
        if (zza2 != null) {
            zzfh(i);
            OnConnectionFailedListener onConnectionFailedListener = zza2.sF;
            if (onConnectionFailedListener != null) {
                onConnectionFailedListener.onConnectionFailed(connectionResult);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zzaoo() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.sC.size()) {
                ((zza) this.sC.valueAt(i2)).sE.connect();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void zzfh(int i) {
        zza zza2 = (zza) this.sC.get(i);
        this.sC.remove(i);
        if (zza2 != null) {
            zza2.zzaop();
        }
    }
}
