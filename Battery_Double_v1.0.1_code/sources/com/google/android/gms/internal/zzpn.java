package com.google.android.gms.internal;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;

public abstract class zzpn extends zzqj implements OnCancelListener {
    protected boolean mStarted;
    protected boolean sL;
    /* access modifiers changed from: private */
    public ConnectionResult sM;
    /* access modifiers changed from: private */
    public int sN;
    private final Handler sO;
    protected final GoogleApiAvailability sh;

    private class zza implements Runnable {
        private zza() {
        }

        @MainThread
        public void run() {
            if (zzpn.this.mStarted) {
                if (zzpn.this.sM.hasResolution()) {
                    zzpn.this.vm.startActivityForResult(GoogleApiActivity.zzb(zzpn.this.getActivity(), zzpn.this.sM.getResolution(), zzpn.this.sN, false), 1);
                } else if (zzpn.this.sh.isUserResolvableError(zzpn.this.sM.getErrorCode())) {
                    zzpn.this.sh.zza(zzpn.this.getActivity(), zzpn.this.vm, zzpn.this.sM.getErrorCode(), 2, zzpn.this);
                } else if (zzpn.this.sM.getErrorCode() == 18) {
                    final Dialog zza = zzpn.this.sh.zza(zzpn.this.getActivity(), (OnCancelListener) zzpn.this);
                    zzpn.this.sh.zza(zzpn.this.getActivity().getApplicationContext(), (com.google.android.gms.internal.zzqe.zza) new com.google.android.gms.internal.zzqe.zza() {
                        public void zzaou() {
                            zzpn.this.zzaot();
                            if (zza.isShowing()) {
                                zza.dismiss();
                            }
                        }
                    });
                } else {
                    zzpn.this.zza(zzpn.this.sM, zzpn.this.sN);
                }
            }
        }
    }

    protected zzpn(zzqk zzqk) {
        this(zzqk, GoogleApiAvailability.getInstance());
    }

    zzpn(zzqk zzqk, GoogleApiAvailability googleApiAvailability) {
        super(zzqk);
        this.sN = -1;
        this.sO = new Handler(Looper.getMainLooper());
        this.sh = googleApiAvailability;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        boolean z = true;
        switch (i) {
            case 1:
                if (i2 != -1) {
                    if (i2 == 0) {
                        this.sM = new ConnectionResult(intent != null ? intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13) : 13, null);
                    }
                }
                break;
            case 2:
                int isGooglePlayServicesAvailable = this.sh.isGooglePlayServicesAvailable(getActivity());
                if (isGooglePlayServicesAvailable != 0) {
                    z = false;
                }
                if (this.sM.getErrorCode() == 18 && isGooglePlayServicesAvailable == 18) {
                    return;
                }
            default:
                z = false;
                break;
        }
        if (z) {
            zzaot();
        } else {
            zza(this.sM, this.sN);
        }
    }

    public void onCancel(DialogInterface dialogInterface) {
        zza(new ConnectionResult(13, null), this.sN);
        zzaot();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.sL = bundle.getBoolean("resolving_error", false);
            if (this.sL) {
                this.sN = bundle.getInt("failed_client_id", -1);
                this.sM = new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution"));
            }
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("resolving_error", this.sL);
        if (this.sL) {
            bundle.putInt("failed_client_id", this.sN);
            bundle.putInt("failed_status", this.sM.getErrorCode());
            bundle.putParcelable("failed_resolution", this.sM.getResolution());
        }
    }

    public void onStart() {
        super.onStart();
        this.mStarted = true;
    }

    public void onStop() {
        super.onStop();
        this.mStarted = false;
    }

    /* access modifiers changed from: protected */
    public abstract void zza(ConnectionResult connectionResult, int i);

    /* access modifiers changed from: protected */
    public abstract void zzaoo();

    /* access modifiers changed from: protected */
    public void zzaot() {
        this.sN = -1;
        this.sL = false;
        this.sM = null;
        zzaoo();
    }

    public void zzb(ConnectionResult connectionResult, int i) {
        if (!this.sL) {
            this.sL = true;
            this.sN = i;
            this.sM = connectionResult;
            this.sO.post(new zza());
        }
    }
}
