package com.google.android.gms.internal;

public final class zzapr implements Cloneable {
    private static final zzaps bjz = new zzaps();
    private boolean bjA;
    private int[] bjB;
    private zzaps[] bjC;
    private int mSize;

    zzapr() {
        this(10);
    }

    zzapr(int i) {
        this.bjA = false;
        int idealIntArraySize = idealIntArraySize(i);
        this.bjB = new int[idealIntArraySize];
        this.bjC = new zzaps[idealIntArraySize];
        this.mSize = 0;
    }

    private int idealByteArraySize(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            if (i <= (1 << i2) - 12) {
                return (1 << i2) - 12;
            }
        }
        return i;
    }

    private int idealIntArraySize(int i) {
        return idealByteArraySize(i * 4) / 4;
    }

    private boolean zza(int[] iArr, int[] iArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (iArr[i2] != iArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    private boolean zza(zzaps[] zzapsArr, zzaps[] zzapsArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (!zzapsArr[i2].equals(zzapsArr2[i2])) {
                return false;
            }
        }
        return true;
    }

    private int zzagh(int i) {
        int i2 = 0;
        int i3 = this.mSize - 1;
        while (i2 <= i3) {
            int i4 = (i2 + i3) >>> 1;
            int i5 = this.bjB[i4];
            if (i5 < i) {
                i2 = i4 + 1;
            } else if (i5 <= i) {
                return i4;
            } else {
                i3 = i4 - 1;
            }
        }
        return i2 ^ -1;
    }

    /* renamed from: aC */
    public final zzapr clone() {
        int size = size();
        zzapr zzapr = new zzapr(size);
        System.arraycopy(this.bjB, 0, zzapr.bjB, 0, size);
        for (int i = 0; i < size; i++) {
            if (this.bjC[i] != null) {
                zzapr.bjC[i] = (zzaps) this.bjC[i].clone();
            }
        }
        zzapr.mSize = size;
        return zzapr;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzapr)) {
            return false;
        }
        zzapr zzapr = (zzapr) obj;
        if (size() != zzapr.size()) {
            return false;
        }
        return zza(this.bjB, zzapr.bjB, this.mSize) && zza(this.bjC, zzapr.bjC, this.mSize);
    }

    public int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.mSize; i2++) {
            i = (((i * 31) + this.bjB[i2]) * 31) + this.bjC[i2].hashCode();
        }
        return i;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    /* access modifiers changed from: 0000 */
    public int size() {
        return this.mSize;
    }

    /* access modifiers changed from: 0000 */
    public void zza(int i, zzaps zzaps) {
        int zzagh = zzagh(i);
        if (zzagh >= 0) {
            this.bjC[zzagh] = zzaps;
            return;
        }
        int i2 = zzagh ^ -1;
        if (i2 >= this.mSize || this.bjC[i2] != bjz) {
            if (this.mSize >= this.bjB.length) {
                int idealIntArraySize = idealIntArraySize(this.mSize + 1);
                int[] iArr = new int[idealIntArraySize];
                zzaps[] zzapsArr = new zzaps[idealIntArraySize];
                System.arraycopy(this.bjB, 0, iArr, 0, this.bjB.length);
                System.arraycopy(this.bjC, 0, zzapsArr, 0, this.bjC.length);
                this.bjB = iArr;
                this.bjC = zzapsArr;
            }
            if (this.mSize - i2 != 0) {
                System.arraycopy(this.bjB, i2, this.bjB, i2 + 1, this.mSize - i2);
                System.arraycopy(this.bjC, i2, this.bjC, i2 + 1, this.mSize - i2);
            }
            this.bjB[i2] = i;
            this.bjC[i2] = zzaps;
            this.mSize++;
            return;
        }
        this.bjB[i2] = i;
        this.bjC[i2] = zzaps;
    }

    /* access modifiers changed from: 0000 */
    public zzaps zzagf(int i) {
        int zzagh = zzagh(i);
        if (zzagh < 0 || this.bjC[zzagh] == bjz) {
            return null;
        }
        return this.bjC[zzagh];
    }

    /* access modifiers changed from: 0000 */
    public zzaps zzagg(int i) {
        return this.bjC[i];
    }
}
