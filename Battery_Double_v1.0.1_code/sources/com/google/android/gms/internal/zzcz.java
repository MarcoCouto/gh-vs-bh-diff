package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@zzin
public class zzcz {
    private final Collection<zzcy> zzaxr = new ArrayList();
    private final Collection<zzcy<String>> zzaxs = new ArrayList();
    private final Collection<zzcy<String>> zzaxt = new ArrayList();

    public void zza(zzcy zzcy) {
        this.zzaxr.add(zzcy);
    }

    public void zzb(zzcy<String> zzcy) {
        this.zzaxs.add(zzcy);
    }

    public void zzc(zzcy<String> zzcy) {
        this.zzaxt.add(zzcy);
    }

    public List<String> zzjx() {
        ArrayList arrayList = new ArrayList();
        for (zzcy zzcy : this.zzaxs) {
            String str = (String) zzcy.get();
            if (str != null) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    public List<String> zzjy() {
        List<String> zzjx = zzjx();
        for (zzcy zzcy : this.zzaxt) {
            String str = (String) zzcy.get();
            if (str != null) {
                zzjx.add(str);
            }
        }
        return zzjx;
    }
}
