package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class zzalu {
    private Map<String, Map<String, byte[]>> bbn;
    private long pJ;

    public zzalu(Map<String, Map<String, byte[]>> map, long j) {
        this.bbn = map;
        this.pJ = j;
    }

    public long getTimestamp() {
        return this.pJ;
    }

    public void setTimestamp(long j) {
        this.pJ = j;
    }

    public boolean zzbt(String str, String str2) {
        return zzcxi() && zztc(str2) && zzbu(str, str2) != null;
    }

    public byte[] zzbu(String str, String str2) {
        if (str == null || !zztc(str2)) {
            return null;
        }
        return (byte[]) ((Map) this.bbn.get(str2)).get(str);
    }

    public Set<String> zzbv(String str, String str2) {
        TreeSet treeSet = new TreeSet();
        if (!zztc(str2)) {
            return treeSet;
        }
        if (str == null || str.isEmpty()) {
            return ((Map) this.bbn.get(str2)).keySet();
        }
        for (String str3 : ((Map) this.bbn.get(str2)).keySet()) {
            if (str3.startsWith(str)) {
                treeSet.add(str3);
            }
        }
        return treeSet;
    }

    public Map<String, Map<String, byte[]>> zzcxh() {
        return this.bbn;
    }

    public boolean zzcxi() {
        return this.bbn != null && !this.bbn.isEmpty();
    }

    public void zzk(Map<String, byte[]> map, String str) {
        if (this.bbn == null) {
            this.bbn = new HashMap();
        }
        this.bbn.put(str, map);
    }

    public boolean zztc(String str) {
        if (str == null) {
            return false;
        }
        return zzcxi() && this.bbn.get(str) != null && !((Map) this.bbn.get(str)).isEmpty();
    }
}
