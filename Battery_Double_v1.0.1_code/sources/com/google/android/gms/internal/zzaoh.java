package com.google.android.gms.internal;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class zzaoh extends zzanh<Date> {
    public static final zzani bfu = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() == Date.class) {
                return new zzaoh();
            }
            return null;
        }
    };
    private final DateFormat bfU = new SimpleDateFormat("MMM d, yyyy");

    public synchronized void zza(zzaoo zzaoo, Date date) throws IOException {
        zzaoo.zzts(date == null ? null : this.bfU.format(date));
    }

    /* renamed from: zzm */
    public synchronized Date zzb(zzaom zzaom) throws IOException {
        Date date;
        if (zzaom.b() == zzaon.NULL) {
            zzaom.nextNull();
            date = null;
        } else {
            try {
                date = new Date(this.bfU.parse(zzaom.nextString()).getTime());
            } catch (ParseException e) {
                throw new zzane((Throwable) e);
            }
        }
        return date;
    }
}
