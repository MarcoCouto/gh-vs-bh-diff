package com.google.android.gms.internal;

import android.app.AlertDialog.Builder;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.R;
import com.google.android.gms.ads.internal.zzu;
import java.util.Map;

@zzin
public class zzhc extends zzhf {
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Map<String, String> zzbeg;

    public zzhc(zzlh zzlh, Map<String, String> map) {
        super(zzlh, "storePicture");
        this.zzbeg = map;
        this.mContext = zzlh.zzue();
    }

    public void execute() {
        if (this.mContext == null) {
            zzbt("Activity context is not available");
        } else if (!zzu.zzfq().zzag(this.mContext).zzjr()) {
            zzbt("Feature is not supported by the device.");
        } else {
            final String str = (String) this.zzbeg.get("iurl");
            if (TextUtils.isEmpty(str)) {
                zzbt("Image url cannot be empty.");
            } else if (!URLUtil.isValidUrl(str)) {
                String str2 = "Invalid image url: ";
                String valueOf = String.valueOf(str);
                zzbt(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            } else {
                final String zzbs = zzbs(str);
                if (!zzu.zzfq().zzcq(zzbs)) {
                    String str3 = "Image type not recognized: ";
                    String valueOf2 = String.valueOf(zzbs);
                    zzbt(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
                    return;
                }
                Resources resources = zzu.zzft().getResources();
                Builder zzaf = zzu.zzfq().zzaf(this.mContext);
                zzaf.setTitle(resources != null ? resources.getString(R.string.store_picture_title) : "Save image");
                zzaf.setMessage(resources != null ? resources.getString(R.string.store_picture_message) : "Allow Ad to store image in Picture gallery?");
                zzaf.setPositiveButton(resources != null ? resources.getString(R.string.accept) : "Accept", new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            ((DownloadManager) zzhc.this.mContext.getSystemService("download")).enqueue(zzhc.this.zzk(str, zzbs));
                        } catch (IllegalStateException e) {
                            zzhc.this.zzbt("Could not store picture.");
                        }
                    }
                });
                zzaf.setNegativeButton(resources != null ? resources.getString(R.string.decline) : "Decline", new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        zzhc.this.zzbt("User canceled the download.");
                    }
                });
                zzaf.create().show();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public String zzbs(String str) {
        return Uri.parse(str).getLastPathSegment();
    }

    /* access modifiers changed from: 0000 */
    public Request zzk(String str, String str2) {
        Request request = new Request(Uri.parse(str));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, str2);
        zzu.zzfs().zza(request);
        return request;
    }
}
