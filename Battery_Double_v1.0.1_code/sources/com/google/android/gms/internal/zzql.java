package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

@TargetApi(11)
public final class zzql extends Fragment implements zzqk {
    private static WeakHashMap<Activity, WeakReference<zzql>> vn = new WeakHashMap<>();
    private Map<String, zzqj> vo = new ArrayMap();
    /* access modifiers changed from: private */
    public Bundle vp;
    /* access modifiers changed from: private */
    public int zzblv = 0;

    private void zzb(final String str, @NonNull final zzqj zzqj) {
        if (this.zzblv > 0) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (zzql.this.zzblv >= 1) {
                        zzqj.onCreate(zzql.this.vp != null ? zzql.this.vp.getBundle(str) : null);
                    }
                    if (zzql.this.zzblv >= 2) {
                        zzqj.onStart();
                    }
                    if (zzql.this.zzblv >= 3) {
                        zzqj.onStop();
                    }
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r0 != null) goto L_0x0012;
     */
    public static zzql zzt(Activity activity) {
        zzql zzql;
        WeakReference weakReference = (WeakReference) vn.get(activity);
        if (weakReference != null) {
            zzql = (zzql) weakReference.get();
        }
        try {
            zzql = (zzql) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
            if (zzql == null || zzql.isRemoving()) {
                zzql = new zzql();
                activity.getFragmentManager().beginTransaction().add(zzql, "LifecycleFragmentImpl").commitAllowingStateLoss();
            }
            vn.put(activity, new WeakReference(zzql));
            return zzql;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (zzqj dump : this.vo.values()) {
            dump.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (zzqj onActivityResult : this.vo.values()) {
            onActivityResult.onActivityResult(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzblv = 1;
        this.vp = bundle;
        for (Entry entry : this.vo.entrySet()) {
            ((zzqj) entry.getValue()).onCreate(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Entry entry : this.vo.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((zzqj) entry.getValue()).onSaveInstanceState(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public void onStart() {
        super.onStop();
        this.zzblv = 2;
        for (zzqj onStart : this.vo.values()) {
            onStart.onStart();
        }
    }

    public void onStop() {
        super.onStop();
        this.zzblv = 3;
        for (zzqj onStop : this.vo.values()) {
            onStop.onStop();
        }
    }

    public <T extends zzqj> T zza(String str, Class<T> cls) {
        return (zzqj) cls.cast(this.vo.get(str));
    }

    public void zza(String str, @NonNull zzqj zzqj) {
        if (!this.vo.containsKey(str)) {
            this.vo.put(str, zzqj);
            zzb(str, zzqj);
            return;
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 59).append("LifecycleCallback with tag ").append(str).append(" already added to this fragment.").toString());
    }

    public Activity zzaqt() {
        return getActivity();
    }
}
