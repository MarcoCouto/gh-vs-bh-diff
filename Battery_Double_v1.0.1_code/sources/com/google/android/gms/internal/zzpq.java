package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.Api.zze;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzg;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

final class zzpq implements zzqh {
    private final Context mContext;
    private final zzpy th;
    /* access modifiers changed from: private */
    public final zzqa ti;
    /* access modifiers changed from: private */
    public final zzqa tj;
    private final Map<zzc<?>, zzqa> tk;
    private final Set<zzqt> tl = Collections.newSetFromMap(new WeakHashMap());
    private final zze tm;
    private Bundle tn;
    /* access modifiers changed from: private */
    public ConnectionResult to = null;
    /* access modifiers changed from: private */
    public ConnectionResult tp = null;
    /* access modifiers changed from: private */
    public boolean tq = false;
    /* access modifiers changed from: private */
    public final Lock tr;
    private int ts = 0;
    private final Looper zzahv;

    private class zza implements com.google.android.gms.internal.zzqh.zza {
        private zza() {
        }

        public void zzc(int i, boolean z) {
            zzpq.this.tr.lock();
            try {
                if (zzpq.this.tq || zzpq.this.tp == null || !zzpq.this.tp.isSuccess()) {
                    zzpq.this.tq = false;
                    zzpq.this.zzb(i, z);
                    return;
                }
                zzpq.this.tq = true;
                zzpq.this.tj.onConnectionSuspended(i);
                zzpq.this.tr.unlock();
            } finally {
                zzpq.this.tr.unlock();
            }
        }

        public void zzd(@NonNull ConnectionResult connectionResult) {
            zzpq.this.tr.lock();
            try {
                zzpq.this.to = connectionResult;
                zzpq.this.zzape();
            } finally {
                zzpq.this.tr.unlock();
            }
        }

        public void zzm(@Nullable Bundle bundle) {
            zzpq.this.tr.lock();
            try {
                zzpq.this.zzl(bundle);
                zzpq.this.to = ConnectionResult.rb;
                zzpq.this.zzape();
            } finally {
                zzpq.this.tr.unlock();
            }
        }
    }

    private class zzb implements com.google.android.gms.internal.zzqh.zza {
        private zzb() {
        }

        public void zzc(int i, boolean z) {
            zzpq.this.tr.lock();
            try {
                if (zzpq.this.tq) {
                    zzpq.this.tq = false;
                    zzpq.this.zzb(i, z);
                    return;
                }
                zzpq.this.tq = true;
                zzpq.this.ti.onConnectionSuspended(i);
                zzpq.this.tr.unlock();
            } finally {
                zzpq.this.tr.unlock();
            }
        }

        public void zzd(@NonNull ConnectionResult connectionResult) {
            zzpq.this.tr.lock();
            try {
                zzpq.this.tp = connectionResult;
                zzpq.this.zzape();
            } finally {
                zzpq.this.tr.unlock();
            }
        }

        public void zzm(@Nullable Bundle bundle) {
            zzpq.this.tr.lock();
            try {
                zzpq.this.tp = ConnectionResult.rb;
                zzpq.this.zzape();
            } finally {
                zzpq.this.tr.unlock();
            }
        }
    }

    private zzpq(Context context, zzpy zzpy, Lock lock, Looper looper, com.google.android.gms.common.zzc zzc, Map<zzc<?>, zze> map, Map<zzc<?>, zze> map2, zzg zzg, com.google.android.gms.common.api.Api.zza<? extends zzvu, zzvv> zza2, zze zze, ArrayList<zzpp> arrayList, ArrayList<zzpp> arrayList2, Map<Api<?>, Integer> map3, Map<Api<?>, Integer> map4) {
        this.mContext = context;
        this.th = zzpy;
        this.tr = lock;
        this.zzahv = looper;
        this.tm = zze;
        this.ti = new zzqa(context, this.th, lock, looper, zzc, map2, null, map4, null, arrayList2, new zza());
        this.tj = new zzqa(context, this.th, lock, looper, zzc, map, zzg, map3, zza2, arrayList, new zzb());
        ArrayMap arrayMap = new ArrayMap();
        for (zzc put : map2.keySet()) {
            arrayMap.put(put, this.ti);
        }
        for (zzc put2 : map.keySet()) {
            arrayMap.put(put2, this.tj);
        }
        this.tk = Collections.unmodifiableMap(arrayMap);
    }

    public static zzpq zza(Context context, zzpy zzpy, Lock lock, Looper looper, com.google.android.gms.common.zzc zzc, Map<zzc<?>, zze> map, zzg zzg, Map<Api<?>, Integer> map2, com.google.android.gms.common.api.Api.zza<? extends zzvu, zzvv> zza2, ArrayList<zzpp> arrayList) {
        zze zze = null;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        for (Entry entry : map.entrySet()) {
            zze zze2 = (zze) entry.getValue();
            if (zze2.zzafz()) {
                zze = zze2;
            }
            if (zze2.zzafk()) {
                arrayMap.put((zzc) entry.getKey(), zze2);
            } else {
                arrayMap2.put((zzc) entry.getKey(), zze2);
            }
        }
        zzab.zza(!arrayMap.isEmpty(), (Object) "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (Api api : map2.keySet()) {
            zzc zzans = api.zzans();
            if (arrayMap.containsKey(zzans)) {
                arrayMap3.put(api, (Integer) map2.get(api));
            } else if (arrayMap2.containsKey(zzans)) {
                arrayMap4.put(api, (Integer) map2.get(api));
            } else {
                throw new IllegalStateException("Each API in the apiTypeMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            zzpp zzpp = (zzpp) it.next();
            if (arrayMap3.containsKey(zzpp.pN)) {
                arrayList2.add(zzpp);
            } else if (arrayMap4.containsKey(zzpp.pN)) {
                arrayList3.add(zzpp);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the apiTypeMap");
            }
        }
        return new zzpq(context, zzpy, lock, looper, zzc, arrayMap, arrayMap2, zzg, zza2, zze, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    private void zzapd() {
        this.tp = null;
        this.to = null;
        this.ti.connect();
        this.tj.connect();
    }

    /* access modifiers changed from: private */
    public void zzape() {
        if (zzc(this.to)) {
            if (zzc(this.tp) || zzaph()) {
                zzapf();
            } else if (this.tp == null) {
            } else {
                if (this.ts == 1) {
                    zzapg();
                    return;
                }
                zzb(this.tp);
                this.ti.disconnect();
            }
        } else if (this.to != null && zzc(this.tp)) {
            this.tj.disconnect();
            zzb(this.to);
        } else if (this.to != null && this.tp != null) {
            ConnectionResult connectionResult = this.to;
            if (this.tj.uA < this.ti.uA) {
                connectionResult = this.tp;
            }
            zzb(connectionResult);
        }
    }

    private void zzapf() {
        switch (this.ts) {
            case 1:
                break;
            case 2:
                this.th.zzm(this.tn);
                break;
            default:
                Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                break;
        }
        zzapg();
        this.ts = 0;
    }

    private void zzapg() {
        for (zzqt zzafy : this.tl) {
            zzafy.zzafy();
        }
        this.tl.clear();
    }

    private boolean zzaph() {
        return this.tp != null && this.tp.getErrorCode() == 4;
    }

    @Nullable
    private PendingIntent zzapi() {
        if (this.tm == null) {
            return null;
        }
        return PendingIntent.getActivity(this.mContext, this.th.getSessionId(), this.tm.zzaga(), 134217728);
    }

    /* access modifiers changed from: private */
    public void zzb(int i, boolean z) {
        this.th.zzc(i, z);
        this.tp = null;
        this.to = null;
    }

    private void zzb(ConnectionResult connectionResult) {
        switch (this.ts) {
            case 1:
                break;
            case 2:
                this.th.zzd(connectionResult);
                break;
            default:
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                break;
        }
        zzapg();
        this.ts = 0;
    }

    private static boolean zzc(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.isSuccess();
    }

    private boolean zze(com.google.android.gms.internal.zzpm.zza<? extends Result, ? extends com.google.android.gms.common.api.Api.zzb> zza2) {
        zzc zzans = zza2.zzans();
        zzab.zzb(this.tk.containsKey(zzans), (Object) "GoogleApiClient is not configured to use the API required for this call.");
        return ((zzqa) this.tk.get(zzans)).equals(this.tj);
    }

    /* access modifiers changed from: private */
    public void zzl(Bundle bundle) {
        if (this.tn == null) {
            this.tn = bundle;
        } else if (bundle != null) {
            this.tn.putAll(bundle);
        }
    }

    public ConnectionResult blockingConnect() {
        throw new UnsupportedOperationException();
    }

    public ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void connect() {
        this.ts = 2;
        this.tq = false;
        zzapd();
    }

    public void disconnect() {
        this.tp = null;
        this.to = null;
        this.ts = 0;
        this.ti.disconnect();
        this.tj.disconnect();
        zzapg();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("authClient").println(":");
        this.tj.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append(str).append("anonClient").println(":");
        this.ti.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    @Nullable
    public ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        return ((zzqa) this.tk.get(api.zzans())).equals(this.tj) ? zzaph() ? new ConnectionResult(4, zzapi()) : this.tj.getConnectionResult(api) : this.ti.getConnectionResult(api);
    }

    public boolean isConnected() {
        boolean z = true;
        this.tr.lock();
        try {
            if (!this.ti.isConnected() || (!zzapc() && !zzaph() && this.ts != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.tr.unlock();
        }
    }

    public boolean isConnecting() {
        this.tr.lock();
        try {
            return this.ts == 2;
        } finally {
            this.tr.unlock();
        }
    }

    public boolean zza(zzqt zzqt) {
        this.tr.lock();
        try {
            if ((isConnecting() || isConnected()) && !zzapc()) {
                this.tl.add(zzqt);
                if (this.ts == 0) {
                    this.ts = 1;
                }
                this.tp = null;
                this.tj.connect();
                return true;
            }
            this.tr.unlock();
            return false;
        } finally {
            this.tr.unlock();
        }
    }

    public void zzaof() {
        this.tr.lock();
        try {
            boolean isConnecting = isConnecting();
            this.tj.disconnect();
            this.tp = new ConnectionResult(4);
            if (isConnecting) {
                new Handler(this.zzahv).post(new Runnable() {
                    public void run() {
                        zzpq.this.tr.lock();
                        try {
                            zzpq.this.zzape();
                        } finally {
                            zzpq.this.tr.unlock();
                        }
                    }
                });
            } else {
                zzapg();
            }
        } finally {
            this.tr.unlock();
        }
    }

    public void zzapb() {
        this.ti.zzapb();
        this.tj.zzapb();
    }

    public boolean zzapc() {
        return this.tj.isConnected();
    }

    public <A extends com.google.android.gms.common.api.Api.zzb, R extends Result, T extends com.google.android.gms.internal.zzpm.zza<R, A>> T zzc(@NonNull T t) {
        if (!zze((com.google.android.gms.internal.zzpm.zza<? extends Result, ? extends com.google.android.gms.common.api.Api.zzb>) t)) {
            return this.ti.zzc(t);
        }
        if (!zzaph()) {
            return this.tj.zzc(t);
        }
        t.zzz(new Status(4, null, zzapi()));
        return t;
    }

    public <A extends com.google.android.gms.common.api.Api.zzb, T extends com.google.android.gms.internal.zzpm.zza<? extends Result, A>> T zzd(@NonNull T t) {
        if (!zze((com.google.android.gms.internal.zzpm.zza<? extends Result, ? extends com.google.android.gms.common.api.Api.zzb>) t)) {
            return this.ti.zzd(t);
        }
        if (!zzaph()) {
            return this.tj.zzd(t);
        }
        t.zzz(new Status(4, null, zzapi()));
        return t;
    }
}
