package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.zzic.zza;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzin
public class zzig extends zzkc {
    private final Object zzail;
    /* access modifiers changed from: private */
    public final zza zzbxq;
    private final zzju.zza zzbxr;
    private final AdResponseParcel zzbxs;
    private final zzii zzbyq;
    private Future<zzju> zzbyr;

    public zzig(Context context, zzq zzq, zzju.zza zza, zzas zzas, zza zza2) {
        this(zza, zza2, new zzii(context, zzq, new zzkn(context), zzas, zza));
    }

    zzig(zzju.zza zza, zza zza2, zzii zzii) {
        this.zzail = new Object();
        this.zzbxr = zza;
        this.zzbxs = zza.zzciq;
        this.zzbxq = zza2;
        this.zzbyq = zzii;
    }

    private zzju zzam(int i) {
        return new zzju(this.zzbxr.zzcip.zzcar, null, null, i, null, null, this.zzbxs.orientation, this.zzbxs.zzbns, this.zzbxr.zzcip.zzcau, false, null, null, null, null, null, this.zzbxs.zzcbz, this.zzbxr.zzapa, this.zzbxs.zzcbx, this.zzbxr.zzcik, this.zzbxs.zzccc, this.zzbxs.zzccd, this.zzbxr.zzcie, null, null, null, null, this.zzbxr.zzciq.zzccq, this.zzbxr.zzciq.zzccr, null, null);
    }

    public void onStop() {
        synchronized (this.zzail) {
            if (this.zzbyr != null) {
                this.zzbyr.cancel(true);
            }
        }
    }

    public void zzew() {
        final zzju zzju;
        int i;
        try {
            synchronized (this.zzail) {
                this.zzbyr = zzkg.zza((Callable<T>) this.zzbyq);
            }
            zzju = (zzju) this.zzbyr.get(60000, TimeUnit.MILLISECONDS);
            i = -2;
        } catch (TimeoutException e) {
            zzkd.zzcx("Timed out waiting for native ad.");
            this.zzbyr.cancel(true);
            i = 2;
            zzju = null;
        } catch (ExecutionException e2) {
            zzju = null;
            i = 0;
        } catch (InterruptedException e3) {
            zzju = null;
            i = 0;
        } catch (CancellationException e4) {
            zzju = null;
            i = 0;
        }
        if (zzju == null) {
            zzju = zzam(i);
        }
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzig.this.zzbxq.zzb(zzju);
            }
        });
    }
}
