package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.concurrent.Future;

@zzin
public class zzfq {

    private static class zza<JavascriptEngine> extends zzkv<JavascriptEngine> {
        JavascriptEngine zzblk;

        private zza() {
        }
    }

    /* access modifiers changed from: private */
    public zzfp zza(Context context, VersionInfoParcel versionInfoParcel, final zza<zzfp> zza2, zzas zzas) {
        JavascriptEngine zzfr = new zzfr(context, versionInfoParcel, zzas);
        zza2.zzblk = zzfr;
        zzfr.zza(new com.google.android.gms.internal.zzfp.zza() {
            public void zzlz() {
                zza2.zzh((zzfp) zza2.zzblk);
            }
        });
        return zzfr;
    }

    public Future<zzfp> zza(Context context, VersionInfoParcel versionInfoParcel, String str, zzas zzas) {
        final zza zza2 = new zza();
        final Context context2 = context;
        final VersionInfoParcel versionInfoParcel2 = versionInfoParcel;
        final zzas zzas2 = zzas;
        final String str2 = str;
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzfq.this.zza(context2, versionInfoParcel2, zza2, zzas2).zzbh(str2);
            }
        });
        return zza2;
    }
}
