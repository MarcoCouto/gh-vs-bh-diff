package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzin
public class zzds extends Image {
    private final Drawable mDrawable;
    private final Uri mUri;
    private final double zzbff;
    private final zzdr zzbhb;

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.graphics.drawable.Drawable] */
    /* JADX WARNING: type inference failed for: r0v12, types: [android.graphics.drawable.Drawable] */
    /* JADX WARNING: type inference failed for: r0v13 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 3 */
    public zzds(zzdr zzdr) {
        ? r0;
        Uri uri;
        ? r1 = 0;
        this.zzbhb = zzdr;
        try {
            zzd zzkt = this.zzbhb.zzkt();
            if (zzkt != null) {
                r0 = (Drawable) zze.zzad(zzkt);
                this.mDrawable = r0;
                uri = this.zzbhb.getUri();
                this.mUri = uri;
                double d = 1.0d;
                d = this.zzbhb.getScale();
                this.zzbff = d;
            }
        } catch (RemoteException e) {
            zzb.zzb("Failed to get drawable.", e);
        }
        r0 = r1;
        this.mDrawable = r0;
        try {
            uri = this.zzbhb.getUri();
        } catch (RemoteException e2) {
            zzb.zzb("Failed to get uri.", e2);
            uri = r1;
        }
        this.mUri = uri;
        double d2 = 1.0d;
        try {
            d2 = this.zzbhb.getScale();
        } catch (RemoteException e3) {
            zzb.zzb("Failed to get scale.", e3);
        }
        this.zzbff = d2;
    }

    public Drawable getDrawable() {
        return this.mDrawable;
    }

    public double getScale() {
        return this.zzbff;
    }

    public Uri getUri() {
        return this.mUri;
    }
}
