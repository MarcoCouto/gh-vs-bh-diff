package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

@zzin
public final class zzgw<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener {
    /* access modifiers changed from: private */
    public final zzgl zzbpk;

    public zzgw(zzgl zzgl) {
        this.zzbpk = zzgl;
    }

    public void onClick(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzcv("Adapter called onClick.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onClick must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdClicked();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdClicked.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdClicked();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdClicked.", e);
        }
    }

    public void onDismissScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzcv("Adapter called onDismissScreen.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onDismissScreen must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdClosed();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdClosed.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdClosed();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdClosed.", e);
        }
    }

    public void onDismissScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzcv("Adapter called onDismissScreen.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onDismissScreen must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdClosed();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdClosed.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdClosed();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdClosed.", e);
        }
    }

    public void onFailedToReceiveAd(MediationBannerAdapter<?, ?> mediationBannerAdapter, final ErrorCode errorCode) {
        String valueOf = String.valueOf(errorCode);
        zzb.zzcv(new StringBuilder(String.valueOf(valueOf).length() + 47).append("Adapter called onFailedToReceiveAd with error. ").append(valueOf).toString());
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onFailedToReceiveAd must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdFailedToLoad(zzgx.zza(errorCode));
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdFailedToLoad.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdFailedToLoad(zzgx.zza(errorCode));
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdFailedToLoad.", e);
        }
    }

    public void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, final ErrorCode errorCode) {
        String valueOf = String.valueOf(errorCode);
        zzb.zzcv(new StringBuilder(String.valueOf(valueOf).length() + 47).append("Adapter called onFailedToReceiveAd with error ").append(valueOf).append(".").toString());
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onFailedToReceiveAd must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdFailedToLoad(zzgx.zza(errorCode));
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdFailedToLoad.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdFailedToLoad(zzgx.zza(errorCode));
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdFailedToLoad.", e);
        }
    }

    public void onLeaveApplication(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzcv("Adapter called onLeaveApplication.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onLeaveApplication must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdLeftApplication();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdLeftApplication.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdLeftApplication();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdLeftApplication.", e);
        }
    }

    public void onLeaveApplication(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzcv("Adapter called onLeaveApplication.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onLeaveApplication must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdLeftApplication();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdLeftApplication.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdLeftApplication();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdLeftApplication.", e);
        }
    }

    public void onPresentScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzcv("Adapter called onPresentScreen.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onPresentScreen must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdOpened();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdOpened.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdOpened();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdOpened.", e);
        }
    }

    public void onPresentScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzcv("Adapter called onPresentScreen.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onPresentScreen must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdOpened();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdOpened.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdOpened();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdOpened.", e);
        }
    }

    public void onReceivedAd(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzb.zzcv("Adapter called onReceivedAd.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onReceivedAd must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdLoaded();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdLoaded.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdLoaded();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdLoaded.", e);
        }
    }

    public void onReceivedAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzb.zzcv("Adapter called onReceivedAd.");
        if (!zzm.zziw().zztx()) {
            zzb.zzcx("onReceivedAd must be called on the main UI thread.");
            zza.zzcnb.post(new Runnable() {
                public void run() {
                    try {
                        zzgw.this.zzbpk.onAdLoaded();
                    } catch (RemoteException e) {
                        zzb.zzd("Could not call onAdLoaded.", e);
                    }
                }
            });
            return;
        }
        try {
            this.zzbpk.onAdLoaded();
        } catch (RemoteException e) {
            zzb.zzd("Could not call onAdLoaded.", e);
        }
    }
}
