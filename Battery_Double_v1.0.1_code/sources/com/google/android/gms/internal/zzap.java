package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.internal.zzae.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class zzap extends zzaq {
    private static final String TAG = zzap.class.getSimpleName();
    private Info zzafm;

    protected zzap(Context context) {
        super(context, "");
    }

    public static zzap zze(Context context) {
        zza(context, true);
        return new zzap(context);
    }

    public String zza(String str, String str2) {
        return zzak.zza(str, str2, true);
    }

    public void zza(Info info) {
        this.zzafm = info;
    }

    /* access modifiers changed from: protected */
    public void zza(zzax zzax, zza zza) {
        if (!zzax.zzci()) {
            zza(zzb(zzax, zza));
        } else if (this.zzafm != null) {
            String id = this.zzafm.getId();
            if (!TextUtils.isEmpty(id)) {
                zza.zzeg = zzay.zzo(id);
                zza.zzeh = Integer.valueOf(5);
                zza.zzei = Boolean.valueOf(this.zzafm.isLimitAdTrackingEnabled());
            }
            this.zzafm = null;
        }
    }

    /* access modifiers changed from: protected */
    public List<Callable<Void>> zzb(zzax zzax, zza zza) {
        ArrayList arrayList = new ArrayList();
        if (zzax.zzcd() == null) {
            return arrayList;
        }
        zzax zzax2 = zzax;
        arrayList.add(new zzbh(zzax2, zzav.zzbl(), zzav.zzbm(), zza, zzax.zzat(), 24));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public zza zzd(Context context) {
        return null;
    }
}
