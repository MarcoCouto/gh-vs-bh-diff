package com.google.android.gms.internal;

import android.os.Process;
import com.google.android.gms.ads.internal.zzu;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

@zzin
public final class zzkg {
    private static final ExecutorService zzcku = Executors.newFixedThreadPool(10, zzcn("Default"));
    private static final ExecutorService zzckv = Executors.newFixedThreadPool(5, zzcn("Loader"));

    public static zzky<Void> zza(int i, final Runnable runnable) {
        return i == 1 ? zza(zzckv, (Callable<T>) new Callable<Void>() {
            /* renamed from: zzcx */
            public Void call() {
                runnable.run();
                return null;
            }
        }) : zza(zzcku, (Callable<T>) new Callable<Void>() {
            /* renamed from: zzcx */
            public Void call() {
                runnable.run();
                return null;
            }
        });
    }

    public static zzky<Void> zza(Runnable runnable) {
        return zza(0, runnable);
    }

    public static <T> zzky<T> zza(Callable<T> callable) {
        return zza(zzcku, callable);
    }

    public static <T> zzky<T> zza(ExecutorService executorService, final Callable<T> callable) {
        final zzkv zzkv = new zzkv();
        try {
            final Future submit = executorService.submit(new Runnable() {
                public void run() {
                    try {
                        Process.setThreadPriority(10);
                        zzkv.this.zzh(callable.call());
                    } catch (Exception e) {
                        zzu.zzft().zzb((Throwable) e, true);
                        zzkv.this.cancel(true);
                    }
                }
            });
            zzkv.zzd(new Runnable() {
                public void run() {
                    if (zzkv.this.isCancelled()) {
                        submit.cancel(true);
                    }
                }
            });
        } catch (RejectedExecutionException e) {
            zzkd.zzd("Thread execution is rejected.", e);
            zzkv.cancel(true);
        }
        return zzkv;
    }

    private static ThreadFactory zzcn(final String str) {
        return new ThreadFactory() {
            private final AtomicInteger zzcla = new AtomicInteger(1);

            public Thread newThread(Runnable runnable) {
                String str = str;
                return new Thread(runnable, new StringBuilder(String.valueOf(str).length() + 23).append("AdWorker(").append(str).append(") #").append(this.zzcla.getAndIncrement()).toString());
            }
        };
    }
}
