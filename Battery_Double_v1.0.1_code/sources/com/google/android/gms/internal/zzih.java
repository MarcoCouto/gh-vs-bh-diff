package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.zzfs.zzb;
import com.google.android.gms.internal.zzfs.zze;
import com.google.android.gms.internal.zzla.zzc;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzin
public class zzih {
    private static final Object zzamr = new Object();
    private static final long zzbyt = TimeUnit.SECONDS.toMillis(60);
    private static boolean zzbyu = false;
    private static zzfs zzbyv = null;
    private final Context mContext;
    /* access modifiers changed from: private */
    public final zzq zzbfx;
    private final zzas zzbgd;
    private final com.google.android.gms.internal.zzju.zza zzbxr;
    private zzfq zzbyw;
    private zze zzbyx;
    private zzfp zzbyy;
    private boolean zzbyz = false;

    public static abstract class zza {
        public abstract void zze(zzft zzft);

        public void zzqq() {
        }
    }

    public zzih(Context context, com.google.android.gms.internal.zzju.zza zza2, zzq zzq, zzas zzas) {
        this.mContext = context;
        this.zzbxr = zza2;
        this.zzbfx = zzq;
        this.zzbgd = zzas;
        this.zzbyz = ((Boolean) zzdc.zzbcf.get()).booleanValue();
    }

    private String zzd(com.google.android.gms.internal.zzju.zza zza2) {
        String str = (String) zzdc.zzbac.get();
        String valueOf = String.valueOf(zza2.zzciq.zzbto.indexOf("https") == 0 ? "https:" : "http:");
        String valueOf2 = String.valueOf(str);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    private void zzqi() {
        synchronized (zzamr) {
            if (!zzbyu) {
                zzbyv = new zzfs(this.mContext.getApplicationContext() != null ? this.mContext.getApplicationContext() : this.mContext, this.zzbxr.zzcip.zzaow, zzd(this.zzbxr), new zzkl<zzfp>() {
                    /* renamed from: zza */
                    public void zzd(zzfp zzfp) {
                        zzfp.zza(zzih.this.zzbfx, zzih.this.zzbfx, zzih.this.zzbfx, zzih.this.zzbfx, false, null, null, null, null);
                    }
                }, new zzb());
                zzbyu = true;
            }
        }
    }

    private void zzqj() {
        this.zzbyx = new zze(zzqo().zzc(this.zzbgd));
    }

    private void zzqk() {
        this.zzbyw = new zzfq();
    }

    private void zzql() throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
        this.zzbyy = (zzfp) zzqm().zza(this.mContext, this.zzbxr.zzcip.zzaow, zzd(this.zzbxr), this.zzbgd).get(zzbyt, TimeUnit.MILLISECONDS);
        this.zzbyy.zza(this.zzbfx, this.zzbfx, this.zzbfx, this.zzbfx, false, null, null, null, null);
    }

    public void zza(final zza zza2) {
        if (this.zzbyz) {
            zze zzqp = zzqp();
            if (zzqp == null) {
                zzkd.zzcx("SharedJavascriptEngine not initialized");
            } else {
                zzqp.zza(new zzc<zzft>() {
                    /* renamed from: zzb */
                    public void zzd(zzft zzft) {
                        zza2.zze(zzft);
                    }
                }, new com.google.android.gms.internal.zzla.zza() {
                    public void run() {
                        zza2.zzqq();
                    }
                });
            }
        } else {
            zzfp zzqn = zzqn();
            if (zzqn == null) {
                zzkd.zzcx("JavascriptEngine not initialized");
            } else {
                zza2.zze(zzqn);
            }
        }
    }

    public void zzqg() {
        if (this.zzbyz) {
            zzqi();
        } else {
            zzqk();
        }
    }

    public void zzqh() throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
        if (this.zzbyz) {
            zzqj();
        } else {
            zzql();
        }
    }

    /* access modifiers changed from: protected */
    public zzfq zzqm() {
        return this.zzbyw;
    }

    /* access modifiers changed from: protected */
    public zzfp zzqn() {
        return this.zzbyy;
    }

    /* access modifiers changed from: protected */
    public zzfs zzqo() {
        return zzbyv;
    }

    /* access modifiers changed from: protected */
    public zze zzqp() {
        return this.zzbyx;
    }
}
