package com.google.android.gms.internal;

@zzin
public interface zzkj<T> {
    void cancel();

    T zzpy();
}
