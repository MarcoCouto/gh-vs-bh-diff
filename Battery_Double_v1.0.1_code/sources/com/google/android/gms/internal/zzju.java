package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

@zzin
public class zzju {
    public final int errorCode;
    public final int orientation;
    public final List<String> zzbnm;
    public final List<String> zzbnn;
    @Nullable
    public final List<String> zzbnp;
    public final long zzbns;
    @Nullable
    public final zzfz zzbon;
    @Nullable
    public final zzgk zzboo;
    @Nullable
    public final String zzbop;
    @Nullable
    public final zzgc zzboq;
    @Nullable
    public final zzlh zzbtm;
    public final AdRequestParcel zzcar;
    public final String zzcau;
    public final long zzcbx;
    public final boolean zzcby;
    public final long zzcbz;
    public final List<String> zzcca;
    public final String zzccd;
    @Nullable
    public final RewardItemParcel zzccn;
    @Nullable
    public final List<String> zzccp;
    public final boolean zzccq;
    public final AutoClickProtectionConfigurationParcel zzccr;
    public final JSONObject zzcie;
    public boolean zzcif;
    public final zzga zzcig;
    @Nullable
    public final String zzcih;
    public final AdSizeParcel zzcii;
    @Nullable
    public final List<String> zzcij;
    public final long zzcik;
    public final long zzcil;
    @Nullable
    public final com.google.android.gms.ads.internal.formats.zzh.zza zzcim;
    public boolean zzcin;
    public boolean zzcio;

    @zzin
    public static final class zza {
        public final int errorCode;
        @Nullable
        public final AdSizeParcel zzapa;
        @Nullable
        public final JSONObject zzcie;
        public final zzga zzcig;
        public final long zzcik;
        public final long zzcil;
        public final AdRequestInfoParcel zzcip;
        public final AdResponseParcel zzciq;

        public zza(AdRequestInfoParcel adRequestInfoParcel, AdResponseParcel adResponseParcel, zzga zzga, AdSizeParcel adSizeParcel, int i, long j, long j2, JSONObject jSONObject) {
            this.zzcip = adRequestInfoParcel;
            this.zzciq = adResponseParcel;
            this.zzcig = zzga;
            this.zzapa = adSizeParcel;
            this.errorCode = i;
            this.zzcik = j;
            this.zzcil = j2;
            this.zzcie = jSONObject;
        }
    }

    public zzju(AdRequestParcel adRequestParcel, @Nullable zzlh zzlh, List<String> list, int i, List<String> list2, List<String> list3, int i2, long j, String str, boolean z, @Nullable zzfz zzfz, @Nullable zzgk zzgk, @Nullable String str2, zzga zzga, @Nullable zzgc zzgc, long j2, AdSizeParcel adSizeParcel, long j3, long j4, long j5, String str3, JSONObject jSONObject, @Nullable com.google.android.gms.ads.internal.formats.zzh.zza zza2, RewardItemParcel rewardItemParcel, List<String> list4, List<String> list5, boolean z2, AutoClickProtectionConfigurationParcel autoClickProtectionConfigurationParcel, @Nullable String str4, List<String> list6) {
        this.zzcin = false;
        this.zzcio = false;
        this.zzcar = adRequestParcel;
        this.zzbtm = zzlh;
        this.zzbnm = zzl(list);
        this.errorCode = i;
        this.zzbnn = zzl(list2);
        this.zzcca = zzl(list3);
        this.orientation = i2;
        this.zzbns = j;
        this.zzcau = str;
        this.zzcby = z;
        this.zzbon = zzfz;
        this.zzboo = zzgk;
        this.zzbop = str2;
        this.zzcig = zzga;
        this.zzboq = zzgc;
        this.zzcbz = j2;
        this.zzcii = adSizeParcel;
        this.zzcbx = j3;
        this.zzcik = j4;
        this.zzcil = j5;
        this.zzccd = str3;
        this.zzcie = jSONObject;
        this.zzcim = zza2;
        this.zzccn = rewardItemParcel;
        this.zzcij = zzl(list4);
        this.zzccp = zzl(list5);
        this.zzccq = z2;
        this.zzccr = autoClickProtectionConfigurationParcel;
        this.zzcih = str4;
        this.zzbnp = zzl(list6);
    }

    public zzju(zza zza2, @Nullable zzlh zzlh, @Nullable zzfz zzfz, @Nullable zzgk zzgk, @Nullable String str, @Nullable zzgc zzgc, @Nullable com.google.android.gms.ads.internal.formats.zzh.zza zza3, @Nullable String str2) {
        this(zza2.zzcip.zzcar, zzlh, zza2.zzciq.zzbnm, zza2.errorCode, zza2.zzciq.zzbnn, zza2.zzciq.zzcca, zza2.zzciq.orientation, zza2.zzciq.zzbns, zza2.zzcip.zzcau, zza2.zzciq.zzcby, zzfz, zzgk, str, zza2.zzcig, zzgc, zza2.zzciq.zzcbz, zza2.zzapa, zza2.zzciq.zzcbx, zza2.zzcik, zza2.zzcil, zza2.zzciq.zzccd, zza2.zzcie, zza3, zza2.zzciq.zzccn, zza2.zzciq.zzcco, zza2.zzciq.zzcco, zza2.zzciq.zzccq, zza2.zzciq.zzccr, str2, zza2.zzciq.zzbnp);
    }

    @Nullable
    private static <T> List<T> zzl(@Nullable List<T> list) {
        if (list == null) {
            return null;
        }
        return Collections.unmodifiableList(list);
    }

    public boolean zzho() {
        if (this.zzbtm == null || this.zzbtm.zzuj() == null) {
            return false;
        }
        return this.zzbtm.zzuj().zzho();
    }
}
