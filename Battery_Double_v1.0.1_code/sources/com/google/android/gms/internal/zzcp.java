package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@zzin
public abstract class zzcp {
    @Nullable
    private static MessageDigest zzasz = null;
    protected Object zzail = new Object();

    /* access modifiers changed from: 0000 */
    public abstract byte[] zzaa(String str);

    /* access modifiers changed from: protected */
    @Nullable
    public MessageDigest zzie() {
        MessageDigest messageDigest;
        synchronized (this.zzail) {
            if (zzasz != null) {
                messageDigest = zzasz;
            } else {
                for (int i = 0; i < 2; i++) {
                    try {
                        zzasz = MessageDigest.getInstance("MD5");
                    } catch (NoSuchAlgorithmException e) {
                    }
                }
                messageDigest = zzasz;
            }
        }
        return messageDigest;
    }
}
