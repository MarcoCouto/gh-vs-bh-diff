package com.google.android.gms.internal;

import com.google.android.gms.internal.zzaog.zza;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

final class zzaoj<T> extends zzanh<T> {
    private final zzanh<T> bdZ;
    private final zzamp bfV;
    private final Type bfW;

    zzaoj(zzamp zzamp, zzanh<T> zzanh, Type type) {
        this.bfV = zzamp;
        this.bdZ = zzanh;
        this.bfW = type;
    }

    private Type zzb(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    public void zza(zzaoo zzaoo, T t) throws IOException {
        zzanh<T> zzanh = this.bdZ;
        Type zzb = zzb(this.bfW, t);
        if (zzb != this.bfW) {
            zzanh = this.bfV.zza(zzaol.zzl(zzb));
            if ((zzanh instanceof zza) && !(this.bdZ instanceof zza)) {
                zzanh = this.bdZ;
            }
        }
        zzanh.zza(zzaoo, t);
    }

    public T zzb(zzaom zzaom) throws IOException {
        return this.bdZ.zzb(zzaom);
    }
}
