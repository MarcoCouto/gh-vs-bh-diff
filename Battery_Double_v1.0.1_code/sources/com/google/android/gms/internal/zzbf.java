package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;

public class zzbf extends zzbp {
    private static final Object zzafc = new Object();
    private static volatile Long zzec = null;

    public zzbf(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        if (zzec == null) {
            synchronized (zzafc) {
                if (zzec == null) {
                    zzec = (Long) this.zzahh.invoke(null, new Object[0]);
                }
            }
        }
        synchronized (this.zzaha) {
            this.zzaha.zzec = zzec;
        }
    }
}
