package com.google.android.gms.internal;

import android.text.TextUtils;

@zzin
public final class zzcv {
    private String zzaxn;

    public zzcv() {
        this((String) zzdc.zzaxy.zzjw());
    }

    public zzcv(String str) {
        this.zzaxn = TextUtils.isEmpty(str) ? (String) zzdc.zzaxy.zzjw() : str;
    }

    public String zzjv() {
        return this.zzaxn;
    }
}
