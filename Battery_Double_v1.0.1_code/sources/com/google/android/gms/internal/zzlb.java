package com.google.android.gms.internal;

import com.google.android.gms.internal.zzla.zzc;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@zzin
public class zzlb<T> implements zzla<T> {
    private final Object zzail = new Object();
    protected int zzblv = 0;
    protected final BlockingQueue<zza> zzcob = new LinkedBlockingQueue();
    protected T zzcoc;

    class zza {
        public final zzc<T> zzcod;
        public final com.google.android.gms.internal.zzla.zza zzcoe;

        public zza(zzc<T> zzc, com.google.android.gms.internal.zzla.zza zza) {
            this.zzcod = zzc;
            this.zzcoe = zza;
        }
    }

    public int getStatus() {
        return this.zzblv;
    }

    public void reject() {
        synchronized (this.zzail) {
            if (this.zzblv != 0) {
                throw new UnsupportedOperationException();
            }
            this.zzblv = -1;
            for (zza zza2 : this.zzcob) {
                zza2.zzcoe.run();
            }
            this.zzcob.clear();
        }
    }

    public void zza(zzc<T> zzc, com.google.android.gms.internal.zzla.zza zza2) {
        synchronized (this.zzail) {
            if (this.zzblv == 1) {
                zzc.zzd(this.zzcoc);
            } else if (this.zzblv == -1) {
                zza2.run();
            } else if (this.zzblv == 0) {
                this.zzcob.add(new zza(zzc, zza2));
            }
        }
    }

    public void zzg(T t) {
        synchronized (this.zzail) {
            if (this.zzblv != 0) {
                throw new UnsupportedOperationException();
            }
            this.zzcoc = t;
            this.zzblv = 1;
            for (zza zza2 : this.zzcob) {
                zza2.zzcod.zzd(t);
            }
            this.zzcob.clear();
        }
    }
}
