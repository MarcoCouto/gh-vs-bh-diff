package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzli.zza;

@zzin
public class zzid extends zzhy implements zza {
    zzid(Context context, zzju.zza zza, zzlh zzlh, zzic.zza zza2) {
        super(context, zza, zzlh, zza2);
    }

    /* access modifiers changed from: protected */
    public void zzpw() {
        if (this.zzbxs.errorCode == -2) {
            this.zzbgf.zzuj().zza((zza) this);
            zzqd();
            zzkd.zzcv("Loading HTML in WebView.");
            this.zzbgf.loadDataWithBaseURL(zzu.zzfq().zzco(this.zzbxs.zzbto), this.zzbxs.body, "text/html", "UTF-8", null);
        }
    }

    /* access modifiers changed from: protected */
    public void zzqd() {
    }
}
