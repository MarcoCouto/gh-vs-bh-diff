package com.google.android.gms.internal;

import java.util.concurrent.Future;

@zzin
public abstract class zzkc implements zzkj<Future> {
    /* access modifiers changed from: private */
    public volatile Thread zzckk;
    private boolean zzckl;
    private final Runnable zzw;

    public zzkc() {
        this.zzw = new Runnable() {
            public final void run() {
                zzkc.this.zzckk = Thread.currentThread();
                zzkc.this.zzew();
            }
        };
        this.zzckl = false;
    }

    public zzkc(boolean z) {
        this.zzw = new Runnable() {
            public final void run() {
                zzkc.this.zzckk = Thread.currentThread();
                zzkc.this.zzew();
            }
        };
        this.zzckl = z;
    }

    public final void cancel() {
        onStop();
        if (this.zzckk != null) {
            this.zzckk.interrupt();
        }
    }

    public abstract void onStop();

    public abstract void zzew();

    /* renamed from: zzsz */
    public final Future zzpy() {
        return this.zzckl ? zzkg.zza(1, this.zzw) : zzkg.zza(this.zzw);
    }
}
