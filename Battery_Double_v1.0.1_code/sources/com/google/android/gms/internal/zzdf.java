package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.text.TextUtils;

@zzin
public class zzdf {
    @Nullable
    public zzde zza(@Nullable zzdd zzdd) {
        if (zzdd == null) {
            throw new IllegalArgumentException("CSI configuration can't be null. ");
        } else if (!zzdd.zzjz()) {
            zzkd.v("CsiReporterFactory: CSI is not enabled. No CSI reporter created.");
            return null;
        } else if (zzdd.getContext() == null) {
            throw new IllegalArgumentException("Context can't be null. Please set up context in CsiConfiguration.");
        } else if (!TextUtils.isEmpty(zzdd.zzhl())) {
            return new zzde(zzdd.getContext(), zzdd.zzhl(), zzdd.zzka(), zzdd.zzkb());
        } else {
            throw new IllegalArgumentException("AfmaVersion can't be null or empty. Please set up afmaVersion in CsiConfiguration.");
        }
    }
}
