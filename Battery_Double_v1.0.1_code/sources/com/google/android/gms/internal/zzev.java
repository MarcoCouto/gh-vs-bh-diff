package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.common.util.zzf;
import java.util.Map;

@zzin
public class zzev implements zzep {
    static final Map<String, Integer> zzbiv = zzf.zza("resize", Integer.valueOf(1), "playVideo", Integer.valueOf(2), "storePicture", Integer.valueOf(3), "createCalendarEvent", Integer.valueOf(4), "setOrientationProperties", Integer.valueOf(5), "closeResizedAd", Integer.valueOf(6));
    private final zze zzbit;
    private final zzha zzbiu;

    public zzev(zze zze, zzha zzha) {
        this.zzbit = zze;
        this.zzbiu = zzha;
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        int intValue = ((Integer) zzbiv.get((String) map.get("a"))).intValue();
        if (intValue == 5 || this.zzbit == null || this.zzbit.zzel()) {
            switch (intValue) {
                case 1:
                    this.zzbiu.execute(map);
                    return;
                case 3:
                    new zzhc(zzlh, map).execute();
                    return;
                case 4:
                    new zzgz(zzlh, map).execute();
                    return;
                case 5:
                    new zzhb(zzlh, map).execute();
                    return;
                case 6:
                    this.zzbiu.zzs(true);
                    return;
                default:
                    zzkd.zzcw("Unknown MRAID command called.");
                    return;
            }
        } else {
            this.zzbit.zzt(null);
        }
    }
}
