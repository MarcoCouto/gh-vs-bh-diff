package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.firebase.FirebaseException;

public class zzalr extends FirebaseException {
    public zzalr(@NonNull String str) {
        super(str);
    }
}
