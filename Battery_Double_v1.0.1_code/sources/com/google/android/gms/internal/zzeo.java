package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.overlay.zzm;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zzeo {
    public static final zzep zzbhn = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
        }
    };
    public static final zzep zzbho = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("urls");
            if (TextUtils.isEmpty(str)) {
                zzkd.zzcx("URLs missing in canOpenURLs GMSG.");
                return;
            }
            String[] split = str.split(",");
            HashMap hashMap = new HashMap();
            PackageManager packageManager = zzlh.getContext().getPackageManager();
            for (String str2 : split) {
                String[] split2 = str2.split(";", 2);
                hashMap.put(str2, Boolean.valueOf(packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), 65536) != null));
            }
            zzlh.zza("openableURLs", (Map<String, ?>) hashMap);
        }
    };
    public static final zzep zzbhp = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            PackageManager packageManager = zzlh.getContext().getPackageManager();
            try {
                try {
                    JSONArray jSONArray = new JSONObject((String) map.get("data")).getJSONArray("intents");
                    JSONObject jSONObject = new JSONObject();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                            String optString = jSONObject2.optString("id");
                            String optString2 = jSONObject2.optString("u");
                            String optString3 = jSONObject2.optString("i");
                            String optString4 = jSONObject2.optString("m");
                            String optString5 = jSONObject2.optString("p");
                            String optString6 = jSONObject2.optString("c");
                            jSONObject2.optString("f");
                            jSONObject2.optString("e");
                            Intent intent = new Intent();
                            if (!TextUtils.isEmpty(optString2)) {
                                intent.setData(Uri.parse(optString2));
                            }
                            if (!TextUtils.isEmpty(optString3)) {
                                intent.setAction(optString3);
                            }
                            if (!TextUtils.isEmpty(optString4)) {
                                intent.setType(optString4);
                            }
                            if (!TextUtils.isEmpty(optString5)) {
                                intent.setPackage(optString5);
                            }
                            if (!TextUtils.isEmpty(optString6)) {
                                String[] split = optString6.split("/", 2);
                                if (split.length == 2) {
                                    intent.setComponent(new ComponentName(split[0], split[1]));
                                }
                            }
                            try {
                                jSONObject.put(optString, packageManager.resolveActivity(intent, 65536) != null);
                            } catch (JSONException e) {
                                zzkd.zzb("Error constructing openable urls response.", e);
                            }
                        } catch (JSONException e2) {
                            zzkd.zzb("Error parsing the intent data.", e2);
                        }
                    }
                    zzlh.zzb("openableIntents", jSONObject);
                } catch (JSONException e3) {
                    zzlh.zzb("openableIntents", new JSONObject());
                }
            } catch (JSONException e4) {
                zzlh.zzb("openableIntents", new JSONObject());
            }
        }
    };
    public static final zzep zzbhq = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            Uri uri;
            String str = (String) map.get("u");
            if (str == null) {
                zzkd.zzcx("URL missing from click GMSG.");
                return;
            }
            Uri parse = Uri.parse(str);
            try {
                zzas zzul = zzlh.zzul();
                if (zzul != null && zzul.zzc(parse)) {
                    uri = zzul.zzb(parse, zzlh.getContext());
                    Future future = (Future) new zzkq(zzlh.getContext(), zzlh.zzum().zzcs, uri.toString()).zzpy();
                }
            } catch (zzat e) {
                String str2 = "Unable to append parameter to URL: ";
                String valueOf = String.valueOf(str);
                zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
            uri = parse;
            Future future2 = (Future) new zzkq(zzlh.getContext(), zzlh.zzum().zzcs, uri.toString()).zzpy();
        }
    };
    public static final zzep zzbhr = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            zzd zzuh = zzlh.zzuh();
            if (zzuh != null) {
                zzuh.close();
                return;
            }
            zzd zzui = zzlh.zzui();
            if (zzui != null) {
                zzui.close();
            } else {
                zzkd.zzcx("A GMSG tried to close something that wasn't an overlay.");
            }
        }
    };
    public static final zzep zzbhs = new zzep() {
        private void zzc(zzlh zzlh) {
            zzkd.zzcw("Received support message, responding.");
            boolean z = false;
            com.google.android.gms.ads.internal.zzd zzug = zzlh.zzug();
            if (zzug != null) {
                zzm zzm = zzug.zzakl;
                if (zzm != null) {
                    z = zzm.zzr(zzlh.getContext());
                }
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("event", "checkSupport");
                jSONObject.put("supports", z);
                zzlh.zzb("appStreaming", jSONObject);
            } catch (Throwable th) {
            }
        }

        public void zza(zzlh zzlh, Map<String, String> map) {
            if ("checkSupport".equals(map.get("action"))) {
                zzc(zzlh);
                return;
            }
            zzd zzuh = zzlh.zzuh();
            if (zzuh != null) {
                zzuh.zzf(zzlh, map);
            }
        }
    };
    public static final zzep zzbht = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            zzlh.zzai("1".equals(map.get("custom_close")));
        }
    };
    public static final zzep zzbhu = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("u");
            if (str == null) {
                zzkd.zzcx("URL missing from httpTrack GMSG.");
            } else {
                Future future = (Future) new zzkq(zzlh.getContext(), zzlh.zzum().zzcs, str).zzpy();
            }
        }
    };
    public static final zzep zzbhv = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = "Received log message: ";
            String valueOf = String.valueOf((String) map.get("string"));
            zzkd.zzcw(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
    };
    public static final zzep zzbhw = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("ty");
            String str2 = (String) map.get("td");
            try {
                int parseInt = Integer.parseInt((String) map.get("tx"));
                int parseInt2 = Integer.parseInt(str);
                int parseInt3 = Integer.parseInt(str2);
                zzas zzul = zzlh.zzul();
                if (zzul != null) {
                    zzul.zzaw().zza(parseInt, parseInt2, parseInt3);
                }
            } catch (NumberFormatException e) {
                zzkd.zzcx("Could not parse touch parameters from gmsg.");
            }
        }
    };
    public static final zzep zzbhx = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            if (((Boolean) zzdc.zzbba.get()).booleanValue()) {
                zzlh.zzaj(!Boolean.parseBoolean((String) map.get("disabled")));
            }
        }
    };
    public static final zzep zzbhy = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("action");
            if ("pause".equals(str)) {
                zzlh.zzef();
            } else if ("resume".equals(str)) {
                zzlh.zzeg();
            }
        }
    };
    public static final zzep zzbhz = new zzez();
    public static final zzep zzbia = new zzfa();
    public static final zzep zzbib = new zzfe();
    public static final zzep zzbic = new zzen();
    public static final zzex zzbid = new zzex();
}
