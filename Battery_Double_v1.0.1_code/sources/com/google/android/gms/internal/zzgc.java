package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzgl.zza;

@zzin
public final class zzgc extends zza {
    private final Object zzail = new Object();
    private zzge.zza zzboa;
    private zzgb zzbob;

    public void onAdClicked() {
        synchronized (this.zzail) {
            if (this.zzbob != null) {
                this.zzbob.zzdz();
            }
        }
    }

    public void onAdClosed() {
        synchronized (this.zzail) {
            if (this.zzbob != null) {
                this.zzbob.zzea();
            }
        }
    }

    public void onAdFailedToLoad(int i) {
        synchronized (this.zzail) {
            if (this.zzboa != null) {
                this.zzboa.zzy(i == 3 ? 1 : 2);
                this.zzboa = null;
            }
        }
    }

    public void onAdImpression() {
        synchronized (this.zzail) {
            if (this.zzbob != null) {
                this.zzbob.zzee();
            }
        }
    }

    public void onAdLeftApplication() {
        synchronized (this.zzail) {
            if (this.zzbob != null) {
                this.zzbob.zzeb();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public void onAdLoaded() {
        synchronized (this.zzail) {
            if (this.zzboa != null) {
                this.zzboa.zzy(0);
                this.zzboa = null;
            } else if (this.zzbob != null) {
                this.zzbob.zzed();
            }
        }
    }

    public void onAdOpened() {
        synchronized (this.zzail) {
            if (this.zzbob != null) {
                this.zzbob.zzec();
            }
        }
    }

    public void zza(@Nullable zzgb zzgb) {
        synchronized (this.zzail) {
            this.zzbob = zzgb;
        }
    }

    public void zza(zzge.zza zza) {
        synchronized (this.zzail) {
            this.zzboa = zza;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public void zza(zzgm zzgm) {
        synchronized (this.zzail) {
            if (this.zzboa != null) {
                this.zzboa.zza(0, zzgm);
                this.zzboa = null;
            } else if (this.zzbob != null) {
                this.zzbob.zzed();
            }
        }
    }
}
