package com.google.android.gms.internal;

import android.annotation.TargetApi;

@TargetApi(17)
@zzin
public class zzln {
    private final zzlh zzbgf;

    public zzln(zzlh zzlh) {
        this.zzbgf = zzlh;
    }
}
