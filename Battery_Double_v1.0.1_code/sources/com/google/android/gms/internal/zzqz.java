package com.google.android.gms.internal;

import android.os.Binder;

public abstract class zzqz<T> {
    private static zza vN = null;
    private static int vO = 0;
    private static String vP = "com.google.android.providers.gsf.permission.READ_GSERVICES";
    private static final Object zzamr = new Object();
    private T vQ = null;
    protected final String zzaxp;
    protected final T zzaxq;

    private interface zza {
        Long getLong(String str, Long l);

        String getString(String str, String str2);

        Boolean zza(String str, Boolean bool);

        Float zzb(String str, Float f);

        Integer zzb(String str, Integer num);
    }

    protected zzqz(String str, T t) {
        this.zzaxp = str;
        this.zzaxq = t;
    }

    public static zzqz<Float> zza(String str, Float f) {
        return new zzqz<Float>(str, f) {
            /* access modifiers changed from: protected */
            /* renamed from: zzhc */
            public Float zzgy(String str) {
                return zzqz.zzarb().zzb(this.zzaxp, (Float) this.zzaxq);
            }
        };
    }

    public static zzqz<Integer> zza(String str, Integer num) {
        return new zzqz<Integer>(str, num) {
            /* access modifiers changed from: protected */
            /* renamed from: zzhb */
            public Integer zzgy(String str) {
                return zzqz.zzarb().zzb(this.zzaxp, (Integer) this.zzaxq);
            }
        };
    }

    public static zzqz<Long> zza(String str, Long l) {
        return new zzqz<Long>(str, l) {
            /* access modifiers changed from: protected */
            /* renamed from: zzha */
            public Long zzgy(String str) {
                return zzqz.zzarb().getLong(this.zzaxp, (Long) this.zzaxq);
            }
        };
    }

    public static zzqz<String> zzab(String str, String str2) {
        return new zzqz<String>(str, str2) {
            /* access modifiers changed from: protected */
            /* renamed from: zzhd */
            public String zzgy(String str) {
                return zzqz.zzarb().getString(this.zzaxp, (String) this.zzaxq);
            }
        };
    }

    static /* synthetic */ zza zzarb() {
        return null;
    }

    public static zzqz<Boolean> zzm(String str, boolean z) {
        return new zzqz<Boolean>(str, Boolean.valueOf(z)) {
            /* access modifiers changed from: protected */
            /* renamed from: zzgz */
            public Boolean zzgy(String str) {
                return zzqz.zzarb().zza(this.zzaxp, (Boolean) this.zzaxq);
            }
        };
    }

    public final T get() {
        long clearCallingIdentity;
        try {
            return zzgy(this.zzaxp);
        } catch (SecurityException e) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            T zzgy = zzgy(this.zzaxp);
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return zzgy;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public abstract T zzgy(String str);
}
