package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

public final class zzanz implements zzani {
    private final zzanp bdR;

    private static final class zza<E> extends zzanh<Collection<E>> {
        private final zzanh<E> bfx;
        private final zzanu<? extends Collection<E>> bfy;

        public zza(zzamp zzamp, Type type, zzanh<E> zzanh, zzanu<? extends Collection<E>> zzanu) {
            this.bfx = new zzaoj(zzamp, zzanh, type);
            this.bfy = zzanu;
        }

        public void zza(zzaoo zzaoo, Collection<E> collection) throws IOException {
            if (collection == null) {
                zzaoo.l();
                return;
            }
            zzaoo.h();
            for (E zza : collection) {
                this.bfx.zza(zzaoo, zza);
            }
            zzaoo.i();
        }

        /* renamed from: zzj */
        public Collection<E> zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            Collection<E> collection = (Collection) this.bfy.zzczu();
            zzaom.beginArray();
            while (zzaom.hasNext()) {
                collection.add(this.bfx.zzb(zzaom));
            }
            zzaom.endArray();
            return collection;
        }
    }

    public zzanz(zzanp zzanp) {
        this.bdR = zzanp;
    }

    public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
        Type n = zzaol.n();
        Class m = zzaol.m();
        if (!Collection.class.isAssignableFrom(m)) {
            return null;
        }
        Type zza2 = zzano.zza(n, m);
        return new zza(zzamp, zza2, zzamp.zza(zzaol.zzl(zza2)), this.bdR.zzb(zzaol));
    }
}
