package com.google.android.gms.internal;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

public final class zzant<K, V> extends AbstractMap<K, V> implements Serializable {
    static final /* synthetic */ boolean $assertionsDisabled = (!zzant.class.desiredAssertionStatus());
    private static final Comparator<Comparable> beW = new Comparator<Comparable>() {
        /* renamed from: zza */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    };
    Comparator<? super K> aPZ;
    zzd<K, V> beX;
    final zzd<K, V> beY;
    private zza beZ;
    private zzb bfa;
    int modCount;
    int size;

    class zza extends AbstractSet<Entry<K, V>> {
        zza() {
        }

        public void clear() {
            zzant.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Entry) && zzant.this.zzc((Entry) obj) != null;
        }

        public Iterator<Entry<K, V>> iterator() {
            return new zzc<Entry<K, V>>() {
                {
                    zzant zzant = zzant.this;
                }

                public Entry<K, V> next() {
                    return zzczw();
                }
            };
        }

        public boolean remove(Object obj) {
            if (!(obj instanceof Entry)) {
                return false;
            }
            zzd zzc = zzant.this.zzc((Entry) obj);
            if (zzc == null) {
                return false;
            }
            zzant.this.zza(zzc, true);
            return true;
        }

        public int size() {
            return zzant.this.size;
        }
    }

    final class zzb extends AbstractSet<K> {
        zzb() {
        }

        public void clear() {
            zzant.this.clear();
        }

        public boolean contains(Object obj) {
            return zzant.this.containsKey(obj);
        }

        public Iterator<K> iterator() {
            return new zzc<K>() {
                {
                    zzant zzant = zzant.this;
                }

                public K next() {
                    return zzczw().aQn;
                }
            };
        }

        public boolean remove(Object obj) {
            return zzant.this.zzcn(obj) != null;
        }

        public int size() {
            return zzant.this.size;
        }
    }

    private abstract class zzc<T> implements Iterator<T> {
        zzd<K, V> bfe;
        zzd<K, V> bff;
        int bfg;

        private zzc() {
            this.bfe = zzant.this.beY.bfe;
            this.bff = null;
            this.bfg = zzant.this.modCount;
        }

        public final boolean hasNext() {
            return this.bfe != zzant.this.beY;
        }

        public final void remove() {
            if (this.bff == null) {
                throw new IllegalStateException();
            }
            zzant.this.zza(this.bff, true);
            this.bff = null;
            this.bfg = zzant.this.modCount;
        }

        /* access modifiers changed from: 0000 */
        public final zzd<K, V> zzczw() {
            zzd<K, V> zzd = this.bfe;
            if (zzd == zzant.this.beY) {
                throw new NoSuchElementException();
            } else if (zzant.this.modCount != this.bfg) {
                throw new ConcurrentModificationException();
            } else {
                this.bfe = zzd.bfe;
                this.bff = zzd;
                return zzd;
            }
        }
    }

    static final class zzd<K, V> implements Entry<K, V> {
        final K aQn;
        zzd<K, V> bfe;
        zzd<K, V> bfh;
        zzd<K, V> bfi;
        zzd<K, V> bfj;
        zzd<K, V> bfk;
        int height;
        V value;

        zzd() {
            this.aQn = null;
            this.bfk = this;
            this.bfe = this;
        }

        zzd(zzd<K, V> zzd, K k, zzd<K, V> zzd2, zzd<K, V> zzd3) {
            this.bfh = zzd;
            this.aQn = k;
            this.height = 1;
            this.bfe = zzd2;
            this.bfk = zzd3;
            zzd3.bfe = this;
            zzd2.bfk = this;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Entry)) {
                return false;
            }
            Entry entry = (Entry) obj;
            if (this.aQn == null) {
                if (entry.getKey() != null) {
                    return false;
                }
            } else if (!this.aQn.equals(entry.getKey())) {
                return false;
            }
            if (this.value == null) {
                if (entry.getValue() != null) {
                    return false;
                }
            } else if (!this.value.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        public K getKey() {
            return this.aQn;
        }

        public V getValue() {
            return this.value;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.aQn == null ? 0 : this.aQn.hashCode();
            if (this.value != null) {
                i = this.value.hashCode();
            }
            return hashCode ^ i;
        }

        public V setValue(V v) {
            V v2 = this.value;
            this.value = v;
            return v2;
        }

        public String toString() {
            String valueOf = String.valueOf(this.aQn);
            String valueOf2 = String.valueOf(this.value);
            return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append("=").append(valueOf2).toString();
        }

        public zzd<K, V> zzczx() {
            for (zzd<K, V> zzd = this.bfi; zzd != null; zzd = zzd.bfi) {
                this = zzd;
            }
            return this;
        }

        public zzd<K, V> zzczy() {
            for (zzd<K, V> zzd = this.bfj; zzd != null; zzd = zzd.bfj) {
                this = zzd;
            }
            return this;
        }
    }

    public zzant() {
        this(beW);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Comparator<? super K>, code=java.util.Comparator, for r2v0, types: [java.util.Comparator<? super K>, java.util.Comparator] */
    public zzant(Comparator comparator) {
        this.size = 0;
        this.modCount = 0;
        this.beY = new zzd<>();
        if (comparator == null) {
            comparator = beW;
        }
        this.aPZ = comparator;
    }

    private boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    private void zza(zzd<K, V> zzd2) {
        int i = 0;
        zzd<K, V> zzd3 = zzd2.bfi;
        zzd<K, V> zzd4 = zzd2.bfj;
        zzd<K, V> zzd5 = zzd4.bfi;
        zzd<K, V> zzd6 = zzd4.bfj;
        zzd2.bfj = zzd5;
        if (zzd5 != null) {
            zzd5.bfh = zzd2;
        }
        zza(zzd2, zzd4);
        zzd4.bfi = zzd2;
        zzd2.bfh = zzd4;
        zzd2.height = Math.max(zzd3 != null ? zzd3.height : 0, zzd5 != null ? zzd5.height : 0) + 1;
        int i2 = zzd2.height;
        if (zzd6 != null) {
            i = zzd6.height;
        }
        zzd4.height = Math.max(i2, i) + 1;
    }

    private void zza(zzd<K, V> zzd2, zzd<K, V> zzd3) {
        zzd<K, V> zzd4 = zzd2.bfh;
        zzd2.bfh = null;
        if (zzd3 != null) {
            zzd3.bfh = zzd4;
        }
        if (zzd4 == null) {
            this.beX = zzd3;
        } else if (zzd4.bfi == zzd2) {
            zzd4.bfi = zzd3;
        } else if ($assertionsDisabled || zzd4.bfj == zzd2) {
            zzd4.bfj = zzd3;
        } else {
            throw new AssertionError();
        }
    }

    private void zzb(zzd<K, V> zzd2) {
        int i = 0;
        zzd<K, V> zzd3 = zzd2.bfi;
        zzd<K, V> zzd4 = zzd2.bfj;
        zzd<K, V> zzd5 = zzd3.bfi;
        zzd<K, V> zzd6 = zzd3.bfj;
        zzd2.bfi = zzd6;
        if (zzd6 != null) {
            zzd6.bfh = zzd2;
        }
        zza(zzd2, zzd3);
        zzd3.bfj = zzd2;
        zzd2.bfh = zzd3;
        zzd2.height = Math.max(zzd4 != null ? zzd4.height : 0, zzd6 != null ? zzd6.height : 0) + 1;
        int i2 = zzd2.height;
        if (zzd5 != null) {
            i = zzd5.height;
        }
        zzd3.height = Math.max(i2, i) + 1;
    }

    private void zzb(zzd<K, V> zzd2, boolean z) {
        while (zzd2 != null) {
            zzd<K, V> zzd3 = zzd2.bfi;
            zzd<K, V> zzd4 = zzd2.bfj;
            int i = zzd3 != null ? zzd3.height : 0;
            int i2 = zzd4 != null ? zzd4.height : 0;
            int i3 = i - i2;
            if (i3 == -2) {
                zzd<K, V> zzd5 = zzd4.bfi;
                zzd<K, V> zzd6 = zzd4.bfj;
                int i4 = (zzd5 != null ? zzd5.height : 0) - (zzd6 != null ? zzd6.height : 0);
                if (i4 == -1 || (i4 == 0 && !z)) {
                    zza(zzd2);
                } else if ($assertionsDisabled || i4 == 1) {
                    zzb(zzd4);
                    zza(zzd2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 2) {
                zzd<K, V> zzd7 = zzd3.bfi;
                zzd<K, V> zzd8 = zzd3.bfj;
                int i5 = (zzd7 != null ? zzd7.height : 0) - (zzd8 != null ? zzd8.height : 0);
                if (i5 == 1 || (i5 == 0 && !z)) {
                    zzb(zzd2);
                } else if ($assertionsDisabled || i5 == -1) {
                    zza(zzd3);
                    zzb(zzd2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 0) {
                zzd2.height = i + 1;
                if (z) {
                    return;
                }
            } else if ($assertionsDisabled || i3 == -1 || i3 == 1) {
                zzd2.height = Math.max(i, i2) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            zzd2 = zzd2.bfh;
        }
    }

    public void clear() {
        this.beX = null;
        this.size = 0;
        this.modCount++;
        zzd<K, V> zzd2 = this.beY;
        zzd2.bfk = zzd2;
        zzd2.bfe = zzd2;
    }

    public boolean containsKey(Object obj) {
        return zzcm(obj) != null;
    }

    public Set<Entry<K, V>> entrySet() {
        zza zza2 = this.beZ;
        if (zza2 != null) {
            return zza2;
        }
        zza zza3 = new zza();
        this.beZ = zza3;
        return zza3;
    }

    public V get(Object obj) {
        zzd zzcm = zzcm(obj);
        if (zzcm != null) {
            return zzcm.value;
        }
        return null;
    }

    public Set<K> keySet() {
        zzb zzb2 = this.bfa;
        if (zzb2 != null) {
            return zzb2;
        }
        zzb zzb3 = new zzb();
        this.bfa = zzb3;
        return zzb3;
    }

    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        zzd zza2 = zza(k, true);
        V v2 = zza2.value;
        zza2.value = v;
        return v2;
    }

    public V remove(Object obj) {
        zzd zzcn = zzcn(obj);
        if (zzcn != null) {
            return zzcn.value;
        }
        return null;
    }

    public int size() {
        return this.size;
    }

    /* access modifiers changed from: 0000 */
    public zzd<K, V> zza(K k, boolean z) {
        zzd<K, V> zzd2;
        int i;
        zzd<K, V> zzd3;
        Comparator<? super K> comparator = this.aPZ;
        zzd<K, V> zzd4 = this.beX;
        if (zzd4 != null) {
            Comparable comparable = comparator == beW ? (Comparable) k : null;
            while (true) {
                int compare = comparable != null ? comparable.compareTo(zzd4.aQn) : comparator.compare(k, zzd4.aQn);
                if (compare == 0) {
                    return zzd4;
                }
                zzd<K, V> zzd5 = compare < 0 ? zzd4.bfi : zzd4.bfj;
                if (zzd5 == null) {
                    int i2 = compare;
                    zzd2 = zzd4;
                    i = i2;
                    break;
                }
                zzd4 = zzd5;
            }
        } else {
            zzd2 = zzd4;
            i = 0;
        }
        if (!z) {
            return null;
        }
        zzd<K, V> zzd6 = this.beY;
        if (zzd2 != null) {
            zzd3 = new zzd<>(zzd2, k, zzd6, zzd6.bfk);
            if (i < 0) {
                zzd2.bfi = zzd3;
            } else {
                zzd2.bfj = zzd3;
            }
            zzb(zzd2, true);
        } else if (comparator != beW || (k instanceof Comparable)) {
            zzd3 = new zzd<>(zzd2, k, zzd6, zzd6.bfk);
            this.beX = zzd3;
        } else {
            throw new ClassCastException(String.valueOf(k.getClass().getName()).concat(" is not Comparable"));
        }
        this.size++;
        this.modCount++;
        return zzd3;
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzd<K, V> zzd2, boolean z) {
        int i;
        int i2 = 0;
        if (z) {
            zzd2.bfk.bfe = zzd2.bfe;
            zzd2.bfe.bfk = zzd2.bfk;
        }
        zzd<K, V> zzd3 = zzd2.bfi;
        zzd<K, V> zzd4 = zzd2.bfj;
        zzd<K, V> zzd5 = zzd2.bfh;
        if (zzd3 == null || zzd4 == null) {
            if (zzd3 != null) {
                zza(zzd2, zzd3);
                zzd2.bfi = null;
            } else if (zzd4 != null) {
                zza(zzd2, zzd4);
                zzd2.bfj = null;
            } else {
                zza(zzd2, null);
            }
            zzb(zzd5, false);
            this.size--;
            this.modCount++;
            return;
        }
        zzd<K, V> zzczx = zzd3.height > zzd4.height ? zzd3.zzczy() : zzd4.zzczx();
        zza(zzczx, false);
        zzd<K, V> zzd6 = zzd2.bfi;
        if (zzd6 != null) {
            i = zzd6.height;
            zzczx.bfi = zzd6;
            zzd6.bfh = zzczx;
            zzd2.bfi = null;
        } else {
            i = 0;
        }
        zzd<K, V> zzd7 = zzd2.bfj;
        if (zzd7 != null) {
            i2 = zzd7.height;
            zzczx.bfj = zzd7;
            zzd7.bfh = zzczx;
            zzd2.bfj = null;
        }
        zzczx.height = Math.max(i, i2) + 1;
        zza(zzd2, zzczx);
    }

    /* access modifiers changed from: 0000 */
    public zzd<K, V> zzc(Entry<?, ?> entry) {
        zzd<K, V> zzcm = zzcm(entry.getKey());
        if (zzcm != null && equal(zzcm.value, entry.getValue())) {
            return zzcm;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public zzd<K, V> zzcm(Object obj) {
        zzd<K, V> zzd2 = null;
        if (obj == null) {
            return zzd2;
        }
        try {
            return zza((K) obj, false);
        } catch (ClassCastException e) {
            return zzd2;
        }
    }

    /* access modifiers changed from: 0000 */
    public zzd<K, V> zzcn(Object obj) {
        zzd<K, V> zzcm = zzcm(obj);
        if (zzcm != null) {
            zza(zzcm, true);
        }
        return zzcm;
    }
}
