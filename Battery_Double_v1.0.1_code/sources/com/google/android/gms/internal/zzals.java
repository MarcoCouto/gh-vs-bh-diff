package com.google.android.gms.internal;

public class zzals {
    private final int bbi;
    private final long bbj;

    public zzals(int i, long j) {
        this.bbi = i;
        this.bbj = j;
    }

    public int zzcxf() {
        return this.bbi;
    }

    public long zzcxg() {
        return this.bbj;
    }
}
