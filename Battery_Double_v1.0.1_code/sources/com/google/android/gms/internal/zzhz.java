package com.google.android.gms.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View.MeasureSpec;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzu;

@zzin
public class zzhz implements Runnable {
    /* access modifiers changed from: private */
    public final int zzaie;
    /* access modifiers changed from: private */
    public final int zzaif;
    protected final zzlh zzbgf;
    /* access modifiers changed from: private */
    public final Handler zzbxx;
    /* access modifiers changed from: private */
    public final long zzbxy;
    /* access modifiers changed from: private */
    public long zzbxz;
    /* access modifiers changed from: private */
    public com.google.android.gms.internal.zzli.zza zzbya;
    protected boolean zzbyb;
    protected boolean zzbyc;

    protected final class zza extends AsyncTask<Void, Void, Boolean> {
        private final WebView zzbyd;
        private Bitmap zzbye;

        public zza(WebView webView) {
            this.zzbyd = webView;
        }

        /* access modifiers changed from: protected */
        public synchronized void onPreExecute() {
            this.zzbye = Bitmap.createBitmap(zzhz.this.zzaie, zzhz.this.zzaif, Config.ARGB_8888);
            this.zzbyd.setVisibility(0);
            this.zzbyd.measure(MeasureSpec.makeMeasureSpec(zzhz.this.zzaie, 0), MeasureSpec.makeMeasureSpec(zzhz.this.zzaif, 0));
            this.zzbyd.layout(0, 0, zzhz.this.zzaie, zzhz.this.zzaif);
            this.zzbyd.draw(new Canvas(this.zzbye));
            this.zzbyd.invalidate();
        }

        /* access modifiers changed from: protected */
        /* renamed from: zza */
        public synchronized Boolean doInBackground(Void... voidArr) {
            Boolean bool;
            int width = this.zzbye.getWidth();
            int height = this.zzbye.getHeight();
            if (width == 0 || height == 0) {
                bool = Boolean.valueOf(false);
            } else {
                int i = 0;
                for (int i2 = 0; i2 < width; i2 += 10) {
                    for (int i3 = 0; i3 < height; i3 += 10) {
                        if (this.zzbye.getPixel(i2, i3) != 0) {
                            i++;
                        }
                    }
                }
                bool = Boolean.valueOf(((double) i) / (((double) (width * height)) / 100.0d) > 0.1d);
            }
            return bool;
        }

        /* access modifiers changed from: protected */
        /* renamed from: zza */
        public void onPostExecute(Boolean bool) {
            zzhz.zzc(zzhz.this);
            if (bool.booleanValue() || zzhz.this.zzqb() || zzhz.this.zzbxz <= 0) {
                zzhz.this.zzbyc = bool.booleanValue();
                zzhz.this.zzbya.zza(zzhz.this.zzbgf, true);
            } else if (zzhz.this.zzbxz > 0) {
                if (zzkd.zzaz(2)) {
                    zzkd.zzcv("Ad not detected, scheduling another run.");
                }
                zzhz.this.zzbxx.postDelayed(zzhz.this, zzhz.this.zzbxy);
            }
        }
    }

    public zzhz(com.google.android.gms.internal.zzli.zza zza2, zzlh zzlh, int i, int i2) {
        this(zza2, zzlh, i, i2, 200, 50);
    }

    public zzhz(com.google.android.gms.internal.zzli.zza zza2, zzlh zzlh, int i, int i2, long j, long j2) {
        this.zzbxy = j;
        this.zzbxz = j2;
        this.zzbxx = new Handler(Looper.getMainLooper());
        this.zzbgf = zzlh;
        this.zzbya = zza2;
        this.zzbyb = false;
        this.zzbyc = false;
        this.zzaif = i2;
        this.zzaie = i;
    }

    static /* synthetic */ long zzc(zzhz zzhz) {
        long j = zzhz.zzbxz - 1;
        zzhz.zzbxz = j;
        return j;
    }

    public void run() {
        if (this.zzbgf == null || zzqb()) {
            this.zzbya.zza(this.zzbgf, true);
        } else {
            new zza(this.zzbgf.getWebView()).execute(new Void[0]);
        }
    }

    public void zza(AdResponseParcel adResponseParcel) {
        zza(adResponseParcel, new zzlr(this, this.zzbgf, adResponseParcel.zzccf));
    }

    public void zza(AdResponseParcel adResponseParcel, zzlr zzlr) {
        this.zzbgf.setWebViewClient(zzlr);
        this.zzbgf.loadDataWithBaseURL(TextUtils.isEmpty(adResponseParcel.zzbto) ? null : zzu.zzfq().zzco(adResponseParcel.zzbto), adResponseParcel.body, "text/html", "UTF-8", null);
    }

    public void zzpz() {
        this.zzbxx.postDelayed(this, this.zzbxy);
    }

    public synchronized void zzqa() {
        this.zzbyb = true;
    }

    public synchronized boolean zzqb() {
        return this.zzbyb;
    }

    public boolean zzqc() {
        return this.zzbyc;
    }
}
