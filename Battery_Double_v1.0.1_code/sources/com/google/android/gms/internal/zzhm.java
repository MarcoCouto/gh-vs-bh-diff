package com.google.android.gms.internal;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzin
public class zzhm implements zzhk {
    private final Context mContext;
    final Set<WebView> zzbwh = Collections.synchronizedSet(new HashSet());

    public zzhm(Context context) {
        this.mContext = context;
    }

    public void zza(String str, final String str2, final String str3) {
        zzkd.zzcv("Fetching assets for the given html");
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                final WebView zzpl = zzhm.this.zzpl();
                zzpl.setWebViewClient(new WebViewClient() {
                    public void onPageFinished(WebView webView, String str) {
                        zzkd.zzcv("Loading assets have finished");
                        zzhm.this.zzbwh.remove(zzpl);
                    }

                    public void onReceivedError(WebView webView, int i, String str, String str2) {
                        zzkd.zzcx("Loading assets have failed.");
                        zzhm.this.zzbwh.remove(zzpl);
                    }
                });
                zzhm.this.zzbwh.add(zzpl);
                zzpl.loadDataWithBaseURL(str2, str3, "text/html", "UTF-8", null);
                zzkd.zzcv("Fetching assets finished.");
            }
        });
    }

    public WebView zzpl() {
        WebView webView = new WebView(this.mContext);
        webView.getSettings().setJavaScriptEnabled(true);
        return webView;
    }
}
