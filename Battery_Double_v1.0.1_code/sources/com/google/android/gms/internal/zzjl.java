package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzju.zza;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzjl extends zzkc implements zzjk {
    private final Context mContext;
    private final Object zzail = new Object();
    private final zza zzbxr;
    private final ArrayList<Future> zzchw = new ArrayList<>();
    private final ArrayList<String> zzchx = new ArrayList<>();
    private final HashSet<String> zzchy = new HashSet<>();
    /* access modifiers changed from: private */
    public final zzjf zzchz;

    public zzjl(Context context, zza zza, zzjf zzjf) {
        this.mContext = context;
        this.zzbxr = zza;
        this.zzchz = zzjf;
    }

    private zzju zza(int i, @Nullable String str, @Nullable zzfz zzfz) {
        return new zzju(this.zzbxr.zzcip.zzcar, null, this.zzbxr.zzciq.zzbnm, i, this.zzbxr.zzciq.zzbnn, this.zzbxr.zzciq.zzcca, this.zzbxr.zzciq.orientation, this.zzbxr.zzciq.zzbns, this.zzbxr.zzcip.zzcau, this.zzbxr.zzciq.zzcby, zzfz, null, str, this.zzbxr.zzcig, null, this.zzbxr.zzciq.zzcbz, this.zzbxr.zzapa, this.zzbxr.zzciq.zzcbx, this.zzbxr.zzcik, this.zzbxr.zzciq.zzccc, this.zzbxr.zzciq.zzccd, this.zzbxr.zzcie, null, this.zzbxr.zzciq.zzccn, this.zzbxr.zzciq.zzcco, this.zzbxr.zzciq.zzccp, this.zzbxr.zzciq.zzccq, this.zzbxr.zzciq.zzccr, null, this.zzbxr.zzciq.zzbnp);
    }

    private zzju zza(String str, zzfz zzfz) {
        return zza(-2, str, zzfz);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    private void zzd(String str, String str2, String str3) {
        synchronized (this.zzail) {
            zzjm zzcf = this.zzchz.zzcf(str);
            if (zzcf != null && zzcf.zzrv() != null && zzcf.zzru() != null) {
                this.zzchw.add((Future) zza(str, str2, str3, zzcf).zzpy());
                this.zzchx.add(str);
            }
        }
    }

    private zzju zzrt() {
        return zza(3, null, null);
    }

    public void onStop() {
    }

    /* access modifiers changed from: protected */
    public zzjg zza(String str, String str2, String str3, zzjm zzjm) {
        return new zzjg(this.mContext, str, str2, str3, this.zzbxr, zzjm, this);
    }

    public void zza(String str, int i) {
    }

    public void zzcg(String str) {
        synchronized (this.zzail) {
            this.zzchy.add(str);
        }
    }

    public void zzew() {
        for (zzfz zzfz : this.zzbxr.zzcig.zzbnk) {
            String str = zzfz.zzbnc;
            for (String str2 : zzfz.zzbmw) {
                if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str2)) {
                    try {
                        str2 = new JSONObject(str).getString("class_name");
                    } catch (JSONException e) {
                        zzkd.zzb("Unable to determine custom event class name, skipping...", e);
                    }
                }
                zzd(str2, str, zzfz.zzbmu);
            }
        }
        int i = 0;
        while (i < this.zzchw.size()) {
            try {
                ((Future) this.zzchw.get(i)).get();
                synchronized (this.zzail) {
                    if (this.zzchy.contains(this.zzchx.get(i))) {
                        final zzju zza = zza((String) this.zzchx.get(i), (zzfz) this.zzbxr.zzcig.zzbnk.get(i));
                        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
                            public void run() {
                                zzjl.this.zzchz.zzb(zza);
                            }
                        });
                        return;
                    }
                }
            } catch (InterruptedException e2) {
            } catch (Exception e3) {
            }
        }
        final zzju zzrt = zzrt();
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                zzjl.this.zzchz.zzb(zzrt);
            }
        });
        return;
        i++;
    }
}
