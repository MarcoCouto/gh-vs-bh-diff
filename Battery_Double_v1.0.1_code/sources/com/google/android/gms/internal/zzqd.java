package com.google.android.gms.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.zzc;
import com.google.android.gms.internal.zzpm.zza;

public class zzqd<O extends ApiOptions> extends zzpu {
    private final zzc<O> vb;

    public zzqd(zzc<O> zzc) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.vb = zzc;
    }

    public Looper getLooper() {
        return this.vb.getLooper();
    }

    public void zza(zzqx zzqx) {
        this.vb.zzanx();
    }

    public void zzb(zzqx zzqx) {
        this.vb.zzany();
    }

    public <A extends zzb, R extends Result, T extends zza<R, A>> T zzc(@NonNull T t) {
        return this.vb.zza(t);
    }

    public <A extends zzb, T extends zza<? extends Result, A>> T zzd(@NonNull T t) {
        return this.vb.zzb(t);
    }
}
