package com.google.android.gms.internal;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class zzaoa extends zzanh<Date> {
    public static final zzani bfu = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() == Date.class) {
                return new zzaoa();
            }
            return null;
        }
    };
    private final DateFormat bdE = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat bdF = DateFormat.getDateTimeInstance(2, 2);
    private final DateFormat bdG = a();

    private static DateFormat a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    private synchronized Date zztq(String str) {
        Date parse;
        try {
            parse = this.bdF.parse(str);
        } catch (ParseException e) {
            try {
                parse = this.bdE.parse(str);
            } catch (ParseException e2) {
                try {
                    parse = this.bdG.parse(str);
                } catch (ParseException e3) {
                    throw new zzane(str, e3);
                }
            }
        }
        return parse;
    }

    public synchronized void zza(zzaoo zzaoo, Date date) throws IOException {
        if (date == null) {
            zzaoo.l();
        } else {
            zzaoo.zzts(this.bdE.format(date));
        }
    }

    /* renamed from: zzk */
    public Date zzb(zzaom zzaom) throws IOException {
        if (zzaom.b() != zzaon.NULL) {
            return zztq(zzaom.nextString());
        }
        zzaom.nextNull();
        return null;
    }
}
