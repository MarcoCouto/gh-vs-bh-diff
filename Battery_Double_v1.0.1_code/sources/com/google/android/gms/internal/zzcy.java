package com.google.android.gms.internal;

import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzu;

@zzin
public abstract class zzcy<T> {
    private final int zzaxo;
    private final String zzaxp;
    private final T zzaxq;

    private zzcy(int i, String str, T t) {
        this.zzaxo = i;
        this.zzaxp = str;
        this.zzaxq = t;
        zzu.zzfy().zza(this);
    }

    public static zzcy<String> zza(int i, String str) {
        zzcy<String> zza = zza(i, str, (String) null);
        zzu.zzfy().zzb(zza);
        return zza;
    }

    public static zzcy<Integer> zza(int i, String str, int i2) {
        return new zzcy<Integer>(i, str, Integer.valueOf(i2)) {
            /* renamed from: zzc */
            public Integer zza(SharedPreferences sharedPreferences) {
                return Integer.valueOf(sharedPreferences.getInt(getKey(), ((Integer) zzjw()).intValue()));
            }
        };
    }

    public static zzcy<Long> zza(int i, String str, long j) {
        return new zzcy<Long>(i, str, Long.valueOf(j)) {
            /* renamed from: zzd */
            public Long zza(SharedPreferences sharedPreferences) {
                return Long.valueOf(sharedPreferences.getLong(getKey(), ((Long) zzjw()).longValue()));
            }
        };
    }

    public static zzcy<Boolean> zza(int i, String str, Boolean bool) {
        return new zzcy<Boolean>(i, str, bool) {
            /* renamed from: zzb */
            public Boolean zza(SharedPreferences sharedPreferences) {
                return Boolean.valueOf(sharedPreferences.getBoolean(getKey(), ((Boolean) zzjw()).booleanValue()));
            }
        };
    }

    public static zzcy<String> zza(int i, String str, String str2) {
        return new zzcy<String>(i, str, str2) {
            /* renamed from: zze */
            public String zza(SharedPreferences sharedPreferences) {
                return sharedPreferences.getString(getKey(), (String) zzjw());
            }
        };
    }

    public static zzcy<String> zzb(int i, String str) {
        zzcy<String> zza = zza(i, str, (String) null);
        zzu.zzfy().zzc(zza);
        return zza;
    }

    public T get() {
        return zzu.zzfz().zzd(this);
    }

    public String getKey() {
        return this.zzaxp;
    }

    /* access modifiers changed from: protected */
    public abstract T zza(SharedPreferences sharedPreferences);

    public T zzjw() {
        return this.zzaxq;
    }
}
