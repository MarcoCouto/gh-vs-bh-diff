package com.google.android.gms.internal;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class zzaeo {
    public static final Uri CONTENT_URI = Uri.parse("content://com.google.android.gsf.gservices");
    public static final Uri aLH = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    public static final Pattern aLI = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
    public static final Pattern aLJ = Pattern.compile("^(0|false|f|off|no|n)$", 2);
    static HashMap<String, String> aLK;
    /* access modifiers changed from: private */
    public static Object aLL;
    static HashSet<String> aLM = new HashSet<>();

    public static long getLong(ContentResolver contentResolver, String str, long j) {
        String string = getString(contentResolver, str);
        if (string == null) {
            return j;
        }
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException e) {
            return j;
        }
    }

    public static String getString(ContentResolver contentResolver, String str) {
        return zza(contentResolver, str, null);
    }

    public static String zza(ContentResolver contentResolver, String str, String str2) {
        synchronized (zzaeo.class) {
            zza(contentResolver);
            Object obj = aLL;
            if (aLK.containsKey(str)) {
                String str3 = (String) aLK.get(str);
                if (str3 != null) {
                    str2 = str3;
                }
            } else {
                Iterator it = aLM.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (str.startsWith((String) it.next())) {
                            break;
                        }
                    } else {
                        Cursor query = contentResolver.query(CONTENT_URI, null, null, new String[]{str}, null);
                        if (query != null) {
                            try {
                                if (query.moveToFirst()) {
                                    String string = query.getString(1);
                                    synchronized (zzaeo.class) {
                                        if (obj == aLL) {
                                            aLK.put(str, string);
                                        }
                                    }
                                    if (string != null) {
                                        str2 = string;
                                    }
                                    if (query != null) {
                                        query.close();
                                    }
                                }
                            } catch (Throwable th) {
                                if (query != null) {
                                    query.close();
                                }
                                throw th;
                            }
                        }
                        aLK.put(str, null);
                        if (query != null) {
                            query.close();
                        }
                    }
                }
            }
        }
        return str2;
    }

    public static Map<String, String> zza(ContentResolver contentResolver, String... strArr) {
        Cursor query = contentResolver.query(aLH, null, null, strArr, null);
        TreeMap treeMap = new TreeMap();
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    treeMap.put(query.getString(0), query.getString(1));
                } finally {
                    query.close();
                }
            }
        }
        return treeMap;
    }

    private static void zza(final ContentResolver contentResolver) {
        if (aLK == null) {
            aLK = new HashMap<>();
            aLL = new Object();
            new Thread("Gservices") {
                public void run() {
                    Looper.prepare();
                    contentResolver.registerContentObserver(zzaeo.CONTENT_URI, true, new ContentObserver(new Handler(Looper.myLooper())) {
                        public void onChange(boolean z) {
                            synchronized (zzaeo.class) {
                                zzaeo.aLK.clear();
                                zzaeo.aLL = new Object();
                                if (!zzaeo.aLM.isEmpty()) {
                                    zzaeo.zzb(contentResolver, (String[]) zzaeo.aLM.toArray(new String[zzaeo.aLM.size()]));
                                }
                            }
                        }
                    });
                    Looper.loop();
                }
            }.start();
        }
    }

    public static void zzb(ContentResolver contentResolver, String... strArr) {
        Map zza = zza(contentResolver, strArr);
        synchronized (zzaeo.class) {
            zza(contentResolver);
            aLM.addAll(Arrays.asList(strArr));
            for (Entry entry : zza.entrySet()) {
                aLK.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }
}
