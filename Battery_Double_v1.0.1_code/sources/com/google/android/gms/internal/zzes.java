package com.google.android.gms.internal;

import java.util.Map;

@zzin
public class zzes implements zzep {
    private final zzet zzbir;

    public zzes(zzet zzet) {
        this.zzbir = zzet;
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        float f;
        boolean equals = "1".equals(map.get("transparentBackground"));
        boolean equals2 = "1".equals(map.get("blur"));
        try {
            if (map.get("blurRadius") != null) {
                f = Float.parseFloat((String) map.get("blurRadius"));
                this.zzbir.zzg(equals);
                this.zzbir.zza(equals2, f);
            }
        } catch (NumberFormatException e) {
            zzkd.zzb("Fail to parse float", e);
        }
        f = 0.0f;
        this.zzbir.zzg(equals);
        this.zzbir.zza(equals2, f);
    }
}
