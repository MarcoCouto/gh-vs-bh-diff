package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public interface zzrr {

    public static class zza {
        private final long Bx;
        private final Map<String, String> By;

        /* renamed from: com.google.android.gms.internal.zzrr$zza$zza reason: collision with other inner class name */
        public static class C0086zza {
            /* access modifiers changed from: private */
            public long Bx = 43200;
            /* access modifiers changed from: private */
            public Map<String, String> By;

            public C0086zza zzah(long j) {
                this.Bx = j;
                return this;
            }

            public C0086zza zzah(String str, String str2) {
                if (this.By == null) {
                    this.By = new HashMap();
                }
                this.By.put(str, str2);
                return this;
            }

            public zza zzawj() {
                return new zza(this);
            }
        }

        private zza(C0086zza zza) {
            this.Bx = zza.Bx;
            this.By = zza.By;
        }

        public long zzawh() {
            return this.Bx;
        }

        public Map<String, String> zzawi() {
            return this.By == null ? Collections.emptyMap() : this.By;
        }
    }

    public interface zzb extends Result {
        Status getStatus();

        long getThrottleEndTimeMillis();

        byte[] zza(String str, byte[] bArr, String str2);

        Map<String, Set<String>> zzawk();
    }

    PendingResult<zzb> zza(GoogleApiClient googleApiClient, zza zza2);
}
