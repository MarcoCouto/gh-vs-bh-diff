package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzin
public class zzcm {
    private final Object zzail = new Object();
    private int zzash;
    private List<zzcl> zzasi = new LinkedList();

    public boolean zza(zzcl zzcl) {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzasi.contains(zzcl);
        }
        return z;
    }

    public boolean zzb(zzcl zzcl) {
        boolean z;
        synchronized (this.zzail) {
            Iterator it = this.zzasi.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                zzcl zzcl2 = (zzcl) it.next();
                if (zzcl != zzcl2 && zzcl2.zzhr().equals(zzcl.zzhr())) {
                    it.remove();
                    z = true;
                    break;
                }
            }
        }
        return z;
    }

    public void zzc(zzcl zzcl) {
        synchronized (this.zzail) {
            if (this.zzasi.size() >= 10) {
                zzkd.zzcv("Queue is full, current size = " + this.zzasi.size());
                this.zzasi.remove(0);
            }
            int i = this.zzash;
            this.zzash = i + 1;
            zzcl.zzl(i);
            this.zzasi.add(zzcl);
        }
    }

    @Nullable
    public zzcl zzhy() {
        int i;
        zzcl zzcl;
        zzcl zzcl2 = null;
        synchronized (this.zzail) {
            if (this.zzasi.size() == 0) {
                zzkd.zzcv("Queue empty");
                return null;
            } else if (this.zzasi.size() >= 2) {
                int i2 = Integer.MIN_VALUE;
                for (zzcl zzcl3 : this.zzasi) {
                    int score = zzcl3.getScore();
                    if (score > i2) {
                        int i3 = score;
                        zzcl = zzcl3;
                        i = i3;
                    } else {
                        i = i2;
                        zzcl = zzcl2;
                    }
                    i2 = i;
                    zzcl2 = zzcl;
                }
                this.zzasi.remove(zzcl2);
                return zzcl2;
            } else {
                zzcl zzcl4 = (zzcl) this.zzasi.get(0);
                zzcl4.zzht();
                return zzcl4;
            }
        }
    }
}
