package com.google.android.gms.internal;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.common.internal.zzaa;
import java.net.URI;
import java.net.URISyntaxException;

@zzin
public class zzlr extends WebViewClient {
    private final zzlh zzbgf;
    private final zzhz zzbyg;
    private final String zzcqm;
    private boolean zzcqn = false;

    public zzlr(zzhz zzhz, zzlh zzlh, String str) {
        this.zzcqm = zzdf(str);
        this.zzbgf = zzlh;
        this.zzbyg = zzhz;
    }

    private String zzdf(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            return str.endsWith("/") ? str.substring(0, str.length() - 1) : str;
        } catch (IndexOutOfBoundsException e) {
            zzkd.e(e.getMessage());
            return str;
        }
    }

    public void onLoadResource(WebView webView, String str) {
        String str2 = "JavascriptAdWebViewClient::onLoadResource: ";
        String valueOf = String.valueOf(str);
        zzkd.zzcv(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        if (!zzde(str)) {
            this.zzbgf.zzuj().onLoadResource(this.zzbgf.getWebView(), str);
        }
    }

    public void onPageFinished(WebView webView, String str) {
        String str2 = "JavascriptAdWebViewClient::onPageFinished: ";
        String valueOf = String.valueOf(str);
        zzkd.zzcv(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        if (!this.zzcqn) {
            this.zzbyg.zzpz();
            this.zzcqn = true;
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2 = "JavascriptAdWebViewClient::shouldOverrideUrlLoading: ";
        String valueOf = String.valueOf(str);
        zzkd.zzcv(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        if (!zzde(str)) {
            return this.zzbgf.zzuj().shouldOverrideUrlLoading(this.zzbgf.getWebView(), str);
        }
        zzkd.zzcv("shouldOverrideUrlLoading: received passback url");
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean zzde(String str) {
        String zzdf = zzdf(str);
        if (TextUtils.isEmpty(zzdf)) {
            return false;
        }
        try {
            URI uri = new URI(zzdf);
            if ("passback".equals(uri.getScheme())) {
                zzkd.zzcv("Passback received");
                this.zzbyg.zzqa();
                return true;
            } else if (TextUtils.isEmpty(this.zzcqm)) {
                return false;
            } else {
                URI uri2 = new URI(this.zzcqm);
                String host = uri2.getHost();
                String host2 = uri.getHost();
                String path = uri2.getPath();
                String path2 = uri.getPath();
                if (!zzaa.equal(host, host2) || !zzaa.equal(path, path2)) {
                    return false;
                }
                zzkd.zzcv("Passback received");
                this.zzbyg.zzqa();
                return true;
            }
        } catch (URISyntaxException e) {
            zzkd.e(e.getMessage());
            return false;
        }
    }
}
