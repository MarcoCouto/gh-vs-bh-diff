package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzpm.zzb;

public final class zzrg implements zzrf {

    private static class zza extends zzrd {
        private final zzb<Status> zv;

        public zza(zzb<Status> zzb) {
            this.zv = zzb;
        }

        public void zzgn(int i) throws RemoteException {
            this.zv.setResult(new Status(i));
        }
    }

    public PendingResult<Status> zzg(GoogleApiClient googleApiClient) {
        return googleApiClient.zzd(new zza(googleApiClient) {
            /* access modifiers changed from: protected */
            public void zza(zzri zzri) throws RemoteException {
                ((zzrk) zzri.zzasa()).zza(new zza(this));
            }
        });
    }
}
