package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzin
public class zzgh implements zzfy {
    private final Context mContext;
    private final Object zzail = new Object();
    private final zzdk zzajn;
    private final zzgj zzajz;
    private final boolean zzarl;
    private final boolean zzawn;
    private final zzga zzboe;
    private final AdRequestInfoParcel zzbot;
    private final long zzbou;
    private final long zzbov;
    private boolean zzbox = false;
    private List<zzge> zzboz = new ArrayList();
    private zzgd zzbpd;

    public zzgh(Context context, AdRequestInfoParcel adRequestInfoParcel, zzgj zzgj, zzga zzga, boolean z, boolean z2, long j, long j2, zzdk zzdk) {
        this.mContext = context;
        this.zzbot = adRequestInfoParcel;
        this.zzajz = zzgj;
        this.zzboe = zzga;
        this.zzarl = z;
        this.zzawn = z2;
        this.zzbou = j;
        this.zzbov = j2;
        this.zzajn = zzdk;
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.zzbox = true;
            if (this.zzbpd != null) {
                this.zzbpd.cancel();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a8, code lost:
        r2 = r21.zzbpd.zza(r21.zzbou, r21.zzbov);
        r21.zzboz.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00c1, code lost:
        if (r2.zzbom != 0) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00c3, code lost:
        com.google.android.gms.internal.zzkd.zzcv("Adapter succeeded.");
        r21.zzajn.zzh("mediation_network_succeed", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00d5, code lost:
        if (r15.isEmpty() != false) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d7, code lost:
        r21.zzajn.zzh("mediation_networks_fail", android.text.TextUtils.join(",", r15));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00e6, code lost:
        r21.zzajn.zza(r19, "mls");
        r21.zzajn.zza(r16, "ttm");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x010d, code lost:
        r15.add(r4);
        r21.zzajn.zza(r19, "mlf");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0123, code lost:
        if (r2.zzboo == null) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0125, code lost:
        com.google.android.gms.internal.zzkh.zzclc.post(new com.google.android.gms.internal.zzgh.AnonymousClass1(r21));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return r2;
     */
    public zzge zzd(List<zzfz> list) {
        zzkd.zzcv("Starting mediation.");
        ArrayList arrayList = new ArrayList();
        zzdi zzkg = this.zzajn.zzkg();
        for (zzfz zzfz : list) {
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(zzfz.zzbmv);
            zzkd.zzcw(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            Iterator it = zzfz.zzbmw.iterator();
            while (true) {
                if (it.hasNext()) {
                    String str2 = (String) it.next();
                    zzdi zzkg2 = this.zzajn.zzkg();
                    synchronized (this.zzail) {
                        if (this.zzbox) {
                            zzge zzge = new zzge(-1);
                            return zzge;
                        }
                        this.zzbpd = new zzgd(this.mContext, str2, this.zzajz, this.zzboe, zzfz, this.zzbot.zzcar, this.zzbot.zzapa, this.zzbot.zzaow, this.zzarl, this.zzawn, this.zzbot.zzapo, this.zzbot.zzaps);
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            this.zzajn.zzh("mediation_networks_fail", TextUtils.join(",", arrayList));
        }
        return new zzge(1);
    }

    public List<zzge> zzmg() {
        return this.zzboz;
    }
}
