package com.google.android.gms.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzab;
import java.util.concurrent.atomic.AtomicReference;

public class zzpm {

    public static abstract class zza<R extends Result, A extends com.google.android.gms.common.api.Api.zzb> extends zzpo<R> implements zzb<R> {
        private final Api<?> pN;
        private final zzc<A> sJ;
        private AtomicReference<zzb> sK = new AtomicReference<>();

        @Deprecated
        protected zza(zzc<A> zzc, GoogleApiClient googleApiClient) {
            super((GoogleApiClient) zzab.zzb(googleApiClient, (Object) "GoogleApiClient must not be null"));
            this.sJ = (zzc) zzab.zzy(zzc);
            this.pN = null;
        }

        protected zza(Api<?> api, GoogleApiClient googleApiClient) {
            super((GoogleApiClient) zzab.zzb(googleApiClient, (Object) "GoogleApiClient must not be null"));
            this.sJ = api.zzans();
            this.pN = api;
        }

        private void zza(RemoteException remoteException) {
            zzz(new Status(8, remoteException.getLocalizedMessage(), null));
        }

        public /* synthetic */ void setResult(Object obj) {
            super.zzc((Result) obj);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(A a) throws RemoteException;

        public void zza(zzb zzb) {
            this.sK.set(zzb);
        }

        public final zzc<A> zzans() {
            return this.sJ;
        }

        public final Api<?> zzanz() {
            return this.pN;
        }

        public void zzaor() {
            setResultCallback(null);
        }

        /* access modifiers changed from: protected */
        public void zzaos() {
            zzb zzb = (zzb) this.sK.getAndSet(null);
            if (zzb != null) {
                zzb.zzh(this);
            }
        }

        public final void zzb(A a) throws DeadObjectException {
            try {
                zza(a);
            } catch (DeadObjectException e) {
                zza((RemoteException) e);
                throw e;
            } catch (RemoteException e2) {
                zza(e2);
            }
        }

        /* access modifiers changed from: protected */
        public void zzb(R r) {
        }

        public final void zzz(Status status) {
            zzab.zzb(!status.isSuccess(), (Object) "Failed result must not be success");
            Result zzc = zzc(status);
            zzc(zzc);
            zzb((R) zzc);
        }
    }

    public interface zzb<R> {
        void setResult(R r);

        void zzz(Status status);
    }
}
