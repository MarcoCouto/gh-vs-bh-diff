package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.zzc;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.lang.reflect.Field;

public final class zzsb {
    private static zzsc KG;
    private static final zza KH = new zza() {
        public int zzd(Context context, String str, boolean z) {
            return zzsb.zzd(context, str, z);
        }

        public int zzt(Context context, String str) {
            return zzsb.zzt(context, str);
        }
    };
    public static final zzb KI = new zzb() {
        public C0088zzb zza(Context context, String str, zza zza) {
            C0088zzb zzb = new C0088zzb();
            zzb.KQ = zza.zzd(context, str, true);
            if (zzb.KQ != 0) {
                zzb.KR = 1;
            } else {
                zzb.KP = zza.zzt(context, str);
                if (zzb.KP != 0) {
                    zzb.KR = -1;
                }
            }
            return zzb;
        }
    };
    public static final zzb KJ = new zzb() {
        public C0088zzb zza(Context context, String str, zza zza) {
            C0088zzb zzb = new C0088zzb();
            zzb.KP = zza.zzt(context, str);
            if (zzb.KP != 0) {
                zzb.KR = -1;
            } else {
                zzb.KQ = zza.zzd(context, str, true);
                if (zzb.KQ != 0) {
                    zzb.KR = 1;
                }
            }
            return zzb;
        }
    };
    public static final zzb KK = new zzb() {
        public C0088zzb zza(Context context, String str, zza zza) {
            C0088zzb zzb = new C0088zzb();
            zzb.KP = zza.zzt(context, str);
            zzb.KQ = zza.zzd(context, str, true);
            if (zzb.KP == 0 && zzb.KQ == 0) {
                zzb.KR = 0;
            } else if (zzb.KP >= zzb.KQ) {
                zzb.KR = -1;
            } else {
                zzb.KR = 1;
            }
            return zzb;
        }
    };
    public static final zzb KL = new zzb() {
        public C0088zzb zza(Context context, String str, zza zza) {
            C0088zzb zzb = new C0088zzb();
            zzb.KP = zza.zzt(context, str);
            zzb.KQ = zza.zzd(context, str, true);
            if (zzb.KP == 0 && zzb.KQ == 0) {
                zzb.KR = 0;
            } else if (zzb.KQ >= zzb.KP) {
                zzb.KR = 1;
            } else {
                zzb.KR = -1;
            }
            return zzb;
        }
    };
    public static final zzb KM = new zzb() {
        public C0088zzb zza(Context context, String str, zza zza) {
            C0088zzb zzb = new C0088zzb();
            zzb.KP = zza.zzt(context, str);
            if (zzb.KP != 0) {
                zzb.KQ = zza.zzd(context, str, false);
            } else {
                zzb.KQ = zza.zzd(context, str, true);
            }
            if (zzb.KP == 0 && zzb.KQ == 0) {
                zzb.KR = 0;
            } else if (zzb.KQ >= zzb.KP) {
                zzb.KR = 1;
            } else {
                zzb.KR = -1;
            }
            return zzb;
        }
    };
    private final Context KN;

    public static class zza extends Exception {
        private zza(String str) {
            super(str);
        }

        private zza(String str, Throwable th) {
            super(str, th);
        }
    }

    public interface zzb {

        public interface zza {
            int zzd(Context context, String str, boolean z);

            int zzt(Context context, String str);
        }

        /* renamed from: com.google.android.gms.internal.zzsb$zzb$zzb reason: collision with other inner class name */
        public static class C0088zzb {
            public int KP = 0;
            public int KQ = 0;
            public int KR = 0;
        }

        C0088zzb zza(Context context, String str, zza zza2);
    }

    private zzsb(Context context) {
        this.KN = (Context) zzab.zzy(context);
    }

    public static zzsb zza(Context context, zzb zzb2, String str) throws zza {
        C0088zzb zza2 = zzb2.zza(context, str, KH);
        Log.i("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 68 + String.valueOf(str).length()).append("Considering local module ").append(str).append(":").append(zza2.KP).append(" and remote module ").append(str).append(":").append(zza2.KQ).toString());
        if (zza2.KR == 0 || ((zza2.KR == -1 && zza2.KP == 0) || (zza2.KR == 1 && zza2.KQ == 0))) {
            throw new zza("No acceptable module found. Local version is " + zza2.KP + " and remote version is " + zza2.KQ + ".");
        } else if (zza2.KR == -1) {
            return zzv(context, str);
        } else {
            if (zza2.KR == 1) {
                try {
                    return zza(context, str, zza2.KQ);
                } catch (zza e) {
                    zza zza3 = e;
                    String str2 = "DynamiteModule";
                    String str3 = "Failed to load remote module: ";
                    String valueOf = String.valueOf(zza3.getMessage());
                    Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
                    if (zza2.KP != 0) {
                        final int i = zza2.KP;
                        if (zzb2.zza(context, str, new zza() {
                            public int zzd(Context context, String str, boolean z) {
                                return 0;
                            }

                            public int zzt(Context context, String str) {
                                return i;
                            }
                        }).KR == -1) {
                            return zzv(context, str);
                        }
                    }
                    throw new zza("Remote load failed. No local fallback found.", zza3);
                }
            } else {
                throw new zza("VersionPolicy returned invalid code:" + zza2.KR);
            }
        }
    }

    private static zzsb zza(Context context, String str, int i) throws zza {
        Log.i("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 51).append("Selected remote version of ").append(str).append(", version >= ").append(i).toString());
        zzsc zzcs = zzcs(context);
        if (zzcs == null) {
            throw new zza("Failed to create IDynamiteLoader.");
        }
        try {
            zzd zza2 = zzcs.zza(zze.zzac(context), str, i);
            if (zze.zzad(zza2) != null) {
                return new zzsb((Context) zze.zzad(zza2));
            }
            throw new zza("Failed to load remote module.");
        } catch (RemoteException e) {
            throw new zza("Failed to load remote module.", e);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private static zzsc zzcs(Context context) {
        synchronized (zzsb.class) {
            if (KG != null) {
                zzsc zzsc = KG;
                return zzsc;
            } else if (zzc.zzang().isGooglePlayServicesAvailable(context) != 0) {
                return null;
            } else {
                try {
                    zzsc zzfd = com.google.android.gms.internal.zzsc.zza.zzfd((IBinder) context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance());
                    if (zzfd != null) {
                        KG = zzfd;
                        return zzfd;
                    }
                } catch (Exception e) {
                    String str = "DynamiteModule";
                    String str2 = "Failed to load IDynamiteLoader from GmsCore: ";
                    String valueOf = String.valueOf(e.getMessage());
                    Log.e(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
                    return null;
                }
            }
        }
    }

    public static int zzd(Context context, String str, boolean z) {
        zzsc zzcs = zzcs(context);
        if (zzcs == null) {
            return 0;
        }
        try {
            return zzcs.zza(zze.zzac(context), str, z);
        } catch (RemoteException e) {
            String str2 = "DynamiteModule";
            String str3 = "Failed to retrieve remote module version: ";
            String valueOf = String.valueOf(e.getMessage());
            Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            return 0;
        }
    }

    public static int zzt(Context context, String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            String valueOf = String.valueOf("com.google.android.gms.dynamite.descriptors.");
            String valueOf2 = String.valueOf("ModuleDescriptor");
            Class loadClass = classLoader.loadClass(new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length() + String.valueOf(valueOf2).length()).append(valueOf).append(str).append(".").append(valueOf2).toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (declaredField.get(null).equals(str)) {
                return declaredField2.getInt(null);
            }
            String valueOf3 = String.valueOf(declaredField.get(null));
            Log.e("DynamiteModule", new StringBuilder(String.valueOf(valueOf3).length() + 51 + String.valueOf(str).length()).append("Module descriptor id '").append(valueOf3).append("' didn't match expected id '").append(str).append("'").toString());
            return 0;
        } catch (ClassNotFoundException e) {
            Log.w("DynamiteModule", new StringBuilder(String.valueOf(str).length() + 45).append("Local module descriptor class for ").append(str).append(" not found.").toString());
            return 0;
        } catch (Exception e2) {
            String str2 = "DynamiteModule";
            String str3 = "Failed to load module descriptor class: ";
            String valueOf4 = String.valueOf(e2.getMessage());
            Log.e(str2, valueOf4.length() != 0 ? str3.concat(valueOf4) : new String(str3));
            return 0;
        }
    }

    public static int zzu(Context context, String str) {
        return zzd(context, str, false);
    }

    private static zzsb zzv(Context context, String str) {
        String str2 = "DynamiteModule";
        String str3 = "Selected local version of ";
        String valueOf = String.valueOf(str);
        Log.i(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
        return new zzsb(context.getApplicationContext());
    }

    public Context zzbby() {
        return this.KN;
    }

    public IBinder zziu(String str) throws zza {
        try {
            return (IBinder) this.KN.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            String str2 = "Failed to instantiate module class: ";
            String valueOf = String.valueOf(str);
            throw new zza(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2), e);
        }
    }
}
