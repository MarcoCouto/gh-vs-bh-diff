package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzeu implements zzep {
    final HashMap<String, zzkv<JSONObject>> zzbis = new HashMap<>();

    public void zza(zzlh zzlh, Map<String, String> map) {
        zzi((String) map.get("request_id"), (String) map.get("fetched_ad"));
    }

    public Future<JSONObject> zzaw(String str) {
        zzkv zzkv = new zzkv();
        this.zzbis.put(str, zzkv);
        return zzkv;
    }

    public void zzax(String str) {
        zzkv zzkv = (zzkv) this.zzbis.get(str);
        if (zzkv == null) {
            zzkd.e("Could not find the ad request for the corresponding ad response.");
            return;
        }
        if (!zzkv.isDone()) {
            zzkv.cancel(true);
        }
        this.zzbis.remove(str);
    }

    public void zzi(String str, String str2) {
        zzkd.zzcv("Received ad from the cache.");
        zzkv zzkv = (zzkv) this.zzbis.get(str);
        if (zzkv == null) {
            zzkd.e("Could not find the ad request for the corresponding ad response.");
            return;
        }
        try {
            zzkv.zzh(new JSONObject(str2));
        } catch (JSONException e) {
            zzkd.zzb("Failed constructing JSON object from value passed from javascript", e);
            zzkv.zzh(null);
        } finally {
            this.zzbis.remove(str);
        }
    }
}
