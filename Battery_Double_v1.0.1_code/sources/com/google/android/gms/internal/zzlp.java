package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.zzu;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@TargetApi(11)
@zzin
public class zzlp extends zzli {
    public zzlp(zzlh zzlh, boolean z) {
        super(zzlh, z);
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (this.zzcot != null) {
            this.zzcot.zzcj(str);
        }
        try {
            if (!"mraid.js".equalsIgnoreCase(new File(str).getName())) {
                return super.shouldInterceptRequest(webView, str);
            }
            if (!(webView instanceof zzlh)) {
                zzkd.zzcx("Tried to intercept request from a WebView that wasn't an AdWebView.");
                return super.shouldInterceptRequest(webView, str);
            }
            zzlh zzlh = (zzlh) webView;
            zzlh.zzuj().zznx();
            String str2 = zzlh.zzdn().zzaus ? (String) zzdc.zzazd.get() : zzlh.zzun() ? (String) zzdc.zzazc.get() : (String) zzdc.zzazb.get();
            zzkd.v(new StringBuilder(String.valueOf(str2).length() + 24).append("shouldInterceptRequest(").append(str2).append(")").toString());
            return zzd(zzlh.getContext(), this.zzbgf.zzum().zzcs, str2);
        } catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
            String str3 = "Could not fetch MRAID JS. ";
            String valueOf = String.valueOf(e.getMessage());
            zzkd.zzcx(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            return super.shouldInterceptRequest(webView, str);
        }
    }

    /* access modifiers changed from: protected */
    public WebResourceResponse zzd(Context context, String str, String str2) throws IOException, ExecutionException, InterruptedException, TimeoutException {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", zzu.zzfq().zzg(context, str));
        hashMap.put("Cache-Control", "max-stale=3600");
        String str3 = (String) new zzkn(context).zzc(str2, hashMap).get(60, TimeUnit.SECONDS);
        if (str3 == null) {
            return null;
        }
        return new WebResourceResponse("application/javascript", "UTF-8", new ByteArrayInputStream(str3.getBytes("UTF-8")));
    }
}
