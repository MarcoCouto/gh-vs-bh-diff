package com.google.android.gms.internal;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzin
public class zzfg extends zzfd {
    private static final Set<String> zzbjp = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzbjq = new DecimalFormat("#,###");
    private File zzbjr;
    private boolean zzbjs;

    public zzfg(zzlh zzlh) {
        super(zzlh);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzkd.zzcx("Context.getCacheDir() returned null");
            return;
        }
        this.zzbjr = new File(cacheDir, "admobVideoStreams");
        if (!this.zzbjr.isDirectory() && !this.zzbjr.mkdirs()) {
            String str = "Could not create preload cache directory at ";
            String valueOf = String.valueOf(this.zzbjr.getAbsolutePath());
            zzkd.zzcx(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.zzbjr = null;
        } else if (!this.zzbjr.setReadable(true, false) || !this.zzbjr.setExecutable(true, false)) {
            String str2 = "Could not set cache file permissions at ";
            String valueOf2 = String.valueOf(this.zzbjr.getAbsolutePath());
            zzkd.zzcx(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            this.zzbjr = null;
        }
    }

    private File zzb(File file) {
        return new File(this.zzbjr, String.valueOf(file.getName()).concat(".done"));
    }

    private static void zzc(File file) {
        if (file.isFile()) {
            file.setLastModified(System.currentTimeMillis());
            return;
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
        }
    }

    public void abort() {
        this.zzbjs = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x030c, code lost:
        r5 = r5 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x030d, code lost:
        if (r5 <= r14) goto L_0x033c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x030f, code lost:
        r4 = "sizeExceeded";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0311, code lost:
        r2 = "File too big for full file cache. Size: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x031f, code lost:
        if (r3.length() == 0) goto L_0x0331;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0321, code lost:
        r3 = r2.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x032c, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x032d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x032e, code lost:
        r5 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        r3 = new java.lang.String(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0337, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0338, code lost:
        r3 = null;
        r5 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        r17.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0343, code lost:
        if (r16.write(r17) > 0) goto L_0x033f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0345, code lost:
        r17.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0354, code lost:
        if ((r18.currentTimeMillis() - r20) <= (1000 * r22)) goto L_0x038f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0356, code lost:
        r4 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:?, code lost:
        r2 = java.lang.String.valueOf(java.lang.Long.toString(r22));
        r3 = new java.lang.StringBuilder(java.lang.String.valueOf(r2).length() + 29).append("Timeout exceeded. Limit: ").append(r2).append(" sec").toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x038a, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x038b, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x038c, code lost:
        r5 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0393, code lost:
        if (r26.zzbjs == false) goto L_0x03a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0395, code lost:
        r4 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x039e, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x039f, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x03a0, code lost:
        r3 = null;
        r5 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x03a8, code lost:
        if (r0.tryAcquire() == false) goto L_0x0304;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x03aa, code lost:
        zza(r27, r12.getAbsolutePath(), r5, r6, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x03b8, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x03b9, code lost:
        r3 = null;
        r4 = r10;
        r5 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x03be, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x03c6, code lost:
        if (com.google.android.gms.internal.zzkd.zzaz(3) == false) goto L_0x0404;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x03c8, code lost:
        r2 = zzbjq.format((long) r5);
        com.google.android.gms.internal.zzkd.zzcv(new java.lang.StringBuilder((java.lang.String.valueOf(r2).length() + 22) + java.lang.String.valueOf(r27).length()).append("Preloaded ").append(r2).append(" bytes from ").append(r27).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0404, code lost:
        r12.setReadable(true, false);
        zzc(r13);
        zza(r27, r12.getAbsolutePath(), r5);
        zzbjp.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x041f, code lost:
        com.google.android.gms.internal.zzkd.zzd(new java.lang.StringBuilder(java.lang.String.valueOf(r27).length() + 25).append("Preload failed for URL \"").append(r27).append("\"").toString(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0449, code lost:
        r2 = new java.lang.String(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0456, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0457, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x045d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x045e, code lost:
        r3 = null;
        r4 = r10;
        r5 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e8, code lost:
        r5 = null;
        r10 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r3 = new java.net.URL(r27).openConnection();
        r2 = ((java.lang.Integer) com.google.android.gms.internal.zzdc.zzayr.get()).intValue();
        r3.setConnectTimeout(r2);
        r3.setReadTimeout(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x010b, code lost:
        if ((r3 instanceof java.net.HttpURLConnection) == false) goto L_0x01dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x010d, code lost:
        r2 = ((java.net.HttpURLConnection) r3).getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0117, code lost:
        if (r2 < 400) goto L_0x01dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0119, code lost:
        r4 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x011b, code lost:
        r6 = "HTTP request failed. Code: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0129, code lost:
        if (r3.length() == 0) goto L_0x01d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x012b, code lost:
        r3 = r6.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x015d, code lost:
        throw new java.io.IOException(new java.lang.StringBuilder(java.lang.String.valueOf(r27).length() + 32).append("HTTP status code ").append(r2).append(" at ").append(r27).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x015e, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0161, code lost:
        if ((r2 instanceof java.lang.RuntimeException) != false) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0163, code lost:
        com.google.android.gms.ads.internal.zzu.zzft().zzb(r2, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0172, code lost:
        if (r26.zzbjs != false) goto L_0x0174;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0174, code lost:
        com.google.android.gms.internal.zzkd.zzcw(new java.lang.StringBuilder(java.lang.String.valueOf(r27).length() + 26).append("Preload aborted for URL \"").append(r27).append("\"").toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01a8, code lost:
        r5 = "Could not delete partial cache file at ";
        r2 = java.lang.String.valueOf(r12.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01b6, code lost:
        if (r2.length() != 0) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01b8, code lost:
        r2 = r5.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01bc, code lost:
        com.google.android.gms.internal.zzkd.zzcx(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01bf, code lost:
        zza(r27, r12.getAbsolutePath(), r4, r3);
        zzbjp.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r3 = new java.lang.String(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01d9, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01da, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r6 = r3.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01e0, code lost:
        if (r6 >= 0) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01e2, code lost:
        r3 = "Stream cache aborted, missing content-length header at ";
        r2 = java.lang.String.valueOf(r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01ec, code lost:
        if (r2.length() == 0) goto L_0x020b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01ee, code lost:
        r2 = r3.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01f2, code lost:
        com.google.android.gms.internal.zzkd.zzcx(r2);
        zza(r27, r12.getAbsolutePath(), "contentLengthMissing", null);
        zzbjp.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x020b, code lost:
        r2 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0211, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0212, code lost:
        r3 = null;
        r4 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0216, code lost:
        r4 = zzbjq.format((long) r6);
        r14 = ((java.lang.Integer) com.google.android.gms.internal.zzdc.zzayn.get()).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0229, code lost:
        if (r6 <= r14) goto L_0x0290;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x022b, code lost:
        com.google.android.gms.internal.zzkd.zzcx(new java.lang.StringBuilder((java.lang.String.valueOf(r4).length() + 33) + java.lang.String.valueOf(r27).length()).append("Content length ").append(r4).append(" exceeds limit at ").append(r27).toString());
        r3 = "File too big for full file cache. Size: ";
        r2 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x026a, code lost:
        if (r2.length() == 0) goto L_0x0285;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x026c, code lost:
        r2 = r3.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0270, code lost:
        zza(r27, r12.getAbsolutePath(), "sizeExceeded", r2);
        zzbjp.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0285, code lost:
        r2 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x028b, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x028c, code lost:
        r3 = null;
        r4 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0290, code lost:
        com.google.android.gms.internal.zzkd.zzcv(new java.lang.StringBuilder((java.lang.String.valueOf(r4).length() + 20) + java.lang.String.valueOf(r27).length()).append("Caching ").append(r4).append(" bytes from ").append(r27).toString());
        r15 = java.nio.channels.Channels.newChannel(r3.getInputStream());
        r11 = new java.io.FileOutputStream(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        r16 = r11.getChannel();
        r17 = java.nio.ByteBuffer.allocate(1048576);
        r18 = com.google.android.gms.ads.internal.zzu.zzfu();
        r5 = 0;
        r20 = r18.currentTimeMillis();
        r0 = new com.google.android.gms.internal.zzkr(((java.lang.Long) com.google.android.gms.internal.zzdc.zzayq.get()).longValue());
        r22 = ((java.lang.Long) com.google.android.gms.internal.zzdc.zzayp.get()).longValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0304, code lost:
        r2 = r15.read(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x030a, code lost:
        if (r2 < 0) goto L_0x03be;
     */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x041f  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0449  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01b8  */
    public boolean zzaz(String str) {
        if (this.zzbjr == null) {
            zza(str, null, "noCacheDir", null);
            return false;
        }
        while (zzll() > ((Integer) zzdc.zzaym.get()).intValue()) {
            if (!zzlm()) {
                zzkd.zzcx("Unable to expire stream cache");
                zza(str, null, "expireFailed", null);
                return false;
            }
        }
        File file = new File(this.zzbjr, zzba(str));
        File zzb = zzb(file);
        if (!file.isFile() || !zzb.isFile()) {
            String valueOf = String.valueOf(this.zzbjr.getAbsolutePath());
            String valueOf2 = String.valueOf(str);
            String str2 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
            synchronized (zzbjp) {
                if (zzbjp.contains(str2)) {
                    String str3 = "Stream cache already in progress at ";
                    String valueOf3 = String.valueOf(str);
                    zzkd.zzcx(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3));
                    zza(str, file.getAbsolutePath(), "inProgress", null);
                    return false;
                }
                zzbjp.add(str2);
            }
        } else {
            int length = (int) file.length();
            String str4 = "Stream cache hit at ";
            String valueOf4 = String.valueOf(str);
            zzkd.zzcv(valueOf4.length() != 0 ? str4.concat(valueOf4) : new String(str4));
            zza(str, file.getAbsolutePath(), length);
            return true;
        }
    }

    public int zzll() {
        int i = 0;
        if (this.zzbjr != null) {
            for (File name : this.zzbjr.listFiles()) {
                if (!name.getName().endsWith(".done")) {
                    i++;
                }
            }
        }
        return i;
    }

    public boolean zzlm() {
        boolean z;
        long j;
        File file;
        if (this.zzbjr == null) {
            return false;
        }
        File file2 = null;
        long j2 = Long.MAX_VALUE;
        File[] listFiles = this.zzbjr.listFiles();
        int length = listFiles.length;
        int i = 0;
        while (i < length) {
            File file3 = listFiles[i];
            if (!file3.getName().endsWith(".done")) {
                j = file3.lastModified();
                if (j < j2) {
                    file = file3;
                    i++;
                    file2 = file;
                    j2 = j;
                }
            }
            j = j2;
            file = file2;
            i++;
            file2 = file;
            j2 = j;
        }
        if (file2 != null) {
            z = file2.delete();
            File zzb = zzb(file2);
            if (zzb.isFile()) {
                z &= zzb.delete();
            }
        } else {
            z = false;
        }
        return z;
    }
}
