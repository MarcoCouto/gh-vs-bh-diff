package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;

public class zzbb extends zzbp {
    private static final Object zzafc = new Object();
    private static volatile String zzagz = null;

    public zzbb(zzax zzax, String str, String str2, zza zza, int i, int i2) {
        super(zzax, str, str2, zza, i, i2);
    }

    /* access modifiers changed from: protected */
    public void zzcu() throws IllegalAccessException, InvocationTargetException {
        this.zzaha.zzdo = "E";
        if (zzagz == null) {
            synchronized (zzafc) {
                if (zzagz == null) {
                    zzagz = zzaj.zza(((ByteBuffer) this.zzahh.invoke(null, new Object[]{this.zzaey.getContext()})).array(), true);
                }
            }
        }
        synchronized (this.zzaha) {
            this.zzaha.zzdo = zzagz;
        }
    }
}
