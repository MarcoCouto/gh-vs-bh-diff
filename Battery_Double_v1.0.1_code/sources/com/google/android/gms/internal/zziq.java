package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.SearchAdRequestParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zziz.zza;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zziq {
    private static final SimpleDateFormat zzcel = new SimpleDateFormat("yyyyMMdd", Locale.US);

    /* JADX WARNING: Removed duplicated region for block: B:53:0x013b A[Catch:{ JSONException -> 0x0220 }] */
    public static AdResponseParcel zza(Context context, AdRequestInfoParcel adRequestInfoParcel, String str) {
        String str2;
        long j;
        boolean optBoolean;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("ad_base_url", null);
            String optString2 = jSONObject.optString("ad_url", null);
            String optString3 = jSONObject.optString("ad_size", null);
            String optString4 = jSONObject.optString("ad_slot_size", optString3);
            boolean z = (adRequestInfoParcel == null || adRequestInfoParcel.zzcax == 0) ? false : true;
            String optString5 = jSONObject.optString("ad_json", null);
            if (optString5 == null) {
                optString5 = jSONObject.optString("ad_html", null);
            }
            if (optString5 == null) {
                optString5 = jSONObject.optString("body", null);
            }
            long j2 = -1;
            String optString6 = jSONObject.optString("debug_dialog", null);
            long j3 = jSONObject.has("interstitial_timeout") ? (long) (jSONObject.getDouble("interstitial_timeout") * 1000.0d) : -1;
            String optString7 = jSONObject.optString("orientation", null);
            int i = -1;
            if ("portrait".equals(optString7)) {
                i = zzu.zzfs().zztk();
            } else if ("landscape".equals(optString7)) {
                i = zzu.zzfs().zztj();
            }
            AdResponseParcel adResponseParcel = null;
            if (!TextUtils.isEmpty(optString5) || TextUtils.isEmpty(optString2)) {
                str2 = optString5;
            } else {
                adResponseParcel = zzip.zza(adRequestInfoParcel, context, adRequestInfoParcel.zzaow.zzcs, optString2, null, null, null, null);
                optString = adResponseParcel.zzbto;
                str2 = adResponseParcel.body;
                j2 = adResponseParcel.zzccc;
            }
            if (str2 == null) {
                return new AdResponseParcel(0);
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("click_urls");
            List<String> list = adResponseParcel == null ? null : adResponseParcel.zzbnm;
            if (optJSONArray != null) {
                list = zza(optJSONArray, list);
            }
            JSONArray optJSONArray2 = jSONObject.optJSONArray("impression_urls");
            List<String> list2 = adResponseParcel == null ? null : adResponseParcel.zzbnn;
            if (optJSONArray2 != null) {
                list2 = zza(optJSONArray2, list2);
            }
            JSONArray optJSONArray3 = jSONObject.optJSONArray("manual_impression_urls");
            List<String> list3 = adResponseParcel == null ? null : adResponseParcel.zzcca;
            if (optJSONArray3 != null) {
                list3 = zza(optJSONArray3, list3);
            }
            if (adResponseParcel != null) {
                if (adResponseParcel.orientation != -1) {
                    i = adResponseParcel.orientation;
                }
                if (adResponseParcel.zzcbx > 0) {
                    j = adResponseParcel.zzcbx;
                    String optString8 = jSONObject.optString("active_view");
                    String str3 = null;
                    optBoolean = jSONObject.optBoolean("ad_is_javascript", false);
                    if (optBoolean) {
                        str3 = jSONObject.optString("ad_passback_url", null);
                    }
                    return new AdResponseParcel(adRequestInfoParcel, optString, str2, list, list2, j, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), list3, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j2, optString6, optBoolean, str3, optString8, jSONObject.optBoolean("custom_render_allowed", false), z, adRequestInfoParcel.zzcaz, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), "height".equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), RewardItemParcel.zza(jSONObject.optJSONArray("rewards")), zza(jSONObject.optJSONArray("video_start_urls"), null), zza(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), AutoClickProtectionConfigurationParcel.zzh(jSONObject.optJSONObject("auto_protection_configuration")), adRequestInfoParcel.zzcbq, jSONObject.optString("set_cookie", ""), zza(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optString("safe_browsing"), jSONObject.optBoolean("render_in_browser", adRequestInfoParcel.zzbnq), optString4);
                }
            }
            j = j3;
            String optString82 = jSONObject.optString("active_view");
            String str32 = null;
            optBoolean = jSONObject.optBoolean("ad_is_javascript", false);
            if (optBoolean) {
            }
            return new AdResponseParcel(adRequestInfoParcel, optString, str2, list, list2, j, jSONObject.optBoolean("mediation", false), jSONObject.optLong("mediation_config_cache_time_milliseconds", -1), list3, jSONObject.optLong("refresh_interval_milliseconds", -1), i, optString3, j2, optString6, optBoolean, str32, optString82, jSONObject.optBoolean("custom_render_allowed", false), z, adRequestInfoParcel.zzcaz, jSONObject.optBoolean("content_url_opted_out", true), jSONObject.optBoolean("prefetch", false), jSONObject.optString("gws_query_id", ""), "height".equals(jSONObject.optString("fluid", "")), jSONObject.optBoolean("native_express", false), RewardItemParcel.zza(jSONObject.optJSONArray("rewards")), zza(jSONObject.optJSONArray("video_start_urls"), null), zza(jSONObject.optJSONArray("video_complete_urls"), null), jSONObject.optBoolean("use_displayed_impression", false), AutoClickProtectionConfigurationParcel.zzh(jSONObject.optJSONObject("auto_protection_configuration")), adRequestInfoParcel.zzcbq, jSONObject.optString("set_cookie", ""), zza(jSONObject.optJSONArray("remote_ping_urls"), null), jSONObject.optString("safe_browsing"), jSONObject.optBoolean("render_in_browser", adRequestInfoParcel.zzbnq), optString4);
        } catch (JSONException e) {
            String str4 = "Could not parse the inline ad response: ";
            String valueOf = String.valueOf(e.getMessage());
            zzkd.zzcx(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
            return new AdResponseParcel(0);
        }
    }

    @Nullable
    private static List<String> zza(@Nullable JSONArray jSONArray, @Nullable List<String> list) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        if (list == null) {
            list = new LinkedList<>();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            list.add(jSONArray.getString(i));
        }
        return list;
    }

    @Nullable
    public static JSONObject zza(Context context, AdRequestInfoParcel adRequestInfoParcel, zziv zziv, zza zza, Location location, zzcv zzcv, String str, List<String> list, Bundle bundle, String str2) {
        AdSizeParcel[] adSizeParcelArr;
        try {
            HashMap hashMap = new HashMap();
            if (list.size() > 0) {
                hashMap.put("eid", TextUtils.join(",", list));
            }
            if (adRequestInfoParcel.zzcaq != null) {
                hashMap.put("ad_pos", adRequestInfoParcel.zzcaq);
            }
            zza(hashMap, adRequestInfoParcel.zzcar);
            if (adRequestInfoParcel.zzapa.zzaut != null) {
                boolean z = false;
                boolean z2 = false;
                for (AdSizeParcel adSizeParcel : adRequestInfoParcel.zzapa.zzaut) {
                    if (!adSizeParcel.zzauv && !z2) {
                        hashMap.put("format", adSizeParcel.zzaur);
                        z2 = true;
                    }
                    if (adSizeParcel.zzauv && !z) {
                        hashMap.put("fluid", "height");
                        z = true;
                    }
                    if (z2 && z) {
                        break;
                    }
                }
            } else {
                hashMap.put("format", adRequestInfoParcel.zzapa.zzaur);
                if (adRequestInfoParcel.zzapa.zzauv) {
                    hashMap.put("fluid", "height");
                }
            }
            if (adRequestInfoParcel.zzapa.width == -1) {
                hashMap.put("smart_w", "full");
            }
            if (adRequestInfoParcel.zzapa.height == -2) {
                hashMap.put("smart_h", "auto");
            }
            if (adRequestInfoParcel.zzapa.zzaut != null) {
                StringBuilder sb = new StringBuilder();
                AdSizeParcel[] adSizeParcelArr2 = adRequestInfoParcel.zzapa.zzaut;
                int length = adSizeParcelArr2.length;
                boolean z3 = false;
                for (int i = 0; i < length; i++) {
                    AdSizeParcel adSizeParcel2 = adSizeParcelArr2[i];
                    if (adSizeParcel2.zzauv) {
                        z3 = true;
                    } else {
                        if (sb.length() != 0) {
                            sb.append("|");
                        }
                        sb.append(adSizeParcel2.width == -1 ? (int) (((float) adSizeParcel2.widthPixels) / zziv.zzcbd) : adSizeParcel2.width);
                        sb.append("x");
                        sb.append(adSizeParcel2.height == -2 ? (int) (((float) adSizeParcel2.heightPixels) / zziv.zzcbd) : adSizeParcel2.height);
                    }
                }
                if (z3) {
                    if (sb.length() != 0) {
                        sb.insert(0, "|");
                    }
                    sb.insert(0, "320x50");
                }
                hashMap.put("sz", sb);
            }
            if (adRequestInfoParcel.zzcax != 0) {
                hashMap.put("native_version", Integer.valueOf(adRequestInfoParcel.zzcax));
                if (!adRequestInfoParcel.zzapa.zzauw) {
                    hashMap.put("native_templates", adRequestInfoParcel.zzaps);
                    hashMap.put("native_image_orientation", zzc(adRequestInfoParcel.zzapo));
                    if (!adRequestInfoParcel.zzcbi.isEmpty()) {
                        hashMap.put("native_custom_templates", adRequestInfoParcel.zzcbi);
                    }
                }
            }
            hashMap.put("slotname", adRequestInfoParcel.zzaou);
            hashMap.put("pn", adRequestInfoParcel.applicationInfo.packageName);
            if (adRequestInfoParcel.zzcas != null) {
                hashMap.put("vc", Integer.valueOf(adRequestInfoParcel.zzcas.versionCode));
            }
            hashMap.put("ms", str);
            hashMap.put("seq_num", adRequestInfoParcel.zzcau);
            hashMap.put("session_id", adRequestInfoParcel.zzcav);
            hashMap.put("js", adRequestInfoParcel.zzaow.zzcs);
            zza(hashMap, zziv, zza, adRequestInfoParcel.zzcbv);
            zza(hashMap, str2);
            hashMap.put("platform", Build.MANUFACTURER);
            hashMap.put("submodel", Build.MODEL);
            if (location != null) {
                zza(hashMap, location);
            } else if (adRequestInfoParcel.zzcar.versionCode >= 2 && adRequestInfoParcel.zzcar.zzatu != null) {
                zza(hashMap, adRequestInfoParcel.zzcar.zzatu);
            }
            if (adRequestInfoParcel.versionCode >= 2) {
                hashMap.put("quality_signals", adRequestInfoParcel.zzcaw);
            }
            if (adRequestInfoParcel.versionCode >= 4 && adRequestInfoParcel.zzcaz) {
                hashMap.put("forceHttps", Boolean.valueOf(adRequestInfoParcel.zzcaz));
            }
            if (bundle != null) {
                hashMap.put("content_info", bundle);
            }
            if (adRequestInfoParcel.versionCode >= 5) {
                hashMap.put("u_sd", Float.valueOf(adRequestInfoParcel.zzcbd));
                hashMap.put("sh", Integer.valueOf(adRequestInfoParcel.zzcbc));
                hashMap.put("sw", Integer.valueOf(adRequestInfoParcel.zzcbb));
            } else {
                hashMap.put("u_sd", Float.valueOf(zziv.zzcbd));
                hashMap.put("sh", Integer.valueOf(zziv.zzcbc));
                hashMap.put("sw", Integer.valueOf(zziv.zzcbb));
            }
            if (adRequestInfoParcel.versionCode >= 6) {
                if (!TextUtils.isEmpty(adRequestInfoParcel.zzcbe)) {
                    try {
                        hashMap.put("view_hierarchy", new JSONObject(adRequestInfoParcel.zzcbe));
                    } catch (JSONException e) {
                        zzkd.zzd("Problem serializing view hierarchy to JSON", e);
                    }
                }
                hashMap.put("correlation_id", Long.valueOf(adRequestInfoParcel.zzcbf));
            }
            if (adRequestInfoParcel.versionCode >= 7) {
                hashMap.put("request_id", adRequestInfoParcel.zzcbg);
            }
            if (adRequestInfoParcel.versionCode >= 11 && adRequestInfoParcel.zzcbk != null) {
                hashMap.put("capability", adRequestInfoParcel.zzcbk.toBundle());
            }
            if (adRequestInfoParcel.versionCode >= 12 && !TextUtils.isEmpty(adRequestInfoParcel.zzcbl)) {
                hashMap.put("anchor", adRequestInfoParcel.zzcbl);
            }
            if (adRequestInfoParcel.versionCode >= 13) {
                hashMap.put("android_app_volume", Float.valueOf(adRequestInfoParcel.zzcbm));
            }
            if (adRequestInfoParcel.versionCode >= 18) {
                hashMap.put("android_app_muted", Boolean.valueOf(adRequestInfoParcel.zzcbs));
            }
            if (adRequestInfoParcel.versionCode >= 14 && adRequestInfoParcel.zzcbn > 0) {
                hashMap.put("target_api", Integer.valueOf(adRequestInfoParcel.zzcbn));
            }
            if (adRequestInfoParcel.versionCode >= 15) {
                hashMap.put("scroll_index", Integer.valueOf(adRequestInfoParcel.zzcbo == -1 ? -1 : adRequestInfoParcel.zzcbo));
            }
            if (adRequestInfoParcel.versionCode >= 16) {
                hashMap.put("_activity_context", Boolean.valueOf(adRequestInfoParcel.zzcbp));
            }
            if (adRequestInfoParcel.versionCode >= 18) {
                if (!TextUtils.isEmpty(adRequestInfoParcel.zzcbt)) {
                    try {
                        hashMap.put("app_settings", new JSONObject(adRequestInfoParcel.zzcbt));
                    } catch (JSONException e2) {
                        zzkd.zzd("Problem creating json from app settings", e2);
                    }
                }
                hashMap.put("render_in_browser", Boolean.valueOf(adRequestInfoParcel.zzbnq));
            }
            if (adRequestInfoParcel.versionCode >= 18) {
                hashMap.put("android_num_video_cache_tasks", Integer.valueOf(adRequestInfoParcel.zzcbu));
            }
            if (zzkd.zzaz(2)) {
                String str3 = "Ad Request JSON: ";
                String valueOf = String.valueOf(zzu.zzfq().zzam((Map<String, ?>) hashMap).toString(2));
                zzkd.v(valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            }
            return zzu.zzfq().zzam((Map<String, ?>) hashMap);
        } catch (JSONException e3) {
            String str4 = "Problem serializing ad request to JSON: ";
            String valueOf2 = String.valueOf(e3.getMessage());
            zzkd.zzcx(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
            return null;
        }
    }

    private static void zza(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put("lat", valueOf3);
        hashMap2.put("long", valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }

    private static void zza(HashMap<String, Object> hashMap, AdRequestParcel adRequestParcel) {
        String zzsy = zzkb.zzsy();
        if (zzsy != null) {
            hashMap.put("abf", zzsy);
        }
        if (adRequestParcel.zzatm != -1) {
            hashMap.put("cust_age", zzcel.format(new Date(adRequestParcel.zzatm)));
        }
        if (adRequestParcel.extras != null) {
            hashMap.put("extras", adRequestParcel.extras);
        }
        if (adRequestParcel.zzatn != -1) {
            hashMap.put("cust_gender", Integer.valueOf(adRequestParcel.zzatn));
        }
        if (adRequestParcel.zzato != null) {
            hashMap.put("kw", adRequestParcel.zzato);
        }
        if (adRequestParcel.zzatq != -1) {
            hashMap.put("tag_for_child_directed_treatment", Integer.valueOf(adRequestParcel.zzatq));
        }
        if (adRequestParcel.zzatp) {
            hashMap.put("adtest", "on");
        }
        if (adRequestParcel.versionCode >= 2) {
            if (adRequestParcel.zzatr) {
                hashMap.put("d_imp_hdr", Integer.valueOf(1));
            }
            if (!TextUtils.isEmpty(adRequestParcel.zzats)) {
                hashMap.put("ppid", adRequestParcel.zzats);
            }
            if (adRequestParcel.zzatt != null) {
                zza(hashMap, adRequestParcel.zzatt);
            }
        }
        if (adRequestParcel.versionCode >= 3 && adRequestParcel.zzatv != null) {
            hashMap.put("url", adRequestParcel.zzatv);
        }
        if (adRequestParcel.versionCode >= 5) {
            if (adRequestParcel.zzatx != null) {
                hashMap.put("custom_targeting", adRequestParcel.zzatx);
            }
            if (adRequestParcel.zzaty != null) {
                hashMap.put("category_exclusions", adRequestParcel.zzaty);
            }
            if (adRequestParcel.zzatz != null) {
                hashMap.put("request_agent", adRequestParcel.zzatz);
            }
        }
        if (adRequestParcel.versionCode >= 6 && adRequestParcel.zzaua != null) {
            hashMap.put("request_pkg", adRequestParcel.zzaua);
        }
        if (adRequestParcel.versionCode >= 7) {
            hashMap.put("is_designed_for_families", Boolean.valueOf(adRequestParcel.zzaub));
        }
    }

    private static void zza(HashMap<String, Object> hashMap, SearchAdRequestParcel searchAdRequestParcel) {
        String str;
        String str2 = null;
        if (Color.alpha(searchAdRequestParcel.zzawz) != 0) {
            hashMap.put("acolor", zzau(searchAdRequestParcel.zzawz));
        }
        if (Color.alpha(searchAdRequestParcel.backgroundColor) != 0) {
            hashMap.put("bgcolor", zzau(searchAdRequestParcel.backgroundColor));
        }
        if (!(Color.alpha(searchAdRequestParcel.zzaxa) == 0 || Color.alpha(searchAdRequestParcel.zzaxb) == 0)) {
            hashMap.put("gradientto", zzau(searchAdRequestParcel.zzaxa));
            hashMap.put("gradientfrom", zzau(searchAdRequestParcel.zzaxb));
        }
        if (Color.alpha(searchAdRequestParcel.zzaxc) != 0) {
            hashMap.put("bcolor", zzau(searchAdRequestParcel.zzaxc));
        }
        hashMap.put("bthick", Integer.toString(searchAdRequestParcel.zzaxd));
        switch (searchAdRequestParcel.zzaxe) {
            case 0:
                str = "none";
                break;
            case 1:
                str = "dashed";
                break;
            case 2:
                str = "dotted";
                break;
            case 3:
                str = "solid";
                break;
            default:
                str = null;
                break;
        }
        if (str != null) {
            hashMap.put("btype", str);
        }
        switch (searchAdRequestParcel.zzaxf) {
            case 0:
                str2 = "light";
                break;
            case 1:
                str2 = "medium";
                break;
            case 2:
                str2 = "dark";
                break;
        }
        if (str2 != null) {
            hashMap.put("callbuttoncolor", str2);
        }
        if (searchAdRequestParcel.zzaxg != null) {
            hashMap.put("channel", searchAdRequestParcel.zzaxg);
        }
        if (Color.alpha(searchAdRequestParcel.zzaxh) != 0) {
            hashMap.put("dcolor", zzau(searchAdRequestParcel.zzaxh));
        }
        if (searchAdRequestParcel.zzaxi != null) {
            hashMap.put("font", searchAdRequestParcel.zzaxi);
        }
        if (Color.alpha(searchAdRequestParcel.zzaxj) != 0) {
            hashMap.put("hcolor", zzau(searchAdRequestParcel.zzaxj));
        }
        hashMap.put("headersize", Integer.toString(searchAdRequestParcel.zzaxk));
        if (searchAdRequestParcel.zzaxl != null) {
            hashMap.put("q", searchAdRequestParcel.zzaxl);
        }
    }

    private static void zza(HashMap<String, Object> hashMap, zziv zziv, zza zza, Bundle bundle) {
        hashMap.put("am", Integer.valueOf(zziv.zzcgd));
        hashMap.put("cog", zzab(zziv.zzcge));
        hashMap.put("coh", zzab(zziv.zzcgf));
        if (!TextUtils.isEmpty(zziv.zzcgg)) {
            hashMap.put("carrier", zziv.zzcgg);
        }
        hashMap.put("gl", zziv.zzcgh);
        if (zziv.zzcgi) {
            hashMap.put("simulator", Integer.valueOf(1));
        }
        if (zziv.zzcgj) {
            hashMap.put("is_sidewinder", Integer.valueOf(1));
        }
        hashMap.put("ma", zzab(zziv.zzcgk));
        hashMap.put("sp", zzab(zziv.zzcgl));
        hashMap.put("hl", zziv.zzcgm);
        if (!TextUtils.isEmpty(zziv.zzcgn)) {
            hashMap.put("mv", zziv.zzcgn);
        }
        hashMap.put("muv", Integer.valueOf(zziv.zzcgo));
        if (zziv.zzcgp != -2) {
            hashMap.put("cnt", Integer.valueOf(zziv.zzcgp));
        }
        hashMap.put("gnt", Integer.valueOf(zziv.zzcgq));
        hashMap.put("pt", Integer.valueOf(zziv.zzcgr));
        hashMap.put("rm", Integer.valueOf(zziv.zzcgs));
        hashMap.put("riv", Integer.valueOf(zziv.zzcgt));
        Bundle bundle2 = new Bundle();
        bundle2.putString("build", zziv.zzcgy);
        Bundle bundle3 = new Bundle();
        bundle3.putBoolean("is_charging", zziv.zzcgv);
        bundle3.putDouble("battery_level", zziv.zzcgu);
        bundle2.putBundle("battery", bundle3);
        Bundle bundle4 = new Bundle();
        bundle4.putInt("active_network_state", zziv.zzcgx);
        bundle4.putBoolean("active_network_metered", zziv.zzcgw);
        if (zza != null) {
            Bundle bundle5 = new Bundle();
            bundle5.putInt("predicted_latency_micros", 0);
            bundle5.putLong("predicted_down_throughput_bps", 0);
            bundle5.putLong("predicted_up_throughput_bps", 0);
            bundle4.putBundle("predictions", bundle5);
        }
        bundle2.putBundle("network", bundle4);
        Bundle bundle6 = new Bundle();
        bundle6.putBoolean("is_browser_custom_tabs_capable", zziv.zzcgz);
        bundle2.putBundle("browser", bundle6);
        if (bundle != null) {
            bundle2.putBundle("android_mem_info", zzf(bundle));
        }
        hashMap.put("device", bundle2);
    }

    private static void zza(HashMap<String, Object> hashMap, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("doritos", str);
        hashMap.put("pii", bundle);
    }

    private static Integer zzab(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }

    private static String zzau(int i) {
        return String.format(Locale.US, "#%06x", new Object[]{Integer.valueOf(16777215 & i)});
    }

    private static String zzc(NativeAdOptionsParcel nativeAdOptionsParcel) {
        switch (nativeAdOptionsParcel != null ? nativeAdOptionsParcel.zzbgq : 0) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return "any";
        }
    }

    public static JSONObject zzc(AdResponseParcel adResponseParcel) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (adResponseParcel.zzbto != null) {
            jSONObject.put("ad_base_url", adResponseParcel.zzbto);
        }
        if (adResponseParcel.zzccb != null) {
            jSONObject.put("ad_size", adResponseParcel.zzccb);
        }
        jSONObject.put("native", adResponseParcel.zzauu);
        if (adResponseParcel.zzauu) {
            jSONObject.put("ad_json", adResponseParcel.body);
        } else {
            jSONObject.put("ad_html", adResponseParcel.body);
        }
        if (adResponseParcel.zzccd != null) {
            jSONObject.put("debug_dialog", adResponseParcel.zzccd);
        }
        if (adResponseParcel.zzcbx != -1) {
            jSONObject.put("interstitial_timeout", ((double) adResponseParcel.zzcbx) / 1000.0d);
        }
        if (adResponseParcel.orientation == zzu.zzfs().zztk()) {
            jSONObject.put("orientation", "portrait");
        } else if (adResponseParcel.orientation == zzu.zzfs().zztj()) {
            jSONObject.put("orientation", "landscape");
        }
        if (adResponseParcel.zzbnm != null) {
            jSONObject.put("click_urls", zzk(adResponseParcel.zzbnm));
        }
        if (adResponseParcel.zzbnn != null) {
            jSONObject.put("impression_urls", zzk(adResponseParcel.zzbnn));
        }
        if (adResponseParcel.zzcca != null) {
            jSONObject.put("manual_impression_urls", zzk(adResponseParcel.zzcca));
        }
        if (adResponseParcel.zzccg != null) {
            jSONObject.put("active_view", adResponseParcel.zzccg);
        }
        jSONObject.put("ad_is_javascript", adResponseParcel.zzcce);
        if (adResponseParcel.zzccf != null) {
            jSONObject.put("ad_passback_url", adResponseParcel.zzccf);
        }
        jSONObject.put("mediation", adResponseParcel.zzcby);
        jSONObject.put("custom_render_allowed", adResponseParcel.zzcch);
        jSONObject.put("content_url_opted_out", adResponseParcel.zzcci);
        jSONObject.put("prefetch", adResponseParcel.zzccj);
        if (adResponseParcel.zzbns != -1) {
            jSONObject.put("refresh_interval_milliseconds", adResponseParcel.zzbns);
        }
        if (adResponseParcel.zzcbz != -1) {
            jSONObject.put("mediation_config_cache_time_milliseconds", adResponseParcel.zzcbz);
        }
        if (!TextUtils.isEmpty(adResponseParcel.zzccm)) {
            jSONObject.put("gws_query_id", adResponseParcel.zzccm);
        }
        jSONObject.put("fluid", adResponseParcel.zzauv ? "height" : "");
        jSONObject.put("native_express", adResponseParcel.zzauw);
        if (adResponseParcel.zzcco != null) {
            jSONObject.put("video_start_urls", zzk(adResponseParcel.zzcco));
        }
        if (adResponseParcel.zzccp != null) {
            jSONObject.put("video_complete_urls", zzk(adResponseParcel.zzccp));
        }
        if (adResponseParcel.zzccn != null) {
            jSONObject.put("rewards", adResponseParcel.zzccn.zzrw());
        }
        jSONObject.put("use_displayed_impression", adResponseParcel.zzccq);
        jSONObject.put("auto_protection_configuration", adResponseParcel.zzccr);
        jSONObject.put("render_in_browser", adResponseParcel.zzbnq);
        return jSONObject;
    }

    private static Bundle zzf(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        bundle2.putString("runtime_free", Long.toString(bundle.getLong("runtime_free_memory", -1)));
        bundle2.putString("runtime_max", Long.toString(bundle.getLong("runtime_max_memory", -1)));
        bundle2.putString("runtime_total", Long.toString(bundle.getLong("runtime_total_memory", -1)));
        MemoryInfo memoryInfo = (MemoryInfo) bundle.getParcelable("debug_memory_info");
        if (memoryInfo != null) {
            bundle2.putString("debug_info_dalvik_private_dirty", Integer.toString(memoryInfo.dalvikPrivateDirty));
            bundle2.putString("debug_info_dalvik_pss", Integer.toString(memoryInfo.dalvikPss));
            bundle2.putString("debug_info_dalvik_shared_dirty", Integer.toString(memoryInfo.dalvikSharedDirty));
            bundle2.putString("debug_info_native_private_dirty", Integer.toString(memoryInfo.nativePrivateDirty));
            bundle2.putString("debug_info_native_pss", Integer.toString(memoryInfo.nativePss));
            bundle2.putString("debug_info_native_shared_dirty", Integer.toString(memoryInfo.nativeSharedDirty));
            bundle2.putString("debug_info_other_private_dirty", Integer.toString(memoryInfo.otherPrivateDirty));
            bundle2.putString("debug_info_other_pss", Integer.toString(memoryInfo.otherPss));
            bundle2.putString("debug_info_other_shared_dirty", Integer.toString(memoryInfo.otherSharedDirty));
        }
        return bundle2;
    }

    @Nullable
    static JSONArray zzk(List<String> list) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return jSONArray;
    }
}
