package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.clearcut.LogEventParcelable;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.internal.zzk;
import com.google.android.gms.internal.zzpf.zza;

public class zzpc extends zzk<zzpf> {
    public zzpc(Context context, Looper looper, zzg zzg, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 40, zzg, connectionCallbacks, onConnectionFailedListener);
    }

    public void zza(zzpe zzpe, LogEventParcelable logEventParcelable) throws RemoteException {
        ((zzpf) zzasa()).zza(zzpe, logEventParcelable);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzdk */
    public zzpf zzbb(IBinder iBinder) {
        return zza.zzdm(iBinder);
    }

    /* access modifiers changed from: protected */
    public String zzqz() {
        return "com.google.android.gms.clearcut.service.START";
    }

    /* access modifiers changed from: protected */
    public String zzra() {
        return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
    }
}
