package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzeq implements zzep {
    private final Context mContext;
    private final VersionInfoParcel zzalo;

    @zzin
    static class zza {
        private final String mValue;
        private final String zzaxp;

        public zza(String str, String str2) {
            this.zzaxp = str;
            this.mValue = str2;
        }

        public String getKey() {
            return this.zzaxp;
        }

        public String getValue() {
            return this.mValue;
        }
    }

    @zzin
    static class zzb {
        private final String zzbii;
        private final URL zzbij;
        private final ArrayList<zza> zzbik;
        private final String zzbil;

        public zzb(String str, URL url, ArrayList<zza> arrayList, String str2) {
            this.zzbii = str;
            this.zzbij = url;
            if (arrayList == null) {
                this.zzbik = new ArrayList<>();
            } else {
                this.zzbik = arrayList;
            }
            this.zzbil = str2;
        }

        public String zzle() {
            return this.zzbii;
        }

        public URL zzlf() {
            return this.zzbij;
        }

        public ArrayList<zza> zzlg() {
            return this.zzbik;
        }

        public String zzlh() {
            return this.zzbil;
        }
    }

    @zzin
    class zzc {
        private final zzd zzbim;
        private final boolean zzbin;
        private final String zzbio;

        public zzc(boolean z, zzd zzd, String str) {
            this.zzbin = z;
            this.zzbim = zzd;
            this.zzbio = str;
        }

        public String getReason() {
            return this.zzbio;
        }

        public boolean isSuccess() {
            return this.zzbin;
        }

        public zzd zzli() {
            return this.zzbim;
        }
    }

    @zzin
    static class zzd {
        private final String zzbfi;
        private final String zzbii;
        private final int zzbip;
        private final List<zza> zzbiq;

        public zzd(String str, int i, List<zza> list, String str2) {
            this.zzbii = str;
            this.zzbip = i;
            if (list == null) {
                this.zzbiq = new ArrayList();
            } else {
                this.zzbiq = list;
            }
            this.zzbfi = str2;
        }

        public String getBody() {
            return this.zzbfi;
        }

        public int getResponseCode() {
            return this.zzbip;
        }

        public String zzle() {
            return this.zzbii;
        }

        public Iterable<zza> zzlj() {
            return this.zzbiq;
        }
    }

    public zzeq(Context context, VersionInfoParcel versionInfoParcel) {
        this.mContext = context;
        this.zzalo = versionInfoParcel;
    }

    /* access modifiers changed from: protected */
    public zzc zza(zzb zzb2) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) zzb2.zzlf().openConnection();
            zzu.zzfq().zza(this.mContext, this.zzalo.zzcs, false, httpURLConnection);
            Iterator it = zzb2.zzlg().iterator();
            while (it.hasNext()) {
                zza zza2 = (zza) it.next();
                httpURLConnection.addRequestProperty(zza2.getKey(), zza2.getValue());
            }
            if (!TextUtils.isEmpty(zzb2.zzlh())) {
                httpURLConnection.setDoOutput(true);
                byte[] bytes = zzb2.zzlh().getBytes();
                httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                bufferedOutputStream.write(bytes);
                bufferedOutputStream.close();
            }
            ArrayList arrayList = new ArrayList();
            if (httpURLConnection.getHeaderFields() != null) {
                for (Entry entry : httpURLConnection.getHeaderFields().entrySet()) {
                    for (String zza3 : (List) entry.getValue()) {
                        arrayList.add(new zza((String) entry.getKey(), zza3));
                    }
                }
            }
            return new zzc(true, new zzd(zzb2.zzle(), httpURLConnection.getResponseCode(), arrayList, zzu.zzfq().zza(new InputStreamReader(httpURLConnection.getInputStream()))), null);
        } catch (Exception e) {
            return new zzc(false, null, e.toString());
        }
    }

    /* access modifiers changed from: protected */
    public JSONObject zza(zzd zzd2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("http_request_id", zzd2.zzle());
            if (zzd2.getBody() != null) {
                jSONObject.put("body", zzd2.getBody());
            }
            JSONArray jSONArray = new JSONArray();
            for (zza zza2 : zzd2.zzlj()) {
                jSONArray.put(new JSONObject().put("key", zza2.getKey()).put(Param.VALUE, zza2.getValue()));
            }
            jSONObject.put("headers", jSONArray);
            jSONObject.put("response_code", zzd2.getResponseCode());
        } catch (JSONException e) {
            zzkd.zzb("Error constructing JSON for http response.", e);
        }
        return jSONObject;
    }

    public void zza(final zzlh zzlh, final Map<String, String> map) {
        zzkg.zza((Runnable) new Runnable() {
            public void run() {
                zzkd.zzcv("Received Http request.");
                final JSONObject zzav = zzeq.this.zzav((String) map.get("http_request"));
                if (zzav == null) {
                    zzkd.e("Response should not be null.");
                } else {
                    zzkh.zzclc.post(new Runnable() {
                        public void run() {
                            zzlh.zzb("fetchHttpRequestCompleted", zzav);
                            zzkd.zzcv("Dispatched http response.");
                        }
                    });
                }
            }
        });
    }

    public JSONObject zzav(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject jSONObject2 = new JSONObject();
            String str2 = "";
            try {
                str2 = jSONObject.optString("http_request_id");
                zzc zza2 = zza(zzc(jSONObject));
                if (zza2.isSuccess()) {
                    jSONObject2.put("response", zza(zza2.zzli()));
                    jSONObject2.put("success", true);
                    return jSONObject2;
                }
                jSONObject2.put("response", new JSONObject().put("http_request_id", str2));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", zza2.getReason());
                return jSONObject2;
            } catch (Exception e) {
                try {
                    jSONObject2.put("response", new JSONObject().put("http_request_id", str2));
                    jSONObject2.put("success", false);
                    jSONObject2.put("reason", e.toString());
                    return jSONObject2;
                } catch (JSONException e2) {
                    return jSONObject2;
                }
            }
        } catch (JSONException e3) {
            zzkd.e("The request is not a valid JSON.");
            try {
                return new JSONObject().put("success", false);
            } catch (JSONException e4) {
                return new JSONObject();
            }
        }
    }

    /* access modifiers changed from: protected */
    public zzb zzc(JSONObject jSONObject) {
        URL url;
        String optString = jSONObject.optString("http_request_id");
        String optString2 = jSONObject.optString("url");
        String optString3 = jSONObject.optString("post_body", null);
        try {
            url = new URL(optString2);
        } catch (MalformedURLException e) {
            zzkd.zzb("Error constructing http request.", e);
            url = null;
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(new zza(optJSONObject.optString("key"), optJSONObject.optString(Param.VALUE)));
            }
        }
        return new zzb(optString, url, arrayList, optString3);
    }
}
