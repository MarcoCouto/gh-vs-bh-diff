package com.google.android.gms.internal;

import com.google.android.gms.internal.zzae.zza;
import java.util.concurrent.Callable;

public class zzbi implements Callable {
    private final zzax zzaey;
    private final zza zzaha;

    public zzbi(zzax zzax, zza zza) {
        this.zzaey = zzax;
        this.zzaha = zza;
    }

    /* renamed from: zzcx */
    public Void call() throws Exception {
        if (this.zzaey.zzcm() != null) {
            this.zzaey.zzcm().get();
        }
        zza zzcl = this.zzaey.zzcl();
        if (zzcl != null) {
            try {
                synchronized (this.zzaha) {
                    zzapv.zza(this.zzaha, zzapv.zzf(zzcl));
                }
            } catch (zzapu e) {
            }
        }
        return null;
    }
}
