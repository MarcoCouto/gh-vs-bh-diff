package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

public final class zzaoe implements zzani {
    private final zzanp bdR;
    /* access modifiers changed from: private */
    public final boolean bfG;

    private final class zza<K, V> extends zzanh<Map<K, V>> {
        private final zzanh<K> bfH;
        private final zzanh<V> bfI;
        private final zzanu<? extends Map<K, V>> bfy;

        public zza(zzamp zzamp, Type type, zzanh<K> zzanh, Type type2, zzanh<V> zzanh2, zzanu<? extends Map<K, V>> zzanu) {
            this.bfH = new zzaoj(zzamp, zzanh, type);
            this.bfI = new zzaoj(zzamp, zzanh2, type2);
            this.bfy = zzanu;
        }

        private String zze(zzamv zzamv) {
            if (zzamv.zzczi()) {
                zzanb zzczm = zzamv.zzczm();
                if (zzczm.zzczp()) {
                    return String.valueOf(zzczm.zzcze());
                }
                if (zzczm.zzczo()) {
                    return Boolean.toString(zzczm.getAsBoolean());
                }
                if (zzczm.zzczq()) {
                    return zzczm.zzczf();
                }
                throw new AssertionError();
            } else if (zzamv.zzczj()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }

        public void zza(zzaoo zzaoo, Map<K, V> map) throws IOException {
            int i = 0;
            if (map == null) {
                zzaoo.l();
            } else if (!zzaoe.this.bfG) {
                zzaoo.j();
                for (Entry entry : map.entrySet()) {
                    zzaoo.zztr(String.valueOf(entry.getKey()));
                    this.bfI.zza(zzaoo, entry.getValue());
                }
                zzaoo.k();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                boolean z = false;
                for (Entry entry2 : map.entrySet()) {
                    zzamv zzcj = this.bfH.zzcj(entry2.getKey());
                    arrayList.add(zzcj);
                    arrayList2.add(entry2.getValue());
                    z = (zzcj.zzczg() || zzcj.zzczh()) | z;
                }
                if (z) {
                    zzaoo.h();
                    while (i < arrayList.size()) {
                        zzaoo.h();
                        zzanw.zzb((zzamv) arrayList.get(i), zzaoo);
                        this.bfI.zza(zzaoo, arrayList2.get(i));
                        zzaoo.i();
                        i++;
                    }
                    zzaoo.i();
                    return;
                }
                zzaoo.j();
                while (i < arrayList.size()) {
                    zzaoo.zztr(zze((zzamv) arrayList.get(i)));
                    this.bfI.zza(zzaoo, arrayList2.get(i));
                    i++;
                }
                zzaoo.k();
            }
        }

        /* renamed from: zzl */
        public Map<K, V> zzb(zzaom zzaom) throws IOException {
            zzaon b = zzaom.b();
            if (b == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            Map<K, V> map = (Map) this.bfy.zzczu();
            if (b == zzaon.BEGIN_ARRAY) {
                zzaom.beginArray();
                while (zzaom.hasNext()) {
                    zzaom.beginArray();
                    Object zzb = this.bfH.zzb(zzaom);
                    if (map.put(zzb, this.bfI.zzb(zzaom)) != null) {
                        String valueOf = String.valueOf(zzb);
                        throw new zzane(new StringBuilder(String.valueOf(valueOf).length() + 15).append("duplicate key: ").append(valueOf).toString());
                    }
                    zzaom.endArray();
                }
                zzaom.endArray();
                return map;
            }
            zzaom.beginObject();
            while (zzaom.hasNext()) {
                zzanr.beV.zzi(zzaom);
                Object zzb2 = this.bfH.zzb(zzaom);
                if (map.put(zzb2, this.bfI.zzb(zzaom)) != null) {
                    String valueOf2 = String.valueOf(zzb2);
                    throw new zzane(new StringBuilder(String.valueOf(valueOf2).length() + 15).append("duplicate key: ").append(valueOf2).toString());
                }
            }
            zzaom.endObject();
            return map;
        }
    }

    public zzaoe(zzanp zzanp, boolean z) {
        this.bdR = zzanp;
        this.bfG = z;
    }

    private zzanh<?> zza(zzamp zzamp, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? zzaok.bgc : zzamp.zza(zzaol.zzl(type));
    }

    public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
        Type n = zzaol.n();
        if (!Map.class.isAssignableFrom(zzaol.m())) {
            return null;
        }
        Type[] zzb = zzano.zzb(n, zzano.zzf(n));
        zzanh zza2 = zza(zzamp, zzb[0]);
        zzanh zza3 = zzamp.zza(zzaol.zzl(zzb[1]));
        zzanu zzb2 = this.bdR.zzb(zzaol);
        return new zza(zzamp, zzb[0], zza2, zzb[1], zza3, zzb2);
    }
}
