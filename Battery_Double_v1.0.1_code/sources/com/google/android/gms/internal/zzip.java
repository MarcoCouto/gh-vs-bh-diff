package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zzk.zza;
import com.google.android.gms.ads.internal.request.zzl;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzfs.zzb;
import com.google.android.gms.internal.zzfs.zzc;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public final class zzip extends zza {
    private static final Object zzamr = new Object();
    private static zzip zzcdx;
    private final Context mContext;
    private final zzio zzcdy;
    private final zzcv zzcdz;
    private final zzfs zzcea;

    zzip(Context context, zzcv zzcv, zzio zzio) {
        this.mContext = context;
        this.zzcdy = zzio;
        this.zzcdz = zzcv;
        this.zzcea = new zzfs(context.getApplicationContext() != null ? context.getApplicationContext() : context, new VersionInfoParcel(9452208, 9452208, true), zzcv.zzjv(), new zzkl<zzfp>() {
            /* renamed from: zza */
            public void zzd(zzfp zzfp) {
                zzfp.zza("/log", zzeo.zzbhv);
            }
        }, new zzb());
    }

    private static AdResponseParcel zza(Context context, zzfs zzfs, zzcv zzcv, zzio zzio, AdRequestInfoParcel adRequestInfoParcel) {
        Bundle bundle;
        Future future;
        zzkd.zzcv("Starting ad request from service using: AFMA_getAd");
        zzdc.initialize(context);
        zzdk zzdk = new zzdk(((Boolean) zzdc.zzaze.get()).booleanValue(), "load_ad", adRequestInfoParcel.zzapa.zzaur);
        if (adRequestInfoParcel.versionCode > 10 && adRequestInfoParcel.zzcbj != -1) {
            zzdk.zza(zzdk.zzc(adRequestInfoParcel.zzcbj), "cts");
        }
        zzdi zzkg = zzdk.zzkg();
        final Bundle bundle2 = (adRequestInfoParcel.versionCode < 4 || adRequestInfoParcel.zzcay == null) ? null : adRequestInfoParcel.zzcay;
        if (!((Boolean) zzdc.zzazn.get()).booleanValue() || zzio.zzcdw == null) {
            bundle = bundle2;
            future = null;
        } else {
            if (bundle2 == null && ((Boolean) zzdc.zzazo.get()).booleanValue()) {
                zzkd.v("contentInfo is not present, but we'll still launch the app index task");
                bundle2 = new Bundle();
            }
            if (bundle2 != null) {
                final zzio zzio2 = zzio;
                final Context context2 = context;
                final AdRequestInfoParcel adRequestInfoParcel2 = adRequestInfoParcel;
                bundle = bundle2;
                future = zzkg.zza((Callable<T>) new Callable<Void>() {
                    /* renamed from: zzcx */
                    public Void call() throws Exception {
                        zzio.this.zzcdw.zza(context2, adRequestInfoParcel2.zzcas.packageName, bundle2);
                        return null;
                    }
                });
            } else {
                bundle = bundle2;
                future = null;
            }
        }
        zzky zzkw = new zzkw(null);
        Bundle bundle3 = adRequestInfoParcel.zzcar.extras;
        zzky zzky = (!adRequestInfoParcel.zzcbq || (bundle3 != null && bundle3.getString("_ad") != null)) ? zzkw : zzio.zzcds.zza(adRequestInfoParcel.applicationInfo);
        zziv zzy = zzu.zzfw().zzy(context);
        if (zzy.zzcgp == -1) {
            zzkd.zzcv("Device is offline.");
            return new AdResponseParcel(2);
        }
        String uuid = adRequestInfoParcel.versionCode >= 7 ? adRequestInfoParcel.zzcbg : UUID.randomUUID().toString();
        zzir zzir = new zzir(uuid, adRequestInfoParcel.applicationInfo.packageName);
        if (adRequestInfoParcel.zzcar.extras != null) {
            String string = adRequestInfoParcel.zzcar.extras.getString("_ad");
            if (string != null) {
                return zziq.zza(context, adRequestInfoParcel, string);
            }
        }
        List zza = zzio.zzcdq.zza(adRequestInfoParcel);
        String zzf = zzio.zzcdt.zzf(adRequestInfoParcel);
        zziz.zza zzz = zzio.zzcdu.zzz(context);
        if (future != null) {
            try {
                zzkd.v("Waiting for app index fetching task.");
                future.get(((Long) zzdc.zzazp.get()).longValue(), TimeUnit.MILLISECONDS);
                zzkd.v("App index fetching task completed.");
            } catch (InterruptedException | ExecutionException e) {
                zzkd.zzd("Failed to fetch app index signal", e);
            } catch (TimeoutException e2) {
                zzkd.zzcv("Timed out waiting for app index fetching task");
            }
        }
        String zzck = zzio.zzcdp.zzck(adRequestInfoParcel.zzcas.packageName);
        JSONObject zza2 = zziq.zza(context, adRequestInfoParcel, zzy, zzz, zzb(zzky), zzcv, zzf, zza, bundle, zzck);
        if (zza2 == null) {
            return new AdResponseParcel(0);
        }
        if (adRequestInfoParcel.versionCode < 7) {
            try {
                zza2.put("request_id", uuid);
            } catch (JSONException e3) {
            }
        }
        try {
            zza2.put("prefetch_mode", "url");
        } catch (JSONException e4) {
            zzkd.zzd("Failed putting prefetch parameters to ad request.", e4);
        }
        final String jSONObject = zza2.toString();
        zzdk.zza(zzkg, "arc");
        final zzdi zzkg2 = zzdk.zzkg();
        final zzfs zzfs2 = zzfs;
        final zzir zzir2 = zzir;
        final zzdk zzdk2 = zzdk;
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                zzc zzma = zzfs.this.zzma();
                zzir2.zzb(zzma);
                zzdk2.zza(zzkg2, "rwc");
                final zzdi zzkg = zzdk2.zzkg();
                zzma.zza(new zzla.zzc<zzft>() {
                    /* renamed from: zzb */
                    public void zzd(zzft zzft) {
                        zzdk2.zza(zzkg, "jsf");
                        zzdk2.zzkh();
                        zzft.zza("/invalidRequest", zzir2.zzcep);
                        zzft.zza("/loadAdURL", zzir2.zzceq);
                        zzft.zza("/loadAd", zzir2.zzcer);
                        try {
                            zzft.zzj("AFMA_getAd", jSONObject);
                        } catch (Exception e) {
                            zzkd.zzb("Error requesting an ad url", e);
                        }
                    }
                }, new zzla.zza() {
                    public void run() {
                    }
                });
            }
        });
        try {
            zziu zziu = (zziu) zzir.zzrh().get(10, TimeUnit.SECONDS);
            if (zziu == null) {
                return new AdResponseParcel(0);
            }
            if (zziu.getErrorCode() != -2) {
                AdResponseParcel adResponseParcel = new AdResponseParcel(zziu.getErrorCode());
                final zzio zzio3 = zzio;
                final Context context3 = context;
                final zzir zzir3 = zzir;
                final AdRequestInfoParcel adRequestInfoParcel3 = adRequestInfoParcel;
                zzkh.zzclc.post(new Runnable() {
                    public void run() {
                        zzio.this.zzcdr.zza(r1, r2, r3.zzaow);
                    }
                });
                return adResponseParcel;
            }
            if (zzdk.zzkj() != null) {
                zzdk.zza(zzdk.zzkj(), "rur");
            }
            AdResponseParcel adResponseParcel2 = null;
            if (!TextUtils.isEmpty(zziu.zzrm())) {
                adResponseParcel2 = zziq.zza(context, adRequestInfoParcel, zziu.zzrm());
            }
            if (adResponseParcel2 == null && !TextUtils.isEmpty(zziu.getUrl())) {
                adResponseParcel2 = zza(adRequestInfoParcel, context, adRequestInfoParcel.zzaow.zzcs, zziu.getUrl(), zzck, zziu, zzdk, zzio);
            }
            if (adResponseParcel2 == null) {
                adResponseParcel2 = new AdResponseParcel(0);
            }
            zzdk.zza(zzkg, "tts");
            adResponseParcel2.zzccl = zzdk.zzki();
            final zzio zzio4 = zzio;
            final Context context4 = context;
            final zzir zzir4 = zzir;
            final AdRequestInfoParcel adRequestInfoParcel4 = adRequestInfoParcel;
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzio.this.zzcdr.zza(r1, r2, r3.zzaow);
                }
            });
            return adResponseParcel2;
        } catch (Exception e5) {
            return new AdResponseParcel(0);
        } finally {
            final zzio zzio5 = zzio;
            final Context context5 = context;
            final zzir zzir5 = zzir;
            final AdRequestInfoParcel adRequestInfoParcel5 = adRequestInfoParcel;
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    zzio.this.zzcdr.zza(context5, zzir5, adRequestInfoParcel5.zzaow);
                }
            });
        }
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.zzis.zzj(long):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01cd, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a9, code lost:
        r6 = r7.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r4 = new java.io.InputStreamReader(r2.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r5 = com.google.android.gms.ads.internal.zzu.zzfq().zza(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        com.google.android.gms.common.util.zzo.zzb(r4);
        zza(r6, r12, r5, r9);
        r8.zzb(r6, r12, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c8, code lost:
        if (r19 == null) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ca, code lost:
        r19.zza(r3, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d7, code lost:
        r3 = r8.zzj(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00de, code lost:
        if (r20 == null) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00e0, code lost:
        r20.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0127, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0128, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        com.google.android.gms.common.util.zzo.zzb(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x012c, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0149, code lost:
        com.google.android.gms.internal.zzkd.zzcx("No location header to follow redirect.");
        r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0157, code lost:
        if (r20 == null) goto L_0x0160;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0159, code lost:
        r20.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x016c, code lost:
        com.google.android.gms.internal.zzkd.zzcx("Too many redirects.");
        r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x017a, code lost:
        if (r20 == null) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x017c, code lost:
        r20.zzcdv.zzrp();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        com.google.android.gms.internal.zzkd.zzcx("Received error HTTP response code: " + r9);
        r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01a7, code lost:
        if (r20 == null) goto L_0x01b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01a9, code lost:
        r20.zzcdv.zzrp();
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:59:0x0115=Splitter:B:59:0x0115, B:94:0x0186=Splitter:B:94:0x0186, B:70:0x0129=Splitter:B:70:0x0129} */
    public static AdResponseParcel zza(AdRequestInfoParcel adRequestInfoParcel, Context context, String str, String str2, String str3, zziu zziu, zzdk zzdk, zzio zzio) {
        HttpURLConnection httpURLConnection;
        BufferedOutputStream bufferedOutputStream;
        zzdi zzdi = zzdk != null ? zzdk.zzkg() : null;
        try {
            zzis zzis = new zzis(adRequestInfoParcel);
            String str4 = "AdRequestServiceImpl: Sending request: ";
            String valueOf = String.valueOf(str2);
            zzkd.zzcv(valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
            URL url = new URL(str2);
            long elapsedRealtime = zzu.zzfu().elapsedRealtime();
            int i = 0;
            URL url2 = url;
            while (true) {
                if (zzio != null) {
                    zzio.zzcdv.zzro();
                }
                httpURLConnection = (HttpURLConnection) url2.openConnection();
                zzu.zzfq().zza(context, str, false, httpURLConnection);
                if (!TextUtils.isEmpty(str3) && zziu.zzrl()) {
                    httpURLConnection.addRequestProperty("x-afma-drt-cookie", str3);
                }
                String str5 = adRequestInfoParcel.zzcbr;
                if (!TextUtils.isEmpty(str5)) {
                    zzkd.zzcv("Sending webview cookie in ad request header.");
                    httpURLConnection.addRequestProperty("Cookie", str5);
                }
                if (zziu != null && !TextUtils.isEmpty(zziu.zzrk())) {
                    httpURLConnection.setDoOutput(true);
                    byte[] bytes = zziu.zzrk().getBytes();
                    httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                    try {
                        bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                        try {
                            bufferedOutputStream.write(bytes);
                            zzo.zzb(bufferedOutputStream);
                        } catch (Throwable th) {
                            th = th;
                            zzo.zzb(bufferedOutputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        bufferedOutputStream = null;
                        zzo.zzb(bufferedOutputStream);
                        throw th;
                    }
                }
                int responseCode = httpURLConnection.getResponseCode();
                Map headerFields = httpURLConnection.getHeaderFields();
                if (responseCode >= 200 && responseCode < 300) {
                    break;
                }
                zza(url2.toString(), headerFields, null, responseCode);
                if (responseCode < 300 || responseCode >= 400) {
                    break;
                }
                String headerField = httpURLConnection.getHeaderField("Location");
                if (TextUtils.isEmpty(headerField)) {
                    break;
                }
                URL url3 = new URL(headerField);
                int i2 = i + 1;
                if (i2 > 5) {
                    break;
                }
                zzis.zzj(headerFields);
                httpURLConnection.disconnect();
                if (zzio != null) {
                    zzio.zzcdv.zzrp();
                }
                i = i2;
                url2 = url3;
            }
        } catch (IOException e) {
            String str6 = "Error while connecting to ad server: ";
            String valueOf2 = String.valueOf(e.getMessage());
            zzkd.zzcx(valueOf2.length() != 0 ? str6.concat(valueOf2) : new String(str6));
            return new AdResponseParcel(2);
        } catch (Throwable th3) {
            httpURLConnection.disconnect();
            if (zzio != null) {
                zzio.zzcdv.zzrp();
            }
            throw th3;
        }
    }

    public static zzip zza(Context context, zzcv zzcv, zzio zzio) {
        zzip zzip;
        synchronized (zzamr) {
            if (zzcdx == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zzcdx = new zzip(context, zzcv, zzio);
            }
            zzip = zzcdx;
        }
        return zzip;
    }

    private static void zza(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzkd.zzaz(2)) {
            zzkd.v(new StringBuilder(String.valueOf(str).length() + 39).append("Http Response: {\n  URL:\n    ").append(str).append("\n  Headers:").toString());
            if (map != null) {
                for (String str3 : map.keySet()) {
                    zzkd.v(new StringBuilder(String.valueOf(str3).length() + 5).append("    ").append(str3).append(":").toString());
                    for (String valueOf : (List) map.get(str3)) {
                        String str4 = "      ";
                        String valueOf2 = String.valueOf(valueOf);
                        zzkd.v(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                    }
                }
            }
            zzkd.v("  Body:");
            if (str2 != null) {
                for (int i2 = 0; i2 < Math.min(str2.length(), 100000); i2 += 1000) {
                    zzkd.v(str2.substring(i2, Math.min(str2.length(), i2 + 1000)));
                }
            } else {
                zzkd.v("    null");
            }
            zzkd.v("  Response Code:\n    " + i + "\n}");
        }
    }

    private static Location zzb(zzky<Location> zzky) {
        try {
            return (Location) zzky.get(((Long) zzdc.zzbcp.get()).longValue(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            zzkd.zzd("Exception caught while getting location", e);
            return null;
        }
    }

    public void zza(final AdRequestInfoParcel adRequestInfoParcel, final zzl zzl) {
        zzu.zzft().zzb(this.mContext, adRequestInfoParcel.zzaow);
        zzkg.zza((Runnable) new Runnable() {
            public void run() {
                AdResponseParcel adResponseParcel;
                try {
                    adResponseParcel = zzip.this.zzd(adRequestInfoParcel);
                } catch (Exception e) {
                    zzu.zzft().zzb((Throwable) e, true);
                    zzkd.zzd("Could not fetch ad response due to an Exception.", e);
                    adResponseParcel = null;
                }
                if (adResponseParcel == null) {
                    adResponseParcel = new AdResponseParcel(0);
                }
                try {
                    zzl.zzb(adResponseParcel);
                } catch (RemoteException e2) {
                    zzkd.zzd("Fail to forward ad response.", e2);
                }
            }
        });
    }

    public AdResponseParcel zzd(AdRequestInfoParcel adRequestInfoParcel) {
        return zza(this.mContext, this.zzcea, this.zzcdz, this.zzcdy, adRequestInfoParcel);
    }
}
