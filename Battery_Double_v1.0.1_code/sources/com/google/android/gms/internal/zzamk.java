package com.google.android.gms.internal;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

final class zzamk implements zzamu<Date>, zzand<Date> {
    private final DateFormat bdE;
    private final DateFormat bdF;
    private final DateFormat bdG;

    zzamk() {
        this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
    }

    public zzamk(int i, int i2) {
        this(DateFormat.getDateTimeInstance(i, i2, Locale.US), DateFormat.getDateTimeInstance(i, i2));
    }

    zzamk(String str) {
        this((DateFormat) new SimpleDateFormat(str, Locale.US), (DateFormat) new SimpleDateFormat(str));
    }

    zzamk(DateFormat dateFormat, DateFormat dateFormat2) {
        this.bdE = dateFormat;
        this.bdF = dateFormat2;
        this.bdG = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        this.bdG.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private Date zza(zzamv zzamv) {
        Date parse;
        synchronized (this.bdF) {
            try {
                parse = this.bdF.parse(zzamv.zzczf());
            } catch (ParseException e) {
                throw new zzane(zzamv.zzczf(), e);
            } catch (ParseException e2) {
                try {
                    parse = this.bdE.parse(zzamv.zzczf());
                } catch (ParseException e3) {
                    parse = this.bdG.parse(zzamv.zzczf());
                }
            }
        }
        return parse;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(zzamk.class.getSimpleName());
        sb.append('(').append(this.bdF.getClass().getSimpleName()).append(')');
        return sb.toString();
    }

    public zzamv zza(Date date, Type type, zzanc zzanc) {
        zzanb zzanb;
        synchronized (this.bdF) {
            zzanb = new zzanb(this.bdE.format(date));
        }
        return zzanb;
    }

    /* renamed from: zza */
    public Date zzb(zzamv zzamv, Type type, zzamt zzamt) throws zzamz {
        if (!(zzamv instanceof zzanb)) {
            throw new zzamz("The date should be a string value");
        }
        Date zza = zza(zzamv);
        if (type == Date.class) {
            return zza;
        }
        if (type == Timestamp.class) {
            return new Timestamp(zza.getTime());
        }
        if (type == java.sql.Date.class) {
            return new java.sql.Date(zza.getTime());
        }
        String valueOf = String.valueOf(getClass());
        String valueOf2 = String.valueOf(type);
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 23 + String.valueOf(valueOf2).length()).append(valueOf).append(" cannot deserialize to ").append(valueOf2).toString());
    }
}
