package com.google.android.gms.internal;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class zzaoi extends zzanh<Time> {
    public static final zzani bfu = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() == Time.class) {
                return new zzaoi();
            }
            return null;
        }
    };
    private final DateFormat bfU = new SimpleDateFormat("hh:mm:ss a");

    public synchronized void zza(zzaoo zzaoo, Time time) throws IOException {
        zzaoo.zzts(time == null ? null : this.bfU.format(time));
    }

    /* renamed from: zzn */
    public synchronized Time zzb(zzaom zzaom) throws IOException {
        Time time;
        if (zzaom.b() == zzaon.NULL) {
            zzaom.nextNull();
            time = null;
        } else {
            try {
                time = new Time(this.bfU.parse(zzaom.nextString()).getTime());
            } catch (ParseException e) {
                throw new zzane((Throwable) e);
            }
        }
        return time;
    }
}
