package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzcd.zza;
import com.google.android.gms.internal.zzcd.zzd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.WeakHashMap;

@zzin
public class zzcg implements zzch {
    private final Object zzail = new Object();
    private final VersionInfoParcel zzalo;
    private final Context zzaql;
    private final WeakHashMap<zzju, zzcd> zzarm = new WeakHashMap<>();
    private final ArrayList<zzcd> zzarn = new ArrayList<>();
    private final zzfs zzaro;

    public zzcg(Context context, VersionInfoParcel versionInfoParcel, zzfs zzfs) {
        this.zzaql = context.getApplicationContext();
        this.zzalo = versionInfoParcel;
        this.zzaro = zzfs;
    }

    public zzcd zza(AdSizeParcel adSizeParcel, zzju zzju) {
        return zza(adSizeParcel, zzju, zzju.zzbtm.getView());
    }

    public zzcd zza(AdSizeParcel adSizeParcel, zzju zzju, View view) {
        return zza(adSizeParcel, zzju, (zzck) new zzd(view, zzju), (zzft) null);
    }

    public zzcd zza(AdSizeParcel adSizeParcel, zzju zzju, View view, zzft zzft) {
        return zza(adSizeParcel, zzju, (zzck) new zzd(view, zzju), zzft);
    }

    public zzcd zza(AdSizeParcel adSizeParcel, zzju zzju, zzh zzh) {
        return zza(adSizeParcel, zzju, (zzck) new zza(zzh), (zzft) null);
    }

    public zzcd zza(AdSizeParcel adSizeParcel, zzju zzju, zzck zzck, @Nullable zzft zzft) {
        zzcd zzcj;
        synchronized (this.zzail) {
            if (zzh(zzju)) {
                zzcj = (zzcd) this.zzarm.get(zzju);
            } else {
                if (zzft != null) {
                    zzcj = new zzci(this.zzaql, adSizeParcel, zzju, this.zzalo, zzck, zzft);
                } else {
                    zzcj = new zzcj(this.zzaql, adSizeParcel, zzju, this.zzalo, zzck, this.zzaro);
                }
                zzcj.zza((zzch) this);
                this.zzarm.put(zzju, zzcj);
                this.zzarn.add(zzcj);
            }
        }
        return zzcj;
    }

    public void zza(zzcd zzcd) {
        synchronized (this.zzail) {
            if (!zzcd.zzha()) {
                this.zzarn.remove(zzcd);
                Iterator it = this.zzarm.entrySet().iterator();
                while (it.hasNext()) {
                    if (((Entry) it.next()).getValue() == zzcd) {
                        it.remove();
                    }
                }
            }
        }
    }

    public boolean zzh(zzju zzju) {
        boolean z;
        synchronized (this.zzail) {
            zzcd zzcd = (zzcd) this.zzarm.get(zzju);
            z = zzcd != null && zzcd.zzha();
        }
        return z;
    }

    public void zzi(zzju zzju) {
        synchronized (this.zzail) {
            zzcd zzcd = (zzcd) this.zzarm.get(zzju);
            if (zzcd != null) {
                zzcd.zzgy();
            }
        }
    }

    public void zzj(zzju zzju) {
        synchronized (this.zzail) {
            zzcd zzcd = (zzcd) this.zzarm.get(zzju);
            if (zzcd != null) {
                zzcd.stop();
            }
        }
    }

    public void zzk(zzju zzju) {
        synchronized (this.zzail) {
            zzcd zzcd = (zzcd) this.zzarm.get(zzju);
            if (zzcd != null) {
                zzcd.pause();
            }
        }
    }

    public void zzl(zzju zzju) {
        synchronized (this.zzail) {
            zzcd zzcd = (zzcd) this.zzarm.get(zzju);
            if (zzcd != null) {
                zzcd.resume();
            }
        }
    }
}
