package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.overlay.zzk;
import java.util.Map;
import java.util.WeakHashMap;
import org.json.JSONObject;

@zzin
public final class zzez implements zzep {
    private final Map<zzlh, Integer> zzbiz = new WeakHashMap();
    private boolean zzbja;

    private static int zza(Context context, Map<String, String> map, String str, int i) {
        String str2 = (String) map.get(str);
        if (str2 == null) {
            return i;
        }
        try {
            return zzm.zziw().zza(context, Integer.parseInt(str2));
        } catch (NumberFormatException e) {
            zzkd.zzcx(new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(str2).length()).append("Could not parse ").append(str).append(" in a video GMSG: ").append(str2).toString());
            return i;
        }
    }

    public void zza(zzlh zzlh, Map<String, String> map) {
        int i;
        String str = (String) map.get("action");
        if (str == null) {
            zzkd.zzcx("Action missing from video GMSG.");
            return;
        }
        if (zzkd.zzaz(3)) {
            JSONObject jSONObject = new JSONObject(map);
            jSONObject.remove("google.afma.Notify_dt");
            String valueOf = String.valueOf(jSONObject.toString());
            zzkd.zzcv(new StringBuilder(String.valueOf(str).length() + 13 + String.valueOf(valueOf).length()).append("Video GMSG: ").append(str).append(" ").append(valueOf).toString());
        }
        if ("background".equals(str)) {
            String str2 = (String) map.get("color");
            if (TextUtils.isEmpty(str2)) {
                zzkd.zzcx("Color parameter missing from color video GMSG.");
                return;
            }
            try {
                int parseColor = Color.parseColor(str2);
                zzlg zzuq = zzlh.zzuq();
                if (zzuq != null) {
                    zzk zzub = zzuq.zzub();
                    if (zzub != null) {
                        zzub.setBackgroundColor(parseColor);
                        return;
                    }
                }
                this.zzbiz.put(zzlh, Integer.valueOf(parseColor));
            } catch (IllegalArgumentException e) {
                zzkd.zzcx("Invalid color parameter in video GMSG.");
            }
        } else {
            zzlg zzuq2 = zzlh.zzuq();
            if (zzuq2 == null) {
                zzkd.zzcx("Could not get underlay container for a video GMSG.");
                return;
            }
            boolean equals = "new".equals(str);
            boolean equals2 = "position".equals(str);
            if (equals || equals2) {
                Context context = zzlh.getContext();
                int zza = zza(context, map, "x", 0);
                int zza2 = zza(context, map, "y", 0);
                int zza3 = zza(context, map, "w", -1);
                int zza4 = zza(context, map, "h", -1);
                try {
                    i = Integer.parseInt((String) map.get("player"));
                } catch (NumberFormatException e2) {
                    i = 0;
                }
                boolean parseBoolean = Boolean.parseBoolean((String) map.get("spherical"));
                if (!equals || zzuq2.zzub() != null) {
                    zzuq2.zze(zza, zza2, zza3, zza4);
                    return;
                }
                zzuq2.zza(zza, zza2, zza3, zza4, i, parseBoolean);
                if (this.zzbiz.containsKey(zzlh)) {
                    zzuq2.zzub().setBackgroundColor(((Integer) this.zzbiz.get(zzlh)).intValue());
                    return;
                }
                return;
            }
            zzk zzub2 = zzuq2.zzub();
            if (zzub2 == null) {
                zzk.zzh(zzlh);
            } else if ("click".equals(str)) {
                Context context2 = zzlh.getContext();
                int zza5 = zza(context2, map, "x", 0);
                int zza6 = zza(context2, map, "y", 0);
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, (float) zza5, (float) zza6, 0);
                zzub2.zzd(obtain);
                obtain.recycle();
            } else if ("currentTime".equals(str)) {
                String str3 = (String) map.get("time");
                if (str3 == null) {
                    zzkd.zzcx("Time parameter missing from currentTime video GMSG.");
                    return;
                }
                try {
                    zzub2.seekTo((int) (Float.parseFloat(str3) * 1000.0f));
                } catch (NumberFormatException e3) {
                    String str4 = "Could not parse time parameter from currentTime video GMSG: ";
                    String valueOf2 = String.valueOf(str3);
                    zzkd.zzcx(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                }
            } else if ("hide".equals(str)) {
                zzub2.setVisibility(4);
            } else if ("load".equals(str)) {
                zzub2.zzlv();
            } else if ("mimetype".equals(str)) {
                zzub2.setMimeType((String) map.get("mimetype"));
            } else if ("muted".equals(str)) {
                if (Boolean.parseBoolean((String) map.get("muted"))) {
                    zzub2.zzno();
                } else {
                    zzub2.zznp();
                }
            } else if ("pause".equals(str)) {
                zzub2.pause();
            } else if ("play".equals(str)) {
                zzub2.play();
            } else if ("show".equals(str)) {
                zzub2.setVisibility(0);
            } else if ("src".equals(str)) {
                zzub2.zzbw((String) map.get("src"));
            } else if ("touchMove".equals(str)) {
                Context context3 = zzlh.getContext();
                zzub2.zza((float) zza(context3, map, "dx", 0), (float) zza(context3, map, "dy", 0));
                if (!this.zzbja) {
                    zzlh.zzuh().zzob();
                    this.zzbja = true;
                }
            } else if ("volume".equals(str)) {
                String str5 = (String) map.get("volume");
                if (str5 == null) {
                    zzkd.zzcx("Level parameter missing from volume video GMSG.");
                    return;
                }
                try {
                    zzub2.zza(Float.parseFloat(str5));
                } catch (NumberFormatException e4) {
                    String str6 = "Could not parse volume parameter from volume video GMSG: ";
                    String valueOf3 = String.valueOf(str5);
                    zzkd.zzcx(valueOf3.length() != 0 ? str6.concat(valueOf3) : new String(str6));
                }
            } else if ("watermark".equals(str)) {
                zzub2.zzon();
            } else {
                String str7 = "Unknown video action: ";
                String valueOf4 = String.valueOf(str);
                zzkd.zzcx(valueOf4.length() != 0 ? str7.concat(valueOf4) : new String(str7));
            }
        }
    }
}
