package com.google.android.gms.internal;

import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class zzanw {

    private static final class zza extends Writer {
        private final Appendable bfn;
        private final C0050zza bfo;

        /* renamed from: com.google.android.gms.internal.zzanw$zza$zza reason: collision with other inner class name */
        static class C0050zza implements CharSequence {
            char[] bfp;

            C0050zza() {
            }

            public char charAt(int i) {
                return this.bfp[i];
            }

            public int length() {
                return this.bfp.length;
            }

            public CharSequence subSequence(int i, int i2) {
                return new String(this.bfp, i, i2 - i);
            }
        }

        private zza(Appendable appendable) {
            this.bfo = new C0050zza();
            this.bfn = appendable;
        }

        public void close() {
        }

        public void flush() {
        }

        public void write(int i) throws IOException {
            this.bfn.append((char) i);
        }

        public void write(char[] cArr, int i, int i2) throws IOException {
            this.bfo.bfp = cArr;
            this.bfn.append(this.bfo, i, i + i2);
        }
    }

    public static Writer zza(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new zza(appendable);
    }

    public static void zzb(zzamv zzamv, zzaoo zzaoo) throws IOException {
        zzaok.bgM.zza(zzaoo, zzamv);
    }

    public static zzamv zzh(zzaom zzaom) throws zzamz {
        boolean z = true;
        try {
            zzaom.b();
            z = false;
            return (zzamv) zzaok.bgM.zzb(zzaom);
        } catch (EOFException e) {
            if (z) {
                return zzamx.bei;
            }
            throw new zzane((Throwable) e);
        } catch (zzaop e2) {
            throw new zzane((Throwable) e2);
        } catch (IOException e3) {
            throw new zzamw((Throwable) e3);
        } catch (NumberFormatException e4) {
            throw new zzane((Throwable) e4);
        }
    }
}
