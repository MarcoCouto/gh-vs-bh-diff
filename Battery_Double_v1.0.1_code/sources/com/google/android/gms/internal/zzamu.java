package com.google.android.gms.internal;

import java.lang.reflect.Type;

public interface zzamu<T> {
    T zzb(zzamv zzamv, Type type, zzamt zzamt) throws zzamz;
}
