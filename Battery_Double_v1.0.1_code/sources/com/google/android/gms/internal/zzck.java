package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.view.View;

public interface zzck {
    @Nullable
    View zzhh();

    boolean zzhi();

    zzck zzhj();
}
