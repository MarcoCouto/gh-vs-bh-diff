package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.overlay.AdLauncherIntentInfoParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.zzu;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@zzin
public class zzli extends WebViewClient {
    private static final String[] zzcoj = {"UNKNOWN", "HOST_LOOKUP", "UNSUPPORTED_AUTH_SCHEME", "AUTHENTICATION", "PROXY_AUTHENTICATION", "CONNECT", "IO", "TIMEOUT", "REDIRECT_LOOP", "UNSUPPORTED_SCHEME", "FAILED_SSL_HANDSHAKE", "BAD_URL", "FILE", "FILE_NOT_FOUND", "TOO_MANY_REQUESTS"};
    private static final String[] zzcok = {"NOT_YET_VALID", "EXPIRED", "ID_MISMATCH", "UNTRUSTED", "DATE_INVALID", "INVALID"};
    private final Object zzail;
    private boolean zzark;
    private com.google.android.gms.ads.internal.client.zza zzatk;
    protected zzlh zzbgf;
    private zzel zzbhm;
    private zzet zzbir;
    private com.google.android.gms.ads.internal.zze zzbit;
    private zzha zzbiu;
    private zzer zzbiw;
    private zzhg zzbqn;
    private zza zzbya;
    private final HashMap<String, List<zzep>> zzcol;
    private zzg zzcom;
    /* access modifiers changed from: private */
    public zzb zzcon;
    private boolean zzcoo;
    private boolean zzcop;
    private zzp zzcoq;
    private final zzhe zzcor;
    private zzd zzcos;
    @Nullable
    protected zzjo zzcot;
    private boolean zzcou;
    private boolean zzcov;
    private boolean zzcow;
    private int zzcox;

    public interface zza {
        void zza(zzlh zzlh, boolean z);
    }

    public interface zzb {
        void zzen();
    }

    private static class zzc implements zzg {
        private zzg zzcom;
        private zzlh zzcoz;

        public zzc(zzlh zzlh, zzg zzg) {
            this.zzcoz = zzlh;
            this.zzcom = zzg;
        }

        public void onPause() {
        }

        public void onResume() {
        }

        public void zzdx() {
            this.zzcom.zzdx();
            this.zzcoz.zzuc();
        }

        public void zzdy() {
            this.zzcom.zzdy();
            this.zzcoz.zzoa();
        }
    }

    public interface zzd {
        void zzem();
    }

    private class zze implements zzep {
        private zze() {
        }

        public void zza(zzlh zzlh, Map<String, String> map) {
            if (map.keySet().contains("start")) {
                zzli.this.zzvb();
            } else if (map.keySet().contains("stop")) {
                zzli.this.zzvc();
            } else if (map.keySet().contains("cancel")) {
                zzli.this.zzvd();
            }
        }
    }

    public zzli(zzlh zzlh, boolean z) {
        this(zzlh, z, new zzhe(zzlh, zzlh.zzuf(), new zzcu(zzlh.getContext())), null);
    }

    zzli(zzlh zzlh, boolean z, zzhe zzhe, zzha zzha) {
        this.zzcol = new HashMap<>();
        this.zzail = new Object();
        this.zzcoo = false;
        this.zzbgf = zzlh;
        this.zzark = z;
        this.zzcor = zzhe;
        this.zzbiu = zzha;
    }

    private void zza(Context context, String str, String str2, String str3) {
        if (((Boolean) zzdc.zzbav.get()).booleanValue()) {
            Bundle bundle = new Bundle();
            bundle.putString("err", str);
            bundle.putString("code", str2);
            bundle.putString("host", zzda(str3));
            zzu.zzfq().zza(context, this.zzbgf.zzum().zzcs, "gmob-apps", bundle, true);
        }
    }

    private String zzda(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        Uri parse = Uri.parse(str);
        return parse.getHost() != null ? parse.getHost() : "";
    }

    private static boolean zzh(Uri uri) {
        String scheme = uri.getScheme();
        return "http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme);
    }

    /* access modifiers changed from: private */
    public void zzvb() {
        synchronized (this.zzail) {
            this.zzcop = true;
        }
        this.zzcox++;
        zzve();
    }

    /* access modifiers changed from: private */
    public void zzvc() {
        this.zzcox--;
        zzve();
    }

    /* access modifiers changed from: private */
    public void zzvd() {
        this.zzcow = true;
        zzve();
    }

    public final void onLoadResource(WebView webView, String str) {
        String str2 = "Loading resource: ";
        String valueOf = String.valueOf(str);
        zzkd.v(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        Uri parse = Uri.parse(str);
        if ("gmsg".equalsIgnoreCase(parse.getScheme()) && "mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            zzi(parse);
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        synchronized (this.zzail) {
            if (this.zzcou) {
                zzkd.v("Blank page loaded, 1...");
                this.zzbgf.zzuo();
                return;
            }
            this.zzcov = true;
            zzve();
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        zza(this.zzbgf.getContext(), "http_err", (i >= 0 || (-i) + -1 >= zzcoj.length) ? String.valueOf(i) : zzcoj[(-i) - 1], str2);
        super.onReceivedError(webView, i, str, str2);
    }

    public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (sslError != null) {
            int primaryError = sslError.getPrimaryError();
            zza(this.zzbgf.getContext(), "ssl_err", (primaryError < 0 || primaryError >= zzcok.length) ? String.valueOf(primaryError) : zzcok[primaryError], zzu.zzfs().zza(sslError));
        }
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
    }

    public final void reset() {
        if (this.zzcot != null) {
            this.zzcot.zzrx();
            this.zzcot = null;
        }
        synchronized (this.zzail) {
            this.zzcol.clear();
            this.zzatk = null;
            this.zzcom = null;
            this.zzbya = null;
            this.zzbhm = null;
            this.zzcoo = false;
            this.zzark = false;
            this.zzcop = false;
            this.zzbiw = null;
            this.zzcoq = null;
            this.zzcon = null;
            if (this.zzbiu != null) {
                this.zzbiu.zzs(true);
                this.zzbiu = null;
            }
        }
    }

    public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case TransportMediator.KEYCODE_MEDIA_PLAY /*126*/:
            case TransportMediator.KEYCODE_MEDIA_PAUSE /*127*/:
            case 128:
            case 129:
            case TransportMediator.KEYCODE_MEDIA_RECORD /*130*/:
            case 222:
                return true;
            default:
                return false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Uri uri;
        String str2 = "AdWebView shouldOverrideUrlLoading: ";
        String valueOf = String.valueOf(str);
        zzkd.v(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        Uri parse = Uri.parse(str);
        if ("gmsg".equalsIgnoreCase(parse.getScheme()) && "mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            zzi(parse);
        } else if (this.zzcoo && webView == this.zzbgf.getWebView() && zzh(parse)) {
            if (this.zzatk != null && ((Boolean) zzdc.zzazu.get()).booleanValue()) {
                this.zzatk.onAdClicked();
                if (this.zzcot != null) {
                    this.zzcot.zzci(str);
                }
                this.zzatk = null;
            }
            return super.shouldOverrideUrlLoading(webView, str);
        } else if (!this.zzbgf.getWebView().willNotDraw()) {
            try {
                zzas zzul = this.zzbgf.zzul();
                if (zzul != null && zzul.zzc(parse)) {
                    parse = zzul.zzb(parse, this.zzbgf.getContext());
                }
                uri = parse;
            } catch (zzat e) {
                String str3 = "Unable to append parameter to URL: ";
                String valueOf2 = String.valueOf(str);
                zzkd.zzcx(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
                uri = parse;
            }
            if (this.zzbit == null || this.zzbit.zzel()) {
                zza(new AdLauncherIntentInfoParcel("android.intent.action.VIEW", uri.toString(), null, null, null, null, null));
            } else {
                this.zzbit.zzt(str);
            }
        } else {
            String str4 = "AdWebView unable to handle URL: ";
            String valueOf3 = String.valueOf(str);
            zzkd.zzcx(valueOf3.length() != 0 ? str4.concat(valueOf3) : new String(str4));
        }
        return true;
    }

    public void zza(int i, int i2, boolean z) {
        this.zzcor.zze(i, i2);
        if (this.zzbiu != null) {
            this.zzbiu.zza(i, i2, z);
        }
    }

    public void zza(com.google.android.gms.ads.internal.client.zza zza2, zzg zzg, zzel zzel, zzp zzp, boolean z, zzer zzer, @Nullable zzet zzet, com.google.android.gms.ads.internal.zze zze2, zzhg zzhg, @Nullable zzjo zzjo) {
        if (zze2 == null) {
            zze2 = new com.google.android.gms.ads.internal.zze(this.zzbgf.getContext());
        }
        this.zzbiu = new zzha(this.zzbgf, zzhg);
        this.zzcot = zzjo;
        zza("/appEvent", (zzep) new zzek(zzel));
        zza("/backButton", zzeo.zzbhx);
        zza("/refresh", zzeo.zzbhy);
        zza("/canOpenURLs", zzeo.zzbho);
        zza("/canOpenIntents", zzeo.zzbhp);
        zza("/click", zzeo.zzbhq);
        zza("/close", zzeo.zzbhr);
        zza("/customClose", zzeo.zzbht);
        zza("/instrument", zzeo.zzbic);
        zza("/delayPageLoaded", (zzep) new zze());
        zza("/httpTrack", zzeo.zzbhu);
        zza("/log", zzeo.zzbhv);
        zza("/mraid", (zzep) new zzev(zze2, this.zzbiu));
        zza("/mraidLoaded", (zzep) this.zzcor);
        zza("/open", (zzep) new zzew(zzer, zze2, this.zzbiu));
        zza("/precache", zzeo.zzbib);
        zza("/touch", zzeo.zzbhw);
        zza("/video", zzeo.zzbhz);
        zza("/videoMeta", zzeo.zzbia);
        zza("/appStreaming", zzeo.zzbhs);
        if (zzet != null) {
            zza("/setInterstitialProperties", (zzep) new zzes(zzet));
        }
        this.zzatk = zza2;
        this.zzcom = zzg;
        this.zzbhm = zzel;
        this.zzbiw = zzer;
        this.zzcoq = zzp;
        this.zzbit = zze2;
        this.zzbqn = zzhg;
        this.zzbir = zzet;
        zzak(z);
    }

    public final void zza(AdLauncherIntentInfoParcel adLauncherIntentInfoParcel) {
        zzg zzg = null;
        boolean zzun = this.zzbgf.zzun();
        com.google.android.gms.ads.internal.client.zza zza2 = (!zzun || this.zzbgf.zzdn().zzaus) ? this.zzatk : null;
        if (!zzun) {
            zzg = this.zzcom;
        }
        zza(new AdOverlayInfoParcel(adLauncherIntentInfoParcel, zza2, zzg, this.zzcoq, this.zzbgf.zzum()));
    }

    public void zza(AdOverlayInfoParcel adOverlayInfoParcel) {
        boolean z = false;
        boolean z2 = this.zzbiu != null ? this.zzbiu.zzmw() : false;
        com.google.android.gms.ads.internal.overlay.zze zzfo = zzu.zzfo();
        Context context = this.zzbgf.getContext();
        if (!z2) {
            z = true;
        }
        zzfo.zza(context, adOverlayInfoParcel, z);
        if (this.zzcot != null) {
            String str = adOverlayInfoParcel.url;
            if (str == null && adOverlayInfoParcel.zzbtj != null) {
                str = adOverlayInfoParcel.zzbtj.url;
            }
            this.zzcot.zzci(str);
        }
    }

    public void zza(zza zza2) {
        this.zzbya = zza2;
    }

    public void zza(zzb zzb2) {
        this.zzcon = zzb2;
    }

    public void zza(zzd zzd2) {
        this.zzcos = zzd2;
    }

    public void zza(String str, zzep zzep) {
        synchronized (this.zzail) {
            List list = (List) this.zzcol.get(str);
            if (list == null) {
                list = new CopyOnWriteArrayList();
                this.zzcol.put(str, list);
            }
            list.add(zzep);
        }
    }

    public final void zza(boolean z, int i) {
        zza(new AdOverlayInfoParcel((!this.zzbgf.zzun() || this.zzbgf.zzdn().zzaus) ? this.zzatk : null, this.zzcom, this.zzcoq, this.zzbgf, z, i, this.zzbgf.zzum()));
    }

    public final void zza(boolean z, int i, String str) {
        zzc zzc2 = null;
        boolean zzun = this.zzbgf.zzun();
        com.google.android.gms.ads.internal.client.zza zza2 = (!zzun || this.zzbgf.zzdn().zzaus) ? this.zzatk : null;
        if (!zzun) {
            zzc2 = new zzc(this.zzbgf, this.zzcom);
        }
        zza(new AdOverlayInfoParcel(zza2, zzc2, this.zzbhm, this.zzcoq, this.zzbgf, z, i, str, this.zzbgf.zzum(), this.zzbiw));
    }

    public final void zza(boolean z, int i, String str, String str2) {
        boolean zzun = this.zzbgf.zzun();
        zza(new AdOverlayInfoParcel((!zzun || this.zzbgf.zzdn().zzaus) ? this.zzatk : null, zzun ? null : new zzc(this.zzbgf, this.zzcom), this.zzbhm, this.zzcoq, this.zzbgf, z, i, str, str2, this.zzbgf.zzum(), this.zzbiw));
    }

    public void zzak(boolean z) {
        this.zzcoo = z;
    }

    public void zzb(String str, zzep zzep) {
        synchronized (this.zzail) {
            List list = (List) this.zzcol.get(str);
            if (list != null) {
                list.remove(zzep);
            }
        }
    }

    public void zzd(int i, int i2) {
        if (this.zzbiu != null) {
            this.zzbiu.zzd(i, i2);
        }
    }

    public boolean zzho() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzark;
        }
        return z;
    }

    public void zzi(Uri uri) {
        String path = uri.getPath();
        List<zzep> list = (List) this.zzcol.get(path);
        if (list != null) {
            Map zzf = zzu.zzfq().zzf(uri);
            if (zzkd.zzaz(2)) {
                String str = "Received GMSG: ";
                String valueOf = String.valueOf(path);
                zzkd.v(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                for (String str2 : zzf.keySet()) {
                    String str3 = (String) zzf.get(str2);
                    zzkd.v(new StringBuilder(String.valueOf(str2).length() + 4 + String.valueOf(str3).length()).append("  ").append(str2).append(": ").append(str3).toString());
                }
            }
            for (zzep zza2 : list) {
                zza2.zza(this.zzbgf, zzf);
            }
            return;
        }
        String valueOf2 = String.valueOf(uri);
        zzkd.v(new StringBuilder(String.valueOf(valueOf2).length() + 32).append("No GMSG handler found for GMSG: ").append(valueOf2).toString());
    }

    public void zzl(zzlh zzlh) {
        this.zzbgf = zzlh;
    }

    public final void zznx() {
        synchronized (this.zzail) {
            this.zzcoo = false;
            this.zzark = true;
            zzu.zzfq().runOnUiThread(new Runnable() {
                public void run() {
                    zzli.this.zzbgf.zzuu();
                    com.google.android.gms.ads.internal.overlay.zzd zzuh = zzli.this.zzbgf.zzuh();
                    if (zzuh != null) {
                        zzuh.zznx();
                    }
                    if (zzli.this.zzcon != null) {
                        zzli.this.zzcon.zzen();
                        zzli.this.zzcon = null;
                    }
                }
            });
        }
    }

    public com.google.android.gms.ads.internal.zze zzux() {
        return this.zzbit;
    }

    public boolean zzuy() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzcop;
        }
        return z;
    }

    public void zzuz() {
        synchronized (this.zzail) {
            zzkd.v("Loading blank page in WebView, 2...");
            this.zzcou = true;
            this.zzbgf.zzcy("about:blank");
        }
    }

    public void zzva() {
        if (this.zzcot != null) {
            zzkh.zzclc.post(new Runnable() {
                public void run() {
                    if (zzli.this.zzcot != null) {
                        zzli.this.zzcot.zzj(zzli.this.zzbgf.getView());
                    }
                }
            });
        }
    }

    public final void zzve() {
        if (this.zzbya != null && ((this.zzcov && this.zzcox <= 0) || this.zzcow)) {
            this.zzbya.zza(this.zzbgf, !this.zzcow);
            this.zzbya = null;
        }
        this.zzbgf.zzuv();
    }

    public zzd zzvf() {
        return this.zzcos;
    }
}
