package com.google.android.gms.internal;

import java.util.concurrent.Future;

public interface zzky<A> extends Future<A> {
    void zzc(Runnable runnable);
}
