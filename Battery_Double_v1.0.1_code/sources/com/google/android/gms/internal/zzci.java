package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import org.json.JSONObject;

@zzin
public class zzci extends zzcd {
    private final zzft zzarp;

    public zzci(Context context, AdSizeParcel adSizeParcel, zzju zzju, VersionInfoParcel versionInfoParcel, zzck zzck, zzft zzft) {
        super(context, adSizeParcel, zzju, versionInfoParcel, zzck);
        this.zzarp = zzft;
        zzc(this.zzarp);
        zzgw();
        zzk(3);
        String str = "Tracking ad unit: ";
        String valueOf = String.valueOf(this.zzaqk.zzhn());
        zzkd.zzcv(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    /* access modifiers changed from: protected */
    public void destroy() {
        synchronized (this.zzail) {
            super.destroy();
            zzd(this.zzarp);
        }
    }

    /* access modifiers changed from: protected */
    public void zzb(JSONObject jSONObject) {
        this.zzarp.zza("AFMA_updateActiveView", jSONObject);
    }

    public void zzgy() {
        destroy();
    }

    /* access modifiers changed from: protected */
    public boolean zzhe() {
        return true;
    }
}
