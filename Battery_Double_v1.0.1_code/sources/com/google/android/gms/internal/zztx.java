package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.flags.ModuleDescriptor;
import com.google.android.gms.internal.zzty.zza;

public class zztx {
    private zzty OS = null;
    private boolean zzamt = false;

    public void initialize(Context context) {
        synchronized (this) {
            if (!this.zzamt) {
                try {
                    this.OS = zza.asInterface(zzsb.zza(context, zzsb.KI, ModuleDescriptor.MODULE_ID).zziu("com.google.android.gms.flags.impl.FlagProviderImpl"));
                    this.OS.init(zze.zzac(context));
                    this.zzamt = true;
                } catch (RemoteException | zzsb.zza e) {
                    Log.w("FlagValueProvider", "Failed to initialize flags module.", e);
                }
                return;
            }
            return;
        }
    }

    public <T> T zzb(zztv<T> zztv) {
        synchronized (this) {
            if (this.zzamt) {
                return zztv.zza(this.OS);
            }
            T zzjw = zztv.zzjw();
            return zzjw;
        }
    }
}
