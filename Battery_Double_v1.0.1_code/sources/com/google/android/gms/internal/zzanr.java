package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzanr {
    public static zzanr beV;

    public abstract void zzi(zzaom zzaom) throws IOException;
}
