package com.google.android.gms.internal;

@zzin
public class zzjm {
    private final zzgk zzbog;
    private final zzjj zzcib;

    public zzjm(zzgk zzgk, zzji zzji) {
        this.zzbog = zzgk;
        this.zzcib = new zzjj(zzji);
    }

    public zzgk zzru() {
        return this.zzbog;
    }

    public zzjj zzrv() {
        return this.zzcib;
    }
}
