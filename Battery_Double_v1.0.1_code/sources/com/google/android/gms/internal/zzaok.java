package com.google.android.gms.internal;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.UUID;

public final class zzaok {
    public static final zzanh<Class> bfX = new zzanh<Class>() {
        public void zza(zzaoo zzaoo, Class cls) throws IOException {
            if (cls == null) {
                zzaoo.l();
            } else {
                String valueOf = String.valueOf(cls.getName());
                throw new UnsupportedOperationException(new StringBuilder(String.valueOf(valueOf).length() + 76).append("Attempted to serialize java.lang.Class: ").append(valueOf).append(". Forgot to register a type adapter?").toString());
            }
        }

        /* renamed from: zzo */
        public Class zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }
    };
    public static final zzani bfY = zza(Class.class, bfX);
    public static final zzanh<BitSet> bfZ = new zzanh<BitSet>() {
        public void zza(zzaoo zzaoo, BitSet bitSet) throws IOException {
            if (bitSet == null) {
                zzaoo.l();
                return;
            }
            zzaoo.h();
            for (int i = 0; i < bitSet.length(); i++) {
                zzaoo.zzcr((long) (bitSet.get(i) ? 1 : 0));
            }
            zzaoo.i();
        }

        /* renamed from: zzx */
        public BitSet zzb(zzaom zzaom) throws IOException {
            boolean z;
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            BitSet bitSet = new BitSet();
            zzaom.beginArray();
            zzaon b = zzaom.b();
            int i = 0;
            while (b != zzaon.END_ARRAY) {
                switch (AnonymousClass26.bfK[b.ordinal()]) {
                    case 1:
                        if (zzaom.nextInt() == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    case 2:
                        z = zzaom.nextBoolean();
                        break;
                    case 3:
                        String nextString = zzaom.nextString();
                        try {
                            if (Integer.parseInt(nextString) == 0) {
                                z = false;
                                break;
                            } else {
                                z = true;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            String str = "Error: Expecting: bitset number value (1, 0), Found: ";
                            String valueOf = String.valueOf(nextString);
                            throw new zzane(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        }
                    default:
                        String valueOf2 = String.valueOf(b);
                        throw new zzane(new StringBuilder(String.valueOf(valueOf2).length() + 27).append("Invalid bitset value type: ").append(valueOf2).toString());
                }
                if (z) {
                    bitSet.set(i);
                }
                i++;
                b = zzaom.b();
            }
            zzaom.endArray();
            return bitSet;
        }
    };
    public static final zzani bgA = zza(URL.class, bgz);
    public static final zzanh<URI> bgB = new zzanh<URI>() {
        public void zza(zzaoo zzaoo, URI uri) throws IOException {
            zzaoo.zzts(uri == null ? null : uri.toASCIIString());
        }

        /* renamed from: zzw */
        public URI zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                String nextString = zzaom.nextString();
                if (!"null".equals(nextString)) {
                    return new URI(nextString);
                }
                return null;
            } catch (URISyntaxException e) {
                throw new zzamw((Throwable) e);
            }
        }
    };
    public static final zzani bgC = zza(URI.class, bgB);
    public static final zzanh<InetAddress> bgD = new zzanh<InetAddress>() {
        public void zza(zzaoo zzaoo, InetAddress inetAddress) throws IOException {
            zzaoo.zzts(inetAddress == null ? null : inetAddress.getHostAddress());
        }

        /* renamed from: zzy */
        public InetAddress zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return InetAddress.getByName(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgE = zzb(InetAddress.class, bgD);
    public static final zzanh<UUID> bgF = new zzanh<UUID>() {
        public void zza(zzaoo zzaoo, UUID uuid) throws IOException {
            zzaoo.zzts(uuid == null ? null : uuid.toString());
        }

        /* renamed from: zzz */
        public UUID zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return UUID.fromString(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgG = zza(UUID.class, bgF);
    public static final zzani bgH = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            if (zzaol.m() != Timestamp.class) {
                return null;
            }
            final zzanh zzk = zzamp.zzk(Date.class);
            return new zzanh<Timestamp>() {
                public void zza(zzaoo zzaoo, Timestamp timestamp) throws IOException {
                    zzk.zza(zzaoo, timestamp);
                }

                /* renamed from: zzaa */
                public Timestamp zzb(zzaom zzaom) throws IOException {
                    Date date = (Date) zzk.zzb(zzaom);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }
            };
        }
    };
    public static final zzanh<Calendar> bgI = new zzanh<Calendar>() {
        public void zza(zzaoo zzaoo, Calendar calendar) throws IOException {
            if (calendar == null) {
                zzaoo.l();
                return;
            }
            zzaoo.j();
            zzaoo.zztr("year");
            zzaoo.zzcr((long) calendar.get(1));
            zzaoo.zztr("month");
            zzaoo.zzcr((long) calendar.get(2));
            zzaoo.zztr("dayOfMonth");
            zzaoo.zzcr((long) calendar.get(5));
            zzaoo.zztr("hourOfDay");
            zzaoo.zzcr((long) calendar.get(11));
            zzaoo.zztr("minute");
            zzaoo.zzcr((long) calendar.get(12));
            zzaoo.zztr("second");
            zzaoo.zzcr((long) calendar.get(13));
            zzaoo.k();
        }

        /* renamed from: zzab */
        public Calendar zzb(zzaom zzaom) throws IOException {
            int i = 0;
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            zzaom.beginObject();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (zzaom.b() != zzaon.END_OBJECT) {
                String nextName = zzaom.nextName();
                int nextInt = zzaom.nextInt();
                if ("year".equals(nextName)) {
                    i6 = nextInt;
                } else if ("month".equals(nextName)) {
                    i5 = nextInt;
                } else if ("dayOfMonth".equals(nextName)) {
                    i4 = nextInt;
                } else if ("hourOfDay".equals(nextName)) {
                    i3 = nextInt;
                } else if ("minute".equals(nextName)) {
                    i2 = nextInt;
                } else if ("second".equals(nextName)) {
                    i = nextInt;
                }
            }
            zzaom.endObject();
            return new GregorianCalendar(i6, i5, i4, i3, i2, i);
        }
    };
    public static final zzani bgJ = zzb(Calendar.class, GregorianCalendar.class, bgI);
    public static final zzanh<Locale> bgK = new zzanh<Locale>() {
        public void zza(zzaoo zzaoo, Locale locale) throws IOException {
            zzaoo.zzts(locale == null ? null : locale.toString());
        }

        /* renamed from: zzac */
        public Locale zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(zzaom.nextString(), "_");
            String str = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String str2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String str3 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            return (str2 == null && str3 == null) ? new Locale(str) : str3 == null ? new Locale(str, str2) : new Locale(str, str2, str3);
        }
    };
    public static final zzani bgL = zza(Locale.class, bgK);
    public static final zzanh<zzamv> bgM = new zzanh<zzamv>() {
        public void zza(zzaoo zzaoo, zzamv zzamv) throws IOException {
            if (zzamv == null || zzamv.zzczj()) {
                zzaoo.l();
            } else if (zzamv.zzczi()) {
                zzanb zzczm = zzamv.zzczm();
                if (zzczm.zzczp()) {
                    zzaoo.zza(zzczm.zzcze());
                } else if (zzczm.zzczo()) {
                    zzaoo.zzda(zzczm.getAsBoolean());
                } else {
                    zzaoo.zzts(zzczm.zzczf());
                }
            } else if (zzamv.zzczg()) {
                zzaoo.h();
                Iterator it = zzamv.zzczl().iterator();
                while (it.hasNext()) {
                    zza(zzaoo, (zzamv) it.next());
                }
                zzaoo.i();
            } else if (zzamv.zzczh()) {
                zzaoo.j();
                for (Entry entry : zzamv.zzczk().entrySet()) {
                    zzaoo.zztr((String) entry.getKey());
                    zza(zzaoo, (zzamv) entry.getValue());
                }
                zzaoo.k();
            } else {
                String valueOf = String.valueOf(zzamv.getClass());
                throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 15).append("Couldn't write ").append(valueOf).toString());
            }
        }

        /* renamed from: zzad */
        public zzamv zzb(zzaom zzaom) throws IOException {
            switch (AnonymousClass26.bfK[zzaom.b().ordinal()]) {
                case 1:
                    return new zzanb((Number) new zzans(zzaom.nextString()));
                case 2:
                    return new zzanb(Boolean.valueOf(zzaom.nextBoolean()));
                case 3:
                    return new zzanb(zzaom.nextString());
                case 4:
                    zzaom.nextNull();
                    return zzamx.bei;
                case 5:
                    zzams zzams = new zzams();
                    zzaom.beginArray();
                    while (zzaom.hasNext()) {
                        zzams.zzc((zzamv) zzb(zzaom));
                    }
                    zzaom.endArray();
                    return zzams;
                case 6:
                    zzamy zzamy = new zzamy();
                    zzaom.beginObject();
                    while (zzaom.hasNext()) {
                        zzamy.zza(zzaom.nextName(), (zzamv) zzb(zzaom));
                    }
                    zzaom.endObject();
                    return zzamy;
                default:
                    throw new IllegalArgumentException();
            }
        }
    };
    public static final zzani bgN = zzb(zzamv.class, bgM);
    public static final zzani bgO = new zzani() {
        public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
            Class<Enum> m = zzaol.m();
            if (!Enum.class.isAssignableFrom(m) || m == Enum.class) {
                return null;
            }
            if (!m.isEnum()) {
                m = m.getSuperclass();
            }
            return new zza(m);
        }
    };
    public static final zzani bga = zza(BitSet.class, bfZ);
    public static final zzanh<Boolean> bgb = new zzanh<Boolean>() {
        public void zza(zzaoo zzaoo, Boolean bool) throws IOException {
            if (bool == null) {
                zzaoo.l();
            } else {
                zzaoo.zzda(bool.booleanValue());
            }
        }

        /* renamed from: zzae */
        public Boolean zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return zzaom.b() == zzaon.STRING ? Boolean.valueOf(Boolean.parseBoolean(zzaom.nextString())) : Boolean.valueOf(zzaom.nextBoolean());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<Boolean> bgc = new zzanh<Boolean>() {
        public void zza(zzaoo zzaoo, Boolean bool) throws IOException {
            zzaoo.zzts(bool == null ? "null" : bool.toString());
        }

        /* renamed from: zzae */
        public Boolean zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return Boolean.valueOf(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgd = zza(Boolean.TYPE, Boolean.class, bgb);
    public static final zzanh<Number> bge = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Byte.valueOf((byte) zzaom.nextInt());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgf = zza(Byte.TYPE, Byte.class, bge);
    public static final zzanh<Number> bgg = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Short.valueOf((short) zzaom.nextInt());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgh = zza(Short.TYPE, Short.class, bgg);
    public static final zzanh<Number> bgi = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Integer.valueOf(zzaom.nextInt());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgj = zza(Integer.TYPE, Integer.class, bgi);
    public static final zzanh<Number> bgk = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return Long.valueOf(zzaom.nextLong());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzanh<Number> bgl = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return Float.valueOf((float) zzaom.nextDouble());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<Number> bgm = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return Double.valueOf(zzaom.nextDouble());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<Number> bgn = new zzanh<Number>() {
        public void zza(zzaoo zzaoo, Number number) throws IOException {
            zzaoo.zza(number);
        }

        /* renamed from: zzg */
        public Number zzb(zzaom zzaom) throws IOException {
            zzaon b = zzaom.b();
            switch (b) {
                case NUMBER:
                    return new zzans(zzaom.nextString());
                case NULL:
                    zzaom.nextNull();
                    return null;
                default:
                    String valueOf = String.valueOf(b);
                    throw new zzane(new StringBuilder(String.valueOf(valueOf).length() + 23).append("Expecting number, got: ").append(valueOf).toString());
            }
        }
    };
    public static final zzani bgo = zza(Number.class, bgn);
    public static final zzanh<Character> bgp = new zzanh<Character>() {
        public void zza(zzaoo zzaoo, Character ch) throws IOException {
            zzaoo.zzts(ch == null ? null : String.valueOf(ch));
        }

        /* renamed from: zzp */
        public Character zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            String nextString = zzaom.nextString();
            if (nextString.length() == 1) {
                return Character.valueOf(nextString.charAt(0));
            }
            String str = "Expecting character, got: ";
            String valueOf = String.valueOf(nextString);
            throw new zzane(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        }
    };
    public static final zzani bgq = zza(Character.TYPE, Character.class, bgp);
    public static final zzanh<String> bgr = new zzanh<String>() {
        public void zza(zzaoo zzaoo, String str) throws IOException {
            zzaoo.zzts(str);
        }

        /* renamed from: zzq */
        public String zzb(zzaom zzaom) throws IOException {
            zzaon b = zzaom.b();
            if (b != zzaon.NULL) {
                return b == zzaon.BOOLEAN ? Boolean.toString(zzaom.nextBoolean()) : zzaom.nextString();
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzanh<BigDecimal> bgs = new zzanh<BigDecimal>() {
        public void zza(zzaoo zzaoo, BigDecimal bigDecimal) throws IOException {
            zzaoo.zza(bigDecimal);
        }

        /* renamed from: zzr */
        public BigDecimal zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return new BigDecimal(zzaom.nextString());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzanh<BigInteger> bgt = new zzanh<BigInteger>() {
        public void zza(zzaoo zzaoo, BigInteger bigInteger) throws IOException {
            zzaoo.zza(bigInteger);
        }

        /* renamed from: zzs */
        public BigInteger zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            try {
                return new BigInteger(zzaom.nextString());
            } catch (NumberFormatException e) {
                throw new zzane((Throwable) e);
            }
        }
    };
    public static final zzani bgu = zza(String.class, bgr);
    public static final zzanh<StringBuilder> bgv = new zzanh<StringBuilder>() {
        public void zza(zzaoo zzaoo, StringBuilder sb) throws IOException {
            zzaoo.zzts(sb == null ? null : sb.toString());
        }

        /* renamed from: zzt */
        public StringBuilder zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return new StringBuilder(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgw = zza(StringBuilder.class, bgv);
    public static final zzanh<StringBuffer> bgx = new zzanh<StringBuffer>() {
        public void zza(zzaoo zzaoo, StringBuffer stringBuffer) throws IOException {
            zzaoo.zzts(stringBuffer == null ? null : stringBuffer.toString());
        }

        /* renamed from: zzu */
        public StringBuffer zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return new StringBuffer(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    };
    public static final zzani bgy = zza(StringBuffer.class, bgx);
    public static final zzanh<URL> bgz = new zzanh<URL>() {
        public void zza(zzaoo zzaoo, URL url) throws IOException {
            zzaoo.zzts(url == null ? null : url.toExternalForm());
        }

        /* renamed from: zzv */
        public URL zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() == zzaon.NULL) {
                zzaom.nextNull();
                return null;
            }
            String nextString = zzaom.nextString();
            if (!"null".equals(nextString)) {
                return new URL(nextString);
            }
            return null;
        }
    };

    private static final class zza<T extends Enum<T>> extends zzanh<T> {
        private final Map<String, T> bgY = new HashMap();
        private final Map<T, String> bgZ = new HashMap();

        public zza(Class<T> cls) {
            Enum[] enumArr;
            try {
                for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                    String name = enumR.name();
                    zzank zzank = (zzank) cls.getField(name).getAnnotation(zzank.class);
                    if (zzank != null) {
                        name = zzank.value();
                        for (String put : zzank.zzczs()) {
                            this.bgY.put(put, enumR);
                        }
                    }
                    String str = name;
                    this.bgY.put(str, enumR);
                    this.bgZ.put(enumR, str);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError();
            }
        }

        public void zza(zzaoo zzaoo, T t) throws IOException {
            zzaoo.zzts(t == null ? null : (String) this.bgZ.get(t));
        }

        /* renamed from: zzaf */
        public T zzb(zzaom zzaom) throws IOException {
            if (zzaom.b() != zzaon.NULL) {
                return (Enum) this.bgY.get(zzaom.nextString());
            }
            zzaom.nextNull();
            return null;
        }
    }

    public static <TT> zzani zza(final zzaol<TT> zzaol, final zzanh<TT> zzanh) {
        return new zzani() {
            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                if (zzaol.equals(zzaol)) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zza(final Class<TT> cls, final zzanh<TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(zzanh);
                return new StringBuilder(String.valueOf(valueOf).length() + 23 + String.valueOf(valueOf2).length()).append("Factory[type=").append(valueOf).append(",adapter=").append(valueOf2).append("]").toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                if (zzaol.m() == cls) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zza(final Class<TT> cls, final Class<TT> cls2, final zzanh<? super TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls2.getName());
                String valueOf2 = String.valueOf(cls.getName());
                String valueOf3 = String.valueOf(zzanh);
                return new StringBuilder(String.valueOf(valueOf).length() + 24 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append("Factory[type=").append(valueOf).append("+").append(valueOf2).append(",adapter=").append(valueOf3).append("]").toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                Class m = zzaol.m();
                if (m == cls || m == cls2) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zzb(final Class<TT> cls, final zzanh<TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(zzanh);
                return new StringBuilder(String.valueOf(valueOf).length() + 32 + String.valueOf(valueOf2).length()).append("Factory[typeHierarchy=").append(valueOf).append(",adapter=").append(valueOf2).append("]").toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                if (cls.isAssignableFrom(zzaol.m())) {
                    return zzanh;
                }
                return null;
            }
        };
    }

    public static <TT> zzani zzb(final Class<TT> cls, final Class<? extends TT> cls2, final zzanh<? super TT> zzanh) {
        return new zzani() {
            public String toString() {
                String valueOf = String.valueOf(cls.getName());
                String valueOf2 = String.valueOf(cls2.getName());
                String valueOf3 = String.valueOf(zzanh);
                return new StringBuilder(String.valueOf(valueOf).length() + 24 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length()).append("Factory[type=").append(valueOf).append("+").append(valueOf2).append(",adapter=").append(valueOf3).append("]").toString();
            }

            public <T> zzanh<T> zza(zzamp zzamp, zzaol<T> zzaol) {
                Class m = zzaol.m();
                if (m == cls || m == cls2) {
                    return zzanh;
                }
                return null;
            }
        };
    }
}
