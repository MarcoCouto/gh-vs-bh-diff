package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzfs.zzc;
import com.google.android.gms.internal.zzla.zza;
import com.google.android.gms.internal.zzla.zzb;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzcj extends zzcd {
    private zzc zzarq;
    /* access modifiers changed from: private */
    public boolean zzarr;

    public zzcj(Context context, AdSizeParcel adSizeParcel, zzju zzju, VersionInfoParcel versionInfoParcel, zzck zzck, zzfs zzfs) {
        super(context, adSizeParcel, zzju, versionInfoParcel, zzck);
        this.zzarq = zzfs.zzma();
        try {
            final JSONObject zzd = zzd(zzck.zzhj().zzhh());
            this.zzarq.zza(new zzla.zzc<zzft>() {
                /* renamed from: zzb */
                public void zzd(zzft zzft) {
                    zzcj.this.zza(zzd);
                }
            }, new zza() {
                public void run() {
                }
            });
        } catch (JSONException e) {
        } catch (RuntimeException e2) {
            zzkd.zzb("Failure while processing active view data.", e2);
        }
        this.zzarq.zza(new zzla.zzc<zzft>() {
            /* renamed from: zzb */
            public void zzd(zzft zzft) {
                zzcj.this.zzarr = true;
                zzcj.this.zzc(zzft);
                zzcj.this.zzgw();
                zzcj.this.zzk(3);
            }
        }, new zza() {
            public void run() {
                zzcj.this.destroy();
            }
        });
        String str = "Tracking ad unit: ";
        String valueOf = String.valueOf(this.zzaqk.zzhn());
        zzkd.zzcv(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
    }

    /* access modifiers changed from: protected */
    public void destroy() {
        synchronized (this.zzail) {
            super.destroy();
            this.zzarq.zza(new zzla.zzc<zzft>() {
                /* renamed from: zzb */
                public void zzd(zzft zzft) {
                    zzcj.this.zzd(zzft);
                }
            }, new zzb());
            this.zzarq.release();
        }
    }

    /* access modifiers changed from: protected */
    public void zzb(final JSONObject jSONObject) {
        this.zzarq.zza(new zzla.zzc<zzft>() {
            /* renamed from: zzb */
            public void zzd(zzft zzft) {
                zzft.zza("AFMA_updateActiveView", jSONObject);
            }
        }, new zzb());
    }

    /* access modifiers changed from: protected */
    public boolean zzhe() {
        return this.zzarr;
    }
}
