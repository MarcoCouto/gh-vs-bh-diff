package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.internal.zzeb.zza;

@zzin
public class zzeg extends zza {
    private final OnAppInstallAdLoadedListener zzbhi;

    public zzeg(OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
        this.zzbhi = onAppInstallAdLoadedListener;
    }

    public void zza(zzdv zzdv) {
        this.zzbhi.onAppInstallAdLoaded(zzb(zzdv));
    }

    /* access modifiers changed from: 0000 */
    public zzdw zzb(zzdv zzdv) {
        return new zzdw(zzdv);
    }
}
