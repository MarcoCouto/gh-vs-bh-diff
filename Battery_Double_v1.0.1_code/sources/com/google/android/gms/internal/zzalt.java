package com.google.android.gms.internal;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.internal.zzaly.zza;
import com.google.android.gms.internal.zzaly.zzb;
import com.google.android.gms.internal.zzaly.zzc;
import com.google.android.gms.internal.zzaly.zzd;
import com.google.android.gms.internal.zzaly.zze;
import com.google.android.gms.internal.zzaly.zzf;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class zzalt implements Runnable {
    public final zzalx bbd;
    public final zzalu bbk;
    public final zzalu bbl;
    public final zzalu bbm;
    public final Context mContext;

    public zzalt(Context context, zzalu zzalu, zzalu zzalu2, zzalu zzalu3, zzalx zzalx) {
        this.mContext = context;
        this.bbk = zzalu;
        this.bbl = zzalu2;
        this.bbm = zzalu3;
        this.bbd = zzalx;
    }

    private zza zza(zzalu zzalu) {
        zza zza = new zza();
        if (zzalu.zzcxh() != null) {
            Map zzcxh = zzalu.zzcxh();
            ArrayList arrayList = new ArrayList();
            for (String str : zzcxh.keySet()) {
                ArrayList arrayList2 = new ArrayList();
                Map map = (Map) zzcxh.get(str);
                for (String str2 : map.keySet()) {
                    zzb zzb = new zzb();
                    zzb.zzcb = str2;
                    zzb.bbw = (byte[]) map.get(str2);
                    arrayList2.add(zzb);
                }
                zzd zzd = new zzd();
                zzd.zx = str;
                zzd.bbA = (zzb[]) arrayList2.toArray(new zzb[arrayList2.size()]);
                arrayList.add(zzd);
            }
            zza.bbu = (zzd[]) arrayList.toArray(new zzd[arrayList.size()]);
        }
        zza.timestamp = zzalu.getTimestamp();
        return zza;
    }

    public void run() {
        zze zze = new zze();
        if (this.bbk != null) {
            zze.bbB = zza(this.bbk);
        }
        if (this.bbl != null) {
            zze.bbC = zza(this.bbl);
        }
        if (this.bbm != null) {
            zze.bbD = zza(this.bbm);
        }
        if (this.bbd != null) {
            zzc zzc = new zzc();
            zzc.bbx = this.bbd.getLastFetchStatus();
            zzc.bby = this.bbd.isDeveloperModeEnabled();
            zze.bbE = zzc;
        }
        if (!(this.bbd == null || this.bbd.zzcxk() == null)) {
            ArrayList arrayList = new ArrayList();
            Map zzcxk = this.bbd.zzcxk();
            for (String str : zzcxk.keySet()) {
                if (zzcxk.get(str) != null) {
                    zzf zzf = new zzf();
                    zzf.zx = str;
                    zzf.bbH = ((zzals) zzcxk.get(str)).zzcxg();
                    zzf.resourceId = ((zzals) zzcxk.get(str)).zzcxf();
                    arrayList.add(zzf);
                }
            }
            zze.bbF = (zzf[]) arrayList.toArray(new zzf[arrayList.size()]);
        }
        byte[] zzf2 = zzapv.zzf(zze);
        try {
            FileOutputStream openFileOutput = this.mContext.openFileOutput("persisted_config", 0);
            openFileOutput.write(zzf2);
            openFileOutput.close();
        } catch (IOException e) {
            Log.e("AsyncPersisterTask", "Could not persist config.", e);
        }
    }
}
