package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@zzin
public class zzgg implements zzfy {
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();
    private final zzgj zzajz;
    private final boolean zzarl;
    private final boolean zzawn;
    private final zzga zzboe;
    private final AdRequestInfoParcel zzbot;
    /* access modifiers changed from: private */
    public final long zzbou;
    /* access modifiers changed from: private */
    public final long zzbov;
    private final int zzbow;
    /* access modifiers changed from: private */
    public boolean zzbox = false;
    /* access modifiers changed from: private */
    public final Map<zzky<zzge>, zzgd> zzboy = new HashMap();
    private List<zzge> zzboz = new ArrayList();

    public zzgg(Context context, AdRequestInfoParcel adRequestInfoParcel, zzgj zzgj, zzga zzga, boolean z, boolean z2, long j, long j2, int i) {
        this.mContext = context;
        this.zzbot = adRequestInfoParcel;
        this.zzajz = zzgj;
        this.zzboe = zzga;
        this.zzarl = z;
        this.zzawn = z2;
        this.zzbou = j;
        this.zzbov = j2;
        this.zzbow = i;
    }

    private void zza(final zzky<zzge> zzky) {
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                for (zzky zzky : zzgg.this.zzboy.keySet()) {
                    if (zzky != zzky) {
                        ((zzgd) zzgg.this.zzboy.get(zzky)).cancel();
                    }
                }
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r2.hasNext() == false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r0 = (com.google.android.gms.internal.zzky) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1 = (com.google.android.gms.internal.zzge) r0.get();
        r4.zzboz.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        if (r1 == null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if (r1.zzbom != 0) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        zza(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        com.google.android.gms.internal.zzkd.zzd("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        zza(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return new com.google.android.gms.internal.zzge(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r2 = r5.iterator();
     */
    private zzge zze(List<zzky<zzge>> list) {
        synchronized (this.zzail) {
            if (this.zzbox) {
                zzge zzge = new zzge(-1);
                return zzge;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        r0 = r15.zzboe.zzbnw;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        r8 = r16.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        r6 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r8.hasNext() == false) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r0 = (com.google.android.gms.internal.zzky) r8.next();
        r10 = com.google.android.gms.ads.internal.zzu.zzfu().currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (r6 != 0) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0044, code lost:
        if (r0.isDone() == false) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0046, code lost:
        r1 = (com.google.android.gms.internal.zzge) r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r15.zzboz.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        if (r1 == null) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        if (r1.zzbom != 0) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        r5 = r1.zzbor;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        if (r5 == null) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005f, code lost:
        if (r5.zzmm() <= r4) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        r2 = r5.zzmm();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0064, code lost:
        r14 = r1;
        r1 = r0;
        r0 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0068, code lost:
        r3 = r1;
        r14 = r0;
        r0 = java.lang.Math.max(r6 - (com.google.android.gms.ads.internal.zzu.zzfu().currentTimeMillis() - r10), 0);
        r4 = r2;
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0083, code lost:
        r0 = org.altbeacon.beacon.BeaconManager.DEFAULT_BACKGROUND_SCAN_PERIOD;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r1 = (com.google.android.gms.internal.zzge) r0.get(r6, java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x008f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        com.google.android.gms.internal.zzkd.zzd("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0095, code lost:
        r0 = java.lang.Math.max(r6 - (com.google.android.gms.ads.internal.zzu.zzfu().currentTimeMillis() - r10), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a8, code lost:
        java.lang.Math.max(r6 - (com.google.android.gms.ads.internal.zzu.zzfu().currentTimeMillis() - r10), 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b9, code lost:
        zza(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bc, code lost:
        if (r2 != null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00cc, code lost:
        r0 = r2;
        r1 = r3;
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return new com.google.android.gms.internal.zzge(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r4 = -1;
        r3 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        if (r15.zzboe.zzbnw == -1) goto L_0x0083;
     */
    private zzge zzf(List<zzky<zzge>> list) {
        synchronized (this.zzail) {
            if (this.zzbox) {
                zzge zzge = new zzge(-1);
                return zzge;
            }
        }
    }

    public void cancel() {
        synchronized (this.zzail) {
            this.zzbox = true;
            for (zzgd cancel : this.zzboy.values()) {
                cancel.cancel();
            }
        }
    }

    public zzge zzd(List<zzfz> list) {
        zzkd.zzcv("Starting mediation.");
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        ArrayList arrayList = new ArrayList();
        for (zzfz zzfz : list) {
            String str = "Trying mediation network: ";
            String valueOf = String.valueOf(zzfz.zzbmv);
            zzkd.zzcw(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            for (String zzgd : zzfz.zzbmw) {
                final zzgd zzgd2 = new zzgd(this.mContext, zzgd, this.zzajz, this.zzboe, zzfz, this.zzbot.zzcar, this.zzbot.zzapa, this.zzbot.zzaow, this.zzarl, this.zzawn, this.zzbot.zzapo, this.zzbot.zzaps);
                zzky zza = zzkg.zza(newCachedThreadPool, (Callable<T>) new Callable<zzge>() {
                    /* renamed from: zzmn */
                    public zzge call() throws Exception {
                        synchronized (zzgg.this.zzail) {
                            if (zzgg.this.zzbox) {
                                return null;
                            }
                            return zzgd2.zza(zzgg.this.zzbou, zzgg.this.zzbov);
                        }
                    }
                });
                this.zzboy.put(zza, zzgd2);
                arrayList.add(zza);
            }
        }
        switch (this.zzbow) {
            case 2:
                return zzf(arrayList);
            default:
                return zze((List<zzky<zzge>>) arrayList);
        }
    }

    public List<zzge> zzmg() {
        return this.zzboz;
    }
}
