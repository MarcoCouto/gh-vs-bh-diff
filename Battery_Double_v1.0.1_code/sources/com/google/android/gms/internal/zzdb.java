package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.zze;
import java.util.concurrent.Callable;

@zzin
public class zzdb {
    private final Object zzail = new Object();
    private boolean zzamt = false;
    /* access modifiers changed from: private */
    @Nullable
    public SharedPreferences zzaxu = null;

    public void initialize(Context context) {
        synchronized (this.zzail) {
            if (!this.zzamt) {
                Context remoteContext = zze.getRemoteContext(context);
                if (remoteContext != null) {
                    this.zzaxu = zzu.zzfx().zzn(remoteContext);
                    this.zzamt = true;
                }
            }
        }
    }

    public <T> T zzd(final zzcy<T> zzcy) {
        synchronized (this.zzail) {
            if (this.zzamt) {
                return zzkt.zzb(new Callable<T>() {
                    public T call() {
                        return zzcy.zza(zzdb.this.zzaxu);
                    }
                });
            }
            T zzjw = zzcy.zzjw();
            return zzjw;
        }
    }
}
