package com.google.android.gms.internal;

import java.io.IOException;

public interface zzae {

    public static final class zza extends zzapp<zza> {
        public String zzcs;
        public String zzct;
        public Long zzcu;
        public Long zzcv;
        public Long zzcw;
        public Long zzcx;
        public Long zzcy;
        public Long zzcz;
        public Long zzda;
        public Long zzdb;
        public Long zzdc;
        public Long zzdd;
        public String zzde;
        public Long zzdf;
        public Long zzdg;
        public Long zzdh;
        public Long zzdi;
        public Long zzdj;
        public Long zzdk;
        public Long zzdl;
        public Long zzdm;
        public Long zzdn;
        public String zzdo;
        public String zzdp;
        public Long zzdq;
        public Long zzdr;
        public Long zzds;
        public String zzdt;
        public Long zzdu;
        public Long zzdv;
        public Long zzdw;
        public zzb zzdx;
        public Long zzdy;
        public Long zzdz;
        public Long zzea;
        public Long zzeb;
        public Long zzec;
        public Long zzed;
        public C0049zza[] zzee;
        public Long zzef;
        public String zzeg;
        public Integer zzeh;
        public Boolean zzei;
        public String zzej;
        public Long zzek;
        public zze zzel;

        /* renamed from: com.google.android.gms.internal.zzae$zza$zza reason: collision with other inner class name */
        public static final class C0049zza extends zzapp<C0049zza> {
            private static volatile C0049zza[] zzem;
            public Long zzdf;
            public Long zzdg;

            public C0049zza() {
                this.zzdf = null;
                this.zzdg = null;
                this.bjG = -1;
            }

            public static C0049zza[] zzy() {
                if (zzem == null) {
                    synchronized (zzapt.bjF) {
                        if (zzem == null) {
                            zzem = new C0049zza[0];
                        }
                    }
                }
                return zzem;
            }

            public void zza(zzapo zzapo) throws IOException {
                if (this.zzdf != null) {
                    zzapo.zzb(1, this.zzdf.longValue());
                }
                if (this.zzdg != null) {
                    zzapo.zzb(2, this.zzdg.longValue());
                }
                super.zza(zzapo);
            }

            /* renamed from: zzd */
            public C0049zza zzb(zzapn zzapn) throws IOException {
                while (true) {
                    int ah = zzapn.ah();
                    switch (ah) {
                        case 0:
                            break;
                        case 8:
                            this.zzdf = Long.valueOf(zzapn.ak());
                            continue;
                        case 16:
                            this.zzdg = Long.valueOf(zzapn.ak());
                            continue;
                        default:
                            if (!super.zza(zzapn, ah)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
                return this;
            }

            /* access modifiers changed from: protected */
            public int zzx() {
                int zzx = super.zzx();
                if (this.zzdf != null) {
                    zzx += zzapo.zze(1, this.zzdf.longValue());
                }
                return this.zzdg != null ? zzx + zzapo.zze(2, this.zzdg.longValue()) : zzx;
            }
        }

        public zza() {
            this.zzct = null;
            this.zzcs = null;
            this.zzcu = null;
            this.zzcv = null;
            this.zzcw = null;
            this.zzcx = null;
            this.zzcy = null;
            this.zzcz = null;
            this.zzda = null;
            this.zzdb = null;
            this.zzdc = null;
            this.zzdd = null;
            this.zzde = null;
            this.zzdf = null;
            this.zzdg = null;
            this.zzdh = null;
            this.zzdi = null;
            this.zzdj = null;
            this.zzdk = null;
            this.zzdl = null;
            this.zzdm = null;
            this.zzdn = null;
            this.zzdo = null;
            this.zzdp = null;
            this.zzdq = null;
            this.zzdr = null;
            this.zzds = null;
            this.zzdt = null;
            this.zzdu = null;
            this.zzdv = null;
            this.zzdw = null;
            this.zzdy = null;
            this.zzdz = null;
            this.zzea = null;
            this.zzeb = null;
            this.zzec = null;
            this.zzed = null;
            this.zzee = C0049zza.zzy();
            this.zzef = null;
            this.zzeg = null;
            this.zzeh = null;
            this.zzei = null;
            this.zzej = null;
            this.zzek = null;
            this.bjG = -1;
        }

        public static zza zzc(byte[] bArr) throws zzapu {
            return (zza) zzapv.zza(new zza(), bArr);
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzct != null) {
                zzapo.zzr(1, this.zzct);
            }
            if (this.zzcs != null) {
                zzapo.zzr(2, this.zzcs);
            }
            if (this.zzcu != null) {
                zzapo.zzb(3, this.zzcu.longValue());
            }
            if (this.zzcv != null) {
                zzapo.zzb(4, this.zzcv.longValue());
            }
            if (this.zzcw != null) {
                zzapo.zzb(5, this.zzcw.longValue());
            }
            if (this.zzcx != null) {
                zzapo.zzb(6, this.zzcx.longValue());
            }
            if (this.zzcy != null) {
                zzapo.zzb(7, this.zzcy.longValue());
            }
            if (this.zzcz != null) {
                zzapo.zzb(8, this.zzcz.longValue());
            }
            if (this.zzda != null) {
                zzapo.zzb(9, this.zzda.longValue());
            }
            if (this.zzdb != null) {
                zzapo.zzb(10, this.zzdb.longValue());
            }
            if (this.zzdc != null) {
                zzapo.zzb(11, this.zzdc.longValue());
            }
            if (this.zzdd != null) {
                zzapo.zzb(12, this.zzdd.longValue());
            }
            if (this.zzde != null) {
                zzapo.zzr(13, this.zzde);
            }
            if (this.zzdf != null) {
                zzapo.zzb(14, this.zzdf.longValue());
            }
            if (this.zzdg != null) {
                zzapo.zzb(15, this.zzdg.longValue());
            }
            if (this.zzdh != null) {
                zzapo.zzb(16, this.zzdh.longValue());
            }
            if (this.zzdi != null) {
                zzapo.zzb(17, this.zzdi.longValue());
            }
            if (this.zzdj != null) {
                zzapo.zzb(18, this.zzdj.longValue());
            }
            if (this.zzdk != null) {
                zzapo.zzb(19, this.zzdk.longValue());
            }
            if (this.zzdl != null) {
                zzapo.zzb(20, this.zzdl.longValue());
            }
            if (this.zzef != null) {
                zzapo.zzb(21, this.zzef.longValue());
            }
            if (this.zzdm != null) {
                zzapo.zzb(22, this.zzdm.longValue());
            }
            if (this.zzdn != null) {
                zzapo.zzb(23, this.zzdn.longValue());
            }
            if (this.zzeg != null) {
                zzapo.zzr(24, this.zzeg);
            }
            if (this.zzek != null) {
                zzapo.zzb(25, this.zzek.longValue());
            }
            if (this.zzeh != null) {
                zzapo.zzae(26, this.zzeh.intValue());
            }
            if (this.zzdo != null) {
                zzapo.zzr(27, this.zzdo);
            }
            if (this.zzei != null) {
                zzapo.zzj(28, this.zzei.booleanValue());
            }
            if (this.zzdp != null) {
                zzapo.zzr(29, this.zzdp);
            }
            if (this.zzej != null) {
                zzapo.zzr(30, this.zzej);
            }
            if (this.zzdq != null) {
                zzapo.zzb(31, this.zzdq.longValue());
            }
            if (this.zzdr != null) {
                zzapo.zzb(32, this.zzdr.longValue());
            }
            if (this.zzds != null) {
                zzapo.zzb(33, this.zzds.longValue());
            }
            if (this.zzdt != null) {
                zzapo.zzr(34, this.zzdt);
            }
            if (this.zzdu != null) {
                zzapo.zzb(35, this.zzdu.longValue());
            }
            if (this.zzdv != null) {
                zzapo.zzb(36, this.zzdv.longValue());
            }
            if (this.zzdw != null) {
                zzapo.zzb(37, this.zzdw.longValue());
            }
            if (this.zzdx != null) {
                zzapo.zza(38, (zzapv) this.zzdx);
            }
            if (this.zzdy != null) {
                zzapo.zzb(39, this.zzdy.longValue());
            }
            if (this.zzdz != null) {
                zzapo.zzb(40, this.zzdz.longValue());
            }
            if (this.zzea != null) {
                zzapo.zzb(41, this.zzea.longValue());
            }
            if (this.zzeb != null) {
                zzapo.zzb(42, this.zzeb.longValue());
            }
            if (this.zzee != null && this.zzee.length > 0) {
                for (C0049zza zza : this.zzee) {
                    if (zza != null) {
                        zzapo.zza(43, (zzapv) zza);
                    }
                }
            }
            if (this.zzec != null) {
                zzapo.zzb(44, this.zzec.longValue());
            }
            if (this.zzed != null) {
                zzapo.zzb(45, this.zzed.longValue());
            }
            if (this.zzel != null) {
                zzapo.zza(201, (zzapv) this.zzel);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzc */
        public zza zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 10:
                        this.zzct = zzapn.readString();
                        continue;
                    case 18:
                        this.zzcs = zzapn.readString();
                        continue;
                    case 24:
                        this.zzcu = Long.valueOf(zzapn.ak());
                        continue;
                    case 32:
                        this.zzcv = Long.valueOf(zzapn.ak());
                        continue;
                    case 40:
                        this.zzcw = Long.valueOf(zzapn.ak());
                        continue;
                    case 48:
                        this.zzcx = Long.valueOf(zzapn.ak());
                        continue;
                    case 56:
                        this.zzcy = Long.valueOf(zzapn.ak());
                        continue;
                    case 64:
                        this.zzcz = Long.valueOf(zzapn.ak());
                        continue;
                    case 72:
                        this.zzda = Long.valueOf(zzapn.ak());
                        continue;
                    case 80:
                        this.zzdb = Long.valueOf(zzapn.ak());
                        continue;
                    case 88:
                        this.zzdc = Long.valueOf(zzapn.ak());
                        continue;
                    case 96:
                        this.zzdd = Long.valueOf(zzapn.ak());
                        continue;
                    case 106:
                        this.zzde = zzapn.readString();
                        continue;
                    case 112:
                        this.zzdf = Long.valueOf(zzapn.ak());
                        continue;
                    case 120:
                        this.zzdg = Long.valueOf(zzapn.ak());
                        continue;
                    case 128:
                        this.zzdh = Long.valueOf(zzapn.ak());
                        continue;
                    case 136:
                        this.zzdi = Long.valueOf(zzapn.ak());
                        continue;
                    case 144:
                        this.zzdj = Long.valueOf(zzapn.ak());
                        continue;
                    case 152:
                        this.zzdk = Long.valueOf(zzapn.ak());
                        continue;
                    case 160:
                        this.zzdl = Long.valueOf(zzapn.ak());
                        continue;
                    case 168:
                        this.zzef = Long.valueOf(zzapn.ak());
                        continue;
                    case 176:
                        this.zzdm = Long.valueOf(zzapn.ak());
                        continue;
                    case 184:
                        this.zzdn = Long.valueOf(zzapn.ak());
                        continue;
                    case 194:
                        this.zzeg = zzapn.readString();
                        continue;
                    case 200:
                        this.zzek = Long.valueOf(zzapn.ak());
                        continue;
                    case 208:
                        int al = zzapn.al();
                        switch (al) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                this.zzeh = Integer.valueOf(al);
                                break;
                            default:
                                continue;
                        }
                    case 218:
                        this.zzdo = zzapn.readString();
                        continue;
                    case 224:
                        this.zzei = Boolean.valueOf(zzapn.an());
                        continue;
                    case 234:
                        this.zzdp = zzapn.readString();
                        continue;
                    case 242:
                        this.zzej = zzapn.readString();
                        continue;
                    case 248:
                        this.zzdq = Long.valueOf(zzapn.ak());
                        continue;
                    case 256:
                        this.zzdr = Long.valueOf(zzapn.ak());
                        continue;
                    case 264:
                        this.zzds = Long.valueOf(zzapn.ak());
                        continue;
                    case 274:
                        this.zzdt = zzapn.readString();
                        continue;
                    case 280:
                        this.zzdu = Long.valueOf(zzapn.ak());
                        continue;
                    case 288:
                        this.zzdv = Long.valueOf(zzapn.ak());
                        continue;
                    case 296:
                        this.zzdw = Long.valueOf(zzapn.ak());
                        continue;
                    case 306:
                        if (this.zzdx == null) {
                            this.zzdx = new zzb();
                        }
                        zzapn.zza(this.zzdx);
                        continue;
                    case 312:
                        this.zzdy = Long.valueOf(zzapn.ak());
                        continue;
                    case 320:
                        this.zzdz = Long.valueOf(zzapn.ak());
                        continue;
                    case 328:
                        this.zzea = Long.valueOf(zzapn.ak());
                        continue;
                    case 336:
                        this.zzeb = Long.valueOf(zzapn.ak());
                        continue;
                    case 346:
                        int zzc = zzapy.zzc(zzapn, 346);
                        int length = this.zzee == null ? 0 : this.zzee.length;
                        C0049zza[] zzaArr = new C0049zza[(zzc + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzee, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new C0049zza();
                            zzapn.zza(zzaArr[length]);
                            zzapn.ah();
                            length++;
                        }
                        zzaArr[length] = new C0049zza();
                        zzapn.zza(zzaArr[length]);
                        this.zzee = zzaArr;
                        continue;
                    case 352:
                        this.zzec = Long.valueOf(zzapn.ak());
                        continue;
                    case 360:
                        this.zzed = Long.valueOf(zzapn.ak());
                        continue;
                    case 1610:
                        if (this.zzel == null) {
                            this.zzel = new zze();
                        }
                        zzapn.zza(this.zzel);
                        continue;
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzct != null) {
                zzx += zzapo.zzs(1, this.zzct);
            }
            if (this.zzcs != null) {
                zzx += zzapo.zzs(2, this.zzcs);
            }
            if (this.zzcu != null) {
                zzx += zzapo.zze(3, this.zzcu.longValue());
            }
            if (this.zzcv != null) {
                zzx += zzapo.zze(4, this.zzcv.longValue());
            }
            if (this.zzcw != null) {
                zzx += zzapo.zze(5, this.zzcw.longValue());
            }
            if (this.zzcx != null) {
                zzx += zzapo.zze(6, this.zzcx.longValue());
            }
            if (this.zzcy != null) {
                zzx += zzapo.zze(7, this.zzcy.longValue());
            }
            if (this.zzcz != null) {
                zzx += zzapo.zze(8, this.zzcz.longValue());
            }
            if (this.zzda != null) {
                zzx += zzapo.zze(9, this.zzda.longValue());
            }
            if (this.zzdb != null) {
                zzx += zzapo.zze(10, this.zzdb.longValue());
            }
            if (this.zzdc != null) {
                zzx += zzapo.zze(11, this.zzdc.longValue());
            }
            if (this.zzdd != null) {
                zzx += zzapo.zze(12, this.zzdd.longValue());
            }
            if (this.zzde != null) {
                zzx += zzapo.zzs(13, this.zzde);
            }
            if (this.zzdf != null) {
                zzx += zzapo.zze(14, this.zzdf.longValue());
            }
            if (this.zzdg != null) {
                zzx += zzapo.zze(15, this.zzdg.longValue());
            }
            if (this.zzdh != null) {
                zzx += zzapo.zze(16, this.zzdh.longValue());
            }
            if (this.zzdi != null) {
                zzx += zzapo.zze(17, this.zzdi.longValue());
            }
            if (this.zzdj != null) {
                zzx += zzapo.zze(18, this.zzdj.longValue());
            }
            if (this.zzdk != null) {
                zzx += zzapo.zze(19, this.zzdk.longValue());
            }
            if (this.zzdl != null) {
                zzx += zzapo.zze(20, this.zzdl.longValue());
            }
            if (this.zzef != null) {
                zzx += zzapo.zze(21, this.zzef.longValue());
            }
            if (this.zzdm != null) {
                zzx += zzapo.zze(22, this.zzdm.longValue());
            }
            if (this.zzdn != null) {
                zzx += zzapo.zze(23, this.zzdn.longValue());
            }
            if (this.zzeg != null) {
                zzx += zzapo.zzs(24, this.zzeg);
            }
            if (this.zzek != null) {
                zzx += zzapo.zze(25, this.zzek.longValue());
            }
            if (this.zzeh != null) {
                zzx += zzapo.zzag(26, this.zzeh.intValue());
            }
            if (this.zzdo != null) {
                zzx += zzapo.zzs(27, this.zzdo);
            }
            if (this.zzei != null) {
                zzx += zzapo.zzk(28, this.zzei.booleanValue());
            }
            if (this.zzdp != null) {
                zzx += zzapo.zzs(29, this.zzdp);
            }
            if (this.zzej != null) {
                zzx += zzapo.zzs(30, this.zzej);
            }
            if (this.zzdq != null) {
                zzx += zzapo.zze(31, this.zzdq.longValue());
            }
            if (this.zzdr != null) {
                zzx += zzapo.zze(32, this.zzdr.longValue());
            }
            if (this.zzds != null) {
                zzx += zzapo.zze(33, this.zzds.longValue());
            }
            if (this.zzdt != null) {
                zzx += zzapo.zzs(34, this.zzdt);
            }
            if (this.zzdu != null) {
                zzx += zzapo.zze(35, this.zzdu.longValue());
            }
            if (this.zzdv != null) {
                zzx += zzapo.zze(36, this.zzdv.longValue());
            }
            if (this.zzdw != null) {
                zzx += zzapo.zze(37, this.zzdw.longValue());
            }
            if (this.zzdx != null) {
                zzx += zzapo.zzc(38, (zzapv) this.zzdx);
            }
            if (this.zzdy != null) {
                zzx += zzapo.zze(39, this.zzdy.longValue());
            }
            if (this.zzdz != null) {
                zzx += zzapo.zze(40, this.zzdz.longValue());
            }
            if (this.zzea != null) {
                zzx += zzapo.zze(41, this.zzea.longValue());
            }
            if (this.zzeb != null) {
                zzx += zzapo.zze(42, this.zzeb.longValue());
            }
            if (this.zzee != null && this.zzee.length > 0) {
                int i = zzx;
                for (C0049zza zza : this.zzee) {
                    if (zza != null) {
                        i += zzapo.zzc(43, (zzapv) zza);
                    }
                }
                zzx = i;
            }
            if (this.zzec != null) {
                zzx += zzapo.zze(44, this.zzec.longValue());
            }
            if (this.zzed != null) {
                zzx += zzapo.zze(45, this.zzed.longValue());
            }
            return this.zzel != null ? zzx + zzapo.zzc(201, (zzapv) this.zzel) : zzx;
        }
    }

    public static final class zzb extends zzapp<zzb> {
        public Long zzen;
        public Integer zzeo;
        public Boolean zzep;
        public int[] zzeq;

        public zzb() {
            this.zzen = null;
            this.zzeo = null;
            this.zzep = null;
            this.zzeq = zzapy.bjH;
            this.bjG = -1;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzen != null) {
                zzapo.zzb(1, this.zzen.longValue());
            }
            if (this.zzeo != null) {
                zzapo.zzae(2, this.zzeo.intValue());
            }
            if (this.zzep != null) {
                zzapo.zzj(3, this.zzep.booleanValue());
            }
            if (this.zzeq != null && this.zzeq.length > 0) {
                for (int zzae : this.zzeq) {
                    zzapo.zzae(4, zzae);
                }
            }
            super.zza(zzapo);
        }

        /* renamed from: zze */
        public zzb zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 8:
                        this.zzen = Long.valueOf(zzapn.ak());
                        continue;
                    case 16:
                        this.zzeo = Integer.valueOf(zzapn.al());
                        continue;
                    case 24:
                        this.zzep = Boolean.valueOf(zzapn.an());
                        continue;
                    case 32:
                        int zzc = zzapy.zzc(zzapn, 32);
                        int length = this.zzeq == null ? 0 : this.zzeq.length;
                        int[] iArr = new int[(zzc + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzeq, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = zzapn.al();
                            zzapn.ah();
                            length++;
                        }
                        iArr[length] = zzapn.al();
                        this.zzeq = iArr;
                        continue;
                    case 34:
                        int zzafr = zzapn.zzafr(zzapn.aq());
                        int position = zzapn.getPosition();
                        int i = 0;
                        while (zzapn.av() > 0) {
                            zzapn.al();
                            i++;
                        }
                        zzapn.zzaft(position);
                        int length2 = this.zzeq == null ? 0 : this.zzeq.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzeq, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = zzapn.al();
                            length2++;
                        }
                        this.zzeq = iArr2;
                        zzapn.zzafs(zzafr);
                        continue;
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzen != null) {
                zzx += zzapo.zze(1, this.zzen.longValue());
            }
            if (this.zzeo != null) {
                zzx += zzapo.zzag(2, this.zzeo.intValue());
            }
            if (this.zzep != null) {
                zzx += zzapo.zzk(3, this.zzep.booleanValue());
            }
            if (this.zzeq == null || this.zzeq.length <= 0) {
                return zzx;
            }
            int i = 0;
            for (int zzafx : this.zzeq) {
                i += zzapo.zzafx(zzafx);
            }
            return zzx + i + (this.zzeq.length * 1);
        }
    }

    public static final class zzc extends zzapp<zzc> {
        public byte[] zzer;
        public byte[] zzes;

        public zzc() {
            this.zzer = null;
            this.zzes = null;
            this.bjG = -1;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzer != null) {
                zzapo.zza(1, this.zzer);
            }
            if (this.zzes != null) {
                zzapo.zza(2, this.zzes);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzf */
        public zzc zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 10:
                        this.zzer = zzapn.readBytes();
                        continue;
                    case 18:
                        this.zzes = zzapn.readBytes();
                        continue;
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzer != null) {
                zzx += zzapo.zzb(1, this.zzer);
            }
            return this.zzes != null ? zzx + zzapo.zzb(2, this.zzes) : zzx;
        }
    }

    public static final class zzd extends zzapp<zzd> {
        public byte[] data;
        public byte[] zzet;
        public byte[] zzeu;
        public byte[] zzev;

        public zzd() {
            this.data = null;
            this.zzet = null;
            this.zzeu = null;
            this.zzev = null;
            this.bjG = -1;
        }

        public static zzd zzd(byte[] bArr) throws zzapu {
            return (zzd) zzapv.zza(new zzd(), bArr);
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.data != null) {
                zzapo.zza(1, this.data);
            }
            if (this.zzet != null) {
                zzapo.zza(2, this.zzet);
            }
            if (this.zzeu != null) {
                zzapo.zza(3, this.zzeu);
            }
            if (this.zzev != null) {
                zzapo.zza(4, this.zzev);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzg */
        public zzd zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 10:
                        this.data = zzapn.readBytes();
                        continue;
                    case 18:
                        this.zzet = zzapn.readBytes();
                        continue;
                    case 26:
                        this.zzeu = zzapn.readBytes();
                        continue;
                    case 34:
                        this.zzev = zzapn.readBytes();
                        continue;
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.data != null) {
                zzx += zzapo.zzb(1, this.data);
            }
            if (this.zzet != null) {
                zzx += zzapo.zzb(2, this.zzet);
            }
            if (this.zzeu != null) {
                zzx += zzapo.zzb(3, this.zzeu);
            }
            return this.zzev != null ? zzx + zzapo.zzb(4, this.zzev) : zzx;
        }
    }

    public static final class zze extends zzapp<zze> {
        public Long zzen;
        public String zzew;
        public byte[] zzex;

        public zze() {
            this.zzen = null;
            this.zzew = null;
            this.zzex = null;
            this.bjG = -1;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzen != null) {
                zzapo.zzb(1, this.zzen.longValue());
            }
            if (this.zzew != null) {
                zzapo.zzr(3, this.zzew);
            }
            if (this.zzex != null) {
                zzapo.zza(4, this.zzex);
            }
            super.zza(zzapo);
        }

        /* renamed from: zzh */
        public zze zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 8:
                        this.zzen = Long.valueOf(zzapn.ak());
                        continue;
                    case 26:
                        this.zzew = zzapn.readString();
                        continue;
                    case 34:
                        this.zzex = zzapn.readBytes();
                        continue;
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int zzx = super.zzx();
            if (this.zzen != null) {
                zzx += zzapo.zze(1, this.zzen.longValue());
            }
            if (this.zzew != null) {
                zzx += zzapo.zzs(3, this.zzew);
            }
            return this.zzex != null ? zzx + zzapo.zzb(4, this.zzex) : zzx;
        }
    }

    public static final class zzf extends zzapp<zzf> {
        public byte[] zzet;
        public byte[][] zzey;
        public Integer zzez;
        public Integer zzfa;

        public zzf() {
            this.zzey = zzapy.bjN;
            this.zzet = null;
            this.zzez = null;
            this.zzfa = null;
            this.bjG = -1;
        }

        public void zza(zzapo zzapo) throws IOException {
            if (this.zzey != null && this.zzey.length > 0) {
                for (byte[] bArr : this.zzey) {
                    if (bArr != null) {
                        zzapo.zza(1, bArr);
                    }
                }
            }
            if (this.zzet != null) {
                zzapo.zza(2, this.zzet);
            }
            if (this.zzez != null) {
                zzapo.zzae(3, this.zzez.intValue());
            }
            if (this.zzfa != null) {
                zzapo.zzae(4, this.zzfa.intValue());
            }
            super.zza(zzapo);
        }

        /* renamed from: zzi */
        public zzf zzb(zzapn zzapn) throws IOException {
            while (true) {
                int ah = zzapn.ah();
                switch (ah) {
                    case 0:
                        break;
                    case 10:
                        int zzc = zzapy.zzc(zzapn, 10);
                        int length = this.zzey == null ? 0 : this.zzey.length;
                        byte[][] bArr = new byte[(zzc + length)][];
                        if (length != 0) {
                            System.arraycopy(this.zzey, 0, bArr, 0, length);
                        }
                        while (length < bArr.length - 1) {
                            bArr[length] = zzapn.readBytes();
                            zzapn.ah();
                            length++;
                        }
                        bArr[length] = zzapn.readBytes();
                        this.zzey = bArr;
                        continue;
                    case 18:
                        this.zzet = zzapn.readBytes();
                        continue;
                    case 24:
                        int al = zzapn.al();
                        switch (al) {
                            case 0:
                            case 1:
                                this.zzez = Integer.valueOf(al);
                                break;
                            default:
                                continue;
                        }
                    case 32:
                        int al2 = zzapn.al();
                        switch (al2) {
                            case 0:
                            case 1:
                                this.zzfa = Integer.valueOf(al2);
                                break;
                            default:
                                continue;
                        }
                    default:
                        if (!super.zza(zzapn, ah)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzx() {
            int i;
            int zzx = super.zzx();
            if (this.zzey == null || this.zzey.length <= 0) {
                i = zzx;
            } else {
                int i2 = 0;
                int i3 = 0;
                for (byte[] bArr : this.zzey) {
                    if (bArr != null) {
                        i3++;
                        i2 += zzapo.zzbg(bArr);
                    }
                }
                i = zzx + i2 + (i3 * 1);
            }
            if (this.zzet != null) {
                i += zzapo.zzb(2, this.zzet);
            }
            if (this.zzez != null) {
                i += zzapo.zzag(3, this.zzez.intValue());
            }
            return this.zzfa != null ? i + zzapo.zzag(4, this.zzfa.intValue()) : i;
        }
    }
}
