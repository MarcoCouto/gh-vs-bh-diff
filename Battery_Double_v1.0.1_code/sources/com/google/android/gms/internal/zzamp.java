package com.google.android.gms.internal;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzamp {
    private final ThreadLocal<Map<zzaol<?>, zza<?>>> bdO;
    private final Map<zzaol<?>, zzanh<?>> bdP;
    private final List<zzani> bdQ;
    private final zzanp bdR;
    private final boolean bdS;
    private final boolean bdT;
    private final boolean bdU;
    private final boolean bdV;
    final zzamt bdW;
    final zzanc bdX;

    static class zza<T> extends zzanh<T> {
        private zzanh<T> bdZ;

        zza() {
        }

        public void zza(zzanh<T> zzanh) {
            if (this.bdZ != null) {
                throw new AssertionError();
            }
            this.bdZ = zzanh;
        }

        public void zza(zzaoo zzaoo, T t) throws IOException {
            if (this.bdZ == null) {
                throw new IllegalStateException();
            }
            this.bdZ.zza(zzaoo, t);
        }

        public T zzb(zzaom zzaom) throws IOException {
            if (this.bdZ != null) {
                return this.bdZ.zzb(zzaom);
            }
            throw new IllegalStateException();
        }
    }

    public zzamp() {
        this(zzanq.beK, zzamn.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, zzanf.DEFAULT, Collections.emptyList());
    }

    zzamp(zzanq zzanq, zzamo zzamo, Map<Type, zzamr<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, zzanf zzanf, List<zzani> list) {
        this.bdO = new ThreadLocal<>();
        this.bdP = Collections.synchronizedMap(new HashMap());
        this.bdW = new zzamt() {
        };
        this.bdX = new zzanc() {
        };
        this.bdR = new zzanp(map);
        this.bdS = z;
        this.bdU = z3;
        this.bdT = z4;
        this.bdV = z5;
        ArrayList arrayList = new ArrayList();
        arrayList.add(zzaok.bgN);
        arrayList.add(zzaof.bfu);
        arrayList.add(zzanq);
        arrayList.addAll(list);
        arrayList.add(zzaok.bgu);
        arrayList.add(zzaok.bgj);
        arrayList.add(zzaok.bgd);
        arrayList.add(zzaok.bgf);
        arrayList.add(zzaok.bgh);
        arrayList.add(zzaok.zza(Long.TYPE, Long.class, zza(zzanf)));
        arrayList.add(zzaok.zza(Double.TYPE, Double.class, zzcy(z6)));
        arrayList.add(zzaok.zza(Float.TYPE, Float.class, zzcz(z6)));
        arrayList.add(zzaok.bgo);
        arrayList.add(zzaok.bgq);
        arrayList.add(zzaok.bgw);
        arrayList.add(zzaok.bgy);
        arrayList.add(zzaok.zza(BigDecimal.class, zzaok.bgs));
        arrayList.add(zzaok.zza(BigInteger.class, zzaok.bgt));
        arrayList.add(zzaok.bgA);
        arrayList.add(zzaok.bgC);
        arrayList.add(zzaok.bgG);
        arrayList.add(zzaok.bgL);
        arrayList.add(zzaok.bgE);
        arrayList.add(zzaok.bga);
        arrayList.add(zzaoa.bfu);
        arrayList.add(zzaok.bgJ);
        arrayList.add(zzaoi.bfu);
        arrayList.add(zzaoh.bfu);
        arrayList.add(zzaok.bgH);
        arrayList.add(zzany.bfu);
        arrayList.add(zzaok.bfY);
        arrayList.add(new zzanz(this.bdR));
        arrayList.add(new zzaoe(this.bdR, z2));
        arrayList.add(new zzaob(this.bdR));
        arrayList.add(zzaok.bgO);
        arrayList.add(new zzaog(this.bdR, zzamo, zzanq));
        this.bdQ = Collections.unmodifiableList(arrayList);
    }

    private zzanh<Number> zza(zzanf zzanf) {
        return zzanf == zzanf.DEFAULT ? zzaok.bgk : new zzanh<Number>() {
            public void zza(zzaoo zzaoo, Number number) throws IOException {
                if (number == null) {
                    zzaoo.l();
                } else {
                    zzaoo.zzts(number.toString());
                }
            }

            /* renamed from: zzg */
            public Number zzb(zzaom zzaom) throws IOException {
                if (zzaom.b() != zzaon.NULL) {
                    return Long.valueOf(zzaom.nextLong());
                }
                zzaom.nextNull();
                return null;
            }
        };
    }

    private static void zza(Object obj, zzaom zzaom) {
        if (obj != null) {
            try {
                if (zzaom.b() != zzaon.END_DOCUMENT) {
                    throw new zzamw("JSON document was not fully consumed.");
                }
            } catch (zzaop e) {
                throw new zzane((Throwable) e);
            } catch (IOException e2) {
                throw new zzamw((Throwable) e2);
            }
        }
    }

    private zzanh<Number> zzcy(boolean z) {
        return z ? zzaok.bgm : new zzanh<Number>() {
            public void zza(zzaoo zzaoo, Number number) throws IOException {
                if (number == null) {
                    zzaoo.l();
                    return;
                }
                zzamp.this.zzn(number.doubleValue());
                zzaoo.zza(number);
            }

            /* renamed from: zze */
            public Double zzb(zzaom zzaom) throws IOException {
                if (zzaom.b() != zzaon.NULL) {
                    return Double.valueOf(zzaom.nextDouble());
                }
                zzaom.nextNull();
                return null;
            }
        };
    }

    private zzanh<Number> zzcz(boolean z) {
        return z ? zzaok.bgl : new zzanh<Number>() {
            public void zza(zzaoo zzaoo, Number number) throws IOException {
                if (number == null) {
                    zzaoo.l();
                    return;
                }
                zzamp.this.zzn((double) number.floatValue());
                zzaoo.zza(number);
            }

            /* renamed from: zzf */
            public Float zzb(zzaom zzaom) throws IOException {
                if (zzaom.b() != zzaon.NULL) {
                    return Float.valueOf((float) zzaom.nextDouble());
                }
                zzaom.nextNull();
                return null;
            }
        };
    }

    /* access modifiers changed from: private */
    public void zzn(double d) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            throw new IllegalArgumentException(d + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.bdS + "factories:" + this.bdQ + ",instanceCreators:" + this.bdR + "}";
    }

    public <T> zzanh<T> zza(zzani zzani, zzaol<T> zzaol) {
        boolean z = false;
        if (!this.bdQ.contains(zzani)) {
            z = true;
        }
        boolean z2 = z;
        for (zzani zzani2 : this.bdQ) {
            if (z2) {
                zzanh<T> zza2 = zzani2.zza(this, zzaol);
                if (zza2 != null) {
                    return zza2;
                }
            } else if (zzani2 == zzani) {
                z2 = true;
            }
        }
        String valueOf = String.valueOf(zzaol);
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 22).append("GSON cannot serialize ").append(valueOf).toString());
    }

    public <T> zzanh<T> zza(zzaol<T> zzaol) {
        Map map;
        zzanh<T> zzanh = (zzanh) this.bdP.get(zzaol);
        if (zzanh == null) {
            Map map2 = (Map) this.bdO.get();
            boolean z = false;
            if (map2 == null) {
                HashMap hashMap = new HashMap();
                this.bdO.set(hashMap);
                map = hashMap;
                z = true;
            } else {
                map = map2;
            }
            zzanh = (zza) map.get(zzaol);
            if (zzanh == null) {
                try {
                    zza zza2 = new zza();
                    map.put(zzaol, zza2);
                    for (zzani zza3 : this.bdQ) {
                        zzanh = zza3.zza(this, zzaol);
                        if (zzanh != null) {
                            zza2.zza(zzanh);
                            this.bdP.put(zzaol, zzanh);
                            map.remove(zzaol);
                            if (z) {
                                this.bdO.remove();
                            }
                        }
                    }
                    String valueOf = String.valueOf(zzaol);
                    throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 19).append("GSON cannot handle ").append(valueOf).toString());
                } catch (Throwable th) {
                    map.remove(zzaol);
                    if (z) {
                        this.bdO.remove();
                    }
                    throw th;
                }
            }
        }
        return zzanh;
    }

    public zzaoo zza(Writer writer) throws IOException {
        if (this.bdU) {
            writer.write(")]}'\n");
        }
        zzaoo zzaoo = new zzaoo(writer);
        if (this.bdV) {
            zzaoo.setIndent("  ");
        }
        zzaoo.zzdd(this.bdS);
        return zzaoo;
    }

    public <T> T zza(zzamv zzamv, Class<T> cls) throws zzane {
        return zzanv.zzp(cls).cast(zza(zzamv, (Type) cls));
    }

    public <T> T zza(zzamv zzamv, Type type) throws zzane {
        if (zzamv == null) {
            return null;
        }
        return zza((zzaom) new zzaoc(zzamv), type);
    }

    public <T> T zza(zzaom zzaom, Type type) throws zzamw, zzane {
        boolean z = true;
        boolean isLenient = zzaom.isLenient();
        zzaom.setLenient(true);
        try {
            zzaom.b();
            z = false;
            T zzb = zza(zzaol.zzl(type)).zzb(zzaom);
            zzaom.setLenient(isLenient);
            return zzb;
        } catch (EOFException e) {
            if (z) {
                zzaom.setLenient(isLenient);
                return null;
            }
            throw new zzane((Throwable) e);
        } catch (IllegalStateException e2) {
            throw new zzane((Throwable) e2);
        } catch (IOException e3) {
            throw new zzane((Throwable) e3);
        } catch (Throwable th) {
            zzaom.setLenient(isLenient);
            throw th;
        }
    }

    public <T> T zza(Reader reader, Type type) throws zzamw, zzane {
        zzaom zzaom = new zzaom(reader);
        T zza2 = zza(zzaom, type);
        zza((Object) zza2, zzaom);
        return zza2;
    }

    public <T> T zza(String str, Type type) throws zzane {
        if (str == null) {
            return null;
        }
        return zza((Reader) new StringReader(str), type);
    }

    public void zza(zzamv zzamv, zzaoo zzaoo) throws zzamw {
        boolean isLenient = zzaoo.isLenient();
        zzaoo.setLenient(true);
        boolean x = zzaoo.x();
        zzaoo.zzdc(this.bdT);
        boolean y = zzaoo.y();
        zzaoo.zzdd(this.bdS);
        try {
            zzanw.zzb(zzamv, zzaoo);
            zzaoo.setLenient(isLenient);
            zzaoo.zzdc(x);
            zzaoo.zzdd(y);
        } catch (IOException e) {
            throw new zzamw((Throwable) e);
        } catch (Throwable th) {
            zzaoo.setLenient(isLenient);
            zzaoo.zzdc(x);
            zzaoo.zzdd(y);
            throw th;
        }
    }

    public void zza(zzamv zzamv, Appendable appendable) throws zzamw {
        try {
            zza(zzamv, zza(zzanw.zza(appendable)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void zza(Object obj, Type type, zzaoo zzaoo) throws zzamw {
        zzanh zza2 = zza(zzaol.zzl(type));
        boolean isLenient = zzaoo.isLenient();
        zzaoo.setLenient(true);
        boolean x = zzaoo.x();
        zzaoo.zzdc(this.bdT);
        boolean y = zzaoo.y();
        zzaoo.zzdd(this.bdS);
        try {
            zza2.zza(zzaoo, obj);
            zzaoo.setLenient(isLenient);
            zzaoo.zzdc(x);
            zzaoo.zzdd(y);
        } catch (IOException e) {
            throw new zzamw((Throwable) e);
        } catch (Throwable th) {
            zzaoo.setLenient(isLenient);
            zzaoo.zzdc(x);
            zzaoo.zzdd(y);
            throw th;
        }
    }

    public void zza(Object obj, Type type, Appendable appendable) throws zzamw {
        try {
            zza(obj, type, zza(zzanw.zza(appendable)));
        } catch (IOException e) {
            throw new zzamw((Throwable) e);
        }
    }

    public String zzb(zzamv zzamv) {
        StringWriter stringWriter = new StringWriter();
        zza(zzamv, (Appendable) stringWriter);
        return stringWriter.toString();
    }

    public String zzc(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        zza(obj, type, (Appendable) stringWriter);
        return stringWriter.toString();
    }

    public String zzch(Object obj) {
        return obj == null ? zzb(zzamx.bei) : zzc(obj, obj.getClass());
    }

    public <T> T zzf(String str, Class<T> cls) throws zzane {
        return zzanv.zzp(cls).cast(zza(str, (Type) cls));
    }

    public <T> zzanh<T> zzk(Class<T> cls) {
        return zza(zzaol.zzr(cls));
    }
}
