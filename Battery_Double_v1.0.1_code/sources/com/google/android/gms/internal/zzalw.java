package com.google.android.gms.internal;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

public class zzalw implements FirebaseRemoteConfigValue {
    public static final Pattern BA = Pattern.compile("^(0|false|f|no|n|off|)$", 2);
    public static final Pattern Bz = Pattern.compile("^(1|true|t|yes|y|on)$", 2);
    public static final Charset UTF_8 = Charset.forName("UTF-8");
    private final byte[] bbr;
    private final int zzaxo;

    public zzalw(byte[] bArr, int i) {
        this.bbr = bArr;
        this.zzaxo = i;
    }

    private void zzcxj() {
        if (this.bbr == null) {
            throw new IllegalArgumentException("Value is null, and cannot be converted to the desired type.");
        }
    }

    public boolean asBoolean() throws IllegalArgumentException {
        if (this.zzaxo == 0) {
            return false;
        }
        String asString = asString();
        if (Bz.matcher(asString).matches()) {
            return true;
        }
        if (BA.matcher(asString).matches()) {
            return false;
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(asString).length() + 45).append("[Value: ").append(asString).append("] cannot be interpreted as a boolean.").toString());
    }

    public byte[] asByteArray() {
        return this.zzaxo == 0 ? FirebaseRemoteConfig.DEFAULT_VALUE_FOR_BYTE_ARRAY : this.bbr;
    }

    public double asDouble() {
        if (this.zzaxo == 0) {
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
        String asString = asString();
        try {
            return Double.valueOf(asString).doubleValue();
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(asString).length() + 42).append("[Value: ").append(asString).append("] cannot be converted to a double.").toString(), e);
        }
    }

    public long asLong() {
        if (this.zzaxo == 0) {
            return 0;
        }
        String asString = asString();
        try {
            return Long.valueOf(asString).longValue();
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(asString).length() + 40).append("[Value: ").append(asString).append("] cannot be converted to a long.").toString(), e);
        }
    }

    public String asString() {
        if (this.zzaxo == 0) {
            return "";
        }
        zzcxj();
        return new String(this.bbr, UTF_8);
    }

    public int getSource() {
        return this.zzaxo;
    }
}
