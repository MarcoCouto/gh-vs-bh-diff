package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.internal.zzii.zza;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzij implements zza<zzd> {
    private final boolean zzcaa;
    private final boolean zzcab;

    public zzij(boolean z, boolean z2) {
        this.zzcaa = z;
        this.zzcab = z2;
    }

    /* renamed from: zzb */
    public zzd zza(zzii zzii, JSONObject jSONObject) throws JSONException, InterruptedException, ExecutionException {
        List<zzky> zza = zzii.zza(jSONObject, "images", true, this.zzcaa, this.zzcab);
        zzky zza2 = zzii.zza(jSONObject, "app_icon", true, this.zzcaa);
        zzky zzg = zzii.zzg(jSONObject);
        ArrayList arrayList = new ArrayList();
        for (zzky zzky : zza) {
            arrayList.add((zzc) zzky.get());
        }
        return new zzd(jSONObject.getString("headline"), arrayList, jSONObject.getString("body"), (zzdr) zza2.get(), jSONObject.getString("call_to_action"), jSONObject.optDouble("rating", -1.0d), jSONObject.optString("store"), jSONObject.optString(Param.PRICE), (com.google.android.gms.ads.internal.formats.zza) zzg.get(), new Bundle());
    }
}
