package com.google.android.gms.internal;

import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzab.zza;
import com.google.android.gms.ads.internal.client.zzac;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzf;
import java.util.HashMap;
import java.util.Map;

@zzin
public class zzlm extends zza {
    /* access modifiers changed from: private */
    public final Object zzail = new Object();
    private boolean zzaio = true;
    /* access modifiers changed from: private */
    public final zzlh zzbgf;
    private final float zzcpy;
    private int zzcpz;
    /* access modifiers changed from: private */
    public zzac zzcqa;
    /* access modifiers changed from: private */
    public boolean zzcqb;
    private boolean zzcqc;
    private float zzcqd;

    public zzlm(zzlh zzlh, float f) {
        this.zzbgf = zzlh;
        this.zzcpy = f;
    }

    private void zzd(String str, @Nullable Map<String, String> map) {
        final HashMap hashMap = map == null ? new HashMap() : new HashMap(map);
        hashMap.put("action", str);
        zzu.zzfq().runOnUiThread(new Runnable() {
            public void run() {
                zzlm.this.zzbgf.zza("pubVideoCmd", hashMap);
            }
        });
    }

    private void zzdd(String str) {
        zzd(str, null);
    }

    private void zzi(final int i, final int i2) {
        zzu.zzfq().runOnUiThread(new Runnable() {
            public void run() {
                boolean z = false;
                synchronized (zzlm.this.zzail) {
                    boolean z2 = i != i2;
                    boolean z3 = !zzlm.this.zzcqb && i2 == 1;
                    boolean z4 = z2 && i2 == 1;
                    boolean z5 = z2 && i2 == 2;
                    boolean z6 = z2 && i2 == 3;
                    zzlm zzlm = zzlm.this;
                    if (zzlm.this.zzcqb || z3) {
                        z = true;
                    }
                    zzlm.zzcqb = z;
                    if (zzlm.this.zzcqa != null) {
                        if (z3) {
                            try {
                                zzlm.this.zzcqa.zzjb();
                            } catch (RemoteException e) {
                                zzkd.zzd("Unable to call onVideoStart()", e);
                            }
                        }
                        if (z4) {
                            try {
                                zzlm.this.zzcqa.zzjc();
                            } catch (RemoteException e2) {
                                zzkd.zzd("Unable to call onVideoPlay()", e2);
                            }
                        }
                        if (z5) {
                            try {
                                zzlm.this.zzcqa.zzjd();
                            } catch (RemoteException e3) {
                                zzkd.zzd("Unable to call onVideoPause()", e3);
                            }
                        }
                        if (z6) {
                            try {
                                zzlm.this.zzcqa.onVideoEnd();
                            } catch (RemoteException e4) {
                                zzkd.zzd("Unable to call onVideoEnd()", e4);
                            }
                        }
                    }
                }
            }
        });
    }

    public int getPlaybackState() {
        int i;
        synchronized (this.zzail) {
            i = this.zzcpz;
        }
        return i;
    }

    public boolean isMuted() {
        boolean z;
        synchronized (this.zzail) {
            z = this.zzcqc;
        }
        return z;
    }

    public void pause() {
        zzdd("pause");
    }

    public void play() {
        zzdd("play");
    }

    public void zza(float f, int i, boolean z) {
        int i2;
        synchronized (this.zzail) {
            this.zzcqd = f;
            this.zzcqc = z;
            i2 = this.zzcpz;
            this.zzcpz = i;
        }
        zzi(i2, i);
    }

    public void zza(zzac zzac) {
        synchronized (this.zzail) {
            this.zzcqa = zzac;
        }
    }

    public void zzam(boolean z) {
        synchronized (this.zzail) {
            this.zzaio = z;
        }
        zzd("initialState", zzf.zze("muteStart", z ? "1" : "0"));
    }

    public float zziz() {
        return this.zzcpy;
    }

    public float zzja() {
        float f;
        synchronized (this.zzail) {
            f = this.zzcqd;
        }
        return f;
    }

    public void zzm(boolean z) {
        zzdd(z ? "mute" : "unmute");
    }
}
