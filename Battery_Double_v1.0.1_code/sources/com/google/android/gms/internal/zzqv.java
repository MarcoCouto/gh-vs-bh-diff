package com.google.android.gms.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public final class zzqv extends Fragment implements zzqk {
    private static WeakHashMap<FragmentActivity, WeakReference<zzqv>> vn = new WeakHashMap<>();
    private Map<String, zzqj> vo = new ArrayMap();
    /* access modifiers changed from: private */
    public Bundle vp;
    /* access modifiers changed from: private */
    public int zzblv = 0;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        if (r0 != null) goto L_0x0012;
     */
    public static zzqv zza(FragmentActivity fragmentActivity) {
        zzqv zzqv;
        WeakReference weakReference = (WeakReference) vn.get(fragmentActivity);
        if (weakReference != null) {
            zzqv = (zzqv) weakReference.get();
        }
        try {
            zzqv = (zzqv) fragmentActivity.getSupportFragmentManager().findFragmentByTag("SupportLifecycleFragmentImpl");
            if (zzqv == null || zzqv.isRemoving()) {
                zzqv = new zzqv();
                fragmentActivity.getSupportFragmentManager().beginTransaction().add((Fragment) zzqv, "SupportLifecycleFragmentImpl").commitAllowingStateLoss();
            }
            vn.put(fragmentActivity, new WeakReference(zzqv));
            return zzqv;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e);
        }
    }

    private void zzb(final String str, @NonNull final zzqj zzqj) {
        if (this.zzblv > 0) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    if (zzqv.this.zzblv >= 1) {
                        zzqj.onCreate(zzqv.this.vp != null ? zzqv.this.vp.getBundle(str) : null);
                    }
                    if (zzqv.this.zzblv >= 2) {
                        zzqj.onStart();
                    }
                    if (zzqv.this.zzblv >= 3) {
                        zzqj.onStop();
                    }
                }
            });
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (zzqj dump : this.vo.values()) {
            dump.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (zzqj onActivityResult : this.vo.values()) {
            onActivityResult.onActivityResult(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzblv = 1;
        this.vp = bundle;
        for (Entry entry : this.vo.entrySet()) {
            ((zzqj) entry.getValue()).onCreate(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Entry entry : this.vo.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((zzqj) entry.getValue()).onSaveInstanceState(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public void onStart() {
        super.onStop();
        this.zzblv = 2;
        for (zzqj onStart : this.vo.values()) {
            onStart.onStart();
        }
    }

    public void onStop() {
        super.onStop();
        this.zzblv = 3;
        for (zzqj onStop : this.vo.values()) {
            onStop.onStop();
        }
    }

    public <T extends zzqj> T zza(String str, Class<T> cls) {
        return (zzqj) cls.cast(this.vo.get(str));
    }

    public void zza(String str, @NonNull zzqj zzqj) {
        if (!this.vo.containsKey(str)) {
            this.vo.put(str, zzqj);
            zzb(str, zzqj);
            return;
        }
        throw new IllegalArgumentException(new StringBuilder(String.valueOf(str).length() + 59).append("LifecycleCallback with tag ").append(str).append(" already added to this fragment.").toString());
    }

    /* renamed from: zzaqv */
    public FragmentActivity zzaqt() {
        return getActivity();
    }
}
