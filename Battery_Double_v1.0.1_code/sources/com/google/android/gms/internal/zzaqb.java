package com.google.android.gms.internal;

import android.content.ComponentName;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsServiceConnection;
import java.lang.ref.WeakReference;

public class zzaqb extends CustomTabsServiceConnection {
    private WeakReference<zzaqc> bkx;

    public zzaqb(zzaqc zzaqc) {
        this.bkx = new WeakReference<>(zzaqc);
    }

    public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
        zzaqc zzaqc = (zzaqc) this.bkx.get();
        if (zzaqc != null) {
            zzaqc.zza(customTabsClient);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        zzaqc zzaqc = (zzaqc) this.bkx.get();
        if (zzaqc != null) {
            zzaqc.zzkm();
        }
    }
}
