package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzanh<T> {
    public abstract void zza(zzaoo zzaoo, T t) throws IOException;

    public abstract T zzb(zzaom zzaom) throws IOException;

    public final zzamv zzcj(T t) {
        try {
            zzaod zzaod = new zzaod();
            zza(zzaod, t);
            return zzaod.f();
        } catch (IOException e) {
            throw new zzamw((Throwable) e);
        }
    }
}
