package com.google.android.gms.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzab;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

public class zzqo {
    private final Set<zzqn<?>> mb = Collections.newSetFromMap(new WeakHashMap());

    public void release() {
        for (zzqn clear : this.mb) {
            clear.clear();
        }
        this.mb.clear();
    }

    public <L> zzqn<L> zzb(@NonNull L l, Looper looper) {
        zzab.zzb(l, (Object) "Listener must not be null");
        zzab.zzb(looper, (Object) "Looper must not be null");
        zzqn<L> zzqn = new zzqn<>(looper, l);
        this.mb.add(zzqn);
        return zzqn;
    }
}
