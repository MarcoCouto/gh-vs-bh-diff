package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

final class zzapx {
    final byte[] apf;
    final int tag;

    zzapx(int i, byte[] bArr) {
        this.tag = i;
        this.apf = bArr;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzapx)) {
            return false;
        }
        zzapx zzapx = (zzapx) obj;
        return this.tag == zzapx.tag && Arrays.equals(this.apf, zzapx.apf);
    }

    public int hashCode() {
        return ((this.tag + 527) * 31) + Arrays.hashCode(this.apf);
    }

    /* access modifiers changed from: 0000 */
    public void zza(zzapo zzapo) throws IOException {
        zzapo.zzagb(this.tag);
        zzapo.zzbh(this.apf);
    }

    /* access modifiers changed from: 0000 */
    public int zzx() {
        return zzapo.zzagc(this.tag) + 0 + this.apf.length;
    }
}
