package com.google.android.gms.dynamic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.zzh;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class zza<T extends LifecycleDelegate> {
    /* access modifiers changed from: private */
    public T Ks;
    /* access modifiers changed from: private */
    public Bundle Kt;
    /* access modifiers changed from: private */
    public LinkedList<C0042zza> Ku;
    private final zzf<T> Kv = new zzf<T>() {
        public void zza(T t) {
            zza.this.Ks = t;
            Iterator it = zza.this.Ku.iterator();
            while (it.hasNext()) {
                ((C0042zza) it.next()).zzb(zza.this.Ks);
            }
            zza.this.Ku.clear();
            zza.this.Kt = null;
        }
    };

    /* renamed from: com.google.android.gms.dynamic.zza$zza reason: collision with other inner class name */
    private interface C0042zza {
        int getState();

        void zzb(LifecycleDelegate lifecycleDelegate);
    }

    private void zza(Bundle bundle, C0042zza zza) {
        if (this.Ks != null) {
            zza.zzb(this.Ks);
            return;
        }
        if (this.Ku == null) {
            this.Ku = new LinkedList<>();
        }
        this.Ku.add(zza);
        if (bundle != null) {
            if (this.Kt == null) {
                this.Kt = (Bundle) bundle.clone();
            } else {
                this.Kt.putAll(bundle);
            }
        }
        zza(this.Kv);
    }

    public static void zzb(FrameLayout frameLayout) {
        final Context context = frameLayout.getContext();
        final int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        String zzc = zzh.zzc(context, isGooglePlayServicesAvailable, GooglePlayServicesUtil.zzbv(context));
        String zzh = zzh.zzh(context, isGooglePlayServicesAvailable);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new LayoutParams(-2, -2));
        textView.setText(zzc);
        linearLayout.addView(textView);
        if (zzh != null) {
            Button button = new Button(context);
            button.setLayoutParams(new LayoutParams(-2, -2));
            button.setText(zzh);
            linearLayout.addView(button);
            button.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    context.startActivity(GooglePlayServicesUtil.zzfd(isGooglePlayServicesAvailable));
                }
            });
        }
    }

    private void zznd(int i) {
        while (!this.Ku.isEmpty() && ((C0042zza) this.Ku.getLast()).getState() >= i) {
            this.Ku.removeLast();
        }
    }

    public void onCreate(final Bundle bundle) {
        zza(bundle, (C0042zza) new C0042zza() {
            public int getState() {
                return 1;
            }

            public void zzb(LifecycleDelegate lifecycleDelegate) {
                zza.this.Ks.onCreate(bundle);
            }
        });
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        final FrameLayout frameLayout = new FrameLayout(layoutInflater.getContext());
        final LayoutInflater layoutInflater2 = layoutInflater;
        final ViewGroup viewGroup2 = viewGroup;
        final Bundle bundle2 = bundle;
        zza(bundle, (C0042zza) new C0042zza() {
            public int getState() {
                return 2;
            }

            public void zzb(LifecycleDelegate lifecycleDelegate) {
                frameLayout.removeAllViews();
                frameLayout.addView(zza.this.Ks.onCreateView(layoutInflater2, viewGroup2, bundle2));
            }
        });
        if (this.Ks == null) {
            zza(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.Ks != null) {
            this.Ks.onDestroy();
        } else {
            zznd(1);
        }
    }

    public void onDestroyView() {
        if (this.Ks != null) {
            this.Ks.onDestroyView();
        } else {
            zznd(2);
        }
    }

    public void onInflate(final Activity activity, final Bundle bundle, final Bundle bundle2) {
        zza(bundle2, (C0042zza) new C0042zza() {
            public int getState() {
                return 0;
            }

            public void zzb(LifecycleDelegate lifecycleDelegate) {
                zza.this.Ks.onInflate(activity, bundle, bundle2);
            }
        });
    }

    public void onLowMemory() {
        if (this.Ks != null) {
            this.Ks.onLowMemory();
        }
    }

    public void onPause() {
        if (this.Ks != null) {
            this.Ks.onPause();
        } else {
            zznd(5);
        }
    }

    public void onResume() {
        zza((Bundle) null, (C0042zza) new C0042zza() {
            public int getState() {
                return 5;
            }

            public void zzb(LifecycleDelegate lifecycleDelegate) {
                zza.this.Ks.onResume();
            }
        });
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (this.Ks != null) {
            this.Ks.onSaveInstanceState(bundle);
        } else if (this.Kt != null) {
            bundle.putAll(this.Kt);
        }
    }

    public void onStart() {
        zza((Bundle) null, (C0042zza) new C0042zza() {
            public int getState() {
                return 4;
            }

            public void zzb(LifecycleDelegate lifecycleDelegate) {
                zza.this.Ks.onStart();
            }
        });
    }

    public void onStop() {
        if (this.Ks != null) {
            this.Ks.onStop();
        } else {
            zznd(4);
        }
    }

    /* access modifiers changed from: protected */
    public void zza(FrameLayout frameLayout) {
        zzb(frameLayout);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(zzf<T> zzf);

    public T zzbbt() {
        return this.Ks;
    }
}
