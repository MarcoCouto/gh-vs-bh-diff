package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzd.zzc;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzio;
import com.google.android.gms.internal.zzip;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkj;
import com.google.android.gms.internal.zzla;

@zzin
public abstract class zzd implements com.google.android.gms.ads.internal.request.zzc.zza, zzkj<Void> {
    private final Object zzail = new Object();
    private final zzla<AdRequestInfoParcel> zzcaj;
    private final com.google.android.gms.ads.internal.request.zzc.zza zzcak;

    @zzin
    public static final class zza extends zzd {
        private final Context mContext;

        public zza(Context context, zzla<AdRequestInfoParcel> zzla, com.google.android.gms.ads.internal.request.zzc.zza zza) {
            super(zzla, zza);
            this.mContext = context;
        }

        public /* synthetic */ Object zzpy() {
            return zzd.super.zzpy();
        }

        public void zzqw() {
        }

        public zzk zzqx() {
            return zzip.zza(this.mContext, new zzcv((String) zzdc.zzaxy.get()), zzio.zzrf());
        }
    }

    @zzin
    public static class zzb extends zzd implements com.google.android.gms.common.internal.zzd.zzb, zzc {
        private Context mContext;
        private final Object zzail = new Object();
        private VersionInfoParcel zzalo;
        private zzla<AdRequestInfoParcel> zzcaj;
        private final com.google.android.gms.ads.internal.request.zzc.zza zzcak;
        protected zze zzcan;
        private boolean zzcao;

        public zzb(Context context, VersionInfoParcel versionInfoParcel, zzla<AdRequestInfoParcel> zzla, com.google.android.gms.ads.internal.request.zzc.zza zza) {
            Looper mainLooper;
            super(zzla, zza);
            this.mContext = context;
            this.zzalo = versionInfoParcel;
            this.zzcaj = zzla;
            this.zzcak = zza;
            if (((Boolean) zzdc.zzayy.get()).booleanValue()) {
                this.zzcao = true;
                mainLooper = zzu.zzgc().zztq();
            } else {
                mainLooper = context.getMainLooper();
            }
            this.zzcan = new zze(context, mainLooper, this, this, this.zzalo.zzcnl);
            connect();
        }

        /* access modifiers changed from: protected */
        public void connect() {
            this.zzcan.zzarx();
        }

        public void onConnected(Bundle bundle) {
            Void voidR = (Void) zzpy();
        }

        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            zzkd.zzcv("Cannot connect to remote service, fallback to local instance.");
            zzqy().zzpy();
            Bundle bundle = new Bundle();
            bundle.putString("action", "gms_connection_failed_fallback_to_local");
            zzu.zzfq().zzb(this.mContext, this.zzalo.zzcs, "gmob-apps", bundle, true);
        }

        public void onConnectionSuspended(int i) {
            zzkd.zzcv("Disconnected from remote ad request service.");
        }

        public /* synthetic */ Object zzpy() {
            return zzd.super.zzpy();
        }

        public void zzqw() {
            synchronized (this.zzail) {
                if (this.zzcan.isConnected() || this.zzcan.isConnecting()) {
                    this.zzcan.disconnect();
                }
                Binder.flushPendingCommands();
                if (this.zzcao) {
                    zzu.zzgc().zztr();
                    this.zzcao = false;
                }
            }
        }

        public zzk zzqx() {
            zzk zzk;
            synchronized (this.zzail) {
                try {
                    zzk = this.zzcan.zzrb();
                } catch (DeadObjectException | IllegalStateException e) {
                    zzk = null;
                }
            }
            return zzk;
        }

        /* access modifiers changed from: 0000 */
        public zzkj zzqy() {
            return new zza(this.mContext, this.zzcaj, this.zzcak);
        }
    }

    public zzd(zzla<AdRequestInfoParcel> zzla, com.google.android.gms.ads.internal.request.zzc.zza zza2) {
        this.zzcaj = zzla;
        this.zzcak = zza2;
    }

    public void cancel() {
        zzqw();
    }

    /* access modifiers changed from: 0000 */
    public boolean zza(zzk zzk, AdRequestInfoParcel adRequestInfoParcel) {
        try {
            zzk.zza(adRequestInfoParcel, new zzg(this));
            return true;
        } catch (RemoteException e) {
            zzkd.zzd("Could not fetch ad response from ad request service.", e);
            zzu.zzft().zzb((Throwable) e, true);
        } catch (NullPointerException e2) {
            zzkd.zzd("Could not fetch ad response from ad request service due to an Exception.", e2);
            zzu.zzft().zzb((Throwable) e2, true);
        } catch (SecurityException e3) {
            zzkd.zzd("Could not fetch ad response from ad request service due to an Exception.", e3);
            zzu.zzft().zzb((Throwable) e3, true);
        } catch (Throwable th) {
            zzkd.zzd("Could not fetch ad response from ad request service due to an Exception.", th);
            zzu.zzft().zzb(th, true);
        }
        this.zzcak.zzb(new AdResponseParcel(0));
        return false;
    }

    public void zzb(AdResponseParcel adResponseParcel) {
        synchronized (this.zzail) {
            this.zzcak.zzb(adResponseParcel);
            zzqw();
        }
    }

    /* renamed from: zzpv */
    public Void zzpy() {
        final zzk zzqx = zzqx();
        if (zzqx == null) {
            this.zzcak.zzb(new AdResponseParcel(0));
            zzqw();
        } else {
            this.zzcaj.zza(new zzla.zzc<AdRequestInfoParcel>() {
                /* renamed from: zzc */
                public void zzd(AdRequestInfoParcel adRequestInfoParcel) {
                    if (!zzd.this.zza(zzqx, adRequestInfoParcel)) {
                        zzd.this.zzqw();
                    }
                }
            }, new com.google.android.gms.internal.zzla.zza() {
                public void run() {
                    zzd.this.zzqw();
                }
            });
        }
        return null;
    }

    public abstract void zzqw();

    public abstract zzk zzqx();
}
