package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.internal.request.zza.C0015zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzfp;
import com.google.android.gms.internal.zzfs;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zziq;
import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkl;
import com.google.android.gms.internal.zzlh;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzn extends zzkc {
    private static final Object zzamr = new Object();
    /* access modifiers changed from: private */
    public static zzfs zzbyv = null;
    static final long zzcdf = TimeUnit.SECONDS.toMillis(10);
    static boolean zzcdg = false;
    private static zzeq zzcdh = null;
    /* access modifiers changed from: private */
    public static zzeu zzcdi = null;
    private static zzep zzcdj = null;
    private final Context mContext;
    private final Object zzbxu = new Object();
    /* access modifiers changed from: private */
    public final C0015zza zzcae;
    private final com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zzcaf;
    /* access modifiers changed from: private */
    public com.google.android.gms.internal.zzfs.zzc zzcdk;

    public static class zza implements zzkl<zzfp> {
        /* renamed from: zza */
        public void zzd(zzfp zzfp) {
            zzn.zzc(zzfp);
        }
    }

    public static class zzb implements zzkl<zzfp> {
        /* renamed from: zza */
        public void zzd(zzfp zzfp) {
            zzn.zzb(zzfp);
        }
    }

    public static class zzc implements zzep {
        public void zza(zzlh zzlh, Map<String, String> map) {
            String str = (String) map.get("request_id");
            String str2 = "Invalid request: ";
            String valueOf = String.valueOf((String) map.get("errors"));
            zzkd.zzcx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            zzn.zzcdi.zzax(str);
        }
    }

    public zzn(Context context, com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zza2, C0015zza zza3) {
        super(true);
        this.zzcae = zza3;
        this.mContext = context;
        this.zzcaf = zza2;
        synchronized (zzamr) {
            if (!zzcdg) {
                zzcdi = new zzeu();
                zzcdh = new zzeq(context.getApplicationContext(), zza2.zzaow);
                zzcdj = new zzc();
                zzbyv = new zzfs(this.mContext.getApplicationContext(), this.zzcaf.zzaow, (String) zzdc.zzaxy.get(), new zzb(), new zza());
                zzcdg = true;
            }
        }
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [org.json.JSONObject, com.google.android.gms.internal.zziz$zza] */
    /* JADX WARNING: type inference failed for: r4v0, types: [android.location.Location] */
    /* JADX WARNING: type inference failed for: r6v0, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r8v0, types: [android.os.Bundle] */
    /* JADX WARNING: type inference failed for: r9v0, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: type inference failed for: r0v7, types: [com.google.android.gms.ads.identifier.AdvertisingIdClient$Info] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.google.android.gms.ads.identifier.AdvertisingIdClient$Info] */
    /* JADX WARNING: type inference failed for: r0v17 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v0, types: [org.json.JSONObject, com.google.android.gms.internal.zziz$zza]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [org.json.JSONObject, ?[OBJECT, ARRAY], com.google.android.gms.internal.zziz$zza]
  mth insns count: 60
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 7 */
    private JSONObject zza(AdRequestInfoParcel adRequestInfoParcel, String str) {
        ? r0;
        ? r3 = 0;
        Bundle bundle = adRequestInfoParcel.zzcar.extras.getBundle("sdk_less_server_data");
        String string = adRequestInfoParcel.zzcar.extras.getString("sdk_less_network_id");
        if (bundle == null) {
            return r3;
        }
        JSONObject zza2 = zziq.zza(this.mContext, adRequestInfoParcel, zzu.zzfw().zzy(this.mContext), r3, r3, new zzcv((String) zzdc.zzaxy.get()), r3, new ArrayList(), r3, r3);
        if (zza2 == null) {
            return r3;
        }
        try {
            r0 = AdvertisingIdClient.getAdvertisingIdInfo(this.mContext);
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e) {
            zzkd.zzd("Cannot get advertising id info", e);
            r0 = r3;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("request_id", str);
        hashMap.put("network_id", string);
        hashMap.put("request_param", zza2);
        hashMap.put("data", bundle);
        if (r0 != 0) {
            hashMap.put("adid", r0.getId());
            hashMap.put("lat", Integer.valueOf(r0.isLimitAdTrackingEnabled() ? 1 : 0));
        }
        try {
            return zzu.zzfq().zzam((Map<String, ?>) hashMap);
        } catch (JSONException e2) {
            return r3;
        }
    }

    protected static void zzb(zzfp zzfp) {
        zzfp.zza("/loadAd", (zzep) zzcdi);
        zzfp.zza("/fetchHttpRequest", (zzep) zzcdh);
        zzfp.zza("/invalidRequest", zzcdj);
    }

    protected static void zzc(zzfp zzfp) {
        zzfp.zzb("/loadAd", (zzep) zzcdi);
        zzfp.zzb("/fetchHttpRequest", (zzep) zzcdh);
        zzfp.zzb("/invalidRequest", zzcdj);
    }

    private AdResponseParcel zze(AdRequestInfoParcel adRequestInfoParcel) {
        final String zzte = zzu.zzfq().zzte();
        final JSONObject zza2 = zza(adRequestInfoParcel, zzte);
        if (zza2 == null) {
            return new AdResponseParcel(0);
        }
        long elapsedRealtime = zzu.zzfu().elapsedRealtime();
        Future zzaw = zzcdi.zzaw(zzte);
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                zzn.this.zzcdk = zzn.zzbyv.zzma();
                zzn.this.zzcdk.zza(new com.google.android.gms.internal.zzla.zzc<zzft>() {
                    /* renamed from: zzb */
                    public void zzd(zzft zzft) {
                        try {
                            zzft.zza("AFMA_getAdapterLessMediationAd", zza2);
                        } catch (Exception e) {
                            zzkd.zzb("Error requesting an ad url", e);
                            zzn.zzcdi.zzax(zzte);
                        }
                    }
                }, new com.google.android.gms.internal.zzla.zza() {
                    public void run() {
                        zzn.zzcdi.zzax(zzte);
                    }
                });
            }
        });
        try {
            JSONObject jSONObject = (JSONObject) zzaw.get(zzcdf - (zzu.zzfu().elapsedRealtime() - elapsedRealtime), TimeUnit.MILLISECONDS);
            if (jSONObject == null) {
                return new AdResponseParcel(-1);
            }
            AdResponseParcel zza3 = zziq.zza(this.mContext, adRequestInfoParcel, jSONObject.toString());
            return (zza3.errorCode == -3 || !TextUtils.isEmpty(zza3.body)) ? zza3 : new AdResponseParcel(3);
        } catch (InterruptedException | CancellationException e) {
            return new AdResponseParcel(-1);
        } catch (TimeoutException e2) {
            return new AdResponseParcel(2);
        } catch (ExecutionException e3) {
            return new AdResponseParcel(0);
        }
    }

    public void onStop() {
        synchronized (this.zzbxu) {
            com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
                public void run() {
                    if (zzn.this.zzcdk != null) {
                        zzn.this.zzcdk.release();
                        zzn.this.zzcdk = null;
                    }
                }
            });
        }
    }

    public void zzew() {
        zzkd.zzcv("SdkLessAdLoaderBackgroundTask started.");
        AdRequestInfoParcel adRequestInfoParcel = new AdRequestInfoParcel(this.zzcaf, null, -1);
        AdResponseParcel zze = zze(adRequestInfoParcel);
        final com.google.android.gms.internal.zzju.zza zza2 = new com.google.android.gms.internal.zzju.zza(adRequestInfoParcel, zze, null, null, zze.errorCode, zzu.zzfu().elapsedRealtime(), zze.zzccc, null);
        com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
            public void run() {
                zzn.this.zzcae.zza(zza2);
                if (zzn.this.zzcdk != null) {
                    zzn.this.zzcdk.release();
                    zzn.this.zzcdk = null;
                }
            }
        });
    }
}
