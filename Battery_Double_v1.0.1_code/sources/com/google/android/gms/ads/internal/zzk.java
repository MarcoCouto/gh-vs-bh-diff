package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzr;
import com.google.android.gms.ads.internal.client.zzs.zza;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzeb;
import com.google.android.gms.internal.zzec;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzee;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzin;

@zzin
public class zzk extends zza {
    private final Context mContext;
    private final zzd zzajv;
    private final zzgj zzajz;
    private zzq zzalf;
    private NativeAdOptionsParcel zzalk;
    private zzy zzalm;
    private final String zzaln;
    private final VersionInfoParcel zzalo;
    private zzeb zzals;
    private zzec zzalt;
    private SimpleArrayMap<String, zzed> zzalu = new SimpleArrayMap<>();
    private SimpleArrayMap<String, zzee> zzalv = new SimpleArrayMap<>();

    public zzk(Context context, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel, zzd zzd) {
        this.mContext = context;
        this.zzaln = str;
        this.zzajz = zzgj;
        this.zzalo = versionInfoParcel;
        this.zzajv = zzd;
    }

    public void zza(NativeAdOptionsParcel nativeAdOptionsParcel) {
        this.zzalk = nativeAdOptionsParcel;
    }

    public void zza(zzeb zzeb) {
        this.zzals = zzeb;
    }

    public void zza(zzec zzec) {
        this.zzalt = zzec;
    }

    public void zza(String str, zzee zzee, zzed zzed) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Custom template ID for native custom template ad is empty. Please provide a valid template id.");
        }
        this.zzalv.put(str, zzee);
        this.zzalu.put(str, zzed);
    }

    public void zzb(zzq zzq) {
        this.zzalf = zzq;
    }

    public void zzb(zzy zzy) {
        this.zzalm = zzy;
    }

    public zzr zzes() {
        return new zzj(this.mContext, this.zzaln, this.zzajz, this.zzalo, this.zzalf, this.zzals, this.zzalt, this.zzalv, this.zzalu, this.zzalk, this.zzalm, this.zzajv);
    }
}
