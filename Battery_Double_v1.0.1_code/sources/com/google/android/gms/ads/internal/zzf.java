package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzce;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjo;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zzli;
import com.google.android.gms.internal.zzli.zzb;
import com.google.android.gms.internal.zzli.zzd;
import com.google.android.gms.internal.zzlm;

@zzin
public class zzf extends zzc implements OnGlobalLayoutListener, OnScrollChangedListener {
    private boolean zzakp;

    public class zza {
        public zza() {
        }

        public void onClick() {
            zzf.this.onAdClicked();
        }
    }

    public zzf(Context context, AdSizeParcel adSizeParcel, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel, zzd zzd) {
        super(context, adSizeParcel, str, zzgj, versionInfoParcel, zzd);
    }

    private AdSizeParcel zzb(com.google.android.gms.internal.zzju.zza zza2) {
        AdSize zzij;
        if (zza2.zzciq.zzauv) {
            return this.zzajs.zzapa;
        }
        String str = zza2.zzciq.zzccb;
        if (str != null) {
            String[] split = str.split("[xX]");
            split[0] = split[0].trim();
            split[1] = split[1].trim();
            zzij = new AdSize(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
        } else {
            zzij = this.zzajs.zzapa.zzij();
        }
        return new AdSizeParcel(this.zzajs.zzagf, zzij);
    }

    private boolean zzb(@Nullable zzju zzju, zzju zzju2) {
        if (zzju2.zzcby) {
            View zzf = zzn.zzf(zzju2);
            if (zzf == null) {
                zzkd.zzcx("Could not get mediation view");
                return false;
            }
            View nextView = this.zzajs.zzaox.getNextView();
            if (nextView != null) {
                if (nextView instanceof zzlh) {
                    ((zzlh) nextView).destroy();
                }
                this.zzajs.zzaox.removeView(nextView);
            }
            if (!zzn.zzg(zzju2)) {
                try {
                    zzb(zzf);
                } catch (Throwable th) {
                    zzkd.zzd("Could not add mediation view to view hierarchy.", th);
                    return false;
                }
            }
        } else if (!(zzju2.zzcii == null || zzju2.zzbtm == null)) {
            zzju2.zzbtm.zza(zzju2.zzcii);
            this.zzajs.zzaox.removeAllViews();
            this.zzajs.zzaox.setMinimumWidth(zzju2.zzcii.widthPixels);
            this.zzajs.zzaox.setMinimumHeight(zzju2.zzcii.heightPixels);
            zzb(zzju2.zzbtm.getView());
        }
        if (this.zzajs.zzaox.getChildCount() > 1) {
            this.zzajs.zzaox.showNext();
        }
        if (zzju != null) {
            View nextView2 = this.zzajs.zzaox.getNextView();
            if (nextView2 instanceof zzlh) {
                ((zzlh) nextView2).zza(this.zzajs.zzagf, this.zzajs.zzapa, this.zzajn);
            } else if (nextView2 != null) {
                this.zzajs.zzaox.removeView(nextView2);
            }
            this.zzajs.zzgo();
        }
        this.zzajs.zzaox.setVisibility(0);
        return true;
    }

    private void zzd(final zzju zzju) {
        if (this.zzajs.zzgp()) {
            if (zzju.zzbtm != null) {
                if (zzju.zzcie != null) {
                    this.zzaju.zza(this.zzajs.zzapa, zzju);
                }
                if (zzju.zzho()) {
                    this.zzaju.zza(this.zzajs.zzapa, zzju).zza((zzce) zzju.zzbtm);
                } else {
                    zzju.zzbtm.zzuj().zza((zzb) new zzb() {
                        public void zzen() {
                            zzf.this.zzaju.zza(zzf.this.zzajs.zzapa, zzju).zza((zzce) zzju.zzbtm);
                        }
                    });
                }
            }
        } else if (this.zzajs.zzapv != null && zzju.zzcie != null) {
            this.zzaju.zza(this.zzajs.zzapa, zzju, this.zzajs.zzapv);
        }
    }

    public void onGlobalLayout() {
        zze(this.zzajs.zzapb);
    }

    public void onScrollChanged() {
        zze(this.zzajs.zzapb);
    }

    public void setManualImpressionsEnabled(boolean z) {
        zzab.zzhi("setManualImpressionsEnabled must be called from the main thread.");
        this.zzakp = z;
    }

    public void showInterstitial() {
        throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
    }

    /* access modifiers changed from: protected */
    public zzlh zza(com.google.android.gms.internal.zzju.zza zza2, @Nullable zze zze, @Nullable zzjo zzjo) {
        if (this.zzajs.zzapa.zzaut == null && this.zzajs.zzapa.zzauv) {
            this.zzajs.zzapa = zzb(zza2);
        }
        return super.zza(zza2, zze, zzjo);
    }

    /* access modifiers changed from: protected */
    public void zza(@Nullable zzju zzju, boolean z) {
        super.zza(zzju, z);
        if (zzn.zzg(zzju)) {
            zzn.zza(zzju, new zza());
        }
    }

    public boolean zza(@Nullable zzju zzju, final zzju zzju2) {
        zzlm zzlm;
        if (!super.zza(zzju, zzju2)) {
            return false;
        }
        if (!this.zzajs.zzgp() || zzb(zzju, zzju2)) {
            if (zzju2.zzccq) {
                zze(zzju2);
                zzu.zzgk().zza((View) this.zzajs.zzaox, (OnGlobalLayoutListener) this);
                zzu.zzgk().zza((View) this.zzajs.zzaox, (OnScrollChangedListener) this);
                if (!zzju2.zzcif) {
                    final AnonymousClass1 r2 = new Runnable() {
                        public void run() {
                            zzf.this.zze(zzf.this.zzajs.zzapb);
                        }
                    };
                    zzli zzli = zzju2.zzbtm != null ? zzju2.zzbtm.zzuj() : null;
                    if (zzli != null) {
                        zzli.zza((zzd) new zzd() {
                            public void zzem() {
                                if (!zzju2.zzcif) {
                                    zzu.zzfq();
                                    zzkh.zzb(r2);
                                }
                            }
                        });
                    }
                }
            } else if (!this.zzajs.zzgq() || ((Boolean) zzdc.zzbce.get()).booleanValue()) {
                zza(zzju2, false);
            }
            if (zzju2.zzbtm != null) {
                zzlm = zzju2.zzbtm.zzut();
                zzli zzuj = zzju2.zzbtm.zzuj();
                if (zzuj != null) {
                    zzuj.zzva();
                }
            } else {
                zzlm = null;
            }
            if (!(this.zzajs.zzapp == null || zzlm == null)) {
                zzlm.zzam(this.zzajs.zzapp.zzaxm);
            }
            zzd(zzju2);
            return true;
        }
        zzh(0);
        return false;
    }

    public boolean zzb(AdRequestParcel adRequestParcel) {
        return super.zzb(zze(adRequestParcel));
    }

    @Nullable
    public com.google.android.gms.ads.internal.client.zzab zzdq() {
        zzab.zzhi("getVideoController must be called from the main thread.");
        if (this.zzajs.zzapb == null || this.zzajs.zzapb.zzbtm == null) {
            return null;
        }
        return this.zzajs.zzapb.zzbtm.zzut();
    }

    /* access modifiers changed from: protected */
    public boolean zzdw() {
        boolean z = true;
        if (!zzu.zzfq().zza(this.zzajs.zzagf.getPackageManager(), this.zzajs.zzagf.getPackageName(), "android.permission.INTERNET")) {
            zzm.zziw().zza(this.zzajs.zzaox, this.zzajs.zzapa, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
            z = false;
        }
        if (!zzu.zzfq().zzac(this.zzajs.zzagf)) {
            zzm.zziw().zza(this.zzajs.zzaox, this.zzajs.zzapa, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
            z = false;
        }
        if (!z && this.zzajs.zzaox != null) {
            this.zzajs.zzaox.setVisibility(0);
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public AdRequestParcel zze(AdRequestParcel adRequestParcel) {
        if (adRequestParcel.zzatr == this.zzakp) {
            return adRequestParcel;
        }
        return new AdRequestParcel(adRequestParcel.versionCode, adRequestParcel.zzatm, adRequestParcel.extras, adRequestParcel.zzatn, adRequestParcel.zzato, adRequestParcel.zzatp, adRequestParcel.zzatq, adRequestParcel.zzatr || this.zzakp, adRequestParcel.zzats, adRequestParcel.zzatt, adRequestParcel.zzatu, adRequestParcel.zzatv, adRequestParcel.zzatw, adRequestParcel.zzatx, adRequestParcel.zzaty, adRequestParcel.zzatz, adRequestParcel.zzaua, adRequestParcel.zzaub);
    }

    /* access modifiers changed from: 0000 */
    public void zze(@Nullable zzju zzju) {
        if (zzju != null && !zzju.zzcif && this.zzajs.zzaox != null && zzu.zzfq().zza((View) this.zzajs.zzaox, this.zzajs.zzagf) && this.zzajs.zzaox.getGlobalVisibleRect(new Rect(), null)) {
            if (!(zzju == null || zzju.zzbtm == null || zzju.zzbtm.zzuj() == null)) {
                zzju.zzbtm.zzuj().zza((zzd) null);
            }
            zza(zzju, false);
            zzju.zzcif = true;
        }
    }
}
