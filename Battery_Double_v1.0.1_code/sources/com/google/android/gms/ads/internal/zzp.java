package com.google.android.gms.ads.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzin;

@zzin
public class zzp {
    @Nullable
    public zzo zzex() {
        return zzo.zzex();
    }
}
