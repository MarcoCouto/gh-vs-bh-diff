package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzr.zza;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzeb;
import com.google.android.gms.internal.zzec;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzee;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkh;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@zzin
public class zzj extends zza {
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();
    private final zzd zzajv;
    private final zzgj zzajz;
    /* access modifiers changed from: private */
    public final zzq zzalf;
    /* access modifiers changed from: private */
    @Nullable
    public final zzeb zzalg;
    /* access modifiers changed from: private */
    @Nullable
    public final zzec zzalh;
    /* access modifiers changed from: private */
    public final SimpleArrayMap<String, zzee> zzali;
    /* access modifiers changed from: private */
    public final SimpleArrayMap<String, zzed> zzalj;
    /* access modifiers changed from: private */
    public final NativeAdOptionsParcel zzalk;
    private final List<String> zzall;
    /* access modifiers changed from: private */
    public final zzy zzalm;
    private final String zzaln;
    private final VersionInfoParcel zzalo;
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<zzq> zzalp;

    zzj(Context context, String str, zzgj zzgj, VersionInfoParcel versionInfoParcel, zzq zzq, zzeb zzeb, zzec zzec, SimpleArrayMap<String, zzee> simpleArrayMap, SimpleArrayMap<String, zzed> simpleArrayMap2, NativeAdOptionsParcel nativeAdOptionsParcel, zzy zzy, zzd zzd) {
        this.mContext = context;
        this.zzaln = str;
        this.zzajz = zzgj;
        this.zzalo = versionInfoParcel;
        this.zzalf = zzq;
        this.zzalh = zzec;
        this.zzalg = zzeb;
        this.zzali = simpleArrayMap;
        this.zzalj = simpleArrayMap2;
        this.zzalk = nativeAdOptionsParcel;
        this.zzall = zzeq();
        this.zzalm = zzy;
        this.zzajv = zzd;
    }

    /* access modifiers changed from: private */
    public List<String> zzeq() {
        ArrayList arrayList = new ArrayList();
        if (this.zzalh != null) {
            arrayList.add("1");
        }
        if (this.zzalg != null) {
            arrayList.add("2");
        }
        if (this.zzali.size() > 0) {
            arrayList.add("3");
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    @Nullable
    public String getMediationAdapterClassName() {
        synchronized (this.zzail) {
            if (this.zzalp == null) {
                return null;
            }
            zzq zzq = (zzq) this.zzalp.get();
            String str = zzq != null ? zzq.getMediationAdapterClassName() : null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    public boolean isLoading() {
        synchronized (this.zzail) {
            if (this.zzalp == null) {
                return false;
            }
            zzq zzq = (zzq) this.zzalp.get();
            boolean z = zzq != null ? zzq.isLoading() : false;
        }
    }

    /* access modifiers changed from: protected */
    public void runOnUiThread(Runnable runnable) {
        zzkh.zzclc.post(runnable);
    }

    /* access modifiers changed from: protected */
    public zzq zzer() {
        return new zzq(this.mContext, this.zzajv, AdSizeParcel.zzk(this.mContext), this.zzaln, this.zzajz, this.zzalo);
    }

    public void zzf(final AdRequestParcel adRequestParcel) {
        runOnUiThread(new Runnable() {
            public void run() {
                synchronized (zzj.this.zzail) {
                    zzq zzer = zzj.this.zzer();
                    zzj.this.zzalp = new WeakReference(zzer);
                    zzer.zzb(zzj.this.zzalg);
                    zzer.zzb(zzj.this.zzalh);
                    zzer.zza(zzj.this.zzali);
                    zzer.zza(zzj.this.zzalf);
                    zzer.zzb(zzj.this.zzalj);
                    zzer.zzb(zzj.this.zzeq());
                    zzer.zzb(zzj.this.zzalk);
                    zzer.zza(zzj.this.zzalm);
                    zzer.zzb(adRequestParcel);
                }
            }
        });
    }
}
