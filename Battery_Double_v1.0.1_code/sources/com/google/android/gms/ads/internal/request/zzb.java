package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.zza.C0015zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzas;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzga;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkg;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzkj;
import com.google.android.gms.internal.zzla;
import com.google.android.gms.internal.zzlb;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzb extends zzkc implements com.google.android.gms.ads.internal.request.zzc.zza {
    private final Context mContext;
    private final zzas zzbgd;
    zzga zzboe;
    private AdRequestInfoParcel zzbot;
    AdResponseParcel zzbxs;
    /* access modifiers changed from: private */
    public Runnable zzbxt;
    /* access modifiers changed from: private */
    public final Object zzbxu = new Object();
    private final C0015zza zzcae;
    /* access modifiers changed from: private */
    public final com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zzcaf;
    zzkj zzcag;

    @zzin
    static final class zza extends Exception {
        private final int zzbyi;

        public zza(String str, int i) {
            super(str);
            this.zzbyi = i;
        }

        public int getErrorCode() {
            return this.zzbyi;
        }
    }

    public zzb(Context context, com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza zza2, zzas zzas, C0015zza zza3) {
        this.zzcae = zza3;
        this.mContext = context;
        this.zzcaf = zza2;
        this.zzbgd = zzas;
    }

    /* access modifiers changed from: private */
    public void zzd(int i, String str) {
        if (i == 3 || i == -1) {
            zzkd.zzcw(str);
        } else {
            zzkd.zzcx(str);
        }
        if (this.zzbxs == null) {
            this.zzbxs = new AdResponseParcel(i);
        } else {
            this.zzbxs = new AdResponseParcel(i, this.zzbxs.zzbns);
        }
        this.zzcae.zza(new com.google.android.gms.internal.zzju.zza(this.zzbot != null ? this.zzbot : new AdRequestInfoParcel(this.zzcaf, null, -1), this.zzbxs, this.zzboe, null, i, -1, this.zzbxs.zzccc, null));
    }

    public void onStop() {
        synchronized (this.zzbxu) {
            if (this.zzcag != null) {
                this.zzcag.cancel();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public zzkj zza(VersionInfoParcel versionInfoParcel, zzla<AdRequestInfoParcel> zzla) {
        return zzc.zza(this.mContext, versionInfoParcel, zzla, this);
    }

    /* access modifiers changed from: protected */
    public AdSizeParcel zzb(AdRequestInfoParcel adRequestInfoParcel) throws zza {
        AdSizeParcel[] adSizeParcelArr;
        if (this.zzbxs.zzauv) {
            for (AdSizeParcel adSizeParcel : adRequestInfoParcel.zzapa.zzaut) {
                if (adSizeParcel.zzauv) {
                    return new AdSizeParcel(adSizeParcel, adRequestInfoParcel.zzapa.zzaut);
                }
            }
        }
        if (this.zzbxs.zzccb == null) {
            throw new zza("The ad response must specify one of the supported ad sizes.", 0);
        }
        String[] split = this.zzbxs.zzccb.split("x");
        if (split.length != 2) {
            String str = "Invalid ad size format from the ad response: ";
            String valueOf = String.valueOf(this.zzbxs.zzccb);
            throw new zza(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), 0);
        }
        try {
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            AdSizeParcel[] adSizeParcelArr2 = adRequestInfoParcel.zzapa.zzaut;
            int length = adSizeParcelArr2.length;
            for (int i = 0; i < length; i++) {
                AdSizeParcel adSizeParcel2 = adSizeParcelArr2[i];
                float f = this.mContext.getResources().getDisplayMetrics().density;
                int i2 = adSizeParcel2.width == -1 ? (int) (((float) adSizeParcel2.widthPixels) / f) : adSizeParcel2.width;
                int i3 = adSizeParcel2.height == -2 ? (int) (((float) adSizeParcel2.heightPixels) / f) : adSizeParcel2.height;
                if (parseInt == i2 && parseInt2 == i3) {
                    return new AdSizeParcel(adSizeParcel2, adRequestInfoParcel.zzapa.zzaut);
                }
            }
            String str2 = "The ad size from the ad response was not one of the requested sizes: ";
            String valueOf2 = String.valueOf(this.zzbxs.zzccb);
            throw new zza(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), 0);
        } catch (NumberFormatException e) {
            String str3 = "Invalid ad size number from the ad response: ";
            String valueOf3 = String.valueOf(this.zzbxs.zzccb);
            throw new zza(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3), 0);
        }
    }

    public void zzb(@NonNull AdResponseParcel adResponseParcel) {
        JSONObject jSONObject;
        zzkd.zzcv("Received ad response.");
        this.zzbxs = adResponseParcel;
        long elapsedRealtime = zzu.zzfu().elapsedRealtime();
        synchronized (this.zzbxu) {
            this.zzcag = null;
        }
        zzu.zzft().zzd(this.mContext, this.zzbxs.zzcbq);
        try {
            if (this.zzbxs.errorCode == -2 || this.zzbxs.errorCode == -3) {
                zzqv();
                AdSizeParcel adSizeParcel = this.zzbot.zzapa.zzaut != null ? zzb(this.zzbot) : null;
                zzu.zzft().zzae(this.zzbxs.zzcci);
                if (!TextUtils.isEmpty(this.zzbxs.zzccg)) {
                    try {
                        jSONObject = new JSONObject(this.zzbxs.zzccg);
                    } catch (Exception e) {
                        zzkd.zzb("Error parsing the JSON for Active View.", e);
                    }
                    this.zzcae.zza(new com.google.android.gms.internal.zzju.zza(this.zzbot, this.zzbxs, this.zzboe, adSizeParcel, -2, elapsedRealtime, this.zzbxs.zzccc, jSONObject));
                    zzkh.zzclc.removeCallbacks(this.zzbxt);
                    return;
                }
                jSONObject = null;
                this.zzcae.zza(new com.google.android.gms.internal.zzju.zza(this.zzbot, this.zzbxs, this.zzboe, adSizeParcel, -2, elapsedRealtime, this.zzbxs.zzccc, jSONObject));
                zzkh.zzclc.removeCallbacks(this.zzbxt);
                return;
            }
            throw new zza("There was a problem getting an ad response. ErrorCode: " + this.zzbxs.errorCode, this.zzbxs.errorCode);
        } catch (zza e2) {
            zzd(e2.getErrorCode(), e2.getMessage());
            zzkh.zzclc.removeCallbacks(this.zzbxt);
        }
    }

    public void zzew() {
        zzkd.zzcv("AdLoaderBackgroundTask started.");
        this.zzbxt = new Runnable() {
            public void run() {
                synchronized (zzb.this.zzbxu) {
                    if (zzb.this.zzcag != null) {
                        zzb.this.onStop();
                        zzb.this.zzd(2, "Timed out waiting for ad response.");
                    }
                }
            }
        };
        zzkh.zzclc.postDelayed(this.zzbxt, ((Long) zzdc.zzbbg.get()).longValue());
        final zzlb zzlb = new zzlb();
        long elapsedRealtime = zzu.zzfu().elapsedRealtime();
        zzkg.zza((Runnable) new Runnable() {
            public void run() {
                synchronized (zzb.this.zzbxu) {
                    zzb.this.zzcag = zzb.this.zza(zzb.this.zzcaf.zzaow, zzlb);
                    if (zzb.this.zzcag == null) {
                        zzb.this.zzd(0, "Could not start the ad request service.");
                        zzkh.zzclc.removeCallbacks(zzb.this.zzbxt);
                    }
                }
            }
        });
        this.zzbot = new AdRequestInfoParcel(this.zzcaf, this.zzbgd.zzaw().zzb(this.mContext), elapsedRealtime);
        zzlb.zzg(this.zzbot);
    }

    /* access modifiers changed from: protected */
    public void zzqv() throws zza {
        if (this.zzbxs.errorCode != -3) {
            if (TextUtils.isEmpty(this.zzbxs.body)) {
                throw new zza("No fill from ad server.", 3);
            }
            zzu.zzft().zzc(this.mContext, this.zzbxs.zzcaz);
            if (this.zzbxs.zzcby) {
                try {
                    this.zzboe = new zzga(this.zzbxs.body);
                    zzu.zzft().zzaf(this.zzboe.zzbnq);
                } catch (JSONException e) {
                    zzkd.zzb("Could not parse mediation config.", e);
                    String str = "Could not parse mediation config: ";
                    String valueOf = String.valueOf(this.zzbxs.body);
                    throw new zza(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), 0);
                }
            } else {
                zzu.zzft().zzaf(this.zzbxs.zzbnq);
            }
            if (!TextUtils.isEmpty(this.zzbxs.zzcbr) && ((Boolean) zzdc.zzbdn.get()).booleanValue()) {
                zzkd.zzcv("Received cookie from server. Setting webview cookie in CookieManager.");
                CookieManager zzao = zzu.zzfs().zzao(this.mContext);
                if (zzao != null) {
                    zzao.setCookie("googleads.g.doubleclick.net", this.zzbxs.zzcbr);
                }
            }
        }
    }
}
