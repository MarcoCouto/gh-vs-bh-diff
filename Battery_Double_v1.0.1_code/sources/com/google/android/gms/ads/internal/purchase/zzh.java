package com.google.android.gms.ads.internal.purchase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

@zzin
public class zzh {
    private static final Object zzail = new Object();
    /* access modifiers changed from: private */
    public static final String zzbxk = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s INTEGER)", new Object[]{"InAppPurchase", "purchase_id", Param.PRODUCT_ID, "developer_payload", "record_time"});
    private static zzh zzbxm;
    private final zza zzbxl;

    public class zza extends SQLiteOpenHelper {
        public zza(Context context, String str) {
            super(context, str, null, 4);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL(zzh.zzbxk);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            zzkd.zzcw("Database updated from version " + i + " to version " + i2);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS InAppPurchase");
            onCreate(sQLiteDatabase);
        }
    }

    zzh(Context context) {
        this.zzbxl = new zza(context, "google_inapp_purchase.db");
    }

    public static zzh zzs(Context context) {
        zzh zzh;
        synchronized (zzail) {
            if (zzbxm == null) {
                zzbxm = new zzh(context);
            }
            zzh = zzbxm;
        }
        return zzh;
    }

    public int getRecordCount() {
        boolean z = null;
        int i = 0;
        synchronized (zzail) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    Cursor rawQuery = writableDatabase.rawQuery("select count(*) from InAppPurchase", null);
                    z = rawQuery.moveToFirst();
                    if (z) {
                        i = rawQuery.getInt(0);
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                    } else if (rawQuery != null) {
                        rawQuery.close();
                    }
                } catch (SQLiteException e) {
                    String str = "Error getting record count";
                    String valueOf = String.valueOf(e.getMessage());
                    z = valueOf.length();
                    zzkd.zzcx(z != 0 ? str.concat(valueOf) : new String(str));
                    if (z != null) {
                        z.close();
                    }
                } finally {
                    if (z != null) {
                        z.close();
                    }
                }
            }
        }
        return i;
    }

    public SQLiteDatabase getWritableDatabase() {
        try {
            return this.zzbxl.getWritableDatabase();
        } catch (SQLiteException e) {
            zzkd.zzcx("Error opening writable conversion tracking database");
            return null;
        }
    }

    public zzf zza(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        return new zzf(cursor.getLong(0), cursor.getString(1), cursor.getString(2));
    }

    public void zza(zzf zzf) {
        if (zzf != null) {
            synchronized (zzail) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    writableDatabase.delete("InAppPurchase", String.format(Locale.US, "%s = %d", new Object[]{"purchase_id", Long.valueOf(zzf.zzbxf)}), null);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    public void zzb(zzf zzf) {
        if (zzf != null) {
            synchronized (zzail) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Param.PRODUCT_ID, zzf.zzbxh);
                    contentValues.put("developer_payload", zzf.zzbxg);
                    contentValues.put("record_time", Long.valueOf(SystemClock.elapsedRealtime()));
                    zzf.zzbxf = writableDatabase.insert("InAppPurchase", null, contentValues);
                    if (((long) getRecordCount()) > 20000) {
                        zzpt();
                    }
                }
            }
        }
    }

    public List<zzf> zzg(long j) {
        Cursor cursor;
        synchronized (zzail) {
            LinkedList linkedList = new LinkedList();
            if (j <= 0) {
                return linkedList;
            }
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return linkedList;
            }
            try {
                cursor = writableDatabase.query("InAppPurchase", null, null, null, null, null, "record_time ASC", String.valueOf(j));
                try {
                    if (cursor.moveToFirst()) {
                        do {
                            linkedList.add(zza(cursor));
                        } while (cursor.moveToNext());
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (SQLiteException e) {
                    e = e;
                    String str = "Error extracing purchase info: ";
                    try {
                        String valueOf = String.valueOf(e.getMessage());
                        zzkd.zzcx(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                        if (cursor != null) {
                            cursor.close();
                        }
                        return linkedList;
                    } catch (Throwable th) {
                        th = th;
                    }
                }
            } catch (SQLiteException e2) {
                e = e2;
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
    }

    public void zzpt() {
        Cursor cursor;
        synchronized (zzail) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    cursor = writableDatabase.query("InAppPurchase", null, null, null, null, null, "record_time ASC", "1");
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                zza(zza(cursor));
                            }
                        } catch (SQLiteException e) {
                            e = e;
                            String str = "Error remove oldest record";
                            try {
                                String valueOf = String.valueOf(e.getMessage());
                                zzkd.zzcx(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                                if (cursor != null) {
                                    cursor.close();
                                }
                                return;
                            } catch (Throwable th) {
                                th = th;
                            }
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                    cursor = null;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        }
    }
}
