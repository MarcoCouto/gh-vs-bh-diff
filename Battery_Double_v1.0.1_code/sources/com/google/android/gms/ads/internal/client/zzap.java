package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.VideoController.VideoLifecycleCallbacks;
import com.google.android.gms.ads.internal.client.zzac.zza;

public final class zzap extends zza {
    private final VideoLifecycleCallbacks zzain;

    public zzap(VideoLifecycleCallbacks videoLifecycleCallbacks) {
        this.zzain = videoLifecycleCallbacks;
    }

    public void onVideoEnd() {
        this.zzain.onVideoEnd();
    }

    public void zzjb() {
    }

    public void zzjc() {
    }

    public void zzjd() {
    }
}
