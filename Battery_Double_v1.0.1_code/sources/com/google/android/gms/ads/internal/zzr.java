package com.google.android.gms.ads.internal;

import android.os.Handler;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import java.lang.ref.WeakReference;

@zzin
public class zzr {
    private final zza zzanb;
    /* access modifiers changed from: private */
    @Nullable
    public AdRequestParcel zzanc;
    /* access modifiers changed from: private */
    public boolean zzand;
    private boolean zzane;
    private long zzanf;
    private final Runnable zzw;

    public static class zza {
        private final Handler mHandler;

        public zza(Handler handler) {
            this.mHandler = handler;
        }

        public boolean postDelayed(Runnable runnable, long j) {
            return this.mHandler.postDelayed(runnable, j);
        }

        public void removeCallbacks(Runnable runnable) {
            this.mHandler.removeCallbacks(runnable);
        }
    }

    public zzr(zza zza2) {
        this(zza2, new zza(zzkh.zzclc));
    }

    zzr(zza zza2, zza zza3) {
        this.zzand = false;
        this.zzane = false;
        this.zzanf = 0;
        this.zzanb = zza3;
        final WeakReference weakReference = new WeakReference(zza2);
        this.zzw = new Runnable() {
            public void run() {
                zzr.this.zzand = false;
                zza zza = (zza) weakReference.get();
                if (zza != null) {
                    zza.zzd(zzr.this.zzanc);
                }
            }
        };
    }

    public void cancel() {
        this.zzand = false;
        this.zzanb.removeCallbacks(this.zzw);
    }

    public void pause() {
        this.zzane = true;
        if (this.zzand) {
            this.zzanb.removeCallbacks(this.zzw);
        }
    }

    public void resume() {
        this.zzane = false;
        if (this.zzand) {
            this.zzand = false;
            zza(this.zzanc, this.zzanf);
        }
    }

    public void zza(AdRequestParcel adRequestParcel, long j) {
        if (this.zzand) {
            zzkd.zzcx("An ad refresh is already scheduled.");
            return;
        }
        this.zzanc = adRequestParcel;
        this.zzand = true;
        this.zzanf = j;
        if (!this.zzane) {
            zzkd.zzcw("Scheduling ad refresh " + j + " milliseconds from now.");
            this.zzanb.postDelayed(this.zzw, j);
        }
    }

    public boolean zzfc() {
        return this.zzand;
    }

    public void zzg(AdRequestParcel adRequestParcel) {
        zza(adRequestParcel, 60000);
    }
}
