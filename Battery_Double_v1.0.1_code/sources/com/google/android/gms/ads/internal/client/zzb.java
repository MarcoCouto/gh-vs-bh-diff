package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.client.zzp.zza;
import com.google.android.gms.internal.zzin;

@zzin
public final class zzb extends zza {
    private final zza zzatk;

    public zzb(zza zza) {
        this.zzatk = zza;
    }

    public void onAdClicked() {
        this.zzatk.onAdClicked();
    }
}
