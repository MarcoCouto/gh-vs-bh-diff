package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

@zzin
public final class LargeParcelTeleporter extends AbstractSafeParcelable {
    public static final Creator<LargeParcelTeleporter> CREATOR = new zzm();
    final int mVersionCode;
    ParcelFileDescriptor zzccz;
    private Parcelable zzcda;
    private boolean zzcdb;

    LargeParcelTeleporter(int i, ParcelFileDescriptor parcelFileDescriptor) {
        this.mVersionCode = i;
        this.zzccz = parcelFileDescriptor;
        this.zzcda = null;
        this.zzcdb = true;
    }

    public LargeParcelTeleporter(SafeParcelable safeParcelable) {
        this.mVersionCode = 1;
        this.zzccz = null;
        this.zzcda = safeParcelable;
        this.zzcdb = false;
    }

    /* JADX INFO: finally extract failed */
    public void writeToParcel(Parcel parcel, int i) {
        if (this.zzccz == null) {
            Parcel obtain = Parcel.obtain();
            try {
                this.zzcda.writeToParcel(obtain, 0);
                byte[] marshall = obtain.marshall();
                obtain.recycle();
                this.zzccz = zzi(marshall);
            } catch (Throwable th) {
                obtain.recycle();
                throw th;
            }
        }
        zzm.zza(this, parcel, i);
    }

    /* JADX INFO: finally extract failed */
    public <T extends SafeParcelable> T zza(Creator<T> creator) {
        if (this.zzcdb) {
            if (this.zzccz == null) {
                zzkd.e("File descriptor is empty, returning null.");
                return null;
            }
            DataInputStream dataInputStream = new DataInputStream(new AutoCloseInputStream(this.zzccz));
            try {
                byte[] bArr = new byte[dataInputStream.readInt()];
                dataInputStream.readFully(bArr, 0, bArr.length);
                zzo.zzb(dataInputStream);
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.unmarshall(bArr, 0, bArr.length);
                    obtain.setDataPosition(0);
                    this.zzcda = (SafeParcelable) creator.createFromParcel(obtain);
                    obtain.recycle();
                    this.zzcdb = false;
                } catch (Throwable th) {
                    obtain.recycle();
                    throw th;
                }
            } catch (IOException e) {
                throw new IllegalStateException("Could not read from parcel file descriptor", e);
            } catch (Throwable th2) {
                zzo.zzb(dataInputStream);
                throw th2;
            }
        }
        return (SafeParcelable) this.zzcda;
    }

    /* access modifiers changed from: protected */
    public <T> ParcelFileDescriptor zzi(final byte[] bArr) {
        final AutoCloseOutputStream autoCloseOutputStream;
        AutoCloseOutputStream autoCloseOutputStream2 = null;
        try {
            ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
            autoCloseOutputStream = new AutoCloseOutputStream(createPipe[1]);
            try {
                new Thread(new Runnable() {
                    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
                    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
                    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
                    /* JADX WARNING: Removed duplicated region for block: B:19:0x003c  */
                    public void run() {
                        DataOutputStream dataOutputStream;
                        try {
                            dataOutputStream = new DataOutputStream(autoCloseOutputStream);
                            try {
                                dataOutputStream.writeInt(bArr.length);
                                dataOutputStream.write(bArr);
                                zzo.zzb(dataOutputStream);
                            } catch (IOException e) {
                                e = e;
                                try {
                                    zzkd.zzb("Error transporting the ad response", e);
                                    zzu.zzft().zzb((Throwable) e, true);
                                    if (dataOutputStream != null) {
                                        zzo.zzb(autoCloseOutputStream);
                                    } else {
                                        zzo.zzb(dataOutputStream);
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    if (dataOutputStream != null) {
                                        zzo.zzb(autoCloseOutputStream);
                                    } else {
                                        zzo.zzb(dataOutputStream);
                                    }
                                    throw th;
                                }
                            }
                        } catch (IOException e2) {
                            e = e2;
                            dataOutputStream = null;
                            zzkd.zzb("Error transporting the ad response", e);
                            zzu.zzft().zzb((Throwable) e, true);
                            if (dataOutputStream != null) {
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            dataOutputStream = null;
                            if (dataOutputStream != null) {
                            }
                            throw th;
                        }
                    }
                }).start();
                return createPipe[0];
            } catch (IOException e) {
                e = e;
            }
        } catch (IOException e2) {
            e = e2;
            autoCloseOutputStream = autoCloseOutputStream2;
            zzkd.zzb("Error transporting the ad response", e);
            zzu.zzft().zzb((Throwable) e, true);
            zzo.zzb(autoCloseOutputStream);
            return autoCloseOutputStream2;
        }
    }
}
