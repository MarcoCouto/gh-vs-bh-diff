package com.google.android.gms.ads.internal.reward.client;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.internal.zzin;

@zzin
public class zze implements RewardItem {
    private final zza zzchk;

    public zze(zza zza) {
        this.zzchk = zza;
    }

    public int getAmount() {
        int i = 0;
        if (this.zzchk == null) {
            return i;
        }
        try {
            return this.zzchk.getAmount();
        } catch (RemoteException e) {
            zzb.zzd("Could not forward getAmount to RewardItem", e);
            return i;
        }
    }

    public String getType() {
        String str = null;
        if (this.zzchk == null) {
            return str;
        }
        try {
            return this.zzchk.getType();
        } catch (RemoteException e) {
            zzb.zzd("Could not forward getType to RewardItem", e);
            return str;
        }
    }
}
