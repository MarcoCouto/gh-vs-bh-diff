package com.google.android.gms.ads.internal.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdt;
import com.google.android.gms.internal.zzef;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzhh;
import com.google.android.gms.internal.zzhi;
import com.google.android.gms.internal.zzhp;
import com.google.android.gms.internal.zzhu;
import com.google.android.gms.internal.zzin;
import com.google.firebase.analytics.FirebaseAnalytics.Event;

@zzin
public class zzl {
    private final Object zzail = new Object();
    private zzx zzauz;
    /* access modifiers changed from: private */
    public final zze zzava;
    /* access modifiers changed from: private */
    public final zzd zzavb;
    /* access modifiers changed from: private */
    public final zzai zzavc;
    /* access modifiers changed from: private */
    public final zzef zzavd;
    /* access modifiers changed from: private */
    public final zzf zzave;
    /* access modifiers changed from: private */
    public final zzhu zzavf;
    /* access modifiers changed from: private */
    public final zzhh zzavg;

    private abstract class zza<T> {
        private zza() {
        }

        /* access modifiers changed from: protected */
        @Nullable
        public abstract T zzb(zzx zzx) throws RemoteException;

        /* access modifiers changed from: protected */
        @Nullable
        public abstract T zzin();

        /* access modifiers changed from: protected */
        @Nullable
        public final T zziu() {
            T t = null;
            zzx zza = zzl.this.zzil();
            if (zza == null) {
                zzb.zzcx("ClientApi class cannot be loaded.");
                return t;
            }
            try {
                return zzb(zza);
            } catch (RemoteException e) {
                zzb.zzd("Cannot invoke local loader using ClientApi class", e);
                return t;
            }
        }
    }

    public zzl(zze zze, zzd zzd, zzai zzai, zzef zzef, zzf zzf, zzhu zzhu, zzhh zzhh) {
        this.zzava = zze;
        this.zzavb = zzd;
        this.zzavc = zzai;
        this.zzavd = zzef;
        this.zzave = zzf;
        this.zzavf = zzhu;
        this.zzavg = zzhh;
    }

    private <T> T zza(Context context, boolean z, zza<T> zza2) {
        if (!z && !zzm.zziw().zzar(context)) {
            zzb.zzcv("Google Play Services is not available");
            z = true;
        }
        if (z) {
            T zziu = zza2.zziu();
            return zziu == null ? zza2.zzin() : zziu;
        }
        T zzin = zza2.zzin();
        return zzin == null ? zza2.zziu() : zzin;
    }

    private static boolean zza(Activity activity, String str) {
        Intent intent = activity.getIntent();
        if (intent.hasExtra(str)) {
            return intent.getBooleanExtra(str, false);
        }
        zzb.e("useClientJar flag not found in activity intent extras.");
        return false;
    }

    /* access modifiers changed from: private */
    public void zzc(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("action", "no_ads_fallback");
        bundle.putString("flow", str);
        zzm.zziw().zza(context, (String) null, "gmob-apps", bundle, true);
    }

    @Nullable
    private static zzx zzik() {
        try {
            Object newInstance = zzl.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").newInstance();
            if (newInstance instanceof IBinder) {
                return com.google.android.gms.ads.internal.client.zzx.zza.asInterface((IBinder) newInstance);
            }
            zzb.zzcx("ClientApi class is not an instance of IBinder");
            return null;
        } catch (Exception e) {
            zzb.zzd("Failed to instantiate ClientApi class.", e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public zzx zzil() {
        zzx zzx;
        synchronized (this.zzail) {
            if (this.zzauz == null) {
                this.zzauz = zzik();
            }
            zzx = this.zzauz;
        }
        return zzx;
    }

    public zzu zza(final Context context, final AdSizeParcel adSizeParcel, final String str) {
        return (zzu) zza(context, false, (zza<T>) new zza<zzu>() {
            /* renamed from: zza */
            public zzu zzb(zzx zzx) throws RemoteException {
                return zzx.createSearchAdManager(zze.zzac(context), adSizeParcel, str, com.google.android.gms.common.internal.zze.xM);
            }

            /* renamed from: zzim */
            public zzu zzin() {
                zzu zza = zzl.this.zzava.zza(context, adSizeParcel, str, null, 3);
                if (zza != null) {
                    return zza;
                }
                zzl.this.zzc(context, Event.SEARCH);
                return new zzak();
            }
        });
    }

    public zzu zza(Context context, AdSizeParcel adSizeParcel, String str, zzgj zzgj) {
        final Context context2 = context;
        final AdSizeParcel adSizeParcel2 = adSizeParcel;
        final String str2 = str;
        final zzgj zzgj2 = zzgj;
        return (zzu) zza(context, false, (zza<T>) new zza<zzu>() {
            /* renamed from: zza */
            public zzu zzb(zzx zzx) throws RemoteException {
                return zzx.createBannerAdManager(zze.zzac(context2), adSizeParcel2, str2, zzgj2, com.google.android.gms.common.internal.zze.xM);
            }

            /* renamed from: zzim */
            public zzu zzin() {
                zzu zza = zzl.this.zzava.zza(context2, adSizeParcel2, str2, zzgj2, 1);
                if (zza != null) {
                    return zza;
                }
                zzl.this.zzc(context2, "banner");
                return new zzak();
            }
        });
    }

    public com.google.android.gms.ads.internal.reward.client.zzb zza(final Context context, final zzgj zzgj) {
        return (com.google.android.gms.ads.internal.reward.client.zzb) zza(context, false, (zza<T>) new zza<com.google.android.gms.ads.internal.reward.client.zzb>() {
            /* renamed from: zzf */
            public com.google.android.gms.ads.internal.reward.client.zzb zzb(zzx zzx) throws RemoteException {
                return zzx.createRewardedVideoAd(zze.zzac(context), zzgj, com.google.android.gms.common.internal.zze.xM);
            }

            /* renamed from: zzir */
            public com.google.android.gms.ads.internal.reward.client.zzb zzin() {
                com.google.android.gms.ads.internal.reward.client.zzb zzb = zzl.this.zzave.zzb(context, zzgj);
                if (zzb != null) {
                    return zzb;
                }
                zzl.this.zzc(context, "rewarded_video");
                return new zzan();
            }
        });
    }

    public zzdt zza(final Context context, final FrameLayout frameLayout, final FrameLayout frameLayout2) {
        return (zzdt) zza(context, false, (zza<T>) new zza<zzdt>() {
            /* renamed from: zze */
            public zzdt zzb(zzx zzx) throws RemoteException {
                return zzx.createNativeAdViewDelegate(zze.zzac(frameLayout), zze.zzac(frameLayout2));
            }

            /* renamed from: zziq */
            public zzdt zzin() {
                zzdt zzb = zzl.this.zzavd.zzb(context, frameLayout, frameLayout2);
                if (zzb != null) {
                    return zzb;
                }
                zzl.this.zzc(context, "native_ad_view_delegate");
                return new zzam();
            }
        });
    }

    public zzs zzb(final Context context, final String str, final zzgj zzgj) {
        return (zzs) zza(context, false, (zza<T>) new zza<zzs>() {
            /* renamed from: zzc */
            public zzs zzb(zzx zzx) throws RemoteException {
                return zzx.createAdLoaderBuilder(zze.zzac(context), str, zzgj, com.google.android.gms.common.internal.zze.xM);
            }

            /* renamed from: zzio */
            public zzs zzin() {
                zzs zza = zzl.this.zzavb.zza(context, str, zzgj);
                if (zza != null) {
                    return zza;
                }
                zzl.this.zzc(context, "native_ad");
                return new zzaj();
            }
        });
    }

    public zzu zzb(Context context, AdSizeParcel adSizeParcel, String str, zzgj zzgj) {
        final Context context2 = context;
        final AdSizeParcel adSizeParcel2 = adSizeParcel;
        final String str2 = str;
        final zzgj zzgj2 = zzgj;
        return (zzu) zza(context, false, (zza<T>) new zza<zzu>() {
            /* renamed from: zza */
            public zzu zzb(zzx zzx) throws RemoteException {
                return zzx.createInterstitialAdManager(zze.zzac(context2), adSizeParcel2, str2, zzgj2, com.google.android.gms.common.internal.zze.xM);
            }

            /* renamed from: zzim */
            public zzu zzin() {
                zzu zza = zzl.this.zzava.zza(context2, adSizeParcel2, str2, zzgj2, 2);
                if (zza != null) {
                    return zza;
                }
                zzl.this.zzc(context2, "interstitial");
                return new zzak();
            }
        });
    }

    @Nullable
    public zzhp zzb(final Activity activity) {
        return (zzhp) zza((Context) activity, zza(activity, "com.google.android.gms.ads.internal.purchase.useClientJar"), (zza<T>) new zza<zzhp>() {
            /* renamed from: zzg */
            public zzhp zzb(zzx zzx) throws RemoteException {
                return zzx.createInAppPurchaseManager(zze.zzac(activity));
            }

            /* renamed from: zzis */
            public zzhp zzin() {
                zzhp zzg = zzl.this.zzavf.zzg(activity);
                if (zzg != null) {
                    return zzg;
                }
                zzl.this.zzc(activity, "iap");
                return null;
            }
        });
    }

    @Nullable
    public zzhi zzc(final Activity activity) {
        return (zzhi) zza((Context) activity, zza(activity, "com.google.android.gms.ads.internal.overlay.useClientJar"), (zza<T>) new zza<zzhi>() {
            /* renamed from: zzh */
            public zzhi zzb(zzx zzx) throws RemoteException {
                return zzx.createAdOverlay(zze.zzac(activity));
            }

            /* renamed from: zzit */
            public zzhi zzin() {
                zzhi zzf = zzl.this.zzavg.zzf(activity);
                if (zzf != null) {
                    return zzf;
                }
                zzl.this.zzc(activity, "ad_overlay");
                return null;
            }
        });
    }

    public zzz zzl(final Context context) {
        return (zzz) zza(context, false, (zza<T>) new zza<zzz>() {
            /* renamed from: zzd */
            public zzz zzb(zzx zzx) throws RemoteException {
                return zzx.getMobileAdsSettingsManagerWithClientJarVersion(zze.zzac(context), com.google.android.gms.common.internal.zze.xM);
            }

            /* renamed from: zzip */
            public zzz zzin() {
                zzz zzm = zzl.this.zzavc.zzm(context);
                if (zzm != null) {
                    return zzm;
                }
                zzl.this.zzc(context, "mobile_ads_settings");
                return new zzal();
            }
        });
    }
}
