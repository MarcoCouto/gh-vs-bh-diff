package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzz.zza;

public class zzal extends zza {
    public void initialize() throws RemoteException {
    }

    public void setAppMuted(boolean z) throws RemoteException {
    }

    public void setAppVolume(float f) throws RemoteException {
    }

    public void zzu(String str) throws RemoteException {
    }
}
