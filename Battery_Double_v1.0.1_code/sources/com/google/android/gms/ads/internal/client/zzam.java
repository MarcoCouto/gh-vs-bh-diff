package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.internal.zzdt.zza;

public class zzam extends zza {
    public void destroy() throws RemoteException {
    }

    public zzd zzap(String str) throws RemoteException {
        return null;
    }

    public void zzc(String str, zzd zzd) throws RemoteException {
    }

    public void zze(zzd zzd) throws RemoteException {
    }
}
