package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzfs;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjw;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import com.google.android.gms.internal.zzla.zzb;
import com.google.android.gms.internal.zzla.zzc;
import com.google.android.gms.internal.zzlh;
import java.util.Map;
import org.json.JSONObject;

@zzin
public class zzg {
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public final Object zzail = new Object();
    public final zzep zzaku = new zzep() {
        public void zza(zzlh zzlh, Map<String, String> map) {
            zzlh.zzb("/appSettingsFetched", (zzep) this);
            synchronized (zzg.this.zzail) {
                if (map != null) {
                    if ("true".equalsIgnoreCase((String) map.get("isSuccessful"))) {
                        zzu.zzft().zzd(zzg.this.mContext, (String) map.get("appSettingsJson"));
                    }
                }
            }
        }
    };

    private static boolean zza(@Nullable zzjw zzjw) {
        if (zzjw == null) {
            return true;
        }
        return (((zzu.zzfu().currentTimeMillis() - zzjw.zzse()) > ((Long) zzdc.zzbcv.get()).longValue() ? 1 : ((zzu.zzfu().currentTimeMillis() - zzjw.zzse()) == ((Long) zzdc.zzbcv.get()).longValue() ? 0 : -1)) > 0) || !zzjw.zzsf();
    }

    public void zza(Context context, VersionInfoParcel versionInfoParcel, boolean z, @Nullable zzjw zzjw, String str, @Nullable String str2) {
        if (zza(zzjw)) {
            if (context == null) {
                zzkd.zzcx("Context not provided to fetch application settings");
            } else if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
                this.mContext = context;
                final zzfs zzc = zzu.zzfq().zzc(context, versionInfoParcel);
                final String str3 = str;
                final String str4 = str2;
                final boolean z2 = z;
                final Context context2 = context;
                zzkh.zzclc.post(new Runnable() {
                    public void run() {
                        zzc.zzma().zza(new zzc<zzft>() {
                            /* renamed from: zzb */
                            public void zzd(zzft zzft) {
                                zzft.zza("/appSettingsFetched", zzg.this.zzaku);
                                try {
                                    JSONObject jSONObject = new JSONObject();
                                    if (!TextUtils.isEmpty(str3)) {
                                        jSONObject.put("app_id", str3);
                                    } else if (!TextUtils.isEmpty(str4)) {
                                        jSONObject.put("ad_unit_id", str4);
                                    }
                                    jSONObject.put("is_init", z2);
                                    jSONObject.put("pn", context2.getPackageName());
                                    zzft.zza("AFMA_fetchAppSettings", jSONObject);
                                } catch (Exception e) {
                                    zzft.zzb("/appSettingsFetched", zzg.this.zzaku);
                                    zzkd.zzb("Error requesting application settings", e);
                                }
                            }
                        }, new zzb());
                    }
                });
            } else {
                zzkd.zzcx("App settings could not be fetched. Required parameters missing");
            }
        }
    }
}
