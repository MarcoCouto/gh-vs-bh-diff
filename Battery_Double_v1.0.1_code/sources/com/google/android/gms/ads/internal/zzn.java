package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.ads.internal.formats.zze;
import com.google.android.gms.ads.internal.zzf.zza;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzge;
import com.google.android.gms.internal.zzgn;
import com.google.android.gms.internal.zzgo;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzju;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zzli;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzn {
    private static zzd zza(zzgn zzgn) throws RemoteException {
        return new zzd(zzgn.getHeadline(), zzgn.getImages(), zzgn.getBody(), zzgn.zzku(), zzgn.getCallToAction(), zzgn.getStarRating(), zzgn.getStore(), zzgn.getPrice(), null, zzgn.getExtras());
    }

    private static zze zza(zzgo zzgo) throws RemoteException {
        return new zze(zzgo.getHeadline(), zzgo.getImages(), zzgo.getBody(), zzgo.zzky(), zzgo.getCallToAction(), zzgo.getAdvertiser(), null, zzgo.getExtras());
    }

    static zzep zza(@Nullable final zzgn zzgn, @Nullable final zzgo zzgo, final zza zza) {
        return new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                View view = zzlh.getView();
                if (view != null) {
                    try {
                        if (zzgn.this != null) {
                            if (!zzgn.this.getOverrideClickHandling()) {
                                zzgn.this.zzk(com.google.android.gms.dynamic.zze.zzac(view));
                                zza.onClick();
                                return;
                            }
                            zzn.zza(zzlh);
                        } else if (zzgo == null) {
                        } else {
                            if (!zzgo.getOverrideClickHandling()) {
                                zzgo.zzk(com.google.android.gms.dynamic.zze.zzac(view));
                                zza.onClick();
                                return;
                            }
                            zzn.zza(zzlh);
                        }
                    } catch (RemoteException e) {
                        zzkd.zzd("Unable to call handleClick on mapper", e);
                    }
                }
            }
        };
    }

    static zzep zza(final CountDownLatch countDownLatch) {
        return new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                countDownLatch.countDown();
                zzlh.getView().setVisibility(0);
            }
        };
    }

    private static String zza(@Nullable Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap == null) {
            zzkd.zzcx("Bitmap is null. Returning empty string");
            return "";
        }
        bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        String valueOf = String.valueOf("data:image/png;base64,");
        String valueOf2 = String.valueOf(encodeToString);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    static String zza(@Nullable zzdr zzdr) {
        if (zzdr == null) {
            zzkd.zzcx("Image is null. Returning empty string");
            return "";
        }
        try {
            Uri uri = zzdr.getUri();
            if (uri != null) {
                return uri.toString();
            }
        } catch (RemoteException e) {
            zzkd.zzcx("Unable to get image uri. Trying data uri next");
        }
        return zzb(zzdr);
    }

    /* access modifiers changed from: private */
    public static JSONObject zza(@Nullable Bundle bundle, String str) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (bundle == null || TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str2 = (String) keys.next();
            if (bundle.containsKey(str2)) {
                if ("image".equals(jSONObject2.getString(str2))) {
                    Object obj = bundle.get(str2);
                    if (obj instanceof Bitmap) {
                        jSONObject.put(str2, zza((Bitmap) obj));
                    } else {
                        zzkd.zzcx("Invalid type. An image type extra should return a bitmap");
                    }
                } else if (bundle.get(str2) instanceof Bitmap) {
                    zzkd.zzcx("Invalid asset type. Bitmap should be returned only for image type");
                } else {
                    jSONObject.put(str2, String.valueOf(bundle.get(str2)));
                }
            }
        }
        return jSONObject;
    }

    public static void zza(@Nullable zzju zzju, zza zza) {
        zzgo zzgo = null;
        if (zzju != null && zzg(zzju)) {
            zzlh zzlh = zzju.zzbtm;
            Object obj = zzlh != null ? zzlh.getView() : null;
            if (obj == null) {
                zzkd.zzcx("AdWebView is null");
                return;
            }
            try {
                List list = zzju.zzbon != null ? zzju.zzbon.zzbni : null;
                if (list == null || list.isEmpty()) {
                    zzkd.zzcx("No template ids present in mediation response");
                    return;
                }
                zzgn zzgn = zzju.zzboo != null ? zzju.zzboo.zzmo() : null;
                if (zzju.zzboo != null) {
                    zzgo = zzju.zzboo.zzmp();
                }
                if (list.contains("2") && zzgn != null) {
                    zzgn.zzl(com.google.android.gms.dynamic.zze.zzac(obj));
                    if (!zzgn.getOverrideImpressionRecording()) {
                        zzgn.recordImpression();
                    }
                    zzlh.zzuj().zza("/nativeExpressViewClicked", zza(zzgn, (zzgo) null, zza));
                } else if (!list.contains("1") || zzgo == null) {
                    zzkd.zzcx("No matching template id and mapper");
                } else {
                    zzgo.zzl(com.google.android.gms.dynamic.zze.zzac(obj));
                    if (!zzgo.getOverrideImpressionRecording()) {
                        zzgo.recordImpression();
                    }
                    zzlh.zzuj().zza("/nativeExpressViewClicked", zza((zzgn) null, zzgo, zza));
                }
            } catch (RemoteException e) {
                zzkd.zzd("Error occurred while recording impression and registering for clicks", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void zza(zzlh zzlh) {
        OnClickListener zzuw = zzlh.zzuw();
        if (zzuw != null) {
            zzuw.onClick(zzlh.getView());
        }
    }

    private static void zza(final zzlh zzlh, final zzd zzd, final String str) {
        zzlh.zzuj().zza((zzli.zza) new zzli.zza() {
            public void zza(zzlh zzlh, boolean z) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("headline", zzd.this.getHeadline());
                    jSONObject.put("body", zzd.this.getBody());
                    jSONObject.put("call_to_action", zzd.this.getCallToAction());
                    jSONObject.put(Param.PRICE, zzd.this.getPrice());
                    jSONObject.put("star_rating", String.valueOf(zzd.this.getStarRating()));
                    jSONObject.put("store", zzd.this.getStore());
                    jSONObject.put("icon", zzn.zza(zzd.this.zzku()));
                    JSONArray jSONArray = new JSONArray();
                    List<Object> images = zzd.this.getImages();
                    if (images != null) {
                        for (Object zzf : images) {
                            jSONArray.put(zzn.zza(zzn.zze(zzf)));
                        }
                    }
                    jSONObject.put("images", jSONArray);
                    jSONObject.put("extras", zzn.zza(zzd.this.getExtras(), str));
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("assets", jSONObject);
                    jSONObject2.put("template_id", "2");
                    zzlh.zza("google.afma.nativeExpressAds.loadAssets", jSONObject2);
                } catch (JSONException e) {
                    zzkd.zzd("Exception occurred when loading assets", e);
                }
            }
        });
    }

    private static void zza(final zzlh zzlh, final zze zze, final String str) {
        zzlh.zzuj().zza((zzli.zza) new zzli.zza() {
            public void zza(zzlh zzlh, boolean z) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("headline", zze.this.getHeadline());
                    jSONObject.put("body", zze.this.getBody());
                    jSONObject.put("call_to_action", zze.this.getCallToAction());
                    jSONObject.put("advertiser", zze.this.getAdvertiser());
                    jSONObject.put("logo", zzn.zza(zze.this.zzky()));
                    JSONArray jSONArray = new JSONArray();
                    List<Object> images = zze.this.getImages();
                    if (images != null) {
                        for (Object zzf : images) {
                            jSONArray.put(zzn.zza(zzn.zze(zzf)));
                        }
                    }
                    jSONObject.put("images", jSONArray);
                    jSONObject.put("extras", zzn.zza(zze.this.getExtras(), str));
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("assets", jSONObject);
                    jSONObject2.put("template_id", "1");
                    zzlh.zza("google.afma.nativeExpressAds.loadAssets", jSONObject2);
                } catch (JSONException e) {
                    zzkd.zzd("Exception occurred when loading assets", e);
                }
            }
        });
    }

    private static void zza(zzlh zzlh, CountDownLatch countDownLatch) {
        zzlh.zzuj().zza("/nativeExpressAssetsLoaded", zza(countDownLatch));
        zzlh.zzuj().zza("/nativeExpressAssetsLoadingFailed", zzb(countDownLatch));
    }

    public static boolean zza(zzlh zzlh, zzge zzge, CountDownLatch countDownLatch) {
        boolean z = false;
        try {
            z = zzb(zzlh, zzge, countDownLatch);
        } catch (RemoteException e) {
            zzkd.zzd("Unable to invoke load assets", e);
        } catch (RuntimeException e2) {
            countDownLatch.countDown();
            throw e2;
        }
        if (!z) {
            countDownLatch.countDown();
        }
        return z;
    }

    static zzep zzb(final CountDownLatch countDownLatch) {
        return new zzep() {
            public void zza(zzlh zzlh, Map<String, String> map) {
                zzkd.zzcx("Adapter returned an ad, but assets substitution failed");
                countDownLatch.countDown();
                zzlh.destroy();
            }
        };
    }

    private static String zzb(zzdr zzdr) {
        try {
            com.google.android.gms.dynamic.zzd zzkt = zzdr.zzkt();
            if (zzkt == null) {
                zzkd.zzcx("Drawable is null. Returning empty string");
                return "";
            }
            Drawable drawable = (Drawable) com.google.android.gms.dynamic.zze.zzad(zzkt);
            if (drawable instanceof BitmapDrawable) {
                return zza(((BitmapDrawable) drawable).getBitmap());
            }
            zzkd.zzcx("Drawable is not an instance of BitmapDrawable. Returning empty string");
            return "";
        } catch (RemoteException e) {
            zzkd.zzcx("Unable to get drawable. Returning empty string");
            return "";
        }
    }

    private static boolean zzb(zzlh zzlh, zzge zzge, CountDownLatch countDownLatch) throws RemoteException {
        View view = zzlh.getView();
        if (view == null) {
            zzkd.zzcx("AdWebView is null");
            return false;
        }
        view.setVisibility(4);
        List<String> list = zzge.zzbon.zzbni;
        if (list == null || list.isEmpty()) {
            zzkd.zzcx("No template ids present in mediation response");
            return false;
        }
        zza(zzlh, countDownLatch);
        zzgn zzmo = zzge.zzboo.zzmo();
        zzgo zzmp = zzge.zzboo.zzmp();
        if (list.contains("2") && zzmo != null) {
            zza(zzlh, zza(zzmo), zzge.zzbon.zzbnh);
        } else if (!list.contains("1") || zzmp == null) {
            zzkd.zzcx("No matching template id and mapper");
            return false;
        } else {
            zza(zzlh, zza(zzmp), zzge.zzbon.zzbnh);
        }
        String str = zzge.zzbon.zzbnf;
        String str2 = zzge.zzbon.zzbng;
        if (str2 != null) {
            zzlh.loadDataWithBaseURL(str2, str, "text/html", "UTF-8", null);
        } else {
            zzlh.loadData(str, "text/html", "UTF-8");
        }
        return true;
    }

    /* access modifiers changed from: private */
    @Nullable
    public static zzdr zze(Object obj) {
        if (obj instanceof IBinder) {
            return zzdr.zza.zzy((IBinder) obj);
        }
        return null;
    }

    @Nullable
    public static View zzf(@Nullable zzju zzju) {
        if (zzju == null) {
            zzkd.e("AdState is null");
            return null;
        } else if (zzg(zzju) && zzju.zzbtm != null) {
            return zzju.zzbtm.getView();
        } else {
            try {
                com.google.android.gms.dynamic.zzd zzd = zzju.zzboo != null ? zzju.zzboo.getView() : null;
                if (zzd != null) {
                    return (View) com.google.android.gms.dynamic.zze.zzad(zzd);
                }
                zzkd.zzcx("View in mediation adapter is null.");
                return null;
            } catch (RemoteException e) {
                zzkd.zzd("Could not get View from mediation adapter.", e);
                return null;
            }
        }
    }

    public static boolean zzg(@Nullable zzju zzju) {
        return (zzju == null || !zzju.zzcby || zzju.zzbon == null || zzju.zzbon.zzbnf == null) ? false : true;
    }
}
