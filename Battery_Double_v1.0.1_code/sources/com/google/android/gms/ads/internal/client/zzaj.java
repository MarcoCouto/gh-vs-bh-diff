package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzeb;
import com.google.android.gms.internal.zzec;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzee;

public class zzaj extends com.google.android.gms.ads.internal.client.zzs.zza {
    /* access modifiers changed from: private */
    public zzq zzalf;

    private class zza extends com.google.android.gms.ads.internal.client.zzr.zza {
        private zza() {
        }

        public String getMediationAdapterClassName() throws RemoteException {
            return null;
        }

        public boolean isLoading() throws RemoteException {
            return false;
        }

        public void zzf(AdRequestParcel adRequestParcel) throws RemoteException {
            zzb.e("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
            com.google.android.gms.ads.internal.util.client.zza.zzcnb.post(new Runnable() {
                public void run() {
                    if (zzaj.this.zzalf != null) {
                        try {
                            zzaj.this.zzalf.onAdFailedToLoad(1);
                        } catch (RemoteException e) {
                            zzb.zzd("Could not notify onAdFailedToLoad event.", e);
                        }
                    }
                }
            });
        }
    }

    public void zza(NativeAdOptionsParcel nativeAdOptionsParcel) throws RemoteException {
    }

    public void zza(zzeb zzeb) throws RemoteException {
    }

    public void zza(zzec zzec) throws RemoteException {
    }

    public void zza(String str, zzee zzee, zzed zzed) throws RemoteException {
    }

    public void zzb(zzq zzq) throws RemoteException {
        this.zzalf = zzq;
    }

    public void zzb(zzy zzy) throws RemoteException {
    }

    public zzr zzes() throws RemoteException {
        return new zza();
    }
}
