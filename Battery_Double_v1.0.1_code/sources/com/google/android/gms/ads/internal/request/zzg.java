package com.google.android.gms.ads.internal.request;

import com.google.android.gms.ads.internal.request.zzl.zza;
import com.google.android.gms.internal.zzin;
import java.lang.ref.WeakReference;

@zzin
public final class zzg extends zza {
    private final WeakReference<zzc.zza> zzcbw;

    public zzg(zzc.zza zza) {
        this.zzcbw = new WeakReference<>(zza);
    }

    public void zzb(AdResponseParcel adResponseParcel) {
        zzc.zza zza = (zzc.zza) this.zzcbw.get();
        if (zza != null) {
            zza.zzb(adResponseParcel);
        }
    }
}
