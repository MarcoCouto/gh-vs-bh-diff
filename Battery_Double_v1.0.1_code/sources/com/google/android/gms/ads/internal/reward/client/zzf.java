package com.google.android.gms.ads.internal.reward.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.reward.client.zzb.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.internal.zzgj;
import com.google.android.gms.internal.zzin;

@zzin
public class zzf extends zzg<zzc> {
    public zzf() {
        super("com.google.android.gms.ads.reward.RewardedVideoAdCreatorImpl");
    }

    public zzb zzb(Context context, zzgj zzgj) {
        try {
            return zza.zzbf(((zzc) zzcr(context)).zza(zze.zzac(context), zzgj, com.google.android.gms.common.internal.zze.xM));
        } catch (RemoteException | zzg.zza e) {
            zzb.zzd("Could not get remote RewardedVideoAd.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzbi */
    public zzc zzc(IBinder iBinder) {
        return zzc.zza.zzbg(iBinder);
    }
}
