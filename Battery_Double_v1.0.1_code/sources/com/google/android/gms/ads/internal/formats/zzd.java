package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdv.zza;
import com.google.android.gms.internal.zzin;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.List;

@zzin
public class zzd extends zza implements zzh.zza {
    private Bundle mExtras;
    private Object zzail = new Object();
    private String zzbfg;
    private List<zzc> zzbfh;
    private String zzbfi;
    private zzdr zzbfj;
    private String zzbfk;
    private double zzbfl;
    private String zzbfm;
    private String zzbfn;
    @Nullable
    private zza zzbfo;
    private zzh zzbfp;

    public zzd(String str, List list, String str2, zzdr zzdr, String str3, double d, String str4, String str5, @Nullable zza zza, Bundle bundle) {
        this.zzbfg = str;
        this.zzbfh = list;
        this.zzbfi = str2;
        this.zzbfj = zzdr;
        this.zzbfk = str3;
        this.zzbfl = d;
        this.zzbfm = str4;
        this.zzbfn = str5;
        this.zzbfo = zza;
        this.mExtras = bundle;
    }

    public void destroy() {
        this.zzbfg = null;
        this.zzbfh = null;
        this.zzbfi = null;
        this.zzbfj = null;
        this.zzbfk = null;
        this.zzbfl = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        this.zzbfm = null;
        this.zzbfn = null;
        this.zzbfo = null;
        this.mExtras = null;
        this.zzail = null;
        this.zzbfp = null;
    }

    public String getBody() {
        return this.zzbfi;
    }

    public String getCallToAction() {
        return this.zzbfk;
    }

    public String getCustomTemplateId() {
        return "";
    }

    public Bundle getExtras() {
        return this.mExtras;
    }

    public String getHeadline() {
        return this.zzbfg;
    }

    public List getImages() {
        return this.zzbfh;
    }

    public String getPrice() {
        return this.zzbfn;
    }

    public double getStarRating() {
        return this.zzbfl;
    }

    public String getStore() {
        return this.zzbfm;
    }

    public void zzb(zzh zzh) {
        synchronized (this.zzail) {
            this.zzbfp = zzh;
        }
    }

    public zzdr zzku() {
        return this.zzbfj;
    }

    public com.google.android.gms.dynamic.zzd zzkv() {
        return zze.zzac(this.zzbfp);
    }

    public String zzkw() {
        return "2";
    }

    public zza zzkx() {
        return this.zzbfo;
    }
}
