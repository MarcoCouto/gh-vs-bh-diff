package com.google.android.gms.ads.internal.purchase;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.stats.zzb;
import com.google.android.gms.internal.zzhs;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzkh;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@zzin
public class zzc extends zzkc implements ServiceConnection {
    /* access modifiers changed from: private */
    public Context mContext;
    private final Object zzail;
    /* access modifiers changed from: private */
    public zzhs zzbld;
    private boolean zzbwr;
    private zzb zzbws;
    private zzh zzbwt;
    private List<zzf> zzbwu;
    /* access modifiers changed from: private */
    public zzk zzbwv;

    public zzc(Context context, zzhs zzhs, zzk zzk) {
        this(context, zzhs, zzk, new zzb(context), zzh.zzs(context.getApplicationContext()));
    }

    zzc(Context context, zzhs zzhs, zzk zzk, zzb zzb, zzh zzh) {
        this.zzail = new Object();
        this.zzbwr = false;
        this.zzbwu = null;
        this.mContext = context;
        this.zzbld = zzhs;
        this.zzbwv = zzk;
        this.zzbws = zzb;
        this.zzbwt = zzh;
        this.zzbwu = this.zzbwt.zzg(10);
    }

    private void zze(long j) {
        do {
            if (!zzf(j)) {
                zzkd.v("Timeout waiting for pending transaction to be processed.");
            }
        } while (!this.zzbwr);
    }

    private boolean zzf(long j) {
        long elapsedRealtime = 60000 - (SystemClock.elapsedRealtime() - j);
        if (elapsedRealtime <= 0) {
            return false;
        }
        try {
            this.zzail.wait(elapsedRealtime);
        } catch (InterruptedException e) {
            zzkd.zzcx("waitWithTimeout_lock interrupted");
        }
        return true;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.zzail) {
            this.zzbws.zzas(iBinder);
            zzpq();
            this.zzbwr = true;
            this.zzail.notify();
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        zzkd.zzcw("In-app billing service disconnected.");
        this.zzbws.destroy();
    }

    public void onStop() {
        synchronized (this.zzail) {
            zzb.zzaux().zza(this.mContext, (ServiceConnection) this);
            this.zzbws.destroy();
        }
    }

    /* access modifiers changed from: protected */
    public void zza(final zzf zzf, String str, String str2) {
        final Intent intent = new Intent();
        zzu.zzga();
        intent.putExtra("RESPONSE_CODE", 0);
        zzu.zzga();
        intent.putExtra("INAPP_PURCHASE_DATA", str);
        zzu.zzga();
        intent.putExtra("INAPP_DATA_SIGNATURE", str2);
        zzkh.zzclc.post(new Runnable() {
            public void run() {
                try {
                    if (zzc.this.zzbwv.zza(zzf.zzbxg, -1, intent)) {
                        zzc.this.zzbld.zza(new zzg(zzc.this.mContext, zzf.zzbxh, true, -1, intent, zzf));
                    } else {
                        zzc.this.zzbld.zza(new zzg(zzc.this.mContext, zzf.zzbxh, false, -1, intent, zzf));
                    }
                } catch (RemoteException e) {
                    zzkd.zzcx("Fail to verify and dispatch pending transaction");
                }
            }
        });
    }

    public void zzew() {
        synchronized (this.zzail) {
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            zzb.zzaux().zza(this.mContext, intent, (ServiceConnection) this, 1);
            zze(SystemClock.elapsedRealtime());
            zzb.zzaux().zza(this.mContext, (ServiceConnection) this);
            this.zzbws.destroy();
        }
    }

    /* access modifiers changed from: protected */
    public void zzpq() {
        if (!this.zzbwu.isEmpty()) {
            HashMap hashMap = new HashMap();
            for (zzf zzf : this.zzbwu) {
                hashMap.put(zzf.zzbxh, zzf);
            }
            String str = null;
            while (true) {
                Bundle zzn = this.zzbws.zzn(this.mContext.getPackageName(), str);
                if (zzn == null || zzu.zzga().zze(zzn) != 0) {
                    break;
                }
                ArrayList stringArrayList = zzn.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList stringArrayList2 = zzn.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList stringArrayList3 = zzn.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                String string = zzn.getString("INAPP_CONTINUATION_TOKEN");
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= stringArrayList.size()) {
                        break;
                    }
                    if (hashMap.containsKey(stringArrayList.get(i2))) {
                        String str2 = (String) stringArrayList.get(i2);
                        String str3 = (String) stringArrayList2.get(i2);
                        String str4 = (String) stringArrayList3.get(i2);
                        zzf zzf2 = (zzf) hashMap.get(str2);
                        if (zzf2.zzbxg.equals(zzu.zzga().zzby(str3))) {
                            zza(zzf2, str3, str4);
                            hashMap.remove(str2);
                        }
                    }
                    i = i2 + 1;
                }
                if (string == null || hashMap.isEmpty()) {
                    break;
                }
                str = string;
            }
            for (String str5 : hashMap.keySet()) {
                this.zzbwt.zza((zzf) hashMap.get(str5));
            }
        }
    }
}
