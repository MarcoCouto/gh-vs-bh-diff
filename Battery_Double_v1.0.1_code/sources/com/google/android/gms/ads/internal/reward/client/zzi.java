package com.google.android.gms.ads.internal.reward.client;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.internal.client.zzh;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzin;

@zzin
public class zzi implements RewardedVideoAd {
    private final Context mContext;
    private final Object zzail = new Object();
    private final zzb zzchl;
    private RewardedVideoAdListener zzfh;

    public zzi(Context context, zzb zzb) {
        this.zzchl = zzb;
        this.mContext = context;
    }

    public void destroy() {
        destroy(null);
    }

    public void destroy(Context context) {
        synchronized (this.zzail) {
            if (this.zzchl != null) {
                try {
                    this.zzchl.zzh(zze.zzac(context));
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward destroy to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public RewardedVideoAdListener getRewardedVideoAdListener() {
        RewardedVideoAdListener rewardedVideoAdListener;
        synchronized (this.zzail) {
            rewardedVideoAdListener = this.zzfh;
        }
        return rewardedVideoAdListener;
    }

    public String getUserId() {
        zzb.zzcx("RewardedVideoAd.getUserId() is deprecated. Please do not call this method.");
        return null;
    }

    public boolean isLoaded() {
        boolean z = false;
        synchronized (this.zzail) {
            if (this.zzchl != null) {
                try {
                    z = this.zzchl.isLoaded();
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward isLoaded to RewardedVideoAd", e);
                }
            }
        }
        return z;
    }

    public void loadAd(String str, AdRequest adRequest) {
        synchronized (this.zzail) {
            if (this.zzchl != null) {
                try {
                    this.zzchl.zza(zzh.zzih().zza(this.mContext, adRequest.zzdc(), str));
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward loadAd to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public void pause() {
        pause(null);
    }

    public void pause(Context context) {
        synchronized (this.zzail) {
            if (this.zzchl != null) {
                try {
                    this.zzchl.zzf(zze.zzac(context));
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward pause to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public void resume() {
        resume(null);
    }

    public void resume(Context context) {
        synchronized (this.zzail) {
            if (this.zzchl != null) {
                try {
                    this.zzchl.zzg(zze.zzac(context));
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward resume to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }

    public void setRewardedVideoAdListener(RewardedVideoAdListener rewardedVideoAdListener) {
        synchronized (this.zzail) {
            this.zzfh = rewardedVideoAdListener;
            if (this.zzchl != null) {
                try {
                    this.zzchl.zza((zzd) new zzg(rewardedVideoAdListener));
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward setRewardedVideoAdListener to RewardedVideoAd", e);
                }
            }
        }
    }

    public void setUserId(String str) {
        zzb.zzcx("RewardedVideoAd.setUserId() is deprecated. Please do not call this method.");
    }

    public void show() {
        synchronized (this.zzail) {
            if (this.zzchl != null) {
                try {
                    this.zzchl.show();
                } catch (RemoteException e) {
                    zzb.zzd("Could not forward show to RewardedVideoAd", e);
                }
                return;
            }
            return;
        }
    }
}
