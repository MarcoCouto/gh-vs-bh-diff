package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzas;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zzli;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

@zzin
public class zzi implements zzh {
    private final Context mContext;
    private final Object zzail = new Object();
    @Nullable
    private final VersionInfoParcel zzalo;
    private final zzq zzbfx;
    @Nullable
    private final JSONObject zzbga;
    @Nullable
    private final zzih zzbgb;
    @Nullable
    private final zza zzbgc;
    private final zzas zzbgd;
    private boolean zzbge;
    /* access modifiers changed from: private */
    public zzlh zzbgf;
    /* access modifiers changed from: private */
    public String zzbgg;
    @Nullable
    private String zzbgh;
    private WeakReference<View> zzbgi = null;

    public zzi(Context context, zzq zzq, @Nullable zzih zzih, zzas zzas, @Nullable JSONObject jSONObject, @Nullable zza zza, @Nullable VersionInfoParcel versionInfoParcel, @Nullable String str) {
        this.mContext = context;
        this.zzbfx = zzq;
        this.zzbgb = zzih;
        this.zzbgd = zzas;
        this.zzbga = jSONObject;
        this.zzbgc = zza;
        this.zzalo = versionInfoParcel;
        this.zzbgh = str;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void recordImpression() {
        zzab.zzhi("recordImpression must be called on the main UI thread.");
        zzq(true);
        try {
            final JSONObject jSONObject = new JSONObject();
            jSONObject.put("ad", this.zzbga);
            jSONObject.put("ads_id", this.zzbgh);
            this.zzbgb.zza((zzih.zza) new zzih.zza() {
                public void zze(zzft zzft) {
                    zzft.zza("google.afma.nativeAds.handleImpressionPing", jSONObject);
                }
            });
        } catch (JSONException e) {
            zzkd.zzb("Unable to create impression JSON.", e);
        }
        this.zzbfx.zza((zzh) this);
    }

    public zzb zza(OnClickListener onClickListener) {
        zza zzkx = this.zzbgc.zzkx();
        if (zzkx == null) {
            return null;
        }
        zzb zzb = new zzb(this.mContext, zzkx);
        zzb.setLayoutParams(new LayoutParams(-1, -1));
        zzb.zzks().setOnClickListener(onClickListener);
        zzb.zzks().setContentDescription("Ad attribution icon");
        return zzb;
    }

    public void zza(View view, Map<String, WeakReference<View>> map, OnTouchListener onTouchListener, OnClickListener onClickListener) {
        if (((Boolean) zzdc.zzbci.get()).booleanValue()) {
            view.setOnTouchListener(onTouchListener);
            view.setClickable(true);
            view.setOnClickListener(onClickListener);
            for (Entry value : map.entrySet()) {
                View view2 = (View) ((WeakReference) value.getValue()).get();
                if (view2 != null) {
                    view2.setOnTouchListener(onTouchListener);
                    view2.setClickable(true);
                    view2.setOnClickListener(onClickListener);
                }
            }
        }
    }

    public void zza(View view, Map<String, WeakReference<View>> map, JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3) {
        zzab.zzhi("performClick must be called on the main UI thread.");
        for (Entry entry : map.entrySet()) {
            if (view.equals((View) ((WeakReference) entry.getValue()).get())) {
                zza((String) entry.getKey(), jSONObject, jSONObject2, jSONObject3);
                return;
            }
        }
        if ("2".equals(this.zzbgc.zzkw())) {
            zza("2099", jSONObject, jSONObject2, jSONObject3);
        } else if ("1".equals(this.zzbgc.zzkw())) {
            zza("1099", jSONObject, jSONObject2, jSONObject3);
        }
    }

    public void zza(String str, @Nullable JSONObject jSONObject, @Nullable JSONObject jSONObject2, @Nullable JSONObject jSONObject3) {
        zzab.zzhi("performClick must be called on the main UI thread.");
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("asset", str);
            jSONObject4.put("template", this.zzbgc.zzkw());
            final JSONObject jSONObject5 = new JSONObject();
            jSONObject5.put("ad", this.zzbga);
            jSONObject5.put("click", jSONObject4);
            jSONObject5.put("has_custom_click_handler", this.zzbfx.zzv(this.zzbgc.getCustomTemplateId()) != null);
            if (jSONObject != null) {
                jSONObject5.put("view_rectangles", jSONObject);
            }
            if (jSONObject2 != null) {
                jSONObject5.put("click_point", jSONObject2);
            }
            if (jSONObject3 != null) {
                jSONObject5.put("native_view_rectangle", jSONObject3);
            }
            try {
                JSONObject optJSONObject = this.zzbga.optJSONObject("tracking_urls_and_actions");
                if (optJSONObject == null) {
                    optJSONObject = new JSONObject();
                }
                jSONObject4.put("click_signals", this.zzbgd.zzaw().zzb(this.mContext, optJSONObject.optString("click_string")));
            } catch (Exception e) {
                zzkd.zzb("Exception obtaining click signals", e);
            }
            jSONObject5.put("ads_id", this.zzbgh);
            this.zzbgb.zza((zzih.zza) new zzih.zza() {
                public void zze(zzft zzft) {
                    zzft.zza("google.afma.nativeAds.handleClickGmsg", jSONObject5);
                }
            });
        } catch (JSONException e2) {
            zzkd.zzb("Unable to create click JSON.", e2);
        }
    }

    public void zzb(MotionEvent motionEvent) {
        this.zzbgd.zza(motionEvent);
    }

    public void zzb(View view, Map<String, WeakReference<View>> map) {
        view.setOnTouchListener(null);
        view.setClickable(false);
        view.setOnClickListener(null);
        for (Entry value : map.entrySet()) {
            View view2 = (View) ((WeakReference) value.getValue()).get();
            if (view2 != null) {
                view2.setOnTouchListener(null);
                view2.setClickable(false);
                view2.setOnClickListener(null);
            }
        }
    }

    public void zzg(View view) {
        synchronized (this.zzail) {
            if (!this.zzbge) {
                if (view.isShown()) {
                    if (view.getGlobalVisibleRect(new Rect(), null)) {
                        recordImpression();
                    }
                }
            }
        }
    }

    public void zzh(View view) {
        this.zzbgi = new WeakReference<>(view);
    }

    public zzlh zzlb() {
        this.zzbgf = zzld();
        this.zzbgf.getView().setVisibility(8);
        this.zzbgb.zza((zzih.zza) new zzih.zza() {
            public void zze(final zzft zzft) {
                zzft.zza("/loadHtml", (zzep) new zzep() {
                    public void zza(zzlh zzlh, final Map<String, String> map) {
                        zzi.this.zzbgf.zzuj().zza((zzli.zza) new zzli.zza() {
                            public void zza(zzlh zzlh, boolean z) {
                                zzi.this.zzbgg = (String) map.get("id");
                                JSONObject jSONObject = new JSONObject();
                                try {
                                    jSONObject.put("messageType", "htmlLoaded");
                                    jSONObject.put("id", zzi.this.zzbgg);
                                    zzft.zzb("sendMessageToNativeJs", jSONObject);
                                } catch (JSONException e) {
                                    zzkd.zzb("Unable to dispatch sendMessageToNativeJs event", e);
                                }
                            }
                        });
                        String str = (String) map.get("overlayHtml");
                        String str2 = (String) map.get("baseUrl");
                        if (TextUtils.isEmpty(str2)) {
                            zzi.this.zzbgf.loadData(str, "text/html", "UTF-8");
                        } else {
                            zzi.this.zzbgf.loadDataWithBaseURL(str2, str, "text/html", "UTF-8", null);
                        }
                    }
                });
                zzft.zza("/showOverlay", (zzep) new zzep() {
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        zzi.this.zzbgf.getView().setVisibility(0);
                    }
                });
                zzft.zza("/hideOverlay", (zzep) new zzep() {
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        zzi.this.zzbgf.getView().setVisibility(8);
                    }
                });
                zzi.this.zzbgf.zzuj().zza("/hideOverlay", (zzep) new zzep() {
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        zzi.this.zzbgf.getView().setVisibility(8);
                    }
                });
                zzi.this.zzbgf.zzuj().zza("/sendMessageToSdk", (zzep) new zzep() {
                    public void zza(zzlh zzlh, Map<String, String> map) {
                        JSONObject jSONObject = new JSONObject();
                        try {
                            for (String str : map.keySet()) {
                                jSONObject.put(str, map.get(str));
                            }
                            jSONObject.put("id", zzi.this.zzbgg);
                            zzft.zzb("sendMessageToNativeJs", jSONObject);
                        } catch (JSONException e) {
                            zzkd.zzb("Unable to dispatch sendMessageToNativeJs event", e);
                        }
                    }
                });
            }
        });
        return this.zzbgf;
    }

    public View zzlc() {
        if (this.zzbgi != null) {
            return (View) this.zzbgi.get();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public zzlh zzld() {
        return zzu.zzfr().zza(this.mContext, AdSizeParcel.zzk(this.mContext), false, false, this.zzbgd, this.zzalo);
    }

    /* access modifiers changed from: protected */
    public void zzq(boolean z) {
        this.zzbge = z;
    }
}
