package com.google.android.gms.ads;

import com.google.android.gms.internal.zzin;

@zzin
public final class VideoOptions {
    private final boolean zzaio;

    public static final class Builder {
        /* access modifiers changed from: private */
        public boolean zzaio = false;

        public VideoOptions build() {
            return new VideoOptions(this);
        }

        public Builder setStartMuted(boolean z) {
            this.zzaio = z;
            return this;
        }
    }

    private VideoOptions(Builder builder) {
        this.zzaio = builder.zzaio;
    }

    public boolean getStartMuted() {
        return this.zzaio;
    }
}
