package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.internal.zzua;
import java.util.concurrent.Callable;

public class zzb {
    private static SharedPreferences Pc = null;

    public static SharedPreferences zzn(final Context context) {
        SharedPreferences sharedPreferences;
        synchronized (SharedPreferences.class) {
            if (Pc == null) {
                Pc = (SharedPreferences) zzua.zzb(new Callable<SharedPreferences>() {
                    /* renamed from: zzbex */
                    public SharedPreferences call() {
                        return context.getSharedPreferences("google_sdk_flags", 1);
                    }
                });
            }
            sharedPreferences = Pc;
        }
        return sharedPreferences;
    }
}
