package com.google.android.gms.config.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi implements Creator<PackageConfigTable> {
    static void zza(PackageConfigTable packageConfigTable, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, packageConfigTable.getVersionCode());
        zzb.zza(parcel, 2, packageConfigTable.zzawt(), false);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzdc */
    public PackageConfigTable createFromParcel(Parcel parcel) {
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        Bundle bundle = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    bundle = zza.zzs(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new PackageConfigTable(i, bundle);
        }
        throw new C0028zza("Overread allowed size end=" + zzcm, parcel);
    }

    /* renamed from: zzhi */
    public PackageConfigTable[] newArray(int i) {
        return new PackageConfigTable[i];
    }
}
