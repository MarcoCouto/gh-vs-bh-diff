package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zze implements Creator<FetchConfigIpcRequest> {
    static void zza(FetchConfigIpcRequest fetchConfigIpcRequest, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, fetchConfigIpcRequest.mVersionCode);
        zzb.zza(parcel, 2, fetchConfigIpcRequest.getPackageName(), false);
        zzb.zza(parcel, 3, fetchConfigIpcRequest.zzawl());
        zzb.zza(parcel, 4, (Parcelable) fetchConfigIpcRequest.zzawm(), i, false);
        zzb.zza(parcel, 5, fetchConfigIpcRequest.zzawn(), false);
        zzb.zza(parcel, 6, fetchConfigIpcRequest.zzawo(), false);
        zzb.zza(parcel, 7, fetchConfigIpcRequest.zzawp(), false);
        zzb.zzb(parcel, 8, fetchConfigIpcRequest.zzawq(), false);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzda */
    public FetchConfigIpcRequest createFromParcel(Parcel parcel) {
        ArrayList arrayList = null;
        int zzcm = zza.zzcm(parcel);
        int i = 0;
        long j = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        DataHolder dataHolder = null;
        String str4 = null;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    str4 = zza.zzq(parcel, zzcl);
                    break;
                case 3:
                    j = zza.zzi(parcel, zzcl);
                    break;
                case 4:
                    dataHolder = (DataHolder) zza.zza(parcel, zzcl, DataHolder.CREATOR);
                    break;
                case 5:
                    str3 = zza.zzq(parcel, zzcl);
                    break;
                case 6:
                    str2 = zza.zzq(parcel, zzcl);
                    break;
                case 7:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case 8:
                    arrayList = zza.zzae(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new FetchConfigIpcRequest(i, str4, j, dataHolder, str3, str2, str, arrayList);
        }
        throw new C0028zza("Overread allowed size end=" + zzcm, parcel);
    }

    /* renamed from: zzhg */
    public FetchConfigIpcRequest[] newArray(int i) {
        return new FetchConfigIpcRequest[i];
    }
}
