package com.google.android.gms.config.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class PackageConfigTable extends AbstractSafeParcelable {
    public static final zzi CREATOR = new zzi();
    private final Bundle BO;
    private final int mVersionCode;

    PackageConfigTable(int i, Bundle bundle) {
        this.mVersionCode = i;
        this.BO = bundle;
    }

    public int getVersionCode() {
        return this.mVersionCode;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzi.zza(this, parcel, i);
    }

    public Bundle zzawt() {
        return this.BO;
    }
}
