package com.google.android.gms.config.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzg;
import com.google.android.gms.common.internal.zzk;
import com.google.android.gms.config.internal.zzh.zza;

public class zzc extends zzk<zzh> {
    public zzc(Context context, Looper looper, zzg zzg, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 64, zzg, connectionCallbacks, onConnectionFailedListener);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzeb */
    public zzh zzbb(IBinder iBinder) {
        return zza.zzed(iBinder);
    }

    /* access modifiers changed from: protected */
    public String zzqz() {
        return "com.google.android.gms.config.START";
    }

    /* access modifiers changed from: protected */
    public String zzra() {
        return "com.google.android.gms.config.internal.IConfigService";
    }
}
