package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.C0028zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf implements Creator<FetchConfigIpcResponse> {
    static void zza(FetchConfigIpcResponse fetchConfigIpcResponse, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, fetchConfigIpcResponse.getVersionCode());
        zzb.zzc(parcel, 2, fetchConfigIpcResponse.getStatusCode());
        zzb.zza(parcel, 3, (Parcelable) fetchConfigIpcResponse.zzawr(), i, false);
        zzb.zza(parcel, 4, fetchConfigIpcResponse.getThrottleEndTimeMillis());
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzdb */
    public FetchConfigIpcResponse createFromParcel(Parcel parcel) {
        int i = 0;
        int zzcm = zza.zzcm(parcel);
        DataHolder dataHolder = null;
        long j = 0;
        int i2 = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 3:
                    dataHolder = (DataHolder) zza.zza(parcel, zzcl, DataHolder.CREATOR);
                    break;
                case 4:
                    j = zza.zzi(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new FetchConfigIpcResponse(i2, i, dataHolder, j);
        }
        throw new C0028zza("Overread allowed size end=" + zzcm, parcel);
    }

    /* renamed from: zzhh */
    public FetchConfigIpcResponse[] newArray(int i) {
        return new FetchConfigIpcResponse[i];
    }
}
