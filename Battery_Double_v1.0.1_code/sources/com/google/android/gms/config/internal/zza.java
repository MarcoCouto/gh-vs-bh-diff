package com.google.android.gms.config.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.zzqf;
import com.google.android.gms.internal.zzrq;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.internal.zzrs;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class zza implements zzrr {
    private static final Pattern BA = Pattern.compile("^(0|false|f|no|n|off|)$", 2);
    private static final Pattern Bz = Pattern.compile("^(1|true|t|yes|y|on)$", 2);
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    /* renamed from: com.google.android.gms.config.internal.zza$zza reason: collision with other inner class name */
    static abstract class C0039zza extends com.google.android.gms.config.internal.zzg.zza {
        C0039zza() {
        }

        public void zza(Status status, FetchConfigIpcResponse fetchConfigIpcResponse) {
            throw new UnsupportedOperationException();
        }

        public void zza(Status status, Map map) {
            throw new UnsupportedOperationException();
        }

        public void zza(Status status, byte[] bArr) {
            throw new UnsupportedOperationException();
        }

        public void zzaf(Status status) {
            throw new UnsupportedOperationException();
        }
    }

    static abstract class zzb<R extends Result> extends com.google.android.gms.internal.zzpm.zza<R, zzc> {
        public zzb(GoogleApiClient googleApiClient) {
            super(zzrq.API, googleApiClient);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(Context context, zzh zzh) throws RemoteException;

        /* access modifiers changed from: protected */
        public final void zza(zzc zzc) throws RemoteException {
            zza(zzc.getContext(), (zzh) zzc.zzasa());
        }
    }

    static abstract class zzc extends zzb<com.google.android.gms.internal.zzrr.zzb> {
        protected zzg BD = new C0039zza() {
            public void zza(Status status, FetchConfigIpcResponse fetchConfigIpcResponse) {
                if (fetchConfigIpcResponse.getStatusCode() == 6502 || fetchConfigIpcResponse.getStatusCode() == 6507) {
                    zzc.this.zzc(new zzd(zza.zzhd(fetchConfigIpcResponse.getStatusCode()), zza.zza(fetchConfigIpcResponse), fetchConfigIpcResponse.getThrottleEndTimeMillis()));
                } else {
                    zzc.this.zzc(new zzd(zza.zzhd(fetchConfigIpcResponse.getStatusCode()), zza.zza(fetchConfigIpcResponse)));
                }
            }
        };

        public zzc(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }
    }

    public static class zzd implements com.google.android.gms.internal.zzrr.zzb {
        private final Map<String, TreeMap<String, byte[]>> BF;
        private final long BG;
        private final Status bY;

        public zzd(Status status, Map<String, TreeMap<String, byte[]>> map) {
            this(status, map, -1);
        }

        public zzd(Status status, Map<String, TreeMap<String, byte[]>> map, long j) {
            this.bY = status;
            this.BF = map;
            this.BG = j;
        }

        public Status getStatus() {
            return this.bY;
        }

        public long getThrottleEndTimeMillis() {
            return this.BG;
        }

        public byte[] zza(String str, byte[] bArr, String str2) {
            return zzai(str, str2) ? (byte[]) ((TreeMap) this.BF.get(str2)).get(str) : bArr;
        }

        public boolean zzai(String str, String str2) {
            if (this.BF == null || this.BF.get(str2) == null) {
                return false;
            }
            return ((TreeMap) this.BF.get(str2)).get(str) != null;
        }

        public Map<String, Set<String>> zzawk() {
            HashMap hashMap = new HashMap();
            if (this.BF != null) {
                for (String str : this.BF.keySet()) {
                    Map map = (Map) this.BF.get(str);
                    if (map != null) {
                        hashMap.put(str, map.keySet());
                    }
                }
            }
            return hashMap;
        }
    }

    /* access modifiers changed from: private */
    public static HashMap<String, TreeMap<String, byte[]>> zza(FetchConfigIpcResponse fetchConfigIpcResponse) {
        if (fetchConfigIpcResponse == null) {
            return null;
        }
        DataHolder zzawr = fetchConfigIpcResponse.zzawr();
        if (zzawr == null) {
            return null;
        }
        PackageConfigTable packageConfigTable = (PackageConfigTable) new com.google.android.gms.common.data.zzd(zzawr, PackageConfigTable.CREATOR).get(0);
        fetchConfigIpcResponse.zzaws();
        HashMap hashMap = new HashMap();
        for (String str : packageConfigTable.zzawt().keySet()) {
            TreeMap treeMap = new TreeMap();
            hashMap.put(str, treeMap);
            Bundle bundle = packageConfigTable.zzawt().getBundle(str);
            for (String str2 : bundle.keySet()) {
                treeMap.put(str2, bundle.getByteArray(str2));
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    public static Status zzhd(int i) {
        return new Status(i, zzrs.getStatusCodeString(i));
    }

    public PendingResult<com.google.android.gms.internal.zzrr.zzb> zza(GoogleApiClient googleApiClient, final com.google.android.gms.internal.zzrr.zza zza) {
        if (googleApiClient == null || zza == null) {
            return null;
        }
        return googleApiClient.zzc(new zzc(googleApiClient) {
            /* access modifiers changed from: protected */
            public void zza(Context context, zzh zzh) throws RemoteException {
                String str;
                String str2;
                String str3 = null;
                com.google.android.gms.common.data.DataHolder.zza zzarg = com.google.android.gms.common.data.zzd.zzarg();
                for (Entry entry : zza.zzawi().entrySet()) {
                    com.google.android.gms.common.data.zzd.zza(zzarg, new CustomVariable((String) entry.getKey(), (String) entry.getValue()));
                }
                DataHolder zzfu = zzarg.zzfu(0);
                String str4 = zzqf.zzcb(context) == Status.sq ? zzqf.zzaqo() : str3;
                try {
                    str = com.google.firebase.iid.zzc.zzcwr().getId();
                    try {
                        str2 = com.google.firebase.iid.zzc.zzcwr().getToken();
                    } catch (IllegalStateException e) {
                        e = e;
                        if (Log.isLoggable("ConfigApiImpl", 3)) {
                            Log.d("ConfigApiImpl", "Cannot retrieve instanceId or instanceIdToken.", e);
                        }
                        str2 = str3;
                        zzh.zza(this.BD, new FetchConfigIpcRequest(context.getPackageName(), zza.zzawh(), zzfu, str4, str, str2));
                        zzfu.close();
                    }
                } catch (IllegalStateException e2) {
                    e = e2;
                    str = str3;
                }
                zzh.zza(this.BD, new FetchConfigIpcRequest(context.getPackageName(), zza.zzawh(), zzfu, str4, str, str2));
                zzfu.close();
            }

            /* access modifiers changed from: protected */
            /* renamed from: zzae */
            public com.google.android.gms.internal.zzrr.zzb zzc(Status status) {
                return new zzd(status, new HashMap());
            }
        });
    }
}
