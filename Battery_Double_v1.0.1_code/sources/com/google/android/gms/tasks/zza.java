package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

class zza<TResult, TContinuationResult> implements zzf<TResult> {
    /* access modifiers changed from: private */
    public final Continuation<TResult, TContinuationResult> aDg;
    /* access modifiers changed from: private */
    public final zzh<TContinuationResult> aDh;
    private final Executor avv;

    public zza(@NonNull Executor executor, @NonNull Continuation<TResult, TContinuationResult> continuation, @NonNull zzh<TContinuationResult> zzh) {
        this.avv = executor;
        this.aDg = continuation;
        this.aDh = zzh;
    }

    public void cancel() {
        throw new UnsupportedOperationException();
    }

    public void onComplete(@NonNull final Task<TResult> task) {
        this.avv.execute(new Runnable() {
            public void run() {
                try {
                    zza.this.aDh.setResult(zza.this.aDg.then(task));
                } catch (RuntimeExecutionException e) {
                    if (e.getCause() instanceof Exception) {
                        zza.this.aDh.setException((Exception) e.getCause());
                    } else {
                        zza.this.aDh.setException(e);
                    }
                } catch (Exception e2) {
                    zza.this.aDh.setException(e2);
                }
            }
        });
    }
}
