package com.google.android.gms.tasks;

import android.app.Activity;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.internal.zzqj;
import com.google.android.gms.internal.zzqk;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

final class zzh<TResult> extends Task<TResult> {
    private final zzg<TResult> aDv = new zzg<>();
    private boolean aDw;
    private TResult aDx;
    private Exception aDy;
    private final Object zzail = new Object();

    private static class zza extends zzqj {
        private final List<WeakReference<zzf<?>>> mListeners = new ArrayList();

        private zza(zzqk zzqk) {
            super(zzqk);
            this.vm.zza("TaskOnStopCallback", (zzqj) this);
        }

        public static zza zzv(Activity activity) {
            zzqk zzs = zzs(activity);
            zza zza = (zza) zzs.zza("TaskOnStopCallback", zza.class);
            return zza == null ? new zza(zzs) : zza;
        }

        @MainThread
        public void onStop() {
            synchronized (this.mListeners) {
                for (WeakReference weakReference : this.mListeners) {
                    zzf zzf = (zzf) weakReference.get();
                    if (zzf != null) {
                        zzf.cancel();
                    }
                }
                this.mListeners.clear();
            }
        }

        public <T> void zzb(zzf<T> zzf) {
            synchronized (this.mListeners) {
                this.mListeners.add(new WeakReference(zzf));
            }
        }
    }

    zzh() {
    }

    private void zzchm() {
        zzab.zza(this.aDw, (Object) "Task is not yet complete");
    }

    private void zzchn() {
        zzab.zza(!this.aDw, (Object) "Task is already complete");
    }

    private void zzcho() {
        synchronized (this.zzail) {
            if (this.aDw) {
                this.aDv.zza((Task<TResult>) this);
            }
        }
    }

    @NonNull
    public Task<TResult> addOnCompleteListener(@NonNull Activity activity, @NonNull OnCompleteListener<TResult> onCompleteListener) {
        zzc zzc = new zzc(TaskExecutors.MAIN_THREAD, onCompleteListener);
        this.aDv.zza((zzf<TResult>) zzc);
        zza.zzv(activity).zzb(zzc);
        zzcho();
        return this;
    }

    @NonNull
    public Task<TResult> addOnCompleteListener(@NonNull OnCompleteListener<TResult> onCompleteListener) {
        return addOnCompleteListener(TaskExecutors.MAIN_THREAD, onCompleteListener);
    }

    @NonNull
    public Task<TResult> addOnCompleteListener(@NonNull Executor executor, @NonNull OnCompleteListener<TResult> onCompleteListener) {
        this.aDv.zza((zzf<TResult>) new zzc<TResult>(executor, onCompleteListener));
        zzcho();
        return this;
    }

    @NonNull
    public Task<TResult> addOnFailureListener(@NonNull Activity activity, @NonNull OnFailureListener onFailureListener) {
        zzd zzd = new zzd(TaskExecutors.MAIN_THREAD, onFailureListener);
        this.aDv.zza((zzf<TResult>) zzd);
        zza.zzv(activity).zzb(zzd);
        zzcho();
        return this;
    }

    @NonNull
    public Task<TResult> addOnFailureListener(@NonNull OnFailureListener onFailureListener) {
        return addOnFailureListener(TaskExecutors.MAIN_THREAD, onFailureListener);
    }

    @NonNull
    public Task<TResult> addOnFailureListener(@NonNull Executor executor, @NonNull OnFailureListener onFailureListener) {
        this.aDv.zza((zzf<TResult>) new zzd<TResult>(executor, onFailureListener));
        zzcho();
        return this;
    }

    @NonNull
    public Task<TResult> addOnSuccessListener(@NonNull Activity activity, @NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        zze zze = new zze(TaskExecutors.MAIN_THREAD, onSuccessListener);
        this.aDv.zza((zzf<TResult>) zze);
        zza.zzv(activity).zzb(zze);
        zzcho();
        return this;
    }

    @NonNull
    public Task<TResult> addOnSuccessListener(@NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        return addOnSuccessListener(TaskExecutors.MAIN_THREAD, onSuccessListener);
    }

    @NonNull
    public Task<TResult> addOnSuccessListener(@NonNull Executor executor, @NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        this.aDv.zza((zzf<TResult>) new zze<TResult>(executor, onSuccessListener));
        zzcho();
        return this;
    }

    @NonNull
    public <TContinuationResult> Task<TContinuationResult> continueWith(@NonNull Continuation<TResult, TContinuationResult> continuation) {
        return continueWith(TaskExecutors.MAIN_THREAD, continuation);
    }

    @NonNull
    public <TContinuationResult> Task<TContinuationResult> continueWith(@NonNull Executor executor, @NonNull Continuation<TResult, TContinuationResult> continuation) {
        zzh zzh = new zzh();
        this.aDv.zza((zzf<TResult>) new zza<TResult>(executor, continuation, zzh));
        zzcho();
        return zzh;
    }

    @NonNull
    public <TContinuationResult> Task<TContinuationResult> continueWithTask(@NonNull Continuation<TResult, Task<TContinuationResult>> continuation) {
        return continueWithTask(TaskExecutors.MAIN_THREAD, continuation);
    }

    @NonNull
    public <TContinuationResult> Task<TContinuationResult> continueWithTask(@NonNull Executor executor, @NonNull Continuation<TResult, Task<TContinuationResult>> continuation) {
        zzh zzh = new zzh();
        this.aDv.zza((zzf<TResult>) new zzb<TResult>(executor, continuation, zzh));
        zzcho();
        return zzh;
    }

    @Nullable
    public Exception getException() {
        Exception exc;
        synchronized (this.zzail) {
            exc = this.aDy;
        }
        return exc;
    }

    public TResult getResult() {
        TResult tresult;
        synchronized (this.zzail) {
            zzchm();
            if (this.aDy != null) {
                throw new RuntimeExecutionException(this.aDy);
            }
            tresult = this.aDx;
        }
        return tresult;
    }

    public <X extends Throwable> TResult getResult(@NonNull Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.zzail) {
            zzchm();
            if (cls.isInstance(this.aDy)) {
                throw ((Throwable) cls.cast(this.aDy));
            } else if (this.aDy != null) {
                throw new RuntimeExecutionException(this.aDy);
            } else {
                tresult = this.aDx;
            }
        }
        return tresult;
    }

    public boolean isComplete() {
        boolean z;
        synchronized (this.zzail) {
            z = this.aDw;
        }
        return z;
    }

    public boolean isSuccessful() {
        boolean z;
        synchronized (this.zzail) {
            z = this.aDw && this.aDy == null;
        }
        return z;
    }

    public void setException(@NonNull Exception exc) {
        zzab.zzb(exc, (Object) "Exception must not be null");
        synchronized (this.zzail) {
            zzchn();
            this.aDw = true;
            this.aDy = exc;
        }
        this.aDv.zza((Task<TResult>) this);
    }

    public void setResult(TResult tresult) {
        synchronized (this.zzail) {
            zzchn();
            this.aDw = true;
            this.aDx = tresult;
        }
        this.aDv.zza((Task<TResult>) this);
    }
}
