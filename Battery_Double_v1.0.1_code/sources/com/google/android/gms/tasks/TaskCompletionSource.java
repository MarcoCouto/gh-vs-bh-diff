package com.google.android.gms.tasks;

import android.support.annotation.NonNull;

public class TaskCompletionSource<TResult> {
    private final zzh<TResult> aDt = new zzh<>();

    @NonNull
    public Task<TResult> getTask() {
        return this.aDt;
    }

    public void setException(@NonNull Exception exc) {
        this.aDt.setException(exc);
    }

    public void setResult(TResult tresult) {
        this.aDt.setResult(tresult);
    }
}
