package com.google.android.gms;

public final class R {

    public static final class array {
    }

    public static final class attr {
        public static final int adSize = 2130771999;
        public static final int adSizes = 2130772000;
        public static final int adUnitId = 2130772001;
        public static final int buttonSize = 2130772044;
        public static final int circleCrop = 2130772023;
        public static final int colorScheme = 2130772045;
        public static final int imageAspectRatio = 2130772022;
        public static final int imageAspectRatioAdjust = 2130772021;
        public static final int scopeUris = 2130772046;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131361876;
        public static final int common_google_signin_btn_text_dark_default = 2131361810;
        public static final int common_google_signin_btn_text_dark_disabled = 2131361811;
        public static final int common_google_signin_btn_text_dark_focused = 2131361812;
        public static final int common_google_signin_btn_text_dark_pressed = 2131361813;
        public static final int common_google_signin_btn_text_light = 2131361877;
        public static final int common_google_signin_btn_text_light_default = 2131361814;
        public static final int common_google_signin_btn_text_light_disabled = 2131361815;
        public static final int common_google_signin_btn_text_light_focused = 2131361816;
        public static final int common_google_signin_btn_text_light_pressed = 2131361817;
        public static final int common_plus_signin_btn_text_dark = 2131361878;
        public static final int common_plus_signin_btn_text_dark_default = 2131361818;
        public static final int common_plus_signin_btn_text_dark_disabled = 2131361819;
        public static final int common_plus_signin_btn_text_dark_focused = 2131361820;
        public static final int common_plus_signin_btn_text_dark_pressed = 2131361821;
        public static final int common_plus_signin_btn_text_light = 2131361879;
        public static final int common_plus_signin_btn_text_light_default = 2131361822;
        public static final int common_plus_signin_btn_text_light_disabled = 2131361823;
        public static final int common_plus_signin_btn_text_light_focused = 2131361824;
        public static final int common_plus_signin_btn_text_light_pressed = 2131361825;
    }

    public static final class dimen {
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2130837574;
        public static final int common_google_signin_btn_icon_dark = 2130837575;
        public static final int common_google_signin_btn_icon_dark_disabled = 2130837576;
        public static final int common_google_signin_btn_icon_dark_focused = 2130837577;
        public static final int common_google_signin_btn_icon_dark_normal = 2130837578;
        public static final int common_google_signin_btn_icon_dark_pressed = 2130837579;
        public static final int common_google_signin_btn_icon_light = 2130837580;
        public static final int common_google_signin_btn_icon_light_disabled = 2130837581;
        public static final int common_google_signin_btn_icon_light_focused = 2130837582;
        public static final int common_google_signin_btn_icon_light_normal = 2130837583;
        public static final int common_google_signin_btn_icon_light_pressed = 2130837584;
        public static final int common_google_signin_btn_text_dark = 2130837585;
        public static final int common_google_signin_btn_text_dark_disabled = 2130837586;
        public static final int common_google_signin_btn_text_dark_focused = 2130837587;
        public static final int common_google_signin_btn_text_dark_normal = 2130837588;
        public static final int common_google_signin_btn_text_dark_pressed = 2130837589;
        public static final int common_google_signin_btn_text_light = 2130837590;
        public static final int common_google_signin_btn_text_light_disabled = 2130837591;
        public static final int common_google_signin_btn_text_light_focused = 2130837592;
        public static final int common_google_signin_btn_text_light_normal = 2130837593;
        public static final int common_google_signin_btn_text_light_pressed = 2130837594;
        public static final int common_ic_googleplayservices = 2130837595;
        public static final int common_plus_signin_btn_icon_dark = 2130837596;
        public static final int common_plus_signin_btn_icon_dark_disabled = 2130837597;
        public static final int common_plus_signin_btn_icon_dark_focused = 2130837598;
        public static final int common_plus_signin_btn_icon_dark_normal = 2130837599;
        public static final int common_plus_signin_btn_icon_dark_pressed = 2130837600;
        public static final int common_plus_signin_btn_icon_light = 2130837601;
        public static final int common_plus_signin_btn_icon_light_disabled = 2130837602;
        public static final int common_plus_signin_btn_icon_light_focused = 2130837603;
        public static final int common_plus_signin_btn_icon_light_normal = 2130837604;
        public static final int common_plus_signin_btn_icon_light_pressed = 2130837605;
        public static final int common_plus_signin_btn_text_dark = 2130837606;
        public static final int common_plus_signin_btn_text_dark_disabled = 2130837607;
        public static final int common_plus_signin_btn_text_dark_focused = 2130837608;
        public static final int common_plus_signin_btn_text_dark_normal = 2130837609;
        public static final int common_plus_signin_btn_text_dark_pressed = 2130837610;
        public static final int common_plus_signin_btn_text_light = 2130837611;
        public static final int common_plus_signin_btn_text_light_disabled = 2130837612;
        public static final int common_plus_signin_btn_text_light_focused = 2130837613;
        public static final int common_plus_signin_btn_text_light_normal = 2130837614;
        public static final int common_plus_signin_btn_text_light_pressed = 2130837615;
    }

    public static final class id {
        public static final int adjust_height = 2131427355;
        public static final int adjust_width = 2131427356;
        public static final int auto = 2131427365;
        public static final int dark = 2131427366;
        public static final int icon_only = 2131427362;
        public static final int light = 2131427367;
        public static final int none = 2131427342;
        public static final int normal = 2131427338;
        public static final int radio = 2131427392;
        public static final int standard = 2131427363;
        public static final int text = 2131427438;
        public static final int text2 = 2131427436;
        public static final int wide = 2131427364;
        public static final int wrap_content = 2131427368;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131296260;
    }

    public static final class layout {
    }

    public static final class raw {
    }

    public static final class string {
        public static final int accept = 2131034150;
        public static final int common_google_play_services_enable_button = 2131034129;
        public static final int common_google_play_services_enable_text = 2131034130;
        public static final int common_google_play_services_enable_title = 2131034131;
        public static final int common_google_play_services_install_button = 2131034132;
        public static final int common_google_play_services_install_text_phone = 2131034133;
        public static final int common_google_play_services_install_text_tablet = 2131034134;
        public static final int common_google_play_services_install_title = 2131034135;
        public static final int common_google_play_services_notification_ticker = 2131034136;
        public static final int common_google_play_services_unknown_issue = 2131034137;
        public static final int common_google_play_services_unsupported_text = 2131034138;
        public static final int common_google_play_services_unsupported_title = 2131034139;
        public static final int common_google_play_services_update_button = 2131034140;
        public static final int common_google_play_services_update_text = 2131034141;
        public static final int common_google_play_services_update_title = 2131034142;
        public static final int common_google_play_services_updating_text = 2131034143;
        public static final int common_google_play_services_updating_title = 2131034144;
        public static final int common_google_play_services_wear_update_text = 2131034145;
        public static final int common_open_on_phone = 2131034146;
        public static final int common_signin_button_text = 2131034147;
        public static final int common_signin_button_text_long = 2131034148;
        public static final int create_calendar_message = 2131034152;
        public static final int create_calendar_title = 2131034153;
        public static final int decline = 2131034154;
        public static final int store_picture_message = 2131034163;
        public static final int store_picture_title = 2131034164;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131165415;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.mansoon.BatteryDouble.R.attr.adSize, com.mansoon.BatteryDouble.R.attr.adSizes, com.mansoon.BatteryDouble.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.mansoon.BatteryDouble.R.attr.imageAspectRatioAdjust, com.mansoon.BatteryDouble.R.attr.imageAspectRatio, com.mansoon.BatteryDouble.R.attr.circleCrop};
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
        public static final int[] SignInButton = {com.mansoon.BatteryDouble.R.attr.buttonSize, com.mansoon.BatteryDouble.R.attr.colorScheme, com.mansoon.BatteryDouble.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
