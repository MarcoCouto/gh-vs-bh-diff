package com.google.firebase.iid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import java.io.IOException;

public class FirebaseInstanceIdService extends zzb {
    private static final Object baA = new Object();
    private static boolean baB = false;
    private static BroadcastReceiver baz;
    /* access modifiers changed from: private */
    public boolean baC = false;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r3.zzcwv().zzcxa() == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        zzen(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r3.zzcwt() == null) goto L_0x001a;
     */
    static void zza(Context context, FirebaseInstanceId firebaseInstanceId) {
        synchronized (baA) {
            if (baB) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x008e A[Catch:{ IOException -> 0x00a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ab A[SYNTHETIC, Splitter:B:51:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0071 A[SYNTHETIC] */
    private void zza(Intent intent, boolean z) {
        synchronized (baA) {
            baB = false;
        }
        if (zzf.zzdi(this) != null) {
            FirebaseInstanceId instance = FirebaseInstanceId.getInstance();
            zze zzcwv = instance.zzcwv();
            if (instance.zzcwt() == null) {
                try {
                    if (instance.zzcwu() != null) {
                        if (this.baC) {
                            Log.d("FirebaseInstanceId", "get master token succeeded");
                        }
                        zza((Context) this, instance);
                        onTokenRefresh();
                        return;
                    }
                    zzd(intent, "returned token is null");
                } catch (IOException e) {
                    zzd(intent, e.getMessage());
                } catch (SecurityException e2) {
                    Log.e("FirebaseInstanceId", "Unable to get master token", e2);
                }
            } else {
                for (String zzcxa = zzcwv.zzcxa(); zzcxa != null; zzcxa = zzcwv.zzcxa()) {
                    String[] split = zzcxa.split("!");
                    if (split.length == 2) {
                        String str = split[0];
                        String str2 = split[1];
                        char c = 65535;
                        try {
                            switch (str.hashCode()) {
                                case 83:
                                    if (str.equals("S")) {
                                        c = 0;
                                    }
                                case 85:
                                    if (str.equals("U")) {
                                        c = 1;
                                    }
                                    switch (c) {
                                        case 0:
                                            FirebaseInstanceId.getInstance().zzsw(str2);
                                            if (!this.baC) {
                                                break;
                                            } else {
                                                Log.d("FirebaseInstanceId", "subscribe operation succeeded");
                                                break;
                                            }
                                        case 1:
                                            FirebaseInstanceId.getInstance().zzsx(str2);
                                            if (!this.baC) {
                                                break;
                                            } else {
                                                Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
                                                break;
                                            }
                                    }
                            }
                            switch (c) {
                                case 0:
                                    break;
                                case 1:
                                    break;
                            }
                        } catch (IOException e3) {
                            zzd(intent, e3.getMessage());
                            return;
                        }
                    }
                    zzcwv.zzsz(zzcxa);
                }
                Log.d("FirebaseInstanceId", "topic sync succeeded");
            }
        }
    }

    private void zza(zzf zzf, Bundle bundle) {
        String zzdi = zzf.zzdi(this);
        if (zzdi == null) {
            Log.w("FirebaseInstanceId", "Unable to respond to ping due to missing target package");
            return;
        }
        Intent intent = new Intent("com.google.android.gcm.intent.SEND");
        intent.setPackage(zzdi);
        intent.putExtras(bundle);
        zzf.zzs(intent);
        intent.putExtra("google.to", "google.com/iid");
        intent.putExtra("google.message_id", zzf.zzbmc());
        sendOrderedBroadcast(intent, "com.google.android.gtalkservice.permission.GTALK_SERVICE");
    }

    private String zzad(Intent intent) {
        String stringExtra = intent.getStringExtra("subtype");
        return stringExtra == null ? "" : stringExtra;
    }

    /* access modifiers changed from: private */
    public static Intent zzafa(int i) {
        Context applicationContext = FirebaseApp.getInstance().getApplicationContext();
        Intent intent = new Intent("ACTION_TOKEN_REFRESH_RETRY");
        intent.putExtra("next_retry_delay_in_seconds", i);
        return FirebaseInstanceIdInternalReceiver.zzh(applicationContext, intent);
    }

    private void zzafb(int i) {
        ((AlarmManager) getSystemService("alarm")).set(3, SystemClock.elapsedRealtime() + ((long) (i * 1000)), PendingIntent.getBroadcast(this, 0, zzafa(i * 2), 268435456));
    }

    private int zzb(Intent intent, boolean z) {
        int intExtra = intent == null ? 10 : intent.getIntExtra("next_retry_delay_in_seconds", 0);
        if (intExtra < 10 && !z) {
            return 30;
        }
        if (intExtra < 10) {
            return 10;
        }
        if (intExtra > 28800) {
            return 28800;
        }
        return intExtra;
    }

    private void zzd(Intent intent, String str) {
        boolean zzeo = zzeo(this);
        final int zzb = zzb(intent, zzeo);
        Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(str).length() + 47).append("background sync failed: ").append(str).append(", retry in ").append(zzb).append("s").toString());
        synchronized (baA) {
            zzafb(zzb);
            baB = true;
        }
        if (!zzeo) {
            if (this.baC) {
                Log.d("FirebaseInstanceId", "device not connected. Connectivity change received registered");
            }
            if (baz == null) {
                baz = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        if (FirebaseInstanceIdService.zzeo(context)) {
                            if (FirebaseInstanceIdService.this.baC) {
                                Log.d("FirebaseInstanceId", "connectivity changed. starting background sync.");
                            }
                            FirebaseInstanceIdService.this.getApplicationContext().unregisterReceiver(this);
                            context.sendBroadcast(FirebaseInstanceIdService.zzafa(zzb));
                        }
                    }
                };
            }
            getApplicationContext().registerReceiver(baz, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    static void zzen(Context context) {
        if (zzf.zzdi(context) != null) {
            synchronized (baA) {
                if (!baB) {
                    context.sendBroadcast(zzafa(0));
                    baB = true;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean zzeo(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private zzd zzsy(String str) {
        if (str == null) {
            return zzd.zzb(this, null);
        }
        Bundle bundle = new Bundle();
        bundle.putString("subtype", str);
        return zzd.zzb(this, bundle);
    }

    @WorkerThread
    public void onTokenRefresh() {
    }

    /* access modifiers changed from: protected */
    public Intent zzaa(Intent intent) {
        return FirebaseInstanceIdInternalReceiver.zzcww();
    }

    /* access modifiers changed from: protected */
    public int zzab(Intent intent) {
        this.baC = Log.isLoggable("FirebaseInstanceId", 3);
        if (intent.getStringExtra("error") == null && intent.getStringExtra("registration_id") == null) {
            return super.zzab(intent);
        }
        String zzad = zzad(intent);
        if (this.baC) {
            String str = "FirebaseInstanceId";
            String str2 = "Register result in service ";
            String valueOf = String.valueOf(zzad);
            Log.d(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        }
        zzsy(zzad).zzcwz().zzv(intent);
        zzble();
        return 2;
    }

    public void zzac(Intent intent) {
        String zzad = zzad(intent);
        zzd zzsy = zzsy(zzad);
        String stringExtra = intent.getStringExtra("CMD");
        if (this.baC) {
            String valueOf = String.valueOf(intent.getExtras());
            Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(zzad).length() + 18 + String.valueOf(stringExtra).length() + String.valueOf(valueOf).length()).append("Service command ").append(zzad).append(" ").append(stringExtra).append(" ").append(valueOf).toString());
        }
        if (intent.getStringExtra("unregistered") != null) {
            zzg zzcwy = zzsy.zzcwy();
            if (zzad == null) {
                zzad = "";
            }
            zzcwy.zzkj(zzad);
            zzsy.zzcwz().zzv(intent);
        } else if ("gcm.googleapis.com/refresh".equals(intent.getStringExtra("from"))) {
            zzsy.zzcwy().zzkj(zzad);
            zza(intent, false);
        } else if ("RST".equals(stringExtra)) {
            zzsy.zzblx();
            zzsy.zzcwy().zzkj(zzad);
            zza(intent, true);
        } else if ("RST_FULL".equals(stringExtra)) {
            if (!zzsy.zzcwy().isEmpty()) {
                zzsy.zzblx();
                zzsy.zzcwy().zzbmd();
                zza(intent, true);
            }
        } else if ("SYNC".equals(stringExtra)) {
            zzsy.zzcwy().zzkj(zzad);
            zza(intent, false);
        } else if ("PING".equals(stringExtra)) {
            zza(zzsy.zzcwz(), intent.getExtras());
        }
    }

    public void zzm(Intent intent) {
        boolean z;
        String action = intent.getAction();
        if (action == null) {
            action = "";
        }
        switch (action.hashCode()) {
            case -1737547627:
                if (action.equals("ACTION_TOKEN_REFRESH_RETRY")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                zza(intent, false);
                return;
            default:
                zzac(intent);
                return;
        }
    }
}
