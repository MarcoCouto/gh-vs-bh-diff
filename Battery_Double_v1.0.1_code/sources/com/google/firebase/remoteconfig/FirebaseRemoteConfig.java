package com.google.firebase.remoteconfig;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.internal.zzals;
import com.google.android.gms.internal.zzalt;
import com.google.android.gms.internal.zzalu;
import com.google.android.gms.internal.zzalv;
import com.google.android.gms.internal.zzalw;
import com.google.android.gms.internal.zzalx;
import com.google.android.gms.internal.zzaly.zzb;
import com.google.android.gms.internal.zzaly.zzc;
import com.google.android.gms.internal.zzaly.zzd;
import com.google.android.gms.internal.zzaly.zze;
import com.google.android.gms.internal.zzaly.zzf;
import com.google.android.gms.internal.zzapn;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.internal.zzrr.zza.C0086zza;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings.Builder;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class FirebaseRemoteConfig {
    public static final boolean DEFAULT_VALUE_FOR_BOOLEAN = false;
    public static final byte[] DEFAULT_VALUE_FOR_BYTE_ARRAY = new byte[0];
    public static final double DEFAULT_VALUE_FOR_DOUBLE = 0.0d;
    public static final long DEFAULT_VALUE_FOR_LONG = 0;
    public static final String DEFAULT_VALUE_FOR_STRING = "";
    public static final int LAST_FETCH_STATUS_FAILURE = 1;
    public static final int LAST_FETCH_STATUS_NO_FETCH_YET = 0;
    public static final int LAST_FETCH_STATUS_SUCCESS = -1;
    public static final int LAST_FETCH_STATUS_THROTTLED = 2;
    public static final int VALUE_SOURCE_DEFAULT = 1;
    public static final int VALUE_SOURCE_REMOTE = 2;
    public static final int VALUE_SOURCE_STATIC = 0;
    private static FirebaseRemoteConfig baZ;
    private zzalu bba;
    private zzalu bbb;
    private zzalu bbc;
    private zzalx bbd;
    private final ReadWriteLock bbe;
    private final Context mContext;

    static class zza implements Executor {
        zza() {
        }

        public void execute(Runnable runnable) {
            new Thread(runnable).start();
        }
    }

    FirebaseRemoteConfig(Context context) {
        this(context, null, null, null, null);
    }

    private FirebaseRemoteConfig(Context context, zzalu zzalu, zzalu zzalu2, zzalu zzalu3, zzalx zzalx) {
        this.bbe = new ReentrantReadWriteLock(true);
        this.mContext = context;
        if (zzalx != null) {
            this.bbd = zzalx;
        } else {
            this.bbd = new zzalx();
        }
        this.bbd.zzcp(zzeu(this.mContext));
        if (zzalu != null) {
            this.bba = zzalu;
        }
        if (zzalu2 != null) {
            this.bbb = zzalu2;
        }
        if (zzalu3 != null) {
            this.bbc = zzalu3;
        }
    }

    public static FirebaseRemoteConfig getInstance() {
        if (baZ != null) {
            return baZ;
        }
        FirebaseApp instance = FirebaseApp.getInstance();
        if (instance != null) {
            return zzet(instance.getApplicationContext());
        }
        throw new IllegalStateException("FirebaseApp has not been initialized.");
    }

    private static zzalu zza(com.google.android.gms.internal.zzaly.zza zza2) {
        zzd[] zzdArr;
        zzb[] zzbArr;
        if (zza2 == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (zzd zzd : zza2.bbu) {
            String str = zzd.zx;
            HashMap hashMap2 = new HashMap();
            for (zzb zzb : zzd.bbA) {
                hashMap2.put(zzb.zzcb, zzb.bbw);
            }
            hashMap.put(str, hashMap2);
        }
        return new zzalu(hashMap, zza2.timestamp);
    }

    private static zzalx zza(zzc zzc) {
        if (zzc == null) {
            return null;
        }
        zzalx zzalx = new zzalx();
        zzalx.zzafe(zzc.bbx);
        zzalx.zzcw(zzc.bby);
        return zzalx;
    }

    private static Map<String, zzals> zza(zzf[] zzfArr) {
        HashMap hashMap = new HashMap();
        if (zzfArr != null) {
            for (zzf zzf : zzfArr) {
                hashMap.put(zzf.zx, new zzals(zzf.resourceId, zzf.bbH));
            }
        }
        return hashMap;
    }

    private static long zzb(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    private void zzc(Map<String, Object> map, String str, boolean z) {
        if (str != null) {
            boolean z2 = map == null || map.isEmpty();
            HashMap hashMap = new HashMap();
            if (!z2) {
                for (String str2 : map.keySet()) {
                    Object obj = map.get(str2);
                    if (obj instanceof String) {
                        hashMap.put(str2, ((String) obj).getBytes(zzalw.UTF_8));
                    } else if (obj instanceof Long) {
                        hashMap.put(str2, ((Long) obj).toString().getBytes(zzalw.UTF_8));
                    } else if (obj instanceof Integer) {
                        hashMap.put(str2, ((Integer) obj).toString().getBytes(zzalw.UTF_8));
                    } else if (obj instanceof Double) {
                        hashMap.put(str2, ((Double) obj).toString().getBytes(zzalw.UTF_8));
                    } else if (obj instanceof Float) {
                        hashMap.put(str2, ((Float) obj).toString().getBytes(zzalw.UTF_8));
                    } else if (obj instanceof byte[]) {
                        hashMap.put(str2, (byte[]) obj);
                    } else if (obj instanceof Boolean) {
                        hashMap.put(str2, ((Boolean) obj).toString().getBytes(zzalw.UTF_8));
                    } else {
                        throw new IllegalArgumentException("The type of a default value needs to beone of String, Long, Double, Boolean, or byte[].");
                    }
                }
            }
            this.bbe.writeLock().lock();
            if (z2) {
                try {
                    if (this.bbc != null && this.bbc.zztc(str)) {
                        this.bbc.zzk(null, str);
                        this.bbc.setTimestamp(System.currentTimeMillis());
                    } else {
                        return;
                    }
                } finally {
                    this.bbe.writeLock().unlock();
                }
            } else {
                if (this.bbc == null) {
                    this.bbc = new zzalu(new HashMap(), System.currentTimeMillis());
                }
                this.bbc.zzk(hashMap, str);
                this.bbc.setTimestamp(System.currentTimeMillis());
            }
            if (z) {
                this.bbd.zztd(str);
            }
            zzcxe();
            this.bbe.writeLock().unlock();
        }
    }

    private void zzcxe() {
        this.bbe.readLock().lock();
        try {
            zzalt zzalt = new zzalt(this.mContext, this.bba, this.bbb, this.bbc, this.bbd);
            if (VERSION.SDK_INT >= 11) {
                AsyncTask.SERIAL_EXECUTOR.execute(zzalt);
            } else {
                new zza().execute(zzalt);
            }
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public static FirebaseRemoteConfig zzet(Context context) {
        if (baZ == null) {
            zze zzev = zzev(context);
            if (zzev == null) {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "No persisted config was found. Initializing from scratch.");
                }
                baZ = new FirebaseRemoteConfig(context);
            } else {
                if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                    Log.d("FirebaseRemoteConfig", "Initializing from persisted config.");
                }
                zzalu zza2 = zza(zzev.bbB);
                zzalu zza3 = zza(zzev.bbC);
                zzalu zza4 = zza(zzev.bbD);
                zzalx zza5 = zza(zzev.bbE);
                if (zza5 != null) {
                    zza5.zzcd(zza(zzev.bbF));
                }
                baZ = new FirebaseRemoteConfig(context, zza2, zza3, zza4, zza5);
            }
        }
        return baZ;
    }

    private long zzeu(Context context) {
        long j = 0;
        try {
            return this.mContext.getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
        } catch (NameNotFoundException e) {
            String valueOf = String.valueOf(context.getPackageName());
            Log.e("FirebaseRemoteConfig", new StringBuilder(String.valueOf(valueOf).length() + 25).append("Package [").append(valueOf).append("] was not found!").toString());
            return j;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x005b A[SYNTHETIC, Splitter:B:33:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x006d A[SYNTHETIC, Splitter:B:41:0x006d] */
    private static zze zzev(Context context) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        if (context == null) {
            return null;
        }
        try {
            fileInputStream = context.openFileInput("persisted_config");
            try {
                zzapn zzbd = zzapn.zzbd(zzl(fileInputStream));
                zze zze = new zze();
                zze zze2 = (zze) zze.zzb(zzbd);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e);
                    }
                }
                return zze;
            } catch (FileNotFoundException e2) {
                e = e2;
                fileInputStream2 = fileInputStream;
                try {
                    if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                        Log.d("FirebaseRemoteConfig", "Persisted config file was not found.", e);
                    }
                    if (fileInputStream2 != null) {
                        try {
                            fileInputStream2.close();
                        } catch (IOException e3) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e3);
                        }
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                try {
                    Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e);
                    if (fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e5) {
                            Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e5);
                        }
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (FileNotFoundException e6) {
            e = e6;
            fileInputStream2 = null;
        } catch (IOException e7) {
            e = e7;
            fileInputStream = null;
            Log.e("FirebaseRemoteConfig", "Cannot initialize from persisted config.", e);
            if (fileInputStream != null) {
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e8) {
                    Log.e("FirebaseRemoteConfig", "Failed to close persisted config file.", e8);
                }
            }
            throw th;
        }
    }

    private static byte[] zzl(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        zzb(inputStream, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public boolean activateFetched() {
        this.bbe.writeLock().lock();
        try {
            if (this.bba == null) {
                return false;
            }
            if (this.bbb == null || this.bbb.getTimestamp() < this.bba.getTimestamp()) {
                long timestamp = this.bba.getTimestamp();
                this.bbb = this.bba;
                this.bbb.setTimestamp(System.currentTimeMillis());
                this.bba = new zzalu(null, timestamp);
                zzcxe();
                this.bbe.writeLock().unlock();
                return true;
            }
            this.bbe.writeLock().unlock();
            return false;
        } finally {
            this.bbe.writeLock().unlock();
        }
    }

    public Task<Void> fetch() {
        return fetch(43200);
    }

    /* JADX INFO: finally extract failed */
    public Task<Void> fetch(long j) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.bbe.writeLock().lock();
        try {
            C0086zza zza2 = new C0086zza();
            zza2.zzah(j);
            if (this.bbd.isDeveloperModeEnabled()) {
                zza2.zzah("_rcn_developer", "true");
            }
            new com.google.android.gms.config.internal.zzb(this.mContext).zza(zza2.zzawj()).setResultCallback(new ResultCallback<zzrr.zzb>() {
                /* renamed from: zza */
                public void onResult(@NonNull zzrr.zzb zzb) {
                    FirebaseRemoteConfig.this.zza(taskCompletionSource, zzb);
                }
            });
            this.bbe.writeLock().unlock();
            return taskCompletionSource.getTask();
        } catch (Throwable th) {
            this.bbe.writeLock().unlock();
            throw th;
        }
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, "configns:firebase");
    }

    /* JADX INFO: finally extract failed */
    public boolean getBoolean(String str, String str2) {
        if (str2 == null) {
            return false;
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                String str3 = new String(this.bbb.zzbu(str, str2), zzalw.UTF_8);
                if (zzalw.Bz.matcher(str3).matches()) {
                    this.bbe.readLock().unlock();
                    return true;
                } else if (zzalw.BA.matcher(str3).matches()) {
                    this.bbe.readLock().unlock();
                    return false;
                }
            }
            if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                String str4 = new String(this.bbc.zzbu(str, str2), zzalw.UTF_8);
                if (zzalw.Bz.matcher(str4).matches()) {
                    this.bbe.readLock().unlock();
                    return true;
                } else if (zzalw.BA.matcher(str4).matches()) {
                    this.bbe.readLock().unlock();
                    return false;
                }
            }
            this.bbe.readLock().unlock();
            return false;
        } catch (Throwable th) {
            this.bbe.readLock().unlock();
            throw th;
        }
    }

    public byte[] getByteArray(String str) {
        return getByteArray(str, "configns:firebase");
    }

    public byte[] getByteArray(String str, String str2) {
        if (str2 == null) {
            return DEFAULT_VALUE_FOR_BYTE_ARRAY;
        }
        this.bbe.readLock().lock();
        if (this.bbb == null || !this.bbb.zzbt(str, str2)) {
            try {
                if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                    return this.bbc.zzbu(str, str2);
                }
                byte[] bArr = DEFAULT_VALUE_FOR_BYTE_ARRAY;
                this.bbe.readLock().unlock();
                return bArr;
            } finally {
                this.bbe.readLock().unlock();
            }
        } else {
            byte[] zzbu = this.bbb.zzbu(str, str2);
            this.bbe.readLock().unlock();
            return zzbu;
        }
    }

    public double getDouble(String str) {
        return getDouble(str, "configns:firebase");
    }

    public double getDouble(String str, String str2) {
        double d = DEFAULT_VALUE_FOR_DOUBLE;
        if (str2 != null) {
            this.bbe.readLock().lock();
            try {
                if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                    try {
                        d = Double.valueOf(new String(this.bbb.zzbu(str, str2), zzalw.UTF_8)).doubleValue();
                    } catch (NumberFormatException e) {
                    }
                }
                if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                    try {
                        d = Double.valueOf(new String(this.bbc.zzbu(str, str2), zzalw.UTF_8)).doubleValue();
                        this.bbe.readLock().unlock();
                    } catch (NumberFormatException e2) {
                    }
                }
                this.bbe.readLock().unlock();
            } finally {
                this.bbe.readLock().unlock();
            }
        }
        return d;
    }

    public FirebaseRemoteConfigInfo getInfo() {
        zzalv zzalv = new zzalv();
        this.bbe.readLock().lock();
        try {
            zzalv.zzco(this.bba == null ? -1 : this.bba.getTimestamp());
            zzalv.zzafe(this.bbd.getLastFetchStatus());
            zzalv.setConfigSettings(new Builder().setDeveloperModeEnabled(this.bbd.isDeveloperModeEnabled()).build());
            return zzalv;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public Set<String> getKeysByPrefix(String str) {
        return getKeysByPrefix(str, "configns:firebase");
    }

    public Set<String> getKeysByPrefix(String str, String str2) {
        Set<String> zzbv;
        this.bbe.readLock().lock();
        try {
            if (this.bbb == null) {
                zzbv = new TreeSet<>();
            } else {
                zzbv = this.bbb.zzbv(str, str2);
                this.bbe.readLock().unlock();
            }
            return zzbv;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public long getLong(String str) {
        return getLong(str, "configns:firebase");
    }

    public long getLong(String str, String str2) {
        long j = 0;
        if (str2 != null) {
            this.bbe.readLock().lock();
            try {
                if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                    try {
                        j = Long.valueOf(new String(this.bbb.zzbu(str, str2), zzalw.UTF_8)).longValue();
                    } catch (NumberFormatException e) {
                    }
                }
                if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                    try {
                        j = Long.valueOf(new String(this.bbc.zzbu(str, str2), zzalw.UTF_8)).longValue();
                        this.bbe.readLock().unlock();
                    } catch (NumberFormatException e2) {
                    }
                }
                this.bbe.readLock().unlock();
            } finally {
                this.bbe.readLock().unlock();
            }
        }
        return j;
    }

    public String getString(String str) {
        return getString(str, "configns:firebase");
    }

    public String getString(String str, String str2) {
        if (str2 == null) {
            return "";
        }
        this.bbe.readLock().lock();
        try {
            if (this.bbb != null && this.bbb.zzbt(str, str2)) {
                return new String(this.bbb.zzbu(str, str2), zzalw.UTF_8);
            }
            if (this.bbc == null || !this.bbc.zzbt(str, str2)) {
                String str3 = "";
                this.bbe.readLock().unlock();
                return str3;
            }
            String str4 = new String(this.bbc.zzbu(str, str2), zzalw.UTF_8);
            this.bbe.readLock().unlock();
            return str4;
        } finally {
            this.bbe.readLock().unlock();
        }
    }

    public FirebaseRemoteConfigValue getValue(String str) {
        return getValue(str, "configns:firebase");
    }

    public FirebaseRemoteConfigValue getValue(String str, String str2) {
        if (str2 == null) {
            return new zzalw(DEFAULT_VALUE_FOR_BYTE_ARRAY, 0);
        }
        this.bbe.readLock().lock();
        if (this.bbb == null || !this.bbb.zzbt(str, str2)) {
            try {
                if (this.bbc != null && this.bbc.zzbt(str, str2)) {
                    return new zzalw(this.bbc.zzbu(str, str2), 1);
                }
                zzalw zzalw = new zzalw(DEFAULT_VALUE_FOR_BYTE_ARRAY, 0);
                this.bbe.readLock().unlock();
                return zzalw;
            } finally {
                this.bbe.readLock().unlock();
            }
        } else {
            zzalw zzalw2 = new zzalw(this.bbb.zzbu(str, str2), 2);
            this.bbe.readLock().unlock();
            return zzalw2;
        }
    }

    public void setConfigSettings(FirebaseRemoteConfigSettings firebaseRemoteConfigSettings) {
        this.bbe.writeLock().lock();
        try {
            boolean isDeveloperModeEnabled = this.bbd.isDeveloperModeEnabled();
            boolean isDeveloperModeEnabled2 = firebaseRemoteConfigSettings == null ? false : firebaseRemoteConfigSettings.isDeveloperModeEnabled();
            this.bbd.zzcw(isDeveloperModeEnabled2);
            if (isDeveloperModeEnabled != isDeveloperModeEnabled2) {
                zzcxe();
            }
        } finally {
            this.bbe.writeLock().unlock();
        }
    }

    public void setDefaults(int i) {
        setDefaults(i, "configns:firebase");
    }

    public void setDefaults(int i, String str) {
        if (str != null) {
            this.bbe.readLock().lock();
            try {
                if (!(this.bbd == null || this.bbd.zzcxk() == null || this.bbd.zzcxk().get(str) == null)) {
                    zzals zzals = (zzals) this.bbd.zzcxk().get(str);
                    if (i == zzals.zzcxf() && this.bbd.zzcxl() == zzals.zzcxg()) {
                        if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
                            Log.d("FirebaseRemoteConfig", "Skipped setting defaults from resource file as this resource file was already applied.");
                        }
                        return;
                    }
                }
                this.bbe.readLock().unlock();
                HashMap hashMap = new HashMap();
                try {
                    XmlResourceParser xml = this.mContext.getResources().getXml(i);
                    Object obj = null;
                    Object obj2 = null;
                    Object obj3 = null;
                    for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                        if (eventType == 2) {
                            obj2 = xml.getName();
                        } else if (eventType == 3) {
                            if (!(!"entry".equals(xml.getName()) || obj == null || obj3 == null)) {
                                hashMap.put(obj, obj3);
                                obj3 = null;
                                obj = null;
                            }
                            obj2 = null;
                        } else if (eventType == 4) {
                            if ("key".equals(obj2)) {
                                obj = xml.getText();
                            } else if (Param.VALUE.equals(obj2)) {
                                obj3 = xml.getText();
                            }
                        }
                    }
                    this.bbd.zza(str, new zzals(i, this.bbd.zzcxl()));
                    zzc(hashMap, str, false);
                } catch (Exception e) {
                    Log.e("FirebaseRemoteConfig", "Caught exception while parsing XML resource. Skipping setDefaults.", e);
                }
            } finally {
                this.bbe.readLock().unlock();
            }
        } else if (Log.isLoggable("FirebaseRemoteConfig", 3)) {
            Log.d("FirebaseRemoteConfig", "namespace cannot be null for setDefaults.");
        }
    }

    public void setDefaults(Map<String, Object> map) {
        setDefaults(map, "configns:firebase");
    }

    public void setDefaults(Map<String, Object> map, String str) {
        zzc(map, str, true);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void zza(TaskCompletionSource<Void> taskCompletionSource, zzrr.zzb zzb) {
        if (zzb == null || zzb.getStatus() == null) {
            this.bbd.zzafe(1);
            taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
            zzcxe();
            return;
        }
        int statusCode = zzb.getStatus().getStatusCode();
        switch (statusCode) {
            case -6508:
            case -6506:
                this.bbd.zzafe(-1);
                if (this.bba != null && !this.bba.zzcxi()) {
                    Map zzawk = zzb.zzawk();
                    HashMap hashMap = new HashMap();
                    for (String str : zzawk.keySet()) {
                        HashMap hashMap2 = new HashMap();
                        for (String str2 : (Set) zzawk.get(str)) {
                            hashMap2.put(str2, zzb.zza(str2, null, str));
                        }
                        hashMap.put(str, hashMap2);
                    }
                    this.bba = new zzalu(hashMap, this.bba.getTimestamp());
                }
                taskCompletionSource.setResult(null);
                zzcxe();
                return;
            case -6505:
                Map zzawk2 = zzb.zzawk();
                HashMap hashMap3 = new HashMap();
                for (String str3 : zzawk2.keySet()) {
                    HashMap hashMap4 = new HashMap();
                    for (String str4 : (Set) zzawk2.get(str3)) {
                        hashMap4.put(str4, zzb.zza(str4, null, str3));
                    }
                    hashMap3.put(str3, hashMap4);
                }
                this.bba = new zzalu(hashMap3, System.currentTimeMillis());
                this.bbd.zzafe(-1);
                taskCompletionSource.setResult(null);
                zzcxe();
                return;
            case 6500:
            case 6501:
            case 6503:
            case 6504:
                this.bbd.zzafe(1);
                taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
                zzcxe();
                return;
            case 6502:
            case 6507:
                this.bbd.zzafe(2);
                taskCompletionSource.setException(new FirebaseRemoteConfigFetchThrottledException(zzb.getThrottleEndTimeMillis()));
                zzcxe();
                return;
            default:
                if (zzb.getStatus().isSuccess()) {
                    Log.w("FirebaseRemoteConfig", "Unknown (successful) status code: " + statusCode);
                }
                this.bbd.zzafe(1);
                taskCompletionSource.setException(new FirebaseRemoteConfigFetchException());
                zzcxe();
                return;
        }
    }
}
