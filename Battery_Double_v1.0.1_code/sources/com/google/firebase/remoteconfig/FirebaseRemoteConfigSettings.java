package com.google.firebase.remoteconfig;

public class FirebaseRemoteConfigSettings {
    private final boolean bbh;

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean bbh = false;

        public FirebaseRemoteConfigSettings build() {
            return new FirebaseRemoteConfigSettings(this);
        }

        public Builder setDeveloperModeEnabled(boolean z) {
            this.bbh = z;
            return this;
        }
    }

    private FirebaseRemoteConfigSettings(Builder builder) {
        this.bbh = builder.bbh;
    }

    public boolean isDeveloperModeEnabled() {
        return this.bbh;
    }
}
