package com.mansoon.BatteryDouble;

import android.support.v4.view.ViewCompat;
import com.dlten.lib.frmWork.CButton;
import com.dlten.lib.graphics.CImgObj;
import com.dlten.lib.graphics.CPoint;

public class WndGameWnd extends WndCommon {
    final int BTN_MOREBTN = 0;
    final int BTN_SETTING = 1;
    final int PRO_HEIGHT = 370;
    final int PRO_WIDTH = 244;
    final int STATE_CHARGING = 1;
    final int STATE_DISCHARGING = 2;
    final int STATE_FULL = 0;
    final int STATE_NOTCHARGING = 3;
    final int TIME_AUDIO = 20;
    final int TIME_INTERNET = 5;
    final int TIME_STANDBY = 100;
    final int TIME_TALK = 10;
    final int TIME_VIDEO = 4;
    final int XINTERVAL_TEXT = 380;
    final int XPOS_PRO1 = 16;
    final int XPOS_PRO2 = 376;
    final int XPOS_TEXT = 100;
    final int YINTERVAL_TEXT = 36;
    final int YPOS_PRO1 = 144;
    final int YPOS_PRO2 = 144;
    final int YPOS_TEXT = 536;
    private BatteryActivity m_activity;
    private CImgObj m_imgBack = null;
    private CImgObj m_imgBatteryBoxBackLeft = null;
    private CImgObj m_imgBatteryBoxBackOrg = null;
    private CImgObj m_imgBatteryBoxBackRight = null;
    private CImgObj m_imgBatteryBoxLeft = null;
    private CImgObj m_imgBatteryBoxRight = null;
    private CImgObj m_imgCharge = null;
    private CButton m_moreGame = null;
    private CPoint m_pos = new CPoint();
    private CButton m_settGame = null;
    private String m_strAudio;
    private String m_strInternet;
    private String m_strLevel;
    private String m_strLevel1;
    private String m_strLevel2;
    private String m_strStandby;
    private String m_strTalk;
    private String m_strVideo;
    private String m_strVoltage;

    public void OnLoadResource() {
        this.m_activity = (BatteryActivity) getView().getActivity();
        setString("logo Wnd");
        this.m_imgBack = new CImgObj("background.jpg");
        this.m_imgBatteryBoxLeft = new CImgObj("batterydocklet.png");
        this.m_imgBatteryBoxBackOrg = new CImgObj("n1.png");
        if (this.m_activity.m_nLevel1 > 0) {
            this.m_imgBatteryBoxBackLeft = new CImgObj("n" + this.m_activity.m_nTheme + ".png", this.m_activity.m_nLevel1);
        }
        if (this.m_activity.m_nLevel2 > 0) {
            this.m_imgBatteryBoxBackRight = new CImgObj("n" + this.m_activity.m_nTheme + ".png", this.m_activity.m_nLevel2);
        }
        this.m_imgBatteryBoxRight = new CImgObj("batterydocklet.png");
        this.m_imgCharge = new CImgObj("charging.png");
        calcValues();
        createButton();
    }

    public void createButton() {
        CButton btn = createButton("more.png", "more.png", "more.png");
        btn.setPoint(520.0f, 600.0f);
        btn.setCommand(0);
        this.m_moreGame = btn;
        CButton btn2 = createButton("set.png", "set.png", "set.png");
        btn2.setPoint(520.0f, 720.0f);
        btn2.setCommand(1);
        this.m_settGame = btn2;
    }

    public void OnGameMore() {
        BatteryActivity batteryActivity = this.m_activity;
        postActivityMsg(1, 1, 0);
    }

    public void OnGameSetting() {
        BatteryActivity batteryActivity = this.m_activity;
        postActivityMsg(2, 1, 0);
    }

    public void OnCommand(int nCmd) {
        switch (nCmd) {
            case 0:
                OnGameMore();
                return;
            case 1:
                OnGameSetting();
                return;
            default:
                return;
        }
    }

    public void calcValues() {
        float fValue = (float) (((double) ((float) (this.m_activity.m_nLevel * 100))) / 100.0d);
        this.m_strStandby = String.format(" %d hr %d min", new Object[]{Integer.valueOf((int) fValue), Integer.valueOf((int) ((fValue - ((float) ((int) fValue))) * 60.0f))});
        float fValue2 = (float) (((double) ((float) (this.m_activity.m_nLevel * 20))) / 100.0d);
        this.m_strAudio = String.format(" %d hr %d min", new Object[]{Integer.valueOf((int) fValue2), Integer.valueOf((int) ((fValue2 - ((float) ((int) fValue2))) * 60.0f))});
        float fValue3 = (float) (((double) ((float) (this.m_activity.m_nLevel * 4))) / 100.0d);
        this.m_strVideo = String.format(" %d hr %d min", new Object[]{Integer.valueOf((int) fValue3), Integer.valueOf((int) ((fValue3 - ((float) ((int) fValue3))) * 60.0f))});
        float fValue4 = (float) (((double) ((float) (this.m_activity.m_nLevel * 5))) / 100.0d);
        this.m_strInternet = String.format(" %d hr %d min", new Object[]{Integer.valueOf((int) fValue4), Integer.valueOf((int) ((fValue4 - ((float) ((int) fValue4))) * 60.0f))});
        float fValue5 = (float) (((double) ((float) (this.m_activity.m_nLevel * 10))) / 100.0d);
        this.m_strTalk = String.format(" %d hr %d min", new Object[]{Integer.valueOf((int) fValue5), Integer.valueOf((int) ((fValue5 - ((float) ((int) fValue5))) * 60.0f))});
        this.m_strVoltage = String.format("%d v", new Object[]{Integer.valueOf(this.m_activity.m_nVoltage)});
        this.m_strLevel = String.format("Battery Level Total %d", new Object[]{Integer.valueOf(this.m_activity.m_nLevel)});
        this.m_strLevel += "%";
        this.m_strLevel1 = String.format("%d", new Object[]{Integer.valueOf(this.m_activity.m_nLevel1)});
        this.m_strLevel1 += "%";
        this.m_strLevel2 = String.format("%d", new Object[]{Integer.valueOf(this.m_activity.m_nLevel2)});
        this.m_strLevel2 += "%";
    }

    public void OnDestroy() {
        this.m_imgBack = unload(this.m_imgBack);
        this.m_imgBatteryBoxLeft = unload(this.m_imgBatteryBoxLeft);
        this.m_imgBatteryBoxBackOrg = unload(this.m_imgBatteryBoxBackOrg);
        this.m_imgBatteryBoxBackLeft = unload(this.m_imgBatteryBoxBackLeft);
        this.m_imgBatteryBoxBackRight = unload(this.m_imgBatteryBoxBackRight);
        this.m_imgBatteryBoxRight = unload(this.m_imgBatteryBoxRight);
        this.m_imgCharge = unload(this.m_imgCharge);
        super.OnDestroy();
    }

    private void drawBackGround() {
        if (this.m_imgBack != null) {
            this.m_imgBack.draw(0.0f, 0.0f);
        }
    }

    public void debug_drawStatus() {
    }

    public void OnPaint() {
        drawBackGround();
        drawBatteryPRoRect();
        drawBattery();
        drawCharging();
        drawString();
        drawStringValue();
        drawLevel();
    }

    private void drawBattery() {
        float sizeY = this.m_imgBatteryBoxLeft.getSizeY();
        float w = this.m_imgBatteryBoxLeft.getSizeX();
        CPoint pos = new CPoint();
        pos.x = (584.0f - (w * 2.0f)) / 2.0f;
        pos.y = 76.0f;
        this.m_imgBatteryBoxLeft.draw(pos);
        pos.x = 348.0f;
        this.m_imgBatteryBoxRight.draw(pos);
    }

    private void drawCharging() {
        if (this.m_activity.m_nState == 0 || this.m_activity.m_nState == 1) {
            this.m_imgCharge.draw(320.0f - (this.m_imgCharge.getSizeY() / 2.0f), 420.0f);
        }
    }

    private void drawString() {
        getView().setFontSize(34);
        drawStr(100, 572, ViewCompat.MEASURED_SIZE_MASK, 18, "Technology:");
        drawStr(100, 608, ViewCompat.MEASURED_SIZE_MASK, 18, "Health:");
        drawStr(100, 644, ViewCompat.MEASURED_SIZE_MASK, 18, "Voltage:");
        drawStr(100, 680, ViewCompat.MEASURED_SIZE_MASK, 18, "Standby:");
        drawStr(100, 716, ViewCompat.MEASURED_SIZE_MASK, 18, "Audio:");
        drawStr(100, 752, ViewCompat.MEASURED_SIZE_MASK, 18, "Video:");
        drawStr(100, 788, ViewCompat.MEASURED_SIZE_MASK, 18, "Internet:");
        drawStr(100, 824, ViewCompat.MEASURED_SIZE_MASK, 18, "Talk:");
    }

    private void drawStringValue() {
        getView().setFontSize(34);
        drawStr(480, 572, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_activity.m_strTech);
        drawStr(480, 608, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_activity.m_strHealth);
        drawStr(480, 644, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strVoltage);
        drawStr(480, 680, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strStandby);
        drawStr(480, 716, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strAudio);
        drawStr(480, 752, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strVideo);
        drawStr(480, 788, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strInternet);
        drawStr(480, 824, ViewCompat.MEASURED_SIZE_MASK, 66, this.m_strTalk);
    }

    private void drawLevel() {
        getView().setFontSize(44);
        drawStr(320, 42, ViewCompat.MEASURED_SIZE_MASK, 34, this.m_strLevel);
        getView().setFontSize(36);
        drawStr(155, 475, 13092807, 34, this.m_strLevel1);
        drawStr(515, 475, 13092807, 34, this.m_strLevel2);
    }

    private void drawBatteryPRoRect() {
        float w = this.m_imgBatteryBoxLeft.getSizeX();
        float h_org = this.m_imgBatteryBoxBackOrg.getSizeY();
        if (this.m_imgBatteryBoxBackLeft != null) {
            float h = h_org - this.m_imgBatteryBoxBackLeft.getSizeY();
            CPoint pos = new CPoint();
            pos.x = ((584.0f - (w * 2.0f)) / 2.0f) + 10.0f;
            pos.y = 114.0f + h;
            this.m_imgBatteryBoxBackLeft.draw(pos);
        }
        if (this.m_imgBatteryBoxBackRight != null) {
            float h2 = h_org - this.m_imgBatteryBoxBackRight.getSizeY();
            CPoint pos2 = new CPoint();
            pos2.x = 358.0f;
            pos2.y = 114.0f + h2;
            this.m_imgBatteryBoxBackRight.draw(pos2);
        }
    }

    private void setProColor(int m_nPro) {
        int nPro = 100 - m_nPro;
        if (m_nPro > 50) {
            getView().setColor(0, 255, 0);
        } else if (m_nPro > 10) {
            getView().setColor((nPro * 2) + 0, 255 - (nPro * 1), 0);
        } else {
            getView().setColor(255, 0, 0);
        }
    }

    public void OnExit() {
        if (this.m_activity.m_bShowSetting) {
            BatteryActivity batteryActivity = this.m_activity;
            postActivityMsg(3, 1, 0);
            return;
        }
        DestroyWindow(0);
    }

    public void OnMenu() {
    }

    public void OnKeyDown(int keycode) {
        switch (keycode) {
            case 1024:
                OnMenu();
                return;
            case 4096:
                OnExit();
                return;
            default:
                super.OnKeyDown(keycode);
                return;
        }
    }

    public void OnAnimEvent(int nAnimID) {
        this.m_imgBatteryBoxBackOrg = unload(this.m_imgBatteryBoxBackOrg);
        this.m_imgBatteryBoxBackLeft = unload(this.m_imgBatteryBoxBackLeft);
        this.m_imgBatteryBoxBackRight = unload(this.m_imgBatteryBoxBackRight);
        this.m_imgBatteryBoxBackOrg = new CImgObj("n" + this.m_activity.m_nTheme + ".png");
        if (this.m_activity.m_nLevel1 > 0) {
            this.m_imgBatteryBoxBackLeft = new CImgObj("n" + this.m_activity.m_nTheme + ".png", this.m_activity.m_nLevel1);
        }
        if (this.m_activity.m_nLevel2 > 0) {
            this.m_imgBatteryBoxBackRight = new CImgObj("n" + this.m_activity.m_nTheme + ".png", this.m_activity.m_nLevel2);
        }
    }
}
