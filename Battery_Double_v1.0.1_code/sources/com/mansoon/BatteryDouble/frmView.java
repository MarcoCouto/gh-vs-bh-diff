package com.mansoon.BatteryDouble;

import android.content.Context;
import android.util.AttributeSet;
import com.dlten.lib.CBaseView;
import com.dlten.lib.Common;
import com.dlten.lib.frmWork.CWndMgr;

public class frmView extends CBaseView {
    public frmView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Initialize();
    }

    public frmView(Context context) {
        super(context);
        Initialize();
    }

    private void Initialize() {
        start();
        setFPS(1000.0f / Common.FPS);
    }

    public void Finish() {
        stop();
    }

    public CWndMgr createWndMgr() {
        return new frmWndMgr((BatteryActivity) getActivity(), this);
    }
}
