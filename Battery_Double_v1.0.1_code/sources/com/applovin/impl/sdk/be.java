package com.applovin.impl.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdkSettings;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

class be {
    private final AppLovinSdkImpl a;
    private final AppLovinLogger b;
    private final Context c;
    private final Object[] d = new Object[bb.b()];

    be(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
        this.c = appLovinSdkImpl.getApplicationContext();
    }

    private static bd a(String str) {
        for (bd bdVar : bb.a()) {
            if (bdVar.b().equals(str)) {
                return bdVar;
            }
        }
        return null;
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        throw new RuntimeException("SDK Error: unknown value type: " + obj.getClass());
    }

    private String e() {
        return "com.applovin.sdk." + cd.a(this.a.getSdkKey()) + ".";
    }

    public SharedPreferences a() {
        if (this.c != null) {
            return this.c.getSharedPreferences("com.applovin.sdk.1", 0);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public Object a(bd bdVar) {
        Object c2;
        if (bdVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        }
        synchronized (this.d) {
            Object obj = this.d[bdVar.a()];
            c2 = obj != null ? bdVar.a(obj) : bdVar.c();
        }
        return c2;
    }

    public void a(bd bdVar, Object obj) {
        if (bdVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj == null) {
            throw new IllegalArgumentException("No new value specified");
        } else {
            synchronized (this.d) {
                this.d[bdVar.a()] = obj;
            }
            this.b.d("SettingsManager", "Setting update: " + bdVar.b() + " set to \"" + obj + "\"");
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        boolean z;
        String[] split;
        long j = 0;
        boolean z2 = false;
        this.b.i("SettingsManager", "Loading user-defined settings...");
        if (appLovinSdkSettings != null) {
            synchronized (this.d) {
                this.d[bb.j.a()] = Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled());
                long bannerAdRefreshSeconds = appLovinSdkSettings.getBannerAdRefreshSeconds();
                if (bannerAdRefreshSeconds >= 0) {
                    if (bannerAdRefreshSeconds > 0) {
                        j = Math.max(30, bannerAdRefreshSeconds);
                    }
                    this.d[bb.A.a()] = Long.valueOf(j);
                    this.d[bb.z.a()] = Boolean.valueOf(true);
                } else if (bannerAdRefreshSeconds == -1) {
                    this.d[bb.z.a()] = Boolean.valueOf(false);
                }
                String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                if (autoPreloadSizes == null) {
                    autoPreloadSizes = "NONE";
                }
                Object[] objArr = this.d;
                int a2 = bb.J.a();
                if (autoPreloadSizes.equals("NONE")) {
                    autoPreloadSizes = "";
                }
                objArr[a2] = autoPreloadSizes;
                String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                if (autoPreloadTypes == null) {
                    autoPreloadTypes = "NONE";
                }
                if (!autoPreloadTypes.equals("NONE")) {
                    z = false;
                    for (String str : autoPreloadTypes.split(",")) {
                        if (str.equals(AppLovinAdType.REGULAR.getLabel())) {
                            z = true;
                        } else if (str.equals(AppLovinAdType.INCENTIVIZED.getLabel()) || str.contains("INCENT") || str.contains("REWARD")) {
                            z2 = true;
                        }
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    this.d[bb.J.a()] = "";
                }
                this.d[bb.K.a()] = Boolean.valueOf(z2);
                if (appLovinSdkSettings instanceof at) {
                    for (Entry entry : ((at) appLovinSdkSettings).b().entrySet()) {
                        this.d[((bd) entry.getKey()).a()] = entry.getValue();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        this.b.d("SettingsManager", "Loading settings from JSON array...");
        synchronized (this.d) {
            String str = "";
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str2 = (String) keys.next();
                if (str2 != null && str2.length() > 0) {
                    try {
                        bd a2 = a(str2);
                        if (a2 != null) {
                            Object a3 = a(str2, jSONObject, a2.c());
                            this.d[a2.a()] = a3;
                            this.b.d("SettingsManager", "Setting update: " + a2.b() + " set to \"" + a3 + "\"");
                        } else {
                            this.b.w("SettingsManager", "Unknown setting recieved: " + str2);
                        }
                    } catch (JSONException e) {
                        this.b.e("SettingsManager", "Unable to parse JSON settings array", e);
                    } catch (Throwable th) {
                        this.b.e("SettingsManager", "Unable to convert setting object ", th);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.c == null) {
            throw new IllegalArgumentException("No context specified");
        }
        this.b.i("SettingsManager", "Saving settings with the application...");
        String e = e();
        Editor edit = a().edit();
        synchronized (this.d) {
            for (bd bdVar : bb.a()) {
                Object obj = this.d[bdVar.a()];
                if (obj != null) {
                    String str = e + bdVar.b();
                    if (obj instanceof Boolean) {
                        edit.putBoolean(str, ((Boolean) obj).booleanValue());
                    } else if (obj instanceof Float) {
                        edit.putFloat(str, ((Float) obj).floatValue());
                    } else if (obj instanceof Integer) {
                        edit.putInt(str, ((Integer) obj).intValue());
                    } else if (obj instanceof Long) {
                        edit.putLong(str, ((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        edit.putString(str, (String) obj);
                    } else {
                        throw new RuntimeException("SDK Error: unknown value: " + obj.getClass());
                    }
                }
            }
        }
        edit.commit();
        this.b.d("SettingsManager", "Settings saved with the application.");
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        Object string;
        if (this.c == null) {
            throw new IllegalArgumentException("No context specified");
        }
        this.b.i("SettingsManager", "Loading settings saved with the application...");
        String e = e();
        SharedPreferences a2 = a();
        synchronized (this.d) {
            for (bd bdVar : bb.a()) {
                try {
                    String str = e + bdVar.b();
                    Object c2 = bdVar.c();
                    if (c2 instanceof Boolean) {
                        string = Boolean.valueOf(a2.getBoolean(str, ((Boolean) c2).booleanValue()));
                    } else if (c2 instanceof Float) {
                        string = Float.valueOf(a2.getFloat(str, ((Float) c2).floatValue()));
                    } else if (c2 instanceof Integer) {
                        string = Integer.valueOf(a2.getInt(str, ((Integer) c2).intValue()));
                    } else if (c2 instanceof Long) {
                        string = Long.valueOf(a2.getLong(str, ((Long) c2).longValue()));
                    } else if (c2 instanceof String) {
                        string = a2.getString(str, (String) c2);
                    } else {
                        throw new RuntimeException("SDK Error: unknown value: " + c2.getClass());
                    }
                    this.d[bdVar.a()] = string;
                } catch (Exception e2) {
                    this.b.e("SettingsManager", "Unable to load \"" + bdVar.b() + "\"", e2);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        synchronized (this.d) {
            Arrays.fill(this.d, null);
        }
        Editor edit = a().edit();
        edit.clear();
        edit.commit();
    }
}
