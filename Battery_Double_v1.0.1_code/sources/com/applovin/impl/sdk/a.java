package com.applovin.impl.sdk;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.HashMap;
import java.util.Map;

class a {
    private static final Object a = new Object();
    private static final Map b = new HashMap();

    static Map a(AppLovinSdkImpl appLovinSdkImpl) {
        Map c;
        synchronized (a) {
            appLovinSdkImpl.getLogger().d("AdDataCache", "Reading cached device data...");
            c = c(appLovinSdkImpl);
        }
        return c;
    }

    private static void a(String str, Map map) {
        String[] split = str.split("=");
        if (split.length == 2) {
            map.put(split[0], split[1]);
        }
    }

    static void a(Map map, AppLovinSdkImpl appLovinSdkImpl) {
        b(map, appLovinSdkImpl);
    }

    static void b(AppLovinSdkImpl appLovinSdkImpl) {
        synchronized (a) {
            appLovinSdkImpl.getLogger().d("AdDataCache", "Clearing old device data cache...");
            a((Map) new HashMap(0), appLovinSdkImpl);
        }
    }

    private static void b(Map map, AppLovinSdkImpl appLovinSdkImpl) {
        if (map == null) {
            throw new IllegalArgumentException("No ad aata specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            try {
                synchronized (b) {
                    Map map2 = (Map) b.get("ad_data_cache");
                    if (map2 == null) {
                        map2 = new HashMap();
                    }
                    map2.clear();
                    map2.putAll(map);
                    b.put("ad_data_cache", map2);
                }
                Editor edit = appLovinSdkImpl.getSettingsManager().a().edit();
                edit.putString("ad_data_cache", cd.a(map));
                edit.commit();
                appLovinSdkImpl.getLogger().d("AdDataCache", map.size() + " " + "ad_data_cache" + " entries saved in cache");
            } catch (Exception e) {
                appLovinSdkImpl.getLogger().e("AdDataCache", "Unable to save ad data entries", e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009f  */
    private static Map c(AppLovinSdkImpl appLovinSdkImpl) {
        Map map;
        Map map2;
        Exception e;
        synchronized (b) {
            map = (Map) b.get("ad_data_cache");
        }
        if (map == null) {
            SharedPreferences a2 = appLovinSdkImpl.getSettingsManager().a();
            String string = a2.getString("ad_data_cache", "");
            if (string != null && string.length() > 0) {
                try {
                    map2 = new HashMap();
                    try {
                        for (String a3 : string.split("&")) {
                            a(a3, map2);
                        }
                        synchronized (b) {
                            b.put("ad_data_cache", map2);
                        }
                        appLovinSdkImpl.getLogger().d("AdDataCache", map2.size() + " " + "ad_data_cache" + " entries loaded from cache");
                    } catch (Exception e2) {
                        e = e2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    map2 = map;
                    e = exc;
                    appLovinSdkImpl.getLogger().e("AdDataCache", "Unable to load ad data", e);
                    Editor edit = a2.edit();
                    edit.putString("ad_data_cache", "");
                    edit.commit();
                    if (map2 != null) {
                    }
                }
                return map2 != null ? new HashMap(map2) : new HashMap();
            }
        }
        map2 = map;
        if (map2 != null) {
        }
    }
}
