package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinLogger;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class bn {
    /* access modifiers changed from: private */
    public final AppLovinSdkImpl a;
    /* access modifiers changed from: private */
    public final AppLovinLogger b;
    private final ScheduledExecutorService c = a("main");
    private final ScheduledExecutorService d = a("back");

    bn(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
    }

    private static void a(Runnable runnable, long j, ScheduledExecutorService scheduledExecutorService) {
        if (j > 0) {
            scheduledExecutorService.schedule(runnable, j, TimeUnit.MILLISECONDS);
        } else {
            scheduledExecutorService.submit(runnable);
        }
    }

    /* access modifiers changed from: 0000 */
    public ScheduledExecutorService a(String str) {
        return Executors.newScheduledThreadPool(1, new bp(this, str));
    }

    /* access modifiers changed from: 0000 */
    public void a(ba baVar, bo boVar) {
        a(baVar, boVar, 0);
    }

    /* access modifiers changed from: 0000 */
    public void a(ba baVar, bo boVar, long j) {
        if (baVar == null) {
            throw new IllegalArgumentException("No task specified");
        } else if (j < 0) {
            throw new IllegalArgumentException("Invalid delay specified: " + j);
        } else {
            this.b.d(baVar.e, "Scheduling " + baVar.e + " on " + boVar + " queue in " + j + "ms.");
            br brVar = new br(this, baVar, boVar);
            if (boVar == bo.MAIN) {
                a((Runnable) brVar, j, this.c);
            } else {
                a((Runnable) brVar, j, this.d);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(bm bmVar, long j) {
        if (bmVar == null) {
            throw new IllegalArgumentException("No task specified");
        }
        a((Runnable) bmVar, j, this.c);
    }
}
