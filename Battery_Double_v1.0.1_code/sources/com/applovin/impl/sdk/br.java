package com.applovin.impl.sdk;

class br implements Runnable {
    final /* synthetic */ bn a;
    private final String b;
    private final ba c;
    private final bo d;

    br(bn bnVar, ba baVar, bo boVar) {
        this.a = bnVar;
        this.b = baVar.a();
        this.c = baVar;
        this.d = boVar;
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            m.a();
            if (this.a.a.c()) {
                this.a.b.i(this.b, "Task re-scheduled...");
                this.a.a(this.c, this.d, 2000);
            } else if (this.a.a.isEnabled()) {
                this.a.b.i(this.b, "Task started execution...");
                this.c.run();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                if (this.c instanceof cc) {
                    bx.a().a(((cc) this.c).f(), currentTimeMillis2, p.a(this.a.a));
                }
                this.a.b.i(this.b, "Task executed successfully in " + currentTimeMillis2 + "ms.");
                bg b2 = this.a.a.b();
                b2.a(this.b + "_count");
                b2.a(this.b + "_time", currentTimeMillis2);
            } else {
                if (this.a.a.d()) {
                    this.a.a.e();
                } else {
                    this.a.b.w(this.b, "Task not executed, SDK is disabled");
                }
                this.c.b();
            }
        } catch (Throwable th) {
            this.a.b.e(this.b, "Task failed execution in " + (System.currentTimeMillis() - currentTimeMillis) + "ms.", th);
        }
    }
}
