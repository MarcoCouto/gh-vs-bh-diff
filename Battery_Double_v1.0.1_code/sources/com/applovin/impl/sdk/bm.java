package com.applovin.impl.sdk;

import android.content.Context;
import android.support.v4.os.EnvironmentCompat;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinLogger;

class bm implements Runnable {
    private final AppLovinSdkImpl a;
    private final AppLovinLogger b;
    private final Context c;

    bm(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.c = appLovinSdkImpl.getApplicationContext();
        this.b = appLovinSdkImpl.getLogger();
    }

    private void c() {
        String str = (String) this.a.a(bb.J);
        e eVar = (e) this.a.getAdService();
        if (str.length() > 0) {
            for (String fromString : str.split(",")) {
                eVar.a(AppLovinAdSize.fromString(fromString), AppLovinAdType.REGULAR);
            }
        }
        if (((Boolean) this.a.a(bb.K)).booleanValue()) {
            eVar.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        if (q.a("android.permission.INTERNET", this.c)) {
            return true;
        }
        this.b.userError("TaskInitializeSdk", "Unable to enable AppLovin SDK: no android.permission.INTERNET");
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (cd.a(bb.m, this.a)) {
            this.a.a().a((ba) new bi(this.a), bo.BACKGROUND, 1500);
        }
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        this.b.d("TaskInitializeSdk", "Initializing AppLovin SDK 5.4.3...");
        try {
            if (a()) {
                bg b2 = this.a.b();
                b2.c();
                b2.c("ad_imp_session");
                a.b(this.a);
                this.a.getFileManager().d(this.c);
                b();
                c();
                if (((String) this.a.a(bb.P)).equals(EnvironmentCompat.MEDIA_UNKNOWN)) {
                    this.a.getSettingsManager().a(bb.P, "true");
                }
                this.a.a(true);
            } else {
                this.a.a(false);
            }
            this.b.d("TaskInitializeSdk", "AppLovin SDK 5.4.3 initialization " + (this.a.isEnabled() ? "succeeded" : "failed") + " in " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        } catch (Throwable th) {
            Throwable th2 = th;
            this.b.d("TaskInitializeSdk", "AppLovin SDK 5.4.3 initialization " + (this.a.isEnabled() ? "succeeded" : "failed") + " in " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
            throw th2;
        }
    }
}
