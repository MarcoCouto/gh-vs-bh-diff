package com.applovin.impl.sdk;

import com.applovin.impl.adview.t;
import com.applovin.impl.sdk.AppLovinAdInternal.AdTarget;
import com.applovin.impl.sdk.AppLovinAdInternal.Builder;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class bs extends ba implements cc {
    private static volatile int b = 0;
    private static volatile int c = 0;
    private final Collection a;
    private final JSONObject d;
    private final AppLovinAdLoadListener i;
    private final v j;
    private c k = new c(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.REGULAR);

    bs(JSONObject jSONObject, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl) {
        super("RenderAd", appLovinSdkImpl);
        this.d = jSONObject;
        this.i = appLovinAdLoadListener;
        this.a = e();
        this.j = appLovinSdkImpl.getFileManager();
    }

    private float a(String str, AppLovinAdType appLovinAdType, int i2) {
        if (appLovinAdType.equals(AppLovinAdType.INCENTIVIZED)) {
            return 0.5f;
        }
        return (!appLovinAdType.equals(AppLovinAdType.REGULAR) || str == null || i2 != -1) ? 0.0f : 0.5f;
    }

    private t a(int i2) {
        return i2 == 1 ? t.WhiteXOnTransparentGrey : t.WhiteXOnOpaqueBlack;
    }

    private t a(String str) {
        return str != null ? t.WhiteXOnTransparentGrey : t.WhiteXOnOpaqueBlack;
    }

    private String a(String str, AppLovinAdType appLovinAdType) {
        boolean equals = AppLovinAdType.INCENTIVIZED.equals(appLovinAdType);
        String str2 = "alvideo" + (equals ? c : b) + (equals ? "a" : "") + str.substring(str.lastIndexOf(".") + 1, str.length());
        File a2 = this.j.a(str2, this.f.getApplicationContext(), false);
        this.f.getFileManager().a(a2);
        if (!a(a2, str)) {
            return null;
        }
        if (equals) {
            c = (c + 1) % 4;
            return str2;
        }
        b = (b + 1) % 4;
        return str2;
    }

    private String a(String str, String str2) {
        File a2 = this.j.a(str2.replace("/", "_"), this.f.getApplicationContext(), true);
        if (a2 == null) {
            return null;
        }
        if (a2.exists()) {
            this.g.d(this.e, "Loaded " + str2 + " from cache: file://" + a2.getAbsolutePath());
            return "file://" + a2.getAbsolutePath();
        } else if (a(a2, str + str2)) {
            return "file://" + a2.getAbsolutePath();
        } else {
            return null;
        }
    }

    private void a(JSONObject jSONObject) {
        int i2;
        float a2;
        t a3;
        String str;
        int i3 = 0;
        String string = jSONObject.getString("html");
        AppLovinAdSize appLovinAdSize = jSONObject.has("size") ? AppLovinAdSize.fromString(jSONObject.getString("size")) : AppLovinAdSize.BANNER;
        String str2 = null;
        if (string == null || string.length() <= 0) {
            this.g.e(this.e, "No HTML received for requested ad");
            d();
            return;
        }
        String b2 = b(string);
        AdTarget adTarget = jSONObject.has("ad_target") ? AdTarget.valueOf(jSONObject.getString("ad_target").toUpperCase(Locale.ENGLISH)) : AdTarget.DEFAULT;
        AppLovinAdType appLovinAdType = jSONObject.has("ad_type") ? AppLovinAdType.fromString(jSONObject.getString("ad_type").toUpperCase(Locale.ENGLISH)) : AppLovinAdType.REGULAR;
        this.k = new c(appLovinAdSize, appLovinAdType);
        if (jSONObject.has("video")) {
            String string2 = jSONObject.getString("video");
            if (string2 != null && !string2.isEmpty()) {
                str2 = a(string2, appLovinAdType);
            }
        }
        long j2 = -1;
        if (jSONObject.has("ad_id")) {
            j2 = jSONObject.getLong("ad_id");
        }
        if (jSONObject.has("countdown_length")) {
            try {
                i2 = jSONObject.getInt("countdown_length");
            } catch (JSONException e) {
                i2 = i3;
            }
        } else {
            i2 = i3;
        }
        if (jSONObject.has("close_delay")) {
            try {
                i3 = jSONObject.getInt("close_delay");
            } catch (JSONException e2) {
            }
        }
        if (jSONObject.has("close_delay_graphic")) {
            try {
                a2 = (float) jSONObject.getInt("close_delay_graphic");
            } catch (JSONException e3) {
                a2 = a(str2, appLovinAdType, i3);
            }
        } else {
            a2 = a(str2, appLovinAdType, i3);
        }
        if (jSONObject.has("close_style")) {
            try {
                a3 = a(jSONObject.getInt("close_style"));
            } catch (JSONException e4) {
                a3 = a(str2);
            }
        } else {
            a3 = a(str2);
        }
        if (jSONObject.has("clcodes")) {
            try {
                str = ((JSONArray) jSONObject.get("clcodes")).getString(0);
            } catch (JSONException e5) {
                str = "";
            }
        } else {
            str = "";
        }
        a((AppLovinAd) new Builder().setHtml(b2).setSize(appLovinAdSize).setType(appLovinAdType).setVideoFilename(str2).setTarget(adTarget).setCloseStyle(a3).setVideoCloseDelay((float) i3).setPoststitialCloseDelay(a2).setCountdownLength(i2).setCurrentAdIdNumber(j2).setClCode(str).create());
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0103 A[SYNTHETIC, Splitter:B:47:0x0103] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0108 A[SYNTHETIC, Splitter:B:50:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x010d A[SYNTHETIC, Splitter:B:53:0x010d] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0117 A[SYNTHETIC, Splitter:B:59:0x0117] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011c A[SYNTHETIC, Splitter:B:62:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0121 A[SYNTHETIC, Splitter:B:65:0x0121] */
    private boolean a(File file, String str) {
        ByteArrayOutputStream byteArrayOutputStream;
        HttpURLConnection httpURLConnection;
        ByteArrayOutputStream byteArrayOutputStream2;
        InputStream inputStream = null;
        this.g.d(this.e, "Starting caching of " + str + " into " + file.getAbsoluteFile());
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
                try {
                    httpURLConnection2.setConnectTimeout(((Integer) this.f.a(bb.t)).intValue());
                    httpURLConnection2.setReadTimeout(((Integer) this.f.a(bb.v)).intValue());
                    httpURLConnection2.setDefaultUseCaches(true);
                    httpURLConnection2.setUseCaches(true);
                    httpURLConnection2.setAllowUserInteraction(false);
                    httpURLConnection2.setInstanceFollowRedirects(true);
                    inputStream = httpURLConnection2.getInputStream();
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream.read(bArr, 0, bArr.length);
                        if (read < 0) {
                            break;
                        }
                        try {
                            byteArrayOutputStream.write(bArr, 0, read);
                        } catch (Exception e) {
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e2) {
                            }
                            d();
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (Exception e3) {
                                }
                            }
                            if (byteArrayOutputStream != null) {
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e4) {
                                }
                            }
                            if (httpURLConnection2 != null) {
                                try {
                                    httpURLConnection2.disconnect();
                                } catch (Exception e5) {
                                }
                            }
                            return false;
                        }
                    }
                    if (!this.j.a(byteArrayOutputStream, file)) {
                        d();
                    }
                    this.g.d(this.e, "Caching completed for " + file);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e6) {
                        }
                    }
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e7) {
                        }
                    }
                    if (httpURLConnection2 != null) {
                        try {
                            httpURLConnection2.disconnect();
                        } catch (Exception e8) {
                        }
                    }
                    return true;
                } catch (IOException e9) {
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    HttpURLConnection httpURLConnection3 = httpURLConnection2;
                    e = e9;
                    httpURLConnection = httpURLConnection3;
                    try {
                        this.g.e(this.e, "Failed to cache \"" + str + "\" into \"" + file.getAbsolutePath() + "\"", e);
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (Exception e10) {
                            }
                        }
                        if (byteArrayOutputStream2 != null) {
                            try {
                                byteArrayOutputStream2.close();
                            } catch (Exception e11) {
                            }
                        }
                        if (httpURLConnection != null) {
                            try {
                                httpURLConnection.disconnect();
                            } catch (Exception e12) {
                            }
                        }
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        byteArrayOutputStream = byteArrayOutputStream2;
                        if (inputStream != null) {
                        }
                        if (byteArrayOutputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    httpURLConnection = httpURLConnection2;
                    th = th3;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e13) {
                        }
                    }
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e14) {
                        }
                    }
                    if (httpURLConnection != null) {
                        try {
                            httpURLConnection.disconnect();
                        } catch (Exception e15) {
                        }
                    }
                    throw th;
                }
            } catch (IOException e16) {
                e = e16;
                httpURLConnection = null;
                byteArrayOutputStream2 = byteArrayOutputStream;
                this.g.e(this.e, "Failed to cache \"" + str + "\" into \"" + file.getAbsolutePath() + "\"", e);
                if (inputStream != null) {
                }
                if (byteArrayOutputStream2 != null) {
                }
                if (httpURLConnection != null) {
                }
                return false;
            } catch (Throwable th4) {
                th = th4;
                httpURLConnection = null;
                if (inputStream != null) {
                }
                if (byteArrayOutputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                throw th;
            }
        } catch (IOException e17) {
            e = e17;
            httpURLConnection = null;
            byteArrayOutputStream2 = null;
            this.g.e(this.e, "Failed to cache \"" + str + "\" into \"" + file.getAbsolutePath() + "\"", e);
            if (inputStream != null) {
            }
            if (byteArrayOutputStream2 != null) {
            }
            if (httpURLConnection != null) {
            }
            return false;
        } catch (Throwable th5) {
            th = th5;
            httpURLConnection = null;
            byteArrayOutputStream = null;
            if (inputStream != null) {
            }
            if (byteArrayOutputStream != null) {
            }
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    private String b(String str) {
        return ((Boolean) this.f.a(bb.H)).booleanValue() ? c(str) : str;
    }

    private String c(String str) {
        String[] split;
        StringBuilder sb = new StringBuilder(str);
        for (String str2 : ((String) this.f.a(bb.I)).split(",")) {
            int i2 = 0;
            int i3 = 0;
            while (i3 < sb.length()) {
                i3 = sb.indexOf(str2, i2);
                if (i3 == -1) {
                    break;
                }
                int length = sb.length();
                i2 = i3;
                while (!this.a.contains(Character.valueOf(sb.charAt(i2))) && i2 < length) {
                    i2++;
                }
                if (i2 <= i3 || i2 == length) {
                    this.g.d(this.e, "Unable to cache resource; ad HTML is invalid.");
                } else {
                    String a2 = a(str2, sb.substring(str2.length() + i3, i2));
                    if (a2 != null) {
                        sb.replace(i3, i2, a2);
                    }
                }
            }
        }
        return sb.toString();
    }

    private Collection e() {
        HashSet hashSet = new HashSet();
        for (char valueOf : ((String) this.f.a(bb.ap)).toCharArray()) {
            hashSet.add(Character.valueOf(valueOf));
        }
        hashSet.add(Character.valueOf('\"'));
        return hashSet;
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAd appLovinAd) {
        if (this.i != null) {
            this.i.adReceived(appLovinAd);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        try {
            if (this.i == null) {
                return;
            }
            if (this.i instanceof u) {
                ((u) this.i).a(this.k, -6);
            } else {
                this.i.failedToReceiveAd(-6);
            }
        } catch (Throwable th) {
            this.g.e(this.e, "Unable process a failure to receive an ad", th);
        }
    }

    public String f() {
        return "tRA";
    }

    public void run() {
        this.g.d(this.e, "Rendering ad...");
        try {
            a(this.d);
        } catch (JSONException e) {
            this.g.e(this.e, "Unable to parse ad service response", e);
            d();
        } catch (IllegalArgumentException e2) {
            this.g.e(this.e, "Ad response is not valid", e2);
            d();
        } catch (Exception e3) {
            this.g.e(this.e, "Unable to render ad", e3);
            d();
        }
    }
}
