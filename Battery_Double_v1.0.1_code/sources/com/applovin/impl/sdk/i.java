package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Map;

class i extends ba {
    final /* synthetic */ e a;
    private final AppLovinAdSize b;

    public i(e eVar, AppLovinAdSize appLovinAdSize) {
        this.a = eVar;
        super("UpdateAdTask", eVar.a);
        this.b = appLovinAdSize;
    }

    public void run() {
        boolean z = true;
        h hVar = (h) ((Map) this.a.d.get(AppLovinAdType.REGULAR)).get(this.b);
        synchronized (hVar.b) {
            boolean a2 = this.a.a(this.b);
            boolean d = this.a.a();
            boolean z2 = !hVar.f.isEmpty();
            if (System.currentTimeMillis() <= hVar.d) {
                z = false;
            }
            if (a2 && z2 && z && d && !hVar.e) {
                hVar.e = true;
                this.a.b(this.b, AppLovinAdType.REGULAR, new g(this.a, hVar));
            }
        }
    }
}
