package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.sdk.AppLovinLogger;

class j implements AppLovinLogger {
    private be a;
    private k b;

    j() {
    }

    /* access modifiers changed from: 0000 */
    public void a(be beVar) {
        this.a = beVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(k kVar) {
        this.b = kVar;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        if (this.a != null) {
            return ((Boolean) this.a.a(bb.j)).booleanValue();
        }
        return false;
    }

    public void d(String str, String str2) {
        if (a()) {
            Log.d(AppLovinLogger.SDK_TAG, "[" + str + "] " + str2);
        }
        if (this.b != null) {
            this.b.a("DEBUG  [" + str + "] " + str2);
        }
    }

    public void e(String str, String str2) {
        e(str, str2, null);
    }

    public void e(String str, String str2, Throwable th) {
        if (a()) {
            Log.e(AppLovinLogger.SDK_TAG, "[" + str + "] " + str2, th);
        }
        if (this.b != null) {
            this.b.a("ERROR  [" + str + "] " + str2 + (th != null ? ": " + th.getMessage() : ""));
        }
    }

    public void i(String str, String str2) {
        if (a()) {
            Log.i(AppLovinLogger.SDK_TAG, "[" + str + "] " + str2);
        }
        if (this.b != null) {
            this.b.a("INFO  [" + str + "] " + str2);
        }
    }

    public void userError(String str, String str2) {
        userError(str, str2, null);
    }

    public void userError(String str, String str2, Throwable th) {
        Log.e(AppLovinLogger.SDK_TAG, "[" + str + "] " + str2, th);
        if (this.b != null) {
            this.b.a("USER  [" + str + "] " + str2 + (th != null ? ": " + th.getMessage() : ""));
        }
    }

    public void w(String str, String str2) {
        w(str, str2, null);
    }

    public void w(String str, String str2, Throwable th) {
        if (a()) {
            Log.w(AppLovinLogger.SDK_TAG, "[" + str + "] " + str2, th);
        }
        if (this.b != null) {
            this.b.a("WARN  [" + str + "] " + str2);
        }
    }
}
