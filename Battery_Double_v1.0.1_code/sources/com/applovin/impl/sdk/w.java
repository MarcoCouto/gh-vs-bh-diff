package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.lang.ref.SoftReference;

public class w {
    private static String k = null;
    /* access modifiers changed from: private */
    public final AppLovinSdkImpl a;
    private final e b;
    /* access modifiers changed from: private */
    public AppLovinAdInternal c;
    private SoftReference d;
    /* access modifiers changed from: private */
    public final Handler e;
    private final Object f = new Object();
    private volatile String g;
    /* access modifiers changed from: private */
    public ca h;
    /* access modifiers changed from: private */
    public volatile boolean i = false;
    private SoftReference j;

    public w(AppLovinSdk appLovinSdk) {
        this.a = (AppLovinSdkImpl) appLovinSdk;
        this.b = (e) appLovinSdk.getAdService();
        this.e = new Handler(Looper.getMainLooper());
    }

    public static void a(String str) {
        k = str;
    }

    public static String b() {
        return k;
    }

    private void e() {
        if (this.d != null) {
            AppLovinAdLoadListener appLovinAdLoadListener = (AppLovinAdLoadListener) this.d.get();
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.failedToReceiveAd(AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED);
            }
        }
    }

    public void a(Activity activity, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (!a()) {
            this.a.getLogger().userError("IncentivizedAdController", "Skipping incentivized video playback: user attempted to play an incentivized video before one was preloaded.");
            e();
        } else if (cd.c(this.c.getVideoFilename()) && !this.a.getFileManager().a(this.c.getVideoFilename(), (Context) activity)) {
        } else {
            if (((Boolean) this.a.a(bb.ah)).booleanValue()) {
                an anVar = new an(this.a, this);
                anVar.a(activity);
                anVar.a(appLovinAdDisplayListener);
                anVar.a(appLovinAdClickListener);
                anVar.a(appLovinAdVideoPlaybackListener);
                anVar.a(appLovinAdRewardListener);
                anVar.a();
                return;
            }
            b(activity, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAd appLovinAd, AppLovinAdRewardListener appLovinAdRewardListener) {
        this.h = new ca(this.a, appLovinAd, appLovinAdRewardListener);
        this.a.a().a((ba) this.h, bo.BACKGROUND);
    }

    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.d = new SoftReference(appLovinAdLoadListener);
        if (a()) {
            this.a.getLogger().userError("IncentivizedAdController", "Attempted to call preloadAndNotify: while an ad was already loaded or currently being played. Do not call preloadAndNotify: again until the last ad has been closed (adHidden).");
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.adReceived(this.c);
                return;
            }
            return;
        }
        this.b.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, new y(this, appLovinAdLoadListener));
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAdRewardListener appLovinAdRewardListener) {
        appLovinAdRewardListener.userDeclinedToViewAd(this.c);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, Activity activity) {
        if (str != null && ((Boolean) this.a.a(bb.ai)).booleanValue()) {
            new al(this.a, activity, str).a();
        }
    }

    public boolean a() {
        return this.c != null;
    }

    /* access modifiers changed from: 0000 */
    public void b(Activity activity, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (a()) {
            AppLovinAdInternal appLovinAdInternal = this.c;
            if (appLovinAdInternal.getType().equals(AppLovinAdType.INCENTIVIZED)) {
                AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.a, activity);
                ab abVar = new ab(this, activity, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
                create.setAdDisplayListener(abVar);
                create.setAdVideoPlaybackListener(abVar);
                create.setAdClickListener(abVar);
                create.showAndRender(appLovinAdInternal);
                this.j = new SoftReference(create);
                a((AppLovinAd) appLovinAdInternal, (AppLovinAdRewardListener) abVar);
                return;
            }
            this.a.getLogger().e("IncentivizedAdController", "Attempted to render an ad of type " + this.c.getType() + " in an Incentivized Ad interstitial.");
            appLovinAdVideoPlaybackListener.videoPlaybackEnded(this.c, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, false);
            return;
        }
        this.a.getLogger().userError("IncentivizedAdController", "Skipping incentivized video playback: user attempted to play an incentivized video before one was preloaded.");
        e();
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        synchronized (this.f) {
            this.g = str;
        }
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        String str;
        synchronized (this.f) {
            str = this.g;
        }
        return str;
    }

    public void d() {
        if (this.j != null) {
            AppLovinInterstitialAdDialog appLovinInterstitialAdDialog = (AppLovinInterstitialAdDialog) this.j.get();
            if (appLovinInterstitialAdDialog != null) {
                appLovinInterstitialAdDialog.dismiss();
            }
        }
    }
}
