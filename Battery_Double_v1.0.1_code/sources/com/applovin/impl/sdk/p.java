package com.applovin.impl.sdk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.os.EnvironmentCompat;
import java.io.InputStream;
import java.util.Scanner;
import org.json.JSONException;
import org.json.JSONObject;

class p {
    private static final int[] a = {7, 4, 2, 1, 11};
    private static final int[] b = {5, 6, 10, 3, 9, 8, 14};
    private static final int[] c = {15, 12, 13};
    private static final String d = p.class.getSimpleName();

    p() {
    }

    private static NetworkInfo a(Context context) {
        if (q.a("android.permission.ACCESS_NETWORK_STATE", context)) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                return connectivityManager.getActiveNetworkInfo();
            }
        }
        return null;
    }

    static String a(AppLovinSdkImpl appLovinSdkImpl) {
        NetworkInfo a2 = a(appLovinSdkImpl.getApplicationContext());
        if (a2 == null) {
            return EnvironmentCompat.MEDIA_UNKNOWN;
        }
        int type = a2.getType();
        int subtype = a2.getSubtype();
        String str = type == 1 ? "wifi" : type == 0 ? a(subtype, a) ? "2g" : a(subtype, b) ? "3g" : a(subtype, c) ? "4g" : "mobile" : EnvironmentCompat.MEDIA_UNKNOWN;
        appLovinSdkImpl.getLogger().d(d, "Network " + type + "/" + subtype + " resolved to " + str);
        return str;
    }

    static String a(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {
            sb.append(scanner.nextLine());
        }
        return sb.toString();
    }

    static String a(String str) {
        return str.startsWith("https://") ? str : str.replace("http://", "https://");
    }

    static String a(String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (str == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            String str2 = (String) appLovinSdkImpl.a(bb.e);
            StringBuilder sb = new StringBuilder();
            sb.append((String) appLovinSdkImpl.a(bb.k));
            sb.append(str);
            if (str2 == null || str2.length() <= 0) {
                sb.append("?api_key=");
                sb.append(appLovinSdkImpl.getSdkKey());
            } else {
                sb.append("?device_token=");
                sb.append(str2);
            }
            return sb.toString();
        }
    }

    static JSONObject a(JSONObject jSONObject) {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    static void a(int i, AppLovinSdkImpl appLovinSdkImpl) {
        be settingsManager = appLovinSdkImpl.getSettingsManager();
        if (i == 401) {
            settingsManager.a(bb.c, "");
            settingsManager.a(bb.e, "");
            settingsManager.a(bb.m, Long.valueOf(0));
            settingsManager.b();
        } else if (i == 418) {
            settingsManager.a(bb.a, Boolean.valueOf(true));
            settingsManager.b();
        } else if (i >= 400 && i < 500) {
            appLovinSdkImpl.f();
        } else if (i == -1) {
            appLovinSdkImpl.f();
        }
    }

    static void a(JSONObject jSONObject, AppLovinSdkImpl appLovinSdkImpl) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            try {
                if (jSONObject.has("settings")) {
                    be settingsManager = appLovinSdkImpl.getSettingsManager();
                    if (!jSONObject.isNull("settings")) {
                        settingsManager.a(jSONObject.getJSONObject("settings"));
                        settingsManager.b();
                        appLovinSdkImpl.getLogger().d(d, "New settings processed");
                    }
                }
            } catch (JSONException e) {
                appLovinSdkImpl.getLogger().e(d, "Unable to parse settings out of API response", e);
            }
        }
    }

    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    static String b(String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (str == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (appLovinSdkImpl != null) {
            return ((String) appLovinSdkImpl.a(bb.l)) + str;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    static void b(int i, AppLovinSdkImpl appLovinSdkImpl) {
        if (i == 418) {
            be settingsManager = appLovinSdkImpl.getSettingsManager();
            settingsManager.a(bb.a, Boolean.valueOf(true));
            settingsManager.b();
        }
    }
}
