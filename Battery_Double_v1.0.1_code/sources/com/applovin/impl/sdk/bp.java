package com.applovin.impl.sdk;

import java.util.concurrent.ThreadFactory;

class bp implements ThreadFactory {
    final /* synthetic */ bn a;
    private final String b;

    public bp(bn bnVar, String str) {
        this.a = bnVar;
        this.b = str;
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, "AppLovinSdk:" + this.b + ":" + cd.a(this.a.a.getSdkKey()));
        thread.setDaemon(true);
        thread.setPriority(10);
        thread.setUncaughtExceptionHandler(new bq(this));
        return thread;
    }
}
