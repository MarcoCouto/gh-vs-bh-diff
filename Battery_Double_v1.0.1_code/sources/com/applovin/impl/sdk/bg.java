package com.applovin.impl.sdk;

import android.content.SharedPreferences.Editor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

class bg {
    private final AppLovinSdkImpl a;
    private final Map b = new HashMap();

    bg(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.a = appLovinSdkImpl;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        synchronized (this.b) {
            this.b.clear();
        }
        d();
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        a(str, 1);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, long j) {
        synchronized (this.b) {
            Long l = (Long) this.b.get(str);
            if (l == null) {
                l = Long.valueOf(0);
            }
            this.b.put(str, Long.valueOf(l.longValue() + j));
        }
        d();
    }

    /* access modifiers changed from: 0000 */
    public long b(String str) {
        long longValue;
        synchronized (this.b) {
            Long l = (Long) this.b.get(str);
            if (l == null) {
                l = Long.valueOf(0);
            }
            longValue = l.longValue();
        }
        return longValue;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject b() {
        JSONObject jSONObject;
        synchronized (this.b) {
            jSONObject = new JSONObject();
            for (Entry entry : this.b.entrySet()) {
                jSONObject.put((String) entry.getKey(), entry.getValue());
            }
        }
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str, long j) {
        synchronized (this.b) {
            this.b.put(str, Long.valueOf(j));
        }
        d();
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        try {
            JSONObject jSONObject = new JSONObject(this.a.getSettingsManager().a().getString("stats", "{}"));
            synchronized (this.b) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    try {
                        String str = (String) keys.next();
                        this.b.put(str, Long.valueOf(jSONObject.getLong(str)));
                    } catch (JSONException e) {
                    }
                }
            }
        } catch (Throwable th) {
            this.a.getLogger().e("StatsManager", "Unable to load stats", th);
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(String str) {
        synchronized (this.b) {
            this.b.remove(str);
        }
        d();
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        try {
            Editor edit = this.a.getSettingsManager().a().edit();
            edit.putString("stats", b().toString());
            edit.commit();
        } catch (JSONException e) {
            this.a.getLogger().e("StatsManager", "Unable to save stats", e);
        }
    }
}
