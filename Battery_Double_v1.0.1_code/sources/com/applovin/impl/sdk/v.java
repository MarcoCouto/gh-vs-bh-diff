package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class v {
    private final AppLovinLogger a;
    private final AppLovinSdkImpl b;
    private final String c = "FileManager";
    private final Object d;

    v(AppLovinSdk appLovinSdk) {
        this.b = (AppLovinSdkImpl) appLovinSdk;
        this.a = appLovinSdk.getLogger();
        this.d = new Object();
    }

    /* access modifiers changed from: 0000 */
    public long a(long j) {
        return j / 1048576;
    }

    public File a(String str, Context context, boolean z) {
        File file;
        this.a.d("FileManager", "Looking up cached resource: " + str);
        if (!a(context) && !z) {
            return null;
        }
        if (str.contains("icon")) {
            str = str.replace("/", "_").replace(".", "_");
        }
        synchronized (this.d) {
            File b2 = b(context);
            file = new File(b2, str);
            try {
                b2.mkdirs();
            } catch (Exception e) {
                return null;
            }
        }
        return file;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j, Context context) {
        long c2 = (long) c();
        if (c2 == -1) {
            this.a.d("FileManager", "Cache has no maximum size set; skipping drop...");
        } else if (a(j) > c2) {
            this.a.d("FileManager", "Cache has exceeded maximum size; dropping...");
            f(context);
            this.b.b().a("cache_drop_count");
        } else {
            this.a.d("FileManager", "Cache is present but under size limit; not dropping...");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return ((Boolean) this.b.a(bb.aA)).booleanValue();
    }

    /* access modifiers changed from: protected */
    public boolean a(Context context) {
        return m.a("android.permission.WRITE_EXTERNAL_STORAGE", context);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040 A[SYNTHETIC, Splitter:B:18:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004c A[SYNTHETIC, Splitter:B:26:0x004c] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:10:0x0031=Splitter:B:10:0x0031, B:28:0x004f=Splitter:B:28:0x004f} */
    public boolean a(ByteArrayOutputStream byteArrayOutputStream, File file) {
        FileOutputStream fileOutputStream;
        boolean z;
        this.a.d("FileManager", "Writing resource to filesystem: " + file.getName());
        synchronized (this.d) {
            try {
                fileOutputStream = new FileOutputStream(file);
                try {
                    byteArrayOutputStream.writeTo(fileOutputStream);
                    z = true;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (Exception e) {
                        }
                    }
                } catch (IOException e2) {
                    e = e2;
                    try {
                        this.a.e("FileManager", "Unable to write data to file", e);
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (Exception e3) {
                                z = false;
                            }
                        }
                        z = false;
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        if (fileOutputStream != null) {
                        }
                        throw th;
                    }
                }
            } catch (IOException e4) {
                e = e4;
                fileOutputStream = null;
                this.a.e("FileManager", "Unable to write data to file", e);
                if (fileOutputStream != null) {
                }
                z = false;
                return z;
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Exception e5) {
                    }
                }
                throw th;
            }
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(File file) {
        boolean z;
        this.a.d("FileManager", "Removing file " + file.getName() + " from filesystem...");
        synchronized (this.d) {
            try {
                z = file.delete();
            } catch (Exception e) {
                this.a.e("FileManager", "Failed to remove file " + file.getName() + " from filesystem!", e);
                z = false;
            }
        }
        return z;
    }

    public boolean a(String str, Context context) {
        boolean b2;
        synchronized (this.d) {
            b2 = b(str, context, false);
        }
        return b2;
    }

    /* access modifiers changed from: 0000 */
    public long b() {
        long longValue = ((Long) this.b.a(bb.aB)).longValue();
        if (longValue < 0 || !a()) {
            return -1;
        }
        return longValue;
    }

    /* access modifiers changed from: 0000 */
    public File b(Context context) {
        return a(context) ? new File(context.getExternalFilesDir(null), "al") : new File(context.getCacheDir(), "al");
    }

    public boolean b(String str, Context context, boolean z) {
        boolean z2;
        synchronized (this.d) {
            File a2 = a(str, context, z);
            z2 = a2 != null && a2.exists() && !a2.isDirectory();
        }
        return z2;
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        int intValue = ((Integer) this.b.a(bb.aC)).intValue();
        if (intValue < 0 || !a()) {
            return -1;
        }
        return intValue;
    }

    public List c(Context context) {
        List asList;
        File b2 = b(context);
        if (!b2.isDirectory()) {
            return new ArrayList(0);
        }
        synchronized (this.d) {
            asList = Arrays.asList(b2.listFiles());
        }
        return asList;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public void d(Context context) {
        try {
            if (!a()) {
                return;
            }
            if (this.b.isEnabled()) {
                this.a.e("FileManager", "Cannot empty file cache after SDK has completed initialization and ad loads are in progress!");
                return;
            }
            this.a.d("FileManager", "Compacting cache...");
            synchronized (this.d) {
                a(e(context), context);
            }
        } catch (Exception e) {
            this.a.e("FileManager", "Caught exception while compacting cache!", e);
            this.b.getSettingsManager().a(bb.aA, Boolean.valueOf(false));
            this.b.getSettingsManager().b();
        }
    }

    /* access modifiers changed from: 0000 */
    public long e(Context context) {
        long j = 0;
        long b2 = b();
        boolean z = b2 != -1;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        synchronized (this.d) {
            for (File file : c(context)) {
                boolean z2 = false;
                if (z && seconds - TimeUnit.MILLISECONDS.toSeconds(file.lastModified()) > b2) {
                    this.a.d("FileManager", "File " + file.getName() + " has expired, removing...");
                    a(file);
                    z2 = true;
                }
                if (z2) {
                    this.b.b().a("cached_files_expired");
                } else {
                    j += file.length();
                }
            }
        }
        return j;
    }

    /* access modifiers changed from: 0000 */
    public void f(Context context) {
        synchronized (this.d) {
            for (File a2 : c(context)) {
                a(a2);
            }
        }
    }
}
