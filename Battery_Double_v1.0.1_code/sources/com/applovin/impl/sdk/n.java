package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinLogger;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

class n {
    private final AppLovinSdkImpl a;
    private final AppLovinLogger b;

    n(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
    }

    private int a(Throwable th) {
        if (th instanceof SocketTimeoutException) {
            return AppLovinErrorCodes.FETCH_AD_TIMEOUT;
        }
        if (!(th instanceof IOException)) {
            return th instanceof JSONException ? -104 : -1;
        }
        String message = th.getMessage();
        return (message == null || !message.toLowerCase(Locale.ENGLISH).contains("authentication challenge")) ? -100 : 401;
    }

    private HttpURLConnection a(String str, String str2, int i) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i < 0 ? ((Integer) this.a.a(bb.t)).intValue() : i);
        if (i < 0) {
            i = ((Integer) this.a.a(bb.v)).intValue();
        }
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        return httpURLConnection;
    }

    private static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e) {
            }
        }
    }

    private void a(String str, int i, String str2, o oVar) {
        this.b.d("ConnectionManager", i + " received from from \"" + str2 + "\": " + str);
        if (i < 200 || i >= 300) {
            this.b.e("ConnectionManager", i + " error received from \"" + str2 + "\"");
            oVar.a(i);
            return;
        }
        JSONObject jSONObject = new JSONObject();
        if (!(i == 204 || str == null || str.length() <= 2)) {
            jSONObject = new JSONObject(str);
        }
        oVar.a(jSONObject, i);
    }

    private void a(String str, String str2, int i, long j) {
        this.b.i("ConnectionManager", "Successful " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s" + " over " + p.a(this.a) + " to \"" + str2 + "\"");
    }

    private void a(String str, String str2, int i, long j, Throwable th) {
        this.b.e("ConnectionManager", "Failed " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s" + " over " + p.a(this.a) + " to \"" + str2 + "\"", th);
    }

    private static void a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, int i, o oVar) {
        a(str, "GET", i, (JSONObject) null, oVar);
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00de A[SYNTHETIC, Splitter:B:30:0x00de] */
    public void a(String str, String str2, int i, JSONObject jSONObject, o oVar) {
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        InputStream inputStream2 = null;
        if (str == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No method specified");
        } else if (oVar == null) {
            throw new IllegalArgumentException("No callback specified");
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            int i2 = -1;
            try {
                this.b.i("ConnectionManager", "Sending " + str2 + " request to \"" + str + "\"...");
                httpURLConnection = a(str, str2, i);
                if (jSONObject != null) {
                    try {
                        String jSONObject2 = jSONObject.toString();
                        this.b.d("ConnectionManager", "Request to \"" + str + "\" is " + jSONObject2);
                        httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setFixedLengthStreamingMode(jSONObject2.getBytes(Charset.forName("UTF-8")).length);
                        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF8"));
                        printWriter.print(jSONObject2);
                        printWriter.close();
                    } catch (Throwable th) {
                        th = th;
                        if (i2 == 0) {
                        }
                        a(str2, str, i2, currentTimeMillis, th);
                        oVar.a(i2);
                        a(inputStream2);
                        a(httpURLConnection);
                        return;
                    }
                }
                i2 = httpURLConnection.getResponseCode();
                if (i2 > 0) {
                    InputStream inputStream3 = httpURLConnection.getInputStream();
                    try {
                        String a2 = p.a(inputStream3);
                        a(str2, str, i2, currentTimeMillis);
                        a(a2, httpURLConnection.getResponseCode(), str, oVar);
                        inputStream = inputStream3;
                    } catch (Throwable th2) {
                        th = th2;
                        inputStream2 = inputStream3;
                        a(inputStream2);
                        a(httpURLConnection);
                        throw th;
                    }
                } else {
                    a(str2, str, i2, currentTimeMillis, (Throwable) null);
                    oVar.a(i2);
                    inputStream = null;
                }
                a(inputStream);
                a(httpURLConnection);
            } catch (Throwable th3) {
                th = th3;
                httpURLConnection = null;
                a(inputStream2);
                a(httpURLConnection);
                throw th;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, JSONObject jSONObject, o oVar) {
        a(str, "POST", -1, jSONObject, oVar);
    }
}
