package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class ca extends ay {
    private final AppLovinAdInternal a;
    private final AppLovinAdRewardListener b;
    private final Object c = new Object();
    private volatile boolean d = false;

    public ca(AppLovinSdkImpl appLovinSdkImpl, AppLovinAd appLovinAd, AppLovinAdRewardListener appLovinAdRewardListener) {
        super("TaskValidateReward", appLovinSdkImpl);
        this.a = (AppLovinAdInternal) appLovinAd;
        this.b = appLovinAdRewardListener;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        if (!d()) {
            String str = "network_timeout";
            if (i < 400 || i > 500) {
                this.b.validationRequestFailed(this.a, i);
            } else {
                this.b.userRewardRejected(this.a, new HashMap(0));
                str = "rejected";
            }
            av.a().a((AppLovinAd) this.a, str);
        }
    }

    private void a(String str, Map map) {
        if (!d()) {
            av a2 = av.a();
            a2.a((AppLovinAd) this.a, str);
            a2.a((AppLovinAd) this.a, map);
            if (str.equals("accepted")) {
                this.b.userRewardVerified(this.a, map);
            } else if (str.equals("quota_exceeded")) {
                this.b.userOverQuota(this.a, map);
            } else if (str.equals("rejected")) {
                this.b.userRewardRejected(this.a, map);
            } else {
                this.b.validationRequestFailed(this.a, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        Map hashMap;
        String str;
        if (!d()) {
            try {
                JSONObject a2 = p.a(jSONObject);
                p.a(a2, this.f);
                try {
                    hashMap = au.a((JSONObject) a2.get("params"));
                } catch (Throwable th) {
                    hashMap = new HashMap(0);
                }
                try {
                    str = a2.getString("result");
                } catch (Throwable th2) {
                    str = "network_timeout";
                }
                a(str, hashMap);
            } catch (JSONException e) {
                this.g.e(this.e, "Unable to parse API response", e);
            }
        }
    }

    public void a(boolean z) {
        synchronized (this.c) {
            this.d = z;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        boolean z;
        synchronized (this.c) {
            z = this.d;
        }
        return z;
    }

    public void run() {
        String b2 = w.b();
        String clCode = this.a.getClCode();
        HashMap hashMap = new HashMap(2);
        if (cd.c(clCode)) {
            hashMap.put("clcode", clCode);
        } else {
            hashMap.put("clcode", "NO_CLCODE");
        }
        if (b2 != null) {
            hashMap.put("user_id", b2);
        }
        a("vr", new JSONObject(hashMap), new cb(this));
    }
}
