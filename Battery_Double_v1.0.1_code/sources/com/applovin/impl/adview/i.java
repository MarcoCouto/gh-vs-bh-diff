package com.applovin.impl.adview;

import com.applovin.adview.AppLovinInterstitialAdDialog;

class i implements Runnable {
    final /* synthetic */ AdViewControllerImpl a;

    private i(AdViewControllerImpl adViewControllerImpl) {
        this.a = adViewControllerImpl;
    }

    /* synthetic */ i(AdViewControllerImpl adViewControllerImpl, a aVar) {
        this(adViewControllerImpl);
    }

    public void run() {
        if (this.a.m != null) {
            try {
                AppLovinInterstitialAdDialog createInterstitialAdDialog = new InterstitialAdDialogCreatorImpl().createInterstitialAdDialog(this.a.b, this.a.a);
                createInterstitialAdDialog.setAdDisplayListener(new d(this.a));
                createInterstitialAdDialog.setAdVideoPlaybackListener(new e(this.a));
                createInterstitialAdDialog.setAdClickListener(new c(this.a));
                createInterstitialAdDialog.showAndRender(this.a.m);
            } catch (Throwable th) {
            }
        }
    }
}
