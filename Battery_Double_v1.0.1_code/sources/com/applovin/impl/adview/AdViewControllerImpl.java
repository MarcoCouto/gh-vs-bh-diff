package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewDatabase;
import android.widget.RelativeLayout;
import com.applovin.adview.AdViewController;
import com.applovin.impl.sdk.b;
import com.applovin.impl.sdk.bh;
import com.applovin.impl.sdk.cd;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicReference;

public class AdViewControllerImpl implements AdViewController {
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public AppLovinSdk b;
    private AppLovinAdService c;
    /* access modifiers changed from: private */
    public AppLovinLogger d;
    private AppLovinAdSize e;
    private r f;
    private l g;
    /* access modifiers changed from: private */
    public o h;
    private AppLovinAd i;
    private Runnable j;
    private Runnable k;
    private Runnable l;
    /* access modifiers changed from: private */
    public volatile AppLovinAd m = null;
    private final AtomicReference n = new AtomicReference();
    private volatile boolean o = false;
    private volatile boolean p = true;
    private volatile boolean q = false;
    private volatile boolean r = false;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener s;
    /* access modifiers changed from: private */
    public volatile AppLovinAdDisplayListener t;
    /* access modifiers changed from: private */
    public volatile AppLovinAdVideoPlaybackListener u;
    /* access modifiers changed from: private */
    public volatile AppLovinAdClickListener v;

    private void a(ViewGroup viewGroup, AppLovinSdk appLovinSdk, AppLovinAdSize appLovinAdSize, Context context) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (appLovinAdSize == null) {
            throw new IllegalArgumentException("No ad size specified");
        } else if (!(context instanceof Activity)) {
            throw new IllegalArgumentException("Specified context is not an activity");
        } else {
            this.b = appLovinSdk;
            this.c = appLovinSdk.getAdService();
            this.d = appLovinSdk.getLogger();
            this.e = appLovinAdSize;
            this.a = (Activity) context;
            this.i = cd.a();
            this.f = new r(this, appLovinSdk);
            this.l = new f(this, null);
            this.j = new k(this, null);
            this.k = new i(this, null);
            this.g = new l(this, appLovinSdk);
            if (a(context)) {
                this.h = b();
                viewGroup.setBackgroundColor(0);
                viewGroup.addView(this.h);
                b(this.h, appLovinAdSize);
                this.h.setVisibility(8);
                a((Runnable) new j(this, null));
                this.o = true;
                return;
            }
            this.d.userError("AppLovinAdView", "Web view database is corrupt, AdView not loaded");
        }
    }

    private void a(Runnable runnable) {
        this.a.runOnUiThread(runnable);
    }

    private static boolean a(Context context) {
        try {
            if (VERSION.SDK_INT >= 11) {
                return true;
            }
            WebViewDatabase instance = WebViewDatabase.getInstance(context);
            Method declaredMethod = WebViewDatabase.class.getDeclaredMethod("getCacheTotalSize", new Class[0]);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(instance, new Object[0]);
            return true;
        } catch (NoSuchMethodException e2) {
            Log.e("AppLovinAdView", "Error invoking getCacheTotalSize()", e2);
            return true;
        } catch (IllegalArgumentException e3) {
            Log.e("AppLovinAdView", "Error invoking getCacheTotalSize()", e3);
            return true;
        } catch (IllegalAccessException e4) {
            Log.e("AppLovinAdView", "Error invoking getCacheTotalSize()", e4);
            return true;
        } catch (InvocationTargetException e5) {
            Log.e("AppLovinAdView", "getCacheTotalSize() reported exception", e5);
            return false;
        } catch (Throwable th) {
            Log.e("AppLovinAdView", "Unexpected error while checking DB state", th);
            return false;
        }
    }

    private o b() {
        o oVar = new o(this.f, this.b, this.a);
        oVar.setBackgroundColor(0);
        oVar.setWillNotCacheDrawing(false);
        return oVar;
    }

    /* access modifiers changed from: private */
    public static void b(View view, AppLovinAdSize appLovinAdSize) {
        DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();
        int applyDimension = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getWidth() == -1 ? displayMetrics.widthPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getWidth(), displayMetrics);
        int applyDimension2 = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getHeight() == -1 ? displayMetrics.heightPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getHeight(), displayMetrics);
        LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        }
        layoutParams.width = applyDimension;
        layoutParams.height = applyDimension2;
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
            layoutParams2.addRule(10);
            layoutParams2.addRule(9);
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        a(this.l);
        a((Runnable) new h(this, this.m));
        this.m = null;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (!this.q) {
            this.c.addAdUpdateListener(this.g, this.e);
            a(this.l);
        }
        a((Runnable) new b(this, i2));
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAd appLovinAd) {
        new b(this.c).a(appLovinAd);
        a((Runnable) new g(this, appLovinAd));
    }

    /* access modifiers changed from: 0000 */
    public void b(AppLovinAd appLovinAd) {
        if (appLovinAd != null) {
            this.r = true;
            if (!this.q) {
                this.c.addAdUpdateListener(this.g, this.e);
                renderAd(appLovinAd);
            } else {
                this.n.set(appLovinAd);
                this.d.d("AppLovinAdView", "Ad view has paused when an ad was recieved, ad saved for later");
            }
            a((Runnable) new a(this, appLovinAd));
            return;
        }
        this.d.e("AppLovinAdView", "No provided when to the view controller");
        a(-1);
    }

    public void destroy() {
        if (this.c != null) {
            this.c.removeAdUpdateListener(this.g, getSize());
        }
        if (this.h != null) {
            try {
                this.h.removeAllViews();
                this.h.destroy();
            } catch (Throwable th) {
                this.d.w("AppLovinAdView", "Unable to destroy ad view", th);
            }
        }
        this.q = true;
    }

    public AppLovinAdSize getSize() {
        return this.e;
    }

    public void initializeAdView(ViewGroup viewGroup, Context context, AppLovinAdSize appLovinAdSize, AppLovinSdk appLovinSdk, AttributeSet attributeSet) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (context == null) {
            Log.e(AppLovinLogger.SDK_TAG, "Unable to create AppLovinAdView: no context provided. Please use a different constructor for this view.");
        } else {
            if (appLovinAdSize == null) {
                appLovinAdSize = m.a(attributeSet);
                if (appLovinAdSize == null) {
                    appLovinAdSize = AppLovinAdSize.BANNER;
                }
            }
            if (appLovinSdk == null) {
                appLovinSdk = AppLovinSdk.getInstance(context);
            }
            if (appLovinSdk != null && !appLovinSdk.hasCriticalErrors()) {
                a(viewGroup, appLovinSdk, appLovinAdSize, context);
                if (m.b(attributeSet)) {
                    loadNextAd();
                }
            }
        }
    }

    public boolean isAdReadyToDisplay() {
        return this.b.getAdService().hasPreloadedAd(this.e);
    }

    public boolean isAutoDestroy() {
        return this.p;
    }

    public void loadNextAd() {
        if (this.b == null || this.g == null || this.a == null || !this.o) {
            Log.i(AppLovinLogger.SDK_TAG, "Unable to load next ad: AppLovinAdView is not initialized.");
        } else {
            this.c.loadNextAd(this.e, this.g);
        }
    }

    public void onAdHtmlLoaded(WebView webView) {
        if (this.m != null) {
            webView.setVisibility(0);
            try {
                if (this.t != null) {
                    this.t.adDisplayed(this.m);
                }
            } catch (Throwable th) {
                this.d.userError("AppLovinAdView", "Exception while notifying ad display listener", th);
            }
        }
    }

    public void onDetachedFromWindow() {
        if (this.o) {
            a((Runnable) new h(this, this.m));
            if (this.p) {
                destroy();
            }
        }
    }

    public void onVisibilityChanged(int i2) {
        if (!this.o || !this.p) {
            return;
        }
        if (i2 == 8 || i2 == 4) {
            pause();
        } else if (i2 == 0) {
            resume();
        }
    }

    public void pause() {
        if (this.o) {
            this.c.removeAdUpdateListener(this.g, getSize());
            AppLovinAd appLovinAd = this.m;
            renderAd(this.i);
            if (appLovinAd != null) {
                this.n.set(appLovinAd);
            }
            this.q = true;
        }
    }

    public void renderAd(AppLovinAd appLovinAd) {
        if (appLovinAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (!this.o) {
            Log.i(AppLovinLogger.SDK_TAG, "Unable to render ad: AppLovinAdView is not initialized.");
        } else if (appLovinAd != this.m) {
            this.d.d("AppLovinAdView", "Rendering ad # " + appLovinAd.getAdIdNumber() + " (" + appLovinAd.getSize() + ")");
            a((Runnable) new h(this, this.m));
            this.n.set(null);
            this.m = appLovinAd;
            if (appLovinAd.getSize() == this.e) {
                a(this.j);
            } else if (appLovinAd.getSize() == AppLovinAdSize.INTERSTITIAL) {
                a(this.l);
                a(this.k);
            }
            new bh(this.b).a();
        } else {
            this.d.w("AppLovinAdView", "Ad # " + appLovinAd.getAdIdNumber() + " is already showing, ignoring");
        }
    }

    public void resume() {
        if (this.o) {
            if (this.r) {
                this.c.addAdUpdateListener(this.g, this.e);
            }
            AppLovinAd appLovinAd = (AppLovinAd) this.n.getAndSet(null);
            if (appLovinAd != null) {
                renderAd(appLovinAd);
            }
            this.q = false;
        }
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.v = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.t = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.s = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.u = appLovinAdVideoPlaybackListener;
    }

    public void setAutoDestroy(boolean z) {
        this.p = z;
    }
}
