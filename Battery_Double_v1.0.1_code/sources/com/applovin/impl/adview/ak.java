package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.v4.view.ViewCompat;
import com.applovin.sdk.AppLovinSdk;

public class ak extends s {
    private float c = 30.0f;
    private float d = 2.0f;
    private float e = 10.0f;
    private float f = 3.0f;
    private float g = 1.0f;

    public ak(AppLovinSdk appLovinSdk, Context context) {
        super(appLovinSdk, context);
    }

    /* access modifiers changed from: protected */
    public float a() {
        return this.c * this.g;
    }

    public void a(float f2) {
        this.g = f2;
    }

    public void a(int i) {
        a(((float) i) / this.c);
    }

    /* access modifiers changed from: protected */
    public float b() {
        return this.e * this.g;
    }

    /* access modifiers changed from: protected */
    public float c() {
        return this.f * this.g;
    }

    /* access modifiers changed from: protected */
    public float d() {
        return a() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public float e() {
        return this.d * this.g;
    }

    /* access modifiers changed from: protected */
    public float f() {
        return d() - e();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float d2 = d();
        Paint paint = new Paint(1);
        paint.setColor(-1);
        canvas.drawCircle(d2, d2, d2, paint);
        Paint paint2 = new Paint(1);
        paint2.setColor(ViewCompat.MEASURED_STATE_MASK);
        canvas.drawCircle(d2, d2, f(), paint2);
        Paint paint3 = new Paint(paint);
        paint3.setStyle(Style.STROKE);
        paint3.setStrokeWidth(c());
        float b = b();
        float a = a() - b;
        canvas.drawLine(b, b, a, a, paint3);
        canvas.drawLine(b, a, a, b, paint3);
    }
}
