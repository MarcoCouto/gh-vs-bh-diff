package com.applovin.adview;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.applovin.impl.adview.AppLovinVideoView;
import com.applovin.impl.adview.ad;
import com.applovin.impl.adview.s;
import com.applovin.impl.adview.u;
import com.applovin.impl.sdk.AppLovinAdInternal;
import com.applovin.impl.sdk.AppLovinAdInternal.AdTarget;
import com.applovin.impl.sdk.AppLovinSdkImpl;
import com.applovin.impl.sdk.bf;
import com.applovin.impl.sdk.cd;
import com.applovin.impl.sdk.m;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdk;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.UUID;

public class AppLovinInterstitialActivity extends Activity implements u {
    public static final String KEY_WRAPPER_ID = "com.applovin.interstitial.wrapper_id";
    public static volatile ad lastKnownWrapper = null;
    private volatile UUID A;
    private AppLovinAdView a;
    /* access modifiers changed from: private */
    public ad b;
    private volatile boolean c = false;
    /* access modifiers changed from: private */
    public AppLovinLogger d;
    /* access modifiers changed from: private */
    public bf e;
    private AppLovinSdkImpl f;
    /* access modifiers changed from: private */
    public volatile AppLovinAdInternal g = cd.a();
    private volatile boolean h = false;
    /* access modifiers changed from: private */
    public volatile boolean i = false;
    private volatile boolean j = false;
    private volatile boolean k = false;
    private volatile boolean l = false;
    /* access modifiers changed from: private */
    public volatile boolean m = false;
    /* access modifiers changed from: private */
    public volatile boolean n = false;
    /* access modifiers changed from: private */
    public volatile boolean o = false;
    private volatile boolean p = false;
    private boolean q = false;
    /* access modifiers changed from: private */
    public Handler r;
    private FrameLayout s;
    /* access modifiers changed from: private */
    public AppLovinVideoView t;
    /* access modifiers changed from: private */
    public s u;
    /* access modifiers changed from: private */
    public View v;
    /* access modifiers changed from: private */
    public s w;
    /* access modifiers changed from: private */
    public View x;
    private TextView y;
    private int z;

    private static int a(Display display) {
        if (display.getWidth() == display.getHeight()) {
            return 3;
        }
        return display.getWidth() < display.getHeight() ? 1 : 2;
    }

    private void a(float f2) {
        float f3 = 1.0f;
        float f4 = 0.0f;
        if (!this.e.d().equals("left_to_right")) {
            f4 = 1.0f;
            f3 = 0.0f;
        }
        a(f4, f3, f2);
    }

    private void a(float f2, float f3, float f4) {
        try {
            ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, 1.0f, 1.0f);
            scaleAnimation.setDuration(cd.b(f4));
            scaleAnimation.setInterpolator(new LinearInterpolator());
            scaleAnimation.setAnimationListener(new e(this));
            this.x.startAnimation(scaleAnimation);
        } catch (Throwable th) {
            this.x.setVisibility(8);
        }
    }

    private void a(float f2, float f3, int i2) {
        float f4;
        float f5;
        float f6 = f3 / f2;
        if (this.e.d().equals("left_to_right")) {
            f4 = f6;
            f5 = 1.0f;
        } else {
            f4 = 1.0f - f6;
            f5 = 0.0f;
        }
        a(f4, f5, ((float) i2) - cd.a(1.0f));
        a((int) Math.floor(cd.a((long) i2)));
    }

    private void a(int i2) {
        a(i2, this.A);
    }

    /* access modifiers changed from: private */
    public void a(int i2, UUID uuid) {
        if (!uuid.equals(this.A)) {
            return;
        }
        if (i2 <= 0) {
            this.y.setVisibility(8);
            return;
        }
        this.y.setVisibility(0);
        int i3 = i2 - 1;
        this.y.setText(Integer.toString(i3));
        this.r.postDelayed(new f(this, i3, uuid), 1000);
    }

    private void a(long j2, s sVar) {
        this.r.postDelayed(new q(this, sVar), j2);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        AppLovinAdDisplayListener d2 = this.b.d();
        if (d2 != null) {
            d2.adDisplayed(appLovinAd);
        }
        this.i = true;
    }

    private void a(AppLovinAd appLovinAd, double d2, boolean z2) {
        this.l = true;
        AppLovinAdVideoPlaybackListener c2 = this.b.c();
        if (c2 != null) {
            c2.videoPlaybackEnded(appLovinAd, d2, z2);
        }
    }

    private boolean a() {
        if (this.b == null || this.e == null || this.e.a()) {
            return true;
        }
        if (!this.e.c() || !this.n) {
            return this.e.b() && this.p;
        }
        return true;
    }

    private int b(int i2) {
        return cd.a((Context) this, i2);
    }

    private void b() {
        Editor edit = u().edit();
        edit.putBoolean("com.applovin.interstitial.should_resume_video", false);
        edit.putInt("com.applovin.interstitial.last_video_position", 0);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        c(appLovinAd);
        dismiss();
    }

    private void c() {
        this.z = m.a(this).x;
        Uri fromFile = Uri.fromFile(this.f.getFileManager().a(this.g.getVideoFilename(), this, false));
        this.t = new AppLovinVideoView(this);
        this.t.setOnPreparedListener(new b(this));
        this.t.setOnCompletionListener(new i(this));
        this.t.setOnErrorListener(new j(this));
        this.t.setVideoURI(fromFile);
        this.t.setLayoutParams(new LayoutParams(-1, -1, 17));
        this.s.addView(this.t);
        setContentView(this.s);
        n();
        k();
        l();
        m();
        f();
    }

    private void c(AppLovinAd appLovinAd) {
        if (!this.j) {
            this.j = true;
            if (this.b != null) {
                AppLovinAdDisplayListener d2 = this.b.d();
                if (d2 != null) {
                    d2.adHidden(appLovinAd);
                }
            }
        }
    }

    private void d() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.s = new FrameLayout(this);
        this.s.setLayoutParams(layoutParams);
        this.s.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.r = new Handler();
    }

    private void d(AppLovinAd appLovinAd) {
        if (!this.k) {
            this.k = true;
            AppLovinAdVideoPlaybackListener c2 = this.b.c();
            if (c2 != null) {
                c2.videoPlaybackBegan(appLovinAd);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.e.k()) {
            finish();
        } else {
            h();
        }
    }

    private void f() {
        d((AppLovinAd) this.g);
        this.t.start();
        a(cd.a((float) this.g.getCountdownLength()));
        p();
    }

    private void g() {
        this.u = s.a(this.f, this, this.g.getCloseStyle());
        this.u.setVisibility(8);
        this.u.setOnClickListener(new l(this));
        int b2 = b(this.e.m());
        LayoutParams layoutParams = new LayoutParams(b2, b2, 53);
        this.u.a(b2);
        int b3 = b(this.e.o());
        int b4 = b(this.e.q());
        layoutParams.setMargins(0, b3, b4, 0);
        this.s.addView(this.u, layoutParams);
        if (this.e.r() > 0) {
            int b5 = b(new bf(this.f).r());
            this.v = new View(this);
            this.v.setBackgroundColor(0);
            this.v.setVisibility(8);
            LayoutParams layoutParams2 = new LayoutParams(b2 + b5, b5 + b2, 53);
            layoutParams2.setMargins(0, b3 - b(5), b4 - b(5), 0);
            this.v.setOnClickListener(new m(this));
            this.s.addView(this.v, layoutParams2);
            this.v.bringToFront();
        }
        this.w = s.a(this.f, this, this.g.getCloseStyle());
        this.w.setVisibility(8);
        this.w.setOnClickListener(new n(this));
        LayoutParams layoutParams3 = new LayoutParams(b2, b2, 51);
        layoutParams3.setMargins(b4, b3, 0, 0);
        this.w.a(b2);
        this.s.addView(this.w, layoutParams3);
        this.w.bringToFront();
    }

    /* access modifiers changed from: private */
    public void h() {
        runOnUiThread(new o(this));
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return this.e.r() > 0 && this.v != null;
    }

    /* access modifiers changed from: private */
    public void j() {
        runOnUiThread(new p(this));
    }

    private void k() {
        if (this.g.getVideoCloseDelay() >= 0.0f) {
            a(cd.c(this.g.getVideoCloseDelay()), (!this.q || this.w == null) ? this.u : this.w);
        }
    }

    private void l() {
        this.x = new View(this);
        this.x.setBackgroundColor(o());
        LayoutParams layoutParams = new LayoutParams(this.z, b(this.e.g()), 81);
        if (this.e.h() && this.g.getCountdownLength() > 0) {
            this.s.addView(this.x, layoutParams);
            this.x.bringToFront();
        }
    }

    private void m() {
        this.y = new TextView(this);
        this.y.setTextColor(o());
        this.y.setTextSize(0, ((float) b(22)) * 0.8f);
        this.y.setText(Integer.toString(this.g.getCountdownLength()));
        LayoutParams layoutParams = new LayoutParams(-2, -2, 83);
        layoutParams.setMargins(b(10), 0, this.x.getHeight() + b(3), 0);
        if (this.e.i() && this.g.getCountdownLength() > 0) {
            this.s.addView(this.y, layoutParams);
            this.y.bringToFront();
        }
    }

    private void n() {
        this.A = UUID.randomUUID();
    }

    private int o() {
        return Color.parseColor(this.e.e());
    }

    private void p() {
        a(this.g.getCountdownLength() + 1);
    }

    /* access modifiers changed from: private */
    public void q() {
        this.t.stopPlayback();
        s();
    }

    private void r() {
        if (this.c) {
            return;
        }
        if (this.a != null) {
            this.a.setAdDisplayListener(new g(this));
            this.a.setAdClickListener(new h(this));
            this.g = (AppLovinAdInternal) this.b.b();
            d();
            g();
            if (this.g.getVideoFilename() != null) {
                c();
            } else {
                this.h = true;
                this.f.getLogger().e("AppLovinInterstitialActivity", "Video file was missing or null - please make sure your app has the WRITE_EXTERNAL_STORAGE permission!");
                s();
            }
            this.u.bringToFront();
            if (i()) {
                this.v.bringToFront();
            }
            if (this.w != null) {
                this.w.bringToFront();
            }
            this.a.renderAd(this.g);
            this.b.a(true);
            return;
        }
        exitWithError("AdView was null");
    }

    /* access modifiers changed from: private */
    public void s() {
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.setBackgroundColor(-1157627904);
        frameLayout.addView(this.a);
        if (this.w != null) {
            this.s.removeView(this.w);
        }
        if (i()) {
            this.s.removeView(this.v);
            frameLayout.addView(this.v);
            this.v.bringToFront();
        }
        this.s.removeView(this.u);
        frameLayout.addView(this.u);
        setContentView(frameLayout);
        this.u.bringToFront();
        if (this.g.getPoststitialCloseDelay() > 0.0f) {
            a(cd.c(this.g.getPoststitialCloseDelay()), this.u);
        } else {
            this.u.setVisibility(0);
        }
        this.p = true;
        t();
    }

    private void t() {
        double d2 = 100.0d;
        if (!this.l) {
            if (!this.m) {
                if (this.t != null) {
                    d2 = 100.0d * (((double) this.t.getCurrentPosition()) / ((double) this.t.getDuration()));
                } else {
                    Log.e("AppLovinInterstitialActivity", "No video view detected on video end");
                    d2 = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
                }
            }
            a((AppLovinAd) this.g, d2, d2 > 95.0d);
        }
    }

    private SharedPreferences u() {
        return getSharedPreferences("com.applovin.interstitial.sharedpreferences", 0);
    }

    public void dismiss() {
        b();
        t();
        if (this.b != null) {
            if (this.g != null) {
                c((AppLovinAd) this.g);
            }
            this.b.a(false);
            this.b.g();
        }
        finish();
    }

    public void exitWithError(String str) {
        try {
            Log.e("AppLovinInterstitialActivity", "Failed to properly render an Interstitial Activity, due to error: " + str, new Throwable("Initialized = " + ad.a + "; CleanedUp = " + ad.b));
            c((AppLovinAd) cd.a());
        } catch (Exception e2) {
            Log.e("AppLovinInterstitialActivity", "Failed to show a video ad due to error:", e2);
        }
        finish();
    }

    public void onBackPressed() {
        if (a()) {
            this.d.d("AppLovinInterstitialActivity", "Back button was pressed; forwarding to Android for handling...");
            super.onBackPressed();
            return;
        }
        try {
            if (this.q && this.w != null && this.w.getVisibility() == 0 && this.w.getAlpha() > 0.0f && !this.n) {
                this.d.d("AppLovinInterstitialActivity", "Back button was pressed; forwarding as a click to skip button.");
                this.w.performClick();
            } else if (this.u == null || this.u.getVisibility() != 0 || this.u.getAlpha() <= 0.0f) {
                this.d.d("AppLovinInterstitialActivity", "Back button was pressed, but was not eligible for dismissal.");
            } else {
                this.d.d("AppLovinInterstitialActivity", "Back button was pressed; forwarding as a click to close button.");
                this.u.performClick();
            }
        } catch (Exception e2) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        try {
            getWindow().setFlags(1024, 1024);
        } catch (Throwable th) {
        }
        setTheme(16973841);
        String stringExtra = getIntent().getStringExtra(KEY_WRAPPER_ID);
        if (stringExtra == null || stringExtra.isEmpty()) {
            exitWithError("Wrapper ID is null");
        } else {
            this.b = ad.a(stringExtra);
            if (this.b == null && lastKnownWrapper != null) {
                this.b = lastKnownWrapper;
            }
            if (this.b != null) {
                AppLovinAd b2 = this.b.b();
                this.f = (AppLovinSdkImpl) this.b.a();
                this.d = this.b.a().getLogger();
                this.e = new bf(this.b.a());
                if (b2 != null) {
                    Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
                    int a2 = a(defaultDisplay);
                    int rotation = defaultDisplay.getRotation();
                    boolean z2 = (a2 == 2 && rotation == 0) || (a2 == 2 && rotation == 2) || ((a2 == 1 && rotation == 1) || (a2 == 1 && rotation == 3));
                    if (this.b.f() == AdTarget.ACTIVITY_PORTRAIT) {
                        if (z2) {
                            if (!(rotation == 1 || rotation == 3)) {
                                this.c = true;
                                setRequestedOrientation(1);
                            }
                        } else if (!(rotation == 0 || rotation == 2)) {
                            this.c = true;
                            setRequestedOrientation(1);
                        }
                    } else if (z2) {
                        if (!(rotation == 0 || rotation == 2)) {
                            this.c = true;
                            setRequestedOrientation(0);
                        }
                    } else if (!(rotation == 1 || rotation == 3)) {
                        this.c = true;
                        setRequestedOrientation(0);
                    }
                    this.a = new AppLovinAdView((AppLovinSdk) this.f, AppLovinAdSize.INTERSTITIAL, (Activity) this);
                    this.a.setAutoDestroy(false);
                    this.b.a((u) this);
                    this.q = this.e.s();
                } else {
                    exitWithError("No current ad found.");
                }
            } else {
                exitWithError("Wrapper is null; initialized state: " + Boolean.toString(ad.a));
            }
        }
        Editor edit = u().edit();
        edit.putBoolean("com.applovin.interstitial.should_resume_video", false);
        edit.commit();
        b();
        r();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.a != null) {
                this.a.destroy();
            }
            if (this.t != null) {
                this.t.pause();
                this.t.stopPlayback();
            }
        } catch (Throwable th) {
            this.d.w("AppLovinInterstitialActivity", "Unable to destroy video view", th);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (!this.c && !this.h) {
            Editor edit = u().edit();
            edit.putInt("com.applovin.interstitial.last_video_position", this.t.getCurrentPosition());
            edit.putBoolean("com.applovin.interstitial.should_resume_video", true);
            edit.commit();
            this.x.clearAnimation();
            this.s.removeView(this.x);
            this.s.removeView(this.y);
            this.t.pause();
        }
        this.b.a(false);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b.a(true);
        SharedPreferences u2 = u();
        if (u2.getBoolean("com.applovin.interstitial.should_resume_video", false)) {
            if (this.t != null) {
                int duration = this.t.getDuration();
                int i2 = u2.getInt("com.applovin.interstitial.last_video_position", duration);
                int i3 = duration - i2;
                l();
                m();
                n();
                this.t.seekTo(i2);
                this.t.start();
                a((float) duration, (float) i2, i3);
            }
            if (this.u == null || !this.e.j()) {
                dismiss();
                return;
            }
            this.d.d("AppLovinInterstitialActivity", "Fading in close button due to app resume.");
            a(0, (!this.q || this.w == null) ? this.u : this.w);
        }
    }
}
