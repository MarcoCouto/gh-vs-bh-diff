package com.dlten.lib.frmWork;

import android.os.Handler;
import android.os.Message;
import com.dlten.lib.CBaseView;
import com.dlten.lib.Common;
import com.dlten.lib.graphics.CImgObj;
import com.dlten.lib.graphics.CPoint;
import java.util.Vector;

public abstract class CEventWnd {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_V_FILTER = 7;
    public static int CODE_HEIGHT = CBaseView.CODE_HEIGHT;
    public static int CODE_WIDTH = CBaseView.CODE_WIDTH;
    public static int HALF_HEIGHT = CBaseView.HALF_HEIGHT;
    public static int HALF_WIDTH = CBaseView.HALF_WIDTH;
    public static final int KEY_0 = 1;
    public static final int KEY_1 = 2;
    public static final int KEY_2 = 4;
    public static final int KEY_3 = 8;
    public static final int KEY_4 = 16;
    public static final int KEY_5 = 32;
    public static final int KEY_6 = 64;
    public static final int KEY_7 = 128;
    public static final int KEY_8 = 256;
    public static final int KEY_9 = 512;
    public static final int KEY_BACK = 4096;
    public static final int KEY_DIAL = 8192;
    public static final int KEY_DOWN = 32768;
    public static final int KEY_HOME = 2048;
    public static final int KEY_LEFT = 65536;
    public static final int KEY_MENU = 1024;
    public static final int KEY_NONE = 0;
    public static final int KEY_RIGHT = 131072;
    public static final int KEY_SELECT = 262144;
    public static final int KEY_UP = 16384;
    public static final int NET_EVT_CLOSED = 1;
    public static final int NET_EVT_CONNECTED = 0;
    public static final int NET_EVT_RECVDATA = 2;
    public static int REAL_HEIGHT = CBaseView.RES_HEIGHT;
    public static int REAL_WIDTH = CBaseView.RES_WIDTH;
    public static final int WM_ANIM_EVENT = 16;
    public static final int WM_APP_EXIT = 18;
    public static final int WM_COMMAND = 3;
    public static final int WM_KEY_DOWN = 12;
    public static final int WM_KEY_PRESS = 14;
    public static final int WM_KEY_UP = 13;
    public static final int WM_NET = 17;
    public static final int WM_PAINT = 2;
    public static final int WM_QUIT = 15;
    public static final int WM_RESIZE = 19;
    public static final int WM_RESUME = 6;
    public static final int WM_SUSPEND = 5;
    public static final int WM_TIMER = 4;
    public static final int WM_TOUCH_DOWN = 8;
    public static final int WM_TOUCH_MOVE = 10;
    public static final int WM_TOUCH_UP = 9;
    public static final int WM_USER = 1024;
    private static CBaseView m_baseView = null;
    private static Handler m_handleUpdateActivity = new Handler() {
        public void handleMessage(Message msg) {
            CEventWnd.getView().getActivity().onRecvMessage(msg.what, msg.arg1, msg.arg2);
        }
    };
    protected Vector<CAnimation> m_Anims = new Vector<>(10, 5);
    protected Vector<CButton> m_Btns = new Vector<>(10, 5);
    private boolean m_bEnable = true;
    private CButton m_btnFocus = null;
    protected CEventWnd m_pActiveWnd = null;
    protected CEventWnd m_pForegroundWnd = null;
    protected CEventWnd m_pModalessWnd = null;
    protected CEventWnd m_pParent = null;
    public String m_strName = null;

    public abstract void DrawPrevProc();

    public abstract void DrawProcess();

    public CEventWnd() {
        m_baseView.clearMsgQueue();
    }

    public void OnLoadResource() {
    }

    public void OnInitWindow() {
    }

    public void OnShowWindow() {
    }

    public void OnDestroy() {
        RemoveAllButtons();
    }

    public final int RunProc() {
        return m_baseView.RunProc(this);
    }

    public void OnSuspend() {
    }

    public void OnResume() {
    }

    public int WindowProc(int message, int wParam, int lParam) {
        switch (message) {
            case 2:
                UpdateWindow();
                break;
            case 3:
                OnCommand(wParam);
                break;
            case 8:
                if (!setFocusButton(wParam, lParam)) {
                    OnTouchDown(wParam, lParam);
                    break;
                }
                break;
            case 9:
                if (!clickFocusButton(wParam, lParam)) {
                    OnTouchUp(wParam, lParam);
                    break;
                }
                break;
            case 10:
                if (!changeFocusButton(wParam, lParam)) {
                    OnTouchMove(wParam, lParam);
                    break;
                }
                break;
            case 12:
                OnKeyDown(wParam);
                break;
            case 13:
                OnKeyUp(wParam);
                break;
            case 14:
                OnKeyPress(wParam);
                break;
            case 16:
                OnAnimEvent((short) wParam);
                break;
        }
        return 0;
    }

    private CButton findButton(int x, int y) {
        CPoint pt = new CPoint(x, y);
        CButton btnFocus = null;
        for (int i = 0; i < this.m_Btns.size(); i++) {
            CButton btn = (CButton) this.m_Btns.elementAt(i);
            if (btn.isUseful() && btn.isInside(pt)) {
                btnFocus = btn;
            }
        }
        return btnFocus;
    }

    private boolean setFocusButton(int x, int y) {
        CButton btnFocus = findButton(x, y);
        if (btnFocus == null) {
            if (this.m_btnFocus != null) {
                this.m_btnFocus.setNormal();
                this.m_btnFocus = null;
            }
            return false;
        }
        if (!(btnFocus == this.m_btnFocus || this.m_btnFocus == null)) {
            this.m_btnFocus.setNormal();
            this.m_btnFocus = null;
        }
        btnFocus.setFocus();
        this.m_btnFocus = btnFocus;
        return true;
    }

    private boolean changeFocusButton(int x, int y) {
        if (this.m_btnFocus == null || !this.m_btnFocus.isVisible()) {
            return false;
        }
        if (this.m_btnFocus.isEnable()) {
            if (this.m_btnFocus.isInside(new CPoint(x, y))) {
                this.m_btnFocus.setFocus();
            } else {
                this.m_btnFocus.setNormal();
            }
        }
        return true;
    }

    private boolean clickFocusButton(int x, int y) {
        if (this.m_btnFocus == null || !this.m_btnFocus.isUseful()) {
            return false;
        }
        CButton btnFocus = this.m_btnFocus;
        this.m_btnFocus = null;
        btnFocus.setNormal();
        if (btnFocus.isInside(new CPoint(x, y))) {
            SendMessage(3, btnFocus.getCommand(), 0);
        }
        return true;
    }

    public void OnKeyDown(int keycode) {
    }

    public void OnKeyUp(int keycode) {
    }

    public void OnKeyPress(int keycode) {
    }

    public void OnTouchDown(int nTouchX, int nTouchY) {
    }

    public void OnTouchUp(int nTouchX, int nTouchY) {
    }

    public void OnTouchMove(int nTouchX, int nTouchY) {
    }

    public void OnCommand(int nCmd) {
    }

    public void OnAnimEvent(int nAnimID) {
    }

    public int OnNetEvent(int nEvtType, int nParam, Object objData) {
        return 0;
    }

    public static void setView(CBaseView dc) {
        m_baseView = dc;
    }

    public static CBaseView getView() {
        return m_baseView;
    }

    public boolean AddAnimation(CAnimation pAnimObj) {
        if (this.m_Anims.indexOf(pAnimObj) != -1) {
            return false;
        }
        this.m_Anims.addElement(pAnimObj);
        return true;
    }

    public void RemoveAnimation(CAnimation pAnimObj) {
        this.m_Anims.removeElement(pAnimObj);
    }

    public CButton createButton(String norName, String focName, String disName) {
        CButton btn = new CButton();
        btn.create(this, norName, focName, disName);
        return btn;
    }

    public boolean AddButton(CButton pBtnObj) {
        if (this.m_Btns.indexOf(pBtnObj) != -1) {
            return false;
        }
        this.m_Btns.addElement(pBtnObj);
        return true;
    }

    public void RemoveButton(CButton pBtnObj) {
        this.m_Btns.removeElement(pBtnObj);
    }

    public void RemoveAllButtons() {
        while (this.m_Btns.size() > 0) {
            ((CButton) this.m_Btns.elementAt(0)).destroy();
        }
    }

    public boolean EnableWindow(boolean bEnable) {
        boolean bTemp = this.m_bEnable;
        this.m_bEnable = bEnable;
        return bTemp;
    }

    public boolean IsEnable() {
        return this.m_bEnable;
    }

    public void NotifyToParentEndRun() {
        if (this.m_pParent != null) {
            this.m_pParent.InitKeyState();
        }
    }

    private void InitKeyState() {
        m_baseView.DeleteMsgs(new int[]{12, 13});
    }

    public void SetParent(CEventWnd pParent) {
        this.m_pParent = pParent;
    }

    public CEventWnd SetActiveWnd(CEventWnd pActive) {
        CEventWnd pOldActiveWnd = this.m_pActiveWnd;
        this.m_pActiveWnd = pActive;
        return pOldActiveWnd;
    }

    public CEventWnd SetModalessWnd(CEventWnd pModaless) {
        CEventWnd pOldModalessWnd = this.m_pModalessWnd;
        this.m_pModalessWnd = pModaless;
        return pOldModalessWnd;
    }

    public CEventWnd SetForegroundWnd(CEventWnd pForegroundWnd) {
        CEventWnd pRet = this.m_pForegroundWnd;
        this.m_pForegroundWnd = pForegroundWnd;
        return pRet;
    }

    public void DestroyWindow(int nCode) {
        PostMessage(15, nCode);
    }

    public CEventWnd GetParent() {
        return this.m_pParent;
    }

    public CEventWnd GetActiveWnd() {
        return this.m_pActiveWnd;
    }

    public CEventWnd GetForegroundWnd() {
        return this.m_pForegroundWnd;
    }

    public CEventWnd GetModalessWnd() {
        return this.m_pModalessWnd;
    }

    public boolean SendMessage(int message) {
        SendMessage(message, 0);
        return true;
    }

    public boolean SendMessage(int message, int wParam) {
        SendMessage(message, wParam, 0);
        return true;
    }

    public boolean SendMessage(int message, int wParam, int lParam) {
        WindowProc(message, wParam, lParam);
        return true;
    }

    public boolean PostMessage(int message) {
        return PostMessage(message, 0);
    }

    public boolean PostMessage(int message, int wParam) {
        return PostMessage(message, wParam, 0);
    }

    public boolean PostMessage(int message, int wParam, int lParam) {
        if (!IsEnable() && message == 4) {
            return false;
        }
        m_baseView.PostMessage(message, wParam, lParam);
        return true;
    }

    public void postActivityMsg(int message, int wParam, int lParam) {
        Message msg = new Message();
        msg.what = message;
        msg.arg1 = wParam;
        msg.arg2 = lParam;
        m_handleUpdateActivity.sendMessage(msg);
    }

    public void UpdateWindow() {
        m_baseView.update(this);
    }

    public void Invalidate() {
        PostMessage(2);
    }

    public void setString(String strName) {
        this.m_strName = strName;
    }

    public void drawStr(int x, int y, int color, int drawType, String str) {
        CBaseView view = getView();
        view.setColor(color);
        view.drawString(str, (float) ((int) Common.code2screen((float) x)), (float) ((int) Common.code2screen((float) y)), drawType);
    }

    public void drawStr(int x, int y, String str) {
        drawStr(x, y, 16777215, 34, str);
    }

    public void drawRectStr(int x, int y, int width, int hieght, int color, int drawType, String str) {
        int x2 = (int) Common.code2screen((float) x);
        int y2 = (int) Common.code2screen((float) y);
        int width2 = (int) Common.code2screen((float) width);
        int hieght2 = (int) Common.code2screen((float) hieght);
        CBaseView view = getView();
        view.setColor(color);
        view.drawRectString(str, x2, y2, width2, hieght2, 0, -1, drawType);
    }

    public void drawRectStr(int x, int y, int color, int drawType, String str) {
        int x2 = (int) Common.code2screen((float) x);
        int y2 = (int) Common.code2screen((float) y);
        CBaseView view = getView();
        view.setColor(color);
        view.drawRectString(str, x2, y2, REAL_WIDTH, REAL_HEIGHT, 0, -1, drawType);
    }

    /* access modifiers changed from: protected */
    public CImgObj unload(CImgObj img) {
        if (img == null) {
            return img;
        }
        img.unload();
        return null;
    }

    public static void prepareDC() {
        CODE_WIDTH = CBaseView.CODE_WIDTH;
        CODE_HEIGHT = CBaseView.CODE_HEIGHT;
        REAL_WIDTH = CBaseView.RES_WIDTH;
        REAL_HEIGHT = CBaseView.RES_HEIGHT;
        HALF_WIDTH = CBaseView.HALF_WIDTH;
        HALF_HEIGHT = CBaseView.HALF_HEIGHT;
    }
}
