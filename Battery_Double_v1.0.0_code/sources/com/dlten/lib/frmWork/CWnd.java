package com.dlten.lib.frmWork;

import android.support.v4.view.MotionEventCompat;
import java.util.Timer;

public abstract class CWnd extends CEventWnd {
    public static final int ID_TIMER_0 = 0;
    public static final int ID_TIMER_1 = 1;
    public static final int ID_TIMER_2 = 2;
    public static final int ID_TIMER_3 = 3;
    public static final int ID_TIMER_4 = 4;
    private static final int ID_TIMER_COUNT = 5;
    static int fpscount = 0;
    private CTimerTask[] m_tasks = new CTimerTask[5];
    private Timer[] m_timers = new Timer[5];

    public CWnd() {
        for (int i = 0; i < this.m_tasks.length; i++) {
            this.m_tasks[i] = new CTimerTask(this);
            this.m_timers[i] = new Timer();
        }
    }

    private void DrawWindowName() {
        if (this.m_strName != null) {
            drawStr(0, 0, MotionEventCompat.ACTION_POINTER_INDEX_MASK, 17, this.m_strName);
        }
    }

    private void DrawFPS() {
        drawStr(CODE_WIDTH, 0, MotionEventCompat.ACTION_POINTER_INDEX_MASK, 65, String.format("%.1f ", new Object[]{Float.valueOf(getView().getFPS())}));
    }

    public void DrawPrevProc() {
        if (GetModalessWnd() != null) {
            GetModalessWnd().DrawPrevProc();
        }
    }

    public void DrawProcess() {
        OnPaint();
        int count = this.m_Anims.size();
        for (int i = 0; i < count; i++) {
            CAnimation animObj = (CAnimation) this.m_Anims.elementAt(i);
            if (animObj.IsDrawBylib()) {
                animObj.Draw();
            }
            animObj.UpdateFrame();
        }
        int count2 = this.m_Btns.size();
        for (int i2 = 0; i2 < count2; i2++) {
            ((CButton) this.m_Btns.elementAt(i2)).Draw();
        }
        if (GetModalessWnd() != null) {
            GetModalessWnd().DrawProcess();
        }
    }

    public void OnShowWindow() {
        super.OnShowWindow();
    }

    public void OnDestroy() {
        super.OnDestroy();
        for (int i = 0; i < this.m_tasks.length; i++) {
            this.m_tasks[i].cancel();
            this.m_timers[i].cancel();
            this.m_tasks[i] = null;
            this.m_timers[i] = null;
        }
        this.m_tasks = null;
        this.m_timers = null;
    }

    public void OnPaint() {
    }

    public void OnTimer(int nTimerID) {
    }

    public int OnNetEvent(int nEvtType, int nParam, Object objData) {
        return -1;
    }

    public int WindowProc(int message, int wParam, int lParam) {
        if (message != 4) {
            return super.WindowProc(message, wParam, lParam);
        }
        OnTimer(wParam);
        return 0;
    }

    public boolean SetTimer(int nTimerID, int nDelay) {
        return SetTimer(nTimerID, nDelay, null);
    }

    public boolean SetTimer(int nTimerID, int nDelay, CTimerListner listner) {
        if (nTimerID < 0 || nTimerID >= 5) {
            return false;
        }
        this.m_tasks[nTimerID] = new CTimerTask(this);
        this.m_tasks[nTimerID].SetTimer(nTimerID, listner);
        this.m_timers[nTimerID].scheduleAtFixedRate(this.m_tasks[nTimerID], 100, (long) nDelay);
        return true;
    }

    public void KillTimer(int nTimerID) {
        this.m_tasks[nTimerID].KillTimer();
    }

    public final int SwitchWindow(int nWndID) {
        return SwitchWindow(nWndID, 0);
    }

    public final int SwitchWindow(int nWndID, int nParam) {
        return CWndMgr.getInstance().SwitchingWnd(nWndID, nParam);
    }

    public final void Exit(int nRetCode) {
        DestroyWindow(0);
    }
}
