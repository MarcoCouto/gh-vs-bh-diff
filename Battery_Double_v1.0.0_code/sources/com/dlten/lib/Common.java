package com.dlten.lib;

import android.content.Context;
import com.dlten.lib.graphics.CDCView;

public class Common {
    public static int CODE_HEIGHT = 960;
    public static int CODE_WIDTH = 640;
    public static float FPS = 30.0f;
    public static int RES_HEIGHT = 720;
    public static int RES_WIDTH = 480;
    public static int SC_HEIGHT;
    public static int SC_WIDTH;
    private static float m_fDensity;
    private static float m_fScaleCode;
    private static float m_fScaleRes;
    private static int m_nOffsetHeight;

    public static void Initialize(Context context, int nScreenWidth, int nScreenHeight) {
        SC_WIDTH = nScreenWidth;
        SC_HEIGHT = nScreenHeight;
        m_fScaleRes = STD.MIN(((float) nScreenWidth) / ((float) RES_WIDTH), ((float) nScreenHeight) / ((float) RES_HEIGHT));
        m_fScaleCode = STD.MIN(((float) nScreenWidth) / ((float) CODE_WIDTH), ((float) nScreenHeight) / ((float) CODE_HEIGHT));
        CDCView.setCodeWidth(CODE_WIDTH);
        CDCView.setCodeHeight(CODE_HEIGHT);
        CDCView.setResWidth(RES_WIDTH);
        CDCView.setResHeight(RES_HEIGHT);
        CDCView.setScaleRes(m_fScaleRes);
        CDCView.setScaleCode(m_fScaleCode);
        m_nOffsetHeight = ((int) (((float) RES_HEIGHT) * m_fScaleRes)) - nScreenHeight;
        m_fDensity = context.getResources().getDisplayMetrics().density;
        STD.logout("Density=" + m_fDensity);
    }

    public static void Finalize() {
    }

    public static float getScaleRes() {
        return m_fScaleRes;
    }

    public static int getDiffHeight() {
        return m_nOffsetHeight;
    }

    public static int getHeightDip() {
        return (int) pixel2dip((float) SC_HEIGHT);
    }

    public static float RES_DIP_HEIGHT() {
        return res2dip((float) RES_HEIGHT);
    }

    public static float RES_DIP_WIDTH() {
        return res2dip((float) RES_WIDTH);
    }

    public static float SC_DIP_HEIGHT() {
        return pixel2dip((float) SC_HEIGHT);
    }

    public static float SC_DIP_WIDTH() {
        return pixel2dip((float) SC_WIDTH);
    }

    public static float code2res(float codePx) {
        return screen2res(code2screen(codePx));
    }

    public static float res2code(float resPx) {
        return screen2code(res2screen(resPx));
    }

    public static float code2screen(float codePx) {
        return m_fScaleCode * codePx;
    }

    public static float screen2code(float screenPx) {
        return screenPx / m_fScaleCode;
    }

    public static float res2screen(float resPx) {
        return m_fScaleRes * resPx;
    }

    public static float screen2res(float screenPx) {
        return screenPx / m_fScaleRes;
    }

    public static float pixel2dip(float pixels) {
        return pixels / m_fDensity;
    }

    public static float dip2pixel(float dips) {
        float den = m_fDensity;
        if (den != 1.0f) {
            return (float) ((int) (((double) (dips * den)) + 0.5d));
        }
        return dips;
    }

    public static float res2dip(float resPx) {
        return pixel2dip(res2screen(resPx));
    }

    public static float dip2res(float dips) {
        return screen2res(dip2pixel(dips));
    }
}
