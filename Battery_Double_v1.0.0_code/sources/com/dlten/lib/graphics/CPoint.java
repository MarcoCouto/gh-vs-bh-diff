package com.dlten.lib.graphics;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class CPoint {
    public float x;
    public float y;

    public CPoint() {
        this.x = BitmapDescriptorFactory.HUE_RED;
        this.y = BitmapDescriptorFactory.HUE_RED;
    }

    public CPoint(float initX, float initY) {
        this.x = initX;
        this.y = initY;
    }

    public CPoint(int initX, int initY) {
        this.x = (float) initX;
        this.y = (float) initY;
    }

    public CPoint(CPoint initPt) {
        this.x = initPt.x;
        this.y = initPt.y;
    }

    public CPoint(CSize initSize) {
        this.x = initSize.w;
        this.y = initSize.h;
    }

    public void Offset(float xOffset, float yOffset) {
        this.x += xOffset;
        this.y += yOffset;
    }

    public void Offset(int xOffset, int yOffset) {
        this.x += (float) xOffset;
        this.y += (float) yOffset;
    }

    public void Offset(CPoint point) {
        this.x += point.x;
        this.y += point.y;
    }

    public void Offset(CSize size) {
        this.x += size.w;
        this.y += size.h;
    }
}
