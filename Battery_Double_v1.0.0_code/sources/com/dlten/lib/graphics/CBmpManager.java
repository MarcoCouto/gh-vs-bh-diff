package com.dlten.lib.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.io.InputStream;

public class CBmpManager {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_V_FILTER = 7;
    public static final int TRANS_MIRROR = 1;
    public static final int TRANS_MIRROR_ROT180 = 3;
    public static final int TRANS_MIRROR_ROT90 = 4;
    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT180 = 2;
    private static Options m_sBmpOption = new Options();

    public static void Initialize() {
        m_sBmpOption.inPreferredConfig = Config.RGB_565;
    }

    public static Bitmap loadImage(Context context, String strAssetName) {
        byte[] byResult;
        try {
            InputStream is = context.getAssets().open(strAssetName);
            if (is == null) {
                return null;
            }
            byResult = new byte[is.available()];
            is.read(byResult);
            is.close();
            return loadImage(byResult);
        } catch (Exception e) {
            byResult = null;
        }
    }

    public static Bitmap loadImage(Resources res, int resID) {
        return BitmapFactory.decodeResource(res, resID, m_sBmpOption);
    }

    public static Bitmap loadImage(byte[] byData) {
        if (byData == null) {
            return null;
        }
        return loadImage(byData, 0, byData.length);
    }

    public static Bitmap loadImage(byte[] byData, int nOffset, int nLen) {
        Bitmap img = null;
        try {
            return BitmapFactory.decodeByteArray(byData, nOffset, nLen, m_sBmpOption);
        } catch (Exception e) {
            e.printStackTrace();
            return img;
        }
    }

    public static int[] getRGBData(Bitmap img) {
        if (img == null) {
            return null;
        }
        return getRGBData(img, 0, 0, img.getWidth(), img.getHeight());
    }

    public static int[] getRGBData(Bitmap img, int x, int y, int w, int h) {
        if (img == null) {
            return null;
        }
        int[] nRet = new int[(w * h)];
        try {
            img.getPixels(nRet, 0, w, x, y, w, h);
            return nRet;
        } catch (Exception e) {
            e.printStackTrace();
            return nRet;
        }
    }

    public static float[] getLeftTopPos(float fWidth, float fHeight, int nAnchor) {
        float[] pos = new float[2];
        switch (nAnchor & 112) {
            case 32:
                pos[0] = BitmapDescriptorFactory.HUE_RED - (fWidth / 2.0f);
                break;
            case 64:
                pos[0] = BitmapDescriptorFactory.HUE_RED - fWidth;
                break;
        }
        switch (nAnchor & 7) {
            case 2:
                pos[1] = BitmapDescriptorFactory.HUE_RED - (fHeight / 2.0f);
                break;
            case 4:
                pos[1] = BitmapDescriptorFactory.HUE_RED - fHeight;
                break;
        }
        return pos;
    }
}
