package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.dlten.lib.file.CResFile;

public class CImage {
    public static final int ANCHOR_BOTTOM = 4;
    public static final int ANCHOR_CENTER = 32;
    public static final int ANCHOR_HCENTER = 32;
    public static final int ANCHOR_H_FILTER = 112;
    public static final int ANCHOR_LEFT = 16;
    public static final int ANCHOR_MIDDLE = 2;
    public static final int ANCHOR_RIGHT = 64;
    public static final int ANCHOR_TOP = 1;
    public static final int ANCHOR_VCENTER = 2;
    public static final int ANCHOR_V_FILTER = 7;
    private static CDCView m_dcView = null;
    protected byte m_byCols = 0;
    protected byte m_byRows = 0;
    protected int m_nCHeight = 0;
    protected int m_nCWidth = 0;
    protected int m_totalHeight = 0;
    protected int m_totalWidth = 0;

    public static void Initialize(CDCView dcView) {
        m_dcView = dcView;
        CBmpManager.Initialize();
    }

    public static CDCView getDCView() {
        return m_dcView;
    }

    public static float[] getLeftTopPos(float fWidth, float fHeight, int nAnchor) {
        return CBmpManager.getLeftTopPos(fWidth, fHeight, nAnchor);
    }

    public void load(String strAsset) {
        load(strAsset, 1, 1);
    }

    public void load(String strAsset, int nCol, int nRow) {
        load(CResFile.load(strAsset));
        setColRows(nCol, nRow);
    }

    public void load(String strAsset, int nPer) {
        load(CResFile.load(strAsset), nPer);
        setColRows(1, 1);
    }

    public void load(int nResID) {
        load(nResID, 1, 1);
    }

    public void load(int nResID, int nCol, int nRow) {
        load(CResFile.load(nResID));
        setColRows(nCol, nRow);
    }

    public void load(Bitmap bmp) {
        load(bmp, 1, 1);
    }

    public void load(Bitmap bmp, int nPer) {
        int nNewHeight = (bmp.getHeight() * nPer) / 100;
        loadResource(Bitmap.createBitmap(bmp, 0, bmp.getHeight() - nNewHeight, bmp.getWidth(), nNewHeight));
        setColRows(1, 1);
    }

    public void load(Bitmap bmp, int nCol, int nRow) {
        loadResource(bmp);
        setColRows(nCol, nRow);
    }

    /* access modifiers changed from: protected */
    public void load(byte[] byData) {
        loadResource(CBmpManager.loadImage(byData));
    }

    /* access modifiers changed from: protected */
    public void load(byte[] byData, int nPer) {
        Bitmap bmp = CBmpManager.loadImage(byData);
        int h = (bmp.getHeight() * nPer) / 100;
        loadResource(Bitmap.createBitmap(bmp, 0, bmp.getHeight() - h, bmp.getWidth(), h));
    }

    public void unload() {
        unloadResource();
    }

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap img) {
        this.m_totalWidth = img.getWidth();
        this.m_totalHeight = img.getHeight();
        return -1;
    }

    public void setColRows(int nCol, int nRow) {
        this.m_byRows = (byte) nRow;
        this.m_byCols = (byte) nCol;
        if (this.m_byRows <= 0) {
            this.m_byRows = 1;
        }
        if (this.m_byCols <= 0) {
            this.m_byCols = 1;
        }
        calcSize();
        reloadImages();
    }

    /* access modifiers changed from: protected */
    public void calcSize() {
        this.m_nCWidth = this.m_totalWidth / this.m_byCols;
        this.m_nCHeight = this.m_totalHeight / this.m_byRows;
    }

    public int getWidth() {
        return this.m_totalWidth;
    }

    public int getHeight() {
        return this.m_totalHeight;
    }

    public int getCols() {
        return this.m_byCols;
    }

    public int getRows() {
        return this.m_byRows;
    }

    public int getCWidth() {
        return this.m_nCWidth;
    }

    public int getCHeight() {
        return this.m_nCHeight;
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
    }

    /* access modifiers changed from: protected */
    public void reloadImages() {
    }

    public void drawImage(float fPosX, float fPosY, float fScaleX, float fScaleY, int nAlpha, Matrix matrix) {
    }
}
