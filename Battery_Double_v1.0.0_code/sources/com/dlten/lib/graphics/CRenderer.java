package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.opengl.GLES10;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.os.SystemClock;
import com.dlten.lib.file.CResFile;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CRenderer implements Renderer {
    private int m_textureID;

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES10.glDisable(3024);
        GLES10.glHint(3152, 4353);
        GLES10.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        GLES10.glShadeModel(7425);
        GLES10.glEnable(2929);
        GLES10.glEnable(3553);
        int[] textures = new int[1];
        GLES10.glGenTextures(1, textures, 0);
        this.m_textureID = textures[0];
        GLES10.glBindTexture(3553, this.m_textureID);
        GLES10.glTexParameterf(3553, 10241, 9728.0f);
        GLES10.glTexParameterf(3553, 10240, 9729.0f);
        GLES10.glTexParameterf(3553, 10242, 33071.0f);
        GLES10.glTexParameterf(3553, 10243, 33071.0f);
        GLES10.glTexEnvf(8960, 8704, 7681.0f);
        Bitmap bitmap = CBmpManager.loadImage(CResFile.load("test.png"));
        GLUtils.texImage2D(3553, 0, bitmap, 0);
        bitmap.recycle();
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES10.glViewport(0, 0, width, height);
        float ratio = ((float) width) / ((float) height);
        GLES10.glMatrixMode(5889);
        GLES10.glLoadIdentity();
        GLES10.glFrustumf(-ratio, ratio, -1.0f, 1.0f, 3.0f, 7.0f);
    }

    public void onDrawFrame(GL10 gl) {
        GLES10.glTexEnvx(8960, 8704, 8448);
        GLES10.glClear(16640);
        GLES10.glMatrixMode(5888);
        GLES10.glLoadIdentity();
        GLU.gluLookAt(gl, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, -5.0f, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 1.0f, BitmapDescriptorFactory.HUE_RED);
        GLES10.glEnableClientState(32884);
        GLES10.glEnableClientState(32888);
        GLES10.glActiveTexture(33984);
        GLES10.glBindTexture(3553, this.m_textureID);
        GLES10.glTexParameterx(3553, 10242, 10497);
        GLES10.glTexParameterx(3553, 10243, 10497);
        GLES10.glRotatef(0.09f * ((float) ((int) (SystemClock.uptimeMillis() % 4000))), BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 1.0f);
    }
}
